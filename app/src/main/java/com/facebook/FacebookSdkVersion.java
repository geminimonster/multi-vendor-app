package com.facebook;

final class FacebookSdkVersion {
    public static final String BUILD = "4.42.0";

    FacebookSdkVersion() {
    }
}
