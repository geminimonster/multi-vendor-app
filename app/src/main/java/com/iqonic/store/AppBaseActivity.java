package com.iqonic.store;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.TextView;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import com.iqonic.store.activity.DashBoardActivity;
import com.iqonic.store.utils.Constants;
import com.iqonic.store.utils.extensions.AppExtensionsKt;
import com.iqonic.store.utils.extensions.ExtensionsKt;
import com.iqonic.store.utils.extensions.ExtensionsKt$launchActivity$1;
import com.store.proshop.R;
import io.github.inflationx.viewpump.ViewPumpContextWrapper;
import java.util.HashMap;
import java.util.Locale;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000j\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0016\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u0012\u0010\u001a\u001a\u00020\u000b2\b\u0010\u001b\u001a\u0004\u0018\u00010\u001cH\u0014J\u0018\u0010\u001d\u001a\u00020\u001c2\u0006\u0010\u001e\u001a\u00020\u001c2\u0006\u0010\u001f\u001a\u00020\u0004H\u0002J\u000e\u0010 \u001a\u00020\u000b2\u0006\u0010!\u001a\u00020\"J\b\u0010#\u001a\u00020\u000bH\u0007J\u0012\u0010$\u001a\u00020\u000b2\b\u0010%\u001a\u0004\u0018\u00010&H\u0014J\u0010\u0010'\u001a\u00020(2\u0006\u0010)\u001a\u00020*H\u0016J\b\u0010+\u001a\u00020\u000bH\u0014J\u000e\u0010,\u001a\u00020\u000b2\u0006\u0010-\u001a\u00020.J\u0010\u0010/\u001a\u00020\u000b2\u0006\u00100\u001a\u000201H\u0002J\u000e\u00102\u001a\u00020\u000b2\u0006\u0010-\u001a\u00020.J\u000e\u00103\u001a\u00020\u000b2\u0006\u00104\u001a\u00020(J\u0010\u00105\u001a\u00020\u001c2\u0006\u0010\u001e\u001a\u00020\u001cH\u0002R\u001c\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\bR\"\u0010\t\u001a\n\u0012\u0004\u0012\u00020\u000b\u0018\u00010\nX\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\f\u0010\r\"\u0004\b\u000e\u0010\u000fR\u001c\u0010\u0010\u001a\u0004\u0018\u00010\u0011X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0012\u0010\u0013\"\u0004\b\u0014\u0010\u0015R\u0010\u0010\u0016\u001a\u0004\u0018\u00010\u0017X\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0018\u001a\u00020\u0019X\u000e¢\u0006\u0002\n\u0000¨\u00066"}, d2 = {"Lcom/iqonic/store/AppBaseActivity;", "Landroidx/appcompat/app/AppCompatActivity;", "()V", "language", "Ljava/util/Locale;", "getLanguage", "()Ljava/util/Locale;", "setLanguage", "(Ljava/util/Locale;)V", "onNetworkRetry", "Lkotlin/Function0;", "", "getOnNetworkRetry", "()Lkotlin/jvm/functions/Function0;", "setOnNetworkRetry", "(Lkotlin/jvm/functions/Function0;)V", "primaryColor", "", "getPrimaryColor", "()Ljava/lang/String;", "setPrimaryColor", "(Ljava/lang/String;)V", "progressDialog", "Landroid/app/Dialog;", "themeApp", "", "attachBaseContext", "newBase", "Landroid/content/Context;", "changeLanguage", "context", "locale", "disableHardwareRendering", "v", "Landroid/view/View;", "mAppBarColor", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onOptionsItemSelected", "", "item", "Landroid/view/MenuItem;", "onResume", "setDetailToolbar", "mToolbar", "Landroidx/appcompat/widget/Toolbar;", "setStatusBarGradient", "activity", "Landroid/app/Activity;", "setToolbar", "showProgress", "show", "updateBaseContextLocale", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: AppBaseActivity.kt */
public class AppBaseActivity extends AppCompatActivity {
    private HashMap _$_findViewCache;
    private Locale language;
    private Function0<Unit> onNetworkRetry;
    private String primaryColor;
    private Dialog progressDialog;
    private int themeApp;

    public void _$_clearFindViewByIdCache() {
        HashMap hashMap = this._$_findViewCache;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    public View _$_findCachedViewById(int i) {
        if (this._$_findViewCache == null) {
            this._$_findViewCache = new HashMap();
        }
        View view = (View) this._$_findViewCache.get(Integer.valueOf(i));
        if (view != null) {
            return view;
        }
        View findViewById = findViewById(i);
        this._$_findViewCache.put(Integer.valueOf(i), findViewById);
        return findViewById;
    }

    public final Locale getLanguage() {
        return this.language;
    }

    public final void setLanguage(Locale locale) {
        this.language = locale;
    }

    public final Function0<Unit> getOnNetworkRetry() {
        return this.onNetworkRetry;
    }

    public final void setOnNetworkRetry(Function0<Unit> function0) {
        this.onNetworkRetry = function0;
    }

    public final String getPrimaryColor() {
        return this.primaryColor;
    }

    public final void setPrimaryColor(String str) {
        this.primaryColor = str;
    }

    public final void disableHardwareRendering(View view) {
        Intrinsics.checkParameterIsNotNull(view, "v");
        view.setLayerType(1, (Paint) null);
    }

    public final void setToolbar(Toolbar toolbar) {
        Intrinsics.checkParameterIsNotNull(toolbar, "mToolbar");
        setSupportActionBar(toolbar);
        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar == null) {
            Intrinsics.throwNpe();
        }
        supportActionBar.setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationIcon((int) R.drawable.ic_keyboard_backspace);
        ExtensionsKt.changeToolbarFont(toolbar);
        Drawable navigationIcon = toolbar.getNavigationIcon();
        if (navigationIcon == null) {
            Intrinsics.throwNpe();
        }
        navigationIcon.setColorFilter(Color.parseColor(AppExtensionsKt.getTextTitleColor()), PorterDuff.Mode.SRC_ATOP);
        toolbar.setTitleTextColor(Color.parseColor(AppExtensionsKt.getTextTitleColor()));
    }

    public final void setDetailToolbar(Toolbar toolbar) {
        Intrinsics.checkParameterIsNotNull(toolbar, "mToolbar");
        setSupportActionBar(toolbar);
        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar == null) {
            Intrinsics.throwNpe();
        }
        supportActionBar.setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationIcon((int) R.drawable.ic_keyboard_backspace);
        ExtensionsKt.changeToolbarFont(toolbar);
        Drawable navigationIcon = toolbar.getNavigationIcon();
        if (navigationIcon == null) {
            Intrinsics.throwNpe();
        }
        navigationIcon.setColorFilter(Color.parseColor(AppExtensionsKt.getAccentColor()), PorterDuff.Mode.SRC_ATOP);
        toolbar.setTitleTextColor(Color.parseColor(AppExtensionsKt.getTextTitleColor()));
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        boolean z = false;
        ExtensionsKt.switchToDarkMode(this, ShopHopApp.Companion.getAppTheme() == 1);
        super.onCreate(bundle);
        setStatusBarGradient(this);
        ShopHopApp.Companion.setNoInternetDialog((Dialog) null);
        if (this.progressDialog == null) {
            Dialog dialog = new Dialog(this);
            this.progressDialog = dialog;
            if (dialog == null) {
                Intrinsics.throwNpe();
            }
            Window window = dialog.getWindow();
            if (window == null) {
                Intrinsics.throwNpe();
            }
            window.setBackgroundDrawable(new ColorDrawable(0));
            Dialog dialog2 = this.progressDialog;
            if (dialog2 == null) {
                Intrinsics.throwNpe();
            }
            dialog2.setContentView(R.layout.custom_dialog);
            if (AppExtensionsKt.getPrimaryColor().length() > 0) {
                z = true;
            }
            if (z) {
                Dialog dialog3 = this.progressDialog;
                if (dialog3 == null) {
                    Intrinsics.throwNpe();
                }
                TextView textView = (TextView) dialog3.findViewById(R.id.tv_progress_msg);
                Intrinsics.checkExpressionValueIsNotNull(textView, "progressDialog!!.tv_progress_msg");
                ExtensionsKt.changePrimaryColor(textView);
            }
        }
        this.themeApp = ShopHopApp.Companion.getAppTheme();
        this.language = new Locale(ShopHopApp.Companion.getLanguage());
        if (!ExtensionsKt.isNetworkAvailable()) {
            AppExtensionsKt.openNetworkDialog(this, new AppBaseActivity$onCreate$1(this));
        }
    }

    private final void setStatusBarGradient(Activity activity) {
        boolean z = true;
        if (Build.VERSION.SDK_INT >= 23) {
            Window window = activity.getWindow();
            Window window2 = activity.getWindow();
            Intrinsics.checkExpressionValueIsNotNull(window2, "activity.window");
            View decorView = window2.getDecorView();
            Intrinsics.checkExpressionValueIsNotNull(decorView, "activity.window.decorView");
            Window window3 = activity.getWindow();
            Intrinsics.checkExpressionValueIsNotNull(window3, "activity.window");
            View decorView2 = window3.getDecorView();
            Intrinsics.checkExpressionValueIsNotNull(decorView2, "activity.window.decorView");
            decorView2.setSystemUiVisibility(decorView.getSystemUiVisibility() | 8192);
            window.addFlags(Integer.MIN_VALUE);
            if (AppExtensionsKt.getPrimaryColor().length() != 0) {
                z = false;
            }
            if (z) {
                Intrinsics.checkExpressionValueIsNotNull(window, "window");
                window.setStatusBarColor(ContextCompat.getColor(this, R.color.colorToolBarBackground));
                return;
            }
            Intrinsics.checkExpressionValueIsNotNull(window, "window");
            window.setStatusBarColor(Color.parseColor(AppExtensionsKt.getPrimaryColor()));
            return;
        }
        if (AppExtensionsKt.getPrimaryColor().length() != 0) {
            z = false;
        }
        if (z) {
            Window window4 = getWindow();
            Intrinsics.checkExpressionValueIsNotNull(window4, "window");
            window4.setStatusBarColor(ContextCompat.getColor(this, R.color.colorToolBarBackground));
            return;
        }
        Window window5 = getWindow();
        Intrinsics.checkExpressionValueIsNotNull(window5, "window");
        window5.setStatusBarColor(Color.parseColor(AppExtensionsKt.getPrimaryColor()));
    }

    public final void mAppBarColor() {
        ColorDrawable colorDrawable;
        ActionBar supportActionBar = getSupportActionBar();
        if (AppExtensionsKt.getSharedPrefInstance().getIntValue(Constants.SharedPref.MODE, 1) == 1) {
            colorDrawable = new ColorDrawable(Color.parseColor(AppExtensionsKt.getPrimaryColor()));
        } else {
            colorDrawable = new ColorDrawable(getColor(R.color.colorPrimary));
        }
        if (supportActionBar == null) {
            Intrinsics.throwNpe();
        }
        supportActionBar.setBackgroundDrawable(colorDrawable);
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        Intrinsics.checkParameterIsNotNull(menuItem, "item");
        if (menuItem.getItemId() == 16908332) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    public final void showProgress(boolean z) {
        if (!z) {
            try {
                Dialog dialog = this.progressDialog;
                if (dialog == null) {
                    Intrinsics.throwNpe();
                }
                if (dialog.isShowing() && !isFinishing()) {
                    Dialog dialog2 = this.progressDialog;
                    if (dialog2 == null) {
                        Intrinsics.throwNpe();
                    }
                    dialog2.dismiss();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (!isFinishing()) {
            Dialog dialog3 = this.progressDialog;
            if (dialog3 == null) {
                Intrinsics.throwNpe();
            }
            dialog3.setCanceledOnTouchOutside(false);
            Dialog dialog4 = this.progressDialog;
            if (dialog4 == null) {
                Intrinsics.throwNpe();
            }
            dialog4.show();
        }
    }

    private final Context changeLanguage(Context context, Locale locale) {
        Locale.setDefault(locale);
        Configuration configuration = new Configuration();
        configuration.setLocale(locale);
        configuration.setLayoutDirection(locale);
        Context createConfigurationContext = context.createConfigurationContext(configuration);
        Intrinsics.checkExpressionValueIsNotNull(createConfigurationContext, "context.createConfigurationContext(config)");
        return createConfigurationContext;
    }

    /* access modifiers changed from: protected */
    public void attachBaseContext(Context context) {
        ViewPumpContextWrapper.Companion companion = ViewPumpContextWrapper.Companion;
        if (context == null) {
            Intrinsics.throwNpe();
        }
        super.attachBaseContext(companion.wrap(updateBaseContextLocale(context)));
    }

    private final Context updateBaseContextLocale(Context context) {
        Locale locale = new Locale(AppExtensionsKt.getSharedPrefInstance().getStringValue(Constants.SharedPref.LANGUAGE, AppExtensionsKt.getLanguage()));
        Locale.setDefault(locale);
        return changeLanguage(context, locale);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        Locale locale = new Locale(ShopHopApp.Companion.getLanguage());
        int appTheme = ShopHopApp.Companion.getAppTheme();
        Locale locale2 = this.language;
        if (locale2 == null || !(!Intrinsics.areEqual((Object) locale, (Object) locale2))) {
            int i = this.themeApp;
            if (i != 0 && i != appTheme) {
                Bundle bundle = null;
                Intent intent = new Intent(this, DashBoardActivity.class);
                ExtensionsKt$launchActivity$1.INSTANCE.invoke(intent);
                if (Build.VERSION.SDK_INT >= 16) {
                    startActivityForResult(intent, -1, bundle);
                } else {
                    startActivityForResult(intent, -1);
                }
            }
        } else {
            recreate();
        }
    }
}
