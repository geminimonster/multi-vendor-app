package com.iqonic.store.network;

import kotlin.Metadata;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\f\u0018\u0000*\u0004\b\u0000\u0010\u00012\u00020\u0002B\u0017\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\b\u0010\u0005\u001a\u0004\u0018\u00018\u0000¢\u0006\u0002\u0010\u0006R\u001a\u0010\u0003\u001a\u00020\u0004X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0007\u0010\b\"\u0004\b\t\u0010\nR\u001e\u0010\u0005\u001a\u0004\u0018\u00018\u0000X\u000e¢\u0006\u0010\n\u0002\u0010\u000f\u001a\u0004\b\u000b\u0010\f\"\u0004\b\r\u0010\u000e¨\u0006\u0010"}, d2 = {"Lcom/iqonic/store/network/ApiError;", "T", "", "apiErrorCode", "", "error", "(ILjava/lang/Object;)V", "getApiErrorCode", "()I", "setApiErrorCode", "(I)V", "getError", "()Ljava/lang/Object;", "setError", "(Ljava/lang/Object;)V", "Ljava/lang/Object;", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: ApiError.kt */
public final class ApiError<T> {
    private int apiErrorCode;
    private T error;

    public ApiError(int i, T t) {
        this.apiErrorCode = i;
        this.error = t;
    }

    public final int getApiErrorCode() {
        return this.apiErrorCode;
    }

    public final T getError() {
        return this.error;
    }

    public final void setApiErrorCode(int i) {
        this.apiErrorCode = i;
    }

    public final void setError(T t) {
        this.error = t;
    }
}
