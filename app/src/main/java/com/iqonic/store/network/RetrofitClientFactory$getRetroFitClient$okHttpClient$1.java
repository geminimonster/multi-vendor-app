package com.iqonic.store.network;

import com.iqonic.store.utils.Constants;
import com.iqonic.store.utils.SharedPrefUtils;
import com.iqonic.store.utils.extensions.AppExtensionsKt;
import java.io.IOException;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import okhttp3.Interceptor;
import okhttp3.Response;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0017\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000*\u0001\u0000\b\n\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\u0016¨\u0006\u0006"}, d2 = {"com/iqonic/store/network/RetrofitClientFactory$getRetroFitClient$okHttpClient$1", "Lokhttp3/Interceptor;", "intercept", "Lokhttp3/Response;", "chain", "Lokhttp3/Interceptor$Chain;", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: RetrofitClientFactory.kt */
public final class RetrofitClientFactory$getRetroFitClient$okHttpClient$1 implements Interceptor {
    RetrofitClientFactory$getRetroFitClient$okHttpClient$1() {
    }

    public Response intercept(Interceptor.Chain chain) throws IOException {
        Intrinsics.checkParameterIsNotNull(chain, "chain");
        return chain.proceed(chain.request().newBuilder().addHeader(Constants.SharedPref.CONSUMERKEY, SharedPrefUtils.getStringValue$default(AppExtensionsKt.getSharedPrefInstance(), Constants.SharedPref.CONSUMERKEY, (String) null, 2, (Object) null)).addHeader(Constants.SharedPref.CONSUMERSECRET, SharedPrefUtils.getStringValue$default(AppExtensionsKt.getSharedPrefInstance(), Constants.SharedPref.CONSUMERSECRET, (String) null, 2, (Object) null)).build());
    }
}
