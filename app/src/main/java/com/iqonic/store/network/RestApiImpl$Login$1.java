package com.iqonic.store.network;

import androidx.core.app.NotificationCompat;
import com.iqonic.store.models.loginResponse;
import com.iqonic.store.utils.extensions.NetworkExtensionKt;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000)\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0003\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000*\u0001\u0000\b\n\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001J\u001e\u0010\u0003\u001a\u00020\u00042\f\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bH\u0016J$\u0010\t\u001a\u00020\u00042\f\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00020\u00062\f\u0010\n\u001a\b\u0012\u0004\u0012\u00020\u00020\u000bH\u0016¨\u0006\f"}, d2 = {"com/iqonic/store/network/RestApiImpl$Login$1", "Lretrofit2/Callback;", "Lcom/iqonic/store/models/loginResponse;", "onFailure", "", "call", "Lretrofit2/Call;", "t", "", "onResponse", "response", "Lretrofit2/Response;", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: RestApiImpl.kt */
public final class RestApiImpl$Login$1 implements Callback<loginResponse> {
    final /* synthetic */ Function1 $onApiError;
    final /* synthetic */ Function1 $onApiSuccess;

    RestApiImpl$Login$1(Function1 function1, Function1 function12) {
        this.$onApiSuccess = function1;
        this.$onApiError = function12;
    }

    public void onResponse(Call<loginResponse> call, Response<loginResponse> response) {
        Intrinsics.checkParameterIsNotNull(call, NotificationCompat.CATEGORY_CALL);
        Intrinsics.checkParameterIsNotNull(response, "response");
        Function1 function1 = this.$onApiSuccess;
        Function1 function12 = this.$onApiError;
        Request request = call.request();
        Intrinsics.checkExpressionValueIsNotNull(request, "call.request()");
        NetworkExtensionKt.successCallback(function1, function12, response, request);
    }

    public void onFailure(Call<loginResponse> call, Throwable th) {
        Intrinsics.checkParameterIsNotNull(call, NotificationCompat.CATEGORY_CALL);
        Intrinsics.checkParameterIsNotNull(th, "t");
        Function1 function1 = this.$onApiError;
        Request request = call.request();
        Intrinsics.checkExpressionValueIsNotNull(request, "call.request()");
        NetworkExtensionKt.failureCallback(function1, request, th.toString());
    }
}
