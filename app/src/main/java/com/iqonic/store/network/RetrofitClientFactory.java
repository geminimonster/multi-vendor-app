package com.iqonic.store.network;

import com.google.gson.GsonBuilder;
import com.iqonic.store.ShopHopApp;
import com.iqonic.store.utils.Constants;
import com.iqonic.store.utils.SharedPrefUtils;
import com.iqonic.store.utils.extensions.AppExtensionsKt;
import com.iqonic.store.utils.oauthInterceptor.OAuthInterceptor;
import com.nimbusds.jose.jwk.source.RemoteJWKSet;
import java.io.File;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import okhttp3.Cache;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u0006\u0010\b\u001a\u00020\tJ\b\u0010\n\u001a\u00020\u000bH\u0002R\u001a\u0010\u0002\u001a\u00020\u0003X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\u0004¨\u0006\f"}, d2 = {"Lcom/iqonic/store/network/RetrofitClientFactory;", "", "url", "", "(Ljava/lang/String;)V", "getUrl", "()Ljava/lang/String;", "setUrl", "getRestApis", "Lcom/iqonic/store/network/RestApis;", "getRetroFitClient", "Lretrofit2/Retrofit;", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: RetrofitClientFactory.kt */
public final class RetrofitClientFactory {
    private String url;

    public RetrofitClientFactory(String str) {
        Intrinsics.checkParameterIsNotNull(str, "url");
        this.url = str;
    }

    public final String getUrl() {
        return this.url;
    }

    public final void setUrl(String str) {
        Intrinsics.checkParameterIsNotNull(str, "<set-?>");
        this.url = str;
    }

    public final RestApis getRestApis() {
        Object create = getRetroFitClient().create(RestApis.class);
        Intrinsics.checkExpressionValueIsNotNull(create, "getRetroFitClient().create(RestApis::class.java)");
        return (RestApis) create;
    }

    private final Retrofit getRetroFitClient() {
        Retrofit build = new Retrofit.Builder().baseUrl(this.url).client(new OkHttpClient().newBuilder().connectTimeout(3, TimeUnit.MINUTES).readTimeout(3, TimeUnit.MINUTES).addInterceptor(new ResponseInterceptor()).addInterceptor(new UnauthorizedInterceptor()).addInterceptor(new RetrofitClientFactory$getRetroFitClient$okHttpClient$1()).addInterceptor(new OAuthInterceptor.Builder().consumerKey(SharedPrefUtils.getStringValue$default(AppExtensionsKt.getSharedPrefInstance(), Constants.SharedPref.CONSUMERKEY, (String) null, 2, (Object) null)).consumerSecret(SharedPrefUtils.getStringValue$default(AppExtensionsKt.getSharedPrefInstance(), Constants.SharedPref.CONSUMERSECRET, (String) null, 2, (Object) null)).build()).cache(new Cache(new File(ShopHopApp.Companion.getAppInstance().getCacheDir(), "responses"), (long) RemoteJWKSet.DEFAULT_HTTP_SIZE_LIMIT)).build()).addConverterFactory(GsonConverterFactory.create(new GsonBuilder().setLenient().disableHtmlEscaping().create())).addConverterFactory(ScalarsConverterFactory.create()).build();
        Intrinsics.checkExpressionValueIsNotNull(build, "Retrofit.Builder()\n     …e())\n            .build()");
        return build;
    }
}
