package com.iqonic.store.network;

import com.facebook.share.internal.ShareConstants;
import com.iqonic.store.models.AddCartResponse;
import com.iqonic.store.models.BaseResponse;
import com.iqonic.store.models.CancelOrderRequest;
import com.iqonic.store.models.CartRequestModel;
import com.iqonic.store.models.CartResponse;
import com.iqonic.store.models.Category;
import com.iqonic.store.models.CheckoutResponse;
import com.iqonic.store.models.CheckoutUrlRequest;
import com.iqonic.store.models.CountryModel;
import com.iqonic.store.models.Coupons;
import com.iqonic.store.models.CreateOrderNotes;
import com.iqonic.store.models.CreateOrderResponse;
import com.iqonic.store.models.CustomerData;
import com.iqonic.store.models.Dashboard;
import com.iqonic.store.models.GetStripeClientSecret;
import com.iqonic.store.models.Order;
import com.iqonic.store.models.OrderNotes;
import com.iqonic.store.models.OrderRequest;
import com.iqonic.store.models.ProductReviewData;
import com.iqonic.store.models.ProfileImage;
import com.iqonic.store.models.RegisterResponse;
import com.iqonic.store.models.RequestModel;
import com.iqonic.store.models.SearchRequest;
import com.iqonic.store.models.ShippingModel;
import com.iqonic.store.models.StoreProductAttribute;
import com.iqonic.store.models.StoreProductModel;
import com.iqonic.store.models.StoreProductModelNew;
import com.iqonic.store.models.UpdateCartResponse;
import com.iqonic.store.models.WishList;
import com.iqonic.store.models.loginResponse;
import com.iqonic.store.network.RestApis;
import com.iqonic.store.utils.extensions.AppExtensionsKt;
import java.util.ArrayList;
import java.util.Map;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000ü\u0001\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\b\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004JE\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\n2\u0012\u0010\u000b\u001a\u000e\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\b0\f2!\u0010\u000e\u001a\u001d\u0012\u0013\u0012\u00110\u0003¢\u0006\f\b\u000f\u0012\b\b\u0010\u0012\u0004\b\b(\u0011\u0012\u0004\u0012\u00020\b0\fJE\u0010\u0012\u001a\u00020\b2\u0006\u0010\u0013\u001a\u00020\u00142\u0012\u0010\u000b\u001a\u000e\u0012\u0004\u0012\u00020\u0015\u0012\u0004\u0012\u00020\b0\f2!\u0010\u000e\u001a\u001d\u0012\u0013\u0012\u00110\u0003¢\u0006\f\b\u000f\u0012\b\b\u0010\u0012\u0004\b\b(\u0011\u0012\u0004\u0012\u00020\b0\fJU\u0010\u0016\u001a\u00020\b2\u0006\u0010\u0017\u001a\u00020\u00032\"\u0010\u000b\u001a\u001e\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u00190\u0018j\b\u0012\u0004\u0012\u00020\u0019`\u001a\u0012\u0004\u0012\u00020\b0\f2!\u0010\u000e\u001a\u001d\u0012\u0013\u0012\u00110\u0003¢\u0006\f\b\u000f\u0012\b\b\u0010\u0012\u0004\b\b(\u0011\u0012\u0004\u0012\u00020\b0\fJE\u0010\u001b\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\n2\u0012\u0010\u000b\u001a\u000e\u0012\u0004\u0012\u00020\u001c\u0012\u0004\u0012\u00020\b0\f2!\u0010\u000e\u001a\u001d\u0012\u0013\u0012\u00110\u0003¢\u0006\f\b\u000f\u0012\b\b\u0010\u0012\u0004\b\b(\u0011\u0012\u0004\u0012\u00020\b0\fJE\u0010\u001d\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\n2\u0012\u0010\u000b\u001a\u000e\u0012\u0004\u0012\u00020\u001e\u0012\u0004\u0012\u00020\b0\f2!\u0010\u000e\u001a\u001d\u0012\u0013\u0012\u00110\u0003¢\u0006\f\b\u000f\u0012\b\b\u0010\u0012\u0004\b\b(\u0011\u0012\u0004\u0012\u00020\b0\fJM\u0010\u001f\u001a\u00020\b2\u0006\u0010 \u001a\u00020\u00142\u0006\u0010\t\u001a\u00020!2\u0012\u0010\u000b\u001a\u000e\u0012\u0004\u0012\u00020\"\u0012\u0004\u0012\u00020\b0\f2!\u0010\u000e\u001a\u001d\u0012\u0013\u0012\u00110\u0003¢\u0006\f\b\u000f\u0012\b\b\u0010\u0012\u0004\b\b(\u0011\u0012\u0004\u0012\u00020\b0\fJE\u0010#\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\n2\u0012\u0010\u000b\u001a\u000e\u0012\u0004\u0012\u00020\"\u0012\u0004\u0012\u00020\b0\f2!\u0010\u000e\u001a\u001d\u0012\u0013\u0012\u00110\u0003¢\u0006\f\b\u000f\u0012\b\b\u0010\u0012\u0004\b\b(\u0011\u0012\u0004\u0012\u00020\b0\fJM\u0010$\u001a\u00020\b2\u0006\u0010%\u001a\u00020\u00142\u0006\u0010\t\u001a\u00020&2\u0012\u0010\u000b\u001a\u000e\u0012\u0004\u0012\u00020\"\u0012\u0004\u0012\u00020\b0\f2!\u0010\u000e\u001a\u001d\u0012\u0013\u0012\u00110\u0003¢\u0006\f\b\u000f\u0012\b\b\u0010\u0012\u0004\b\b(\u0011\u0012\u0004\u0012\u00020\b0\fJE\u0010'\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\n2\u0012\u0010\u000b\u001a\u000e\u0012\u0004\u0012\u00020\"\u0012\u0004\u0012\u00020\b0\f2!\u0010\u000e\u001a\u001d\u0012\u0013\u0012\u00110\u0003¢\u0006\f\b\u000f\u0012\b\b\u0010\u0012\u0004\b\b(\u0011\u0012\u0004\u0012\u00020\b0\fJE\u0010(\u001a\u00020\b2\u0006\u0010\t\u001a\u00020)2\u0012\u0010\u000b\u001a\u000e\u0012\u0004\u0012\u00020*\u0012\u0004\u0012\u00020\b0\f2!\u0010\u000e\u001a\u001d\u0012\u0013\u0012\u00110\u0003¢\u0006\f\b\u000f\u0012\b\b\u0010\u0012\u0004\b\b(\u0011\u0012\u0004\u0012\u00020\b0\fJE\u0010+\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\n2\u0012\u0010\u000b\u001a\u000e\u0012\u0004\u0012\u00020\u0015\u0012\u0004\u0012\u00020\b0\f2!\u0010\u000e\u001a\u001d\u0012\u0013\u0012\u00110\u0003¢\u0006\f\b\u000f\u0012\b\b\u0010\u0012\u0004\b\b(\u0011\u0012\u0004\u0012\u00020\b0\fJE\u0010,\u001a\u00020\b2\u0006\u0010%\u001a\u00020\u00142\u0012\u0010\u000b\u001a\u000e\u0012\u0004\u0012\u00020\"\u0012\u0004\u0012\u00020\b0\f2!\u0010\u000e\u001a\u001d\u0012\u0013\u0012\u00110\u0003¢\u0006\f\b\u000f\u0012\b\b\u0010\u0012\u0004\b\b(\u0011\u0012\u0004\u0012\u00020\b0\fJE\u0010-\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\n2\u0012\u0010\u000b\u001a\u000e\u0012\u0004\u0012\u00020\"\u0012\u0004\u0012\u00020\b0\f2!\u0010\u000e\u001a\u001d\u0012\u0013\u0012\u00110\u0003¢\u0006\f\b\u000f\u0012\b\b\u0010\u0012\u0004\b\b(\u0011\u0012\u0004\u0012\u00020\b0\fJM\u0010.\u001a\u00020\b2\"\u0010\u000b\u001a\u001e\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020/0\u0018j\b\u0012\u0004\u0012\u00020/`\u001a\u0012\u0004\u0012\u00020\b0\f2!\u0010\u000e\u001a\u001d\u0012\u0013\u0012\u00110\u0003¢\u0006\f\b\u000f\u0012\b\b\u0010\u0012\u0004\b\b(\u0011\u0012\u0004\u0012\u00020\b0\fJE\u00100\u001a\u00020\b2\u0006\u0010\t\u001a\u0002012\u0012\u0010\u000b\u001a\u000e\u0012\u0004\u0012\u000202\u0012\u0004\u0012\u00020\b0\f2!\u0010\u000e\u001a\u001d\u0012\u0013\u0012\u00110\u0003¢\u0006\f\b\u000f\u0012\b\b\u0010\u0012\u0004\b\b(\u0011\u0012\u0004\u0012\u00020\b0\fJ=\u00103\u001a\u00020\b2\u0012\u0010\u000b\u001a\u000e\u0012\u0004\u0012\u000204\u0012\u0004\u0012\u00020\b0\f2!\u0010\u000e\u001a\u001d\u0012\u0013\u0012\u00110\u0003¢\u0006\f\b\u000f\u0012\b\b\u0010\u0012\u0004\b\b(\u0011\u0012\u0004\u0012\u00020\b0\fJM\u00105\u001a\u00020\b2\"\u0010\u000b\u001a\u001e\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u0002060\u0018j\b\u0012\u0004\u0012\u000206`\u001a\u0012\u0004\u0012\u00020\b0\f2!\u0010\u000e\u001a\u001d\u0012\u0013\u0012\u00110\u0003¢\u0006\f\b\u000f\u0012\b\b\u0010\u0012\u0004\b\b(\u0011\u0012\u0004\u0012\u00020\b0\fJU\u00107\u001a\u00020\b2\u0006\u0010\u0013\u001a\u00020\u00142\"\u0010\u000b\u001a\u001e\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u0002080\u0018j\b\u0012\u0004\u0012\u000208`\u001a\u0012\u0004\u0012\u00020\b0\f2!\u0010\u000e\u001a\u001d\u0012\u0013\u0012\u00110\u0003¢\u0006\f\b\u000f\u0012\b\b\u0010\u0012\u0004\b\b(\u0011\u0012\u0004\u0012\u00020\b0\fJE\u00109\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\n2\u0012\u0010\u000b\u001a\u000e\u0012\u0004\u0012\u00020:\u0012\u0004\u0012\u00020\b0\f2!\u0010\u000e\u001a\u001d\u0012\u0013\u0012\u00110\u0003¢\u0006\f\b\u000f\u0012\b\b\u0010\u0012\u0004\b\b(\u0011\u0012\u0004\u0012\u00020\b0\fJE\u0010;\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\n2\u0012\u0010\u000b\u001a\u000e\u0012\u0004\u0012\u00020<\u0012\u0004\u0012\u00020\b0\f2!\u0010\u000e\u001a\u001d\u0012\u0013\u0012\u00110\u0003¢\u0006\f\b\u000f\u0012\b\b\u0010\u0012\u0004\b\b(\u0011\u0012\u0004\u0012\u00020\b0\fJM\u0010=\u001a\u00020\b2\"\u0010\u000b\u001a\u001e\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020>0\u0018j\b\u0012\u0004\u0012\u00020>`\u001a\u0012\u0004\u0012\u00020\b0\f2!\u0010\u000e\u001a\u001d\u0012\u0013\u0012\u00110\u0003¢\u0006\f\b\u000f\u0012\b\b\u0010\u0012\u0004\b\b(\u0011\u0012\u0004\u0012\u00020\b0\fJa\u0010?\u001a\u00020\b2\u0012\u0010@\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00140A2\"\u0010\u000b\u001a\u001e\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020B0\u0018j\b\u0012\u0004\u0012\u00020B`\u001a\u0012\u0004\u0012\u00020\b0\f2!\u0010\u000e\u001a\u001d\u0012\u0013\u0012\u00110\u0003¢\u0006\f\b\u000f\u0012\b\b\u0010\u0012\u0004\b\b(\u0011\u0012\u0004\u0012\u00020\b0\fJa\u0010C\u001a\u00020\b2\u0012\u0010@\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00140A2\"\u0010\u000b\u001a\u001e\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020D0\u0018j\b\u0012\u0004\u0012\u00020D`\u001a\u0012\u0004\u0012\u00020\b0\f2!\u0010\u000e\u001a\u001d\u0012\u0013\u0012\u00110\u0003¢\u0006\f\b\u000f\u0012\b\b\u0010\u0012\u0004\b\b(\u0011\u0012\u0004\u0012\u00020\b0\fJM\u0010E\u001a\u00020\b2\"\u0010\u000b\u001a\u001e\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020F0\u0018j\b\u0012\u0004\u0012\u00020F`\u001a\u0012\u0004\u0012\u00020\b0\f2!\u0010\u000e\u001a\u001d\u0012\u0013\u0012\u00110\u0003¢\u0006\f\b\u000f\u0012\b\b\u0010\u0012\u0004\b\b(\u0011\u0012\u0004\u0012\u00020\b0\fJ=\u0010G\u001a\u00020\b2\u0012\u0010\u000b\u001a\u000e\u0012\u0004\u0012\u00020H\u0012\u0004\u0012\u00020\b0\f2!\u0010\u000e\u001a\u001d\u0012\u0013\u0012\u00110\u0003¢\u0006\f\b\u000f\u0012\b\b\u0010\u0012\u0004\b\b(\u0011\u0012\u0004\u0012\u00020\b0\fJM\u0010I\u001a\u00020\b2\"\u0010\u000b\u001a\u001e\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020D0\u0018j\b\u0012\u0004\u0012\u00020D`\u001a\u0012\u0004\u0012\u00020\b0\f2!\u0010\u000e\u001a\u001d\u0012\u0013\u0012\u00110\u0003¢\u0006\f\b\u000f\u0012\b\b\u0010\u0012\u0004\b\b(\u0011\u0012\u0004\u0012\u00020\b0\fJM\u0010J\u001a\u00020\b2\"\u0010\u000b\u001a\u001e\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u00190\u0018j\b\u0012\u0004\u0012\u00020\u0019`\u001a\u0012\u0004\u0012\u00020\b0\f2!\u0010\u000e\u001a\u001d\u0012\u0013\u0012\u00110\u0003¢\u0006\f\b\u000f\u0012\b\b\u0010\u0012\u0004\b\b(\u0011\u0012\u0004\u0012\u00020\b0\fJU\u0010K\u001a\u00020\b2\u0006\u0010\u0013\u001a\u00020\u00142\"\u0010\u000b\u001a\u001e\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u00150\u0018j\b\u0012\u0004\u0012\u00020\u0015`\u001a\u0012\u0004\u0012\u00020\b0\f2!\u0010\u000e\u001a\u001d\u0012\u0013\u0012\u00110\u0003¢\u0006\f\b\u000f\u0012\b\b\u0010\u0012\u0004\b\b(\u0011\u0012\u0004\u0012\u00020\b0\fJE\u0010L\u001a\u00020\b2\u0006\u0010@\u001a\u00020M2\u0012\u0010\u000b\u001a\u000e\u0012\u0004\u0012\u00020N\u0012\u0004\u0012\u00020\b0\f2!\u0010\u000e\u001a\u001d\u0012\u0013\u0012\u00110\u0003¢\u0006\f\b\u000f\u0012\b\b\u0010\u0012\u0004\b\b(\u0011\u0012\u0004\u0012\u00020\b0\fJU\u0010O\u001a\u00020\b2\u0006\u0010\u0013\u001a\u00020\u00142\"\u0010\u000b\u001a\u001e\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020D0\u0018j\b\u0012\u0004\u0012\u00020D`\u001a\u0012\u0004\u0012\u00020\b0\f2!\u0010\u000e\u001a\u001d\u0012\u0013\u0012\u00110\u0003¢\u0006\f\b\u000f\u0012\b\b\u0010\u0012\u0004\b\b(\u0011\u0012\u0004\u0012\u00020\b0\fJU\u0010P\u001a\u00020\b2\u0006\u0010Q\u001a\u00020\u00142\"\u0010\u000b\u001a\u001e\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020D0\u0018j\b\u0012\u0004\u0012\u00020D`\u001a\u0012\u0004\u0012\u00020\b0\f2!\u0010\u000e\u001a\u001d\u0012\u0013\u0012\u00110\u0003¢\u0006\f\b\u000f\u0012\b\b\u0010\u0012\u0004\b\b(\u0011\u0012\u0004\u0012\u00020\b0\fJE\u0010R\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\n2\u0012\u0010\u000b\u001a\u000e\u0012\u0004\u0012\u00020\"\u0012\u0004\u0012\u00020\b0\f2!\u0010\u000e\u001a\u001d\u0012\u0013\u0012\u00110\u0003¢\u0006\f\b\u000f\u0012\b\b\u0010\u0012\u0004\b\b(\u0011\u0012\u0004\u0012\u00020\b0\fJE\u0010S\u001a\u00020\b2\u0006\u0010\t\u001a\u00020T2\u0012\u0010\u000b\u001a\u000e\u0012\u0004\u0012\u00020\"\u0012\u0004\u0012\u00020\b0\f2!\u0010\u000e\u001a\u001d\u0012\u0013\u0012\u00110\u0003¢\u0006\f\b\u000f\u0012\b\b\u0010\u0012\u0004\b\b(\u0011\u0012\u0004\u0012\u00020\b0\fJE\u0010U\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\n2\u0012\u0010\u000b\u001a\u000e\u0012\u0004\u0012\u00020\"\u0012\u0004\u0012\u00020\b0\f2!\u0010\u000e\u001a\u001d\u0012\u0013\u0012\u00110\u0003¢\u0006\f\b\u000f\u0012\b\b\u0010\u0012\u0004\b\b(\u0011\u0012\u0004\u0012\u00020\b0\fJ=\u0010V\u001a\u00020\b2\u0012\u0010\u000b\u001a\u000e\u0012\u0004\u0012\u00020W\u0012\u0004\u0012\u00020\b0\f2!\u0010\u000e\u001a\u001d\u0012\u0013\u0012\u00110\u0003¢\u0006\f\b\u000f\u0012\b\b\u0010\u0012\u0004\b\b(\u0011\u0012\u0004\u0012\u00020\b0\fJE\u0010X\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\n2\u0012\u0010\u000b\u001a\u000e\u0012\u0004\u0012\u00020Y\u0012\u0004\u0012\u00020\b0\f2!\u0010\u000e\u001a\u001d\u0012\u0013\u0012\u00110\u0003¢\u0006\f\b\u000f\u0012\b\b\u0010\u0012\u0004\b\b(\u0011\u0012\u0004\u0012\u00020\b0\fJE\u0010Z\u001a\u00020\b2\u0006\u0010[\u001a\u00020M2\u0012\u0010\u000b\u001a\u000e\u0012\u0004\u0012\u00020N\u0012\u0004\u0012\u00020\b0\f2!\u0010\u000e\u001a\u001d\u0012\u0013\u0012\u00110\u0003¢\u0006\f\b\u000f\u0012\b\b\u0010\u0012\u0004\b\b(\u0011\u0012\u0004\u0012\u00020\b0\fJE\u0010\\\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\n2\u0012\u0010\u000b\u001a\u000e\u0012\u0004\u0012\u00020\u001c\u0012\u0004\u0012\u00020\b0\f2!\u0010\u000e\u001a\u001d\u0012\u0013\u0012\u00110\u0003¢\u0006\f\b\u000f\u0012\b\b\u0010\u0012\u0004\b\b(\u0011\u0012\u0004\u0012\u00020\b0\fJE\u0010]\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\n2\u0012\u0010\u000b\u001a\u000e\u0012\u0004\u0012\u00020W\u0012\u0004\u0012\u00020\b0\f2!\u0010\u000e\u001a\u001d\u0012\u0013\u0012\u00110\u0003¢\u0006\f\b\u000f\u0012\b\b\u0010\u0012\u0004\b\b(\u0011\u0012\u0004\u0012\u00020\b0\fJE\u0010^\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\n2\u0012\u0010\u000b\u001a\u000e\u0012\u0004\u0012\u00020_\u0012\u0004\u0012\u00020\b0\f2!\u0010\u000e\u001a\u001d\u0012\u0013\u0012\u00110\u0003¢\u0006\f\b\u000f\u0012\b\b\u0010\u0012\u0004\b\b(\u0011\u0012\u0004\u0012\u00020\b0\fJM\u0010`\u001a\u00020\b2\u0006\u0010\u0013\u001a\u00020\u00142\u0006\u0010\t\u001a\u00020\n2\u0012\u0010\u000b\u001a\u000e\u0012\u0004\u0012\u00020\u0015\u0012\u0004\u0012\u00020\b0\f2!\u0010\u000e\u001a\u001d\u0012\u0013\u0012\u00110\u0003¢\u0006\f\b\u000f\u0012\b\b\u0010\u0012\u0004\b\b(\u0011\u0012\u0004\u0012\u00020\b0\fR\u000e\u0010\u0005\u001a\u00020\u0006X\u000e¢\u0006\u0002\n\u0000¨\u0006a"}, d2 = {"Lcom/iqonic/store/network/RestApiImpl;", "", "s", "", "(Ljava/lang/String;)V", "getRestApis", "Lcom/iqonic/store/network/RestApis;", "CreateCustomer", "", "request", "Lcom/iqonic/store/models/RequestModel;", "onApiSuccess", "Lkotlin/Function1;", "Lcom/iqonic/store/models/RegisterResponse;", "onApiError", "Lkotlin/ParameterName;", "name", "aError", "DeleteProductReview", "id", "", "Lcom/iqonic/store/models/ProductReviewData;", "FindCoupon", "acode", "Ljava/util/ArrayList;", "Lcom/iqonic/store/models/Coupons;", "Lkotlin/collections/ArrayList;", "Login", "Lcom/iqonic/store/models/loginResponse;", "addItemToCart", "Lcom/iqonic/store/models/AddCartResponse;", "addOrderNotes", "oderID", "Lcom/iqonic/store/models/CreateOrderNotes;", "Lcom/iqonic/store/models/BaseResponse;", "addWishList", "cancelOrder", "orderId", "Lcom/iqonic/store/models/CancelOrderRequest;", "changePwd", "createOrderRequest", "Lcom/iqonic/store/models/OrderRequest;", "Lcom/iqonic/store/models/CreateOrderResponse;", "createProductReview", "deleteOrder", "forgetPassword", "getCart", "Lcom/iqonic/store/models/CartResponse;", "getCheckoutUrl", "Lcom/iqonic/store/models/CheckoutUrlRequest;", "Lcom/iqonic/store/models/CheckoutResponse;", "getDashboardData", "Lcom/iqonic/store/models/Dashboard;", "getOrderData", "Lcom/iqonic/store/models/Order;", "getOrderTracking", "Lcom/iqonic/store/models/OrderNotes;", "getShippingMethod", "Lcom/iqonic/store/models/ShippingModel;", "getStripeClientSecret", "Lcom/iqonic/store/models/GetStripeClientSecret;", "getWishList", "Lcom/iqonic/store/models/WishList;", "listAllCategory", "option", "", "Lcom/iqonic/store/models/Category;", "listAllCategoryProduct", "Lcom/iqonic/store/models/StoreProductModel;", "listAllCountry", "Lcom/iqonic/store/models/CountryModel;", "listAllProductAttribute", "Lcom/iqonic/store/models/StoreProductAttribute;", "listAllProducts", "listCoupons", "listReview", "listSaleProducts", "Lcom/iqonic/store/models/SearchRequest;", "Lcom/iqonic/store/models/StoreProductModelNew;", "listSingleCategory", "productDetail", "aProductId", "removeCartItem", "removeMultipleCartItem", "Lcom/iqonic/store/models/CartRequestModel;", "removeWishList", "retrieveCustomer", "Lcom/iqonic/store/models/CustomerData;", "saveProfileImage", "Lcom/iqonic/store/models/ProfileImage;", "search", "options", "socialLogin", "updateCustomer", "updateItemInCart", "Lcom/iqonic/store/models/UpdateCartResponse;", "updateProductReview", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: RestApiImpl.kt */
public final class RestApiImpl {
    private RestApis getRestApis;

    public RestApiImpl(String str) {
        Intrinsics.checkParameterIsNotNull(str, "s");
        this.getRestApis = new RetrofitClientFactory(str).getRestApis();
    }

    public final void getDashboardData(Function1<? super Dashboard, Unit> function1, Function1<? super String, Unit> function12) {
        Intrinsics.checkParameterIsNotNull(function1, "onApiSuccess");
        Intrinsics.checkParameterIsNotNull(function12, "onApiError");
        this.getRestApis.getDashboardData().enqueue(new RestApiImpl$getDashboardData$1(function1, function12));
    }

    public final void listAllProducts(Function1<? super ArrayList<StoreProductModel>, Unit> function1, Function1<? super String, Unit> function12) {
        Intrinsics.checkParameterIsNotNull(function1, "onApiSuccess");
        Intrinsics.checkParameterIsNotNull(function12, "onApiError");
        this.getRestApis.listAllProducts().enqueue(new RestApiImpl$listAllProducts$1(function1, function12));
    }

    public final void listAllCountry(Function1<? super ArrayList<CountryModel>, Unit> function1, Function1<? super String, Unit> function12) {
        Intrinsics.checkParameterIsNotNull(function1, "onApiSuccess");
        Intrinsics.checkParameterIsNotNull(function12, "onApiError");
        this.getRestApis.listCountry().enqueue(new RestApiImpl$listAllCountry$1(function1, function12));
    }

    public final void listAllProductAttribute(Function1<? super StoreProductAttribute, Unit> function1, Function1<? super String, Unit> function12) {
        Intrinsics.checkParameterIsNotNull(function1, "onApiSuccess");
        Intrinsics.checkParameterIsNotNull(function12, "onApiError");
        this.getRestApis.getProductAttribute().enqueue(new RestApiImpl$listAllProductAttribute$1(function1, function12));
    }

    public final void productDetail(int i, Function1<? super ArrayList<StoreProductModel>, Unit> function1, Function1<? super String, Unit> function12) {
        Intrinsics.checkParameterIsNotNull(function1, "onApiSuccess");
        Intrinsics.checkParameterIsNotNull(function12, "onApiError");
        RestApis.DefaultImpls.listSingleProducts$default(this.getRestApis, i, (String) null, (String) null, 6, (Object) null).enqueue(new RestApiImpl$productDetail$1(function1, function12));
    }

    public final void listAllCategory(Map<String, Integer> map, Function1<? super ArrayList<Category>, Unit> function1, Function1<? super String, Unit> function12) {
        Intrinsics.checkParameterIsNotNull(map, "option");
        Intrinsics.checkParameterIsNotNull(function1, "onApiSuccess");
        Intrinsics.checkParameterIsNotNull(function12, "onApiError");
        this.getRestApis.listAllCategory(map).enqueue(new RestApiImpl$listAllCategory$1(function1, function12));
    }

    public final void listSingleCategory(int i, Function1<? super ArrayList<StoreProductModel>, Unit> function1, Function1<? super String, Unit> function12) {
        Intrinsics.checkParameterIsNotNull(function1, "onApiSuccess");
        Intrinsics.checkParameterIsNotNull(function12, "onApiError");
        this.getRestApis.listSingleCategory(i).enqueue(new RestApiImpl$listSingleCategory$1(function1, function12));
    }

    public final void listAllCategoryProduct(Map<String, Integer> map, Function1<? super ArrayList<StoreProductModel>, Unit> function1, Function1<? super String, Unit> function12) {
        Intrinsics.checkParameterIsNotNull(map, "option");
        Intrinsics.checkParameterIsNotNull(function1, "onApiSuccess");
        Intrinsics.checkParameterIsNotNull(function12, "onApiError");
        this.getRestApis.listAllCategoryProduct(map).enqueue(new RestApiImpl$listAllCategoryProduct$1(function1, function12));
    }

    public final void listSaleProducts(SearchRequest searchRequest, Function1<? super StoreProductModelNew, Unit> function1, Function1<? super String, Unit> function12) {
        Intrinsics.checkParameterIsNotNull(searchRequest, "option");
        Intrinsics.checkParameterIsNotNull(function1, "onApiSuccess");
        Intrinsics.checkParameterIsNotNull(function12, "onApiError");
        this.getRestApis.getSaleProducts(searchRequest).enqueue(new RestApiImpl$listSaleProducts$1(function1, function12));
    }

    public final void listReview(int i, Function1<? super ArrayList<ProductReviewData>, Unit> function1, Function1<? super String, Unit> function12) {
        Intrinsics.checkParameterIsNotNull(function1, "onApiSuccess");
        Intrinsics.checkParameterIsNotNull(function12, "onApiError");
        this.getRestApis.listReview(i).enqueue(new RestApiImpl$listReview$1(function1, function12));
    }

    public final void createProductReview(RequestModel requestModel, Function1<? super ProductReviewData, Unit> function1, Function1<? super String, Unit> function12) {
        Intrinsics.checkParameterIsNotNull(requestModel, ShareConstants.WEB_DIALOG_RESULT_PARAM_REQUEST_ID);
        Intrinsics.checkParameterIsNotNull(function1, "onApiSuccess");
        Intrinsics.checkParameterIsNotNull(function12, "onApiError");
        this.getRestApis.createProductReview(requestModel).enqueue(new RestApiImpl$createProductReview$1(function1, function12));
    }

    public final void DeleteProductReview(int i, Function1<? super ProductReviewData, Unit> function1, Function1<? super String, Unit> function12) {
        Intrinsics.checkParameterIsNotNull(function1, "onApiSuccess");
        Intrinsics.checkParameterIsNotNull(function12, "onApiError");
        this.getRestApis.deleteProductReview(i).enqueue(new RestApiImpl$DeleteProductReview$1(function1, function12));
    }

    public final void updateProductReview(int i, RequestModel requestModel, Function1<? super ProductReviewData, Unit> function1, Function1<? super String, Unit> function12) {
        Intrinsics.checkParameterIsNotNull(requestModel, ShareConstants.WEB_DIALOG_RESULT_PARAM_REQUEST_ID);
        Intrinsics.checkParameterIsNotNull(function1, "onApiSuccess");
        Intrinsics.checkParameterIsNotNull(function12, "onApiError");
        this.getRestApis.updateProductReview(i, requestModel).enqueue(new RestApiImpl$updateProductReview$1(function1, function12));
    }

    public final void listCoupons(Function1<? super ArrayList<Coupons>, Unit> function1, Function1<? super String, Unit> function12) {
        Intrinsics.checkParameterIsNotNull(function1, "onApiSuccess");
        Intrinsics.checkParameterIsNotNull(function12, "onApiError");
        this.getRestApis.GetCoupon().enqueue(new RestApiImpl$listCoupons$1(function1, function12));
    }

    public final void FindCoupon(String str, Function1<? super ArrayList<Coupons>, Unit> function1, Function1<? super String, Unit> function12) {
        Intrinsics.checkParameterIsNotNull(str, "acode");
        Intrinsics.checkParameterIsNotNull(function1, "onApiSuccess");
        Intrinsics.checkParameterIsNotNull(function12, "onApiError");
        this.getRestApis.FindCoupon(str).enqueue(new RestApiImpl$FindCoupon$1(function1, function12));
    }

    public final void search(SearchRequest searchRequest, Function1<? super StoreProductModelNew, Unit> function1, Function1<? super String, Unit> function12) {
        Intrinsics.checkParameterIsNotNull(searchRequest, "options");
        Intrinsics.checkParameterIsNotNull(function1, "onApiSuccess");
        Intrinsics.checkParameterIsNotNull(function12, "onApiError");
        this.getRestApis.Search(searchRequest).enqueue(new RestApiImpl$search$1(function1, function12));
    }

    public final void CreateCustomer(RequestModel requestModel, Function1<? super RegisterResponse, Unit> function1, Function1<? super String, Unit> function12) {
        Intrinsics.checkParameterIsNotNull(requestModel, ShareConstants.WEB_DIALOG_RESULT_PARAM_REQUEST_ID);
        Intrinsics.checkParameterIsNotNull(function1, "onApiSuccess");
        Intrinsics.checkParameterIsNotNull(function12, "onApiError");
        this.getRestApis.createCustomer(requestModel).enqueue(new RestApiImpl$CreateCustomer$1(function1, function12));
    }

    public final void Login(RequestModel requestModel, Function1<? super loginResponse, Unit> function1, Function1<? super String, Unit> function12) {
        Intrinsics.checkParameterIsNotNull(requestModel, ShareConstants.WEB_DIALOG_RESULT_PARAM_REQUEST_ID);
        Intrinsics.checkParameterIsNotNull(function1, "onApiSuccess");
        Intrinsics.checkParameterIsNotNull(function12, "onApiError");
        this.getRestApis.login(requestModel).enqueue(new RestApiImpl$Login$1(function1, function12));
    }

    public final void retrieveCustomer(Function1<? super CustomerData, Unit> function1, Function1<? super String, Unit> function12) {
        Intrinsics.checkParameterIsNotNull(function1, "onApiSuccess");
        Intrinsics.checkParameterIsNotNull(function12, "onApiError");
        RestApis.DefaultImpls.retrieveCustomer$default(this.getRestApis, (String) null, 1, (Object) null).enqueue(new RestApiImpl$retrieveCustomer$1(function1, function12));
    }

    public final void updateCustomer(RequestModel requestModel, Function1<? super CustomerData, Unit> function1, Function1<? super String, Unit> function12) {
        Intrinsics.checkParameterIsNotNull(requestModel, ShareConstants.WEB_DIALOG_RESULT_PARAM_REQUEST_ID);
        Intrinsics.checkParameterIsNotNull(function1, "onApiSuccess");
        Intrinsics.checkParameterIsNotNull(function12, "onApiError");
        this.getRestApis.updateCustomer(AppExtensionsKt.getUserId(), requestModel).enqueue(new RestApiImpl$updateCustomer$1(function1, function12));
    }

    public final void addItemToCart(RequestModel requestModel, Function1<? super AddCartResponse, Unit> function1, Function1<? super String, Unit> function12) {
        Intrinsics.checkParameterIsNotNull(requestModel, ShareConstants.WEB_DIALOG_RESULT_PARAM_REQUEST_ID);
        Intrinsics.checkParameterIsNotNull(function1, "onApiSuccess");
        Intrinsics.checkParameterIsNotNull(function12, "onApiError");
        RestApis.DefaultImpls.addItemToCart$default(this.getRestApis, requestModel, (String) null, (String) null, 6, (Object) null).enqueue(new RestApiImpl$addItemToCart$1(function1, function12));
    }

    public final void removeCartItem(RequestModel requestModel, Function1<? super BaseResponse, Unit> function1, Function1<? super String, Unit> function12) {
        Intrinsics.checkParameterIsNotNull(requestModel, ShareConstants.WEB_DIALOG_RESULT_PARAM_REQUEST_ID);
        Intrinsics.checkParameterIsNotNull(function1, "onApiSuccess");
        Intrinsics.checkParameterIsNotNull(function12, "onApiError");
        RestApis.DefaultImpls.removeCartItem$default(this.getRestApis, requestModel, (String) null, (String) null, 6, (Object) null).enqueue(new RestApiImpl$removeCartItem$1(function1, function12));
    }

    public final void addOrderNotes(int i, CreateOrderNotes createOrderNotes, Function1<? super BaseResponse, Unit> function1, Function1<? super String, Unit> function12) {
        Intrinsics.checkParameterIsNotNull(createOrderNotes, ShareConstants.WEB_DIALOG_RESULT_PARAM_REQUEST_ID);
        Intrinsics.checkParameterIsNotNull(function1, "onApiSuccess");
        Intrinsics.checkParameterIsNotNull(function12, "onApiError");
        this.getRestApis.createOrderNotes(i, createOrderNotes).enqueue(new RestApiImpl$addOrderNotes$1(function1, function12));
    }

    public final void removeMultipleCartItem(CartRequestModel cartRequestModel, Function1<? super BaseResponse, Unit> function1, Function1<? super String, Unit> function12) {
        Intrinsics.checkParameterIsNotNull(cartRequestModel, ShareConstants.WEB_DIALOG_RESULT_PARAM_REQUEST_ID);
        Intrinsics.checkParameterIsNotNull(function1, "onApiSuccess");
        Intrinsics.checkParameterIsNotNull(function12, "onApiError");
        RestApis.DefaultImpls.removeMultipleCartItem$default(this.getRestApis, cartRequestModel, (String) null, (String) null, 6, (Object) null).enqueue(new RestApiImpl$removeMultipleCartItem$1(function1, function12));
    }

    public final void getCheckoutUrl(CheckoutUrlRequest checkoutUrlRequest, Function1<? super CheckoutResponse, Unit> function1, Function1<? super String, Unit> function12) {
        Intrinsics.checkParameterIsNotNull(checkoutUrlRequest, ShareConstants.WEB_DIALOG_RESULT_PARAM_REQUEST_ID);
        Intrinsics.checkParameterIsNotNull(function1, "onApiSuccess");
        Intrinsics.checkParameterIsNotNull(function12, "onApiError");
        RestApis.DefaultImpls.getCheckoutUrl$default(this.getRestApis, checkoutUrlRequest, (String) null, (String) null, 6, (Object) null).enqueue(new RestApiImpl$getCheckoutUrl$1(function1, function12));
    }

    public final void createOrderRequest(OrderRequest orderRequest, Function1<? super CreateOrderResponse, Unit> function1, Function1<? super String, Unit> function12) {
        Intrinsics.checkParameterIsNotNull(orderRequest, ShareConstants.WEB_DIALOG_RESULT_PARAM_REQUEST_ID);
        Intrinsics.checkParameterIsNotNull(function1, "onApiSuccess");
        Intrinsics.checkParameterIsNotNull(function12, "onApiError");
        RestApis.DefaultImpls.createOrder$default(this.getRestApis, orderRequest, (String) null, (String) null, 6, (Object) null).enqueue(new RestApiImpl$createOrderRequest$1(function1, function12));
    }

    public final void getStripeClientSecret(RequestModel requestModel, Function1<? super GetStripeClientSecret, Unit> function1, Function1<? super String, Unit> function12) {
        Intrinsics.checkParameterIsNotNull(requestModel, ShareConstants.WEB_DIALOG_RESULT_PARAM_REQUEST_ID);
        Intrinsics.checkParameterIsNotNull(function1, "onApiSuccess");
        Intrinsics.checkParameterIsNotNull(function12, "onApiError");
        RestApis.DefaultImpls.getStripeClientSecret$default(this.getRestApis, requestModel, (String) null, (String) null, 6, (Object) null).enqueue(new RestApiImpl$getStripeClientSecret$1(function1, function12));
    }

    public final void getCart(Function1<? super ArrayList<CartResponse>, Unit> function1, Function1<? super String, Unit> function12) {
        Intrinsics.checkParameterIsNotNull(function1, "onApiSuccess");
        Intrinsics.checkParameterIsNotNull(function12, "onApiError");
        RestApis.DefaultImpls.getCart$default(this.getRestApis, (String) null, (String) null, 3, (Object) null).enqueue(new RestApiImpl$getCart$1(function1, function12));
    }

    public final void updateItemInCart(RequestModel requestModel, Function1<? super UpdateCartResponse, Unit> function1, Function1<? super String, Unit> function12) {
        Intrinsics.checkParameterIsNotNull(requestModel, ShareConstants.WEB_DIALOG_RESULT_PARAM_REQUEST_ID);
        Intrinsics.checkParameterIsNotNull(function1, "onApiSuccess");
        Intrinsics.checkParameterIsNotNull(function12, "onApiError");
        RestApis.DefaultImpls.updateItemInCart$default(this.getRestApis, requestModel, (String) null, (String) null, 6, (Object) null).enqueue(new RestApiImpl$updateItemInCart$1(function1, function12));
    }

    public final void addWishList(RequestModel requestModel, Function1<? super BaseResponse, Unit> function1, Function1<? super String, Unit> function12) {
        Intrinsics.checkParameterIsNotNull(requestModel, ShareConstants.WEB_DIALOG_RESULT_PARAM_REQUEST_ID);
        Intrinsics.checkParameterIsNotNull(function1, "onApiSuccess");
        Intrinsics.checkParameterIsNotNull(function12, "onApiError");
        RestApis.DefaultImpls.addWishList$default(this.getRestApis, requestModel, (String) null, (String) null, 6, (Object) null).enqueue(new RestApiImpl$addWishList$1(function1, function12));
    }

    public final void removeWishList(RequestModel requestModel, Function1<? super BaseResponse, Unit> function1, Function1<? super String, Unit> function12) {
        Intrinsics.checkParameterIsNotNull(requestModel, ShareConstants.WEB_DIALOG_RESULT_PARAM_REQUEST_ID);
        Intrinsics.checkParameterIsNotNull(function1, "onApiSuccess");
        Intrinsics.checkParameterIsNotNull(function12, "onApiError");
        RestApis.DefaultImpls.removeWishList$default(this.getRestApis, requestModel, (String) null, (String) null, 6, (Object) null).enqueue(new RestApiImpl$removeWishList$1(function1, function12));
    }

    public final void getWishList(Function1<? super ArrayList<WishList>, Unit> function1, Function1<? super String, Unit> function12) {
        Intrinsics.checkParameterIsNotNull(function1, "onApiSuccess");
        Intrinsics.checkParameterIsNotNull(function12, "onApiError");
        RestApis.DefaultImpls.getWishList$default(this.getRestApis, (String) null, (String) null, 3, (Object) null).enqueue(new RestApiImpl$getWishList$1(function1, function12));
    }

    public final void cancelOrder(int i, CancelOrderRequest cancelOrderRequest, Function1<? super BaseResponse, Unit> function1, Function1<? super String, Unit> function12) {
        Intrinsics.checkParameterIsNotNull(cancelOrderRequest, ShareConstants.WEB_DIALOG_RESULT_PARAM_REQUEST_ID);
        Intrinsics.checkParameterIsNotNull(function1, "onApiSuccess");
        Intrinsics.checkParameterIsNotNull(function12, "onApiError");
        this.getRestApis.cancelOrder(i, cancelOrderRequest).enqueue(new RestApiImpl$cancelOrder$1(function1, function12));
    }

    public final void getOrderData(Function1<? super ArrayList<Order>, Unit> function1, Function1<? super String, Unit> function12) {
        Intrinsics.checkParameterIsNotNull(function1, "onApiSuccess");
        Intrinsics.checkParameterIsNotNull(function12, "onApiError");
        RestApis.DefaultImpls.getOrderData$default(this.getRestApis, (String) null, (String) null, 3, (Object) null).enqueue(new RestApiImpl$getOrderData$1(function1, function12));
    }

    public final void getOrderTracking(int i, Function1<? super ArrayList<OrderNotes>, Unit> function1, Function1<? super String, Unit> function12) {
        Intrinsics.checkParameterIsNotNull(function1, "onApiSuccess");
        Intrinsics.checkParameterIsNotNull(function12, "onApiError");
        this.getRestApis.getOrderTracking(i).enqueue(new RestApiImpl$getOrderTracking$1(function1, function12));
    }

    public final void changePwd(RequestModel requestModel, Function1<? super BaseResponse, Unit> function1, Function1<? super String, Unit> function12) {
        Intrinsics.checkParameterIsNotNull(requestModel, ShareConstants.WEB_DIALOG_RESULT_PARAM_REQUEST_ID);
        Intrinsics.checkParameterIsNotNull(function1, "onApiSuccess");
        Intrinsics.checkParameterIsNotNull(function12, "onApiError");
        RestApis.DefaultImpls.changePwd$default(this.getRestApis, requestModel, (String) null, (String) null, 6, (Object) null).enqueue(new RestApiImpl$changePwd$1(function1, function12));
    }

    public final void saveProfileImage(RequestModel requestModel, Function1<? super ProfileImage, Unit> function1, Function1<? super String, Unit> function12) {
        Intrinsics.checkParameterIsNotNull(requestModel, ShareConstants.WEB_DIALOG_RESULT_PARAM_REQUEST_ID);
        Intrinsics.checkParameterIsNotNull(function1, "onApiSuccess");
        Intrinsics.checkParameterIsNotNull(function12, "onApiError");
        RestApis.DefaultImpls.saveProfileImage$default(this.getRestApis, requestModel, (String) null, (String) null, (String) null, 14, (Object) null).enqueue(new RestApiImpl$saveProfileImage$1(function1, function12));
    }

    public final void forgetPassword(RequestModel requestModel, Function1<? super BaseResponse, Unit> function1, Function1<? super String, Unit> function12) {
        Intrinsics.checkParameterIsNotNull(requestModel, ShareConstants.WEB_DIALOG_RESULT_PARAM_REQUEST_ID);
        Intrinsics.checkParameterIsNotNull(function1, "onApiSuccess");
        Intrinsics.checkParameterIsNotNull(function12, "onApiError");
        this.getRestApis.forgetPassword(requestModel).enqueue(new RestApiImpl$forgetPassword$1(function1, function12));
    }

    public final void getShippingMethod(RequestModel requestModel, Function1<? super ShippingModel, Unit> function1, Function1<? super String, Unit> function12) {
        Intrinsics.checkParameterIsNotNull(requestModel, ShareConstants.WEB_DIALOG_RESULT_PARAM_REQUEST_ID);
        Intrinsics.checkParameterIsNotNull(function1, "onApiSuccess");
        Intrinsics.checkParameterIsNotNull(function12, "onApiError");
        RestApis.DefaultImpls.getShippingMethod$default(this.getRestApis, requestModel, (String) null, (String) null, 6, (Object) null).enqueue(new RestApiImpl$getShippingMethod$1(function1, function12));
    }

    public final void deleteOrder(int i, Function1<? super BaseResponse, Unit> function1, Function1<? super String, Unit> function12) {
        Intrinsics.checkParameterIsNotNull(function1, "onApiSuccess");
        Intrinsics.checkParameterIsNotNull(function12, "onApiError");
        this.getRestApis.deleteOrder(i).enqueue(new RestApiImpl$deleteOrder$1(function1, function12));
    }

    public final void socialLogin(RequestModel requestModel, Function1<? super loginResponse, Unit> function1, Function1<? super String, Unit> function12) {
        Intrinsics.checkParameterIsNotNull(requestModel, ShareConstants.WEB_DIALOG_RESULT_PARAM_REQUEST_ID);
        Intrinsics.checkParameterIsNotNull(function1, "onApiSuccess");
        Intrinsics.checkParameterIsNotNull(function12, "onApiError");
        this.getRestApis.socialLogin(requestModel).enqueue(new RestApiImpl$socialLogin$1(function1, function12));
    }
}
