package com.iqonic.store.network;

import com.iqonic.store.models.AddCartResponse;
import com.iqonic.store.models.BaseResponse;
import com.iqonic.store.models.CancelOrderRequest;
import com.iqonic.store.models.CartRequestModel;
import com.iqonic.store.models.CartResponse;
import com.iqonic.store.models.Category;
import com.iqonic.store.models.CheckoutResponse;
import com.iqonic.store.models.CheckoutUrlRequest;
import com.iqonic.store.models.CountryModel;
import com.iqonic.store.models.Coupons;
import com.iqonic.store.models.CreateOrderNotes;
import com.iqonic.store.models.CreateOrderResponse;
import com.iqonic.store.models.CustomerData;
import com.iqonic.store.models.Dashboard;
import com.iqonic.store.models.GetStripeClientSecret;
import com.iqonic.store.models.Order;
import com.iqonic.store.models.OrderNotes;
import com.iqonic.store.models.OrderRequest;
import com.iqonic.store.models.ProductReviewData;
import com.iqonic.store.models.ProfileImage;
import com.iqonic.store.models.RegisterResponse;
import com.iqonic.store.models.RequestModel;
import com.iqonic.store.models.SearchRequest;
import com.iqonic.store.models.ShippingModel;
import com.iqonic.store.models.StoreProductAttribute;
import com.iqonic.store.models.StoreProductModel;
import com.iqonic.store.models.StoreProductModelNew;
import com.iqonic.store.models.UpdateCartResponse;
import com.iqonic.store.models.WishList;
import com.iqonic.store.models.loginResponse;
import com.iqonic.store.utils.extensions.AppExtensionsKt;
import java.util.ArrayList;
import java.util.Map;
import kotlin.Metadata;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.HTTP;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000ì\u0001\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\bf\u0018\u00002\u00020\u0001J(\u0010\u0002\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u00050\u0004j\b\u0012\u0004\u0012\u00020\u0005`\u00060\u00032\b\b\u0001\u0010\u0007\u001a\u00020\bH'J\u001e\u0010\t\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u00050\u0004j\b\u0012\u0004\u0012\u00020\u0005`\u00060\u0003H'J\u0018\u0010\n\u001a\b\u0012\u0004\u0012\u00020\u000b0\u00032\b\b\u0001\u0010\f\u001a\u00020\rH'J,\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\u000f0\u00032\b\b\u0001\u0010\u0010\u001a\u00020\u00112\b\b\u0003\u0010\u0012\u001a\u00020\b2\b\b\u0003\u0010\u0013\u001a\u00020\bH'J,\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u00150\u00032\b\b\u0001\u0010\u0010\u001a\u00020\u00112\b\b\u0003\u0010\u0012\u001a\u00020\b2\b\b\u0003\u0010\u0013\u001a\u00020\bH'J\"\u0010\u0016\u001a\b\u0012\u0004\u0012\u00020\u00150\u00032\b\b\u0001\u0010\u0017\u001a\u00020\u00182\b\b\u0001\u0010\u0010\u001a\u00020\u0019H'J,\u0010\u001a\u001a\b\u0012\u0004\u0012\u00020\u00150\u00032\b\b\u0001\u0010\u0010\u001a\u00020\u00112\b\b\u0003\u0010\u0012\u001a\u00020\b2\b\b\u0003\u0010\u0013\u001a\u00020\bH'J\u0018\u0010\u001b\u001a\b\u0012\u0004\u0012\u00020\u001c0\u00032\b\b\u0001\u0010\u0010\u001a\u00020\u0011H'J,\u0010\u001d\u001a\b\u0012\u0004\u0012\u00020\u001e0\u00032\b\b\u0001\u0010\u0010\u001a\u00020\u001f2\b\b\u0003\u0010\u0012\u001a\u00020\b2\b\b\u0003\u0010\u0013\u001a\u00020\bH'J\"\u0010 \u001a\b\u0012\u0004\u0012\u00020\u00150\u00032\b\b\u0001\u0010\u0017\u001a\u00020\u00182\b\b\u0001\u0010\u0010\u001a\u00020!H'J\u0018\u0010\"\u001a\b\u0012\u0004\u0012\u00020#0\u00032\b\b\u0001\u0010\u0010\u001a\u00020\u0011H'J\u0018\u0010$\u001a\b\u0012\u0004\u0012\u00020\u00150\u00032\b\b\u0001\u0010\u0013\u001a\u00020\u0018H'J\u0018\u0010%\u001a\b\u0012\u0004\u0012\u00020#0\u00032\b\b\u0001\u0010\u0013\u001a\u00020\u0018H'J\u0018\u0010&\u001a\b\u0012\u0004\u0012\u00020\u00150\u00032\b\b\u0001\u0010\u0010\u001a\u00020\u0011H'J2\u0010'\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020(0\u0004j\b\u0012\u0004\u0012\u00020(`\u00060\u00032\b\b\u0003\u0010\u0012\u001a\u00020\b2\b\b\u0003\u0010\u0013\u001a\u00020\bH'J,\u0010)\u001a\b\u0012\u0004\u0012\u00020*0\u00032\b\b\u0001\u0010\u0010\u001a\u00020+2\b\b\u0003\u0010\u0012\u001a\u00020\b2\b\b\u0003\u0010\u0013\u001a\u00020\bH'J\u000e\u0010,\u001a\b\u0012\u0004\u0012\u00020-0\u0003H'J2\u0010.\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020/0\u0004j\b\u0012\u0004\u0012\u00020/`\u00060\u00032\b\b\u0003\u0010\u0012\u001a\u00020\b2\b\b\u0003\u0010\u0013\u001a\u00020\bH'J(\u00100\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u0002010\u0004j\b\u0012\u0004\u0012\u000201`\u00060\u00032\b\b\u0001\u0010\u0013\u001a\u00020\u0018H'J\u000e\u00102\u001a\b\u0012\u0004\u0012\u0002030\u0003H'J\u0018\u00104\u001a\b\u0012\u0004\u0012\u00020\u000b0\u00032\b\b\u0001\u0010\f\u001a\u00020\rH'J,\u00105\u001a\b\u0012\u0004\u0012\u0002060\u00032\b\b\u0001\u0010\u0010\u001a\u00020\u00112\b\b\u0003\u0010\u0012\u001a\u00020\b2\b\b\u0003\u0010\u0013\u001a\u00020\bH'J,\u00107\u001a\b\u0012\u0004\u0012\u0002080\u00032\b\b\u0001\u0010\u0010\u001a\u00020\u00112\b\b\u0003\u0010\u0012\u001a\u00020\b2\b\b\u0003\u0010\u0013\u001a\u00020\bH'J2\u00109\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020:0\u0004j\b\u0012\u0004\u0012\u00020:`\u00060\u00032\b\b\u0003\u0010\u0012\u001a\u00020\b2\b\b\u0003\u0010\u0013\u001a\u00020\bH'J4\u0010;\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020<0\u0004j\b\u0012\u0004\u0012\u00020<`\u00060\u00032\u0014\b\u0001\u0010\f\u001a\u000e\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\u00180=H'J4\u0010>\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020?0\u0004j\b\u0012\u0004\u0012\u00020?`\u00060\u00032\u0014\b\u0001\u0010\f\u001a\u000e\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\u00180=H'J\u001e\u0010@\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020?0\u0004j\b\u0012\u0004\u0012\u00020?`\u00060\u0003H'J\u001e\u0010A\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020B0\u0004j\b\u0012\u0004\u0012\u00020B`\u00060\u0003H'J(\u0010C\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020#0\u0004j\b\u0012\u0004\u0012\u00020#`\u00060\u00032\b\b\u0001\u0010\u0013\u001a\u00020\u0018H'J(\u0010D\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020?0\u0004j\b\u0012\u0004\u0012\u00020?`\u00060\u00032\b\b\u0001\u0010E\u001a\u00020\u0018H'J<\u0010F\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020?0\u0004j\b\u0012\u0004\u0012\u00020?`\u00060\u00032\b\b\u0001\u0010G\u001a\u00020\u00182\b\b\u0003\u0010\u0012\u001a\u00020\b2\b\b\u0003\u0010\u0013\u001a\u00020\bH'J\u0018\u0010H\u001a\b\u0012\u0004\u0012\u00020I0\u00032\b\b\u0001\u0010\u0010\u001a\u00020\u0011H'J,\u0010J\u001a\b\u0012\u0004\u0012\u00020\u00150\u00032\b\b\u0001\u0010\u0010\u001a\u00020\u00112\b\b\u0003\u0010\u0012\u001a\u00020\b2\b\b\u0003\u0010\u0013\u001a\u00020\bH'J,\u0010K\u001a\b\u0012\u0004\u0012\u00020\u00150\u00032\b\b\u0001\u0010\u0010\u001a\u00020L2\b\b\u0003\u0010\u0012\u001a\u00020\b2\b\b\u0003\u0010\u0013\u001a\u00020\bH'J,\u0010M\u001a\b\u0012\u0004\u0012\u00020\u00150\u00032\b\b\u0001\u0010\u0010\u001a\u00020\u00112\b\b\u0003\u0010\u0012\u001a\u00020\b2\b\b\u0003\u0010\u0013\u001a\u00020\bH'J\u0018\u0010N\u001a\b\u0012\u0004\u0012\u00020O0\u00032\b\b\u0003\u0010\u0013\u001a\u00020\bH'J6\u0010P\u001a\b\u0012\u0004\u0012\u00020Q0\u00032\b\b\u0001\u0010\u0010\u001a\u00020\u00112\b\b\u0003\u0010R\u001a\u00020\b2\b\b\u0003\u0010\u0012\u001a\u00020\b2\b\b\u0003\u0010\u0013\u001a\u00020\bH'J\u0018\u0010S\u001a\b\u0012\u0004\u0012\u00020I0\u00032\b\b\u0001\u0010\u0010\u001a\u00020\u0011H'J\"\u0010T\u001a\b\u0012\u0004\u0012\u00020O0\u00032\b\b\u0001\u0010\u0013\u001a\u00020\b2\b\b\u0001\u0010\u0010\u001a\u00020\u0011H'J,\u0010U\u001a\b\u0012\u0004\u0012\u00020V0\u00032\b\b\u0001\u0010\u0010\u001a\u00020\u00112\b\b\u0003\u0010\u0012\u001a\u00020\b2\b\b\u0003\u0010\u0013\u001a\u00020\bH'J\"\u0010W\u001a\b\u0012\u0004\u0012\u00020#0\u00032\b\b\u0001\u0010\u0013\u001a\u00020\u00182\b\b\u0001\u0010\u0010\u001a\u00020\u0011H'¨\u0006X"}, d2 = {"Lcom/iqonic/store/network/RestApis;", "", "FindCoupon", "Lretrofit2/Call;", "Ljava/util/ArrayList;", "Lcom/iqonic/store/models/Coupons;", "Lkotlin/collections/ArrayList;", "code", "", "GetCoupon", "Search", "Lcom/iqonic/store/models/StoreProductModelNew;", "options", "Lcom/iqonic/store/models/SearchRequest;", "addItemToCart", "Lcom/iqonic/store/models/AddCartResponse;", "request", "Lcom/iqonic/store/models/RequestModel;", "token", "id", "addWishList", "Lcom/iqonic/store/models/BaseResponse;", "cancelOrder", "ids", "", "Lcom/iqonic/store/models/CancelOrderRequest;", "changePwd", "createCustomer", "Lcom/iqonic/store/models/RegisterResponse;", "createOrder", "Lcom/iqonic/store/models/CreateOrderResponse;", "Lcom/iqonic/store/models/OrderRequest;", "createOrderNotes", "Lcom/iqonic/store/models/CreateOrderNotes;", "createProductReview", "Lcom/iqonic/store/models/ProductReviewData;", "deleteOrder", "deleteProductReview", "forgetPassword", "getCart", "Lcom/iqonic/store/models/CartResponse;", "getCheckoutUrl", "Lcom/iqonic/store/models/CheckoutResponse;", "Lcom/iqonic/store/models/CheckoutUrlRequest;", "getDashboardData", "Lcom/iqonic/store/models/Dashboard;", "getOrderData", "Lcom/iqonic/store/models/Order;", "getOrderTracking", "Lcom/iqonic/store/models/OrderNotes;", "getProductAttribute", "Lcom/iqonic/store/models/StoreProductAttribute;", "getSaleProducts", "getShippingMethod", "Lcom/iqonic/store/models/ShippingModel;", "getStripeClientSecret", "Lcom/iqonic/store/models/GetStripeClientSecret;", "getWishList", "Lcom/iqonic/store/models/WishList;", "listAllCategory", "Lcom/iqonic/store/models/Category;", "", "listAllCategoryProduct", "Lcom/iqonic/store/models/StoreProductModel;", "listAllProducts", "listCountry", "Lcom/iqonic/store/models/CountryModel;", "listReview", "listSingleCategory", "categories", "listSingleProducts", "product_id", "login", "Lcom/iqonic/store/models/loginResponse;", "removeCartItem", "removeMultipleCartItem", "Lcom/iqonic/store/models/CartRequestModel;", "removeWishList", "retrieveCustomer", "Lcom/iqonic/store/models/CustomerData;", "saveProfileImage", "Lcom/iqonic/store/models/ProfileImage;", "type", "socialLogin", "updateCustomer", "updateItemInCart", "Lcom/iqonic/store/models/UpdateCartResponse;", "updateProductReview", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: RestApis.kt */
public interface RestApis {
    @GET("wc/v3/Coupons")
    Call<ArrayList<Coupons>> FindCoupon(@Query("code") String str);

    @GET("wc/v3/Coupons")
    Call<ArrayList<Coupons>> GetCoupon();

    @POST("iqonic-api/api/v1/woocommerce/get-product")
    Call<StoreProductModelNew> Search(@Body SearchRequest searchRequest);

    @POST("iqonic-api/api/v1/cart/add-cart/")
    Call<AddCartResponse> addItemToCart(@Body RequestModel requestModel, @Header("token") String str, @Header("id") String str2);

    @POST("iqonic-api/api/v1/wishlist/add-wishlist/")
    Call<BaseResponse> addWishList(@Body RequestModel requestModel, @Header("token") String str, @Header("id") String str2);

    @POST("wc/v3/orders/{order_id}")
    Call<BaseResponse> cancelOrder(@Path("order_id") int i, @Body CancelOrderRequest cancelOrderRequest);

    @POST("iqonic-api/api/v1/woocommerce/change-password")
    Call<BaseResponse> changePwd(@Body RequestModel requestModel, @Header("token") String str, @Header("id") String str2);

    @POST("iqonic-api/api/v1/woocommerce/customer/register")
    Call<RegisterResponse> createCustomer(@Body RequestModel requestModel);

    @POST("wc/v3/orders")
    Call<CreateOrderResponse> createOrder(@Body OrderRequest orderRequest, @Header("token") String str, @Header("id") String str2);

    @POST("wc/v3/orders/{order_id}/notes")
    Call<BaseResponse> createOrderNotes(@Path("order_id") int i, @Body CreateOrderNotes createOrderNotes);

    @POST("wc/v3/products/reviews")
    Call<ProductReviewData> createProductReview(@Body RequestModel requestModel);

    @HTTP(hasBody = true, method = "DELETE", path = "wc/v3/orders/{id}")
    Call<BaseResponse> deleteOrder(@Path("id") int i);

    @HTTP(hasBody = true, method = "DELETE", path = "wc/v3/products/reviews/{id}")
    Call<ProductReviewData> deleteProductReview(@Path("id") int i);

    @POST("iqonic-api/api/v1/customer/forget-password")
    Call<BaseResponse> forgetPassword(@Body RequestModel requestModel);

    @GET("iqonic-api/api/v1/cart/get-cart/")
    Call<ArrayList<CartResponse>> getCart(@Header("token") String str, @Header("id") String str2);

    @POST("iqonic-api/api/v1/woocommerce/get-checkout-url")
    Call<CheckoutResponse> getCheckoutUrl(@Body CheckoutUrlRequest checkoutUrlRequest, @Header("token") String str, @Header("id") String str2);

    @GET("iqonic-api/api/v1/woocommerce/get-dashboard")
    Call<Dashboard> getDashboardData();

    @GET("iqonic-api/api/v1/woocommerce/get-customer-orders")
    Call<ArrayList<Order>> getOrderData(@Header("token") String str, @Header("id") String str2);

    @GET("wc/v3/orders/{order_id}/notes")
    Call<ArrayList<OrderNotes>> getOrderTracking(@Path("order_id") int i);

    @GET("iqonic-api/api/v1/woocommerce/get-product-attributes")
    Call<StoreProductAttribute> getProductAttribute();

    @POST("iqonic-api/api/v1/woocommerce/get-product")
    Call<StoreProductModelNew> getSaleProducts(@Body SearchRequest searchRequest);

    @POST("iqonic-api/api/v1/woocommerce/get-shipping-methods")
    Call<ShippingModel> getShippingMethod(@Body RequestModel requestModel, @Header("token") String str, @Header("id") String str2);

    @POST("iqonic-api/api/v1/woocommerce/get-stripe-client-secret")
    Call<GetStripeClientSecret> getStripeClientSecret(@Body RequestModel requestModel, @Header("token") String str, @Header("id") String str2);

    @GET("iqonic-api/api/v1/wishlist/get-wishlist/")
    Call<ArrayList<WishList>> getWishList(@Header("token") String str, @Header("id") String str2);

    @GET("wc/v3/products/categories")
    Call<ArrayList<Category>> listAllCategory(@QueryMap Map<String, Integer> map);

    @GET("wc/v3/products")
    Call<ArrayList<StoreProductModel>> listAllCategoryProduct(@QueryMap Map<String, Integer> map);

    @GET("wc/v3/products")
    Call<ArrayList<StoreProductModel>> listAllProducts();

    @GET("wc/v3/data/countries")
    Call<ArrayList<CountryModel>> listCountry();

    @GET("wc/v1/products/{id}/reviews")
    Call<ArrayList<ProductReviewData>> listReview(@Path("id") int i);

    @GET("wc/v3/products")
    Call<ArrayList<StoreProductModel>> listSingleCategory(@Query("category") int i);

    @GET("iqonic-api/api/v1/woocommerce/get-product-details")
    Call<ArrayList<StoreProductModel>> listSingleProducts(@Query("product_id") int i, @Header("token") String str, @Header("id") String str2);

    @POST("jwt-auth/v1/token")
    Call<loginResponse> login(@Body RequestModel requestModel);

    @POST("iqonic-api/api/v1/cart/delete-cart/")
    Call<BaseResponse> removeCartItem(@Body RequestModel requestModel, @Header("token") String str, @Header("id") String str2);

    @POST("iqonic-api/api/v1/cart/delete-cart/")
    Call<BaseResponse> removeMultipleCartItem(@Body CartRequestModel cartRequestModel, @Header("token") String str, @Header("id") String str2);

    @POST("iqonic-api/api/v1/wishlist/delete-wishlist/")
    Call<BaseResponse> removeWishList(@Body RequestModel requestModel, @Header("token") String str, @Header("id") String str2);

    @GET("wc/v3/customers/{id}")
    Call<CustomerData> retrieveCustomer(@Path("id") String str);

    @POST("iqonic-api/api/v1/customer/save-profile-image")
    Call<ProfileImage> saveProfileImage(@Body RequestModel requestModel, @Header("Content-Type") String str, @Header("token") String str2, @Header("id") String str3);

    @POST("woobox-api/api/v1/customer/social_login")
    Call<loginResponse> socialLogin(@Body RequestModel requestModel);

    @POST("wc/v3/customers/{id}")
    Call<CustomerData> updateCustomer(@Path("id") String str, @Body RequestModel requestModel);

    @POST("iqonic-api/api/v1/cart/update-cart/")
    Call<UpdateCartResponse> updateItemInCart(@Body RequestModel requestModel, @Header("token") String str, @Header("id") String str2);

    @HTTP(hasBody = true, method = "PUT", path = "wc/v3/products/reviews/{id}")
    Call<ProductReviewData> updateProductReview(@Path("id") int i, @Body RequestModel requestModel);

    @Metadata(bv = {1, 0, 3}, k = 3, mv = {1, 1, 16})
    /* compiled from: RestApis.kt */
    public static final class DefaultImpls {
        public static /* synthetic */ Call listSingleProducts$default(RestApis restApis, int i, String str, String str2, int i2, Object obj) {
            if (obj == null) {
                if ((i2 & 2) != 0) {
                    str = AppExtensionsKt.getApiToken();
                }
                if ((i2 & 4) != 0) {
                    str2 = AppExtensionsKt.getUserId();
                }
                return restApis.listSingleProducts(i, str, str2);
            }
            throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: listSingleProducts");
        }

        public static /* synthetic */ Call retrieveCustomer$default(RestApis restApis, String str, int i, Object obj) {
            if (obj == null) {
                if ((i & 1) != 0) {
                    str = AppExtensionsKt.getUserId();
                }
                return restApis.retrieveCustomer(str);
            }
            throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: retrieveCustomer");
        }

        public static /* synthetic */ Call addItemToCart$default(RestApis restApis, RequestModel requestModel, String str, String str2, int i, Object obj) {
            if (obj == null) {
                if ((i & 2) != 0) {
                    str = AppExtensionsKt.getApiToken();
                }
                if ((i & 4) != 0) {
                    str2 = AppExtensionsKt.getUserId();
                }
                return restApis.addItemToCart(requestModel, str, str2);
            }
            throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: addItemToCart");
        }

        public static /* synthetic */ Call getCart$default(RestApis restApis, String str, String str2, int i, Object obj) {
            if (obj == null) {
                if ((i & 1) != 0) {
                    str = AppExtensionsKt.getApiToken();
                }
                if ((i & 2) != 0) {
                    str2 = AppExtensionsKt.getUserId();
                }
                return restApis.getCart(str, str2);
            }
            throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: getCart");
        }

        public static /* synthetic */ Call getStripeClientSecret$default(RestApis restApis, RequestModel requestModel, String str, String str2, int i, Object obj) {
            if (obj == null) {
                if ((i & 2) != 0) {
                    str = AppExtensionsKt.getApiToken();
                }
                if ((i & 4) != 0) {
                    str2 = AppExtensionsKt.getUserId();
                }
                return restApis.getStripeClientSecret(requestModel, str, str2);
            }
            throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: getStripeClientSecret");
        }

        public static /* synthetic */ Call getCheckoutUrl$default(RestApis restApis, CheckoutUrlRequest checkoutUrlRequest, String str, String str2, int i, Object obj) {
            if (obj == null) {
                if ((i & 2) != 0) {
                    str = AppExtensionsKt.getApiToken();
                }
                if ((i & 4) != 0) {
                    str2 = AppExtensionsKt.getUserId();
                }
                return restApis.getCheckoutUrl(checkoutUrlRequest, str, str2);
            }
            throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: getCheckoutUrl");
        }

        public static /* synthetic */ Call createOrder$default(RestApis restApis, OrderRequest orderRequest, String str, String str2, int i, Object obj) {
            if (obj == null) {
                if ((i & 2) != 0) {
                    str = AppExtensionsKt.getApiToken();
                }
                if ((i & 4) != 0) {
                    str2 = AppExtensionsKt.getUserId();
                }
                return restApis.createOrder(orderRequest, str, str2);
            }
            throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: createOrder");
        }

        public static /* synthetic */ Call removeCartItem$default(RestApis restApis, RequestModel requestModel, String str, String str2, int i, Object obj) {
            if (obj == null) {
                if ((i & 2) != 0) {
                    str = AppExtensionsKt.getApiToken();
                }
                if ((i & 4) != 0) {
                    str2 = AppExtensionsKt.getUserId();
                }
                return restApis.removeCartItem(requestModel, str, str2);
            }
            throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: removeCartItem");
        }

        public static /* synthetic */ Call removeMultipleCartItem$default(RestApis restApis, CartRequestModel cartRequestModel, String str, String str2, int i, Object obj) {
            if (obj == null) {
                if ((i & 2) != 0) {
                    str = AppExtensionsKt.getApiToken();
                }
                if ((i & 4) != 0) {
                    str2 = AppExtensionsKt.getUserId();
                }
                return restApis.removeMultipleCartItem(cartRequestModel, str, str2);
            }
            throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: removeMultipleCartItem");
        }

        public static /* synthetic */ Call updateItemInCart$default(RestApis restApis, RequestModel requestModel, String str, String str2, int i, Object obj) {
            if (obj == null) {
                if ((i & 2) != 0) {
                    str = AppExtensionsKt.getApiToken();
                }
                if ((i & 4) != 0) {
                    str2 = AppExtensionsKt.getUserId();
                }
                return restApis.updateItemInCart(requestModel, str, str2);
            }
            throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: updateItemInCart");
        }

        public static /* synthetic */ Call addWishList$default(RestApis restApis, RequestModel requestModel, String str, String str2, int i, Object obj) {
            if (obj == null) {
                if ((i & 2) != 0) {
                    str = AppExtensionsKt.getApiToken();
                }
                if ((i & 4) != 0) {
                    str2 = AppExtensionsKt.getUserId();
                }
                return restApis.addWishList(requestModel, str, str2);
            }
            throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: addWishList");
        }

        public static /* synthetic */ Call getWishList$default(RestApis restApis, String str, String str2, int i, Object obj) {
            if (obj == null) {
                if ((i & 1) != 0) {
                    str = AppExtensionsKt.getApiToken();
                }
                if ((i & 2) != 0) {
                    str2 = AppExtensionsKt.getUserId();
                }
                return restApis.getWishList(str, str2);
            }
            throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: getWishList");
        }

        public static /* synthetic */ Call removeWishList$default(RestApis restApis, RequestModel requestModel, String str, String str2, int i, Object obj) {
            if (obj == null) {
                if ((i & 2) != 0) {
                    str = AppExtensionsKt.getApiToken();
                }
                if ((i & 4) != 0) {
                    str2 = AppExtensionsKt.getUserId();
                }
                return restApis.removeWishList(requestModel, str, str2);
            }
            throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: removeWishList");
        }

        public static /* synthetic */ Call getOrderData$default(RestApis restApis, String str, String str2, int i, Object obj) {
            if (obj == null) {
                if ((i & 1) != 0) {
                    str = AppExtensionsKt.getApiToken();
                }
                if ((i & 2) != 0) {
                    str2 = AppExtensionsKt.getUserId();
                }
                return restApis.getOrderData(str, str2);
            }
            throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: getOrderData");
        }

        public static /* synthetic */ Call changePwd$default(RestApis restApis, RequestModel requestModel, String str, String str2, int i, Object obj) {
            if (obj == null) {
                if ((i & 2) != 0) {
                    str = AppExtensionsKt.getApiToken();
                }
                if ((i & 4) != 0) {
                    str2 = AppExtensionsKt.getUserId();
                }
                return restApis.changePwd(requestModel, str, str2);
            }
            throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: changePwd");
        }

        public static /* synthetic */ Call saveProfileImage$default(RestApis restApis, RequestModel requestModel, String str, String str2, String str3, int i, Object obj) {
            if (obj == null) {
                if ((i & 2) != 0) {
                    str = "application/json";
                }
                if ((i & 4) != 0) {
                    str2 = AppExtensionsKt.getApiToken();
                }
                if ((i & 8) != 0) {
                    str3 = AppExtensionsKt.getUserId();
                }
                return restApis.saveProfileImage(requestModel, str, str2, str3);
            }
            throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: saveProfileImage");
        }

        public static /* synthetic */ Call getShippingMethod$default(RestApis restApis, RequestModel requestModel, String str, String str2, int i, Object obj) {
            if (obj == null) {
                if ((i & 2) != 0) {
                    str = AppExtensionsKt.getApiToken();
                }
                if ((i & 4) != 0) {
                    str2 = AppExtensionsKt.getUserId();
                }
                return restApis.getShippingMethod(requestModel, str, str2);
            }
            throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: getShippingMethod");
        }
    }
}
