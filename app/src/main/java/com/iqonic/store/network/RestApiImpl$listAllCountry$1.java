package com.iqonic.store.network;

import androidx.core.app.NotificationCompat;
import com.iqonic.store.models.CountryModel;
import com.iqonic.store.utils.extensions.NetworkExtensionKt;
import java.util.ArrayList;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@Metadata(bv = {1, 0, 3}, d1 = {"\u00001\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0003\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000*\u0001\u0000\b\n\u0018\u00002\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u00030\u0002j\b\u0012\u0004\u0012\u00020\u0003`\u00040\u0001J.\u0010\u0005\u001a\u00020\u00062\u001c\u0010\u0007\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u00030\u0002j\b\u0012\u0004\u0012\u00020\u0003`\u00040\b2\u0006\u0010\t\u001a\u00020\nH\u0016JD\u0010\u000b\u001a\u00020\u00062\u001c\u0010\u0007\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u00030\u0002j\b\u0012\u0004\u0012\u00020\u0003`\u00040\b2\u001c\u0010\f\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u00030\u0002j\b\u0012\u0004\u0012\u00020\u0003`\u00040\rH\u0016¨\u0006\u000e"}, d2 = {"com/iqonic/store/network/RestApiImpl$listAllCountry$1", "Lretrofit2/Callback;", "Ljava/util/ArrayList;", "Lcom/iqonic/store/models/CountryModel;", "Lkotlin/collections/ArrayList;", "onFailure", "", "call", "Lretrofit2/Call;", "t", "", "onResponse", "response", "Lretrofit2/Response;", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: RestApiImpl.kt */
public final class RestApiImpl$listAllCountry$1 implements Callback<ArrayList<CountryModel>> {
    final /* synthetic */ Function1 $onApiError;
    final /* synthetic */ Function1 $onApiSuccess;

    RestApiImpl$listAllCountry$1(Function1 function1, Function1 function12) {
        this.$onApiSuccess = function1;
        this.$onApiError = function12;
    }

    public void onResponse(Call<ArrayList<CountryModel>> call, Response<ArrayList<CountryModel>> response) {
        Intrinsics.checkParameterIsNotNull(call, NotificationCompat.CATEGORY_CALL);
        Intrinsics.checkParameterIsNotNull(response, "response");
        Function1 function1 = this.$onApiSuccess;
        Function1 function12 = this.$onApiError;
        Request request = call.request();
        Intrinsics.checkExpressionValueIsNotNull(request, "call.request()");
        NetworkExtensionKt.successCallback(function1, function12, response, request);
    }

    public void onFailure(Call<ArrayList<CountryModel>> call, Throwable th) {
        Intrinsics.checkParameterIsNotNull(call, NotificationCompat.CATEGORY_CALL);
        Intrinsics.checkParameterIsNotNull(th, "t");
        Function1 function1 = this.$onApiError;
        Request request = call.request();
        Intrinsics.checkExpressionValueIsNotNull(request, "call.request()");
        NetworkExtensionKt.failureCallback(function1, request, th.toString());
    }
}
