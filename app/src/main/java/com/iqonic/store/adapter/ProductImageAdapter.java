package com.iqonic.store.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.viewpager.widget.PagerAdapter;
import com.iqonic.store.utils.extensions.NetworkExtensionKt;
import com.store.proshop.R;
import java.util.ArrayList;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000J\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u0000 \u00192\u00020\u0001:\u0002\u0019\u001aB\u001d\u0012\u0016\u0010\u0002\u001a\u0012\u0012\u0004\u0012\u00020\u00040\u0003j\b\u0012\u0004\u0012\u00020\u0004`\u0005¢\u0006\u0002\u0010\u0006J \u0010\n\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u0011H\u0016J\b\u0010\u0012\u001a\u00020\u000fH\u0016J\u0018\u0010\u0013\u001a\u00020\u00112\u0006\u0010\f\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000fH\u0016J\u0018\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u0010\u001a\u00020\u0011H\u0016J\u000e\u0010\u0018\u001a\u00020\u000b2\u0006\u0010\u0007\u001a\u00020\bR\u001e\u0010\u0002\u001a\u0012\u0012\u0004\u0012\u00020\u00040\u0003j\b\u0012\u0004\u0012\u00020\u0004`\u0005X\u000e¢\u0006\u0002\n\u0000R\u0012\u0010\u0007\u001a\u0004\u0018\u00010\bX\u000e¢\u0006\u0004\n\u0002\b\t¨\u0006\u001b"}, d2 = {"Lcom/iqonic/store/adapter/ProductImageAdapter;", "Landroidx/viewpager/widget/PagerAdapter;", "mImg", "Ljava/util/ArrayList;", "", "Lkotlin/collections/ArrayList;", "(Ljava/util/ArrayList;)V", "mListener", "Lcom/iqonic/store/adapter/ProductImageAdapter$OnClickListener;", "mListener$1", "destroyItem", "", "parent", "Landroid/view/ViewGroup;", "position", "", "object", "", "getCount", "instantiateItem", "isViewFromObject", "", "v", "Landroid/view/View;", "setListener", "Companion", "OnClickListener", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: ProductImageAdapter.kt */
public final class ProductImageAdapter extends PagerAdapter {
    public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
    /* access modifiers changed from: private */
    public static OnClickListener mListener;
    private ArrayList<String> mImg;
    /* access modifiers changed from: private */
    public OnClickListener mListener$1;

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\b\n\u0000\bf\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&¨\u0006\u0006"}, d2 = {"Lcom/iqonic/store/adapter/ProductImageAdapter$OnClickListener;", "", "onClick", "", "position", "", "app_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: ProductImageAdapter.kt */
    public interface OnClickListener {
        void onClick(int i);
    }

    public ProductImageAdapter(ArrayList<String> arrayList) {
        Intrinsics.checkParameterIsNotNull(arrayList, "mImg");
        this.mImg = arrayList;
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u001c\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\b¨\u0006\t"}, d2 = {"Lcom/iqonic/store/adapter/ProductImageAdapter$Companion;", "", "()V", "mListener", "Lcom/iqonic/store/adapter/ProductImageAdapter$OnClickListener;", "getMListener", "()Lcom/iqonic/store/adapter/ProductImageAdapter$OnClickListener;", "setMListener", "(Lcom/iqonic/store/adapter/ProductImageAdapter$OnClickListener;)V", "app_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: ProductImageAdapter.kt */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        public final OnClickListener getMListener() {
            return ProductImageAdapter.mListener;
        }

        public final void setMListener(OnClickListener onClickListener) {
            ProductImageAdapter.mListener = onClickListener;
        }
    }

    public final void setListener(OnClickListener onClickListener) {
        Intrinsics.checkParameterIsNotNull(onClickListener, "mListener");
        this.mListener$1 = onClickListener;
    }

    public Object instantiateItem(ViewGroup viewGroup, int i) {
        Intrinsics.checkParameterIsNotNull(viewGroup, "parent");
        View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_itemimage, viewGroup, false);
        ImageView imageView = (ImageView) inflate.findViewById(com.iqonic.store.R.id.imgSlider);
        Intrinsics.checkExpressionValueIsNotNull(imageView, "imgSlider");
        String str = this.mImg.get(i);
        Intrinsics.checkExpressionValueIsNotNull(str, "mImg[position]");
        NetworkExtensionKt.loadImageFromUrl$default(imageView, str, 0, 0, 6, (Object) null);
        inflate.setOnClickListener(new ProductImageAdapter$instantiateItem$1(this, i));
        viewGroup.addView(inflate);
        Intrinsics.checkExpressionValueIsNotNull(inflate, "view");
        return inflate;
    }

    public boolean isViewFromObject(View view, Object obj) {
        Intrinsics.checkParameterIsNotNull(view, "v");
        Intrinsics.checkParameterIsNotNull(obj, "object");
        return view == ((View) obj);
    }

    public int getCount() {
        return this.mImg.size();
    }

    public void destroyItem(ViewGroup viewGroup, int i, Object obj) {
        Intrinsics.checkParameterIsNotNull(viewGroup, "parent");
        Intrinsics.checkParameterIsNotNull(obj, "object");
        viewGroup.removeView((View) obj);
    }
}
