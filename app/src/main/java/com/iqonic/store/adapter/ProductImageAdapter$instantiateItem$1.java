package com.iqonic.store.adapter;

import android.view.View;
import com.iqonic.store.adapter.ProductImageAdapter;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0017\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000*\u0001\u0000\b\n\u0018\u00002\u00020\u0001J\u0012\u0010\u0002\u001a\u00020\u00032\b\u0010\u0004\u001a\u0004\u0018\u00010\u0005H\u0016¨\u0006\u0006"}, d2 = {"com/iqonic/store/adapter/ProductImageAdapter$instantiateItem$1", "Landroid/view/View$OnClickListener;", "onClick", "", "v", "Landroid/view/View;", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: ProductImageAdapter.kt */
public final class ProductImageAdapter$instantiateItem$1 implements View.OnClickListener {
    final /* synthetic */ int $position;
    final /* synthetic */ ProductImageAdapter this$0;

    ProductImageAdapter$instantiateItem$1(ProductImageAdapter productImageAdapter, int i) {
        this.this$0 = productImageAdapter;
        this.$position = i;
    }

    public void onClick(View view) {
        if (this.this$0.mListener$1 != null) {
            ProductImageAdapter.OnClickListener access$getMListener$p = this.this$0.mListener$1;
            if (access$getMListener$p == null) {
                Intrinsics.throwNpe();
            }
            access$getMListener$p.onClick(this.$position);
        }
    }
}
