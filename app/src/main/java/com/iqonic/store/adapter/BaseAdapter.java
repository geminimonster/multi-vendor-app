package com.iqonic.store.adapter;

import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.iqonic.store.utils.extensions.ExtensionsKt;
import java.util.ArrayList;
import java.util.List;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function3;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000J\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0014\n\u0002\u0010 \n\u0002\b\n\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u0000*\u0004\b\u0000\u0010\u00012\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u0002H\u00010\u0003R\b\u0012\u0004\u0012\u0002H\u00010\u00000\u0002:\u00017BZ\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012K\u0010\u0006\u001aG\u0012\u0013\u0012\u00110\b¢\u0006\f\b\t\u0012\b\b\n\u0012\u0004\b\b(\u000b\u0012\u0013\u0012\u00118\u0000¢\u0006\f\b\t\u0012\b\b\n\u0012\u0004\b\b(\f\u0012\u0013\u0012\u00110\u0005¢\u0006\f\b\t\u0012\b\b\n\u0012\u0004\b\b(\r\u0012\u0004\u0012\u00020\u000e0\u0007¢\u0006\u0002\u0010\u000fJ\u0013\u0010$\u001a\u00020\u000e2\u0006\u0010\f\u001a\u00028\u0000¢\u0006\u0002\u0010%J\u0014\u0010&\u001a\u00020\u000e2\f\u0010\u0010\u001a\b\u0012\u0004\u0012\u00028\u00000'J\u0014\u0010(\u001a\u00020\u000e2\f\u0010)\u001a\b\u0012\u0004\u0012\u00028\u00000\u0011J\u0013\u0010*\u001a\u00020\u000e2\u0006\u0010\f\u001a\u00028\u0000¢\u0006\u0002\u0010%J\u0006\u0010+\u001a\u00020\u000eJ\b\u0010,\u001a\u00020\u0005H\u0016J\f\u0010-\u001a\b\u0012\u0004\u0012\u00028\u00000\u0011J(\u0010.\u001a\u00020\u000e2\u0016\u0010/\u001a\u0012\u0012\u0004\u0012\u00028\u00000\u0003R\b\u0012\u0004\u0012\u00028\u00000\u00002\u0006\u0010\u001a\u001a\u00020\u0005H\u0016J(\u00100\u001a\u0012\u0012\u0004\u0012\u00028\u00000\u0003R\b\u0012\u0004\u0012\u00028\u00000\u00002\u0006\u00101\u001a\u0002022\u0006\u00103\u001a\u00020\u0005H\u0016J\u000e\u00104\u001a\u00020\u000e2\u0006\u0010\u001a\u001a\u00020\u0005J\u000e\u00105\u001a\u00020\u000e2\u0006\u00106\u001a\u00020\u0005R*\u0010\u0010\u001a\u0012\u0012\u0004\u0012\u00028\u00000\u0011j\b\u0012\u0004\u0012\u00028\u0000`\u0012X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0013\u0010\u0014\"\u0004\b\u0015\u0010\u0016R\u000e\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000RV\u0010\u0006\u001aG\u0012\u0013\u0012\u00110\b¢\u0006\f\b\t\u0012\b\b\n\u0012\u0004\b\b(\u000b\u0012\u0013\u0012\u00118\u0000¢\u0006\f\b\t\u0012\b\b\n\u0012\u0004\b\b(\f\u0012\u0013\u0012\u00110\u0005¢\u0006\f\b\t\u0012\b\b\n\u0012\u0004\b\b(\r\u0012\u0004\u0012\u00020\u000e0\u0007¢\u0006\b\n\u0000\u001a\u0004\b\u0017\u0010\u0018Ra\u0010\u0019\u001aI\u0012\u0013\u0012\u00110\u0005¢\u0006\f\b\t\u0012\b\b\n\u0012\u0004\b\b(\u001a\u0012\u0013\u0012\u00110\b¢\u0006\f\b\t\u0012\b\b\n\u0012\u0004\b\b(\u000b\u0012\u0013\u0012\u00118\u0000¢\u0006\f\b\t\u0012\b\b\n\u0012\u0004\b\b(\f\u0012\u0004\u0012\u00020\u000e\u0018\u00010\u0007X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u001b\u0010\u0018\"\u0004\b\u001c\u0010\u001dR\u001e\u0010\u001e\u001a\u0004\u0018\u00010\u0005X\u000e¢\u0006\u0010\n\u0002\u0010#\u001a\u0004\b\u001f\u0010 \"\u0004\b!\u0010\"¨\u00068"}, d2 = {"Lcom/iqonic/store/adapter/BaseAdapter;", "T", "Landroidx/recyclerview/widget/RecyclerView$Adapter;", "Lcom/iqonic/store/adapter/BaseAdapter$ViewHolder;", "layout", "", "onBind", "Lkotlin/Function3;", "Landroid/view/View;", "Lkotlin/ParameterName;", "name", "view", "item", "position", "", "(ILkotlin/jvm/functions/Function3;)V", "items", "Ljava/util/ArrayList;", "Lkotlin/collections/ArrayList;", "getItems", "()Ljava/util/ArrayList;", "setItems", "(Ljava/util/ArrayList;)V", "getOnBind", "()Lkotlin/jvm/functions/Function3;", "onItemClick", "pos", "getOnItemClick", "setOnItemClick", "(Lkotlin/jvm/functions/Function3;)V", "size", "getSize", "()Ljava/lang/Integer;", "setSize", "(Ljava/lang/Integer;)V", "Ljava/lang/Integer;", "addItem", "(Ljava/lang/Object;)V", "addItems", "", "addMoreItems", "aList", "addNewItem", "clearItems", "getItemCount", "getModel", "onBindViewHolder", "holder", "onCreateViewHolder", "p0", "Landroid/view/ViewGroup;", "p1", "removeItem", "setModelSize", "aSize", "ViewHolder", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: BaseAdapter.kt */
public final class BaseAdapter<T> extends RecyclerView.Adapter<BaseAdapter<T>.ViewHolder<T>> {
    private ArrayList<T> items = new ArrayList<>();
    private final int layout;
    private final Function3<View, T, Integer, Unit> onBind;
    private Function3<? super Integer, ? super View, ? super T, Unit> onItemClick;
    private Integer size;

    public BaseAdapter(int i, Function3<? super View, ? super T, ? super Integer, Unit> function3) {
        Intrinsics.checkParameterIsNotNull(function3, "onBind");
        this.layout = i;
        this.onBind = function3;
    }

    public final Function3<View, T, Integer, Unit> getOnBind() {
        return this.onBind;
    }

    public final ArrayList<T> getItems() {
        return this.items;
    }

    public final void setItems(ArrayList<T> arrayList) {
        Intrinsics.checkParameterIsNotNull(arrayList, "<set-?>");
        this.items = arrayList;
    }

    public final Function3<Integer, View, T, Unit> getOnItemClick() {
        return this.onItemClick;
    }

    public final void setOnItemClick(Function3<? super Integer, ? super View, ? super T, Unit> function3) {
        this.onItemClick = function3;
    }

    public final Integer getSize() {
        return this.size;
    }

    public final void setSize(Integer num) {
        this.size = num;
    }

    public final void addItem(T t) {
        this.items.add(t);
        notifyItemInserted(this.items.size() - 1);
    }

    public final void addMoreItems(ArrayList<T> arrayList) {
        Intrinsics.checkParameterIsNotNull(arrayList, "aList");
        this.items.addAll(arrayList);
        setModelSize(getItemCount() + this.items.size());
        notifyDataSetChanged();
    }

    public final void addNewItem(T t) {
        this.items.add(t);
        notifyItemInserted(this.items.size() - 1);
    }

    public final ArrayList<T> getModel() {
        return this.items;
    }

    public final void addItems(List<? extends T> list) {
        Intrinsics.checkParameterIsNotNull(list, FirebaseAnalytics.Param.ITEMS);
        this.items.addAll(list);
        notifyDataSetChanged();
    }

    public final void removeItem(int i) {
        this.items.remove(i);
        notifyItemRemoved(i);
    }

    public final void clearItems() {
        this.items.clear();
        notifyDataSetChanged();
    }

    public final void setModelSize(int i) {
        this.size = Integer.valueOf(i);
        notifyDataSetChanged();
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\b\b\u0004\u0018\u0000*\u0004\b\u0001\u0010\u00012\u00020\u00022\u00020\u0003BZ\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012K\u0010\u0006\u001aG\u0012\u0013\u0012\u00110\u0005¢\u0006\f\b\b\u0012\b\b\t\u0012\u0004\b\b(\u0004\u0012\u0013\u0012\u00118\u0001¢\u0006\f\b\b\u0012\b\b\t\u0012\u0004\b\b(\n\u0012\u0013\u0012\u00110\u000b¢\u0006\f\b\b\u0012\b\b\t\u0012\u0004\b\b(\f\u0012\u0004\u0012\u00020\r0\u0007¢\u0006\u0002\u0010\u000eJ\u001b\u0010\u0011\u001a\u00020\r2\u0006\u0010\n\u001a\u00028\u00012\u0006\u0010\f\u001a\u00020\u000b¢\u0006\u0002\u0010\u0012J\u0012\u0010\u0013\u001a\u00020\r2\b\u0010\u0014\u001a\u0004\u0018\u00010\u0005H\u0016RV\u0010\u0006\u001aG\u0012\u0013\u0012\u00110\u0005¢\u0006\f\b\b\u0012\b\b\t\u0012\u0004\b\b(\u0004\u0012\u0013\u0012\u00118\u0001¢\u0006\f\b\b\u0012\b\b\t\u0012\u0004\b\b(\n\u0012\u0013\u0012\u00110\u000b¢\u0006\f\b\b\u0012\b\b\t\u0012\u0004\b\b(\f\u0012\u0004\u0012\u00020\r0\u0007¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010R\u000e\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0015"}, d2 = {"Lcom/iqonic/store/adapter/BaseAdapter$ViewHolder;", "T", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "Landroid/view/View$OnClickListener;", "view", "Landroid/view/View;", "onBind", "Lkotlin/Function3;", "Lkotlin/ParameterName;", "name", "item", "", "position", "", "(Lcom/iqonic/store/adapter/BaseAdapter;Landroid/view/View;Lkotlin/jvm/functions/Function3;)V", "getOnBind", "()Lkotlin/jvm/functions/Function3;", "bind", "(Ljava/lang/Object;I)V", "onClick", "p0", "app_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: BaseAdapter.kt */
    public final class ViewHolder<T> extends RecyclerView.ViewHolder implements View.OnClickListener {
        private final Function3<View, T, Integer, Unit> onBind;
        final /* synthetic */ BaseAdapter this$0;
        private final View view;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public ViewHolder(BaseAdapter baseAdapter, View view2, Function3<? super View, ? super T, ? super Integer, Unit> function3) {
            super(view2);
            Intrinsics.checkParameterIsNotNull(view2, "view");
            Intrinsics.checkParameterIsNotNull(function3, "onBind");
            this.this$0 = baseAdapter;
            this.view = view2;
            this.onBind = function3;
        }

        public final Function3<View, T, Integer, Unit> getOnBind() {
            return this.onBind;
        }

        public void onClick(View view2) {
            Function3 onItemClick = this.this$0.getOnItemClick();
            if (onItemClick != null) {
                Integer valueOf = Integer.valueOf(getAdapterPosition());
                if (view2 == null) {
                    Intrinsics.throwNpe();
                }
                Unit unit = (Unit) onItemClick.invoke(valueOf, view2, this.this$0.getItems().get(getAdapterPosition()));
            }
        }

        public final void bind(T t, int i) {
            this.view.setOnClickListener(this);
            this.onBind.invoke(this.view, t, Integer.valueOf(i));
        }
    }

    public BaseAdapter<T>.ViewHolder<T> onCreateViewHolder(ViewGroup viewGroup, int i) {
        Intrinsics.checkParameterIsNotNull(viewGroup, "p0");
        return new ViewHolder<>(this, ExtensionsKt.inflate(viewGroup, this.layout), this.onBind);
    }

    public int getItemCount() {
        return this.items.size();
    }

    public void onBindViewHolder(BaseAdapter<T>.ViewHolder<T> viewHolder, int i) {
        Intrinsics.checkParameterIsNotNull(viewHolder, "holder");
        viewHolder.bind(this.items.get(i), i);
    }
}
