package com.iqonic.store.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.iqonic.store.utils.extensions.ExtensionsKt;
import com.store.proshop.R;
import java.util.ArrayList;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B'\u0012\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004\u0012\u0016\u0010\u0005\u001a\u0012\u0012\u0004\u0012\u00020\u00020\u0006j\b\u0012\u0004\u0012\u00020\u0002`\u0007¢\u0006\u0002\u0010\bJ\"\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\f2\b\u0010\r\u001a\u0004\u0018\u00010\n2\u0006\u0010\u000e\u001a\u00020\u000fH\u0017¨\u0006\u0010"}, d2 = {"Lcom/iqonic/store/adapter/SpinnerAdapter;", "Landroid/widget/ArrayAdapter;", "", "ctx", "Landroid/content/Context;", "texts", "Ljava/util/ArrayList;", "Lkotlin/collections/ArrayList;", "(Landroid/content/Context;Ljava/util/ArrayList;)V", "getView", "Landroid/view/View;", "position", "", "convertView", "parent", "Landroid/view/ViewGroup;", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: SpinnerAdapter.kt */
public final class SpinnerAdapter extends ArrayAdapter<String> {
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SpinnerAdapter(Context context, ArrayList<String> arrayList) {
        super(context, 17367048, arrayList);
        Intrinsics.checkParameterIsNotNull(arrayList, "texts");
        if (context == null) {
            Intrinsics.throwNpe();
        }
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        Intrinsics.checkParameterIsNotNull(viewGroup, "parent");
        View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.spinner_items, viewGroup, false);
        Intrinsics.checkExpressionValueIsNotNull(inflate, "LayoutInflater.from(pare…ner_items, parent, false)");
        View findViewById = inflate.findViewById(R.id.tvItem);
        if (findViewById != null) {
            TextView textView = (TextView) findViewById;
            textView.setText((CharSequence) getItem(i));
            textView.setPadding(4, 20, 4, 8);
            ExtensionsKt.changeTextSecondaryColor(textView);
            return inflate;
        }
        throw new TypeCastException("null cannot be cast to non-null type android.widget.TextView");
    }
}
