package com.iqonic.store.adapter;

import android.view.View;
import com.iqonic.store.adapter.HomeSliderAdapter;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n¢\u0006\u0002\b\u0005"}, d2 = {"<anonymous>", "", "it", "Landroid/view/View;", "kotlin.jvm.PlatformType", "onClick"}, k = 3, mv = {1, 1, 16})
/* compiled from: HomeSliderAdapter.kt */
final class HomeSliderAdapter$instantiateItem$1 implements View.OnClickListener {
    final /* synthetic */ int $position;
    final /* synthetic */ HomeSliderAdapter this$0;

    HomeSliderAdapter$instantiateItem$1(HomeSliderAdapter homeSliderAdapter, int i) {
        this.this$0 = homeSliderAdapter;
        this.$position = i;
    }

    public final void onClick(View view) {
        if (this.this$0.mListener != null) {
            HomeSliderAdapter.OnClickListener access$getMListener$p = this.this$0.mListener;
            if (access$getMListener$p == null) {
                Intrinsics.throwNpe();
            }
            access$getMListener$p.onClick(this.$position);
        }
    }
}
