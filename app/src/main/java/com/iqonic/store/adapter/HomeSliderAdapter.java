package com.iqonic.store.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.viewpager.widget.PagerAdapter;
import com.iqonic.store.models.DashboardBanner;
import com.iqonic.store.utils.extensions.NetworkExtensionKt;
import com.store.proshop.R;
import java.util.List;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000H\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0006\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\u00020\u0001:\u0001\u001dB\u0013\u0012\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003¢\u0006\u0002\u0010\u0005J \u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\t2\u0006\u0010\u0014\u001a\u00020\u0015H\u0016J\b\u0010\u0016\u001a\u00020\tH\u0016J\u0018\u0010\u0017\u001a\u00020\u00152\u0006\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\tH\u0016J\u0018\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00020\u001b2\u0006\u0010\u0014\u001a\u00020\u0015H\u0016J\u000e\u0010\u001c\u001a\u00020\u00102\u0006\u0010\u0006\u001a\u00020\u0007R\u0014\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003X\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\u0006\u001a\u0004\u0018\u00010\u0007X\u000e¢\u0006\u0002\n\u0000R\u001e\u0010\b\u001a\u0004\u0018\u00010\tX\u000e¢\u0006\u0010\n\u0002\u0010\u000e\u001a\u0004\b\n\u0010\u000b\"\u0004\b\f\u0010\r¨\u0006\u001e"}, d2 = {"Lcom/iqonic/store/adapter/HomeSliderAdapter;", "Landroidx/viewpager/widget/PagerAdapter;", "mImg", "", "Lcom/iqonic/store/models/DashboardBanner;", "(Ljava/util/List;)V", "mListener", "Lcom/iqonic/store/adapter/HomeSliderAdapter$OnClickListener;", "size", "", "getSize", "()Ljava/lang/Integer;", "setSize", "(Ljava/lang/Integer;)V", "Ljava/lang/Integer;", "destroyItem", "", "parent", "Landroid/view/ViewGroup;", "position", "object", "", "getCount", "instantiateItem", "isViewFromObject", "", "v", "Landroid/view/View;", "setListener", "OnClickListener", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: HomeSliderAdapter.kt */
public final class HomeSliderAdapter extends PagerAdapter {
    private List<DashboardBanner> mImg;
    /* access modifiers changed from: private */
    public OnClickListener mListener;
    private Integer size;

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\b\n\u0000\bf\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&¨\u0006\u0006"}, d2 = {"Lcom/iqonic/store/adapter/HomeSliderAdapter$OnClickListener;", "", "onClick", "", "position", "", "app_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: HomeSliderAdapter.kt */
    public interface OnClickListener {
        void onClick(int i);
    }

    public HomeSliderAdapter(List<DashboardBanner> list) {
        Intrinsics.checkParameterIsNotNull(list, "mImg");
        this.mImg = list;
    }

    public final Integer getSize() {
        return this.size;
    }

    public final void setSize(Integer num) {
        this.size = num;
    }

    public final void setListener(OnClickListener onClickListener) {
        Intrinsics.checkParameterIsNotNull(onClickListener, "mListener");
        this.mListener = onClickListener;
    }

    public Object instantiateItem(ViewGroup viewGroup, int i) {
        Intrinsics.checkParameterIsNotNull(viewGroup, "parent");
        View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_slider, viewGroup, false);
        Intrinsics.checkExpressionValueIsNotNull(inflate, "view");
        ImageView imageView = (ImageView) inflate.findViewById(com.iqonic.store.R.id.img);
        Intrinsics.checkExpressionValueIsNotNull(imageView, "view.img");
        NetworkExtensionKt.loadImageFromUrl$default(imageView, this.mImg.get(i).getImage(), 0, 0, 6, (Object) null);
        inflate.setOnClickListener(new HomeSliderAdapter$instantiateItem$1(this, i));
        viewGroup.addView(inflate);
        return inflate;
    }

    public boolean isViewFromObject(View view, Object obj) {
        Intrinsics.checkParameterIsNotNull(view, "v");
        Intrinsics.checkParameterIsNotNull(obj, "object");
        return view == ((View) obj);
    }

    public int getCount() {
        return this.mImg.size();
    }

    public void destroyItem(ViewGroup viewGroup, int i, Object obj) {
        Intrinsics.checkParameterIsNotNull(viewGroup, "parent");
        Intrinsics.checkParameterIsNotNull(obj, "object");
        viewGroup.removeView((View) obj);
    }
}
