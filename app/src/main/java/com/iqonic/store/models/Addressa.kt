<?xml version="1.0" encoding="utf-8"?>
<resources>

<style name="AccountLabel">
<item name="android:textSize">@dimen /font_size_normal</item>
<item name="android:textColor">@color /textColorPrimary</item>
<item name="android:layout_width">fill_parent</item>
<item name="android:layout_height">wrap_content</item>
<item name="android:l