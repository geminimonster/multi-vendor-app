package com.iqonic.store.models;

import java.io.Serializable;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u000b\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0003\b\b\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006¢\u0006\u0002\u0010\u0007J\t\u0010\r\u001a\u00020\u0003HÆ\u0003J\t\u0010\u000e\u001a\u00020\u0003HÆ\u0003J\t\u0010\u000f\u001a\u00020\u0006HÆ\u0003J'\u0010\u0010\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00032\b\b\u0002\u0010\u0005\u001a\u00020\u0006HÆ\u0001J\u0013\u0010\u0011\u001a\u00020\u00122\b\u0010\u0013\u001a\u0004\u0018\u00010\u0014HÖ\u0003J\t\u0010\u0015\u001a\u00020\u0006HÖ\u0001J\t\u0010\u0016\u001a\u00020\u0003HÖ\u0001R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u0011\u0010\u0004\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\tR\u0011\u0010\u0005\u001a\u00020\u0006¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\f¨\u0006\u0017"}, d2 = {"Lcom/iqonic/store/models/DateModified;", "Ljava/io/Serializable;", "date", "", "timezone", "timezone_type", "", "(Ljava/lang/String;Ljava/lang/String;I)V", "getDate", "()Ljava/lang/String;", "getTimezone", "getTimezone_type", "()I", "component1", "component2", "component3", "copy", "equals", "", "other", "", "hashCode", "toString", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: BaseResponse.kt */
public final class DateModified implements Serializable {
    private final String date;
    private final String timezone;
    private final int timezone_type;

    public static /* synthetic */ DateModified copy$default(DateModified dateModified, String str, String str2, int i, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            str = dateModified.date;
        }
        if ((i2 & 2) != 0) {
            str2 = dateModified.timezone;
        }
        if ((i2 & 4) != 0) {
            i = dateModified.timezone_type;
        }
        return dateModified.copy(str, str2, i);
    }

    public final String component1() {
        return this.date;
    }

    public final String component2() {
        return this.timezone;
    }

    public final int component3() {
        return this.timezone_type;
    }

    public final DateModified copy(String str, String str2, int i) {
        Intrinsics.checkParameterIsNotNull(str, "date");
        Intrinsics.checkParameterIsNotNull(str2, "timezone");
        return new DateModified(str, str2, i);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof DateModified)) {
            return false;
        }
        DateModified dateModified = (DateModified) obj;
        return Intrinsics.areEqual((Object) this.date, (Object) dateModified.date) && Intrinsics.areEqual((Object) this.timezone, (Object) dateModified.timezone) && this.timezone_type == dateModified.timezone_type;
    }

    public int hashCode() {
        String str = this.date;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.timezone;
        if (str2 != null) {
            i = str2.hashCode();
        }
        return ((hashCode + i) * 31) + this.timezone_type;
    }

    public String toString() {
        return "DateModified(date=" + this.date + ", timezone=" + this.timezone + ", timezone_type=" + this.timezone_type + ")";
    }

    public DateModified(String str, String str2, int i) {
        Intrinsics.checkParameterIsNotNull(str, "date");
        Intrinsics.checkParameterIsNotNull(str2, "timezone");
        this.date = str;
        this.timezone = str2;
        this.timezone_type = i;
    }

    public final String getDate() {
        return this.date;
    }

    public final String getTimezone() {
        return this.timezone;
    }

    public final int getTimezone_type() {
        return this.timezone_type;
    }
}
