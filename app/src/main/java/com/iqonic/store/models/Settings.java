package com.iqonic.store.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import kotlin.Metadata;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\b\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002R \u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\bR \u0010\t\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\n\u0010\u0006\"\u0004\b\u000b\u0010\b¨\u0006\f"}, d2 = {"Lcom/iqonic/store/models/Settings;", "", "()V", "classCostFlate", "", "getClassCostFlate", "()Ljava/lang/String;", "setClassCostFlate", "(Ljava/lang/String;)V", "classCostFreeShipping", "getClassCostFreeShipping", "setClassCostFreeShipping", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: ShippingModel.kt */
public final class Settings {
    @SerializedName("class_cost_flate")
    @Expose
    private String classCostFlate;
    @SerializedName("class_cost_free-shipping")
    @Expose
    private String classCostFreeShipping;

    public final String getClassCostFlate() {
        return this.classCostFlate;
    }

    public final void setClassCostFlate(String str) {
        this.classCostFlate = str;
    }

    public final String getClassCostFreeShipping() {
        return this.classCostFreeShipping;
    }

    public final void setClassCostFreeShipping(String str) {
        this.classCostFreeShipping = str;
    }
}
