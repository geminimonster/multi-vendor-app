package com.iqonic.store.models;

import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0002\b\t\n\u0002\u0010\u000b\n\u0002\b&\b\b\u0018\u00002\u00020\u0001B}\u0012\b\b\u0002\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0002\u0010\u0004\u001a\u00020\u0005\u0012\b\b\u0002\u0010\u0006\u001a\u00020\u0005\u0012\b\b\u0002\u0010\u0007\u001a\u00020\u0005\u0012\b\b\u0002\u0010\b\u001a\u00020\u0003\u0012\b\b\u0002\u0010\t\u001a\u00020\u0005\u0012\b\b\u0002\u0010\n\u001a\u00020\u0005\u0012\b\b\u0002\u0010\u000b\u001a\u00020\u0003\u0012\b\b\u0002\u0010\f\u001a\u00020\u0003\u0012\b\b\u0002\u0010\r\u001a\u00020\u0003\u0012\b\b\u0002\u0010\u000e\u001a\u00020\u000f\u0012\b\b\u0002\u0010\u0010\u001a\u00020\u000f¢\u0006\u0002\u0010\u0011J\t\u0010$\u001a\u00020\u0003HÆ\u0003J\t\u0010%\u001a\u00020\u0003HÆ\u0003J\t\u0010&\u001a\u00020\u000fHÆ\u0003J\t\u0010'\u001a\u00020\u000fHÆ\u0003J\t\u0010(\u001a\u00020\u0005HÆ\u0003J\t\u0010)\u001a\u00020\u0005HÆ\u0003J\t\u0010*\u001a\u00020\u0005HÆ\u0003J\t\u0010+\u001a\u00020\u0003HÆ\u0003J\t\u0010,\u001a\u00020\u0005HÆ\u0003J\t\u0010-\u001a\u00020\u0005HÆ\u0003J\t\u0010.\u001a\u00020\u0003HÆ\u0003J\t\u0010/\u001a\u00020\u0003HÆ\u0003J\u0001\u00100\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u00052\b\b\u0002\u0010\u0007\u001a\u00020\u00052\b\b\u0002\u0010\b\u001a\u00020\u00032\b\b\u0002\u0010\t\u001a\u00020\u00052\b\b\u0002\u0010\n\u001a\u00020\u00052\b\b\u0002\u0010\u000b\u001a\u00020\u00032\b\b\u0002\u0010\f\u001a\u00020\u00032\b\b\u0002\u0010\r\u001a\u00020\u00032\b\b\u0002\u0010\u000e\u001a\u00020\u000f2\b\b\u0002\u0010\u0010\u001a\u00020\u000fHÆ\u0001J\u0013\u00101\u001a\u00020\u000f2\b\u00102\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u00103\u001a\u00020\u0003HÖ\u0001J\t\u00104\u001a\u00020\u0005HÖ\u0001R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u0013R\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0014\u0010\u0015R\u0011\u0010\u0006\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0016\u0010\u0015R\u001a\u0010\u0010\u001a\u00020\u000fX\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0010\u0010\u0017\"\u0004\b\u0018\u0010\u0019R\u001a\u0010\u000e\u001a\u00020\u000fX\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u000e\u0010\u0017\"\u0004\b\u001a\u0010\u0019R\u001a\u0010\u0007\u001a\u00020\u0005X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u001b\u0010\u0015\"\u0004\b\u001c\u0010\u001dR\u0011\u0010\b\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u001e\u0010\u0013R\u0011\u0010\t\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u001f\u0010\u0015R\u0011\u0010\n\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b \u0010\u0015R\u0011\u0010\u000b\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b!\u0010\u0013R\u0011\u0010\f\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\"\u0010\u0013R\u0011\u0010\r\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b#\u0010\u0013¨\u00065"}, d2 = {"Lcom/iqonic/store/models/Term;", "", "count", "", "description", "", "filter", "name", "parent", "slug", "taxonomy", "term_group", "term_id", "term_taxonomy_id", "isSelected", "", "isParent", "(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IIIZZ)V", "getCount", "()I", "getDescription", "()Ljava/lang/String;", "getFilter", "()Z", "setParent", "(Z)V", "setSelected", "getName", "setName", "(Ljava/lang/String;)V", "getParent", "getSlug", "getTaxonomy", "getTerm_group", "getTerm_id", "getTerm_taxonomy_id", "component1", "component10", "component11", "component12", "component2", "component3", "component4", "component5", "component6", "component7", "component8", "component9", "copy", "equals", "other", "hashCode", "toString", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: BaseResponse.kt */
public final class Term {
    private final int count;
    private final String description;
    private final String filter;
    private boolean isParent;
    private boolean isSelected;
    private String name;
    private final int parent;
    private final String slug;
    private final String taxonomy;
    private final int term_group;
    private final int term_id;
    private final int term_taxonomy_id;

    public Term() {
        this(0, (String) null, (String) null, (String) null, 0, (String) null, (String) null, 0, 0, 0, false, false, 4095, (DefaultConstructorMarker) null);
    }

    public static /* synthetic */ Term copy$default(Term term, int i, String str, String str2, String str3, int i2, String str4, String str5, int i3, int i4, int i5, boolean z, boolean z2, int i6, Object obj) {
        Term term2 = term;
        int i7 = i6;
        return term.copy((i7 & 1) != 0 ? term2.count : i, (i7 & 2) != 0 ? term2.description : str, (i7 & 4) != 0 ? term2.filter : str2, (i7 & 8) != 0 ? term2.name : str3, (i7 & 16) != 0 ? term2.parent : i2, (i7 & 32) != 0 ? term2.slug : str4, (i7 & 64) != 0 ? term2.taxonomy : str5, (i7 & 128) != 0 ? term2.term_group : i3, (i7 & 256) != 0 ? term2.term_id : i4, (i7 & 512) != 0 ? term2.term_taxonomy_id : i5, (i7 & 1024) != 0 ? term2.isSelected : z, (i7 & 2048) != 0 ? term2.isParent : z2);
    }

    public final int component1() {
        return this.count;
    }

    public final int component10() {
        return this.term_taxonomy_id;
    }

    public final boolean component11() {
        return this.isSelected;
    }

    public final boolean component12() {
        return this.isParent;
    }

    public final String component2() {
        return this.description;
    }

    public final String component3() {
        return this.filter;
    }

    public final String component4() {
        return this.name;
    }

    public final int component5() {
        return this.parent;
    }

    public final String component6() {
        return this.slug;
    }

    public final String component7() {
        return this.taxonomy;
    }

    public final int component8() {
        return this.term_group;
    }

    public final int component9() {
        return this.term_id;
    }

    public final Term copy(int i, String str, String str2, String str3, int i2, String str4, String str5, int i3, int i4, int i5, boolean z, boolean z2) {
        String str6 = str;
        Intrinsics.checkParameterIsNotNull(str6, "description");
        String str7 = str2;
        Intrinsics.checkParameterIsNotNull(str7, "filter");
        String str8 = str3;
        Intrinsics.checkParameterIsNotNull(str8, "name");
        String str9 = str4;
        Intrinsics.checkParameterIsNotNull(str9, "slug");
        String str10 = str5;
        Intrinsics.checkParameterIsNotNull(str10, "taxonomy");
        return new Term(i, str6, str7, str8, i2, str9, str10, i3, i4, i5, z, z2);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Term)) {
            return false;
        }
        Term term = (Term) obj;
        return this.count == term.count && Intrinsics.areEqual((Object) this.description, (Object) term.description) && Intrinsics.areEqual((Object) this.filter, (Object) term.filter) && Intrinsics.areEqual((Object) this.name, (Object) term.name) && this.parent == term.parent && Intrinsics.areEqual((Object) this.slug, (Object) term.slug) && Intrinsics.areEqual((Object) this.taxonomy, (Object) term.taxonomy) && this.term_group == term.term_group && this.term_id == term.term_id && this.term_taxonomy_id == term.term_taxonomy_id && this.isSelected == term.isSelected && this.isParent == term.isParent;
    }

    public int hashCode() {
        int i = this.count * 31;
        String str = this.description;
        int i2 = 0;
        int hashCode = (i + (str != null ? str.hashCode() : 0)) * 31;
        String str2 = this.filter;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.name;
        int hashCode3 = (((hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31) + this.parent) * 31;
        String str4 = this.slug;
        int hashCode4 = (hashCode3 + (str4 != null ? str4.hashCode() : 0)) * 31;
        String str5 = this.taxonomy;
        if (str5 != null) {
            i2 = str5.hashCode();
        }
        int i3 = (((((((hashCode4 + i2) * 31) + this.term_group) * 31) + this.term_id) * 31) + this.term_taxonomy_id) * 31;
        boolean z = this.isSelected;
        boolean z2 = true;
        if (z) {
            z = true;
        }
        int i4 = (i3 + (z ? 1 : 0)) * 31;
        boolean z3 = this.isParent;
        if (!z3) {
            z2 = z3;
        }
        return i4 + (z2 ? 1 : 0);
    }

    public String toString() {
        return "Term(count=" + this.count + ", description=" + this.description + ", filter=" + this.filter + ", name=" + this.name + ", parent=" + this.parent + ", slug=" + this.slug + ", taxonomy=" + this.taxonomy + ", term_group=" + this.term_group + ", term_id=" + this.term_id + ", term_taxonomy_id=" + this.term_taxonomy_id + ", isSelected=" + this.isSelected + ", isParent=" + this.isParent + ")";
    }

    public Term(int i, String str, String str2, String str3, int i2, String str4, String str5, int i3, int i4, int i5, boolean z, boolean z2) {
        Intrinsics.checkParameterIsNotNull(str, "description");
        Intrinsics.checkParameterIsNotNull(str2, "filter");
        Intrinsics.checkParameterIsNotNull(str3, "name");
        Intrinsics.checkParameterIsNotNull(str4, "slug");
        Intrinsics.checkParameterIsNotNull(str5, "taxonomy");
        this.count = i;
        this.description = str;
        this.filter = str2;
        this.name = str3;
        this.parent = i2;
        this.slug = str4;
        this.taxonomy = str5;
        this.term_group = i3;
        this.term_id = i4;
        this.term_taxonomy_id = i5;
        this.isSelected = z;
        this.isParent = z2;
    }

    public final int getCount() {
        return this.count;
    }

    public final String getDescription() {
        return this.description;
    }

    public final String getFilter() {
        return this.filter;
    }

    public final String getName() {
        return this.name;
    }

    public final void setName(String str) {
        Intrinsics.checkParameterIsNotNull(str, "<set-?>");
        this.name = str;
    }

    public final int getParent() {
        return this.parent;
    }

    public final String getSlug() {
        return this.slug;
    }

    public final String getTaxonomy() {
        return this.taxonomy;
    }

    public final int getTerm_group() {
        return this.term_group;
    }

    public final int getTerm_id() {
        return this.term_id;
    }

    public final int getTerm_taxonomy_id() {
        return this.term_taxonomy_id;
    }

    public final boolean isSelected() {
        return this.isSelected;
    }

    public final void setSelected(boolean z) {
        this.isSelected = z;
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* synthetic */ Term(int r14, java.lang.String r15, java.lang.String r16, java.lang.String r17, int r18, java.lang.String r19, java.lang.String r20, int r21, int r22, int r23, boolean r24, boolean r25, int r26, kotlin.jvm.internal.DefaultConstructorMarker r27) {
        /*
            r13 = this;
            r0 = r26
            r1 = r0 & 1
            r2 = 0
            if (r1 == 0) goto L_0x0009
            r1 = 0
            goto L_0x000a
        L_0x0009:
            r1 = r14
        L_0x000a:
            r3 = r0 & 2
            java.lang.String r4 = ""
            if (r3 == 0) goto L_0x0012
            r3 = r4
            goto L_0x0013
        L_0x0012:
            r3 = r15
        L_0x0013:
            r5 = r0 & 4
            if (r5 == 0) goto L_0x0019
            r5 = r4
            goto L_0x001b
        L_0x0019:
            r5 = r16
        L_0x001b:
            r6 = r0 & 8
            if (r6 == 0) goto L_0x0021
            r6 = r4
            goto L_0x0023
        L_0x0021:
            r6 = r17
        L_0x0023:
            r7 = r0 & 16
            if (r7 == 0) goto L_0x0029
            r7 = 0
            goto L_0x002b
        L_0x0029:
            r7 = r18
        L_0x002b:
            r8 = r0 & 32
            if (r8 == 0) goto L_0x0031
            r8 = r4
            goto L_0x0033
        L_0x0031:
            r8 = r19
        L_0x0033:
            r9 = r0 & 64
            if (r9 == 0) goto L_0x0038
            goto L_0x003a
        L_0x0038:
            r4 = r20
        L_0x003a:
            r9 = r0 & 128(0x80, float:1.794E-43)
            if (r9 == 0) goto L_0x0040
            r9 = 0
            goto L_0x0042
        L_0x0040:
            r9 = r21
        L_0x0042:
            r10 = r0 & 256(0x100, float:3.59E-43)
            if (r10 == 0) goto L_0x0048
            r10 = 0
            goto L_0x004a
        L_0x0048:
            r10 = r22
        L_0x004a:
            r11 = r0 & 512(0x200, float:7.175E-43)
            if (r11 == 0) goto L_0x0050
            r11 = 0
            goto L_0x0052
        L_0x0050:
            r11 = r23
        L_0x0052:
            r12 = r0 & 1024(0x400, float:1.435E-42)
            if (r12 == 0) goto L_0x0058
            r12 = 0
            goto L_0x005a
        L_0x0058:
            r12 = r24
        L_0x005a:
            r0 = r0 & 2048(0x800, float:2.87E-42)
            if (r0 == 0) goto L_0x005f
            goto L_0x0061
        L_0x005f:
            r2 = r25
        L_0x0061:
            r14 = r13
            r15 = r1
            r16 = r3
            r17 = r5
            r18 = r6
            r19 = r7
            r20 = r8
            r21 = r4
            r22 = r9
            r23 = r10
            r24 = r11
            r25 = r12
            r26 = r2
            r14.<init>(r15, r16, r17, r18, r19, r20, r21, r22, r23, r24, r25, r26)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.iqonic.store.models.Term.<init>(int, java.lang.String, java.lang.String, java.lang.String, int, java.lang.String, java.lang.String, int, int, int, boolean, boolean, int, kotlin.jvm.internal.DefaultConstructorMarker):void");
    }

    public final boolean isParent() {
        return this.isParent;
    }

    public final void setParent(boolean z) {
        this.isParent = z;
    }
}
