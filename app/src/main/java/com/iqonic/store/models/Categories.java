package com.iqonic.store.models;

import java.util.List;
import kotlin.Metadata;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b#\n\u0002\u0010 \n\u0002\b\u0011\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002R\u001a\u0010\u0003\u001a\u00020\u0004X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\bR\u001c\u0010\t\u001a\u0004\u0018\u00010\nX\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u000b\u0010\f\"\u0004\b\r\u0010\u000eR\u001a\u0010\u000f\u001a\u00020\u0004X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0010\u0010\u0006\"\u0004\b\u0011\u0010\bR\u001c\u0010\u0012\u001a\u0004\u0018\u00010\nX\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0013\u0010\f\"\u0004\b\u0014\u0010\u000eR\u001c\u0010\u0015\u001a\u0004\u0018\u00010\nX\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0016\u0010\f\"\u0004\b\u0017\u0010\u000eR\u001a\u0010\u0018\u001a\u00020\u0004X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0019\u0010\u0006\"\u0004\b\u001a\u0010\bR\u001a\u0010\u001b\u001a\u00020\u0004X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u001c\u0010\u0006\"\u0004\b\u001d\u0010\bR\u001c\u0010\u001e\u001a\u0004\u0018\u00010\nX\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u001f\u0010\f\"\u0004\b \u0010\u000eR\u001c\u0010!\u001a\u0004\u0018\u00010\nX\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\"\u0010\f\"\u0004\b#\u0010\u000eR\u001c\u0010$\u001a\u0004\u0018\u00010\nX\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b%\u0010\f\"\u0004\b&\u0010\u000eR\u001a\u0010'\u001a\u00020\u0004X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b(\u0010\u0006\"\u0004\b)\u0010\bR\u001c\u0010*\u001a\u0004\u0018\u00010\nX\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b+\u0010\f\"\u0004\b,\u0010\u000eR\"\u0010-\u001a\n\u0012\u0004\u0012\u00020\n\u0018\u00010.X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b/\u00100\"\u0004\b1\u00102R\u001c\u00103\u001a\u0004\u0018\u00010\nX\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b4\u0010\f\"\u0004\b5\u0010\u000eR\u001a\u00106\u001a\u00020\u0004X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b7\u0010\u0006\"\u0004\b8\u0010\bR\u001a\u00109\u001a\u00020\u0004X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b:\u0010\u0006\"\u0004\b;\u0010\bR\u001a\u0010<\u001a\u00020\u0004X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b=\u0010\u0006\"\u0004\b>\u0010\b¨\u0006?"}, d2 = {"Lcom/iqonic/store/models/Categories;", "", "()V", "cat_ID", "", "getCat_ID", "()I", "setCat_ID", "(I)V", "cat_name", "", "getCat_name", "()Ljava/lang/String;", "setCat_name", "(Ljava/lang/String;)V", "category_count", "getCategory_count", "setCategory_count", "category_description", "getCategory_description", "setCategory_description", "category_nicename", "getCategory_nicename", "setCategory_nicename", "category_parent", "getCategory_parent", "setCategory_parent", "count", "getCount", "setCount", "description", "getDescription", "setDescription", "filter", "getFilter", "setFilter", "name", "getName", "setName", "parent", "getParent", "setParent", "slug", "getSlug", "setSlug", "sub_categories", "", "getSub_categories", "()Ljava/util/List;", "setSub_categories", "(Ljava/util/List;)V", "taxonomy", "getTaxonomy", "setTaxonomy", "term_group", "getTerm_group", "setTerm_group", "term_id", "getTerm_id", "setTerm_id", "term_taxonomy_id", "getTerm_taxonomy_id", "setTerm_taxonomy_id", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: BaseResponse.kt */
public final class Categories {
    private int cat_ID;
    private String cat_name;
    private int category_count;
    private String category_description;
    private String category_nicename;
    private int category_parent;
    private int count;
    private String description;
    private String filter;
    private String name;
    private int parent;
    private String slug;
    private List<String> sub_categories;
    private String taxonomy;
    private int term_group;
    private int term_id;
    private int term_taxonomy_id;

    public final int getTerm_id() {
        return this.term_id;
    }

    public final void setTerm_id(int i) {
        this.term_id = i;
    }

    public final String getName() {
        return this.name;
    }

    public final void setName(String str) {
        this.name = str;
    }

    public final String getSlug() {
        return this.slug;
    }

    public final void setSlug(String str) {
        this.slug = str;
    }

    public final int getTerm_group() {
        return this.term_group;
    }

    public final void setTerm_group(int i) {
        this.term_group = i;
    }

    public final int getTerm_taxonomy_id() {
        return this.term_taxonomy_id;
    }

    public final void setTerm_taxonomy_id(int i) {
        this.term_taxonomy_id = i;
    }

    public final String getTaxonomy() {
        return this.taxonomy;
    }

    public final void setTaxonomy(String str) {
        this.taxonomy = str;
    }

    public final String getDescription() {
        return this.description;
    }

    public final void setDescription(String str) {
        this.description = str;
    }

    public final int getParent() {
        return this.parent;
    }

    public final void setParent(int i) {
        this.parent = i;
    }

    public final int getCount() {
        return this.count;
    }

    public final void setCount(int i) {
        this.count = i;
    }

    public final String getFilter() {
        return this.filter;
    }

    public final void setFilter(String str) {
        this.filter = str;
    }

    public final int getCat_ID() {
        return this.cat_ID;
    }

    public final void setCat_ID(int i) {
        this.cat_ID = i;
    }

    public final int getCategory_count() {
        return this.category_count;
    }

    public final void setCategory_count(int i) {
        this.category_count = i;
    }

    public final String getCategory_description() {
        return this.category_description;
    }

    public final void setCategory_description(String str) {
        this.category_description = str;
    }

    public final String getCat_name() {
        return this.cat_name;
    }

    public final void setCat_name(String str) {
        this.cat_name = str;
    }

    public final String getCategory_nicename() {
        return this.category_nicename;
    }

    public final void setCategory_nicename(String str) {
        this.category_nicename = str;
    }

    public final int getCategory_parent() {
        return this.category_parent;
    }

    public final void setCategory_parent(int i) {
        this.category_parent = i;
    }

    public final List<String> getSub_categories() {
        return this.sub_categories;
    }

    public final void setSub_categories(List<String> list) {
        this.sub_categories = list;
    }
}
