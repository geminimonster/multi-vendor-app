package com.iqonic.store.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import java.util.List;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0010 \n\u0002\b\u0014\n\u0002\u0010\u000b\n\u0002\b\f\n\u0002\u0010\b\n\u0002\b.\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002R \u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\bR\u001e\u0010\t\u001a\u00020\n8\u0006@\u0006X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u000b\u0010\f\"\u0004\b\r\u0010\u000eR&\u0010\u000f\u001a\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u00108\u0006@\u0006X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0011\u0010\u0012\"\u0004\b\u0013\u0010\u0014R&\u0010\u0015\u001a\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u00108\u0006@\u0006X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0016\u0010\u0012\"\u0004\b\u0017\u0010\u0014R\u001e\u0010\u0018\u001a\u00020\n8\u0006@\u0006X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0019\u0010\f\"\u0004\b\u001a\u0010\u000eR&\u0010\u001b\u001a\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u00108\u0006@\u0006X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u001c\u0010\u0012\"\u0004\b\u001d\u0010\u0014R \u0010\u001e\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u001f\u0010\u0006\"\u0004\b \u0010\bR&\u0010!\u001a\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u00108\u0006@\u0006X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\"\u0010\u0012\"\u0004\b#\u0010\u0014R\"\u0010$\u001a\u0004\u0018\u00010%8\u0006@\u0006X\u000e¢\u0006\u0010\n\u0002\u0010*\u001a\u0004\b&\u0010'\"\u0004\b(\u0010)R\u001e\u0010+\u001a\u00020\n8\u0006@\u0006X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b,\u0010\f\"\u0004\b-\u0010\u000eR\u001e\u0010.\u001a\u00020\n8\u0006@\u0006X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b/\u0010\f\"\u0004\b0\u0010\u000eR\"\u00101\u001a\u0004\u0018\u0001028\u0006@\u0006X\u000e¢\u0006\u0010\n\u0002\u00107\u001a\u0004\b3\u00104\"\u0004\b5\u00106R\u001a\u00108\u001a\u00020%X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b8\u00109\"\u0004\b:\u0010;R\u001e\u0010<\u001a\u00020\n8\u0006@\u0006X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b=\u0010\f\"\u0004\b>\u0010\u000eR\"\u0010?\u001a\u0004\u0018\u0001028\u0006@\u0006X\u000e¢\u0006\u0010\n\u0002\u00107\u001a\u0004\b@\u00104\"\u0004\bA\u00106R\u001e\u0010B\u001a\u00020\n8\u0006@\u0006X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\bC\u0010\f\"\u0004\bD\u0010\u000eR\u001e\u0010E\u001a\u00020\n8\u0006@\u0006X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\bF\u0010\f\"\u0004\bG\u0010\u000eR \u0010H\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\bI\u0010\u0006\"\u0004\bJ\u0010\bR\u001e\u0010K\u001a\u00020\n8\u0006@\u0006X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\bL\u0010\f\"\u0004\bM\u0010\u000eR&\u0010N\u001a\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u00108\u0006@\u0006X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\bO\u0010\u0012\"\u0004\bP\u0010\u0014R\u001e\u0010Q\u001a\u00020\n8\u0006@\u0006X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\bR\u0010\f\"\u0004\bS\u0010\u000eR\u001e\u0010T\u001a\u00020\n8\u0006@\u0006X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\bU\u0010\f\"\u0004\bV\u0010\u000eR&\u0010W\u001a\n\u0012\u0004\u0012\u00020\n\u0018\u00010\u00108\u0006@\u0006X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\bX\u0010\u0012\"\u0004\bY\u0010\u0014R\u001e\u0010Z\u001a\u00020\n8\u0006@\u0006X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b[\u0010\f\"\u0004\b\\\u0010\u000eR\u001e\u0010]\u001a\u00020\n8\u0006@\u0006X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b^\u0010\f\"\u0004\b_\u0010\u000e¨\u0006`"}, d2 = {"Lcom/iqonic/store/models/Method;", "Ljava/io/Serializable;", "()V", "availability", "", "getAvailability", "()Ljava/lang/Object;", "setAvailability", "(Ljava/lang/Object;)V", "cost", "", "getCost", "()Ljava/lang/String;", "setCost", "(Ljava/lang/String;)V", "countries", "", "getCountries", "()Ljava/util/List;", "setCountries", "(Ljava/util/List;)V", "data", "getData", "setData", "enabled", "getEnabled", "setEnabled", "errors", "getErrors", "setErrors", "fee", "getFee", "setFee", "formFields", "getFormFields", "setFormFields", "hasSettings", "", "getHasSettings", "()Ljava/lang/Boolean;", "setHasSettings", "(Ljava/lang/Boolean;)V", "Ljava/lang/Boolean;", "id", "getId", "setId", "ignoreDiscounts", "getIgnoreDiscounts", "setIgnoreDiscounts", "instanceId", "", "getInstanceId", "()Ljava/lang/Integer;", "setInstanceId", "(Ljava/lang/Integer;)V", "Ljava/lang/Integer;", "isSelected", "()Z", "setSelected", "(Z)V", "methodDescription", "getMethodDescription", "setMethodDescription", "methodOrder", "getMethodOrder", "setMethodOrder", "methodTitle", "getMethodTitle", "setMethodTitle", "minAmount", "getMinAmount", "setMinAmount", "minimumFee", "getMinimumFee", "setMinimumFee", "pluginId", "getPluginId", "setPluginId", "rates", "getRates", "setRates", "requires", "getRequires", "setRequires", "settingsHtml", "getSettingsHtml", "setSettingsHtml", "supports", "getSupports", "setSupports", "taxStatus", "getTaxStatus", "setTaxStatus", "title", "getTitle", "setTitle", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: ShippingModel.kt */
public final class Method implements Serializable {
    @SerializedName("availability")
    @Expose
    private Object availability;
    @SerializedName("cost")
    @Expose
    private String cost = "";
    @SerializedName("countries")
    @Expose
    private List<? extends Object> countries;
    @SerializedName("data")
    @Expose
    private List<? extends Object> data;
    @SerializedName("enabled")
    @Expose
    private String enabled = "";
    @SerializedName("errors")
    @Expose
    private List<? extends Object> errors;
    @SerializedName("fee")
    @Expose
    private Object fee;
    @SerializedName("form_fields")
    @Expose
    private List<? extends Object> formFields;
    @SerializedName("has_settings")
    @Expose
    private Boolean hasSettings;
    @SerializedName("id")
    @Expose
    private String id = "";
    @SerializedName("ignore_discounts")
    @Expose
    private String ignoreDiscounts = "";
    @SerializedName("instance_id")
    @Expose
    private Integer instanceId;
    private boolean isSelected;
    @SerializedName("method_description")
    @Expose
    private String methodDescription = "";
    @SerializedName("method_order")
    @Expose
    private Integer methodOrder;
    @SerializedName("method_title")
    @Expose
    private String methodTitle = "";
    @SerializedName("min_amount")
    @Expose
    private String minAmount = "";
    @SerializedName("minimum_fee")
    @Expose
    private Object minimumFee;
    @SerializedName("plugin_id")
    @Expose
    private String pluginId = "";
    @SerializedName("rates")
    @Expose
    private List<? extends Object> rates;
    @SerializedName("requires")
    @Expose
    private String requires = "";
    @SerializedName("settings_html")
    @Expose
    private String settingsHtml = "";
    @SerializedName("supports")
    @Expose
    private List<String> supports;
    @SerializedName("tax_status")
    @Expose
    private String taxStatus = "";
    @SerializedName("title")
    @Expose
    private String title = "";

    public final String getMinAmount() {
        return this.minAmount;
    }

    public final void setMinAmount(String str) {
        Intrinsics.checkParameterIsNotNull(str, "<set-?>");
        this.minAmount = str;
    }

    public final String getRequires() {
        return this.requires;
    }

    public final void setRequires(String str) {
        Intrinsics.checkParameterIsNotNull(str, "<set-?>");
        this.requires = str;
    }

    public final List<String> getSupports() {
        return this.supports;
    }

    public final void setSupports(List<String> list) {
        this.supports = list;
    }

    public final String getId() {
        return this.id;
    }

    public final void setId(String str) {
        Intrinsics.checkParameterIsNotNull(str, "<set-?>");
        this.id = str;
    }

    public final String getMethodTitle() {
        return this.methodTitle;
    }

    public final void setMethodTitle(String str) {
        Intrinsics.checkParameterIsNotNull(str, "<set-?>");
        this.methodTitle = str;
    }

    public final String getMethodDescription() {
        return this.methodDescription;
    }

    public final void setMethodDescription(String str) {
        Intrinsics.checkParameterIsNotNull(str, "<set-?>");
        this.methodDescription = str;
    }

    public final String getEnabled() {
        return this.enabled;
    }

    public final void setEnabled(String str) {
        Intrinsics.checkParameterIsNotNull(str, "<set-?>");
        this.enabled = str;
    }

    public final String getTitle() {
        return this.title;
    }

    public final void setTitle(String str) {
        Intrinsics.checkParameterIsNotNull(str, "<set-?>");
        this.title = str;
    }

    public final List<Object> getRates() {
        return this.rates;
    }

    public final void setRates(List<? extends Object> list) {
        this.rates = list;
    }

    public final String getTaxStatus() {
        return this.taxStatus;
    }

    public final void setTaxStatus(String str) {
        Intrinsics.checkParameterIsNotNull(str, "<set-?>");
        this.taxStatus = str;
    }

    public final Object getFee() {
        return this.fee;
    }

    public final void setFee(Object obj) {
        this.fee = obj;
    }

    public final Object getMinimumFee() {
        return this.minimumFee;
    }

    public final void setMinimumFee(Object obj) {
        this.minimumFee = obj;
    }

    public final Integer getInstanceId() {
        return this.instanceId;
    }

    public final void setInstanceId(Integer num) {
        this.instanceId = num;
    }

    public final Object getAvailability() {
        return this.availability;
    }

    public final void setAvailability(Object obj) {
        this.availability = obj;
    }

    public final List<Object> getCountries() {
        return this.countries;
    }

    public final void setCountries(List<? extends Object> list) {
        this.countries = list;
    }

    public final String getPluginId() {
        return this.pluginId;
    }

    public final void setPluginId(String str) {
        Intrinsics.checkParameterIsNotNull(str, "<set-?>");
        this.pluginId = str;
    }

    public final List<Object> getErrors() {
        return this.errors;
    }

    public final void setErrors(List<? extends Object> list) {
        this.errors = list;
    }

    public final List<Object> getFormFields() {
        return this.formFields;
    }

    public final void setFormFields(List<? extends Object> list) {
        this.formFields = list;
    }

    public final List<Object> getData() {
        return this.data;
    }

    public final void setData(List<? extends Object> list) {
        this.data = list;
    }

    public final String getIgnoreDiscounts() {
        return this.ignoreDiscounts;
    }

    public final void setIgnoreDiscounts(String str) {
        Intrinsics.checkParameterIsNotNull(str, "<set-?>");
        this.ignoreDiscounts = str;
    }

    public final Integer getMethodOrder() {
        return this.methodOrder;
    }

    public final void setMethodOrder(Integer num) {
        this.methodOrder = num;
    }

    public final Boolean getHasSettings() {
        return this.hasSettings;
    }

    public final void setHasSettings(Boolean bool) {
        this.hasSettings = bool;
    }

    public final String getSettingsHtml() {
        return this.settingsHtml;
    }

    public final void setSettingsHtml(String str) {
        Intrinsics.checkParameterIsNotNull(str, "<set-?>");
        this.settingsHtml = str;
    }

    public final String getCost() {
        return this.cost;
    }

    public final void setCost(String str) {
        Intrinsics.checkParameterIsNotNull(str, "<set-?>");
        this.cost = str;
    }

    public final boolean isSelected() {
        return this.isSelected;
    }

    public final void setSelected(boolean z) {
        this.isSelected = z;
    }
}
