package com.iqonic.store.models;

import com.facebook.internal.ServerProtocol;
import com.google.firebase.analytics.FirebaseAnalytics;
import java.io.Serializable;
import java.util.List;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000T\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\t\n\u0002\u0010\u0006\n\u0002\b\u0007\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b4\u0018\u00002\u00020\u0001Bñ\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0003\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\u0007\u0012\u0006\u0010\t\u001a\u00020\u0007\u0012\u0006\u0010\n\u001a\u00020\u0007\u0012\u0006\u0010\u000b\u001a\u00020\u0007\u0012\u0006\u0010\f\u001a\u00020\u0007\u0012\u0006\u0010\r\u001a\u00020\u0007\u0012\u0006\u0010\u000e\u001a\u00020\u0007\u0012\u0006\u0010\u000f\u001a\u00020\u0007\u0012\u0006\u0010\u0010\u001a\u00020\u0011\u0012\u0006\u0010\u0012\u001a\u00020\u0011\u0012\u0006\u0010\u0013\u001a\u00020\u0011\u0012\u0006\u0010\u0014\u001a\u00020\u0011\u0012\u0006\u0010\u0015\u001a\u00020\u0011\u0012\u0006\u0010\u0016\u001a\u00020\u0011\u0012\u0006\u0010\u0017\u001a\u00020\u0011\u0012\u0006\u0010\u0018\u001a\u00020\u0019\u0012\u0006\u0010\u001a\u001a\u00020\u0003\u0012\u0006\u0010\u001b\u001a\u00020\u0007\u0012\u0006\u0010\u001c\u001a\u00020\u0007\u0012\u0006\u0010\u001d\u001a\u00020\u0007\u0012\u0006\u0010\u001e\u001a\u00020\u001f\u0012\u0006\u0010 \u001a\u00020!\u0012\u0006\u0010\"\u001a\u00020\u0007\u0012\u0006\u0010#\u001a\u00020\u0007\u0012\u0006\u0010$\u001a\u00020\u0007\u0012\u0006\u0010%\u001a\u00020\u0007\u0012\u0006\u0010&\u001a\u00020\u0007\u0012\u0006\u0010'\u001a\u00020\u0007\u0012\u0006\u0010(\u001a\u00020\u0007\u0012\u0006\u0010)\u001a\u00020\u0007\u0012\f\u0010*\u001a\b\u0012\u0004\u0012\u00020,0+\u0012\f\u0010-\u001a\b\u0012\u0004\u0012\u00020\u00070+\u0012\f\u0010.\u001a\b\u0012\u0004\u0012\u00020/0+\u0012\f\u00100\u001a\b\u0012\u0004\u0012\u00020\u00070+\u0012\f\u00101\u001a\b\u0012\u0004\u0012\u0002020+\u0012\f\u00103\u001a\b\u0012\u0004\u0012\u00020\u00070+\u0012\u0006\u00104\u001a\u00020\u0007¢\u0006\u0002\u00105R\u0011\u0010\u001e\u001a\u00020\u001f¢\u0006\b\n\u0000\u001a\u0004\b6\u00107R\u0011\u0010)\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\b8\u00109R\u0011\u0010\u0015\u001a\u00020\u0011¢\u0006\b\n\u0000\u001a\u0004\b:\u0010;R\u0017\u00101\u001a\b\u0012\u0004\u0012\u0002020+¢\u0006\b\n\u0000\u001a\u0004\b<\u0010=R\u0011\u0010\b\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\b>\u00109R\u0011\u0010\u000b\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\b?\u00109R\u0011\u00104\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\b@\u00109R\u0011\u0010\u001a\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\bA\u0010BR\u0011\u0010\u001b\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\bC\u00109R\u0011\u0010\u001d\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\bD\u00109R\u0011\u0010\u001c\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\bE\u00109R\u0011\u0010'\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\bF\u00109R\u0011\u0010(\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\bG\u00109R\u0011\u0010\f\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\bH\u00109R\u0011\u0010\r\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\bI\u00109R\u0011\u0010\u000e\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\bJ\u00109R\u0011\u0010\u000f\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\bK\u00109R\u0011\u0010%\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\bL\u00109R\u0011\u0010&\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\bM\u00109R\u0011\u0010\u0012\u001a\u00020\u0011¢\u0006\b\n\u0000\u001a\u0004\bN\u0010;R\u0011\u0010\u0010\u001a\u00020\u0011¢\u0006\b\n\u0000\u001a\u0004\bO\u0010;R\u0017\u00100\u001a\b\u0012\u0004\u0012\u00020\u00070+¢\u0006\b\n\u0000\u001a\u0004\bP\u0010=R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\bQ\u0010BR\u0017\u0010*\u001a\b\u0012\u0004\u0012\u00020,0+¢\u0006\b\n\u0000\u001a\u0004\bR\u0010=R\u0011\u0010\u0005\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\bS\u0010BR\u0011\u0010\u0006\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\bT\u00109R\u0011\u0010\u0004\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\bU\u0010BR\u0011\u0010\"\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\bV\u00109R\u0011\u0010#\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\bW\u00109R\u0011\u0010\u0018\u001a\u00020\u0019¢\u0006\b\n\u0000\u001a\u0004\bX\u0010YR\u0017\u00103\u001a\b\u0012\u0004\u0012\u00020\u00070+¢\u0006\b\n\u0000\u001a\u0004\bZ\u0010=R\u0011\u0010 \u001a\u00020!¢\u0006\b\n\u0000\u001a\u0004\b[\u0010\\R\u0017\u0010.\u001a\b\u0012\u0004\u0012\u00020/0+¢\u0006\b\n\u0000\u001a\u0004\b]\u0010=R\u0011\u0010\u0014\u001a\u00020\u0011¢\u0006\b\n\u0000\u001a\u0004\b^\u0010;R\u0011\u0010\u0013\u001a\u00020\u0011¢\u0006\b\n\u0000\u001a\u0004\b_\u0010;R\u0011\u0010\n\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\b`\u00109R\u0017\u0010-\u001a\b\u0012\u0004\u0012\u00020\u00070+¢\u0006\b\n\u0000\u001a\u0004\ba\u0010=R\u0011\u0010\u0016\u001a\u00020\u0011¢\u0006\b\n\u0000\u001a\u0004\bb\u0010;R\u0011\u0010\u0017\u001a\u00020\u0011¢\u0006\b\n\u0000\u001a\u0004\bc\u0010;R\u0011\u0010$\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\bd\u00109R\u0011\u0010\t\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\be\u00109¨\u0006f"}, d2 = {"Lcom/iqonic/store/models/CreateOrderResponse;", "Ljava/io/Serializable;", "id", "", "parent_id", "number", "order_key", "", "created_via", "version", "status", "currency", "date_created", "date_created_gmt", "date_modified", "date_modified_gmt", "discount_total", "", "discount_tax", "shipping_total", "shipping_tax", "cart_tax", "total", "total_tax", "prices_include_tax", "", "customer_id", "customer_ip_address", "customer_user_agent", "customer_note", "billing", "Lcom/iqonic/store/models/Billing;", "shipping", "Lcom/iqonic/store/models/Shipping;", "payment_method", "payment_method_title", "transaction_id", "date_paid", "date_paid_gmt", "date_completed", "date_completed_gmt", "cart_hash", "line_items", "", "Lcom/iqonic/store/models/Line_items;", "tax_lines", "shipping_lines", "Lcom/iqonic/store/models/ShippingLines;", "fee_lines", "coupon_lines", "Lcom/iqonic/store/models/CouponLines;", "refunds", "currency_symbol", "(IIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;DDDDDDDZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/iqonic/store/models/Billing;Lcom/iqonic/store/models/Shipping;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/lang/String;)V", "getBilling", "()Lcom/iqonic/store/models/Billing;", "getCart_hash", "()Ljava/lang/String;", "getCart_tax", "()D", "getCoupon_lines", "()Ljava/util/List;", "getCreated_via", "getCurrency", "getCurrency_symbol", "getCustomer_id", "()I", "getCustomer_ip_address", "getCustomer_note", "getCustomer_user_agent", "getDate_completed", "getDate_completed_gmt", "getDate_created", "getDate_created_gmt", "getDate_modified", "getDate_modified_gmt", "getDate_paid", "getDate_paid_gmt", "getDiscount_tax", "getDiscount_total", "getFee_lines", "getId", "getLine_items", "getNumber", "getOrder_key", "getParent_id", "getPayment_method", "getPayment_method_title", "getPrices_include_tax", "()Z", "getRefunds", "getShipping", "()Lcom/iqonic/store/models/Shipping;", "getShipping_lines", "getShipping_tax", "getShipping_total", "getStatus", "getTax_lines", "getTotal", "getTotal_tax", "getTransaction_id", "getVersion", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: BaseResponse.kt */
public final class CreateOrderResponse implements Serializable {
    private final Billing billing;
    private final String cart_hash;
    private final double cart_tax;
    private final List<CouponLines> coupon_lines;
    private final String created_via;
    private final String currency;
    private final String currency_symbol;
    private final int customer_id;
    private final String customer_ip_address;
    private final String customer_note;
    private final String customer_user_agent;
    private final String date_completed;
    private final String date_completed_gmt;
    private final String date_created;
    private final String date_created_gmt;
    private final String date_modified;
    private final String date_modified_gmt;
    private final String date_paid;
    private final String date_paid_gmt;
    private final double discount_tax;
    private final double discount_total;
    private final List<String> fee_lines;
    private final int id;
    private final List<Line_items> line_items;
    private final int number;
    private final String order_key;
    private final int parent_id;
    private final String payment_method;
    private final String payment_method_title;
    private final boolean prices_include_tax;
    private final List<String> refunds;
    private final Shipping shipping;
    private final List<ShippingLines> shipping_lines;
    private final double shipping_tax;
    private final double shipping_total;
    private final String status;
    private final List<String> tax_lines;
    private final double total;
    private final double total_tax;
    private final String transaction_id;
    private final String version;

    public CreateOrderResponse(int i, int i2, int i3, String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, String str9, double d, double d2, double d3, double d4, double d5, double d6, double d7, boolean z, int i4, String str10, String str11, String str12, Billing billing2, Shipping shipping2, String str13, String str14, String str15, String str16, String str17, String str18, String str19, String str20, List<Line_items> list, List<String> list2, List<ShippingLines> list3, List<String> list4, List<CouponLines> list5, List<String> list6, String str21) {
        String str22 = str;
        String str23 = str2;
        String str24 = str3;
        String str25 = str4;
        String str26 = str5;
        String str27 = str6;
        String str28 = str7;
        String str29 = str8;
        String str30 = str9;
        String str31 = str10;
        String str32 = str11;
        String str33 = str12;
        Billing billing3 = billing2;
        Shipping shipping3 = shipping2;
        String str34 = str14;
        Intrinsics.checkParameterIsNotNull(str22, "order_key");
        Intrinsics.checkParameterIsNotNull(str23, "created_via");
        Intrinsics.checkParameterIsNotNull(str24, ServerProtocol.FALLBACK_DIALOG_PARAM_VERSION);
        Intrinsics.checkParameterIsNotNull(str25, "status");
        Intrinsics.checkParameterIsNotNull(str26, FirebaseAnalytics.Param.CURRENCY);
        Intrinsics.checkParameterIsNotNull(str27, "date_created");
        Intrinsics.checkParameterIsNotNull(str28, "date_created_gmt");
        Intrinsics.checkParameterIsNotNull(str29, "date_modified");
        Intrinsics.checkParameterIsNotNull(str30, "date_modified_gmt");
        Intrinsics.checkParameterIsNotNull(str31, "customer_ip_address");
        Intrinsics.checkParameterIsNotNull(str32, "customer_user_agent");
        Intrinsics.checkParameterIsNotNull(str33, "customer_note");
        Intrinsics.checkParameterIsNotNull(billing3, "billing");
        Intrinsics.checkParameterIsNotNull(shipping3, FirebaseAnalytics.Param.SHIPPING);
        Intrinsics.checkParameterIsNotNull(str13, "payment_method");
        Intrinsics.checkParameterIsNotNull(str14, "payment_method_title");
        Intrinsics.checkParameterIsNotNull(str15, FirebaseAnalytics.Param.TRANSACTION_ID);
        Intrinsics.checkParameterIsNotNull(str16, "date_paid");
        Intrinsics.checkParameterIsNotNull(str17, "date_paid_gmt");
        Intrinsics.checkParameterIsNotNull(str18, "date_completed");
        Intrinsics.checkParameterIsNotNull(str19, "date_completed_gmt");
        Intrinsics.checkParameterIsNotNull(str20, "cart_hash");
        Intrinsics.checkParameterIsNotNull(list, "line_items");
        Intrinsics.checkParameterIsNotNull(list2, "tax_lines");
        Intrinsics.checkParameterIsNotNull(list3, "shipping_lines");
        Intrinsics.checkParameterIsNotNull(list4, "fee_lines");
        Intrinsics.checkParameterIsNotNull(list5, "coupon_lines");
        Intrinsics.checkParameterIsNotNull(list6, "refunds");
        Intrinsics.checkParameterIsNotNull(str21, "currency_symbol");
        this.id = i;
        this.parent_id = i2;
        this.number = i3;
        this.order_key = str22;
        this.created_via = str23;
        this.version = str24;
        this.status = str25;
        this.currency = str26;
        this.date_created = str27;
        this.date_created_gmt = str28;
        this.date_modified = str29;
        this.date_modified_gmt = str30;
        this.discount_total = d;
        this.discount_tax = d2;
        this.shipping_total = d3;
        this.shipping_tax = d4;
        this.cart_tax = d5;
        this.total = d6;
        this.total_tax = d7;
        this.prices_include_tax = z;
        this.customer_id = i4;
        this.customer_ip_address = str31;
        this.customer_user_agent = str32;
        this.customer_note = str33;
        this.billing = billing3;
        this.shipping = shipping3;
        this.payment_method = str13;
        this.payment_method_title = str14;
        this.transaction_id = str15;
        this.date_paid = str16;
        this.date_paid_gmt = str17;
        this.date_completed = str18;
        this.date_completed_gmt = str19;
        this.cart_hash = str20;
        this.line_items = list;
        this.tax_lines = list2;
        this.shipping_lines = list3;
        this.fee_lines = list4;
        this.coupon_lines = list5;
        this.refunds = list6;
        this.currency_symbol = str21;
    }

    public final int getId() {
        return this.id;
    }

    public final int getParent_id() {
        return this.parent_id;
    }

    public final int getNumber() {
        return this.number;
    }

    public final String getOrder_key() {
        return this.order_key;
    }

    public final String getCreated_via() {
        return this.created_via;
    }

    public final String getVersion() {
        return this.version;
    }

    public final String getStatus() {
        return this.status;
    }

    public final String getCurrency() {
        return this.currency;
    }

    public final String getDate_created() {
        return this.date_created;
    }

    public final String getDate_created_gmt() {
        return this.date_created_gmt;
    }

    public final String getDate_modified() {
        return this.date_modified;
    }

    public final String getDate_modified_gmt() {
        return this.date_modified_gmt;
    }

    public final double getDiscount_total() {
        return this.discount_total;
    }

    public final double getDiscount_tax() {
        return this.discount_tax;
    }

    public final double getShipping_total() {
        return this.shipping_total;
    }

    public final double getShipping_tax() {
        return this.shipping_tax;
    }

    public final double getCart_tax() {
        return this.cart_tax;
    }

    public final double getTotal() {
        return this.total;
    }

    public final double getTotal_tax() {
        return this.total_tax;
    }

    public final boolean getPrices_include_tax() {
        return this.prices_include_tax;
    }

    public final int getCustomer_id() {
        return this.customer_id;
    }

    public final String getCustomer_ip_address() {
        return this.customer_ip_address;
    }

    public final String getCustomer_user_agent() {
        return this.customer_user_agent;
    }

    public final String getCustomer_note() {
        return this.customer_note;
    }

    public final Billing getBilling() {
        return this.billing;
    }

    public final Shipping getShipping() {
        return this.shipping;
    }

    public final String getPayment_method() {
        return this.payment_method;
    }

    public final String getPayment_method_title() {
        return this.payment_method_title;
    }

    public final String getTransaction_id() {
        return this.transaction_id;
    }

    public final String getDate_paid() {
        return this.date_paid;
    }

    public final String getDate_paid_gmt() {
        return this.date_paid_gmt;
    }

    public final String getDate_completed() {
        return this.date_completed;
    }

    public final String getDate_completed_gmt() {
        return this.date_completed_gmt;
    }

    public final String getCart_hash() {
        return this.cart_hash;
    }

    public final List<Line_items> getLine_items() {
        return this.line_items;
    }

    public final List<String> getTax_lines() {
        return this.tax_lines;
    }

    public final List<ShippingLines> getShipping_lines() {
        return this.shipping_lines;
    }

    public final List<String> getFee_lines() {
        return this.fee_lines;
    }

    public final List<CouponLines> getCoupon_lines() {
        return this.coupon_lines;
    }

    public final List<String> getRefunds() {
        return this.refunds;
    }

    public final String getCurrency_symbol() {
        return this.currency_symbol;
    }
}
