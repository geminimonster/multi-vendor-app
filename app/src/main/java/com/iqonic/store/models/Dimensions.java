package com.iqonic.store.models;

import java.io.Serializable;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\f\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\b\b\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0003¢\u0006\u0002\u0010\u0006J\t\u0010\u000b\u001a\u00020\u0003HÆ\u0003J\t\u0010\f\u001a\u00020\u0003HÆ\u0003J\t\u0010\r\u001a\u00020\u0003HÆ\u0003J'\u0010\u000e\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00032\b\b\u0002\u0010\u0005\u001a\u00020\u0003HÆ\u0001J\u0013\u0010\u000f\u001a\u00020\u00102\b\u0010\u0011\u001a\u0004\u0018\u00010\u0012HÖ\u0003J\t\u0010\u0013\u001a\u00020\u0014HÖ\u0001J\t\u0010\u0015\u001a\u00020\u0003HÖ\u0001R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\u0004\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\bR\u0011\u0010\u0005\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\b¨\u0006\u0016"}, d2 = {"Lcom/iqonic/store/models/Dimensions;", "Ljava/io/Serializable;", "height", "", "length", "width", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", "getHeight", "()Ljava/lang/String;", "getLength", "getWidth", "component1", "component2", "component3", "copy", "equals", "", "other", "", "hashCode", "", "toString", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: ProductModel.kt */
public final class Dimensions implements Serializable {
    private final String height;
    private final String length;
    private final String width;

    public static /* synthetic */ Dimensions copy$default(Dimensions dimensions, String str, String str2, String str3, int i, Object obj) {
        if ((i & 1) != 0) {
            str = dimensions.height;
        }
        if ((i & 2) != 0) {
            str2 = dimensions.length;
        }
        if ((i & 4) != 0) {
            str3 = dimensions.width;
        }
        return dimensions.copy(str, str2, str3);
    }

    public final String component1() {
        return this.height;
    }

    public final String component2() {
        return this.length;
    }

    public final String component3() {
        return this.width;
    }

    public final Dimensions copy(String str, String str2, String str3) {
        Intrinsics.checkParameterIsNotNull(str, "height");
        Intrinsics.checkParameterIsNotNull(str2, "length");
        Intrinsics.checkParameterIsNotNull(str3, "width");
        return new Dimensions(str, str2, str3);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Dimensions)) {
            return false;
        }
        Dimensions dimensions = (Dimensions) obj;
        return Intrinsics.areEqual((Object) this.height, (Object) dimensions.height) && Intrinsics.areEqual((Object) this.length, (Object) dimensions.length) && Intrinsics.areEqual((Object) this.width, (Object) dimensions.width);
    }

    public int hashCode() {
        String str = this.height;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.length;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.width;
        if (str3 != null) {
            i = str3.hashCode();
        }
        return hashCode2 + i;
    }

    public String toString() {
        return "Dimensions(height=" + this.height + ", length=" + this.length + ", width=" + this.width + ")";
    }

    public Dimensions(String str, String str2, String str3) {
        Intrinsics.checkParameterIsNotNull(str, "height");
        Intrinsics.checkParameterIsNotNull(str2, "length");
        Intrinsics.checkParameterIsNotNull(str3, "width");
        this.height = str;
        this.length = str2;
        this.width = str3;
    }

    public final String getHeight() {
        return this.height;
    }

    public final String getLength() {
        return this.length;
    }

    public final String getWidth() {
        return this.width;
    }
}
