package com.iqonic.store.models;

import com.google.gson.annotations.SerializedName;
import kotlin.Metadata;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u001d\b\u0016\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002R \u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\bR \u0010\t\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\n\u0010\u0006\"\u0004\b\u000b\u0010\bR \u0010\f\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\r\u0010\u0006\"\u0004\b\u000e\u0010\bR \u0010\u000f\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0010\u0010\u0006\"\u0004\b\u0011\u0010\bR \u0010\u0012\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0013\u0010\u0006\"\u0004\b\u0014\u0010\bR \u0010\u0015\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0016\u0010\u0006\"\u0004\b\u0017\u0010\bR \u0010\u0018\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0019\u0010\u0006\"\u0004\b\u001a\u0010\bR \u0010\u001b\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u001c\u0010\u0006\"\u0004\b\u001d\u0010\bR \u0010\u001e\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u001f\u0010\u0006\"\u0004\b \u0010\b¨\u0006!"}, d2 = {"Lcom/iqonic/store/models/AppSetup;", "", "()V", "appName", "", "getAppName", "()Ljava/lang/String;", "setAppName", "(Ljava/lang/String;)V", "appUrl", "getAppUrl", "setAppUrl", "backgroundColor", "getBackgroundColor", "setBackgroundColor", "consumerKey", "getConsumerKey", "setConsumerKey", "consumerSecret", "getConsumerSecret", "setConsumerSecret", "primaryColor", "getPrimaryColor", "setPrimaryColor", "secondaryColor", "getSecondaryColor", "setSecondaryColor", "textPrimaryColor", "getTextPrimaryColor", "setTextPrimaryColor", "textSecondaryColor", "getTextSecondaryColor", "setTextSecondaryColor", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: DashBoardResponse.kt */
public class AppSetup {
    @SerializedName("appName")
    private String appName;
    @SerializedName("appUrl")
    private String appUrl;
    @SerializedName("backgroundColor")
    private String backgroundColor;
    @SerializedName("consumerKey")
    private String consumerKey;
    @SerializedName("consumerSecret")
    private String consumerSecret;
    @SerializedName("primaryColor")
    private String primaryColor;
    @SerializedName("secondaryColor")
    private String secondaryColor;
    @SerializedName("textPrimaryColor")
    private String textPrimaryColor;
    @SerializedName("textSecondaryColor")
    private String textSecondaryColor;

    public final String getAppName() {
        return this.appName;
    }

    public final void setAppName(String str) {
        this.appName = str;
    }

    public final String getPrimaryColor() {
        return this.primaryColor;
    }

    public final void setPrimaryColor(String str) {
        this.primaryColor = str;
    }

    public final String getSecondaryColor() {
        return this.secondaryColor;
    }

    public final void setSecondaryColor(String str) {
        this.secondaryColor = str;
    }

    public final String getTextPrimaryColor() {
        return this.textPrimaryColor;
    }

    public final void setTextPrimaryColor(String str) {
        this.textPrimaryColor = str;
    }

    public final String getTextSecondaryColor() {
        return this.textSecondaryColor;
    }

    public final void setTextSecondaryColor(String str) {
        this.textSecondaryColor = str;
    }

    public final String getBackgroundColor() {
        return this.backgroundColor;
    }

    public final void setBackgroundColor(String str) {
        this.backgroundColor = str;
    }

    public final String getConsumerKey() {
        return this.consumerKey;
    }

    public final void setConsumerKey(String str) {
        this.consumerKey = str;
    }

    public final String getConsumerSecret() {
        return this.consumerSecret;
    }

    public final void setConsumerSecret(String str) {
        this.consumerSecret = str;
    }

    public final String getAppUrl() {
        return this.appUrl;
    }

    public final void setAppUrl(String str) {
        this.appUrl = str;
    }
}
