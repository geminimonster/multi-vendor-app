<?xml version="1.0" encoding="utf-8"?>
<RelativeLayout android:orientation="vertical"
android:id="@id/rlMain"
android:background="@color/colorScreenBackground"
android:layout_width="fill_parent"
android:layout_height="fill_parent"
xmlns:android="http://schemas.android.com/apk/res/android">

<include
android:id="@id/toolbars"
layout="@layout/toolbar" />

<androidx.core.widget.NestedScrollView
android:layout_width="fill_parent"
android:layout_height="wrap_content"
android:layout_below="@id/toolbars"
android:overScrollMode="never">

<LinearLayout
android:orientation="vertical"
android:layout_width="fill_parent"
android:layout_height="wrap_content">

<LinearLayout
android:orientation="vertical"
android:background="@drawable/bg_order_shadow"
android:padding="@dimen/spacing_standard_new"
android:layout_width="fill_parent"
android:layout_height="wrap_content">

<TextView
android:textColor="@color/textColorSecondary"
android:id="@id/tvOrderId"
