package com.iqonic.store.models;

import com.facebook.internal.ServerProtocol;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u001f\n\u0002\u0010\u000b\n\u0002\b\u0004\b\b\u0018\u00002\u00020\u0001BY\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\u0007\u0012\u0006\u0010\t\u001a\u00020\u0005\u0012\n\b\u0002\u0010\n\u001a\u0004\u0018\u00010\u000b\u0012\u0006\u0010\f\u001a\u00020\u0005\u0012\u0006\u0010\r\u001a\u00020\u0007\u0012\u0006\u0010\u000e\u001a\u00020\u0005\u0012\u0006\u0010\u000f\u001a\u00020\u0007¢\u0006\u0002\u0010\u0010J\t\u0010\u001f\u001a\u00020\u0003HÆ\u0003J\t\u0010 \u001a\u00020\u0007HÆ\u0003J\t\u0010!\u001a\u00020\u0005HÆ\u0003J\t\u0010\"\u001a\u00020\u0007HÆ\u0003J\t\u0010#\u001a\u00020\u0007HÆ\u0003J\t\u0010$\u001a\u00020\u0005HÆ\u0003J\u000b\u0010%\u001a\u0004\u0018\u00010\u000bHÆ\u0003J\t\u0010&\u001a\u00020\u0005HÆ\u0003J\t\u0010'\u001a\u00020\u0007HÆ\u0003J\t\u0010(\u001a\u00020\u0005HÆ\u0003Jo\u0010)\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u00072\b\b\u0002\u0010\b\u001a\u00020\u00072\b\b\u0002\u0010\t\u001a\u00020\u00052\n\b\u0002\u0010\n\u001a\u0004\u0018\u00010\u000b2\b\b\u0002\u0010\f\u001a\u00020\u00052\b\b\u0002\u0010\r\u001a\u00020\u00072\b\b\u0002\u0010\u000e\u001a\u00020\u00052\b\b\u0002\u0010\u000f\u001a\u00020\u0007HÆ\u0001J\u0013\u0010*\u001a\u00020+2\b\u0010,\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010-\u001a\u00020\u0005HÖ\u0001J\t\u0010.\u001a\u00020\u0007HÖ\u0001R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u0012R\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u0014R\u0011\u0010\u0006\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\b\u0015\u0010\u0016R\u0011\u0010\b\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\b\u0017\u0010\u0016R\u0011\u0010\t\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0018\u0010\u0014R\u0013\u0010\n\u001a\u0004\u0018\u00010\u000b¢\u0006\b\n\u0000\u001a\u0004\b\u0019\u0010\u001aR\u0011\u0010\f\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u001b\u0010\u0014R\u0011\u0010\r\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\b\u001c\u0010\u0016R\u0011\u0010\u000e\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u001d\u0010\u0014R\u0011\u0010\u000f\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\b\u001e\u0010\u0016¨\u0006/"}, d2 = {"Lcom/iqonic/store/models/Category;", "", "_links", "Lcom/iqonic/store/models/CategoryLinks;", "count", "", "description", "", "display", "id", "image", "Lcom/iqonic/store/models/CategoryImage;", "menu_order", "name", "parent", "slug", "(Lcom/iqonic/store/models/CategoryLinks;ILjava/lang/String;Ljava/lang/String;ILcom/iqonic/store/models/CategoryImage;ILjava/lang/String;ILjava/lang/String;)V", "get_links", "()Lcom/iqonic/store/models/CategoryLinks;", "getCount", "()I", "getDescription", "()Ljava/lang/String;", "getDisplay", "getId", "getImage", "()Lcom/iqonic/store/models/CategoryImage;", "getMenu_order", "getName", "getParent", "getSlug", "component1", "component10", "component2", "component3", "component4", "component5", "component6", "component7", "component8", "component9", "copy", "equals", "", "other", "hashCode", "toString", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: StoreCategory.kt */
public final class Category {
    private final CategoryLinks _links;
    private final int count;
    private final String description;
    private final String display;
    private final int id;
    private final CategoryImage image;
    private final int menu_order;
    private final String name;
    private final int parent;
    private final String slug;

    public static /* synthetic */ Category copy$default(Category category, CategoryLinks categoryLinks, int i, String str, String str2, int i2, CategoryImage categoryImage, int i3, String str3, int i4, String str4, int i5, Object obj) {
        Category category2 = category;
        int i6 = i5;
        return category.copy((i6 & 1) != 0 ? category2._links : categoryLinks, (i6 & 2) != 0 ? category2.count : i, (i6 & 4) != 0 ? category2.description : str, (i6 & 8) != 0 ? category2.display : str2, (i6 & 16) != 0 ? category2.id : i2, (i6 & 32) != 0 ? category2.image : categoryImage, (i6 & 64) != 0 ? category2.menu_order : i3, (i6 & 128) != 0 ? category2.name : str3, (i6 & 256) != 0 ? category2.parent : i4, (i6 & 512) != 0 ? category2.slug : str4);
    }

    public final CategoryLinks component1() {
        return this._links;
    }

    public final String component10() {
        return this.slug;
    }

    public final int component2() {
        return this.count;
    }

    public final String component3() {
        return this.description;
    }

    public final String component4() {
        return this.display;
    }

    public final int component5() {
        return this.id;
    }

    public final CategoryImage component6() {
        return this.image;
    }

    public final int component7() {
        return this.menu_order;
    }

    public final String component8() {
        return this.name;
    }

    public final int component9() {
        return this.parent;
    }

    public final Category copy(CategoryLinks categoryLinks, int i, String str, String str2, int i2, CategoryImage categoryImage, int i3, String str3, int i4, String str4) {
        Intrinsics.checkParameterIsNotNull(categoryLinks, "_links");
        Intrinsics.checkParameterIsNotNull(str, "description");
        String str5 = str2;
        Intrinsics.checkParameterIsNotNull(str5, ServerProtocol.DIALOG_PARAM_DISPLAY);
        String str6 = str3;
        Intrinsics.checkParameterIsNotNull(str6, "name");
        String str7 = str4;
        Intrinsics.checkParameterIsNotNull(str7, "slug");
        return new Category(categoryLinks, i, str, str5, i2, categoryImage, i3, str6, i4, str7);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Category)) {
            return false;
        }
        Category category = (Category) obj;
        return Intrinsics.areEqual((Object) this._links, (Object) category._links) && this.count == category.count && Intrinsics.areEqual((Object) this.description, (Object) category.description) && Intrinsics.areEqual((Object) this.display, (Object) category.display) && this.id == category.id && Intrinsics.areEqual((Object) this.image, (Object) category.image) && this.menu_order == category.menu_order && Intrinsics.areEqual((Object) this.name, (Object) category.name) && this.parent == category.parent && Intrinsics.areEqual((Object) this.slug, (Object) category.slug);
    }

    public int hashCode() {
        CategoryLinks categoryLinks = this._links;
        int i = 0;
        int hashCode = (((categoryLinks != null ? categoryLinks.hashCode() : 0) * 31) + this.count) * 31;
        String str = this.description;
        int hashCode2 = (hashCode + (str != null ? str.hashCode() : 0)) * 31;
        String str2 = this.display;
        int hashCode3 = (((hashCode2 + (str2 != null ? str2.hashCode() : 0)) * 31) + this.id) * 31;
        CategoryImage categoryImage = this.image;
        int hashCode4 = (((hashCode3 + (categoryImage != null ? categoryImage.hashCode() : 0)) * 31) + this.menu_order) * 31;
        String str3 = this.name;
        int hashCode5 = (((hashCode4 + (str3 != null ? str3.hashCode() : 0)) * 31) + this.parent) * 31;
        String str4 = this.slug;
        if (str4 != null) {
            i = str4.hashCode();
        }
        return hashCode5 + i;
    }

    public String toString() {
        return "Category(_links=" + this._links + ", count=" + this.count + ", description=" + this.description + ", display=" + this.display + ", id=" + this.id + ", image=" + this.image + ", menu_order=" + this.menu_order + ", name=" + this.name + ", parent=" + this.parent + ", slug=" + this.slug + ")";
    }

    public Category(CategoryLinks categoryLinks, int i, String str, String str2, int i2, CategoryImage categoryImage, int i3, String str3, int i4, String str4) {
        Intrinsics.checkParameterIsNotNull(categoryLinks, "_links");
        Intrinsics.checkParameterIsNotNull(str, "description");
        Intrinsics.checkParameterIsNotNull(str2, ServerProtocol.DIALOG_PARAM_DISPLAY);
        Intrinsics.checkParameterIsNotNull(str3, "name");
        Intrinsics.checkParameterIsNotNull(str4, "slug");
        this._links = categoryLinks;
        this.count = i;
        this.description = str;
        this.display = str2;
        this.id = i2;
        this.image = categoryImage;
        this.menu_order = i3;
        this.name = str3;
        this.parent = i4;
        this.slug = str4;
    }

    public final CategoryLinks get_links() {
        return this._links;
    }

    public final int getCount() {
        return this.count;
    }

    public final String getDescription() {
        return this.description;
    }

    public final String getDisplay() {
        return this.display;
    }

    public final int getId() {
        return this.id;
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ Category(CategoryLinks categoryLinks, int i, String str, String str2, int i2, CategoryImage categoryImage, int i3, String str3, int i4, String str4, int i5, DefaultConstructorMarker defaultConstructorMarker) {
        this(categoryLinks, i, str, str2, i2, (i5 & 32) != 0 ? null : categoryImage, i3, str3, i4, str4);
    }

    public final CategoryImage getImage() {
        return this.image;
    }

    public final int getMenu_order() {
        return this.menu_order;
    }

    public final String getName() {
        return this.name;
    }

    public final int getParent() {
        return this.parent;
    }

    public final String getSlug() {
        return this.slug;
    }
}
