package com.iqonic.store.models;

import com.facebook.internal.ServerProtocol;
import com.google.firebase.analytics.FirebaseAnalytics;
import java.io.Serializable;
import java.util.List;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.bouncycastle.asn1.cmp.PKIFailureInfo;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000X\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010 \n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b]\b\b\u0018\u00002\u00020\u0001B­\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0005\u0012\u0006\u0010\u0007\u001a\u00020\u0005\u0012\u0006\u0010\b\u001a\u00020\u0005\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u0006\u0010\u000b\u001a\u00020\u0005\u0012\u0006\u0010\f\u001a\u00020\u0005\u0012\u0006\u0010\r\u001a\u00020\u0005\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u0012\u0006\u0010\u0010\u001a\u00020\u0011\u0012\u0006\u0010\u0012\u001a\u00020\u0013\u0012\u0006\u0010\u0014\u001a\u00020\u0015\u0012\u0006\u0010\u0016\u001a\u00020\u0005\u0012\u0006\u0010\u0017\u001a\u00020\u0005\u0012\f\u0010\u0018\u001a\b\u0012\u0004\u0012\u00020\u000f0\u0019\u0012\u0006\u0010\u001a\u001a\u00020\n\u0012\f\u0010\u001b\u001a\b\u0012\u0004\u0012\u00020\u001c0\u0019\u0012\f\u0010\u001d\u001a\b\u0012\u0004\u0012\u00020\u000f0\u0019\u0012\u0006\u0010\u001e\u001a\u00020\u0005\u0012\u0006\u0010\u001f\u001a\u00020\u0005\u0012\u0006\u0010 \u001a\u00020\n\u0012\u0006\u0010!\u001a\u00020\u0005\u0012\u0006\u0010\"\u001a\u00020\u0005\u0012\u0006\u0010#\u001a\u00020$\u0012\u0006\u0010%\u001a\u00020&\u0012\u0006\u0010'\u001a\u00020\u0005\u0012\u0006\u0010(\u001a\u00020\u0005\u0012\u0006\u0010)\u001a\u00020\u0005\u0012\f\u0010*\u001a\b\u0012\u0004\u0012\u00020\u000f0\u0019\u0012\u0006\u0010+\u001a\u00020\u0005\u0012\u0006\u0010,\u001a\u00020\u0005\u0012\u0006\u0010-\u001a\u00020\u0005\u0012\u0006\u0010.\u001a\u00020\u0005¢\u0006\u0002\u0010/J\t\u0010\\\u001a\u00020\u0003HÆ\u0003J\t\u0010]\u001a\u00020\u000fHÆ\u0003J\t\u0010^\u001a\u00020\u0011HÆ\u0003J\t\u0010_\u001a\u00020\u0013HÆ\u0003J\t\u0010`\u001a\u00020\u0015HÆ\u0003J\t\u0010a\u001a\u00020\u0005HÆ\u0003J\t\u0010b\u001a\u00020\u0005HÆ\u0003J\u000f\u0010c\u001a\b\u0012\u0004\u0012\u00020\u000f0\u0019HÆ\u0003J\t\u0010d\u001a\u00020\nHÆ\u0003J\u000f\u0010e\u001a\b\u0012\u0004\u0012\u00020\u001c0\u0019HÆ\u0003J\u000f\u0010f\u001a\b\u0012\u0004\u0012\u00020\u000f0\u0019HÆ\u0003J\t\u0010g\u001a\u00020\u0005HÆ\u0003J\t\u0010h\u001a\u00020\u0005HÆ\u0003J\t\u0010i\u001a\u00020\u0005HÆ\u0003J\t\u0010j\u001a\u00020\nHÆ\u0003J\t\u0010k\u001a\u00020\u0005HÆ\u0003J\t\u0010l\u001a\u00020\u0005HÆ\u0003J\t\u0010m\u001a\u00020$HÆ\u0003J\t\u0010n\u001a\u00020&HÆ\u0003J\t\u0010o\u001a\u00020\u0005HÆ\u0003J\t\u0010p\u001a\u00020\u0005HÆ\u0003J\t\u0010q\u001a\u00020\u0005HÆ\u0003J\t\u0010r\u001a\u00020\u0005HÆ\u0003J\u000f\u0010s\u001a\b\u0012\u0004\u0012\u00020\u000f0\u0019HÆ\u0003J\t\u0010t\u001a\u00020\u0005HÆ\u0003J\t\u0010u\u001a\u00020\u0005HÆ\u0003J\t\u0010v\u001a\u00020\u0005HÆ\u0003J\t\u0010w\u001a\u00020\u0005HÆ\u0003J\t\u0010x\u001a\u00020\u0005HÆ\u0003J\t\u0010y\u001a\u00020\u0005HÆ\u0003J\t\u0010z\u001a\u00020\nHÆ\u0003J\t\u0010{\u001a\u00020\u0005HÆ\u0003J\t\u0010|\u001a\u00020\u0005HÆ\u0003J\t\u0010}\u001a\u00020\u0005HÆ\u0003Jõ\u0002\u0010~\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u00052\b\b\u0002\u0010\u0007\u001a\u00020\u00052\b\b\u0002\u0010\b\u001a\u00020\u00052\b\b\u0002\u0010\t\u001a\u00020\n2\b\b\u0002\u0010\u000b\u001a\u00020\u00052\b\b\u0002\u0010\f\u001a\u00020\u00052\b\b\u0002\u0010\r\u001a\u00020\u00052\b\b\u0002\u0010\u000e\u001a\u00020\u000f2\b\b\u0002\u0010\u0010\u001a\u00020\u00112\b\b\u0002\u0010\u0012\u001a\u00020\u00132\b\b\u0002\u0010\u0014\u001a\u00020\u00152\b\b\u0002\u0010\u0016\u001a\u00020\u00052\b\b\u0002\u0010\u0017\u001a\u00020\u00052\u000e\b\u0002\u0010\u0018\u001a\b\u0012\u0004\u0012\u00020\u000f0\u00192\b\b\u0002\u0010\u001a\u001a\u00020\n2\u000e\b\u0002\u0010\u001b\u001a\b\u0012\u0004\u0012\u00020\u001c0\u00192\u000e\b\u0002\u0010\u001d\u001a\b\u0012\u0004\u0012\u00020\u000f0\u00192\b\b\u0002\u0010\u001e\u001a\u00020\u00052\b\b\u0002\u0010\u001f\u001a\u00020\u00052\b\b\u0002\u0010 \u001a\u00020\n2\b\b\u0002\u0010!\u001a\u00020\u00052\b\b\u0002\u0010\"\u001a\u00020\u00052\b\b\u0002\u0010#\u001a\u00020$2\b\b\u0002\u0010%\u001a\u00020&2\b\b\u0002\u0010'\u001a\u00020\u00052\b\b\u0002\u0010(\u001a\u00020\u00052\b\b\u0002\u0010)\u001a\u00020\u00052\u000e\b\u0002\u0010*\u001a\b\u0012\u0004\u0012\u00020\u000f0\u00192\b\b\u0002\u0010+\u001a\u00020\u00052\b\b\u0002\u0010,\u001a\u00020\u00052\b\b\u0002\u0010-\u001a\u00020\u00052\b\b\u0002\u0010.\u001a\u00020\u0005HÆ\u0001J\u0014\u0010\u001a\u00020$2\t\u0010\u0001\u001a\u0004\u0018\u00010\u000fHÖ\u0003J\n\u0010\u0001\u001a\u00020\nHÖ\u0001J\n\u0010\u0001\u001a\u00020\u0005HÖ\u0001R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b0\u00101R\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b2\u00103R\u0011\u0010\u0006\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b4\u00103R\u0011\u0010\u0007\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b5\u00103R\u0011\u0010\b\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b6\u00103R\u0011\u0010\t\u001a\u00020\n¢\u0006\b\n\u0000\u001a\u0004\b7\u00108R\u0011\u0010\u000b\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b9\u00103R\u0011\u0010\f\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b:\u00103R\u0011\u0010\r\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b;\u00103R\u0011\u0010\u000e\u001a\u00020\u000f¢\u0006\b\n\u0000\u001a\u0004\b<\u0010=R\u0011\u0010\u0010\u001a\u00020\u0011¢\u0006\b\n\u0000\u001a\u0004\b>\u0010?R\u0011\u0010\u0012\u001a\u00020\u0013¢\u0006\b\n\u0000\u001a\u0004\b@\u0010AR\u0011\u0010\u0014\u001a\u00020\u0015¢\u0006\b\n\u0000\u001a\u0004\bB\u0010CR\u0011\u0010\u0016\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\bD\u00103R\u0011\u0010\u0017\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\bE\u00103R\u0017\u0010\u0018\u001a\b\u0012\u0004\u0012\u00020\u000f0\u0019¢\u0006\b\n\u0000\u001a\u0004\bF\u0010GR\u0011\u0010\u001a\u001a\u00020\n¢\u0006\b\n\u0000\u001a\u0004\bH\u00108R\u0017\u0010\u001b\u001a\b\u0012\u0004\u0012\u00020\u001c0\u0019¢\u0006\b\n\u0000\u001a\u0004\bI\u0010GR\u0017\u0010\u001d\u001a\b\u0012\u0004\u0012\u00020\u000f0\u0019¢\u0006\b\n\u0000\u001a\u0004\bJ\u0010GR\u0011\u0010\u001e\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\bK\u00103R\u0011\u0010\u001f\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\bL\u00103R\u0011\u0010 \u001a\u00020\n¢\u0006\b\n\u0000\u001a\u0004\bM\u00108R\u0011\u0010!\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\bN\u00103R\u0011\u0010\"\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\bO\u00103R\u0011\u0010#\u001a\u00020$¢\u0006\b\n\u0000\u001a\u0004\bP\u0010QR\u0011\u0010%\u001a\u00020&¢\u0006\b\n\u0000\u001a\u0004\bR\u0010SR\u0011\u0010'\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\bT\u00103R\u0011\u0010(\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\bU\u00103R\u0011\u0010)\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\bV\u00103R\u0017\u0010*\u001a\b\u0012\u0004\u0012\u00020\u000f0\u0019¢\u0006\b\n\u0000\u001a\u0004\bW\u0010GR\u0011\u0010+\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\bX\u00103R\u0011\u0010,\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\bY\u00103R\u0011\u0010-\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\bZ\u00103R\u0011\u0010.\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b[\u00103¨\u0006\u0001"}, d2 = {"Lcom/iqonic/store/models/Order;", "Ljava/io/Serializable;", "billing", "Lcom/iqonic/store/models/Billing;", "cart_hash", "", "cart_tax", "created_via", "currency", "customer_id", "", "customer_ip_address", "customer_note", "customer_user_agent", "date_completed", "", "date_created", "Lcom/iqonic/store/models/DateCreated;", "date_modified", "Lcom/iqonic/store/models/DateModified;", "date_paid", "Lcom/iqonic/store/models/DatePaid;", "discount_tax", "discount_total", "fee_lines", "", "id", "line_items", "Lcom/iqonic/store/models/LineItems;", "meta_data", "number", "order_key", "parent_id", "payment_method", "payment_method_title", "prices_include_tax", "", "shipping", "Lcom/iqonic/store/models/Shipping;", "shipping_tax", "shipping_total", "status", "tax_lines", "total", "total_tax", "transaction_id", "version", "(Lcom/iqonic/store/models/Billing;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;Lcom/iqonic/store/models/DateCreated;Lcom/iqonic/store/models/DateModified;Lcom/iqonic/store/models/DatePaid;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;ILjava/util/List;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;ZLcom/iqonic/store/models/Shipping;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", "getBilling", "()Lcom/iqonic/store/models/Billing;", "getCart_hash", "()Ljava/lang/String;", "getCart_tax", "getCreated_via", "getCurrency", "getCustomer_id", "()I", "getCustomer_ip_address", "getCustomer_note", "getCustomer_user_agent", "getDate_completed", "()Ljava/lang/Object;", "getDate_created", "()Lcom/iqonic/store/models/DateCreated;", "getDate_modified", "()Lcom/iqonic/store/models/DateModified;", "getDate_paid", "()Lcom/iqonic/store/models/DatePaid;", "getDiscount_tax", "getDiscount_total", "getFee_lines", "()Ljava/util/List;", "getId", "getLine_items", "getMeta_data", "getNumber", "getOrder_key", "getParent_id", "getPayment_method", "getPayment_method_title", "getPrices_include_tax", "()Z", "getShipping", "()Lcom/iqonic/store/models/Shipping;", "getShipping_tax", "getShipping_total", "getStatus", "getTax_lines", "getTotal", "getTotal_tax", "getTransaction_id", "getVersion", "component1", "component10", "component11", "component12", "component13", "component14", "component15", "component16", "component17", "component18", "component19", "component2", "component20", "component21", "component22", "component23", "component24", "component25", "component26", "component27", "component28", "component29", "component3", "component30", "component31", "component32", "component33", "component34", "component4", "component5", "component6", "component7", "component8", "component9", "copy", "equals", "other", "hashCode", "toString", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: BaseResponse.kt */
public final class Order implements Serializable {
    private final Billing billing;
    private final String cart_hash;
    private final String cart_tax;
    private final String created_via;
    private final String currency;
    private final int customer_id;
    private final String customer_ip_address;
    private final String customer_note;
    private final String customer_user_agent;
    private final Object date_completed;
    private final DateCreated date_created;
    private final DateModified date_modified;
    private final DatePaid date_paid;
    private final String discount_tax;
    private final String discount_total;
    private final List<Object> fee_lines;
    private final int id;
    private final List<LineItems> line_items;
    private final List<Object> meta_data;
    private final String number;
    private final String order_key;
    private final int parent_id;
    private final String payment_method;
    private final String payment_method_title;
    private final boolean prices_include_tax;
    private final Shipping shipping;
    private final String shipping_tax;
    private final String shipping_total;
    private final String status;
    private final List<Object> tax_lines;
    private final String total;
    private final String total_tax;
    private final String transaction_id;
    private final String version;

    public static /* synthetic */ Order copy$default(Order order, Billing billing2, String str, String str2, String str3, String str4, int i, String str5, String str6, String str7, Object obj, DateCreated dateCreated, DateModified dateModified, DatePaid datePaid, String str8, String str9, List list, int i2, List list2, List list3, String str10, String str11, int i3, String str12, String str13, boolean z, Shipping shipping2, String str14, String str15, String str16, List list4, String str17, String str18, String str19, String str20, int i4, int i5, Object obj2) {
        Order order2 = order;
        int i6 = i4;
        return order.copy((i6 & 1) != 0 ? order2.billing : billing2, (i6 & 2) != 0 ? order2.cart_hash : str, (i6 & 4) != 0 ? order2.cart_tax : str2, (i6 & 8) != 0 ? order2.created_via : str3, (i6 & 16) != 0 ? order2.currency : str4, (i6 & 32) != 0 ? order2.customer_id : i, (i6 & 64) != 0 ? order2.customer_ip_address : str5, (i6 & 128) != 0 ? order2.customer_note : str6, (i6 & 256) != 0 ? order2.customer_user_agent : str7, (i6 & 512) != 0 ? order2.date_completed : obj, (i6 & 1024) != 0 ? order2.date_created : dateCreated, (i6 & 2048) != 0 ? order2.date_modified : dateModified, (i6 & 4096) != 0 ? order2.date_paid : datePaid, (i6 & 8192) != 0 ? order2.discount_tax : str8, (i6 & 16384) != 0 ? order2.discount_total : str9, (i6 & 32768) != 0 ? order2.fee_lines : list, (i6 & 65536) != 0 ? order2.id : i2, (i6 & 131072) != 0 ? order2.line_items : list2, (i6 & 262144) != 0 ? order2.meta_data : list3, (i6 & 524288) != 0 ? order2.number : str10, (i6 & 1048576) != 0 ? order2.order_key : str11, (i6 & 2097152) != 0 ? order2.parent_id : i3, (i6 & 4194304) != 0 ? order2.payment_method : str12, (i6 & 8388608) != 0 ? order2.payment_method_title : str13, (i6 & 16777216) != 0 ? order2.prices_include_tax : z, (i6 & 33554432) != 0 ? order2.shipping : shipping2, (i6 & 67108864) != 0 ? order2.shipping_tax : str14, (i6 & 134217728) != 0 ? order2.shipping_total : str15, (i6 & 268435456) != 0 ? order2.status : str16, (i6 & PKIFailureInfo.duplicateCertReq) != 0 ? order2.tax_lines : list4, (i6 & 1073741824) != 0 ? order2.total : str17, (i6 & Integer.MIN_VALUE) != 0 ? order2.total_tax : str18, (i5 & 1) != 0 ? order2.transaction_id : str19, (i5 & 2) != 0 ? order2.version : str20);
    }

    public final Billing component1() {
        return this.billing;
    }

    public final Object component10() {
        return this.date_completed;
    }

    public final DateCreated component11() {
        return this.date_created;
    }

    public final DateModified component12() {
        return this.date_modified;
    }

    public final DatePaid component13() {
        return this.date_paid;
    }

    public final String component14() {
        return this.discount_tax;
    }

    public final String component15() {
        return this.discount_total;
    }

    public final List<Object> component16() {
        return this.fee_lines;
    }

    public final int component17() {
        return this.id;
    }

    public final List<LineItems> component18() {
        return this.line_items;
    }

    public final List<Object> component19() {
        return this.meta_data;
    }

    public final String component2() {
        return this.cart_hash;
    }

    public final String component20() {
        return this.number;
    }

    public final String component21() {
        return this.order_key;
    }

    public final int component22() {
        return this.parent_id;
    }

    public final String component23() {
        return this.payment_method;
    }

    public final String component24() {
        return this.payment_method_title;
    }

    public final boolean component25() {
        return this.prices_include_tax;
    }

    public final Shipping component26() {
        return this.shipping;
    }

    public final String component27() {
        return this.shipping_tax;
    }

    public final String component28() {
        return this.shipping_total;
    }

    public final String component29() {
        return this.status;
    }

    public final String component3() {
        return this.cart_tax;
    }

    public final List<Object> component30() {
        return this.tax_lines;
    }

    public final String component31() {
        return this.total;
    }

    public final String component32() {
        return this.total_tax;
    }

    public final String component33() {
        return this.transaction_id;
    }

    public final String component34() {
        return this.version;
    }

    public final String component4() {
        return this.created_via;
    }

    public final String component5() {
        return this.currency;
    }

    public final int component6() {
        return this.customer_id;
    }

    public final String component7() {
        return this.customer_ip_address;
    }

    public final String component8() {
        return this.customer_note;
    }

    public final String component9() {
        return this.customer_user_agent;
    }

    public final Order copy(Billing billing2, String str, String str2, String str3, String str4, int i, String str5, String str6, String str7, Object obj, DateCreated dateCreated, DateModified dateModified, DatePaid datePaid, String str8, String str9, List<? extends Object> list, int i2, List<LineItems> list2, List<? extends Object> list3, String str10, String str11, int i3, String str12, String str13, boolean z, Shipping shipping2, String str14, String str15, String str16, List<? extends Object> list4, String str17, String str18, String str19, String str20) {
        Billing billing3 = billing2;
        Intrinsics.checkParameterIsNotNull(billing3, "billing");
        Intrinsics.checkParameterIsNotNull(str, "cart_hash");
        Intrinsics.checkParameterIsNotNull(str2, "cart_tax");
        Intrinsics.checkParameterIsNotNull(str3, "created_via");
        Intrinsics.checkParameterIsNotNull(str4, FirebaseAnalytics.Param.CURRENCY);
        Intrinsics.checkParameterIsNotNull(str5, "customer_ip_address");
        Intrinsics.checkParameterIsNotNull(str6, "customer_note");
        Intrinsics.checkParameterIsNotNull(str7, "customer_user_agent");
        Intrinsics.checkParameterIsNotNull(obj, "date_completed");
        Intrinsics.checkParameterIsNotNull(dateCreated, "date_created");
        Intrinsics.checkParameterIsNotNull(dateModified, "date_modified");
        Intrinsics.checkParameterIsNotNull(datePaid, "date_paid");
        Intrinsics.checkParameterIsNotNull(str8, "discount_tax");
        Intrinsics.checkParameterIsNotNull(str9, "discount_total");
        Intrinsics.checkParameterIsNotNull(list, "fee_lines");
        Intrinsics.checkParameterIsNotNull(list2, "line_items");
        Intrinsics.checkParameterIsNotNull(list3, "meta_data");
        Intrinsics.checkParameterIsNotNull(str10, "number");
        Intrinsics.checkParameterIsNotNull(str11, "order_key");
        Intrinsics.checkParameterIsNotNull(str12, "payment_method");
        Intrinsics.checkParameterIsNotNull(str13, "payment_method_title");
        Intrinsics.checkParameterIsNotNull(shipping2, FirebaseAnalytics.Param.SHIPPING);
        Intrinsics.checkParameterIsNotNull(str14, "shipping_tax");
        Intrinsics.checkParameterIsNotNull(str15, "shipping_total");
        Intrinsics.checkParameterIsNotNull(str16, "status");
        Intrinsics.checkParameterIsNotNull(list4, "tax_lines");
        Intrinsics.checkParameterIsNotNull(str17, "total");
        Intrinsics.checkParameterIsNotNull(str18, "total_tax");
        Intrinsics.checkParameterIsNotNull(str19, FirebaseAnalytics.Param.TRANSACTION_ID);
        Intrinsics.checkParameterIsNotNull(str20, ServerProtocol.FALLBACK_DIALOG_PARAM_VERSION);
        return new Order(billing3, str, str2, str3, str4, i, str5, str6, str7, obj, dateCreated, dateModified, datePaid, str8, str9, list, i2, list2, list3, str10, str11, i3, str12, str13, z, shipping2, str14, str15, str16, list4, str17, str18, str19, str20);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Order)) {
            return false;
        }
        Order order = (Order) obj;
        return Intrinsics.areEqual((Object) this.billing, (Object) order.billing) && Intrinsics.areEqual((Object) this.cart_hash, (Object) order.cart_hash) && Intrinsics.areEqual((Object) this.cart_tax, (Object) order.cart_tax) && Intrinsics.areEqual((Object) this.created_via, (Object) order.created_via) && Intrinsics.areEqual((Object) this.currency, (Object) order.currency) && this.customer_id == order.customer_id && Intrinsics.areEqual((Object) this.customer_ip_address, (Object) order.customer_ip_address) && Intrinsics.areEqual((Object) this.customer_note, (Object) order.customer_note) && Intrinsics.areEqual((Object) this.customer_user_agent, (Object) order.customer_user_agent) && Intrinsics.areEqual(this.date_completed, order.date_completed) && Intrinsics.areEqual((Object) this.date_created, (Object) order.date_created) && Intrinsics.areEqual((Object) this.date_modified, (Object) order.date_modified) && Intrinsics.areEqual((Object) this.date_paid, (Object) order.date_paid) && Intrinsics.areEqual((Object) this.discount_tax, (Object) order.discount_tax) && Intrinsics.areEqual((Object) this.discount_total, (Object) order.discount_total) && Intrinsics.areEqual((Object) this.fee_lines, (Object) order.fee_lines) && this.id == order.id && Intrinsics.areEqual((Object) this.line_items, (Object) order.line_items) && Intrinsics.areEqual((Object) this.meta_data, (Object) order.meta_data) && Intrinsics.areEqual((Object) this.number, (Object) order.number) && Intrinsics.areEqual((Object) this.order_key, (Object) order.order_key) && this.parent_id == order.parent_id && Intrinsics.areEqual((Object) this.payment_method, (Object) order.payment_method) && Intrinsics.areEqual((Object) this.payment_method_title, (Object) order.payment_method_title) && this.prices_include_tax == order.prices_include_tax && Intrinsics.areEqual((Object) this.shipping, (Object) order.shipping) && Intrinsics.areEqual((Object) this.shipping_tax, (Object) order.shipping_tax) && Intrinsics.areEqual((Object) this.shipping_total, (Object) order.shipping_total) && Intrinsics.areEqual((Object) this.status, (Object) order.status) && Intrinsics.areEqual((Object) this.tax_lines, (Object) order.tax_lines) && Intrinsics.areEqual((Object) this.total, (Object) order.total) && Intrinsics.areEqual((Object) this.total_tax, (Object) order.total_tax) && Intrinsics.areEqual((Object) this.transaction_id, (Object) order.transaction_id) && Intrinsics.areEqual((Object) this.version, (Object) order.version);
    }

    public int hashCode() {
        Billing billing2 = this.billing;
        int i = 0;
        int hashCode = (billing2 != null ? billing2.hashCode() : 0) * 31;
        String str = this.cart_hash;
        int hashCode2 = (hashCode + (str != null ? str.hashCode() : 0)) * 31;
        String str2 = this.cart_tax;
        int hashCode3 = (hashCode2 + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.created_via;
        int hashCode4 = (hashCode3 + (str3 != null ? str3.hashCode() : 0)) * 31;
        String str4 = this.currency;
        int hashCode5 = (((hashCode4 + (str4 != null ? str4.hashCode() : 0)) * 31) + this.customer_id) * 31;
        String str5 = this.customer_ip_address;
        int hashCode6 = (hashCode5 + (str5 != null ? str5.hashCode() : 0)) * 31;
        String str6 = this.customer_note;
        int hashCode7 = (hashCode6 + (str6 != null ? str6.hashCode() : 0)) * 31;
        String str7 = this.customer_user_agent;
        int hashCode8 = (hashCode7 + (str7 != null ? str7.hashCode() : 0)) * 31;
        Object obj = this.date_completed;
        int hashCode9 = (hashCode8 + (obj != null ? obj.hashCode() : 0)) * 31;
        DateCreated dateCreated = this.date_created;
        int hashCode10 = (hashCode9 + (dateCreated != null ? dateCreated.hashCode() : 0)) * 31;
        DateModified dateModified = this.date_modified;
        int hashCode11 = (hashCode10 + (dateModified != null ? dateModified.hashCode() : 0)) * 31;
        DatePaid datePaid = this.date_paid;
        int hashCode12 = (hashCode11 + (datePaid != null ? datePaid.hashCode() : 0)) * 31;
        String str8 = this.discount_tax;
        int hashCode13 = (hashCode12 + (str8 != null ? str8.hashCode() : 0)) * 31;
        String str9 = this.discount_total;
        int hashCode14 = (hashCode13 + (str9 != null ? str9.hashCode() : 0)) * 31;
        List<Object> list = this.fee_lines;
        int hashCode15 = (((hashCode14 + (list != null ? list.hashCode() : 0)) * 31) + this.id) * 31;
        List<LineItems> list2 = this.line_items;
        int hashCode16 = (hashCode15 + (list2 != null ? list2.hashCode() : 0)) * 31;
        List<Object> list3 = this.meta_data;
        int hashCode17 = (hashCode16 + (list3 != null ? list3.hashCode() : 0)) * 31;
        String str10 = this.number;
        int hashCode18 = (hashCode17 + (str10 != null ? str10.hashCode() : 0)) * 31;
        String str11 = this.order_key;
        int hashCode19 = (((hashCode18 + (str11 != null ? str11.hashCode() : 0)) * 31) + this.parent_id) * 31;
        String str12 = this.payment_method;
        int hashCode20 = (hashCode19 + (str12 != null ? str12.hashCode() : 0)) * 31;
        String str13 = this.payment_method_title;
        int hashCode21 = (hashCode20 + (str13 != null ? str13.hashCode() : 0)) * 31;
        boolean z = this.prices_include_tax;
        if (z) {
            z = true;
        }
        int i2 = (hashCode21 + (z ? 1 : 0)) * 31;
        Shipping shipping2 = this.shipping;
        int hashCode22 = (i2 + (shipping2 != null ? shipping2.hashCode() : 0)) * 31;
        String str14 = this.shipping_tax;
        int hashCode23 = (hashCode22 + (str14 != null ? str14.hashCode() : 0)) * 31;
        String str15 = this.shipping_total;
        int hashCode24 = (hashCode23 + (str15 != null ? str15.hashCode() : 0)) * 31;
        String str16 = this.status;
        int hashCode25 = (hashCode24 + (str16 != null ? str16.hashCode() : 0)) * 31;
        List<Object> list4 = this.tax_lines;
        int hashCode26 = (hashCode25 + (list4 != null ? list4.hashCode() : 0)) * 31;
        String str17 = this.total;
        int hashCode27 = (hashCode26 + (str17 != null ? str17.hashCode() : 0)) * 31;
        String str18 = this.total_tax;
        int hashCode28 = (hashCode27 + (str18 != null ? str18.hashCode() : 0)) * 31;
        String str19 = this.transaction_id;
        int hashCode29 = (hashCode28 + (str19 != null ? str19.hashCode() : 0)) * 31;
        String str20 = this.version;
        if (str20 != null) {
            i = str20.hashCode();
        }
        return hashCode29 + i;
    }

    public String toString() {
        return "Order(billing=" + this.billing + ", cart_hash=" + this.cart_hash + ", cart_tax=" + this.cart_tax + ", created_via=" + this.created_via + ", currency=" + this.currency + ", customer_id=" + this.customer_id + ", customer_ip_address=" + this.customer_ip_address + ", customer_note=" + this.customer_note + ", customer_user_agent=" + this.customer_user_agent + ", date_completed=" + this.date_completed + ", date_created=" + this.date_created + ", date_modified=" + this.date_modified + ", date_paid=" + this.date_paid + ", discount_tax=" + this.discount_tax + ", discount_total=" + this.discount_total + ", fee_lines=" + this.fee_lines + ", id=" + this.id + ", line_items=" + this.line_items + ", meta_data=" + this.meta_data + ", number=" + this.number + ", order_key=" + this.order_key + ", parent_id=" + this.parent_id + ", payment_method=" + this.payment_method + ", payment_method_title=" + this.payment_method_title + ", prices_include_tax=" + this.prices_include_tax + ", shipping=" + this.shipping + ", shipping_tax=" + this.shipping_tax + ", shipping_total=" + this.shipping_total + ", status=" + this.status + ", tax_lines=" + this.tax_lines + ", total=" + this.total + ", total_tax=" + this.total_tax + ", transaction_id=" + this.transaction_id + ", version=" + this.version + ")";
    }

    public Order(Billing billing2, String str, String str2, String str3, String str4, int i, String str5, String str6, String str7, Object obj, DateCreated dateCreated, DateModified dateModified, DatePaid datePaid, String str8, String str9, List<? extends Object> list, int i2, List<LineItems> list2, List<? extends Object> list3, String str10, String str11, int i3, String str12, String str13, boolean z, Shipping shipping2, String str14, String str15, String str16, List<? extends Object> list4, String str17, String str18, String str19, String str20) {
        Billing billing3 = billing2;
        String str21 = str;
        String str22 = str2;
        String str23 = str3;
        String str24 = str4;
        String str25 = str5;
        String str26 = str6;
        String str27 = str7;
        Object obj2 = obj;
        DateCreated dateCreated2 = dateCreated;
        DateModified dateModified2 = dateModified;
        DatePaid datePaid2 = datePaid;
        String str28 = str8;
        String str29 = str9;
        List<LineItems> list5 = list2;
        Intrinsics.checkParameterIsNotNull(billing3, "billing");
        Intrinsics.checkParameterIsNotNull(str21, "cart_hash");
        Intrinsics.checkParameterIsNotNull(str22, "cart_tax");
        Intrinsics.checkParameterIsNotNull(str23, "created_via");
        Intrinsics.checkParameterIsNotNull(str24, FirebaseAnalytics.Param.CURRENCY);
        Intrinsics.checkParameterIsNotNull(str25, "customer_ip_address");
        Intrinsics.checkParameterIsNotNull(str26, "customer_note");
        Intrinsics.checkParameterIsNotNull(str27, "customer_user_agent");
        Intrinsics.checkParameterIsNotNull(obj2, "date_completed");
        Intrinsics.checkParameterIsNotNull(dateCreated2, "date_created");
        Intrinsics.checkParameterIsNotNull(dateModified2, "date_modified");
        Intrinsics.checkParameterIsNotNull(datePaid2, "date_paid");
        Intrinsics.checkParameterIsNotNull(str28, "discount_tax");
        Intrinsics.checkParameterIsNotNull(str29, "discount_total");
        Intrinsics.checkParameterIsNotNull(list, "fee_lines");
        Intrinsics.checkParameterIsNotNull(list2, "line_items");
        Intrinsics.checkParameterIsNotNull(list3, "meta_data");
        Intrinsics.checkParameterIsNotNull(str10, "number");
        Intrinsics.checkParameterIsNotNull(str11, "order_key");
        Intrinsics.checkParameterIsNotNull(str12, "payment_method");
        Intrinsics.checkParameterIsNotNull(str13, "payment_method_title");
        Intrinsics.checkParameterIsNotNull(shipping2, FirebaseAnalytics.Param.SHIPPING);
        Intrinsics.checkParameterIsNotNull(str14, "shipping_tax");
        Intrinsics.checkParameterIsNotNull(str15, "shipping_total");
        Intrinsics.checkParameterIsNotNull(str16, "status");
        Intrinsics.checkParameterIsNotNull(list4, "tax_lines");
        Intrinsics.checkParameterIsNotNull(str17, "total");
        Intrinsics.checkParameterIsNotNull(str18, "total_tax");
        Intrinsics.checkParameterIsNotNull(str19, FirebaseAnalytics.Param.TRANSACTION_ID);
        Intrinsics.checkParameterIsNotNull(str20, ServerProtocol.FALLBACK_DIALOG_PARAM_VERSION);
        this.billing = billing3;
        this.cart_hash = str21;
        this.cart_tax = str22;
        this.created_via = str23;
        this.currency = str24;
        this.customer_id = i;
        this.customer_ip_address = str25;
        this.customer_note = str26;
        this.customer_user_agent = str27;
        this.date_completed = obj2;
        this.date_created = dateCreated2;
        this.date_modified = dateModified2;
        this.date_paid = datePaid2;
        this.discount_tax = str28;
        this.discount_total = str29;
        this.fee_lines = list;
        this.id = i2;
        this.line_items = list2;
        this.meta_data = list3;
        this.number = str10;
        this.order_key = str11;
        this.parent_id = i3;
        this.payment_method = str12;
        this.payment_method_title = str13;
        this.prices_include_tax = z;
        this.shipping = shipping2;
        this.shipping_tax = str14;
        this.shipping_total = str15;
        this.status = str16;
        this.tax_lines = list4;
        this.total = str17;
        this.total_tax = str18;
        this.transaction_id = str19;
        this.version = str20;
    }

    public final Billing getBilling() {
        return this.billing;
    }

    public final String getCart_hash() {
        return this.cart_hash;
    }

    public final String getCart_tax() {
        return this.cart_tax;
    }

    public final String getCreated_via() {
        return this.created_via;
    }

    public final String getCurrency() {
        return this.currency;
    }

    public final int getCustomer_id() {
        return this.customer_id;
    }

    public final String getCustomer_ip_address() {
        return this.customer_ip_address;
    }

    public final String getCustomer_note() {
        return this.customer_note;
    }

    public final String getCustomer_user_agent() {
        return this.customer_user_agent;
    }

    public final Object getDate_completed() {
        return this.date_completed;
    }

    public final DateCreated getDate_created() {
        return this.date_created;
    }

    public final DateModified getDate_modified() {
        return this.date_modified;
    }

    public final DatePaid getDate_paid() {
        return this.date_paid;
    }

    public final String getDiscount_tax() {
        return this.discount_tax;
    }

    public final String getDiscount_total() {
        return this.discount_total;
    }

    public final List<Object> getFee_lines() {
        return this.fee_lines;
    }

    public final int getId() {
        return this.id;
    }

    public final List<LineItems> getLine_items() {
        return this.line_items;
    }

    public final List<Object> getMeta_data() {
        return this.meta_data;
    }

    public final String getNumber() {
        return this.number;
    }

    public final String getOrder_key() {
        return this.order_key;
    }

    public final int getParent_id() {
        return this.parent_id;
    }

    public final String getPayment_method() {
        return this.payment_method;
    }

    public final String getPayment_method_title() {
        return this.payment_method_title;
    }

    public final boolean getPrices_include_tax() {
        return this.prices_include_tax;
    }

    public final Shipping getShipping() {
        return this.shipping;
    }

    public final String getShipping_tax() {
        return this.shipping_tax;
    }

    public final String getShipping_total() {
        return this.shipping_total;
    }

    public final String getStatus() {
        return this.status;
    }

    public final List<Object> getTax_lines() {
        return this.tax_lines;
    }

    public final String getTotal() {
        return this.total;
    }

    public final String getTotal_tax() {
        return this.total_tax;
    }

    public final String getTransaction_id() {
        return this.transaction_id;
    }

    public final String getVersion() {
        return this.version;
    }
}
