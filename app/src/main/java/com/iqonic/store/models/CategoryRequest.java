package com.iqonic.store.models;

import kotlin.Metadata;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\b\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002R\u001a\u0010\u0003\u001a\u00020\u0004X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\bR\u001a\u0010\t\u001a\u00020\u0004X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\n\u0010\u0006\"\u0004\b\u000b\u0010\b¨\u0006\f"}, d2 = {"Lcom/iqonic/store/models/CategoryRequest;", "", "()V", "page", "", "getPage", "()I", "setPage", "(I)V", "parent", "getParent", "setParent", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: RequestModel.kt */
public final class CategoryRequest {
    private int page;
    private int parent;

    public final int getPage() {
        return this.page;
    }

    public final void setPage(int i) {
        this.page = i;
    }

    public final int getParent() {
        return this.parent;
    }

    public final void setParent(int i) {
        this.parent = i;
    }
}
