package com.iqonic.store.models;

import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u000b\n\u0002\u0010\u000b\n\u0002\b\u0014\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002R\u0014\u0010\u0003\u001a\u00020\u0004XD¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006R\u0014\u0010\u0007\u001a\u00020\u0004XD¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\u0006R\u0014\u0010\t\u001a\u00020\u0004XD¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u0006R\u0014\u0010\u000b\u001a\u00020\u0004XD¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\u0006R\u0014\u0010\r\u001a\u00020\u0004XD¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u0006R\u0014\u0010\u000f\u001a\u00020\u0010XD¢\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u0012R\u0014\u0010\u0013\u001a\u00020\u0004XD¢\u0006\b\n\u0000\u001a\u0004\b\u0014\u0010\u0006R\u001a\u0010\u0015\u001a\u00020\u0004X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0016\u0010\u0006\"\u0004\b\u0017\u0010\u0018R\u001a\u0010\u0019\u001a\u00020\u0004X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u001a\u0010\u0006\"\u0004\b\u001b\u0010\u0018R\u0014\u0010\u001c\u001a\u00020\u0004XD¢\u0006\b\n\u0000\u001a\u0004\b\u001d\u0010\u0006R\u0014\u0010\u001e\u001a\u00020\u0004XD¢\u0006\b\n\u0000\u001a\u0004\b\u001f\u0010\u0006R\u0014\u0010 \u001a\u00020\u0004XD¢\u0006\b\n\u0000\u001a\u0004\b!\u0010\u0006R\u0014\u0010\"\u001a\u00020\u0004XD¢\u0006\b\n\u0000\u001a\u0004\b#\u0010\u0006¨\u0006$"}, d2 = {"Lcom/iqonic/store/models/CartResponse;", "", "()V", "cart_id", "", "getCart_id", "()Ljava/lang/String;", "code", "getCode", "created_at", "getCreated_at", "full", "getFull", "name", "getName", "on_sale", "", "getOn_sale", "()Z", "price", "getPrice", "pro_id", "getPro_id", "setPro_id", "(Ljava/lang/String;)V", "quantity", "getQuantity", "setQuantity", "regular_price", "getRegular_price", "sale_price", "getSale_price", "sku", "getSku", "thumbnail", "getThumbnail", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: BaseResponse.kt */
public final class CartResponse {
    private final String cart_id = "";
    private final String code = "";
    private final String created_at = "";
    private final String full = "";
    private final String name = "";
    private final boolean on_sale;
    private final String price = "0.0";
    private String pro_id = "";
    private String quantity = "";
    private final String regular_price = "0.0";
    private final String sale_price = "0.0";
    private final String sku = "";
    private final String thumbnail = "";

    public final String getCode() {
        return this.code;
    }

    public final String getCart_id() {
        return this.cart_id;
    }

    public final String getCreated_at() {
        return this.created_at;
    }

    public final String getFull() {
        return this.full;
    }

    public final String getName() {
        return this.name;
    }

    public final String getPrice() {
        return this.price;
    }

    public final String getPro_id() {
        return this.pro_id;
    }

    public final void setPro_id(String str) {
        Intrinsics.checkParameterIsNotNull(str, "<set-?>");
        this.pro_id = str;
    }

    public final String getQuantity() {
        return this.quantity;
    }

    public final void setQuantity(String str) {
        Intrinsics.checkParameterIsNotNull(str, "<set-?>");
        this.quantity = str;
    }

    public final String getRegular_price() {
        return this.regular_price;
    }

    public final String getSale_price() {
        return this.sale_price;
    }

    public final boolean getOn_sale() {
        return this.on_sale;
    }

    public final String getSku() {
        return this.sku;
    }

    public final String getThumbnail() {
        return this.thumbnail;
    }
}
