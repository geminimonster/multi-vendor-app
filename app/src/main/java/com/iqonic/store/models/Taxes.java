package com.iqonic.store.models;

import java.io.Serializable;
import java.util.List;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0010\u0000\n\u0002\b\t\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B!\u0012\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003\u0012\f\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003¢\u0006\u0002\u0010\u0006J\u000f\u0010\n\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003HÆ\u0003J\u000f\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003HÆ\u0003J)\u0010\f\u001a\u00020\u00002\u000e\b\u0002\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u00032\u000e\b\u0002\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003HÆ\u0001J\u0013\u0010\r\u001a\u00020\u000e2\b\u0010\u000f\u001a\u0004\u0018\u00010\u0004HÖ\u0003J\t\u0010\u0010\u001a\u00020\u0011HÖ\u0001J\t\u0010\u0012\u001a\u00020\u0013HÖ\u0001R\u0017\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0017\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\b¨\u0006\u0014"}, d2 = {"Lcom/iqonic/store/models/Taxes;", "Ljava/io/Serializable;", "subtotal", "", "", "total", "(Ljava/util/List;Ljava/util/List;)V", "getSubtotal", "()Ljava/util/List;", "getTotal", "component1", "component2", "copy", "equals", "", "other", "hashCode", "", "toString", "", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: BaseResponse.kt */
public final class Taxes implements Serializable {
    private final List<Object> subtotal;
    private final List<Object> total;

    public static /* synthetic */ Taxes copy$default(Taxes taxes, List<Object> list, List<Object> list2, int i, Object obj) {
        if ((i & 1) != 0) {
            list = taxes.subtotal;
        }
        if ((i & 2) != 0) {
            list2 = taxes.total;
        }
        return taxes.copy(list, list2);
    }

    public final List<Object> component1() {
        return this.subtotal;
    }

    public final List<Object> component2() {
        return this.total;
    }

    public final Taxes copy(List<? extends Object> list, List<? extends Object> list2) {
        Intrinsics.checkParameterIsNotNull(list, "subtotal");
        Intrinsics.checkParameterIsNotNull(list2, "total");
        return new Taxes(list, list2);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Taxes)) {
            return false;
        }
        Taxes taxes = (Taxes) obj;
        return Intrinsics.areEqual((Object) this.subtotal, (Object) taxes.subtotal) && Intrinsics.areEqual((Object) this.total, (Object) taxes.total);
    }

    public int hashCode() {
        List<Object> list = this.subtotal;
        int i = 0;
        int hashCode = (list != null ? list.hashCode() : 0) * 31;
        List<Object> list2 = this.total;
        if (list2 != null) {
            i = list2.hashCode();
        }
        return hashCode + i;
    }

    public String toString() {
        return "Taxes(subtotal=" + this.subtotal + ", total=" + this.total + ")";
    }

    public Taxes(List<? extends Object> list, List<? extends Object> list2) {
        Intrinsics.checkParameterIsNotNull(list, "subtotal");
        Intrinsics.checkParameterIsNotNull(list2, "total");
        this.subtotal = list;
        this.total = list2;
    }

    public final List<Object> getSubtotal() {
        return this.subtotal;
    }

    public final List<Object> getTotal() {
        return this.total;
    }
}
