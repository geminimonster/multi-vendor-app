package com.iqonic.store.models;

import com.google.gson.annotations.SerializedName;
import kotlin.Metadata;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002R\u0018\u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0006X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006R\u0018\u0010\u0007\u001a\u0004\u0018\u00010\b8\u0006X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0018\u0010\u000b\u001a\u0004\u0018\u00010\f8\u0006X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000e¨\u0006\u000f"}, d2 = {"Lcom/iqonic/store/models/DashBoardResponse;", "", "()V", "Productdetailview", "Lcom/iqonic/store/models/BuilderDetail;", "getProductdetailview", "()Lcom/iqonic/store/models/BuilderDetail;", "appSetup", "Lcom/iqonic/store/models/AppSetup;", "getAppSetup", "()Lcom/iqonic/store/models/AppSetup;", "dashboard", "Lcom/iqonic/store/models/BuilderDashboard;", "getDashboard", "()Lcom/iqonic/store/models/BuilderDashboard;", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: DashBoardResponse.kt */
public final class DashBoardResponse {
    @SerializedName("Productdetailview")
    private final BuilderDetail Productdetailview;
    @SerializedName("appSetup")
    private final AppSetup appSetup;
    @SerializedName("dashboard")
    private final BuilderDashboard dashboard;

    public final BuilderDashboard getDashboard() {
        return this.dashboard;
    }

    public final BuilderDetail getProductdetailview() {
        return this.Productdetailview;
    }

    public final AppSetup getAppSetup() {
        return this.appSetup;
    }
}
