package com.iqonic.store.models;

import java.io.Serializable;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\b\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u001d\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002R\u001a\u0010\u0003\u001a\u00020\u0004X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\bR\u001a\u0010\t\u001a\u00020\u0004X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\n\u0010\u0006\"\u0004\b\u000b\u0010\bR\u001a\u0010\f\u001a\u00020\rX\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u000e\u0010\u000f\"\u0004\b\u0010\u0010\u0011R\u001a\u0010\u0012\u001a\u00020\u0013X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0014\u0010\u0015\"\u0004\b\u0016\u0010\u0017R\u001a\u0010\u0018\u001a\u00020\u0004X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0019\u0010\u0006\"\u0004\b\u001a\u0010\bR\u001a\u0010\u001b\u001a\u00020\u0004X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u001c\u0010\u0006\"\u0004\b\u001d\u0010\bR\u001a\u0010\u001e\u001a\u00020\u0004X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u001f\u0010\u0006\"\u0004\b \u0010\bR\u001a\u0010!\u001a\u00020\u0004X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\"\u0010\u0006\"\u0004\b#\u0010\bR\u001a\u0010$\u001a\u00020\u0004X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b%\u0010\u0006\"\u0004\b&\u0010\bR\u001a\u0010'\u001a\u00020\u0004X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b(\u0010\u0006\"\u0004\b)\u0010\bR\u001a\u0010*\u001a\u00020\u0004X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b+\u0010\u0006\"\u0004\b,\u0010\bR\u001a\u0010-\u001a\u00020\u0004X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b.\u0010\u0006\"\u0004\b/\u0010\b¨\u00060"}, d2 = {"Lcom/iqonic/store/models/SearchProduct;", "Ljava/io/Serializable;", "()V", "description", "", "getDescription", "()Ljava/lang/String;", "setDescription", "(Ljava/lang/String;)V", "height", "getHeight", "setHeight", "id", "", "getId", "()I", "setId", "(I)V", "images", "Lcom/iqonic/store/models/Images;", "getImages", "()Lcom/iqonic/store/models/Images;", "setImages", "(Lcom/iqonic/store/models/Images;)V", "length", "getLength", "setLength", "name", "getName", "setName", "price", "getPrice", "setPrice", "regular_price", "getRegular_price", "setRegular_price", "sale_price", "getSale_price", "setSale_price", "status", "getStatus", "setStatus", "stock_status", "getStock_status", "setStock_status", "width", "getWidth", "setWidth", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: SearchProduct.kt */
public final class SearchProduct implements Serializable {
    private String description = "";
    private String height = "";
    private int id;
    private Images images = new Images();
    private String length = "";
    private String name = "";
    private String price = "";
    private String regular_price = "";
    private String sale_price = "";
    private String status = "";
    private String stock_status = "";
    private String width = "";

    public final String getDescription() {
        return this.description;
    }

    public final void setDescription(String str) {
        Intrinsics.checkParameterIsNotNull(str, "<set-?>");
        this.description = str;
    }

    public final String getHeight() {
        return this.height;
    }

    public final void setHeight(String str) {
        Intrinsics.checkParameterIsNotNull(str, "<set-?>");
        this.height = str;
    }

    public final int getId() {
        return this.id;
    }

    public final void setId(int i) {
        this.id = i;
    }

    public final Images getImages() {
        return this.images;
    }

    public final void setImages(Images images2) {
        Intrinsics.checkParameterIsNotNull(images2, "<set-?>");
        this.images = images2;
    }

    public final String getLength() {
        return this.length;
    }

    public final void setLength(String str) {
        Intrinsics.checkParameterIsNotNull(str, "<set-?>");
        this.length = str;
    }

    public final String getName() {
        return this.name;
    }

    public final void setName(String str) {
        Intrinsics.checkParameterIsNotNull(str, "<set-?>");
        this.name = str;
    }

    public final String getPrice() {
        return this.price;
    }

    public final void setPrice(String str) {
        Intrinsics.checkParameterIsNotNull(str, "<set-?>");
        this.price = str;
    }

    public final String getRegular_price() {
        return this.regular_price;
    }

    public final void setRegular_price(String str) {
        Intrinsics.checkParameterIsNotNull(str, "<set-?>");
        this.regular_price = str;
    }

    public final String getSale_price() {
        return this.sale_price;
    }

    public final void setSale_price(String str) {
        Intrinsics.checkParameterIsNotNull(str, "<set-?>");
        this.sale_price = str;
    }

    public final String getStatus() {
        return this.status;
    }

    public final void setStatus(String str) {
        Intrinsics.checkParameterIsNotNull(str, "<set-?>");
        this.status = str;
    }

    public final String getStock_status() {
        return this.stock_status;
    }

    public final void setStock_status(String str) {
        Intrinsics.checkParameterIsNotNull(str, "<set-?>");
        this.stock_status = str;
    }

    public final String getWidth() {
        return this.width;
    }

    public final void setWidth(String str) {
        Intrinsics.checkParameterIsNotNull(str, "<set-?>");
        this.width = str;
    }
}
