package com.iqonic.store.models;

import com.google.firebase.analytics.FirebaseAnalytics;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b(\b\b\u0018\u00002\u00020\u0001Bu\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0003\u0012\u0006\u0010\u0007\u001a\u00020\u0003\u0012\u0006\u0010\b\u001a\u00020\u0003\u0012\u0006\u0010\t\u001a\u00020\u0003\u0012\u0006\u0010\n\u001a\u00020\u0003\u0012\u0006\u0010\u000b\u001a\u00020\u0003\u0012\u0006\u0010\f\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u0012\u0006\u0010\u0010\u001a\u00020\u0003\u0012\u0006\u0010\u0011\u001a\u00020\u0003\u0012\u0006\u0010\u0012\u001a\u00020\u0013\u0012\u0006\u0010\u0014\u001a\u00020\u0003¢\u0006\u0002\u0010\u0015J\t\u0010(\u001a\u00020\u0003HÆ\u0003J\t\u0010)\u001a\u00020\u000fHÆ\u0003J\t\u0010*\u001a\u00020\u0003HÆ\u0003J\t\u0010+\u001a\u00020\u0003HÆ\u0003J\t\u0010,\u001a\u00020\u0013HÆ\u0003J\t\u0010-\u001a\u00020\u0003HÆ\u0003J\t\u0010.\u001a\u00020\u0005HÆ\u0003J\t\u0010/\u001a\u00020\u0003HÆ\u0003J\t\u00100\u001a\u00020\u0003HÆ\u0003J\t\u00101\u001a\u00020\u0003HÆ\u0003J\t\u00102\u001a\u00020\u0003HÆ\u0003J\t\u00103\u001a\u00020\u0003HÆ\u0003J\t\u00104\u001a\u00020\u0003HÆ\u0003J\t\u00105\u001a\u00020\rHÆ\u0003J\u0001\u00106\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u00032\b\b\u0002\u0010\u0007\u001a\u00020\u00032\b\b\u0002\u0010\b\u001a\u00020\u00032\b\b\u0002\u0010\t\u001a\u00020\u00032\b\b\u0002\u0010\n\u001a\u00020\u00032\b\b\u0002\u0010\u000b\u001a\u00020\u00032\b\b\u0002\u0010\f\u001a\u00020\r2\b\b\u0002\u0010\u000e\u001a\u00020\u000f2\b\b\u0002\u0010\u0010\u001a\u00020\u00032\b\b\u0002\u0010\u0011\u001a\u00020\u00032\b\b\u0002\u0010\u0012\u001a\u00020\u00132\b\b\u0002\u0010\u0014\u001a\u00020\u0003HÆ\u0001J\u0013\u00107\u001a\u00020\u000f2\b\u00108\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u00109\u001a\u00020\rHÖ\u0001J\t\u0010:\u001a\u00020\u0003HÖ\u0001R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0016\u0010\u0017R\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0018\u0010\u0019R\u0011\u0010\u0006\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u001a\u0010\u0017R\u0011\u0010\u0007\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u001b\u0010\u0017R\u0011\u0010\b\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u001c\u0010\u0017R\u0011\u0010\t\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u001d\u0010\u0017R\u0011\u0010\n\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u001e\u0010\u0017R\u0011\u0010\u000b\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u001f\u0010\u0017R\u0011\u0010\f\u001a\u00020\r¢\u0006\b\n\u0000\u001a\u0004\b \u0010!R\u0011\u0010\u000e\u001a\u00020\u000f¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\"R\u0011\u0010\u0010\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b#\u0010\u0017R\u0011\u0010\u0011\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b$\u0010\u0017R\u0011\u0010\u0012\u001a\u00020\u0013¢\u0006\b\n\u0000\u001a\u0004\b%\u0010&R\u0011\u0010\u0014\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b'\u0010\u0017¨\u0006;"}, d2 = {"Lcom/iqonic/store/models/CustomerData;", "", "avatar_url", "", "billing", "Lcom/iqonic/store/models/Billing;", "date_created", "date_created_gmt", "date_modified", "date_modified_gmt", "email", "first_name", "id", "", "is_paying_customer", "", "last_name", "role", "shipping", "Lcom/iqonic/store/models/Shipping;", "username", "(Ljava/lang/String;Lcom/iqonic/store/models/Billing;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZLjava/lang/String;Ljava/lang/String;Lcom/iqonic/store/models/Shipping;Ljava/lang/String;)V", "getAvatar_url", "()Ljava/lang/String;", "getBilling", "()Lcom/iqonic/store/models/Billing;", "getDate_created", "getDate_created_gmt", "getDate_modified", "getDate_modified_gmt", "getEmail", "getFirst_name", "getId", "()I", "()Z", "getLast_name", "getRole", "getShipping", "()Lcom/iqonic/store/models/Shipping;", "getUsername", "component1", "component10", "component11", "component12", "component13", "component14", "component2", "component3", "component4", "component5", "component6", "component7", "component8", "component9", "copy", "equals", "other", "hashCode", "toString", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: BaseResponse.kt */
public final class CustomerData {
    private final String avatar_url;
    private final Billing billing;
    private final String date_created;
    private final String date_created_gmt;
    private final String date_modified;
    private final String date_modified_gmt;
    private final String email;
    private final String first_name;
    private final int id;
    private final boolean is_paying_customer;
    private final String last_name;
    private final String role;
    private final Shipping shipping;
    private final String username;

    public static /* synthetic */ CustomerData copy$default(CustomerData customerData, String str, Billing billing2, String str2, String str3, String str4, String str5, String str6, String str7, int i, boolean z, String str8, String str9, Shipping shipping2, String str10, int i2, Object obj) {
        CustomerData customerData2 = customerData;
        int i3 = i2;
        return customerData.copy((i3 & 1) != 0 ? customerData2.avatar_url : str, (i3 & 2) != 0 ? customerData2.billing : billing2, (i3 & 4) != 0 ? customerData2.date_created : str2, (i3 & 8) != 0 ? customerData2.date_created_gmt : str3, (i3 & 16) != 0 ? customerData2.date_modified : str4, (i3 & 32) != 0 ? customerData2.date_modified_gmt : str5, (i3 & 64) != 0 ? customerData2.email : str6, (i3 & 128) != 0 ? customerData2.first_name : str7, (i3 & 256) != 0 ? customerData2.id : i, (i3 & 512) != 0 ? customerData2.is_paying_customer : z, (i3 & 1024) != 0 ? customerData2.last_name : str8, (i3 & 2048) != 0 ? customerData2.role : str9, (i3 & 4096) != 0 ? customerData2.shipping : shipping2, (i3 & 8192) != 0 ? customerData2.username : str10);
    }

    public final String component1() {
        return this.avatar_url;
    }

    public final boolean component10() {
        return this.is_paying_customer;
    }

    public final String component11() {
        return this.last_name;
    }

    public final String component12() {
        return this.role;
    }

    public final Shipping component13() {
        return this.shipping;
    }

    public final String component14() {
        return this.username;
    }

    public final Billing component2() {
        return this.billing;
    }

    public final String component3() {
        return this.date_created;
    }

    public final String component4() {
        return this.date_created_gmt;
    }

    public final String component5() {
        return this.date_modified;
    }

    public final String component6() {
        return this.date_modified_gmt;
    }

    public final String component7() {
        return this.email;
    }

    public final String component8() {
        return this.first_name;
    }

    public final int component9() {
        return this.id;
    }

    public final CustomerData copy(String str, Billing billing2, String str2, String str3, String str4, String str5, String str6, String str7, int i, boolean z, String str8, String str9, Shipping shipping2, String str10) {
        String str11 = str;
        Intrinsics.checkParameterIsNotNull(str11, "avatar_url");
        Billing billing3 = billing2;
        Intrinsics.checkParameterIsNotNull(billing3, "billing");
        String str12 = str2;
        Intrinsics.checkParameterIsNotNull(str12, "date_created");
        String str13 = str3;
        Intrinsics.checkParameterIsNotNull(str13, "date_created_gmt");
        String str14 = str4;
        Intrinsics.checkParameterIsNotNull(str14, "date_modified");
        String str15 = str5;
        Intrinsics.checkParameterIsNotNull(str15, "date_modified_gmt");
        String str16 = str6;
        Intrinsics.checkParameterIsNotNull(str16, "email");
        String str17 = str7;
        Intrinsics.checkParameterIsNotNull(str17, "first_name");
        String str18 = str8;
        Intrinsics.checkParameterIsNotNull(str18, "last_name");
        String str19 = str9;
        Intrinsics.checkParameterIsNotNull(str19, "role");
        Shipping shipping3 = shipping2;
        Intrinsics.checkParameterIsNotNull(shipping3, FirebaseAnalytics.Param.SHIPPING);
        String str20 = str10;
        Intrinsics.checkParameterIsNotNull(str20, "username");
        return new CustomerData(str11, billing3, str12, str13, str14, str15, str16, str17, i, z, str18, str19, shipping3, str20);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof CustomerData)) {
            return false;
        }
        CustomerData customerData = (CustomerData) obj;
        return Intrinsics.areEqual((Object) this.avatar_url, (Object) customerData.avatar_url) && Intrinsics.areEqual((Object) this.billing, (Object) customerData.billing) && Intrinsics.areEqual((Object) this.date_created, (Object) customerData.date_created) && Intrinsics.areEqual((Object) this.date_created_gmt, (Object) customerData.date_created_gmt) && Intrinsics.areEqual((Object) this.date_modified, (Object) customerData.date_modified) && Intrinsics.areEqual((Object) this.date_modified_gmt, (Object) customerData.date_modified_gmt) && Intrinsics.areEqual((Object) this.email, (Object) customerData.email) && Intrinsics.areEqual((Object) this.first_name, (Object) customerData.first_name) && this.id == customerData.id && this.is_paying_customer == customerData.is_paying_customer && Intrinsics.areEqual((Object) this.last_name, (Object) customerData.last_name) && Intrinsics.areEqual((Object) this.role, (Object) customerData.role) && Intrinsics.areEqual((Object) this.shipping, (Object) customerData.shipping) && Intrinsics.areEqual((Object) this.username, (Object) customerData.username);
    }

    public int hashCode() {
        String str = this.avatar_url;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        Billing billing2 = this.billing;
        int hashCode2 = (hashCode + (billing2 != null ? billing2.hashCode() : 0)) * 31;
        String str2 = this.date_created;
        int hashCode3 = (hashCode2 + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.date_created_gmt;
        int hashCode4 = (hashCode3 + (str3 != null ? str3.hashCode() : 0)) * 31;
        String str4 = this.date_modified;
        int hashCode5 = (hashCode4 + (str4 != null ? str4.hashCode() : 0)) * 31;
        String str5 = this.date_modified_gmt;
        int hashCode6 = (hashCode5 + (str5 != null ? str5.hashCode() : 0)) * 31;
        String str6 = this.email;
        int hashCode7 = (hashCode6 + (str6 != null ? str6.hashCode() : 0)) * 31;
        String str7 = this.first_name;
        int hashCode8 = (((hashCode7 + (str7 != null ? str7.hashCode() : 0)) * 31) + this.id) * 31;
        boolean z = this.is_paying_customer;
        if (z) {
            z = true;
        }
        int i2 = (hashCode8 + (z ? 1 : 0)) * 31;
        String str8 = this.last_name;
        int hashCode9 = (i2 + (str8 != null ? str8.hashCode() : 0)) * 31;
        String str9 = this.role;
        int hashCode10 = (hashCode9 + (str9 != null ? str9.hashCode() : 0)) * 31;
        Shipping shipping2 = this.shipping;
        int hashCode11 = (hashCode10 + (shipping2 != null ? shipping2.hashCode() : 0)) * 31;
        String str10 = this.username;
        if (str10 != null) {
            i = str10.hashCode();
        }
        return hashCode11 + i;
    }

    public String toString() {
        return "CustomerData(avatar_url=" + this.avatar_url + ", billing=" + this.billing + ", date_created=" + this.date_created + ", date_created_gmt=" + this.date_created_gmt + ", date_modified=" + this.date_modified + ", date_modified_gmt=" + this.date_modified_gmt + ", email=" + this.email + ", first_name=" + this.first_name + ", id=" + this.id + ", is_paying_customer=" + this.is_paying_customer + ", last_name=" + this.last_name + ", role=" + this.role + ", shipping=" + this.shipping + ", username=" + this.username + ")";
    }

    public CustomerData(String str, Billing billing2, String str2, String str3, String str4, String str5, String str6, String str7, int i, boolean z, String str8, String str9, Shipping shipping2, String str10) {
        Intrinsics.checkParameterIsNotNull(str, "avatar_url");
        Intrinsics.checkParameterIsNotNull(billing2, "billing");
        Intrinsics.checkParameterIsNotNull(str2, "date_created");
        Intrinsics.checkParameterIsNotNull(str3, "date_created_gmt");
        Intrinsics.checkParameterIsNotNull(str4, "date_modified");
        Intrinsics.checkParameterIsNotNull(str5, "date_modified_gmt");
        Intrinsics.checkParameterIsNotNull(str6, "email");
        Intrinsics.checkParameterIsNotNull(str7, "first_name");
        Intrinsics.checkParameterIsNotNull(str8, "last_name");
        Intrinsics.checkParameterIsNotNull(str9, "role");
        Intrinsics.checkParameterIsNotNull(shipping2, FirebaseAnalytics.Param.SHIPPING);
        Intrinsics.checkParameterIsNotNull(str10, "username");
        this.avatar_url = str;
        this.billing = billing2;
        this.date_created = str2;
        this.date_created_gmt = str3;
        this.date_modified = str4;
        this.date_modified_gmt = str5;
        this.email = str6;
        this.first_name = str7;
        this.id = i;
        this.is_paying_customer = z;
        this.last_name = str8;
        this.role = str9;
        this.shipping = shipping2;
        this.username = str10;
    }

    public final String getAvatar_url() {
        return this.avatar_url;
    }

    public final Billing getBilling() {
        return this.billing;
    }

    public final String getDate_created() {
        return this.date_created;
    }

    public final String getDate_created_gmt() {
        return this.date_created_gmt;
    }

    public final String getDate_modified() {
        return this.date_modified;
    }

    public final String getDate_modified_gmt() {
        return this.date_modified_gmt;
    }

    public final String getEmail() {
        return this.email;
    }

    public final String getFirst_name() {
        return this.first_name;
    }

    public final int getId() {
        return this.id;
    }

    public final boolean is_paying_customer() {
        return this.is_paying_customer;
    }

    public final String getLast_name() {
        return this.last_name;
    }

    public final String getRole() {
        return this.role;
    }

    public final Shipping getShipping() {
        return this.shipping;
    }

    public final String getUsername() {
        return this.username;
    }
}
