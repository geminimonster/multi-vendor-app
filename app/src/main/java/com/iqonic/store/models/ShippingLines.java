package com.iqonic.store.models;

import java.io.Serializable;
import java.util.ArrayList;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u001d\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\b\u0018\u00002\u00020\u0001Bs\u0012\b\b\u0002\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0002\u0010\u0004\u001a\u00020\u0003\u0012\b\b\u0002\u0010\u0005\u001a\u00020\u0003\u0012\b\b\u0002\u0010\u0006\u001a\u00020\u0003\u0012\b\b\u0002\u0010\u0007\u001a\u00020\u0003\u0012\u001c\b\u0002\u0010\b\u001a\u0016\u0012\u0004\u0012\u00020\n\u0018\u00010\tj\n\u0012\u0004\u0012\u00020\n\u0018\u0001`\u000b\u0012\u001c\b\u0002\u0010\f\u001a\u0016\u0012\u0004\u0012\u00020\n\u0018\u00010\tj\n\u0012\u0004\u0012\u00020\n\u0018\u0001`\u000b¢\u0006\u0002\u0010\rJ\t\u0010 \u001a\u00020\u0003HÆ\u0003J\t\u0010!\u001a\u00020\u0003HÆ\u0003J\t\u0010\"\u001a\u00020\u0003HÆ\u0003J\t\u0010#\u001a\u00020\u0003HÆ\u0003J\t\u0010$\u001a\u00020\u0003HÆ\u0003J\u001d\u0010%\u001a\u0016\u0012\u0004\u0012\u00020\n\u0018\u00010\tj\n\u0012\u0004\u0012\u00020\n\u0018\u0001`\u000bHÆ\u0003J\u001d\u0010&\u001a\u0016\u0012\u0004\u0012\u00020\n\u0018\u00010\tj\n\u0012\u0004\u0012\u00020\n\u0018\u0001`\u000bHÆ\u0003Jw\u0010'\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00032\b\b\u0002\u0010\u0005\u001a\u00020\u00032\b\b\u0002\u0010\u0006\u001a\u00020\u00032\b\b\u0002\u0010\u0007\u001a\u00020\u00032\u001c\b\u0002\u0010\b\u001a\u0016\u0012\u0004\u0012\u00020\n\u0018\u00010\tj\n\u0012\u0004\u0012\u00020\n\u0018\u0001`\u000b2\u001c\b\u0002\u0010\f\u001a\u0016\u0012\u0004\u0012\u00020\n\u0018\u00010\tj\n\u0012\u0004\u0012\u00020\n\u0018\u0001`\u000bHÆ\u0001J\u0013\u0010(\u001a\u00020)2\b\u0010*\u001a\u0004\u0018\u00010\nHÖ\u0003J\t\u0010+\u001a\u00020,HÖ\u0001J\t\u0010-\u001a\u00020\u0003HÖ\u0001R\u001a\u0010\u0006\u001a\u00020\u0003X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u000e\u0010\u000f\"\u0004\b\u0010\u0010\u0011R.\u0010\f\u001a\u0016\u0012\u0004\u0012\u00020\n\u0018\u00010\tj\n\u0012\u0004\u0012\u00020\n\u0018\u0001`\u000bX\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0012\u0010\u0013\"\u0004\b\u0014\u0010\u0015R\u001a\u0010\u0002\u001a\u00020\u0003X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0016\u0010\u000f\"\u0004\b\u0017\u0010\u0011R\u001a\u0010\u0004\u001a\u00020\u0003X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0018\u0010\u000f\"\u0004\b\u0019\u0010\u0011R.\u0010\b\u001a\u0016\u0012\u0004\u0012\u00020\n\u0018\u00010\tj\n\u0012\u0004\u0012\u00020\n\u0018\u0001`\u000bX\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u001a\u0010\u0013\"\u0004\b\u001b\u0010\u0015R\u001a\u0010\u0005\u001a\u00020\u0003X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u001c\u0010\u000f\"\u0004\b\u001d\u0010\u0011R\u001a\u0010\u0007\u001a\u00020\u0003X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u001e\u0010\u000f\"\u0004\b\u001f\u0010\u0011¨\u0006."}, d2 = {"Lcom/iqonic/store/models/ShippingLines;", "Ljava/io/Serializable;", "method_id", "", "method_title", "total", "instance_id", "total_tax", "taxes", "Ljava/util/ArrayList;", "", "Lkotlin/collections/ArrayList;", "meta_data", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;)V", "getInstance_id", "()Ljava/lang/String;", "setInstance_id", "(Ljava/lang/String;)V", "getMeta_data", "()Ljava/util/ArrayList;", "setMeta_data", "(Ljava/util/ArrayList;)V", "getMethod_id", "setMethod_id", "getMethod_title", "setMethod_title", "getTaxes", "setTaxes", "getTotal", "setTotal", "getTotal_tax", "setTotal_tax", "component1", "component2", "component3", "component4", "component5", "component6", "component7", "copy", "equals", "", "other", "hashCode", "", "toString", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: OrderRequest.kt */
public final class ShippingLines implements Serializable {
    private String instance_id;
    private ArrayList<Object> meta_data;
    private String method_id;
    private String method_title;
    private ArrayList<Object> taxes;
    private String total;
    private String total_tax;

    public ShippingLines() {
        this((String) null, (String) null, (String) null, (String) null, (String) null, (ArrayList) null, (ArrayList) null, 127, (DefaultConstructorMarker) null);
    }

    public static /* synthetic */ ShippingLines copy$default(ShippingLines shippingLines, String str, String str2, String str3, String str4, String str5, ArrayList<Object> arrayList, ArrayList<Object> arrayList2, int i, Object obj) {
        if ((i & 1) != 0) {
            str = shippingLines.method_id;
        }
        if ((i & 2) != 0) {
            str2 = shippingLines.method_title;
        }
        String str6 = str2;
        if ((i & 4) != 0) {
            str3 = shippingLines.total;
        }
        String str7 = str3;
        if ((i & 8) != 0) {
            str4 = shippingLines.instance_id;
        }
        String str8 = str4;
        if ((i & 16) != 0) {
            str5 = shippingLines.total_tax;
        }
        String str9 = str5;
        if ((i & 32) != 0) {
            arrayList = shippingLines.taxes;
        }
        ArrayList<Object> arrayList3 = arrayList;
        if ((i & 64) != 0) {
            arrayList2 = shippingLines.meta_data;
        }
        return shippingLines.copy(str, str6, str7, str8, str9, arrayList3, arrayList2);
    }

    public final String component1() {
        return this.method_id;
    }

    public final String component2() {
        return this.method_title;
    }

    public final String component3() {
        return this.total;
    }

    public final String component4() {
        return this.instance_id;
    }

    public final String component5() {
        return this.total_tax;
    }

    public final ArrayList<Object> component6() {
        return this.taxes;
    }

    public final ArrayList<Object> component7() {
        return this.meta_data;
    }

    public final ShippingLines copy(String str, String str2, String str3, String str4, String str5, ArrayList<Object> arrayList, ArrayList<Object> arrayList2) {
        Intrinsics.checkParameterIsNotNull(str, "method_id");
        Intrinsics.checkParameterIsNotNull(str2, "method_title");
        Intrinsics.checkParameterIsNotNull(str3, "total");
        Intrinsics.checkParameterIsNotNull(str4, "instance_id");
        Intrinsics.checkParameterIsNotNull(str5, "total_tax");
        return new ShippingLines(str, str2, str3, str4, str5, arrayList, arrayList2);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ShippingLines)) {
            return false;
        }
        ShippingLines shippingLines = (ShippingLines) obj;
        return Intrinsics.areEqual((Object) this.method_id, (Object) shippingLines.method_id) && Intrinsics.areEqual((Object) this.method_title, (Object) shippingLines.method_title) && Intrinsics.areEqual((Object) this.total, (Object) shippingLines.total) && Intrinsics.areEqual((Object) this.instance_id, (Object) shippingLines.instance_id) && Intrinsics.areEqual((Object) this.total_tax, (Object) shippingLines.total_tax) && Intrinsics.areEqual((Object) this.taxes, (Object) shippingLines.taxes) && Intrinsics.areEqual((Object) this.meta_data, (Object) shippingLines.meta_data);
    }

    public int hashCode() {
        String str = this.method_id;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.method_title;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.total;
        int hashCode3 = (hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31;
        String str4 = this.instance_id;
        int hashCode4 = (hashCode3 + (str4 != null ? str4.hashCode() : 0)) * 31;
        String str5 = this.total_tax;
        int hashCode5 = (hashCode4 + (str5 != null ? str5.hashCode() : 0)) * 31;
        ArrayList<Object> arrayList = this.taxes;
        int hashCode6 = (hashCode5 + (arrayList != null ? arrayList.hashCode() : 0)) * 31;
        ArrayList<Object> arrayList2 = this.meta_data;
        if (arrayList2 != null) {
            i = arrayList2.hashCode();
        }
        return hashCode6 + i;
    }

    public String toString() {
        return "ShippingLines(method_id=" + this.method_id + ", method_title=" + this.method_title + ", total=" + this.total + ", instance_id=" + this.instance_id + ", total_tax=" + this.total_tax + ", taxes=" + this.taxes + ", meta_data=" + this.meta_data + ")";
    }

    public ShippingLines(String str, String str2, String str3, String str4, String str5, ArrayList<Object> arrayList, ArrayList<Object> arrayList2) {
        Intrinsics.checkParameterIsNotNull(str, "method_id");
        Intrinsics.checkParameterIsNotNull(str2, "method_title");
        Intrinsics.checkParameterIsNotNull(str3, "total");
        Intrinsics.checkParameterIsNotNull(str4, "instance_id");
        Intrinsics.checkParameterIsNotNull(str5, "total_tax");
        this.method_id = str;
        this.method_title = str2;
        this.total = str3;
        this.instance_id = str4;
        this.total_tax = str5;
        this.taxes = arrayList;
        this.meta_data = arrayList2;
    }

    public final String getMethod_id() {
        return this.method_id;
    }

    public final void setMethod_id(String str) {
        Intrinsics.checkParameterIsNotNull(str, "<set-?>");
        this.method_id = str;
    }

    public final String getMethod_title() {
        return this.method_title;
    }

    public final void setMethod_title(String str) {
        Intrinsics.checkParameterIsNotNull(str, "<set-?>");
        this.method_title = str;
    }

    public final String getTotal() {
        return this.total;
    }

    public final void setTotal(String str) {
        Intrinsics.checkParameterIsNotNull(str, "<set-?>");
        this.total = str;
    }

    public final String getInstance_id() {
        return this.instance_id;
    }

    public final void setInstance_id(String str) {
        Intrinsics.checkParameterIsNotNull(str, "<set-?>");
        this.instance_id = str;
    }

    public final String getTotal_tax() {
        return this.total_tax;
    }

    public final void setTotal_tax(String str) {
        Intrinsics.checkParameterIsNotNull(str, "<set-?>");
        this.total_tax = str;
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* synthetic */ ShippingLines(java.lang.String r6, java.lang.String r7, java.lang.String r8, java.lang.String r9, java.lang.String r10, java.util.ArrayList r11, java.util.ArrayList r12, int r13, kotlin.jvm.internal.DefaultConstructorMarker r14) {
        /*
            r5 = this;
            r14 = r13 & 1
            java.lang.String r0 = ""
            if (r14 == 0) goto L_0x0008
            r14 = r0
            goto L_0x0009
        L_0x0008:
            r14 = r6
        L_0x0009:
            r6 = r13 & 2
            if (r6 == 0) goto L_0x000f
            r1 = r0
            goto L_0x0010
        L_0x000f:
            r1 = r7
        L_0x0010:
            r6 = r13 & 4
            if (r6 == 0) goto L_0x0016
            r2 = r0
            goto L_0x0017
        L_0x0016:
            r2 = r8
        L_0x0017:
            r6 = r13 & 8
            if (r6 == 0) goto L_0x001d
            r3 = r0
            goto L_0x001e
        L_0x001d:
            r3 = r9
        L_0x001e:
            r6 = r13 & 16
            if (r6 == 0) goto L_0x0023
            goto L_0x0024
        L_0x0023:
            r0 = r10
        L_0x0024:
            r6 = r13 & 32
            r7 = 0
            if (r6 == 0) goto L_0x002c
            r11 = r7
            java.util.ArrayList r11 = (java.util.ArrayList) r11
        L_0x002c:
            r4 = r11
            r6 = r13 & 64
            if (r6 == 0) goto L_0x0034
            r12 = r7
            java.util.ArrayList r12 = (java.util.ArrayList) r12
        L_0x0034:
            r13 = r12
            r6 = r5
            r7 = r14
            r8 = r1
            r9 = r2
            r10 = r3
            r11 = r0
            r12 = r4
            r6.<init>(r7, r8, r9, r10, r11, r12, r13)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.iqonic.store.models.ShippingLines.<init>(java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.util.ArrayList, java.util.ArrayList, int, kotlin.jvm.internal.DefaultConstructorMarker):void");
    }

    public final ArrayList<Object> getTaxes() {
        return this.taxes;
    }

    public final void setTaxes(ArrayList<Object> arrayList) {
        this.taxes = arrayList;
    }

    public final ArrayList<Object> getMeta_data() {
        return this.meta_data;
    }

    public final void setMeta_data(ArrayList<Object> arrayList) {
        this.meta_data = arrayList;
    }
}
