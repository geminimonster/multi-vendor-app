package com.iqonic.store.models;

import java.util.List;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B/\u0012\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003\u0012\f\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00060\u0003\u0012\f\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\b0\u0003¢\u0006\u0002\u0010\tJ\u000f\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003HÆ\u0003J\u000f\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u00060\u0003HÆ\u0003J\u000f\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\b0\u0003HÆ\u0003J9\u0010\u0011\u001a\u00020\u00002\u000e\b\u0002\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u00032\u000e\b\u0002\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00060\u00032\u000e\b\u0002\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\b0\u0003HÆ\u0001J\u0013\u0010\u0012\u001a\u00020\u00132\b\u0010\u0014\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0015\u001a\u00020\u0016HÖ\u0001J\t\u0010\u0017\u001a\u00020\u0018HÖ\u0001R\u0017\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u0017\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00060\u0003¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\u000bR\u0017\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\b0\u0003¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000b¨\u0006\u0019"}, d2 = {"Lcom/iqonic/store/models/ReviewLinks;", "", "collection", "", "Lcom/iqonic/store/models/ReviewCollection;", "self", "Lcom/iqonic/store/models/ReviewSelf;", "up", "Lcom/iqonic/store/models/ReviewUp;", "(Ljava/util/List;Ljava/util/List;Ljava/util/List;)V", "getCollection", "()Ljava/util/List;", "getSelf", "getUp", "component1", "component2", "component3", "copy", "equals", "", "other", "hashCode", "", "toString", "", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: ProductReviewData.kt */
public final class ReviewLinks {
    private final List<ReviewCollection> collection;
    private final List<ReviewSelf> self;
    private final List<ReviewUp> up;

    public static /* synthetic */ ReviewLinks copy$default(ReviewLinks reviewLinks, List<ReviewCollection> list, List<ReviewSelf> list2, List<ReviewUp> list3, int i, Object obj) {
        if ((i & 1) != 0) {
            list = reviewLinks.collection;
        }
        if ((i & 2) != 0) {
            list2 = reviewLinks.self;
        }
        if ((i & 4) != 0) {
            list3 = reviewLinks.up;
        }
        return reviewLinks.copy(list, list2, list3);
    }

    public final List<ReviewCollection> component1() {
        return this.collection;
    }

    public final List<ReviewSelf> component2() {
        return this.self;
    }

    public final List<ReviewUp> component3() {
        return this.up;
    }

    public final ReviewLinks copy(List<ReviewCollection> list, List<ReviewSelf> list2, List<ReviewUp> list3) {
        Intrinsics.checkParameterIsNotNull(list, "collection");
        Intrinsics.checkParameterIsNotNull(list2, "self");
        Intrinsics.checkParameterIsNotNull(list3, "up");
        return new ReviewLinks(list, list2, list3);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ReviewLinks)) {
            return false;
        }
        ReviewLinks reviewLinks = (ReviewLinks) obj;
        return Intrinsics.areEqual((Object) this.collection, (Object) reviewLinks.collection) && Intrinsics.areEqual((Object) this.self, (Object) reviewLinks.self) && Intrinsics.areEqual((Object) this.up, (Object) reviewLinks.up);
    }

    public int hashCode() {
        List<ReviewCollection> list = this.collection;
        int i = 0;
        int hashCode = (list != null ? list.hashCode() : 0) * 31;
        List<ReviewSelf> list2 = this.self;
        int hashCode2 = (hashCode + (list2 != null ? list2.hashCode() : 0)) * 31;
        List<ReviewUp> list3 = this.up;
        if (list3 != null) {
            i = list3.hashCode();
        }
        return hashCode2 + i;
    }

    public String toString() {
        return "ReviewLinks(collection=" + this.collection + ", self=" + this.self + ", up=" + this.up + ")";
    }

    public ReviewLinks(List<ReviewCollection> list, List<ReviewSelf> list2, List<ReviewUp> list3) {
        Intrinsics.checkParameterIsNotNull(list, "collection");
        Intrinsics.checkParameterIsNotNull(list2, "self");
        Intrinsics.checkParameterIsNotNull(list3, "up");
        this.collection = list;
        this.self = list2;
        this.up = list3;
    }

    public final List<ReviewCollection> getCollection() {
        return this.collection;
    }

    public final List<ReviewSelf> getSelf() {
        return this.self;
    }

    public final List<ReviewUp> getUp() {
        return this.up;
    }
}
