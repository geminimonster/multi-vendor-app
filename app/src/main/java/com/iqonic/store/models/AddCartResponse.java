package com.iqonic.store.models;

import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\f\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\b\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0003¢\u0006\u0002\u0010\u0007J\t\u0010\r\u001a\u00020\u0003HÆ\u0003J\t\u0010\u000e\u001a\u00020\u0005HÆ\u0003J\t\u0010\u000f\u001a\u00020\u0003HÆ\u0003J'\u0010\u0010\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u0003HÆ\u0001J\u0013\u0010\u0011\u001a\u00020\u00122\b\u0010\u0013\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0014\u001a\u00020\u0015HÖ\u0001J\t\u0010\u0016\u001a\u00020\u0003HÖ\u0001R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u0011\u0010\u0006\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\t¨\u0006\u0017"}, d2 = {"Lcom/iqonic/store/models/AddCartResponse;", "", "code", "", "data", "Lcom/iqonic/store/models/AddCartData;", "message", "(Ljava/lang/String;Lcom/iqonic/store/models/AddCartData;Ljava/lang/String;)V", "getCode", "()Ljava/lang/String;", "getData", "()Lcom/iqonic/store/models/AddCartData;", "getMessage", "component1", "component2", "component3", "copy", "equals", "", "other", "hashCode", "", "toString", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: BaseResponse.kt */
public final class AddCartResponse {
    private final String code;
    private final AddCartData data;
    private final String message;

    public static /* synthetic */ AddCartResponse copy$default(AddCartResponse addCartResponse, String str, AddCartData addCartData, String str2, int i, Object obj) {
        if ((i & 1) != 0) {
            str = addCartResponse.code;
        }
        if ((i & 2) != 0) {
            addCartData = addCartResponse.data;
        }
        if ((i & 4) != 0) {
            str2 = addCartResponse.message;
        }
        return addCartResponse.copy(str, addCartData, str2);
    }

    public final String component1() {
        return this.code;
    }

    public final AddCartData component2() {
        return this.data;
    }

    public final String component3() {
        return this.message;
    }

    public final AddCartResponse copy(String str, AddCartData addCartData, String str2) {
        Intrinsics.checkParameterIsNotNull(str, "code");
        Intrinsics.checkParameterIsNotNull(addCartData, "data");
        Intrinsics.checkParameterIsNotNull(str2, "message");
        return new AddCartResponse(str, addCartData, str2);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof AddCartResponse)) {
            return false;
        }
        AddCartResponse addCartResponse = (AddCartResponse) obj;
        return Intrinsics.areEqual((Object) this.code, (Object) addCartResponse.code) && Intrinsics.areEqual((Object) this.data, (Object) addCartResponse.data) && Intrinsics.areEqual((Object) this.message, (Object) addCartResponse.message);
    }

    public int hashCode() {
        String str = this.code;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        AddCartData addCartData = this.data;
        int hashCode2 = (hashCode + (addCartData != null ? addCartData.hashCode() : 0)) * 31;
        String str2 = this.message;
        if (str2 != null) {
            i = str2.hashCode();
        }
        return hashCode2 + i;
    }

    public String toString() {
        return "AddCartResponse(code=" + this.code + ", data=" + this.data + ", message=" + this.message + ")";
    }

    public AddCartResponse(String str, AddCartData addCartData, String str2) {
        Intrinsics.checkParameterIsNotNull(str, "code");
        Intrinsics.checkParameterIsNotNull(addCartData, "data");
        Intrinsics.checkParameterIsNotNull(str2, "message");
        this.code = str;
        this.data = addCartData;
        this.message = str2;
    }

    public final String getCode() {
        return this.code;
    }

    public final AddCartData getData() {
        return this.data;
    }

    public final String getMessage() {
        return this.message;
    }
}
