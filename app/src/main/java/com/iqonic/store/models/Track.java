package com.iqonic.store.models;

import kotlin.Metadata;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\b\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002R\u001c\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\bR\u001e\u0010\t\u001a\u0004\u0018\u00010\nX\u000e¢\u0006\u0010\n\u0002\u0010\u000e\u001a\u0004\b\t\u0010\u000b\"\u0004\b\f\u0010\rR\u001e\u0010\u000f\u001a\u0004\u0018\u00010\nX\u000e¢\u0006\u0010\n\u0002\u0010\u000e\u001a\u0004\b\u000f\u0010\u000b\"\u0004\b\u0010\u0010\rR\u001c\u0010\u0011\u001a\u0004\u0018\u00010\u0012X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0013\u0010\u0014\"\u0004\b\u0015\u0010\u0016R\u001c\u0010\u0017\u001a\u0004\u0018\u00010\u0004X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0018\u0010\u0006\"\u0004\b\u0019\u0010\b¨\u0006\u001a"}, d2 = {"Lcom/iqonic/store/models/Track;", "", "()V", "date", "", "getDate", "()Ljava/lang/String;", "setDate", "(Ljava/lang/String;)V", "isActive", "", "()Ljava/lang/Boolean;", "setActive", "(Ljava/lang/Boolean;)V", "Ljava/lang/Boolean;", "isDone", "setDone", "status", "Lcom/iqonic/store/models/TrackStatus;", "getStatus", "()Lcom/iqonic/store/models/TrackStatus;", "setStatus", "(Lcom/iqonic/store/models/TrackStatus;)V", "trackStatus", "getTrackStatus", "setTrackStatus", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: Track.kt */
public final class Track {
    private String date;
    private Boolean isActive = false;
    private Boolean isDone = true;
    private TrackStatus status;
    private String trackStatus;

    public final TrackStatus getStatus() {
        return this.status;
    }

    public final void setStatus(TrackStatus trackStatus2) {
        this.status = trackStatus2;
    }

    public final String getTrackStatus() {
        return this.trackStatus;
    }

    public final void setTrackStatus(String str) {
        this.trackStatus = str;
    }

    public final String getDate() {
        return this.date;
    }

    public final void setDate(String str) {
        this.date = str;
    }

    public final Boolean isActive() {
        return this.isActive;
    }

    public final void setActive(Boolean bool) {
        this.isActive = bool;
    }

    public final Boolean isDone() {
        return this.isDone;
    }

    public final void setDone(Boolean bool) {
        this.isDone = bool;
    }
}
