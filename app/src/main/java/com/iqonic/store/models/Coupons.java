package com.iqonic.store.models;

import java.io.Serializable;
import java.util.List;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\n\n\u0002\u0010 \n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0010\b\n\u0002\bP\b\b\u0018\u00002\u00020\u0001B\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0005\u0012\u0006\u0010\u0007\u001a\u00020\u0005\u0012\u0006\u0010\b\u001a\u00020\u0005\u0012\u0006\u0010\t\u001a\u00020\u0005\u0012\u0006\u0010\n\u001a\u00020\u0005\u0012\u0006\u0010\u000b\u001a\u00020\u0005\u0012\u0006\u0010\f\u001a\u00020\u0005\u0012\u0006\u0010\r\u001a\u00020\u0005\u0012\u0006\u0010\u000e\u001a\u00020\u0005\u0012\f\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u00110\u0010\u0012\u0006\u0010\u0012\u001a\u00020\u0013\u0012\f\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u00110\u0010\u0012\f\u0010\u0015\u001a\b\u0012\u0004\u0012\u00020\u00110\u0010\u0012\u0006\u0010\u0016\u001a\u00020\u0013\u0012\u0006\u0010\u0017\u001a\u00020\u0018\u0012\u0006\u0010\u0019\u001a\u00020\u0013\u0012\u0006\u0010\u001a\u001a\u00020\u0011\u0012\u0006\u0010\u001b\u001a\u00020\u0005\u0012\f\u0010\u001c\u001a\b\u0012\u0004\u0012\u00020\u00110\u0010\u0012\u0006\u0010\u001d\u001a\u00020\u0005\u0012\f\u0010\u001e\u001a\b\u0012\u0004\u0012\u00020\u00110\u0010\u0012\f\u0010\u001f\u001a\b\u0012\u0004\u0012\u00020\u00110\u0010\u0012\b\b\u0002\u0010 \u001a\u00020\u0018\u0012\b\b\u0002\u0010!\u001a\u00020\u0018\u0012\u0006\u0010\"\u001a\u00020\u0011\u0012\f\u0010#\u001a\b\u0012\u0004\u0012\u00020\u00110\u0010¢\u0006\u0002\u0010$J\t\u0010G\u001a\u00020\u0003HÆ\u0003J\t\u0010H\u001a\u00020\u0005HÆ\u0003J\t\u0010I\u001a\u00020\u0005HÆ\u0003J\u000f\u0010J\u001a\b\u0012\u0004\u0012\u00020\u00110\u0010HÆ\u0003J\t\u0010K\u001a\u00020\u0013HÆ\u0003J\u000f\u0010L\u001a\b\u0012\u0004\u0012\u00020\u00110\u0010HÆ\u0003J\u000f\u0010M\u001a\b\u0012\u0004\u0012\u00020\u00110\u0010HÆ\u0003J\t\u0010N\u001a\u00020\u0013HÆ\u0003J\t\u0010O\u001a\u00020\u0018HÆ\u0003J\t\u0010P\u001a\u00020\u0013HÆ\u0003J\t\u0010Q\u001a\u00020\u0011HÆ\u0003J\t\u0010R\u001a\u00020\u0005HÆ\u0003J\t\u0010S\u001a\u00020\u0005HÆ\u0003J\u000f\u0010T\u001a\b\u0012\u0004\u0012\u00020\u00110\u0010HÆ\u0003J\t\u0010U\u001a\u00020\u0005HÆ\u0003J\u000f\u0010V\u001a\b\u0012\u0004\u0012\u00020\u00110\u0010HÆ\u0003J\u000f\u0010W\u001a\b\u0012\u0004\u0012\u00020\u00110\u0010HÆ\u0003J\t\u0010X\u001a\u00020\u0018HÆ\u0003J\t\u0010Y\u001a\u00020\u0018HÆ\u0003J\t\u0010Z\u001a\u00020\u0011HÆ\u0003J\u000f\u0010[\u001a\b\u0012\u0004\u0012\u00020\u00110\u0010HÆ\u0003J\t\u0010\\\u001a\u00020\u0005HÆ\u0003J\t\u0010]\u001a\u00020\u0005HÆ\u0003J\t\u0010^\u001a\u00020\u0005HÆ\u0003J\t\u0010_\u001a\u00020\u0005HÆ\u0003J\t\u0010`\u001a\u00020\u0005HÆ\u0003J\t\u0010a\u001a\u00020\u0005HÆ\u0003J\t\u0010b\u001a\u00020\u0005HÆ\u0003JË\u0002\u0010c\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u00052\b\b\u0002\u0010\u0007\u001a\u00020\u00052\b\b\u0002\u0010\b\u001a\u00020\u00052\b\b\u0002\u0010\t\u001a\u00020\u00052\b\b\u0002\u0010\n\u001a\u00020\u00052\b\b\u0002\u0010\u000b\u001a\u00020\u00052\b\b\u0002\u0010\f\u001a\u00020\u00052\b\b\u0002\u0010\r\u001a\u00020\u00052\b\b\u0002\u0010\u000e\u001a\u00020\u00052\u000e\b\u0002\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u00110\u00102\b\b\u0002\u0010\u0012\u001a\u00020\u00132\u000e\b\u0002\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u00110\u00102\u000e\b\u0002\u0010\u0015\u001a\b\u0012\u0004\u0012\u00020\u00110\u00102\b\b\u0002\u0010\u0016\u001a\u00020\u00132\b\b\u0002\u0010\u0017\u001a\u00020\u00182\b\b\u0002\u0010\u0019\u001a\u00020\u00132\b\b\u0002\u0010\u001a\u001a\u00020\u00112\b\b\u0002\u0010\u001b\u001a\u00020\u00052\u000e\b\u0002\u0010\u001c\u001a\b\u0012\u0004\u0012\u00020\u00110\u00102\b\b\u0002\u0010\u001d\u001a\u00020\u00052\u000e\b\u0002\u0010\u001e\u001a\b\u0012\u0004\u0012\u00020\u00110\u00102\u000e\b\u0002\u0010\u001f\u001a\b\u0012\u0004\u0012\u00020\u00110\u00102\b\b\u0002\u0010 \u001a\u00020\u00182\b\b\u0002\u0010!\u001a\u00020\u00182\b\b\u0002\u0010\"\u001a\u00020\u00112\u000e\b\u0002\u0010#\u001a\b\u0012\u0004\u0012\u00020\u00110\u0010HÆ\u0001J\u0013\u0010d\u001a\u00020\u00132\b\u0010e\u001a\u0004\u0018\u00010\u0011HÖ\u0003J\t\u0010f\u001a\u00020\u0018HÖ\u0001J\t\u0010g\u001a\u00020\u0005HÖ\u0001R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b%\u0010&R\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b'\u0010(R\u0011\u0010\u0006\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b)\u0010(R\u0011\u0010\u0007\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b*\u0010(R\u0011\u0010\b\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b+\u0010(R\u0011\u0010\t\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b,\u0010(R\u0011\u0010\n\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b-\u0010(R\u0011\u0010\u000b\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b.\u0010(R\u0011\u0010\f\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b/\u0010(R\u0011\u0010\r\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b0\u0010(R\u0011\u0010\u000e\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b1\u0010(R\u0017\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u00110\u0010¢\u0006\b\n\u0000\u001a\u0004\b2\u00103R\u0011\u0010\u0012\u001a\u00020\u0013¢\u0006\b\n\u0000\u001a\u0004\b4\u00105R\u0017\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u00110\u0010¢\u0006\b\n\u0000\u001a\u0004\b6\u00103R\u0017\u0010\u0015\u001a\b\u0012\u0004\u0012\u00020\u00110\u0010¢\u0006\b\n\u0000\u001a\u0004\b7\u00103R\u0011\u0010\u0016\u001a\u00020\u0013¢\u0006\b\n\u0000\u001a\u0004\b8\u00105R\u0011\u0010\u0017\u001a\u00020\u0018¢\u0006\b\n\u0000\u001a\u0004\b9\u0010:R\u0011\u0010\u0019\u001a\u00020\u0013¢\u0006\b\n\u0000\u001a\u0004\b;\u00105R\u0011\u0010\u001a\u001a\u00020\u0011¢\u0006\b\n\u0000\u001a\u0004\b<\u0010=R\u0011\u0010\u001b\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b>\u0010(R\u0017\u0010\u001c\u001a\b\u0012\u0004\u0012\u00020\u00110\u0010¢\u0006\b\n\u0000\u001a\u0004\b?\u00103R\u0011\u0010\u001d\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b@\u0010(R\u0017\u0010\u001e\u001a\b\u0012\u0004\u0012\u00020\u00110\u0010¢\u0006\b\n\u0000\u001a\u0004\bA\u00103R\u0017\u0010\u001f\u001a\b\u0012\u0004\u0012\u00020\u00110\u0010¢\u0006\b\n\u0000\u001a\u0004\bB\u00103R\u0011\u0010 \u001a\u00020\u0018¢\u0006\b\n\u0000\u001a\u0004\bC\u0010:R\u0011\u0010!\u001a\u00020\u0018¢\u0006\b\n\u0000\u001a\u0004\bD\u0010:R\u0011\u0010\"\u001a\u00020\u0011¢\u0006\b\n\u0000\u001a\u0004\bE\u0010=R\u0017\u0010#\u001a\b\u0012\u0004\u0012\u00020\u00110\u0010¢\u0006\b\n\u0000\u001a\u0004\bF\u00103¨\u0006h"}, d2 = {"Lcom/iqonic/store/models/Coupons;", "Ljava/io/Serializable;", "_links", "Lcom/iqonic/store/models/CouponsLinks;", "amount", "", "code", "date_created", "date_created_gmt", "date_expires", "date_expires_gmt", "date_modified", "date_modified_gmt", "description", "discount_type", "email_restrictions", "", "", "exclude_sale_items", "", "excluded_product_categories", "excluded_product_ids", "free_shipping", "id", "", "individual_use", "limit_usage_to_x_items", "maximum_amount", "meta_data", "minimum_amount", "product_categories", "product_ids", "usage_count", "usage_limit", "usage_limit_per_user", "used_by", "(Lcom/iqonic/store/models/CouponsLinks;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;ZLjava/util/List;Ljava/util/List;ZIZLjava/lang/Object;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/util/List;Ljava/util/List;IILjava/lang/Object;Ljava/util/List;)V", "get_links", "()Lcom/iqonic/store/models/CouponsLinks;", "getAmount", "()Ljava/lang/String;", "getCode", "getDate_created", "getDate_created_gmt", "getDate_expires", "getDate_expires_gmt", "getDate_modified", "getDate_modified_gmt", "getDescription", "getDiscount_type", "getEmail_restrictions", "()Ljava/util/List;", "getExclude_sale_items", "()Z", "getExcluded_product_categories", "getExcluded_product_ids", "getFree_shipping", "getId", "()I", "getIndividual_use", "getLimit_usage_to_x_items", "()Ljava/lang/Object;", "getMaximum_amount", "getMeta_data", "getMinimum_amount", "getProduct_categories", "getProduct_ids", "getUsage_count", "getUsage_limit", "getUsage_limit_per_user", "getUsed_by", "component1", "component10", "component11", "component12", "component13", "component14", "component15", "component16", "component17", "component18", "component19", "component2", "component20", "component21", "component22", "component23", "component24", "component25", "component26", "component27", "component28", "component3", "component4", "component5", "component6", "component7", "component8", "component9", "copy", "equals", "other", "hashCode", "toString", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: Coupons.kt */
public final class Coupons implements Serializable {
    private final CouponsLinks _links;
    private final String amount;
    private final String code;
    private final String date_created;
    private final String date_created_gmt;
    private final String date_expires;
    private final String date_expires_gmt;
    private final String date_modified;
    private final String date_modified_gmt;
    private final String description;
    private final String discount_type;
    private final List<Object> email_restrictions;
    private final boolean exclude_sale_items;
    private final List<Object> excluded_product_categories;
    private final List<Object> excluded_product_ids;
    private final boolean free_shipping;
    private final int id;
    private final boolean individual_use;
    private final Object limit_usage_to_x_items;
    private final String maximum_amount;
    private final List<Object> meta_data;
    private final String minimum_amount;
    private final List<Object> product_categories;
    private final List<Object> product_ids;
    private final int usage_count;
    private final int usage_limit;
    private final Object usage_limit_per_user;
    private final List<Object> used_by;

    public static /* synthetic */ Coupons copy$default(Coupons coupons, CouponsLinks couponsLinks, String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, String str9, String str10, List list, boolean z, List list2, List list3, boolean z2, int i, boolean z3, Object obj, String str11, List list4, String str12, List list5, List list6, int i2, int i3, Object obj2, List list7, int i4, Object obj3) {
        Coupons coupons2 = coupons;
        int i5 = i4;
        return coupons.copy((i5 & 1) != 0 ? coupons2._links : couponsLinks, (i5 & 2) != 0 ? coupons2.amount : str, (i5 & 4) != 0 ? coupons2.code : str2, (i5 & 8) != 0 ? coupons2.date_created : str3, (i5 & 16) != 0 ? coupons2.date_created_gmt : str4, (i5 & 32) != 0 ? coupons2.date_expires : str5, (i5 & 64) != 0 ? coupons2.date_expires_gmt : str6, (i5 & 128) != 0 ? coupons2.date_modified : str7, (i5 & 256) != 0 ? coupons2.date_modified_gmt : str8, (i5 & 512) != 0 ? coupons2.description : str9, (i5 & 1024) != 0 ? coupons2.discount_type : str10, (i5 & 2048) != 0 ? coupons2.email_restrictions : list, (i5 & 4096) != 0 ? coupons2.exclude_sale_items : z, (i5 & 8192) != 0 ? coupons2.excluded_product_categories : list2, (i5 & 16384) != 0 ? coupons2.excluded_product_ids : list3, (i5 & 32768) != 0 ? coupons2.free_shipping : z2, (i5 & 65536) != 0 ? coupons2.id : i, (i5 & 131072) != 0 ? coupons2.individual_use : z3, (i5 & 262144) != 0 ? coupons2.limit_usage_to_x_items : obj, (i5 & 524288) != 0 ? coupons2.maximum_amount : str11, (i5 & 1048576) != 0 ? coupons2.meta_data : list4, (i5 & 2097152) != 0 ? coupons2.minimum_amount : str12, (i5 & 4194304) != 0 ? coupons2.product_categories : list5, (i5 & 8388608) != 0 ? coupons2.product_ids : list6, (i5 & 16777216) != 0 ? coupons2.usage_count : i2, (i5 & 33554432) != 0 ? coupons2.usage_limit : i3, (i5 & 67108864) != 0 ? coupons2.usage_limit_per_user : obj2, (i5 & 134217728) != 0 ? coupons2.used_by : list7);
    }

    public final CouponsLinks component1() {
        return this._links;
    }

    public final String component10() {
        return this.description;
    }

    public final String component11() {
        return this.discount_type;
    }

    public final List<Object> component12() {
        return this.email_restrictions;
    }

    public final boolean component13() {
        return this.exclude_sale_items;
    }

    public final List<Object> component14() {
        return this.excluded_product_categories;
    }

    public final List<Object> component15() {
        return this.excluded_product_ids;
    }

    public final boolean component16() {
        return this.free_shipping;
    }

    public final int component17() {
        return this.id;
    }

    public final boolean component18() {
        return this.individual_use;
    }

    public final Object component19() {
        return this.limit_usage_to_x_items;
    }

    public final String component2() {
        return this.amount;
    }

    public final String component20() {
        return this.maximum_amount;
    }

    public final List<Object> component21() {
        return this.meta_data;
    }

    public final String component22() {
        return this.minimum_amount;
    }

    public final List<Object> component23() {
        return this.product_categories;
    }

    public final List<Object> component24() {
        return this.product_ids;
    }

    public final int component25() {
        return this.usage_count;
    }

    public final int component26() {
        return this.usage_limit;
    }

    public final Object component27() {
        return this.usage_limit_per_user;
    }

    public final List<Object> component28() {
        return this.used_by;
    }

    public final String component3() {
        return this.code;
    }

    public final String component4() {
        return this.date_created;
    }

    public final String component5() {
        return this.date_created_gmt;
    }

    public final String component6() {
        return this.date_expires;
    }

    public final String component7() {
        return this.date_expires_gmt;
    }

    public final String component8() {
        return this.date_modified;
    }

    public final String component9() {
        return this.date_modified_gmt;
    }

    public final Coupons copy(CouponsLinks couponsLinks, String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, String str9, String str10, List<? extends Object> list, boolean z, List<? extends Object> list2, List<? extends Object> list3, boolean z2, int i, boolean z3, Object obj, String str11, List<? extends Object> list4, String str12, List<? extends Object> list5, List<? extends Object> list6, int i2, int i3, Object obj2, List<? extends Object> list7) {
        CouponsLinks couponsLinks2 = couponsLinks;
        Intrinsics.checkParameterIsNotNull(couponsLinks2, "_links");
        Intrinsics.checkParameterIsNotNull(str, "amount");
        Intrinsics.checkParameterIsNotNull(str2, "code");
        Intrinsics.checkParameterIsNotNull(str3, "date_created");
        Intrinsics.checkParameterIsNotNull(str4, "date_created_gmt");
        Intrinsics.checkParameterIsNotNull(str5, "date_expires");
        Intrinsics.checkParameterIsNotNull(str6, "date_expires_gmt");
        Intrinsics.checkParameterIsNotNull(str7, "date_modified");
        Intrinsics.checkParameterIsNotNull(str8, "date_modified_gmt");
        Intrinsics.checkParameterIsNotNull(str9, "description");
        Intrinsics.checkParameterIsNotNull(str10, "discount_type");
        Intrinsics.checkParameterIsNotNull(list, "email_restrictions");
        Intrinsics.checkParameterIsNotNull(list2, "excluded_product_categories");
        Intrinsics.checkParameterIsNotNull(list3, "excluded_product_ids");
        Intrinsics.checkParameterIsNotNull(obj, "limit_usage_to_x_items");
        Intrinsics.checkParameterIsNotNull(str11, "maximum_amount");
        Intrinsics.checkParameterIsNotNull(list4, "meta_data");
        Intrinsics.checkParameterIsNotNull(str12, "minimum_amount");
        Intrinsics.checkParameterIsNotNull(list5, "product_categories");
        Intrinsics.checkParameterIsNotNull(list6, "product_ids");
        Intrinsics.checkParameterIsNotNull(obj2, "usage_limit_per_user");
        Intrinsics.checkParameterIsNotNull(list7, "used_by");
        return new Coupons(couponsLinks2, str, str2, str3, str4, str5, str6, str7, str8, str9, str10, list, z, list2, list3, z2, i, z3, obj, str11, list4, str12, list5, list6, i2, i3, obj2, list7);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Coupons)) {
            return false;
        }
        Coupons coupons = (Coupons) obj;
        return Intrinsics.areEqual((Object) this._links, (Object) coupons._links) && Intrinsics.areEqual((Object) this.amount, (Object) coupons.amount) && Intrinsics.areEqual((Object) this.code, (Object) coupons.code) && Intrinsics.areEqual((Object) this.date_created, (Object) coupons.date_created) && Intrinsics.areEqual((Object) this.date_created_gmt, (Object) coupons.date_created_gmt) && Intrinsics.areEqual((Object) this.date_expires, (Object) coupons.date_expires) && Intrinsics.areEqual((Object) this.date_expires_gmt, (Object) coupons.date_expires_gmt) && Intrinsics.areEqual((Object) this.date_modified, (Object) coupons.date_modified) && Intrinsics.areEqual((Object) this.date_modified_gmt, (Object) coupons.date_modified_gmt) && Intrinsics.areEqual((Object) this.description, (Object) coupons.description) && Intrinsics.areEqual((Object) this.discount_type, (Object) coupons.discount_type) && Intrinsics.areEqual((Object) this.email_restrictions, (Object) coupons.email_restrictions) && this.exclude_sale_items == coupons.exclude_sale_items && Intrinsics.areEqual((Object) this.excluded_product_categories, (Object) coupons.excluded_product_categories) && Intrinsics.areEqual((Object) this.excluded_product_ids, (Object) coupons.excluded_product_ids) && this.free_shipping == coupons.free_shipping && this.id == coupons.id && this.individual_use == coupons.individual_use && Intrinsics.areEqual(this.limit_usage_to_x_items, coupons.limit_usage_to_x_items) && Intrinsics.areEqual((Object) this.maximum_amount, (Object) coupons.maximum_amount) && Intrinsics.areEqual((Object) this.meta_data, (Object) coupons.meta_data) && Intrinsics.areEqual((Object) this.minimum_amount, (Object) coupons.minimum_amount) && Intrinsics.areEqual((Object) this.product_categories, (Object) coupons.product_categories) && Intrinsics.areEqual((Object) this.product_ids, (Object) coupons.product_ids) && this.usage_count == coupons.usage_count && this.usage_limit == coupons.usage_limit && Intrinsics.areEqual(this.usage_limit_per_user, coupons.usage_limit_per_user) && Intrinsics.areEqual((Object) this.used_by, (Object) coupons.used_by);
    }

    public int hashCode() {
        CouponsLinks couponsLinks = this._links;
        int i = 0;
        int hashCode = (couponsLinks != null ? couponsLinks.hashCode() : 0) * 31;
        String str = this.amount;
        int hashCode2 = (hashCode + (str != null ? str.hashCode() : 0)) * 31;
        String str2 = this.code;
        int hashCode3 = (hashCode2 + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.date_created;
        int hashCode4 = (hashCode3 + (str3 != null ? str3.hashCode() : 0)) * 31;
        String str4 = this.date_created_gmt;
        int hashCode5 = (hashCode4 + (str4 != null ? str4.hashCode() : 0)) * 31;
        String str5 = this.date_expires;
        int hashCode6 = (hashCode5 + (str5 != null ? str5.hashCode() : 0)) * 31;
        String str6 = this.date_expires_gmt;
        int hashCode7 = (hashCode6 + (str6 != null ? str6.hashCode() : 0)) * 31;
        String str7 = this.date_modified;
        int hashCode8 = (hashCode7 + (str7 != null ? str7.hashCode() : 0)) * 31;
        String str8 = this.date_modified_gmt;
        int hashCode9 = (hashCode8 + (str8 != null ? str8.hashCode() : 0)) * 31;
        String str9 = this.description;
        int hashCode10 = (hashCode9 + (str9 != null ? str9.hashCode() : 0)) * 31;
        String str10 = this.discount_type;
        int hashCode11 = (hashCode10 + (str10 != null ? str10.hashCode() : 0)) * 31;
        List<Object> list = this.email_restrictions;
        int hashCode12 = (hashCode11 + (list != null ? list.hashCode() : 0)) * 31;
        boolean z = this.exclude_sale_items;
        boolean z2 = true;
        if (z) {
            z = true;
        }
        int i2 = (hashCode12 + (z ? 1 : 0)) * 31;
        List<Object> list2 = this.excluded_product_categories;
        int hashCode13 = (i2 + (list2 != null ? list2.hashCode() : 0)) * 31;
        List<Object> list3 = this.excluded_product_ids;
        int hashCode14 = (hashCode13 + (list3 != null ? list3.hashCode() : 0)) * 31;
        boolean z3 = this.free_shipping;
        if (z3) {
            z3 = true;
        }
        int i3 = (((hashCode14 + (z3 ? 1 : 0)) * 31) + this.id) * 31;
        boolean z4 = this.individual_use;
        if (!z4) {
            z2 = z4;
        }
        int i4 = (i3 + (z2 ? 1 : 0)) * 31;
        Object obj = this.limit_usage_to_x_items;
        int hashCode15 = (i4 + (obj != null ? obj.hashCode() : 0)) * 31;
        String str11 = this.maximum_amount;
        int hashCode16 = (hashCode15 + (str11 != null ? str11.hashCode() : 0)) * 31;
        List<Object> list4 = this.meta_data;
        int hashCode17 = (hashCode16 + (list4 != null ? list4.hashCode() : 0)) * 31;
        String str12 = this.minimum_amount;
        int hashCode18 = (hashCode17 + (str12 != null ? str12.hashCode() : 0)) * 31;
        List<Object> list5 = this.product_categories;
        int hashCode19 = (hashCode18 + (list5 != null ? list5.hashCode() : 0)) * 31;
        List<Object> list6 = this.product_ids;
        int hashCode20 = (((((hashCode19 + (list6 != null ? list6.hashCode() : 0)) * 31) + this.usage_count) * 31) + this.usage_limit) * 31;
        Object obj2 = this.usage_limit_per_user;
        int hashCode21 = (hashCode20 + (obj2 != null ? obj2.hashCode() : 0)) * 31;
        List<Object> list7 = this.used_by;
        if (list7 != null) {
            i = list7.hashCode();
        }
        return hashCode21 + i;
    }

    public String toString() {
        return "Coupons(_links=" + this._links + ", amount=" + this.amount + ", code=" + this.code + ", date_created=" + this.date_created + ", date_created_gmt=" + this.date_created_gmt + ", date_expires=" + this.date_expires + ", date_expires_gmt=" + this.date_expires_gmt + ", date_modified=" + this.date_modified + ", date_modified_gmt=" + this.date_modified_gmt + ", description=" + this.description + ", discount_type=" + this.discount_type + ", email_restrictions=" + this.email_restrictions + ", exclude_sale_items=" + this.exclude_sale_items + ", excluded_product_categories=" + this.excluded_product_categories + ", excluded_product_ids=" + this.excluded_product_ids + ", free_shipping=" + this.free_shipping + ", id=" + this.id + ", individual_use=" + this.individual_use + ", limit_usage_to_x_items=" + this.limit_usage_to_x_items + ", maximum_amount=" + this.maximum_amount + ", meta_data=" + this.meta_data + ", minimum_amount=" + this.minimum_amount + ", product_categories=" + this.product_categories + ", product_ids=" + this.product_ids + ", usage_count=" + this.usage_count + ", usage_limit=" + this.usage_limit + ", usage_limit_per_user=" + this.usage_limit_per_user + ", used_by=" + this.used_by + ")";
    }

    public Coupons(CouponsLinks couponsLinks, String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, String str9, String str10, List<? extends Object> list, boolean z, List<? extends Object> list2, List<? extends Object> list3, boolean z2, int i, boolean z3, Object obj, String str11, List<? extends Object> list4, String str12, List<? extends Object> list5, List<? extends Object> list6, int i2, int i3, Object obj2, List<? extends Object> list7) {
        CouponsLinks couponsLinks2 = couponsLinks;
        String str13 = str;
        String str14 = str2;
        String str15 = str3;
        String str16 = str4;
        String str17 = str5;
        String str18 = str6;
        String str19 = str7;
        String str20 = str8;
        String str21 = str9;
        String str22 = str10;
        List<? extends Object> list8 = list;
        List<? extends Object> list9 = list2;
        List<? extends Object> list10 = list3;
        String str23 = str11;
        Intrinsics.checkParameterIsNotNull(couponsLinks2, "_links");
        Intrinsics.checkParameterIsNotNull(str13, "amount");
        Intrinsics.checkParameterIsNotNull(str14, "code");
        Intrinsics.checkParameterIsNotNull(str15, "date_created");
        Intrinsics.checkParameterIsNotNull(str16, "date_created_gmt");
        Intrinsics.checkParameterIsNotNull(str17, "date_expires");
        Intrinsics.checkParameterIsNotNull(str18, "date_expires_gmt");
        Intrinsics.checkParameterIsNotNull(str19, "date_modified");
        Intrinsics.checkParameterIsNotNull(str20, "date_modified_gmt");
        Intrinsics.checkParameterIsNotNull(str21, "description");
        Intrinsics.checkParameterIsNotNull(str22, "discount_type");
        Intrinsics.checkParameterIsNotNull(list8, "email_restrictions");
        Intrinsics.checkParameterIsNotNull(list9, "excluded_product_categories");
        Intrinsics.checkParameterIsNotNull(list10, "excluded_product_ids");
        Intrinsics.checkParameterIsNotNull(obj, "limit_usage_to_x_items");
        Intrinsics.checkParameterIsNotNull(str11, "maximum_amount");
        Intrinsics.checkParameterIsNotNull(list4, "meta_data");
        Intrinsics.checkParameterIsNotNull(str12, "minimum_amount");
        Intrinsics.checkParameterIsNotNull(list5, "product_categories");
        Intrinsics.checkParameterIsNotNull(list6, "product_ids");
        Intrinsics.checkParameterIsNotNull(obj2, "usage_limit_per_user");
        Intrinsics.checkParameterIsNotNull(list7, "used_by");
        this._links = couponsLinks2;
        this.amount = str13;
        this.code = str14;
        this.date_created = str15;
        this.date_created_gmt = str16;
        this.date_expires = str17;
        this.date_expires_gmt = str18;
        this.date_modified = str19;
        this.date_modified_gmt = str20;
        this.description = str21;
        this.discount_type = str22;
        this.email_restrictions = list8;
        this.exclude_sale_items = z;
        this.excluded_product_categories = list9;
        this.excluded_product_ids = list10;
        this.free_shipping = z2;
        this.id = i;
        this.individual_use = z3;
        this.limit_usage_to_x_items = obj;
        this.maximum_amount = str11;
        this.meta_data = list4;
        this.minimum_amount = str12;
        this.product_categories = list5;
        this.product_ids = list6;
        this.usage_count = i2;
        this.usage_limit = i3;
        this.usage_limit_per_user = obj2;
        this.used_by = list7;
    }

    public final CouponsLinks get_links() {
        return this._links;
    }

    public final String getAmount() {
        return this.amount;
    }

    public final String getCode() {
        return this.code;
    }

    public final String getDate_created() {
        return this.date_created;
    }

    public final String getDate_created_gmt() {
        return this.date_created_gmt;
    }

    public final String getDate_expires() {
        return this.date_expires;
    }

    public final String getDate_expires_gmt() {
        return this.date_expires_gmt;
    }

    public final String getDate_modified() {
        return this.date_modified;
    }

    public final String getDate_modified_gmt() {
        return this.date_modified_gmt;
    }

    public final String getDescription() {
        return this.description;
    }

    public final String getDiscount_type() {
        return this.discount_type;
    }

    public final List<Object> getEmail_restrictions() {
        return this.email_restrictions;
    }

    public final boolean getExclude_sale_items() {
        return this.exclude_sale_items;
    }

    public final List<Object> getExcluded_product_categories() {
        return this.excluded_product_categories;
    }

    public final List<Object> getExcluded_product_ids() {
        return this.excluded_product_ids;
    }

    public final boolean getFree_shipping() {
        return this.free_shipping;
    }

    public final int getId() {
        return this.id;
    }

    public final boolean getIndividual_use() {
        return this.individual_use;
    }

    public final Object getLimit_usage_to_x_items() {
        return this.limit_usage_to_x_items;
    }

    public final String getMaximum_amount() {
        return this.maximum_amount;
    }

    public final List<Object> getMeta_data() {
        return this.meta_data;
    }

    public final String getMinimum_amount() {
        return this.minimum_amount;
    }

    public final List<Object> getProduct_categories() {
        return this.product_categories;
    }

    public final List<Object> getProduct_ids() {
        return this.product_ids;
    }

    public final int getUsage_count() {
        return this.usage_count;
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ Coupons(CouponsLinks couponsLinks, String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, String str9, String str10, List list, boolean z, List list2, List list3, boolean z2, int i, boolean z3, Object obj, String str11, List list4, String str12, List list5, List list6, int i2, int i3, Object obj2, List list7, int i4, DefaultConstructorMarker defaultConstructorMarker) {
        this(couponsLinks, str, str2, str3, str4, str5, str6, str7, str8, str9, str10, list, z, list2, list3, z2, i, z3, obj, str11, list4, str12, list5, list6, (i4 & 16777216) != 0 ? 0 : i2, (i4 & 33554432) != 0 ? 0 : i3, obj2, list7);
    }

    public final int getUsage_limit() {
        return this.usage_limit;
    }

    public final Object getUsage_limit_per_user() {
        return this.usage_limit_per_user;
    }

    public final List<Object> getUsed_by() {
        return this.used_by;
    }
}
