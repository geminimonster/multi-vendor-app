package com.iqonic.store.models;

import com.iqonic.store.utils.Constants;
import java.util.List;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000B\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b)\n\u0002\u0010\b\n\u0002\b\u0002\b\b\u0018\u00002\u00020\u0001B³\u0001\u0012\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003\u0012\f\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00060\u0003\u0012\u0006\u0010\u0007\u001a\u00020\b\u0012\f\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00060\u0003\u0012\f\u0010\n\u001a\b\u0012\u0004\u0012\u00020\u00060\u0003\u0012\f\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\u00060\u0003\u0012\f\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u00060\u0003\u0012\f\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u00060\u0003\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u0012\f\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u00060\u0003\u0012\u0006\u0010\u0011\u001a\u00020\u0012\u0012\u0006\u0010\u0013\u001a\u00020\u0012\u0012\u0006\u0010\u0014\u001a\u00020\u0012\u0012\u0006\u0010\u0015\u001a\u00020\u0016\u0012\f\u0010\u0017\u001a\b\u0012\u0004\u0012\u00020\u00060\u0003¢\u0006\u0002\u0010\u0018J\u000f\u0010-\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003HÆ\u0003J\u000f\u0010.\u001a\b\u0012\u0004\u0012\u00020\u00060\u0003HÆ\u0003J\t\u0010/\u001a\u00020\u0012HÆ\u0003J\t\u00100\u001a\u00020\u0012HÆ\u0003J\t\u00101\u001a\u00020\u0012HÆ\u0003J\t\u00102\u001a\u00020\u0016HÆ\u0003J\u000f\u00103\u001a\b\u0012\u0004\u0012\u00020\u00060\u0003HÆ\u0003J\u000f\u00104\u001a\b\u0012\u0004\u0012\u00020\u00060\u0003HÆ\u0003J\t\u00105\u001a\u00020\bHÆ\u0003J\u000f\u00106\u001a\b\u0012\u0004\u0012\u00020\u00060\u0003HÆ\u0003J\u000f\u00107\u001a\b\u0012\u0004\u0012\u00020\u00060\u0003HÆ\u0003J\u000f\u00108\u001a\b\u0012\u0004\u0012\u00020\u00060\u0003HÆ\u0003J\u000f\u00109\u001a\b\u0012\u0004\u0012\u00020\u00060\u0003HÆ\u0003J\u000f\u0010:\u001a\b\u0012\u0004\u0012\u00020\u00060\u0003HÆ\u0003J\t\u0010;\u001a\u00020\u000fHÆ\u0003JÕ\u0001\u0010<\u001a\u00020\u00002\u000e\b\u0002\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u00032\u000e\b\u0002\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00060\u00032\b\b\u0002\u0010\u0007\u001a\u00020\b2\u000e\b\u0002\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00060\u00032\u000e\b\u0002\u0010\n\u001a\b\u0012\u0004\u0012\u00020\u00060\u00032\u000e\b\u0002\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\u00060\u00032\u000e\b\u0002\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u00060\u00032\u000e\b\u0002\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u00060\u00032\b\b\u0002\u0010\u000e\u001a\u00020\u000f2\u000e\b\u0002\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u00060\u00032\b\b\u0002\u0010\u0011\u001a\u00020\u00122\b\b\u0002\u0010\u0013\u001a\u00020\u00122\b\b\u0002\u0010\u0014\u001a\u00020\u00122\b\b\u0002\u0010\u0015\u001a\u00020\u00162\u000e\b\u0002\u0010\u0017\u001a\b\u0012\u0004\u0012\u00020\u00060\u0003HÆ\u0001J\u0013\u0010=\u001a\u00020\u00162\b\u0010>\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010?\u001a\u00020@HÖ\u0001J\t\u0010A\u001a\u00020\u0012HÖ\u0001R\u0011\u0010\u0014\u001a\u00020\u0012¢\u0006\b\n\u0000\u001a\u0004\b\u0019\u0010\u001aR\u0017\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u001b\u0010\u001cR\u0017\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00060\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u001d\u0010\u001cR\u0011\u0010\u0007\u001a\u00020\b¢\u0006\b\n\u0000\u001a\u0004\b\u001e\u0010\u001fR\u0017\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00060\u0003¢\u0006\b\n\u0000\u001a\u0004\b \u0010\u001cR\u0011\u0010\u0015\u001a\u00020\u0016¢\u0006\b\n\u0000\u001a\u0004\b!\u0010\"R\u0017\u0010\n\u001a\b\u0012\u0004\u0012\u00020\u00060\u0003¢\u0006\b\n\u0000\u001a\u0004\b#\u0010\u001cR\u0017\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\u00060\u0003¢\u0006\b\n\u0000\u001a\u0004\b$\u0010\u001cR\u0017\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u00060\u0003¢\u0006\b\n\u0000\u001a\u0004\b%\u0010\u001cR\u0011\u0010\u0013\u001a\u00020\u0012¢\u0006\b\n\u0000\u001a\u0004\b&\u0010\u001aR\u0017\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u00060\u0003¢\u0006\b\n\u0000\u001a\u0004\b'\u0010\u001cR\u0011\u0010\u000e\u001a\u00020\u000f¢\u0006\b\n\u0000\u001a\u0004\b(\u0010)R\u0017\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u00060\u0003¢\u0006\b\n\u0000\u001a\u0004\b*\u0010\u001cR\u0011\u0010\u0011\u001a\u00020\u0012¢\u0006\b\n\u0000\u001a\u0004\b+\u0010\u001aR\u0017\u0010\u0017\u001a\b\u0012\u0004\u0012\u00020\u00060\u0003¢\u0006\b\n\u0000\u001a\u0004\b,\u0010\u001c¨\u0006B"}, d2 = {"Lcom/iqonic/store/models/Dashboard;", "", "banner", "", "Lcom/iqonic/store/models/DashboardBanner;", "best_selling_product", "Lcom/iqonic/store/models/StoreProductModel;", "currency_symbol", "Lcom/iqonic/store/models/CurrencySymbol;", "deal_of_the_day", "featured", "newest", "offer", "sale_product", "social_link", "Lcom/iqonic/store/models/SocialLink;", "suggested_for_you", "theme_color", "", "payment_method", "app_lang", "enable_coupons", "", "you_may_like", "(Ljava/util/List;Ljava/util/List;Lcom/iqonic/store/models/CurrencySymbol;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Lcom/iqonic/store/models/SocialLink;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/util/List;)V", "getApp_lang", "()Ljava/lang/String;", "getBanner", "()Ljava/util/List;", "getBest_selling_product", "getCurrency_symbol", "()Lcom/iqonic/store/models/CurrencySymbol;", "getDeal_of_the_day", "getEnable_coupons", "()Z", "getFeatured", "getNewest", "getOffer", "getPayment_method", "getSale_product", "getSocial_link", "()Lcom/iqonic/store/models/SocialLink;", "getSuggested_for_you", "getTheme_color", "getYou_may_like", "component1", "component10", "component11", "component12", "component13", "component14", "component15", "component2", "component3", "component4", "component5", "component6", "component7", "component8", "component9", "copy", "equals", "other", "hashCode", "", "toString", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: Dashboard.kt */
public final class Dashboard {
    private final String app_lang;
    private final List<DashboardBanner> banner;
    private final List<StoreProductModel> best_selling_product;
    private final CurrencySymbol currency_symbol;
    private final List<StoreProductModel> deal_of_the_day;
    private final boolean enable_coupons;
    private final List<StoreProductModel> featured;
    private final List<StoreProductModel> newest;
    private final List<StoreProductModel> offer;
    private final String payment_method;
    private final List<StoreProductModel> sale_product;
    private final SocialLink social_link;
    private final List<StoreProductModel> suggested_for_you;
    private final String theme_color;
    private final List<StoreProductModel> you_may_like;

    public static /* synthetic */ Dashboard copy$default(Dashboard dashboard, List list, List list2, CurrencySymbol currencySymbol, List list3, List list4, List list5, List list6, List list7, SocialLink socialLink, List list8, String str, String str2, String str3, boolean z, List list9, int i, Object obj) {
        Dashboard dashboard2 = dashboard;
        int i2 = i;
        return dashboard.copy((i2 & 1) != 0 ? dashboard2.banner : list, (i2 & 2) != 0 ? dashboard2.best_selling_product : list2, (i2 & 4) != 0 ? dashboard2.currency_symbol : currencySymbol, (i2 & 8) != 0 ? dashboard2.deal_of_the_day : list3, (i2 & 16) != 0 ? dashboard2.featured : list4, (i2 & 32) != 0 ? dashboard2.newest : list5, (i2 & 64) != 0 ? dashboard2.offer : list6, (i2 & 128) != 0 ? dashboard2.sale_product : list7, (i2 & 256) != 0 ? dashboard2.social_link : socialLink, (i2 & 512) != 0 ? dashboard2.suggested_for_you : list8, (i2 & 1024) != 0 ? dashboard2.theme_color : str, (i2 & 2048) != 0 ? dashboard2.payment_method : str2, (i2 & 4096) != 0 ? dashboard2.app_lang : str3, (i2 & 8192) != 0 ? dashboard2.enable_coupons : z, (i2 & 16384) != 0 ? dashboard2.you_may_like : list9);
    }

    public final List<DashboardBanner> component1() {
        return this.banner;
    }

    public final List<StoreProductModel> component10() {
        return this.suggested_for_you;
    }

    public final String component11() {
        return this.theme_color;
    }

    public final String component12() {
        return this.payment_method;
    }

    public final String component13() {
        return this.app_lang;
    }

    public final boolean component14() {
        return this.enable_coupons;
    }

    public final List<StoreProductModel> component15() {
        return this.you_may_like;
    }

    public final List<StoreProductModel> component2() {
        return this.best_selling_product;
    }

    public final CurrencySymbol component3() {
        return this.currency_symbol;
    }

    public final List<StoreProductModel> component4() {
        return this.deal_of_the_day;
    }

    public final List<StoreProductModel> component5() {
        return this.featured;
    }

    public final List<StoreProductModel> component6() {
        return this.newest;
    }

    public final List<StoreProductModel> component7() {
        return this.offer;
    }

    public final List<StoreProductModel> component8() {
        return this.sale_product;
    }

    public final SocialLink component9() {
        return this.social_link;
    }

    public final Dashboard copy(List<DashboardBanner> list, List<StoreProductModel> list2, CurrencySymbol currencySymbol, List<StoreProductModel> list3, List<StoreProductModel> list4, List<StoreProductModel> list5, List<StoreProductModel> list6, List<StoreProductModel> list7, SocialLink socialLink, List<StoreProductModel> list8, String str, String str2, String str3, boolean z, List<StoreProductModel> list9) {
        List<DashboardBanner> list10 = list;
        Intrinsics.checkParameterIsNotNull(list10, "banner");
        List<StoreProductModel> list11 = list2;
        Intrinsics.checkParameterIsNotNull(list11, Constants.viewName.VIEW_BEST_SELLING);
        CurrencySymbol currencySymbol2 = currencySymbol;
        Intrinsics.checkParameterIsNotNull(currencySymbol2, "currency_symbol");
        List<StoreProductModel> list12 = list3;
        Intrinsics.checkParameterIsNotNull(list12, Constants.viewName.VIEW_DEAL_OF_THE_DAY);
        List<StoreProductModel> list13 = list4;
        Intrinsics.checkParameterIsNotNull(list13, "featured");
        List<StoreProductModel> list14 = list5;
        Intrinsics.checkParameterIsNotNull(list14, "newest");
        List<StoreProductModel> list15 = list6;
        Intrinsics.checkParameterIsNotNull(list15, Constants.viewName.VIEW_OFFER);
        List<StoreProductModel> list16 = list7;
        Intrinsics.checkParameterIsNotNull(list16, Constants.viewName.VIEW_SALE);
        SocialLink socialLink2 = socialLink;
        Intrinsics.checkParameterIsNotNull(socialLink2, "social_link");
        List<StoreProductModel> list17 = list8;
        Intrinsics.checkParameterIsNotNull(list17, Constants.viewName.VIEW_SUGGESTED_FOR_YOU);
        String str4 = str;
        Intrinsics.checkParameterIsNotNull(str4, Constants.SharedPref.THEME_COLOR);
        String str5 = str2;
        Intrinsics.checkParameterIsNotNull(str5, "payment_method");
        String str6 = str3;
        Intrinsics.checkParameterIsNotNull(str6, "app_lang");
        Intrinsics.checkParameterIsNotNull(list9, Constants.viewName.VIEW_YOU_MAY_LIKE);
        return new Dashboard(list10, list11, currencySymbol2, list12, list13, list14, list15, list16, socialLink2, list17, str4, str5, str6, z, list9);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Dashboard)) {
            return false;
        }
        Dashboard dashboard = (Dashboard) obj;
        return Intrinsics.areEqual((Object) this.banner, (Object) dashboard.banner) && Intrinsics.areEqual((Object) this.best_selling_product, (Object) dashboard.best_selling_product) && Intrinsics.areEqual((Object) this.currency_symbol, (Object) dashboard.currency_symbol) && Intrinsics.areEqual((Object) this.deal_of_the_day, (Object) dashboard.deal_of_the_day) && Intrinsics.areEqual((Object) this.featured, (Object) dashboard.featured) && Intrinsics.areEqual((Object) this.newest, (Object) dashboard.newest) && Intrinsics.areEqual((Object) this.offer, (Object) dashboard.offer) && Intrinsics.areEqual((Object) this.sale_product, (Object) dashboard.sale_product) && Intrinsics.areEqual((Object) this.social_link, (Object) dashboard.social_link) && Intrinsics.areEqual((Object) this.suggested_for_you, (Object) dashboard.suggested_for_you) && Intrinsics.areEqual((Object) this.theme_color, (Object) dashboard.theme_color) && Intrinsics.areEqual((Object) this.payment_method, (Object) dashboard.payment_method) && Intrinsics.areEqual((Object) this.app_lang, (Object) dashboard.app_lang) && this.enable_coupons == dashboard.enable_coupons && Intrinsics.areEqual((Object) this.you_may_like, (Object) dashboard.you_may_like);
    }

    public int hashCode() {
        List<DashboardBanner> list = this.banner;
        int i = 0;
        int hashCode = (list != null ? list.hashCode() : 0) * 31;
        List<StoreProductModel> list2 = this.best_selling_product;
        int hashCode2 = (hashCode + (list2 != null ? list2.hashCode() : 0)) * 31;
        CurrencySymbol currencySymbol = this.currency_symbol;
        int hashCode3 = (hashCode2 + (currencySymbol != null ? currencySymbol.hashCode() : 0)) * 31;
        List<StoreProductModel> list3 = this.deal_of_the_day;
        int hashCode4 = (hashCode3 + (list3 != null ? list3.hashCode() : 0)) * 31;
        List<StoreProductModel> list4 = this.featured;
        int hashCode5 = (hashCode4 + (list4 != null ? list4.hashCode() : 0)) * 31;
        List<StoreProductModel> list5 = this.newest;
        int hashCode6 = (hashCode5 + (list5 != null ? list5.hashCode() : 0)) * 31;
        List<StoreProductModel> list6 = this.offer;
        int hashCode7 = (hashCode6 + (list6 != null ? list6.hashCode() : 0)) * 31;
        List<StoreProductModel> list7 = this.sale_product;
        int hashCode8 = (hashCode7 + (list7 != null ? list7.hashCode() : 0)) * 31;
        SocialLink socialLink = this.social_link;
        int hashCode9 = (hashCode8 + (socialLink != null ? socialLink.hashCode() : 0)) * 31;
        List<StoreProductModel> list8 = this.suggested_for_you;
        int hashCode10 = (hashCode9 + (list8 != null ? list8.hashCode() : 0)) * 31;
        String str = this.theme_color;
        int hashCode11 = (hashCode10 + (str != null ? str.hashCode() : 0)) * 31;
        String str2 = this.payment_method;
        int hashCode12 = (hashCode11 + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.app_lang;
        int hashCode13 = (hashCode12 + (str3 != null ? str3.hashCode() : 0)) * 31;
        boolean z = this.enable_coupons;
        if (z) {
            z = true;
        }
        int i2 = (hashCode13 + (z ? 1 : 0)) * 31;
        List<StoreProductModel> list9 = this.you_may_like;
        if (list9 != null) {
            i = list9.hashCode();
        }
        return i2 + i;
    }

    public String toString() {
        return "Dashboard(banner=" + this.banner + ", best_selling_product=" + this.best_selling_product + ", currency_symbol=" + this.currency_symbol + ", deal_of_the_day=" + this.deal_of_the_day + ", featured=" + this.featured + ", newest=" + this.newest + ", offer=" + this.offer + ", sale_product=" + this.sale_product + ", social_link=" + this.social_link + ", suggested_for_you=" + this.suggested_for_you + ", theme_color=" + this.theme_color + ", payment_method=" + this.payment_method + ", app_lang=" + this.app_lang + ", enable_coupons=" + this.enable_coupons + ", you_may_like=" + this.you_may_like + ")";
    }

    public Dashboard(List<DashboardBanner> list, List<StoreProductModel> list2, CurrencySymbol currencySymbol, List<StoreProductModel> list3, List<StoreProductModel> list4, List<StoreProductModel> list5, List<StoreProductModel> list6, List<StoreProductModel> list7, SocialLink socialLink, List<StoreProductModel> list8, String str, String str2, String str3, boolean z, List<StoreProductModel> list9) {
        List<DashboardBanner> list10 = list;
        List<StoreProductModel> list11 = list2;
        CurrencySymbol currencySymbol2 = currencySymbol;
        List<StoreProductModel> list12 = list3;
        List<StoreProductModel> list13 = list4;
        List<StoreProductModel> list14 = list5;
        List<StoreProductModel> list15 = list6;
        List<StoreProductModel> list16 = list7;
        SocialLink socialLink2 = socialLink;
        List<StoreProductModel> list17 = list8;
        String str4 = str;
        String str5 = str2;
        String str6 = str3;
        List<StoreProductModel> list18 = list9;
        Intrinsics.checkParameterIsNotNull(list10, "banner");
        Intrinsics.checkParameterIsNotNull(list11, Constants.viewName.VIEW_BEST_SELLING);
        Intrinsics.checkParameterIsNotNull(currencySymbol2, "currency_symbol");
        Intrinsics.checkParameterIsNotNull(list12, Constants.viewName.VIEW_DEAL_OF_THE_DAY);
        Intrinsics.checkParameterIsNotNull(list13, "featured");
        Intrinsics.checkParameterIsNotNull(list14, "newest");
        Intrinsics.checkParameterIsNotNull(list15, Constants.viewName.VIEW_OFFER);
        Intrinsics.checkParameterIsNotNull(list16, Constants.viewName.VIEW_SALE);
        Intrinsics.checkParameterIsNotNull(socialLink2, "social_link");
        Intrinsics.checkParameterIsNotNull(list17, Constants.viewName.VIEW_SUGGESTED_FOR_YOU);
        Intrinsics.checkParameterIsNotNull(str4, Constants.SharedPref.THEME_COLOR);
        Intrinsics.checkParameterIsNotNull(str5, "payment_method");
        Intrinsics.checkParameterIsNotNull(str6, "app_lang");
        Intrinsics.checkParameterIsNotNull(list18, Constants.viewName.VIEW_YOU_MAY_LIKE);
        this.banner = list10;
        this.best_selling_product = list11;
        this.currency_symbol = currencySymbol2;
        this.deal_of_the_day = list12;
        this.featured = list13;
        this.newest = list14;
        this.offer = list15;
        this.sale_product = list16;
        this.social_link = socialLink2;
        this.suggested_for_you = list17;
        this.theme_color = str4;
        this.payment_method = str5;
        this.app_lang = str6;
        this.enable_coupons = z;
        this.you_may_like = list18;
    }

    public final List<DashboardBanner> getBanner() {
        return this.banner;
    }

    public final List<StoreProductModel> getBest_selling_product() {
        return this.best_selling_product;
    }

    public final CurrencySymbol getCurrency_symbol() {
        return this.currency_symbol;
    }

    public final List<StoreProductModel> getDeal_of_the_day() {
        return this.deal_of_the_day;
    }

    public final List<StoreProductModel> getFeatured() {
        return this.featured;
    }

    public final List<StoreProductModel> getNewest() {
        return this.newest;
    }

    public final List<StoreProductModel> getOffer() {
        return this.offer;
    }

    public final List<StoreProductModel> getSale_product() {
        return this.sale_product;
    }

    public final SocialLink getSocial_link() {
        return this.social_link;
    }

    public final List<StoreProductModel> getSuggested_for_you() {
        return this.suggested_for_you;
    }

    public final String getTheme_color() {
        return this.theme_color;
    }

    public final String getPayment_method() {
        return this.payment_method;
    }

    public final String getApp_lang() {
        return this.app_lang;
    }

    public final boolean getEnable_coupons() {
        return this.enable_coupons;
    }

    public final List<StoreProductModel> getYou_may_like() {
        return this.you_may_like;
    }
}
