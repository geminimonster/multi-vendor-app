package com.iqonic.store.models;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;

@Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010 \n\u0002\u0010\b\n\u0002\b\u001f\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002R\u001c\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\bRJ\u0010\t\u001a2\u0012\u0012\u0012\u0010\u0012\u0004\u0012\u00020\u0004\u0012\u0006\u0012\u0004\u0018\u00010\u00010\u000b\u0018\u00010\nj\u0018\u0012\u0012\u0012\u0010\u0012\u0004\u0012\u00020\u0004\u0012\u0006\u0012\u0004\u0018\u00010\u00010\u000b\u0018\u0001`\fX\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\r\u0010\u000e\"\u0004\b\u000f\u0010\u0010R\"\u0010\u0011\u001a\n\u0012\u0004\u0012\u00020\u0013\u0018\u00010\u0012X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0014\u0010\u0015\"\u0004\b\u0016\u0010\u0017R\u001c\u0010\u0018\u001a\u0004\u0018\u00010\u0004X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0019\u0010\u0006\"\u0004\b\u001a\u0010\bR\u001c\u0010\u001b\u001a\u0004\u0018\u00010\u0004X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u001c\u0010\u0006\"\u0004\b\u001d\u0010\bR\u001c\u0010\u001e\u001a\u0004\u0018\u00010\u0004X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u001f\u0010\u0006\"\u0004\b \u0010\bR\u001a\u0010!\u001a\u00020\u0013X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\"\u0010#\"\u0004\b$\u0010%R\"\u0010&\u001a\n\u0012\u0004\u0012\u00020\u0013\u0018\u00010\u0012X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b'\u0010\u0015\"\u0004\b(\u0010\u0017R\u001a\u0010)\u001a\u00020\u0013X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b*\u0010#\"\u0004\b+\u0010%R\u001c\u0010,\u001a\u0004\u0018\u00010\u0004X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b-\u0010\u0006\"\u0004\b.\u0010\bR\u001c\u0010/\u001a\u0004\u0018\u00010\u0004X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b0\u0010\u0006\"\u0004\b1\u0010\b¨\u00062"}, d2 = {"Lcom/iqonic/store/models/SearchRequest;", "", "()V", "Optional_selling", "", "getOptional_selling", "()Ljava/lang/String;", "setOptional_selling", "(Ljava/lang/String;)V", "attribute", "Ljava/util/ArrayList;", "", "Lkotlin/collections/ArrayList;", "getAttribute", "()Ljava/util/ArrayList;", "setAttribute", "(Ljava/util/ArrayList;)V", "category", "", "", "getCategory", "()Ljava/util/List;", "setCategory", "(Ljava/util/List;)V", "featured", "getFeatured", "setFeatured", "newest", "getNewest", "setNewest", "on_sale", "getOn_sale", "setOn_sale", "page", "getPage", "()I", "setPage", "(I)V", "price", "getPrice", "setPrice", "product_per_page", "getProduct_per_page", "setProduct_per_page", "special_product", "getSpecial_product", "setSpecial_product", "text", "getText", "setText", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: RequestModel.kt */
public final class SearchRequest {
    private String Optional_selling;
    private ArrayList<Map<String, Object>> attribute;
    private List<Integer> category;
    private String featured;
    private String newest;
    private String on_sale;
    private int page;
    private List<Integer> price;
    private int product_per_page;
    private String special_product;
    private String text;

    public final ArrayList<Map<String, Object>> getAttribute() {
        return this.attribute;
    }

    public final void setAttribute(ArrayList<Map<String, Object>> arrayList) {
        this.attribute = arrayList;
    }

    public final List<Integer> getCategory() {
        return this.category;
    }

    public final void setCategory(List<Integer> list) {
        this.category = list;
    }

    public final List<Integer> getPrice() {
        return this.price;
    }

    public final void setPrice(List<Integer> list) {
        this.price = list;
    }

    public final String getText() {
        return this.text;
    }

    public final void setText(String str) {
        this.text = str;
    }

    public final String getOptional_selling() {
        return this.Optional_selling;
    }

    public final void setOptional_selling(String str) {
        this.Optional_selling = str;
    }

    public final String getOn_sale() {
        return this.on_sale;
    }

    public final void setOn_sale(String str) {
        this.on_sale = str;
    }

    public final String getNewest() {
        return this.newest;
    }

    public final void setNewest(String str) {
        this.newest = str;
    }

    public final String getFeatured() {
        return this.featured;
    }

    public final void setFeatured(String str) {
        this.featured = str;
    }

    public final int getPage() {
        return this.page;
    }

    public final void setPage(int i) {
        this.page = i;
    }

    public final int getProduct_per_page() {
        return this.product_per_page;
    }

    public final void setProduct_per_page(int i) {
        this.product_per_page = i;
    }

    public final String getSpecial_product() {
        return this.special_product;
    }

    public final void setSpecial_product(String str) {
        this.special_product = str;
    }
}
