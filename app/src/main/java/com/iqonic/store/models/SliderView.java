package com.iqonic.store.models;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import kotlin.Metadata;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0006\b\u0016\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002R\"\u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u000e¢\u0006\u0010\n\u0002\u0010\t\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\b¨\u0006\n"}, d2 = {"Lcom/iqonic/store/models/SliderView;", "Ljava/io/Serializable;", "()V", "enable", "", "getEnable", "()Ljava/lang/Boolean;", "setEnable", "(Ljava/lang/Boolean;)V", "Ljava/lang/Boolean;", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: DashBoardResponse.kt */
public class SliderView implements Serializable {
    @SerializedName("enable")
    private Boolean enable;

    public final Boolean getEnable() {
        return this.enable;
    }

    public final void setEnable(Boolean bool) {
        this.enable = bool;
    }
}
