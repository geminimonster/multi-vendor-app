package com.iqonic.store.models;

import java.util.List;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B\u0017\u0012\u0010\b\u0002\u0010\u0002\u001a\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u0003¢\u0006\u0002\u0010\u0005J\u0011\u0010\b\u001a\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u0003HÆ\u0003J\u001b\u0010\t\u001a\u00020\u00002\u0010\b\u0002\u0010\u0002\u001a\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u0003HÆ\u0001J\u0013\u0010\n\u001a\u00020\u000b2\b\u0010\f\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\r\u001a\u00020\u000eHÖ\u0001J\t\u0010\u000f\u001a\u00020\u0010HÖ\u0001R\u0019\u0010\u0002\u001a\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\u0007¨\u0006\u0011"}, d2 = {"Lcom/iqonic/store/models/StoreProductAttribute;", "", "attribute", "", "Lcom/iqonic/store/models/StoreAttribute;", "(Ljava/util/List;)V", "getAttribute", "()Ljava/util/List;", "component1", "copy", "equals", "", "other", "hashCode", "", "toString", "", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: BaseResponse.kt */
public final class StoreProductAttribute {
    private final List<StoreAttribute> attribute;

    public StoreProductAttribute() {
        this((List) null, 1, (DefaultConstructorMarker) null);
    }

    public static /* synthetic */ StoreProductAttribute copy$default(StoreProductAttribute storeProductAttribute, List<StoreAttribute> list, int i, Object obj) {
        if ((i & 1) != 0) {
            list = storeProductAttribute.attribute;
        }
        return storeProductAttribute.copy(list);
    }

    public final List<StoreAttribute> component1() {
        return this.attribute;
    }

    public final StoreProductAttribute copy(List<StoreAttribute> list) {
        return new StoreProductAttribute(list);
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            return (obj instanceof StoreProductAttribute) && Intrinsics.areEqual((Object) this.attribute, (Object) ((StoreProductAttribute) obj).attribute);
        }
        return true;
    }

    public int hashCode() {
        List<StoreAttribute> list = this.attribute;
        if (list != null) {
            return list.hashCode();
        }
        return 0;
    }

    public String toString() {
        return "StoreProductAttribute(attribute=" + this.attribute + ")";
    }

    public StoreProductAttribute(List<StoreAttribute> list) {
        this.attribute = list;
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ StoreProductAttribute(List list, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this((i & 1) != 0 ? null : list);
    }

    public final List<StoreAttribute> getAttribute() {
        return this.attribute;
    }
}
