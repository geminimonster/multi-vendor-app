package com.iqonic.store.models;

import com.google.gson.annotations.SerializedName;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u000e\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002R\u001e\u0010\u0003\u001a\u00020\u00048\u0006@\u0006X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\bR\u001e\u0010\t\u001a\u00020\u00048\u0006@\u0006X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\n\u0010\u0006\"\u0004\b\u000b\u0010\bR\u001e\u0010\f\u001a\u00020\u00048\u0006@\u0006X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\r\u0010\u0006\"\u0004\b\u000e\u0010\bR\u001e\u0010\u000f\u001a\u00020\u00048\u0006@\u0006X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0010\u0010\u0006\"\u0004\b\u0011\u0010\b¨\u0006\u0012"}, d2 = {"Lcom/iqonic/store/models/AppBar;", "", "()V", "appBarIcon", "", "getAppBarIcon", "()Ljava/lang/String;", "setAppBarIcon", "(Ljava/lang/String;)V", "appBarLayout", "getAppBarLayout", "setAppBarLayout", "backgroundColor", "getBackgroundColor", "setBackgroundColor", "title", "getTitle", "setTitle", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: DashBoardResponse.kt */
public final class AppBar {
    @SerializedName("appBarIcon")
    private String appBarIcon = "";
    @SerializedName("appBarLayout")
    private String appBarLayout = "";
    @SerializedName("backgroundColor")
    private String backgroundColor = "";
    @SerializedName("title")
    private String title = "";

    public final String getTitle() {
        return this.title;
    }

    public final void setTitle(String str) {
        Intrinsics.checkParameterIsNotNull(str, "<set-?>");
        this.title = str;
    }

    public final String getAppBarIcon() {
        return this.appBarIcon;
    }

    public final void setAppBarIcon(String str) {
        Intrinsics.checkParameterIsNotNull(str, "<set-?>");
        this.appBarIcon = str;
    }

    public final String getBackgroundColor() {
        return this.backgroundColor;
    }

    public final void setBackgroundColor(String str) {
        Intrinsics.checkParameterIsNotNull(str, "<set-?>");
        this.backgroundColor = str;
    }

    public final String getAppBarLayout() {
        return this.appBarLayout;
    }

    public final void setAppBarLayout(String str) {
        Intrinsics.checkParameterIsNotNull(str, "<set-?>");
        this.appBarLayout = str;
    }
}
