package com.iqonic.store.models;

import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\f\n\u0002\u0010\u000b\n\u0002\b\u0004\b\b\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ\t\u0010\u000f\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0010\u001a\u00020\u0005HÆ\u0003J\t\u0010\u0011\u001a\u00020\u0007HÆ\u0003J'\u0010\u0012\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u0007HÆ\u0001J\u0013\u0010\u0013\u001a\u00020\u00142\b\u0010\u0015\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0016\u001a\u00020\u0003HÖ\u0001J\t\u0010\u0017\u001a\u00020\u0007HÖ\u0001R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0011\u0010\u0006\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000e¨\u0006\u0018"}, d2 = {"Lcom/iqonic/store/models/RegisterResponse;", "", "code", "", "data", "Lcom/iqonic/store/models/Data;", "message", "", "(ILcom/iqonic/store/models/Data;Ljava/lang/String;)V", "getCode", "()I", "getData", "()Lcom/iqonic/store/models/Data;", "getMessage", "()Ljava/lang/String;", "component1", "component2", "component3", "copy", "equals", "", "other", "hashCode", "toString", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: BaseResponse.kt */
public final class RegisterResponse {
    private final int code;
    private final Data data;
    private final String message;

    public static /* synthetic */ RegisterResponse copy$default(RegisterResponse registerResponse, int i, Data data2, String str, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            i = registerResponse.code;
        }
        if ((i2 & 2) != 0) {
            data2 = registerResponse.data;
        }
        if ((i2 & 4) != 0) {
            str = registerResponse.message;
        }
        return registerResponse.copy(i, data2, str);
    }

    public final int component1() {
        return this.code;
    }

    public final Data component2() {
        return this.data;
    }

    public final String component3() {
        return this.message;
    }

    public final RegisterResponse copy(int i, Data data2, String str) {
        Intrinsics.checkParameterIsNotNull(data2, "data");
        Intrinsics.checkParameterIsNotNull(str, "message");
        return new RegisterResponse(i, data2, str);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof RegisterResponse)) {
            return false;
        }
        RegisterResponse registerResponse = (RegisterResponse) obj;
        return this.code == registerResponse.code && Intrinsics.areEqual((Object) this.data, (Object) registerResponse.data) && Intrinsics.areEqual((Object) this.message, (Object) registerResponse.message);
    }

    public int hashCode() {
        int i = this.code * 31;
        Data data2 = this.data;
        int i2 = 0;
        int hashCode = (i + (data2 != null ? data2.hashCode() : 0)) * 31;
        String str = this.message;
        if (str != null) {
            i2 = str.hashCode();
        }
        return hashCode + i2;
    }

    public String toString() {
        return "RegisterResponse(code=" + this.code + ", data=" + this.data + ", message=" + this.message + ")";
    }

    public RegisterResponse(int i, Data data2, String str) {
        Intrinsics.checkParameterIsNotNull(data2, "data");
        Intrinsics.checkParameterIsNotNull(str, "message");
        this.code = i;
        this.data = data2;
        this.message = str;
    }

    public final int getCode() {
        return this.code;
    }

    public final Data getData() {
        return this.data;
    }

    public final String getMessage() {
        return this.message;
    }
}
