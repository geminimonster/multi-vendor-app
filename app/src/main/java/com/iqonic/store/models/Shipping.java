package com.iqonic.store.models;

import java.io.Serializable;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\"\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u0010\u0010$\u001a\u00020\u00042\b\b\u0002\u0010%\u001a\u00020\u0004R\u001a\u0010\u0003\u001a\u00020\u0004X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\bR\u001a\u0010\t\u001a\u00020\u0004X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\n\u0010\u0006\"\u0004\b\u000b\u0010\bR\u001a\u0010\f\u001a\u00020\u0004X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\r\u0010\u0006\"\u0004\b\u000e\u0010\bR\u001a\u0010\u000f\u001a\u00020\u0004X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0010\u0010\u0006\"\u0004\b\u0011\u0010\bR\u001a\u0010\u0012\u001a\u00020\u0004X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0013\u0010\u0006\"\u0004\b\u0014\u0010\bR\u001a\u0010\u0015\u001a\u00020\u0004X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0016\u0010\u0006\"\u0004\b\u0017\u0010\bR\u001a\u0010\u0018\u001a\u00020\u0004X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0019\u0010\u0006\"\u0004\b\u001a\u0010\bR\u001a\u0010\u001b\u001a\u00020\u0004X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u001c\u0010\u0006\"\u0004\b\u001d\u0010\bR\u001a\u0010\u001e\u001a\u00020\u0004X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u001f\u0010\u0006\"\u0004\b \u0010\bR\u001a\u0010!\u001a\u00020\u0004X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\"\u0010\u0006\"\u0004\b#\u0010\b¨\u0006&"}, d2 = {"Lcom/iqonic/store/models/Shipping;", "Ljava/io/Serializable;", "()V", "address_1", "", "getAddress_1", "()Ljava/lang/String;", "setAddress_1", "(Ljava/lang/String;)V", "address_2", "getAddress_2", "setAddress_2", "city", "getCity", "setCity", "company", "getCompany", "setCompany", "country", "getCountry", "setCountry", "first_name", "getFirst_name", "setFirst_name", "last_name", "getLast_name", "setLast_name", "phone", "getPhone", "setPhone", "postcode", "getPostcode", "setPostcode", "state", "getState", "setState", "getFullAddress", "sap", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: BaseResponse.kt */
public final class Shipping implements Serializable {
    private String address_1 = "";
    private String address_2 = "";
    private String city = "";
    private String company = "";
    private String country = "";
    private String first_name = "";
    private String last_name = "";
    private String phone = "";
    private String postcode = "";
    private String state = "";

    public final String getAddress_1() {
        return this.address_1;
    }

    public final void setAddress_1(String str) {
        Intrinsics.checkParameterIsNotNull(str, "<set-?>");
        this.address_1 = str;
    }

    public final String getAddress_2() {
        return this.address_2;
    }

    public final void setAddress_2(String str) {
        Intrinsics.checkParameterIsNotNull(str, "<set-?>");
        this.address_2 = str;
    }

    public final String getCity() {
        return this.city;
    }

    public final void setCity(String str) {
        Intrinsics.checkParameterIsNotNull(str, "<set-?>");
        this.city = str;
    }

    public final String getCountry() {
        return this.country;
    }

    public final void setCountry(String str) {
        Intrinsics.checkParameterIsNotNull(str, "<set-?>");
        this.country = str;
    }

    public final String getFirst_name() {
        return this.first_name;
    }

    public final void setFirst_name(String str) {
        Intrinsics.checkParameterIsNotNull(str, "<set-?>");
        this.first_name = str;
    }

    public final String getLast_name() {
        return this.last_name;
    }

    public final void setLast_name(String str) {
        Intrinsics.checkParameterIsNotNull(str, "<set-?>");
        this.last_name = str;
    }

    public final String getPhone() {
        return this.phone;
    }

    public final void setPhone(String str) {
        Intrinsics.checkParameterIsNotNull(str, "<set-?>");
        this.phone = str;
    }

    public final String getPostcode() {
        return this.postcode;
    }

    public final void setPostcode(String str) {
        Intrinsics.checkParameterIsNotNull(str, "<set-?>");
        this.postcode = str;
    }

    public final String getState() {
        return this.state;
    }

    public final void setState(String str) {
        Intrinsics.checkParameterIsNotNull(str, "<set-?>");
        this.state = str;
    }

    public final String getCompany() {
        return this.company;
    }

    public final void setCompany(String str) {
        Intrinsics.checkParameterIsNotNull(str, "<set-?>");
        this.company = str;
    }

    public static /* synthetic */ String getFullAddress$default(Shipping shipping, String str, int i, Object obj) {
        if ((i & 1) != 0) {
            str = ",";
        }
        return shipping.getFullAddress(str);
    }

    public final String getFullAddress(String str) {
        Intrinsics.checkParameterIsNotNull(str, "sap");
        return this.address_1 + str + this.address_2 + str + this.city + ' ' + this.postcode + str + this.state + str + this.country;
    }
}
