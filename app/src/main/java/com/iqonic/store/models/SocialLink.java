package com.iqonic.store.models;

import com.iqonic.store.utils.Constants;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u001b\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\b\u0018\u00002\u00020\u0001BE\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0003\u0012\u0006\u0010\u0006\u001a\u00020\u0003\u0012\u0006\u0010\u0007\u001a\u00020\u0003\u0012\u0006\u0010\b\u001a\u00020\u0003\u0012\u0006\u0010\t\u001a\u00020\u0003\u0012\u0006\u0010\n\u001a\u00020\u0003¢\u0006\u0002\u0010\u000bJ\t\u0010\u0015\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0016\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0017\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0018\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0019\u001a\u00020\u0003HÆ\u0003J\t\u0010\u001a\u001a\u00020\u0003HÆ\u0003J\t\u0010\u001b\u001a\u00020\u0003HÆ\u0003J\t\u0010\u001c\u001a\u00020\u0003HÆ\u0003JY\u0010\u001d\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00032\b\b\u0002\u0010\u0005\u001a\u00020\u00032\b\b\u0002\u0010\u0006\u001a\u00020\u00032\b\b\u0002\u0010\u0007\u001a\u00020\u00032\b\b\u0002\u0010\b\u001a\u00020\u00032\b\b\u0002\u0010\t\u001a\u00020\u00032\b\b\u0002\u0010\n\u001a\u00020\u0003HÆ\u0001J\u0013\u0010\u001e\u001a\u00020\u001f2\b\u0010 \u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010!\u001a\u00020\"HÖ\u0001J\t\u0010#\u001a\u00020\u0003HÖ\u0001R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\rR\u0011\u0010\u0004\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\rR\u0011\u0010\u0005\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\rR\u0011\u0010\u0006\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\rR\u0011\u0010\u0007\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\rR\u0011\u0010\b\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\rR\u0011\u0010\t\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\rR\u0011\u0010\n\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0014\u0010\r¨\u0006$"}, d2 = {"Lcom/iqonic/store/models/SocialLink;", "", "contact", "", "copyright_text", "facebook", "instagram", "privacy_policy", "term_condition", "twitter", "whatsapp", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", "getContact", "()Ljava/lang/String;", "getCopyright_text", "getFacebook", "getInstagram", "getPrivacy_policy", "getTerm_condition", "getTwitter", "getWhatsapp", "component1", "component2", "component3", "component4", "component5", "component6", "component7", "component8", "copy", "equals", "", "other", "hashCode", "", "toString", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: Dashboard.kt */
public final class SocialLink {
    private final String contact;
    private final String copyright_text;
    private final String facebook;
    private final String instagram;
    private final String privacy_policy;
    private final String term_condition;
    private final String twitter;
    private final String whatsapp;

    public static /* synthetic */ SocialLink copy$default(SocialLink socialLink, String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, int i, Object obj) {
        SocialLink socialLink2 = socialLink;
        int i2 = i;
        return socialLink.copy((i2 & 1) != 0 ? socialLink2.contact : str, (i2 & 2) != 0 ? socialLink2.copyright_text : str2, (i2 & 4) != 0 ? socialLink2.facebook : str3, (i2 & 8) != 0 ? socialLink2.instagram : str4, (i2 & 16) != 0 ? socialLink2.privacy_policy : str5, (i2 & 32) != 0 ? socialLink2.term_condition : str6, (i2 & 64) != 0 ? socialLink2.twitter : str7, (i2 & 128) != 0 ? socialLink2.whatsapp : str8);
    }

    public final String component1() {
        return this.contact;
    }

    public final String component2() {
        return this.copyright_text;
    }

    public final String component3() {
        return this.facebook;
    }

    public final String component4() {
        return this.instagram;
    }

    public final String component5() {
        return this.privacy_policy;
    }

    public final String component6() {
        return this.term_condition;
    }

    public final String component7() {
        return this.twitter;
    }

    public final String component8() {
        return this.whatsapp;
    }

    public final SocialLink copy(String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8) {
        Intrinsics.checkParameterIsNotNull(str, Constants.SharedPref.CONTACT);
        Intrinsics.checkParameterIsNotNull(str2, Constants.SharedPref.COPYRIGHT_TEXT);
        Intrinsics.checkParameterIsNotNull(str3, Constants.SharedPref.FACEBOOK);
        Intrinsics.checkParameterIsNotNull(str4, Constants.SharedPref.INSTAGRAM);
        Intrinsics.checkParameterIsNotNull(str5, Constants.SharedPref.PRIVACY_POLICY);
        String str9 = str6;
        Intrinsics.checkParameterIsNotNull(str9, Constants.SharedPref.TERM_CONDITION);
        String str10 = str7;
        Intrinsics.checkParameterIsNotNull(str10, Constants.SharedPref.TWITTER);
        String str11 = str8;
        Intrinsics.checkParameterIsNotNull(str11, Constants.SharedPref.WHATSAPP);
        return new SocialLink(str, str2, str3, str4, str5, str9, str10, str11);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof SocialLink)) {
            return false;
        }
        SocialLink socialLink = (SocialLink) obj;
        return Intrinsics.areEqual((Object) this.contact, (Object) socialLink.contact) && Intrinsics.areEqual((Object) this.copyright_text, (Object) socialLink.copyright_text) && Intrinsics.areEqual((Object) this.facebook, (Object) socialLink.facebook) && Intrinsics.areEqual((Object) this.instagram, (Object) socialLink.instagram) && Intrinsics.areEqual((Object) this.privacy_policy, (Object) socialLink.privacy_policy) && Intrinsics.areEqual((Object) this.term_condition, (Object) socialLink.term_condition) && Intrinsics.areEqual((Object) this.twitter, (Object) socialLink.twitter) && Intrinsics.areEqual((Object) this.whatsapp, (Object) socialLink.whatsapp);
    }

    public int hashCode() {
        String str = this.contact;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.copyright_text;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.facebook;
        int hashCode3 = (hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31;
        String str4 = this.instagram;
        int hashCode4 = (hashCode3 + (str4 != null ? str4.hashCode() : 0)) * 31;
        String str5 = this.privacy_policy;
        int hashCode5 = (hashCode4 + (str5 != null ? str5.hashCode() : 0)) * 31;
        String str6 = this.term_condition;
        int hashCode6 = (hashCode5 + (str6 != null ? str6.hashCode() : 0)) * 31;
        String str7 = this.twitter;
        int hashCode7 = (hashCode6 + (str7 != null ? str7.hashCode() : 0)) * 31;
        String str8 = this.whatsapp;
        if (str8 != null) {
            i = str8.hashCode();
        }
        return hashCode7 + i;
    }

    public String toString() {
        return "SocialLink(contact=" + this.contact + ", copyright_text=" + this.copyright_text + ", facebook=" + this.facebook + ", instagram=" + this.instagram + ", privacy_policy=" + this.privacy_policy + ", term_condition=" + this.term_condition + ", twitter=" + this.twitter + ", whatsapp=" + this.whatsapp + ")";
    }

    public SocialLink(String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8) {
        Intrinsics.checkParameterIsNotNull(str, Constants.SharedPref.CONTACT);
        Intrinsics.checkParameterIsNotNull(str2, Constants.SharedPref.COPYRIGHT_TEXT);
        Intrinsics.checkParameterIsNotNull(str3, Constants.SharedPref.FACEBOOK);
        Intrinsics.checkParameterIsNotNull(str4, Constants.SharedPref.INSTAGRAM);
        Intrinsics.checkParameterIsNotNull(str5, Constants.SharedPref.PRIVACY_POLICY);
        Intrinsics.checkParameterIsNotNull(str6, Constants.SharedPref.TERM_CONDITION);
        Intrinsics.checkParameterIsNotNull(str7, Constants.SharedPref.TWITTER);
        Intrinsics.checkParameterIsNotNull(str8, Constants.SharedPref.WHATSAPP);
        this.contact = str;
        this.copyright_text = str2;
        this.facebook = str3;
        this.instagram = str4;
        this.privacy_policy = str5;
        this.term_condition = str6;
        this.twitter = str7;
        this.whatsapp = str8;
    }

    public final String getContact() {
        return this.contact;
    }

    public final String getCopyright_text() {
        return this.copyright_text;
    }

    public final String getFacebook() {
        return this.facebook;
    }

    public final String getInstagram() {
        return this.instagram;
    }

    public final String getPrivacy_policy() {
        return this.privacy_policy;
    }

    public final String getTerm_condition() {
        return this.term_condition;
    }

    public final String getTwitter() {
        return this.twitter;
    }

    public final String getWhatsapp() {
        return this.whatsapp;
    }
}
