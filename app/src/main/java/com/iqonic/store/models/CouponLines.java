package com.iqonic.store.models;

import java.io.Serializable;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0007\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\b\b\u0018\u00002\u00020\u0001B\u000f\u0012\b\b\u0002\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\t\u0010\b\u001a\u00020\u0003HÆ\u0003J\u0013\u0010\t\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u0003HÆ\u0001J\u0013\u0010\n\u001a\u00020\u000b2\b\u0010\f\u001a\u0004\u0018\u00010\rHÖ\u0003J\t\u0010\u000e\u001a\u00020\u000fHÖ\u0001J\t\u0010\u0010\u001a\u00020\u0003HÖ\u0001R\u001a\u0010\u0002\u001a\u00020\u0003X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\u0004¨\u0006\u0011"}, d2 = {"Lcom/iqonic/store/models/CouponLines;", "Ljava/io/Serializable;", "code", "", "(Ljava/lang/String;)V", "getCode", "()Ljava/lang/String;", "setCode", "component1", "copy", "equals", "", "other", "", "hashCode", "", "toString", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: CouponLines.kt */
public final class CouponLines implements Serializable {
    private String code;

    public CouponLines() {
        this((String) null, 1, (DefaultConstructorMarker) null);
    }

    public static /* synthetic */ CouponLines copy$default(CouponLines couponLines, String str, int i, Object obj) {
        if ((i & 1) != 0) {
            str = couponLines.code;
        }
        return couponLines.copy(str);
    }

    public final String component1() {
        return this.code;
    }

    public final CouponLines copy(String str) {
        Intrinsics.checkParameterIsNotNull(str, "code");
        return new CouponLines(str);
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            return (obj instanceof CouponLines) && Intrinsics.areEqual((Object) this.code, (Object) ((CouponLines) obj).code);
        }
        return true;
    }

    public int hashCode() {
        String str = this.code;
        if (str != null) {
            return str.hashCode();
        }
        return 0;
    }

    public String toString() {
        return "CouponLines(code=" + this.code + ")";
    }

    public CouponLines(String str) {
        Intrinsics.checkParameterIsNotNull(str, "code");
        this.code = str;
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ CouponLines(String str, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this((i & 1) != 0 ? "" : str);
    }

    public final String getCode() {
        return this.code;
    }

    public final void setCode(String str) {
        Intrinsics.checkParameterIsNotNull(str, "<set-?>");
        this.code = str;
    }
}
