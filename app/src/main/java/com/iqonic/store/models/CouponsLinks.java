package com.iqonic.store.models;

import java.io.Serializable;
import java.util.List;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B!\u0012\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003\u0012\f\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00060\u0003¢\u0006\u0002\u0010\u0007J\u000f\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003HÆ\u0003J\u000f\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u00060\u0003HÆ\u0003J)\u0010\r\u001a\u00020\u00002\u000e\b\u0002\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u00032\u000e\b\u0002\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00060\u0003HÆ\u0001J\u0013\u0010\u000e\u001a\u00020\u000f2\b\u0010\u0010\u001a\u0004\u0018\u00010\u0011HÖ\u0003J\t\u0010\u0012\u001a\u00020\u0013HÖ\u0001J\t\u0010\u0014\u001a\u00020\u0015HÖ\u0001R\u0017\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u0017\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00060\u0003¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\t¨\u0006\u0016"}, d2 = {"Lcom/iqonic/store/models/CouponsLinks;", "Ljava/io/Serializable;", "collection", "", "Lcom/iqonic/store/models/CouponsCollection;", "self", "Lcom/iqonic/store/models/CouponsSelf;", "(Ljava/util/List;Ljava/util/List;)V", "getCollection", "()Ljava/util/List;", "getSelf", "component1", "component2", "copy", "equals", "", "other", "", "hashCode", "", "toString", "", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: Coupons.kt */
public final class CouponsLinks implements Serializable {
    private final List<CouponsCollection> collection;
    private final List<CouponsSelf> self;

    public static /* synthetic */ CouponsLinks copy$default(CouponsLinks couponsLinks, List<CouponsCollection> list, List<CouponsSelf> list2, int i, Object obj) {
        if ((i & 1) != 0) {
            list = couponsLinks.collection;
        }
        if ((i & 2) != 0) {
            list2 = couponsLinks.self;
        }
        return couponsLinks.copy(list, list2);
    }

    public final List<CouponsCollection> component1() {
        return this.collection;
    }

    public final List<CouponsSelf> component2() {
        return this.self;
    }

    public final CouponsLinks copy(List<CouponsCollection> list, List<CouponsSelf> list2) {
        Intrinsics.checkParameterIsNotNull(list, "collection");
        Intrinsics.checkParameterIsNotNull(list2, "self");
        return new CouponsLinks(list, list2);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof CouponsLinks)) {
            return false;
        }
        CouponsLinks couponsLinks = (CouponsLinks) obj;
        return Intrinsics.areEqual((Object) this.collection, (Object) couponsLinks.collection) && Intrinsics.areEqual((Object) this.self, (Object) couponsLinks.self);
    }

    public int hashCode() {
        List<CouponsCollection> list = this.collection;
        int i = 0;
        int hashCode = (list != null ? list.hashCode() : 0) * 31;
        List<CouponsSelf> list2 = this.self;
        if (list2 != null) {
            i = list2.hashCode();
        }
        return hashCode + i;
    }

    public String toString() {
        return "CouponsLinks(collection=" + this.collection + ", self=" + this.self + ")";
    }

    public CouponsLinks(List<CouponsCollection> list, List<CouponsSelf> list2) {
        Intrinsics.checkParameterIsNotNull(list, "collection");
        Intrinsics.checkParameterIsNotNull(list2, "self");
        this.collection = list;
        this.self = list2;
    }

    public final List<CouponsCollection> getCollection() {
        return this.collection;
    }

    public final List<CouponsSelf> getSelf() {
        return this.self;
    }
}
