package com.iqonic.store.models;

import com.iqonic.store.utils.Constants;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u001e\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\b\u0018\u00002\u00020\u0001BM\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0003\u0012\u0006\u0010\u0006\u001a\u00020\u0003\u0012\u0006\u0010\u0007\u001a\u00020\u0003\u0012\u0006\u0010\b\u001a\u00020\u0003\u0012\u0006\u0010\t\u001a\u00020\u0003\u0012\u0006\u0010\n\u001a\u00020\u0003\u0012\u0006\u0010\u000b\u001a\u00020\u0003¢\u0006\u0002\u0010\fJ\t\u0010\u0017\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0018\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0019\u001a\u00020\u0003HÆ\u0003J\t\u0010\u001a\u001a\u00020\u0003HÆ\u0003J\t\u0010\u001b\u001a\u00020\u0003HÆ\u0003J\t\u0010\u001c\u001a\u00020\u0003HÆ\u0003J\t\u0010\u001d\u001a\u00020\u0003HÆ\u0003J\t\u0010\u001e\u001a\u00020\u0003HÆ\u0003J\t\u0010\u001f\u001a\u00020\u0003HÆ\u0003Jc\u0010 \u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00032\b\b\u0002\u0010\u0005\u001a\u00020\u00032\b\b\u0002\u0010\u0006\u001a\u00020\u00032\b\b\u0002\u0010\u0007\u001a\u00020\u00032\b\b\u0002\u0010\b\u001a\u00020\u00032\b\b\u0002\u0010\t\u001a\u00020\u00032\b\b\u0002\u0010\n\u001a\u00020\u00032\b\b\u0002\u0010\u000b\u001a\u00020\u0003HÆ\u0001J\u0013\u0010!\u001a\u00020\"2\b\u0010#\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010$\u001a\u00020%HÖ\u0001J\t\u0010&\u001a\u00020\u0003HÖ\u0001R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000eR\u0011\u0010\u0004\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u000eR\u0011\u0010\u0005\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u000eR\u0011\u0010\u0006\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u000eR\u0011\u0010\u0007\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u000eR\u0011\u0010\b\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u000eR\u0011\u0010\t\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0014\u0010\u000eR\u0011\u0010\n\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0015\u0010\u000eR\u0011\u0010\u000b\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0016\u0010\u000e¨\u0006'"}, d2 = {"Lcom/iqonic/store/models/Data;", "", "ID", "", "display_name", "user_activation_key", "user_email", "user_login", "user_nicename", "user_registered", "user_status", "user_url", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", "getID", "()Ljava/lang/String;", "getDisplay_name", "getUser_activation_key", "getUser_email", "getUser_login", "getUser_nicename", "getUser_registered", "getUser_status", "getUser_url", "component1", "component2", "component3", "component4", "component5", "component6", "component7", "component8", "component9", "copy", "equals", "", "other", "hashCode", "", "toString", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: BaseResponse.kt */
public final class Data {
    private final String ID;
    private final String display_name;
    private final String user_activation_key;
    private final String user_email;
    private final String user_login;
    private final String user_nicename;
    private final String user_registered;
    private final String user_status;
    private final String user_url;

    public static /* synthetic */ Data copy$default(Data data, String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, String str9, int i, Object obj) {
        Data data2 = data;
        int i2 = i;
        return data.copy((i2 & 1) != 0 ? data2.ID : str, (i2 & 2) != 0 ? data2.display_name : str2, (i2 & 4) != 0 ? data2.user_activation_key : str3, (i2 & 8) != 0 ? data2.user_email : str4, (i2 & 16) != 0 ? data2.user_login : str5, (i2 & 32) != 0 ? data2.user_nicename : str6, (i2 & 64) != 0 ? data2.user_registered : str7, (i2 & 128) != 0 ? data2.user_status : str8, (i2 & 256) != 0 ? data2.user_url : str9);
    }

    public final String component1() {
        return this.ID;
    }

    public final String component2() {
        return this.display_name;
    }

    public final String component3() {
        return this.user_activation_key;
    }

    public final String component4() {
        return this.user_email;
    }

    public final String component5() {
        return this.user_login;
    }

    public final String component6() {
        return this.user_nicename;
    }

    public final String component7() {
        return this.user_registered;
    }

    public final String component8() {
        return this.user_status;
    }

    public final String component9() {
        return this.user_url;
    }

    public final Data copy(String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, String str9) {
        Intrinsics.checkParameterIsNotNull(str, "ID");
        Intrinsics.checkParameterIsNotNull(str2, "display_name");
        Intrinsics.checkParameterIsNotNull(str3, "user_activation_key");
        Intrinsics.checkParameterIsNotNull(str4, Constants.SharedPref.USER_EMAIL);
        String str10 = str5;
        Intrinsics.checkParameterIsNotNull(str10, "user_login");
        String str11 = str6;
        Intrinsics.checkParameterIsNotNull(str11, Constants.SharedPref.USER_NICE_NAME);
        String str12 = str7;
        Intrinsics.checkParameterIsNotNull(str12, "user_registered");
        String str13 = str8;
        Intrinsics.checkParameterIsNotNull(str13, "user_status");
        String str14 = str9;
        Intrinsics.checkParameterIsNotNull(str14, "user_url");
        return new Data(str, str2, str3, str4, str10, str11, str12, str13, str14);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Data)) {
            return false;
        }
        Data data = (Data) obj;
        return Intrinsics.areEqual((Object) this.ID, (Object) data.ID) && Intrinsics.areEqual((Object) this.display_name, (Object) data.display_name) && Intrinsics.areEqual((Object) this.user_activation_key, (Object) data.user_activation_key) && Intrinsics.areEqual((Object) this.user_email, (Object) data.user_email) && Intrinsics.areEqual((Object) this.user_login, (Object) data.user_login) && Intrinsics.areEqual((Object) this.user_nicename, (Object) data.user_nicename) && Intrinsics.areEqual((Object) this.user_registered, (Object) data.user_registered) && Intrinsics.areEqual((Object) this.user_status, (Object) data.user_status) && Intrinsics.areEqual((Object) this.user_url, (Object) data.user_url);
    }

    public int hashCode() {
        String str = this.ID;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.display_name;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.user_activation_key;
        int hashCode3 = (hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31;
        String str4 = this.user_email;
        int hashCode4 = (hashCode3 + (str4 != null ? str4.hashCode() : 0)) * 31;
        String str5 = this.user_login;
        int hashCode5 = (hashCode4 + (str5 != null ? str5.hashCode() : 0)) * 31;
        String str6 = this.user_nicename;
        int hashCode6 = (hashCode5 + (str6 != null ? str6.hashCode() : 0)) * 31;
        String str7 = this.user_registered;
        int hashCode7 = (hashCode6 + (str7 != null ? str7.hashCode() : 0)) * 31;
        String str8 = this.user_status;
        int hashCode8 = (hashCode7 + (str8 != null ? str8.hashCode() : 0)) * 31;
        String str9 = this.user_url;
        if (str9 != null) {
            i = str9.hashCode();
        }
        return hashCode8 + i;
    }

    public String toString() {
        return "Data(ID=" + this.ID + ", display_name=" + this.display_name + ", user_activation_key=" + this.user_activation_key + ", user_email=" + this.user_email + ", user_login=" + this.user_login + ", user_nicename=" + this.user_nicename + ", user_registered=" + this.user_registered + ", user_status=" + this.user_status + ", user_url=" + this.user_url + ")";
    }

    public Data(String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, String str9) {
        Intrinsics.checkParameterIsNotNull(str, "ID");
        Intrinsics.checkParameterIsNotNull(str2, "display_name");
        Intrinsics.checkParameterIsNotNull(str3, "user_activation_key");
        Intrinsics.checkParameterIsNotNull(str4, Constants.SharedPref.USER_EMAIL);
        Intrinsics.checkParameterIsNotNull(str5, "user_login");
        Intrinsics.checkParameterIsNotNull(str6, Constants.SharedPref.USER_NICE_NAME);
        Intrinsics.checkParameterIsNotNull(str7, "user_registered");
        Intrinsics.checkParameterIsNotNull(str8, "user_status");
        Intrinsics.checkParameterIsNotNull(str9, "user_url");
        this.ID = str;
        this.display_name = str2;
        this.user_activation_key = str3;
        this.user_email = str4;
        this.user_login = str5;
        this.user_nicename = str6;
        this.user_registered = str7;
        this.user_status = str8;
        this.user_url = str9;
    }

    public final String getID() {
        return this.ID;
    }

    public final String getDisplay_name() {
        return this.display_name;
    }

    public final String getUser_activation_key() {
        return this.user_activation_key;
    }

    public final String getUser_email() {
        return this.user_email;
    }

    public final String getUser_login() {
        return this.user_login;
    }

    public final String getUser_nicename() {
        return this.user_nicename;
    }

    public final String getUser_registered() {
        return this.user_registered;
    }

    public final String getUser_status() {
        return this.user_status;
    }

    public final String getUser_url() {
        return this.user_url;
    }
}
