package com.iqonic.store.models;

import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b\f\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\b\u0018\u00002\u00020\u0001B\u0019\u0012\b\b\u0002\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0002\u0010\u0004\u001a\u00020\u0003¢\u0006\u0002\u0010\u0005J\t\u0010\f\u001a\u00020\u0003HÆ\u0003J\t\u0010\r\u001a\u00020\u0003HÆ\u0003J\u001d\u0010\u000e\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u0003HÆ\u0001J\u0013\u0010\u000f\u001a\u00020\u00102\b\u0010\u0011\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0012\u001a\u00020\u0013HÖ\u0001J\t\u0010\u0014\u001a\u00020\u0003HÖ\u0001R\u001a\u0010\u0004\u001a\u00020\u0003X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0006\u0010\u0007\"\u0004\b\b\u0010\tR\u001a\u0010\u0002\u001a\u00020\u0003X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\n\u0010\u0007\"\u0004\b\u000b\u0010\t¨\u0006\u0015"}, d2 = {"Lcom/iqonic/store/models/CancelOrderRequest;", "", "status", "", "customer_note", "(Ljava/lang/String;Ljava/lang/String;)V", "getCustomer_note", "()Ljava/lang/String;", "setCustomer_note", "(Ljava/lang/String;)V", "getStatus", "setStatus", "component1", "component2", "copy", "equals", "", "other", "hashCode", "", "toString", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: CancelOrderRequest.kt */
public final class CancelOrderRequest {
    private String customer_note;
    private String status;

    public CancelOrderRequest() {
        this((String) null, (String) null, 3, (DefaultConstructorMarker) null);
    }

    public static /* synthetic */ CancelOrderRequest copy$default(CancelOrderRequest cancelOrderRequest, String str, String str2, int i, Object obj) {
        if ((i & 1) != 0) {
            str = cancelOrderRequest.status;
        }
        if ((i & 2) != 0) {
            str2 = cancelOrderRequest.customer_note;
        }
        return cancelOrderRequest.copy(str, str2);
    }

    public final String component1() {
        return this.status;
    }

    public final String component2() {
        return this.customer_note;
    }

    public final CancelOrderRequest copy(String str, String str2) {
        Intrinsics.checkParameterIsNotNull(str, "status");
        Intrinsics.checkParameterIsNotNull(str2, "customer_note");
        return new CancelOrderRequest(str, str2);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof CancelOrderRequest)) {
            return false;
        }
        CancelOrderRequest cancelOrderRequest = (CancelOrderRequest) obj;
        return Intrinsics.areEqual((Object) this.status, (Object) cancelOrderRequest.status) && Intrinsics.areEqual((Object) this.customer_note, (Object) cancelOrderRequest.customer_note);
    }

    public int hashCode() {
        String str = this.status;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.customer_note;
        if (str2 != null) {
            i = str2.hashCode();
        }
        return hashCode + i;
    }

    public String toString() {
        return "CancelOrderRequest(status=" + this.status + ", customer_note=" + this.customer_note + ")";
    }

    public CancelOrderRequest(String str, String str2) {
        Intrinsics.checkParameterIsNotNull(str, "status");
        Intrinsics.checkParameterIsNotNull(str2, "customer_note");
        this.status = str;
        this.customer_note = str2;
    }

    public final String getStatus() {
        return this.status;
    }

    public final void setStatus(String str) {
        Intrinsics.checkParameterIsNotNull(str, "<set-?>");
        this.status = str;
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ CancelOrderRequest(String str, String str2, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this((i & 1) != 0 ? "" : str, (i & 2) != 0 ? "" : str2);
    }

    public final String getCustomer_note() {
        return this.customer_note;
    }

    public final void setCustomer_note(String str) {
        Intrinsics.checkParameterIsNotNull(str, "<set-?>");
        this.customer_note = str;
    }
}
