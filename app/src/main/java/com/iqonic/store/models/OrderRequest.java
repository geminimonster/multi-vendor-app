package com.iqonic.store.models;

import com.google.firebase.analytics.FirebaseAnalytics;
import java.util.ArrayList;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000H\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b4\b\b\u0018\u00002\u00020\u0001B³\u0001\u0012\b\b\u0002\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0002\u0010\u0004\u001a\u00020\u0003\u0012\b\b\u0002\u0010\u0005\u001a\u00020\u0006\u0012\b\b\u0002\u0010\u0007\u001a\u00020\u0003\u0012\b\b\u0002\u0010\b\u001a\u00020\u0003\u0012\b\b\u0002\u0010\t\u001a\u00020\n\u0012\n\b\u0002\u0010\u000b\u001a\u0004\u0018\u00010\f\u0012\n\b\u0002\u0010\r\u001a\u0004\u0018\u00010\u000e\u0012\u001c\b\u0002\u0010\u000f\u001a\u0016\u0012\u0004\u0012\u00020\u0011\u0018\u00010\u0010j\n\u0012\u0004\u0012\u00020\u0011\u0018\u0001`\u0012\u0012\u001c\b\u0002\u0010\u0013\u001a\u0016\u0012\u0004\u0012\u00020\u0014\u0018\u00010\u0010j\n\u0012\u0004\u0012\u00020\u0014\u0018\u0001`\u0012\u0012\u001c\b\u0002\u0010\u0015\u001a\u0016\u0012\u0004\u0012\u00020\u0016\u0018\u00010\u0010j\n\u0012\u0004\u0012\u00020\u0016\u0018\u0001`\u0012¢\u0006\u0002\u0010\u0017J\t\u0010:\u001a\u00020\u0003HÆ\u0003J\u001d\u0010;\u001a\u0016\u0012\u0004\u0012\u00020\u0014\u0018\u00010\u0010j\n\u0012\u0004\u0012\u00020\u0014\u0018\u0001`\u0012HÆ\u0003J\u001d\u0010<\u001a\u0016\u0012\u0004\u0012\u00020\u0016\u0018\u00010\u0010j\n\u0012\u0004\u0012\u00020\u0016\u0018\u0001`\u0012HÆ\u0003J\t\u0010=\u001a\u00020\u0003HÆ\u0003J\t\u0010>\u001a\u00020\u0006HÆ\u0003J\t\u0010?\u001a\u00020\u0003HÆ\u0003J\t\u0010@\u001a\u00020\u0003HÆ\u0003J\t\u0010A\u001a\u00020\nHÆ\u0003J\u000b\u0010B\u001a\u0004\u0018\u00010\fHÆ\u0003J\u000b\u0010C\u001a\u0004\u0018\u00010\u000eHÆ\u0003J\u001d\u0010D\u001a\u0016\u0012\u0004\u0012\u00020\u0011\u0018\u00010\u0010j\n\u0012\u0004\u0012\u00020\u0011\u0018\u0001`\u0012HÆ\u0003J·\u0001\u0010E\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00032\b\b\u0002\u0010\u0005\u001a\u00020\u00062\b\b\u0002\u0010\u0007\u001a\u00020\u00032\b\b\u0002\u0010\b\u001a\u00020\u00032\b\b\u0002\u0010\t\u001a\u00020\n2\n\b\u0002\u0010\u000b\u001a\u0004\u0018\u00010\f2\n\b\u0002\u0010\r\u001a\u0004\u0018\u00010\u000e2\u001c\b\u0002\u0010\u000f\u001a\u0016\u0012\u0004\u0012\u00020\u0011\u0018\u00010\u0010j\n\u0012\u0004\u0012\u00020\u0011\u0018\u0001`\u00122\u001c\b\u0002\u0010\u0013\u001a\u0016\u0012\u0004\u0012\u00020\u0014\u0018\u00010\u0010j\n\u0012\u0004\u0012\u00020\u0014\u0018\u0001`\u00122\u001c\b\u0002\u0010\u0015\u001a\u0016\u0012\u0004\u0012\u00020\u0016\u0018\u00010\u0010j\n\u0012\u0004\u0012\u00020\u0016\u0018\u0001`\u0012HÆ\u0001J\u0013\u0010F\u001a\u00020\n2\b\u0010G\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010H\u001a\u00020\u0006HÖ\u0001J\t\u0010I\u001a\u00020\u0003HÖ\u0001R\u001c\u0010\u000b\u001a\u0004\u0018\u00010\fX\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0018\u0010\u0019\"\u0004\b\u001a\u0010\u001bR.\u0010\u0015\u001a\u0016\u0012\u0004\u0012\u00020\u0016\u0018\u00010\u0010j\n\u0012\u0004\u0012\u00020\u0016\u0018\u0001`\u0012X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u001c\u0010\u001d\"\u0004\b\u001e\u0010\u001fR\u001a\u0010\u0007\u001a\u00020\u0003X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b \u0010!\"\u0004\b\"\u0010#R\u001a\u0010\u0005\u001a\u00020\u0006X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b$\u0010%\"\u0004\b&\u0010'R.\u0010\u000f\u001a\u0016\u0012\u0004\u0012\u00020\u0011\u0018\u00010\u0010j\n\u0012\u0004\u0012\u00020\u0011\u0018\u0001`\u0012X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b(\u0010\u001d\"\u0004\b)\u0010\u001fR\u001a\u0010\u0002\u001a\u00020\u0003X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b*\u0010!\"\u0004\b+\u0010#R\u001a\u0010\t\u001a\u00020\nX\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b,\u0010-\"\u0004\b.\u0010/R\u001c\u0010\r\u001a\u0004\u0018\u00010\u000eX\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b0\u00101\"\u0004\b2\u00103R.\u0010\u0013\u001a\u0016\u0012\u0004\u0012\u00020\u0014\u0018\u00010\u0010j\n\u0012\u0004\u0012\u00020\u0014\u0018\u0001`\u0012X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b4\u0010\u001d\"\u0004\b5\u0010\u001fR\u001a\u0010\b\u001a\u00020\u0003X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b6\u0010!\"\u0004\b7\u0010#R\u001a\u0010\u0004\u001a\u00020\u0003X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b8\u0010!\"\u0004\b9\u0010#¨\u0006J"}, d2 = {"Lcom/iqonic/store/models/OrderRequest;", "", "payment_method", "", "transaction_id", "customer_id", "", "currency", "status", "set_paid", "", "billing", "Lcom/iqonic/store/models/Billing;", "shipping", "Lcom/iqonic/store/models/Shipping;", "line_items", "Ljava/util/ArrayList;", "Lcom/iqonic/store/models/Line_items;", "Lkotlin/collections/ArrayList;", "shipping_lines", "Lcom/iqonic/store/models/ShippingLines;", "coupon_lines", "Lcom/iqonic/store/models/CouponLines;", "(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;ZLcom/iqonic/store/models/Billing;Lcom/iqonic/store/models/Shipping;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;)V", "getBilling", "()Lcom/iqonic/store/models/Billing;", "setBilling", "(Lcom/iqonic/store/models/Billing;)V", "getCoupon_lines", "()Ljava/util/ArrayList;", "setCoupon_lines", "(Ljava/util/ArrayList;)V", "getCurrency", "()Ljava/lang/String;", "setCurrency", "(Ljava/lang/String;)V", "getCustomer_id", "()I", "setCustomer_id", "(I)V", "getLine_items", "setLine_items", "getPayment_method", "setPayment_method", "getSet_paid", "()Z", "setSet_paid", "(Z)V", "getShipping", "()Lcom/iqonic/store/models/Shipping;", "setShipping", "(Lcom/iqonic/store/models/Shipping;)V", "getShipping_lines", "setShipping_lines", "getStatus", "setStatus", "getTransaction_id", "setTransaction_id", "component1", "component10", "component11", "component2", "component3", "component4", "component5", "component6", "component7", "component8", "component9", "copy", "equals", "other", "hashCode", "toString", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: OrderRequest.kt */
public final class OrderRequest {
    private Billing billing;
    private ArrayList<CouponLines> coupon_lines;
    private String currency;
    private int customer_id;
    private ArrayList<Line_items> line_items;
    private String payment_method;
    private boolean set_paid;
    private Shipping shipping;
    private ArrayList<ShippingLines> shipping_lines;
    private String status;
    private String transaction_id;

    public OrderRequest() {
        this((String) null, (String) null, 0, (String) null, (String) null, false, (Billing) null, (Shipping) null, (ArrayList) null, (ArrayList) null, (ArrayList) null, 2047, (DefaultConstructorMarker) null);
    }

    public static /* synthetic */ OrderRequest copy$default(OrderRequest orderRequest, String str, String str2, int i, String str3, String str4, boolean z, Billing billing2, Shipping shipping2, ArrayList arrayList, ArrayList arrayList2, ArrayList arrayList3, int i2, Object obj) {
        OrderRequest orderRequest2 = orderRequest;
        int i3 = i2;
        return orderRequest.copy((i3 & 1) != 0 ? orderRequest2.payment_method : str, (i3 & 2) != 0 ? orderRequest2.transaction_id : str2, (i3 & 4) != 0 ? orderRequest2.customer_id : i, (i3 & 8) != 0 ? orderRequest2.currency : str3, (i3 & 16) != 0 ? orderRequest2.status : str4, (i3 & 32) != 0 ? orderRequest2.set_paid : z, (i3 & 64) != 0 ? orderRequest2.billing : billing2, (i3 & 128) != 0 ? orderRequest2.shipping : shipping2, (i3 & 256) != 0 ? orderRequest2.line_items : arrayList, (i3 & 512) != 0 ? orderRequest2.shipping_lines : arrayList2, (i3 & 1024) != 0 ? orderRequest2.coupon_lines : arrayList3);
    }

    public final String component1() {
        return this.payment_method;
    }

    public final ArrayList<ShippingLines> component10() {
        return this.shipping_lines;
    }

    public final ArrayList<CouponLines> component11() {
        return this.coupon_lines;
    }

    public final String component2() {
        return this.transaction_id;
    }

    public final int component3() {
        return this.customer_id;
    }

    public final String component4() {
        return this.currency;
    }

    public final String component5() {
        return this.status;
    }

    public final boolean component6() {
        return this.set_paid;
    }

    public final Billing component7() {
        return this.billing;
    }

    public final Shipping component8() {
        return this.shipping;
    }

    public final ArrayList<Line_items> component9() {
        return this.line_items;
    }

    public final OrderRequest copy(String str, String str2, int i, String str3, String str4, boolean z, Billing billing2, Shipping shipping2, ArrayList<Line_items> arrayList, ArrayList<ShippingLines> arrayList2, ArrayList<CouponLines> arrayList3) {
        Intrinsics.checkParameterIsNotNull(str, "payment_method");
        Intrinsics.checkParameterIsNotNull(str2, FirebaseAnalytics.Param.TRANSACTION_ID);
        String str5 = str3;
        Intrinsics.checkParameterIsNotNull(str5, FirebaseAnalytics.Param.CURRENCY);
        String str6 = str4;
        Intrinsics.checkParameterIsNotNull(str6, "status");
        return new OrderRequest(str, str2, i, str5, str6, z, billing2, shipping2, arrayList, arrayList2, arrayList3);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof OrderRequest)) {
            return false;
        }
        OrderRequest orderRequest = (OrderRequest) obj;
        return Intrinsics.areEqual((Object) this.payment_method, (Object) orderRequest.payment_method) && Intrinsics.areEqual((Object) this.transaction_id, (Object) orderRequest.transaction_id) && this.customer_id == orderRequest.customer_id && Intrinsics.areEqual((Object) this.currency, (Object) orderRequest.currency) && Intrinsics.areEqual((Object) this.status, (Object) orderRequest.status) && this.set_paid == orderRequest.set_paid && Intrinsics.areEqual((Object) this.billing, (Object) orderRequest.billing) && Intrinsics.areEqual((Object) this.shipping, (Object) orderRequest.shipping) && Intrinsics.areEqual((Object) this.line_items, (Object) orderRequest.line_items) && Intrinsics.areEqual((Object) this.shipping_lines, (Object) orderRequest.shipping_lines) && Intrinsics.areEqual((Object) this.coupon_lines, (Object) orderRequest.coupon_lines);
    }

    public int hashCode() {
        String str = this.payment_method;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.transaction_id;
        int hashCode2 = (((hashCode + (str2 != null ? str2.hashCode() : 0)) * 31) + this.customer_id) * 31;
        String str3 = this.currency;
        int hashCode3 = (hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31;
        String str4 = this.status;
        int hashCode4 = (hashCode3 + (str4 != null ? str4.hashCode() : 0)) * 31;
        boolean z = this.set_paid;
        if (z) {
            z = true;
        }
        int i2 = (hashCode4 + (z ? 1 : 0)) * 31;
        Billing billing2 = this.billing;
        int hashCode5 = (i2 + (billing2 != null ? billing2.hashCode() : 0)) * 31;
        Shipping shipping2 = this.shipping;
        int hashCode6 = (hashCode5 + (shipping2 != null ? shipping2.hashCode() : 0)) * 31;
        ArrayList<Line_items> arrayList = this.line_items;
        int hashCode7 = (hashCode6 + (arrayList != null ? arrayList.hashCode() : 0)) * 31;
        ArrayList<ShippingLines> arrayList2 = this.shipping_lines;
        int hashCode8 = (hashCode7 + (arrayList2 != null ? arrayList2.hashCode() : 0)) * 31;
        ArrayList<CouponLines> arrayList3 = this.coupon_lines;
        if (arrayList3 != null) {
            i = arrayList3.hashCode();
        }
        return hashCode8 + i;
    }

    public String toString() {
        return "OrderRequest(payment_method=" + this.payment_method + ", transaction_id=" + this.transaction_id + ", customer_id=" + this.customer_id + ", currency=" + this.currency + ", status=" + this.status + ", set_paid=" + this.set_paid + ", billing=" + this.billing + ", shipping=" + this.shipping + ", line_items=" + this.line_items + ", shipping_lines=" + this.shipping_lines + ", coupon_lines=" + this.coupon_lines + ")";
    }

    public OrderRequest(String str, String str2, int i, String str3, String str4, boolean z, Billing billing2, Shipping shipping2, ArrayList<Line_items> arrayList, ArrayList<ShippingLines> arrayList2, ArrayList<CouponLines> arrayList3) {
        Intrinsics.checkParameterIsNotNull(str, "payment_method");
        Intrinsics.checkParameterIsNotNull(str2, FirebaseAnalytics.Param.TRANSACTION_ID);
        Intrinsics.checkParameterIsNotNull(str3, FirebaseAnalytics.Param.CURRENCY);
        Intrinsics.checkParameterIsNotNull(str4, "status");
        this.payment_method = str;
        this.transaction_id = str2;
        this.customer_id = i;
        this.currency = str3;
        this.status = str4;
        this.set_paid = z;
        this.billing = billing2;
        this.shipping = shipping2;
        this.line_items = arrayList;
        this.shipping_lines = arrayList2;
        this.coupon_lines = arrayList3;
    }

    public final String getPayment_method() {
        return this.payment_method;
    }

    public final void setPayment_method(String str) {
        Intrinsics.checkParameterIsNotNull(str, "<set-?>");
        this.payment_method = str;
    }

    public final String getTransaction_id() {
        return this.transaction_id;
    }

    public final void setTransaction_id(String str) {
        Intrinsics.checkParameterIsNotNull(str, "<set-?>");
        this.transaction_id = str;
    }

    public final int getCustomer_id() {
        return this.customer_id;
    }

    public final void setCustomer_id(int i) {
        this.customer_id = i;
    }

    public final String getCurrency() {
        return this.currency;
    }

    public final void setCurrency(String str) {
        Intrinsics.checkParameterIsNotNull(str, "<set-?>");
        this.currency = str;
    }

    public final String getStatus() {
        return this.status;
    }

    public final void setStatus(String str) {
        Intrinsics.checkParameterIsNotNull(str, "<set-?>");
        this.status = str;
    }

    public final boolean getSet_paid() {
        return this.set_paid;
    }

    public final void setSet_paid(boolean z) {
        this.set_paid = z;
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* synthetic */ OrderRequest(java.lang.String r13, java.lang.String r14, int r15, java.lang.String r16, java.lang.String r17, boolean r18, com.iqonic.store.models.Billing r19, com.iqonic.store.models.Shipping r20, java.util.ArrayList r21, java.util.ArrayList r22, java.util.ArrayList r23, int r24, kotlin.jvm.internal.DefaultConstructorMarker r25) {
        /*
            r12 = this;
            r0 = r24
            r1 = r0 & 1
            java.lang.String r2 = ""
            if (r1 == 0) goto L_0x000a
            r1 = r2
            goto L_0x000b
        L_0x000a:
            r1 = r13
        L_0x000b:
            r3 = r0 & 2
            if (r3 == 0) goto L_0x0011
            r3 = r2
            goto L_0x0012
        L_0x0011:
            r3 = r14
        L_0x0012:
            r4 = r0 & 4
            if (r4 == 0) goto L_0x0018
            r4 = 0
            goto L_0x0019
        L_0x0018:
            r4 = r15
        L_0x0019:
            r5 = r0 & 8
            if (r5 == 0) goto L_0x001f
            r5 = r2
            goto L_0x0021
        L_0x001f:
            r5 = r16
        L_0x0021:
            r6 = r0 & 16
            if (r6 == 0) goto L_0x0026
            goto L_0x0028
        L_0x0026:
            r2 = r17
        L_0x0028:
            r6 = r0 & 32
            if (r6 == 0) goto L_0x002e
            r6 = 1
            goto L_0x0030
        L_0x002e:
            r6 = r18
        L_0x0030:
            r7 = r0 & 64
            r8 = 0
            if (r7 == 0) goto L_0x0039
            r7 = r8
            com.iqonic.store.models.Billing r7 = (com.iqonic.store.models.Billing) r7
            goto L_0x003b
        L_0x0039:
            r7 = r19
        L_0x003b:
            r9 = r0 & 128(0x80, float:1.794E-43)
            if (r9 == 0) goto L_0x0043
            r9 = r8
            com.iqonic.store.models.Shipping r9 = (com.iqonic.store.models.Shipping) r9
            goto L_0x0045
        L_0x0043:
            r9 = r20
        L_0x0045:
            r10 = r0 & 256(0x100, float:3.59E-43)
            if (r10 == 0) goto L_0x004d
            r10 = r8
            java.util.ArrayList r10 = (java.util.ArrayList) r10
            goto L_0x004f
        L_0x004d:
            r10 = r21
        L_0x004f:
            r11 = r0 & 512(0x200, float:7.175E-43)
            if (r11 == 0) goto L_0x0057
            r11 = r8
            java.util.ArrayList r11 = (java.util.ArrayList) r11
            goto L_0x0059
        L_0x0057:
            r11 = r22
        L_0x0059:
            r0 = r0 & 1024(0x400, float:1.435E-42)
            if (r0 == 0) goto L_0x0061
            r0 = r8
            java.util.ArrayList r0 = (java.util.ArrayList) r0
            goto L_0x0063
        L_0x0061:
            r0 = r23
        L_0x0063:
            r13 = r12
            r14 = r1
            r15 = r3
            r16 = r4
            r17 = r5
            r18 = r2
            r19 = r6
            r20 = r7
            r21 = r9
            r22 = r10
            r23 = r11
            r24 = r0
            r13.<init>(r14, r15, r16, r17, r18, r19, r20, r21, r22, r23, r24)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.iqonic.store.models.OrderRequest.<init>(java.lang.String, java.lang.String, int, java.lang.String, java.lang.String, boolean, com.iqonic.store.models.Billing, com.iqonic.store.models.Shipping, java.util.ArrayList, java.util.ArrayList, java.util.ArrayList, int, kotlin.jvm.internal.DefaultConstructorMarker):void");
    }

    public final Billing getBilling() {
        return this.billing;
    }

    public final void setBilling(Billing billing2) {
        this.billing = billing2;
    }

    public final Shipping getShipping() {
        return this.shipping;
    }

    public final void setShipping(Shipping shipping2) {
        this.shipping = shipping2;
    }

    public final ArrayList<Line_items> getLine_items() {
        return this.line_items;
    }

    public final void setLine_items(ArrayList<Line_items> arrayList) {
        this.line_items = arrayList;
    }

    public final ArrayList<ShippingLines> getShipping_lines() {
        return this.shipping_lines;
    }

    public final void setShipping_lines(ArrayList<ShippingLines> arrayList) {
        this.shipping_lines = arrayList;
    }

    public final ArrayList<CouponLines> getCoupon_lines() {
        return this.coupon_lines;
    }

    public final void setCoupon_lines(ArrayList<CouponLines> arrayList) {
        this.coupon_lines = arrayList;
    }
}
