package com.iqonic.store.models;

import java.io.Serializable;
import kotlin.Metadata;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\f\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B\u0019\u0012\b\b\u0002\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0002\u0010\u0004\u001a\u00020\u0003¢\u0006\u0002\u0010\u0005J\t\u0010\f\u001a\u00020\u0003HÆ\u0003J\t\u0010\r\u001a\u00020\u0003HÆ\u0003J\u001d\u0010\u000e\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u0003HÆ\u0001J\u0013\u0010\u000f\u001a\u00020\u00102\b\u0010\u0011\u001a\u0004\u0018\u00010\u0012HÖ\u0003J\t\u0010\u0013\u001a\u00020\u0003HÖ\u0001J\t\u0010\u0014\u001a\u00020\u0015HÖ\u0001R\u001a\u0010\u0002\u001a\u00020\u0003X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0006\u0010\u0007\"\u0004\b\b\u0010\tR\u001a\u0010\u0004\u001a\u00020\u0003X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\n\u0010\u0007\"\u0004\b\u000b\u0010\t¨\u0006\u0016"}, d2 = {"Lcom/iqonic/store/models/Line_items;", "Ljava/io/Serializable;", "product_id", "", "quantity", "(II)V", "getProduct_id", "()I", "setProduct_id", "(I)V", "getQuantity", "setQuantity", "component1", "component2", "copy", "equals", "", "other", "", "hashCode", "toString", "", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: Line_items.kt */
public final class Line_items implements Serializable {
    private int product_id;
    private int quantity;

    public Line_items() {
        this(0, 0, 3, (DefaultConstructorMarker) null);
    }

    public static /* synthetic */ Line_items copy$default(Line_items line_items, int i, int i2, int i3, Object obj) {
        if ((i3 & 1) != 0) {
            i = line_items.product_id;
        }
        if ((i3 & 2) != 0) {
            i2 = line_items.quantity;
        }
        return line_items.copy(i, i2);
    }

    public final int component1() {
        return this.product_id;
    }

    public final int component2() {
        return this.quantity;
    }

    public final Line_items copy(int i, int i2) {
        return new Line_items(i, i2);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Line_items)) {
            return false;
        }
        Line_items line_items = (Line_items) obj;
        return this.product_id == line_items.product_id && this.quantity == line_items.quantity;
    }

    public int hashCode() {
        return (this.product_id * 31) + this.quantity;
    }

    public String toString() {
        return "Line_items(product_id=" + this.product_id + ", quantity=" + this.quantity + ")";
    }

    public Line_items(int i, int i2) {
        this.product_id = i;
        this.quantity = i2;
    }

    public final int getProduct_id() {
        return this.product_id;
    }

    public final void setProduct_id(int i) {
        this.product_id = i;
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ Line_items(int i, int i2, int i3, DefaultConstructorMarker defaultConstructorMarker) {
        this((i3 & 1) != 0 ? 0 : i, (i3 & 2) != 0 ? 0 : i2);
    }

    public final int getQuantity() {
        return this.quantity;
    }

    public final void setQuantity(int i) {
        this.quantity = i;
    }
}
