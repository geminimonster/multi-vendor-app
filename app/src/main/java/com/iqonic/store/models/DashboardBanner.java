package com.iqonic.store.models;

import com.facebook.share.internal.MessengerShareContentUtility;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u000f\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\b\u0018\u00002\u00020\u0001B%\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0003\u0012\u0006\u0010\u0006\u001a\u00020\u0003¢\u0006\u0002\u0010\u0007J\t\u0010\r\u001a\u00020\u0003HÆ\u0003J\t\u0010\u000e\u001a\u00020\u0003HÆ\u0003J\t\u0010\u000f\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0010\u001a\u00020\u0003HÆ\u0003J1\u0010\u0011\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00032\b\b\u0002\u0010\u0005\u001a\u00020\u00032\b\b\u0002\u0010\u0006\u001a\u00020\u0003HÆ\u0001J\u0013\u0010\u0012\u001a\u00020\u00132\b\u0010\u0014\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0015\u001a\u00020\u0016HÖ\u0001J\t\u0010\u0017\u001a\u00020\u0003HÖ\u0001R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u0011\u0010\u0004\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\tR\u0011\u0010\u0005\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\tR\u0011\u0010\u0006\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\t¨\u0006\u0018"}, d2 = {"Lcom/iqonic/store/models/DashboardBanner;", "", "desc", "", "image", "thumb", "url", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", "getDesc", "()Ljava/lang/String;", "getImage", "getThumb", "getUrl", "component1", "component2", "component3", "component4", "copy", "equals", "", "other", "hashCode", "", "toString", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: Dashboard.kt */
public final class DashboardBanner {
    private final String desc;
    private final String image;
    private final String thumb;
    private final String url;

    public static /* synthetic */ DashboardBanner copy$default(DashboardBanner dashboardBanner, String str, String str2, String str3, String str4, int i, Object obj) {
        if ((i & 1) != 0) {
            str = dashboardBanner.desc;
        }
        if ((i & 2) != 0) {
            str2 = dashboardBanner.image;
        }
        if ((i & 4) != 0) {
            str3 = dashboardBanner.thumb;
        }
        if ((i & 8) != 0) {
            str4 = dashboardBanner.url;
        }
        return dashboardBanner.copy(str, str2, str3, str4);
    }

    public final String component1() {
        return this.desc;
    }

    public final String component2() {
        return this.image;
    }

    public final String component3() {
        return this.thumb;
    }

    public final String component4() {
        return this.url;
    }

    public final DashboardBanner copy(String str, String str2, String str3, String str4) {
        Intrinsics.checkParameterIsNotNull(str, "desc");
        Intrinsics.checkParameterIsNotNull(str2, MessengerShareContentUtility.MEDIA_IMAGE);
        Intrinsics.checkParameterIsNotNull(str3, "thumb");
        Intrinsics.checkParameterIsNotNull(str4, "url");
        return new DashboardBanner(str, str2, str3, str4);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof DashboardBanner)) {
            return false;
        }
        DashboardBanner dashboardBanner = (DashboardBanner) obj;
        return Intrinsics.areEqual((Object) this.desc, (Object) dashboardBanner.desc) && Intrinsics.areEqual((Object) this.image, (Object) dashboardBanner.image) && Intrinsics.areEqual((Object) this.thumb, (Object) dashboardBanner.thumb) && Intrinsics.areEqual((Object) this.url, (Object) dashboardBanner.url);
    }

    public int hashCode() {
        String str = this.desc;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.image;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.thumb;
        int hashCode3 = (hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31;
        String str4 = this.url;
        if (str4 != null) {
            i = str4.hashCode();
        }
        return hashCode3 + i;
    }

    public String toString() {
        return "DashboardBanner(desc=" + this.desc + ", image=" + this.image + ", thumb=" + this.thumb + ", url=" + this.url + ")";
    }

    public DashboardBanner(String str, String str2, String str3, String str4) {
        Intrinsics.checkParameterIsNotNull(str, "desc");
        Intrinsics.checkParameterIsNotNull(str2, MessengerShareContentUtility.MEDIA_IMAGE);
        Intrinsics.checkParameterIsNotNull(str3, "thumb");
        Intrinsics.checkParameterIsNotNull(str4, "url");
        this.desc = str;
        this.image = str2;
        this.thumb = str3;
        this.url = str4;
    }

    public final String getDesc() {
        return this.desc;
    }

    public final String getImage() {
        return this.image;
    }

    public final String getThumb() {
        return this.thumb;
    }

    public final String getUrl() {
        return this.url;
    }
}
