package com.iqonic.store.models;

import java.util.List;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0015\n\u0002\u0010\u000b\n\u0002\b\u0004\b\b\u0018\u00002\u00020\u0001BC\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0005\u0012\u0006\u0010\u0007\u001a\u00020\u0005\u0012\u0006\u0010\b\u001a\u00020\u0005\u0012\f\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u000b0\n\u0012\u0006\u0010\f\u001a\u00020\u0005¢\u0006\u0002\u0010\rJ\t\u0010\u0018\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0019\u001a\u00020\u0005HÆ\u0003J\t\u0010\u001a\u001a\u00020\u0005HÆ\u0003J\t\u0010\u001b\u001a\u00020\u0005HÆ\u0003J\t\u0010\u001c\u001a\u00020\u0005HÆ\u0003J\u000f\u0010\u001d\u001a\b\u0012\u0004\u0012\u00020\u000b0\nHÆ\u0003J\t\u0010\u001e\u001a\u00020\u0005HÆ\u0003JU\u0010\u001f\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u00052\b\b\u0002\u0010\u0007\u001a\u00020\u00052\b\b\u0002\u0010\b\u001a\u00020\u00052\u000e\b\u0002\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u000b0\n2\b\b\u0002\u0010\f\u001a\u00020\u0005HÆ\u0001J\u0013\u0010 \u001a\u00020!2\b\u0010\"\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010#\u001a\u00020\u0003HÖ\u0001J\t\u0010$\u001a\u00020\u0005HÖ\u0001R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u000fR\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u0011R\u0011\u0010\u0006\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u0011R\u0011\u0010\u0007\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u0011R\u0011\u0010\b\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0014\u0010\u0011R\u0017\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u000b0\n¢\u0006\b\n\u0000\u001a\u0004\b\u0015\u0010\u0016R\u0011\u0010\f\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0017\u0010\u0011¨\u0006%"}, d2 = {"Lcom/iqonic/store/models/StoreAttribute;", "", "has_archives", "", "id", "", "name", "order_by", "slug", "terms", "", "Lcom/iqonic/store/models/Term;", "type", "(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)V", "getHas_archives", "()I", "getId", "()Ljava/lang/String;", "getName", "getOrder_by", "getSlug", "getTerms", "()Ljava/util/List;", "getType", "component1", "component2", "component3", "component4", "component5", "component6", "component7", "copy", "equals", "", "other", "hashCode", "toString", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: BaseResponse.kt */
public final class StoreAttribute {
    private final int has_archives;
    private final String id;
    private final String name;
    private final String order_by;
    private final String slug;
    private final List<Term> terms;
    private final String type;

    public static /* synthetic */ StoreAttribute copy$default(StoreAttribute storeAttribute, int i, String str, String str2, String str3, String str4, List<Term> list, String str5, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            i = storeAttribute.has_archives;
        }
        if ((i2 & 2) != 0) {
            str = storeAttribute.id;
        }
        String str6 = str;
        if ((i2 & 4) != 0) {
            str2 = storeAttribute.name;
        }
        String str7 = str2;
        if ((i2 & 8) != 0) {
            str3 = storeAttribute.order_by;
        }
        String str8 = str3;
        if ((i2 & 16) != 0) {
            str4 = storeAttribute.slug;
        }
        String str9 = str4;
        if ((i2 & 32) != 0) {
            list = storeAttribute.terms;
        }
        List<Term> list2 = list;
        if ((i2 & 64) != 0) {
            str5 = storeAttribute.type;
        }
        return storeAttribute.copy(i, str6, str7, str8, str9, list2, str5);
    }

    public final int component1() {
        return this.has_archives;
    }

    public final String component2() {
        return this.id;
    }

    public final String component3() {
        return this.name;
    }

    public final String component4() {
        return this.order_by;
    }

    public final String component5() {
        return this.slug;
    }

    public final List<Term> component6() {
        return this.terms;
    }

    public final String component7() {
        return this.type;
    }

    public final StoreAttribute copy(int i, String str, String str2, String str3, String str4, List<Term> list, String str5) {
        Intrinsics.checkParameterIsNotNull(str, "id");
        Intrinsics.checkParameterIsNotNull(str2, "name");
        Intrinsics.checkParameterIsNotNull(str3, "order_by");
        Intrinsics.checkParameterIsNotNull(str4, "slug");
        Intrinsics.checkParameterIsNotNull(list, "terms");
        String str6 = str5;
        Intrinsics.checkParameterIsNotNull(str6, "type");
        return new StoreAttribute(i, str, str2, str3, str4, list, str6);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof StoreAttribute)) {
            return false;
        }
        StoreAttribute storeAttribute = (StoreAttribute) obj;
        return this.has_archives == storeAttribute.has_archives && Intrinsics.areEqual((Object) this.id, (Object) storeAttribute.id) && Intrinsics.areEqual((Object) this.name, (Object) storeAttribute.name) && Intrinsics.areEqual((Object) this.order_by, (Object) storeAttribute.order_by) && Intrinsics.areEqual((Object) this.slug, (Object) storeAttribute.slug) && Intrinsics.areEqual((Object) this.terms, (Object) storeAttribute.terms) && Intrinsics.areEqual((Object) this.type, (Object) storeAttribute.type);
    }

    public int hashCode() {
        int i = this.has_archives * 31;
        String str = this.id;
        int i2 = 0;
        int hashCode = (i + (str != null ? str.hashCode() : 0)) * 31;
        String str2 = this.name;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.order_by;
        int hashCode3 = (hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31;
        String str4 = this.slug;
        int hashCode4 = (hashCode3 + (str4 != null ? str4.hashCode() : 0)) * 31;
        List<Term> list = this.terms;
        int hashCode5 = (hashCode4 + (list != null ? list.hashCode() : 0)) * 31;
        String str5 = this.type;
        if (str5 != null) {
            i2 = str5.hashCode();
        }
        return hashCode5 + i2;
    }

    public String toString() {
        return "StoreAttribute(has_archives=" + this.has_archives + ", id=" + this.id + ", name=" + this.name + ", order_by=" + this.order_by + ", slug=" + this.slug + ", terms=" + this.terms + ", type=" + this.type + ")";
    }

    public StoreAttribute(int i, String str, String str2, String str3, String str4, List<Term> list, String str5) {
        Intrinsics.checkParameterIsNotNull(str, "id");
        Intrinsics.checkParameterIsNotNull(str2, "name");
        Intrinsics.checkParameterIsNotNull(str3, "order_by");
        Intrinsics.checkParameterIsNotNull(str4, "slug");
        Intrinsics.checkParameterIsNotNull(list, "terms");
        Intrinsics.checkParameterIsNotNull(str5, "type");
        this.has_archives = i;
        this.id = str;
        this.name = str2;
        this.order_by = str3;
        this.slug = str4;
        this.terms = list;
        this.type = str5;
    }

    public final int getHas_archives() {
        return this.has_archives;
    }

    public final String getId() {
        return this.id;
    }

    public final String getName() {
        return this.name;
    }

    public final String getOrder_by() {
        return this.order_by;
    }

    public final String getSlug() {
        return this.slug;
    }

    public final List<Term> getTerms() {
        return this.terms;
    }

    public final String getType() {
        return this.type;
    }
}
