package com.iqonic.store.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.ArrayList;
import kotlin.Metadata;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\b\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002R2\u0010\u0003\u001a\u0016\u0012\u0004\u0012\u00020\u0005\u0018\u00010\u0004j\n\u0012\u0004\u0012\u00020\u0005\u0018\u0001`\u00068\u0006@\u0006X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0007\u0010\b\"\u0004\b\t\u0010\nR\u001e\u0010\u000b\u001a\u00020\f8\u0006@\u0006X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\r\u0010\u000e\"\u0004\b\u000f\u0010\u0010¨\u0006\u0011"}, d2 = {"Lcom/iqonic/store/models/StoreProductModelNew;", "", "()V", "data", "Ljava/util/ArrayList;", "Lcom/iqonic/store/models/StoreProductModel;", "Lkotlin/collections/ArrayList;", "getData", "()Ljava/util/ArrayList;", "setData", "(Ljava/util/ArrayList;)V", "numOfPages", "", "getNumOfPages", "()I", "setNumOfPages", "(I)V", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: StoreProductModelNew.kt */
public final class StoreProductModelNew {
    @SerializedName("data")
    @Expose
    private ArrayList<StoreProductModel> data;
    @SerializedName("num_of_pages")
    @Expose
    private int numOfPages;

    public final int getNumOfPages() {
        return this.numOfPages;
    }

    public final void setNumOfPages(int i) {
        this.numOfPages = i;
    }

    public final ArrayList<StoreProductModel> getData() {
        return this.data;
    }

    public final void setData(ArrayList<StoreProductModel> arrayList) {
        this.data = arrayList;
    }
}
