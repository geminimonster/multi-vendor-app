package com.iqonic.store.models;

import java.io.Serializable;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0017\n\u0002\u0010\u0000\n\u0002\b\u0003\b\b\u0018\u00002\u00020\u0001B=\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\u0005\u0012\u0006\u0010\t\u001a\u00020\u0005\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\f\u001a\u00020\u0005¢\u0006\u0002\u0010\rJ\t\u0010\u0019\u001a\u00020\u0003HÆ\u0003J\t\u0010\u001a\u001a\u00020\u0005HÆ\u0003J\t\u0010\u001b\u001a\u00020\u0007HÆ\u0003J\t\u0010\u001c\u001a\u00020\u0005HÆ\u0003J\t\u0010\u001d\u001a\u00020\u0005HÆ\u0003J\t\u0010\u001e\u001a\u00020\u000bHÆ\u0003J\t\u0010\u001f\u001a\u00020\u0005HÆ\u0003JO\u0010 \u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u00072\b\b\u0002\u0010\b\u001a\u00020\u00052\b\b\u0002\u0010\t\u001a\u00020\u00052\b\b\u0002\u0010\n\u001a\u00020\u000b2\b\b\u0002\u0010\f\u001a\u00020\u0005HÆ\u0001J\u0013\u0010!\u001a\u00020\u00072\b\u0010\"\u001a\u0004\u0018\u00010#HÖ\u0003J\t\u0010$\u001a\u00020\u000bHÖ\u0001J\t\u0010%\u001a\u00020\u0005HÖ\u0001R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u000fR\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u0011R\u0011\u0010\u0006\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u0013R\u0011\u0010\b\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0014\u0010\u0011R\u0011\u0010\t\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0015\u0010\u0011R\u0011\u0010\n\u001a\u00020\u000b¢\u0006\b\n\u0000\u001a\u0004\b\u0016\u0010\u0017R\u0011\u0010\f\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0018\u0010\u0011¨\u0006&"}, d2 = {"Lcom/iqonic/store/models/OrderNotes;", "Ljava/io/Serializable;", "_links", "Lcom/iqonic/store/models/Links;", "author", "", "customer_note", "", "date_created", "date_created_gmt", "id", "", "note", "(Lcom/iqonic/store/models/Links;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;ILjava/lang/String;)V", "get_links", "()Lcom/iqonic/store/models/Links;", "getAuthor", "()Ljava/lang/String;", "getCustomer_note", "()Z", "getDate_created", "getDate_created_gmt", "getId", "()I", "getNote", "component1", "component2", "component3", "component4", "component5", "component6", "component7", "copy", "equals", "other", "", "hashCode", "toString", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: BaseResponse.kt */
public final class OrderNotes implements Serializable {
    private final Links _links;
    private final String author;
    private final boolean customer_note;
    private final String date_created;
    private final String date_created_gmt;
    private final int id;
    private final String note;

    public static /* synthetic */ OrderNotes copy$default(OrderNotes orderNotes, Links links, String str, boolean z, String str2, String str3, int i, String str4, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            links = orderNotes._links;
        }
        if ((i2 & 2) != 0) {
            str = orderNotes.author;
        }
        String str5 = str;
        if ((i2 & 4) != 0) {
            z = orderNotes.customer_note;
        }
        boolean z2 = z;
        if ((i2 & 8) != 0) {
            str2 = orderNotes.date_created;
        }
        String str6 = str2;
        if ((i2 & 16) != 0) {
            str3 = orderNotes.date_created_gmt;
        }
        String str7 = str3;
        if ((i2 & 32) != 0) {
            i = orderNotes.id;
        }
        int i3 = i;
        if ((i2 & 64) != 0) {
            str4 = orderNotes.note;
        }
        return orderNotes.copy(links, str5, z2, str6, str7, i3, str4);
    }

    public final Links component1() {
        return this._links;
    }

    public final String component2() {
        return this.author;
    }

    public final boolean component3() {
        return this.customer_note;
    }

    public final String component4() {
        return this.date_created;
    }

    public final String component5() {
        return this.date_created_gmt;
    }

    public final int component6() {
        return this.id;
    }

    public final String component7() {
        return this.note;
    }

    public final OrderNotes copy(Links links, String str, boolean z, String str2, String str3, int i, String str4) {
        Intrinsics.checkParameterIsNotNull(links, "_links");
        Intrinsics.checkParameterIsNotNull(str, "author");
        Intrinsics.checkParameterIsNotNull(str2, "date_created");
        Intrinsics.checkParameterIsNotNull(str3, "date_created_gmt");
        String str5 = str4;
        Intrinsics.checkParameterIsNotNull(str5, "note");
        return new OrderNotes(links, str, z, str2, str3, i, str5);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof OrderNotes)) {
            return false;
        }
        OrderNotes orderNotes = (OrderNotes) obj;
        return Intrinsics.areEqual((Object) this._links, (Object) orderNotes._links) && Intrinsics.areEqual((Object) this.author, (Object) orderNotes.author) && this.customer_note == orderNotes.customer_note && Intrinsics.areEqual((Object) this.date_created, (Object) orderNotes.date_created) && Intrinsics.areEqual((Object) this.date_created_gmt, (Object) orderNotes.date_created_gmt) && this.id == orderNotes.id && Intrinsics.areEqual((Object) this.note, (Object) orderNotes.note);
    }

    public int hashCode() {
        Links links = this._links;
        int i = 0;
        int hashCode = (links != null ? links.hashCode() : 0) * 31;
        String str = this.author;
        int hashCode2 = (hashCode + (str != null ? str.hashCode() : 0)) * 31;
        boolean z = this.customer_note;
        if (z) {
            z = true;
        }
        int i2 = (hashCode2 + (z ? 1 : 0)) * 31;
        String str2 = this.date_created;
        int hashCode3 = (i2 + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.date_created_gmt;
        int hashCode4 = (((hashCode3 + (str3 != null ? str3.hashCode() : 0)) * 31) + this.id) * 31;
        String str4 = this.note;
        if (str4 != null) {
            i = str4.hashCode();
        }
        return hashCode4 + i;
    }

    public String toString() {
        return "OrderNotes(_links=" + this._links + ", author=" + this.author + ", customer_note=" + this.customer_note + ", date_created=" + this.date_created + ", date_created_gmt=" + this.date_created_gmt + ", id=" + this.id + ", note=" + this.note + ")";
    }

    public OrderNotes(Links links, String str, boolean z, String str2, String str3, int i, String str4) {
        Intrinsics.checkParameterIsNotNull(links, "_links");
        Intrinsics.checkParameterIsNotNull(str, "author");
        Intrinsics.checkParameterIsNotNull(str2, "date_created");
        Intrinsics.checkParameterIsNotNull(str3, "date_created_gmt");
        Intrinsics.checkParameterIsNotNull(str4, "note");
        this._links = links;
        this.author = str;
        this.customer_note = z;
        this.date_created = str2;
        this.date_created_gmt = str3;
        this.id = i;
        this.note = str4;
    }

    public final Links get_links() {
        return this._links;
    }

    public final String getAuthor() {
        return this.author;
    }

    public final boolean getCustomer_note() {
        return this.customer_note;
    }

    public final String getDate_created() {
        return this.date_created;
    }

    public final String getDate_created_gmt() {
        return this.date_created_gmt;
    }

    public final int getId() {
        return this.id;
    }

    public final String getNote() {
        return this.note;
    }
}
