package com.iqonic.store.models;

import java.util.List;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0010\u000e\n\u0002\b\b\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002R \u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0006\u0010\u0007\"\u0004\b\b\u0010\tR \u0010\n\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u000b\u0010\u0007\"\u0004\b\f\u0010\t¨\u0006\r"}, d2 = {"Lcom/iqonic/store/models/Images;", "", "()V", "gallery", "", "", "getGallery", "()Ljava/util/List;", "setGallery", "(Ljava/util/List;)V", "image", "getImage", "setImage", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: SearchProduct.kt */
public final class Images {
    private List<String> gallery = CollectionsKt.emptyList();
    private List<String> image = CollectionsKt.emptyList();

    public final List<String> getGallery() {
        return this.gallery;
    }

    public final void setGallery(List<String> list) {
        Intrinsics.checkParameterIsNotNull(list, "<set-?>");
        this.gallery = list;
    }

    public final List<String> getImage() {
        return this.image;
    }

    public final void setImage(List<String> list) {
        Intrinsics.checkParameterIsNotNull(list, "<set-?>");
        this.image = list;
    }
}
