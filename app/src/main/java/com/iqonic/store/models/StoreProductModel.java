package com.iqonic.store.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import java.util.List;
import kotlin.Metadata;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000`\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0002\b\u0012\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0000\n\u0002\b\u001a\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\b\n\u0002\b\u001d\n\u0002\u0018\u0002\n\u0002\b\f\n\u0002\u0018\u0002\n\u0002\bh\n\u0002\u0018\u0002\n\u0002\b\f\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002R&\u0010\u0003\u001a\n\u0012\u0004\u0012\u00020\u0005\u0018\u00010\u00048\u0006@\u0006X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0006\u0010\u0007\"\u0004\b\b\u0010\tR \u0010\n\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\f\u0010\r\"\u0004\b\u000e\u0010\u000fR\"\u0010\u0010\u001a\u0004\u0018\u00010\u00118\u0006@\u0006X\u000e¢\u0006\u0010\n\u0002\u0010\u0016\u001a\u0004\b\u0012\u0010\u0013\"\u0004\b\u0014\u0010\u0015R \u0010\u0017\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0018\u0010\r\"\u0004\b\u0019\u0010\u000fR\"\u0010\u001a\u001a\u0004\u0018\u00010\u00118\u0006@\u0006X\u000e¢\u0006\u0010\n\u0002\u0010\u0016\u001a\u0004\b\u001b\u0010\u0013\"\u0004\b\u001c\u0010\u0015R \u0010\u001d\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u001e\u0010\r\"\u0004\b\u001f\u0010\u000fR \u0010 \u001a\u0004\u0018\u00010\u000b8\u0006@\u0006X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b!\u0010\r\"\u0004\b\"\u0010\u000fR&\u0010#\u001a\n\u0012\u0004\u0012\u00020$\u0018\u00010\u00048\u0006@\u0006X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b%\u0010\u0007\"\u0004\b&\u0010\tR&\u0010'\u001a\n\u0012\u0004\u0012\u00020(\u0018\u00010\u00048\u0006@\u0006X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b)\u0010\u0007\"\u0004\b*\u0010\tR \u0010+\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b,\u0010\r\"\u0004\b-\u0010\u000fR \u0010.\u001a\u0004\u0018\u00010(8\u0006@\u0006X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b/\u00100\"\u0004\b1\u00102R \u00103\u001a\u0004\u0018\u00010(8\u0006@\u0006X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b4\u00100\"\u0004\b5\u00102R \u00106\u001a\u0004\u0018\u00010(8\u0006@\u0006X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b7\u00100\"\u0004\b8\u00102R \u00109\u001a\u0004\u0018\u00010(8\u0006@\u0006X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b:\u00100\"\u0004\b;\u00102R&\u0010<\u001a\n\u0012\u0004\u0012\u00020(\u0018\u00010\u00048\u0006@\u0006X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b=\u0010\u0007\"\u0004\b>\u0010\tR \u0010?\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b@\u0010\r\"\u0004\bA\u0010\u000fR \u0010B\u001a\u0004\u0018\u00010C8\u0006@\u0006X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\bD\u0010E\"\u0004\bF\u0010GR\"\u0010H\u001a\u0004\u0018\u00010I8\u0006@\u0006X\u000e¢\u0006\u0010\n\u0002\u0010N\u001a\u0004\bJ\u0010K\"\u0004\bL\u0010MR\"\u0010O\u001a\u0004\u0018\u00010I8\u0006@\u0006X\u000e¢\u0006\u0010\n\u0002\u0010N\u001a\u0004\bP\u0010K\"\u0004\bQ\u0010MR\"\u0010R\u001a\u0004\u0018\u00010\u00118\u0006@\u0006X\u000e¢\u0006\u0010\n\u0002\u0010\u0016\u001a\u0004\bS\u0010\u0013\"\u0004\bT\u0010\u0015R&\u0010U\u001a\n\u0012\u0004\u0012\u00020(\u0018\u00010\u00048\u0006@\u0006X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\bV\u0010\u0007\"\u0004\bW\u0010\tR \u0010X\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\bY\u0010\r\"\u0004\bZ\u0010\u000fR\"\u0010[\u001a\u0004\u0018\u00010\u00118\u0006@\u0006X\u000e¢\u0006\u0010\n\u0002\u0010\u0016\u001a\u0004\b\\\u0010\u0013\"\u0004\b]\u0010\u0015R&\u0010^\u001a\n\u0012\u0004\u0012\u00020(\u0018\u00010\u00048\u0006@\u0006X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b_\u0010\u0007\"\u0004\b`\u0010\tR\u001e\u0010a\u001a\u00020I8\u0006@\u0006X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\bb\u0010c\"\u0004\bd\u0010eR&\u0010f\u001a\n\u0012\u0004\u0012\u00020g\u0018\u00010\u00048\u0006@\u0006X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\bh\u0010\u0007\"\u0004\bi\u0010\tR\u001e\u0010j\u001a\u00020\u00118\u0006@\u0006X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\bk\u0010l\"\u0004\bm\u0010nR\u001e\u0010o\u001a\u00020\u00118\u0006@\u0006X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\bo\u0010l\"\u0004\bp\u0010nR\u001e\u0010q\u001a\u00020\u00118\u0006@\u0006X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\bq\u0010l\"\u0004\br\u0010nR \u0010s\u001a\u0004\u0018\u00010t8\u0006@\u0006X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\bu\u0010v\"\u0004\bw\u0010xR\"\u0010y\u001a\u0004\u0018\u00010\u00118\u0006@\u0006X\u000e¢\u0006\u0010\n\u0002\u0010\u0016\u001a\u0004\bz\u0010\u0013\"\u0004\b{\u0010\u0015R\"\u0010|\u001a\u0004\u0018\u00010I8\u0006@\u0006X\u000e¢\u0006\u0010\n\u0002\u0010N\u001a\u0004\b}\u0010K\"\u0004\b~\u0010MR(\u0010\u001a\n\u0012\u0004\u0012\u00020(\u0018\u00010\u00048\u0006@\u0006X\u000e¢\u0006\u0010\n\u0000\u001a\u0005\b\u0001\u0010\u0007\"\u0005\b\u0001\u0010\tR#\u0010\u0001\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006X\u000e¢\u0006\u0010\n\u0000\u001a\u0005\b\u0001\u0010\r\"\u0005\b\u0001\u0010\u000fR!\u0010\u0001\u001a\u00020\u00118\u0006@\u0006X\u000e¢\u0006\u0010\n\u0000\u001a\u0005\b\u0001\u0010l\"\u0005\b\u0001\u0010nR%\u0010\u0001\u001a\u0004\u0018\u00010I8\u0006@\u0006X\u000e¢\u0006\u0012\n\u0002\u0010N\u001a\u0005\b\u0001\u0010K\"\u0005\b\u0001\u0010MR#\u0010\u0001\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006X\u000e¢\u0006\u0010\n\u0000\u001a\u0005\b\u0001\u0010\r\"\u0005\b\u0001\u0010\u000fR#\u0010\u0001\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006X\u000e¢\u0006\u0010\n\u0000\u001a\u0005\b\u0001\u0010\r\"\u0005\b\u0001\u0010\u000fR#\u0010\u0001\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006X\u000e¢\u0006\u0010\n\u0000\u001a\u0005\b\u0001\u0010\r\"\u0005\b\u0001\u0010\u000fR!\u0010\u0001\u001a\u00020\u00118\u0006@\u0006X\u000e¢\u0006\u0010\n\u0000\u001a\u0005\b\u0001\u0010l\"\u0005\b\u0001\u0010nR#\u0010\u0001\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006X\u000e¢\u0006\u0010\n\u0000\u001a\u0005\b\u0001\u0010\r\"\u0005\b\u0001\u0010\u000fR%\u0010\u0001\u001a\u0004\u0018\u00010I8\u0006@\u0006X\u000e¢\u0006\u0012\n\u0002\u0010N\u001a\u0005\b\u0001\u0010K\"\u0005\b\u0001\u0010MR#\u0010\u0001\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006X\u000e¢\u0006\u0010\n\u0000\u001a\u0005\b\u0001\u0010\r\"\u0005\b\u0001\u0010\u000fR)\u0010 \u0001\u001a\n\u0012\u0004\u0012\u00020I\u0018\u00010\u00048\u0006@\u0006X\u000e¢\u0006\u0010\n\u0000\u001a\u0005\b¡\u0001\u0010\u0007\"\u0005\b¢\u0001\u0010\tR%\u0010£\u0001\u001a\u0004\u0018\u00010\u00118\u0006@\u0006X\u000e¢\u0006\u0012\n\u0002\u0010\u0016\u001a\u0005\b¤\u0001\u0010\u0013\"\u0005\b¥\u0001\u0010\u0015R#\u0010¦\u0001\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006X\u000e¢\u0006\u0010\n\u0000\u001a\u0005\b§\u0001\u0010\r\"\u0005\b¨\u0001\u0010\u000fR#\u0010©\u0001\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006X\u000e¢\u0006\u0010\n\u0000\u001a\u0005\bª\u0001\u0010\r\"\u0005\b«\u0001\u0010\u000fR%\u0010¬\u0001\u001a\u0004\u0018\u00010I8\u0006@\u0006X\u000e¢\u0006\u0012\n\u0002\u0010N\u001a\u0005\b­\u0001\u0010K\"\u0005\b®\u0001\u0010MR%\u0010¯\u0001\u001a\u0004\u0018\u00010\u00118\u0006@\u0006X\u000e¢\u0006\u0012\n\u0002\u0010\u0016\u001a\u0005\b°\u0001\u0010\u0013\"\u0005\b±\u0001\u0010\u0015R%\u0010²\u0001\u001a\u0004\u0018\u00010\u00118\u0006@\u0006X\u000e¢\u0006\u0012\n\u0002\u0010\u0016\u001a\u0005\b³\u0001\u0010\u0013\"\u0005\b´\u0001\u0010\u0015R#\u0010µ\u0001\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006X\u000e¢\u0006\u0010\n\u0000\u001a\u0005\b¶\u0001\u0010\r\"\u0005\b·\u0001\u0010\u000fR#\u0010¸\u0001\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006X\u000e¢\u0006\u0010\n\u0000\u001a\u0005\b¹\u0001\u0010\r\"\u0005\bº\u0001\u0010\u000fR#\u0010»\u0001\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006X\u000e¢\u0006\u0010\n\u0000\u001a\u0005\b¼\u0001\u0010\r\"\u0005\b½\u0001\u0010\u000fR%\u0010¾\u0001\u001a\u0004\u0018\u00010\u00118\u0006@\u0006X\u000e¢\u0006\u0012\n\u0002\u0010\u0016\u001a\u0005\b¿\u0001\u0010\u0013\"\u0005\bÀ\u0001\u0010\u0015R#\u0010Á\u0001\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006X\u000e¢\u0006\u0010\n\u0000\u001a\u0005\bÂ\u0001\u0010\r\"\u0005\bÃ\u0001\u0010\u000fR%\u0010Ä\u0001\u001a\u0004\u0018\u00010I8\u0006@\u0006X\u000e¢\u0006\u0012\n\u0002\u0010N\u001a\u0005\bÅ\u0001\u0010K\"\u0005\bÆ\u0001\u0010MR#\u0010Ç\u0001\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006X\u000e¢\u0006\u0010\n\u0000\u001a\u0005\bÈ\u0001\u0010\r\"\u0005\bÉ\u0001\u0010\u000fR)\u0010Ê\u0001\u001a\n\u0012\u0004\u0012\u00020(\u0018\u00010\u00048\u0006@\u0006X\u000e¢\u0006\u0010\n\u0000\u001a\u0005\bË\u0001\u0010\u0007\"\u0005\bÌ\u0001\u0010\tR#\u0010Í\u0001\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006X\u000e¢\u0006\u0010\n\u0000\u001a\u0005\bÎ\u0001\u0010\r\"\u0005\bÏ\u0001\u0010\u000fR#\u0010Ð\u0001\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006X\u000e¢\u0006\u0010\n\u0000\u001a\u0005\bÑ\u0001\u0010\r\"\u0005\bÒ\u0001\u0010\u000fR%\u0010Ó\u0001\u001a\u0004\u0018\u00010I8\u0006@\u0006X\u000e¢\u0006\u0012\n\u0002\u0010N\u001a\u0005\bÔ\u0001\u0010K\"\u0005\bÕ\u0001\u0010MR#\u0010Ö\u0001\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006X\u000e¢\u0006\u0010\n\u0000\u001a\u0005\b×\u0001\u0010\r\"\u0005\bØ\u0001\u0010\u000fR)\u0010Ù\u0001\u001a\n\u0012\u0004\u0012\u00020I\u0018\u00010\u00048\u0006@\u0006X\u000e¢\u0006\u0010\n\u0000\u001a\u0005\bÚ\u0001\u0010\u0007\"\u0005\bÛ\u0001\u0010\tR*\u0010Ü\u0001\u001a\u000b\u0012\u0005\u0012\u00030Ý\u0001\u0018\u00010\u00048\u0006@\u0006X\u000e¢\u0006\u0010\n\u0000\u001a\u0005\bÞ\u0001\u0010\u0007\"\u0005\bß\u0001\u0010\tR)\u0010à\u0001\u001a\n\u0012\u0004\u0012\u00020I\u0018\u00010\u00048\u0006@\u0006X\u000e¢\u0006\u0010\n\u0000\u001a\u0005\bá\u0001\u0010\u0007\"\u0005\bâ\u0001\u0010\tR%\u0010ã\u0001\u001a\u0004\u0018\u00010\u00118\u0006@\u0006X\u000e¢\u0006\u0012\n\u0002\u0010\u0016\u001a\u0005\bä\u0001\u0010\u0013\"\u0005\bå\u0001\u0010\u0015R#\u0010æ\u0001\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006X\u000e¢\u0006\u0010\n\u0000\u001a\u0005\bç\u0001\u0010\r\"\u0005\bè\u0001\u0010\u000f¨\u0006é\u0001"}, d2 = {"Lcom/iqonic/store/models/StoreProductModel;", "Ljava/io/Serializable;", "()V", "attributes", "", "Lcom/iqonic/store/models/Attributes;", "getAttributes", "()Ljava/util/List;", "setAttributes", "(Ljava/util/List;)V", "averageRating", "", "getAverageRating", "()Ljava/lang/String;", "setAverageRating", "(Ljava/lang/String;)V", "backordered", "", "getBackordered", "()Ljava/lang/Boolean;", "setBackordered", "(Ljava/lang/Boolean;)V", "Ljava/lang/Boolean;", "backorders", "getBackorders", "setBackorders", "backordersAllowed", "getBackordersAllowed", "setBackordersAllowed", "buttonText", "getButtonText", "setButtonText", "catalogVisibility", "getCatalogVisibility", "setCatalogVisibility", "categories", "Lcom/iqonic/store/models/StoreCategory;", "getCategories", "setCategories", "crossSellIds", "", "getCrossSellIds", "setCrossSellIds", "dateModifiedGmt", "getDateModifiedGmt", "setDateModifiedGmt", "dateOnSaleFrom", "getDateOnSaleFrom", "()Ljava/lang/Object;", "setDateOnSaleFrom", "(Ljava/lang/Object;)V", "dateOnSaleFromGmt", "getDateOnSaleFromGmt", "setDateOnSaleFromGmt", "dateOnSaleTo", "getDateOnSaleTo", "setDateOnSaleTo", "dateOnSaleToGmt", "getDateOnSaleToGmt", "setDateOnSaleToGmt", "defaultAttributes", "getDefaultAttributes", "setDefaultAttributes", "description", "getDescription", "setDescription", "dimensions", "Lcom/iqonic/store/models/Dimensions;", "getDimensions", "()Lcom/iqonic/store/models/Dimensions;", "setDimensions", "(Lcom/iqonic/store/models/Dimensions;)V", "downloadExpiry", "", "getDownloadExpiry", "()Ljava/lang/Integer;", "setDownloadExpiry", "(Ljava/lang/Integer;)V", "Ljava/lang/Integer;", "downloadLimit", "getDownloadLimit", "setDownloadLimit", "downloadable", "getDownloadable", "setDownloadable", "downloads", "getDownloads", "setDownloads", "externalUrl", "getExternalUrl", "setExternalUrl", "featured", "getFeatured", "setFeatured", "groupedProducts", "getGroupedProducts", "setGroupedProducts", "id", "getId", "()I", "setId", "(I)V", "images", "Lcom/iqonic/store/models/Image;", "getImages", "setImages", "in_stock", "getIn_stock", "()Z", "setIn_stock", "(Z)V", "is_added_cart", "set_added_cart", "is_added_wishlist", "set_added_wishlist", "links", "Lcom/iqonic/store/models/Links;", "getLinks", "()Lcom/iqonic/store/models/Links;", "setLinks", "(Lcom/iqonic/store/models/Links;)V", "manageStock", "getManageStock", "setManageStock", "menuOrder", "getMenuOrder", "setMenuOrder", "metaData", "getMetaData", "setMetaData", "name", "getName", "setName", "onSale", "getOnSale", "setOnSale", "parentId", "getParentId", "setParentId", "permalink", "getPermalink", "setPermalink", "price", "getPrice", "setPrice", "priceHtml", "getPriceHtml", "setPriceHtml", "purchasable", "getPurchasable", "setPurchasable", "purchaseNote", "getPurchaseNote", "setPurchaseNote", "ratingCount", "getRatingCount", "setRatingCount", "regularPrice", "getRegularPrice", "setRegularPrice", "relatedIds", "getRelatedIds", "setRelatedIds", "reviewsAllowed", "getReviewsAllowed", "setReviewsAllowed", "salePrice", "getSalePrice", "setSalePrice", "shippingClass", "getShippingClass", "setShippingClass", "shippingClassId", "getShippingClassId", "setShippingClassId", "shippingRequired", "getShippingRequired", "setShippingRequired", "shippingTaxable", "getShippingTaxable", "setShippingTaxable", "shortDescription", "getShortDescription", "setShortDescription", "sku", "getSku", "setSku", "slug", "getSlug", "setSlug", "soldIndividually", "getSoldIndividually", "setSoldIndividually", "status", "getStatus", "setStatus", "stockQuantity", "getStockQuantity", "setStockQuantity", "stockStatus", "getStockStatus", "setStockStatus", "tags", "getTags", "setTags", "taxClass", "getTaxClass", "setTaxClass", "taxStatus", "getTaxStatus", "setTaxStatus", "totalSales", "getTotalSales", "setTotalSales", "type", "getType", "setType", "upsellIds", "getUpsellIds", "setUpsellIds", "upsell_id", "Lcom/iqonic/store/models/StoreUpSale;", "getUpsell_id", "setUpsell_id", "variations", "getVariations", "setVariations", "virtual", "getVirtual", "setVirtual", "weight", "getWeight", "setWeight", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: StoreProductModel.kt */
public final class StoreProductModel implements Serializable {
    @SerializedName("attributes")
    @Expose
    private List<Attributes> attributes;
    @SerializedName("average_rating")
    @Expose
    private String averageRating;
    @SerializedName("backordered")
    @Expose
    private Boolean backordered;
    @SerializedName("backorders")
    @Expose
    private String backorders;
    @SerializedName("backorders_allowed")
    @Expose
    private Boolean backordersAllowed;
    @SerializedName("button_text")
    @Expose
    private String buttonText;
    @SerializedName("catalog_visibility")
    @Expose
    private String catalogVisibility;
    @SerializedName("categories")
    @Expose
    private List<StoreCategory> categories;
    @SerializedName("cross_sell_ids")
    @Expose
    private List<? extends Object> crossSellIds;
    @SerializedName("date_modified_gmt")
    @Expose
    private String dateModifiedGmt;
    @SerializedName("date_on_sale_from")
    @Expose
    private Object dateOnSaleFrom;
    @SerializedName("date_on_sale_from_gmt")
    @Expose
    private Object dateOnSaleFromGmt;
    @SerializedName("date_on_sale_to")
    @Expose
    private Object dateOnSaleTo;
    @SerializedName("date_on_sale_to_gmt")
    @Expose
    private Object dateOnSaleToGmt;
    @SerializedName("default_attributes")
    @Expose
    private List<? extends Object> defaultAttributes;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("dimensions")
    @Expose
    private Dimensions dimensions;
    @SerializedName("download_expiry")
    @Expose
    private Integer downloadExpiry;
    @SerializedName("download_limit")
    @Expose
    private Integer downloadLimit;
    @SerializedName("downloadable")
    @Expose
    private Boolean downloadable;
    @SerializedName("downloads")
    @Expose
    private List<? extends Object> downloads;
    @SerializedName("external_url")
    @Expose
    private String externalUrl;
    @SerializedName("featured")
    @Expose
    private Boolean featured;
    @SerializedName("grouped_products")
    @Expose
    private List<? extends Object> groupedProducts;
    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("images")
    @Expose
    private List<Image> images;
    @SerializedName("in_stock")
    @Expose
    private boolean in_stock;
    @SerializedName("is_added_cart")
    @Expose
    private boolean is_added_cart;
    @SerializedName("is_added_wishlist")
    @Expose
    private boolean is_added_wishlist;
    @SerializedName("_links")
    @Expose
    private Links links;
    @SerializedName("manage_stock")
    @Expose
    private Boolean manageStock;
    @SerializedName("menu_order")
    @Expose
    private Integer menuOrder;
    @SerializedName("meta_data")
    @Expose
    private List<? extends Object> metaData;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("on_sale")
    @Expose
    private boolean onSale;
    @SerializedName("parent_id")
    @Expose
    private Integer parentId;
    @SerializedName("permalink")
    @Expose
    private String permalink;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("price_html")
    @Expose
    private String priceHtml;
    @SerializedName("purchasable")
    @Expose
    private boolean purchasable;
    @SerializedName("purchase_note")
    @Expose
    private String purchaseNote;
    @SerializedName("rating_count")
    @Expose
    private Integer ratingCount;
    @SerializedName("regular_price")
    @Expose
    private String regularPrice;
    @SerializedName("related_ids")
    @Expose
    private List<Integer> relatedIds;
    @SerializedName("reviews_allowed")
    @Expose
    private Boolean reviewsAllowed;
    @SerializedName("sale_price")
    @Expose
    private String salePrice;
    @SerializedName("shipping_class")
    @Expose
    private String shippingClass;
    @SerializedName("shipping_class_id")
    @Expose
    private Integer shippingClassId;
    @SerializedName("shipping_required")
    @Expose
    private Boolean shippingRequired;
    @SerializedName("shipping_taxable")
    @Expose
    private Boolean shippingTaxable;
    @SerializedName("short_description")
    @Expose
    private String shortDescription;
    @SerializedName("sku")
    @Expose
    private String sku;
    @SerializedName("slug")
    @Expose
    private String slug;
    @SerializedName("sold_individually")
    @Expose
    private Boolean soldIndividually;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("stock_quantity")
    @Expose
    private Integer stockQuantity;
    @SerializedName("stock_status")
    @Expose
    private String stockStatus;
    @SerializedName("tags")
    @Expose
    private List<? extends Object> tags;
    @SerializedName("tax_class")
    @Expose
    private String taxClass;
    @SerializedName("tax_status")
    @Expose
    private String taxStatus;
    @SerializedName("total_sales")
    @Expose
    private Integer totalSales;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("upsell_ids")
    @Expose
    private List<Integer> upsellIds;
    @SerializedName("upsell_id")
    @Expose
    private List<StoreUpSale> upsell_id;
    @SerializedName("variations")
    @Expose
    private List<Integer> variations;
    @SerializedName("virtual")
    @Expose
    private Boolean virtual;
    @SerializedName("weight")
    @Expose
    private String weight;

    public final boolean is_added_cart() {
        return this.is_added_cart;
    }

    public final void set_added_cart(boolean z) {
        this.is_added_cart = z;
    }

    public final boolean is_added_wishlist() {
        return this.is_added_wishlist;
    }

    public final void set_added_wishlist(boolean z) {
        this.is_added_wishlist = z;
    }

    public final List<StoreUpSale> getUpsell_id() {
        return this.upsell_id;
    }

    public final void setUpsell_id(List<StoreUpSale> list) {
        this.upsell_id = list;
    }

    public final int getId() {
        return this.id;
    }

    public final void setId(int i) {
        this.id = i;
    }

    public final String getName() {
        return this.name;
    }

    public final void setName(String str) {
        this.name = str;
    }

    public final String getSlug() {
        return this.slug;
    }

    public final void setSlug(String str) {
        this.slug = str;
    }

    public final String getPermalink() {
        return this.permalink;
    }

    public final void setPermalink(String str) {
        this.permalink = str;
    }

    public final String getDateModifiedGmt() {
        return this.dateModifiedGmt;
    }

    public final void setDateModifiedGmt(String str) {
        this.dateModifiedGmt = str;
    }

    public final String getType() {
        return this.type;
    }

    public final void setType(String str) {
        this.type = str;
    }

    public final String getStatus() {
        return this.status;
    }

    public final void setStatus(String str) {
        this.status = str;
    }

    public final Boolean getFeatured() {
        return this.featured;
    }

    public final void setFeatured(Boolean bool) {
        this.featured = bool;
    }

    public final String getCatalogVisibility() {
        return this.catalogVisibility;
    }

    public final void setCatalogVisibility(String str) {
        this.catalogVisibility = str;
    }

    public final String getDescription() {
        return this.description;
    }

    public final void setDescription(String str) {
        this.description = str;
    }

    public final String getShortDescription() {
        return this.shortDescription;
    }

    public final void setShortDescription(String str) {
        this.shortDescription = str;
    }

    public final String getSku() {
        return this.sku;
    }

    public final void setSku(String str) {
        this.sku = str;
    }

    public final String getPrice() {
        return this.price;
    }

    public final void setPrice(String str) {
        this.price = str;
    }

    public final String getRegularPrice() {
        return this.regularPrice;
    }

    public final void setRegularPrice(String str) {
        this.regularPrice = str;
    }

    public final String getSalePrice() {
        return this.salePrice;
    }

    public final void setSalePrice(String str) {
        this.salePrice = str;
    }

    public final Object getDateOnSaleFrom() {
        return this.dateOnSaleFrom;
    }

    public final void setDateOnSaleFrom(Object obj) {
        this.dateOnSaleFrom = obj;
    }

    public final Object getDateOnSaleFromGmt() {
        return this.dateOnSaleFromGmt;
    }

    public final void setDateOnSaleFromGmt(Object obj) {
        this.dateOnSaleFromGmt = obj;
    }

    public final Object getDateOnSaleTo() {
        return this.dateOnSaleTo;
    }

    public final void setDateOnSaleTo(Object obj) {
        this.dateOnSaleTo = obj;
    }

    public final Object getDateOnSaleToGmt() {
        return this.dateOnSaleToGmt;
    }

    public final void setDateOnSaleToGmt(Object obj) {
        this.dateOnSaleToGmt = obj;
    }

    public final String getPriceHtml() {
        return this.priceHtml;
    }

    public final void setPriceHtml(String str) {
        this.priceHtml = str;
    }

    public final boolean getOnSale() {
        return this.onSale;
    }

    public final void setOnSale(boolean z) {
        this.onSale = z;
    }

    public final boolean getPurchasable() {
        return this.purchasable;
    }

    public final void setPurchasable(boolean z) {
        this.purchasable = z;
    }

    public final Integer getTotalSales() {
        return this.totalSales;
    }

    public final void setTotalSales(Integer num) {
        this.totalSales = num;
    }

    public final Boolean getVirtual() {
        return this.virtual;
    }

    public final void setVirtual(Boolean bool) {
        this.virtual = bool;
    }

    public final Boolean getDownloadable() {
        return this.downloadable;
    }

    public final void setDownloadable(Boolean bool) {
        this.downloadable = bool;
    }

    public final List<Object> getDownloads() {
        return this.downloads;
    }

    public final void setDownloads(List<? extends Object> list) {
        this.downloads = list;
    }

    public final Integer getDownloadLimit() {
        return this.downloadLimit;
    }

    public final void setDownloadLimit(Integer num) {
        this.downloadLimit = num;
    }

    public final Integer getDownloadExpiry() {
        return this.downloadExpiry;
    }

    public final void setDownloadExpiry(Integer num) {
        this.downloadExpiry = num;
    }

    public final String getExternalUrl() {
        return this.externalUrl;
    }

    public final void setExternalUrl(String str) {
        this.externalUrl = str;
    }

    public final String getButtonText() {
        return this.buttonText;
    }

    public final void setButtonText(String str) {
        this.buttonText = str;
    }

    public final String getTaxStatus() {
        return this.taxStatus;
    }

    public final void setTaxStatus(String str) {
        this.taxStatus = str;
    }

    public final String getTaxClass() {
        return this.taxClass;
    }

    public final void setTaxClass(String str) {
        this.taxClass = str;
    }

    public final Boolean getManageStock() {
        return this.manageStock;
    }

    public final void setManageStock(Boolean bool) {
        this.manageStock = bool;
    }

    public final Integer getStockQuantity() {
        return this.stockQuantity;
    }

    public final void setStockQuantity(Integer num) {
        this.stockQuantity = num;
    }

    public final String getStockStatus() {
        return this.stockStatus;
    }

    public final void setStockStatus(String str) {
        this.stockStatus = str;
    }

    public final String getBackorders() {
        return this.backorders;
    }

    public final void setBackorders(String str) {
        this.backorders = str;
    }

    public final Boolean getBackordersAllowed() {
        return this.backordersAllowed;
    }

    public final void setBackordersAllowed(Boolean bool) {
        this.backordersAllowed = bool;
    }

    public final Boolean getBackordered() {
        return this.backordered;
    }

    public final void setBackordered(Boolean bool) {
        this.backordered = bool;
    }

    public final Boolean getSoldIndividually() {
        return this.soldIndividually;
    }

    public final void setSoldIndividually(Boolean bool) {
        this.soldIndividually = bool;
    }

    public final String getWeight() {
        return this.weight;
    }

    public final void setWeight(String str) {
        this.weight = str;
    }

    public final Dimensions getDimensions() {
        return this.dimensions;
    }

    public final void setDimensions(Dimensions dimensions2) {
        this.dimensions = dimensions2;
    }

    public final Boolean getShippingRequired() {
        return this.shippingRequired;
    }

    public final void setShippingRequired(Boolean bool) {
        this.shippingRequired = bool;
    }

    public final Boolean getShippingTaxable() {
        return this.shippingTaxable;
    }

    public final void setShippingTaxable(Boolean bool) {
        this.shippingTaxable = bool;
    }

    public final String getShippingClass() {
        return this.shippingClass;
    }

    public final void setShippingClass(String str) {
        this.shippingClass = str;
    }

    public final Integer getShippingClassId() {
        return this.shippingClassId;
    }

    public final void setShippingClassId(Integer num) {
        this.shippingClassId = num;
    }

    public final Boolean getReviewsAllowed() {
        return this.reviewsAllowed;
    }

    public final void setReviewsAllowed(Boolean bool) {
        this.reviewsAllowed = bool;
    }

    public final String getAverageRating() {
        return this.averageRating;
    }

    public final void setAverageRating(String str) {
        this.averageRating = str;
    }

    public final Integer getRatingCount() {
        return this.ratingCount;
    }

    public final void setRatingCount(Integer num) {
        this.ratingCount = num;
    }

    public final List<Integer> getRelatedIds() {
        return this.relatedIds;
    }

    public final void setRelatedIds(List<Integer> list) {
        this.relatedIds = list;
    }

    public final List<Integer> getUpsellIds() {
        return this.upsellIds;
    }

    public final void setUpsellIds(List<Integer> list) {
        this.upsellIds = list;
    }

    public final List<Object> getCrossSellIds() {
        return this.crossSellIds;
    }

    public final void setCrossSellIds(List<? extends Object> list) {
        this.crossSellIds = list;
    }

    public final Integer getParentId() {
        return this.parentId;
    }

    public final void setParentId(Integer num) {
        this.parentId = num;
    }

    public final String getPurchaseNote() {
        return this.purchaseNote;
    }

    public final void setPurchaseNote(String str) {
        this.purchaseNote = str;
    }

    public final List<StoreCategory> getCategories() {
        return this.categories;
    }

    public final void setCategories(List<StoreCategory> list) {
        this.categories = list;
    }

    public final List<Object> getTags() {
        return this.tags;
    }

    public final void setTags(List<? extends Object> list) {
        this.tags = list;
    }

    public final List<Image> getImages() {
        return this.images;
    }

    public final void setImages(List<Image> list) {
        this.images = list;
    }

    public final List<Attributes> getAttributes() {
        return this.attributes;
    }

    public final void setAttributes(List<Attributes> list) {
        this.attributes = list;
    }

    public final List<Object> getDefaultAttributes() {
        return this.defaultAttributes;
    }

    public final void setDefaultAttributes(List<? extends Object> list) {
        this.defaultAttributes = list;
    }

    public final List<Integer> getVariations() {
        return this.variations;
    }

    public final void setVariations(List<Integer> list) {
        this.variations = list;
    }

    public final List<Object> getGroupedProducts() {
        return this.groupedProducts;
    }

    public final void setGroupedProducts(List<? extends Object> list) {
        this.groupedProducts = list;
    }

    public final Integer getMenuOrder() {
        return this.menuOrder;
    }

    public final void setMenuOrder(Integer num) {
        this.menuOrder = num;
    }

    public final List<Object> getMetaData() {
        return this.metaData;
    }

    public final void setMetaData(List<? extends Object> list) {
        this.metaData = list;
    }

    public final Links getLinks() {
        return this.links;
    }

    public final void setLinks(Links links2) {
        this.links = links2;
    }

    public final boolean getIn_stock() {
        return this.in_stock;
    }

    public final void setIn_stock(boolean z) {
        this.in_stock = z;
    }
}
