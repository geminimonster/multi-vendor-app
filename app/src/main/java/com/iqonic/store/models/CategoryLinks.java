package com.iqonic.store.models;

import java.util.List;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B!\u0012\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003\u0012\f\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00060\u0003¢\u0006\u0002\u0010\u0007J\u000f\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003HÆ\u0003J\u000f\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u00060\u0003HÆ\u0003J)\u0010\r\u001a\u00020\u00002\u000e\b\u0002\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u00032\u000e\b\u0002\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00060\u0003HÆ\u0001J\u0013\u0010\u000e\u001a\u00020\u000f2\b\u0010\u0010\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0011\u001a\u00020\u0012HÖ\u0001J\t\u0010\u0013\u001a\u00020\u0014HÖ\u0001R\u0017\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u0017\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00060\u0003¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\t¨\u0006\u0015"}, d2 = {"Lcom/iqonic/store/models/CategoryLinks;", "", "collection", "", "Lcom/iqonic/store/models/CategoryCollection;", "self", "Lcom/iqonic/store/models/CategorySelf;", "(Ljava/util/List;Ljava/util/List;)V", "getCollection", "()Ljava/util/List;", "getSelf", "component1", "component2", "copy", "equals", "", "other", "hashCode", "", "toString", "", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: StoreCategory.kt */
public final class CategoryLinks {
    private final List<CategoryCollection> collection;
    private final List<CategorySelf> self;

    public static /* synthetic */ CategoryLinks copy$default(CategoryLinks categoryLinks, List<CategoryCollection> list, List<CategorySelf> list2, int i, Object obj) {
        if ((i & 1) != 0) {
            list = categoryLinks.collection;
        }
        if ((i & 2) != 0) {
            list2 = categoryLinks.self;
        }
        return categoryLinks.copy(list, list2);
    }

    public final List<CategoryCollection> component1() {
        return this.collection;
    }

    public final List<CategorySelf> component2() {
        return this.self;
    }

    public final CategoryLinks copy(List<CategoryCollection> list, List<CategorySelf> list2) {
        Intrinsics.checkParameterIsNotNull(list, "collection");
        Intrinsics.checkParameterIsNotNull(list2, "self");
        return new CategoryLinks(list, list2);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof CategoryLinks)) {
            return false;
        }
        CategoryLinks categoryLinks = (CategoryLinks) obj;
        return Intrinsics.areEqual((Object) this.collection, (Object) categoryLinks.collection) && Intrinsics.areEqual((Object) this.self, (Object) categoryLinks.self);
    }

    public int hashCode() {
        List<CategoryCollection> list = this.collection;
        int i = 0;
        int hashCode = (list != null ? list.hashCode() : 0) * 31;
        List<CategorySelf> list2 = this.self;
        if (list2 != null) {
            i = list2.hashCode();
        }
        return hashCode + i;
    }

    public String toString() {
        return "CategoryLinks(collection=" + this.collection + ", self=" + this.self + ")";
    }

    public CategoryLinks(List<CategoryCollection> list, List<CategorySelf> list2) {
        Intrinsics.checkParameterIsNotNull(list, "collection");
        Intrinsics.checkParameterIsNotNull(list2, "self");
        this.collection = list;
        this.self = list2;
    }

    public final List<CategoryCollection> getCollection() {
        return this.collection;
    }

    public final List<CategorySelf> getSelf() {
        return this.self;
    }
}
