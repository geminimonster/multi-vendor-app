package com.iqonic.store.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import kotlin.Metadata;

@Metadata(bv = {1, 0, 3}, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0010\b\n\u0002\b\f\n\u0002\u0018\u0002\n\u0002\b\u0014\n\u0002\u0010\u000b\n\u0002\b9\n\u0002\u0018\u0002\n\u0002\b\u0011\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002R\u001c\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\bR\"\u0010\t\u001a\u0004\u0018\u00010\n8\u0006@\u0006X\u000e¢\u0006\u0010\n\u0002\u0010\u000f\u001a\u0004\b\u000b\u0010\f\"\u0004\b\r\u0010\u000eR \u0010\u0010\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0011\u0010\u0006\"\u0004\b\u0012\u0010\bR\u001c\u0010\u0013\u001a\u0004\u0018\u00010\u0004X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0014\u0010\u0006\"\u0004\b\u0015\u0010\bR \u0010\u0016\u001a\u0004\u0018\u00010\u00178\u0006@\u0006X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0018\u0010\u0019\"\u0004\b\u001a\u0010\u001bR\"\u0010\u001c\u001a\u0004\u0018\u00010\n8\u0006@\u0006X\u000e¢\u0006\u0010\n\u0002\u0010\u000f\u001a\u0004\b\u001d\u0010\f\"\u0004\b\u001e\u0010\u000eR \u0010\u001f\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b \u0010\u0006\"\u0004\b!\u0010\bR \u0010\"\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b#\u0010\u0006\"\u0004\b$\u0010\bR \u0010%\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b&\u0010\u0006\"\u0004\b'\u0010\bR \u0010(\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b)\u0010\u0006\"\u0004\b*\u0010\bR\"\u0010+\u001a\u0004\u0018\u00010,8\u0006@\u0006X\u000e¢\u0006\u0010\n\u0002\u00101\u001a\u0004\b-\u0010.\"\u0004\b/\u00100R \u00102\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b3\u0010\u0006\"\u0004\b4\u0010\bR \u00105\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b6\u0010\u0006\"\u0004\b7\u0010\bR \u00108\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b9\u0010\u0006\"\u0004\b:\u0010\bR\u001c\u0010;\u001a\u0004\u0018\u00010\u0004X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b<\u0010\u0006\"\u0004\b=\u0010\bR \u0010>\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b?\u0010\u0006\"\u0004\b@\u0010\bR\"\u0010A\u001a\u0004\u0018\u00010\n8\u0006@\u0006X\u000e¢\u0006\u0010\n\u0002\u0010\u000f\u001a\u0004\bB\u0010\f\"\u0004\bC\u0010\u000eR \u0010D\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\bE\u0010\u0006\"\u0004\bF\u0010\bR\u001c\u0010G\u001a\u0004\u0018\u00010\u0004X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\bH\u0010\u0006\"\u0004\bI\u0010\bR \u0010J\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\bK\u0010\u0006\"\u0004\bL\u0010\bR\"\u0010M\u001a\u0004\u0018\u00010\n8\u0006@\u0006X\u000e¢\u0006\u0010\n\u0002\u0010\u000f\u001a\u0004\bN\u0010\f\"\u0004\bO\u0010\u000eR\"\u0010P\u001a\u0004\u0018\u00010\n8\u0006@\u0006X\u000e¢\u0006\u0010\n\u0002\u0010\u000f\u001a\u0004\bQ\u0010\f\"\u0004\bR\u0010\u000eR\"\u0010S\u001a\u0004\u0018\u00010\n8\u0006@\u0006X\u000e¢\u0006\u0010\n\u0002\u0010\u000f\u001a\u0004\bT\u0010\f\"\u0004\bU\u0010\u000eR \u0010V\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\bW\u0010\u0006\"\u0004\bX\u0010\bR \u0010Y\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\bZ\u0010\u0006\"\u0004\b[\u0010\bR \u0010\\\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b]\u0010\u0006\"\u0004\b^\u0010\bR \u0010_\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b`\u0010\u0006\"\u0004\ba\u0010\bR \u0010b\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\bc\u0010\u0006\"\u0004\bd\u0010\bR \u0010e\u001a\u0004\u0018\u00010f8\u0006@\u0006X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\bg\u0010h\"\u0004\bi\u0010jR \u0010k\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\bl\u0010\u0006\"\u0004\bm\u0010\bR \u0010n\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\bo\u0010\u0006\"\u0004\bp\u0010\bR \u0010q\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\br\u0010\u0006\"\u0004\bs\u0010\bR \u0010t\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\bu\u0010\u0006\"\u0004\bv\u0010\b¨\u0006w"}, d2 = {"Lcom/iqonic/store/models/RequestModel;", "", "()V", "accessToken", "", "getAccessToken", "()Ljava/lang/String;", "setAccessToken", "(Ljava/lang/String;)V", "amount", "", "getAmount", "()Ljava/lang/Integer;", "setAmount", "(Ljava/lang/Integer;)V", "Ljava/lang/Integer;", "apiKey", "getApiKey", "setApiKey", "base64_img", "getBase64_img", "setBase64_img", "billing", "Lcom/iqonic/store/models/Billing;", "getBilling", "()Lcom/iqonic/store/models/Billing;", "setBilling", "(Lcom/iqonic/store/models/Billing;)V", "cartid", "getCartid", "setCartid", "country_code", "getCountry_code", "setCountry_code", "currency", "getCurrency", "setCurrency", "description", "getDescription", "setDescription", "firstName", "getFirstName", "setFirstName", "force", "", "getForce", "()Ljava/lang/Boolean;", "setForce", "(Ljava/lang/Boolean;)V", "Ljava/lang/Boolean;", "id", "getId", "setId", "image", "getImage", "setImage", "lastName", "getLastName", "setLastName", "loginType", "getLoginType", "setLoginType", "new_password", "getNew_password", "setNew_password", "page", "getPage", "setPage", "password", "getPassword", "setPassword", "photoURL", "getPhotoURL", "setPhotoURL", "postcode", "getPostcode", "setPostcode", "pro_id", "getPro_id", "setPro_id", "product_id", "getProduct_id", "setProduct_id", "quantity", "getQuantity", "setQuantity", "rating", "getRating", "setRating", "review", "getReview", "setReview", "reviewer", "getReviewer", "setReviewer", "reviewer_email", "getReviewer_email", "setReviewer_email", "search", "getSearch", "setSearch", "shipping", "Lcom/iqonic/store/models/Shipping;", "getShipping", "()Lcom/iqonic/store/models/Shipping;", "setShipping", "(Lcom/iqonic/store/models/Shipping;)V", "state_code", "getState_code", "setState_code", "status", "getStatus", "setStatus", "userEmail", "getUserEmail", "setUserEmail", "username", "getUsername", "setUsername", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: RequestModel.kt */
public final class RequestModel {
    private String accessToken;
    @SerializedName("amount")
    @Expose
    private Integer amount;
    @SerializedName("apiKey")
    @Expose
    private String apiKey;
    private String base64_img;
    @SerializedName("billing")
    @Expose
    private Billing billing;
    @SerializedName("cart_id")
    @Expose
    private Integer cartid;
    @SerializedName("country_code")
    @Expose
    private String country_code;
    @SerializedName("currency")
    @Expose
    private String currency;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("force")
    @Expose
    private Boolean force;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("profile_image")
    @Expose
    private String image;
    @SerializedName("last_name")
    @Expose
    private String lastName;
    private String loginType;
    @SerializedName("new_password")
    @Expose
    private String new_password;
    @SerializedName("page")
    @Expose
    private Integer page;
    @SerializedName("password")
    @Expose
    private String password;
    private String photoURL;
    @SerializedName("postcode")
    @Expose
    private String postcode;
    @SerializedName("pro_id")
    @Expose
    private Integer pro_id;
    @SerializedName("product_id")
    @Expose
    private Integer product_id;
    @SerializedName("quantity")
    @Expose
    private Integer quantity;
    @SerializedName("rating")
    @Expose
    private String rating;
    @SerializedName("review")
    @Expose
    private String review;
    @SerializedName("reviewer")
    @Expose
    private String reviewer;
    @SerializedName("reviewer_email")
    @Expose
    private String reviewer_email;
    @SerializedName("search")
    @Expose
    private String search;
    @SerializedName("shipping")
    @Expose
    private Shipping shipping;
    @SerializedName("state_code")
    @Expose
    private String state_code;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("email")
    @Expose
    private String userEmail;
    @SerializedName("username")
    @Expose
    private String username;

    public final String getPassword() {
        return this.password;
    }

    public final void setPassword(String str) {
        this.password = str;
    }

    public final String getUsername() {
        return this.username;
    }

    public final void setUsername(String str) {
        this.username = str;
    }

    public final String getAccessToken() {
        return this.accessToken;
    }

    public final void setAccessToken(String str) {
        this.accessToken = str;
    }

    public final String getLoginType() {
        return this.loginType;
    }

    public final void setLoginType(String str) {
        this.loginType = str;
    }

    public final String getPhotoURL() {
        return this.photoURL;
    }

    public final void setPhotoURL(String str) {
        this.photoURL = str;
    }

    public final String getNew_password() {
        return this.new_password;
    }

    public final void setNew_password(String str) {
        this.new_password = str;
    }

    public final Integer getProduct_id() {
        return this.product_id;
    }

    public final void setProduct_id(Integer num) {
        this.product_id = num;
    }

    public final String getReview() {
        return this.review;
    }

    public final void setReview(String str) {
        this.review = str;
    }

    public final String getReviewer() {
        return this.reviewer;
    }

    public final void setReviewer(String str) {
        this.reviewer = str;
    }

    public final String getReviewer_email() {
        return this.reviewer_email;
    }

    public final void setReviewer_email(String str) {
        this.reviewer_email = str;
    }

    public final String getRating() {
        return this.rating;
    }

    public final void setRating(String str) {
        this.rating = str;
    }

    public final String getStatus() {
        return this.status;
    }

    public final void setStatus(String str) {
        this.status = str;
    }

    public final Boolean getForce() {
        return this.force;
    }

    public final void setForce(Boolean bool) {
        this.force = bool;
    }

    public final String getApiKey() {
        return this.apiKey;
    }

    public final void setApiKey(String str) {
        this.apiKey = str;
    }

    public final Integer getAmount() {
        return this.amount;
    }

    public final void setAmount(Integer num) {
        this.amount = num;
    }

    public final String getCurrency() {
        return this.currency;
    }

    public final void setCurrency(String str) {
        this.currency = str;
    }

    public final String getDescription() {
        return this.description;
    }

    public final void setDescription(String str) {
        this.description = str;
    }

    public final String getId() {
        return this.id;
    }

    public final void setId(String str) {
        this.id = str;
    }

    public final Integer getPro_id() {
        return this.pro_id;
    }

    public final void setPro_id(Integer num) {
        this.pro_id = num;
    }

    public final Integer getCartid() {
        return this.cartid;
    }

    public final void setCartid(Integer num) {
        this.cartid = num;
    }

    public final Integer getQuantity() {
        return this.quantity;
    }

    public final void setQuantity(Integer num) {
        this.quantity = num;
    }

    public final String getFirstName() {
        return this.firstName;
    }

    public final void setFirstName(String str) {
        this.firstName = str;
    }

    public final String getLastName() {
        return this.lastName;
    }

    public final void setLastName(String str) {
        this.lastName = str;
    }

    public final String getUserEmail() {
        return this.userEmail;
    }

    public final void setUserEmail(String str) {
        this.userEmail = str;
    }

    public final String getImage() {
        return this.image;
    }

    public final void setImage(String str) {
        this.image = str;
    }

    public final Integer getPage() {
        return this.page;
    }

    public final void setPage(Integer num) {
        this.page = num;
    }

    public final String getSearch() {
        return this.search;
    }

    public final void setSearch(String str) {
        this.search = str;
    }

    public final Billing getBilling() {
        return this.billing;
    }

    public final void setBilling(Billing billing2) {
        this.billing = billing2;
    }

    public final Shipping getShipping() {
        return this.shipping;
    }

    public final void setShipping(Shipping shipping2) {
        this.shipping = shipping2;
    }

    public final String getBase64_img() {
        return this.base64_img;
    }

    public final void setBase64_img(String str) {
        this.base64_img = str;
    }

    public final String getCountry_code() {
        return this.country_code;
    }

    public final void setCountry_code(String str) {
        this.country_code = str;
    }

    public final String getState_code() {
        return this.state_code;
    }

    public final void setState_code(String str) {
        this.state_code = str;
    }

    public final String getPostcode() {
        return this.postcode;
    }

    public final void setPostcode(String str) {
        this.postcode = str;
    }
}
