package com.iqonic.store.models;

import java.io.Serializable;
import java.util.ArrayList;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u000e\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\b\b\u0018\u00002\u00020\u0001B5\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0016\u0010\u0005\u001a\u0012\u0012\u0004\u0012\u00020\u00070\u0006j\b\u0012\u0004\u0012\u00020\u0007`\b\u0012\u0006\u0010\t\u001a\u00020\n¢\u0006\u0002\u0010\u000bJ\t\u0010\u0013\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0014\u001a\u00020\u0003HÆ\u0003J\u0019\u0010\u0015\u001a\u0012\u0012\u0004\u0012\u00020\u00070\u0006j\b\u0012\u0004\u0012\u00020\u0007`\bHÆ\u0003J\t\u0010\u0016\u001a\u00020\nHÆ\u0003JA\u0010\u0017\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00032\u0018\b\u0002\u0010\u0005\u001a\u0012\u0012\u0004\u0012\u00020\u00070\u0006j\b\u0012\u0004\u0012\u00020\u0007`\b2\b\b\u0002\u0010\t\u001a\u00020\nHÆ\u0001J\u0013\u0010\u0018\u001a\u00020\u00192\b\u0010\u001a\u001a\u0004\u0018\u00010\u001bHÖ\u0003J\t\u0010\u001c\u001a\u00020\u001dHÖ\u0001J\t\u0010\u001e\u001a\u00020\u0003HÖ\u0001R\u0011\u0010\t\u001a\u00020\n¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\rR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u000fR\u0011\u0010\u0004\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u000fR!\u0010\u0005\u001a\u0012\u0012\u0004\u0012\u00020\u00070\u0006j\b\u0012\u0004\u0012\u00020\u0007`\b¢\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u0012¨\u0006\u001f"}, d2 = {"Lcom/iqonic/store/models/CountryModel;", "Ljava/io/Serializable;", "code", "", "name", "states", "Ljava/util/ArrayList;", "Lcom/iqonic/store/models/State;", "Lkotlin/collections/ArrayList;", "_links", "Lcom/iqonic/store/models/Links;", "(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Lcom/iqonic/store/models/Links;)V", "get_links", "()Lcom/iqonic/store/models/Links;", "getCode", "()Ljava/lang/String;", "getName", "getStates", "()Ljava/util/ArrayList;", "component1", "component2", "component3", "component4", "copy", "equals", "", "other", "", "hashCode", "", "toString", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: CountryModel.kt */
public final class CountryModel implements Serializable {
    private final Links _links;
    private final String code;
    private final String name;
    private final ArrayList<State> states;

    public static /* synthetic */ CountryModel copy$default(CountryModel countryModel, String str, String str2, ArrayList<State> arrayList, Links links, int i, Object obj) {
        if ((i & 1) != 0) {
            str = countryModel.code;
        }
        if ((i & 2) != 0) {
            str2 = countryModel.name;
        }
        if ((i & 4) != 0) {
            arrayList = countryModel.states;
        }
        if ((i & 8) != 0) {
            links = countryModel._links;
        }
        return countryModel.copy(str, str2, arrayList, links);
    }

    public final String component1() {
        return this.code;
    }

    public final String component2() {
        return this.name;
    }

    public final ArrayList<State> component3() {
        return this.states;
    }

    public final Links component4() {
        return this._links;
    }

    public final CountryModel copy(String str, String str2, ArrayList<State> arrayList, Links links) {
        Intrinsics.checkParameterIsNotNull(str, "code");
        Intrinsics.checkParameterIsNotNull(str2, "name");
        Intrinsics.checkParameterIsNotNull(arrayList, "states");
        Intrinsics.checkParameterIsNotNull(links, "_links");
        return new CountryModel(str, str2, arrayList, links);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof CountryModel)) {
            return false;
        }
        CountryModel countryModel = (CountryModel) obj;
        return Intrinsics.areEqual((Object) this.code, (Object) countryModel.code) && Intrinsics.areEqual((Object) this.name, (Object) countryModel.name) && Intrinsics.areEqual((Object) this.states, (Object) countryModel.states) && Intrinsics.areEqual((Object) this._links, (Object) countryModel._links);
    }

    public int hashCode() {
        String str = this.code;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.name;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        ArrayList<State> arrayList = this.states;
        int hashCode3 = (hashCode2 + (arrayList != null ? arrayList.hashCode() : 0)) * 31;
        Links links = this._links;
        if (links != null) {
            i = links.hashCode();
        }
        return hashCode3 + i;
    }

    public String toString() {
        return "CountryModel(code=" + this.code + ", name=" + this.name + ", states=" + this.states + ", _links=" + this._links + ")";
    }

    public CountryModel(String str, String str2, ArrayList<State> arrayList, Links links) {
        Intrinsics.checkParameterIsNotNull(str, "code");
        Intrinsics.checkParameterIsNotNull(str2, "name");
        Intrinsics.checkParameterIsNotNull(arrayList, "states");
        Intrinsics.checkParameterIsNotNull(links, "_links");
        this.code = str;
        this.name = str2;
        this.states = arrayList;
        this._links = links;
    }

    public final String getCode() {
        return this.code;
    }

    public final String getName() {
        return this.name;
    }

    public final ArrayList<State> getStates() {
        return this.states;
    }

    public final Links get_links() {
        return this._links;
    }
}
