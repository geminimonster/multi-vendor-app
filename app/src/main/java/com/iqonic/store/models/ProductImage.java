package com.iqonic.store.models;

import java.io.Serializable;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0016\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0003\b\b\u0018\u00002\u00020\u0001B=\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0003\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\u0003\u0012\u0006\u0010\t\u001a\u00020\u0007\u0012\u0006\u0010\n\u001a\u00020\u0003¢\u0006\u0002\u0010\u000bJ\t\u0010\u0015\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0016\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0017\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0018\u001a\u00020\u0007HÆ\u0003J\t\u0010\u0019\u001a\u00020\u0003HÆ\u0003J\t\u0010\u001a\u001a\u00020\u0007HÆ\u0003J\t\u0010\u001b\u001a\u00020\u0003HÆ\u0003JO\u0010\u001c\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00032\b\b\u0002\u0010\u0005\u001a\u00020\u00032\b\b\u0002\u0010\u0006\u001a\u00020\u00072\b\b\u0002\u0010\b\u001a\u00020\u00032\b\b\u0002\u0010\t\u001a\u00020\u00072\b\b\u0002\u0010\n\u001a\u00020\u0003HÆ\u0001J\u0013\u0010\u001d\u001a\u00020\u001e2\b\u0010\u001f\u001a\u0004\u0018\u00010 HÖ\u0003J\t\u0010!\u001a\u00020\u0007HÖ\u0001J\t\u0010\"\u001a\u00020\u0003HÖ\u0001R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\rR\u0011\u0010\u0004\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\rR\u0011\u0010\u0005\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\rR\u0011\u0010\u0006\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u0011R\u0011\u0010\b\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\rR\u0011\u0010\t\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u0011R\u0011\u0010\n\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0014\u0010\r¨\u0006#"}, d2 = {"Lcom/iqonic/store/models/ProductImage;", "Ljava/io/Serializable;", "alt", "", "date_created", "date_modified", "id", "", "name", "position", "src", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;ILjava/lang/String;)V", "getAlt", "()Ljava/lang/String;", "getDate_created", "getDate_modified", "getId", "()I", "getName", "getPosition", "getSrc", "component1", "component2", "component3", "component4", "component5", "component6", "component7", "copy", "equals", "", "other", "", "hashCode", "toString", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: BaseResponse.kt */
public final class ProductImage implements Serializable {
    private final String alt;
    private final String date_created;
    private final String date_modified;
    private final int id;
    private final String name;
    private final int position;
    private final String src;

    public static /* synthetic */ ProductImage copy$default(ProductImage productImage, String str, String str2, String str3, int i, String str4, int i2, String str5, int i3, Object obj) {
        if ((i3 & 1) != 0) {
            str = productImage.alt;
        }
        if ((i3 & 2) != 0) {
            str2 = productImage.date_created;
        }
        String str6 = str2;
        if ((i3 & 4) != 0) {
            str3 = productImage.date_modified;
        }
        String str7 = str3;
        if ((i3 & 8) != 0) {
            i = productImage.id;
        }
        int i4 = i;
        if ((i3 & 16) != 0) {
            str4 = productImage.name;
        }
        String str8 = str4;
        if ((i3 & 32) != 0) {
            i2 = productImage.position;
        }
        int i5 = i2;
        if ((i3 & 64) != 0) {
            str5 = productImage.src;
        }
        return productImage.copy(str, str6, str7, i4, str8, i5, str5);
    }

    public final String component1() {
        return this.alt;
    }

    public final String component2() {
        return this.date_created;
    }

    public final String component3() {
        return this.date_modified;
    }

    public final int component4() {
        return this.id;
    }

    public final String component5() {
        return this.name;
    }

    public final int component6() {
        return this.position;
    }

    public final String component7() {
        return this.src;
    }

    public final ProductImage copy(String str, String str2, String str3, int i, String str4, int i2, String str5) {
        Intrinsics.checkParameterIsNotNull(str, "alt");
        Intrinsics.checkParameterIsNotNull(str2, "date_created");
        Intrinsics.checkParameterIsNotNull(str3, "date_modified");
        Intrinsics.checkParameterIsNotNull(str4, "name");
        String str6 = str5;
        Intrinsics.checkParameterIsNotNull(str6, "src");
        return new ProductImage(str, str2, str3, i, str4, i2, str6);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ProductImage)) {
            return false;
        }
        ProductImage productImage = (ProductImage) obj;
        return Intrinsics.areEqual((Object) this.alt, (Object) productImage.alt) && Intrinsics.areEqual((Object) this.date_created, (Object) productImage.date_created) && Intrinsics.areEqual((Object) this.date_modified, (Object) productImage.date_modified) && this.id == productImage.id && Intrinsics.areEqual((Object) this.name, (Object) productImage.name) && this.position == productImage.position && Intrinsics.areEqual((Object) this.src, (Object) productImage.src);
    }

    public int hashCode() {
        String str = this.alt;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.date_created;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.date_modified;
        int hashCode3 = (((hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31) + this.id) * 31;
        String str4 = this.name;
        int hashCode4 = (((hashCode3 + (str4 != null ? str4.hashCode() : 0)) * 31) + this.position) * 31;
        String str5 = this.src;
        if (str5 != null) {
            i = str5.hashCode();
        }
        return hashCode4 + i;
    }

    public String toString() {
        return "ProductImage(alt=" + this.alt + ", date_created=" + this.date_created + ", date_modified=" + this.date_modified + ", id=" + this.id + ", name=" + this.name + ", position=" + this.position + ", src=" + this.src + ")";
    }

    public ProductImage(String str, String str2, String str3, int i, String str4, int i2, String str5) {
        Intrinsics.checkParameterIsNotNull(str, "alt");
        Intrinsics.checkParameterIsNotNull(str2, "date_created");
        Intrinsics.checkParameterIsNotNull(str3, "date_modified");
        Intrinsics.checkParameterIsNotNull(str4, "name");
        Intrinsics.checkParameterIsNotNull(str5, "src");
        this.alt = str;
        this.date_created = str2;
        this.date_modified = str3;
        this.id = i;
        this.name = str4;
        this.position = i2;
        this.src = str5;
    }

    public final String getAlt() {
        return this.alt;
    }

    public final String getDate_created() {
        return this.date_created;
    }

    public final String getDate_modified() {
        return this.date_modified;
    }

    public final int getId() {
        return this.id;
    }

    public final String getName() {
        return this.name;
    }

    public final int getPosition() {
        return this.position;
    }

    public final String getSrc() {
        return this.src;
    }
}
