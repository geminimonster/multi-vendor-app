<?xml version="1.0" encoding="utf-8"?>
<ScrollView android:id="@id/buttonPanel"
android:layout_width="fill_parent"
android:layout_height="wrap_content"
android:fillViewport="true"
android:scrollIndicators="bottom|top"
style="?buttonBarStyle"
xmlns:android="http://schemas.android.com/apk/res/android">

<androidx.appcompat.widget.ButtonBarLayout
android:gravity="bottom"
android:orientation="horizontal"
android:paddingLeft="8.0dip"
android:paddingTop="2.0dip"
android:paddingRight="8.0dip"
android:paddingBottom="2.0dip"
android:layout_width="fill_parent"
android:layout_height="wrap_content"
android:layoutDirection="locale"
android:paddingStart="8.0dip"
android:paddi