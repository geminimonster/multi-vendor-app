package com.iqonic.store.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import kotlin.Metadata;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u001d\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002R \u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\bR \u0010\t\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\n\u0010\u0006\"\u0004\b\u000b\u0010\bR \u0010\f\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\r\u0010\u0006\"\u0004\b\u000e\u0010\bR \u0010\u000f\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0010\u0010\u0006\"\u0004\b\u0011\u0010\bR \u0010\u0012\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0013\u0010\u0006\"\u0004\b\u0014\u0010\bR \u0010\u0015\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0016\u0010\u0006\"\u0004\b\u0017\u0010\bR \u0010\u0018\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0019\u0010\u0006\"\u0004\b\u001a\u0010\bR \u0010\u001b\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u001c\u0010\u0006\"\u0004\b\u001d\u0010\bR \u0010\u001e\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u001f\u0010\u0006\"\u0004\b \u0010\b¨\u0006!"}, d2 = {"Lcom/iqonic/store/models/InstanceSettings;", "", "()V", "classCost35", "", "getClassCost35", "()Ljava/lang/String;", "setClassCost35", "(Ljava/lang/String;)V", "classCost36", "getClassCost36", "setClassCost36", "classCosts", "getClassCosts", "setClassCosts", "cost", "getCost", "setCost", "ignore_discounts", "getIgnore_discounts", "setIgnore_discounts", "noClassCost", "getNoClassCost", "setNoClassCost", "taxStatus", "getTaxStatus", "setTaxStatus", "title", "getTitle", "setTitle", "type", "getType", "setType", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: ShippingModel.kt */
public final class InstanceSettings {
    @SerializedName("class_cost_35")
    @Expose
    private String classCost35;
    @SerializedName("class_cost_36")
    @Expose
    private String classCost36;
    @SerializedName("class_costs")
    @Expose
    private String classCosts;
    @SerializedName("cost")
    @Expose
    private String cost;
    @SerializedName("ignore_discounts")
    @Expose
    private String ignore_discounts;
    @SerializedName("no_class_cost")
    @Expose
    private String noClassCost;
    @SerializedName("tax_status")
    @Expose
    private String taxStatus;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("type")
    @Expose
    private String type;

    public final String getTitle() {
        return this.title;
    }

    public final void setTitle(String str) {
        this.title = str;
    }

    public final String getTaxStatus() {
        return this.taxStatus;
    }

    public final void setTaxStatus(String str) {
        this.taxStatus = str;
    }

    public final String getCost() {
        return this.cost;
    }

    public final void setCost(String str) {
        this.cost = str;
    }

    public final String getClassCosts() {
        return this.classCosts;
    }

    public final void setClassCosts(String str) {
        this.classCosts = str;
    }

    public final String getClassCost35() {
        return this.classCost35;
    }

    public final void setClassCost35(String str) {
        this.classCost35 = str;
    }

    public final String getClassCost36() {
        return this.classCost36;
    }

    public final void setClassCost36(String str) {
        this.classCost36 = str;
    }

    public final String getNoClassCost() {
        return this.noClassCost;
    }

    public final void setNoClassCost(String str) {
        this.noClassCost = str;
    }

    public final String getType() {
        return this.type;
    }

    public final void setType(String str) {
        this.type = str;
    }

    public final String getIgnore_discounts() {
        return this.ignore_discounts;
    }

    public final void setIgnore_discounts(String str) {
        this.ignore_discounts = str;
    }
}
