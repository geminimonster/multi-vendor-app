package com.iqonic.store.models;

import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0010\b\n\u0002\b\u0017\n\u0002\u0010\u000b\n\u0002\b\u0004\b\b\u0018\u00002\u00020\u0001BE\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0003\u0012\u0006\u0010\u0006\u001a\u00020\u0003\u0012\u0006\u0010\u0007\u001a\u00020\u0003\u0012\u0006\u0010\b\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u0003\u0012\u0006\u0010\u000b\u001a\u00020\u0003¢\u0006\u0002\u0010\fJ\t\u0010\u0017\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0018\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0019\u001a\u00020\u0003HÆ\u0003J\t\u0010\u001a\u001a\u00020\u0003HÆ\u0003J\t\u0010\u001b\u001a\u00020\u0003HÆ\u0003J\t\u0010\u001c\u001a\u00020\tHÆ\u0003J\t\u0010\u001d\u001a\u00020\u0003HÆ\u0003J\t\u0010\u001e\u001a\u00020\u0003HÆ\u0003JY\u0010\u001f\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00032\b\b\u0002\u0010\u0005\u001a\u00020\u00032\b\b\u0002\u0010\u0006\u001a\u00020\u00032\b\b\u0002\u0010\u0007\u001a\u00020\u00032\b\b\u0002\u0010\b\u001a\u00020\t2\b\b\u0002\u0010\n\u001a\u00020\u00032\b\b\u0002\u0010\u000b\u001a\u00020\u0003HÆ\u0001J\u0013\u0010 \u001a\u00020!2\b\u0010\"\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010#\u001a\u00020\tHÖ\u0001J\t\u0010$\u001a\u00020\u0003HÖ\u0001R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000eR\u0011\u0010\u0004\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u000eR\u0011\u0010\u0005\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u000eR\u0011\u0010\u0006\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u000eR\u0011\u0010\u0007\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u000eR\u0011\u0010\b\u001a\u00020\t¢\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u0014R\u0011\u0010\n\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0015\u0010\u000eR\u0011\u0010\u000b\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0016\u0010\u000e¨\u0006%"}, d2 = {"Lcom/iqonic/store/models/CategoryImage;", "", "alt", "", "date_created", "date_created_gmt", "date_modified", "date_modified_gmt", "id", "", "name", "src", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V", "getAlt", "()Ljava/lang/String;", "getDate_created", "getDate_created_gmt", "getDate_modified", "getDate_modified_gmt", "getId", "()I", "getName", "getSrc", "component1", "component2", "component3", "component4", "component5", "component6", "component7", "component8", "copy", "equals", "", "other", "hashCode", "toString", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: StoreCategory.kt */
public final class CategoryImage {
    private final String alt;
    private final String date_created;
    private final String date_created_gmt;
    private final String date_modified;
    private final String date_modified_gmt;
    private final int id;
    private final String name;
    private final String src;

    public static /* synthetic */ CategoryImage copy$default(CategoryImage categoryImage, String str, String str2, String str3, String str4, String str5, int i, String str6, String str7, int i2, Object obj) {
        CategoryImage categoryImage2 = categoryImage;
        int i3 = i2;
        return categoryImage.copy((i3 & 1) != 0 ? categoryImage2.alt : str, (i3 & 2) != 0 ? categoryImage2.date_created : str2, (i3 & 4) != 0 ? categoryImage2.date_created_gmt : str3, (i3 & 8) != 0 ? categoryImage2.date_modified : str4, (i3 & 16) != 0 ? categoryImage2.date_modified_gmt : str5, (i3 & 32) != 0 ? categoryImage2.id : i, (i3 & 64) != 0 ? categoryImage2.name : str6, (i3 & 128) != 0 ? categoryImage2.src : str7);
    }

    public final String component1() {
        return this.alt;
    }

    public final String component2() {
        return this.date_created;
    }

    public final String component3() {
        return this.date_created_gmt;
    }

    public final String component4() {
        return this.date_modified;
    }

    public final String component5() {
        return this.date_modified_gmt;
    }

    public final int component6() {
        return this.id;
    }

    public final String component7() {
        return this.name;
    }

    public final String component8() {
        return this.src;
    }

    public final CategoryImage copy(String str, String str2, String str3, String str4, String str5, int i, String str6, String str7) {
        Intrinsics.checkParameterIsNotNull(str, "alt");
        Intrinsics.checkParameterIsNotNull(str2, "date_created");
        Intrinsics.checkParameterIsNotNull(str3, "date_created_gmt");
        Intrinsics.checkParameterIsNotNull(str4, "date_modified");
        Intrinsics.checkParameterIsNotNull(str5, "date_modified_gmt");
        String str8 = str6;
        Intrinsics.checkParameterIsNotNull(str8, "name");
        String str9 = str7;
        Intrinsics.checkParameterIsNotNull(str9, "src");
        return new CategoryImage(str, str2, str3, str4, str5, i, str8, str9);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof CategoryImage)) {
            return false;
        }
        CategoryImage categoryImage = (CategoryImage) obj;
        return Intrinsics.areEqual((Object) this.alt, (Object) categoryImage.alt) && Intrinsics.areEqual((Object) this.date_created, (Object) categoryImage.date_created) && Intrinsics.areEqual((Object) this.date_created_gmt, (Object) categoryImage.date_created_gmt) && Intrinsics.areEqual((Object) this.date_modified, (Object) categoryImage.date_modified) && Intrinsics.areEqual((Object) this.date_modified_gmt, (Object) categoryImage.date_modified_gmt) && this.id == categoryImage.id && Intrinsics.areEqual((Object) this.name, (Object) categoryImage.name) && Intrinsics.areEqual((Object) this.src, (Object) categoryImage.src);
    }

    public int hashCode() {
        String str = this.alt;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.date_created;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.date_created_gmt;
        int hashCode3 = (hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31;
        String str4 = this.date_modified;
        int hashCode4 = (hashCode3 + (str4 != null ? str4.hashCode() : 0)) * 31;
        String str5 = this.date_modified_gmt;
        int hashCode5 = (((hashCode4 + (str5 != null ? str5.hashCode() : 0)) * 31) + this.id) * 31;
        String str6 = this.name;
        int hashCode6 = (hashCode5 + (str6 != null ? str6.hashCode() : 0)) * 31;
        String str7 = this.src;
        if (str7 != null) {
            i = str7.hashCode();
        }
        return hashCode6 + i;
    }

    public String toString() {
        return "CategoryImage(alt=" + this.alt + ", date_created=" + this.date_created + ", date_created_gmt=" + this.date_created_gmt + ", date_modified=" + this.date_modified + ", date_modified_gmt=" + this.date_modified_gmt + ", id=" + this.id + ", name=" + this.name + ", src=" + this.src + ")";
    }

    public CategoryImage(String str, String str2, String str3, String str4, String str5, int i, String str6, String str7) {
        Intrinsics.checkParameterIsNotNull(str, "alt");
        Intrinsics.checkParameterIsNotNull(str2, "date_created");
        Intrinsics.checkParameterIsNotNull(str3, "date_created_gmt");
        Intrinsics.checkParameterIsNotNull(str4, "date_modified");
        Intrinsics.checkParameterIsNotNull(str5, "date_modified_gmt");
        Intrinsics.checkParameterIsNotNull(str6, "name");
        Intrinsics.checkParameterIsNotNull(str7, "src");
        this.alt = str;
        this.date_created = str2;
        this.date_created_gmt = str3;
        this.date_modified = str4;
        this.date_modified_gmt = str5;
        this.id = i;
        this.name = str6;
        this.src = str7;
    }

    public final String getAlt() {
        return this.alt;
    }

    public final String getDate_created() {
        return this.date_created;
    }

    public final String getDate_created_gmt() {
        return this.date_created_gmt;
    }

    public final String getDate_modified() {
        return this.date_modified;
    }

    public final String getDate_modified_gmt() {
        return this.date_modified_gmt;
    }

    public final int getId() {
        return this.id;
    }

    public final String getName() {
        return this.name;
    }

    public final String getSrc() {
        return this.src;
    }
}
