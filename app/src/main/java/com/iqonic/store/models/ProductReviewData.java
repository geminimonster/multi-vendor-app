package com.iqonic.store.models;

import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b$\b\b\u0018\u00002\u00020\u0001Be\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0005\u0012\u0006\u0010\u0007\u001a\u00020\b\u0012\u0006\u0010\t\u001a\u00020\b\u0012\u0006\u0010\n\u001a\u00020\b\u0012\u0006\u0010\u000b\u001a\u00020\u0005\u0012\u0006\u0010\f\u001a\u00020\u0005\u0012\u0006\u0010\r\u001a\u00020\u000e\u0012\u0006\u0010\u000f\u001a\u00020\u0005\u0012\u0006\u0010\u0010\u001a\u00020\u0005\u0012\u0006\u0010\u0011\u001a\u00020\u0012¢\u0006\u0002\u0010\u0013J\t\u0010%\u001a\u00020\u0003HÆ\u0003J\t\u0010&\u001a\u00020\u0005HÆ\u0003J\t\u0010'\u001a\u00020\u0005HÆ\u0003J\t\u0010(\u001a\u00020\u0012HÆ\u0003J\t\u0010)\u001a\u00020\u0005HÆ\u0003J\t\u0010*\u001a\u00020\u0005HÆ\u0003J\t\u0010+\u001a\u00020\bHÆ\u0003J\t\u0010,\u001a\u00020\bHÆ\u0003J\t\u0010-\u001a\u00020\bHÆ\u0003J\t\u0010.\u001a\u00020\u0005HÆ\u0003J\t\u0010/\u001a\u00020\u0005HÆ\u0003J\t\u00100\u001a\u00020\u000eHÆ\u0003J\u0001\u00101\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u00052\b\b\u0002\u0010\u0007\u001a\u00020\b2\b\b\u0002\u0010\t\u001a\u00020\b2\b\b\u0002\u0010\n\u001a\u00020\b2\b\b\u0002\u0010\u000b\u001a\u00020\u00052\b\b\u0002\u0010\f\u001a\u00020\u00052\b\b\u0002\u0010\r\u001a\u00020\u000e2\b\b\u0002\u0010\u000f\u001a\u00020\u00052\b\b\u0002\u0010\u0010\u001a\u00020\u00052\b\b\u0002\u0010\u0011\u001a\u00020\u0012HÆ\u0001J\u0013\u00102\u001a\u00020\u00122\b\u00103\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u00104\u001a\u00020\bHÖ\u0001J\t\u00105\u001a\u00020\u0005HÖ\u0001R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0014\u0010\u0015R\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0016\u0010\u0017R\u0011\u0010\u0006\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0018\u0010\u0017R\u0011\u0010\u000f\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0019\u0010\u0017R\u0011\u0010\u0007\u001a\u00020\b¢\u0006\b\n\u0000\u001a\u0004\b\u001a\u0010\u001bR\u0011\u0010\f\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u001c\u0010\u0017R\u0011\u0010\t\u001a\u00020\b¢\u0006\b\n\u0000\u001a\u0004\b\u001d\u0010\u001bR\u0011\u0010\n\u001a\u00020\b¢\u0006\b\n\u0000\u001a\u0004\b\u001e\u0010\u001bR\u0011\u0010\u000b\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u001f\u0010\u0017R\u0011\u0010\r\u001a\u00020\u000e¢\u0006\b\n\u0000\u001a\u0004\b \u0010!R\u0011\u0010\u0010\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\"\u0010\u0017R\u0011\u0010\u0011\u001a\u00020\u0012¢\u0006\b\n\u0000\u001a\u0004\b#\u0010$¨\u00066"}, d2 = {"Lcom/iqonic/store/models/ProductReviewData;", "", "_links", "Lcom/iqonic/store/models/ReviewLinks;", "date_created", "", "date_created_gmt", "id", "", "product_id", "rating", "review", "name", "reviewer_avatar_urls", "Lcom/iqonic/store/models/ReviewerAvatarUrls;", "email", "status", "verified", "", "(Lcom/iqonic/store/models/ReviewLinks;Ljava/lang/String;Ljava/lang/String;IIILjava/lang/String;Ljava/lang/String;Lcom/iqonic/store/models/ReviewerAvatarUrls;Ljava/lang/String;Ljava/lang/String;Z)V", "get_links", "()Lcom/iqonic/store/models/ReviewLinks;", "getDate_created", "()Ljava/lang/String;", "getDate_created_gmt", "getEmail", "getId", "()I", "getName", "getProduct_id", "getRating", "getReview", "getReviewer_avatar_urls", "()Lcom/iqonic/store/models/ReviewerAvatarUrls;", "getStatus", "getVerified", "()Z", "component1", "component10", "component11", "component12", "component2", "component3", "component4", "component5", "component6", "component7", "component8", "component9", "copy", "equals", "other", "hashCode", "toString", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: ProductReviewData.kt */
public final class ProductReviewData {
    private final ReviewLinks _links;
    private final String date_created;
    private final String date_created_gmt;
    private final String email;
    private final int id;
    private final String name;
    private final int product_id;
    private final int rating;
    private final String review;
    private final ReviewerAvatarUrls reviewer_avatar_urls;
    private final String status;
    private final boolean verified;

    public static /* synthetic */ ProductReviewData copy$default(ProductReviewData productReviewData, ReviewLinks reviewLinks, String str, String str2, int i, int i2, int i3, String str3, String str4, ReviewerAvatarUrls reviewerAvatarUrls, String str5, String str6, boolean z, int i4, Object obj) {
        ProductReviewData productReviewData2 = productReviewData;
        int i5 = i4;
        return productReviewData.copy((i5 & 1) != 0 ? productReviewData2._links : reviewLinks, (i5 & 2) != 0 ? productReviewData2.date_created : str, (i5 & 4) != 0 ? productReviewData2.date_created_gmt : str2, (i5 & 8) != 0 ? productReviewData2.id : i, (i5 & 16) != 0 ? productReviewData2.product_id : i2, (i5 & 32) != 0 ? productReviewData2.rating : i3, (i5 & 64) != 0 ? productReviewData2.review : str3, (i5 & 128) != 0 ? productReviewData2.name : str4, (i5 & 256) != 0 ? productReviewData2.reviewer_avatar_urls : reviewerAvatarUrls, (i5 & 512) != 0 ? productReviewData2.email : str5, (i5 & 1024) != 0 ? productReviewData2.status : str6, (i5 & 2048) != 0 ? productReviewData2.verified : z);
    }

    public final ReviewLinks component1() {
        return this._links;
    }

    public final String component10() {
        return this.email;
    }

    public final String component11() {
        return this.status;
    }

    public final boolean component12() {
        return this.verified;
    }

    public final String component2() {
        return this.date_created;
    }

    public final String component3() {
        return this.date_created_gmt;
    }

    public final int component4() {
        return this.id;
    }

    public final int component5() {
        return this.product_id;
    }

    public final int component6() {
        return this.rating;
    }

    public final String component7() {
        return this.review;
    }

    public final String component8() {
        return this.name;
    }

    public final ReviewerAvatarUrls component9() {
        return this.reviewer_avatar_urls;
    }

    public final ProductReviewData copy(ReviewLinks reviewLinks, String str, String str2, int i, int i2, int i3, String str3, String str4, ReviewerAvatarUrls reviewerAvatarUrls, String str5, String str6, boolean z) {
        Intrinsics.checkParameterIsNotNull(reviewLinks, "_links");
        String str7 = str;
        Intrinsics.checkParameterIsNotNull(str7, "date_created");
        String str8 = str2;
        Intrinsics.checkParameterIsNotNull(str8, "date_created_gmt");
        String str9 = str3;
        Intrinsics.checkParameterIsNotNull(str9, "review");
        String str10 = str4;
        Intrinsics.checkParameterIsNotNull(str10, "name");
        ReviewerAvatarUrls reviewerAvatarUrls2 = reviewerAvatarUrls;
        Intrinsics.checkParameterIsNotNull(reviewerAvatarUrls2, "reviewer_avatar_urls");
        String str11 = str5;
        Intrinsics.checkParameterIsNotNull(str11, "email");
        String str12 = str6;
        Intrinsics.checkParameterIsNotNull(str12, "status");
        return new ProductReviewData(reviewLinks, str7, str8, i, i2, i3, str9, str10, reviewerAvatarUrls2, str11, str12, z);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ProductReviewData)) {
            return false;
        }
        ProductReviewData productReviewData = (ProductReviewData) obj;
        return Intrinsics.areEqual((Object) this._links, (Object) productReviewData._links) && Intrinsics.areEqual((Object) this.date_created, (Object) productReviewData.date_created) && Intrinsics.areEqual((Object) this.date_created_gmt, (Object) productReviewData.date_created_gmt) && this.id == productReviewData.id && this.product_id == productReviewData.product_id && this.rating == productReviewData.rating && Intrinsics.areEqual((Object) this.review, (Object) productReviewData.review) && Intrinsics.areEqual((Object) this.name, (Object) productReviewData.name) && Intrinsics.areEqual((Object) this.reviewer_avatar_urls, (Object) productReviewData.reviewer_avatar_urls) && Intrinsics.areEqual((Object) this.email, (Object) productReviewData.email) && Intrinsics.areEqual((Object) this.status, (Object) productReviewData.status) && this.verified == productReviewData.verified;
    }

    public int hashCode() {
        ReviewLinks reviewLinks = this._links;
        int i = 0;
        int hashCode = (reviewLinks != null ? reviewLinks.hashCode() : 0) * 31;
        String str = this.date_created;
        int hashCode2 = (hashCode + (str != null ? str.hashCode() : 0)) * 31;
        String str2 = this.date_created_gmt;
        int hashCode3 = (((((((hashCode2 + (str2 != null ? str2.hashCode() : 0)) * 31) + this.id) * 31) + this.product_id) * 31) + this.rating) * 31;
        String str3 = this.review;
        int hashCode4 = (hashCode3 + (str3 != null ? str3.hashCode() : 0)) * 31;
        String str4 = this.name;
        int hashCode5 = (hashCode4 + (str4 != null ? str4.hashCode() : 0)) * 31;
        ReviewerAvatarUrls reviewerAvatarUrls = this.reviewer_avatar_urls;
        int hashCode6 = (hashCode5 + (reviewerAvatarUrls != null ? reviewerAvatarUrls.hashCode() : 0)) * 31;
        String str5 = this.email;
        int hashCode7 = (hashCode6 + (str5 != null ? str5.hashCode() : 0)) * 31;
        String str6 = this.status;
        if (str6 != null) {
            i = str6.hashCode();
        }
        int i2 = (hashCode7 + i) * 31;
        boolean z = this.verified;
        if (z) {
            z = true;
        }
        return i2 + (z ? 1 : 0);
    }

    public String toString() {
        return "ProductReviewData(_links=" + this._links + ", date_created=" + this.date_created + ", date_created_gmt=" + this.date_created_gmt + ", id=" + this.id + ", product_id=" + this.product_id + ", rating=" + this.rating + ", review=" + this.review + ", name=" + this.name + ", reviewer_avatar_urls=" + this.reviewer_avatar_urls + ", email=" + this.email + ", status=" + this.status + ", verified=" + this.verified + ")";
    }

    public ProductReviewData(ReviewLinks reviewLinks, String str, String str2, int i, int i2, int i3, String str3, String str4, ReviewerAvatarUrls reviewerAvatarUrls, String str5, String str6, boolean z) {
        Intrinsics.checkParameterIsNotNull(reviewLinks, "_links");
        Intrinsics.checkParameterIsNotNull(str, "date_created");
        Intrinsics.checkParameterIsNotNull(str2, "date_created_gmt");
        Intrinsics.checkParameterIsNotNull(str3, "review");
        Intrinsics.checkParameterIsNotNull(str4, "name");
        Intrinsics.checkParameterIsNotNull(reviewerAvatarUrls, "reviewer_avatar_urls");
        Intrinsics.checkParameterIsNotNull(str5, "email");
        Intrinsics.checkParameterIsNotNull(str6, "status");
        this._links = reviewLinks;
        this.date_created = str;
        this.date_created_gmt = str2;
        this.id = i;
        this.product_id = i2;
        this.rating = i3;
        this.review = str3;
        this.name = str4;
        this.reviewer_avatar_urls = reviewerAvatarUrls;
        this.email = str5;
        this.status = str6;
        this.verified = z;
    }

    public final ReviewLinks get_links() {
        return this._links;
    }

    public final String getDate_created() {
        return this.date_created;
    }

    public final String getDate_created_gmt() {
        return this.date_created_gmt;
    }

    public final int getId() {
        return this.id;
    }

    public final int getProduct_id() {
        return this.product_id;
    }

    public final int getRating() {
        return this.rating;
    }

    public final String getReview() {
        return this.review;
    }

    public final String getName() {
        return this.name;
    }

    public final ReviewerAvatarUrls getReviewer_avatar_urls() {
        return this.reviewer_avatar_urls;
    }

    public final String getEmail() {
        return this.email;
    }

    public final String getStatus() {
        return this.status;
    }

    public final boolean getVerified() {
        return this.verified;
    }
}
