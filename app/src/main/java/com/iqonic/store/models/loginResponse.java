package com.iqonic.store.models;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.iqonic.store.utils.Constants;
import java.util.List;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0007\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010 \n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b \n\u0002\u0010\u000b\n\u0002\b\u0004\b\b\u0018\u00002\u00020\u0001Bk\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0003\u0012\u0006\u0010\u0006\u001a\u00020\u0003\u0012\u0006\u0010\u0007\u001a\u00020\u0003\u0012\u0006\u0010\b\u001a\u00020\u0003\u0012\u0006\u0010\t\u001a\u00020\u0003\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\f\u001a\u00020\u0003\u0012\f\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u00030\u000e\u0012\u0006\u0010\u000f\u001a\u00020\u0010\u0012\u0006\u0010\u0011\u001a\u00020\u0012¢\u0006\u0002\u0010\u0013J\t\u0010%\u001a\u00020\u0003HÆ\u0003J\u000f\u0010&\u001a\b\u0012\u0004\u0012\u00020\u00030\u000eHÆ\u0003J\t\u0010'\u001a\u00020\u0010HÆ\u0003J\t\u0010(\u001a\u00020\u0012HÆ\u0003J\t\u0010)\u001a\u00020\u0003HÆ\u0003J\t\u0010*\u001a\u00020\u0003HÆ\u0003J\t\u0010+\u001a\u00020\u0003HÆ\u0003J\t\u0010,\u001a\u00020\u0003HÆ\u0003J\t\u0010-\u001a\u00020\u0003HÆ\u0003J\t\u0010.\u001a\u00020\u0003HÆ\u0003J\t\u0010/\u001a\u00020\u000bHÆ\u0003J\t\u00100\u001a\u00020\u0003HÆ\u0003J\u0001\u00101\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00032\b\b\u0002\u0010\u0005\u001a\u00020\u00032\b\b\u0002\u0010\u0006\u001a\u00020\u00032\b\b\u0002\u0010\u0007\u001a\u00020\u00032\b\b\u0002\u0010\b\u001a\u00020\u00032\b\b\u0002\u0010\t\u001a\u00020\u00032\b\b\u0002\u0010\n\u001a\u00020\u000b2\b\b\u0002\u0010\f\u001a\u00020\u00032\u000e\b\u0002\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u00030\u000e2\b\b\u0002\u0010\u000f\u001a\u00020\u00102\b\b\u0002\u0010\u0011\u001a\u00020\u0012HÆ\u0001J\u0013\u00102\u001a\u0002032\b\u00104\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u00105\u001a\u00020\u000bHÖ\u0001J\t\u00106\u001a\u00020\u0003HÖ\u0001R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0014\u0010\u0015R\u0011\u0010\u0011\u001a\u00020\u0012¢\u0006\b\n\u0000\u001a\u0004\b\u0016\u0010\u0017R\u0011\u0010\u0006\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0018\u0010\u0015R\u0011\u0010\u0007\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0019\u0010\u0015R\u0011\u0010\u0004\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u001a\u0010\u0015R\u0011\u0010\u000f\u001a\u00020\u0010¢\u0006\b\n\u0000\u001a\u0004\b\u001b\u0010\u001cR\u0011\u0010\u0005\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u001d\u0010\u0015R\u0011\u0010\b\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u001e\u0010\u0015R\u0011\u0010\t\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u001f\u0010\u0015R\u0011\u0010\n\u001a\u00020\u000b¢\u0006\b\n\u0000\u001a\u0004\b \u0010!R\u0011\u0010\f\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\"\u0010\u0015R\u0017\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u00030\u000e¢\u0006\b\n\u0000\u001a\u0004\b#\u0010$¨\u00067"}, d2 = {"Lcom/iqonic/store/models/loginResponse;", "", "avatar", "", "profile_image", "token", "first_name", "last_name", "user_display_name", "user_email", "user_id", "", "user_nicename", "user_role", "", "shipping", "Lcom/iqonic/store/models/Shipping;", "billing", "Lcom/iqonic/store/models/Billing;", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/util/List;Lcom/iqonic/store/models/Shipping;Lcom/iqonic/store/models/Billing;)V", "getAvatar", "()Ljava/lang/String;", "getBilling", "()Lcom/iqonic/store/models/Billing;", "getFirst_name", "getLast_name", "getProfile_image", "getShipping", "()Lcom/iqonic/store/models/Shipping;", "getToken", "getUser_display_name", "getUser_email", "getUser_id", "()I", "getUser_nicename", "getUser_role", "()Ljava/util/List;", "component1", "component10", "component11", "component12", "component2", "component3", "component4", "component5", "component6", "component7", "component8", "component9", "copy", "equals", "", "other", "hashCode", "toString", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: BaseResponse.kt */
public final class loginResponse {
    private final String avatar;
    private final Billing billing;
    private final String first_name;
    private final String last_name;
    private final String profile_image;
    private final Shipping shipping;
    private final String token;
    private final String user_display_name;
    private final String user_email;
    private final int user_id;
    private final String user_nicename;
    private final List<String> user_role;

    public static /* synthetic */ loginResponse copy$default(loginResponse loginresponse, String str, String str2, String str3, String str4, String str5, String str6, String str7, int i, String str8, List list, Shipping shipping2, Billing billing2, int i2, Object obj) {
        loginResponse loginresponse2 = loginresponse;
        int i3 = i2;
        return loginresponse.copy((i3 & 1) != 0 ? loginresponse2.avatar : str, (i3 & 2) != 0 ? loginresponse2.profile_image : str2, (i3 & 4) != 0 ? loginresponse2.token : str3, (i3 & 8) != 0 ? loginresponse2.first_name : str4, (i3 & 16) != 0 ? loginresponse2.last_name : str5, (i3 & 32) != 0 ? loginresponse2.user_display_name : str6, (i3 & 64) != 0 ? loginresponse2.user_email : str7, (i3 & 128) != 0 ? loginresponse2.user_id : i, (i3 & 256) != 0 ? loginresponse2.user_nicename : str8, (i3 & 512) != 0 ? loginresponse2.user_role : list, (i3 & 1024) != 0 ? loginresponse2.shipping : shipping2, (i3 & 2048) != 0 ? loginresponse2.billing : billing2);
    }

    public final String component1() {
        return this.avatar;
    }

    public final List<String> component10() {
        return this.user_role;
    }

    public final Shipping component11() {
        return this.shipping;
    }

    public final Billing component12() {
        return this.billing;
    }

    public final String component2() {
        return this.profile_image;
    }

    public final String component3() {
        return this.token;
    }

    public final String component4() {
        return this.first_name;
    }

    public final String component5() {
        return this.last_name;
    }

    public final String component6() {
        return this.user_display_name;
    }

    public final String component7() {
        return this.user_email;
    }

    public final int component8() {
        return this.user_id;
    }

    public final String component9() {
        return this.user_nicename;
    }

    public final loginResponse copy(String str, String str2, String str3, String str4, String str5, String str6, String str7, int i, String str8, List<String> list, Shipping shipping2, Billing billing2) {
        Intrinsics.checkParameterIsNotNull(str, "avatar");
        String str9 = str2;
        Intrinsics.checkParameterIsNotNull(str9, "profile_image");
        String str10 = str3;
        Intrinsics.checkParameterIsNotNull(str10, "token");
        String str11 = str4;
        Intrinsics.checkParameterIsNotNull(str11, "first_name");
        String str12 = str5;
        Intrinsics.checkParameterIsNotNull(str12, "last_name");
        String str13 = str6;
        Intrinsics.checkParameterIsNotNull(str13, Constants.SharedPref.USER_DISPLAY_NAME);
        String str14 = str7;
        Intrinsics.checkParameterIsNotNull(str14, Constants.SharedPref.USER_EMAIL);
        String str15 = str8;
        Intrinsics.checkParameterIsNotNull(str15, Constants.SharedPref.USER_NICE_NAME);
        List<String> list2 = list;
        Intrinsics.checkParameterIsNotNull(list2, "user_role");
        Shipping shipping3 = shipping2;
        Intrinsics.checkParameterIsNotNull(shipping3, FirebaseAnalytics.Param.SHIPPING);
        Billing billing3 = billing2;
        Intrinsics.checkParameterIsNotNull(billing3, "billing");
        return new loginResponse(str, str9, str10, str11, str12, str13, str14, i, str15, list2, shipping3, billing3);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof loginResponse)) {
            return false;
        }
        loginResponse loginresponse = (loginResponse) obj;
        return Intrinsics.areEqual((Object) this.avatar, (Object) loginresponse.avatar) && Intrinsics.areEqual((Object) this.profile_image, (Object) loginresponse.profile_image) && Intrinsics.areEqual((Object) this.token, (Object) loginresponse.token) && Intrinsics.areEqual((Object) this.first_name, (Object) loginresponse.first_name) && Intrinsics.areEqual((Object) this.last_name, (Object) loginresponse.last_name) && Intrinsics.areEqual((Object) this.user_display_name, (Object) loginresponse.user_display_name) && Intrinsics.areEqual((Object) this.user_email, (Object) loginresponse.user_email) && this.user_id == loginresponse.user_id && Intrinsics.areEqual((Object) this.user_nicename, (Object) loginresponse.user_nicename) && Intrinsics.areEqual((Object) this.user_role, (Object) loginresponse.user_role) && Intrinsics.areEqual((Object) this.shipping, (Object) loginresponse.shipping) && Intrinsics.areEqual((Object) this.billing, (Object) loginresponse.billing);
    }

    public int hashCode() {
        String str = this.avatar;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.profile_image;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.token;
        int hashCode3 = (hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31;
        String str4 = this.first_name;
        int hashCode4 = (hashCode3 + (str4 != null ? str4.hashCode() : 0)) * 31;
        String str5 = this.last_name;
        int hashCode5 = (hashCode4 + (str5 != null ? str5.hashCode() : 0)) * 31;
        String str6 = this.user_display_name;
        int hashCode6 = (hashCode5 + (str6 != null ? str6.hashCode() : 0)) * 31;
        String str7 = this.user_email;
        int hashCode7 = (((hashCode6 + (str7 != null ? str7.hashCode() : 0)) * 31) + this.user_id) * 31;
        String str8 = this.user_nicename;
        int hashCode8 = (hashCode7 + (str8 != null ? str8.hashCode() : 0)) * 31;
        List<String> list = this.user_role;
        int hashCode9 = (hashCode8 + (list != null ? list.hashCode() : 0)) * 31;
        Shipping shipping2 = this.shipping;
        int hashCode10 = (hashCode9 + (shipping2 != null ? shipping2.hashCode() : 0)) * 31;
        Billing billing2 = this.billing;
        if (billing2 != null) {
            i = billing2.hashCode();
        }
        return hashCode10 + i;
    }

    public String toString() {
        return "loginResponse(avatar=" + this.avatar + ", profile_image=" + this.profile_image + ", token=" + this.token + ", first_name=" + this.first_name + ", last_name=" + this.last_name + ", user_display_name=" + this.user_display_name + ", user_email=" + this.user_email + ", user_id=" + this.user_id + ", user_nicename=" + this.user_nicename + ", user_role=" + this.user_role + ", shipping=" + this.shipping + ", billing=" + this.billing + ")";
    }

    public loginResponse(String str, String str2, String str3, String str4, String str5, String str6, String str7, int i, String str8, List<String> list, Shipping shipping2, Billing billing2) {
        Intrinsics.checkParameterIsNotNull(str, "avatar");
        Intrinsics.checkParameterIsNotNull(str2, "profile_image");
        Intrinsics.checkParameterIsNotNull(str3, "token");
        Intrinsics.checkParameterIsNotNull(str4, "first_name");
        Intrinsics.checkParameterIsNotNull(str5, "last_name");
        Intrinsics.checkParameterIsNotNull(str6, Constants.SharedPref.USER_DISPLAY_NAME);
        Intrinsics.checkParameterIsNotNull(str7, Constants.SharedPref.USER_EMAIL);
        Intrinsics.checkParameterIsNotNull(str8, Constants.SharedPref.USER_NICE_NAME);
        Intrinsics.checkParameterIsNotNull(list, "user_role");
        Intrinsics.checkParameterIsNotNull(shipping2, FirebaseAnalytics.Param.SHIPPING);
        Intrinsics.checkParameterIsNotNull(billing2, "billing");
        this.avatar = str;
        this.profile_image = str2;
        this.token = str3;
        this.first_name = str4;
        this.last_name = str5;
        this.user_display_name = str6;
        this.user_email = str7;
        this.user_id = i;
        this.user_nicename = str8;
        this.user_role = list;
        this.shipping = shipping2;
        this.billing = billing2;
    }

    public final String getAvatar() {
        return this.avatar;
    }

    public final String getProfile_image() {
        return this.profile_image;
    }

    public final String getToken() {
        return this.token;
    }

    public final String getFirst_name() {
        return this.first_name;
    }

    public final String getLast_name() {
        return this.last_name;
    }

    public final String getUser_display_name() {
        return this.user_display_name;
    }

    public final String getUser_email() {
        return this.user_email;
    }

    public final int getUser_id() {
        return this.user_id;
    }

    public final String getUser_nicename() {
        return this.user_nicename;
    }

    public final List<String> getUser_role() {
        return this.user_role;
    }

    public final Shipping getShipping() {
        return this.shipping;
    }

    public final Billing getBilling() {
        return this.billing;
    }
}
