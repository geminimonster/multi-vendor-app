package com.iqonic.store.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import kotlin.Metadata;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002R \u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\b¨\u0006\t"}, d2 = {"Lcom/iqonic/store/models/CartRequestModel;", "", "()V", "multpleId", "", "getMultpleId", "()Ljava/lang/String;", "setMultpleId", "(Ljava/lang/String;)V", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: CartRequestModel.kt */
public final class CartRequestModel {
    @SerializedName("pro_id")
    @Expose
    private String multpleId;

    public final String getMultpleId() {
        return this.multpleId;
    }

    public final void setMultpleId(String str) {
        this.multpleId = str;
    }
}
