package com.iqonic.store.models;

import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0007\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\b\u0018\u00002\u00020\u0001B\u000f\u0012\b\b\u0002\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\t\u0010\b\u001a\u00020\u0003HÆ\u0003J\u0013\u0010\t\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u0003HÆ\u0001J\u0013\u0010\n\u001a\u00020\u000b2\b\u0010\f\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\r\u001a\u00020\u000eHÖ\u0001J\t\u0010\u000f\u001a\u00020\u0003HÖ\u0001R\u001a\u0010\u0002\u001a\u00020\u0003X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\u0004¨\u0006\u0010"}, d2 = {"Lcom/iqonic/store/models/CheckoutUrlRequest;", "", "order_id", "", "(Ljava/lang/String;)V", "getOrder_id", "()Ljava/lang/String;", "setOrder_id", "component1", "copy", "equals", "", "other", "hashCode", "", "toString", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: CheckoutUrlRequest.kt */
public final class CheckoutUrlRequest {
    private String order_id;

    public CheckoutUrlRequest() {
        this((String) null, 1, (DefaultConstructorMarker) null);
    }

    public static /* synthetic */ CheckoutUrlRequest copy$default(CheckoutUrlRequest checkoutUrlRequest, String str, int i, Object obj) {
        if ((i & 1) != 0) {
            str = checkoutUrlRequest.order_id;
        }
        return checkoutUrlRequest.copy(str);
    }

    public final String component1() {
        return this.order_id;
    }

    public final CheckoutUrlRequest copy(String str) {
        Intrinsics.checkParameterIsNotNull(str, "order_id");
        return new CheckoutUrlRequest(str);
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            return (obj instanceof CheckoutUrlRequest) && Intrinsics.areEqual((Object) this.order_id, (Object) ((CheckoutUrlRequest) obj).order_id);
        }
        return true;
    }

    public int hashCode() {
        String str = this.order_id;
        if (str != null) {
            return str.hashCode();
        }
        return 0;
    }

    public String toString() {
        return "CheckoutUrlRequest(order_id=" + this.order_id + ")";
    }

    public CheckoutUrlRequest(String str) {
        Intrinsics.checkParameterIsNotNull(str, "order_id");
        this.order_id = str;
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ CheckoutUrlRequest(String str, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this((i & 1) != 0 ? "" : str);
    }

    public final String getOrder_id() {
        return this.order_id;
    }

    public final void setOrder_id(String str) {
        Intrinsics.checkParameterIsNotNull(str, "<set-?>");
        this.order_id = str;
    }
}
