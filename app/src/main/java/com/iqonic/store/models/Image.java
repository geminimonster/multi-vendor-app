package com.iqonic.store.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import kotlin.Metadata;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0011\n\u0002\u0010\b\n\u0002\b\f\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002R \u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\bR \u0010\t\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\n\u0010\u0006\"\u0004\b\u000b\u0010\bR \u0010\f\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\r\u0010\u0006\"\u0004\b\u000e\u0010\bR \u0010\u000f\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0010\u0010\u0006\"\u0004\b\u0011\u0010\bR \u0010\u0012\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0013\u0010\u0006\"\u0004\b\u0014\u0010\bR\"\u0010\u0015\u001a\u0004\u0018\u00010\u00168\u0006@\u0006X\u000e¢\u0006\u0010\n\u0002\u0010\u001b\u001a\u0004\b\u0017\u0010\u0018\"\u0004\b\u0019\u0010\u001aR \u0010\u001c\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u001d\u0010\u0006\"\u0004\b\u001e\u0010\bR \u0010\u001f\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b \u0010\u0006\"\u0004\b!\u0010\b¨\u0006\""}, d2 = {"Lcom/iqonic/store/models/Image;", "Ljava/io/Serializable;", "()V", "alt", "", "getAlt", "()Ljava/lang/String;", "setAlt", "(Ljava/lang/String;)V", "dateCreated", "getDateCreated", "setDateCreated", "dateCreatedGmt", "getDateCreatedGmt", "setDateCreatedGmt", "dateModified", "getDateModified", "setDateModified", "dateModifiedGmt", "getDateModifiedGmt", "setDateModifiedGmt", "id", "", "getId", "()Ljava/lang/Integer;", "setId", "(Ljava/lang/Integer;)V", "Ljava/lang/Integer;", "name", "getName", "setName", "src", "getSrc", "setSrc", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: StoreProductModel.kt */
public final class Image implements Serializable {
    @SerializedName("alt")
    @Expose
    private String alt;
    @SerializedName("date_created")
    @Expose
    private String dateCreated;
    @SerializedName("date_created_gmt")
    @Expose
    private String dateCreatedGmt;
    @SerializedName("date_modified")
    @Expose
    private String dateModified;
    @SerializedName("date_modified_gmt")
    @Expose
    private String dateModifiedGmt;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("src")
    @Expose
    private String src;

    public final Integer getId() {
        return this.id;
    }

    public final void setId(Integer num) {
        this.id = num;
    }

    public final String getDateCreated() {
        return this.dateCreated;
    }

    public final void setDateCreated(String str) {
        this.dateCreated = str;
    }

    public final String getDateCreatedGmt() {
        return this.dateCreatedGmt;
    }

    public final void setDateCreatedGmt(String str) {
        this.dateCreatedGmt = str;
    }

    public final String getDateModified() {
        return this.dateModified;
    }

    public final void setDateModified(String str) {
        this.dateModified = str;
    }

    public final String getDateModifiedGmt() {
        return this.dateModifiedGmt;
    }

    public final void setDateModifiedGmt(String str) {
        this.dateModifiedGmt = str;
    }

    public final String getSrc() {
        return this.src;
    }

    public final void setSrc(String str) {
        this.src = str;
    }

    public final String getName() {
        return this.name;
    }

    public final void setName(String str) {
        this.name = str;
    }

    public final String getAlt() {
        return this.alt;
    }

    public final void setAlt(String str) {
        this.alt = str;
    }
}
