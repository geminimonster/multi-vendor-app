package com.iqonic.store.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import java.util.List;
import kotlin.Metadata;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0010 \n\u0002\b\u000b\n\u0002\u0010\u000b\n\u0002\b\t\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002R\"\u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u000e¢\u0006\u0010\n\u0002\u0010\t\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\bR \u0010\n\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\f\u0010\r\"\u0004\b\u000e\u0010\u000fR&\u0010\u0010\u001a\n\u0012\u0004\u0012\u00020\u000b\u0018\u00010\u00118\u0006@\u0006X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0012\u0010\u0013\"\u0004\b\u0014\u0010\u0015R \u0010\u0016\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0017\u0010\r\"\u0004\b\u0018\u0010\u000fR\"\u0010\u0019\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u000e¢\u0006\u0010\n\u0002\u0010\t\u001a\u0004\b\u001a\u0010\u0006\"\u0004\b\u001b\u0010\bR\"\u0010\u001c\u001a\u0004\u0018\u00010\u001d8\u0006@\u0006X\u000e¢\u0006\u0010\n\u0002\u0010\"\u001a\u0004\b\u001e\u0010\u001f\"\u0004\b \u0010!R\"\u0010#\u001a\u0004\u0018\u00010\u001d8\u0006@\u0006X\u000e¢\u0006\u0010\n\u0002\u0010\"\u001a\u0004\b$\u0010\u001f\"\u0004\b%\u0010!¨\u0006&"}, d2 = {"Lcom/iqonic/store/models/Attributes;", "Ljava/io/Serializable;", "()V", "id", "", "getId", "()Ljava/lang/Integer;", "setId", "(Ljava/lang/Integer;)V", "Ljava/lang/Integer;", "name", "", "getName", "()Ljava/lang/String;", "setName", "(Ljava/lang/String;)V", "options", "", "getOptions", "()Ljava/util/List;", "setOptions", "(Ljava/util/List;)V", "optionsString", "getOptionsString", "setOptionsString", "position", "getPosition", "setPosition", "variation", "", "getVariation", "()Ljava/lang/Boolean;", "setVariation", "(Ljava/lang/Boolean;)V", "Ljava/lang/Boolean;", "visible", "getVisible", "setVisible", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: StoreProductModel.kt */
public final class Attributes implements Serializable {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("options")
    @Expose
    private List<String> options;
    @SerializedName("option")
    @Expose
    private String optionsString;
    @SerializedName("position")
    @Expose
    private Integer position;
    @SerializedName("variation")
    @Expose
    private Boolean variation;
    @SerializedName("visible")
    @Expose
    private Boolean visible;

    public final Integer getId() {
        return this.id;
    }

    public final void setId(Integer num) {
        this.id = num;
    }

    public final String getName() {
        return this.name;
    }

    public final void setName(String str) {
        this.name = str;
    }

    public final Integer getPosition() {
        return this.position;
    }

    public final void setPosition(Integer num) {
        this.position = num;
    }

    public final Boolean getVisible() {
        return this.visible;
    }

    public final void setVisible(Boolean bool) {
        this.visible = bool;
    }

    public final Boolean getVariation() {
        return this.variation;
    }

    public final void setVariation(Boolean bool) {
        this.variation = bool;
    }

    public final List<String> getOptions() {
        return this.options;
    }

    public final void setOptions(List<String> list) {
        this.options = list;
    }

    public final String getOptionsString() {
        return this.optionsString;
    }

    public final void setOptionsString(String str) {
        this.optionsString = str;
    }
}
