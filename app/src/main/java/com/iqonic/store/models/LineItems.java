package com.iqonic.store.models;

import java.io.Serializable;
import java.util.List;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b&\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0003\b\b\u0018\u00002\u00020\u0001B\u0001\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005\u0012\u0006\u0010\u0007\u001a\u00020\b\u0012\u0006\u0010\t\u001a\u00020\u0003\u0012\u0006\u0010\n\u001a\u00020\u0003\u0012\f\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\f0\u0005\u0012\u0006\u0010\r\u001a\u00020\u0003\u0012\u0006\u0010\u000e\u001a\u00020\b\u0012\u0006\u0010\u000f\u001a\u00020\b\u0012\u0006\u0010\u0010\u001a\u00020\b\u0012\u0006\u0010\u0011\u001a\u00020\u0012\u0012\u0006\u0010\u0013\u001a\u00020\b\u0012\u0006\u0010\u0014\u001a\u00020\b\u0012\u0006\u0010\u0015\u001a\u00020\u0003¢\u0006\u0002\u0010\u0016J\t\u0010)\u001a\u00020\u0003HÆ\u0003J\t\u0010*\u001a\u00020\bHÆ\u0003J\t\u0010+\u001a\u00020\u0012HÆ\u0003J\t\u0010,\u001a\u00020\bHÆ\u0003J\t\u0010-\u001a\u00020\bHÆ\u0003J\t\u0010.\u001a\u00020\u0003HÆ\u0003J\u000f\u0010/\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005HÆ\u0003J\t\u00100\u001a\u00020\bHÆ\u0003J\t\u00101\u001a\u00020\u0003HÆ\u0003J\t\u00102\u001a\u00020\u0003HÆ\u0003J\u000f\u00103\u001a\b\u0012\u0004\u0012\u00020\f0\u0005HÆ\u0003J\t\u00104\u001a\u00020\u0003HÆ\u0003J\t\u00105\u001a\u00020\bHÆ\u0003J\t\u00106\u001a\u00020\bHÆ\u0003J¡\u0001\u00107\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\u000e\b\u0002\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u00052\b\b\u0002\u0010\u0007\u001a\u00020\b2\b\b\u0002\u0010\t\u001a\u00020\u00032\b\b\u0002\u0010\n\u001a\u00020\u00032\u000e\b\u0002\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\f0\u00052\b\b\u0002\u0010\r\u001a\u00020\u00032\b\b\u0002\u0010\u000e\u001a\u00020\b2\b\b\u0002\u0010\u000f\u001a\u00020\b2\b\b\u0002\u0010\u0010\u001a\u00020\b2\b\b\u0002\u0010\u0011\u001a\u00020\u00122\b\b\u0002\u0010\u0013\u001a\u00020\b2\b\b\u0002\u0010\u0014\u001a\u00020\b2\b\b\u0002\u0010\u0015\u001a\u00020\u0003HÆ\u0001J\u0013\u00108\u001a\u0002092\b\u0010:\u001a\u0004\u0018\u00010;HÖ\u0003J\t\u0010<\u001a\u00020\u0003HÖ\u0001J\t\u0010=\u001a\u00020\bHÖ\u0001R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0017\u0010\u0018R\u0017\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0019\u0010\u001aR\u0011\u0010\u0007\u001a\u00020\b¢\u0006\b\n\u0000\u001a\u0004\b\u001b\u0010\u001cR\u0011\u0010\t\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u001d\u0010\u0018R\u0011\u0010\n\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u001e\u0010\u0018R\u0017\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\f0\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u001f\u0010\u001aR\u0011\u0010\r\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b \u0010\u0018R\u0011\u0010\u000e\u001a\u00020\b¢\u0006\b\n\u0000\u001a\u0004\b!\u0010\u001cR\u0011\u0010\u000f\u001a\u00020\b¢\u0006\b\n\u0000\u001a\u0004\b\"\u0010\u001cR\u0011\u0010\u0010\u001a\u00020\b¢\u0006\b\n\u0000\u001a\u0004\b#\u0010\u001cR\u0011\u0010\u0011\u001a\u00020\u0012¢\u0006\b\n\u0000\u001a\u0004\b$\u0010%R\u0011\u0010\u0013\u001a\u00020\b¢\u0006\b\n\u0000\u001a\u0004\b&\u0010\u001cR\u0011\u0010\u0014\u001a\u00020\b¢\u0006\b\n\u0000\u001a\u0004\b'\u0010\u001cR\u0011\u0010\u0015\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b(\u0010\u0018¨\u0006>"}, d2 = {"Lcom/iqonic/store/models/LineItems;", "Ljava/io/Serializable;", "id", "", "meta_data", "", "Lcom/iqonic/store/models/MetaData;", "name", "", "order_id", "product_id", "product_images", "Lcom/iqonic/store/models/ProductImage;", "quantity", "subtotal", "subtotal_tax", "tax_class", "taxes", "Lcom/iqonic/store/models/Taxes;", "total", "total_tax", "variation_id", "(ILjava/util/List;Ljava/lang/String;IILjava/util/List;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/iqonic/store/models/Taxes;Ljava/lang/String;Ljava/lang/String;I)V", "getId", "()I", "getMeta_data", "()Ljava/util/List;", "getName", "()Ljava/lang/String;", "getOrder_id", "getProduct_id", "getProduct_images", "getQuantity", "getSubtotal", "getSubtotal_tax", "getTax_class", "getTaxes", "()Lcom/iqonic/store/models/Taxes;", "getTotal", "getTotal_tax", "getVariation_id", "component1", "component10", "component11", "component12", "component13", "component14", "component2", "component3", "component4", "component5", "component6", "component7", "component8", "component9", "copy", "equals", "", "other", "", "hashCode", "toString", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: BaseResponse.kt */
public final class LineItems implements Serializable {
    private final int id;
    private final List<MetaData> meta_data;
    private final String name;
    private final int order_id;
    private final int product_id;
    private final List<ProductImage> product_images;
    private final int quantity;
    private final String subtotal;
    private final String subtotal_tax;
    private final String tax_class;
    private final Taxes taxes;
    private final String total;
    private final String total_tax;
    private final int variation_id;

    public static /* synthetic */ LineItems copy$default(LineItems lineItems, int i, List list, String str, int i2, int i3, List list2, int i4, String str2, String str3, String str4, Taxes taxes2, String str5, String str6, int i5, int i6, Object obj) {
        LineItems lineItems2 = lineItems;
        int i7 = i6;
        return lineItems.copy((i7 & 1) != 0 ? lineItems2.id : i, (i7 & 2) != 0 ? lineItems2.meta_data : list, (i7 & 4) != 0 ? lineItems2.name : str, (i7 & 8) != 0 ? lineItems2.order_id : i2, (i7 & 16) != 0 ? lineItems2.product_id : i3, (i7 & 32) != 0 ? lineItems2.product_images : list2, (i7 & 64) != 0 ? lineItems2.quantity : i4, (i7 & 128) != 0 ? lineItems2.subtotal : str2, (i7 & 256) != 0 ? lineItems2.subtotal_tax : str3, (i7 & 512) != 0 ? lineItems2.tax_class : str4, (i7 & 1024) != 0 ? lineItems2.taxes : taxes2, (i7 & 2048) != 0 ? lineItems2.total : str5, (i7 & 4096) != 0 ? lineItems2.total_tax : str6, (i7 & 8192) != 0 ? lineItems2.variation_id : i5);
    }

    public final int component1() {
        return this.id;
    }

    public final String component10() {
        return this.tax_class;
    }

    public final Taxes component11() {
        return this.taxes;
    }

    public final String component12() {
        return this.total;
    }

    public final String component13() {
        return this.total_tax;
    }

    public final int component14() {
        return this.variation_id;
    }

    public final List<MetaData> component2() {
        return this.meta_data;
    }

    public final String component3() {
        return this.name;
    }

    public final int component4() {
        return this.order_id;
    }

    public final int component5() {
        return this.product_id;
    }

    public final List<ProductImage> component6() {
        return this.product_images;
    }

    public final int component7() {
        return this.quantity;
    }

    public final String component8() {
        return this.subtotal;
    }

    public final String component9() {
        return this.subtotal_tax;
    }

    public final LineItems copy(int i, List<MetaData> list, String str, int i2, int i3, List<ProductImage> list2, int i4, String str2, String str3, String str4, Taxes taxes2, String str5, String str6, int i5) {
        List<MetaData> list3 = list;
        Intrinsics.checkParameterIsNotNull(list3, "meta_data");
        String str7 = str;
        Intrinsics.checkParameterIsNotNull(str7, "name");
        List<ProductImage> list4 = list2;
        Intrinsics.checkParameterIsNotNull(list4, "product_images");
        String str8 = str2;
        Intrinsics.checkParameterIsNotNull(str8, "subtotal");
        String str9 = str3;
        Intrinsics.checkParameterIsNotNull(str9, "subtotal_tax");
        String str10 = str4;
        Intrinsics.checkParameterIsNotNull(str10, "tax_class");
        Taxes taxes3 = taxes2;
        Intrinsics.checkParameterIsNotNull(taxes3, "taxes");
        String str11 = str5;
        Intrinsics.checkParameterIsNotNull(str11, "total");
        String str12 = str6;
        Intrinsics.checkParameterIsNotNull(str12, "total_tax");
        return new LineItems(i, list3, str7, i2, i3, list4, i4, str8, str9, str10, taxes3, str11, str12, i5);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof LineItems)) {
            return false;
        }
        LineItems lineItems = (LineItems) obj;
        return this.id == lineItems.id && Intrinsics.areEqual((Object) this.meta_data, (Object) lineItems.meta_data) && Intrinsics.areEqual((Object) this.name, (Object) lineItems.name) && this.order_id == lineItems.order_id && this.product_id == lineItems.product_id && Intrinsics.areEqual((Object) this.product_images, (Object) lineItems.product_images) && this.quantity == lineItems.quantity && Intrinsics.areEqual((Object) this.subtotal, (Object) lineItems.subtotal) && Intrinsics.areEqual((Object) this.subtotal_tax, (Object) lineItems.subtotal_tax) && Intrinsics.areEqual((Object) this.tax_class, (Object) lineItems.tax_class) && Intrinsics.areEqual((Object) this.taxes, (Object) lineItems.taxes) && Intrinsics.areEqual((Object) this.total, (Object) lineItems.total) && Intrinsics.areEqual((Object) this.total_tax, (Object) lineItems.total_tax) && this.variation_id == lineItems.variation_id;
    }

    public int hashCode() {
        int i = this.id * 31;
        List<MetaData> list = this.meta_data;
        int i2 = 0;
        int hashCode = (i + (list != null ? list.hashCode() : 0)) * 31;
        String str = this.name;
        int hashCode2 = (((((hashCode + (str != null ? str.hashCode() : 0)) * 31) + this.order_id) * 31) + this.product_id) * 31;
        List<ProductImage> list2 = this.product_images;
        int hashCode3 = (((hashCode2 + (list2 != null ? list2.hashCode() : 0)) * 31) + this.quantity) * 31;
        String str2 = this.subtotal;
        int hashCode4 = (hashCode3 + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.subtotal_tax;
        int hashCode5 = (hashCode4 + (str3 != null ? str3.hashCode() : 0)) * 31;
        String str4 = this.tax_class;
        int hashCode6 = (hashCode5 + (str4 != null ? str4.hashCode() : 0)) * 31;
        Taxes taxes2 = this.taxes;
        int hashCode7 = (hashCode6 + (taxes2 != null ? taxes2.hashCode() : 0)) * 31;
        String str5 = this.total;
        int hashCode8 = (hashCode7 + (str5 != null ? str5.hashCode() : 0)) * 31;
        String str6 = this.total_tax;
        if (str6 != null) {
            i2 = str6.hashCode();
        }
        return ((hashCode8 + i2) * 31) + this.variation_id;
    }

    public String toString() {
        return "LineItems(id=" + this.id + ", meta_data=" + this.meta_data + ", name=" + this.name + ", order_id=" + this.order_id + ", product_id=" + this.product_id + ", product_images=" + this.product_images + ", quantity=" + this.quantity + ", subtotal=" + this.subtotal + ", subtotal_tax=" + this.subtotal_tax + ", tax_class=" + this.tax_class + ", taxes=" + this.taxes + ", total=" + this.total + ", total_tax=" + this.total_tax + ", variation_id=" + this.variation_id + ")";
    }

    public LineItems(int i, List<MetaData> list, String str, int i2, int i3, List<ProductImage> list2, int i4, String str2, String str3, String str4, Taxes taxes2, String str5, String str6, int i5) {
        Intrinsics.checkParameterIsNotNull(list, "meta_data");
        Intrinsics.checkParameterIsNotNull(str, "name");
        Intrinsics.checkParameterIsNotNull(list2, "product_images");
        Intrinsics.checkParameterIsNotNull(str2, "subtotal");
        Intrinsics.checkParameterIsNotNull(str3, "subtotal_tax");
        Intrinsics.checkParameterIsNotNull(str4, "tax_class");
        Intrinsics.checkParameterIsNotNull(taxes2, "taxes");
        Intrinsics.checkParameterIsNotNull(str5, "total");
        Intrinsics.checkParameterIsNotNull(str6, "total_tax");
        this.id = i;
        this.meta_data = list;
        this.name = str;
        this.order_id = i2;
        this.product_id = i3;
        this.product_images = list2;
        this.quantity = i4;
        this.subtotal = str2;
        this.subtotal_tax = str3;
        this.tax_class = str4;
        this.taxes = taxes2;
        this.total = str5;
        this.total_tax = str6;
        this.variation_id = i5;
    }

    public final int getId() {
        return this.id;
    }

    public final List<MetaData> getMeta_data() {
        return this.meta_data;
    }

    public final String getName() {
        return this.name;
    }

    public final int getOrder_id() {
        return this.order_id;
    }

    public final int getProduct_id() {
        return this.product_id;
    }

    public final List<ProductImage> getProduct_images() {
        return this.product_images;
    }

    public final int getQuantity() {
        return this.quantity;
    }

    public final String getSubtotal() {
        return this.subtotal;
    }

    public final String getSubtotal_tax() {
        return this.subtotal_tax;
    }

    public final String getTax_class() {
        return this.tax_class;
    }

    public final Taxes getTaxes() {
        return this.taxes;
    }

    public final String getTotal() {
        return this.total;
    }

    public final String getTotal_tax() {
        return this.total_tax;
    }

    public final int getVariation_id() {
        return this.variation_id;
    }
}
