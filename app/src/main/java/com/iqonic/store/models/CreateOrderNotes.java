package com.iqonic.store.models;

import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u000f\n\u0002\u0010\b\n\u0002\b\u0002\b\b\u0018\u00002\u00020\u0001B\u0019\u0012\b\b\u0002\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0002\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\t\u0010\u000f\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0010\u001a\u00020\u0005HÆ\u0003J\u001d\u0010\u0011\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u0005HÆ\u0001J\u0013\u0010\u0012\u001a\u00020\u00052\b\u0010\u0013\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0014\u001a\u00020\u0015HÖ\u0001J\t\u0010\u0016\u001a\u00020\u0003HÖ\u0001R\u001a\u0010\u0004\u001a\u00020\u0005X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0007\u0010\b\"\u0004\b\t\u0010\nR\u001a\u0010\u0002\u001a\u00020\u0003X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u000b\u0010\f\"\u0004\b\r\u0010\u000e¨\u0006\u0017"}, d2 = {"Lcom/iqonic/store/models/CreateOrderNotes;", "", "note", "", "customer_note", "", "(Ljava/lang/String;Z)V", "getCustomer_note", "()Z", "setCustomer_note", "(Z)V", "getNote", "()Ljava/lang/String;", "setNote", "(Ljava/lang/String;)V", "component1", "component2", "copy", "equals", "other", "hashCode", "", "toString", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: CreateOrderNotes.kt */
public final class CreateOrderNotes {
    private boolean customer_note;
    private String note;

    public CreateOrderNotes() {
        this((String) null, false, 3, (DefaultConstructorMarker) null);
    }

    public static /* synthetic */ CreateOrderNotes copy$default(CreateOrderNotes createOrderNotes, String str, boolean z, int i, Object obj) {
        if ((i & 1) != 0) {
            str = createOrderNotes.note;
        }
        if ((i & 2) != 0) {
            z = createOrderNotes.customer_note;
        }
        return createOrderNotes.copy(str, z);
    }

    public final String component1() {
        return this.note;
    }

    public final boolean component2() {
        return this.customer_note;
    }

    public final CreateOrderNotes copy(String str, boolean z) {
        Intrinsics.checkParameterIsNotNull(str, "note");
        return new CreateOrderNotes(str, z);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof CreateOrderNotes)) {
            return false;
        }
        CreateOrderNotes createOrderNotes = (CreateOrderNotes) obj;
        return Intrinsics.areEqual((Object) this.note, (Object) createOrderNotes.note) && this.customer_note == createOrderNotes.customer_note;
    }

    public int hashCode() {
        String str = this.note;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        boolean z = this.customer_note;
        if (z) {
            z = true;
        }
        return hashCode + (z ? 1 : 0);
    }

    public String toString() {
        return "CreateOrderNotes(note=" + this.note + ", customer_note=" + this.customer_note + ")";
    }

    public CreateOrderNotes(String str, boolean z) {
        Intrinsics.checkParameterIsNotNull(str, "note");
        this.note = str;
        this.customer_note = z;
    }

    public final String getNote() {
        return this.note;
    }

    public final void setNote(String str) {
        Intrinsics.checkParameterIsNotNull(str, "<set-?>");
        this.note = str;
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ CreateOrderNotes(String str, boolean z, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this((i & 1) != 0 ? "" : str, (i & 2) != 0 ? true : z);
    }

    public final boolean getCustomer_note() {
        return this.customer_note;
    }

    public final void setCustomer_note(boolean z) {
        this.customer_note = z;
    }
}
