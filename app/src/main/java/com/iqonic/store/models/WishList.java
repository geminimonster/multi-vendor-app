package com.iqonic.store.models;

import com.facebook.share.internal.MessengerShareContentUtility;
import java.util.List;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\"\n\u0002\u0010\u000b\n\u0002\b\u0004\b\b\u0018\u00002\u00020\u0001Bc\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\f\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00030\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0003\u0012\u0006\u0010\b\u001a\u00020\u0003\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u0006\u0010\u000b\u001a\u00020\u0003\u0012\u0006\u0010\f\u001a\u00020\u0003\u0012\u0006\u0010\r\u001a\u00020\u0003\u0012\u0006\u0010\u000e\u001a\u00020\u0001\u0012\u0006\u0010\u000f\u001a\u00020\u0003¢\u0006\u0002\u0010\u0010J\t\u0010 \u001a\u00020\u0003HÆ\u0003J\t\u0010!\u001a\u00020\u0001HÆ\u0003J\t\u0010\"\u001a\u00020\u0003HÆ\u0003J\t\u0010#\u001a\u00020\u0003HÆ\u0003J\u000f\u0010$\u001a\b\u0012\u0004\u0012\u00020\u00030\u0006HÆ\u0003J\t\u0010%\u001a\u00020\u0003HÆ\u0003J\t\u0010&\u001a\u00020\u0003HÆ\u0003J\t\u0010'\u001a\u00020\nHÆ\u0003J\t\u0010(\u001a\u00020\u0003HÆ\u0003J\t\u0010)\u001a\u00020\u0003HÆ\u0003J\t\u0010*\u001a\u00020\u0003HÆ\u0003J}\u0010+\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00032\u000e\b\u0002\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00030\u00062\b\b\u0002\u0010\u0007\u001a\u00020\u00032\b\b\u0002\u0010\b\u001a\u00020\u00032\b\b\u0002\u0010\t\u001a\u00020\n2\b\b\u0002\u0010\u000b\u001a\u00020\u00032\b\b\u0002\u0010\f\u001a\u00020\u00032\b\b\u0002\u0010\r\u001a\u00020\u00032\b\b\u0002\u0010\u000e\u001a\u00020\u00012\b\b\u0002\u0010\u000f\u001a\u00020\u0003HÆ\u0001J\u0013\u0010,\u001a\u00020-2\b\u0010.\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010/\u001a\u00020\nHÖ\u0001J\t\u00100\u001a\u00020\u0003HÖ\u0001R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u0012R\u0011\u0010\u0004\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u0012R\u0017\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00030\u0006¢\u0006\b\n\u0000\u001a\u0004\b\u0014\u0010\u0015R\u0011\u0010\u0007\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0016\u0010\u0012R\u0011\u0010\b\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0017\u0010\u0012R\u0011\u0010\t\u001a\u00020\n¢\u0006\b\n\u0000\u001a\u0004\b\u0018\u0010\u0019R\u0011\u0010\u000b\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u001a\u0010\u0012R\u0011\u0010\f\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u001b\u0010\u0012R\u0011\u0010\r\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u001c\u0010\u0012R\u0011\u0010\u000e\u001a\u00020\u0001¢\u0006\b\n\u0000\u001a\u0004\b\u001d\u0010\u001eR\u0011\u0010\u000f\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u001f\u0010\u0012¨\u00061"}, d2 = {"Lcom/iqonic/store/models/WishList;", "", "created_at", "", "full", "gallery", "", "name", "price", "pro_id", "", "regular_price", "sale_price", "sku", "stock_quantity", "thumbnail", "(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;)V", "getCreated_at", "()Ljava/lang/String;", "getFull", "getGallery", "()Ljava/util/List;", "getName", "getPrice", "getPro_id", "()I", "getRegular_price", "getSale_price", "getSku", "getStock_quantity", "()Ljava/lang/Object;", "getThumbnail", "component1", "component10", "component11", "component2", "component3", "component4", "component5", "component6", "component7", "component8", "component9", "copy", "equals", "", "other", "hashCode", "toString", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: WishList.kt */
public final class WishList {
    private final String created_at;
    private final String full;
    private final List<String> gallery;
    private final String name;
    private final String price;
    private final int pro_id;
    private final String regular_price;
    private final String sale_price;
    private final String sku;
    private final Object stock_quantity;
    private final String thumbnail;

    public static /* synthetic */ WishList copy$default(WishList wishList, String str, String str2, List list, String str3, String str4, int i, String str5, String str6, String str7, Object obj, String str8, int i2, Object obj2) {
        WishList wishList2 = wishList;
        int i3 = i2;
        return wishList.copy((i3 & 1) != 0 ? wishList2.created_at : str, (i3 & 2) != 0 ? wishList2.full : str2, (i3 & 4) != 0 ? wishList2.gallery : list, (i3 & 8) != 0 ? wishList2.name : str3, (i3 & 16) != 0 ? wishList2.price : str4, (i3 & 32) != 0 ? wishList2.pro_id : i, (i3 & 64) != 0 ? wishList2.regular_price : str5, (i3 & 128) != 0 ? wishList2.sale_price : str6, (i3 & 256) != 0 ? wishList2.sku : str7, (i3 & 512) != 0 ? wishList2.stock_quantity : obj, (i3 & 1024) != 0 ? wishList2.thumbnail : str8);
    }

    public final String component1() {
        return this.created_at;
    }

    public final Object component10() {
        return this.stock_quantity;
    }

    public final String component11() {
        return this.thumbnail;
    }

    public final String component2() {
        return this.full;
    }

    public final List<String> component3() {
        return this.gallery;
    }

    public final String component4() {
        return this.name;
    }

    public final String component5() {
        return this.price;
    }

    public final int component6() {
        return this.pro_id;
    }

    public final String component7() {
        return this.regular_price;
    }

    public final String component8() {
        return this.sale_price;
    }

    public final String component9() {
        return this.sku;
    }

    public final WishList copy(String str, String str2, List<String> list, String str3, String str4, int i, String str5, String str6, String str7, Object obj, String str8) {
        Intrinsics.checkParameterIsNotNull(str, "created_at");
        Intrinsics.checkParameterIsNotNull(str2, MessengerShareContentUtility.WEBVIEW_RATIO_FULL);
        List<String> list2 = list;
        Intrinsics.checkParameterIsNotNull(list2, "gallery");
        String str9 = str3;
        Intrinsics.checkParameterIsNotNull(str9, "name");
        String str10 = str4;
        Intrinsics.checkParameterIsNotNull(str10, "price");
        String str11 = str5;
        Intrinsics.checkParameterIsNotNull(str11, "regular_price");
        String str12 = str6;
        Intrinsics.checkParameterIsNotNull(str12, "sale_price");
        String str13 = str7;
        Intrinsics.checkParameterIsNotNull(str13, "sku");
        Object obj2 = obj;
        Intrinsics.checkParameterIsNotNull(obj2, "stock_quantity");
        String str14 = str8;
        Intrinsics.checkParameterIsNotNull(str14, "thumbnail");
        return new WishList(str, str2, list2, str9, str10, i, str11, str12, str13, obj2, str14);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof WishList)) {
            return false;
        }
        WishList wishList = (WishList) obj;
        return Intrinsics.areEqual((Object) this.created_at, (Object) wishList.created_at) && Intrinsics.areEqual((Object) this.full, (Object) wishList.full) && Intrinsics.areEqual((Object) this.gallery, (Object) wishList.gallery) && Intrinsics.areEqual((Object) this.name, (Object) wishList.name) && Intrinsics.areEqual((Object) this.price, (Object) wishList.price) && this.pro_id == wishList.pro_id && Intrinsics.areEqual((Object) this.regular_price, (Object) wishList.regular_price) && Intrinsics.areEqual((Object) this.sale_price, (Object) wishList.sale_price) && Intrinsics.areEqual((Object) this.sku, (Object) wishList.sku) && Intrinsics.areEqual(this.stock_quantity, wishList.stock_quantity) && Intrinsics.areEqual((Object) this.thumbnail, (Object) wishList.thumbnail);
    }

    public int hashCode() {
        String str = this.created_at;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.full;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        List<String> list = this.gallery;
        int hashCode3 = (hashCode2 + (list != null ? list.hashCode() : 0)) * 31;
        String str3 = this.name;
        int hashCode4 = (hashCode3 + (str3 != null ? str3.hashCode() : 0)) * 31;
        String str4 = this.price;
        int hashCode5 = (((hashCode4 + (str4 != null ? str4.hashCode() : 0)) * 31) + this.pro_id) * 31;
        String str5 = this.regular_price;
        int hashCode6 = (hashCode5 + (str5 != null ? str5.hashCode() : 0)) * 31;
        String str6 = this.sale_price;
        int hashCode7 = (hashCode6 + (str6 != null ? str6.hashCode() : 0)) * 31;
        String str7 = this.sku;
        int hashCode8 = (hashCode7 + (str7 != null ? str7.hashCode() : 0)) * 31;
        Object obj = this.stock_quantity;
        int hashCode9 = (hashCode8 + (obj != null ? obj.hashCode() : 0)) * 31;
        String str8 = this.thumbnail;
        if (str8 != null) {
            i = str8.hashCode();
        }
        return hashCode9 + i;
    }

    public String toString() {
        return "WishList(created_at=" + this.created_at + ", full=" + this.full + ", gallery=" + this.gallery + ", name=" + this.name + ", price=" + this.price + ", pro_id=" + this.pro_id + ", regular_price=" + this.regular_price + ", sale_price=" + this.sale_price + ", sku=" + this.sku + ", stock_quantity=" + this.stock_quantity + ", thumbnail=" + this.thumbnail + ")";
    }

    public WishList(String str, String str2, List<String> list, String str3, String str4, int i, String str5, String str6, String str7, Object obj, String str8) {
        Intrinsics.checkParameterIsNotNull(str, "created_at");
        Intrinsics.checkParameterIsNotNull(str2, MessengerShareContentUtility.WEBVIEW_RATIO_FULL);
        Intrinsics.checkParameterIsNotNull(list, "gallery");
        Intrinsics.checkParameterIsNotNull(str3, "name");
        Intrinsics.checkParameterIsNotNull(str4, "price");
        Intrinsics.checkParameterIsNotNull(str5, "regular_price");
        Intrinsics.checkParameterIsNotNull(str6, "sale_price");
        Intrinsics.checkParameterIsNotNull(str7, "sku");
        Intrinsics.checkParameterIsNotNull(obj, "stock_quantity");
        Intrinsics.checkParameterIsNotNull(str8, "thumbnail");
        this.created_at = str;
        this.full = str2;
        this.gallery = list;
        this.name = str3;
        this.price = str4;
        this.pro_id = i;
        this.regular_price = str5;
        this.sale_price = str6;
        this.sku = str7;
        this.stock_quantity = obj;
        this.thumbnail = str8;
    }

    public final String getCreated_at() {
        return this.created_at;
    }

    public final String getFull() {
        return this.full;
    }

    public final List<String> getGallery() {
        return this.gallery;
    }

    public final String getName() {
        return this.name;
    }

    public final String getPrice() {
        return this.price;
    }

    public final int getPro_id() {
        return this.pro_id;
    }

    public final String getRegular_price() {
        return this.regular_price;
    }

    public final String getSale_price() {
        return this.sale_price;
    }

    public final String getSku() {
        return this.sku;
    }

    public final Object getStock_quantity() {
        return this.stock_quantity;
    }

    public final String getThumbnail() {
        return this.thumbnail;
    }
}
