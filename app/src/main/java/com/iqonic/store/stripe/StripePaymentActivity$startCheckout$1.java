package com.iqonic.store.stripe;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import com.iqonic.store.R;
import com.stripe.android.PaymentConfiguration;
import com.stripe.android.Stripe;
import com.stripe.android.model.ConfirmPaymentIntentParams;
import com.stripe.android.model.MandateDataParams;
import com.stripe.android.model.PaymentMethodCreateParams;
import com.stripe.android.view.CardInputWidget;
import java.util.Map;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n¢\u0006\u0002\b\u0005"}, d2 = {"<anonymous>", "", "it", "Landroid/view/View;", "kotlin.jvm.PlatformType", "onClick"}, k = 3, mv = {1, 1, 16})
/* compiled from: StripePaymentActivity.kt */
final class StripePaymentActivity$startCheckout$1 implements View.OnClickListener {
    final /* synthetic */ StripePaymentActivity this$0;

    StripePaymentActivity$startCheckout$1(StripePaymentActivity stripePaymentActivity) {
        this.this$0 = stripePaymentActivity;
    }

    public final void onClick(View view) {
        this.this$0.showProgress(true);
        PaymentMethodCreateParams paymentMethodCreateParams = ((CardInputWidget) this.this$0._$_findCachedViewById(R.id.cardInputWidget)).getPaymentMethodCreateParams();
        if (paymentMethodCreateParams != null) {
            ConfirmPaymentIntentParams createWithPaymentMethodCreateParams$default = ConfirmPaymentIntentParams.Companion.createWithPaymentMethodCreateParams$default(ConfirmPaymentIntentParams.Companion, paymentMethodCreateParams, StripePaymentActivity.access$getPaymentIntentClientSecret$p(this.this$0), (String) null, (Boolean) null, (Map) null, (String) null, (MandateDataParams) null, (ConfirmPaymentIntentParams.SetupFutureUsage) null, (ConfirmPaymentIntentParams.Shipping) null, 508, (Object) null);
            StripePaymentActivity stripePaymentActivity = this.this$0;
            Context applicationContext = stripePaymentActivity.getApplicationContext();
            Intrinsics.checkExpressionValueIsNotNull(applicationContext, "applicationContext");
            PaymentConfiguration.Companion companion = PaymentConfiguration.Companion;
            Context applicationContext2 = this.this$0.getApplicationContext();
            Intrinsics.checkExpressionValueIsNotNull(applicationContext2, "applicationContext");
            stripePaymentActivity.stripe = new Stripe(applicationContext, companion.getInstance(applicationContext2).getPublishableKey(), (String) null, false, 12, (DefaultConstructorMarker) null);
            Stripe.confirmPayment$default(StripePaymentActivity.access$getStripe$p(this.this$0), (Activity) this.this$0, createWithPaymentMethodCreateParams$default, (String) null, 4, (Object) null);
        }
    }
}
