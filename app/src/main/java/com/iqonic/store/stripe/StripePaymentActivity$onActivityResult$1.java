package com.iqonic.store.stripe;

import android.util.Log;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.iqonic.store.R;
import com.stripe.android.ApiResultCallback;
import com.stripe.android.PaymentIntentResult;
import com.stripe.android.model.PaymentIntent;
import com.stripe.android.model.StripeIntent;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000!\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003*\u0001\u0000\b\n\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001J\u0014\u0010\u0003\u001a\u00020\u00042\n\u0010\u0005\u001a\u00060\u0006j\u0002`\u0007H\u0016J\u0010\u0010\b\u001a\u00020\u00042\u0006\u0010\t\u001a\u00020\u0002H\u0016¨\u0006\n"}, d2 = {"com/iqonic/store/stripe/StripePaymentActivity$onActivityResult$1", "Lcom/stripe/android/ApiResultCallback;", "Lcom/stripe/android/PaymentIntentResult;", "onError", "", "e", "Ljava/lang/Exception;", "Lkotlin/Exception;", "onSuccess", "result", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: StripePaymentActivity.kt */
public final class StripePaymentActivity$onActivityResult$1 implements ApiResultCallback<PaymentIntentResult> {
    final /* synthetic */ StripePaymentActivity this$0;

    StripePaymentActivity$onActivityResult$1(StripePaymentActivity stripePaymentActivity) {
        this.this$0 = stripePaymentActivity;
    }

    public void onSuccess(PaymentIntentResult paymentIntentResult) {
        Intrinsics.checkParameterIsNotNull(paymentIntentResult, "result");
        PaymentIntent paymentIntent = (PaymentIntent) paymentIntentResult.getIntent();
        if (paymentIntent.getStatus() == StripeIntent.Status.Succeeded) {
            Log.e("res", "Payment succeeded");
            LinearLayout linearLayout = (LinearLayout) this.this$0._$_findCachedViewById(R.id.paymentView);
            Intrinsics.checkExpressionValueIsNotNull(linearLayout, "paymentView");
            linearLayout.setVisibility(8);
            LinearLayout linearLayout2 = (LinearLayout) this.this$0._$_findCachedViewById(R.id.createOrderView);
            Intrinsics.checkExpressionValueIsNotNull(linearLayout2, "createOrderView");
            linearLayout2.setVisibility(0);
            TextView textView = (TextView) this.this$0._$_findCachedViewById(R.id.transactionId);
            Intrinsics.checkExpressionValueIsNotNull(textView, "transactionId");
            textView.setText(String.valueOf(paymentIntent.getId()));
            this.this$0.paymentId = String.valueOf(paymentIntent.getId());
            this.this$0.createOrder();
            return;
        }
        Log.e("res", "Payment failed");
        this.this$0.showPaymentFailDialog();
    }

    public void onError(Exception exc) {
        Intrinsics.checkParameterIsNotNull(exc, "e");
        this.this$0.showProgress(false);
        this.this$0.showPaymentFailDialog();
        Log.e("res", "Payment failed");
    }
}
