package com.iqonic.store.stripe;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import com.iqonic.store.activity.PaymentSuccessfullyActivity;
import com.iqonic.store.models.CreateOrderResponse;
import com.iqonic.store.utils.Constants;
import com.iqonic.store.utils.extensions.AppExtensionsKt;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n¢\u0006\u0002\b\u0004"}, d2 = {"<anonymous>", "", "it", "Lcom/iqonic/store/models/CreateOrderResponse;", "invoke"}, k = 3, mv = {1, 1, 16})
/* compiled from: StripePaymentActivity.kt */
final class StripePaymentActivity$createOrder$1 extends Lambda implements Function1<CreateOrderResponse, Unit> {
    final /* synthetic */ StripePaymentActivity this$0;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    StripePaymentActivity$createOrder$1(StripePaymentActivity stripePaymentActivity) {
        super(1);
        this.this$0 = stripePaymentActivity;
    }

    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        invoke((CreateOrderResponse) obj);
        return Unit.INSTANCE;
    }

    public final void invoke(final CreateOrderResponse createOrderResponse) {
        Intrinsics.checkParameterIsNotNull(createOrderResponse, "it");
        this.this$0.showProgress(false);
        StripePaymentActivity stripePaymentActivity = this.this$0;
        Bundle bundle = null;
        Intent intent = new Intent(stripePaymentActivity, PaymentSuccessfullyActivity.class);
        new Function1<Intent, Unit>() {
            public /* bridge */ /* synthetic */ Object invoke(Object obj) {
                invoke((Intent) obj);
                return Unit.INSTANCE;
            }

            public final void invoke(Intent intent) {
                Intrinsics.checkParameterIsNotNull(intent, "$receiver");
                intent.putExtra(Constants.KeyIntent.ORDER_DATA, createOrderResponse);
            }
        }.invoke(intent);
        if (Build.VERSION.SDK_INT >= 16) {
            stripePaymentActivity.startActivityForResult(intent, -1, bundle);
        } else {
            stripePaymentActivity.startActivityForResult(intent, -1);
        }
        AppExtensionsKt.getSharedPrefInstance().removeKey(Constants.SharedPref.KEY_USER_CART);
        AppExtensionsKt.getSharedPrefInstance().removeKey(Constants.SharedPref.KEY_CART_COUNT);
        this.this$0.setResult(-1);
        this.this$0.finish();
    }
}
