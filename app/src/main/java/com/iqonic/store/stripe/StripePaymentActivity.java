package com.iqonic.store.stripe;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import androidx.appcompat.widget.Toolbar;
import com.google.android.material.button.MaterialButton;
import com.iqonic.store.AppBaseActivity;
import com.iqonic.store.models.Billing;
import com.iqonic.store.models.CouponLines;
import com.iqonic.store.models.OrderRequest;
import com.iqonic.store.models.RequestModel;
import com.iqonic.store.models.Shipping;
import com.iqonic.store.utils.Constants;
import com.iqonic.store.utils.extensions.AppExtensionsKt;
import com.iqonic.store.utils.extensions.ExtensionsKt;
import com.iqonic.store.utils.extensions.NetworkExtensionKt;
import com.iqonic.store.utils.extensions.StringExtensionsKt;
import com.store.proshop.R;
import com.stripe.android.PaymentConfiguration;
import com.stripe.android.Stripe;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.jvm.internal.Intrinsics;
import kotlin.text.StringsKt;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\b\u0010\b\u001a\u00020\tH\u0002J\b\u0010\n\u001a\u00020\u000bH\u0002J\u0006\u0010\f\u001a\u00020\tJ\"\u0010\r\u001a\u00020\t2\u0006\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u000f2\b\u0010\u0011\u001a\u0004\u0018\u00010\u0012H\u0014J\u0012\u0010\u0013\u001a\u00020\t2\b\u0010\u0014\u001a\u0004\u0018\u00010\u0015H\u0014J\b\u0010\u0016\u001a\u00020\tH\u0002J\b\u0010\u0017\u001a\u00020\tH\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X.¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X.¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X.¢\u0006\u0002\n\u0000¨\u0006\u0018"}, d2 = {"Lcom/iqonic/store/stripe/StripePaymentActivity;", "Lcom/iqonic/store/AppBaseActivity;", "()V", "paymentId", "", "paymentIntentClientSecret", "stripe", "Lcom/stripe/android/Stripe;", "changeColor", "", "checkZeroDecimalCurrencies", "", "createOrder", "onActivityResult", "requestCode", "", "resultCode", "data", "Landroid/content/Intent;", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "showPaymentFailDialog", "startCheckout", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: StripePaymentActivity.kt */
public final class StripePaymentActivity extends AppBaseActivity {
    private HashMap _$_findViewCache;
    /* access modifiers changed from: private */
    public String paymentId;
    /* access modifiers changed from: private */
    public String paymentIntentClientSecret;
    /* access modifiers changed from: private */
    public Stripe stripe;

    public void _$_clearFindViewByIdCache() {
        HashMap hashMap = this._$_findViewCache;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    public View _$_findCachedViewById(int i) {
        if (this._$_findViewCache == null) {
            this._$_findViewCache = new HashMap();
        }
        View view = (View) this._$_findViewCache.get(Integer.valueOf(i));
        if (view != null) {
            return view;
        }
        View findViewById = findViewById(i);
        this._$_findViewCache.put(Integer.valueOf(i), findViewById);
        return findViewById;
    }

    public static final /* synthetic */ String access$getPaymentId$p(StripePaymentActivity stripePaymentActivity) {
        String str = stripePaymentActivity.paymentId;
        if (str == null) {
            Intrinsics.throwUninitializedPropertyAccessException("paymentId");
        }
        return str;
    }

    public static final /* synthetic */ String access$getPaymentIntentClientSecret$p(StripePaymentActivity stripePaymentActivity) {
        String str = stripePaymentActivity.paymentIntentClientSecret;
        if (str == null) {
            Intrinsics.throwUninitializedPropertyAccessException("paymentIntentClientSecret");
        }
        return str;
    }

    public static final /* synthetic */ Stripe access$getStripe$p(StripePaymentActivity stripePaymentActivity) {
        Stripe stripe2 = stripePaymentActivity.stripe;
        if (stripe2 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("stripe");
        }
        return stripe2;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.payment_stripe_main);
        setTitle(getString(R.string.lbl_payment_via_stripe));
        Toolbar toolbar = (Toolbar) _$_findCachedViewById(com.iqonic.store.R.id.toolbar);
        Intrinsics.checkExpressionValueIsNotNull(toolbar, "toolbar");
        setToolbar(toolbar);
        mAppBarColor();
        PaymentConfiguration.Companion companion = PaymentConfiguration.Companion;
        Context applicationContext = getApplicationContext();
        Intrinsics.checkExpressionValueIsNotNull(applicationContext, "applicationContext");
        String string = getString(R.string.stripe_publisher_key);
        Intrinsics.checkExpressionValueIsNotNull(string, "getString(R.string.stripe_publisher_key)");
        companion.init(applicationContext, string);
        if (ExtensionsKt.isNetworkAvailable()) {
            showProgress(true);
            RequestModel requestModel = new RequestModel();
            requestModel.setApiKey(getString(R.string.stripe_secret_key));
            String stringExtra = getIntent().getStringExtra("price");
            Intrinsics.checkExpressionValueIsNotNull(stringExtra, "amount");
            List split$default = StringsKt.split$default((CharSequence) stringExtra, new String[]{"."}, false, 0, 6, (Object) null);
            TextView textView = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.txtAmount);
            Intrinsics.checkExpressionValueIsNotNull(textView, "txtAmount");
            textView.setText(StringExtensionsKt.currencyFormat$default((String) split$default.get(0), (String) null, 1, (Object) null) + " ");
            if (checkZeroDecimalCurrencies()) {
                requestModel.setAmount(Integer.valueOf(Integer.parseInt((String) split$default.get(0))));
            } else {
                requestModel.setAmount(Integer.valueOf(Integer.parseInt((String) split$default.get(0)) * 100));
            }
            requestModel.setCurrency(AppExtensionsKt.getDefaultCurrencyFormate());
            requestModel.setDescription("Online purchased from " + getString(R.string.app_name) + "(Proshop App) Mobile App ");
            NetworkExtensionKt.getRestApiImpl$default((String) null, 1, (Object) null).getStripeClientSecret(requestModel, new StripePaymentActivity$onCreate$1(this), new StripePaymentActivity$onCreate$2(this));
            return;
        }
        ExtensionsKt.noInternetSnackBar(this);
    }

    /* access modifiers changed from: private */
    public final void startCheckout() {
        View findViewById = findViewById(R.id.tvPay);
        Intrinsics.checkExpressionValueIsNotNull(findViewById, "findViewById(R.id.tvPay)");
        ((Button) findViewById).setOnClickListener(new StripePaymentActivity$startCheckout$1(this));
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        Stripe stripe2 = this.stripe;
        if (stripe2 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("stripe");
        }
        stripe2.onPaymentResult(i, intent, new StripePaymentActivity$onActivityResult$1(this));
    }

    public final void createOrder() {
        OrderRequest orderRequest = new OrderRequest((String) null, (String) null, 0, (String) null, (String) null, false, (Billing) null, (Shipping) null, (ArrayList) null, (ArrayList) null, (ArrayList) null, 2047, (DefaultConstructorMarker) null);
        orderRequest.setPayment_method("stripe");
        String str = this.paymentId;
        if (str == null) {
            Intrinsics.throwUninitializedPropertyAccessException("paymentId");
        }
        orderRequest.setTransaction_id(str);
        orderRequest.setCustomer_id(Integer.parseInt(AppExtensionsKt.getUserId()));
        orderRequest.setCurrency(AppExtensionsKt.getDefaultCurrencyFormate());
        orderRequest.setStatus("pending");
        orderRequest.setSet_paid(true);
        Billing billing = new Billing();
        billing.setAddress_1(AppExtensionsKt.getbillingList().getAddress_1());
        billing.setAddress_2(AppExtensionsKt.getbillingList().getAddress_2());
        billing.setCity(AppExtensionsKt.getbillingList().getCity());
        billing.setCountry(AppExtensionsKt.getbillingList().getCountry());
        billing.setState(AppExtensionsKt.getbillingList().getState());
        billing.setPhone(AppExtensionsKt.getbillingList().getPhone());
        billing.setEmail(AppExtensionsKt.getEmail());
        billing.setFirst_name(AppExtensionsKt.getbillingList().getFirst_name());
        billing.setLast_name(AppExtensionsKt.getbillingList().getLast_name());
        Shipping shipping = new Shipping();
        shipping.setAddress_1(AppExtensionsKt.getShippingList().getAddress_1());
        shipping.setAddress_2(AppExtensionsKt.getShippingList().getAddress_2());
        shipping.setCity(AppExtensionsKt.getShippingList().getCity());
        shipping.setCountry(AppExtensionsKt.getShippingList().getCountry());
        shipping.setState(AppExtensionsKt.getShippingList().getState());
        shipping.setPhone(AppExtensionsKt.getShippingList().getPhone());
        shipping.setFirst_name(AppExtensionsKt.getShippingList().getFirst_name());
        shipping.setLast_name(AppExtensionsKt.getShippingList().getLast_name());
        orderRequest.setBilling(billing);
        orderRequest.setShipping(shipping);
        String stringExtra = getIntent().getStringExtra(Constants.KeyIntent.COUPON_CODE);
        Serializable serializableExtra = getIntent().getSerializableExtra(Constants.KeyIntent.PRODUCTDATA);
        if (serializableExtra != null) {
            orderRequest.setLine_items((ArrayList) serializableExtra);
            Intrinsics.checkExpressionValueIsNotNull(stringExtra, "couponCode");
            if (stringExtra.length() > 0) {
                ArrayList arrayList = new ArrayList(1);
                CouponLines couponLines = new CouponLines((String) null, 1, (DefaultConstructorMarker) null);
                couponLines.setCode(stringExtra);
                arrayList.add(couponLines);
                orderRequest.setCoupon_lines(arrayList);
            }
            NetworkExtensionKt.getRestApiImpl$default((String) null, 1, (Object) null).createOrderRequest(orderRequest, new StripePaymentActivity$createOrder$1(this), new StripePaymentActivity$createOrder$2(this));
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type kotlin.collections.ArrayList<com.iqonic.store.models.Line_items> /* = java.util.ArrayList<com.iqonic.store.models.Line_items> */");
    }

    /* access modifiers changed from: private */
    public final void showPaymentFailDialog() {
        Dialog dialog = new Dialog(this);
        dialog.setCancelable(false);
        Window window = dialog.getWindow();
        if (window != null) {
            window.setBackgroundDrawable(new ColorDrawable(0));
        }
        dialog.setContentView(R.layout.dialog_failed_transaction);
        Window window2 = dialog.getWindow();
        if (window2 != null) {
            window2.setLayout(-1, -2);
        }
        TextView textView = (TextView) dialog.findViewById(com.iqonic.store.R.id.tv_close);
        textView.setOnClickListener(new StripePaymentActivity$showPaymentFailDialog$$inlined$onClick$1(textView, this, dialog));
        dialog.show();
    }

    private final boolean checkZeroDecimalCurrencies() {
        String defaultCurrencyFormate = AppExtensionsKt.getDefaultCurrencyFormate();
        if (defaultCurrencyFormate != null) {
            String upperCase = defaultCurrencyFormate.toUpperCase();
            Intrinsics.checkExpressionValueIsNotNull(upperCase, "(this as java.lang.String).toUpperCase()");
            return StringsKt.contains$default(r1, (CharSequence) upperCase, false, 2, (Object) null);
        }
        throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
    }

    private final void changeColor() {
        MaterialButton materialButton = (MaterialButton) _$_findCachedViewById(com.iqonic.store.R.id.tvPay);
        Intrinsics.checkExpressionValueIsNotNull(materialButton, "tvPay");
        ExtensionsKt.changeTint(materialButton, AppExtensionsKt.getAccentColor());
        TextView textView = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.lblCardDetail);
        Intrinsics.checkExpressionValueIsNotNull(textView, "lblCardDetail");
        ExtensionsKt.changeTextPrimaryColor(textView);
        TextView textView2 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.lblAmountPayable);
        Intrinsics.checkExpressionValueIsNotNull(textView2, "lblAmountPayable");
        ExtensionsKt.changeTextPrimaryColor(textView2);
        TextView textView3 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.txtAmount);
        Intrinsics.checkExpressionValueIsNotNull(textView3, "txtAmount");
        ExtensionsKt.changeTextPrimaryColor(textView3);
        TextView textView4 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.txtPaymentMessage);
        Intrinsics.checkExpressionValueIsNotNull(textView4, "txtPaymentMessage");
        ExtensionsKt.changeTextPrimaryColor(textView4);
        TextView textView5 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.lblTransactionId);
        Intrinsics.checkExpressionValueIsNotNull(textView5, "lblTransactionId");
        ExtensionsKt.changeTextSecondaryColor(textView5);
        TextView textView6 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.transactionId);
        Intrinsics.checkExpressionValueIsNotNull(textView6, "transactionId");
        ExtensionsKt.changeTextPrimaryColor(textView6);
        MaterialButton materialButton2 = (MaterialButton) _$_findCachedViewById(com.iqonic.store.R.id.tvOrderAgain);
        Intrinsics.checkExpressionValueIsNotNull(materialButton2, "tvOrderAgain");
        ExtensionsKt.changeTint(materialButton2, AppExtensionsKt.getAccentColor());
    }
}
