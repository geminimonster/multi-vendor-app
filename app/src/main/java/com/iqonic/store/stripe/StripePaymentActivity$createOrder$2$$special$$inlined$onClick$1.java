package com.iqonic.store.stripe;

import android.view.View;
import android.widget.TextView;
import com.google.android.material.button.MaterialButton;
import com.iqonic.store.R;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\b\u0003\n\u0002\b\u0004\u0010\u0000\u001a\u00020\u0001\"\b\b\u0000\u0010\u0002*\u00020\u00032\u000e\u0010\u0004\u001a\n \u0005*\u0004\u0018\u00010\u00030\u0003H\n¢\u0006\u0002\b\u0006¨\u0006\u0007"}, d2 = {"<anonymous>", "", "T", "Landroid/view/View;", "it", "kotlin.jvm.PlatformType", "onClick", "com/iqonic/store/utils/extensions/ExtensionsKt$onClick$1"}, k = 3, mv = {1, 1, 16})
/* compiled from: Extensions.kt */
public final class StripePaymentActivity$createOrder$2$$special$$inlined$onClick$1 implements View.OnClickListener {
    final /* synthetic */ View $this_onClick;
    final /* synthetic */ StripePaymentActivity$createOrder$2 this$0;

    public StripePaymentActivity$createOrder$2$$special$$inlined$onClick$1(View view, StripePaymentActivity$createOrder$2 stripePaymentActivity$createOrder$2) {
        this.$this_onClick = view;
        this.this$0 = stripePaymentActivity$createOrder$2;
    }

    public final void onClick(View view) {
        MaterialButton materialButton = (MaterialButton) this.$this_onClick;
        MaterialButton materialButton2 = (MaterialButton) this.this$0.this$0._$_findCachedViewById(R.id.tvOrderAgain);
        Intrinsics.checkExpressionValueIsNotNull(materialButton2, "tvOrderAgain");
        materialButton2.setVisibility(8);
        TextView textView = (TextView) this.this$0.this$0._$_findCachedViewById(R.id.txtPaymentMessage);
        Intrinsics.checkExpressionValueIsNotNull(textView, "txtPaymentMessage");
        textView.setText(this.this$0.this$0.getString(com.store.proshop.R.string.lbl_we_are_replace_your_order));
        this.this$0.this$0.createOrder();
    }
}
