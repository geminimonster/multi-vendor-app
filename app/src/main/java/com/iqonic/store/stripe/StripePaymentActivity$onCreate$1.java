package com.iqonic.store.stripe;

import android.widget.Toast;
import com.iqonic.store.models.GetStripeClientSecret;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n¢\u0006\u0002\b\u0004"}, d2 = {"<anonymous>", "", "it", "Lcom/iqonic/store/models/GetStripeClientSecret;", "invoke"}, k = 3, mv = {1, 1, 16})
/* compiled from: StripePaymentActivity.kt */
final class StripePaymentActivity$onCreate$1 extends Lambda implements Function1<GetStripeClientSecret, Unit> {
    final /* synthetic */ StripePaymentActivity this$0;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    StripePaymentActivity$onCreate$1(StripePaymentActivity stripePaymentActivity) {
        super(1);
        this.this$0 = stripePaymentActivity;
    }

    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        invoke((GetStripeClientSecret) obj);
        return Unit.INSTANCE;
    }

    public final void invoke(GetStripeClientSecret getStripeClientSecret) {
        Intrinsics.checkParameterIsNotNull(getStripeClientSecret, "it");
        boolean z = false;
        this.this$0.showProgress(false);
        if (getStripeClientSecret.getClient_secret().length() > 0) {
            z = true;
        }
        if (z) {
            this.this$0.paymentIntentClientSecret = getStripeClientSecret.getClient_secret();
            this.this$0.startCheckout();
            return;
        }
        Toast.makeText(this.this$0, getStripeClientSecret.getMessage(), 1).show();
        this.this$0.finish();
    }
}
