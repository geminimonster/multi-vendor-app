package com.iqonic.store.stripe;

import android.widget.TextView;
import com.google.android.material.button.MaterialButton;
import com.iqonic.store.R;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n¢\u0006\u0002\b\u0004"}, d2 = {"<anonymous>", "", "it", "", "invoke"}, k = 3, mv = {1, 1, 16})
/* compiled from: StripePaymentActivity.kt */
final class StripePaymentActivity$createOrder$2 extends Lambda implements Function1<String, Unit> {
    final /* synthetic */ StripePaymentActivity this$0;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    StripePaymentActivity$createOrder$2(StripePaymentActivity stripePaymentActivity) {
        super(1);
        this.this$0 = stripePaymentActivity;
    }

    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        invoke((String) obj);
        return Unit.INSTANCE;
    }

    public final void invoke(String str) {
        Intrinsics.checkParameterIsNotNull(str, "it");
        TextView textView = (TextView) this.this$0._$_findCachedViewById(R.id.txtPaymentMessage);
        Intrinsics.checkExpressionValueIsNotNull(textView, "txtPaymentMessage");
        textView.setText(this.this$0.getString(com.store.proshop.R.string.error_something_went_wrong) + ". " + this.this$0.getString(com.store.proshop.R.string.lbl_replace_your_order));
        MaterialButton materialButton = (MaterialButton) this.this$0._$_findCachedViewById(R.id.tvOrderAgain);
        Intrinsics.checkExpressionValueIsNotNull(materialButton, "tvOrderAgain");
        materialButton.setVisibility(0);
        MaterialButton materialButton2 = (MaterialButton) this.this$0._$_findCachedViewById(R.id.tvOrderAgain);
        materialButton2.setOnClickListener(new StripePaymentActivity$createOrder$2$$special$$inlined$onClick$1(materialButton2, this));
        this.this$0.showProgress(false);
    }
}
