package com.iqonic.store;

public final class BuildConfig {
    public static final String APPLICATION_ID = "com.store.proshop";
    public static final String BUILD_TYPE = "release";
    public static final boolean DEBUG = false;
    public static final String FLAVOR = "";
    public static final int VERSION_CODE = 16;
    public static final String VERSION_NAME = "16.0.0";
}
