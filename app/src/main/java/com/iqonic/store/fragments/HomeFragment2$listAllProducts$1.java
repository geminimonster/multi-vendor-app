package com.iqonic.store.fragments;

import android.view.View;
import android.widget.LinearLayout;
import androidx.fragment.app.FragmentActivity;
import com.iqonic.store.AppBaseActivity;
import com.iqonic.store.models.Dashboard;
import com.iqonic.store.models.Product;
import com.iqonic.store.models.SliderView;
import com.iqonic.store.utils.Constants;
import com.iqonic.store.utils.SharedPrefUtils;
import com.iqonic.store.utils.extensions.AppExtensionsKt;
import java.util.List;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n¢\u0006\u0002\b\u0004"}, d2 = {"<anonymous>", "", "it", "Lcom/iqonic/store/models/Dashboard;", "invoke"}, k = 3, mv = {1, 1, 16})
/* compiled from: HomeFragment2.kt */
final class HomeFragment2$listAllProducts$1 extends Lambda implements Function1<Dashboard, Unit> {
    final /* synthetic */ HomeFragment2 this$0;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    HomeFragment2$listAllProducts$1(HomeFragment2 homeFragment2) {
        super(1);
        this.this$0 = homeFragment2;
    }

    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        invoke((Dashboard) obj);
        return Unit.INSTANCE;
    }

    public final void invoke(Dashboard dashboard) {
        Intrinsics.checkParameterIsNotNull(dashboard, "it");
        if (this.this$0.getActivity() != null) {
            FragmentActivity activity = this.this$0.getActivity();
            if (activity != null) {
                ((AppBaseActivity) activity).showProgress(false);
                SharedPrefUtils sharedPrefInstance = AppExtensionsKt.getSharedPrefInstance();
                sharedPrefInstance.removeKey(Constants.SharedPref.WHATSAPP);
                sharedPrefInstance.removeKey(Constants.SharedPref.FACEBOOK);
                sharedPrefInstance.removeKey(Constants.SharedPref.TWITTER);
                sharedPrefInstance.removeKey(Constants.SharedPref.INSTAGRAM);
                sharedPrefInstance.removeKey(Constants.SharedPref.CONTACT);
                sharedPrefInstance.removeKey(Constants.SharedPref.PRIVACY_POLICY);
                sharedPrefInstance.removeKey(Constants.SharedPref.TERM_CONDITION);
                sharedPrefInstance.removeKey(Constants.SharedPref.COPYRIGHT_TEXT);
                sharedPrefInstance.removeKey(Constants.SharedPref.LANGUAGE);
                sharedPrefInstance.setValue(Constants.SharedPref.LANGUAGE, dashboard.getApp_lang());
                sharedPrefInstance.setValue(Constants.SharedPref.DEFAULT_CURRENCY, dashboard.getCurrency_symbol().getCurrency_symbol());
                sharedPrefInstance.setValue(Constants.SharedPref.DEFAULT_CURRENCY_FORMATE, dashboard.getCurrency_symbol().getCurrency());
                sharedPrefInstance.setValue(Constants.SharedPref.WHATSAPP, dashboard.getSocial_link().getWhatsapp());
                sharedPrefInstance.setValue(Constants.SharedPref.FACEBOOK, dashboard.getSocial_link().getFacebook());
                sharedPrefInstance.setValue(Constants.SharedPref.TWITTER, dashboard.getSocial_link().getTwitter());
                sharedPrefInstance.setValue(Constants.SharedPref.INSTAGRAM, dashboard.getSocial_link().getInstagram());
                sharedPrefInstance.setValue(Constants.SharedPref.CONTACT, dashboard.getSocial_link().getContact());
                sharedPrefInstance.setValue(Constants.SharedPref.PRIVACY_POLICY, dashboard.getSocial_link().getPrivacy_policy());
                sharedPrefInstance.setValue(Constants.SharedPref.TERM_CONDITION, dashboard.getSocial_link().getTerm_condition());
                sharedPrefInstance.setValue(Constants.SharedPref.COPYRIGHT_TEXT, dashboard.getSocial_link().getCopyright_text());
                sharedPrefInstance.setValue(Constants.SharedPref.ENABLE_COUPONS, Boolean.valueOf(dashboard.getEnable_coupons()));
                sharedPrefInstance.setValue("payment_method", dashboard.getPayment_method());
                Unit unit = Unit.INSTANCE;
                this.this$0.setNewLocale(dashboard.getApp_lang());
                HomeFragment2 homeFragment2 = this.this$0;
                homeFragment2.onAddCategory(homeFragment2.mViewCategory);
                HomeFragment2 homeFragment22 = this.this$0;
                homeFragment22.onAddSearch(homeFragment22.mViewSearch);
                LinearLayout access$getMLLDynamic$p = this.this$0.mLLDynamic;
                if (access$getMLLDynamic$p == null) {
                    Intrinsics.throwNpe();
                }
                View access$getMViewSearch$p = this.this$0.mViewSearch;
                if (access$getMViewSearch$p == null) {
                    Intrinsics.throwNpe();
                }
                access$getMLLDynamic$p.addView(access$getMViewSearch$p);
                LinearLayout access$getMLLDynamic$p2 = this.this$0.mLLDynamic;
                if (access$getMLLDynamic$p2 == null) {
                    Intrinsics.throwNpe();
                }
                View access$getMViewCategory$p = this.this$0.mViewCategory;
                if (access$getMViewCategory$p == null) {
                    Intrinsics.throwNpe();
                }
                access$getMLLDynamic$p2.addView(access$getMViewCategory$p);
                List<String> sorting = HomeFragment2.access$getMDashboardJson$p(this.this$0).getSorting();
                if (sorting == null) {
                    Intrinsics.throwNpe();
                }
                for (String next : sorting) {
                    switch (next.hashCode()) {
                        case -899647263:
                            if (next.equals(Constants.viewName.VIEW_BANNER) && (!dashboard.getBanner().isEmpty())) {
                                SliderView sliderView = HomeFragment2.access$getMDashboardJson$p(this.this$0).getSliderView();
                                if (sliderView == null) {
                                    Intrinsics.throwNpe();
                                }
                                if (!Intrinsics.areEqual((Object) sliderView.getEnable(), (Object) true)) {
                                    break;
                                } else {
                                    this.this$0.addSlider(dashboard.getBanner());
                                    break;
                                }
                            }
                        case -555079274:
                            if (next.equals(Constants.viewName.VIEW_NEWEST) && (!dashboard.getNewest().isEmpty())) {
                                Product newProduct = HomeFragment2.access$getMDashboardJson$p(this.this$0).getNewProduct();
                                if (newProduct == null) {
                                    Intrinsics.throwNpe();
                                }
                                if (!Intrinsics.areEqual((Object) newProduct.getEnable(), (Object) true)) {
                                    break;
                                } else {
                                    HomeFragment2 homeFragment23 = this.this$0;
                                    View access$getMViewNewest$p = homeFragment23.mViewNewest;
                                    Product newProduct2 = HomeFragment2.access$getMDashboardJson$p(this.this$0).getNewProduct();
                                    if (newProduct2 == null) {
                                        Intrinsics.throwNpe();
                                    }
                                    String title = newProduct2.getTitle();
                                    if (title == null) {
                                        Intrinsics.throwNpe();
                                    }
                                    Product newProduct3 = HomeFragment2.access$getMDashboardJson$p(this.this$0).getNewProduct();
                                    if (newProduct3 == null) {
                                        Intrinsics.throwNpe();
                                    }
                                    String viewAll = newProduct3.getViewAll();
                                    if (viewAll == null) {
                                        Intrinsics.throwNpe();
                                    }
                                    HomeFragment2.onAddView$default(homeFragment23, access$getMViewNewest$p, false, title, viewAll, 102, (String) null, dashboard.getNewest(), 5, 34, (Object) null);
                                    LinearLayout access$getMLLDynamic$p3 = this.this$0.mLLDynamic;
                                    if (access$getMLLDynamic$p3 == null) {
                                        Intrinsics.throwNpe();
                                    }
                                    View access$getMViewNewest$p2 = this.this$0.mViewNewest;
                                    if (access$getMViewNewest$p2 == null) {
                                        Intrinsics.throwNpe();
                                    }
                                    access$getMLLDynamic$p3.addView(access$getMViewNewest$p2);
                                    break;
                                }
                            }
                        case -484772187:
                            if (next.equals(Constants.viewName.VIEW_BEST_SELLING) && (!dashboard.getBest_selling_product().isEmpty())) {
                                Product bestSaleProduct = HomeFragment2.access$getMDashboardJson$p(this.this$0).getBestSaleProduct();
                                if (bestSaleProduct == null) {
                                    Intrinsics.throwNpe();
                                }
                                if (!Intrinsics.areEqual((Object) bestSaleProduct.getEnable(), (Object) true)) {
                                    break;
                                } else {
                                    HomeFragment2 homeFragment24 = this.this$0;
                                    View access$getMViewBestSelling$p = homeFragment24.mViewBestSelling;
                                    Product bestSaleProduct2 = HomeFragment2.access$getMDashboardJson$p(this.this$0).getBestSaleProduct();
                                    if (bestSaleProduct2 == null) {
                                        Intrinsics.throwNpe();
                                    }
                                    String title2 = bestSaleProduct2.getTitle();
                                    if (title2 == null) {
                                        Intrinsics.throwNpe();
                                    }
                                    Product bestSaleProduct3 = HomeFragment2.access$getMDashboardJson$p(this.this$0).getBestSaleProduct();
                                    if (bestSaleProduct3 == null) {
                                        Intrinsics.throwNpe();
                                    }
                                    String viewAll2 = bestSaleProduct3.getViewAll();
                                    if (viewAll2 == null) {
                                        Intrinsics.throwNpe();
                                    }
                                    HomeFragment2.onAddView$default(homeFragment24, access$getMViewBestSelling$p, false, title2, viewAll2, 107, (String) null, dashboard.getBest_selling_product(), 5, 34, (Object) null);
                                    LinearLayout access$getMLLDynamic$p4 = this.this$0.mLLDynamic;
                                    if (access$getMLLDynamic$p4 == null) {
                                        Intrinsics.throwNpe();
                                    }
                                    View access$getMViewBestSelling$p2 = this.this$0.mViewBestSelling;
                                    if (access$getMViewBestSelling$p2 == null) {
                                        Intrinsics.throwNpe();
                                    }
                                    access$getMLLDynamic$p4.addView(access$getMViewBestSelling$p2);
                                    break;
                                }
                            }
                        case -410125065:
                            if (next.equals(Constants.viewName.VIEW_SALE) && (!dashboard.getSale_product().isEmpty())) {
                                Product saleProduct = HomeFragment2.access$getMDashboardJson$p(this.this$0).getSaleProduct();
                                if (saleProduct == null) {
                                    Intrinsics.throwNpe();
                                }
                                if (!Intrinsics.areEqual((Object) saleProduct.getEnable(), (Object) true)) {
                                    break;
                                } else {
                                    HomeFragment2 homeFragment25 = this.this$0;
                                    View access$getMViewSale$p = homeFragment25.mViewSale;
                                    Product saleProduct2 = HomeFragment2.access$getMDashboardJson$p(this.this$0).getSaleProduct();
                                    if (saleProduct2 == null) {
                                        Intrinsics.throwNpe();
                                    }
                                    String title3 = saleProduct2.getTitle();
                                    if (title3 == null) {
                                        Intrinsics.throwNpe();
                                    }
                                    Product saleProduct3 = HomeFragment2.access$getMDashboardJson$p(this.this$0).getSaleProduct();
                                    if (saleProduct3 == null) {
                                        Intrinsics.throwNpe();
                                    }
                                    String viewAll3 = saleProduct3.getViewAll();
                                    if (viewAll3 == null) {
                                        Intrinsics.throwNpe();
                                    }
                                    HomeFragment2.onAddView$default(homeFragment25, access$getMViewSale$p, false, title3, viewAll3, 106, (String) null, dashboard.getSale_product(), 5, 34, (Object) null);
                                    LinearLayout access$getMLLDynamic$p5 = this.this$0.mLLDynamic;
                                    if (access$getMLLDynamic$p5 == null) {
                                        Intrinsics.throwNpe();
                                    }
                                    View access$getMViewSale$p2 = this.this$0.mViewSale;
                                    if (access$getMViewSale$p2 == null) {
                                        Intrinsics.throwNpe();
                                    }
                                    access$getMLLDynamic$p5.addView(access$getMViewSale$p2);
                                    break;
                                }
                            }
                        case -149231023:
                            if (next.equals(Constants.viewName.VIEW_YOU_MAY_LIKE) && (!dashboard.getYou_may_like().isEmpty())) {
                                Product youMayLikeProduct = HomeFragment2.access$getMDashboardJson$p(this.this$0).getYouMayLikeProduct();
                                if (youMayLikeProduct == null) {
                                    Intrinsics.throwNpe();
                                }
                                if (!Intrinsics.areEqual((Object) youMayLikeProduct.getEnable(), (Object) true)) {
                                    break;
                                } else {
                                    HomeFragment2 homeFragment26 = this.this$0;
                                    View access$getMViewYouMayLike$p = homeFragment26.mViewYouMayLike;
                                    Product youMayLikeProduct2 = HomeFragment2.access$getMDashboardJson$p(this.this$0).getYouMayLikeProduct();
                                    if (youMayLikeProduct2 == null) {
                                        Intrinsics.throwNpe();
                                    }
                                    String title4 = youMayLikeProduct2.getTitle();
                                    if (title4 == null) {
                                        Intrinsics.throwNpe();
                                    }
                                    Product youMayLikeProduct3 = HomeFragment2.access$getMDashboardJson$p(this.this$0).getYouMayLikeProduct();
                                    if (youMayLikeProduct3 == null) {
                                        Intrinsics.throwNpe();
                                    }
                                    String viewAll4 = youMayLikeProduct3.getViewAll();
                                    if (viewAll4 == null) {
                                        Intrinsics.throwNpe();
                                    }
                                    HomeFragment2.onAddView$default(homeFragment26, access$getMViewYouMayLike$p, false, title4, viewAll4, 108, Constants.viewName.VIEW_YOU_MAY_LIKE, dashboard.getYou_may_like(), 5, 2, (Object) null);
                                    LinearLayout access$getMLLDynamic$p6 = this.this$0.mLLDynamic;
                                    if (access$getMLLDynamic$p6 == null) {
                                        Intrinsics.throwNpe();
                                    }
                                    View access$getMViewYouMayLike$p2 = this.this$0.mViewYouMayLike;
                                    if (access$getMViewYouMayLike$p2 == null) {
                                        Intrinsics.throwNpe();
                                    }
                                    access$getMLLDynamic$p6.addView(access$getMViewYouMayLike$p2);
                                    break;
                                }
                            }
                        case -123044723:
                            if (next.equals(Constants.viewName.VIEW_FEATURED) && (!dashboard.getFeatured().isEmpty())) {
                                Product feature = HomeFragment2.access$getMDashboardJson$p(this.this$0).getFeature();
                                if (feature == null) {
                                    Intrinsics.throwNpe();
                                }
                                if (!Intrinsics.areEqual((Object) feature.getEnable(), (Object) true)) {
                                    break;
                                } else {
                                    HomeFragment2 homeFragment27 = this.this$0;
                                    View access$getMViewFeatured$p = homeFragment27.mViewFeatured;
                                    Product feature2 = HomeFragment2.access$getMDashboardJson$p(this.this$0).getFeature();
                                    if (feature2 == null) {
                                        Intrinsics.throwNpe();
                                    }
                                    String title5 = feature2.getTitle();
                                    if (title5 == null) {
                                        Intrinsics.throwNpe();
                                    }
                                    Product feature3 = HomeFragment2.access$getMDashboardJson$p(this.this$0).getFeature();
                                    if (feature3 == null) {
                                        Intrinsics.throwNpe();
                                    }
                                    String viewAll5 = feature3.getViewAll();
                                    if (viewAll5 == null) {
                                        Intrinsics.throwNpe();
                                    }
                                    HomeFragment2.onAddView$default(homeFragment27, access$getMViewFeatured$p, false, title5, viewAll5, 103, (String) null, dashboard.getFeatured(), 5, 34, (Object) null);
                                    LinearLayout access$getMLLDynamic$p7 = this.this$0.mLLDynamic;
                                    if (access$getMLLDynamic$p7 == null) {
                                        Intrinsics.throwNpe();
                                    }
                                    View access$getMViewFeatured$p2 = this.this$0.mViewFeatured;
                                    if (access$getMViewFeatured$p2 == null) {
                                        Intrinsics.throwNpe();
                                    }
                                    access$getMLLDynamic$p7.addView(access$getMViewFeatured$p2);
                                    break;
                                }
                            }
                        case 105650780:
                            if (next.equals(Constants.viewName.VIEW_OFFER) && (!dashboard.getOffer().isEmpty())) {
                                Product offerProduct = HomeFragment2.access$getMDashboardJson$p(this.this$0).getOfferProduct();
                                if (offerProduct == null) {
                                    Intrinsics.throwNpe();
                                }
                                if (!Intrinsics.areEqual((Object) offerProduct.getEnable(), (Object) true)) {
                                    break;
                                } else {
                                    HomeFragment2 homeFragment28 = this.this$0;
                                    View access$getMViewOffer$p = homeFragment28.mViewOffer;
                                    Product offerProduct2 = HomeFragment2.access$getMDashboardJson$p(this.this$0).getOfferProduct();
                                    if (offerProduct2 == null) {
                                        Intrinsics.throwNpe();
                                    }
                                    String title6 = offerProduct2.getTitle();
                                    if (title6 == null) {
                                        Intrinsics.throwNpe();
                                    }
                                    Product offerProduct3 = HomeFragment2.access$getMDashboardJson$p(this.this$0).getOfferProduct();
                                    if (offerProduct3 == null) {
                                        Intrinsics.throwNpe();
                                    }
                                    String viewAll6 = offerProduct3.getViewAll();
                                    if (viewAll6 == null) {
                                        Intrinsics.throwNpe();
                                    }
                                    homeFragment28.onAddView(access$getMViewOffer$p, true, title6, viewAll6, 108, Constants.viewName.VIEW_OFFER, dashboard.getOffer(), 2);
                                    LinearLayout access$getMLLDynamic$p8 = this.this$0.mLLDynamic;
                                    if (access$getMLLDynamic$p8 == null) {
                                        Intrinsics.throwNpe();
                                    }
                                    View access$getMViewOffer$p2 = this.this$0.mViewOffer;
                                    if (access$getMViewOffer$p2 == null) {
                                        Intrinsics.throwNpe();
                                    }
                                    access$getMLLDynamic$p8.addView(access$getMViewOffer$p2);
                                    break;
                                }
                            }
                        case 293241485:
                            if (next.equals(Constants.viewName.VIEW_SUGGESTED_FOR_YOU) && (!dashboard.getSuggested_for_you().isEmpty())) {
                                Product suggestionProduct = HomeFragment2.access$getMDashboardJson$p(this.this$0).getSuggestionProduct();
                                if (suggestionProduct == null) {
                                    Intrinsics.throwNpe();
                                }
                                if (!Intrinsics.areEqual((Object) suggestionProduct.getEnable(), (Object) true)) {
                                    break;
                                } else {
                                    HomeFragment2 homeFragment29 = this.this$0;
                                    View access$getMViewSuggested$p = homeFragment29.mViewSuggested;
                                    Product suggestionProduct2 = HomeFragment2.access$getMDashboardJson$p(this.this$0).getSuggestionProduct();
                                    if (suggestionProduct2 == null) {
                                        Intrinsics.throwNpe();
                                    }
                                    String title7 = suggestionProduct2.getTitle();
                                    if (title7 == null) {
                                        Intrinsics.throwNpe();
                                    }
                                    Product suggestionProduct3 = HomeFragment2.access$getMDashboardJson$p(this.this$0).getSuggestionProduct();
                                    if (suggestionProduct3 == null) {
                                        Intrinsics.throwNpe();
                                    }
                                    String viewAll7 = suggestionProduct3.getViewAll();
                                    if (viewAll7 == null) {
                                        Intrinsics.throwNpe();
                                    }
                                    HomeFragment2.onAddView$default(homeFragment29, access$getMViewSuggested$p, false, title7, viewAll7, 108, Constants.viewName.VIEW_SUGGESTED_FOR_YOU, dashboard.getSuggested_for_you(), 5, 2, (Object) null);
                                    LinearLayout access$getMLLDynamic$p9 = this.this$0.mLLDynamic;
                                    if (access$getMLLDynamic$p9 == null) {
                                        Intrinsics.throwNpe();
                                    }
                                    View access$getMViewSuggested$p2 = this.this$0.mViewSuggested;
                                    if (access$getMViewSuggested$p2 == null) {
                                        Intrinsics.throwNpe();
                                    }
                                    access$getMLLDynamic$p9.addView(access$getMViewSuggested$p2);
                                    break;
                                }
                            }
                        case 1356750361:
                            if (next.equals(Constants.viewName.VIEW_DEAL_OF_THE_DAY) && (!dashboard.getDeal_of_the_day().isEmpty())) {
                                Product dealOfTheDay = HomeFragment2.access$getMDashboardJson$p(this.this$0).getDealOfTheDay();
                                if (dealOfTheDay == null) {
                                    Intrinsics.throwNpe();
                                }
                                if (!Intrinsics.areEqual((Object) dealOfTheDay.getEnable(), (Object) true)) {
                                    break;
                                } else {
                                    HomeFragment2 homeFragment210 = this.this$0;
                                    View access$getMViewDealOfTheDay$p = homeFragment210.mViewDealOfTheDay;
                                    Product dealOfTheDay2 = HomeFragment2.access$getMDashboardJson$p(this.this$0).getDealOfTheDay();
                                    if (dealOfTheDay2 == null) {
                                        Intrinsics.throwNpe();
                                    }
                                    String title8 = dealOfTheDay2.getTitle();
                                    if (title8 == null) {
                                        Intrinsics.throwNpe();
                                    }
                                    Product dealOfTheDay3 = HomeFragment2.access$getMDashboardJson$p(this.this$0).getDealOfTheDay();
                                    if (dealOfTheDay3 == null) {
                                        Intrinsics.throwNpe();
                                    }
                                    String viewAll8 = dealOfTheDay3.getViewAll();
                                    if (viewAll8 == null) {
                                        Intrinsics.throwNpe();
                                    }
                                    homeFragment210.onAddView(access$getMViewDealOfTheDay$p, true, title8, viewAll8, 108, Constants.viewName.VIEW_DEAL_OF_THE_DAY, dashboard.getDeal_of_the_day(), 2);
                                    LinearLayout access$getMLLDynamic$p10 = this.this$0.mLLDynamic;
                                    if (access$getMLLDynamic$p10 == null) {
                                        Intrinsics.throwNpe();
                                    }
                                    View access$getMViewDealOfTheDay$p2 = this.this$0.mViewDealOfTheDay;
                                    if (access$getMViewDealOfTheDay$p2 == null) {
                                        Intrinsics.throwNpe();
                                    }
                                    access$getMLLDynamic$p10.addView(access$getMViewDealOfTheDay$p2);
                                    break;
                                }
                            }
                    }
                }
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type com.iqonic.store.AppBaseActivity");
        }
    }
}
