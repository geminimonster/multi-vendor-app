package com.iqonic.store.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.fragment.app.FragmentActivity;
import com.iqonic.store.AppBaseActivity;
import com.iqonic.store.models.RequestModel;
import com.iqonic.store.utils.extensions.AppExtensionsKt;
import com.iqonic.store.utils.extensions.EditTextExtensionsKt;
import com.iqonic.store.utils.extensions.ExtensionsKt;
import com.iqonic.store.utils.extensions.NetworkExtensionKt;
import com.store.proshop.R;
import java.util.HashMap;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.jvm.internal.Intrinsics;
import kotlin.text.StringsKt;

@Metadata(bv = {1, 0, 3}, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0000\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\b\u0010\u0003\u001a\u00020\u0004H\u0002J\b\u0010\u0005\u001a\u00020\u0004H\u0002J\b\u0010\u0006\u001a\u00020\u0004H\u0002J&\u0010\u0007\u001a\u0004\u0018\u00010\b2\u0006\u0010\t\u001a\u00020\n2\b\u0010\u000b\u001a\u0004\u0018\u00010\f2\b\u0010\r\u001a\u0004\u0018\u00010\u000eH\u0016J\u001a\u0010\u000f\u001a\u00020\u00042\u0006\u0010\u0010\u001a\u00020\b2\b\u0010\r\u001a\u0004\u0018\u00010\u000eH\u0016J\b\u0010\u0011\u001a\u00020\u0012H\u0002¨\u0006\u0013"}, d2 = {"Lcom/iqonic/store/fragments/SignUpFragment;", "Lcom/iqonic/store/fragments/BaseFragment;", "()V", "changeColor", "", "createCustomer", "initializeFragment", "onCreateView", "Landroid/view/View;", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "savedInstanceState", "Landroid/os/Bundle;", "onViewCreated", "view", "validate", "", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: SignUpFragment.kt */
public final class SignUpFragment extends BaseFragment {
    private HashMap _$_findViewCache;

    public void _$_clearFindViewByIdCache() {
        HashMap hashMap = this._$_findViewCache;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    public View _$_findCachedViewById(int i) {
        if (this._$_findViewCache == null) {
            this._$_findViewCache = new HashMap();
        }
        View view = (View) this._$_findViewCache.get(Integer.valueOf(i));
        if (view != null) {
            return view;
        }
        View view2 = getView();
        if (view2 == null) {
            return null;
        }
        View findViewById = view2.findViewById(i);
        this._$_findViewCache.put(Integer.valueOf(i), findViewById);
        return findViewById;
    }

    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        _$_clearFindViewByIdCache();
    }

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Intrinsics.checkParameterIsNotNull(layoutInflater, "inflater");
        return layoutInflater.inflate(R.layout.activity_sign_up, viewGroup, false);
    }

    public void onViewCreated(View view, Bundle bundle) {
        Intrinsics.checkParameterIsNotNull(view, "view");
        super.onViewCreated(view, bundle);
        initializeFragment();
    }

    private final void initializeFragment() {
        changeColor();
        TextView textView = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.btnSignUp);
        textView.setOnClickListener(new SignUpFragment$initializeFragment$$inlined$onClick$1(textView, this));
        TextView textView2 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.btnSignIn);
        textView2.setOnClickListener(new SignUpFragment$initializeFragment$$inlined$onClick$2(textView2, this));
        ImageView imageView = (ImageView) _$_findCachedViewById(com.iqonic.store.R.id.backButton);
        imageView.setOnClickListener(new SignUpFragment$initializeFragment$$inlined$onClick$3(imageView, this));
        ImageView imageView2 = (ImageView) _$_findCachedViewById(com.iqonic.store.R.id.ivPwd);
        imageView2.setOnClickListener(new SignUpFragment$initializeFragment$$inlined$onClick$4(imageView2, this));
        ImageView imageView3 = (ImageView) _$_findCachedViewById(com.iqonic.store.R.id.ivConfirmPwd);
        imageView3.setOnClickListener(new SignUpFragment$initializeFragment$$inlined$onClick$5(imageView3, this));
    }

    /* access modifiers changed from: private */
    public final void createCustomer() {
        RequestModel requestModel = new RequestModel();
        EditText editText = (EditText) _$_findCachedViewById(com.iqonic.store.R.id.edtEmail);
        Intrinsics.checkExpressionValueIsNotNull(editText, "edtEmail");
        requestModel.setUserEmail(EditTextExtensionsKt.textToString(editText));
        EditText editText2 = (EditText) _$_findCachedViewById(com.iqonic.store.R.id.edtFirstName);
        Intrinsics.checkExpressionValueIsNotNull(editText2, "edtFirstName");
        requestModel.setFirstName(EditTextExtensionsKt.textToString(editText2));
        EditText editText3 = (EditText) _$_findCachedViewById(com.iqonic.store.R.id.edtLastName);
        Intrinsics.checkExpressionValueIsNotNull(editText3, "edtLastName");
        requestModel.setLastName(EditTextExtensionsKt.textToString(editText3));
        EditText editText4 = (EditText) _$_findCachedViewById(com.iqonic.store.R.id.edtUsername);
        Intrinsics.checkExpressionValueIsNotNull(editText4, "edtUsername");
        requestModel.setUsername(EditTextExtensionsKt.textToString(editText4));
        EditText editText5 = (EditText) _$_findCachedViewById(com.iqonic.store.R.id.edtPassword);
        Intrinsics.checkExpressionValueIsNotNull(editText5, "edtPassword");
        requestModel.setPassword(EditTextExtensionsKt.textToString(editText5));
        FragmentActivity activity = getActivity();
        if (activity == null) {
            Intrinsics.throwNpe();
        }
        if (activity != null) {
            NetworkExtensionKt.createCustomer((AppBaseActivity) activity, requestModel, new SignUpFragment$createCustomer$1(this));
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.iqonic.store.AppBaseActivity");
    }

    /* access modifiers changed from: private */
    public final boolean validate() {
        EditText editText = (EditText) _$_findCachedViewById(com.iqonic.store.R.id.edtFirstName);
        Intrinsics.checkExpressionValueIsNotNull(editText, "edtFirstName");
        if (EditTextExtensionsKt.checkIsEmpty(editText)) {
            EditText editText2 = (EditText) _$_findCachedViewById(com.iqonic.store.R.id.edtFirstName);
            Intrinsics.checkExpressionValueIsNotNull(editText2, "edtFirstName");
            String string = getString(R.string.error_field_required);
            Intrinsics.checkExpressionValueIsNotNull(string, "getString(R.string.error_field_required)");
            EditTextExtensionsKt.showError(editText2, string);
            return false;
        }
        EditText editText3 = (EditText) _$_findCachedViewById(com.iqonic.store.R.id.edtFirstName);
        Intrinsics.checkExpressionValueIsNotNull(editText3, "edtFirstName");
        if (!EditTextExtensionsKt.isValidText(editText3)) {
            EditText editText4 = (EditText) _$_findCachedViewById(com.iqonic.store.R.id.edtFirstName);
            Intrinsics.checkExpressionValueIsNotNull(editText4, "edtFirstName");
            String string2 = getString(R.string.error_validText);
            Intrinsics.checkExpressionValueIsNotNull(string2, "getString(R.string.error_validText)");
            EditTextExtensionsKt.showError(editText4, string2);
            return false;
        }
        EditText editText5 = (EditText) _$_findCachedViewById(com.iqonic.store.R.id.edtLastName);
        Intrinsics.checkExpressionValueIsNotNull(editText5, "edtLastName");
        if (EditTextExtensionsKt.checkIsEmpty(editText5)) {
            EditText editText6 = (EditText) _$_findCachedViewById(com.iqonic.store.R.id.edtLastName);
            Intrinsics.checkExpressionValueIsNotNull(editText6, "edtLastName");
            String string3 = getString(R.string.error_field_required);
            Intrinsics.checkExpressionValueIsNotNull(string3, "getString(R.string.error_field_required)");
            EditTextExtensionsKt.showError(editText6, string3);
            return false;
        }
        EditText editText7 = (EditText) _$_findCachedViewById(com.iqonic.store.R.id.edtLastName);
        Intrinsics.checkExpressionValueIsNotNull(editText7, "edtLastName");
        if (!EditTextExtensionsKt.isValidText(editText7)) {
            EditText editText8 = (EditText) _$_findCachedViewById(com.iqonic.store.R.id.edtFirstName);
            Intrinsics.checkExpressionValueIsNotNull(editText8, "edtFirstName");
            String string4 = getString(R.string.error_validText);
            Intrinsics.checkExpressionValueIsNotNull(string4, "getString(R.string.error_validText)");
            EditTextExtensionsKt.showError(editText8, string4);
            return false;
        }
        EditText editText9 = (EditText) _$_findCachedViewById(com.iqonic.store.R.id.edtEmail);
        Intrinsics.checkExpressionValueIsNotNull(editText9, "edtEmail");
        if (EditTextExtensionsKt.checkIsEmpty(editText9)) {
            EditText editText10 = (EditText) _$_findCachedViewById(com.iqonic.store.R.id.edtEmail);
            Intrinsics.checkExpressionValueIsNotNull(editText10, "edtEmail");
            String string5 = getString(R.string.error_field_required);
            Intrinsics.checkExpressionValueIsNotNull(string5, "getString(R.string.error_field_required)");
            EditTextExtensionsKt.showError(editText10, string5);
            return false;
        }
        EditText editText11 = (EditText) _$_findCachedViewById(com.iqonic.store.R.id.edtEmail);
        Intrinsics.checkExpressionValueIsNotNull(editText11, "edtEmail");
        if (!EditTextExtensionsKt.isValidEmail(editText11)) {
            EditText editText12 = (EditText) _$_findCachedViewById(com.iqonic.store.R.id.edtEmail);
            Intrinsics.checkExpressionValueIsNotNull(editText12, "edtEmail");
            String string6 = getString(R.string.error_enter_valid_email);
            Intrinsics.checkExpressionValueIsNotNull(string6, "getString(R.string.error_enter_valid_email)");
            EditTextExtensionsKt.showError(editText12, string6);
            return false;
        }
        EditText editText13 = (EditText) _$_findCachedViewById(com.iqonic.store.R.id.edtPassword);
        Intrinsics.checkExpressionValueIsNotNull(editText13, "edtPassword");
        if (EditTextExtensionsKt.checkIsEmpty(editText13)) {
            EditText editText14 = (EditText) _$_findCachedViewById(com.iqonic.store.R.id.edtPassword);
            Intrinsics.checkExpressionValueIsNotNull(editText14, "edtPassword");
            String string7 = getString(R.string.error_field_required);
            Intrinsics.checkExpressionValueIsNotNull(string7, "getString(R.string.error_field_required)");
            EditTextExtensionsKt.showError(editText14, string7);
            return false;
        }
        EditText editText15 = (EditText) _$_findCachedViewById(com.iqonic.store.R.id.edtPassword);
        Intrinsics.checkExpressionValueIsNotNull(editText15, "edtPassword");
        if (EditTextExtensionsKt.validPassword(editText15)) {
            EditText editText16 = (EditText) _$_findCachedViewById(com.iqonic.store.R.id.edtPassword);
            Intrinsics.checkExpressionValueIsNotNull(editText16, "edtPassword");
            String string8 = getString(R.string.error_pwd_digit_required);
            Intrinsics.checkExpressionValueIsNotNull(string8, "getString(R.string.error_pwd_digit_required)");
            EditTextExtensionsKt.showError(editText16, string8);
            return false;
        }
        EditText editText17 = (EditText) _$_findCachedViewById(com.iqonic.store.R.id.edtConfirmPassword);
        Intrinsics.checkExpressionValueIsNotNull(editText17, "edtConfirmPassword");
        if (EditTextExtensionsKt.checkIsEmpty(editText17)) {
            EditText editText18 = (EditText) _$_findCachedViewById(com.iqonic.store.R.id.edtConfirmPassword);
            Intrinsics.checkExpressionValueIsNotNull(editText18, "edtConfirmPassword");
            String string9 = getString(R.string.error_field_required);
            Intrinsics.checkExpressionValueIsNotNull(string9, "getString(R.string.error_field_required)");
            EditTextExtensionsKt.showError(editText18, string9);
            return false;
        }
        EditText editText19 = (EditText) _$_findCachedViewById(com.iqonic.store.R.id.edtPassword);
        Intrinsics.checkExpressionValueIsNotNull(editText19, "edtPassword");
        String obj = editText19.getText().toString();
        EditText editText20 = (EditText) _$_findCachedViewById(com.iqonic.store.R.id.edtConfirmPassword);
        Intrinsics.checkExpressionValueIsNotNull(editText20, "edtConfirmPassword");
        if (StringsKt.equals(obj, editText20.getText().toString(), false)) {
            return true;
        }
        EditText editText21 = (EditText) _$_findCachedViewById(com.iqonic.store.R.id.edtConfirmPassword);
        Intrinsics.checkExpressionValueIsNotNull(editText21, "edtConfirmPassword");
        String string10 = getString(R.string.error_password_not_matches);
        Intrinsics.checkExpressionValueIsNotNull(string10, "getString(R.string.error_password_not_matches)");
        EditTextExtensionsKt.showError(editText21, string10);
        return false;
    }

    private final void changeColor() {
        ImageView imageView = (ImageView) _$_findCachedViewById(com.iqonic.store.R.id.ivBackground);
        Intrinsics.checkExpressionValueIsNotNull(imageView, "ivBackground");
        ExtensionsKt.changeBackgroundImageTint(imageView, AppExtensionsKt.getPrimaryColor());
        TextView textView = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.btnSignUp);
        Intrinsics.checkExpressionValueIsNotNull(textView, "btnSignUp");
        ExtensionsKt.changeBackgroundTint(textView, AppExtensionsKt.getButtonColor());
        TextView textView2 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.lblAlreadyAccount);
        Intrinsics.checkExpressionValueIsNotNull(textView2, "lblAlreadyAccount");
        ExtensionsKt.changeTextPrimaryColor(textView2);
        TextView textView3 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.btnSignIn);
        Intrinsics.checkExpressionValueIsNotNull(textView3, "btnSignIn");
        ExtensionsKt.changePrimaryColor(textView3);
        EditText editText = (EditText) _$_findCachedViewById(com.iqonic.store.R.id.edtEmail);
        Intrinsics.checkExpressionValueIsNotNull(editText, "edtEmail");
        ExtensionsKt.changeTextPrimaryColor(editText);
        EditText editText2 = (EditText) _$_findCachedViewById(com.iqonic.store.R.id.edtFirstName);
        Intrinsics.checkExpressionValueIsNotNull(editText2, "edtFirstName");
        ExtensionsKt.changeTextPrimaryColor(editText2);
        EditText editText3 = (EditText) _$_findCachedViewById(com.iqonic.store.R.id.edtLastName);
        Intrinsics.checkExpressionValueIsNotNull(editText3, "edtLastName");
        ExtensionsKt.changeTextPrimaryColor(editText3);
        EditText editText4 = (EditText) _$_findCachedViewById(com.iqonic.store.R.id.edtUsername);
        Intrinsics.checkExpressionValueIsNotNull(editText4, "edtUsername");
        ExtensionsKt.changeTextPrimaryColor(editText4);
        EditText editText5 = (EditText) _$_findCachedViewById(com.iqonic.store.R.id.edtPassword);
        Intrinsics.checkExpressionValueIsNotNull(editText5, "edtPassword");
        ExtensionsKt.changeTextPrimaryColor(editText5);
        EditText editText6 = (EditText) _$_findCachedViewById(com.iqonic.store.R.id.edtConfirmPassword);
        Intrinsics.checkExpressionValueIsNotNull(editText6, "edtConfirmPassword");
        ExtensionsKt.changeTextPrimaryColor(editText6);
        RelativeLayout relativeLayout = (RelativeLayout) _$_findCachedViewById(com.iqonic.store.R.id.rlMain);
        Intrinsics.checkExpressionValueIsNotNull(relativeLayout, "rlMain");
        ExtensionsKt.changeBackgroundColor(relativeLayout);
    }
}
