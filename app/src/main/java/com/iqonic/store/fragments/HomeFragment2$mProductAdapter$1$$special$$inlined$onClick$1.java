package com.iqonic.store.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import com.iqonic.store.ShopHopApp;
import com.iqonic.store.activity.ViewAllProductActivity;
import com.iqonic.store.models.Category;
import kotlin.Metadata;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\b\u0003\n\u0002\b\u0004\u0010\u0000\u001a\u00020\u0001\"\b\b\u0000\u0010\u0002*\u00020\u00032\u000e\u0010\u0004\u001a\n \u0005*\u0004\u0018\u00010\u00030\u0003H\n¢\u0006\u0002\b\u0006¨\u0006\u0007"}, d2 = {"<anonymous>", "", "T", "Landroid/view/View;", "it", "kotlin.jvm.PlatformType", "onClick", "com/iqonic/store/utils/extensions/ExtensionsKt$onClick$1"}, k = 3, mv = {1, 1, 16})
/* compiled from: Extensions.kt */
public final class HomeFragment2$mProductAdapter$1$$special$$inlined$onClick$1 implements View.OnClickListener {
    final /* synthetic */ Category $model$inlined;
    final /* synthetic */ View $this_onClick;
    final /* synthetic */ HomeFragment2$mProductAdapter$1 this$0;

    public HomeFragment2$mProductAdapter$1$$special$$inlined$onClick$1(View view, HomeFragment2$mProductAdapter$1 homeFragment2$mProductAdapter$1, Category category) {
        this.$this_onClick = view;
        this.this$0 = homeFragment2$mProductAdapter$1;
        this.$model$inlined = category;
    }

    public final void onClick(View view) {
        HomeFragment2 homeFragment2 = this.this$0.this$0;
        Intent intent = new Intent(ShopHopApp.Companion.getAppInstance(), ViewAllProductActivity.class);
        new HomeFragment2$mProductAdapter$1$$special$$inlined$onClick$1$lambda$1(this).invoke(intent);
        homeFragment2.startActivityForResult(intent, -1, (Bundle) null);
    }
}
