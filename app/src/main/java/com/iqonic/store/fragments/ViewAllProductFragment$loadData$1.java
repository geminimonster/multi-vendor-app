package com.iqonic.store.fragments;

import androidx.fragment.app.FragmentActivity;
import com.iqonic.store.AppBaseActivity;
import com.iqonic.store.adapter.BaseAdapter;
import com.iqonic.store.models.StoreProductModel;
import com.iqonic.store.models.StoreProductModelNew;
import java.util.ArrayList;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n¢\u0006\u0002\b\u0004"}, d2 = {"<anonymous>", "", "it", "Lcom/iqonic/store/models/StoreProductModelNew;", "invoke"}, k = 3, mv = {1, 1, 16})
/* compiled from: ViewAllProductFragment.kt */
final class ViewAllProductFragment$loadData$1 extends Lambda implements Function1<StoreProductModelNew, Unit> {
    final /* synthetic */ ViewAllProductFragment this$0;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    ViewAllProductFragment$loadData$1(ViewAllProductFragment viewAllProductFragment) {
        super(1);
        this.this$0 = viewAllProductFragment;
    }

    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        invoke((StoreProductModelNew) obj);
        return Unit.INSTANCE;
    }

    public final void invoke(StoreProductModelNew storeProductModelNew) {
        Intrinsics.checkParameterIsNotNull(storeProductModelNew, "it");
        if (this.this$0.getActivity() != null) {
            FragmentActivity activity = this.this$0.getActivity();
            if (activity == null) {
                Intrinsics.throwNpe();
            }
            if (activity != null) {
                ((AppBaseActivity) activity).showProgress(false);
                if (this.this$0.countLoadMore == 1) {
                    this.this$0.mProductAdapter.clearItems();
                }
                this.this$0.mIsLoading = false;
                this.this$0.totalPages = storeProductModelNew.getNumOfPages();
                BaseAdapter access$getMProductAdapter$p = this.this$0.mProductAdapter;
                ArrayList<StoreProductModel> data = storeProductModelNew.getData();
                if (data == null) {
                    Intrinsics.throwNpe();
                }
                access$getMProductAdapter$p.addMoreItems(data);
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type com.iqonic.store.AppBaseActivity");
        }
    }
}
