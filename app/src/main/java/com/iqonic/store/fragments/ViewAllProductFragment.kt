package com.iqonic.store.fragments

import android.os.Bundle
import android.view.*
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.iqonic.store.AppBaseActivity
import com.iqonic.store.R
import com.iqonic.store.activity.ProductDetailActivityNew
import com.iqonic.store.adapter.BaseAdapter
import com.iqonic.store.models.Category
import com.iqonic.store.models.RequestModel
import com.iqonic.store.models.StoreProductModel
import com.iqonic.store.utils.Constants
import com.iqonic.store.utils.Constants.KeyIntent.PRODUCT_ID
import com.iqonic.store.utils.Constants.viewAllCode.CATEGORY
import com.iqonic.store.utils.Constants.viewAllCode.FEATURED
import com.iqonic.store.utils.Constants.viewAllCode.NEWEST
import com.iqonic.store.utils.Constants.viewAllCode.SALE
import kotlinx.android.synthetic.main.menu_cart.view.*

class ViewAllProductFragment : BaseFragment() {

    private var showPagination: Boolean? = true
    private var mIsLoading = false
    private var countLoadMore = 1
    private var mCategoryId: Int = 0
    private lateinit var mProductAttributeResponseMsg: String
    private var menuCart: View? = null
    private var mId: Int = 0
    private var filterFragment: FilterFragment? = null
    var image: String = ""
    private var mColorArray = intArrayOf(R.color.colorAccent)

    companion object {
        fun getNewInstance(
            id: Int,
            mCategoryId: Int,
            showPagination: Boolean = true
        ): ViewAllProductFragment {

            val fragment = ViewAllProductFragment()
            val bundle = Bundle()
            bundle.putSerializable(Constants.KeyIntent.VIEWALLID, id)
            bundle.putSerializable(Constants.KeyIntent.KEYID, mCategoryId)
            bundle.putSerializable(Constants.KeyIntent.SHOW_PAGINATION, showPagination)

            fragment.arguments = bundle
            return fragment
        }
    }

    private val mSubCategoryAdapter =
        BaseAdapter<Category>(R.layout.item_subcategory, onBind = { view, model, position ->
            view.tvSubCategory.text = model.name
            if (model.image != null) {
                if (model.image.src.isNotEmpty()) {
                    view.ivProducts.loadImageFromUrl(model.image.src)
                    view.ivProducts.visibility = View.VISIBLE
                }
            } else {
                view.ivProducts.visibility = View.GONE
            }
            view.llMain.setStrokedBackground(
                (activity as AppBaseActivity).color(R.color.transparent),
                (activity as AppBaseActivity).color(mColorArray[position % mColorArray.size])
            )
            view.tvSubCategory.setTextColor((activity as AppBaseActivity).color(mColorArray[position % mColorArray.size]))
            view.onClick {
                (activity as AppBaseActivity).launchActivity<SubCategoryActivity> {
                    putExtra(Constants.KeyIntent.TITLE, model.name)
                    putExtra(Constants.KeyIntent.KEYID, model.id)
                }
            }
        })

    private val mProductAdapter =
        BaseAdapter<StoreProductModel>(R.layout.item_viewproductgrid, onBind = { view, model, _ ->

            if (model.images!!.isNotEmpty()) {
                view.ivProduct.loadImageFromUrl(model.images!![0].src!!)
                image = model.images!![0].src!!
            }

            view.tvProductName.text = model.name
            if (model.salePrice!!.isEmpty()) {
                view.tvDiscountPrice.text = model.price!!.currencyFormat()
                view.tvOriginalPrice.visibility = View.GONE
                view.tvOriginalPrice.text = ""
                view.tvSale.hide()
            } else {
                if (model.onSale) {
                    view.tvDiscountPrice.text = model.salePrice.currencyFormat()
                    view.tvSale.show()
                    view.tvOriginalPrice.applyStrike()
                    view.tvOriginalPrice.text = model.regularPrice.currencyFormat()
                    view.tvOriginalPrice.visibility = View.VISIBLE
                } else {
                    view.tvDiscountPrice.text = model.regularPrice.currencyFormat()
                    view.tvOriginalPrice.text = ""
                    view.tvOriginalPrice.visibility = View.GONE
                    view.tvSale.hide()
                }
            }
            if (model.attributes!!.isNotEmpty()) {
                view.tvProductWeight.text = model.attributes.get(0).options!![0]
            }
            if (model.purchasable) {
                if (model.stockStatus == "instock") {
                    view.tvAdd.show()
                } else {
                    view.tvAdd.hide()
                }
            } else {
                view.tvAdd.hide()
            }

            view.tvAdd.onClick {
                addCart(model)
            }
            view.onClick {
                activity.launchActivity<ProductDetailActivityNew> {
                    putExtra(PRODUCT_ID, model.id)
                }
            }
        })

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_newest_product, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setHasOptionsMenu(true)

        mId = arguments.getInt(Constants.KeyIntent.VIEWALLID)!!
        mCategoryId = arguments.getInt(Constants.KeyIntent.KEYID)!!
        showPagination = arguments.getBoolean(Constants.KeyIntent.SHOW_PAGINATION)
        mProductAttributeResponseMsg = getString(R.string.lbl_please_wait)

        val linearLayoutManager = GridLayoutManager(activity, 2)
        rvNewestProduct.layoutManager = linearLayoutManager

        rvCategory.apply {
            setHorizontalLayout(false)
            setHasFixedSize(true)
            rvCategory.adapter = mSubCategoryAdapter
            rvCategory.rvItemAnimation()
        }

        rvNewestProduct.apply {
            rvNewestProduct.rvItemAnimation()
            rvNewestProduct.adapter = mProductAdapter

            if (showPagination!!) {
                addOnScrollListener(object : RecyclerView.OnScrollListener() {
                    override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                        super.onScrollStateChanged(recyclerView, newState)
                        val countItem = recyclerView.layoutManager.itemCount

                        var lastVisiblePosition = 0
                        if (recyclerView.layoutManager is LinearLayoutManager) {
                            lastVisiblePosition =
                                (recyclerView.layoutManager as LinearLayoutManager).findLastCompletelyVisibleItemPosition()
                        } else if (recyclerView.layoutManager is GridLayoutManager) {
                            lastVisiblePosition =
                                (recyclerView.layoutManager as GridLayoutManager).findLastCompletelyVisibleItemPosition()
                        }

                        if (lastVisiblePosition != 0 && !mIsLoading && countItem.minus(1) == lastVisiblePosition) {
                            mIsLoading = true
                            countLoadMore = countLoadMore.plus(1)
                            when (mId) {
                                FEATURED -> {
                                    loadFeatured(countLoadMore)
                                }
                                NEWEST -> {
                                    loadProduct(countLoadMore)
                                }
                                SALE -> {
                                    loadOnSale(countLoadMore)
                                }

                            }
                        }
                    }
                })
            }
        }

        when (mId) {
            FEATURED -> {
                loadFeatured()
            }
            NEWEST -> {
                loadProduct()
            }
            CATEGORY -> {
                loadCategory(mCategoryId)
                loadSubCategory(mCategoryId)
            }
            SALE -> {
                loadOnSale()
            }
            else -> {
                loadProduct()
            }
        }
    }

    private fun addCart(model: StoreProductModel) {
        if (isLoggedIn()) {
            val requestModel = RequestModel()
            if (model.type == "variable") {
                requestModel.pro_id = model.variations!![0]
            } else {
                requestModel.pro_id = model.id
            }
            requestModel.quantity = 1
            (activity as AppBaseActivity).addItemToCart(requestModel, onApiSuccess = {
            })
        } else (activity as AppBaseActivity).launchActivity<SignInUpActivity> { }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        menu.clear()
        inflater.inflate(R.menu.menu_dashboard, menu)
        val menuWishItem = menu.findItem(R.id.action_cart)
        menuWishItem.isVisible = true
        menuCart = menuWishItem.actionView
        menuCart!!.ivCart.setColorFilter(activity!!.color(R.color.menuIconColor))
        menuWishItem.actionView.onClick {
            activity!!.launchActivity<MyCartActivity> { }
        }
        setCartCount()
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_search -> {
                activity!!.launchActivity<SearchActivity>()
                true
            }
            R.id.action_filter -> {
                filterFragment!!.show(activity!!.supportFragmentManager, "Filter")
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    fun setCartCount() {
        val count = getCartCount()
        if (menuCart != null) {
            menuCart!!.tvNotificationCount.text = count
            if (count.checkIsEmpty() || count == "0") {
                menuCart!!.tvNotificationCount.hide()
            } else {
                menuCart!!.tvNotificationCount.show()
            }
        }

    }

    private fun loadProduct(countLoadMore: Int = 1) {
        if (isNetworkAvailable()) {
            if (activity == null) return
            (activity!! as AppBaseActivity).showProgress(true)
            getRestApiImpl().listProducts(countLoadMore, onApiSuccess = {
                if (activity == null) return@listProducts
                (activity!! as AppBaseActivity).showProgress(false)
                if (it.isEmpty()) {
                    rvNewestProduct.hide()
                } else {
                    mIsLoading = it.size != 10
                }
                mProductAdapter.addItems(it)
                if (mProductAdapter.getModel().isEmpty()) {
                    rvNewestProduct.hide()
                } else {
                    rvNewestProduct.show()
                }
            }, onApiError = {
                if (activity == null) return@listProducts
                (activity!! as AppBaseActivity).showProgress(false)
                snackBar(it)
            })

        } else {
            (activity!! as AppBaseActivity).showProgress(false)
            (activity as AppBaseActivity).noInternetSnackBar()
        }


    }

    private fun loadCategory(id: Int) {
        if (isNetworkAvailable()) {
            (activity!! as AppBaseActivity).showProgress(true)
            getRestApiImpl().listSingleCategory(id, onApiSuccess = {
                if (activity == null) return@listSingleCategory
                (activity!! as AppBaseActivity).showProgress(false)
                when {
                    it.isNullOrEmpty() -> {
                        rvNewestProduct.hide()
                        rlNoData.show()
                    }
                    else -> {
                        rvNewestProduct.show()
                        mProductAdapter.clearItems()
                        mProductAdapter.addItems(it)
                    }
                }
            }, onApiError = {
                if (activity == null) return@listSingleCategory
                (activity!! as AppBaseActivity).showProgress(false)
                snackBar(it)
            })

        } else {
            (activity!! as AppBaseActivity).showProgress(false)
            (activity as AppBaseActivity).noInternetSnackBar()
        }
    }

    private fun loadSubCategory(id: Int) {
        (activity as AppBaseActivity).listAllCategory(id, onApiSuccess = {
            if (it.isEmpty()) {
                if (rvCategory != null) {
                    rvCategory.hide()
                }
            } else {
                if (rvCategory != null) {
                    rvCategory.show()
                    mSubCategoryAdapter.clearItems()
                    mSubCategoryAdapter.addItems(it)
                }
            }
        })
    }

    private fun loadFeatured(countLoadMore: Int = 1) {
        if (isNetworkAvailable()) {
            (activity!! as AppBaseActivity).showProgress(true)
            getRestApiImpl().listFeaturedProducts(countLoadMore, onApiSuccess = {
                if (activity == null) return@listFeaturedProducts
                (activity!! as AppBaseActivity).showProgress(false)
                if (it.isEmpty()) {
                    rvNewestProduct.hide()
                } else {
                    mIsLoading = it.size != 10
                }
                mProductAdapter.addItems(it)
                if (mProductAdapter.getModel().isEmpty()) {
                    // rlNoData.show()
                    rvNewestProduct.hide()
                } else {
                    //  rlNoData.hide()
                    rvNewestProduct.show()
                }
            }, onApiError = {
                if (activity == null) return@listFeaturedProducts
                (activity!! as AppBaseActivity).showProgress(false)
                snackBar(it)
            })

        } else {
            (activity!! as AppBaseActivity).showProgress(false)
            (activity as AppBaseActivity).noInternetSnackBar()
        }


    }

    private fun loadOnSale(countLoadMore: Int = 1) {
        if (isNetworkAvailable()) {
            (activity!! as AppBaseActivity).showProgress(true)
            getRestApiImpl().listSaleProducts(countLoadMore, onApiSuccess = {
                (activity!! as AppBaseActivity).showProgress(false)
                if (it.isEmpty()) {
                    rvNewestProduct.hide()
                } else {
                    mIsLoading = it.size != 10
                }
                mProductAdapter.addItems(it)
                if (mProductAdapter.getModel().isEmpty()) {
                    // rlNoData.show()
                    rvNewestProduct.hide()
                } else {
                    //  rlNoData.hide()
                    rvNewestProduct.show()
                }
            }, onApiError = {
                (activity!! as AppBaseActivity).showProgress(false)
                snackBar(it)
            })

        }
    }
}
