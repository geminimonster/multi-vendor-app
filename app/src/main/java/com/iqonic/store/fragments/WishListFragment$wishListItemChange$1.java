package com.iqonic.store.fragments;

import android.widget.RelativeLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;
import com.iqonic.store.AppBaseActivity;
import com.iqonic.store.R;
import com.iqonic.store.models.WishList;
import com.iqonic.store.utils.Constants;
import com.iqonic.store.utils.extensions.AppExtensionsKt;
import com.iqonic.store.utils.extensions.ViewExtensionsKt;
import java.util.ArrayList;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0016\u0010\u0002\u001a\u0012\u0012\u0004\u0012\u00020\u00040\u0003j\b\u0012\u0004\u0012\u00020\u0004`\u0005H\n¢\u0006\u0002\b\u0006"}, d2 = {"<anonymous>", "", "it", "Ljava/util/ArrayList;", "Lcom/iqonic/store/models/WishList;", "Lkotlin/collections/ArrayList;", "invoke"}, k = 3, mv = {1, 1, 16})
/* compiled from: WishListFragment.kt */
final class WishListFragment$wishListItemChange$1 extends Lambda implements Function1<ArrayList<WishList>, Unit> {
    final /* synthetic */ WishListFragment this$0;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    WishListFragment$wishListItemChange$1(WishListFragment wishListFragment) {
        super(1);
        this.this$0 = wishListFragment;
    }

    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        invoke((ArrayList<WishList>) (ArrayList) obj);
        return Unit.INSTANCE;
    }

    public final void invoke(ArrayList<WishList> arrayList) {
        Intrinsics.checkParameterIsNotNull(arrayList, "it");
        if (this.this$0.getActivity() != null) {
            FragmentActivity activity = this.this$0.getActivity();
            if (activity != null) {
                ((AppBaseActivity) activity).showProgress(false);
                AppExtensionsKt.getSharedPrefInstance().setValue(Constants.SharedPref.KEY_WISHLIST_COUNT, Integer.valueOf(arrayList.size()));
                if (arrayList.isEmpty()) {
                    RecyclerView recyclerView = (RecyclerView) this.this$0._$_findCachedViewById(R.id.rvWishList);
                    Intrinsics.checkExpressionValueIsNotNull(recyclerView, "rvWishList");
                    ViewExtensionsKt.hide(recyclerView);
                    RelativeLayout relativeLayout = (RelativeLayout) this.this$0._$_findCachedViewById(R.id.rlNoData);
                    Intrinsics.checkExpressionValueIsNotNull(relativeLayout, "rlNoData");
                    ViewExtensionsKt.show(relativeLayout);
                    return;
                }
                RelativeLayout relativeLayout2 = (RelativeLayout) this.this$0._$_findCachedViewById(R.id.rlNoData);
                Intrinsics.checkExpressionValueIsNotNull(relativeLayout2, "rlNoData");
                ViewExtensionsKt.hide(relativeLayout2);
                RecyclerView recyclerView2 = (RecyclerView) this.this$0._$_findCachedViewById(R.id.rvWishList);
                Intrinsics.checkExpressionValueIsNotNull(recyclerView2, "rvWishList");
                ViewExtensionsKt.show(recyclerView2);
                this.this$0.mListAdapter.clearItems();
                this.this$0.mListAdapter.addItems(arrayList);
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type com.iqonic.store.AppBaseActivity");
        }
    }
}
