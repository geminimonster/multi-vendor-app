package com.iqonic.store.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import com.iqonic.store.AppBaseActivity
import com.iqonic.store.R
import com.iqonic.store.activity.ProductDetailActivityNew
import com.iqonic.store.adapter.BaseAdapter
import com.iqonic.store.models.RequestModel
import com.iqonic.store.models.WishList
import com.iqonic.store.utils.Constants

class WishListFragment : BaseFragment() {

    private val mListAdapter =
        BaseAdapter<WishList>(R.layout.item_wishlist, onBind = { view, model, _ ->
            if (activity !== null) {
                view.tvDiscountPrice.text = model.sale_price.currencyFormat()
                view.tvOriginalPrice.text = model.regular_price.currencyFormat()
                view.tvProductName.text = model.name
                view.tvOriginalPrice.applyStrike()
                if (model.full.isNotEmpty()) view.ivProduct.loadImageFromUrl(model.full)
            }
            view.onClick {
                activity.launchActivity<ProductDetailActivityNew> {
                    putExtra(Constants.KeyIntent.PRODUCT_ID, model.pro_id)
                }
            }
            view.ivMoveToCart.onClick {
                val requestModel = RequestModel()
                requestModel.pro_id = model.pro_id
                requestModel.quantity = 1

                getRestApiImpl().addItemToCart(request = requestModel, onApiSuccess = {
                    if (activity == null) return@addItemToCart
                    snackBar(getString(R.string.success_add))
                    (activity as AppBaseActivity).removeFromWishList(requestModel)
                    {
                        if (it) snackBar(getString(R.string.lbl_remove)); hideProgress()
                        wishListItemChange()
                    }
                }, onApiError = {
                    if (activity == null) return@addItemToCart
                    snackBar(it)
                    (activity as AppBaseActivity).removeFromWishList(requestModel)
                    {
                        if (it) snackBar(getString(R.string.lbl_remove)); hideProgress()
                        wishListItemChange()
                    }
                })
            }
        })

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_wishlist, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        rvWishList.apply {
            layoutManager = GridLayoutManager(activity, 2)
            adapter = mListAdapter
            setHasFixedSize(true)
            rvItemAnimation()

        }
        wishListItemChange()

    }

    private fun wishListItemChange() {
        if (isNetworkAvailable()) {
            if (activity == null) return
            (activity as AppBaseActivity).showProgress(true)
            getRestApiImpl().getWishList(onApiSuccess = {
                if (activity == null) return@getWishList
                (activity as AppBaseActivity).showProgress(false)
                getSharedPrefInstance().setValue(Constants.SharedPref.KEY_WISHLIST_COUNT, it.size)
                if (it.isNullOrEmpty()) {
                    rvWishList.hide()
                    rlNoData.show()
                } else {
                    rlNoData.hide()
                    rvWishList.show()
                    mListAdapter.clearItems()
                    mListAdapter.addItems(it)
                }
            }, onApiError = {
                if (activity == null) return@getWishList
                (activity as AppBaseActivity).showProgress(false)
                if (it == "no product available") {
                    getSharedPrefInstance().setValue(Constants.SharedPref.KEY_WISHLIST_COUNT, 0)
                    (activity as AppBaseActivity).sendWishlistBroadcast()
                    rlNoData.show()
                    rvWishList.hide()
                } else {
                    (activity as AppBaseActivity).snackBarError(it)
                }
            })
        } else {
            if (activity == null) return
            (activity as AppBaseActivity).showProgress(false)
            (activity as AppBaseActivity).noInternetSnackBar()
        }
    }

}
