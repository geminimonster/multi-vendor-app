package com.iqonic.store.fragments;

import android.view.View;
import com.iqonic.store.models.Method;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function3;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0007H\n¢\u0006\u0002\b\b"}, d2 = {"<anonymous>", "", "pos", "", "<anonymous parameter 1>", "Landroid/view/View;", "model", "Lcom/iqonic/store/models/Method;", "invoke"}, k = 3, mv = {1, 1, 16})
/* compiled from: MyCartFragment.kt */
final class MyCartFragment$onViewCreated$3 extends Lambda implements Function3<Integer, View, Method, Unit> {
    final /* synthetic */ MyCartFragment this$0;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    MyCartFragment$onViewCreated$3(MyCartFragment myCartFragment) {
        super(3);
        this.this$0 = myCartFragment;
    }

    public /* bridge */ /* synthetic */ Object invoke(Object obj, Object obj2, Object obj3) {
        invoke(((Number) obj).intValue(), (View) obj2, (Method) obj3);
        return Unit.INSTANCE;
    }

    public final void invoke(int i, View view, Method method) {
        Intrinsics.checkParameterIsNotNull(view, "<anonymous parameter 1>");
        Intrinsics.checkParameterIsNotNull(method, "model");
        this.this$0.onShippingMethodChanged(i, method);
    }
}
