package com.iqonic.store.fragments;

import android.widget.LinearLayout;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.FragmentActivity;
import com.iqonic.store.AppBaseActivity;
import com.iqonic.store.R;
import com.iqonic.store.utils.Constants;
import com.iqonic.store.utils.extensions.AppExtensionsKt;
import com.iqonic.store.utils.extensions.ViewExtensionsKt;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n¢\u0006\u0002\b\u0004"}, d2 = {"<anonymous>", "", "it", "", "invoke"}, k = 3, mv = {1, 1, 16})
/* compiled from: MyCartFragment.kt */
final class MyCartFragment$invalidateCartLayout$2 extends Lambda implements Function1<String, Unit> {
    final /* synthetic */ MyCartFragment this$0;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    MyCartFragment$invalidateCartLayout$2(MyCartFragment myCartFragment) {
        super(1);
        this.this$0 = myCartFragment;
    }

    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        invoke((String) obj);
        return Unit.INSTANCE;
    }

    public final void invoke(String str) {
        Intrinsics.checkParameterIsNotNull(str, "it");
        if (this.this$0.getActivity() != null) {
            FragmentActivity activity = this.this$0.getActivity();
            if (activity != null) {
                ((AppBaseActivity) activity).showProgress(false);
                AppExtensionsKt.getSharedPrefInstance().setValue(Constants.SharedPref.KEY_CART_COUNT, 0);
                FragmentActivity activity2 = this.this$0.getActivity();
                if (activity2 != null) {
                    AppExtensionsKt.sendCartCountChangeBroadcast((AppBaseActivity) activity2);
                    LinearLayout linearLayout = (LinearLayout) this.this$0._$_findCachedViewById(R.id.llNoItems);
                    Intrinsics.checkExpressionValueIsNotNull(linearLayout, "llNoItems");
                    ViewExtensionsKt.show(linearLayout);
                    LinearLayout linearLayout2 = (LinearLayout) this.this$0._$_findCachedViewById(R.id.lay_button);
                    Intrinsics.checkExpressionValueIsNotNull(linearLayout2, "lay_button");
                    ViewExtensionsKt.hide(linearLayout2);
                    LinearLayout linearLayout3 = (LinearLayout) this.this$0._$_findCachedViewById(R.id.llShipping);
                    Intrinsics.checkExpressionValueIsNotNull(linearLayout3, "llShipping");
                    ViewExtensionsKt.hide(linearLayout3);
                    NestedScrollView nestedScrollView = (NestedScrollView) this.this$0._$_findCachedViewById(R.id.nsvCart);
                    Intrinsics.checkExpressionValueIsNotNull(nestedScrollView, "nsvCart");
                    ViewExtensionsKt.hide(nestedScrollView);
                    return;
                }
                throw new TypeCastException("null cannot be cast to non-null type com.iqonic.store.AppBaseActivity");
            }
            throw new TypeCastException("null cannot be cast to non-null type com.iqonic.store.AppBaseActivity");
        }
    }
}
