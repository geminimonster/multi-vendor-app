package com.iqonic.store.fragments;

import android.view.View;
import com.iqonic.store.models.Method;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function3;
import kotlin.jvm.internal.Lambda;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0007H\n¢\u0006\u0002\b\b"}, d2 = {"<anonymous>", "", "view", "Landroid/view/View;", "model", "Lcom/iqonic/store/models/Method;", "position", "", "invoke"}, k = 3, mv = {1, 1, 16})
/* compiled from: MyCartFragment.kt */
final class MyCartFragment$mShippingMethodAdapter$1 extends Lambda implements Function3<View, Method, Integer, Unit> {
    final /* synthetic */ MyCartFragment this$0;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    MyCartFragment$mShippingMethodAdapter$1(MyCartFragment myCartFragment) {
        super(3);
        this.this$0 = myCartFragment;
    }

    public /* bridge */ /* synthetic */ Object invoke(Object obj, Object obj2, Object obj3) {
        invoke((View) obj, (Method) obj2, ((Number) obj3).intValue());
        return Unit.INSTANCE;
    }

    /* JADX WARNING: Removed duplicated region for block: B:13:0x0088  */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0097  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void invoke(android.view.View r7, com.iqonic.store.models.Method r8, int r9) {
        /*
            r6 = this;
            java.lang.String r0 = "view"
            kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(r7, r0)
            java.lang.String r0 = "model"
            kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(r8, r0)
            java.lang.String r0 = r8.getId()
            java.lang.String r1 = "free_shipping"
            boolean r0 = kotlin.jvm.internal.Intrinsics.areEqual((java.lang.Object) r0, (java.lang.Object) r1)
            r1 = 0
            java.lang.String r2 = "view.shippingMethod"
            if (r0 != 0) goto L_0x006c
            java.lang.String r0 = r8.getCost()
            java.lang.String r3 = "0"
            boolean r0 = kotlin.jvm.internal.Intrinsics.areEqual((java.lang.Object) r0, (java.lang.Object) r3)
            if (r0 != 0) goto L_0x006c
            java.lang.String r0 = r8.getCost()
            java.lang.CharSequence r0 = (java.lang.CharSequence) r0
            int r0 = r0.length()
            r3 = 1
            if (r0 != 0) goto L_0x0036
            r0 = 1
            goto L_0x0037
        L_0x0036:
            r0 = 0
        L_0x0037:
            if (r0 == 0) goto L_0x003a
            goto L_0x006c
        L_0x003a:
            int r0 = com.iqonic.store.R.id.shippingMethod
            android.view.View r0 = r7.findViewById(r0)
            android.widget.TextView r0 = (android.widget.TextView) r0
            kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r0, r2)
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = r8.getMethodTitle()
            r4.append(r5)
            java.lang.String r5 = ": "
            r4.append(r5)
            java.lang.String r8 = r8.getCost()
            r5 = 0
            java.lang.String r8 = com.iqonic.store.utils.extensions.StringExtensionsKt.currencyFormat$default(r8, r5, r3, r5)
            r4.append(r8)
            java.lang.String r8 = r4.toString()
            java.lang.CharSequence r8 = (java.lang.CharSequence) r8
            r0.setText(r8)
            goto L_0x0080
        L_0x006c:
            int r0 = com.iqonic.store.R.id.shippingMethod
            android.view.View r0 = r7.findViewById(r0)
            android.widget.TextView r0 = (android.widget.TextView) r0
            kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r0, r2)
            java.lang.String r8 = r8.getMethodTitle()
            java.lang.CharSequence r8 = (java.lang.CharSequence) r8
            r0.setText(r8)
        L_0x0080:
            com.iqonic.store.fragments.MyCartFragment r8 = r6.this$0
            int r8 = r8.selectedMethod
            if (r8 != r9) goto L_0x0097
            int r8 = com.iqonic.store.R.id.imgDone
            android.view.View r8 = r7.findViewById(r8)
            android.widget.ImageView r8 = (android.widget.ImageView) r8
            r9 = 2131230925(0x7f0800cd, float:1.8077917E38)
            r8.setImageResource(r9)
            goto L_0x00a2
        L_0x0097:
            int r8 = com.iqonic.store.R.id.imgDone
            android.view.View r8 = r7.findViewById(r8)
            android.widget.ImageView r8 = (android.widget.ImageView) r8
            r8.setImageResource(r1)
        L_0x00a2:
            int r8 = com.iqonic.store.R.id.shippingMethod
            android.view.View r7 = r7.findViewById(r8)
            android.widget.TextView r7 = (android.widget.TextView) r7
            kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r7, r2)
            com.iqonic.store.utils.extensions.ExtensionsKt.changeTextPrimaryColor(r7)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.iqonic.store.fragments.MyCartFragment$mShippingMethodAdapter$1.invoke(android.view.View, com.iqonic.store.models.Method, int):void");
    }
}
