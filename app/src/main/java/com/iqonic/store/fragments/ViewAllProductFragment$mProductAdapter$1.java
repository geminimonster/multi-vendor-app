package com.iqonic.store.fragments;

import android.graphics.Color;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.iqonic.store.R;
import com.iqonic.store.models.Attributes;
import com.iqonic.store.models.Image;
import com.iqonic.store.models.StoreProductModel;
import com.iqonic.store.utils.extensions.AppExtensionsKt;
import com.iqonic.store.utils.extensions.ExtensionsKt;
import com.iqonic.store.utils.extensions.NetworkExtensionKt;
import com.iqonic.store.utils.extensions.StringExtensionsKt;
import com.iqonic.store.utils.extensions.ViewExtensionsKt;
import java.util.List;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function3;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;
import kotlin.text.StringsKt;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0007H\n¢\u0006\u0002\b\b"}, d2 = {"<anonymous>", "", "view", "Landroid/view/View;", "model", "Lcom/iqonic/store/models/StoreProductModel;", "<anonymous parameter 2>", "", "invoke"}, k = 3, mv = {1, 1, 16})
/* compiled from: ViewAllProductFragment.kt */
final class ViewAllProductFragment$mProductAdapter$1 extends Lambda implements Function3<View, StoreProductModel, Integer, Unit> {
    final /* synthetic */ ViewAllProductFragment this$0;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    ViewAllProductFragment$mProductAdapter$1(ViewAllProductFragment viewAllProductFragment) {
        super(3);
        this.this$0 = viewAllProductFragment;
    }

    public /* bridge */ /* synthetic */ Object invoke(Object obj, Object obj2, Object obj3) {
        invoke((View) obj, (StoreProductModel) obj2, ((Number) obj3).intValue());
        return Unit.INSTANCE;
    }

    public final void invoke(View view, StoreProductModel storeProductModel, int i) {
        Intrinsics.checkParameterIsNotNull(view, "view");
        Intrinsics.checkParameterIsNotNull(storeProductModel, "model");
        List<Image> images = storeProductModel.getImages();
        if (images == null) {
            Intrinsics.throwNpe();
        }
        if (!images.isEmpty()) {
            List<Image> images2 = storeProductModel.getImages();
            if (images2 == null) {
                Intrinsics.throwNpe();
            }
            String src = images2.get(0).getSrc();
            if (src == null) {
                Intrinsics.throwNpe();
            }
            if (src.length() > 0) {
                ImageView imageView = (ImageView) view.findViewById(R.id.ivProduct);
                Intrinsics.checkExpressionValueIsNotNull(imageView, "view.ivProduct");
                List<Image> images3 = storeProductModel.getImages();
                if (images3 == null) {
                    Intrinsics.throwNpe();
                }
                String src2 = images3.get(0).getSrc();
                if (src2 == null) {
                    Intrinsics.throwNpe();
                }
                NetworkExtensionKt.loadImageFromUrl$default(imageView, src2, 0, 0, 6, (Object) null);
            }
        } else {
            ImageView imageView2 = (ImageView) view.findViewById(R.id.ivProduct);
            Intrinsics.checkExpressionValueIsNotNull(imageView2, "view.ivProduct");
            AppExtensionsKt.loadImageFromDrawable(imageView2, com.store.proshop.R.drawable.app_logo);
        }
        String name = storeProductModel.getName();
        if (name == null) {
            Intrinsics.throwNpe();
        }
        List split$default = StringsKt.split$default((CharSequence) name, new String[]{","}, false, 0, 6, (Object) null);
        TextView textView = (TextView) view.findViewById(R.id.tvProductName);
        Intrinsics.checkExpressionValueIsNotNull(textView, "view.tvProductName");
        textView.setText((CharSequence) split$default.get(0));
        TextView textView2 = (TextView) view.findViewById(R.id.tvProductWeight);
        Intrinsics.checkExpressionValueIsNotNull(textView2, "view.tvProductWeight");
        ExtensionsKt.changePrimaryColor(textView2);
        TextView textView3 = (TextView) view.findViewById(R.id.tvProductName);
        Intrinsics.checkExpressionValueIsNotNull(textView3, "view.tvProductName");
        ExtensionsKt.changeTextPrimaryColor(textView3);
        TextView textView4 = (TextView) view.findViewById(R.id.tvOriginalPrice);
        Intrinsics.checkExpressionValueIsNotNull(textView4, "view.tvOriginalPrice");
        ExtensionsKt.changeTextSecondaryColor(textView4);
        TextView textView5 = (TextView) view.findViewById(R.id.tvDiscountPrice);
        Intrinsics.checkExpressionValueIsNotNull(textView5, "view.tvDiscountPrice");
        ExtensionsKt.changeTextPrimaryColor(textView5);
        TextView textView6 = (TextView) view.findViewById(R.id.tvAdd);
        Intrinsics.checkExpressionValueIsNotNull(textView6, "view.tvAdd");
        textView6.getBackground().setTint(Color.parseColor(AppExtensionsKt.getAccentColor()));
        if (!storeProductModel.getOnSale()) {
            TextView textView7 = (TextView) view.findViewById(R.id.tvDiscountPrice);
            Intrinsics.checkExpressionValueIsNotNull(textView7, "view.tvDiscountPrice");
            String price = storeProductModel.getPrice();
            if (price == null) {
                Intrinsics.throwNpe();
            }
            textView7.setText(StringExtensionsKt.currencyFormat$default(price, (String) null, 1, (Object) null));
            TextView textView8 = (TextView) view.findViewById(R.id.tvOriginalPrice);
            Intrinsics.checkExpressionValueIsNotNull(textView8, "view.tvOriginalPrice");
            textView8.setVisibility(0);
            TextView textView9 = (TextView) view.findViewById(R.id.tvOriginalPrice);
            Intrinsics.checkExpressionValueIsNotNull(textView9, "view.tvOriginalPrice");
            textView9.setText("");
        } else {
            String salePrice = storeProductModel.getSalePrice();
            if (salePrice == null) {
                Intrinsics.throwNpe();
            }
            if (salePrice.length() > 0) {
                TextView textView10 = (TextView) view.findViewById(R.id.tvDiscountPrice);
                Intrinsics.checkExpressionValueIsNotNull(textView10, "view.tvDiscountPrice");
                String salePrice2 = storeProductModel.getSalePrice();
                if (salePrice2 == null) {
                    Intrinsics.throwNpe();
                }
                textView10.setText(StringExtensionsKt.currencyFormat$default(salePrice2, (String) null, 1, (Object) null));
                TextView textView11 = (TextView) view.findViewById(R.id.tvOriginalPrice);
                Intrinsics.checkExpressionValueIsNotNull(textView11, "view.tvOriginalPrice");
                ExtensionsKt.applyStrike(textView11);
                TextView textView12 = (TextView) view.findViewById(R.id.tvOriginalPrice);
                Intrinsics.checkExpressionValueIsNotNull(textView12, "view.tvOriginalPrice");
                String regularPrice = storeProductModel.getRegularPrice();
                if (regularPrice == null) {
                    Intrinsics.throwNpe();
                }
                textView12.setText(StringExtensionsKt.currencyFormat$default(regularPrice, (String) null, 1, (Object) null));
                TextView textView13 = (TextView) view.findViewById(R.id.tvOriginalPrice);
                Intrinsics.checkExpressionValueIsNotNull(textView13, "view.tvOriginalPrice");
                textView13.setVisibility(0);
            } else {
                TextView textView14 = (TextView) view.findViewById(R.id.tvOriginalPrice);
                Intrinsics.checkExpressionValueIsNotNull(textView14, "view.tvOriginalPrice");
                textView14.setVisibility(0);
                String regularPrice2 = storeProductModel.getRegularPrice();
                if (regularPrice2 == null) {
                    Intrinsics.throwNpe();
                }
                if (regularPrice2.length() == 0) {
                    TextView textView15 = (TextView) view.findViewById(R.id.tvOriginalPrice);
                    Intrinsics.checkExpressionValueIsNotNull(textView15, "view.tvOriginalPrice");
                    textView15.setText("");
                    TextView textView16 = (TextView) view.findViewById(R.id.tvDiscountPrice);
                    Intrinsics.checkExpressionValueIsNotNull(textView16, "view.tvDiscountPrice");
                    String price2 = storeProductModel.getPrice();
                    if (price2 == null) {
                        Intrinsics.throwNpe();
                    }
                    textView16.setText(StringExtensionsKt.currencyFormat$default(price2, (String) null, 1, (Object) null));
                } else {
                    TextView textView17 = (TextView) view.findViewById(R.id.tvOriginalPrice);
                    Intrinsics.checkExpressionValueIsNotNull(textView17, "view.tvOriginalPrice");
                    textView17.setText("");
                    TextView textView18 = (TextView) view.findViewById(R.id.tvDiscountPrice);
                    Intrinsics.checkExpressionValueIsNotNull(textView18, "view.tvDiscountPrice");
                    String regularPrice3 = storeProductModel.getRegularPrice();
                    if (regularPrice3 == null) {
                        Intrinsics.throwNpe();
                    }
                    textView18.setText(StringExtensionsKt.currencyFormat$default(regularPrice3, (String) null, 1, (Object) null));
                }
            }
        }
        List<Attributes> attributes = storeProductModel.getAttributes();
        if (attributes == null) {
            Intrinsics.throwNpe();
        }
        if (!attributes.isEmpty()) {
            TextView textView19 = (TextView) view.findViewById(R.id.tvProductWeight);
            Intrinsics.checkExpressionValueIsNotNull(textView19, "view.tvProductWeight");
            List<Attributes> attributes2 = storeProductModel.getAttributes();
            if (attributes2 == null) {
                Intrinsics.throwNpe();
            }
            List<String> options = attributes2.get(0).getOptions();
            if (options == null) {
                Intrinsics.throwNpe();
            }
            textView19.setText(options.get(0));
        }
        if (storeProductModel.getIn_stock()) {
            TextView textView20 = (TextView) view.findViewById(R.id.tvAdd);
            Intrinsics.checkExpressionValueIsNotNull(textView20, "view.tvAdd");
            ViewExtensionsKt.show(textView20);
        } else {
            TextView textView21 = (TextView) view.findViewById(R.id.tvAdd);
            Intrinsics.checkExpressionValueIsNotNull(textView21, "view.tvAdd");
            ViewExtensionsKt.hide(textView21);
        }
        if (!storeProductModel.getPurchasable()) {
            TextView textView22 = (TextView) view.findViewById(R.id.tvAdd);
            Intrinsics.checkExpressionValueIsNotNull(textView22, "view.tvAdd");
            ViewExtensionsKt.hide(textView22);
        } else {
            TextView textView23 = (TextView) view.findViewById(R.id.tvAdd);
            Intrinsics.checkExpressionValueIsNotNull(textView23, "view.tvAdd");
            ViewExtensionsKt.show(textView23);
        }
        view.setOnClickListener(new ViewAllProductFragment$mProductAdapter$1$$special$$inlined$onClick$1(view, this, storeProductModel));
        TextView textView24 = (TextView) view.findViewById(R.id.tvAdd);
        textView24.setOnClickListener(new ViewAllProductFragment$mProductAdapter$1$$special$$inlined$onClick$2(textView24, this, storeProductModel));
    }
}
