package com.iqonic.store.fragments;

import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.fragment.app.FragmentActivity;
import com.google.android.material.button.MaterialButton;
import com.iqonic.store.AppBaseActivity;
import com.iqonic.store.utils.extensions.AppExtensionsKt;
import com.iqonic.store.utils.extensions.EditTextExtensionsKt;
import com.iqonic.store.utils.extensions.ExtensionsKt;
import com.iqonic.store.utils.extensions.NetworkExtensionKt;
import com.store.proshop.R;
import java.util.HashMap;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0000\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\b\u0010\u0003\u001a\u00020\u0004H\u0002J\b\u0010\u0005\u001a\u00020\u0004H\u0002J\b\u0010\u0006\u001a\u00020\u0004H\u0002J&\u0010\u0007\u001a\u0004\u0018\u00010\b2\u0006\u0010\t\u001a\u00020\n2\b\u0010\u000b\u001a\u0004\u0018\u00010\f2\b\u0010\r\u001a\u0004\u0018\u00010\u000eH\u0016J\u001a\u0010\u000f\u001a\u00020\u00042\u0006\u0010\u0010\u001a\u00020\b2\b\u0010\r\u001a\u0004\u0018\u00010\u000eH\u0016J\b\u0010\u0011\u001a\u00020\u0004H\u0002J\b\u0010\u0012\u001a\u00020\u0013H\u0002¨\u0006\u0014"}, d2 = {"Lcom/iqonic/store/fragments/SignInFragment;", "Lcom/iqonic/store/fragments/BaseFragment;", "()V", "changeColor", "", "doLogin", "initializeFragment", "onCreateView", "Landroid/view/View;", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "savedInstanceState", "Landroid/os/Bundle;", "onViewCreated", "view", "showChangePasswordDialog", "validate", "", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: SignInFragment.kt */
public final class SignInFragment extends BaseFragment {
    private HashMap _$_findViewCache;

    public void _$_clearFindViewByIdCache() {
        HashMap hashMap = this._$_findViewCache;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    public View _$_findCachedViewById(int i) {
        if (this._$_findViewCache == null) {
            this._$_findViewCache = new HashMap();
        }
        View view = (View) this._$_findViewCache.get(Integer.valueOf(i));
        if (view != null) {
            return view;
        }
        View view2 = getView();
        if (view2 == null) {
            return null;
        }
        View findViewById = view2.findViewById(i);
        this._$_findViewCache.put(Integer.valueOf(i), findViewById);
        return findViewById;
    }

    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        _$_clearFindViewByIdCache();
    }

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Intrinsics.checkParameterIsNotNull(layoutInflater, "inflater");
        return layoutInflater.inflate(R.layout.activity_sign_in, viewGroup, false);
    }

    public void onViewCreated(View view, Bundle bundle) {
        Intrinsics.checkParameterIsNotNull(view, "view");
        super.onViewCreated(view, bundle);
        initializeFragment();
    }

    private final void initializeFragment() {
        ((EditText) _$_findCachedViewById(com.iqonic.store.R.id.edtEmail)).setSelection(((EditText) _$_findCachedViewById(com.iqonic.store.R.id.edtEmail)).length());
        changeColor();
        TextView textView = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.btnSignIn);
        textView.setOnClickListener(new SignInFragment$initializeFragment$$inlined$onClick$1(textView, this));
        TextView textView2 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvForget);
        textView2.setOnClickListener(new SignInFragment$initializeFragment$$inlined$onClick$2(textView2, this));
        TextView textView3 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.btnSignUp);
        textView3.setOnClickListener(new SignInFragment$initializeFragment$$inlined$onClick$3(textView3, this));
        ImageView imageView = (ImageView) _$_findCachedViewById(com.iqonic.store.R.id.backButton);
        imageView.setOnClickListener(new SignInFragment$initializeFragment$$inlined$onClick$4(imageView, this));
        TextView textView4 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvForget);
        textView4.setOnClickListener(new SignInFragment$initializeFragment$$inlined$onClick$5(textView4, this));
        ImageView imageView2 = (ImageView) _$_findCachedViewById(com.iqonic.store.R.id.ivPwd);
        imageView2.setOnClickListener(new SignInFragment$initializeFragment$$inlined$onClick$6(imageView2, this));
    }

    /* access modifiers changed from: private */
    public final boolean validate() {
        EditText editText = (EditText) _$_findCachedViewById(com.iqonic.store.R.id.edtEmail);
        Intrinsics.checkExpressionValueIsNotNull(editText, "edtEmail");
        if (EditTextExtensionsKt.checkIsEmpty(editText)) {
            EditText editText2 = (EditText) _$_findCachedViewById(com.iqonic.store.R.id.edtEmail);
            Intrinsics.checkExpressionValueIsNotNull(editText2, "edtEmail");
            String string = getString(R.string.error_field_required);
            Intrinsics.checkExpressionValueIsNotNull(string, "getString(R.string.error_field_required)");
            EditTextExtensionsKt.showError(editText2, string);
            return false;
        }
        EditText editText3 = (EditText) _$_findCachedViewById(com.iqonic.store.R.id.edtPassword);
        Intrinsics.checkExpressionValueIsNotNull(editText3, "edtPassword");
        if (!EditTextExtensionsKt.checkIsEmpty(editText3)) {
            return true;
        }
        EditText editText4 = (EditText) _$_findCachedViewById(com.iqonic.store.R.id.edtPassword);
        Intrinsics.checkExpressionValueIsNotNull(editText4, "edtPassword");
        String string2 = getString(R.string.error_field_required);
        Intrinsics.checkExpressionValueIsNotNull(string2, "getString(R.string.error_field_required)");
        EditTextExtensionsKt.showError(editText4, string2);
        return false;
    }

    /* access modifiers changed from: private */
    public final void doLogin() {
        EditText editText = (EditText) _$_findCachedViewById(com.iqonic.store.R.id.edtEmail);
        Intrinsics.checkExpressionValueIsNotNull(editText, "edtEmail");
        String textToString = EditTextExtensionsKt.textToString(editText);
        FragmentActivity activity = getActivity();
        if (activity != null) {
            EditText editText2 = (EditText) _$_findCachedViewById(com.iqonic.store.R.id.edtEmail);
            Intrinsics.checkExpressionValueIsNotNull(editText2, "edtEmail");
            String textToString2 = EditTextExtensionsKt.textToString(editText2);
            EditText editText3 = (EditText) _$_findCachedViewById(com.iqonic.store.R.id.edtPassword);
            Intrinsics.checkExpressionValueIsNotNull(editText3, "edtPassword");
            NetworkExtensionKt.signIn((AppBaseActivity) activity, textToString2, EditTextExtensionsKt.textToString(editText3), new SignInFragment$doLogin$2(this), new SignInFragment$doLogin$1(this, textToString));
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.iqonic.store.AppBaseActivity");
    }

    /* access modifiers changed from: private */
    public final void showChangePasswordDialog() {
        FragmentActivity activity = getActivity();
        if (activity == null) {
            Intrinsics.throwNpe();
        }
        Dialog dialog = new Dialog(activity);
        Window window = dialog.getWindow();
        if (window != null) {
            window.setBackgroundDrawable(new ColorDrawable(0));
        }
        dialog.setContentView(R.layout.dialog_change_password);
        Window window2 = dialog.getWindow();
        if (window2 != null) {
            window2.setLayout(-1, -2);
        }
        TextView textView = (TextView) dialog.findViewById(com.iqonic.store.R.id.lblForgotPwd);
        Intrinsics.checkExpressionValueIsNotNull(textView, "changePasswordDialog.lblForgotPwd");
        ExtensionsKt.changeTextPrimaryColor(textView);
        EditText editText = (EditText) dialog.findViewById(com.iqonic.store.R.id.edtForgotEmail);
        Intrinsics.checkExpressionValueIsNotNull(editText, "changePasswordDialog.edtForgotEmail");
        ExtensionsKt.changeTextPrimaryColor(editText);
        MaterialButton materialButton = (MaterialButton) dialog.findViewById(com.iqonic.store.R.id.btnForgotPassword);
        Intrinsics.checkExpressionValueIsNotNull(materialButton, "changePasswordDialog.btnForgotPassword");
        ExtensionsKt.changeBackgroundTint(materialButton, AppExtensionsKt.getButtonColor());
        MaterialButton materialButton2 = (MaterialButton) dialog.findViewById(com.iqonic.store.R.id.btnForgotPassword);
        materialButton2.setOnClickListener(new SignInFragment$showChangePasswordDialog$$inlined$onClick$1(materialButton2, this, dialog));
        dialog.show();
    }

    private final void changeColor() {
        ImageView imageView = (ImageView) _$_findCachedViewById(com.iqonic.store.R.id.ivBackground);
        Intrinsics.checkExpressionValueIsNotNull(imageView, "ivBackground");
        ExtensionsKt.changeBackgroundImageTint(imageView, AppExtensionsKt.getPrimaryColor());
        TextView textView = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.btnSignIn);
        Intrinsics.checkExpressionValueIsNotNull(textView, "btnSignIn");
        ExtensionsKt.changeBackgroundTint(textView, AppExtensionsKt.getButtonColor());
        TextView textView2 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvForget);
        Intrinsics.checkExpressionValueIsNotNull(textView2, "tvForget");
        ExtensionsKt.changeTextSecondaryColor(textView2);
        TextView textView3 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.lblAccount);
        Intrinsics.checkExpressionValueIsNotNull(textView3, "lblAccount");
        ExtensionsKt.changeTextPrimaryColor(textView3);
        TextView textView4 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.btnSignUp);
        Intrinsics.checkExpressionValueIsNotNull(textView4, "btnSignUp");
        ExtensionsKt.changePrimaryColor(textView4);
        EditText editText = (EditText) _$_findCachedViewById(com.iqonic.store.R.id.edtEmail);
        Intrinsics.checkExpressionValueIsNotNull(editText, "edtEmail");
        ExtensionsKt.changeTextPrimaryColor(editText);
        EditText editText2 = (EditText) _$_findCachedViewById(com.iqonic.store.R.id.edtPassword);
        Intrinsics.checkExpressionValueIsNotNull(editText2, "edtPassword");
        ExtensionsKt.changeTextPrimaryColor(editText2);
        LinearLayout linearLayout = (LinearLayout) _$_findCachedViewById(com.iqonic.store.R.id.llMainUI);
        Intrinsics.checkExpressionValueIsNotNull(linearLayout, "llMainUI");
        ExtensionsKt.changeBackgroundColor(linearLayout);
    }
}
