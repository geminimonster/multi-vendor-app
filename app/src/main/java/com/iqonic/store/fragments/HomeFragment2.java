package com.iqonic.store.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;
import com.facebook.appevents.AppEventsConstants;
import com.iqonic.store.AppBaseActivity;
import com.iqonic.store.ShopHopApp;
import com.iqonic.store.activity.SignInUpActivity;
import com.iqonic.store.adapter.BaseAdapter;
import com.iqonic.store.adapter.HomeSliderAdapter;
import com.iqonic.store.models.Attributes;
import com.iqonic.store.models.BuilderDashboard;
import com.iqonic.store.models.Category;
import com.iqonic.store.models.DashboardBanner;
import com.iqonic.store.models.Image;
import com.iqonic.store.models.RequestModel;
import com.iqonic.store.models.StoreProductModel;
import com.iqonic.store.utils.CustomSwipeToRefresh;
import com.iqonic.store.utils.dotsindicator.DotsIndicator;
import com.iqonic.store.utils.extensions.AppExtensionsKt;
import com.iqonic.store.utils.extensions.ExtensionsKt;
import com.iqonic.store.utils.extensions.NetworkExtensionKt;
import com.iqonic.store.utils.extensions.StringExtensionsKt;
import com.iqonic.store.utils.extensions.ViewExtensionsKt;
import com.store.proshop.R;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.jvm.internal.Intrinsics;
import kotlin.text.StringsKt;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000|\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010%\n\u0002\u0010\u000e\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\f\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\t\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u0016\u0010\u001f\u001a\u00020 2\f\u0010!\u001a\b\u0012\u0004\u0012\u00020#0\"H\u0002J\b\u0010$\u001a\u00020 H\u0002J\b\u0010%\u001a\u00020 H\u0002J\u0010\u0010&\u001a\u00020 2\u0006\u0010'\u001a\u00020(H\u0002J\b\u0010)\u001a\u00020 H\u0003J\b\u0010*\u001a\u00020 H\u0003J\u0012\u0010+\u001a\u00020 2\b\u0010,\u001a\u0004\u0018\u00010\u0010H\u0002J\u0012\u0010-\u001a\u00020 2\b\u0010,\u001a\u0004\u0018\u00010\u0010H\u0002JV\u0010.\u001a\u00020 2\b\u0010,\u001a\u0004\u0018\u00010\u00102\b\b\u0002\u0010/\u001a\u00020\t2\u0006\u00100\u001a\u00020\u00052\u0006\u00101\u001a\u00020\u00052\u0006\u00102\u001a\u00020\u00062\b\b\u0002\u00103\u001a\u00020\u00052\f\u0010!\u001a\b\u0012\u0004\u0012\u00020(0\"2\b\b\u0002\u00104\u001a\u00020\u0006H\u0002J\u0018\u00105\u001a\u00020 2\u0006\u00106\u001a\u0002072\u0006\u00108\u001a\u000209H\u0016J&\u0010:\u001a\u0004\u0018\u00010\u00102\u0006\u00108\u001a\u00020;2\b\u0010<\u001a\u0004\u0018\u00010=2\b\u0010>\u001a\u0004\u0018\u00010?H\u0016J\u001a\u0010@\u001a\u00020 2\u0006\u0010A\u001a\u00020\u00102\b\u0010>\u001a\u0004\u0018\u00010?H\u0016J\u0006\u0010B\u001a\u00020 J\u0010\u0010C\u001a\u00020 2\u0006\u0010D\u001a\u00020\u0005H\u0002J\"\u0010E\u001a\u00020 2\u0006\u0010A\u001a\u00020\u00102\u0006\u0010'\u001a\u00020(2\b\b\u0002\u0010F\u001a\u00020\tH\u0002J\b\u0010G\u001a\u00020 H\u0002R\u001a\u0010\u0003\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0005X\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u0005X.¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\fX.¢\u0006\u0002\n\u0000R\u0010\u0010\r\u001a\u0004\u0018\u00010\u000eX\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\u000f\u001a\u0004\u0018\u00010\u0010X\u000e¢\u0006\u0002\n\u0000R\u0014\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u00130\u0012X\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u0014\u001a\u0004\u0018\u00010\u0010X\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\u0015\u001a\u0004\u0018\u00010\u0010X\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\u0016\u001a\u0004\u0018\u00010\u0010X\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\u0017\u001a\u0004\u0018\u00010\u0010X\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\u0018\u001a\u0004\u0018\u00010\u0010X\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\u0019\u001a\u0004\u0018\u00010\u0010X\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\u001a\u001a\u0004\u0018\u00010\u0010X\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\u001b\u001a\u0004\u0018\u00010\u0010X\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\u001c\u001a\u0004\u0018\u00010\u0010X\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\u001d\u001a\u0004\u0018\u00010\u0010X\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\u001e\u001a\u0004\u0018\u00010\u0010X\u000e¢\u0006\u0002\n\u0000¨\u0006H"}, d2 = {"Lcom/iqonic/store/fragments/HomeFragment2;", "Lcom/iqonic/store/fragments/BaseFragment;", "()V", "data", "", "", "", "image", "isAddedToCart", "", "lan", "mDashboardJson", "Lcom/iqonic/store/models/BuilderDashboard;", "mLLDynamic", "Landroid/widget/LinearLayout;", "mMenuCart", "Landroid/view/View;", "mProductAdapter", "Lcom/iqonic/store/adapter/BaseAdapter;", "Lcom/iqonic/store/models/Category;", "mSliderView", "mViewBestSelling", "mViewCategory", "mViewDealOfTheDay", "mViewFeatured", "mViewNewest", "mViewOffer", "mViewSale", "mViewSearch", "mViewSuggested", "mViewYouMayLike", "addSlider", "", "productList", "", "Lcom/iqonic/store/models/DashboardBanner;", "listAllProducts", "loadApis", "mAddCart", "model", "Lcom/iqonic/store/models/StoreProductModel;", "mProductUI", "mSliderUI", "onAddCategory", "mView", "onAddSearch", "onAddView", "isGridView", "title", "mViewAll", "code", "specialKey", "modelSize", "onCreateOptionsMenu", "menu", "Landroid/view/Menu;", "inflater", "Landroid/view/MenuInflater;", "onCreateView", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "savedInstanceState", "Landroid/os/Bundle;", "onViewCreated", "view", "setCartCount", "setNewLocale", "language", "setProductItem", "params", "showLoader", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: HomeFragment2.kt */
public final class HomeFragment2 extends BaseFragment {
    private HashMap _$_findViewCache;
    private final Map<String, Integer> data = new HashMap();
    private String image = "";
    /* access modifiers changed from: private */
    public boolean isAddedToCart;
    private String lan;
    /* access modifiers changed from: private */
    public BuilderDashboard mDashboardJson;
    /* access modifiers changed from: private */
    public LinearLayout mLLDynamic;
    private View mMenuCart;
    /* access modifiers changed from: private */
    public final BaseAdapter<Category> mProductAdapter = new BaseAdapter<>(R.layout.dashboard_item_category, new HomeFragment2$mProductAdapter$1(this));
    private View mSliderView;
    /* access modifiers changed from: private */
    public View mViewBestSelling;
    /* access modifiers changed from: private */
    public View mViewCategory;
    /* access modifiers changed from: private */
    public View mViewDealOfTheDay;
    /* access modifiers changed from: private */
    public View mViewFeatured;
    /* access modifiers changed from: private */
    public View mViewNewest;
    /* access modifiers changed from: private */
    public View mViewOffer;
    /* access modifiers changed from: private */
    public View mViewSale;
    /* access modifiers changed from: private */
    public View mViewSearch;
    /* access modifiers changed from: private */
    public View mViewSuggested;
    /* access modifiers changed from: private */
    public View mViewYouMayLike;

    public void _$_clearFindViewByIdCache() {
        HashMap hashMap = this._$_findViewCache;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    public View _$_findCachedViewById(int i) {
        if (this._$_findViewCache == null) {
            this._$_findViewCache = new HashMap();
        }
        View view = (View) this._$_findViewCache.get(Integer.valueOf(i));
        if (view != null) {
            return view;
        }
        View view2 = getView();
        if (view2 == null) {
            return null;
        }
        View findViewById = view2.findViewById(i);
        this._$_findViewCache.put(Integer.valueOf(i), findViewById);
        return findViewById;
    }

    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        _$_clearFindViewByIdCache();
    }

    public static final /* synthetic */ BuilderDashboard access$getMDashboardJson$p(HomeFragment2 homeFragment2) {
        BuilderDashboard builderDashboard = homeFragment2.mDashboardJson;
        if (builderDashboard == null) {
            Intrinsics.throwUninitializedPropertyAccessException("mDashboardJson");
        }
        return builderDashboard;
    }

    static /* synthetic */ void setProductItem$default(HomeFragment2 homeFragment2, View view, StoreProductModel storeProductModel, boolean z, int i, Object obj) {
        if ((i & 4) != 0) {
            z = false;
        }
        homeFragment2.setProductItem(view, storeProductModel, z);
    }

    private final void setProductItem(View view, StoreProductModel storeProductModel, boolean z) {
        if (!z) {
            ImageView imageView = (ImageView) view.findViewById(com.iqonic.store.R.id.ivProduct);
            Intrinsics.checkExpressionValueIsNotNull(imageView, "view.ivProduct");
            FragmentActivity activity = getActivity();
            imageView.setLayoutParams(activity != null ? AppExtensionsKt.productLayoutParams(activity) : null);
        } else {
            ImageView imageView2 = (ImageView) view.findViewById(com.iqonic.store.R.id.ivProduct);
            Intrinsics.checkExpressionValueIsNotNull(imageView2, "view.ivProduct");
            FragmentActivity activity2 = getActivity();
            imageView2.setLayoutParams(activity2 != null ? AppExtensionsKt.productLayoutParamsForDealOffer(activity2) : null);
        }
        List<Image> images = storeProductModel.getImages();
        if (images == null) {
            Intrinsics.throwNpe();
        }
        String src = images.get(0).getSrc();
        if (src == null) {
            Intrinsics.throwNpe();
        }
        if (src.length() > 0) {
            ImageView imageView3 = (ImageView) view.findViewById(com.iqonic.store.R.id.ivProduct);
            Intrinsics.checkExpressionValueIsNotNull(imageView3, "view.ivProduct");
            List<Image> images2 = storeProductModel.getImages();
            if (images2 == null) {
                Intrinsics.throwNpe();
            }
            String src2 = images2.get(0).getSrc();
            if (src2 == null) {
                Intrinsics.throwNpe();
            }
            NetworkExtensionKt.loadImageFromUrl$default(imageView3, src2, 0, 0, 6, (Object) null);
            List<Image> images3 = storeProductModel.getImages();
            if (images3 == null) {
                Intrinsics.throwNpe();
            }
            String src3 = images3.get(0).getSrc();
            if (src3 == null) {
                Intrinsics.throwNpe();
            }
            this.image = src3;
        }
        String name = storeProductModel.getName();
        if (name == null) {
            Intrinsics.throwNpe();
        }
        List split$default = StringsKt.split$default((CharSequence) name, new String[]{","}, false, 0, 6, (Object) null);
        TextView textView = (TextView) view.findViewById(com.iqonic.store.R.id.tvProductName);
        Intrinsics.checkExpressionValueIsNotNull(textView, "view.tvProductName");
        textView.setText((CharSequence) split$default.get(0));
        TextView textView2 = (TextView) view.findViewById(com.iqonic.store.R.id.tvProductName);
        Intrinsics.checkExpressionValueIsNotNull(textView2, "view.tvProductName");
        ExtensionsKt.changeTextPrimaryColor(textView2);
        if (!storeProductModel.getOnSale()) {
            TextView textView3 = (TextView) view.findViewById(com.iqonic.store.R.id.tvDiscountPrice);
            Intrinsics.checkExpressionValueIsNotNull(textView3, "view.tvDiscountPrice");
            String price = storeProductModel.getPrice();
            if (price == null) {
                Intrinsics.throwNpe();
            }
            textView3.setText(StringExtensionsKt.currencyFormat$default(price, (String) null, 1, (Object) null));
            TextView textView4 = (TextView) view.findViewById(com.iqonic.store.R.id.tvOriginalPrice);
            Intrinsics.checkExpressionValueIsNotNull(textView4, "view.tvOriginalPrice");
            textView4.setVisibility(0);
            TextView textView5 = (TextView) view.findViewById(com.iqonic.store.R.id.tvSaleLabel);
            Intrinsics.checkExpressionValueIsNotNull(textView5, "view.tvSaleLabel");
            textView5.setVisibility(8);
            TextView textView6 = (TextView) view.findViewById(com.iqonic.store.R.id.tvOriginalPrice);
            Intrinsics.checkExpressionValueIsNotNull(textView6, "view.tvOriginalPrice");
            textView6.setText("");
        } else {
            String salePrice = storeProductModel.getSalePrice();
            if (salePrice == null) {
                Intrinsics.throwNpe();
            }
            if (salePrice.length() > 0) {
                TextView textView7 = (TextView) view.findViewById(com.iqonic.store.R.id.tvSaleLabel);
                Intrinsics.checkExpressionValueIsNotNull(textView7, "view.tvSaleLabel");
                textView7.setVisibility(0);
                TextView textView8 = (TextView) view.findViewById(com.iqonic.store.R.id.tvDiscountPrice);
                Intrinsics.checkExpressionValueIsNotNull(textView8, "view.tvDiscountPrice");
                String salePrice2 = storeProductModel.getSalePrice();
                if (salePrice2 == null) {
                    Intrinsics.throwNpe();
                }
                textView8.setText(StringExtensionsKt.currencyFormat$default(salePrice2, (String) null, 1, (Object) null));
                TextView textView9 = (TextView) view.findViewById(com.iqonic.store.R.id.tvOriginalPrice);
                Intrinsics.checkExpressionValueIsNotNull(textView9, "view.tvOriginalPrice");
                ExtensionsKt.applyStrike(textView9);
                TextView textView10 = (TextView) view.findViewById(com.iqonic.store.R.id.tvOriginalPrice);
                Intrinsics.checkExpressionValueIsNotNull(textView10, "view.tvOriginalPrice");
                String regularPrice = storeProductModel.getRegularPrice();
                if (regularPrice == null) {
                    Intrinsics.throwNpe();
                }
                textView10.setText(StringExtensionsKt.currencyFormat$default(regularPrice, (String) null, 1, (Object) null));
                TextView textView11 = (TextView) view.findViewById(com.iqonic.store.R.id.tvOriginalPrice);
                Intrinsics.checkExpressionValueIsNotNull(textView11, "view.tvOriginalPrice");
                textView11.setVisibility(0);
            } else {
                TextView textView12 = (TextView) view.findViewById(com.iqonic.store.R.id.tvSaleLabel);
                Intrinsics.checkExpressionValueIsNotNull(textView12, "view.tvSaleLabel");
                textView12.setVisibility(8);
                TextView textView13 = (TextView) view.findViewById(com.iqonic.store.R.id.tvOriginalPrice);
                Intrinsics.checkExpressionValueIsNotNull(textView13, "view.tvOriginalPrice");
                textView13.setVisibility(0);
                String regularPrice2 = storeProductModel.getRegularPrice();
                if (regularPrice2 == null) {
                    Intrinsics.throwNpe();
                }
                if (regularPrice2.length() == 0) {
                    TextView textView14 = (TextView) view.findViewById(com.iqonic.store.R.id.tvOriginalPrice);
                    Intrinsics.checkExpressionValueIsNotNull(textView14, "view.tvOriginalPrice");
                    textView14.setText("");
                    TextView textView15 = (TextView) view.findViewById(com.iqonic.store.R.id.tvDiscountPrice);
                    Intrinsics.checkExpressionValueIsNotNull(textView15, "view.tvDiscountPrice");
                    String price2 = storeProductModel.getPrice();
                    if (price2 == null) {
                        Intrinsics.throwNpe();
                    }
                    textView15.setText(StringExtensionsKt.currencyFormat$default(price2, (String) null, 1, (Object) null));
                } else {
                    TextView textView16 = (TextView) view.findViewById(com.iqonic.store.R.id.tvOriginalPrice);
                    Intrinsics.checkExpressionValueIsNotNull(textView16, "view.tvOriginalPrice");
                    textView16.setText("");
                    TextView textView17 = (TextView) view.findViewById(com.iqonic.store.R.id.tvDiscountPrice);
                    Intrinsics.checkExpressionValueIsNotNull(textView17, "view.tvDiscountPrice");
                    String regularPrice3 = storeProductModel.getRegularPrice();
                    if (regularPrice3 == null) {
                        Intrinsics.throwNpe();
                    }
                    textView17.setText(StringExtensionsKt.currencyFormat$default(regularPrice3, (String) null, 1, (Object) null));
                }
            }
        }
        TextView textView18 = (TextView) view.findViewById(com.iqonic.store.R.id.tvOriginalPrice);
        Intrinsics.checkExpressionValueIsNotNull(textView18, "view.tvOriginalPrice");
        ExtensionsKt.changeTextSecondaryColor(textView18);
        TextView textView19 = (TextView) view.findViewById(com.iqonic.store.R.id.tvDiscountPrice);
        Intrinsics.checkExpressionValueIsNotNull(textView19, "view.tvDiscountPrice");
        ExtensionsKt.changeTextPrimaryColor(textView19);
        ImageView imageView4 = (ImageView) view.findViewById(com.iqonic.store.R.id.tvAdd);
        Intrinsics.checkExpressionValueIsNotNull(imageView4, "view.tvAdd");
        imageView4.getBackground().setTint(Color.parseColor(AppExtensionsKt.getAccentColor()));
        List<Attributes> attributes = storeProductModel.getAttributes();
        if (attributes == null) {
            Intrinsics.throwNpe();
        }
        if (!attributes.isEmpty()) {
            TextView textView20 = (TextView) view.findViewById(com.iqonic.store.R.id.tvProductWeight);
            Intrinsics.checkExpressionValueIsNotNull(textView20, "view.tvProductWeight");
            List<Attributes> attributes2 = storeProductModel.getAttributes();
            if (attributes2 == null) {
                Intrinsics.throwNpe();
            }
            List<String> options = attributes2.get(0).getOptions();
            if (options == null) {
                Intrinsics.throwNpe();
            }
            textView20.setText(options.get(0));
            TextView textView21 = (TextView) view.findViewById(com.iqonic.store.R.id.tvProductWeight);
            Intrinsics.checkExpressionValueIsNotNull(textView21, "view.tvProductWeight");
            ExtensionsKt.changeAccentColor(textView21);
        }
        if (storeProductModel.getIn_stock()) {
            ImageView imageView5 = (ImageView) view.findViewById(com.iqonic.store.R.id.tvAdd);
            Intrinsics.checkExpressionValueIsNotNull(imageView5, "view.tvAdd");
            ViewExtensionsKt.show(imageView5);
        } else {
            ImageView imageView6 = (ImageView) view.findViewById(com.iqonic.store.R.id.tvAdd);
            Intrinsics.checkExpressionValueIsNotNull(imageView6, "view.tvAdd");
            ViewExtensionsKt.hide(imageView6);
        }
        if (!storeProductModel.getPurchasable()) {
            ImageView imageView7 = (ImageView) view.findViewById(com.iqonic.store.R.id.tvAdd);
            Intrinsics.checkExpressionValueIsNotNull(imageView7, "view.tvAdd");
            ViewExtensionsKt.hide(imageView7);
        } else {
            ImageView imageView8 = (ImageView) view.findViewById(com.iqonic.store.R.id.tvAdd);
            Intrinsics.checkExpressionValueIsNotNull(imageView8, "view.tvAdd");
            ViewExtensionsKt.show(imageView8);
        }
        view.setOnClickListener(new HomeFragment2$setProductItem$$inlined$onClick$1(view, this, storeProductModel));
        ImageView imageView9 = (ImageView) view.findViewById(com.iqonic.store.R.id.tvAdd);
        imageView9.setOnClickListener(new HomeFragment2$setProductItem$$inlined$onClick$2(imageView9, this, storeProductModel));
    }

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Intrinsics.checkParameterIsNotNull(layoutInflater, "inflater");
        return layoutInflater.inflate(R.layout.fragment_home2, viewGroup, false);
    }

    /* access modifiers changed from: private */
    public final void mAddCart(StoreProductModel storeProductModel) {
        if (AppExtensionsKt.isLoggedIn()) {
            RequestModel requestModel = new RequestModel();
            if (Intrinsics.areEqual((Object) storeProductModel.getType(), (Object) "variable")) {
                List<Integer> variations = storeProductModel.getVariations();
                if (variations == null) {
                    Intrinsics.throwNpe();
                }
                requestModel.setPro_id(variations.get(0));
            } else {
                requestModel.setPro_id(Integer.valueOf(storeProductModel.getId()));
            }
            requestModel.setQuantity(1);
            FragmentActivity activity = getActivity();
            if (activity != null) {
                NetworkExtensionKt.addItemToCart((AppBaseActivity) activity, requestModel, new HomeFragment2$mAddCart$1(this));
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type com.iqonic.store.AppBaseActivity");
        }
        FragmentActivity activity2 = getActivity();
        if (activity2 != null) {
            Activity activity3 = (AppBaseActivity) activity2;
            Bundle bundle = null;
            Intent intent = new Intent(activity3, SignInUpActivity.class);
            HomeFragment2$mAddCart$2.INSTANCE.invoke(intent);
            if (Build.VERSION.SDK_INT >= 16) {
                activity3.startActivityForResult(intent, -1, bundle);
            } else {
                activity3.startActivityForResult(intent, -1);
            }
        } else {
            throw new TypeCastException("null cannot be cast to non-null type com.iqonic.store.AppBaseActivity");
        }
    }

    public void onViewCreated(View view, Bundle bundle) {
        Intrinsics.checkParameterIsNotNull(view, "view");
        super.onViewCreated(view, bundle);
        setHasOptionsMenu(true);
        if (AppExtensionsKt.isLoggedIn()) {
            loadApis();
        }
        listAllProducts();
        ((CustomSwipeToRefresh) _$_findCachedViewById(com.iqonic.store.R.id.refreshLayout)).setOnRefreshListener(new HomeFragment2$onViewCreated$1(this));
        CustomSwipeToRefresh customSwipeToRefresh = (CustomSwipeToRefresh) _$_findCachedViewById(com.iqonic.store.R.id.refreshLayout);
        Intrinsics.checkExpressionValueIsNotNull(customSwipeToRefresh, "refreshLayout");
        customSwipeToRefresh.getViewTreeObserver().addOnScrollChangedListener(new HomeFragment2$onViewCreated$2(this));
        this.mLLDynamic = (LinearLayout) view.findViewById(R.id.dashboardMainView);
        ScrollView scrollView = (ScrollView) _$_findCachedViewById(com.iqonic.store.R.id.scrollView);
        Intrinsics.checkExpressionValueIsNotNull(scrollView, "scrollView");
        ExtensionsKt.changeBackgroundColor(scrollView);
        this.mDashboardJson = AppExtensionsKt.getBuilderDashboard();
        mSliderUI();
        mProductUI();
    }

    private final void mProductUI() {
        Context context = getContext();
        if (context == null) {
            Intrinsics.throwNpe();
        }
        Object systemService = context.getSystemService("layout_inflater");
        if (systemService != null) {
            LayoutInflater layoutInflater = (LayoutInflater) systemService;
            this.mViewNewest = layoutInflater.inflate(R.layout.dashboard_productlist, (ViewGroup) null);
            this.mViewFeatured = layoutInflater.inflate(R.layout.dashboard_productlist, (ViewGroup) null);
            this.mViewDealOfTheDay = layoutInflater.inflate(R.layout.dashboard_dealofferview, (ViewGroup) null);
            this.mViewOffer = layoutInflater.inflate(R.layout.dashboard_dealofferview, (ViewGroup) null);
            this.mViewBestSelling = layoutInflater.inflate(R.layout.dashboard_productlist, (ViewGroup) null);
            this.mViewSale = layoutInflater.inflate(R.layout.dashboard_productlist, (ViewGroup) null);
            this.mViewYouMayLike = layoutInflater.inflate(R.layout.dashboard_productlist, (ViewGroup) null);
            this.mViewSuggested = layoutInflater.inflate(R.layout.dashboard_productlist, (ViewGroup) null);
            this.mViewCategory = layoutInflater.inflate(R.layout.dashboard_category, (ViewGroup) null);
            this.mViewSearch = layoutInflater.inflate(R.layout.dashboard_search, (ViewGroup) null);
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type android.view.LayoutInflater");
    }

    private final void mSliderUI() {
        Context context = getContext();
        if (context == null) {
            Intrinsics.throwNpe();
        }
        Object systemService = context.getSystemService("layout_inflater");
        if (systemService != null) {
            this.mSliderView = ((LayoutInflater) systemService).inflate(R.layout.dashboard_sliderview, (ViewGroup) null);
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type android.view.LayoutInflater");
    }

    /* access modifiers changed from: private */
    public final void onAddCategory(View view) {
        if (view == null) {
            Intrinsics.throwNpe();
        }
        View findViewById = view.findViewById(R.id.rvCategory);
        if (findViewById != null) {
            RecyclerView recyclerView = (RecyclerView) findViewById;
            ExtensionsKt.setHorizontalLayout$default(recyclerView, false, 1, (Object) null);
            recyclerView.setHasFixedSize(true);
            recyclerView.setAdapter(this.mProductAdapter);
            AppExtensionsKt.rvItemAnimation(recyclerView);
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type androidx.recyclerview.widget.RecyclerView");
    }

    /* access modifiers changed from: private */
    public final void onAddSearch(View view) {
        if (view == null) {
            Intrinsics.throwNpe();
        }
        View findViewById = view.findViewById(R.id.edtSearch);
        if (findViewById != null) {
            RelativeLayout relativeLayout = (RelativeLayout) view.findViewById(com.iqonic.store.R.id.rlMain);
            Intrinsics.checkExpressionValueIsNotNull(relativeLayout, "mView.rlMain");
            ExtensionsKt.changeTint(relativeLayout, AppExtensionsKt.getPrimaryColor());
            View view2 = (EditText) findViewById;
            view2.setOnClickListener(new HomeFragment2$onAddSearch$$inlined$onClick$1(view2, this));
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type android.widget.EditText");
    }

    static /* synthetic */ void onAddView$default(HomeFragment2 homeFragment2, View view, boolean z, String str, String str2, int i, String str3, List list, int i2, int i3, Object obj) {
        int i4 = i3;
        homeFragment2.onAddView(view, (i4 & 2) != 0 ? false : z, str, str2, i, (i4 & 32) != 0 ? "" : str3, list, (i4 & 128) != 0 ? 5 : i2);
    }

    /* access modifiers changed from: private */
    public final void onAddView(View view, boolean z, String str, String str2, int i, String str3, List<StoreProductModel> list, int i2) {
        List<StoreProductModel> list2 = list;
        int i3 = i2;
        if (view == null) {
            Intrinsics.throwNpe();
        }
        View findViewById = view.findViewById(R.id.rvNewProduct);
        if (findViewById != null) {
            RecyclerView recyclerView = (RecyclerView) findViewById;
            View findViewById2 = view.findViewById(R.id.viewAllItem);
            if (findViewById2 != null) {
                TextView textView = (TextView) findViewById2;
                View findViewById3 = view.findViewById(R.id.tvTitleBar);
                if (findViewById3 != null) {
                    TextView textView2 = (TextView) findViewById3;
                    if (Intrinsics.areEqual((Object) view, (Object) this.mViewDealOfTheDay) || Intrinsics.areEqual((Object) view, (Object) this.mViewOffer)) {
                        LinearLayout linearLayout = (LinearLayout) view.findViewById(com.iqonic.store.R.id.llDeal);
                        Intrinsics.checkExpressionValueIsNotNull(linearLayout, "mView.llDeal");
                        ExtensionsKt.changeTint(linearLayout, AppExtensionsKt.getPrimaryColor());
                        TextView textView3 = (TextView) view.findViewById(com.iqonic.store.R.id.viewAllItem);
                        Intrinsics.checkExpressionValueIsNotNull(textView3, "mView.viewAllItem");
                        ExtensionsKt.changeBackgroundTint(textView3, AppExtensionsKt.getAccentColor());
                        ExtensionsKt.changeTitleColor(textView2);
                    } else {
                        TextView textView4 = (TextView) view.findViewById(com.iqonic.store.R.id.viewAllItem);
                        Intrinsics.checkExpressionValueIsNotNull(textView4, "mView.viewAllItem");
                        ExtensionsKt.changeTextSecondaryColor(textView4);
                        ExtensionsKt.changeAccentColor(textView2);
                    }
                    TextView textView5 = (TextView) view.findViewById(com.iqonic.store.R.id.viewAllItem);
                    Intrinsics.checkExpressionValueIsNotNull(textView5, "mView.viewAllItem");
                    textView5.setText(str2);
                    textView2.setText(str);
                    textView2.setTextSize(20.0f);
                    if (z) {
                        BaseAdapter baseAdapter = new BaseAdapter(R.layout.item_home_product_list, new HomeFragment2$onAddView$productAdapter$1(this));
                        baseAdapter.addItems(list2);
                        baseAdapter.setModelSize(i3);
                        recyclerView.setAdapter(baseAdapter);
                        ExtensionsKt.setVerticalLayout$default(recyclerView, false, 1, (Object) null);
                        baseAdapter.setModelSize(4);
                    } else {
                        BaseAdapter baseAdapter2 = new BaseAdapter(R.layout.item_home_dashboard2, new HomeFragment2$onAddView$productAdapter$2(this));
                        baseAdapter2.addItems(list2);
                        baseAdapter2.setModelSize(i3);
                        ExtensionsKt.setHorizontalLayout$default(recyclerView, false, 1, (Object) null);
                        recyclerView.setAdapter(baseAdapter2);
                    }
                    View view2 = textView;
                    view2.setOnClickListener(new HomeFragment2$onAddView$$inlined$onClick$1(view2, this, str, i, str3));
                    if (view.getParent() != null) {
                        ViewParent parent = view.getParent();
                        if (parent != null) {
                            ((ViewGroup) parent).removeView(view);
                            return;
                        }
                        throw new TypeCastException("null cannot be cast to non-null type android.view.ViewGroup");
                    }
                    return;
                }
                throw new TypeCastException("null cannot be cast to non-null type android.widget.TextView");
            }
            throw new TypeCastException("null cannot be cast to non-null type android.widget.TextView");
        }
        throw new TypeCastException("null cannot be cast to non-null type androidx.recyclerview.widget.RecyclerView");
    }

    /* access modifiers changed from: private */
    public final void addSlider(List<DashboardBanner> list) {
        View view = this.mSliderView;
        if (view == null) {
            Intrinsics.throwNpe();
        }
        View findViewById = view.findViewById(R.id.slideViewPager);
        if (findViewById != null) {
            ViewPager viewPager = (ViewPager) findViewById;
            View view2 = this.mSliderView;
            if (view2 == null) {
                Intrinsics.throwNpe();
            }
            View findViewById2 = view2.findViewById(R.id.dots);
            if (findViewById2 != null) {
                DotsIndicator dotsIndicator = (DotsIndicator) findViewById2;
                viewPager.setAdapter(new HomeSliderAdapter(list));
                dotsIndicator.attachViewPager(viewPager);
                dotsIndicator.setDotDrawable(R.drawable.bg_circle_primary, R.drawable.black_dot);
                LinearLayout linearLayout = this.mLLDynamic;
                if (linearLayout == null) {
                    Intrinsics.throwNpe();
                }
                View view3 = this.mSliderView;
                if (view3 == null) {
                    Intrinsics.throwNpe();
                }
                linearLayout.addView(view3);
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type com.iqonic.store.utils.dotsindicator.DotsIndicator");
        }
        throw new TypeCastException("null cannot be cast to non-null type androidx.viewpager.widget.ViewPager");
    }

    private final void showLoader() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            ((AppBaseActivity) activity).showProgress(true);
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.iqonic.store.AppBaseActivity");
    }

    private final void loadApis() {
        if (ExtensionsKt.isNetworkAvailable()) {
            FragmentActivity activity = getActivity();
            if (activity == null) {
                Intrinsics.throwNpe();
            }
            Intrinsics.checkExpressionValueIsNotNull(activity, "activity!!");
            AppExtensionsKt.fetchAndStoreCartData(activity);
        }
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater menuInflater) {
        Intrinsics.checkParameterIsNotNull(menu, "menu");
        Intrinsics.checkParameterIsNotNull(menuInflater, "inflater");
        super.onCreateOptionsMenu(menu, menuInflater);
        menu.clear();
        menuInflater.inflate(R.menu.menu_dashboard, menu);
        MenuItem findItem = menu.findItem(R.id.action_cart);
        Intrinsics.checkExpressionValueIsNotNull(findItem, "menu.findItem(R.id.action_cart)");
        MenuItem findItem2 = menu.findItem(R.id.action_search);
        Intrinsics.checkExpressionValueIsNotNull(findItem2, "menu.findItem(R.id.action_search)");
        findItem.setVisible(true);
        findItem2.setVisible(false);
        View actionView = findItem.getActionView();
        this.mMenuCart = actionView;
        if (actionView != null) {
            actionView.setOnClickListener(new HomeFragment2$onCreateOptionsMenu$$inlined$onClick$1(actionView, this));
        }
        setCartCount();
    }

    public final void setCartCount() {
        TextView textView;
        TextView textView2;
        TextView textView3;
        TextView textView4;
        TextView textView5;
        ImageView imageView;
        String cartCount = AppExtensionsKt.getCartCount();
        View view = this.mMenuCart;
        if (!(view == null || (imageView = (ImageView) view.findViewById(com.iqonic.store.R.id.ivCart)) == null)) {
            ExtensionsKt.changeBackgroundImageTint(imageView, AppExtensionsKt.getTextTitleColor());
        }
        View view2 = this.mMenuCart;
        if (!(view2 == null || (textView5 = (TextView) view2.findViewById(com.iqonic.store.R.id.tvNotificationCount)) == null)) {
            ExtensionsKt.changeTint(textView5, AppExtensionsKt.getTextTitleColor());
        }
        View view3 = this.mMenuCart;
        if (!(view3 == null || (textView4 = (TextView) view3.findViewById(com.iqonic.store.R.id.tvNotificationCount)) == null)) {
            textView4.setText(cartCount);
        }
        View view4 = this.mMenuCart;
        if (!(view4 == null || (textView3 = (TextView) view4.findViewById(com.iqonic.store.R.id.tvNotificationCount)) == null)) {
            ExtensionsKt.changeAccentColor(textView3);
        }
        if (StringExtensionsKt.checkIsEmpty(cartCount) || Intrinsics.areEqual((Object) cartCount, (Object) AppEventsConstants.EVENT_PARAM_VALUE_NO)) {
            View view5 = this.mMenuCart;
            if (view5 != null && (textView = (TextView) view5.findViewById(com.iqonic.store.R.id.tvNotificationCount)) != null) {
                ViewExtensionsKt.hide(textView);
                return;
            }
            return;
        }
        View view6 = this.mMenuCart;
        if (view6 != null && (textView2 = (TextView) view6.findViewById(com.iqonic.store.R.id.tvNotificationCount)) != null) {
            ViewExtensionsKt.show(textView2);
        }
    }

    /* access modifiers changed from: private */
    public final void listAllProducts() {
        if (ExtensionsKt.isNetworkAvailable()) {
            showLoader();
            NetworkExtensionKt.getRestApiImpl$default((String) null, 1, (Object) null).getDashboardData(new HomeFragment2$listAllProducts$1(this), new HomeFragment2$listAllProducts$2(this));
            this.data.put("per_page", 20);
            NetworkExtensionKt.getRestApiImpl$default((String) null, 1, (Object) null).listAllCategory(this.data, new HomeFragment2$listAllProducts$3(this), new HomeFragment2$listAllProducts$4(this));
        }
    }

    /* access modifiers changed from: private */
    public final void setNewLocale(String str) {
        ShopHopApp.Companion.changeLanguage(str);
        String language = ShopHopApp.Companion.getLanguage();
        this.lan = language;
        if (language == null) {
            Intrinsics.throwUninitializedPropertyAccessException("lan");
        }
        if (!Intrinsics.areEqual((Object) language, (Object) str)) {
            FragmentActivity activity = getActivity();
            if (activity != null) {
                ((AppBaseActivity) activity).recreate();
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type com.iqonic.store.AppBaseActivity");
        }
    }
}
