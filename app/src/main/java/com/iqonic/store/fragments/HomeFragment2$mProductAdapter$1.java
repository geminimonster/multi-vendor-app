package com.iqonic.store.fragments;

import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.iqonic.store.models.Category;
import com.iqonic.store.utils.extensions.NetworkExtensionKt;
import com.iqonic.store.utils.extensions.StringExtensionsKt;
import com.store.proshop.R;
import java.util.Random;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function3;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0007H\n¢\u0006\u0002\b\b"}, d2 = {"<anonymous>", "", "view", "Landroid/view/View;", "model", "Lcom/iqonic/store/models/Category;", "<anonymous parameter 2>", "", "invoke"}, k = 3, mv = {1, 1, 16})
/* compiled from: HomeFragment2.kt */
final class HomeFragment2$mProductAdapter$1 extends Lambda implements Function3<View, Category, Integer, Unit> {
    final /* synthetic */ HomeFragment2 this$0;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    HomeFragment2$mProductAdapter$1(HomeFragment2 homeFragment2) {
        super(3);
        this.this$0 = homeFragment2;
    }

    public /* bridge */ /* synthetic */ Object invoke(Object obj, Object obj2, Object obj3) {
        invoke((View) obj, (Category) obj2, ((Number) obj3).intValue());
        return Unit.INSTANCE;
    }

    public final void invoke(View view, Category category, int i) {
        Intrinsics.checkParameterIsNotNull(view, "view");
        Intrinsics.checkParameterIsNotNull(category, "model");
        int[] intArray = this.this$0.getResources().getIntArray(R.array.categoryColor);
        Intrinsics.checkExpressionValueIsNotNull(intArray, "resources.getIntArray(R.array.categoryColor)");
        int i2 = intArray[new Random().nextInt(intArray.length)];
        ImageView imageView = (ImageView) view.findViewById(com.iqonic.store.R.id.ivBackground);
        Intrinsics.checkExpressionValueIsNotNull(imageView, "view.ivBackground");
        NetworkExtensionKt.loadImageFromUrl$default(imageView, "https://bestwomenapparels.com/wp-content/uploads/2019/05/bestwomenapparels-fashion.jpg", 0, 0, 6, (Object) null);
        View findViewById = view.findViewById(com.iqonic.store.R.id.mViewBackground);
        Intrinsics.checkExpressionValueIsNotNull(findViewById, "view.mViewBackground");
        Drawable background = findViewById.getBackground();
        Intrinsics.checkExpressionValueIsNotNull(background, "view.mViewBackground.background");
        background.setColorFilter(new PorterDuffColorFilter(i2, PorterDuff.Mode.SRC_IN));
        TextView textView = (TextView) view.findViewById(com.iqonic.store.R.id.tvCategories);
        Intrinsics.checkExpressionValueIsNotNull(textView, "view.tvCategories");
        textView.setText(StringExtensionsKt.getHtmlString(category.getName()));
        view.setOnClickListener(new HomeFragment2$mProductAdapter$1$$special$$inlined$onClick$1(view, this, category));
    }
}
