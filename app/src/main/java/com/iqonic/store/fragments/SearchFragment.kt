package com.iqonic.store.fragments

import android.graphics.PorterDuff
import android.os.Bundle
import android.view.*
import android.widget.AutoCompleteTextView
import android.widget.ImageView
import androidx.appcompat.widget.SearchView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.iqonic.store.AppBaseActivity
import com.iqonic.store.R
import com.iqonic.store.activity.ProductDetailActivityNew
import com.iqonic.store.activity.SignInUpActivity
import com.iqonic.store.adapter.BaseAdapter
import com.iqonic.store.models.RequestModel
import com.iqonic.store.models.StoreProductAttribute
import com.iqonic.store.models.StoreProductModel
import com.iqonic.store.models.Term
import com.iqonic.store.utils.Constants
import java.util.*
import kotlin.collections.ArrayList


class SearchFragment : BaseFragment() {
    private var productList = ArrayList<StoreProductModel>()
    private var searchList = ArrayList<StoreProductModel>()
    private var mAttributeName = ArrayList<StoreProductAttribute>()
    private var mTerms = ArrayList<Term>()
    private var mSelectedSlug: ArrayList<String> = ArrayList()
    private var mSelectedTermId: ArrayList<String> = ArrayList()
    private var searchQuery = ""
    private var mSearchQuery = ""
    var image: String = ""
    private var mIsFilterDataLoaded = false
    private var mPage = 1
    private val mProductAdapter =
        BaseAdapter<StoreProductModel>(R.layout.item_viewproductgrid, onBind = { view, model, _ ->

            if (model.images!!.isNotEmpty()) {
                view.ivProduct.loadImageFromUrl(model.images!![0].src!!)
                image = model.images!![0].src!!

            }

            view.tvProductName.text = model.name
            if (model.onSale) {
                view.tvDiscountPrice.text = model.salePrice.currencyFormat()
                view.tvSale.show()
                view.tvOriginalPrice.applyStrike()
                view.tvOriginalPrice.text = model.regularPrice.currencyFormat()

            } else {
                view.tvDiscountPrice.text = model.regularPrice.currencyFormat()
                view.tvOriginalPrice.text = ""
                view.tvSale.hide()

            }
            if (model.attributes!!.isNotEmpty()) {
                view.tvProductWeight.text = model.attributes.get(0).options!![0]
            }

            if (model.regularPrice!!.isEmpty()) {
                view.tvAdd.hide()
            }
            if (model.stockStatus == "instock") {
                view.tvAdd.show()

            } else {
                view.tvAdd.hide()

            }
            view.onClick {
                (activity as AppBaseActivity).launchActivity<ProductDetailActivityNew> {
                    putExtra(Constants.KeyIntent.PRODUCT_ID, model.id)
                }
            }
            view.tvAdd.onClick {
                addCart(model)
            }
        })
    var isLoading = false
    private var showPagination: Boolean? = true

    private fun addCart(model: StoreProductModel) {
        if (isLoggedIn()) {
            val requestModel = RequestModel()
            requestModel.pro_id = model.id
            requestModel.quantity = 1
            (activity as AppBaseActivity).addItemToCart(requestModel, onApiSuccess = {
            })
        } else (activity as AppBaseActivity).launchActivity<SignInUpActivity> { }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.activity_search, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        toolbar.navigationIcon!!.setColorFilter(
            resources.getColor(R.color.colorBackArrow),
            PorterDuff.Mode.SRC_ATOP
        )

        setHasOptionsMenu(true)
        (activity as AppBaseActivity).setToolbar(toolbar)


        aSearch_rvSearch.apply {
            adapter = mProductAdapter
            layoutManager = GridLayoutManager(activity, 2)
            if (showPagination!!) {
                addOnScrollListener(object : RecyclerView.OnScrollListener() {
                    override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                        super.onScrollStateChanged(recyclerView, newState)
                        val countItem = recyclerView.layoutManager.itemCount

                        var lastVisiblePosition = 0
                        if (recyclerView.layoutManager is LinearLayoutManager) {
                            lastVisiblePosition =
                                (recyclerView.layoutManager as LinearLayoutManager).findLastCompletelyVisibleItemPosition()
                        } else if (recyclerView.layoutManager is GridLayoutManager) {
                            lastVisiblePosition =
                                (recyclerView.layoutManager as GridLayoutManager).findLastCompletelyVisibleItemPosition()
                        }

                        if (lastVisiblePosition != 0 && !isLoading && countItem.minus(1) == lastVisiblePosition) {
                            isLoading = true
                            mPage = mPage.plus(1)
                            val data = HashMap<String, String>()
                            data.put("search", mSearchQuery)
                            data.put("page", mPage.toString())
                            loadProducts(data)
                        }
                    }
                })
            }
        }
        dataNotFound()
        getProductAttribute()


    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        menu.clear()
        inflater.inflate(R.menu.menu_search, menu)
        super.onCreateOptionsMenu(menu, inflater)
        val search = menu.findItem(R.id.action_search)
        val searchView = search.actionView as SearchView
        val searchIcon = searchView.findViewById<ImageView>(androidx.appcompat.R.id.search_button)
        val searchAutoComplete =
            searchView.findViewById<AutoCompleteTextView>(androidx.appcompat.R.id.search_src_text)
        searchAutoComplete.setHintTextColor(resources.getColor(R.color.white))
        searchAutoComplete.setTextColor(resources.getColor(android.R.color.white))
        searchIcon.setImageDrawable(
            ContextCompat.getDrawable(activity!!, R.drawable.ic_search)
        )
        //searchView.onActionViewExpanded()
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                mPage = 1
                mSearchQuery = query!!
                mProductAdapter.clearItems()
                productList.clear()
                searchList.clear()
                val data = HashMap<String, String>()
                data.put("search", query)
                data.put("page", mPage.toString())
                loadProducts(data)
                return true
            }

            override fun onQueryTextChange(newText: String): Boolean {

                return true
            }

        })

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_filter -> {
                if (mIsFilterDataLoaded) openFilterBottomSheet()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun filter(newText: String) {
        productList.clear()
        if (newText.toLowerCase(Locale.ENGLISH).isEmpty()) {
            productList.addAll(searchList)
        } else {
            for (data in searchList) if (data.name.toLowerCase(Locale.ENGLISH)!!
                    .contains(newText.toLowerCase(Locale.ENGLISH))
            ) productList.add(data)
        }
        if (productList.size > 0) dataFound() else dataNotFound()
        mProductAdapter.clearItems()
        mProductAdapter.addItems(productList)

    }

    private fun dataNotFound() {
        rlNoData.show(); aSearch_rvSearch.hide()
    }

    private fun dataFound() {
        rlNoData.hide(); aSearch_rvSearch.show()
    }

    private fun loadProducts(value: HashMap<String, String>) {
        if (isNetworkAvailable()) {
            (activity!! as AppBaseActivity).showProgress(true)
            getRestApiImpl().search(value, onApiSuccess = {
                if (activity == null) return@search
                (activity!! as AppBaseActivity).showProgress(false)

                if (it.isNullOrEmpty()) {
                    rlNoData.show()
                    aSearch_rvSearch.hide()
                } else {
                    productList.addAll(it)
                    searchList.addAll(it)
                    if (activity != null) {
                        rlNoData.hide()
                        aSearch_rvSearch.hide()
                        mProductAdapter.clearItems()
                        if (productList.isNotEmpty()) {
                            mProductAdapter.addMoreItems(productList)
                            filter(searchQuery)
                        }
                    }
                }


            }, onApiError = {
                (activity!! as AppBaseActivity).showProgress(false)
                snackBar(it)
            })
        }
    }

    private fun getProductAttribute() {
        if (isNetworkAvailable()) {
            (activity!! as AppBaseActivity).showProgress(true)
            getRestApiImpl().listAllProductAttribute(onApiSuccess = {
                if (activity == null) return@listAllProductAttribute
                (activity!! as AppBaseActivity).showProgress(false)
                mIsFilterDataLoaded = true
                it.forEachIndexed { _, storeProductAttribute ->
                    mAttributeName.add(storeProductAttribute)

                    val teams = Term()
                    teams.name = storeProductAttribute.name
                    teams.isParent = true

                    mTerms.add(teams)
                    storeProductAttribute.terms.forEachIndexed { index, term ->
                        mTerms.add(term)
                    }
                }
            }, onApiError = {
                if (activity == null) return@listAllProductAttribute
                (activity!! as AppBaseActivity).showProgress(false)
                snackBar(it)
            })
        }
    }

    private fun openFilterBottomSheet() {
        if (activity != null) {
            val filterDialog =
                BottomSheetDialog(activity!!); filterDialog.setContentView(R.layout.layout_filter)

            val brandAdapter =
                BaseAdapter<Term>(R.layout.item_filter_brand, onBind = { view, model, position ->

                    if (model.isParent) {
                        view.tvAttributesName.visibility = View.VISIBLE
                        view.termsView.visibility = View.GONE
                        view.tvAttributesName.text = model.name
                    } else {
                        view.termsView.visibility = View.VISIBLE
                        view.tvAttributesName.visibility = View.GONE
                        view.tvBrandName.text = model.name

                        if (model.isSelected) {
                            view.tvBrandName.setTextColor(activity!!.color(R.color.colorPrimary))
                            view.ivSelect.setImageResource(R.drawable.ic_check)
                            view.ivSelect.setColorFilter(activity!!.color(R.color.colorPrimary))
                            view.ivSelect.setStrokedBackground(
                                activity!!.color(R.color.colorAccent),
                                activity!!.color(R.color.colorAccent),
                                0.4f
                            )
                        } else {
                            view.tvBrandName.setTextColor(activity!!.color(R.color.textColorSecondary))
                            view.ivSelect.setImageResource(0)
                            view.ivSelect.setStrokedBackground(activity!!.color(R.color.checkbox_color))
                        }
                    }
                })
            brandAdapter.onItemClick = { _, _, model ->
                model.isSelected = !(model.isSelected)
                brandAdapter.notifyDataSetChanged()
            }

            filterDialog.rcvBrands.apply {
                setVerticalLayout(); adapter = brandAdapter
            }
            brandAdapter.clearItems()
            brandAdapter.addItems(mTerms)

            filterDialog.tvApply.onClick {
                mProductAdapter.clearItems()
                productList.clear()
                searchList.clear()
                mSelectedSlug.clear()
                mSelectedTermId.clear()
                mTerms.forEachIndexed { index, storeProductAttribute ->
                    if (storeProductAttribute.isSelected) {
                        mSelectedSlug.add(storeProductAttribute.slug)
                        mSelectedTermId.add(storeProductAttribute.term_id.toString())
                    }
                }
                var mSelectedTeramName = ""
                var mSelectedTeramId = ""

                for (s in mSelectedSlug) {
                    if (mSelectedTeramName.isNotEmpty()) {
                        mSelectedTeramName += ",$s"
                    } else {
                        mSelectedTeramName = s
                    }
                }
                for (s in mSelectedTermId) {
                    if (mSelectedTeramId.isNotEmpty()) {
                        mSelectedTeramId += ",$s"
                    } else {
                        mSelectedTeramId = s
                    }
                }

                val data = HashMap<String, String>()
                data.put("attribute", mSelectedTeramName)
                data.put("attribute_term", mSelectedTeramId)
                data.put("page", mPage.toString())
                loadProducts(data)
                filterDialog.dismiss()
            }
            filterDialog.tvReset.onClick {
                mTerms.forEach { it.isSelected = false }
                filterDialog.dismiss()
            }
            filterDialog.tvSelectAll.onClick {
                mTerms.forEach { it.isSelected = true }
                brandAdapter.notifyDataSetChanged()
            }

            filterDialog.ivClose.onClick { filterDialog.dismiss() }

            filterDialog.show()
        }
    }


    private fun setProducts(it: ArrayList<StoreProductModel>) {
        mProductAdapter.addItems(it)
        if (mProductAdapter.getModel().isEmpty()) {
            rlNoData.show(); aSearch_rvSearch.hide()
        } else {
            rlNoData.hide(); aSearch_rvSearch.show()
        }
    }

}
