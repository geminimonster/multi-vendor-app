package com.iqonic.store.fragments;

import android.widget.RelativeLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;
import com.iqonic.store.AppBaseActivity;
import com.iqonic.store.R;
import com.iqonic.store.utils.Constants;
import com.iqonic.store.utils.extensions.AppExtensionsKt;
import com.iqonic.store.utils.extensions.ExtensionsKt;
import com.iqonic.store.utils.extensions.ViewExtensionsKt;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n¢\u0006\u0002\b\u0004"}, d2 = {"<anonymous>", "", "it", "", "invoke"}, k = 3, mv = {1, 1, 16})
/* compiled from: WishListFragment.kt */
final class WishListFragment$wishListItemChange$2 extends Lambda implements Function1<String, Unit> {
    final /* synthetic */ WishListFragment this$0;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    WishListFragment$wishListItemChange$2(WishListFragment wishListFragment) {
        super(1);
        this.this$0 = wishListFragment;
    }

    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        invoke((String) obj);
        return Unit.INSTANCE;
    }

    public final void invoke(String str) {
        Intrinsics.checkParameterIsNotNull(str, "it");
        if (this.this$0.getActivity() != null) {
            FragmentActivity activity = this.this$0.getActivity();
            if (activity != null) {
                ((AppBaseActivity) activity).showProgress(false);
                if (Intrinsics.areEqual((Object) str, (Object) "no product available")) {
                    AppExtensionsKt.getSharedPrefInstance().setValue(Constants.SharedPref.KEY_WISHLIST_COUNT, 0);
                    FragmentActivity activity2 = this.this$0.getActivity();
                    if (activity2 != null) {
                        AppExtensionsKt.sendWishlistBroadcast((AppBaseActivity) activity2);
                        RelativeLayout relativeLayout = (RelativeLayout) this.this$0._$_findCachedViewById(R.id.rlNoData);
                        Intrinsics.checkExpressionValueIsNotNull(relativeLayout, "rlNoData");
                        ViewExtensionsKt.show(relativeLayout);
                        RecyclerView recyclerView = (RecyclerView) this.this$0._$_findCachedViewById(R.id.rvWishList);
                        Intrinsics.checkExpressionValueIsNotNull(recyclerView, "rvWishList");
                        ViewExtensionsKt.hide(recyclerView);
                        return;
                    }
                    throw new TypeCastException("null cannot be cast to non-null type com.iqonic.store.AppBaseActivity");
                }
                FragmentActivity activity3 = this.this$0.getActivity();
                if (activity3 != null) {
                    ExtensionsKt.snackBarError((AppBaseActivity) activity3, str);
                    return;
                }
                throw new TypeCastException("null cannot be cast to non-null type com.iqonic.store.AppBaseActivity");
            }
            throw new TypeCastException("null cannot be cast to non-null type com.iqonic.store.AppBaseActivity");
        }
    }
}
