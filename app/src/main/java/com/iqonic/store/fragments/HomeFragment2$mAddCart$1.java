package com.iqonic.store.fragments;

import androidx.fragment.app.FragmentActivity;
import com.iqonic.store.models.AddCartResponse;
import com.iqonic.store.utils.extensions.AppExtensionsKt;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n¢\u0006\u0002\b\u0004"}, d2 = {"<anonymous>", "", "it", "Lcom/iqonic/store/models/AddCartResponse;", "invoke"}, k = 3, mv = {1, 1, 16})
/* compiled from: HomeFragment2.kt */
final class HomeFragment2$mAddCart$1 extends Lambda implements Function1<AddCartResponse, Unit> {
    final /* synthetic */ HomeFragment2 this$0;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    HomeFragment2$mAddCart$1(HomeFragment2 homeFragment2) {
        super(1);
        this.this$0 = homeFragment2;
    }

    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        invoke((AddCartResponse) obj);
        return Unit.INSTANCE;
    }

    public final void invoke(AddCartResponse addCartResponse) {
        Intrinsics.checkParameterIsNotNull(addCartResponse, "it");
        this.this$0.isAddedToCart = true;
        FragmentActivity activity = this.this$0.getActivity();
        if (activity == null) {
            Intrinsics.throwNpe();
        }
        Intrinsics.checkExpressionValueIsNotNull(activity, "activity!!");
        AppExtensionsKt.fetchAndStoreCartData(activity);
    }
}
