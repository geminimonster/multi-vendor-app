package com.iqonic.store.fragments;

import android.util.Log;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.fragment.app.FragmentActivity;
import com.facebook.appevents.AppEventsConstants;
import com.iqonic.store.R;
import com.iqonic.store.models.Method;
import com.iqonic.store.utils.extensions.ExtensionsKt;
import com.iqonic.store.utils.extensions.StringExtensionsKt;
import com.iqonic.store.utils.extensions.ViewExtensionsKt;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n¢\u0006\u0002\b\u0002"}, d2 = {"<anonymous>", "", "run"}, k = 3, mv = {1, 1, 16})
/* compiled from: MyCartFragment.kt */
final class MyCartFragment$onShippingMethodChanged$1 implements Runnable {
    final /* synthetic */ Method $model;
    final /* synthetic */ int $pos;
    final /* synthetic */ MyCartFragment this$0;

    MyCartFragment$onShippingMethodChanged$1(MyCartFragment myCartFragment, int i, Method method) {
        this.this$0 = myCartFragment;
        this.$pos = i;
        this.$model = method;
    }

    public final void run() {
        this.this$0.selectedMethod = this.$pos;
        this.this$0.mShippingMethodAdapter.notifyDataSetChanged();
        if (Intrinsics.areEqual((Object) this.$model.getId(), (Object) "free_shipping")) {
            TextView textView = (TextView) this.this$0._$_findCachedViewById(R.id.tvShipping);
            Intrinsics.checkExpressionValueIsNotNull(textView, "tvShipping");
            textView.setText("Free");
            this.this$0.mShippingCost = AppEventsConstants.EVENT_PARAM_VALUE_NO;
            TextView textView2 = (TextView) this.this$0._$_findCachedViewById(R.id.tvShipping);
            FragmentActivity activity = this.this$0.getActivity();
            if (activity == null) {
                Intrinsics.throwNpe();
            }
            Intrinsics.checkExpressionValueIsNotNull(activity, "activity!!");
            textView2.setTextColor(ExtensionsKt.color(activity, com.store.proshop.R.color.green));
            LinearLayout linearLayout = (LinearLayout) this.this$0._$_findCachedViewById(R.id.llShippingAmount);
            Intrinsics.checkExpressionValueIsNotNull(linearLayout, "llShippingAmount");
            ViewExtensionsKt.show(linearLayout);
            Log.d("mTotalCount", String.valueOf(this.this$0.mTotalCount));
        } else {
            if (this.$model.getCost().length() == 0) {
                this.$model.setCost(AppEventsConstants.EVENT_PARAM_VALUE_NO);
            }
            this.this$0.mShippingCost = this.$model.getCost();
            LinearLayout linearLayout2 = (LinearLayout) this.this$0._$_findCachedViewById(R.id.llShippingAmount);
            Intrinsics.checkExpressionValueIsNotNull(linearLayout2, "llShippingAmount");
            ViewExtensionsKt.show(linearLayout2);
            TextView textView3 = (TextView) this.this$0._$_findCachedViewById(R.id.tvShipping);
            Intrinsics.checkExpressionValueIsNotNull(textView3, "tvShipping");
            ExtensionsKt.changeTextSecondaryColor(textView3);
            TextView textView4 = (TextView) this.this$0._$_findCachedViewById(R.id.tvShipping);
            Intrinsics.checkExpressionValueIsNotNull(textView4, "tvShipping");
            textView4.setText(StringExtensionsKt.currencyFormat$default(this.this$0.mShippingCost, (String) null, 1, (Object) null));
        }
        TextView textView5 = (TextView) this.this$0._$_findCachedViewById(R.id.tvTotalCartAmount);
        Intrinsics.checkExpressionValueIsNotNull(textView5, "tvTotalCartAmount");
        textView5.setText(StringExtensionsKt.currencyFormat$default(String.valueOf(((int) this.this$0.mTotalCount) + Integer.parseInt(this.this$0.mShippingCost)), (String) null, 1, (Object) null));
    }
}
