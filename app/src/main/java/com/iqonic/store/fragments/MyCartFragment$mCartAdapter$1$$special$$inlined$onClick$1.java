package com.iqonic.store.fragments;

import android.view.View;
import com.google.android.material.card.MaterialCardView;
import com.iqonic.store.R;
import com.iqonic.store.models.CartResponse;
import com.iqonic.store.utils.swipereveallayout.SwipeRevealLayout;
import kotlin.Metadata;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\b\u0003\n\u0002\b\u0004\u0010\u0000\u001a\u00020\u0001\"\b\b\u0000\u0010\u0002*\u00020\u00032\u000e\u0010\u0004\u001a\n \u0005*\u0004\u0018\u00010\u00030\u0003H\n¢\u0006\u0002\b\u0006¨\u0006\u0007"}, d2 = {"<anonymous>", "", "T", "Landroid/view/View;", "it", "kotlin.jvm.PlatformType", "onClick", "com/iqonic/store/utils/extensions/ExtensionsKt$onClick$1"}, k = 3, mv = {1, 1, 16})
/* compiled from: Extensions.kt */
public final class MyCartFragment$mCartAdapter$1$$special$$inlined$onClick$1 implements View.OnClickListener {
    final /* synthetic */ CartResponse $model$inlined;
    final /* synthetic */ View $this_onClick;
    final /* synthetic */ View $view$inlined;
    final /* synthetic */ MyCartFragment$mCartAdapter$1 this$0;

    public MyCartFragment$mCartAdapter$1$$special$$inlined$onClick$1(View view, MyCartFragment$mCartAdapter$1 myCartFragment$mCartAdapter$1, View view2, CartResponse cartResponse) {
        this.$this_onClick = view;
        this.this$0 = myCartFragment$mCartAdapter$1;
        this.$view$inlined = view2;
        this.$model$inlined = cartResponse;
    }

    public final void onClick(View view) {
        MaterialCardView materialCardView = (MaterialCardView) this.$this_onClick;
        ((SwipeRevealLayout) this.$view$inlined.findViewById(R.id.swipeLayout)).close(true);
        this.this$0.this$0.removeCartItem(this.$model$inlined);
    }
}
