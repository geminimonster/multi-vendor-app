package com.iqonic.store.fragments;

import android.content.Intent;
import com.iqonic.store.models.Method;
import com.iqonic.store.utils.Constants;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;
import kotlin.jvm.internal.Ref;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\u0010\u0000\u001a\u00020\u0001*\u00020\u0002H\n¢\u0006\u0002\b\u0003¨\u0006\u0004"}, d2 = {"<anonymous>", "", "Landroid/content/Intent;", "invoke", "com/iqonic/store/fragments/MyCartFragment$onViewCreated$1$1"}, k = 3, mv = {1, 1, 16})
/* compiled from: MyCartFragment.kt */
final class MyCartFragment$onViewCreated$$inlined$onClick$1$lambda$1 extends Lambda implements Function1<Intent, Unit> {
    final /* synthetic */ double $mAmount;
    final /* synthetic */ Ref.ObjectRef $selectedShippingMethod;
    final /* synthetic */ MyCartFragment$onViewCreated$$inlined$onClick$1 this$0;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    MyCartFragment$onViewCreated$$inlined$onClick$1$lambda$1(double d, Ref.ObjectRef objectRef, MyCartFragment$onViewCreated$$inlined$onClick$1 myCartFragment$onViewCreated$$inlined$onClick$1) {
        super(1);
        this.$mAmount = d;
        this.$selectedShippingMethod = objectRef;
        this.this$0 = myCartFragment$onViewCreated$$inlined$onClick$1;
    }

    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        invoke((Intent) obj);
        return Unit.INSTANCE;
    }

    public final void invoke(Intent intent) {
        Intrinsics.checkParameterIsNotNull(intent, "$receiver");
        intent.putExtra(Constants.KeyIntent.COUPON_CODE, this.this$0.this$0.mCoupons);
        intent.putExtra("price", String.valueOf(this.$mAmount));
        intent.putExtra(Constants.KeyIntent.PRODUCTDATA, this.this$0.this$0.mOrderItems);
        intent.putExtra(Constants.KeyIntent.SHIPPINGDATA, (Method) this.$selectedShippingMethod.element);
        intent.putExtra(Constants.KeyIntent.SUBTOTAL, String.valueOf(this.this$0.this$0.mSubTotal));
        intent.putExtra(Constants.KeyIntent.DISCOUNT, this.this$0.this$0.mTotalDiscount);
        intent.putExtra(Constants.KeyIntent.SHIPPING, this.this$0.this$0.mShippingCost);
    }
}
