package com.iqonic.store.fragments;

import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;
import com.iqonic.store.AppBaseActivity;
import com.iqonic.store.R;
import com.iqonic.store.models.StoreProductModel;
import com.iqonic.store.utils.extensions.ViewExtensionsKt;
import java.util.ArrayList;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0016\u0010\u0002\u001a\u0012\u0012\u0004\u0012\u00020\u00040\u0003j\b\u0012\u0004\u0012\u00020\u0004`\u0005H\n¢\u0006\u0002\b\u0006"}, d2 = {"<anonymous>", "", "it", "Ljava/util/ArrayList;", "Lcom/iqonic/store/models/StoreProductModel;", "Lkotlin/collections/ArrayList;", "invoke"}, k = 3, mv = {1, 1, 16})
/* compiled from: ViewAllProductFragment.kt */
final class ViewAllProductFragment$loadCategory$1 extends Lambda implements Function1<ArrayList<StoreProductModel>, Unit> {
    final /* synthetic */ ViewAllProductFragment this$0;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    ViewAllProductFragment$loadCategory$1(ViewAllProductFragment viewAllProductFragment) {
        super(1);
        this.this$0 = viewAllProductFragment;
    }

    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        invoke((ArrayList<StoreProductModel>) (ArrayList) obj);
        return Unit.INSTANCE;
    }

    public final void invoke(ArrayList<StoreProductModel> arrayList) {
        Intrinsics.checkParameterIsNotNull(arrayList, "it");
        if (this.this$0.getActivity() != null) {
            FragmentActivity activity = this.this$0.getActivity();
            if (activity == null) {
                Intrinsics.throwNpe();
            }
            if (activity != null) {
                ((AppBaseActivity) activity).showProgress(false);
                if (this.this$0.countLoadMore == 1) {
                    this.this$0.mProductAdapter.clearItems();
                }
                if (arrayList.isEmpty()) {
                    this.this$0.isLastPage = true;
                }
                this.this$0.mIsLoading = false;
                this.this$0.mProductAdapter.addMoreItems(arrayList);
                if (this.this$0.mProductAdapter.getItemCount() == 0) {
                    RecyclerView recyclerView = (RecyclerView) this.this$0._$_findCachedViewById(R.id.rvNewestProduct);
                    Intrinsics.checkExpressionValueIsNotNull(recyclerView, "rvNewestProduct");
                    ViewExtensionsKt.hide(recyclerView);
                    return;
                }
                RecyclerView recyclerView2 = (RecyclerView) this.this$0._$_findCachedViewById(R.id.rvNewestProduct);
                Intrinsics.checkExpressionValueIsNotNull(recyclerView2, "rvNewestProduct");
                ViewExtensionsKt.show(recyclerView2);
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type com.iqonic.store.AppBaseActivity");
        }
    }
}
