package com.iqonic.store.fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import androidx.fragment.app.FragmentActivity;
import com.iqonic.store.activity.ViewAllProductActivity;
import kotlin.Metadata;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\b\u0003\n\u0002\b\u0004\u0010\u0000\u001a\u00020\u0001\"\b\b\u0000\u0010\u0002*\u00020\u00032\u000e\u0010\u0004\u001a\n \u0005*\u0004\u0018\u00010\u00030\u0003H\n¢\u0006\u0002\b\u0006¨\u0006\u0007"}, d2 = {"<anonymous>", "", "T", "Landroid/view/View;", "it", "kotlin.jvm.PlatformType", "onClick", "com/iqonic/store/utils/extensions/ExtensionsKt$onClick$1"}, k = 3, mv = {1, 1, 16})
/* compiled from: Extensions.kt */
public final class HomeFragment2$onAddView$$inlined$onClick$1 implements View.OnClickListener {
    final /* synthetic */ int $code$inlined;
    final /* synthetic */ String $specialKey$inlined;
    final /* synthetic */ View $this_onClick;
    final /* synthetic */ String $title$inlined;
    final /* synthetic */ HomeFragment2 this$0;

    public HomeFragment2$onAddView$$inlined$onClick$1(View view, HomeFragment2 homeFragment2, String str, int i, String str2) {
        this.$this_onClick = view;
        this.this$0 = homeFragment2;
        this.$title$inlined = str;
        this.$code$inlined = i;
        this.$specialKey$inlined = str2;
    }

    public final void onClick(View view) {
        TextView textView = (TextView) this.$this_onClick;
        FragmentActivity activity = this.this$0.getActivity();
        if (activity != null) {
            Activity activity2 = activity;
            Bundle bundle = null;
            Intent intent = new Intent(activity2, ViewAllProductActivity.class);
            new HomeFragment2$onAddView$$inlined$onClick$1$lambda$1(this).invoke(intent);
            if (Build.VERSION.SDK_INT >= 16) {
                activity2.startActivityForResult(intent, -1, bundle);
            } else {
                activity2.startActivityForResult(intent, -1);
            }
        }
    }
}
