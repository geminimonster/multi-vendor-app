package com.iqonic.store.fragments;

import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import com.iqonic.store.R;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\b\u0003\n\u0002\b\u0004\u0010\u0000\u001a\u00020\u0001\"\b\b\u0000\u0010\u0002*\u00020\u00032\u000e\u0010\u0004\u001a\n \u0005*\u0004\u0018\u00010\u00030\u0003H\n¢\u0006\u0002\b\u0006¨\u0006\u0007"}, d2 = {"<anonymous>", "", "T", "Landroid/view/View;", "it", "kotlin.jvm.PlatformType", "onClick", "com/iqonic/store/utils/extensions/ExtensionsKt$onClick$1"}, k = 3, mv = {1, 1, 16})
/* compiled from: Extensions.kt */
public final class SignUpFragment$initializeFragment$$inlined$onClick$5 implements View.OnClickListener {
    final /* synthetic */ View $this_onClick;
    final /* synthetic */ SignUpFragment this$0;

    public SignUpFragment$initializeFragment$$inlined$onClick$5(View view, SignUpFragment signUpFragment) {
        this.$this_onClick = view;
        this.this$0 = signUpFragment;
    }

    public final void onClick(View view) {
        ImageView imageView = (ImageView) this.$this_onClick;
        EditText editText = (EditText) this.this$0._$_findCachedViewById(R.id.edtConfirmPassword);
        Intrinsics.checkExpressionValueIsNotNull(editText, "edtConfirmPassword");
        if (Intrinsics.areEqual((Object) editText.getTransformationMethod(), (Object) PasswordTransformationMethod.getInstance())) {
            ((ImageView) this.this$0._$_findCachedViewById(R.id.ivConfirmPwd)).setImageResource(com.store.proshop.R.drawable.ic_eye_line);
            EditText editText2 = (EditText) this.this$0._$_findCachedViewById(R.id.edtConfirmPassword);
            Intrinsics.checkExpressionValueIsNotNull(editText2, "edtConfirmPassword");
            editText2.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
            EditText editText3 = (EditText) this.this$0._$_findCachedViewById(R.id.edtConfirmPassword);
            Intrinsics.checkExpressionValueIsNotNull(editText3, "edtConfirmPassword");
            ((EditText) this.this$0._$_findCachedViewById(R.id.edtConfirmPassword)).setSelection(editText3.getText().length());
            return;
        }
        ((ImageView) this.this$0._$_findCachedViewById(R.id.ivConfirmPwd)).setImageResource(com.store.proshop.R.drawable.ic_eye_off_line);
        EditText editText4 = (EditText) this.this$0._$_findCachedViewById(R.id.edtConfirmPassword);
        Intrinsics.checkExpressionValueIsNotNull(editText4, "edtConfirmPassword");
        editText4.setTransformationMethod(PasswordTransformationMethod.getInstance());
        EditText editText5 = (EditText) this.this$0._$_findCachedViewById(R.id.edtConfirmPassword);
        Intrinsics.checkExpressionValueIsNotNull(editText5, "edtConfirmPassword");
        ((EditText) this.this$0._$_findCachedViewById(R.id.edtConfirmPassword)).setSelection(editText5.getText().length());
    }
}
