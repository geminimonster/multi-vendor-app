package com.iqonic.store.fragments;

import android.os.Bundle;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.EditText;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import com.iqonic.store.AppBaseActivity;
import com.iqonic.store.utils.extensions.ExtensionsKt;
import com.store.proshop.R;
import java.util.HashMap;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\b&\u0018\u00002\u00020\u00012\u00020\u0002:\u0001\u000fB\u0005¢\u0006\u0002\u0010\u0003J\u0006\u0010\u0004\u001a\u00020\u0005J\u0012\u0010\u0006\u001a\u00020\u00052\b\u0010\u0007\u001a\u0004\u0018\u00010\bH\u0016J\u001a\u0010\t\u001a\u00020\u00052\b\u0010\n\u001a\u0004\u0018\u00010\u000b2\u0006\u0010\f\u001a\u00020\rH\u0016J\u0006\u0010\u000e\u001a\u00020\u0005¨\u0006\u0010"}, d2 = {"Lcom/iqonic/store/fragments/BaseFragment;", "Landroidx/fragment/app/Fragment;", "Landroid/view/View$OnFocusChangeListener;", "()V", "hideProgress", "", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onFocusChange", "v", "Landroid/view/View;", "hasFocus", "", "showProgress", "BiggerDotTransformation", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: BaseFragment.kt */
public abstract class BaseFragment extends Fragment implements View.OnFocusChangeListener {
    private HashMap _$_findViewCache;

    public void _$_clearFindViewByIdCache() {
        HashMap hashMap = this._$_findViewCache;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    public View _$_findCachedViewById(int i) {
        if (this._$_findViewCache == null) {
            this._$_findViewCache = new HashMap();
        }
        View view = (View) this._$_findViewCache.get(Integer.valueOf(i));
        if (view != null) {
            return view;
        }
        View view2 = getView();
        if (view2 == null) {
            return null;
        }
        View findViewById = view2.findViewById(i);
        this._$_findViewCache.put(Integer.valueOf(i), findViewById);
        return findViewById;
    }

    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        _$_clearFindViewByIdCache();
    }

    public void onFocusChange(View view, boolean z) {
        if (z) {
            if (view != null) {
                EditText editText = (EditText) view;
                FragmentActivity activity = getActivity();
                if (activity == null) {
                    Intrinsics.throwNpe();
                }
                Intrinsics.checkExpressionValueIsNotNull(activity, "activity!!");
                editText.setTextColor(ExtensionsKt.color(activity, R.color.colorPrimaryDark));
                FragmentActivity activity2 = getActivity();
                if (activity2 == null) {
                    Intrinsics.throwNpe();
                }
                editText.setBackground(activity2.getDrawable(R.drawable.bg_ractangle_rounded_active));
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type android.widget.EditText");
        } else if (view != null) {
            EditText editText2 = (EditText) view;
            FragmentActivity activity3 = getActivity();
            if (activity3 == null) {
                Intrinsics.throwNpe();
            }
            Intrinsics.checkExpressionValueIsNotNull(activity3, "activity!!");
            editText2.setTextColor(ExtensionsKt.color(activity3, R.color.textColorPrimary));
            FragmentActivity activity4 = getActivity();
            if (activity4 == null) {
                Intrinsics.throwNpe();
            }
            editText2.setBackground(activity4.getDrawable(R.drawable.bg_ractangle_rounded_inactive));
        } else {
            throw new TypeCastException("null cannot be cast to non-null type android.widget.EditText");
        }
    }

    public final void hideProgress() {
        if (getActivity() != null) {
            FragmentActivity activity = getActivity();
            if (activity == null) {
                Intrinsics.throwNpe();
            }
            if (activity != null) {
                ((AppBaseActivity) activity).showProgress(false);
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type com.iqonic.store.AppBaseActivity");
        }
    }

    public final void showProgress() {
        if (getActivity() != null) {
            FragmentActivity activity = getActivity();
            if (activity == null) {
                Intrinsics.throwNpe();
            }
            if (activity != null) {
                ((AppBaseActivity) activity).showProgress(true);
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type com.iqonic.store.AppBaseActivity");
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\f\n\u0002\b\u0002\n\u0002\u0010\r\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\bÆ\u0002\u0018\u00002\u00020\u0001:\u0001\u000bB\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0018\u0010\u0006\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\u00072\u0006\u0010\t\u001a\u00020\nH\u0016R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\f"}, d2 = {"Lcom/iqonic/store/fragments/BaseFragment$BiggerDotTransformation;", "Landroid/text/method/PasswordTransformationMethod;", "()V", "BIGGER_DOT", "", "DOT", "getTransformation", "", "source", "view", "Landroid/view/View;", "PasswordCharSequence", "app_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: BaseFragment.kt */
    public static final class BiggerDotTransformation extends PasswordTransformationMethod {
        private static final char BIGGER_DOT = '●';
        private static final char DOT = '•';
        public static final BiggerDotTransformation INSTANCE = new BiggerDotTransformation();

        private BiggerDotTransformation() {
        }

        public CharSequence getTransformation(CharSequence charSequence, View view) {
            Intrinsics.checkParameterIsNotNull(charSequence, "source");
            Intrinsics.checkParameterIsNotNull(view, "view");
            CharSequence transformation = super.getTransformation(charSequence, view);
            Intrinsics.checkExpressionValueIsNotNull(transformation, "super.getTransformation(source, view)");
            return new PasswordCharSequence(transformation);
        }

        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\r\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0010\f\n\u0002\b\u0005\b\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0001¢\u0006\u0002\u0010\u0003J\u0011\u0010\n\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\u0005H\u0002J\u0019\u0010\r\u001a\u00020\u00012\u0006\u0010\u000e\u001a\u00020\u00052\u0006\u0010\u000f\u001a\u00020\u0005H\u0001R\u0012\u0010\u0004\u001a\u00020\u0005X\u0005¢\u0006\u0006\u001a\u0004\b\u0006\u0010\u0007R\u0011\u0010\u0002\u001a\u00020\u0001¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\t¨\u0006\u0010"}, d2 = {"Lcom/iqonic/store/fragments/BaseFragment$BiggerDotTransformation$PasswordCharSequence;", "", "transformation", "(Ljava/lang/CharSequence;)V", "length", "", "getLength", "()I", "getTransformation", "()Ljava/lang/CharSequence;", "get", "", "index", "subSequence", "startIndex", "endIndex", "app_release"}, k = 1, mv = {1, 1, 16})
        /* compiled from: BaseFragment.kt */
        private static final class PasswordCharSequence implements CharSequence {
            private final CharSequence transformation;

            public int getLength() {
                return this.transformation.length();
            }

            public CharSequence subSequence(int i, int i2) {
                return this.transformation.subSequence(i, i2);
            }

            public PasswordCharSequence(CharSequence charSequence) {
                Intrinsics.checkParameterIsNotNull(charSequence, "transformation");
                this.transformation = charSequence;
            }

            public final /* bridge */ char charAt(int i) {
                return get(i);
            }

            public final /* bridge */ int length() {
                return getLength();
            }

            public final CharSequence getTransformation() {
                return this.transformation;
            }

            public char get(int i) {
                if (this.transformation.charAt(i) == 8226) {
                    return BiggerDotTransformation.BIGGER_DOT;
                }
                return this.transformation.charAt(i);
            }
        }
    }
}
