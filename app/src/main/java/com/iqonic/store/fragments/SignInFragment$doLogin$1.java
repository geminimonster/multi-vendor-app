package com.iqonic.store.fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import androidx.fragment.app.FragmentActivity;
import com.iqonic.store.AppBaseActivity;
import com.iqonic.store.activity.DashBoardActivity;
import com.iqonic.store.activity.EditProfileActivity;
import com.iqonic.store.models.loginResponse;
import com.iqonic.store.utils.extensions.AppExtensionsKt;
import com.iqonic.store.utils.extensions.ExtensionsKt$launchActivityWithNewTask$1;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n¢\u0006\u0002\b\u0004"}, d2 = {"<anonymous>", "", "it", "Lcom/iqonic/store/models/loginResponse;", "invoke"}, k = 3, mv = {1, 1, 16})
/* compiled from: SignInFragment.kt */
final class SignInFragment$doLogin$1 extends Lambda implements Function1<loginResponse, Unit> {
    final /* synthetic */ String $mUsername;
    final /* synthetic */ SignInFragment this$0;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    SignInFragment$doLogin$1(SignInFragment signInFragment, String str) {
        super(1);
        this.this$0 = signInFragment;
        this.$mUsername = str;
    }

    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        invoke((loginResponse) obj);
        return Unit.INSTANCE;
    }

    public final void invoke(loginResponse loginresponse) {
        Intrinsics.checkParameterIsNotNull(loginresponse, "it");
        FragmentActivity activity = this.this$0.getActivity();
        if (activity != null) {
            ((AppBaseActivity) activity).setResult(-1);
            AppExtensionsKt.getSharedPrefInstance().removeKey("user_username");
            AppExtensionsKt.getSharedPrefInstance().setValue("user_username", this.$mUsername);
            if (loginresponse.getBilling().getFirst_name().length() == 0) {
                FragmentActivity activity2 = this.this$0.getActivity();
                if (activity2 != null) {
                    Activity activity3 = activity2;
                    Bundle bundle = null;
                    Intent intent = new Intent(activity3, EditProfileActivity.class);
                    AnonymousClass1.INSTANCE.invoke(intent);
                    if (Build.VERSION.SDK_INT >= 16) {
                        activity3.startActivityForResult(intent, -1, bundle);
                    } else {
                        activity3.startActivityForResult(intent, -1);
                    }
                }
            } else {
                FragmentActivity activity4 = this.this$0.getActivity();
                if (activity4 != null) {
                    Activity activity5 = activity4;
                    Bundle bundle2 = null;
                    Intent intent2 = new Intent(activity5, DashBoardActivity.class);
                    ExtensionsKt$launchActivityWithNewTask$1.INSTANCE.invoke(intent2);
                    if (Build.VERSION.SDK_INT >= 16) {
                        activity5.startActivityForResult(intent2, -1, bundle2);
                    } else {
                        activity5.startActivityForResult(intent2, -1);
                    }
                }
            }
            FragmentActivity activity6 = this.this$0.getActivity();
            if (activity6 != null) {
                ((AppBaseActivity) activity6).finish();
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type com.iqonic.store.AppBaseActivity");
        }
        throw new TypeCastException("null cannot be cast to non-null type com.iqonic.store.AppBaseActivity");
    }
}
