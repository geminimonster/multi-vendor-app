package com.iqonic.store.fragments;

import android.app.Activity;
import android.widget.EditText;
import androidx.fragment.app.FragmentActivity;
import com.iqonic.store.AppBaseActivity;
import com.iqonic.store.R;
import com.iqonic.store.models.BaseResponse;
import com.iqonic.store.utils.extensions.ExtensionsKt;
import com.iqonic.store.utils.extensions.ViewExtensionsKt;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n¢\u0006\u0002\b\u0004¨\u0006\u0005"}, d2 = {"<anonymous>", "", "it", "Lcom/iqonic/store/models/BaseResponse;", "invoke", "com/iqonic/store/fragments/SignInFragment$showChangePasswordDialog$1$1"}, k = 3, mv = {1, 1, 16})
/* compiled from: SignInFragment.kt */
final class SignInFragment$showChangePasswordDialog$$inlined$onClick$1$lambda$1 extends Lambda implements Function1<BaseResponse, Unit> {
    final /* synthetic */ SignInFragment$showChangePasswordDialog$$inlined$onClick$1 this$0;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    SignInFragment$showChangePasswordDialog$$inlined$onClick$1$lambda$1(SignInFragment$showChangePasswordDialog$$inlined$onClick$1 signInFragment$showChangePasswordDialog$$inlined$onClick$1) {
        super(1);
        this.this$0 = signInFragment$showChangePasswordDialog$$inlined$onClick$1;
    }

    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        invoke((BaseResponse) obj);
        return Unit.INSTANCE;
    }

    public final void invoke(BaseResponse baseResponse) {
        Intrinsics.checkParameterIsNotNull(baseResponse, "it");
        this.this$0.this$0.hideProgress();
        EditText editText = (EditText) this.this$0.$changePasswordDialog$inlined.findViewById(R.id.edtForgotEmail);
        Intrinsics.checkExpressionValueIsNotNull(editText, "changePasswordDialog.edtForgotEmail");
        ViewExtensionsKt.hideSoftKeyboard(editText);
        FragmentActivity activity = this.this$0.this$0.getActivity();
        if (activity != null) {
            Activity activity2 = (AppBaseActivity) activity;
            String message = baseResponse.getMessage();
            if (message == null) {
                message = "";
            }
            ExtensionsKt.snackBar$default(activity2, message, 0, 2, (Object) null);
            this.this$0.$changePasswordDialog$inlined.dismiss();
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.iqonic.store.AppBaseActivity");
    }
}
