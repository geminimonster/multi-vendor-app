package com.iqonic.store.fragments;

import com.iqonic.store.models.Category;
import java.util.ArrayList;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0016\u0010\u0002\u001a\u0012\u0012\u0004\u0012\u00020\u00040\u0003j\b\u0012\u0004\u0012\u00020\u0004`\u0005H\n¢\u0006\u0002\b\u0006"}, d2 = {"<anonymous>", "", "it", "Ljava/util/ArrayList;", "Lcom/iqonic/store/models/Category;", "Lkotlin/collections/ArrayList;", "invoke"}, k = 3, mv = {1, 1, 16})
/* compiled from: HomeFragment2.kt */
final class HomeFragment2$listAllProducts$3 extends Lambda implements Function1<ArrayList<Category>, Unit> {
    final /* synthetic */ HomeFragment2 this$0;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    HomeFragment2$listAllProducts$3(HomeFragment2 homeFragment2) {
        super(1);
        this.this$0 = homeFragment2;
    }

    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        invoke((ArrayList<Category>) (ArrayList) obj);
        return Unit.INSTANCE;
    }

    public final void invoke(ArrayList<Category> arrayList) {
        Intrinsics.checkParameterIsNotNull(arrayList, "it");
        this.this$0.mProductAdapter.addMoreItems(arrayList);
    }
}
