package com.iqonic.store.fragments;

import android.widget.LinearLayout;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import com.iqonic.store.R;
import com.iqonic.store.utils.CustomSwipeToRefresh;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n¢\u0006\u0002\b\u0002"}, d2 = {"<anonymous>", "", "onRefresh"}, k = 3, mv = {1, 1, 16})
/* compiled from: HomeFragment1.kt */
final class HomeFragment1$onViewCreated$1 implements SwipeRefreshLayout.OnRefreshListener {
    final /* synthetic */ HomeFragment1 this$0;

    HomeFragment1$onViewCreated$1(HomeFragment1 homeFragment1) {
        this.this$0 = homeFragment1;
    }

    public final void onRefresh() {
        LinearLayout linearLayout = (LinearLayout) this.this$0._$_findCachedViewById(R.id.dashboardMainView);
        if (linearLayout == null) {
            Intrinsics.throwNpe();
        }
        linearLayout.removeAllViews();
        this.this$0.listAllProducts();
        CustomSwipeToRefresh customSwipeToRefresh = (CustomSwipeToRefresh) this.this$0._$_findCachedViewById(R.id.refreshLayout);
        Intrinsics.checkExpressionValueIsNotNull(customSwipeToRefresh, "refreshLayout");
        customSwipeToRefresh.setRefreshing(false);
    }
}
