package com.iqonic.store.fragments;

import androidx.fragment.app.FragmentActivity;
import com.iqonic.store.AppBaseActivity;
import com.iqonic.store.models.RequestModel;
import com.iqonic.store.utils.extensions.AppExtensionsKt;
import com.iqonic.store.utils.extensions.ExtensionsKt;
import com.iqonic.store.utils.extensions.NetworkExtensionKt;
import com.store.proshop.R;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n¢\u0006\u0002\b\u0004¨\u0006\u0005"}, d2 = {"<anonymous>", "", "it", "", "invoke", "com/iqonic/store/fragments/WishListFragment$mListAdapter$1$2$2"}, k = 3, mv = {1, 1, 16})
/* compiled from: WishListFragment.kt */
final class WishListFragment$mListAdapter$1$$special$$inlined$onClick$2$lambda$2 extends Lambda implements Function1<String, Unit> {
    final /* synthetic */ RequestModel $requestModel;
    final /* synthetic */ WishListFragment$mListAdapter$1$$special$$inlined$onClick$2 this$0;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    WishListFragment$mListAdapter$1$$special$$inlined$onClick$2$lambda$2(RequestModel requestModel, WishListFragment$mListAdapter$1$$special$$inlined$onClick$2 wishListFragment$mListAdapter$1$$special$$inlined$onClick$2) {
        super(1);
        this.$requestModel = requestModel;
        this.this$0 = wishListFragment$mListAdapter$1$$special$$inlined$onClick$2;
    }

    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        invoke((String) obj);
        return Unit.INSTANCE;
    }

    public final void invoke(String str) {
        Intrinsics.checkParameterIsNotNull(str, "it");
        if (this.this$0.this$0.this$0.getActivity() != null) {
            ExtensionsKt.snackBar(this.this$0.this$0.this$0, str);
            FragmentActivity activity = this.this$0.this$0.this$0.getActivity();
            if (activity != null) {
                NetworkExtensionKt.removeFromWishList((AppBaseActivity) activity, this.$requestModel, new Function1<Boolean, Unit>(this) {
                    final /* synthetic */ WishListFragment$mListAdapter$1$$special$$inlined$onClick$2$lambda$2 this$0;

                    {
                        this.this$0 = r1;
                    }

                    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
                        invoke(((Boolean) obj).booleanValue());
                        return Unit.INSTANCE;
                    }

                    public final void invoke(boolean z) {
                        if (z) {
                            WishListFragment wishListFragment = this.this$0.this$0.this$0.this$0;
                            String string = this.this$0.this$0.this$0.this$0.getString(R.string.lbl_remove);
                            Intrinsics.checkExpressionValueIsNotNull(string, "getString(R.string.lbl_remove)");
                            ExtensionsKt.snackBar(wishListFragment, string);
                        }
                        this.this$0.this$0.this$0.this$0.hideProgress();
                        this.this$0.this$0.this$0.this$0.wishListItemChange();
                    }
                });
                FragmentActivity activity2 = this.this$0.this$0.this$0.getActivity();
                if (activity2 != null) {
                    AppExtensionsKt.fetchAndStoreCartData((AppBaseActivity) activity2);
                    return;
                }
                throw new TypeCastException("null cannot be cast to non-null type com.iqonic.store.AppBaseActivity");
            }
            throw new TypeCastException("null cannot be cast to non-null type com.iqonic.store.AppBaseActivity");
        }
    }
}
