package com.iqonic.store.fragments;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.iqonic.store.R;
import com.iqonic.store.models.WishList;
import com.iqonic.store.utils.extensions.ExtensionsKt;
import com.iqonic.store.utils.extensions.NetworkExtensionKt;
import com.iqonic.store.utils.extensions.StringExtensionsKt;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function3;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0007H\n¢\u0006\u0002\b\b"}, d2 = {"<anonymous>", "", "view", "Landroid/view/View;", "model", "Lcom/iqonic/store/models/WishList;", "<anonymous parameter 2>", "", "invoke"}, k = 3, mv = {1, 1, 16})
/* compiled from: WishListFragment.kt */
final class WishListFragment$mListAdapter$1 extends Lambda implements Function3<View, WishList, Integer, Unit> {
    final /* synthetic */ WishListFragment this$0;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    WishListFragment$mListAdapter$1(WishListFragment wishListFragment) {
        super(3);
        this.this$0 = wishListFragment;
    }

    public /* bridge */ /* synthetic */ Object invoke(Object obj, Object obj2, Object obj3) {
        invoke((View) obj, (WishList) obj2, ((Number) obj3).intValue());
        return Unit.INSTANCE;
    }

    public final void invoke(View view, WishList wishList, int i) {
        Intrinsics.checkParameterIsNotNull(view, "view");
        Intrinsics.checkParameterIsNotNull(wishList, "model");
        if (this.this$0.getActivity() != null) {
            boolean z = false;
            if (wishList.getSale_price().length() > 0) {
                TextView textView = (TextView) view.findViewById(R.id.tvOriginalPrice);
                Intrinsics.checkExpressionValueIsNotNull(textView, "view.tvOriginalPrice");
                ExtensionsKt.applyStrike(textView);
                TextView textView2 = (TextView) view.findViewById(R.id.tvDiscountPrice);
                Intrinsics.checkExpressionValueIsNotNull(textView2, "view.tvDiscountPrice");
                textView2.setText(StringExtensionsKt.currencyFormat$default(wishList.getSale_price(), (String) null, 1, (Object) null));
                TextView textView3 = (TextView) view.findViewById(R.id.tvOriginalPrice);
                Intrinsics.checkExpressionValueIsNotNull(textView3, "view.tvOriginalPrice");
                textView3.setText(StringExtensionsKt.currencyFormat$default(wishList.getRegular_price(), (String) null, 1, (Object) null));
            } else {
                if (wishList.getRegular_price().length() == 0) {
                    TextView textView4 = (TextView) view.findViewById(R.id.tvDiscountPrice);
                    Intrinsics.checkExpressionValueIsNotNull(textView4, "view.tvDiscountPrice");
                    textView4.setText(StringExtensionsKt.currencyFormat$default(wishList.getPrice(), (String) null, 1, (Object) null));
                } else {
                    TextView textView5 = (TextView) view.findViewById(R.id.tvDiscountPrice);
                    Intrinsics.checkExpressionValueIsNotNull(textView5, "view.tvDiscountPrice");
                    textView5.setText(StringExtensionsKt.currencyFormat$default(wishList.getRegular_price(), (String) null, 1, (Object) null));
                }
            }
            TextView textView6 = (TextView) view.findViewById(R.id.tvProductName);
            Intrinsics.checkExpressionValueIsNotNull(textView6, "view.tvProductName");
            textView6.setText(wishList.getName());
            TextView textView7 = (TextView) view.findViewById(R.id.tvProductName);
            Intrinsics.checkExpressionValueIsNotNull(textView7, "view.tvProductName");
            ExtensionsKt.changeTextPrimaryColor(textView7);
            TextView textView8 = (TextView) view.findViewById(R.id.tvDiscountPrice);
            Intrinsics.checkExpressionValueIsNotNull(textView8, "view.tvDiscountPrice");
            ExtensionsKt.changeTextPrimaryColor(textView8);
            TextView textView9 = (TextView) view.findViewById(R.id.tvOriginalPrice);
            Intrinsics.checkExpressionValueIsNotNull(textView9, "view.tvOriginalPrice");
            ExtensionsKt.changeTextSecondaryColor(textView9);
            if (wishList.getFull().length() > 0) {
                z = true;
            }
            if (z) {
                ImageView imageView = (ImageView) view.findViewById(R.id.ivProduct);
                Intrinsics.checkExpressionValueIsNotNull(imageView, "view.ivProduct");
                NetworkExtensionKt.loadImageFromUrl$default(imageView, wishList.getFull(), 0, 0, 6, (Object) null);
            }
        }
        view.setOnClickListener(new WishListFragment$mListAdapter$1$$special$$inlined$onClick$1(view, this, wishList));
        ImageView imageView2 = (ImageView) view.findViewById(R.id.ivMoveToCart);
        imageView2.setOnClickListener(new WishListFragment$mListAdapter$1$$special$$inlined$onClick$2(imageView2, this, wishList));
    }
}
