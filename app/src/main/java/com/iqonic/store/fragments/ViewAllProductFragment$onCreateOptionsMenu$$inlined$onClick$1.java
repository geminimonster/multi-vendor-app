package com.iqonic.store.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import com.iqonic.store.ShopHopApp;
import com.iqonic.store.activity.MyCartActivity;
import com.iqonic.store.activity.SignInUpActivity;
import com.iqonic.store.utils.extensions.AppExtensionsKt;
import com.iqonic.store.utils.extensions.ExtensionsKt$launchActivity$2;
import kotlin.Metadata;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\b\u0003\n\u0002\b\u0004\u0010\u0000\u001a\u00020\u0001\"\b\b\u0000\u0010\u0002*\u00020\u00032\u000e\u0010\u0004\u001a\n \u0005*\u0004\u0018\u00010\u00030\u0003H\n¢\u0006\u0002\b\u0006¨\u0006\u0007"}, d2 = {"<anonymous>", "", "T", "Landroid/view/View;", "it", "kotlin.jvm.PlatformType", "onClick", "com/iqonic/store/utils/extensions/ExtensionsKt$onClick$1"}, k = 3, mv = {1, 1, 16})
/* compiled from: Extensions.kt */
public final class ViewAllProductFragment$onCreateOptionsMenu$$inlined$onClick$1 implements View.OnClickListener {
    final /* synthetic */ View $this_onClick;
    final /* synthetic */ ViewAllProductFragment this$0;

    public ViewAllProductFragment$onCreateOptionsMenu$$inlined$onClick$1(View view, ViewAllProductFragment viewAllProductFragment) {
        this.$this_onClick = view;
        this.this$0 = viewAllProductFragment;
    }

    public final void onClick(View view) {
        if (AppExtensionsKt.isLoggedIn()) {
            Intent intent = new Intent(ShopHopApp.Companion.getAppInstance(), MyCartActivity.class);
            ExtensionsKt$launchActivity$2.INSTANCE.invoke(intent);
            this.this$0.startActivityForResult(intent, -1, (Bundle) null);
            return;
        }
        Intent intent2 = new Intent(ShopHopApp.Companion.getAppInstance(), SignInUpActivity.class);
        ExtensionsKt$launchActivity$2.INSTANCE.invoke(intent2);
        this.this$0.startActivityForResult(intent2, -1, (Bundle) null);
    }
}
