package com.iqonic.store.fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import androidx.fragment.app.FragmentActivity;
import com.iqonic.store.activity.ProductDetailActivity1;
import com.iqonic.store.activity.ProductDetailActivity2;
import com.iqonic.store.models.WishList;
import com.iqonic.store.utils.extensions.AppExtensionsKt;
import kotlin.Metadata;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\b\u0003\n\u0002\b\u0004\u0010\u0000\u001a\u00020\u0001\"\b\b\u0000\u0010\u0002*\u00020\u00032\u000e\u0010\u0004\u001a\n \u0005*\u0004\u0018\u00010\u00030\u0003H\n¢\u0006\u0002\b\u0006¨\u0006\u0007"}, d2 = {"<anonymous>", "", "T", "Landroid/view/View;", "it", "kotlin.jvm.PlatformType", "onClick", "com/iqonic/store/utils/extensions/ExtensionsKt$onClick$1"}, k = 3, mv = {1, 1, 16})
/* compiled from: Extensions.kt */
public final class WishListFragment$mListAdapter$1$$special$$inlined$onClick$1 implements View.OnClickListener {
    final /* synthetic */ WishList $model$inlined;
    final /* synthetic */ View $this_onClick;
    final /* synthetic */ WishListFragment$mListAdapter$1 this$0;

    public WishListFragment$mListAdapter$1$$special$$inlined$onClick$1(View view, WishListFragment$mListAdapter$1 wishListFragment$mListAdapter$1, WishList wishList) {
        this.$this_onClick = view;
        this.this$0 = wishListFragment$mListAdapter$1;
        this.$model$inlined = wishList;
    }

    public final void onClick(View view) {
        if (AppExtensionsKt.getProductDetailConstant() == 0) {
            FragmentActivity activity = this.this$0.this$0.getActivity();
            if (activity != null) {
                Activity activity2 = activity;
                Bundle bundle = null;
                Intent intent = new Intent(activity2, ProductDetailActivity1.class);
                new WishListFragment$mListAdapter$1$$special$$inlined$onClick$1$lambda$1(this).invoke(intent);
                if (Build.VERSION.SDK_INT >= 16) {
                    activity2.startActivityForResult(intent, -1, bundle);
                } else {
                    activity2.startActivityForResult(intent, -1);
                }
            }
        } else {
            FragmentActivity activity3 = this.this$0.this$0.getActivity();
            if (activity3 != null) {
                Activity activity4 = activity3;
                Bundle bundle2 = null;
                Intent intent2 = new Intent(activity4, ProductDetailActivity2.class);
                new WishListFragment$mListAdapter$1$$special$$inlined$onClick$1$lambda$2(this).invoke(intent2);
                if (Build.VERSION.SDK_INT >= 16) {
                    activity4.startActivityForResult(intent2, -1, bundle2);
                } else {
                    activity4.startActivityForResult(intent2, -1);
                }
            }
        }
    }
}
