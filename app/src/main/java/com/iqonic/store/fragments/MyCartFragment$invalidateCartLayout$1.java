package com.iqonic.store.fragments;

import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.FragmentActivity;
import com.facebook.appevents.AppEventsConstants;
import com.iqonic.store.AppBaseActivity;
import com.iqonic.store.R;
import com.iqonic.store.models.CartResponse;
import com.iqonic.store.models.Line_items;
import com.iqonic.store.utils.Constants;
import com.iqonic.store.utils.extensions.AppExtensionsKt;
import com.iqonic.store.utils.extensions.StringExtensionsKt;
import com.iqonic.store.utils.extensions.ViewExtensionsKt;
import java.util.ArrayList;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0016\u0010\u0002\u001a\u0012\u0012\u0004\u0012\u00020\u00040\u0003j\b\u0012\u0004\u0012\u00020\u0004`\u0005H\n¢\u0006\u0002\b\u0006"}, d2 = {"<anonymous>", "", "it", "Ljava/util/ArrayList;", "Lcom/iqonic/store/models/CartResponse;", "Lkotlin/collections/ArrayList;", "invoke"}, k = 3, mv = {1, 1, 16})
/* compiled from: MyCartFragment.kt */
final class MyCartFragment$invalidateCartLayout$1 extends Lambda implements Function1<ArrayList<CartResponse>, Unit> {
    final /* synthetic */ MyCartFragment this$0;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    MyCartFragment$invalidateCartLayout$1(MyCartFragment myCartFragment) {
        super(1);
        this.this$0 = myCartFragment;
    }

    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        invoke((ArrayList<CartResponse>) (ArrayList) obj);
        return Unit.INSTANCE;
    }

    public final void invoke(ArrayList<CartResponse> arrayList) {
        int i;
        int i2;
        String str;
        Intrinsics.checkParameterIsNotNull(arrayList, "it");
        if (this.this$0.getActivity() != null) {
            AppExtensionsKt.getSharedPrefInstance().setValue(Constants.SharedPref.KEY_CART_COUNT, Integer.valueOf(arrayList.size()));
            FragmentActivity activity = this.this$0.getActivity();
            if (activity != null) {
                AppExtensionsKt.sendCartCountChangeBroadcast((AppBaseActivity) activity);
                if (arrayList.size() == 0) {
                    this.this$0.invalidatePaymentLayout(false);
                    FragmentActivity activity2 = this.this$0.getActivity();
                    if (activity2 != null) {
                        ((AppBaseActivity) activity2).showProgress(false);
                        LinearLayout linearLayout = (LinearLayout) this.this$0._$_findCachedViewById(R.id.llNoItems);
                        Intrinsics.checkExpressionValueIsNotNull(linearLayout, "llNoItems");
                        ViewExtensionsKt.show(linearLayout);
                        LinearLayout linearLayout2 = (LinearLayout) this.this$0._$_findCachedViewById(R.id.lay_button);
                        Intrinsics.checkExpressionValueIsNotNull(linearLayout2, "lay_button");
                        ViewExtensionsKt.hide(linearLayout2);
                        LinearLayout linearLayout3 = (LinearLayout) this.this$0._$_findCachedViewById(R.id.llShipping);
                        Intrinsics.checkExpressionValueIsNotNull(linearLayout3, "llShipping");
                        ViewExtensionsKt.hide(linearLayout3);
                        NestedScrollView nestedScrollView = (NestedScrollView) this.this$0._$_findCachedViewById(R.id.nsvCart);
                        Intrinsics.checkExpressionValueIsNotNull(nestedScrollView, "nsvCart");
                        ViewExtensionsKt.hide(nestedScrollView);
                        return;
                    }
                    throw new TypeCastException("null cannot be cast to non-null type com.iqonic.store.AppBaseActivity");
                }
                LinearLayout linearLayout4 = (LinearLayout) this.this$0._$_findCachedViewById(R.id.llNoItems);
                Intrinsics.checkExpressionValueIsNotNull(linearLayout4, "llNoItems");
                ViewExtensionsKt.hide(linearLayout4);
                LinearLayout linearLayout5 = (LinearLayout) this.this$0._$_findCachedViewById(R.id.lay_button);
                Intrinsics.checkExpressionValueIsNotNull(linearLayout5, "lay_button");
                ViewExtensionsKt.show(linearLayout5);
                LinearLayout linearLayout6 = (LinearLayout) this.this$0._$_findCachedViewById(R.id.llShipping);
                Intrinsics.checkExpressionValueIsNotNull(linearLayout6, "llShipping");
                ViewExtensionsKt.show(linearLayout6);
                NestedScrollView nestedScrollView2 = (NestedScrollView) this.this$0._$_findCachedViewById(R.id.nsvCart);
                Intrinsics.checkExpressionValueIsNotNull(nestedScrollView2, "nsvCart");
                ViewExtensionsKt.show(nestedScrollView2);
                if (this.this$0.getActivity() != null) {
                    LinearLayout linearLayout7 = (LinearLayout) this.this$0._$_findCachedViewById(R.id.llNoItems);
                    Intrinsics.checkExpressionValueIsNotNull(linearLayout7, "llNoItems");
                    ViewExtensionsKt.hide(linearLayout7);
                    this.this$0.cartItemId = "";
                    this.this$0.mTotalCount = 0.0d;
                    ArrayList access$getMOrderItems$p = this.this$0.mOrderItems;
                    if (access$getMOrderItems$p == null) {
                        Intrinsics.throwNpe();
                    }
                    access$getMOrderItems$p.clear();
                    int size = arrayList.size();
                    int i3 = 0;
                    while (true) {
                        boolean z = true;
                        if (i3 >= size) {
                            break;
                        }
                        Line_items line_items = new Line_items(0, 0, 3, (DefaultConstructorMarker) null);
                        line_items.setProduct_id(Integer.parseInt(arrayList.get(i3).getPro_id()));
                        line_items.setQuantity(Integer.parseInt(arrayList.get(i3).getQuantity()));
                        ArrayList access$getMOrderItems$p2 = this.this$0.mOrderItems;
                        if (access$getMOrderItems$p2 == null) {
                            Intrinsics.throwNpe();
                        }
                        access$getMOrderItems$p2.add(line_items);
                        MyCartFragment myCartFragment = this.this$0;
                        double access$getMTotalCount$p = myCartFragment.mTotalCount;
                        try {
                            if (arrayList.get(i3).getSale_price().length() > 0) {
                                i2 = (int) Float.parseFloat(arrayList.get(i3).getSale_price());
                                i = Integer.parseInt(arrayList.get(i3).getQuantity());
                            } else {
                                if (arrayList.get(i3).getRegular_price().length() > 0) {
                                    i2 = (int) Float.parseFloat(arrayList.get(i3).getRegular_price());
                                    i = Integer.parseInt(arrayList.get(i3).getQuantity());
                                } else {
                                    i2 = (int) Float.parseFloat(arrayList.get(i3).getPrice());
                                    i = Integer.parseInt(arrayList.get(i3).getQuantity());
                                }
                            }
                        } catch (Exception unused) {
                            i2 = (int) Float.parseFloat(arrayList.get(i3).getPrice());
                            i = Integer.parseInt(arrayList.get(i3).getQuantity());
                        }
                        myCartFragment.mTotalCount = access$getMTotalCount$p + ((double) (i2 * i));
                        MyCartFragment myCartFragment2 = this.this$0;
                        if (myCartFragment2.cartItemId.length() <= 0) {
                            z = false;
                        }
                        if (z) {
                            str = this.this$0.cartItemId + "," + String.valueOf(line_items.getProduct_id());
                        } else {
                            str = String.valueOf(line_items.getProduct_id());
                        }
                        myCartFragment2.cartItemId = str;
                        i3++;
                    }
                    MyCartFragment myCartFragment3 = this.this$0;
                    myCartFragment3.mSubTotal = myCartFragment3.mTotalCount;
                    TextView textView = (TextView) this.this$0._$_findCachedViewById(R.id.tvTotal);
                    Intrinsics.checkExpressionValueIsNotNull(textView, "tvTotal");
                    textView.setText(StringExtensionsKt.currencyFormat$default(String.valueOf(this.this$0.mTotalCount), (String) null, 1, (Object) null));
                    if (this.this$0.isRemoveCoupons) {
                        TextView textView2 = (TextView) this.this$0._$_findCachedViewById(R.id.tvDiscount);
                        Intrinsics.checkExpressionValueIsNotNull(textView2, "tvDiscount");
                        textView2.setText(StringExtensionsKt.currencyFormat$default(AppEventsConstants.EVENT_PARAM_VALUE_NO, (String) null, 1, (Object) null));
                        TextView textView3 = (TextView) this.this$0._$_findCachedViewById(R.id.tvTotalCartAmount);
                        Intrinsics.checkExpressionValueIsNotNull(textView3, "tvTotalCartAmount");
                        textView3.setText(StringExtensionsKt.currencyFormat$default(String.valueOf(((int) this.this$0.mTotalCount) + Integer.parseInt(this.this$0.mShippingCost)), (String) null, 1, (Object) null));
                        TextView textView4 = (TextView) this.this$0._$_findCachedViewById(R.id.tvEditCoupon);
                        Intrinsics.checkExpressionValueIsNotNull(textView4, "tvEditCoupon");
                        textView4.setText(this.this$0.getString(com.store.proshop.R.string.lbl_apply));
                        TextView textView5 = (TextView) this.this$0._$_findCachedViewById(R.id.tvEditCoupon);
                        textView5.setOnClickListener(new MyCartFragment$invalidateCartLayout$1$$special$$inlined$onClick$1(textView5, this));
                    } else {
                        this.this$0.applyCouponCode();
                    }
                    this.this$0.invalidatePaymentLayout(true);
                    this.this$0.mCartAdapter.clearItems();
                    this.this$0.mCartAdapter.addItems(arrayList);
                    this.this$0.fetchShippingMethods();
                    return;
                }
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type com.iqonic.store.AppBaseActivity");
        }
    }
}
