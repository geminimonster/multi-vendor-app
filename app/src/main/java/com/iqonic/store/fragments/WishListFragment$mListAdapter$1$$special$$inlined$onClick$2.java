package com.iqonic.store.fragments;

import android.view.View;
import android.widget.ImageView;
import androidx.fragment.app.FragmentActivity;
import com.iqonic.store.models.RequestModel;
import com.iqonic.store.models.WishList;
import com.iqonic.store.utils.extensions.NetworkExtensionKt;
import kotlin.Metadata;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\b\u0003\n\u0002\b\u0004\u0010\u0000\u001a\u00020\u0001\"\b\b\u0000\u0010\u0002*\u00020\u00032\u000e\u0010\u0004\u001a\n \u0005*\u0004\u0018\u00010\u00030\u0003H\n¢\u0006\u0002\b\u0006¨\u0006\u0007"}, d2 = {"<anonymous>", "", "T", "Landroid/view/View;", "it", "kotlin.jvm.PlatformType", "onClick", "com/iqonic/store/utils/extensions/ExtensionsKt$onClick$1"}, k = 3, mv = {1, 1, 16})
/* compiled from: Extensions.kt */
public final class WishListFragment$mListAdapter$1$$special$$inlined$onClick$2 implements View.OnClickListener {
    final /* synthetic */ WishList $model$inlined;
    final /* synthetic */ View $this_onClick;
    final /* synthetic */ WishListFragment$mListAdapter$1 this$0;

    public WishListFragment$mListAdapter$1$$special$$inlined$onClick$2(View view, WishListFragment$mListAdapter$1 wishListFragment$mListAdapter$1, WishList wishList) {
        this.$this_onClick = view;
        this.this$0 = wishListFragment$mListAdapter$1;
        this.$model$inlined = wishList;
    }

    public final void onClick(View view) {
        ImageView imageView = (ImageView) this.$this_onClick;
        RequestModel requestModel = new RequestModel();
        requestModel.setPro_id(Integer.valueOf(this.$model$inlined.getPro_id()));
        requestModel.setQuantity(1);
        FragmentActivity activity = this.this$0.this$0.getActivity();
        if (activity != null) {
            activity.setResult(-1);
        }
        NetworkExtensionKt.getRestApiImpl$default((String) null, 1, (Object) null).addItemToCart(requestModel, new WishListFragment$mListAdapter$1$$special$$inlined$onClick$2$lambda$1(requestModel, this), new WishListFragment$mListAdapter$1$$special$$inlined$onClick$2$lambda$2(requestModel, this));
    }
}
