package com.iqonic.store.fragments;

import androidx.fragment.app.FragmentActivity;
import com.iqonic.store.activity.SignInUpActivity;
import com.iqonic.store.models.RegisterResponse;
import com.iqonic.store.utils.extensions.ExtensionsKt;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n¢\u0006\u0002\b\u0004"}, d2 = {"<anonymous>", "", "it", "Lcom/iqonic/store/models/RegisterResponse;", "invoke"}, k = 3, mv = {1, 1, 16})
/* compiled from: SignUpFragment.kt */
final class SignUpFragment$createCustomer$1 extends Lambda implements Function1<RegisterResponse, Unit> {
    final /* synthetic */ SignUpFragment this$0;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    SignUpFragment$createCustomer$1(SignUpFragment signUpFragment) {
        super(1);
        this.this$0 = signUpFragment;
    }

    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        invoke((RegisterResponse) obj);
        return Unit.INSTANCE;
    }

    public final void invoke(RegisterResponse registerResponse) {
        Intrinsics.checkParameterIsNotNull(registerResponse, "it");
        ExtensionsKt.snackBar(this.this$0, registerResponse.getMessage());
        FragmentActivity activity = this.this$0.getActivity();
        if (activity != null) {
            ((SignInUpActivity) activity).loadSignInFragment();
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.iqonic.store.activity.SignInUpActivity");
    }
}
