package com.iqonic.store.fragments;

import android.content.Intent;
import com.iqonic.store.utils.Constants;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\u0010\u0000\u001a\u00020\u0001*\u00020\u0002H\n¢\u0006\u0002\b\u0003¨\u0006\u0004"}, d2 = {"<anonymous>", "", "Landroid/content/Intent;", "invoke", "com/iqonic/store/fragments/MyCartFragment$mCartAdapter$1$4$2"}, k = 3, mv = {1, 1, 16})
/* compiled from: MyCartFragment.kt */
final class MyCartFragment$mCartAdapter$1$$special$$inlined$onClick$4$lambda$2 extends Lambda implements Function1<Intent, Unit> {
    final /* synthetic */ MyCartFragment$mCartAdapter$1$$special$$inlined$onClick$4 this$0;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    MyCartFragment$mCartAdapter$1$$special$$inlined$onClick$4$lambda$2(MyCartFragment$mCartAdapter$1$$special$$inlined$onClick$4 myCartFragment$mCartAdapter$1$$special$$inlined$onClick$4) {
        super(1);
        this.this$0 = myCartFragment$mCartAdapter$1$$special$$inlined$onClick$4;
    }

    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        invoke((Intent) obj);
        return Unit.INSTANCE;
    }

    public final void invoke(Intent intent) {
        Intrinsics.checkParameterIsNotNull(intent, "$receiver");
        intent.putExtra(Constants.KeyIntent.PRODUCT_ID, Integer.parseInt(this.this$0.$model$inlined.getPro_id()));
    }
}
