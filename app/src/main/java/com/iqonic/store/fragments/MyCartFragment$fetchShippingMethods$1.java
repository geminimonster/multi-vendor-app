package com.iqonic.store.fragments;

import androidx.fragment.app.FragmentActivity;
import com.iqonic.store.AppBaseActivity;
import com.iqonic.store.models.CountryModel;
import com.iqonic.store.models.Method;
import com.iqonic.store.models.RequestModel;
import com.iqonic.store.models.Shipping;
import com.iqonic.store.models.ShippingModel;
import com.iqonic.store.models.State;
import com.iqonic.store.utils.extensions.ExtensionsKt;
import com.iqonic.store.utils.extensions.NetworkExtensionKt;
import java.util.ArrayList;
import java.util.Iterator;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0016\u0010\u0002\u001a\u0012\u0012\u0004\u0012\u00020\u00040\u0003j\b\u0012\u0004\u0012\u00020\u0004`\u0005H\n¢\u0006\u0002\b\u0006"}, d2 = {"<anonymous>", "", "it", "Ljava/util/ArrayList;", "Lcom/iqonic/store/models/CountryModel;", "Lkotlin/collections/ArrayList;", "invoke"}, k = 3, mv = {1, 1, 16})
/* compiled from: MyCartFragment.kt */
final class MyCartFragment$fetchShippingMethods$1 extends Lambda implements Function1<ArrayList<CountryModel>, Unit> {
    final /* synthetic */ RequestModel $requestModel;
    final /* synthetic */ MyCartFragment this$0;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    MyCartFragment$fetchShippingMethods$1(MyCartFragment myCartFragment, RequestModel requestModel) {
        super(1);
        this.this$0 = myCartFragment;
        this.$requestModel = requestModel;
    }

    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        invoke((ArrayList<CountryModel>) (ArrayList) obj);
        return Unit.INSTANCE;
    }

    public final void invoke(ArrayList<CountryModel> arrayList) {
        Intrinsics.checkParameterIsNotNull(arrayList, "it");
        Iterator it = arrayList.iterator();
        while (true) {
            boolean z = true;
            if (!it.hasNext()) {
                break;
            }
            CountryModel countryModel = (CountryModel) it.next();
            String name = countryModel.getName();
            Shipping access$getShipping$p = this.this$0.shipping;
            if (Intrinsics.areEqual((Object) name, (Object) access$getShipping$p != null ? access$getShipping$p.getCountry() : null)) {
                this.$requestModel.setCountry_code(countryModel.getCode());
                Shipping access$getShipping$p2 = this.this$0.shipping;
                String state = access$getShipping$p2 != null ? access$getShipping$p2.getState() : null;
                if (state == null) {
                    Intrinsics.throwNpe();
                }
                if (state.length() <= 0) {
                    z = false;
                }
                if (z) {
                    for (State state2 : countryModel.getStates()) {
                        String name2 = state2.getName();
                        Shipping access$getShipping$p3 = this.this$0.shipping;
                        if (Intrinsics.areEqual((Object) name2, (Object) access$getShipping$p3 != null ? access$getShipping$p3.getState() : null)) {
                            this.$requestModel.setState_code(state2.getCode());
                        }
                    }
                }
            }
        }
        RequestModel requestModel = this.$requestModel;
        Shipping access$getShipping$p4 = this.this$0.shipping;
        requestModel.setPostcode(access$getShipping$p4 != null ? access$getShipping$p4.getPostcode() : null);
        NetworkExtensionKt.getRestApiImpl$default((String) null, 1, (Object) null).getShippingMethod(this.$requestModel, new Function1<ShippingModel, Unit>(this) {
            final /* synthetic */ MyCartFragment$fetchShippingMethods$1 this$0;

            {
                this.this$0 = r1;
            }

            public /* bridge */ /* synthetic */ Object invoke(Object obj) {
                invoke((ShippingModel) obj);
                return Unit.INSTANCE;
            }

            public final void invoke(ShippingModel shippingModel) {
                Intrinsics.checkParameterIsNotNull(shippingModel, "shippingModel");
                this.this$0.this$0.hideProgress();
                this.this$0.this$0.shippingMethods.clear();
                if (shippingModel.getMethods() != null) {
                    ArrayList access$getShippingMethods$p = this.this$0.this$0.shippingMethods;
                    ArrayList<Method> methods = shippingModel.getMethods();
                    if (methods == null) {
                        Intrinsics.throwNpe();
                    }
                    access$getShippingMethods$p.addAll(methods);
                }
                this.this$0.this$0.invalidateShippingMethods();
            }
        }, new Function1<String, Unit>(this) {
            final /* synthetic */ MyCartFragment$fetchShippingMethods$1 this$0;

            {
                this.this$0 = r1;
            }

            public /* bridge */ /* synthetic */ Object invoke(Object obj) {
                invoke((String) obj);
                return Unit.INSTANCE;
            }

            public final void invoke(String str) {
                Intrinsics.checkParameterIsNotNull(str, "it");
                FragmentActivity activity = this.this$0.this$0.getActivity();
                if (activity != null) {
                    ExtensionsKt.snackBar$default((AppBaseActivity) activity, str, 0, 2, (Object) null);
                    FragmentActivity activity2 = this.this$0.this$0.getActivity();
                    if (activity2 != null) {
                        ((AppBaseActivity) activity2).showProgress(false);
                        return;
                    }
                    throw new TypeCastException("null cannot be cast to non-null type com.iqonic.store.AppBaseActivity");
                }
                throw new TypeCastException("null cannot be cast to non-null type com.iqonic.store.AppBaseActivity");
            }
        });
    }
}
