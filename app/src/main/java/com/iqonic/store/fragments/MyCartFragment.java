package com.iqonic.store.fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.SpannableString;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.appevents.AppEventsConstants;
import com.google.android.material.button.MaterialButton;
import com.iqonic.store.AppBaseActivity;
import com.iqonic.store.activity.DashBoardActivity;
import com.iqonic.store.activity.WishlistActivity;
import com.iqonic.store.adapter.BaseAdapter;
import com.iqonic.store.models.CartRequestModel;
import com.iqonic.store.models.CartResponse;
import com.iqonic.store.models.Coupons;
import com.iqonic.store.models.Line_items;
import com.iqonic.store.models.Method;
import com.iqonic.store.models.RequestModel;
import com.iqonic.store.models.Shipping;
import com.iqonic.store.utils.Constants;
import com.iqonic.store.utils.extensions.AppExtensionsKt;
import com.iqonic.store.utils.extensions.ExtensionsKt;
import com.iqonic.store.utils.extensions.ExtensionsKt$launchActivity$1;
import com.iqonic.store.utils.extensions.NetworkExtensionKt;
import com.iqonic.store.utils.extensions.StringExtensionsKt;
import com.iqonic.store.utils.extensions.ViewExtensionsKt;
import com.store.proshop.R;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.collections.CollectionsKt;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0006\n\u0002\b\u0003\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0012\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\t\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u0018\u0010\u001d\u001a\u00020\u001e2\u0006\u0010\u001f\u001a\u00020\t2\u0006\u0010 \u001a\u00020\u0018H\u0002J\b\u0010!\u001a\u00020\u001eH\u0003J\b\u0010\"\u001a\u00020\u001eH\u0002J\b\u0010#\u001a\u00020\u0006H\u0002J\b\u0010$\u001a\u00020\u001eH\u0003J\b\u0010%\u001a\u00020\u001eH\u0007J\u0010\u0010&\u001a\u00020\u001e2\u0006\u0010'\u001a\u00020\u0006H\u0002J\b\u0010(\u001a\u00020\u001eH\u0002J\u0010\u0010)\u001a\u00020\u00062\u0006\u0010*\u001a\u00020\u0012H\u0002J\u0010\u0010+\u001a\u00020\u00062\u0006\u0010*\u001a\u00020\u0012H\u0002J\u0018\u0010,\u001a\u00020\u001e2\u0006\u0010\u001f\u001a\u00020\t2\u0006\u0010 \u001a\u00020\u0018H\u0002J\"\u0010-\u001a\u00020\u001e2\u0006\u0010.\u001a\u00020\u00182\u0006\u0010/\u001a\u00020\u00182\b\u00100\u001a\u0004\u0018\u000101H\u0016J\u0012\u00102\u001a\u00020\u001e2\b\u00103\u001a\u0004\u0018\u000104H\u0016J\u0018\u00105\u001a\u00020\u001e2\u0006\u00106\u001a\u0002072\u0006\u00108\u001a\u000209H\u0016J&\u0010:\u001a\u0004\u0018\u00010;2\u0006\u00108\u001a\u00020<2\b\u0010=\u001a\u0004\u0018\u00010>2\b\u00103\u001a\u0004\u0018\u000104H\u0016J\u0010\u0010?\u001a\u00020\u00062\u0006\u0010@\u001a\u00020AH\u0016J\u0018\u0010B\u001a\u00020\u001e2\u0006\u0010C\u001a\u00020\u00182\u0006\u0010\u001f\u001a\u00020\u0012H\u0002J\u001a\u0010D\u001a\u00020\u001e2\u0006\u0010E\u001a\u00020;2\b\u00103\u001a\u0004\u0018\u000104H\u0016J\u0010\u0010F\u001a\u00020\u001e2\u0006\u0010\u001f\u001a\u00020\tH\u0002J\b\u0010G\u001a\u00020\u001eH\u0002J\b\u0010H\u001a\u00020\u001eH\u0002J\u0010\u0010I\u001a\u00020\u001e2\u0006\u0010\u001f\u001a\u00020\tH\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u000e¢\u0006\u0002\n\u0000R\u0014\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\t0\bX\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\n\u001a\u0004\u0018\u00010\u000bX\u000e¢\u0006\u0002\n\u0000R\"\u0010\f\u001a\u0016\u0012\u0004\u0012\u00020\u000e\u0018\u00010\rj\n\u0012\u0004\u0012\u00020\u000e\u0018\u0001`\u000fX\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0004X\u000e¢\u0006\u0002\n\u0000R\u0016\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u00120\b8\u0002X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0014X\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u0014X\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\u0004X\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0017\u001a\u00020\u0018X\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\u0019\u001a\u0004\u0018\u00010\u001aX\u000e¢\u0006\u0002\n\u0000R\u001e\u0010\u001b\u001a\u0012\u0012\u0004\u0012\u00020\u00120\rj\b\u0012\u0004\u0012\u00020\u0012`\u000fX\u000e¢\u0006\u0002\n\u0000R\u001e\u0010\u001c\u001a\u0012\u0012\u0004\u0012\u00020\u00120\rj\b\u0012\u0004\u0012\u00020\u0012`\u000fX\u000e¢\u0006\u0002\n\u0000¨\u0006J"}, d2 = {"Lcom/iqonic/store/fragments/MyCartFragment;", "Lcom/iqonic/store/fragments/BaseFragment;", "()V", "cartItemId", "", "isRemoveCoupons", "", "mCartAdapter", "Lcom/iqonic/store/adapter/BaseAdapter;", "Lcom/iqonic/store/models/CartResponse;", "mCoupons", "Lcom/iqonic/store/models/Coupons;", "mOrderItems", "Ljava/util/ArrayList;", "Lcom/iqonic/store/models/Line_items;", "Lkotlin/collections/ArrayList;", "mShippingCost", "mShippingMethodAdapter", "Lcom/iqonic/store/models/Method;", "mSubTotal", "", "mTotalCount", "mTotalDiscount", "selectedMethod", "", "shipping", "Lcom/iqonic/store/models/Shipping;", "shippingMethods", "shippingMethodsAvailble", "addClick", "", "model", "position", "applyCouponCode", "changeColor", "coupan", "fetchShippingMethods", "invalidateCartLayout", "invalidatePaymentLayout", "b", "invalidateShippingMethods", "isMethodAvailable", "method", "minAmount", "minusClick", "onActivityResult", "requestCode", "resultCode", "data", "Landroid/content/Intent;", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onCreateOptionsMenu", "menu", "Landroid/view/Menu;", "inflater", "Landroid/view/MenuInflater;", "onCreateView", "Landroid/view/View;", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "onOptionsItemSelected", "item", "Landroid/view/MenuItem;", "onShippingMethodChanged", "pos", "onViewCreated", "view", "removeCartItem", "removeCoupon", "removeMultipleCartItem", "updateCartItem", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: MyCartFragment.kt */
public final class MyCartFragment extends BaseFragment {
    private HashMap _$_findViewCache;
    /* access modifiers changed from: private */
    public String cartItemId = "";
    /* access modifiers changed from: private */
    public boolean isRemoveCoupons = true;
    /* access modifiers changed from: private */
    public final BaseAdapter<CartResponse> mCartAdapter = new BaseAdapter<>(R.layout.item_cart, new MyCartFragment$mCartAdapter$1(this));
    /* access modifiers changed from: private */
    public Coupons mCoupons;
    /* access modifiers changed from: private */
    public ArrayList<Line_items> mOrderItems = new ArrayList<>();
    /* access modifiers changed from: private */
    public String mShippingCost = AppEventsConstants.EVENT_PARAM_VALUE_NO;
    /* access modifiers changed from: private */
    public final BaseAdapter<Method> mShippingMethodAdapter = new BaseAdapter<>(R.layout.item_shipping_method, new MyCartFragment$mShippingMethodAdapter$1(this));
    /* access modifiers changed from: private */
    public double mSubTotal;
    /* access modifiers changed from: private */
    public double mTotalCount;
    /* access modifiers changed from: private */
    public String mTotalDiscount = AppEventsConstants.EVENT_PARAM_VALUE_NO;
    /* access modifiers changed from: private */
    public int selectedMethod;
    /* access modifiers changed from: private */
    public Shipping shipping;
    /* access modifiers changed from: private */
    public ArrayList<Method> shippingMethods = new ArrayList<>();
    /* access modifiers changed from: private */
    public ArrayList<Method> shippingMethodsAvailble = new ArrayList<>();

    public void _$_clearFindViewByIdCache() {
        HashMap hashMap = this._$_findViewCache;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    public View _$_findCachedViewById(int i) {
        if (this._$_findViewCache == null) {
            this._$_findViewCache = new HashMap();
        }
        View view = (View) this._$_findViewCache.get(Integer.valueOf(i));
        if (view != null) {
            return view;
        }
        View view2 = getView();
        if (view2 == null) {
            return null;
        }
        View findViewById = view2.findViewById(i);
        this._$_findViewCache.put(Integer.valueOf(i), findViewById);
        return findViewById;
    }

    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        _$_clearFindViewByIdCache();
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setHasOptionsMenu(true);
    }

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Intrinsics.checkParameterIsNotNull(layoutInflater, "inflater");
        return layoutInflater.inflate(R.layout.fragment_cart, viewGroup, false);
    }

    /* access modifiers changed from: private */
    public final void addClick(CartResponse cartResponse, int i) {
        this.mCartAdapter.getItems().get(i).setQuantity(String.valueOf(Integer.parseInt(cartResponse.getQuantity()) + 1));
        this.mCartAdapter.notifyItemChanged(i);
        removeCoupon();
        CartResponse cartResponse2 = this.mCartAdapter.getItems().get(i);
        Intrinsics.checkExpressionValueIsNotNull(cartResponse2, "mCartAdapter.items[position]");
        updateCartItem(cartResponse2);
    }

    /* access modifiers changed from: private */
    public final void minusClick(CartResponse cartResponse, int i) {
        int parseInt = Integer.parseInt(cartResponse.getQuantity());
        if (parseInt > 1) {
            this.mCartAdapter.getItems().get(i).setQuantity(String.valueOf(parseInt - 1));
            this.mCartAdapter.notifyItemChanged(i);
            removeCoupon();
            CartResponse cartResponse2 = this.mCartAdapter.getItems().get(i);
            Intrinsics.checkExpressionValueIsNotNull(cartResponse2, "mCartAdapter.items[position]");
            updateCartItem(cartResponse2);
        }
    }

    public void onViewCreated(View view, Bundle bundle) {
        Intrinsics.checkParameterIsNotNull(view, "view");
        super.onViewCreated(view, bundle);
        setHasOptionsMenu(true);
        changeColor();
        MaterialButton materialButton = (MaterialButton) _$_findCachedViewById(com.iqonic.store.R.id.tvContinue);
        materialButton.setOnClickListener(new MyCartFragment$onViewCreated$$inlined$onClick$1(materialButton, this));
        TextView textView = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvChange);
        textView.setOnClickListener(new MyCartFragment$onViewCreated$$inlined$onClick$2(textView, this));
        RecyclerView recyclerView = (RecyclerView) _$_findCachedViewById(com.iqonic.store.R.id.rvCart);
        Intrinsics.checkExpressionValueIsNotNull(recyclerView, "rvCart");
        ExtensionsKt.setVerticalLayout$default(recyclerView, false, 1, (Object) null);
        RecyclerView recyclerView2 = (RecyclerView) _$_findCachedViewById(com.iqonic.store.R.id.rvCart);
        Intrinsics.checkExpressionValueIsNotNull(recyclerView2, "rvCart");
        recyclerView2.setAdapter(this.mCartAdapter);
        RecyclerView recyclerView3 = (RecyclerView) _$_findCachedViewById(com.iqonic.store.R.id.rvShippingMethod);
        Intrinsics.checkExpressionValueIsNotNull(recyclerView3, "rvShippingMethod");
        ExtensionsKt.setVerticalLayout$default(recyclerView3, false, 1, (Object) null);
        RecyclerView recyclerView4 = (RecyclerView) _$_findCachedViewById(com.iqonic.store.R.id.rvShippingMethod);
        Intrinsics.checkExpressionValueIsNotNull(recyclerView4, "rvShippingMethod");
        recyclerView4.setAdapter(this.mShippingMethodAdapter);
        this.mShippingMethodAdapter.setOnItemClick(new MyCartFragment$onViewCreated$3(this));
        MaterialButton materialButton2 = (MaterialButton) _$_findCachedViewById(com.iqonic.store.R.id.btnShopNow);
        materialButton2.setOnClickListener(new MyCartFragment$onViewCreated$$inlined$onClick$3(materialButton2, this));
        if (AppExtensionsKt.getSharedPrefInstance().getBooleanValue(Constants.SharedPref.ENABLE_COUPONS, false)) {
            RelativeLayout relativeLayout = (RelativeLayout) _$_findCachedViewById(com.iqonic.store.R.id.rlCoupon);
            Intrinsics.checkExpressionValueIsNotNull(relativeLayout, "rlCoupon");
            relativeLayout.setVisibility(0);
        } else {
            RelativeLayout relativeLayout2 = (RelativeLayout) _$_findCachedViewById(com.iqonic.store.R.id.rlCoupon);
            Intrinsics.checkExpressionValueIsNotNull(relativeLayout2, "rlCoupon");
            relativeLayout2.setVisibility(8);
        }
        invalidateCartLayout();
    }

    /* access modifiers changed from: private */
    public final void onShippingMethodChanged(int i, Method method) {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.runOnUiThread(new MyCartFragment$onShippingMethodChanged$1(this, i, method));
        }
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater menuInflater) {
        Intrinsics.checkParameterIsNotNull(menu, "menu");
        Intrinsics.checkParameterIsNotNull(menuInflater, "inflater");
        menu.clear();
        menuInflater.inflate(R.menu.menu_cart, menu);
        MenuItem item = menu.getItem(0);
        SpannableString spannableString = new SpannableString(getString(R.string.lbl_wish_list));
        Intrinsics.checkExpressionValueIsNotNull(item, "item");
        item.setTitle(spannableString);
        super.onCreateOptionsMenu(menu, menuInflater);
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        Intrinsics.checkParameterIsNotNull(menuItem, "item");
        if (menuItem.getItemId() != R.id.action_wishlist) {
            return super.onOptionsItemSelected(menuItem);
        }
        if (getActivity() instanceof DashBoardActivity) {
            FragmentActivity activity = getActivity();
            if (activity != null) {
                ((DashBoardActivity) activity).loadWishlistFragment();
            } else {
                throw new TypeCastException("null cannot be cast to non-null type com.iqonic.store.activity.DashBoardActivity");
            }
        } else {
            FragmentActivity activity2 = getActivity();
            if (activity2 != null) {
                Activity activity3 = activity2;
                Bundle bundle = null;
                Intent intent = new Intent(activity3, WishlistActivity.class);
                ExtensionsKt$launchActivity$1.INSTANCE.invoke(intent);
                if (Build.VERSION.SDK_INT >= 16) {
                    activity3.startActivityForResult(intent, Constants.RequestCode.WISHLIST, bundle);
                } else {
                    activity3.startActivityForResult(intent, Constants.RequestCode.WISHLIST);
                }
            }
        }
        return true;
    }

    /* access modifiers changed from: private */
    public final void invalidatePaymentLayout(boolean z) {
        if (getActivity() == null) {
            return;
        }
        if (!z) {
            LinearLayout linearLayout = (LinearLayout) _$_findCachedViewById(com.iqonic.store.R.id.lay_button);
            Intrinsics.checkExpressionValueIsNotNull(linearLayout, "lay_button");
            ViewExtensionsKt.hide(linearLayout);
            RecyclerView recyclerView = (RecyclerView) _$_findCachedViewById(com.iqonic.store.R.id.rvCart);
            Intrinsics.checkExpressionValueIsNotNull(recyclerView, "rvCart");
            ViewExtensionsKt.hide(recyclerView);
            LinearLayout linearLayout2 = (LinearLayout) _$_findCachedViewById(com.iqonic.store.R.id.llShipping);
            Intrinsics.checkExpressionValueIsNotNull(linearLayout2, "llShipping");
            ViewExtensionsKt.hide(linearLayout2);
            return;
        }
        LinearLayout linearLayout3 = (LinearLayout) _$_findCachedViewById(com.iqonic.store.R.id.lay_button);
        Intrinsics.checkExpressionValueIsNotNull(linearLayout3, "lay_button");
        ViewExtensionsKt.show(linearLayout3);
        LinearLayout linearLayout4 = (LinearLayout) _$_findCachedViewById(com.iqonic.store.R.id.llShipping);
        Intrinsics.checkExpressionValueIsNotNull(linearLayout4, "llShipping");
        ViewExtensionsKt.show(linearLayout4);
        RecyclerView recyclerView2 = (RecyclerView) _$_findCachedViewById(com.iqonic.store.R.id.rvCart);
        Intrinsics.checkExpressionValueIsNotNull(recyclerView2, "rvCart");
        ViewExtensionsKt.show(recyclerView2);
    }

    private final void removeMultipleCartItem() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            ((AppBaseActivity) activity).showProgress(true);
            CartRequestModel cartRequestModel = new CartRequestModel();
            cartRequestModel.setMultpleId(this.cartItemId);
            FragmentActivity activity2 = getActivity();
            if (activity2 != null) {
                NetworkExtensionKt.removeMultipleCartItem((AppBaseActivity) activity2, cartRequestModel, new MyCartFragment$removeMultipleCartItem$1(this));
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type com.iqonic.store.AppBaseActivity");
        }
        throw new TypeCastException("null cannot be cast to non-null type com.iqonic.store.AppBaseActivity");
    }

    /* access modifiers changed from: private */
    public final void removeCartItem(CartResponse cartResponse) {
        RequestModel requestModel = new RequestModel();
        requestModel.setPro_id(Integer.valueOf(Integer.parseInt(cartResponse.getPro_id())));
        FragmentActivity activity = getActivity();
        if (activity != null) {
            NetworkExtensionKt.removeCartItem((AppBaseActivity) activity, requestModel, new MyCartFragment$removeCartItem$1(this));
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.iqonic.store.AppBaseActivity");
    }

    public final void invalidateCartLayout() {
        if (ExtensionsKt.isNetworkAvailable()) {
            FragmentActivity activity = getActivity();
            if (activity != null) {
                ((AppBaseActivity) activity).showProgress(true);
                NetworkExtensionKt.getRestApiImpl$default((String) null, 1, (Object) null).getCart(new MyCartFragment$invalidateCartLayout$1(this), new MyCartFragment$invalidateCartLayout$2(this));
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type com.iqonic.store.AppBaseActivity");
        } else if (getActivity() != null) {
            FragmentActivity activity2 = getActivity();
            if (activity2 != null) {
                ExtensionsKt.noInternetSnackBar((AppBaseActivity) activity2);
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type com.iqonic.store.AppBaseActivity");
        }
    }

    /* access modifiers changed from: private */
    public final void fetchShippingMethods() {
        RequestModel requestModel = new RequestModel();
        if (this.shipping == null) {
            this.shipping = AppExtensionsKt.getShippingList();
        }
        TextView textView = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvAddress);
        Intrinsics.checkExpressionValueIsNotNull(textView, "tvAddress");
        StringBuilder sb = new StringBuilder();
        sb.append('(');
        Shipping shipping2 = this.shipping;
        boolean z = true;
        String str = null;
        sb.append(shipping2 != null ? Shipping.getFullAddress$default(shipping2, (String) null, 1, (Object) null) : null);
        sb.append(')');
        textView.setText(sb.toString());
        Shipping shipping3 = this.shipping;
        if (shipping3 != null) {
            str = shipping3.getCountry();
        }
        if (str == null) {
            Intrinsics.throwNpe();
        }
        if (str.length() <= 0) {
            z = false;
        }
        if (z) {
            AppExtensionsKt.fetchCountry(new MyCartFragment$fetchShippingMethods$1(this, requestModel), new MyCartFragment$fetchShippingMethods$2(this));
            return;
        }
        hideProgress();
        TextView textView2 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvAddress);
        Intrinsics.checkExpressionValueIsNotNull(textView2, "tvAddress");
        textView2.setText("You do not provided shipping address.");
        LinearLayout linearLayout = (LinearLayout) _$_findCachedViewById(com.iqonic.store.R.id.llShippingAmount);
        Intrinsics.checkExpressionValueIsNotNull(linearLayout, "llShippingAmount");
        ViewExtensionsKt.hide(linearLayout);
    }

    /* access modifiers changed from: private */
    public final void invalidateShippingMethods() {
        this.shippingMethodsAvailble.clear();
        this.mShippingMethodAdapter.clearItems();
        Collection collection = this.shippingMethods;
        if (collection == null || collection.isEmpty()) {
            LinearLayout linearLayout = (LinearLayout) _$_findCachedViewById(com.iqonic.store.R.id.llShippingAmount);
            Intrinsics.checkExpressionValueIsNotNull(linearLayout, "llShippingAmount");
            ViewExtensionsKt.hide(linearLayout);
            TextView textView = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvFreeShipping);
            Intrinsics.checkExpressionValueIsNotNull(textView, "tvFreeShipping");
            ViewExtensionsKt.show(textView);
            return;
        }
        LinearLayout linearLayout2 = (LinearLayout) _$_findCachedViewById(com.iqonic.store.R.id.llShippingAmount);
        Intrinsics.checkExpressionValueIsNotNull(linearLayout2, "llShippingAmount");
        ViewExtensionsKt.hide(linearLayout2);
        TextView textView2 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvFreeShipping);
        Intrinsics.checkExpressionValueIsNotNull(textView2, "tvFreeShipping");
        ViewExtensionsKt.hide(textView2);
        int i = 0;
        for (Object next : this.shippingMethods) {
            int i2 = i + 1;
            if (i < 0) {
                CollectionsKt.throwIndexOverflow();
            }
            Method method = (Method) next;
            if (Intrinsics.areEqual((Object) method.getEnabled(), (Object) "yes")) {
                if (!Intrinsics.areEqual((Object) method.getId(), (Object) "free_shipping")) {
                    this.shippingMethodsAvailble.add(method);
                } else if (isMethodAvailable(method)) {
                    this.shippingMethodsAvailble.add(method);
                }
            }
            i = i2;
        }
        this.mShippingMethodAdapter.addItems(this.shippingMethodsAvailble);
        if (this.shippingMethodsAvailble.isEmpty()) {
            TextView textView3 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvFreeShipping);
            Intrinsics.checkExpressionValueIsNotNull(textView3, "tvFreeShipping");
            ViewExtensionsKt.show(textView3);
            return;
        }
        Method method2 = this.shippingMethodsAvailble.get(0);
        Intrinsics.checkExpressionValueIsNotNull(method2, "shippingMethodsAvailble[0]");
        onShippingMethodChanged(0, method2);
    }

    /* JADX WARNING: Removed duplicated region for block: B:20:0x0052 A[ORIG_RETURN, RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final boolean isMethodAvailable(com.iqonic.store.models.Method r5) {
        /*
            r4 = this;
            java.lang.String r0 = r5.getRequires()
            int r1 = r0.hashCode()
            r2 = 0
            r3 = 1
            switch(r1) {
                case -1354573786: goto L_0x0045;
                case -1302894395: goto L_0x0030;
                case -1019066651: goto L_0x0023;
                case 3029889: goto L_0x000e;
                default: goto L_0x000d;
            }
        L_0x000d:
            goto L_0x0052
        L_0x000e:
            java.lang.String r1 = "both"
            boolean r0 = r0.equals(r1)
            if (r0 == 0) goto L_0x0052
            boolean r5 = r4.minAmount(r5)
            if (r5 == 0) goto L_0x0053
            boolean r5 = r4.coupan()
            if (r5 == 0) goto L_0x0053
            goto L_0x0052
        L_0x0023:
            java.lang.String r1 = "min_amount"
            boolean r0 = r0.equals(r1)
            if (r0 == 0) goto L_0x0052
            boolean r2 = r4.minAmount(r5)
            goto L_0x0053
        L_0x0030:
            java.lang.String r1 = "either"
            boolean r0 = r0.equals(r1)
            if (r0 == 0) goto L_0x0052
            boolean r5 = r4.minAmount(r5)
            if (r5 != 0) goto L_0x0052
            boolean r5 = r4.coupan()
            if (r5 == 0) goto L_0x0053
            goto L_0x0052
        L_0x0045:
            java.lang.String r5 = "coupon"
            boolean r5 = r0.equals(r5)
            if (r5 == 0) goto L_0x0052
            boolean r2 = r4.coupan()
            goto L_0x0053
        L_0x0052:
            r2 = 1
        L_0x0053:
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.iqonic.store.fragments.MyCartFragment.isMethodAvailable(com.iqonic.store.models.Method):boolean");
    }

    private final boolean minAmount(Method method) {
        if (Intrinsics.areEqual((Object) method.getIgnoreDiscounts(), (Object) "yes")) {
            if (this.mSubTotal >= Double.parseDouble(method.getMinAmount())) {
                return true;
            }
        } else if (this.mTotalCount >= Double.parseDouble(method.getMinAmount())) {
            return true;
        }
        return false;
    }

    private final boolean coupan() {
        Coupons coupons;
        if (!this.isRemoveCoupons && (coupons = this.mCoupons) != null) {
            Boolean valueOf = coupons != null ? Boolean.valueOf(coupons.getFree_shipping()) : null;
            if (valueOf == null) {
                Intrinsics.throwNpe();
            }
            if (valueOf.booleanValue()) {
                return true;
            }
        }
        return false;
    }

    private final void updateCartItem(CartResponse cartResponse) {
        if (getActivity() != null) {
            FragmentActivity activity = getActivity();
            if (activity != null) {
                ((AppBaseActivity) activity).showProgress(true);
                if (ExtensionsKt.isNetworkAvailable()) {
                    if (getActivity() != null) {
                        FragmentActivity activity2 = getActivity();
                        if (activity2 != null) {
                            ((AppBaseActivity) activity2).showProgress(true);
                            RequestModel requestModel = new RequestModel();
                            requestModel.setPro_id(Integer.valueOf(Integer.parseInt(cartResponse.getPro_id())));
                            requestModel.setCartid(Integer.valueOf(Integer.parseInt(cartResponse.getCart_id())));
                            requestModel.setQuantity(Integer.valueOf(Integer.parseInt(cartResponse.getQuantity())));
                            NetworkExtensionKt.getRestApiImpl$default((String) null, 1, (Object) null).updateItemInCart(requestModel, new MyCartFragment$updateCartItem$1(this), new MyCartFragment$updateCartItem$2(this));
                            return;
                        }
                        throw new TypeCastException("null cannot be cast to non-null type com.iqonic.store.AppBaseActivity");
                    }
                } else if (getActivity() != null) {
                    FragmentActivity activity3 = getActivity();
                    if (activity3 != null) {
                        ((AppBaseActivity) activity3).showProgress(false);
                        FragmentActivity activity4 = getActivity();
                        if (activity4 != null) {
                            ExtensionsKt.noInternetSnackBar((AppBaseActivity) activity4);
                            return;
                        }
                        throw new TypeCastException("null cannot be cast to non-null type com.iqonic.store.AppBaseActivity");
                    }
                    throw new TypeCastException("null cannot be cast to non-null type com.iqonic.store.AppBaseActivity");
                }
            } else {
                throw new TypeCastException("null cannot be cast to non-null type com.iqonic.store.AppBaseActivity");
            }
        }
    }

    public void onActivityResult(int i, int i2, Intent intent) {
        if (i2 != -1) {
            return;
        }
        if (i == 202) {
            if (intent == null) {
                Intrinsics.throwNpe();
            }
            Serializable serializableExtra = intent.getSerializableExtra("couponData");
            if (serializableExtra != null) {
                this.mCoupons = (Coupons) serializableExtra;
                this.isRemoveCoupons = false;
                applyCouponCode();
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type com.iqonic.store.models.Coupons");
        } else if (i == 205) {
            removeMultipleCartItem();
        } else if (i == 209) {
            this.shipping = null;
            fetchShippingMethods();
        } else if (i != 210) {
            FragmentActivity activity = getActivity();
            if (activity != null) {
                activity.finish();
            }
        } else {
            invalidateCartLayout();
        }
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Removed duplicated region for block: B:148:0x0431  */
    /* JADX WARNING: Removed duplicated region for block: B:149:0x0448  */
    /* JADX WARNING: Removed duplicated region for block: B:152:0x04bd  */
    /* JADX WARNING: Removed duplicated region for block: B:155:0x04c6  */
    /* JADX WARNING: Removed duplicated region for block: B:156:0x050a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void applyCouponCode() {
        /*
            r22 = this;
            r0 = r22
            com.iqonic.store.models.Coupons r1 = r0.mCoupons
            if (r1 == 0) goto L_0x0512
            java.lang.String r2 = "0"
            r0.mTotalDiscount = r2
            if (r1 != 0) goto L_0x000f
            kotlin.jvm.internal.Intrinsics.throwNpe()
        L_0x000f:
            java.lang.String r1 = r1.getMinimum_amount()
            float r1 = java.lang.Float.parseFloat(r1)
            double r1 = (double) r1
            java.lang.String r3 = "fixed_product"
            java.lang.String r4 = " and below."
            java.lang.String r5 = "fixed_cart"
            r6 = 2131820853(0x7f110135, float:1.9274433E38)
            r7 = 0
            r9 = 0
            java.lang.String r10 = "txtApplyCouponCode"
            r11 = 1
            r12 = 0
            int r13 = (r1 > r7 ? 1 : (r1 == r7 ? 0 : -1))
            if (r13 <= 0) goto L_0x00df
            double r1 = r0.mTotalCount
            com.iqonic.store.models.Coupons r13 = r0.mCoupons
            if (r13 != 0) goto L_0x0035
            kotlin.jvm.internal.Intrinsics.throwNpe()
        L_0x0035:
            java.lang.String r13 = r13.getMinimum_amount()
            float r13 = java.lang.Float.parseFloat(r13)
            double r13 = (double) r13
            int r15 = (r1 > r13 ? 1 : (r1 == r13 ? 0 : -1))
            if (r15 >= 0) goto L_0x007f
            int r1 = com.iqonic.store.R.id.txtApplyCouponCode
            android.view.View r1 = r0._$_findCachedViewById(r1)
            android.widget.TextView r1 = (android.widget.TextView) r1
            kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r1, r10)
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = r0.getString(r6)
            r2.append(r3)
            com.iqonic.store.models.Coupons r3 = r0.mCoupons
            if (r3 != 0) goto L_0x0060
            kotlin.jvm.internal.Intrinsics.throwNpe()
        L_0x0060:
            java.lang.String r3 = r3.getMinimum_amount()
            java.lang.String r3 = com.iqonic.store.utils.extensions.StringExtensionsKt.currencyFormat$default(r3, r12, r11, r12)
            r2.append(r3)
            r3 = 2131820854(0x7f110136, float:1.9274435E38)
            java.lang.String r3 = r0.getString(r3)
            r2.append(r3)
            java.lang.String r2 = r2.toString()
            java.lang.CharSequence r2 = (java.lang.CharSequence) r2
            r1.setText(r2)
            return
        L_0x007f:
            com.iqonic.store.models.Coupons r1 = r0.mCoupons
            if (r1 != 0) goto L_0x0086
            kotlin.jvm.internal.Intrinsics.throwNpe()
        L_0x0086:
            java.lang.String r1 = r1.getMaximum_amount()
            float r1 = java.lang.Float.parseFloat(r1)
            double r1 = (double) r1
            int r13 = (r1 > r7 ? 1 : (r1 == r7 ? 0 : -1))
            if (r13 <= 0) goto L_0x0284
            double r1 = r0.mTotalCount
            com.iqonic.store.models.Coupons r13 = r0.mCoupons
            if (r13 != 0) goto L_0x009c
            kotlin.jvm.internal.Intrinsics.throwNpe()
        L_0x009c:
            java.lang.String r13 = r13.getMaximum_amount()
            float r13 = java.lang.Float.parseFloat(r13)
            double r13 = (double) r13
            int r15 = (r1 > r13 ? 1 : (r1 == r13 ? 0 : -1))
            if (r15 <= 0) goto L_0x0284
            int r1 = com.iqonic.store.R.id.txtApplyCouponCode
            android.view.View r1 = r0._$_findCachedViewById(r1)
            android.widget.TextView r1 = (android.widget.TextView) r1
            kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r1, r10)
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = r0.getString(r6)
            r2.append(r3)
            com.iqonic.store.models.Coupons r3 = r0.mCoupons
            if (r3 != 0) goto L_0x00c7
            kotlin.jvm.internal.Intrinsics.throwNpe()
        L_0x00c7:
            java.lang.String r3 = r3.getMaximum_amount()
            java.lang.String r3 = com.iqonic.store.utils.extensions.StringExtensionsKt.currencyFormat$default(r3, r12, r11, r12)
            r2.append(r3)
            r2.append(r4)
            java.lang.String r2 = r2.toString()
            java.lang.CharSequence r2 = (java.lang.CharSequence) r2
            r1.setText(r2)
            return
        L_0x00df:
            com.iqonic.store.models.Coupons r1 = r0.mCoupons
            if (r1 != 0) goto L_0x00e6
            kotlin.jvm.internal.Intrinsics.throwNpe()
        L_0x00e6:
            java.lang.String r1 = r1.getMaximum_amount()
            float r1 = java.lang.Float.parseFloat(r1)
            double r1 = (double) r1
            int r13 = (r1 > r7 ? 1 : (r1 == r7 ? 0 : -1))
            if (r13 <= 0) goto L_0x013f
            double r1 = r0.mTotalCount
            com.iqonic.store.models.Coupons r13 = r0.mCoupons
            if (r13 != 0) goto L_0x00fc
            kotlin.jvm.internal.Intrinsics.throwNpe()
        L_0x00fc:
            java.lang.String r13 = r13.getMaximum_amount()
            float r13 = java.lang.Float.parseFloat(r13)
            double r13 = (double) r13
            int r15 = (r1 > r13 ? 1 : (r1 == r13 ? 0 : -1))
            if (r15 <= 0) goto L_0x0284
            int r1 = com.iqonic.store.R.id.txtApplyCouponCode
            android.view.View r1 = r0._$_findCachedViewById(r1)
            android.widget.TextView r1 = (android.widget.TextView) r1
            kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r1, r10)
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = r0.getString(r6)
            r2.append(r3)
            com.iqonic.store.models.Coupons r3 = r0.mCoupons
            if (r3 != 0) goto L_0x0127
            kotlin.jvm.internal.Intrinsics.throwNpe()
        L_0x0127:
            java.lang.String r3 = r3.getMaximum_amount()
            java.lang.String r3 = com.iqonic.store.utils.extensions.StringExtensionsKt.currencyFormat$default(r3, r12, r11, r12)
            r2.append(r3)
            r2.append(r4)
            java.lang.String r2 = r2.toString()
            java.lang.CharSequence r2 = (java.lang.CharSequence) r2
            r1.setText(r2)
            return
        L_0x013f:
            com.iqonic.store.models.Coupons r1 = r0.mCoupons
            if (r1 == 0) goto L_0x0148
            java.lang.String r1 = r1.getDiscount_type()
            goto L_0x0149
        L_0x0148:
            r1 = r12
        L_0x0149:
            boolean r1 = kotlin.jvm.internal.Intrinsics.areEqual((java.lang.Object) r1, (java.lang.Object) r5)
            java.lang.String r2 = " and above. Try other coupon."
            r4 = 55
            if (r1 == 0) goto L_0x019f
            double r13 = r0.mTotalCount
            com.iqonic.store.models.Coupons r1 = r0.mCoupons
            if (r1 != 0) goto L_0x015c
            kotlin.jvm.internal.Intrinsics.throwNpe()
        L_0x015c:
            java.lang.String r1 = r1.getAmount()
            float r1 = java.lang.Float.parseFloat(r1)
            double r7 = (double) r1
            int r1 = (r13 > r7 ? 1 : (r13 == r7 ? 0 : -1))
            if (r1 >= 0) goto L_0x0284
            int r1 = com.iqonic.store.R.id.txtApplyCouponCode
            android.view.View r1 = r0._$_findCachedViewById(r1)
            android.widget.TextView r1 = (android.widget.TextView) r1
            kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r1, r10)
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r5 = "Coupon is valid only order of "
            r3.append(r5)
            com.iqonic.store.models.Coupons r5 = r0.mCoupons
            if (r5 != 0) goto L_0x0185
            kotlin.jvm.internal.Intrinsics.throwNpe()
        L_0x0185:
            java.lang.String r5 = r5.getAmount()
            float r5 = java.lang.Float.parseFloat(r5)
            float r4 = (float) r4
            float r5 = r5 + r4
            r3.append(r5)
            r3.append(r2)
            java.lang.String r2 = r3.toString()
            java.lang.CharSequence r2 = (java.lang.CharSequence) r2
            r1.setText(r2)
            return
        L_0x019f:
            com.iqonic.store.models.Coupons r1 = r0.mCoupons
            if (r1 == 0) goto L_0x01a8
            java.lang.String r1 = r1.getDiscount_type()
            goto L_0x01a9
        L_0x01a8:
            r1 = r12
        L_0x01a9:
            boolean r1 = kotlin.jvm.internal.Intrinsics.areEqual((java.lang.Object) r1, (java.lang.Object) r3)
            if (r1 == 0) goto L_0x0284
            com.iqonic.store.adapter.BaseAdapter<com.iqonic.store.models.CartResponse> r1 = r0.mCartAdapter
            java.util.ArrayList r1 = r1.getModel()
            int r1 = r1.size()
            r6 = 0
            r7 = 1
        L_0x01bb:
            if (r6 >= r1) goto L_0x024c
            com.iqonic.store.adapter.BaseAdapter<com.iqonic.store.models.CartResponse> r8 = r0.mCartAdapter
            java.util.ArrayList r8 = r8.getModel()
            java.lang.Object r8 = r8.get(r6)
            com.iqonic.store.models.CartResponse r8 = (com.iqonic.store.models.CartResponse) r8
            java.lang.String r8 = r8.getSale_price()
            java.lang.CharSequence r8 = (java.lang.CharSequence) r8
            int r8 = r8.length()
            if (r8 <= 0) goto L_0x01d7
            r8 = 1
            goto L_0x01d8
        L_0x01d7:
            r8 = 0
        L_0x01d8:
            if (r8 == 0) goto L_0x0203
            com.iqonic.store.adapter.BaseAdapter<com.iqonic.store.models.CartResponse> r8 = r0.mCartAdapter
            java.util.ArrayList r8 = r8.getModel()
            java.lang.Object r8 = r8.get(r6)
            com.iqonic.store.models.CartResponse r8 = (com.iqonic.store.models.CartResponse) r8
            java.lang.String r8 = r8.getSale_price()
            float r8 = java.lang.Float.parseFloat(r8)
            com.iqonic.store.models.Coupons r13 = r0.mCoupons
            if (r13 != 0) goto L_0x01f5
            kotlin.jvm.internal.Intrinsics.throwNpe()
        L_0x01f5:
            java.lang.String r13 = r13.getAmount()
            float r13 = java.lang.Float.parseFloat(r13)
            int r8 = (r8 > r13 ? 1 : (r8 == r13 ? 0 : -1))
            if (r8 >= 0) goto L_0x0248
            r7 = 0
            goto L_0x024c
        L_0x0203:
            com.iqonic.store.adapter.BaseAdapter<com.iqonic.store.models.CartResponse> r8 = r0.mCartAdapter
            java.util.ArrayList r8 = r8.getModel()
            java.lang.Object r8 = r8.get(r6)
            com.iqonic.store.models.CartResponse r8 = (com.iqonic.store.models.CartResponse) r8
            java.lang.String r8 = r8.getPrice()
            java.lang.CharSequence r8 = (java.lang.CharSequence) r8
            int r8 = r8.length()
            if (r8 <= 0) goto L_0x021d
            r8 = 1
            goto L_0x021e
        L_0x021d:
            r8 = 0
        L_0x021e:
            if (r8 == 0) goto L_0x0248
            com.iqonic.store.adapter.BaseAdapter<com.iqonic.store.models.CartResponse> r8 = r0.mCartAdapter
            java.util.ArrayList r8 = r8.getModel()
            java.lang.Object r8 = r8.get(r6)
            com.iqonic.store.models.CartResponse r8 = (com.iqonic.store.models.CartResponse) r8
            java.lang.String r8 = r8.getPrice()
            float r8 = java.lang.Float.parseFloat(r8)
            com.iqonic.store.models.Coupons r13 = r0.mCoupons
            if (r13 != 0) goto L_0x023b
            kotlin.jvm.internal.Intrinsics.throwNpe()
        L_0x023b:
            java.lang.String r13 = r13.getAmount()
            float r13 = java.lang.Float.parseFloat(r13)
            int r8 = (r8 > r13 ? 1 : (r8 == r13 ? 0 : -1))
            if (r8 >= 0) goto L_0x0248
            r7 = 0
        L_0x0248:
            int r6 = r6 + 1
            goto L_0x01bb
        L_0x024c:
            if (r7 != 0) goto L_0x0284
            int r1 = com.iqonic.store.R.id.txtApplyCouponCode
            android.view.View r1 = r0._$_findCachedViewById(r1)
            android.widget.TextView r1 = (android.widget.TextView) r1
            kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r1, r10)
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r5 = "Coupon is valid only if all product price have "
            r3.append(r5)
            com.iqonic.store.models.Coupons r5 = r0.mCoupons
            if (r5 != 0) goto L_0x026a
            kotlin.jvm.internal.Intrinsics.throwNpe()
        L_0x026a:
            java.lang.String r5 = r5.getAmount()
            float r5 = java.lang.Float.parseFloat(r5)
            float r4 = (float) r4
            float r5 = r5 + r4
            r3.append(r5)
            r3.append(r2)
            java.lang.String r2 = r3.toString()
            java.lang.CharSequence r2 = (java.lang.CharSequence) r2
            r1.setText(r2)
            return
        L_0x0284:
            com.iqonic.store.models.Coupons r1 = r0.mCoupons
            if (r1 == 0) goto L_0x028d
            java.lang.String r1 = r1.getDiscount_type()
            goto L_0x028e
        L_0x028d:
            r1 = r12
        L_0x028e:
            r2 = 2131820885(0x7f110155, float:1.9274498E38)
            java.lang.String r4 = ")"
            java.lang.String r6 = " ("
            r7 = 2131820865(0x7f110141, float:1.9274457E38)
            java.lang.String r8 = "txtDiscountlbl"
            if (r1 != 0) goto L_0x029e
            goto L_0x03cd
        L_0x029e:
            int r13 = r1.hashCode()
            r14 = -1055414492(0xffffffffc117a724, float:-9.478306)
            if (r13 == r14) goto L_0x0374
            r3 = -678927291(0xffffffffd7886445, float:-2.99928471E14)
            r9 = 2131820923(0x7f11017b, float:1.9274575E38)
            if (r13 == r3) goto L_0x0311
            r3 = 1707398411(0x65c4d50b, float:1.1618922E23)
            if (r13 == r3) goto L_0x02b6
            goto L_0x03cd
        L_0x02b6:
            boolean r1 = r1.equals(r5)
            if (r1 == 0) goto L_0x03cd
            com.iqonic.store.models.Coupons r1 = r0.mCoupons
            if (r1 != 0) goto L_0x02c3
            kotlin.jvm.internal.Intrinsics.throwNpe()
        L_0x02c3:
            java.lang.String r1 = r1.getAmount()
            r0.mTotalDiscount = r1
            int r1 = com.iqonic.store.R.id.txtDiscountlbl
            android.view.View r1 = r0._$_findCachedViewById(r1)
            android.widget.TextView r1 = (android.widget.TextView) r1
            kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r1, r8)
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r5 = r0.getString(r7)
            r3.append(r5)
            r3.append(r6)
            java.lang.String r2 = r0.getString(r2)
            r3.append(r2)
            com.iqonic.store.models.Coupons r2 = r0.mCoupons
            if (r2 != 0) goto L_0x02f1
            kotlin.jvm.internal.Intrinsics.throwNpe()
        L_0x02f1:
            java.lang.String r2 = r2.getAmount()
            java.lang.String r2 = com.iqonic.store.utils.extensions.StringExtensionsKt.currencyFormat$default(r2, r12, r11, r12)
            r3.append(r2)
            java.lang.String r2 = r0.getString(r9)
            r3.append(r2)
            r3.append(r4)
            java.lang.String r2 = r3.toString()
            java.lang.CharSequence r2 = (java.lang.CharSequence) r2
            r1.setText(r2)
            goto L_0x0423
        L_0x0311:
            java.lang.String r3 = "percent"
            boolean r1 = r1.equals(r3)
            if (r1 == 0) goto L_0x03cd
            double r1 = r0.mTotalCount
            float r1 = (float) r1
            com.iqonic.store.models.Coupons r2 = r0.mCoupons
            if (r2 != 0) goto L_0x0323
            kotlin.jvm.internal.Intrinsics.throwNpe()
        L_0x0323:
            java.lang.String r2 = r2.getAmount()
            float r2 = java.lang.Float.parseFloat(r2)
            float r1 = r1 * r2
            r2 = 100
            float r2 = (float) r2
            float r1 = r1 / r2
            java.lang.String r1 = java.lang.String.valueOf(r1)
            r0.mTotalDiscount = r1
            int r1 = com.iqonic.store.R.id.txtDiscountlbl
            android.view.View r1 = r0._$_findCachedViewById(r1)
            android.widget.TextView r1 = (android.widget.TextView) r1
            kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r1, r8)
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = r0.getString(r7)
            r2.append(r3)
            r2.append(r6)
            com.iqonic.store.models.Coupons r3 = r0.mCoupons
            if (r3 != 0) goto L_0x0358
            kotlin.jvm.internal.Intrinsics.throwNpe()
        L_0x0358:
            java.lang.String r3 = r3.getAmount()
            r2.append(r3)
            java.lang.String r3 = r0.getString(r9)
            r2.append(r3)
            r2.append(r4)
            java.lang.String r2 = r2.toString()
            java.lang.CharSequence r2 = (java.lang.CharSequence) r2
            r1.setText(r2)
            goto L_0x0423
        L_0x0374:
            boolean r1 = r1.equals(r3)
            if (r1 == 0) goto L_0x03cd
            com.iqonic.store.models.Coupons r1 = r0.mCoupons
            if (r1 != 0) goto L_0x0381
            kotlin.jvm.internal.Intrinsics.throwNpe()
        L_0x0381:
            java.lang.String r1 = r1.getAmount()
            r16 = r1
            java.lang.CharSequence r16 = (java.lang.CharSequence) r16
            java.lang.String r1 = "."
            java.lang.String[] r17 = new java.lang.String[]{r1}
            r18 = 0
            r19 = 0
            r20 = 6
            r21 = 0
            java.util.List r1 = kotlin.text.StringsKt.split$default((java.lang.CharSequence) r16, (java.lang.String[]) r17, (boolean) r18, (int) r19, (int) r20, (java.lang.Object) r21)
            java.lang.Object r1 = r1.get(r9)
            java.lang.String r1 = (java.lang.String) r1
            int r1 = java.lang.Integer.parseInt(r1)
            java.util.ArrayList<com.iqonic.store.models.Line_items> r2 = r0.mOrderItems
            if (r2 != 0) goto L_0x03ac
            kotlin.jvm.internal.Intrinsics.throwNpe()
        L_0x03ac:
            int r2 = r2.size()
            int r1 = r1 * r2
            java.lang.String r1 = java.lang.String.valueOf(r1)
            r0.mTotalDiscount = r1
            int r1 = com.iqonic.store.R.id.txtDiscountlbl
            android.view.View r1 = r0._$_findCachedViewById(r1)
            android.widget.TextView r1 = (android.widget.TextView) r1
            kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r1, r8)
            java.lang.String r2 = r0.getString(r7)
            java.lang.CharSequence r2 = (java.lang.CharSequence) r2
            r1.setText(r2)
            goto L_0x0423
        L_0x03cd:
            com.iqonic.store.models.Coupons r1 = r0.mCoupons
            if (r1 != 0) goto L_0x03d4
            kotlin.jvm.internal.Intrinsics.throwNpe()
        L_0x03d4:
            java.lang.String r1 = r1.getAmount()
            r0.mTotalDiscount = r1
            int r1 = com.iqonic.store.R.id.txtDiscountlbl
            android.view.View r1 = r0._$_findCachedViewById(r1)
            android.widget.TextView r1 = (android.widget.TextView) r1
            kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r1, r8)
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r5 = r0.getString(r7)
            r3.append(r5)
            r3.append(r6)
            java.lang.String r2 = r0.getString(r2)
            r3.append(r2)
            com.iqonic.store.models.Coupons r2 = r0.mCoupons
            if (r2 != 0) goto L_0x0402
            kotlin.jvm.internal.Intrinsics.throwNpe()
        L_0x0402:
            java.lang.String r2 = r2.getAmount()
            java.lang.String r2 = com.iqonic.store.utils.extensions.StringExtensionsKt.currencyFormat$default(r2, r12, r11, r12)
            r3.append(r2)
            r2 = 2131820924(0x7f11017c, float:1.9274577E38)
            java.lang.String r2 = r0.getString(r2)
            r3.append(r2)
            r3.append(r4)
            java.lang.String r2 = r3.toString()
            java.lang.CharSequence r2 = (java.lang.CharSequence) r2
            r1.setText(r2)
        L_0x0423:
            java.lang.String r1 = r0.mTotalDiscount
            double r1 = java.lang.Double.parseDouble(r1)
            java.lang.String r3 = "tvDiscount"
            r4 = 0
            int r6 = (r1 > r4 ? 1 : (r1 == r4 ? 0 : -1))
            if (r6 != 0) goto L_0x0448
            int r1 = com.iqonic.store.R.id.tvDiscount
            android.view.View r1 = r0._$_findCachedViewById(r1)
            android.widget.TextView r1 = (android.widget.TextView) r1
            kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r1, r3)
            java.lang.String r2 = r0.mTotalDiscount
            java.lang.String r2 = com.iqonic.store.utils.extensions.StringExtensionsKt.currencyFormat$default(r2, r12, r11, r12)
            java.lang.CharSequence r2 = (java.lang.CharSequence) r2
            r1.setText(r2)
            goto L_0x046f
        L_0x0448:
            int r1 = com.iqonic.store.R.id.tvDiscount
            android.view.View r1 = r0._$_findCachedViewById(r1)
            android.widget.TextView r1 = (android.widget.TextView) r1
            kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r1, r3)
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "-"
            r2.append(r3)
            java.lang.String r3 = r0.mTotalDiscount
            java.lang.String r3 = com.iqonic.store.utils.extensions.StringExtensionsKt.currencyFormat$default(r3, r12, r11, r12)
            r2.append(r3)
            java.lang.String r2 = r2.toString()
            java.lang.CharSequence r2 = (java.lang.CharSequence) r2
            r1.setText(r2)
        L_0x046f:
            double r1 = r0.mTotalCount
            java.lang.String r3 = r0.mTotalDiscount
            float r3 = java.lang.Float.parseFloat(r3)
            double r3 = (double) r3
            double r1 = r1 - r3
            r0.mTotalCount = r1
            int r1 = com.iqonic.store.R.id.tvTotalCartAmount
            android.view.View r1 = r0._$_findCachedViewById(r1)
            android.widget.TextView r1 = (android.widget.TextView) r1
            java.lang.String r2 = "tvTotalCartAmount"
            kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r1, r2)
            double r2 = r0.mTotalCount
            int r2 = (int) r2
            java.lang.String r3 = r0.mShippingCost
            int r3 = java.lang.Integer.parseInt(r3)
            int r2 = r2 + r3
            java.lang.String r2 = java.lang.String.valueOf(r2)
            java.lang.String r2 = com.iqonic.store.utils.extensions.StringExtensionsKt.currencyFormat$default(r2, r12, r11, r12)
            java.lang.CharSequence r2 = (java.lang.CharSequence) r2
            r1.setText(r2)
            int r1 = com.iqonic.store.R.id.txtApplyCouponCode
            android.view.View r1 = r0._$_findCachedViewById(r1)
            android.widget.TextView r1 = (android.widget.TextView) r1
            kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r1, r10)
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            r3 = 2131820826(0x7f11011a, float:1.9274378E38)
            java.lang.String r3 = r0.getString(r3)
            r2.append(r3)
            com.iqonic.store.models.Coupons r3 = r0.mCoupons
            if (r3 != 0) goto L_0x04c0
            kotlin.jvm.internal.Intrinsics.throwNpe()
        L_0x04c0:
            java.lang.String r3 = r3.getCode()
            if (r3 == 0) goto L_0x050a
            java.lang.String r3 = r3.toUpperCase()
            java.lang.String r4 = "(this as java.lang.String).toUpperCase()"
            kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r3, r4)
            r2.append(r3)
            java.lang.String r2 = r2.toString()
            java.lang.CharSequence r2 = (java.lang.CharSequence) r2
            r1.setText(r2)
            int r1 = com.iqonic.store.R.id.tvEditCoupon
            android.view.View r1 = r0._$_findCachedViewById(r1)
            android.widget.TextView r1 = (android.widget.TextView) r1
            java.lang.String r2 = "tvEditCoupon"
            kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r1, r2)
            r2 = 2131820959(0x7f11019f, float:1.9274648E38)
            java.lang.String r2 = r0.getString(r2)
            java.lang.CharSequence r2 = (java.lang.CharSequence) r2
            r1.setText(r2)
            int r1 = com.iqonic.store.R.id.tvEditCoupon
            android.view.View r1 = r0._$_findCachedViewById(r1)
            android.widget.TextView r1 = (android.widget.TextView) r1
            com.iqonic.store.fragments.MyCartFragment$applyCouponCode$$inlined$onClick$1 r2 = new com.iqonic.store.fragments.MyCartFragment$applyCouponCode$$inlined$onClick$1
            r2.<init>(r1, r0)
            android.view.View$OnClickListener r2 = (android.view.View.OnClickListener) r2
            r1.setOnClickListener(r2)
            r22.invalidateShippingMethods()
            goto L_0x0512
        L_0x050a:
            kotlin.TypeCastException r1 = new kotlin.TypeCastException
            java.lang.String r2 = "null cannot be cast to non-null type java.lang.String"
            r1.<init>(r2)
            throw r1
        L_0x0512:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.iqonic.store.fragments.MyCartFragment.applyCouponCode():void");
    }

    /* access modifiers changed from: private */
    public final void removeCoupon() {
        this.isRemoveCoupons = true;
        this.mCoupons = null;
        TextView textView = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.txtDiscountlbl);
        Intrinsics.checkExpressionValueIsNotNull(textView, "txtDiscountlbl");
        textView.setText(getString(R.string.lbl_discount));
        TextView textView2 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.txtApplyCouponCode);
        Intrinsics.checkExpressionValueIsNotNull(textView2, "txtApplyCouponCode");
        textView2.setText(getString(R.string.lbl_coupon_code));
        this.mTotalCount += (double) Float.parseFloat(this.mTotalDiscount);
        TextView textView3 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvEditCoupon);
        Intrinsics.checkExpressionValueIsNotNull(textView3, "tvEditCoupon");
        textView3.setText(getString(R.string.lbl_apply));
        TextView textView4 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvDiscount);
        Intrinsics.checkExpressionValueIsNotNull(textView4, "tvDiscount");
        textView4.setText(StringExtensionsKt.currencyFormat$default(AppEventsConstants.EVENT_PARAM_VALUE_NO, (String) null, 1, (Object) null));
        TextView textView5 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvTotalCartAmount);
        Intrinsics.checkExpressionValueIsNotNull(textView5, "tvTotalCartAmount");
        textView5.setText(StringExtensionsKt.currencyFormat$default(String.valueOf(((int) this.mTotalCount) + Integer.parseInt(this.mShippingCost)), (String) null, 1, (Object) null));
        TextView textView6 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvEditCoupon);
        textView6.setOnClickListener(new MyCartFragment$removeCoupon$$inlined$onClick$1(textView6, this));
        invalidateShippingMethods();
    }

    private final void changeColor() {
        TextView textView = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.lblNoItems);
        Intrinsics.checkExpressionValueIsNotNull(textView, "lblNoItems");
        ExtensionsKt.changeTextPrimaryColor(textView);
        MaterialButton materialButton = (MaterialButton) _$_findCachedViewById(com.iqonic.store.R.id.btnShopNow);
        Intrinsics.checkExpressionValueIsNotNull(materialButton, "btnShopNow");
        ExtensionsKt.changeBackgroundTint(materialButton, AppExtensionsKt.getButtonColor());
        TextView textView2 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.txtApplyCouponCode);
        Intrinsics.checkExpressionValueIsNotNull(textView2, "txtApplyCouponCode");
        ExtensionsKt.changeTextSecondaryColor(textView2);
        TextView textView3 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvEditCoupon);
        Intrinsics.checkExpressionValueIsNotNull(textView3, "tvEditCoupon");
        ExtensionsKt.changePrimaryColor(textView3);
        TextView textView4 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.lblShipping);
        Intrinsics.checkExpressionValueIsNotNull(textView4, "lblShipping");
        ExtensionsKt.changeTextPrimaryColor(textView4);
        TextView textView5 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvChange);
        Intrinsics.checkExpressionValueIsNotNull(textView5, "tvChange");
        ExtensionsKt.changePrimaryColor(textView5);
        TextView textView6 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvAddress);
        Intrinsics.checkExpressionValueIsNotNull(textView6, "tvAddress");
        ExtensionsKt.changeTextSecondaryColor(textView6);
        TextView textView7 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvFreeShipping);
        Intrinsics.checkExpressionValueIsNotNull(textView7, "tvFreeShipping");
        ExtensionsKt.changeTextSecondaryColor(textView7);
        TextView textView8 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.lblSubTotal);
        Intrinsics.checkExpressionValueIsNotNull(textView8, "lblSubTotal");
        ExtensionsKt.changeTextSecondaryColor(textView8);
        TextView textView9 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvTotal);
        Intrinsics.checkExpressionValueIsNotNull(textView9, "tvTotal");
        ExtensionsKt.changeTextSecondaryColor(textView9);
        TextView textView10 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.txtDiscountlbl);
        Intrinsics.checkExpressionValueIsNotNull(textView10, "txtDiscountlbl");
        ExtensionsKt.changeTextSecondaryColor(textView10);
        TextView textView11 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvDiscount);
        Intrinsics.checkExpressionValueIsNotNull(textView11, "tvDiscount");
        ExtensionsKt.changeTextSecondaryColor(textView11);
        TextView textView12 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.txtShippinglbl);
        Intrinsics.checkExpressionValueIsNotNull(textView12, "txtShippinglbl");
        ExtensionsKt.changeTextSecondaryColor(textView12);
        TextView textView13 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvShipping);
        Intrinsics.checkExpressionValueIsNotNull(textView13, "tvShipping");
        ExtensionsKt.changeTextSecondaryColor(textView13);
        TextView textView14 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.lblTotalAmount);
        Intrinsics.checkExpressionValueIsNotNull(textView14, "lblTotalAmount");
        ExtensionsKt.changeTextPrimaryColor(textView14);
        TextView textView15 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvTotalCartAmount);
        Intrinsics.checkExpressionValueIsNotNull(textView15, "tvTotalCartAmount");
        ExtensionsKt.changeTextPrimaryColor(textView15);
        MaterialButton materialButton2 = (MaterialButton) _$_findCachedViewById(com.iqonic.store.R.id.tvContinue);
        Intrinsics.checkExpressionValueIsNotNull(materialButton2, "tvContinue");
        ExtensionsKt.changeBackgroundTint(materialButton2, AppExtensionsKt.getButtonColor());
        RelativeLayout relativeLayout = (RelativeLayout) _$_findCachedViewById(com.iqonic.store.R.id.rlMain);
        Intrinsics.checkExpressionValueIsNotNull(relativeLayout, "rlMain");
        ExtensionsKt.changeBackgroundColor(relativeLayout);
    }
}
