package com.iqonic.store.fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.appevents.AppEventsConstants;
import com.iqonic.store.AppBaseActivity;
import com.iqonic.store.activity.SignInUpActivity;
import com.iqonic.store.adapter.BaseAdapter;
import com.iqonic.store.models.Category;
import com.iqonic.store.models.RequestModel;
import com.iqonic.store.models.SearchRequest;
import com.iqonic.store.models.StoreProductModel;
import com.iqonic.store.utils.Constants;
import com.iqonic.store.utils.extensions.AppExtensionsKt;
import com.iqonic.store.utils.extensions.ExtensionsKt;
import com.iqonic.store.utils.extensions.NetworkExtensionKt;
import com.iqonic.store.utils.extensions.StringExtensionsKt;
import com.iqonic.store.utils.extensions.ViewExtensionsKt;
import com.store.proshop.R;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010%\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010\u0015\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u0000 42\u00020\u0001:\u00014B\u0005¢\u0006\u0002\u0010\u0002J\u0010\u0010 \u001a\u00020!2\u0006\u0010\"\u001a\u00020\u0013H\u0002J\u001c\u0010#\u001a\u00020!2\u0012\u0010\u0005\u001a\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u00040\u0006H\u0002J\b\u0010$\u001a\u00020!H\u0002J\u001c\u0010%\u001a\u00020!2\u0012\u0010\u0005\u001a\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u00040\u0006H\u0002J\u0018\u0010&\u001a\u00020!2\u0006\u0010'\u001a\u00020(2\u0006\u0010)\u001a\u00020*H\u0016J&\u0010+\u001a\u0004\u0018\u00010\u00182\u0006\u0010)\u001a\u00020,2\b\u0010-\u001a\u0004\u0018\u00010.2\b\u0010/\u001a\u0004\u0018\u000100H\u0016J\u001a\u00101\u001a\u00020!2\u0006\u00102\u001a\u00020\u00182\b\u0010/\u001a\u0004\u0018\u000100H\u0016J\u0006\u00103\u001a\u00020!R\u000e\u0010\u0003\u001a\u00020\u0004X\u000e¢\u0006\u0002\n\u0000R\u001a\u0010\u0005\u001a\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u00040\u0006X\u0004¢\u0006\u0002\n\u0000R\u0012\u0010\b\u001a\u0004\u0018\u00010\tX\u000e¢\u0006\u0004\n\u0002\u0010\nR\u000e\u0010\u000b\u001a\u00020\u0004X\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\rX\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u0004X\u000e¢\u0006\u0002\n\u0000R\u0012\u0010\u000f\u001a\u0004\u0018\u00010\tX\u000e¢\u0006\u0004\n\u0002\u0010\nR\u000e\u0010\u0010\u001a\u00020\tX\u000e¢\u0006\u0002\n\u0000R\u0014\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u00130\u0012X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0007X.¢\u0006\u0002\n\u0000R\u0014\u0010\u0015\u001a\b\u0012\u0004\u0012\u00020\u00160\u0012X\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u0017\u001a\u0004\u0018\u00010\u0018X\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0019\u001a\u00020\u001aX\u000e¢\u0006\u0002\n\u0000R\u0012\u0010\u001b\u001a\u0004\u0018\u00010\tX\u000e¢\u0006\u0004\n\u0002\u0010\nR\u000e\u0010\u001c\u001a\u00020\u0007X\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u001d\u001a\u00020\u0004X\u000e¢\u0006\u0002\n\u0000R\u001a\u0010\u001e\u001a\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u00040\u0006X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u001f\u001a\u00020\u0004X\u000e¢\u0006\u0002\n\u0000¨\u00065"}, d2 = {"Lcom/iqonic/store/fragments/ViewAllProductFragment;", "Lcom/iqonic/store/fragments/BaseFragment;", "()V", "countLoadMore", "", "data", "", "", "isLastPage", "", "Ljava/lang/Boolean;", "mCategoryId", "mColorArray", "", "mId", "mIsLastPage", "mIsLoading", "mProductAdapter", "Lcom/iqonic/store/adapter/BaseAdapter;", "Lcom/iqonic/store/models/StoreProductModel;", "mProductAttributeResponseMsg", "mSubCategoryAdapter", "Lcom/iqonic/store/models/Category;", "menuCart", "Landroid/view/View;", "searchRequest", "Lcom/iqonic/store/models/SearchRequest;", "showPagination", "specialProduct", "subCategoryCountLoadMore", "subCategoryData", "totalPages", "addCart", "", "model", "loadCategory", "loadData", "loadSubCategory", "onCreateOptionsMenu", "menu", "Landroid/view/Menu;", "inflater", "Landroid/view/MenuInflater;", "onCreateView", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "savedInstanceState", "Landroid/os/Bundle;", "onViewCreated", "view", "setCartCount", "Companion", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: ViewAllProductFragment.kt */
public final class ViewAllProductFragment extends BaseFragment {
    public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
    private HashMap _$_findViewCache;
    /* access modifiers changed from: private */
    public int countLoadMore = 1;
    /* access modifiers changed from: private */
    public final Map<String, Integer> data = new HashMap();
    /* access modifiers changed from: private */
    public Boolean isLastPage = false;
    /* access modifiers changed from: private */
    public int mCategoryId;
    /* access modifiers changed from: private */
    public int[] mColorArray = {R.color.cat_1};
    /* access modifiers changed from: private */
    public int mId;
    /* access modifiers changed from: private */
    public Boolean mIsLastPage = false;
    /* access modifiers changed from: private */
    public boolean mIsLoading;
    /* access modifiers changed from: private */
    public final BaseAdapter<StoreProductModel> mProductAdapter = new BaseAdapter<>(R.layout.item_viewproductgrid, new ViewAllProductFragment$mProductAdapter$1(this));
    private String mProductAttributeResponseMsg;
    /* access modifiers changed from: private */
    public final BaseAdapter<Category> mSubCategoryAdapter = new BaseAdapter<>(R.layout.item_subcategory, new ViewAllProductFragment$mSubCategoryAdapter$1(this));
    private View menuCart;
    /* access modifiers changed from: private */
    public SearchRequest searchRequest = new SearchRequest();
    private Boolean showPagination = true;
    private String specialProduct = "";
    /* access modifiers changed from: private */
    public int subCategoryCountLoadMore = 1;
    private final Map<String, Integer> subCategoryData = new HashMap();
    /* access modifiers changed from: private */
    public int totalPages;

    public void _$_clearFindViewByIdCache() {
        HashMap hashMap = this._$_findViewCache;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    public View _$_findCachedViewById(int i) {
        if (this._$_findViewCache == null) {
            this._$_findViewCache = new HashMap();
        }
        View view = (View) this._$_findViewCache.get(Integer.valueOf(i));
        if (view != null) {
            return view;
        }
        View view2 = getView();
        if (view2 == null) {
            return null;
        }
        View findViewById = view2.findViewById(i);
        this._$_findViewCache.put(Integer.valueOf(i), findViewById);
        return findViewById;
    }

    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        _$_clearFindViewByIdCache();
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J*\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00062\b\b\u0002\u0010\b\u001a\u00020\t2\b\b\u0002\u0010\n\u001a\u00020\u000b¨\u0006\f"}, d2 = {"Lcom/iqonic/store/fragments/ViewAllProductFragment$Companion;", "", "()V", "getNewInstance", "Lcom/iqonic/store/fragments/ViewAllProductFragment;", "id", "", "mCategoryId", "showPagination", "", "specialProduct", "", "app_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: ViewAllProductFragment.kt */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        public static /* synthetic */ ViewAllProductFragment getNewInstance$default(Companion companion, int i, int i2, boolean z, String str, int i3, Object obj) {
            if ((i3 & 4) != 0) {
                z = true;
            }
            if ((i3 & 8) != 0) {
                str = "";
            }
            return companion.getNewInstance(i, i2, z, str);
        }

        public final ViewAllProductFragment getNewInstance(int i, int i2, boolean z, String str) {
            Intrinsics.checkParameterIsNotNull(str, "specialProduct");
            ViewAllProductFragment viewAllProductFragment = new ViewAllProductFragment();
            Bundle bundle = new Bundle();
            bundle.putSerializable(Constants.KeyIntent.VIEWALLID, Integer.valueOf(i));
            bundle.putSerializable(Constants.KeyIntent.KEYID, Integer.valueOf(i2));
            bundle.putSerializable(Constants.KeyIntent.SHOW_PAGINATION, Boolean.valueOf(z));
            if (str.length() > 0) {
                bundle.putSerializable(Constants.KeyIntent.SPECIAL_PRODUCT_KEY, str);
            }
            viewAllProductFragment.setArguments(bundle);
            return viewAllProductFragment;
        }
    }

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Intrinsics.checkParameterIsNotNull(layoutInflater, "inflater");
        return layoutInflater.inflate(R.layout.fragment_newest_product, viewGroup, false);
    }

    public void onViewCreated(View view, Bundle bundle) {
        Intrinsics.checkParameterIsNotNull(view, "view");
        super.onViewCreated(view, bundle);
        setHasOptionsMenu(true);
        Bundle arguments = getArguments();
        Boolean bool = null;
        Integer valueOf = arguments != null ? Integer.valueOf(arguments.getInt(Constants.KeyIntent.VIEWALLID)) : null;
        if (valueOf == null) {
            Intrinsics.throwNpe();
        }
        int intValue = valueOf.intValue();
        this.mId = intValue;
        if (intValue == 108) {
            Bundle arguments2 = getArguments();
            String string = arguments2 != null ? arguments2.getString(Constants.KeyIntent.SPECIAL_PRODUCT_KEY) : null;
            if (string == null) {
                Intrinsics.throwNpe();
            }
            this.specialProduct = string;
        }
        Bundle arguments3 = getArguments();
        Integer valueOf2 = arguments3 != null ? Integer.valueOf(arguments3.getInt(Constants.KeyIntent.KEYID)) : null;
        if (valueOf2 == null) {
            Intrinsics.throwNpe();
        }
        this.mCategoryId = valueOf2.intValue();
        Bundle arguments4 = getArguments();
        if (arguments4 != null) {
            bool = Boolean.valueOf(arguments4.getBoolean(Constants.KeyIntent.SHOW_PAGINATION));
        }
        this.showPagination = bool;
        String string2 = getString(R.string.lbl_please_wait);
        Intrinsics.checkExpressionValueIsNotNull(string2, "getString(R.string.lbl_please_wait)");
        this.mProductAttributeResponseMsg = string2;
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 2);
        RecyclerView recyclerView = (RecyclerView) _$_findCachedViewById(com.iqonic.store.R.id.rvNewestProduct);
        Intrinsics.checkExpressionValueIsNotNull(recyclerView, "rvNewestProduct");
        recyclerView.setLayoutManager(gridLayoutManager);
        FrameLayout frameLayout = (FrameLayout) _$_findCachedViewById(com.iqonic.store.R.id.fLMain);
        Intrinsics.checkExpressionValueIsNotNull(frameLayout, "fLMain");
        ExtensionsKt.changeBackgroundColor(frameLayout);
        int i = this.mId;
        if (i == 104) {
            this.data.put("page", Integer.valueOf(this.countLoadMore));
            this.data.put("per_page", 20);
            this.data.put("category", Integer.valueOf(this.mCategoryId));
            loadCategory(this.data);
            this.subCategoryData.put("per_page", 50);
            this.subCategoryData.put("parent", Integer.valueOf(this.mCategoryId));
            loadSubCategory(this.subCategoryData);
        } else {
            switch (i) {
                case 102:
                    this.searchRequest.setNewest("newest");
                    break;
                case 103:
                    this.searchRequest.setFeatured("product_visibility");
                    break;
                case 106:
                    this.searchRequest.setOn_sale("_sale_price");
                    break;
                case 107:
                    this.searchRequest.setOptional_selling("total_sales");
                    break;
                case 108:
                    this.searchRequest.setSpecial_product(this.specialProduct);
                    break;
            }
            this.searchRequest.setProduct_per_page(20);
            this.searchRequest.setPage(this.countLoadMore);
            loadData();
        }
        RecyclerView recyclerView2 = (RecyclerView) _$_findCachedViewById(com.iqonic.store.R.id.rvCategory);
        ExtensionsKt.setHorizontalLayout(recyclerView2, false);
        recyclerView2.setHasFixedSize(true);
        RecyclerView recyclerView3 = (RecyclerView) _$_findCachedViewById(com.iqonic.store.R.id.rvCategory);
        Intrinsics.checkExpressionValueIsNotNull(recyclerView3, "rvCategory");
        recyclerView3.setAdapter(this.mSubCategoryAdapter);
        RecyclerView recyclerView4 = (RecyclerView) _$_findCachedViewById(com.iqonic.store.R.id.rvCategory);
        Intrinsics.checkExpressionValueIsNotNull(recyclerView4, "rvCategory");
        AppExtensionsKt.rvItemAnimation(recyclerView4);
        RecyclerView recyclerView5 = (RecyclerView) _$_findCachedViewById(com.iqonic.store.R.id.rvNewestProduct);
        RecyclerView recyclerView6 = (RecyclerView) _$_findCachedViewById(com.iqonic.store.R.id.rvNewestProduct);
        Intrinsics.checkExpressionValueIsNotNull(recyclerView6, "rvNewestProduct");
        AppExtensionsKt.rvItemAnimation(recyclerView6);
        RecyclerView recyclerView7 = (RecyclerView) _$_findCachedViewById(com.iqonic.store.R.id.rvNewestProduct);
        Intrinsics.checkExpressionValueIsNotNull(recyclerView7, "rvNewestProduct");
        recyclerView7.setAdapter(this.mProductAdapter);
        Boolean bool2 = this.showPagination;
        if (bool2 == null) {
            Intrinsics.throwNpe();
        }
        if (bool2.booleanValue()) {
            recyclerView5.addOnScrollListener(new ViewAllProductFragment$onViewCreated$$inlined$apply$lambda$1(this));
        }
    }

    /* access modifiers changed from: private */
    public final void addCart(StoreProductModel storeProductModel) {
        if (AppExtensionsKt.isLoggedIn()) {
            RequestModel requestModel = new RequestModel();
            if (Intrinsics.areEqual((Object) storeProductModel.getType(), (Object) "variable")) {
                List<Integer> variations = storeProductModel.getVariations();
                if (variations == null) {
                    Intrinsics.throwNpe();
                }
                requestModel.setPro_id(variations.get(0));
            } else {
                requestModel.setPro_id(Integer.valueOf(storeProductModel.getId()));
            }
            requestModel.setQuantity(1);
            FragmentActivity activity = getActivity();
            if (activity != null) {
                NetworkExtensionKt.addItemToCart((AppBaseActivity) activity, requestModel, new ViewAllProductFragment$addCart$1(this));
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type com.iqonic.store.AppBaseActivity");
        }
        FragmentActivity activity2 = getActivity();
        if (activity2 != null) {
            Activity activity3 = (AppBaseActivity) activity2;
            Bundle bundle = null;
            Intent intent = new Intent(activity3, SignInUpActivity.class);
            ViewAllProductFragment$addCart$2.INSTANCE.invoke(intent);
            if (Build.VERSION.SDK_INT >= 16) {
                activity3.startActivityForResult(intent, -1, bundle);
            } else {
                activity3.startActivityForResult(intent, -1);
            }
        } else {
            throw new TypeCastException("null cannot be cast to non-null type com.iqonic.store.AppBaseActivity");
        }
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater menuInflater) {
        Intrinsics.checkParameterIsNotNull(menu, "menu");
        Intrinsics.checkParameterIsNotNull(menuInflater, "inflater");
        menu.clear();
        menuInflater.inflate(R.menu.menu_view_all, menu);
        MenuItem findItem = menu.findItem(R.id.action_cart);
        Intrinsics.checkExpressionValueIsNotNull(findItem, "menuWishItem");
        findItem.setVisible(true);
        this.menuCart = findItem.getActionView();
        View actionView = findItem.getActionView();
        actionView.setOnClickListener(new ViewAllProductFragment$onCreateOptionsMenu$$inlined$onClick$1(actionView, this));
        setCartCount();
        super.onCreateOptionsMenu(menu, menuInflater);
    }

    public final void setCartCount() {
        TextView textView;
        ImageView imageView;
        String cartCount = AppExtensionsKt.getCartCount();
        View view = this.menuCart;
        if (view != null) {
            if (!(view == null || (imageView = (ImageView) view.findViewById(com.iqonic.store.R.id.ivCart)) == null)) {
                ExtensionsKt.changeBackgroundImageTint(imageView, AppExtensionsKt.getTextTitleColor());
            }
            View view2 = this.menuCart;
            if (!(view2 == null || (textView = (TextView) view2.findViewById(com.iqonic.store.R.id.tvNotificationCount)) == null)) {
                ExtensionsKt.changeTint(textView, AppExtensionsKt.getTextTitleColor());
            }
            View view3 = this.menuCart;
            if (view3 == null) {
                Intrinsics.throwNpe();
            }
            TextView textView2 = (TextView) view3.findViewById(com.iqonic.store.R.id.tvNotificationCount);
            Intrinsics.checkExpressionValueIsNotNull(textView2, "menuCart!!.tvNotificationCount");
            textView2.setText(cartCount);
            View view4 = this.menuCart;
            if (view4 == null) {
                Intrinsics.throwNpe();
            }
            TextView textView3 = (TextView) view4.findViewById(com.iqonic.store.R.id.tvNotificationCount);
            Intrinsics.checkExpressionValueIsNotNull(textView3, "menuCart!!.tvNotificationCount");
            ExtensionsKt.changeAccentColor(textView3);
            if (StringExtensionsKt.checkIsEmpty(cartCount) || Intrinsics.areEqual((Object) cartCount, (Object) AppEventsConstants.EVENT_PARAM_VALUE_NO)) {
                View view5 = this.menuCart;
                if (view5 == null) {
                    Intrinsics.throwNpe();
                }
                TextView textView4 = (TextView) view5.findViewById(com.iqonic.store.R.id.tvNotificationCount);
                Intrinsics.checkExpressionValueIsNotNull(textView4, "menuCart!!.tvNotificationCount");
                ViewExtensionsKt.hide(textView4);
                return;
            }
            View view6 = this.menuCart;
            if (view6 == null) {
                Intrinsics.throwNpe();
            }
            TextView textView5 = (TextView) view6.findViewById(com.iqonic.store.R.id.tvNotificationCount);
            Intrinsics.checkExpressionValueIsNotNull(textView5, "menuCart!!.tvNotificationCount");
            ViewExtensionsKt.show(textView5);
        }
    }

    /* access modifiers changed from: private */
    public final void loadCategory(Map<String, Integer> map) {
        if (ExtensionsKt.isNetworkAvailable()) {
            FragmentActivity activity = getActivity();
            if (activity == null) {
                Intrinsics.throwNpe();
            }
            if (activity != null) {
                ((AppBaseActivity) activity).showProgress(true);
                NetworkExtensionKt.getRestApiImpl$default((String) null, 1, (Object) null).listAllCategoryProduct(map, new ViewAllProductFragment$loadCategory$1(this), new ViewAllProductFragment$loadCategory$2(this));
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type com.iqonic.store.AppBaseActivity");
        }
        FragmentActivity activity2 = getActivity();
        if (activity2 == null) {
            Intrinsics.throwNpe();
        }
        if (activity2 != null) {
            ((AppBaseActivity) activity2).showProgress(false);
            FragmentActivity activity3 = getActivity();
            if (activity3 != null) {
                ExtensionsKt.noInternetSnackBar((AppBaseActivity) activity3);
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type com.iqonic.store.AppBaseActivity");
        }
        throw new TypeCastException("null cannot be cast to non-null type com.iqonic.store.AppBaseActivity");
    }

    private final void loadSubCategory(Map<String, Integer> map) {
        if (ExtensionsKt.isNetworkAvailable()) {
            FragmentActivity activity = getActivity();
            if (activity == null) {
                Intrinsics.throwNpe();
            }
            if (activity != null) {
                ((AppBaseActivity) activity).showProgress(true);
                NetworkExtensionKt.getRestApiImpl$default((String) null, 1, (Object) null).listAllCategory(map, new ViewAllProductFragment$loadSubCategory$1(this), new ViewAllProductFragment$loadSubCategory$2(this));
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type com.iqonic.store.AppBaseActivity");
        }
    }

    /* access modifiers changed from: private */
    public final void loadData() {
        if (ExtensionsKt.isNetworkAvailable()) {
            FragmentActivity activity = getActivity();
            if (activity == null) {
                Intrinsics.throwNpe();
            }
            if (activity != null) {
                ((AppBaseActivity) activity).showProgress(true);
                NetworkExtensionKt.getRestApiImpl$default((String) null, 1, (Object) null).listSaleProducts(this.searchRequest, new ViewAllProductFragment$loadData$1(this), new ViewAllProductFragment$loadData$2(this));
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type com.iqonic.store.AppBaseActivity");
        }
    }
}
