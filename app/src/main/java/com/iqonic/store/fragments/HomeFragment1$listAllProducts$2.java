package com.iqonic.store.fragments;

import androidx.fragment.app.FragmentActivity;
import com.iqonic.store.AppBaseActivity;
import com.iqonic.store.utils.extensions.ExtensionsKt;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n¢\u0006\u0002\b\u0004"}, d2 = {"<anonymous>", "", "it", "", "invoke"}, k = 3, mv = {1, 1, 16})
/* compiled from: HomeFragment1.kt */
final class HomeFragment1$listAllProducts$2 extends Lambda implements Function1<String, Unit> {
    final /* synthetic */ HomeFragment1 this$0;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    HomeFragment1$listAllProducts$2(HomeFragment1 homeFragment1) {
        super(1);
        this.this$0 = homeFragment1;
    }

    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        invoke((String) obj);
        return Unit.INSTANCE;
    }

    public final void invoke(String str) {
        Intrinsics.checkParameterIsNotNull(str, "it");
        FragmentActivity activity = this.this$0.getActivity();
        if (activity != null) {
            ((AppBaseActivity) activity).showProgress(false);
            ExtensionsKt.snackBar(this.this$0, str);
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.iqonic.store.AppBaseActivity");
    }
}
