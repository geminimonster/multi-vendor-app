package com.iqonic.store.fragments;

import android.view.ViewTreeObserver;
import android.widget.ScrollView;
import com.iqonic.store.R;
import com.iqonic.store.utils.CustomSwipeToRefresh;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n¢\u0006\u0002\b\u0002"}, d2 = {"<anonymous>", "", "onScrollChanged"}, k = 3, mv = {1, 1, 16})
/* compiled from: HomeFragment2.kt */
final class HomeFragment2$onViewCreated$2 implements ViewTreeObserver.OnScrollChangedListener {
    final /* synthetic */ HomeFragment2 this$0;

    HomeFragment2$onViewCreated$2(HomeFragment2 homeFragment2) {
        this.this$0 = homeFragment2;
    }

    public final void onScrollChanged() {
        CustomSwipeToRefresh customSwipeToRefresh = (CustomSwipeToRefresh) this.this$0._$_findCachedViewById(R.id.refreshLayout);
        Intrinsics.checkExpressionValueIsNotNull(customSwipeToRefresh, "refreshLayout");
        ScrollView scrollView = (ScrollView) this.this$0._$_findCachedViewById(R.id.scrollView);
        Intrinsics.checkExpressionValueIsNotNull(scrollView, "scrollView");
        customSwipeToRefresh.setEnabled(scrollView.getScrollY() == 0);
    }
}
