package com.iqonic.store.fragments;

import android.graphics.Color;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.fragment.app.FragmentActivity;
import com.iqonic.store.AppBaseActivity;
import com.iqonic.store.R;
import com.iqonic.store.models.Category;
import com.iqonic.store.utils.extensions.AppExtensionsKt;
import com.iqonic.store.utils.extensions.ExtensionsKt;
import com.iqonic.store.utils.extensions.NetworkExtensionKt;
import com.iqonic.store.utils.extensions.ViewExtensionsKt;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.Unit;
import kotlin.jvm.functions.Function3;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0007H\n¢\u0006\u0002\b\b"}, d2 = {"<anonymous>", "", "view", "Landroid/view/View;", "model", "Lcom/iqonic/store/models/Category;", "position", "", "invoke"}, k = 3, mv = {1, 1, 16})
/* compiled from: ViewAllProductFragment.kt */
final class ViewAllProductFragment$mSubCategoryAdapter$1 extends Lambda implements Function3<View, Category, Integer, Unit> {
    final /* synthetic */ ViewAllProductFragment this$0;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    ViewAllProductFragment$mSubCategoryAdapter$1(ViewAllProductFragment viewAllProductFragment) {
        super(3);
        this.this$0 = viewAllProductFragment;
    }

    public /* bridge */ /* synthetic */ Object invoke(Object obj, Object obj2, Object obj3) {
        invoke((View) obj, (Category) obj2, ((Number) obj3).intValue());
        return Unit.INSTANCE;
    }

    public final void invoke(View view, Category category, int i) {
        Intrinsics.checkParameterIsNotNull(view, "view");
        Intrinsics.checkParameterIsNotNull(category, "model");
        TextView textView = (TextView) view.findViewById(R.id.tvSubCategory);
        Intrinsics.checkExpressionValueIsNotNull(textView, "view.tvSubCategory");
        textView.setText(category.getName());
        if (category.getImage() != null) {
            if (category.getImage().getSrc().length() > 0) {
                ImageView imageView = (ImageView) view.findViewById(R.id.ivProducts);
                Intrinsics.checkExpressionValueIsNotNull(imageView, "view.ivProducts");
                NetworkExtensionKt.loadImageFromUrl$default(imageView, category.getImage().getSrc(), 0, 0, 6, (Object) null);
                ImageView imageView2 = (ImageView) view.findViewById(R.id.ivProducts);
                Intrinsics.checkExpressionValueIsNotNull(imageView2, "view.ivProducts");
                imageView2.setVisibility(0);
            }
        } else {
            ImageView imageView3 = (ImageView) view.findViewById(R.id.ivProducts);
            Intrinsics.checkExpressionValueIsNotNull(imageView3, "view.ivProducts");
            imageView3.setVisibility(8);
        }
        LinearLayout linearLayout = (LinearLayout) view.findViewById(R.id.llMain);
        Intrinsics.checkExpressionValueIsNotNull(linearLayout, "view.llMain");
        View view2 = linearLayout;
        FragmentActivity activity = this.this$0.getActivity();
        if (activity != null) {
            int color = ExtensionsKt.color((AppBaseActivity) activity, com.store.proshop.R.color.transparent);
            FragmentActivity activity2 = this.this$0.getActivity();
            if (activity2 != null) {
                ViewExtensionsKt.setStrokedBackground$default(view2, color, ExtensionsKt.color((AppBaseActivity) activity2, this.this$0.mColorArray[i % this.this$0.mColorArray.length]), 0.0f, 0, 12, (Object) null);
                TextView textView2 = (TextView) view.findViewById(R.id.tvSubCategory);
                FragmentActivity activity3 = this.this$0.getActivity();
                if (activity3 != null) {
                    textView2.setTextColor(ExtensionsKt.color((AppBaseActivity) activity3, this.this$0.mColorArray[i % this.this$0.mColorArray.length]));
                    view.setOnClickListener(new ViewAllProductFragment$mSubCategoryAdapter$1$$special$$inlined$onClick$1(view, this, category));
                    TextView textView3 = (TextView) view.findViewById(R.id.tvSubCategory);
                    Intrinsics.checkExpressionValueIsNotNull(textView3, "view.tvSubCategory");
                    ExtensionsKt.changeTextPrimaryColor(textView3);
                    LinearLayout linearLayout2 = (LinearLayout) view.findViewById(R.id.llMain);
                    Intrinsics.checkExpressionValueIsNotNull(linearLayout2, "view.llMain");
                    ViewExtensionsKt.setStrokedBackground$default(linearLayout2, Color.parseColor(AppExtensionsKt.getTextPrimaryColor()), Color.parseColor(AppExtensionsKt.getTextPrimaryColor()), 0.4f, 0, 8, (Object) null);
                    return;
                }
                throw new TypeCastException("null cannot be cast to non-null type com.iqonic.store.AppBaseActivity");
            }
            throw new TypeCastException("null cannot be cast to non-null type com.iqonic.store.AppBaseActivity");
        }
        throw new TypeCastException("null cannot be cast to non-null type com.iqonic.store.AppBaseActivity");
    }
}
