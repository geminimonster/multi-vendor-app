package com.iqonic.store.fragments;

import android.app.Dialog;
import android.view.View;
import android.widget.EditText;
import com.google.android.material.button.MaterialButton;
import com.iqonic.store.R;
import com.iqonic.store.models.RequestModel;
import com.iqonic.store.utils.extensions.EditTextExtensionsKt;
import com.iqonic.store.utils.extensions.ExtensionsKt;
import com.iqonic.store.utils.extensions.NetworkExtensionKt;
import com.iqonic.store.utils.extensions.ViewExtensionsKt;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\b\u0003\n\u0002\b\u0004\u0010\u0000\u001a\u00020\u0001\"\b\b\u0000\u0010\u0002*\u00020\u00032\u000e\u0010\u0004\u001a\n \u0005*\u0004\u0018\u00010\u00030\u0003H\n¢\u0006\u0002\b\u0006¨\u0006\u0007"}, d2 = {"<anonymous>", "", "T", "Landroid/view/View;", "it", "kotlin.jvm.PlatformType", "onClick", "com/iqonic/store/utils/extensions/ExtensionsKt$onClick$1"}, k = 3, mv = {1, 1, 16})
/* compiled from: Extensions.kt */
public final class SignInFragment$showChangePasswordDialog$$inlined$onClick$1 implements View.OnClickListener {
    final /* synthetic */ Dialog $changePasswordDialog$inlined;
    final /* synthetic */ View $this_onClick;
    final /* synthetic */ SignInFragment this$0;

    public SignInFragment$showChangePasswordDialog$$inlined$onClick$1(View view, SignInFragment signInFragment, Dialog dialog) {
        this.$this_onClick = view;
        this.this$0 = signInFragment;
        this.$changePasswordDialog$inlined = dialog;
    }

    public final void onClick(View view) {
        MaterialButton materialButton = (MaterialButton) this.$this_onClick;
        EditText editText = (EditText) this.$changePasswordDialog$inlined.findViewById(R.id.edtForgotEmail);
        Intrinsics.checkExpressionValueIsNotNull(editText, "changePasswordDialog.edtForgotEmail");
        ViewExtensionsKt.hideSoftKeyboard(editText);
        EditText editText2 = (EditText) this.$changePasswordDialog$inlined.findViewById(R.id.edtForgotEmail);
        Intrinsics.checkExpressionValueIsNotNull(editText2, "changePasswordDialog.edtForgotEmail");
        if (EditTextExtensionsKt.textToString(editText2).length() == 0) {
            EditText editText3 = (EditText) this.$changePasswordDialog$inlined.findViewById(R.id.edtForgotEmail);
            Intrinsics.checkExpressionValueIsNotNull(editText3, "changePasswordDialog.edtForgotEmail");
            String string = this.this$0.getString(com.store.proshop.R.string.hint_enter_your_email_id);
            Intrinsics.checkExpressionValueIsNotNull(string, "getString(R.string.hint_enter_your_email_id)");
            EditTextExtensionsKt.showError(editText3, string);
            return;
        }
        EditText editText4 = (EditText) this.$changePasswordDialog$inlined.findViewById(R.id.edtForgotEmail);
        Intrinsics.checkExpressionValueIsNotNull(editText4, "changePasswordDialog.edtForgotEmail");
        if (!EditTextExtensionsKt.isValidEmail(editText4)) {
            EditText editText5 = (EditText) this.$changePasswordDialog$inlined.findViewById(R.id.edtForgotEmail);
            Intrinsics.checkExpressionValueIsNotNull(editText5, "changePasswordDialog.edtForgotEmail");
            String string2 = this.this$0.getString(com.store.proshop.R.string.error_enter_valid_email);
            Intrinsics.checkExpressionValueIsNotNull(string2, "getString(R.string.error_enter_valid_email)");
            EditTextExtensionsKt.showError(editText5, string2);
            return;
        }
        RequestModel requestModel = new RequestModel();
        EditText editText6 = (EditText) this.$changePasswordDialog$inlined.findViewById(R.id.edtForgotEmail);
        Intrinsics.checkExpressionValueIsNotNull(editText6, "changePasswordDialog.edtForgotEmail");
        requestModel.setUserEmail(EditTextExtensionsKt.textToString(editText6));
        if (ExtensionsKt.isNetworkAvailable()) {
            this.this$0.showProgress();
            NetworkExtensionKt.getRestApiImpl$default((String) null, 1, (Object) null).forgetPassword(requestModel, new SignInFragment$showChangePasswordDialog$$inlined$onClick$1$lambda$1(this), new SignInFragment$showChangePasswordDialog$$inlined$onClick$1$lambda$2(this));
            return;
        }
        this.this$0.hideProgress();
        String string3 = this.this$0.getString(com.store.proshop.R.string.error_no_internet);
        Intrinsics.checkExpressionValueIsNotNull(string3, "getString(R.string.error_no_internet)");
        ViewExtensionsKt.snackBarError(materialButton, string3);
    }
}
