package com.iqonic.store.fragments;

import android.content.res.ColorStateList;
import android.graphics.Color;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.google.android.material.card.MaterialCardView;
import com.iqonic.store.R;
import com.iqonic.store.models.CartResponse;
import com.iqonic.store.utils.extensions.AppExtensionsKt;
import com.iqonic.store.utils.extensions.ExtensionsKt;
import com.iqonic.store.utils.extensions.NetworkExtensionKt;
import com.iqonic.store.utils.extensions.StringExtensionsKt;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function3;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0007H\n¢\u0006\u0002\b\b"}, d2 = {"<anonymous>", "", "view", "Landroid/view/View;", "model", "Lcom/iqonic/store/models/CartResponse;", "position", "", "invoke"}, k = 3, mv = {1, 1, 16})
/* compiled from: MyCartFragment.kt */
final class MyCartFragment$mCartAdapter$1 extends Lambda implements Function3<View, CartResponse, Integer, Unit> {
    final /* synthetic */ MyCartFragment this$0;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    MyCartFragment$mCartAdapter$1(MyCartFragment myCartFragment) {
        super(3);
        this.this$0 = myCartFragment;
    }

    public /* bridge */ /* synthetic */ Object invoke(Object obj, Object obj2, Object obj3) {
        invoke((View) obj, (CartResponse) obj2, ((Number) obj3).intValue());
        return Unit.INSTANCE;
    }

    public final void invoke(View view, CartResponse cartResponse, int i) {
        Intrinsics.checkParameterIsNotNull(view, "view");
        Intrinsics.checkParameterIsNotNull(cartResponse, "model");
        TextView textView = (TextView) view.findViewById(R.id.tvProductName);
        Intrinsics.checkExpressionValueIsNotNull(textView, "view.tvProductName");
        textView.setText(cartResponse.getName());
        TextView textView2 = (TextView) view.findViewById(R.id.tvOriginalPrice);
        Intrinsics.checkExpressionValueIsNotNull(textView2, "view.tvOriginalPrice");
        ExtensionsKt.applyStrike(textView2);
        if (cartResponse.getFull() != null) {
            ImageView imageView = (ImageView) view.findViewById(R.id.ivProduct);
            Intrinsics.checkExpressionValueIsNotNull(imageView, "view.ivProduct");
            NetworkExtensionKt.loadImageFromUrl$default(imageView, cartResponse.getFull(), 0, 0, 6, (Object) null);
        }
        try {
            TextView textView3 = (TextView) view.findViewById(R.id.tvPrice);
            Intrinsics.checkExpressionValueIsNotNull(textView3, "view.tvPrice");
            textView3.setText(StringExtensionsKt.currencyFormat$default(String.valueOf(((int) Float.parseFloat(cartResponse.getRegular_price())) * Integer.parseInt(cartResponse.getQuantity())), (String) null, 1, (Object) null));
        } catch (Exception unused) {
            TextView textView4 = (TextView) view.findViewById(R.id.tvPrice);
            Intrinsics.checkExpressionValueIsNotNull(textView4, "view.tvPrice");
            textView4.setText(StringExtensionsKt.currencyFormat$default(String.valueOf(((int) Float.parseFloat(cartResponse.getPrice())) * Integer.parseInt(cartResponse.getQuantity())), (String) null, 1, (Object) null));
        }
        TextView textView5 = (TextView) view.findViewById(R.id.qty_spinner);
        Intrinsics.checkExpressionValueIsNotNull(textView5, "view.qty_spinner");
        textView5.setText(cartResponse.getQuantity());
        MaterialCardView materialCardView = (MaterialCardView) view.findViewById(R.id.delete_layout);
        materialCardView.setOnClickListener(new MyCartFragment$mCartAdapter$1$$special$$inlined$onClick$1(materialCardView, this, view, cartResponse));
        ImageButton imageButton = (ImageButton) view.findViewById(R.id.ivMinus);
        imageButton.setOnClickListener(new MyCartFragment$mCartAdapter$1$$special$$inlined$onClick$2(imageButton, this, cartResponse, i));
        ImageButton imageButton2 = (ImageButton) view.findViewById(R.id.ivAdd);
        imageButton2.setOnClickListener(new MyCartFragment$mCartAdapter$1$$special$$inlined$onClick$3(imageButton2, this, cartResponse, i));
        RelativeLayout relativeLayout = (RelativeLayout) view.findViewById(R.id.front_layout);
        relativeLayout.setOnClickListener(new MyCartFragment$mCartAdapter$1$$special$$inlined$onClick$4(relativeLayout, this, cartResponse));
        TextView textView6 = (TextView) view.findViewById(R.id.tvProductName);
        Intrinsics.checkExpressionValueIsNotNull(textView6, "view.tvProductName");
        ExtensionsKt.changeTextPrimaryColor(textView6);
        TextView textView7 = (TextView) view.findViewById(R.id.qty_spinner);
        Intrinsics.checkExpressionValueIsNotNull(textView7, "view.qty_spinner");
        ExtensionsKt.changeTextPrimaryColor(textView7);
        ImageButton imageButton3 = (ImageButton) view.findViewById(R.id.ivMinus);
        Intrinsics.checkExpressionValueIsNotNull(imageButton3, "view.ivMinus");
        imageButton3.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor(AppExtensionsKt.getPrimaryColor())));
        ImageButton imageButton4 = (ImageButton) view.findViewById(R.id.ivAdd);
        Intrinsics.checkExpressionValueIsNotNull(imageButton4, "view.ivAdd");
        imageButton4.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor(AppExtensionsKt.getPrimaryColor())));
        TextView textView8 = (TextView) view.findViewById(R.id.tvPrice);
        Intrinsics.checkExpressionValueIsNotNull(textView8, "view.tvPrice");
        ExtensionsKt.changeTextPrimaryColor(textView8);
        TextView textView9 = (TextView) view.findViewById(R.id.tvOriginalPrice);
        Intrinsics.checkExpressionValueIsNotNull(textView9, "view.tvOriginalPrice");
        ExtensionsKt.changeTextSecondaryColor(textView9);
        ((MaterialCardView) view.findViewById(R.id.delete_layout)).setCardBackgroundColor(Color.parseColor(AppExtensionsKt.getButtonColor()));
    }
}
