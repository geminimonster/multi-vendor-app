package com.iqonic.store.fragments;

import android.content.Intent;
import com.iqonic.store.utils.Constants;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\u0010\u0000\u001a\u00020\u0001*\u00020\u0002H\n¢\u0006\u0002\b\u0003¨\u0006\u0004"}, d2 = {"<anonymous>", "", "Landroid/content/Intent;", "invoke", "com/iqonic/store/fragments/HomeFragment2$setProductItem$1$1"}, k = 3, mv = {1, 1, 16})
/* compiled from: HomeFragment2.kt */
final class HomeFragment2$setProductItem$$inlined$onClick$1$lambda$1 extends Lambda implements Function1<Intent, Unit> {
    final /* synthetic */ HomeFragment2$setProductItem$$inlined$onClick$1 this$0;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    HomeFragment2$setProductItem$$inlined$onClick$1$lambda$1(HomeFragment2$setProductItem$$inlined$onClick$1 homeFragment2$setProductItem$$inlined$onClick$1) {
        super(1);
        this.this$0 = homeFragment2$setProductItem$$inlined$onClick$1;
    }

    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        invoke((Intent) obj);
        return Unit.INSTANCE;
    }

    public final void invoke(Intent intent) {
        Intrinsics.checkParameterIsNotNull(intent, "$receiver");
        intent.putExtra(Constants.KeyIntent.PRODUCT_ID, this.this$0.$model$inlined.getId());
        intent.putExtra("data", this.this$0.$model$inlined);
    }
}
