package com.iqonic.store.fragments

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.SpannableString
import android.view.*
import com.iqonic.store.AppBaseActivity
import com.iqonic.store.R
import com.iqonic.store.activity.CouponActivity
import com.iqonic.store.activity.DashBoardActivity
import com.iqonic.store.activity.OrderSummaryActivity
import com.iqonic.store.activity.WishlistActivity
import com.iqonic.store.adapter.BaseAdapter
import com.iqonic.store.utils.Constants
import com.iqonic.store.utils.Constants.EXTRA_ADD_AMOUNT
import java.text.DecimalFormat

class MyCartFragment : BaseFragment() {

    var mCoupons: Coupons? = null
    var mTotalCount = 0.0
    var mTotalDisocunt = "0"
    var cartItemId = ""
    var isRemoveCoupons = true
    var mOrderItems: ArrayList<Line_items>? = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_cart, container, false)
    }

    private val mCartAdapter =
        BaseAdapter<CartResponse>(R.layout.item_cart, onBind = { view, model, position ->
            view.tvProductName.text = model.name
            view.tvOriginalPrice.applyStrike()

            if (model.full != null) {
                view.ivProduct.loadImageFromUrl(model.full)
            }

            if (model.on_sale) {
                try {
                    view.tvPrice.text =
                        (model.sale_price.toInt() * model.quantity.toInt()).toString()
                            .currencyFormat()
                } catch (e: java.lang.Exception) {
                    view.tvPrice.text =
                        (model.price.toFloat().toInt() * model.quantity.toInt()).toString()
                            .currencyFormat()
                }
            } else {
                try {
                    view.tvPrice.text =
                        (model.regular_price.toFloat().toInt() * model.quantity.toInt()).toString()
                            .currencyFormat()
                } catch (e: java.lang.Exception) {
                    view.tvPrice.text =
                        (model.price.toFloat().toInt() * model.quantity.toInt()).toString()
                            .currencyFormat()
                }
            }

            view.qty_spinner.text = model.quantity

            view.delete_layout.onClick { view.swipeLayout.close(true); removeCartItem(model) }

            view.ivMinus.onClick { minusClick(model, position) }

            view.ivAdd.onClick { addClick(model, position) }
        })

    private fun addClick(model: CartResponse, position: Int) {
        val qty = model.quantity.toInt()
        mCartAdapter.items[position].quantity = qty.plus(1).toString()
        mCartAdapter.notifyItemChanged(position)
        removeCoupon()
        updateCartItem(mCartAdapter.items[position])
    }

    private fun minusClick(model: CartResponse, position: Int) {
        val qty = model.quantity.toInt()
        if (qty > 1) {
            mCartAdapter.items[position].quantity = qty.minus(1).toString()
            mCartAdapter.notifyItemChanged(position)
            removeCoupon()
            updateCartItem(mCartAdapter.items[position])
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setHasOptionsMenu(true)

        tvContinue.onClick {
            launchActivity<OrderSummaryActivity>(Constants.RequestCode.ORDER_SUMMARY) {
                if (mCoupons != null) {
                    putExtra(Constants.KeyIntent.COUPON_CODE, mCoupons!!.code)
                } else {
                    putExtra(Constants.KeyIntent.COUPON_CODE, "")
                }
                putExtra(Constants.KeyIntent.PRICE, mTotalCount.toString())
                putExtra(Constants.KeyIntent.PRODUCTDATA, mOrderItems)
            }
        }

        rvCart.setVerticalLayout()
        rvCart.adapter = mCartAdapter

        btnShopNow.onClick { launchActivity<DashBoardActivity> { } }

        if (getSharedPrefInstance().getBooleanValue(Constants.SharedPref.ENABLE_COUPONS, false)) {
            rlCoupon.visibility = View.VISIBLE
        } else {
            rlCoupon.visibility = View.GONE
        }

        invalidateCartLayout()
    }


    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        menu.clear()
        inflater.inflate(R.menu.menu_cart, menu)
        val positionOfMenuItem = 0
        val item = menu.getItem(positionOfMenuItem)
        val s = SpannableString(getString(R.string.lbl_wish_list))
        item.title = s
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_wishlist -> {
                if (activity is DashBoardActivity) {
                    (activity as DashBoardActivity).loadWishlistFragment()
                } else {
                    activity.launchActivity<WishlistActivity>()
                }
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun invalidatePaymentLayout(b: Boolean) {
        if (activity != null) {
            if (!b) {
                lay_button.hide()
                rvCart.hide()
            } else {
                lay_button.show()
                rvCart.show()
            }
        }
    }

    private fun removeMultipleCartItem() {
        (activity as AppBaseActivity).showProgress(true)
        val requestModel = CartRequestModel()
        requestModel.multpleId = cartItemId
        (activity as AppBaseActivity).removeMultipleCartItem(requestModel, onApiSuccess = {
            (activity as AppBaseActivity).showProgress(false)
            activity!!.finish()
        })
    }

    private fun removeCartItem(model: CartResponse) {
        val requestModel = RequestModel()
        requestModel.pro_id = model.pro_id.toInt()
        (activity as AppBaseActivity).removeCartItem(requestModel, onApiSuccess = {
            invalidateCartLayout()
        })
    }


    @SuppressLint("SetTextI18n")
    fun invalidateCartLayout() {
        if (isNetworkAvailable()) {
            (activity as AppBaseActivity).showProgress(true)
            getRestApiImpl().getCart(onApiSuccess = {
                if (activity == null) return@getCart
                (activity as AppBaseActivity).showProgress(false)
                getSharedPrefInstance().setValue(Constants.SharedPref.KEY_CART_COUNT, it.size)
                (activity as AppBaseActivity).sendCartCountChangeBroadcast()

                if (it.size == 0) {
                    invalidatePaymentLayout(false)
                    llNoItems.show()
                    lay_button.hide()
                    nsvCart.hide()
                } else {
                    llNoItems.hide()
                    lay_button.show()
                    nsvCart.show()
                    if (activity != null) {
                        llNoItems.hide()
                        cartItemId = ""
                        mTotalCount = 0.0
                        mOrderItems!!.clear()
                        for (i in 0 until it.size) {
                            // Add data to list
                            val itemData = Line_items()
                            itemData.product_id = it[i].pro_id.toInt()
                            itemData.quantity = it[i].quantity.toInt()
                            mOrderItems!!.add(itemData)

                            if (it[i].on_sale) {
                                mTotalCount += try {
                                    it[i].sale_price.toInt() * it[i].quantity.toInt()
                                } catch (e: java.lang.Exception) {
                                    it[i].price.toFloat()
                                        .toInt() * it[i].quantity.toInt()
                                }
                            } else {
                                try {
                                    if (it[i].regular_price.isNotEmpty()) {
                                        mTotalCount += it[i].regular_price.toFloat()
                                            .toInt() * it[i].quantity.toInt()
                                    } else {
                                        mTotalCount += it[i].price.toFloat()
                                            .toInt() * it[i].quantity.toInt()
                                    }
                                } catch (e: Exception) {
                                    mTotalCount += it[i].price.toFloat()
                                        .toInt() * it[i].quantity.toInt()
                                }
                            }
                            cartItemId = if (cartItemId.isNotEmpty()) {
                                cartItemId + "," + itemData.product_id.toString()
                            } else {
                                itemData.product_id.toString()
                            }
                        }

                        tvTotal.text = mTotalCount.toString().currencyFormat()

                        if (isRemoveCoupons) {
                            tvDiscount.text = "0".currencyFormat()
                            tvTotalCartAmount.text = mTotalCount.toString().currencyFormat()
                            tvEditCoupon.text = getString(R.string.lbl_apply)
                            tvEditCoupon.onClick {
                                launchActivity<CouponActivity>(Constants.RequestCode.COUPON_CODE) { }
                            }
                        } else {
                            applyCouponCode()
                        }
                        invalidatePaymentLayout(true)
                        mCartAdapter.clearItems()
                        mCartAdapter.addItems(it)
                    }
                }

            }, onApiError = {
                if (activity == null) return@getCart
                (activity as AppBaseActivity).showProgress(false)
                getSharedPrefInstance().setValue(Constants.SharedPref.KEY_CART_COUNT, 0)
                (activity as AppBaseActivity).sendCartCountChangeBroadcast()
                llNoItems.show()
                lay_button.hide()
                nsvCart.hide()
            })
        } else {
            if (activity == null) return
            (activity as AppBaseActivity).noInternetSnackBar()
        }
    }

    private fun updateCartItem(model: CartResponse) {
        if (activity == null) return
        (activity as AppBaseActivity).showProgress(true)
        if (isNetworkAvailable()) {
            if (activity == null) return
            (activity as AppBaseActivity).showProgress(true)
            val requestModel = RequestModel()
            requestModel.pro_id = model.pro_id.toInt()
            requestModel.cartid = model.cart_id.toInt()
            requestModel.quantity = model.quantity.toInt()

            getRestApiImpl().updateItemInCart(request = requestModel, onApiSuccess = {
                if (activity == null) return@updateItemInCart
                (activity as AppBaseActivity).showProgress(false)
                invalidateCartLayout()
            }, onApiError = {
                if (activity == null) return@updateItemInCart
                (activity as AppBaseActivity).showProgress(false)
                snackBar(it)
            })

        } else {
            if (activity == null) return
            (activity as AppBaseActivity).showProgress(false)
            (activity as AppBaseActivity).noInternetSnackBar()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) {
                Constants.RequestCode.COUPON_CODE -> {
                    mCoupons = data!!.getSerializableExtra("couponData") as Coupons
                    isRemoveCoupons = false
                    applyCouponCode()
                }
                Constants.RequestCode.ORDER_SUMMARY -> {
                    removeMultipleCartItem()
                }
                else -> {
                    activity.finish()
                }
            }
        }
    }

    /**
     * Apply coupon code
     *
     */
    @SuppressLint("SetTextI18n")
    private fun applyCouponCode() {
        if (mCoupons != null) {

            mTotalDisocunt = "0"
            if (mCoupons!!.minimum_amount.toFloat() > 0.0) {
                if (mTotalCount < mCoupons!!.minimum_amount.toFloat()) {
                    txtApplyCouponCode.text =
                        getString(R.string.lbl_coupon_is_valid_only_orders_of) + mCoupons!!.minimum_amount.currencyFormat() + getString(
                            R.string.lbl_coupon_is_valid_only_orders_of1
                        )
                    return
                } else if (mCoupons!!.maximum_amount.toFloat() > 0.0 && mTotalCount > mCoupons!!.maximum_amount.toFloat()) {
                    txtApplyCouponCode.text =
                        getString(R.string.lbl_coupon_is_valid_only_orders_of) + mCoupons!!.maximum_amount.currencyFormat() + " and below."
                    return
                }
            } else if (mCoupons!!.maximum_amount.toFloat() > 0.0) {
                if (mTotalCount > mCoupons!!.maximum_amount.toFloat()) {
                    txtApplyCouponCode.text =
                        getString(R.string.lbl_coupon_is_valid_only_orders_of) + mCoupons!!.maximum_amount.currencyFormat() + " and below."
                    return
                }
            } else if (mCoupons.discount_type == "fixed_cart") {
                if (mTotalCount < mCoupons!!.amount.toFloat()) {
                    txtApplyCouponCode.text =
                        "Coupon is valid only order of " + (mCoupons!!.amount.toFloat() + EXTRA_ADD_AMOUNT) + " and above. Try other coupon."
                    return
                }
            } else if (mCoupons.discount_type == "fixed_product") {
                var isValidCoupon = true
                for (i in 0 until mCartAdapter.getModel().size) {
                    if (mCartAdapter.getModel()[i].sale_price.isNotEmpty()) {
                        if (mCartAdapter.getModel()[i].sale_price.toFloat() < mCoupons!!.amount.toFloat()) {
                            isValidCoupon = false
                            break
                        }
                    } else {
                        if (mCartAdapter.getModel()[i].price.isNotEmpty() && mCartAdapter.getModel()[i].price.toFloat() < mCoupons!!.amount.toFloat()) {
                            isValidCoupon = false
                        }
                    }
                }
                if (!isValidCoupon) {
                    txtApplyCouponCode.text =
                        "Coupon is valid only if all product price have " + (mCoupons!!.amount.toFloat() + EXTRA_ADD_AMOUNT) + " and above. Try other coupon."
                    return
                }
            }
            when (mCoupons.discount_type) {
                "percent" -> {
                    mTotalDisocunt =
                        ((mTotalCount.toFloat() * mCoupons!!.amount.toFloat()) / 100).toString()
                    txtDiscountlbl.text =
                        getString(R.string.lbl_discount) + " (" + mCoupons!!.amount + getString(R.string.lbl_off) + ")"
                }
                "fixed_cart" -> {
                    mTotalDisocunt = mCoupons!!.amount
                    txtDiscountlbl.text =
                        getString(R.string.lbl_discount) + " (" + getString(R.string.lbl_flat) + mCoupons!!.amount.currencyFormat() + getString(
                            R.string.lbl_off
                        ) + ")"
                }
                "fixed_product" -> {
                    val finalAmout = mCoupons!!.amount.split(".")
                    mTotalDisocunt = (finalAmout[0].toInt() * (mOrderItems!!.size)).toString()
                    txtDiscountlbl.text = getString(R.string.lbl_discount)
                }
                else -> {
                    mTotalDisocunt = mCoupons!!.amount
                    txtDiscountlbl.text =
                        getString(R.string.lbl_discount) + " (" + getString(R.string.lbl_flat) + mCoupons!!.amount.currencyFormat() + getString(
                            R.string.lbl_off1
                        ) + ")"
                }
            }

            tvDiscount.text = mTotalDisocunt.currencyFormat()
            mTotalCount = mTotalCount.minus(mTotalDisocunt.toFloat())
            val precision = DecimalFormat("0.00")
            tvTotalCartAmount.text = precision.format(mTotalCount).toString().currencyFormat()
            txtApplyCouponCode.text =
                getString(R.string.lbl_applied_coupon) + mCoupons!!.code.toUpperCase()
            tvEditCoupon.text = getString(R.string.lbl_remove)

            tvEditCoupon.onClick {
                removeCoupon()
            }
        }
    }

    fun removeCoupon() {
        isRemoveCoupons = true
        txtDiscountlbl.text = getString(R.string.lbl_discount)
        txtApplyCouponCode.text = getString(R.string.lbl_coupon_code)
        mTotalCount += mTotalDisocunt.toFloat()
        tvEditCoupon.text = getString(R.string.lbl_apply)
        tvDiscount.text = "0".currencyFormat()
        val precision = DecimalFormat("0.00")
        tvTotalCartAmount.text = precision.format(mTotalCount).toString().currencyFormat()
        tvEditCoupon.onClick {
            launchActivity<CouponActivity>(Constants.RequestCode.COUPON_CODE) { }
        }
    }
}
