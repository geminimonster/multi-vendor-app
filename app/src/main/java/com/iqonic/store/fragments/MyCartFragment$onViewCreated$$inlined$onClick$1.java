package com.iqonic.store.fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;
import androidx.fragment.app.FragmentActivity;
import com.google.android.material.button.MaterialButton;
import com.iqonic.store.ShopHopApp;
import com.iqonic.store.activity.OrderSummaryActivity;
import com.iqonic.store.models.Method;
import com.iqonic.store.models.Shipping;
import com.iqonic.store.utils.Constants;
import com.iqonic.store.utils.extensions.ExtensionsKt;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Ref;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\b\u0003\n\u0002\b\u0004\u0010\u0000\u001a\u00020\u0001\"\b\b\u0000\u0010\u0002*\u00020\u00032\u000e\u0010\u0004\u001a\n \u0005*\u0004\u0018\u00010\u00030\u0003H\n¢\u0006\u0002\b\u0006¨\u0006\u0007"}, d2 = {"<anonymous>", "", "T", "Landroid/view/View;", "it", "kotlin.jvm.PlatformType", "onClick", "com/iqonic/store/utils/extensions/ExtensionsKt$onClick$1"}, k = 3, mv = {1, 1, 16})
/* compiled from: Extensions.kt */
public final class MyCartFragment$onViewCreated$$inlined$onClick$1 implements View.OnClickListener {
    final /* synthetic */ View $this_onClick;
    final /* synthetic */ MyCartFragment this$0;

    public MyCartFragment$onViewCreated$$inlined$onClick$1(View view, MyCartFragment myCartFragment) {
        this.$this_onClick = view;
        this.this$0 = myCartFragment;
    }

    public final void onClick(View view) {
        MaterialButton materialButton = (MaterialButton) this.$this_onClick;
        Shipping access$getShipping$p = this.this$0.shipping;
        String country = access$getShipping$p != null ? access$getShipping$p.getCountry() : null;
        if (country == null) {
            Intrinsics.throwNpe();
        }
        if (country.length() > 0) {
            Ref.ObjectRef objectRef = new Ref.ObjectRef();
            objectRef.element = (Method) null;
            double access$getMTotalCount$p = this.this$0.mTotalCount + Double.parseDouble(this.this$0.mShippingCost);
            if (!this.this$0.shippingMethodsAvailble.isEmpty()) {
                if (this.this$0.selectedMethod == -1) {
                    FragmentActivity activity = this.this$0.getActivity();
                    if (activity != null) {
                        ExtensionsKt.toast$default((Activity) activity, "Select Shipping Method", 0, 2, (Object) null);
                        return;
                    }
                    return;
                }
                objectRef.element = (Method) this.this$0.shippingMethodsAvailble.get(this.this$0.selectedMethod);
            }
            MyCartFragment myCartFragment = this.this$0;
            Intent intent = new Intent(ShopHopApp.Companion.getAppInstance(), OrderSummaryActivity.class);
            new MyCartFragment$onViewCreated$$inlined$onClick$1$lambda$1(access$getMTotalCount$p, objectRef, this).invoke(intent);
            myCartFragment.startActivityForResult(intent, Constants.RequestCode.ORDER_SUMMARY, (Bundle) null);
            return;
        }
        Toast.makeText(materialButton.getContext(), "You do not provided shipping address.", 0).show();
    }
}
