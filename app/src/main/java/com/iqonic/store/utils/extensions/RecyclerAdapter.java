package com.iqonic.store.utils.extensions;

import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import com.google.firebase.analytics.FirebaseAnalytics;
import java.util.List;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.functions.Function3;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000H\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0011\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010!\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u0000\u0018\u0000*\u0004\b\u0000\u0010\u00012\b\u0012\u0004\u0012\u00020\u00030\u0002:\u0001\u001eB\u0001\b\u0016\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00028\u00000\u0007\u0012\b\b\u0002\u0010\b\u001a\u00020\u0005\u0012#\u0010\t\u001a\u001f\u0012\u0004\u0012\u00020\u000b\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\f0\n¢\u0006\u0002\b\r\u0012\u001a\b\u0002\u0010\u000e\u001a\u0014\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\f0\u000f\u0012\u001a\b\u0002\u0010\u0010\u001a\u0014\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\f0\u000f¢\u0006\u0002\u0010\u0011B\u0001\b\u0016\u0012\b\b\u0002\u0010\b\u001a\u00020\u0005\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00028\u00000\u0012\u0012#\u0010\t\u001a\u001f\u0012\u0004\u0012\u00020\u000b\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\f0\n¢\u0006\u0002\b\r\u0012\u001a\b\u0002\u0010\u000e\u001a\u0014\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\f0\u000f\u0012\u001a\b\u0002\u0010\u0010\u001a\u0014\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\f0\u000f¢\u0006\u0002\u0010\u0013J\b\u0010\u0016\u001a\u00020\u0005H\u0016J\u0018\u0010\u0017\u001a\u00020\f2\u0006\u0010\u0018\u001a\u00020\u00032\u0006\u0010\u0019\u001a\u00020\u0005H\u0016J\u0018\u0010\u001a\u001a\u00020\u00032\u0006\u0010\u001b\u001a\u00020\u001c2\u0006\u0010\u001d\u001a\u00020\u0005H\u0016R \u0010\u000e\u001a\u0014\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\f0\u000fX\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u0004\u001a\u00020\u00058\u0002X\u0004¢\u0006\u0002\n\u0000R \u0010\u0010\u001a\u0014\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\f0\u000fX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0005X\u000e¢\u0006\u0002\n\u0000R\u0018\u0010\u0006\u001a\n\u0012\u0004\u0012\u00028\u0000\u0018\u00010\u0007X\u000e¢\u0006\u0004\n\u0002\u0010\u0014R\u0016\u0010\u0015\u001a\n\u0012\u0004\u0012\u00028\u0000\u0018\u00010\u0012X\u000e¢\u0006\u0002\n\u0000R+\u0010\t\u001a\u001f\u0012\u0004\u0012\u00020\u000b\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\f0\n¢\u0006\u0002\b\rX\u0004¢\u0006\u0002\n\u0000¨\u0006\u001f"}, d2 = {"Lcom/iqonic/store/utils/extensions/RecyclerAdapter;", "T", "Landroidx/recyclerview/widget/RecyclerView$Adapter;", "Lcom/iqonic/store/utils/extensions/RecyclerAdapter$ViewHolder;", "itemLayout", "", "items", "", "itemSize", "onBindView", "Lkotlin/Function3;", "Landroid/view/View;", "", "Lkotlin/ExtensionFunctionType;", "itemClick", "Lkotlin/Function2;", "itemLongClick", "(I[Ljava/lang/Object;ILkotlin/jvm/functions/Function3;Lkotlin/jvm/functions/Function2;Lkotlin/jvm/functions/Function2;)V", "", "(IILjava/util/List;Lkotlin/jvm/functions/Function3;Lkotlin/jvm/functions/Function2;Lkotlin/jvm/functions/Function2;)V", "[Ljava/lang/Object;", "itemsList", "getItemCount", "onBindViewHolder", "holder", "position", "onCreateViewHolder", "parent", "Landroid/view/ViewGroup;", "viewType", "ViewHolder", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: AdapterExtensions.kt */
public final class RecyclerAdapter<T> extends RecyclerView.Adapter<ViewHolder> {
    /* access modifiers changed from: private */
    public final Function2<T, Integer, Unit> itemClick;
    private final int itemLayout;
    /* access modifiers changed from: private */
    public final Function2<T, Integer, Unit> itemLongClick;
    private int itemSize;
    private T[] items;
    private List<T> itemsList;
    private final Function3<View, T, Integer, Unit> onBindView;

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ RecyclerAdapter(int i, Object[] objArr, int i2, Function3 function3, Function2 function2, Function2 function22, int i3, DefaultConstructorMarker defaultConstructorMarker) {
        this(i, (T[]) objArr, (i3 & 4) != 0 ? 0 : i2, function3, (i3 & 16) != 0 ? AnonymousClass1.INSTANCE : function2, (i3 & 32) != 0 ? AnonymousClass2.INSTANCE : function22);
    }

    public RecyclerAdapter(int i, T[] tArr, int i2, Function3<? super View, ? super T, ? super Integer, Unit> function3, Function2<? super T, ? super Integer, Unit> function2, Function2<? super T, ? super Integer, Unit> function22) {
        Intrinsics.checkParameterIsNotNull(tArr, FirebaseAnalytics.Param.ITEMS);
        Intrinsics.checkParameterIsNotNull(function3, "onBindView");
        Intrinsics.checkParameterIsNotNull(function2, "itemClick");
        Intrinsics.checkParameterIsNotNull(function22, "itemLongClick");
        this.itemLayout = i;
        this.items = tArr;
        this.itemSize = i2;
        this.onBindView = function3;
        this.itemClick = function2;
        this.itemLongClick = function22;
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ RecyclerAdapter(int i, int i2, List list, Function3 function3, Function2 function2, Function2 function22, int i3, DefaultConstructorMarker defaultConstructorMarker) {
        this((i3 & 1) != 0 ? 0 : i, i2, list, function3, (i3 & 16) != 0 ? AnonymousClass3.INSTANCE : function2, (i3 & 32) != 0 ? AnonymousClass4.INSTANCE : function22);
    }

    public RecyclerAdapter(int i, int i2, List<T> list, Function3<? super View, ? super T, ? super Integer, Unit> function3, Function2<? super T, ? super Integer, Unit> function2, Function2<? super T, ? super Integer, Unit> function22) {
        Intrinsics.checkParameterIsNotNull(list, FirebaseAnalytics.Param.ITEMS);
        Intrinsics.checkParameterIsNotNull(function3, "onBindView");
        Intrinsics.checkParameterIsNotNull(function2, "itemClick");
        Intrinsics.checkParameterIsNotNull(function22, "itemLongClick");
        this.itemLayout = i2;
        this.itemsList = list;
        this.itemSize = i;
        this.onBindView = function3;
        this.itemClick = function2;
        this.itemLongClick = function22;
    }

    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        Intrinsics.checkParameterIsNotNull(viewGroup, "parent");
        return new ViewHolder(ExtensionsKt.inflate(viewGroup, this.itemLayout));
    }

    public int getItemCount() {
        return this.itemSize;
    }

    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        T t;
        Intrinsics.checkParameterIsNotNull(viewHolder, "holder");
        T[] tArr = this.items;
        if (tArr == null) {
            List<T> list = this.itemsList;
            if (list == null) {
                Intrinsics.throwNpe();
            }
            t = list.get(i);
        } else {
            if (tArr == null) {
                Intrinsics.throwNpe();
            }
            t = tArr[i];
        }
        viewHolder.itemView.setOnClickListener(new RecyclerAdapter$onBindViewHolder$1(this, t, i));
        viewHolder.itemView.setOnLongClickListener(new RecyclerAdapter$onBindViewHolder$2(this, t, i));
        Function3<View, T, Integer, Unit> function3 = this.onBindView;
        View view = viewHolder.itemView;
        Intrinsics.checkExpressionValueIsNotNull(view, "holder.itemView");
        function3.invoke(view, t, Integer.valueOf(i));
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0000\u0018\u00002\u00020\u0001B\u000f\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003¢\u0006\u0002\u0010\u0004¨\u0006\u0005"}, d2 = {"Lcom/iqonic/store/utils/extensions/RecyclerAdapter$ViewHolder;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "itemView", "Landroid/view/View;", "(Landroid/view/View;)V", "app_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: AdapterExtensions.kt */
    public static final class ViewHolder extends RecyclerView.ViewHolder {
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public ViewHolder(View view) {
            super(view);
            if (view == null) {
                Intrinsics.throwNpe();
            }
        }
    }
}
