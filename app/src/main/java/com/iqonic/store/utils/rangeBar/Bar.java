package com.iqonic.store.utils.rangeBar;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.util.TypedValue;
import com.store.proshop.R;
import java.util.ArrayList;
import java.util.List;

public class Bar {
    private final Paint mBarPaint;
    private Paint mLabelPaint;
    private final float mLeftX;
    private final Resources mRes;
    private final Paint mTickPaint;
    private final float mRightX;
    private final float mY;
    private int mNumSegments;
    private CharSequence[] mTickBottomLabels;
    private List<Integer> mTickColors;
    private float mTickDistance;
    private final float mTickHeight;
    private int mTickLabelColor;
    private int mTickLabelSelectedColor;
    private int mTickDefaultColor;
    private String mTickDefaultLabel;
    private CharSequence[] mTickTopLabels;
    private float mTickLabelSize;
    private Typeface plain;

    public Bar(Context context, float f, float f2, float f3, int i, float f4, float f5, int i2, boolean z) {
        this.mTickColors = new ArrayList();
        this.mRes = context.getResources();
        this.plain = Typeface.createFromAsset(context.getAssets(), context.getString(R.string.font_Medium));
        this.mLeftX = f;
        this.mRightX = f + f3;
        this.mY = f2;
        int i3 = i - 1;
        this.mNumSegments = i3;
        this.mTickDistance = f3 / ((float) i3);
        this.mTickHeight = f4;
        Paint paint = new Paint();
        this.mBarPaint = paint;
        paint.setColor(i2);
        this.mBarPaint.setStrokeWidth(f5);
        this.mBarPaint.setAntiAlias(true);
        if (z) {
            this.mBarPaint.setStrokeCap(Paint.Cap.ROUND);
        }
        Paint paint2 = new Paint();
        this.mTickPaint = paint2;
        paint2.setStrokeWidth(f5);
        this.mTickPaint.setAntiAlias(true);
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public Bar(Context context, float f, float f2, float f3, int i, float f4, int i2, float f5, int i3, boolean z) {
        this(context, f, f2, f3, i, f4, f5, i3, z);
        int i4 = i2;
        this.mTickDefaultColor = i4;
        this.mTickPaint.setColor(i4);
    }

    public Bar(Context context, float f, float f2, float f3, int i, float f4, float f5, int i2, boolean z, int i3, int i4, CharSequence[] charSequenceArr, CharSequence[] charSequenceArr2, String str, float f6) {
        this(context, f, f2, f3, i, f4, f5, i2, z);
        if (charSequenceArr != null || charSequenceArr2 != null) {
            Paint paint = new Paint();
            this.mLabelPaint = paint;
            paint.setColor(i3);
            this.mLabelPaint.setAntiAlias(true);
            this.mTickLabelColor = i3;
            this.mTickLabelSelectedColor = i4;
            this.mTickTopLabels = charSequenceArr;
            this.mTickBottomLabels = charSequenceArr2;
            this.mTickDefaultLabel = str;
            this.mTickLabelSize = f6;
        }
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public Bar(Context context, float f, float f2, float f3, int i, float f4, int i2, float f5, int i3, boolean z, int i4, int i5, CharSequence[] charSequenceArr, CharSequence[] charSequenceArr2, String str, float f6) {
        this(context, f, f2, f3, i, f4, f5, i3, z, i4, i5, charSequenceArr, charSequenceArr2, str, f6);
        int i6 = i2;
        int i7 = i2;
        this.mTickDefaultColor = i7;
        this.mTickPaint.setColor(i7);
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public Bar(Context context, float f, float f2, float f3, int i, float f4, int i2, List<Integer> list, float f5, int i3, boolean z, int i4, int i5, CharSequence[] charSequenceArr, CharSequence[] charSequenceArr2, String str, float f6) {
        this(context, f, f2, f3, i, f4, f5, i3, z, i4, i5, charSequenceArr, charSequenceArr2, str, f6);
        this.mTickDefaultColor = i2;
        this.mTickColors = list;
    }

    public void draw(Canvas canvas) {
        float f = this.mLeftX;
        float f2 = this.mY;
        canvas.drawLine(f, f2, this.mRightX, f2, this.mBarPaint);
    }

    public float getLeftX() {
        return this.mLeftX;
    }

    public float getRightX() {
        return this.mRightX;
    }

    public float getNearestTickCoordinate(PinView pinView) {
        return this.mLeftX + (((float) getNearestTickIndex(pinView)) * this.mTickDistance);
    }

    public int getNearestTickIndex(PinView pinView) {
        float x = pinView.getX() - this.mLeftX;
        float f = this.mTickDistance;
        int i = (int) ((x + (f / 2.0f)) / f);
        int i2 = this.mNumSegments;
        if (i > i2) {
            return i2;
        }
        if (i < 0) {
            return 0;
        }
        return i;
    }

    public void setTickCount(int i) {
        int i2 = i - 1;
        this.mNumSegments = i2;
        this.mTickDistance = (this.mRightX - this.mLeftX) / ((float) i2);
    }

    private String getTickLabel(int i, CharSequence[] charSequenceArr) {
        if (i >= charSequenceArr.length) {
            return this.mTickDefaultLabel;
        }
        return charSequenceArr[i].toString();
    }

    private String getTickTopLabel(int i) {
        return getTickLabel(i, this.mTickTopLabels);
    }

    private String getTickBottomLabel(int i) {
        return getTickLabel(i, this.mTickBottomLabels);
    }

    public void drawTicks(Canvas canvas, float f, PinView pinView) {
        drawTicks(canvas, f, pinView, (PinView) null);
    }

    public void drawTicks(Canvas canvas, float f, PinView pinView, PinView pinView2) {
        boolean z;
        int i;
        float f2;
        Canvas canvas2 = canvas;
        if (this.mLabelPaint != null) {
            TypedValue.applyDimension(2, this.mTickLabelSize, this.mRes.getDisplayMetrics());
            this.mLabelPaint.setTextSize(17.0f);
            this.mLabelPaint.setTypeface(this.plain);
            z = true;
        } else {
            z = false;
        }
        int i2 = 0;
        while (true) {
            i = this.mNumSegments;
            if (i2 >= i) {
                break;
            }
            float f3 = (((float) i2) * this.mTickDistance) + this.mLeftX;
            canvas2.drawCircle(f3, this.mY, this.mTickHeight, getTick(i2));
            if (z) {
                if (this.mTickTopLabels != null) {
                    f2 = f3;
                    drawTickLabel(canvas, getTickTopLabel(i2), f3, f, i2 == 0, false, true, pinView, pinView2);
                } else {
                    f2 = f3;
                }
                if (this.mTickBottomLabels != null) {
                    drawTickLabel(canvas, getTickBottomLabel(i2), f2, f, i2 == 0, false, false, pinView, pinView2);
                }
            }
            i2++;
        }
        canvas2.drawCircle(this.mRightX, this.mY, this.mTickHeight, getTick(i));
        if (z) {
            if (this.mTickTopLabels != null) {
                drawTickLabel(canvas, getTickTopLabel(this.mNumSegments), this.mRightX, f, false, true, true, pinView, pinView2);
            }
            if (this.mTickBottomLabels != null) {
                drawTickLabel(canvas, getTickBottomLabel(this.mNumSegments), this.mRightX, f, false, true, false, pinView, pinView2);
            }
        }
    }

    private void drawTickLabel(Canvas canvas, String str, float f, float f2, boolean z, boolean z2, boolean z3, PinView pinView, PinView pinView2) {
        float f3;
        Rect rect = new Rect();
        boolean z4 = false;
        this.mLabelPaint.getTextBounds(str, 0, str.length(), rect);
        float width = f - ((float) (rect.width() / 2));
        if (z) {
            width += this.mTickHeight;
        } else if (z2) {
            width -= this.mTickHeight;
        }
        boolean z5 = pinView.getX() == f;
        if (!z5 && pinView2 != null) {
            if (pinView2.getX() == f) {
                z4 = true;
            }
            z5 = z4;
        }
        if (z5) {
            this.mLabelPaint.setColor(this.mTickLabelSelectedColor);
        } else {
            this.mLabelPaint.setColor(this.mTickLabelColor);
        }
        if (z3) {
            f3 = (this.mY - ((float) rect.height())) - f2;
        } else {
            f3 = this.mY + ((float) rect.height()) + f2;
        }
        canvas.drawText(str, width, f3, this.mLabelPaint);
    }

    private Paint getTick(int i) {
        List<Integer> list = this.mTickColors;
        if (list == null || i >= list.size()) {
            this.mTickPaint.setColor(this.mTickDefaultColor);
        } else {
            this.mTickPaint.setColor(this.mTickColors.get(i).intValue());
        }
        return this.mTickPaint;
    }
}
