package com.iqonic.store.utils;

import android.content.Intent;
import android.net.Uri;
import com.facebook.internal.NativeProtocol;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0003\u000b\f\rB\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0018\u0010\u0003\u001a\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u0007H&J\u0010\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u0005H&\u0001\u0003\u000e\u000f\u0010¨\u0006\u0011"}, d2 = {"Lcom/iqonic/store/utils/Instructions;", "", "()V", "execution", "Lkotlin/Function1;", "Landroid/content/Intent;", "", "Lcom/iqonic/store/utils/Execution;", "matches", "", "intent", "OnAction", "OnCategory", "OnDataScheme", "Lcom/iqonic/store/utils/Instructions$OnAction;", "Lcom/iqonic/store/utils/Instructions$OnDataScheme;", "Lcom/iqonic/store/utils/Instructions$OnCategory;", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: BroadcastReceiverExt.kt */
public abstract class Instructions {
    public abstract Function1<Intent, Unit> execution();

    public abstract boolean matches(Intent intent);

    private Instructions() {
    }

    public /* synthetic */ Instructions(DefaultConstructorMarker defaultConstructorMarker) {
        this();
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0004\b\b\u0018\u00002\u00020\u0001B%\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0016\u0010\u0004\u001a\u0012\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00070\u0005j\u0002`\b¢\u0006\u0002\u0010\tJ\t\u0010\u000e\u001a\u00020\u0003HÆ\u0003J\u0019\u0010\u000f\u001a\u0012\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00070\u0005j\u0002`\bHÆ\u0003J-\u0010\u0010\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\u0018\b\u0002\u0010\u0004\u001a\u0012\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00070\u0005j\u0002`\bHÆ\u0001J\u0013\u0010\u0011\u001a\u00020\u00122\b\u0010\u0013\u001a\u0004\u0018\u00010\u0014HÖ\u0003J\u0018\u0010\u0004\u001a\u0012\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00070\u0005j\u0002`\bH\u0016J\t\u0010\u0015\u001a\u00020\u0016HÖ\u0001J\u0010\u0010\u0017\u001a\u00020\u00122\u0006\u0010\u0018\u001a\u00020\u0006H\u0016J\t\u0010\u0019\u001a\u00020\u0003HÖ\u0001R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR!\u0010\u0004\u001a\u0012\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00070\u0005j\u0002`\b¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\r¨\u0006\u001a"}, d2 = {"Lcom/iqonic/store/utils/Instructions$OnAction;", "Lcom/iqonic/store/utils/Instructions;", "action", "", "execution", "Lkotlin/Function1;", "Landroid/content/Intent;", "", "Lcom/iqonic/store/utils/Execution;", "(Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V", "getAction", "()Ljava/lang/String;", "getExecution", "()Lkotlin/jvm/functions/Function1;", "component1", "component2", "copy", "equals", "", "other", "", "hashCode", "", "matches", "intent", "toString", "app_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: BroadcastReceiverExt.kt */
    public static final class OnAction extends Instructions {
        private final String action;
        private final Function1<Intent, Unit> execution;

        public static /* synthetic */ OnAction copy$default(OnAction onAction, String str, Function1<Intent, Unit> function1, int i, Object obj) {
            if ((i & 1) != 0) {
                str = onAction.action;
            }
            if ((i & 2) != 0) {
                function1 = onAction.execution;
            }
            return onAction.copy(str, function1);
        }

        public final String component1() {
            return this.action;
        }

        public final Function1<Intent, Unit> component2() {
            return this.execution;
        }

        public final OnAction copy(String str, Function1<? super Intent, Unit> function1) {
            Intrinsics.checkParameterIsNotNull(str, NativeProtocol.WEB_DIALOG_ACTION);
            Intrinsics.checkParameterIsNotNull(function1, "execution");
            return new OnAction(str, function1);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof OnAction)) {
                return false;
            }
            OnAction onAction = (OnAction) obj;
            return Intrinsics.areEqual((Object) this.action, (Object) onAction.action) && Intrinsics.areEqual((Object) this.execution, (Object) onAction.execution);
        }

        public int hashCode() {
            String str = this.action;
            int i = 0;
            int hashCode = (str != null ? str.hashCode() : 0) * 31;
            Function1<Intent, Unit> function1 = this.execution;
            if (function1 != null) {
                i = function1.hashCode();
            }
            return hashCode + i;
        }

        public String toString() {
            return "OnAction(action=" + this.action + ", execution=" + this.execution + ")";
        }

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public OnAction(String str, Function1<? super Intent, Unit> function1) {
            super((DefaultConstructorMarker) null);
            Intrinsics.checkParameterIsNotNull(str, NativeProtocol.WEB_DIALOG_ACTION);
            Intrinsics.checkParameterIsNotNull(function1, "execution");
            this.action = str;
            this.execution = function1;
        }

        public final String getAction() {
            return this.action;
        }

        public final Function1<Intent, Unit> getExecution() {
            return this.execution;
        }

        public boolean matches(Intent intent) {
            Intrinsics.checkParameterIsNotNull(intent, "intent");
            return Intrinsics.areEqual((Object) intent.getAction(), (Object) this.action);
        }

        public Function1<Intent, Unit> execution() {
            return this.execution;
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0004\b\b\u0018\u00002\u00020\u0001B%\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0016\u0010\u0004\u001a\u0012\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00070\u0005j\u0002`\b¢\u0006\u0002\u0010\tJ\t\u0010\u000e\u001a\u00020\u0003HÆ\u0003J\u0019\u0010\u000f\u001a\u0012\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00070\u0005j\u0002`\bHÆ\u0003J-\u0010\u0010\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\u0018\b\u0002\u0010\u0004\u001a\u0012\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00070\u0005j\u0002`\bHÆ\u0001J\u0013\u0010\u0011\u001a\u00020\u00122\b\u0010\u0013\u001a\u0004\u0018\u00010\u0014HÖ\u0003J\u0018\u0010\u0004\u001a\u0012\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00070\u0005j\u0002`\bH\u0016J\t\u0010\u0015\u001a\u00020\u0016HÖ\u0001J\u0010\u0010\u0017\u001a\u00020\u00122\u0006\u0010\u0018\u001a\u00020\u0006H\u0016J\t\u0010\u0019\u001a\u00020\u0003HÖ\u0001R!\u0010\u0004\u001a\u0012\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00070\u0005j\u0002`\b¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\r¨\u0006\u001a"}, d2 = {"Lcom/iqonic/store/utils/Instructions$OnDataScheme;", "Lcom/iqonic/store/utils/Instructions;", "scheme", "", "execution", "Lkotlin/Function1;", "Landroid/content/Intent;", "", "Lcom/iqonic/store/utils/Execution;", "(Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V", "getExecution", "()Lkotlin/jvm/functions/Function1;", "getScheme", "()Ljava/lang/String;", "component1", "component2", "copy", "equals", "", "other", "", "hashCode", "", "matches", "intent", "toString", "app_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: BroadcastReceiverExt.kt */
    public static final class OnDataScheme extends Instructions {
        private final Function1<Intent, Unit> execution;
        private final String scheme;

        public static /* synthetic */ OnDataScheme copy$default(OnDataScheme onDataScheme, String str, Function1<Intent, Unit> function1, int i, Object obj) {
            if ((i & 1) != 0) {
                str = onDataScheme.scheme;
            }
            if ((i & 2) != 0) {
                function1 = onDataScheme.execution;
            }
            return onDataScheme.copy(str, function1);
        }

        public final String component1() {
            return this.scheme;
        }

        public final Function1<Intent, Unit> component2() {
            return this.execution;
        }

        public final OnDataScheme copy(String str, Function1<? super Intent, Unit> function1) {
            Intrinsics.checkParameterIsNotNull(str, "scheme");
            Intrinsics.checkParameterIsNotNull(function1, "execution");
            return new OnDataScheme(str, function1);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof OnDataScheme)) {
                return false;
            }
            OnDataScheme onDataScheme = (OnDataScheme) obj;
            return Intrinsics.areEqual((Object) this.scheme, (Object) onDataScheme.scheme) && Intrinsics.areEqual((Object) this.execution, (Object) onDataScheme.execution);
        }

        public int hashCode() {
            String str = this.scheme;
            int i = 0;
            int hashCode = (str != null ? str.hashCode() : 0) * 31;
            Function1<Intent, Unit> function1 = this.execution;
            if (function1 != null) {
                i = function1.hashCode();
            }
            return hashCode + i;
        }

        public String toString() {
            return "OnDataScheme(scheme=" + this.scheme + ", execution=" + this.execution + ")";
        }

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public OnDataScheme(String str, Function1<? super Intent, Unit> function1) {
            super((DefaultConstructorMarker) null);
            Intrinsics.checkParameterIsNotNull(str, "scheme");
            Intrinsics.checkParameterIsNotNull(function1, "execution");
            this.scheme = str;
            this.execution = function1;
        }

        public final Function1<Intent, Unit> getExecution() {
            return this.execution;
        }

        public final String getScheme() {
            return this.scheme;
        }

        public boolean matches(Intent intent) {
            Intrinsics.checkParameterIsNotNull(intent, "intent");
            Uri data = intent.getData();
            return Intrinsics.areEqual((Object) data != null ? data.getScheme() : null, (Object) this.scheme);
        }

        public Function1<Intent, Unit> execution() {
            return this.execution;
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0004\b\b\u0018\u00002\u00020\u0001B%\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0016\u0010\u0004\u001a\u0012\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00070\u0005j\u0002`\b¢\u0006\u0002\u0010\tJ\t\u0010\u000e\u001a\u00020\u0003HÆ\u0003J\u0019\u0010\u000f\u001a\u0012\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00070\u0005j\u0002`\bHÆ\u0003J-\u0010\u0010\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\u0018\b\u0002\u0010\u0004\u001a\u0012\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00070\u0005j\u0002`\bHÆ\u0001J\u0013\u0010\u0011\u001a\u00020\u00122\b\u0010\u0013\u001a\u0004\u0018\u00010\u0014HÖ\u0003J\u0018\u0010\u0004\u001a\u0012\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00070\u0005j\u0002`\bH\u0016J\t\u0010\u0015\u001a\u00020\u0016HÖ\u0001J\u0010\u0010\u0017\u001a\u00020\u00122\u0006\u0010\u0018\u001a\u00020\u0006H\u0016J\t\u0010\u0019\u001a\u00020\u0003HÖ\u0001R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR!\u0010\u0004\u001a\u0012\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00070\u0005j\u0002`\b¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\r¨\u0006\u001a"}, d2 = {"Lcom/iqonic/store/utils/Instructions$OnCategory;", "Lcom/iqonic/store/utils/Instructions;", "category", "", "execution", "Lkotlin/Function1;", "Landroid/content/Intent;", "", "Lcom/iqonic/store/utils/Execution;", "(Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V", "getCategory", "()Ljava/lang/String;", "getExecution", "()Lkotlin/jvm/functions/Function1;", "component1", "component2", "copy", "equals", "", "other", "", "hashCode", "", "matches", "intent", "toString", "app_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: BroadcastReceiverExt.kt */
    public static final class OnCategory extends Instructions {
        private final String category;
        private final Function1<Intent, Unit> execution;

        public static /* synthetic */ OnCategory copy$default(OnCategory onCategory, String str, Function1<Intent, Unit> function1, int i, Object obj) {
            if ((i & 1) != 0) {
                str = onCategory.category;
            }
            if ((i & 2) != 0) {
                function1 = onCategory.execution;
            }
            return onCategory.copy(str, function1);
        }

        public final String component1() {
            return this.category;
        }

        public final Function1<Intent, Unit> component2() {
            return this.execution;
        }

        public final OnCategory copy(String str, Function1<? super Intent, Unit> function1) {
            Intrinsics.checkParameterIsNotNull(str, "category");
            Intrinsics.checkParameterIsNotNull(function1, "execution");
            return new OnCategory(str, function1);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof OnCategory)) {
                return false;
            }
            OnCategory onCategory = (OnCategory) obj;
            return Intrinsics.areEqual((Object) this.category, (Object) onCategory.category) && Intrinsics.areEqual((Object) this.execution, (Object) onCategory.execution);
        }

        public int hashCode() {
            String str = this.category;
            int i = 0;
            int hashCode = (str != null ? str.hashCode() : 0) * 31;
            Function1<Intent, Unit> function1 = this.execution;
            if (function1 != null) {
                i = function1.hashCode();
            }
            return hashCode + i;
        }

        public String toString() {
            return "OnCategory(category=" + this.category + ", execution=" + this.execution + ")";
        }

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public OnCategory(String str, Function1<? super Intent, Unit> function1) {
            super((DefaultConstructorMarker) null);
            Intrinsics.checkParameterIsNotNull(str, "category");
            Intrinsics.checkParameterIsNotNull(function1, "execution");
            this.category = str;
            this.execution = function1;
        }

        public final String getCategory() {
            return this.category;
        }

        public final Function1<Intent, Unit> getExecution() {
            return this.execution;
        }

        public boolean matches(Intent intent) {
            Intrinsics.checkParameterIsNotNull(intent, "intent");
            return intent.hasCategory(this.category);
        }

        public Function1<Intent, Unit> execution() {
            return this.execution;
        }
    }
}
