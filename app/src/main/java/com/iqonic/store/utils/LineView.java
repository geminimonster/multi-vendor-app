package com.iqonic.store.utils;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;
import com.iqonic.store.R;
import java.util.HashMap;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u0000 \u00142\u00020\u0001:\u0001\u0014B!\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\b\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bB\u0019\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\b\u0010\u0004\u001a\u0004\u0018\u00010\u0005¢\u0006\u0002\u0010\tB\u000f\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\nJ\u001a\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0002\u001a\u00020\u00032\b\u0010\u0004\u001a\u0004\u0018\u00010\u0005H\u0002J\u0010\u0010\u0010\u001a\u00020\u000f2\u0006\u0010\u0011\u001a\u00020\u0012H\u0014J\u000e\u0010\u0013\u001a\u00020\u000f2\u0006\u0010\u000b\u001a\u00020\u0007R\u000e\u0010\u000b\u001a\u00020\u0007X\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\f\u001a\u0004\u0018\u00010\rX\u000e¢\u0006\u0002\n\u0000¨\u0006\u0015"}, d2 = {"Lcom/iqonic/store/utils/LineView;", "Landroid/view/View;", "context", "Landroid/content/Context;", "attrs", "Landroid/util/AttributeSet;", "defStyleAttr", "", "(Landroid/content/Context;Landroid/util/AttributeSet;I)V", "(Landroid/content/Context;Landroid/util/AttributeSet;)V", "(Landroid/content/Context;)V", "mLineColor", "mLinePaint", "Landroid/graphics/Paint;", "initLine", "", "onDraw", "canvas", "Landroid/graphics/Canvas;", "setLineColor", "Companion", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: LineView.kt */
public final class LineView extends View {
    public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
    private static final int DEFAULT_TEXT_COLOR = DEFAULT_TEXT_COLOR;
    private HashMap _$_findViewCache;
    private int mLineColor;
    private Paint mLinePaint;

    public void _$_clearFindViewByIdCache() {
        HashMap hashMap = this._$_findViewCache;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    public View _$_findCachedViewById(int i) {
        if (this._$_findViewCache == null) {
            this._$_findViewCache = new HashMap();
        }
        View view = (View) this._$_findViewCache.get(Integer.valueOf(i));
        if (view != null) {
            return view;
        }
        View findViewById = findViewById(i);
        this._$_findViewCache.put(Integer.valueOf(i), findViewById);
        return findViewById;
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public LineView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        Intrinsics.checkParameterIsNotNull(context, "context");
        initLine(context, attributeSet);
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public LineView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        Intrinsics.checkParameterIsNotNull(context, "context");
        initLine(context, attributeSet);
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public LineView(Context context) {
        super(context);
        Intrinsics.checkParameterIsNotNull(context, "context");
    }

    private final void initLine(Context context, AttributeSet attributeSet) {
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, R.styleable.LineView, 0, 0);
        try {
            this.mLineColor = obtainStyledAttributes.getColor(2, DEFAULT_TEXT_COLOR);
            Paint paint = new Paint();
            this.mLinePaint = paint;
            if (paint == null) {
                Intrinsics.throwNpe();
            }
            paint.setAntiAlias(true);
            Paint paint2 = this.mLinePaint;
            if (paint2 == null) {
                Intrinsics.throwNpe();
            }
            paint2.setColor(this.mLineColor);
            Paint paint3 = this.mLinePaint;
            if (paint3 == null) {
                Intrinsics.throwNpe();
            }
            paint3.setStyle(Paint.Style.STROKE);
            Paint paint4 = this.mLinePaint;
            if (paint4 == null) {
                Intrinsics.throwNpe();
            }
            paint4.setStrokeWidth(5.0f);
            Paint paint5 = this.mLinePaint;
            if (paint5 == null) {
                Intrinsics.throwNpe();
            }
            paint5.setPathEffect(new DashPathEffect(new float[]{(float) 10, (float) 5}, 0.0f));
        } finally {
            obtainStyledAttributes.recycle();
        }
    }

    public final void setLineColor(int i) {
        this.mLineColor = i;
        Paint paint = this.mLinePaint;
        if (paint == null) {
            Intrinsics.throwNpe();
        }
        paint.setColor(i);
        invalidate();
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        Intrinsics.checkParameterIsNotNull(canvas, "canvas");
        super.onDraw(canvas);
        float width = (float) (getWidth() / 2);
        float width2 = (float) (getWidth() / 2);
        float height = (float) (canvas.getHeight() + 0);
        Paint paint = this.mLinePaint;
        if (paint == null) {
            Intrinsics.throwNpe();
        }
        canvas.drawLine(width, 0.0f, width2, height, paint);
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XD¢\u0006\u0002\n\u0000¨\u0006\u0005"}, d2 = {"Lcom/iqonic/store/utils/LineView$Companion;", "", "()V", "DEFAULT_TEXT_COLOR", "", "app_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: LineView.kt */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }
}
