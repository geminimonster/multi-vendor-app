package com.iqonic.store.utils.extensions;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.internal.NativeProtocol;
import com.google.android.material.snackbar.Snackbar;
import com.iqonic.store.ShopHopApp;
import com.store.proshop.R;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.functions.Function3;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000¼\u0001\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0010\u0011\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\f\n\u0002\u0018\u0002\n\u0002\b\u0005\u001a\b\u0010\u0000\u001a\u00020\u0001H\u0007\u001a\u001d\u0010\u0002\u001a\u00020\u0003\"\n\b\u0000\u0010\u0004\u0018\u0001*\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0007H\b\u001a\u001c\u0010\b\u001a\u00020\u00012\u0006\u0010\t\u001a\u00020\n2\f\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\r0\f\u001a\u0016\u0010\u000e\u001a\u00020\r2\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0012\u001a\u001a\u0010\u0013\u001a\u00020\u0014*\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u0014\u001a\u0012\u0010\u0019\u001a\u00020\r*\u00020\u001a2\u0006\u0010\u001b\u001a\u00020\u0014\u001a\n\u0010\u001c\u001a\u00020\r*\u00020\u0010\u001a\n\u0010\u001d\u001a\u00020\r*\u00020\u0010\u001a\n\u0010\u001e\u001a\u00020\r*\u00020\u001f\u001a\u0012\u0010 \u001a\u00020\r*\u00020\u001a2\u0006\u0010\u001b\u001a\u00020\u0012\u001a\u0012\u0010!\u001a\u00020\r*\u00020\u00102\u0006\u0010\u001b\u001a\u00020\u0012\u001a\n\u0010\"\u001a\u00020\r*\u00020\u0010\u001a\n\u0010#\u001a\u00020\r*\u00020\u0010\u001a\n\u0010$\u001a\u00020\r*\u00020\u0010\u001a\u0012\u0010%\u001a\u00020\r*\u00020\u001f2\u0006\u0010\u001b\u001a\u00020\u0012\u001a\n\u0010&\u001a\u00020\r*\u00020\u0010\u001a\n\u0010'\u001a\u00020\r*\u00020(\u001a\u0012\u0010\u001b\u001a\u00020\u0014*\u00020)2\u0006\u0010\u001b\u001a\u00020\u0014\u001a\u0012\u0010*\u001a\u00020\u0014*\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u0017\u001a\n\u0010+\u001a\u00020\r*\u00020,\u001a&\u0010-\u001a\u00020\u0014*\u00020.2\u0017\u0010/\u001a\u0013\u0012\u0004\u0012\u000201\u0012\u0004\u0012\u00020100¢\u0006\u0002\b2H\b\u001a\u0015\u00103\u001a\u00020\u001f*\u0002042\u0006\u00105\u001a\u00020\u0014H\u0004\u001aJ\u00106\u001a\u00020\r\"\n\b\u0000\u0010\u0004\u0018\u0001*\u00020\u0005*\u00020,2\b\b\u0002\u00107\u001a\u00020\u00142\n\b\u0002\u00108\u001a\u0004\u0018\u0001092\u0019\b\n\u0010:\u001a\u0013\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\r00¢\u0006\u0002\b2H\b\u001aJ\u00106\u001a\u00020\r\"\n\b\u0000\u0010\u0004\u0018\u0001*\u00020\u0005*\u00020\u00172\b\b\u0002\u00107\u001a\u00020\u00142\n\b\u0002\u00108\u001a\u0004\u0018\u0001092\u0019\b\n\u0010:\u001a\u0013\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\r00¢\u0006\u0002\b2H\b\u001a\u0019\u0010;\u001a\u00020\r\"\n\b\u0000\u0010\u0004\u0018\u0001*\u00020\u0005*\u00020,H\b\u001a\n\u0010<\u001a\u00020\r*\u00020\u0015\u001a\n\u0010=\u001a\u00020\r*\u00020,\u001af\u0010>\u001a\u0002H\u0004\"\b\b\u0000\u0010\u0004*\u00020?*\u0002H\u00042K\u0010@\u001aG\u0012\u0013\u0012\u00110\u001f¢\u0006\f\bB\u0012\b\bC\u0012\u0004\b\b(D\u0012\u0013\u0012\u00110\u0014¢\u0006\f\bB\u0012\b\bC\u0012\u0004\b\b(E\u0012\u0013\u0012\u00110\u0014¢\u0006\f\bB\u0012\b\bC\u0012\u0004\b\b(F\u0012\u0004\u0012\u00020\r0A¢\u0006\u0002\u0010G\u001a7\u0010>\u001a\u00020\r\"\b\b\u0000\u0010\u0004*\u00020\u001f*\u0002H\u00042\u0019\b\u0004\u0010/\u001a\u0013\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u00020\r00¢\u0006\u0002\b2H\b¢\u0006\u0002\u0010H\u001a\u0012\u0010I\u001a\u00020\u0014*\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u0017\u001a\u001a\u0010J\u001a\u00020\u0014*\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u0014\u001a@\u0010K\u001a\u00020\r*\u00020)2\f\u0010L\u001a\b\u0012\u0004\u0012\u00020\u00120M2!\u0010N\u001a\u001d\u0012\u0013\u0012\u00110\u0001¢\u0006\f\bB\u0012\b\bC\u0012\u0004\b\b(O\u0012\u0004\u0012\u00020\r00¢\u0006\u0002\u0010P\u001a\u0014\u0010Q\u001a\u00020\r*\u00020R2\b\b\u0002\u0010S\u001a\u00020\u0001\u001a\u0014\u0010T\u001a\u00020\r*\u00020R2\b\b\u0002\u0010S\u001a\u00020\u0001\u001a\u0012\u0010U\u001a\u00020\u0014*\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u0017\u001a\u0012\u0010V\u001a\u00020\r*\u00020,2\u0006\u0010D\u001a\u00020\u001f\u001a\u001c\u0010W\u001a\u00020\r*\u00020,2\u0006\u0010X\u001a\u00020\u00122\b\b\u0002\u0010Y\u001a\u00020\u0014\u001a\u0012\u0010W\u001a\u00020\r*\u00020\u00172\u0006\u0010X\u001a\u00020\u0012\u001a\u0012\u0010Z\u001a\u00020\r*\u00020,2\u0006\u0010X\u001a\u00020\u0012\u001a\u0012\u0010[\u001a\u00020\u0014*\u00020)2\u0006\u0010\u001b\u001a\u00020\u0012\u001a\u0012\u0010\\\u001a\u00020\r*\u00020\u00152\u0006\u0010]\u001a\u00020\u0001\u001a\u0016\u0010^\u001a\u00020_*\u00020_2\b\b\u0001\u0010\u001b\u001a\u00020\u0014H\u0000\u001a\u001e\u0010`\u001a\u00020\r*\u00020,2\b\b\u0001\u0010a\u001a\u00020\u00142\b\b\u0002\u0010b\u001a\u00020\u0014\u001a\u001c\u0010`\u001a\u00020\r*\u00020,2\u0006\u0010c\u001a\u00020\u00122\b\b\u0002\u0010b\u001a\u00020\u0014¨\u0006d"}, d2 = {"isNetworkAvailable", "", "newIntent", "Landroid/content/Intent;", "T", "", "context", "Landroid/content/Context;", "runDelayed", "delayMillis", "", "action", "Lkotlin/Function0;", "", "setTextViewDrawableColor", "textView", "Landroid/widget/TextView;", "colors", "", "addFragment", "", "Landroidx/appcompat/app/AppCompatActivity;", "fragment", "Landroidx/fragment/app/Fragment;", "frameId", "applyColorFilter", "Landroid/widget/ImageView;", "color", "applyStrike", "changeAccentColor", "changeBackgroundColor", "Landroid/view/View;", "changeBackgroundImageTint", "changeBackgroundTint", "changePrimaryColor", "changeTextPrimaryColor", "changeTextSecondaryColor", "changeTint", "changeTitleColor", "changeToolbarFont", "Landroidx/appcompat/widget/Toolbar;", "Landroidx/fragment/app/FragmentActivity;", "hideFragment", "hideSoftKeyboard", "Landroid/app/Activity;", "inTransaction", "Landroidx/fragment/app/FragmentManager;", "func", "Lkotlin/Function1;", "Landroidx/fragment/app/FragmentTransaction;", "Lkotlin/ExtensionFunctionType;", "inflate", "Landroid/view/ViewGroup;", "layoutRes", "launchActivity", "requestCode", "options", "Landroid/os/Bundle;", "init", "launchActivityWithNewTask", "makeTransaprant", "noInternetSnackBar", "onClick", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "event", "Lkotlin/Function3;", "Lkotlin/ParameterName;", "name", "view", "position", "type", "(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;Lkotlin/jvm/functions/Function3;)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "(Landroid/view/View;Lkotlin/jvm/functions/Function1;)V", "removeFragment", "replaceFragment", "requestPermissions", "permissions", "", "onResult", "isGranted", "(Landroidx/fragment/app/FragmentActivity;[Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V", "setHorizontalLayout", "Landroidx/recyclerview/widget/RecyclerView;", "aReverseLayout", "setVerticalLayout", "showFragment", "showPermissionAlert", "snackBar", "msg", "length", "snackBarError", "stringColor", "switchToDarkMode", "isDark", "tint", "Landroid/graphics/drawable/Drawable;", "toast", "stringRes", "duration", "text", "app_release"}, k = 2, mv = {1, 1, 16})
/* compiled from: Extensions.kt */
public final class ExtensionsKt {
    public static final boolean isNetworkAvailable() {
        NetworkInfo activeNetworkInfo = ContextExtensionsKt.getConnectivityManager(ShopHopApp.Companion.getAppInstance()).getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public static final <T extends View> void onClick(T t, Function1<? super T, Unit> function1) {
        Intrinsics.checkParameterIsNotNull(t, "$this$onClick");
        Intrinsics.checkParameterIsNotNull(function1, "func");
        t.setOnClickListener(new ExtensionsKt$onClick$1(t, function1));
    }

    public static /* synthetic */ void toast$default(Activity activity, String str, int i, int i2, Object obj) {
        if ((i2 & 2) != 0) {
            i = 0;
        }
        toast(activity, str, i);
    }

    public static final void toast(Activity activity, String str, int i) {
        Intrinsics.checkParameterIsNotNull(activity, "$this$toast");
        Intrinsics.checkParameterIsNotNull(str, "text");
        Toast.makeText(activity, str, i).show();
    }

    public static final int inTransaction(FragmentManager fragmentManager, Function1<? super FragmentTransaction, ? extends FragmentTransaction> function1) {
        Intrinsics.checkParameterIsNotNull(fragmentManager, "$this$inTransaction");
        Intrinsics.checkParameterIsNotNull(function1, "func");
        FragmentTransaction beginTransaction = fragmentManager.beginTransaction();
        Intrinsics.checkExpressionValueIsNotNull(beginTransaction, "beginTransaction()");
        return ((FragmentTransaction) function1.invoke(beginTransaction)).commit();
    }

    public static /* synthetic */ void snackBar$default(Activity activity, String str, int i, int i2, Object obj) {
        if ((i2 & 2) != 0) {
            i = -1;
        }
        snackBar(activity, str, i);
    }

    public static final void snackBar(Activity activity, String str, int i) {
        Intrinsics.checkParameterIsNotNull(activity, "$this$snackBar");
        Intrinsics.checkParameterIsNotNull(str, NotificationCompat.CATEGORY_MESSAGE);
        Snackbar.make(activity.findViewById(16908290), (CharSequence) str, i).setTextColor(ViewExtensionsKt.color(activity, R.color.white)).show();
    }

    public static final void snackBar(Fragment fragment, String str) {
        Intrinsics.checkParameterIsNotNull(fragment, "$this$snackBar");
        Intrinsics.checkParameterIsNotNull(str, NotificationCompat.CATEGORY_MESSAGE);
        FragmentActivity activity = fragment.getActivity();
        if (activity == null) {
            Intrinsics.throwNpe();
        }
        Intrinsics.checkExpressionValueIsNotNull(activity, "activity!!");
        snackBar$default(activity, str, 0, 2, (Object) null);
    }

    public static final void snackBarError(Activity activity, String str) {
        Intrinsics.checkParameterIsNotNull(activity, "$this$snackBarError");
        Intrinsics.checkParameterIsNotNull(str, NotificationCompat.CATEGORY_MESSAGE);
        Snackbar make = Snackbar.make(activity.findViewById(16908290), (CharSequence) str, 0);
        Intrinsics.checkExpressionValueIsNotNull(make, "Snackbar.make(findViewBy…sg, Snackbar.LENGTH_LONG)");
        View view = make.getView();
        Intrinsics.checkExpressionValueIsNotNull(view, "snackBar.view");
        view.setBackgroundColor(ShopHopApp.Companion.getAppInstance().getResources().getColor(R.color.tomato));
        make.setTextColor(-1);
        make.show();
    }

    public static final void noInternetSnackBar(Activity activity) {
        Intrinsics.checkParameterIsNotNull(activity, "$this$noInternetSnackBar");
        String string = ShopHopApp.Companion.getAppInstance().getString(R.string.error_no_internet);
        Intrinsics.checkExpressionValueIsNotNull(string, "getAppInstance().getStri…string.error_no_internet)");
        snackBarError(activity, string);
    }

    public static final void showPermissionAlert(Activity activity, View view) {
        Intrinsics.checkParameterIsNotNull(activity, "$this$showPermissionAlert");
        Intrinsics.checkParameterIsNotNull(view, "view");
        Snackbar make = Snackbar.make(view, (CharSequence) activity.getString(R.string.error_permission_required), -2);
        Intrinsics.checkExpressionValueIsNotNull(make, "Snackbar.make(\n        v…r.LENGTH_INDEFINITE\n    )");
        View view2 = make.getView();
        Intrinsics.checkExpressionValueIsNotNull(view2, "snackBar.view");
        make.setAction((CharSequence) "Enable", (View.OnClickListener) new ExtensionsKt$showPermissionAlert$1(activity, make));
        view2.setBackgroundColor(ContextCompat.getColor(activity, R.color.tomato));
        make.setTextColor(-1);
        make.show();
    }

    public static final int replaceFragment(AppCompatActivity appCompatActivity, Fragment fragment, int i) {
        Intrinsics.checkParameterIsNotNull(appCompatActivity, "$this$replaceFragment");
        Intrinsics.checkParameterIsNotNull(fragment, "fragment");
        FragmentManager supportFragmentManager = appCompatActivity.getSupportFragmentManager();
        Intrinsics.checkExpressionValueIsNotNull(supportFragmentManager, "supportFragmentManager");
        FragmentTransaction beginTransaction = supportFragmentManager.beginTransaction();
        Intrinsics.checkExpressionValueIsNotNull(beginTransaction, "beginTransaction()");
        FragmentTransaction replace = beginTransaction.replace(i, fragment);
        Intrinsics.checkExpressionValueIsNotNull(replace, "replace(frameId, fragment)");
        return replace.commit();
    }

    public static final int addFragment(AppCompatActivity appCompatActivity, Fragment fragment, int i) {
        Intrinsics.checkParameterIsNotNull(appCompatActivity, "$this$addFragment");
        Intrinsics.checkParameterIsNotNull(fragment, "fragment");
        FragmentManager supportFragmentManager = appCompatActivity.getSupportFragmentManager();
        Intrinsics.checkExpressionValueIsNotNull(supportFragmentManager, "supportFragmentManager");
        FragmentTransaction beginTransaction = supportFragmentManager.beginTransaction();
        Intrinsics.checkExpressionValueIsNotNull(beginTransaction, "beginTransaction()");
        FragmentTransaction add = beginTransaction.add(i, fragment);
        Intrinsics.checkExpressionValueIsNotNull(add, "add(frameId, fragment)");
        return add.commit();
    }

    public static final int removeFragment(AppCompatActivity appCompatActivity, Fragment fragment) {
        Intrinsics.checkParameterIsNotNull(appCompatActivity, "$this$removeFragment");
        Intrinsics.checkParameterIsNotNull(fragment, "fragment");
        FragmentManager supportFragmentManager = appCompatActivity.getSupportFragmentManager();
        Intrinsics.checkExpressionValueIsNotNull(supportFragmentManager, "supportFragmentManager");
        FragmentTransaction beginTransaction = supportFragmentManager.beginTransaction();
        Intrinsics.checkExpressionValueIsNotNull(beginTransaction, "beginTransaction()");
        FragmentTransaction remove = beginTransaction.remove(fragment);
        Intrinsics.checkExpressionValueIsNotNull(remove, "remove(fragment)");
        return remove.commit();
    }

    public static final int showFragment(AppCompatActivity appCompatActivity, Fragment fragment) {
        Intrinsics.checkParameterIsNotNull(appCompatActivity, "$this$showFragment");
        Intrinsics.checkParameterIsNotNull(fragment, "fragment");
        FragmentManager supportFragmentManager = appCompatActivity.getSupportFragmentManager();
        Intrinsics.checkExpressionValueIsNotNull(supportFragmentManager, "supportFragmentManager");
        FragmentTransaction beginTransaction = supportFragmentManager.beginTransaction();
        Intrinsics.checkExpressionValueIsNotNull(beginTransaction, "beginTransaction()");
        beginTransaction.setCustomAnimations(17498112, 17498113);
        FragmentTransaction show = beginTransaction.show(fragment);
        Intrinsics.checkExpressionValueIsNotNull(show, "show(fragment)");
        return show.commit();
    }

    public static final int hideFragment(AppCompatActivity appCompatActivity, Fragment fragment) {
        Intrinsics.checkParameterIsNotNull(appCompatActivity, "$this$hideFragment");
        Intrinsics.checkParameterIsNotNull(fragment, "fragment");
        FragmentManager supportFragmentManager = appCompatActivity.getSupportFragmentManager();
        Intrinsics.checkExpressionValueIsNotNull(supportFragmentManager, "supportFragmentManager");
        FragmentTransaction beginTransaction = supportFragmentManager.beginTransaction();
        Intrinsics.checkExpressionValueIsNotNull(beginTransaction, "beginTransaction()");
        FragmentTransaction hide = beginTransaction.hide(fragment);
        Intrinsics.checkExpressionValueIsNotNull(hide, "hide(fragment)");
        return hide.commit();
    }

    public static final void makeTransaprant(AppCompatActivity appCompatActivity) {
        Intrinsics.checkParameterIsNotNull(appCompatActivity, "$this$makeTransaprant");
        appCompatActivity.getWindow().addFlags(512);
        appCompatActivity.getWindow().addFlags(512);
    }

    public static final boolean runDelayed(long j, Function0<Unit> function0) {
        Intrinsics.checkParameterIsNotNull(function0, NativeProtocol.WEB_DIALOG_ACTION);
        return new Handler().postDelayed(new ExtensionsKt$sam$java_lang_Runnable$0(function0), j);
    }

    public static final <T extends RecyclerView.ViewHolder> T onClick(T t, Function3<? super View, ? super Integer, ? super Integer, Unit> function3) {
        Intrinsics.checkParameterIsNotNull(t, "$this$onClick");
        Intrinsics.checkParameterIsNotNull(function3, "event");
        t.itemView.setOnClickListener(new ExtensionsKt$onClick$2(t, function3));
        return t;
    }

    public static final View inflate(ViewGroup viewGroup, int i) {
        Intrinsics.checkParameterIsNotNull(viewGroup, "$this$inflate");
        View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(i, viewGroup, false);
        Intrinsics.checkExpressionValueIsNotNull(inflate, "LayoutInflater.from(cont…e(layoutRes, this, false)");
        return inflate;
    }

    public static /* synthetic */ void toast$default(Activity activity, int i, int i2, int i3, Object obj) {
        if ((i3 & 2) != 0) {
            i2 = 0;
        }
        toast(activity, i, i2);
    }

    public static final void toast(Activity activity, int i, int i2) {
        Intrinsics.checkParameterIsNotNull(activity, "$this$toast");
        Toast.makeText(activity, i, i2).show();
    }

    public static final void hideSoftKeyboard(Activity activity) {
        Intrinsics.checkParameterIsNotNull(activity, "$this$hideSoftKeyboard");
        if (activity.getCurrentFocus() != null) {
            Object systemService = activity.getSystemService("input_method");
            if (systemService != null) {
                InputMethodManager inputMethodManager = (InputMethodManager) systemService;
                View currentFocus = activity.getCurrentFocus();
                if (currentFocus == null) {
                    Intrinsics.throwNpe();
                }
                Intrinsics.checkExpressionValueIsNotNull(currentFocus, "currentFocus!!");
                inputMethodManager.hideSoftInputFromWindow(currentFocus.getWindowToken(), 0);
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type android.view.inputmethod.InputMethodManager");
        }
    }

    public static final void changeToolbarFont(Toolbar toolbar) {
        Intrinsics.checkParameterIsNotNull(toolbar, "$this$changeToolbarFont");
        int childCount = toolbar.getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = toolbar.getChildAt(i);
            if (childAt instanceof TextView) {
                TextView textView = (TextView) childAt;
                if (Intrinsics.areEqual((Object) textView.getText(), (Object) toolbar.getTitle())) {
                    Context context = textView.getContext();
                    Intrinsics.checkExpressionValueIsNotNull(context, "view.context");
                    textView.setTypeface(AppExtensionsKt.fontBold(context));
                    return;
                }
            }
        }
    }

    public static final void applyColorFilter(ImageView imageView, int i) {
        Intrinsics.checkParameterIsNotNull(imageView, "$this$applyColorFilter");
        DrawableCompat.setTint(DrawableCompat.wrap(imageView.getDrawable()), i);
    }

    public static final void applyStrike(TextView textView) {
        Intrinsics.checkParameterIsNotNull(textView, "$this$applyStrike");
        textView.setPaintFlags(textView.getPaintFlags() | 16);
    }

    public static final /* synthetic */ <T> void launchActivityWithNewTask(Activity activity) {
        Intrinsics.checkParameterIsNotNull(activity, "$this$launchActivityWithNewTask");
        Bundle bundle = null;
        Intrinsics.reifiedOperationMarker(4, "T");
        Intent intent = new Intent(activity, Object.class);
        ExtensionsKt$launchActivityWithNewTask$1.INSTANCE.invoke(intent);
        if (Build.VERSION.SDK_INT >= 16) {
            activity.startActivityForResult(intent, -1, bundle);
        } else {
            activity.startActivityForResult(intent, -1);
        }
    }

    public static /* synthetic */ void launchActivity$default(Activity activity, int i, Bundle bundle, Function1 function1, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            i = -1;
        }
        if ((i2 & 2) != 0) {
            bundle = null;
        }
        if ((i2 & 4) != 0) {
            function1 = ExtensionsKt$launchActivity$1.INSTANCE;
        }
        Intrinsics.checkParameterIsNotNull(activity, "$this$launchActivity");
        Intrinsics.checkParameterIsNotNull(function1, "init");
        Intrinsics.reifiedOperationMarker(4, "T");
        Intent intent = new Intent(activity, Object.class);
        function1.invoke(intent);
        if (Build.VERSION.SDK_INT >= 16) {
            activity.startActivityForResult(intent, i, bundle);
        } else {
            activity.startActivityForResult(intent, i);
        }
    }

    public static /* synthetic */ void launchActivity$default(Fragment fragment, int i, Bundle bundle, Function1 function1, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            i = -1;
        }
        if ((i2 & 2) != 0) {
            bundle = null;
        }
        if ((i2 & 4) != 0) {
            function1 = ExtensionsKt$launchActivity$2.INSTANCE;
        }
        Intrinsics.checkParameterIsNotNull(fragment, "$this$launchActivity");
        Intrinsics.checkParameterIsNotNull(function1, "init");
        Intrinsics.reifiedOperationMarker(4, "T");
        Intent intent = new Intent(ShopHopApp.Companion.getAppInstance(), Object.class);
        function1.invoke(intent);
        fragment.startActivityForResult(intent, i, bundle);
    }

    public static final /* synthetic */ <T> void launchActivity(Fragment fragment, int i, Bundle bundle, Function1<? super Intent, Unit> function1) {
        Intrinsics.checkParameterIsNotNull(fragment, "$this$launchActivity");
        Intrinsics.checkParameterIsNotNull(function1, "init");
        Intrinsics.reifiedOperationMarker(4, "T");
        Intent intent = new Intent(ShopHopApp.Companion.getAppInstance(), Object.class);
        function1.invoke(intent);
        fragment.startActivityForResult(intent, i, bundle);
    }

    public static final /* synthetic */ <T> Intent newIntent(Context context) {
        Intrinsics.checkParameterIsNotNull(context, "context");
        Intrinsics.reifiedOperationMarker(4, "T");
        return new Intent(context, Object.class);
    }

    public static final void requestPermissions(FragmentActivity fragmentActivity, String[] strArr, Function1<? super Boolean, Unit> function1) {
        Intrinsics.checkParameterIsNotNull(fragmentActivity, "$this$requestPermissions");
        Intrinsics.checkParameterIsNotNull(strArr, NativeProtocol.RESULT_ARGS_PERMISSIONS);
        Intrinsics.checkParameterIsNotNull(function1, "onResult");
        if (ContextExtensionsKt.isPermissionGranted(fragmentActivity, strArr)) {
            function1.invoke(true);
            return;
        }
        PermissionObserver permissionObserver = new PermissionObserver();
        permissionObserver.setOnResumeCallback(new ExtensionsKt$requestPermissions$1(fragmentActivity, permissionObserver, function1, strArr));
        fragmentActivity.getLifecycle().addObserver(permissionObserver);
        ActivityCompat.requestPermissions(fragmentActivity, strArr, 100);
    }

    public static final Drawable tint(Drawable drawable, int i) {
        Intrinsics.checkParameterIsNotNull(drawable, "$this$tint");
        Drawable wrap = DrawableCompat.wrap(drawable);
        DrawableCompat.setTint(wrap, i);
        Intrinsics.checkExpressionValueIsNotNull(wrap, "wrapped");
        return wrap;
    }

    public static /* synthetic */ void setVerticalLayout$default(RecyclerView recyclerView, boolean z, int i, Object obj) {
        if ((i & 1) != 0) {
            z = false;
        }
        setVerticalLayout(recyclerView, z);
    }

    public static final void setVerticalLayout(RecyclerView recyclerView, boolean z) {
        Intrinsics.checkParameterIsNotNull(recyclerView, "$this$setVerticalLayout");
        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext(), 1, z));
    }

    public static /* synthetic */ void setHorizontalLayout$default(RecyclerView recyclerView, boolean z, int i, Object obj) {
        if ((i & 1) != 0) {
            z = false;
        }
        setHorizontalLayout(recyclerView, z);
    }

    public static final void setHorizontalLayout(RecyclerView recyclerView, boolean z) {
        Intrinsics.checkParameterIsNotNull(recyclerView, "$this$setHorizontalLayout");
        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext(), 0, z));
    }

    public static final int color(FragmentActivity fragmentActivity, int i) {
        Intrinsics.checkParameterIsNotNull(fragmentActivity, "$this$color");
        return ContextCompat.getColor(fragmentActivity, i);
    }

    public static final int stringColor(FragmentActivity fragmentActivity, String str) {
        Intrinsics.checkParameterIsNotNull(fragmentActivity, "$this$stringColor");
        Intrinsics.checkParameterIsNotNull(str, "color");
        return Color.parseColor(str);
    }

    public static final void switchToDarkMode(AppCompatActivity appCompatActivity, boolean z) {
        Intrinsics.checkParameterIsNotNull(appCompatActivity, "$this$switchToDarkMode");
        if (z) {
            AppCompatDelegate delegate = appCompatActivity.getDelegate();
            Intrinsics.checkExpressionValueIsNotNull(delegate, "delegate");
            delegate.setLocalNightMode(2);
            return;
        }
        AppCompatDelegate delegate2 = appCompatActivity.getDelegate();
        Intrinsics.checkExpressionValueIsNotNull(delegate2, "delegate");
        delegate2.setLocalNightMode(1);
    }

    public static final void changeAccentColor(TextView textView) {
        Intrinsics.checkParameterIsNotNull(textView, "$this$changeAccentColor");
        textView.setTextColor(Color.parseColor(AppExtensionsKt.getAccentColor()));
    }

    public static final void changePrimaryColor(TextView textView) {
        Intrinsics.checkParameterIsNotNull(textView, "$this$changePrimaryColor");
        textView.setTextColor(Color.parseColor(AppExtensionsKt.getPrimaryColor()));
    }

    public static final void changeTextPrimaryColor(TextView textView) {
        Intrinsics.checkParameterIsNotNull(textView, "$this$changeTextPrimaryColor");
        textView.setTextColor(Color.parseColor(AppExtensionsKt.getTextPrimaryColor()));
    }

    public static final void changeTextSecondaryColor(TextView textView) {
        Intrinsics.checkParameterIsNotNull(textView, "$this$changeTextSecondaryColor");
        textView.setTextColor(Color.parseColor(AppExtensionsKt.getTextSecondaryColor()));
    }

    public static final void changeTitleColor(TextView textView) {
        Intrinsics.checkParameterIsNotNull(textView, "$this$changeTitleColor");
        textView.setTextColor(Color.parseColor(AppExtensionsKt.getTextTitleColor()));
    }

    public static final void changeBackgroundTint(TextView textView, String str) {
        Intrinsics.checkParameterIsNotNull(textView, "$this$changeBackgroundTint");
        Intrinsics.checkParameterIsNotNull(str, "color");
        textView.getBackground().setTint(Color.parseColor(str));
    }

    public static final void changeBackgroundImageTint(ImageView imageView, String str) {
        Intrinsics.checkParameterIsNotNull(imageView, "$this$changeBackgroundImageTint");
        Intrinsics.checkParameterIsNotNull(str, "color");
        imageView.setColorFilter(new PorterDuffColorFilter(Color.parseColor(str), PorterDuff.Mode.SRC_IN));
    }

    public static final void changeTint(View view, String str) {
        Intrinsics.checkParameterIsNotNull(view, "$this$changeTint");
        Intrinsics.checkParameterIsNotNull(str, "color");
        Drawable background = view.getBackground();
        Intrinsics.checkExpressionValueIsNotNull(background, "background");
        background.setColorFilter(new PorterDuffColorFilter(Color.parseColor(str), PorterDuff.Mode.SRC_IN));
    }

    public static final void changeBackgroundColor(View view) {
        Intrinsics.checkParameterIsNotNull(view, "$this$changeBackgroundColor");
        view.setBackgroundColor(Color.parseColor(AppExtensionsKt.getBackgroundColor()));
    }

    public static final void setTextViewDrawableColor(TextView textView, String str) {
        Intrinsics.checkParameterIsNotNull(textView, "textView");
        Intrinsics.checkParameterIsNotNull(str, "colors");
        for (Drawable drawable : textView.getCompoundDrawablesRelative()) {
            if (drawable != null) {
                drawable.setColorFilter(new PorterDuffColorFilter(Color.parseColor(str), PorterDuff.Mode.SRC_IN));
            }
        }
    }

    public static final /* synthetic */ <T> void launchActivity(Activity activity, int i, Bundle bundle, Function1<? super Intent, Unit> function1) {
        Intrinsics.checkParameterIsNotNull(activity, "$this$launchActivity");
        Intrinsics.checkParameterIsNotNull(function1, "init");
        Intrinsics.reifiedOperationMarker(4, "T");
        Intent intent = new Intent(activity, Object.class);
        function1.invoke(intent);
        if (Build.VERSION.SDK_INT >= 16) {
            activity.startActivityForResult(intent, i, bundle);
        } else {
            activity.startActivityForResult(intent, i);
        }
    }
}
