package com.iqonic.store.utils.extensions;

import com.iqonic.store.AppBaseActivity;
import com.iqonic.store.models.RegisterResponse;
import com.iqonic.store.utils.Constants;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n¢\u0006\u0002\b\u0004"}, d2 = {"<anonymous>", "", "it", "Lcom/iqonic/store/models/RegisterResponse;", "invoke"}, k = 3, mv = {1, 1, 16})
/* compiled from: NetworkExtension.kt */
final class NetworkExtensionKt$createCustomer$1 extends Lambda implements Function1<RegisterResponse, Unit> {
    final /* synthetic */ Function1 $onApiSuccess;
    final /* synthetic */ AppBaseActivity $this_createCustomer;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    NetworkExtensionKt$createCustomer$1(AppBaseActivity appBaseActivity, Function1 function1) {
        super(1);
        this.$this_createCustomer = appBaseActivity;
        this.$onApiSuccess = function1;
    }

    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        invoke((RegisterResponse) obj);
        return Unit.INSTANCE;
    }

    public final void invoke(RegisterResponse registerResponse) {
        Intrinsics.checkParameterIsNotNull(registerResponse, "it");
        this.$this_createCustomer.showProgress(false);
        if (registerResponse.getCode() == 200) {
            AppExtensionsKt.getSharedPrefInstance().setValue(Constants.SharedPref.USER_DISPLAY_NAME, registerResponse.getData().getDisplay_name());
            AppExtensionsKt.getSharedPrefInstance().setValue(Constants.SharedPref.USER_EMAIL, registerResponse.getData().getUser_email());
            AppExtensionsKt.getSharedPrefInstance().setValue(Constants.SharedPref.USER_FIRST_NAME, registerResponse.getData().getDisplay_name());
            AppExtensionsKt.getSharedPrefInstance().setValue(Constants.SharedPref.USER_LAST_NAME, registerResponse.getData().getDisplay_name());
            this.$onApiSuccess.invoke(registerResponse);
            AppExtensionsKt.sendProfileUpdateBroadcast(this.$this_createCustomer);
            return;
        }
        ExtensionsKt.snackBar$default(this.$this_createCustomer, registerResponse.getMessage(), 0, 2, (Object) null);
    }
}
