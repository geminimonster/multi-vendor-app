package com.iqonic.store.utils;

import android.view.ViewTreeObserver;
import android.widget.TextView;
import com.iqonic.store.utils.extensions.ViewExtensionsKt;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0011\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000*\u0001\u0000\b\n\u0018\u00002\u00020\u0001J\b\u0010\u0002\u001a\u00020\u0003H\u0016¨\u0006\u0004"}, d2 = {"com/iqonic/store/utils/ExpandableTextView$setText$1", "Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;", "onGlobalLayout", "", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: ExpandableTextView.kt */
public final class ExpandableTextView$setText$1 implements ViewTreeObserver.OnGlobalLayoutListener {
    final /* synthetic */ ExpandableTextView this$0;

    ExpandableTextView$setText$1(ExpandableTextView expandableTextView) {
        this.this$0 = expandableTextView;
    }

    public void onGlobalLayout() {
        TextView content = this.this$0.getContent();
        if (content == null) {
            Intrinsics.throwNpe();
        }
        content.getViewTreeObserver().removeOnGlobalLayoutListener(this);
        TextView content2 = this.this$0.getContent();
        if (content2 == null) {
            Intrinsics.throwNpe();
        }
        if (content2.getLineCount() <= this.this$0.getMaxLine()) {
            TextView moreLess = this.this$0.getMoreLess();
            if (moreLess == null) {
                Intrinsics.throwNpe();
            }
            ViewExtensionsKt.hide(moreLess);
            return;
        }
        TextView moreLess2 = this.this$0.getMoreLess();
        if (moreLess2 == null) {
            Intrinsics.throwNpe();
        }
        ViewExtensionsKt.show(moreLess2);
        TextView content3 = this.this$0.getContent();
        if (content3 == null) {
            Intrinsics.throwNpe();
        }
        content3.setMaxLines(this.this$0.getMaxLine());
    }
}
