package com.iqonic.store.utils.oauthInterceptor;

import com.google.android.gms.ads.reward.mediation.MediationRewardedVideoAdAdapter;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u000f\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0003\u0018\u0000 \u000f2\b\u0012\u0004\u0012\u00020\u00000\u0001:\u0001\u000fB\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003¢\u0006\u0002\u0010\u0005J\u0006\u0010\u0006\u001a\u00020\u0003J\u0011\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\u0000H\u0002J\u0013\u0010\n\u001a\u00020\u000b2\b\u0010\f\u001a\u0004\u0018\u00010\rH\u0002J\b\u0010\u000e\u001a\u00020\bH\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0010"}, d2 = {"Lcom/iqonic/store/utils/oauthInterceptor/Parameter;", "", "key", "", "value", "(Ljava/lang/String;Ljava/lang/String;)V", "asUrlEncodedPair", "compareTo", "", "parameter", "equals", "", "other", "", "hashCode", "Companion", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: Parameter.kt */
public final class Parameter implements Comparable<Parameter> {
    public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
    private static final String UTF = UTF;
    private final String key;
    private final String value;

    public Parameter(String str, String str2) {
        Intrinsics.checkParameterIsNotNull(str, "key");
        Intrinsics.checkParameterIsNotNull(str2, "value");
        this.key = str;
        this.value = str2;
    }

    public final String asUrlEncodedPair() {
        return OAuthEncoder.INSTANCE.encode(this.key) + "=" + OAuthEncoder.INSTANCE.encode(this.value);
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof Parameter)) {
            return false;
        }
        Parameter parameter = (Parameter) obj;
        if (!Intrinsics.areEqual((Object) parameter.key, (Object) this.key) || !Intrinsics.areEqual((Object) parameter.value, (Object) this.value)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return this.key.hashCode() + this.value.hashCode();
    }

    public int compareTo(Parameter parameter) {
        Intrinsics.checkParameterIsNotNull(parameter, MediationRewardedVideoAdAdapter.CUSTOM_EVENT_SERVER_PARAMETER_FIELD);
        int compareTo = this.key.compareTo(parameter.key);
        return compareTo != 0 ? compareTo : this.value.compareTo(parameter.value);
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XD¢\u0006\u0002\n\u0000¨\u0006\u0005"}, d2 = {"Lcom/iqonic/store/utils/oauthInterceptor/Parameter$Companion;", "", "()V", "UTF", "", "app_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: Parameter.kt */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }
}
