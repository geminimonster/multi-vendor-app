package com.iqonic.store.utils.extensions;

import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.Lambda;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0002\u0010\u0000\u001a\u00020\u0001\"\u0004\b\u0000\u0010\u00022\u0006\u0010\u0003\u001a\u0002H\u00022\u0006\u0010\u0004\u001a\u00020\u0005H\n¢\u0006\u0004\b\u0006\u0010\u0007"}, d2 = {"<anonymous>", "", "T", "<anonymous parameter 0>", "<anonymous parameter 1>", "", "invoke", "(Ljava/lang/Object;I)V"}, k = 3, mv = {1, 1, 16})
/* compiled from: ListExtensions.kt */
final class ListExtensionsKt$create$10 extends Lambda implements Function2<T, Integer, Unit> {
    public static final ListExtensionsKt$create$10 INSTANCE = new ListExtensionsKt$create$10();

    ListExtensionsKt$create$10() {
        super(2);
    }

    public /* bridge */ /* synthetic */ Object invoke(Object obj, Object obj2) {
        invoke(obj, ((Number) obj2).intValue());
        return Unit.INSTANCE;
    }

    public final void invoke(T t, int i) {
    }
}
