package com.iqonic.store.utils;

import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.firebase.analytics.FirebaseAnalytics;
import java.util.List;
import java.util.Locale;
import java.util.Random;

public class SimpleLocation {
    private static final long INTERVAL_DEFAULT = 10000;
    private static final float KILOMETER_TO_METER = 1000.0f;
    private static final float LATITUDE_TO_KILOMETER = 111.133f;
    private static final float LONGITUDE_TO_KILOMETER_AT_ZERO_LATITUDE = 111.32f;
    private static final String PROVIDER_COARSE = "network";
    private static final String PROVIDER_FINE = "gps";
    private static final String PROVIDER_FINE_PASSIVE = "passive";
    private static final double SQUARE_ROOT_TWO = Math.sqrt(2.0d);
    private static Location mCachedPosition;
    private static final Random mRandom = new Random();
    private Geocoder geocoder;
    private int mBlurRadius;
    private final long mInterval;
    private final LocationManager mLocationManager;
    private LocationListener mLocationListener;
    private final boolean mPassive;
    private final boolean mRequireFine;
    private final boolean mRequireNewLocation;
    /* access modifiers changed from: private */
    public Listener mListener;
    /* access modifiers changed from: private */
    public Location mPosition;

    public SimpleLocation(Context context) {
        this(context, false);
    }

    public SimpleLocation(Context context, boolean z) {
        this(context, z, false);
    }

    public SimpleLocation(Context context, boolean z, boolean z2) {
        this(context, z, z2, INTERVAL_DEFAULT);
    }

    public SimpleLocation(Context context, boolean z, boolean z2, long j) {
        this(context, z, z2, j, false);
    }

    public SimpleLocation(Context context, boolean z, boolean z2, long j, boolean z3) {
        this.mLocationManager = (LocationManager) context.getApplicationContext().getSystemService(FirebaseAnalytics.Param.LOCATION);
        this.mRequireFine = z;
        this.mPassive = z2;
        this.mInterval = j;
        this.mRequireNewLocation = z3;
        if (!z3) {
            this.mPosition = getCachedPosition();
            cachePosition();
        }
        this.geocoder = new Geocoder(context, Locale.getDefault());
    }

    public static double latitudeToKilometer(double d) {
        return d * 111.13300323486328d;
    }

    private static int calculateRandomOffset(int i) {
        return mRandom.nextInt((i + 1) * 2) - i;
    }

    public static void openSettings(Context context) {
        context.startActivity(new Intent("android.settings.LOCATION_SOURCE_SETTINGS"));
    }

    public static double kilometerToLatitude(double d) {
        return d / latitudeToKilometer(1.0d);
    }

    public static double latitudeToMeter(double d) {
        return latitudeToKilometer(d) * 1000.0d;
    }

    public static double meterToLatitude(double d) {
        return d / latitudeToMeter(1.0d);
    }

    public static double longitudeToKilometer(double d, double d2) {
        return d * 111.31999969482422d * Math.cos(Math.toRadians(d2));
    }

    public static double kilometerToLongitude(double d, double d2) {
        return d / longitudeToKilometer(1.0d, d2);
    }

    public static double longitudeToMeter(double d, double d2) {
        return longitudeToKilometer(d, d2) * 1000.0d;
    }

    public static double meterToLongitude(double d, double d2) {
        return d / longitudeToMeter(1.0d, d2);
    }

    public static double calculateDistance(Point point, Point point2) {
        return calculateDistance(point.latitude, point.longitude, point2.latitude, point2.longitude);
    }

    public static double calculateDistance(double d, double d2, double d3, double d4) {
        float[] fArr = new float[3];
        Location.distanceBetween(d, d2, d3, d4, fArr);
        return (double) fArr[0];
    }

    public void setListener(Listener listener) {
        this.mListener = listener;
    }

    private boolean hasLocationEnabled(String str) {
        try {
            return this.mLocationManager.isProviderEnabled(str);
        } catch (Exception unused) {
            return false;
        }
    }

    public boolean hasLocationEnabled() {
        return hasLocationEnabled(getProviderName());
    }

    public void beginUpdates() {
        if (this.mLocationListener != null) {
            endUpdates();
        }
        if (!this.mRequireNewLocation) {
            this.mPosition = getCachedPosition();
        }
        this.mLocationListener = createLocationListener();
        this.mLocationManager.requestLocationUpdates(getProviderName(), this.mInterval, 0.0f, this.mLocationListener);
    }

    public void endUpdates() {
        LocationListener locationListener = this.mLocationListener;
        if (locationListener != null) {
            this.mLocationManager.removeUpdates(locationListener);
            this.mLocationListener = null;
        }
    }

    private Location blurWithRadius(Location location) {
        if (this.mBlurRadius <= 0) {
            return location;
        }
        Location location2 = new Location(location);
        double calculateRandomOffset = ((double) calculateRandomOffset(this.mBlurRadius)) / SQUARE_ROOT_TWO;
        double calculateRandomOffset2 = ((double) calculateRandomOffset(this.mBlurRadius)) / SQUARE_ROOT_TWO;
        location2.setLongitude(location2.getLongitude() + meterToLongitude(calculateRandomOffset, location2.getLatitude()));
        location2.setLatitude(location2.getLatitude() + meterToLatitude(calculateRandomOffset2));
        return location2;
    }

    public Point getPosition() {
        Location location = this.mPosition;
        if (location == null) {
            return null;
        }
        Location blurWithRadius = blurWithRadius(location);
        return new Point(blurWithRadius.getLatitude(), blurWithRadius.getLongitude());
    }

    public double getLatitude() {
        Location location = this.mPosition;
        if (location == null) {
            return 0.0d;
        }
        return blurWithRadius(location).getLatitude();
    }

    public double getLongitude() {
        Location location = this.mPosition;
        if (location == null) {
            return 0.0d;
        }
        return blurWithRadius(location).getLongitude();
    }

    public float getSpeed() {
        Location location = this.mPosition;
        if (location == null) {
            return 0.0f;
        }
        return location.getSpeed();
    }

    public double getAltitude() {
        Location location = this.mPosition;
        if (location == null) {
            return 0.0d;
        }
        return location.getAltitude();
    }

    public Location getLocation() {
        return this.mPosition;
    }

    public void setBlurRadius(int i) {
        this.mBlurRadius = i;
    }

    private LocationListener createLocationListener() {
        return new LocationListener() {
            public void onLocationChanged(Location location) {
                Location unused = SimpleLocation.this.mPosition = location;
                SimpleLocation.this.cachePosition();
                if (SimpleLocation.this.mListener != null) {
                    SimpleLocation.this.mListener.onPositionChanged();
                }
            }

            public void onStatusChanged(String str, int i, Bundle bundle) {
                if (SimpleLocation.this.mListener != null) {
                    SimpleLocation.this.mListener.onStatusChanged(str, i, bundle);
                }
            }

            public void onProviderEnabled(String str) {
                if (SimpleLocation.this.mListener != null) {
                    SimpleLocation.this.mListener.onProviderEnabled(str);
                }
            }

            public void onProviderDisabled(String str) {
                if (SimpleLocation.this.mListener != null) {
                    SimpleLocation.this.mListener.onProviderDisabled(str);
                }
            }
        };
    }

    private String getProviderName() {
        return getProviderName(this.mRequireFine);
    }

    private String getProviderName(boolean z) {
        if (z) {
            return this.mPassive ? PROVIDER_FINE_PASSIVE : PROVIDER_FINE;
        }
        if (hasLocationEnabled(PROVIDER_COARSE)) {
            if (!this.mPassive) {
                return PROVIDER_COARSE;
            }
            throw new RuntimeException("There is no passive provider for the coarse location");
        } else if (hasLocationEnabled(PROVIDER_FINE) || hasLocationEnabled(PROVIDER_FINE_PASSIVE)) {
            return getProviderName(true);
        } else {
            return PROVIDER_COARSE;
        }
    }

    private Location getCachedPosition() {
        Location location = mCachedPosition;
        if (location != null) {
            return location;
        }
        try {
            return this.mLocationManager.getLastKnownLocation(getProviderName());
        } catch (Exception unused) {
            return null;
        }
    }

    /* access modifiers changed from: private */
    public void cachePosition() {
        Location location = this.mPosition;
        if (location != null) {
            mCachedPosition = location;
        }
    }

    public Address getAddress() {
        try {
            List<Address> fromLocation = this.geocoder.getFromLocation(getLatitude(), getLongitude(), 1);
            if (fromLocation != null) {
                return fromLocation.get(0);
            }
        } catch (Exception unused) {
        }
        return null;
    }

    public interface Listener {
        void onPositionChanged();

        void onProviderDisabled(String str);

        void onProviderEnabled(String str);

        void onStatusChanged(String str, int i, Bundle bundle);
    }

    public static class Point implements Parcelable {
        public static final Parcelable.Creator<Point> CREATOR = new Parcelable.Creator<Point>() {
            public Point createFromParcel(Parcel parcel) {
                return new Point(parcel);
            }

            public Point[] newArray(int i) {
                return new Point[i];
            }
        };
        public final double latitude;
        public final double longitude;

        public int describeContents() {
            return 0;
        }

        public Point(double d, double d2) {
            this.latitude = d;
            this.longitude = d2;
        }

        private Point(Parcel parcel) {
            this.latitude = parcel.readDouble();
            this.longitude = parcel.readDouble();
        }

        public String toString() {
            return "(" + this.latitude + ", " + this.longitude + ")";
        }

        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeDouble(this.latitude);
            parcel.writeDouble(this.longitude);
        }

        public double getLatitude() {
            return this.latitude;
        }

        public double getLongitude() {
            return this.longitude;
        }
    }
}
