package com.iqonic.store.utils.photoview;

import android.graphics.RectF;

public interface OnMatrixChangedListener {
    void onMatrixChanged(RectF rectF);
}
