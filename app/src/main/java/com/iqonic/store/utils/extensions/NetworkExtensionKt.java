package com.iqonic.store.utils.extensions;

import android.content.Context;
import android.util.Log;
import android.widget.ImageView;
import androidx.core.app.NotificationCompat;
import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestBuilder;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.facebook.share.internal.ShareConstants;
import com.google.gson.Gson;
import com.iqonic.store.AppBaseActivity;
import com.iqonic.store.ShopHopApp;
import com.iqonic.store.models.AddCartResponse;
import com.iqonic.store.models.BaseResponse;
import com.iqonic.store.models.CartRequestModel;
import com.iqonic.store.models.Coupons;
import com.iqonic.store.models.CustomerData;
import com.iqonic.store.models.Order;
import com.iqonic.store.models.OrderNotes;
import com.iqonic.store.models.ProductReviewData;
import com.iqonic.store.models.RegisterResponse;
import com.iqonic.store.models.RequestModel;
import com.iqonic.store.models.WishList;
import com.iqonic.store.models.loginResponse;
import com.iqonic.store.network.RestApiImpl;
import com.iqonic.store.utils.Constants;
import com.iqonic.store.utils.SharedPrefUtils;
import com.store.proshop.R;
import java.util.ArrayList;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import okio.Buffer;
import org.json.JSONException;
import org.json.JSONObject;
import retrofit2.Call;
import retrofit2.Response;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000¼\u0001\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0000\u001a\u0010\u0010\u0000\u001a\u00020\u00012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u001ae\u0010\u0004\u001a\u00020\u0005\"\u0004\b\u0000\u0010\u00062\f\u0010\u0007\u001a\b\u0012\u0004\u0012\u0002H\u00060\b2\u0014\b\u0002\u0010\t\u001a\u000e\u0012\u0004\u0012\u0002H\u0006\u0012\u0004\u0012\u00020\u00050\n2#\b\u0002\u0010\u000b\u001a\u001d\u0012\u0013\u0012\u00110\u0001¢\u0006\f\b\f\u0012\b\b\r\u0012\u0004\b\b(\u000e\u0012\u0004\u0012\u00020\u00050\n2\u000e\b\u0002\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u00050\u0010\u001a9\u0010\u0011\u001a\u00020\u00052!\u0010\u000b\u001a\u001d\u0012\u0013\u0012\u00110\u0001¢\u0006\f\b\f\u0012\b\b\r\u0012\u0004\b\b(\u000e\u0012\u0004\u0012\u00020\u00050\n2\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u000e\u001a\u00020\u0001\u001a\u000e\u0010\u0012\u001a\u00020\u00012\u0006\u0010\u0013\u001a\u00020\u0014\u001a\u0010\u0010\u0015\u001a\u00020\u00162\b\b\u0002\u0010\u0017\u001a\u00020\u0001\u001a*\u0010\u0018\u001a\u00020\u00052\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0019\u001a\u00020\u00012\b\b\u0002\u0010\u001a\u001a\u00020\u001b2\b\b\u0002\u0010\u001c\u001a\u00020\u001d\u001aY\u0010\u001e\u001a\u00020\u0005\"\u0004\b\u0000\u0010\u00062\u0012\u0010\t\u001a\u000e\u0012\u0004\u0012\u0002H\u0006\u0012\u0004\u0012\u00020\u00050\n2!\u0010\u000b\u001a\u001d\u0012\u0013\u0012\u00110\u0001¢\u0006\f\b\f\u0012\b\b\r\u0012\u0004\b\b(\u000e\u0012\u0004\u0012\u00020\u00050\n2\f\u0010\u0019\u001a\b\u0012\u0004\u0012\u0002H\u00060\u001f2\u0006\u0010\u0002\u001a\u00020\u0003\u001a&\u0010 \u001a\u00020\u0005*\u00020!2\u0006\u0010\"\u001a\u00020#2\u0012\u0010\t\u001a\u000e\u0012\u0004\u0012\u00020$\u0012\u0004\u0012\u00020\u00050\n\u001a&\u0010%\u001a\u00020\u0005*\u00020!2\u0006\u0010\"\u001a\u00020#2\u0012\u0010&\u001a\u000e\u0012\u0004\u0012\u00020\u001d\u0012\u0004\u0012\u00020\u00050\n\u001a&\u0010'\u001a\u00020\u0005*\u00020!2\u0006\u0010\"\u001a\u00020#2\u0012\u0010\t\u001a\u000e\u0012\u0004\u0012\u00020(\u0012\u0004\u0012\u00020\u00050\n\u001a.\u0010)\u001a\u00020\u0005*\u00020!2\"\u0010\t\u001a\u001e\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020+0*j\b\u0012\u0004\u0012\u00020+`,\u0012\u0004\u0012\u00020\u00050\n\u001a.\u0010-\u001a\u00020\u0005*\u00020!2\"\u0010\t\u001a\u001e\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020.0*j\b\u0012\u0004\u0012\u00020.`,\u0012\u0004\u0012\u00020\u00050\n\u001a6\u0010/\u001a\u00020\u0005*\u00020!2\u0006\u00100\u001a\u0002012\"\u0010\t\u001a\u001e\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u0002020*j\b\u0012\u0004\u0012\u000202`,\u0012\u0004\u0012\u00020\u00050\n\u001a.\u00103\u001a\u00020\u0005*\u00020!2\"\u0010\t\u001a\u001e\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u0002040*j\b\u0012\u0004\u0012\u000204`,\u0012\u0004\u0012\u00020\u00050\n\u001a6\u00105\u001a\u00020\u0005*\u00020!2\u0006\u00100\u001a\u0002012\"\u0010\t\u001a\u001e\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u0002060*j\b\u0012\u0004\u0012\u000206`,\u0012\u0004\u0012\u00020\u00050\n\u001a&\u00107\u001a\u00020\u0005*\u0002082\u0006\u00109\u001a\u00020\u00012\b\b\u0002\u0010:\u001a\u0002012\b\b\u0002\u0010;\u001a\u000201\u001a&\u0010<\u001a\u00020\u0005*\u00020!2\u0006\u0010\"\u001a\u00020#2\u0012\u0010\t\u001a\u000e\u0012\u0004\u0012\u00020=\u0012\u0004\u0012\u00020\u00050\n\u001a&\u0010>\u001a\u00020\u0005*\u00020!2\u0006\u0010\"\u001a\u00020#2\u0012\u0010&\u001a\u000e\u0012\u0004\u0012\u00020\u001d\u0012\u0004\u0012\u00020\u00050\n\u001a&\u0010?\u001a\u00020\u0005*\u00020!2\u0006\u0010\"\u001a\u00020@2\u0012\u0010\t\u001a\u000e\u0012\u0004\u0012\u00020=\u0012\u0004\u0012\u00020\u00050\n\u001a6\u0010A\u001a\u00020\u0005*\u00020!2\u0006\u0010B\u001a\u00020C2\u0006\u0010D\u001a\u00020\u001d2\u0006\u0010E\u001a\u00020\u00012\u0012\u0010F\u001a\u000e\u0012\u0004\u0012\u00020\u0001\u0012\u0004\u0012\u00020\u00050\n\u001a&\u0010G\u001a\u00020\u0005*\u00020!2\u0006\u0010\"\u001a\u00020#2\u0012\u0010\t\u001a\u000e\u0012\u0004\u0012\u00020\u001d\u0012\u0004\u0012\u00020\u00050\n\u001aB\u0010H\u001a\u00020\u0005*\u00020!2\u0006\u0010I\u001a\u00020\u00012\u0006\u0010E\u001a\u00020\u00012\u0012\u0010F\u001a\u000e\u0012\u0004\u0012\u00020\u0001\u0012\u0004\u0012\u00020\u00050\n2\u0012\u0010\t\u001a\u000e\u0012\u0004\u0012\u00020C\u0012\u0004\u0012\u00020\u00050\n\u001a&\u0010J\u001a\u00020\u0005*\u00020!2\u0006\u0010\"\u001a\u00020#2\u0012\u0010\t\u001a\u000e\u0012\u0004\u0012\u00020K\u0012\u0004\u0012\u00020\u00050\n¨\u0006L"}, d2 = {"bodyToString", "", "request", "Lokhttp3/Request;", "callApi", "", "T", "call", "Lretrofit2/Call;", "onApiSuccess", "Lkotlin/Function1;", "onApiError", "Lkotlin/ParameterName;", "name", "aError", "onNetworkError", "Lkotlin/Function0;", "failureCallback", "getJsonMsg", "errorBody", "Lokhttp3/ResponseBody;", "getRestApiImpl", "Lcom/iqonic/store/network/RestApiImpl;", "url", "logData", "response", "time", "", "isError", "", "successCallback", "Lretrofit2/Response;", "addItemToCart", "Lcom/iqonic/store/AppBaseActivity;", "requestModel", "Lcom/iqonic/store/models/RequestModel;", "Lcom/iqonic/store/models/AddCartResponse;", "addToWishList", "onSuccess", "createCustomer", "Lcom/iqonic/store/models/RegisterResponse;", "fetchAndStoreWishListData", "Ljava/util/ArrayList;", "Lcom/iqonic/store/models/WishList;", "Lkotlin/collections/ArrayList;", "getOrder", "Lcom/iqonic/store/models/Order;", "getOrderTracking", "id", "", "Lcom/iqonic/store/models/OrderNotes;", "listCoupons", "Lcom/iqonic/store/models/Coupons;", "listReview", "Lcom/iqonic/store/models/ProductReviewData;", "loadImageFromUrl", "Landroid/widget/ImageView;", "aImageUrl", "aPlaceHolderImage", "aErrorImage", "removeCartItem", "Lcom/iqonic/store/models/BaseResponse;", "removeFromWishList", "removeMultipleCartItem", "Lcom/iqonic/store/models/CartRequestModel;", "saveLoginResponse", "it", "Lcom/iqonic/store/models/loginResponse;", "isSocialLogin", "password", "onError", "saveProfileImage", "signIn", "email", "updateCustomer", "Lcom/iqonic/store/models/CustomerData;", "app_release"}, k = 2, mv = {1, 1, 16})
/* compiled from: NetworkExtension.kt */
public final class NetworkExtensionKt {
    public static final String bodyToString(Request request) {
        try {
            Buffer buffer = new Buffer();
            if (request == null) {
                Intrinsics.throwNpe();
            }
            if (request.body() == null) {
                return "";
            }
            RequestBody body = request.body();
            if (body == null) {
                Intrinsics.throwNpe();
            }
            body.writeTo(buffer);
            return buffer.readUtf8();
        } catch (Exception unused) {
            return "Request Body is Null";
        }
    }

    public static final String getJsonMsg(ResponseBody responseBody) {
        Intrinsics.checkParameterIsNotNull(responseBody, "errorBody");
        try {
            JSONObject jSONObject = new JSONObject(StringExtensionsKt.getHtmlString(responseBody.string()).toString());
            Log.d("api_", jSONObject.toString());
            if (jSONObject.has("message")) {
                String string = jSONObject.getString("message");
                Intrinsics.checkExpressionValueIsNotNull(string, "jsonObject.getString(\"message\")");
                return string;
            }
            String string2 = ShopHopApp.Companion.getAppInstance().getString(R.string.error_something_went_wrong);
            Intrinsics.checkExpressionValueIsNotNull(string2, "getAppInstance().getStri…ror_something_went_wrong)");
            return string2;
        } catch (JSONException e) {
            e.printStackTrace();
            String string3 = ShopHopApp.Companion.getAppInstance().getString(R.string.error_something_went_wrong);
            Intrinsics.checkExpressionValueIsNotNull(string3, "getAppInstance().getStri…ror_something_went_wrong)");
            return string3;
        }
    }

    public static /* synthetic */ void logData$default(Request request, String str, long j, boolean z, int i, Object obj) {
        if ((i & 4) != 0) {
            j = 0;
        }
        if ((i & 8) != 0) {
            z = false;
        }
        logData(request, str, j, z);
    }

    public static final void logData(Request request, String str, long j, boolean z) {
        Intrinsics.checkParameterIsNotNull(request, ShareConstants.WEB_DIALOG_RESULT_PARAM_REQUEST_ID);
        Intrinsics.checkParameterIsNotNull(str, "response");
        try {
            Log.d("api_headers", new Gson().toJson((Object) request.headers()));
            Log.d("api_response_arrived in", String.valueOf(j / 1000) + " second");
            Log.d("api_url", request.url().toString());
            Log.d("api_request", bodyToString(request));
            if (z) {
                Log.e("api_response", str);
            } else {
                Log.d("api_response", str);
            }
            Log.d("api_", "------------------");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static /* synthetic */ void callApi$default(Call call, Function1 function1, Function1 function12, Function0 function0, int i, Object obj) {
        if ((i & 2) != 0) {
            function1 = NetworkExtensionKt$callApi$1.INSTANCE;
        }
        if ((i & 4) != 0) {
            function12 = NetworkExtensionKt$callApi$2.INSTANCE;
        }
        if ((i & 8) != 0) {
            function0 = NetworkExtensionKt$callApi$3.INSTANCE;
        }
        callApi(call, function1, function12, function0);
    }

    public static final <T> void callApi(Call<T> call, Function1<? super T, Unit> function1, Function1<? super String, Unit> function12, Function0<Unit> function0) {
        Intrinsics.checkParameterIsNotNull(call, NotificationCompat.CATEGORY_CALL);
        Intrinsics.checkParameterIsNotNull(function1, "onApiSuccess");
        Intrinsics.checkParameterIsNotNull(function12, "onApiError");
        Intrinsics.checkParameterIsNotNull(function0, "onNetworkError");
        Log.d("api_calling", call.request().url().toString() + " " + bodyToString(call.request()));
        call.enqueue(new NetworkExtensionKt$callApi$4(function1, function12, function0));
    }

    public static final <T> void successCallback(Function1<? super T, Unit> function1, Function1<? super String, Unit> function12, Response<T> response, Request request) {
        Intrinsics.checkParameterIsNotNull(function1, "onApiSuccess");
        Intrinsics.checkParameterIsNotNull(function12, "onApiError");
        Intrinsics.checkParameterIsNotNull(response, "response");
        Intrinsics.checkParameterIsNotNull(request, ShareConstants.WEB_DIALOG_RESULT_PARAM_REQUEST_ID);
        if (response.isSuccessful()) {
            T body = response.body();
            if (body == null) {
                Intrinsics.throwNpe();
            }
            function1.invoke(body);
            String json = new Gson().toJson((Object) response.body());
            Intrinsics.checkExpressionValueIsNotNull(json, "Gson().toJson(response.body())");
            logData$default(request, json, response.raw().receivedResponseAtMillis() - response.raw().sentRequestAtMillis(), false, 8, (Object) null);
            return;
        }
        ResponseBody errorBody = response.errorBody();
        if (errorBody == null) {
            Intrinsics.throwNpe();
        }
        Intrinsics.checkExpressionValueIsNotNull(errorBody, "response.errorBody()!!");
        function12.invoke(getJsonMsg(errorBody));
        String json2 = new Gson().toJson((Object) response.errorBody());
        Intrinsics.checkExpressionValueIsNotNull(json2, "Gson().toJson(response.errorBody())");
        logData$default(request, json2, response.raw().receivedResponseAtMillis() - response.raw().sentRequestAtMillis(), false, 8, (Object) null);
    }

    public static final void failureCallback(Function1<? super String, Unit> function1, Request request, String str) {
        Intrinsics.checkParameterIsNotNull(function1, "onApiError");
        Intrinsics.checkParameterIsNotNull(request, ShareConstants.WEB_DIALOG_RESULT_PARAM_REQUEST_ID);
        Intrinsics.checkParameterIsNotNull(str, "aError");
        function1.invoke(str);
        logData$default(request, str, 0, false, 12, (Object) null);
    }

    public static /* synthetic */ void loadImageFromUrl$default(ImageView imageView, String str, int i, int i2, int i3, Object obj) {
        if ((i3 & 2) != 0) {
            i = R.drawable.placeholder;
        }
        if ((i3 & 4) != 0) {
            i2 = R.drawable.placeholder;
        }
        loadImageFromUrl(imageView, str, i, i2);
    }

    public static final void loadImageFromUrl(ImageView imageView, String str, int i, int i2) {
        Intrinsics.checkParameterIsNotNull(imageView, "$this$loadImageFromUrl");
        Intrinsics.checkParameterIsNotNull(str, "aImageUrl");
        if (!StringExtensionsKt.checkIsEmpty(str)) {
            Intrinsics.checkExpressionValueIsNotNull(((RequestBuilder) ((RequestBuilder) ((RequestBuilder) Glide.with((Context) ShopHopApp.Companion.getAppInstance()).load(str).placeholder(i)).diskCacheStrategy(DiskCacheStrategy.ALL)).error(i2)).into(imageView), "Glide.with(getAppInstanc…)\n            .into(this)");
        } else {
            AppExtensionsKt.displayBlankImage(imageView, ShopHopApp.Companion.getAppInstance(), i);
        }
    }

    public static /* synthetic */ RestApiImpl getRestApiImpl$default(String str, int i, Object obj) {
        if ((i & 1) != 0) {
            str = SharedPrefUtils.getStringValue$default(AppExtensionsKt.getSharedPrefInstance(), Constants.SharedPref.APPURL, (String) null, 2, (Object) null);
        }
        return getRestApiImpl(str);
    }

    public static final RestApiImpl getRestApiImpl(String str) {
        Intrinsics.checkParameterIsNotNull(str, "url");
        if (ShopHopApp.Companion.getRestApiImpl() == null) {
            ShopHopApp.Companion.setRestApiImpl(new RestApiImpl(str));
            return new RestApiImpl(str);
        }
        RestApiImpl restApiImpl = ShopHopApp.Companion.getRestApiImpl();
        if (restApiImpl != null) {
            return restApiImpl;
        }
        Intrinsics.throwNpe();
        return restApiImpl;
    }

    public static final void listReview(AppBaseActivity appBaseActivity, int i, Function1<? super ArrayList<ProductReviewData>, Unit> function1) {
        Intrinsics.checkParameterIsNotNull(appBaseActivity, "$this$listReview");
        Intrinsics.checkParameterIsNotNull(function1, "onApiSuccess");
        if (ExtensionsKt.isNetworkAvailable()) {
            appBaseActivity.showProgress(true);
            getRestApiImpl$default((String) null, 1, (Object) null).listReview(i, new NetworkExtensionKt$listReview$1(appBaseActivity, function1), new NetworkExtensionKt$listReview$2(appBaseActivity));
        }
    }

    public static final void listCoupons(AppBaseActivity appBaseActivity, Function1<? super ArrayList<Coupons>, Unit> function1) {
        Intrinsics.checkParameterIsNotNull(appBaseActivity, "$this$listCoupons");
        Intrinsics.checkParameterIsNotNull(function1, "onApiSuccess");
        if (ExtensionsKt.isNetworkAvailable()) {
            appBaseActivity.showProgress(true);
            getRestApiImpl$default((String) null, 1, (Object) null).listCoupons(new NetworkExtensionKt$listCoupons$1(appBaseActivity, function1), new NetworkExtensionKt$listCoupons$2(appBaseActivity));
        }
    }

    public static final void createCustomer(AppBaseActivity appBaseActivity, RequestModel requestModel, Function1<? super RegisterResponse, Unit> function1) {
        Intrinsics.checkParameterIsNotNull(appBaseActivity, "$this$createCustomer");
        Intrinsics.checkParameterIsNotNull(requestModel, "requestModel");
        Intrinsics.checkParameterIsNotNull(function1, "onApiSuccess");
        appBaseActivity.showProgress(true);
        if (ExtensionsKt.isNetworkAvailable()) {
            appBaseActivity.showProgress(true);
            getRestApiImpl$default((String) null, 1, (Object) null).CreateCustomer(requestModel, new NetworkExtensionKt$createCustomer$1(appBaseActivity, function1), new NetworkExtensionKt$createCustomer$2(appBaseActivity));
            return;
        }
        appBaseActivity.showProgress(false);
        String string = appBaseActivity.getString(R.string.error_no_internet);
        Intrinsics.checkExpressionValueIsNotNull(string, "getString(R.string.error_no_internet)");
        ExtensionsKt.snackBarError(appBaseActivity, string);
    }

    public static final void updateCustomer(AppBaseActivity appBaseActivity, RequestModel requestModel, Function1<? super CustomerData, Unit> function1) {
        Intrinsics.checkParameterIsNotNull(appBaseActivity, "$this$updateCustomer");
        Intrinsics.checkParameterIsNotNull(requestModel, "requestModel");
        Intrinsics.checkParameterIsNotNull(function1, "onApiSuccess");
        appBaseActivity.showProgress(true);
        if (ExtensionsKt.isNetworkAvailable()) {
            appBaseActivity.showProgress(true);
            getRestApiImpl$default((String) null, 1, (Object) null).updateCustomer(requestModel, new NetworkExtensionKt$updateCustomer$1(appBaseActivity, function1), new NetworkExtensionKt$updateCustomer$2(appBaseActivity));
            return;
        }
        appBaseActivity.showProgress(false);
        String string = appBaseActivity.getString(R.string.error_no_internet);
        Intrinsics.checkExpressionValueIsNotNull(string, "getString(R.string.error_no_internet)");
        ExtensionsKt.snackBarError(appBaseActivity, string);
    }

    public static final void signIn(AppBaseActivity appBaseActivity, String str, String str2, Function1<? super String, Unit> function1, Function1<? super loginResponse, Unit> function12) {
        Intrinsics.checkParameterIsNotNull(appBaseActivity, "$this$signIn");
        Intrinsics.checkParameterIsNotNull(str, "email");
        Intrinsics.checkParameterIsNotNull(str2, "password");
        Intrinsics.checkParameterIsNotNull(function1, "onError");
        Intrinsics.checkParameterIsNotNull(function12, "onApiSuccess");
        RequestModel requestModel = new RequestModel();
        requestModel.setUsername(str);
        requestModel.setPassword(str2);
        appBaseActivity.showProgress(true);
        if (ExtensionsKt.isNetworkAvailable()) {
            appBaseActivity.showProgress(true);
            getRestApiImpl$default((String) null, 1, (Object) null).Login(requestModel, new NetworkExtensionKt$signIn$1(appBaseActivity, str2, function1, function12), new NetworkExtensionKt$signIn$2(appBaseActivity, function1));
            return;
        }
        appBaseActivity.showProgress(false);
        String string = appBaseActivity.getString(R.string.error_no_internet);
        Intrinsics.checkExpressionValueIsNotNull(string, "getString(R.string.error_no_internet)");
        ExtensionsKt.snackBarError(appBaseActivity, string);
    }

    public static final void saveLoginResponse(AppBaseActivity appBaseActivity, loginResponse loginresponse, boolean z, String str, Function1<? super String, Unit> function1) {
        Intrinsics.checkParameterIsNotNull(appBaseActivity, "$this$saveLoginResponse");
        Intrinsics.checkParameterIsNotNull(loginresponse, "it");
        Intrinsics.checkParameterIsNotNull(str, "password");
        Intrinsics.checkParameterIsNotNull(function1, "onError");
        if (!loginresponse.getUser_role().isEmpty()) {
            boolean z2 = false;
            if (Intrinsics.areEqual((Object) loginresponse.getUser_role().get(0), (Object) "administrator")) {
                appBaseActivity.showProgress(false);
                function1.invoke("Admin is not allowed");
                return;
            }
            AppExtensionsKt.getSharedPrefInstance().setValue("user_id", String.valueOf(loginresponse.getUser_id()));
            AppExtensionsKt.getSharedPrefInstance().setValue(Constants.SharedPref.USER_DISPLAY_NAME, loginresponse.getUser_display_name());
            AppExtensionsKt.getSharedPrefInstance().setValue(Constants.SharedPref.USER_FIRST_NAME, loginresponse.getFirst_name());
            AppExtensionsKt.getSharedPrefInstance().setValue(Constants.SharedPref.USER_LAST_NAME, loginresponse.getLast_name());
            AppExtensionsKt.getSharedPrefInstance().setValue(Constants.SharedPref.USER_EMAIL, loginresponse.getUser_email());
            AppExtensionsKt.getSharedPrefInstance().setValue(Constants.SharedPref.USER_NICE_NAME, loginresponse.getUser_nicename());
            AppExtensionsKt.getSharedPrefInstance().setValue(Constants.SharedPref.USER_TOKEN, loginresponse.getToken());
            if (loginresponse.getProfile_image().length() > 0) {
                z2 = true;
            }
            if (z2) {
                AppExtensionsKt.getSharedPrefInstance().setValue(Constants.SharedPref.USER_PROFILE, loginresponse.getProfile_image());
            }
            AppExtensionsKt.getSharedPrefInstance().setValue(Constants.SharedPref.IS_SOCIAL_LOGIN, Boolean.valueOf(z));
            AppExtensionsKt.getSharedPrefInstance().setValue(Constants.SharedPref.USER_PASSWORD, str);
            AppExtensionsKt.getSharedPrefInstance().setValue(Constants.SharedPref.BILLING, new Gson().toJson((Object) loginresponse.getBilling()));
            AppExtensionsKt.getSharedPrefInstance().setValue(Constants.SharedPref.SHIPPING, new Gson().toJson((Object) loginresponse.getShipping()));
            AppExtensionsKt.getSharedPrefInstance().setValue(Constants.SharedPref.IS_LOGGED_IN, true);
        }
    }

    public static final void addItemToCart(AppBaseActivity appBaseActivity, RequestModel requestModel, Function1<? super AddCartResponse, Unit> function1) {
        Intrinsics.checkParameterIsNotNull(appBaseActivity, "$this$addItemToCart");
        Intrinsics.checkParameterIsNotNull(requestModel, "requestModel");
        Intrinsics.checkParameterIsNotNull(function1, "onApiSuccess");
        if (ExtensionsKt.isNetworkAvailable()) {
            appBaseActivity.showProgress(true);
            getRestApiImpl$default((String) null, 1, (Object) null).addItemToCart(requestModel, new NetworkExtensionKt$addItemToCart$1(appBaseActivity, function1), new NetworkExtensionKt$addItemToCart$2(appBaseActivity));
            return;
        }
        ExtensionsKt.noInternetSnackBar(appBaseActivity);
    }

    public static final void removeCartItem(AppBaseActivity appBaseActivity, RequestModel requestModel, Function1<? super BaseResponse, Unit> function1) {
        Intrinsics.checkParameterIsNotNull(appBaseActivity, "$this$removeCartItem");
        Intrinsics.checkParameterIsNotNull(requestModel, "requestModel");
        Intrinsics.checkParameterIsNotNull(function1, "onApiSuccess");
        String string = appBaseActivity.getString(R.string.msg_confirmation);
        Intrinsics.checkExpressionValueIsNotNull(string, "getString(R.string.msg_confirmation)");
        AppExtensionsKt.getAlertDialog$default(appBaseActivity, string, (String) null, (String) null, (String) null, new NetworkExtensionKt$removeCartItem$1(appBaseActivity, requestModel, function1), NetworkExtensionKt$removeCartItem$2.INSTANCE, 14, (Object) null).show();
    }

    public static final void removeMultipleCartItem(AppBaseActivity appBaseActivity, CartRequestModel cartRequestModel, Function1<? super BaseResponse, Unit> function1) {
        Intrinsics.checkParameterIsNotNull(appBaseActivity, "$this$removeMultipleCartItem");
        Intrinsics.checkParameterIsNotNull(cartRequestModel, "requestModel");
        Intrinsics.checkParameterIsNotNull(function1, "onApiSuccess");
        getRestApiImpl$default((String) null, 1, (Object) null).removeMultipleCartItem(cartRequestModel, new NetworkExtensionKt$removeMultipleCartItem$1(appBaseActivity, function1), new NetworkExtensionKt$removeMultipleCartItem$2(appBaseActivity));
    }

    public static final void removeFromWishList(AppBaseActivity appBaseActivity, RequestModel requestModel, Function1<? super Boolean, Unit> function1) {
        Intrinsics.checkParameterIsNotNull(appBaseActivity, "$this$removeFromWishList");
        Intrinsics.checkParameterIsNotNull(requestModel, "requestModel");
        Intrinsics.checkParameterIsNotNull(function1, "onSuccess");
        appBaseActivity.showProgress(true);
        if (ExtensionsKt.isNetworkAvailable()) {
            appBaseActivity.showProgress(true);
            getRestApiImpl$default((String) null, 1, (Object) null).removeWishList(requestModel, new NetworkExtensionKt$removeFromWishList$1(appBaseActivity, function1), new NetworkExtensionKt$removeFromWishList$2(appBaseActivity, function1));
            return;
        }
        appBaseActivity.showProgress(false);
        function1.invoke(false);
        ExtensionsKt.noInternetSnackBar(appBaseActivity);
    }

    public static final void addToWishList(AppBaseActivity appBaseActivity, RequestModel requestModel, Function1<? super Boolean, Unit> function1) {
        Intrinsics.checkParameterIsNotNull(appBaseActivity, "$this$addToWishList");
        Intrinsics.checkParameterIsNotNull(requestModel, "requestModel");
        Intrinsics.checkParameterIsNotNull(function1, "onSuccess");
        appBaseActivity.showProgress(true);
        if (ExtensionsKt.isNetworkAvailable()) {
            appBaseActivity.showProgress(true);
            getRestApiImpl$default((String) null, 1, (Object) null).addWishList(requestModel, new NetworkExtensionKt$addToWishList$1(appBaseActivity, function1), new NetworkExtensionKt$addToWishList$2(appBaseActivity, function1));
            return;
        }
        appBaseActivity.showProgress(false);
        function1.invoke(false);
        ExtensionsKt.noInternetSnackBar(appBaseActivity);
    }

    public static final void fetchAndStoreWishListData(AppBaseActivity appBaseActivity, Function1<? super ArrayList<WishList>, Unit> function1) {
        Intrinsics.checkParameterIsNotNull(appBaseActivity, "$this$fetchAndStoreWishListData");
        Intrinsics.checkParameterIsNotNull(function1, "onApiSuccess");
        getRestApiImpl$default((String) null, 1, (Object) null).getWishList(new NetworkExtensionKt$fetchAndStoreWishListData$1(appBaseActivity, function1), new NetworkExtensionKt$fetchAndStoreWishListData$2(appBaseActivity));
    }

    public static final void getOrder(AppBaseActivity appBaseActivity, Function1<? super ArrayList<Order>, Unit> function1) {
        Intrinsics.checkParameterIsNotNull(appBaseActivity, "$this$getOrder");
        Intrinsics.checkParameterIsNotNull(function1, "onApiSuccess");
        if (ExtensionsKt.isNetworkAvailable()) {
            appBaseActivity.showProgress(true);
            getRestApiImpl$default((String) null, 1, (Object) null).getOrderData(new NetworkExtensionKt$getOrder$1(appBaseActivity, function1), new NetworkExtensionKt$getOrder$2(appBaseActivity));
            return;
        }
        appBaseActivity.showProgress(false);
        ExtensionsKt.noInternetSnackBar(appBaseActivity);
    }

    public static final void getOrderTracking(AppBaseActivity appBaseActivity, int i, Function1<? super ArrayList<OrderNotes>, Unit> function1) {
        Intrinsics.checkParameterIsNotNull(appBaseActivity, "$this$getOrderTracking");
        Intrinsics.checkParameterIsNotNull(function1, "onApiSuccess");
        if (ExtensionsKt.isNetworkAvailable()) {
            appBaseActivity.showProgress(true);
            getRestApiImpl$default((String) null, 1, (Object) null).getOrderTracking(i, new NetworkExtensionKt$getOrderTracking$1(appBaseActivity, function1), new NetworkExtensionKt$getOrderTracking$2(appBaseActivity));
            return;
        }
        appBaseActivity.showProgress(false);
        ExtensionsKt.noInternetSnackBar(appBaseActivity);
    }

    public static final void saveProfileImage(AppBaseActivity appBaseActivity, RequestModel requestModel, Function1<? super Boolean, Unit> function1) {
        Intrinsics.checkParameterIsNotNull(appBaseActivity, "$this$saveProfileImage");
        Intrinsics.checkParameterIsNotNull(requestModel, "requestModel");
        Intrinsics.checkParameterIsNotNull(function1, "onApiSuccess");
        if (ExtensionsKt.isNetworkAvailable()) {
            appBaseActivity.showProgress(true);
            getRestApiImpl$default((String) null, 1, (Object) null).saveProfileImage(requestModel, new NetworkExtensionKt$saveProfileImage$1(appBaseActivity, function1), new NetworkExtensionKt$saveProfileImage$2(appBaseActivity, function1));
            return;
        }
        appBaseActivity.showProgress(false);
        function1.invoke(false);
        ExtensionsKt.noInternetSnackBar(appBaseActivity);
    }
}
