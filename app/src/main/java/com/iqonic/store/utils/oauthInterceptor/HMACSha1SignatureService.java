package com.iqonic.store.utils.oauthInterceptor;

import android.util.Base64;
import android.util.Log;
import java.nio.charset.Charset;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.jvm.internal.Intrinsics;
import kotlin.text.StringsKt;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0010\u0012\n\u0002\b\t\u0018\u0000 \u00112\u00020\u0001:\u0001\u0011B\u000f\u0012\b\b\u0002\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u0010\u0010\u0007\u001a\u00020\u00032\u0006\u0010\b\u001a\u00020\tH\u0002J\u0018\u0010\n\u001a\u00020\u00032\u0006\u0010\u000b\u001a\u00020\u00032\u0006\u0010\f\u001a\u00020\u0003H\u0002J \u0010\r\u001a\u00020\u00032\u0006\u0010\u000e\u001a\u00020\u00032\u0006\u0010\u000f\u001a\u00020\u00032\u0006\u0010\u0010\u001a\u00020\u0003H\u0016R\u0014\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006¨\u0006\u0012"}, d2 = {"Lcom/iqonic/store/utils/oauthInterceptor/HMACSha1SignatureService;", "Lcom/iqonic/store/utils/oauthInterceptor/SignatureService;", "signatureMethod", "", "(Ljava/lang/String;)V", "getSignatureMethod", "()Ljava/lang/String;", "bytesToBase64String", "bytes", "", "doSign", "toSign", "keyString", "getSignature", "baseString", "apiSecret", "tokenSecret", "Companion", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: HMACSha1SignatureService.kt */
public final class HMACSha1SignatureService implements SignatureService {
    private static final String CARRIAGE_RETURN = CARRIAGE_RETURN;
    public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
    private static final String HMAC_SHA1 = HMAC_SHA1;
    private static final String METHOD = METHOD;
    private static final String UTF8 = "UTF-8";
    private final String signatureMethod;

    public HMACSha1SignatureService() {
        this((String) null, 1, (DefaultConstructorMarker) null);
    }

    public HMACSha1SignatureService(String str) {
        Intrinsics.checkParameterIsNotNull(str, "signatureMethod");
        this.signatureMethod = str;
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ HMACSha1SignatureService(String str, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this((i & 1) != 0 ? METHOD : str);
    }

    public String getSignatureMethod() {
        return this.signatureMethod;
    }

    public String getSignature(String str, String str2, String str3) {
        Intrinsics.checkParameterIsNotNull(str, "baseString");
        Intrinsics.checkParameterIsNotNull(str2, "apiSecret");
        Intrinsics.checkParameterIsNotNull(str3, "tokenSecret");
        try {
            Preconditions.INSTANCE.checkEmptyString(str, "Base string cant be null or empty string");
            Preconditions.INSTANCE.checkEmptyString(str2, "Api secret cant be null or empty string");
            return doSign(str, OAuthEncoder.INSTANCE.encode(str2) + "&" + OAuthEncoder.INSTANCE.encode(str3));
        } catch (Exception e) {
            throw new OAuthSignatureException(str, e);
        }
    }

    private final String doSign(String str, String str2) throws Exception {
        Log.d("is it signing", "----------------------" + str);
        Log.d("is 22222222", str2 + "");
        Charset forName = Charset.forName(UTF8);
        Intrinsics.checkExpressionValueIsNotNull(forName, "Charset.forName(charsetName)");
        if (str2 != null) {
            byte[] bytes = str2.getBytes(forName);
            Intrinsics.checkExpressionValueIsNotNull(bytes, "(this as java.lang.String).getBytes(charset)");
            SecretKeySpec secretKeySpec = new SecretKeySpec(bytes, HMAC_SHA1);
            Mac instance = Mac.getInstance(HMAC_SHA1);
            instance.init(secretKeySpec);
            Charset forName2 = Charset.forName(UTF8);
            Intrinsics.checkExpressionValueIsNotNull(forName2, "Charset.forName(charsetName)");
            if (str != null) {
                byte[] bytes2 = str.getBytes(forName2);
                Intrinsics.checkExpressionValueIsNotNull(bytes2, "(this as java.lang.String).getBytes(charset)");
                byte[] doFinal = instance.doFinal(bytes2);
                Intrinsics.checkExpressionValueIsNotNull(doFinal, "bytes");
                return StringsKt.replace$default(bytesToBase64String(doFinal), CARRIAGE_RETURN, "", false, 4, (Object) null);
            }
            throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
        }
        throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
    }

    private final String bytesToBase64String(byte[] bArr) {
        String encodeToString = Base64.encodeToString(bArr, 2);
        Intrinsics.checkExpressionValueIsNotNull(encodeToString, "Base64.encodeToString(bytes, Base64.NO_WRAP)");
        return encodeToString;
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0004\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XD¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XD¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004XD¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004XD¢\u0006\u0002\n\u0000¨\u0006\b"}, d2 = {"Lcom/iqonic/store/utils/oauthInterceptor/HMACSha1SignatureService$Companion;", "", "()V", "CARRIAGE_RETURN", "", "HMAC_SHA1", "METHOD", "UTF8", "app_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: HMACSha1SignatureService.kt */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }
}
