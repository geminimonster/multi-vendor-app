package com.iqonic.store.utils;

import android.graphics.drawable.Drawable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import androidx.fragment.app.FragmentActivity;
import com.iqonic.store.utils.extensions.ExtensionsKt;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0011\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u00002\u00020\u0001:\u0002\u0015\u0016B-\u0012\u000e\u0010\u0002\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00040\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\b\u0012\u0006\u0010\t\u001a\u00020\b¢\u0006\u0002\u0010\nR\u0011\u0010\u0005\u001a\u00020\u0006¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0011\u0010\t\u001a\u00020\b¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000eR\u000e\u0010\u000f\u001a\u00020\u0010X\u0004¢\u0006\u0002\n\u0000R\u001b\u0010\u0002\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00040\u0003¢\u0006\n\n\u0002\u0010\u0013\u001a\u0004\b\u0011\u0010\u0012R\u0011\u0010\u0007\u001a\u00020\b¢\u0006\b\n\u0000\u001a\u0004\b\u0014\u0010\u000e¨\u0006\u0017"}, d2 = {"Lcom/iqonic/store/utils/OTPEditText;", "", "mEditTextList", "", "Landroid/widget/EditText;", "context", "Landroidx/fragment/app/FragmentActivity;", "transaparant", "Landroid/graphics/drawable/Drawable;", "dot", "([Landroid/widget/EditText;Landroidx/fragment/app/FragmentActivity;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V", "getContext", "()Landroidx/fragment/app/FragmentActivity;", "getDot", "()Landroid/graphics/drawable/Drawable;", "focusChangeListener", "Landroid/view/View$OnFocusChangeListener;", "getMEditTextList", "()[Landroid/widget/EditText;", "[Landroid/widget/EditText;", "getTransaparant", "CodeTextWatcher", "PinOnKeyListener", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: OTPEditText.kt */
public final class OTPEditText {
    private final FragmentActivity context;
    private final Drawable dot;
    private final View.OnFocusChangeListener focusChangeListener;
    private final EditText[] mEditTextList;
    private final Drawable transaparant;

    public OTPEditText(EditText[] editTextArr, FragmentActivity fragmentActivity, Drawable drawable, Drawable drawable2) {
        Intrinsics.checkParameterIsNotNull(editTextArr, "mEditTextList");
        Intrinsics.checkParameterIsNotNull(fragmentActivity, "context");
        Intrinsics.checkParameterIsNotNull(drawable, "transaparant");
        Intrinsics.checkParameterIsNotNull(drawable2, "dot");
        this.mEditTextList = editTextArr;
        this.context = fragmentActivity;
        this.transaparant = drawable;
        this.dot = drawable2;
        int length = editTextArr.length;
        int i = 0;
        int i2 = 0;
        while (i < length) {
            EditText editText = editTextArr[i];
            int i3 = i2 + 1;
            if (editText != null) {
                editText.setOnKeyListener(new PinOnKeyListener(i2));
            }
            if (editText != null) {
                editText.addTextChangedListener(new CodeTextWatcher(i2));
            }
            if (editText != null) {
                editText.setOnFocusChangeListener(this.focusChangeListener);
            }
            i++;
            i2 = i3;
        }
        this.focusChangeListener = new OTPEditText$focusChangeListener$1(this);
    }

    public final FragmentActivity getContext() {
        return this.context;
    }

    public final Drawable getDot() {
        return this.dot;
    }

    public final EditText[] getMEditTextList() {
        return this.mEditTextList;
    }

    public final Drawable getTransaparant() {
        return this.transaparant;
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\b\u0004\u0018\u00002\u00020\u0001B\u000f\b\u0000\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J \u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\u00032\u0006\u0010\n\u001a\u00020\u000bH\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\f"}, d2 = {"Lcom/iqonic/store/utils/OTPEditText$PinOnKeyListener;", "Landroid/view/View$OnKeyListener;", "mCurrentIndex", "", "(Lcom/iqonic/store/utils/OTPEditText;I)V", "onKey", "", "v", "Landroid/view/View;", "keyCode", "event", "Landroid/view/KeyEvent;", "app_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: OTPEditText.kt */
    public final class PinOnKeyListener implements View.OnKeyListener {
        private final int mCurrentIndex;

        public PinOnKeyListener(int i) {
            this.mCurrentIndex = i;
        }

        public boolean onKey(View view, int i, KeyEvent keyEvent) {
            EditText editText;
            Intrinsics.checkParameterIsNotNull(view, "v");
            Intrinsics.checkParameterIsNotNull(keyEvent, "event");
            if (i == 67 && keyEvent.getAction() == 0) {
                EditText editText2 = OTPEditText.this.getMEditTextList()[this.mCurrentIndex];
                if (!(!(String.valueOf(editText2 != null ? editText2.getText() : null).length() == 0) || this.mCurrentIndex == 0 || (editText = OTPEditText.this.getMEditTextList()[this.mCurrentIndex - 1]) == null)) {
                    editText.requestFocus();
                }
            }
            return false;
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\r\n\u0002\b\b\b\u0004\u0018\u00002\u00020\u0001B\u000f\b\u0000\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u0010\u0010\f\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000fH\u0017J(\u0010\u0010\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u00032\u0006\u0010\u0013\u001a\u00020\u00032\u0006\u0010\u0014\u001a\u00020\u0003H\u0016J\b\u0010\u0015\u001a\u00020\rH\u0002J\b\u0010\u0016\u001a\u00020\rH\u0002J(\u0010\u0017\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u00032\u0006\u0010\u0018\u001a\u00020\u00032\u0006\u0010\u0013\u001a\u00020\u0003H\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\u0005\u001a\u00020\u00068BX\u0004¢\u0006\u0006\u001a\u0004\b\u0005\u0010\u0007R\u000e\u0010\b\u001a\u00020\u0006X\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0006X\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u000e¢\u0006\u0002\n\u0000¨\u0006\u0019"}, d2 = {"Lcom/iqonic/store/utils/OTPEditText$CodeTextWatcher;", "Landroid/text/TextWatcher;", "aCurrentIndex", "", "(Lcom/iqonic/store/utils/OTPEditText;I)V", "isAllEditTextsFilled", "", "()Z", "mIsFirst", "mIsLast", "mNewString", "", "afterTextChanged", "", "s", "Landroid/text/Editable;", "beforeTextChanged", "", "start", "count", "after", "moveToNext", "moveToPrevious", "onTextChanged", "before", "app_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: OTPEditText.kt */
    public final class CodeTextWatcher implements TextWatcher {
        private final int aCurrentIndex;
        private boolean mIsFirst;
        private boolean mIsLast;
        private String mNewString = "";

        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            Intrinsics.checkParameterIsNotNull(charSequence, "s");
        }

        public CodeTextWatcher(int i) {
            this.aCurrentIndex = i;
            if (i == 0) {
                this.mIsFirst = true;
            } else if (i == OTPEditText.this.getMEditTextList().length - 1) {
                this.mIsLast = true;
            }
        }

        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            Intrinsics.checkParameterIsNotNull(charSequence, "s");
            CharSequence obj = charSequence.subSequence(i, i3 + i).toString();
            int length = obj.length() - 1;
            int i4 = 0;
            boolean z = false;
            while (i4 <= length) {
                boolean z2 = obj.charAt(!z ? i4 : length) <= ' ';
                if (!z) {
                    if (!z2) {
                        z = true;
                    } else {
                        i4++;
                    }
                } else if (!z2) {
                    break;
                } else {
                    length--;
                }
            }
            this.mNewString = obj.subSequence(i4, length + 1).toString();
        }

        public void afterTextChanged(Editable editable) {
            Intrinsics.checkParameterIsNotNull(editable, "s");
            String str = this.mNewString;
            boolean z = false;
            if (str.length() > 1) {
                str = String.valueOf(str.charAt(0));
            }
            EditText editText = OTPEditText.this.getMEditTextList()[this.aCurrentIndex];
            if (editText != null) {
                editText.removeTextChangedListener(this);
            }
            EditText editText2 = OTPEditText.this.getMEditTextList()[this.aCurrentIndex];
            if (editText2 != null) {
                editText2.setText(str);
            }
            EditText editText3 = OTPEditText.this.getMEditTextList()[this.aCurrentIndex];
            if (editText3 != null) {
                editText3.setSelection(str.length());
            }
            EditText editText4 = OTPEditText.this.getMEditTextList()[this.aCurrentIndex];
            if (editText4 != null) {
                editText4.addTextChangedListener(this);
            }
            if (str.length() == 1) {
                moveToNext();
                return;
            }
            if (str.length() == 0) {
                z = true;
            }
            if (z) {
                moveToPrevious();
            }
        }

        private final boolean isAllEditTextsFilled() {
            EditText[] mEditTextList = OTPEditText.this.getMEditTextList();
            int length = mEditTextList.length;
            int i = 0;
            while (true) {
                boolean z = true;
                if (i >= length) {
                    return true;
                }
                EditText editText = mEditTextList[i];
                if (String.valueOf(editText != null ? editText.getText() : null).length() != 0) {
                    z = false;
                }
                if (z) {
                    return false;
                }
                i++;
            }
        }

        private final void moveToNext() {
            EditText editText;
            if (!this.mIsLast && (editText = OTPEditText.this.getMEditTextList()[this.aCurrentIndex + 1]) != null) {
                editText.requestFocus();
            }
            if (isAllEditTextsFilled() && this.mIsLast) {
                EditText editText2 = OTPEditText.this.getMEditTextList()[this.aCurrentIndex];
                if (editText2 != null) {
                    editText2.clearFocus();
                }
                FragmentActivity context = OTPEditText.this.getContext();
                if (context != null) {
                    ExtensionsKt.hideSoftKeyboard(context);
                    return;
                }
                throw new TypeCastException("null cannot be cast to non-null type android.app.Activity");
            }
        }

        private final void moveToPrevious() {
            EditText editText;
            if (!this.mIsFirst && (editText = OTPEditText.this.getMEditTextList()[this.aCurrentIndex - 1]) != null) {
                editText.requestFocus();
            }
            EditText editText2 = OTPEditText.this.getMEditTextList()[this.aCurrentIndex];
            if (editText2 == null) {
                Intrinsics.throwNpe();
            }
            editText2.setBackground(OTPEditText.this.getDot());
            if (this.mIsFirst) {
                EditText editText3 = OTPEditText.this.getMEditTextList()[this.aCurrentIndex];
                if (editText3 == null) {
                    Intrinsics.throwNpe();
                }
                editText3.setBackground(OTPEditText.this.getTransaparant());
            }
        }
    }
}
