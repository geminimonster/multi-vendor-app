package com.iqonic.store.utils.extensions;

import android.os.Build;
import android.text.Html;
import android.text.Spanned;
import java.util.List;
import java.util.ListIterator;
import java.util.Locale;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.collections.CollectionsKt;
import kotlin.jvm.internal.Intrinsics;
import kotlin.text.Regex;
import kotlin.text.StringsKt;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\u001a\u000e\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0001\u001a\n\u0010\u0003\u001a\u00020\u0004*\u00020\u0001\u001a\u0014\u0010\u0005\u001a\u00020\u0001*\u00020\u00012\b\b\u0002\u0010\u0006\u001a\u00020\u0001\u001a\n\u0010\u0007\u001a\u00020\b*\u00020\u0001\u001a\n\u0010\t\u001a\u00020\u0004*\u00020\u0001\u001a\n\u0010\n\u001a\u00020\u0001*\u00020\u0001¨\u0006\u000b"}, d2 = {"toProperCase", "", "str", "checkIsEmpty", "", "currencyFormat", "code", "getHtmlString", "Landroid/text/Spanned;", "isValidColor", "toCamelCase", "app_release"}, k = 2, mv = {1, 1, 16})
/* compiled from: StringExtensions.kt */
public final class StringExtensionsKt {
    public static final boolean checkIsEmpty(String str) {
        Intrinsics.checkParameterIsNotNull(str, "$this$checkIsEmpty");
        return (str.length() == 0) || Intrinsics.areEqual((Object) "", (Object) str) || StringsKt.equals(str, "null", true);
    }

    public static final Spanned getHtmlString(String str) {
        Intrinsics.checkParameterIsNotNull(str, "$this$getHtmlString");
        if (Build.VERSION.SDK_INT >= 24) {
            Spanned fromHtml = Html.fromHtml(str, 0);
            Intrinsics.checkExpressionValueIsNotNull(fromHtml, "Html.fromHtml(this, Html…at.FROM_HTML_MODE_LEGACY)");
            return fromHtml;
        }
        Spanned fromHtml2 = Html.fromHtml(str);
        Intrinsics.checkExpressionValueIsNotNull(fromHtml2, "Html.fromHtml(this)");
        return fromHtml2;
    }

    public static final String toCamelCase(String str) {
        List list;
        boolean z;
        Intrinsics.checkParameterIsNotNull(str, "$this$toCamelCase");
        StringBuilder sb = new StringBuilder();
        try {
            Locale locale = Locale.getDefault();
            Intrinsics.checkExpressionValueIsNotNull(locale, "Locale.getDefault()");
            String lowerCase = str.toLowerCase(locale);
            Intrinsics.checkExpressionValueIsNotNull(lowerCase, "(this as java.lang.String).toLowerCase(locale)");
            if (lowerCase.length() > 0) {
                CharSequence charSequence = lowerCase;
                int length = charSequence.length() - 1;
                int i = 0;
                boolean z2 = false;
                while (true) {
                    if (i > length) {
                        break;
                    }
                    boolean z3 = charSequence.charAt(!z2 ? i : length) <= ' ';
                    if (!z2) {
                        if (!z3) {
                            z2 = true;
                        } else {
                            i++;
                        }
                    } else if (!z3) {
                        break;
                    } else {
                        length--;
                    }
                }
                List<String> split = new Regex(" ").split(charSequence.subSequence(i, length + 1).toString(), 0);
                if (!split.isEmpty()) {
                    ListIterator<String> listIterator = split.listIterator(split.size());
                    while (true) {
                        if (!listIterator.hasPrevious()) {
                            break;
                        }
                        if (listIterator.previous().length() == 0) {
                            z = true;
                            continue;
                        } else {
                            z = false;
                            continue;
                        }
                        if (!z) {
                            list = CollectionsKt.take(split, listIterator.nextIndex() + 1);
                            break;
                        }
                    }
                }
                list = CollectionsKt.emptyList();
                Object[] array = list.toArray(new String[0]);
                if (array != null) {
                    for (String properCase : (String[]) array) {
                        sb.append(" ");
                        sb.append(toProperCase(properCase));
                    }
                } else {
                    throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
                }
            }
        } catch (NullPointerException unused) {
            sb = new StringBuilder();
        }
        String sb2 = sb.toString();
        Intrinsics.checkExpressionValueIsNotNull(sb2, "stringBuilder.toString()");
        return sb2;
    }

    public static /* synthetic */ String currencyFormat$default(String str, String str2, int i, Object obj) {
        if ((i & 1) != 0) {
            str2 = "INR";
        }
        return currencyFormat(str, str2);
    }

    public static final String currencyFormat(String str, String str2) {
        Intrinsics.checkParameterIsNotNull(str, "$this$currencyFormat");
        Intrinsics.checkParameterIsNotNull(str2, "code");
        if (checkIsEmpty(str)) {
            return "";
        }
        return getHtmlString(AppExtensionsKt.getDefaultCurrency()) + str;
    }

    public static final String toProperCase(String str) {
        Intrinsics.checkParameterIsNotNull(str, "str");
        try {
            if (!(str.length() > 0)) {
                return "";
            }
            StringBuilder sb = new StringBuilder();
            String substring = str.substring(0, 1);
            Intrinsics.checkExpressionValueIsNotNull(substring, "(this as java.lang.Strin…ing(startIndex, endIndex)");
            Locale locale = Locale.getDefault();
            Intrinsics.checkExpressionValueIsNotNull(locale, "Locale.getDefault()");
            if (substring != null) {
                String upperCase = substring.toUpperCase(locale);
                Intrinsics.checkExpressionValueIsNotNull(upperCase, "(this as java.lang.String).toUpperCase(locale)");
                sb.append(upperCase);
                String substring2 = str.substring(1);
                Intrinsics.checkExpressionValueIsNotNull(substring2, "(this as java.lang.String).substring(startIndex)");
                Locale locale2 = Locale.getDefault();
                Intrinsics.checkExpressionValueIsNotNull(locale2, "Locale.getDefault()");
                if (substring2 != null) {
                    String lowerCase = substring2.toLowerCase(locale2);
                    Intrinsics.checkExpressionValueIsNotNull(lowerCase, "(this as java.lang.String).toLowerCase(locale)");
                    sb.append(lowerCase);
                    return sb.toString();
                }
                throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
            }
            throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
        } catch (NullPointerException unused) {
            return "";
        }
    }

    public static final boolean isValidColor(String str) {
        Intrinsics.checkParameterIsNotNull(str, "$this$isValidColor");
        return StringsKt.contains$default((CharSequence) str, (CharSequence) "#", false, 2, (Object) null) && str.length() >= 6;
    }
}
