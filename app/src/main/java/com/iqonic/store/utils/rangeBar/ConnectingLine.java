package com.iqonic.store.utils.rangeBar;

import android.graphics.Canvas;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Shader;
import java.util.ArrayList;

public class ConnectingLine {
    private final int[] colors;
    private final float mY;
    private final Paint paint = new Paint();
    private final float[] positions;

    public ConnectingLine(float f, float f2, ArrayList<Integer> arrayList) {
        if (arrayList.size() == 1) {
            arrayList.add(arrayList.get(0));
        }
        this.colors = new int[arrayList.size()];
        this.positions = new float[arrayList.size()];
        for (int i = 0; i < arrayList.size(); i++) {
            this.colors[i] = arrayList.get(i).intValue();
            this.positions[i] = ((float) i) / ((float) (arrayList.size() - 1));
        }
        this.paint.setStrokeWidth(f2);
        this.paint.setStrokeCap(Paint.Cap.ROUND);
        this.paint.setAntiAlias(true);
        this.mY = f;
    }

    private LinearGradient getLinearGradient(float f, float f2, float f3) {
        return new LinearGradient(f, f3, f2, f3, this.colors, this.positions, Shader.TileMode.REPEAT);
    }

    public void draw(Canvas canvas, PinView pinView, PinView pinView2) {
        this.paint.setShader(getLinearGradient(0.0f, (float) canvas.getWidth(), this.mY));
        canvas.drawLine(pinView.getX(), this.mY, pinView2.getX(), this.mY, this.paint);
    }

    public void draw(Canvas canvas, float f, PinView pinView) {
        this.paint.setShader(getLinearGradient(0.0f, (float) canvas.getWidth(), this.mY));
        canvas.drawLine(f, this.mY, pinView.getX(), this.mY, this.paint);
    }
}
