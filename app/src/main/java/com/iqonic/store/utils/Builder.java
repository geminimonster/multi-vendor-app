package com.iqonic.store.utils;

import android.content.Intent;
import android.content.IntentFilter;
import com.facebook.internal.NativeProtocol;
import com.iqonic.store.utils.Instructions;
import java.util.ArrayList;
import java.util.List;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\u0007\b\u0000¢\u0006\u0002\u0010\u0002J\r\u0010\u0003\u001a\u00020\u0004H\u0000¢\u0006\u0002\b\bJ\u0013\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006H\u0000¢\u0006\u0002\b\tJ&\u0010\n\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\r2\u0016\u0010\u000e\u001a\u0012\u0012\u0004\u0012\u00020\u0010\u0012\u0004\u0012\u00020\u000b0\u000fj\u0002`\u0011J&\u0010\u0012\u001a\u00020\u000b2\u0006\u0010\u0013\u001a\u00020\r2\u0016\u0010\u000e\u001a\u0012\u0012\u0004\u0012\u00020\u0010\u0012\u0004\u0012\u00020\u000b0\u000fj\u0002`\u0011J&\u0010\u0014\u001a\u00020\u000b2\u0006\u0010\u0015\u001a\u00020\r2\u0016\u0010\u000e\u001a\u0012\u0012\u0004\u0012\u00020\u0010\u0012\u0004\u0012\u00020\u000b0\u000fj\u0002`\u0011R\u000e\u0010\u0003\u001a\u00020\u0004X\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0016"}, d2 = {"Lcom/iqonic/store/utils/Builder;", "", "()V", "filter", "Landroid/content/IntentFilter;", "instructions", "", "Lcom/iqonic/store/utils/Instructions;", "filter$app_release", "instructions$app_release", "onAction", "", "action", "", "execution", "Lkotlin/Function1;", "Landroid/content/Intent;", "Lcom/iqonic/store/utils/Execution;", "onCategory", "category", "onDataScheme", "scheme", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: BroadcastReceiverExt.kt */
public final class Builder {
    private final IntentFilter filter = new IntentFilter();
    private final List<Instructions> instructions = new ArrayList();

    public final void onAction(String str, Function1<? super Intent, Unit> function1) {
        Intrinsics.checkParameterIsNotNull(str, NativeProtocol.WEB_DIALOG_ACTION);
        Intrinsics.checkParameterIsNotNull(function1, "execution");
        this.filter.addAction(str);
        this.instructions.add(new Instructions.OnAction(str, function1));
    }

    public final void onDataScheme(String str, Function1<? super Intent, Unit> function1) {
        Intrinsics.checkParameterIsNotNull(str, "scheme");
        Intrinsics.checkParameterIsNotNull(function1, "execution");
        this.filter.addDataScheme(str);
        this.instructions.add(new Instructions.OnDataScheme(str, function1));
    }

    public final void onCategory(String str, Function1<? super Intent, Unit> function1) {
        Intrinsics.checkParameterIsNotNull(str, "category");
        Intrinsics.checkParameterIsNotNull(function1, "execution");
        this.filter.addCategory(str);
        this.instructions.add(new Instructions.OnCategory(str, function1));
    }

    public final IntentFilter filter$app_release() {
        return this.filter;
    }

    public final List<Instructions> instructions$app_release() {
        return this.instructions;
    }
}
