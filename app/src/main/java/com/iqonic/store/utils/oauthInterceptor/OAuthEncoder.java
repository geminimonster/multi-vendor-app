package com.iqonic.store.utils.oauthInterceptor;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import kotlin.text.Regex;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010$\n\u0002\b\b\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J \u0010\u0007\u001a\u00020\u00042\u0006\u0010\b\u001a\u00020\u00042\u0006\u0010\t\u001a\u00020\u00042\u0006\u0010\n\u001a\u00020\u0004H\u0002J\u000e\u0010\u000b\u001a\u00020\u00042\u0006\u0010\b\u001a\u00020\u0004J\u000e\u0010\f\u001a\u00020\u00042\u0006\u0010\r\u001a\u00020\u0004R\u000e\u0010\u0003\u001a\u00020\u0004XD¢\u0006\u0002\n\u0000R\u001a\u0010\u0005\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00040\u0006X\u0004¢\u0006\u0002\n\u0000¨\u0006\u000e"}, d2 = {"Lcom/iqonic/store/utils/oauthInterceptor/OAuthEncoder;", "", "()V", "CHARSET", "", "ENCODING_RULES", "", "applyRule", "encoded", "toReplace", "replacement", "decode", "encode", "plain", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: OAuthEncoder.kt */
public final class OAuthEncoder {
    private static final String CHARSET = "UTF-8";
    private static final Map<String, String> ENCODING_RULES;
    public static final OAuthEncoder INSTANCE = new OAuthEncoder();

    static {
        HashMap hashMap = new HashMap();
        hashMap.put("*", "%2A");
        hashMap.put("+", "%20");
        hashMap.put("%7E", "~");
        Map<String, String> unmodifiableMap = Collections.unmodifiableMap(hashMap);
        Intrinsics.checkExpressionValueIsNotNull(unmodifiableMap, "Collections.unmodifiableMap<String, String>(rules)");
        ENCODING_RULES = unmodifiableMap;
    }

    private OAuthEncoder() {
    }

    public final String encode(String str) {
        Intrinsics.checkParameterIsNotNull(str, "plain");
        try {
            String encode = URLEncoder.encode(str, CHARSET);
            Intrinsics.checkExpressionValueIsNotNull(encode, "URLEncoder.encode(plain, CHARSET)");
            for (Map.Entry next : ENCODING_RULES.entrySet()) {
                encode = applyRule(encode, (String) next.getKey(), (String) next.getValue());
            }
            return encode;
        } catch (UnsupportedEncodingException e) {
            throw new OAuthException("Charset not found while encoding string: " + CHARSET, e);
        }
    }

    private final String applyRule(String str, String str2, String str3) {
        String quote = Pattern.quote(str2);
        Intrinsics.checkExpressionValueIsNotNull(quote, "(Pattern.quote(toReplace))");
        return new Regex(quote).replace((CharSequence) str, str3);
    }

    public final String decode(String str) {
        Intrinsics.checkParameterIsNotNull(str, "encoded");
        try {
            String decode = URLDecoder.decode(str, CHARSET);
            Intrinsics.checkExpressionValueIsNotNull(decode, "URLDecoder.decode(encoded, CHARSET)");
            return decode;
        } catch (UnsupportedEncodingException e) {
            throw new OAuthException("Charset not found while decoding string: " + CHARSET, e);
        }
    }
}
