package com.iqonic.store.utils.extensions;

import com.google.gson.reflect.TypeToken;
import com.iqonic.store.models.CountryModel;
import java.util.ArrayList;
import kotlin.Metadata;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0017\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000*\u0001\u0000\b\n\u0018\u00002\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u00030\u0002j\b\u0012\u0004\u0012\u00020\u0003`\u00040\u0001¨\u0006\u0005"}, d2 = {"com/iqonic/store/utils/extensions/AppExtensionsKt$fetchCountry$3", "Lcom/google/gson/reflect/TypeToken;", "Ljava/util/ArrayList;", "Lcom/iqonic/store/models/CountryModel;", "Lkotlin/collections/ArrayList;", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: AppExtensions.kt */
public final class AppExtensionsKt$fetchCountry$3 extends TypeToken<ArrayList<CountryModel>> {
    AppExtensionsKt$fetchCountry$3() {
    }
}
