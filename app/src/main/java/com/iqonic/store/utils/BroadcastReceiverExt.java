package com.iqonic.store.utils;

import android.content.Context;
import android.content.IntentFilter;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.OnLifecycleEvent;
import java.util.List;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000C\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003*\u0001\u000f\u0018\u0000*\f\b\u0000\u0010\u0001*\u00020\u0002*\u00020\u00032\u00020\u0004B&\u0012\u0006\u0010\u0005\u001a\u00028\u0000\u0012\u0017\u0010\u0006\u001a\u0013\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\t0\u0007¢\u0006\u0002\b\n¢\u0006\u0002\u0010\u000bJ\b\u0010\u0016\u001a\u00020\tH\u0007J\b\u0010\u0017\u001a\u00020\tH\u0007R\u0016\u0010\f\u001a\n \r*\u0004\u0018\u00010\u00020\u0002X\u0004¢\u0006\u0002\n\u0000R\u0016\u0010\u000e\u001a\b\u0012\u0004\u0012\u00028\u00000\u000fX\u0004¢\u0006\u0004\n\u0002\u0010\u0010R\u000e\u0010\u0011\u001a\u00020\u0012X\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u00150\u0014X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0018"}, d2 = {"Lcom/iqonic/store/utils/BroadcastReceiverExt;", "T", "Landroid/content/Context;", "Landroidx/lifecycle/LifecycleOwner;", "Landroidx/lifecycle/LifecycleObserver;", "context", "constructor", "Lkotlin/Function1;", "Lcom/iqonic/store/utils/Builder;", "", "Lkotlin/ExtensionFunctionType;", "(Landroid/content/Context;Lkotlin/jvm/functions/Function1;)V", "appContext", "kotlin.jvm.PlatformType", "broadcastReceiver", "com/iqonic/store/utils/BroadcastReceiverExt$broadcastReceiver$1", "Lcom/iqonic/store/utils/BroadcastReceiverExt$broadcastReceiver$1;", "filter", "Landroid/content/IntentFilter;", "instructions", "", "Lcom/iqonic/store/utils/Instructions;", "start", "stop", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: BroadcastReceiverExt.kt */
public final class BroadcastReceiverExt<T extends Context & LifecycleOwner> implements LifecycleObserver {
    private final Context appContext;
    private final BroadcastReceiverExt$broadcastReceiver$1 broadcastReceiver = new BroadcastReceiverExt$broadcastReceiver$1(this);
    private final IntentFilter filter;
    /* access modifiers changed from: private */
    public final List<Instructions> instructions;

    public BroadcastReceiverExt(T t, Function1<? super Builder, Unit> function1) {
        Intrinsics.checkParameterIsNotNull(t, "context");
        Intrinsics.checkParameterIsNotNull(function1, "constructor");
        this.appContext = t.getApplicationContext();
        Builder builder = new Builder();
        function1.invoke(builder);
        this.filter = builder.filter$app_release();
        this.instructions = builder.instructions$app_release();
        ((LifecycleOwner) t).getLifecycle().addObserver(this);
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    public final void start() {
        this.appContext.registerReceiver(this.broadcastReceiver, this.filter);
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    public final void stop() {
        this.appContext.unregisterReceiver(this.broadcastReceiver);
    }
}
