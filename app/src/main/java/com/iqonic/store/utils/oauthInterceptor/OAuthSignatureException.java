package com.iqonic.store.utils.oauthInterceptor;

import kotlin.Metadata;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u0000 \b2\u00020\u0001:\u0001\bB\u0019\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\u0010\u0004\u001a\u00060\u0005j\u0002`\u0006¢\u0006\u0002\u0010\u0007¨\u0006\t"}, d2 = {"Lcom/iqonic/store/utils/oauthInterceptor/OAuthSignatureException;", "Lcom/iqonic/store/utils/oauthInterceptor/OAuthException;", "stringToSign", "", "e", "Ljava/lang/Exception;", "Lkotlin/Exception;", "(Ljava/lang/String;Ljava/lang/Exception;)V", "Companion", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: OAuthSignatureException.kt */
public final class OAuthSignatureException extends OAuthException {
    public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
    private static final String MSG = MSG;
    private static final long serialVersionUID = 1;

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\t\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XD¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006XD¢\u0006\u0002\n\u0000¨\u0006\u0007"}, d2 = {"Lcom/iqonic/store/utils/oauthInterceptor/OAuthSignatureException$Companion;", "", "()V", "MSG", "", "serialVersionUID", "", "app_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: OAuthSignatureException.kt */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public OAuthSignatureException(java.lang.String r5, java.lang.Exception r6) {
        /*
            r4 = this;
            java.lang.String r0 = "stringToSign"
            kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(r5, r0)
            java.lang.String r0 = "e"
            kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(r6, r0)
            kotlin.jvm.internal.StringCompanionObject r0 = kotlin.jvm.internal.StringCompanionObject.INSTANCE
            java.lang.String r0 = MSG
            r1 = 1
            java.lang.Object[] r2 = new java.lang.Object[r1]
            r3 = 0
            r2[r3] = r5
            java.lang.Object[] r5 = java.util.Arrays.copyOf(r2, r1)
            java.lang.String r5 = java.lang.String.format(r0, r5)
            java.lang.String r0 = "java.lang.String.format(format, *args)"
            kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r5, r0)
            r4.<init>(r5, r6)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.iqonic.store.utils.oauthInterceptor.OAuthSignatureException.<init>(java.lang.String, java.lang.Exception):void");
    }
}
