package com.iqonic.store.utils.extensions;

import com.iqonic.store.utils.Constants;
import java.text.SimpleDateFormat;
import java.util.Date;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\u001a\u0018\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00012\b\b\u0002\u0010\u0003\u001a\u00020\u0004¨\u0006\u0005"}, d2 = {"toDate", "", "string", "currentFormat", "", "app_release"}, k = 2, mv = {1, 1, 16})
/* compiled from: DateExtensions.kt */
public final class DateExtensionsKt {
    public static /* synthetic */ String toDate$default(String str, int i, int i2, Object obj) {
        if ((i2 & 2) != 0) {
            i = 0;
        }
        return toDate(str, i);
    }

    public static final String toDate(String str, int i) {
        Intrinsics.checkParameterIsNotNull(str, "string");
        if (i == 0) {
            SimpleDateFormat dd_mmm_yyyy = Constants.INSTANCE.getDD_MMM_YYYY();
            Date parse = Constants.INSTANCE.getFULL_DATE_FORMATTER().parse(str);
            if (parse == null) {
                Intrinsics.throwNpe();
            }
            String format = dd_mmm_yyyy.format(parse);
            Intrinsics.checkExpressionValueIsNotNull(format, "Constants.DD_MMM_YYYY.fo…ORMATTER.parse(string)!!)");
            return format;
        } else if (i != 1) {
            return str;
        } else {
            SimpleDateFormat dd_mmm_yyyy2 = Constants.INSTANCE.getDD_MMM_YYYY();
            Date parse2 = Constants.INSTANCE.getYYYY_MM_DD().parse(str);
            if (parse2 == null) {
                Intrinsics.throwNpe();
            }
            String format2 = dd_mmm_yyyy2.format(parse2);
            Intrinsics.checkExpressionValueIsNotNull(format2, "Constants.DD_MMM_YYYY.fo…YY_MM_DD.parse(string)!!)");
            return format2;
        }
    }
}
