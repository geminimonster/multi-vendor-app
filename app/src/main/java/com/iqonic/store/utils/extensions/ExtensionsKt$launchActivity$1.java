package com.iqonic.store.utils.extensions;

import android.content.Intent;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001\"\n\b\u0000\u0010\u0002\u0018\u0001*\u00020\u0003*\u00020\u0004H\n¢\u0006\u0002\b\u0005"}, d2 = {"<anonymous>", "", "T", "", "Landroid/content/Intent;", "invoke"}, k = 3, mv = {1, 1, 16})
/* compiled from: Extensions.kt */
public final class ExtensionsKt$launchActivity$1 extends Lambda implements Function1<Intent, Unit> {
    public static final ExtensionsKt$launchActivity$1 INSTANCE = new ExtensionsKt$launchActivity$1();

    public ExtensionsKt$launchActivity$1() {
        super(1);
    }

    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        invoke((Intent) obj);
        return Unit.INSTANCE;
    }

    public final void invoke(Intent intent) {
        Intrinsics.checkParameterIsNotNull(intent, "$receiver");
    }
}
