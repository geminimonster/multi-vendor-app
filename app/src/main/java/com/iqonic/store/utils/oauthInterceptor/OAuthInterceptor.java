package com.iqonic.store.utils.oauthInterceptor;

import android.util.Log;
import com.bumptech.glide.load.Key;
import com.iqonic.store.utils.Constants;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import kotlin.text.StringsKt;
import kotlin.text.Typography;
import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u0000 \u000f2\u00020\u0001:\u0002\u000e\u000fB%\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0003\u0012\u0006\u0010\u0006\u001a\u00020\u0003¢\u0006\u0002\u0010\u0007J\u0010\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000bH\u0016J\u0010\u0010\f\u001a\u00020\u00032\u0006\u0010\r\u001a\u00020\u0003H\u0002R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0010"}, d2 = {"Lcom/iqonic/store/utils/oauthInterceptor/OAuthInterceptor;", "Lokhttp3/Interceptor;", "consumerKey", "", "consumerSecret", "tokenSecret", "token", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", "intercept", "Lokhttp3/Response;", "chain", "Lokhttp3/Interceptor$Chain;", "urlEncoded", "url", "Builder", "Companion", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: OAuthInterceptor.kt */
public final class OAuthInterceptor implements Interceptor {
    public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
    private static final String OAUTH_CONSUMER_KEY = "oauth_consumer_key";
    private static final String OAUTH_NONCE = "oauth_nonce";
    private static final String OAUTH_SIGNATURE = "oauth_signature";
    private static final String OAUTH_SIGNATURE_METHOD = "oauth_signature_method";
    private static final String OAUTH_SIGNATURE_METHOD_VALUE = "HMAC-SHA1";
    private static final String OAUTH_TIMESTAMP = "oauth_timestamp";
    private static final String OAUTH_TOKEN = "oauth_token";
    private static final String OAUTH_VERSION = "oauth_version";
    private static final String OAUTH_VERSION_VALUE = "1.0";
    private final String consumerKey;
    private final String consumerSecret;
    private final String token;
    private final String tokenSecret;

    public OAuthInterceptor(String str, String str2, String str3, String str4) {
        Intrinsics.checkParameterIsNotNull(str, Constants.SharedPref.CONSUMERKEY);
        Intrinsics.checkParameterIsNotNull(str2, Constants.SharedPref.CONSUMERSECRET);
        Intrinsics.checkParameterIsNotNull(str3, "tokenSecret");
        Intrinsics.checkParameterIsNotNull(str4, "token");
        this.consumerKey = str;
        this.consumerSecret = str2;
        this.tokenSecret = str3;
        this.token = str4;
    }

    public Response intercept(Interceptor.Chain chain) throws IOException {
        String str;
        Intrinsics.checkParameterIsNotNull(chain, "chain");
        Request request = chain.request();
        HttpUrl url = request.url();
        String nonce = new TimestampServiceImpl().getNonce();
        String timestampInSeconds = new TimestampServiceImpl().getTimestampInSeconds();
        String str2 = request.url().scheme() + "://" + request.url().host() + request.url().encodedPath();
        String str3 = request.method() + "&" + urlEncoded(str2);
        if (request.url().encodedQuery() != null) {
            str = Intrinsics.stringPlus(request.url().encodedQuery(), "&oauth_consumer_key=" + this.consumerKey + "&oauth_nonce=" + nonce + "&oauth_token=" + this.token + "&oauth_signature_method=HMAC-SHA1&oauth_timestamp=" + timestampInSeconds + "&oauth_version=1.0");
        } else {
            str = "oauth_consumer_key=" + this.consumerKey + "&oauth_nonce=" + nonce + "&oauth_token=" + this.token + "&oauth_signature_method=HMAC-SHA1&oauth_timestamp=" + timestampInSeconds + "&oauth_version=1.0";
        }
        ParameterList parameterList = new ParameterList();
        parameterList.addQuerystring(str);
        String asOauthBaseString = parameterList.sort().asOauthBaseString();
        String str4 = Typography.amp + asOauthBaseString;
        if (StringsKt.contains$default((CharSequence) str3, (CharSequence) "%3F", false, 2, (Object) null)) {
            str4 = "%26" + urlEncoded(asOauthBaseString);
        }
        String signature = new HMACSha1SignatureService((String) null, 1, (DefaultConstructorMarker) null).getSignature(str3 + str4, this.consumerSecret, this.tokenSecret);
        Log.d("Signature", signature);
        return chain.proceed(request.newBuilder().url(url.newBuilder().addQueryParameter(OAUTH_SIGNATURE_METHOD, OAUTH_SIGNATURE_METHOD_VALUE).addQueryParameter(OAUTH_CONSUMER_KEY, this.consumerKey).addQueryParameter(OAUTH_TOKEN, this.token).addQueryParameter(OAUTH_VERSION, OAUTH_VERSION_VALUE).addQueryParameter(OAUTH_TIMESTAMP, timestampInSeconds).addQueryParameter(OAUTH_NONCE, nonce).addQueryParameter(OAUTH_SIGNATURE, signature).build()).build());
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u0006\u0010\n\u001a\u00020\u000bJ\u000e\u0010\u0003\u001a\u00020\u00002\u0006\u0010\u0003\u001a\u00020\u0004J\u000e\u0010\u0005\u001a\u00020\u00002\u0006\u0010\u0005\u001a\u00020\u0004J\u000e\u0010\u0006\u001a\u00020\u00002\u0006\u0010\u0006\u001a\u00020\u0004J\u000e\u0010\u0007\u001a\u00020\u00002\u0006\u0010\u0007\u001a\u00020\u0004R\u000e\u0010\u0003\u001a\u00020\u0004X\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004X\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004X\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tXD¢\u0006\u0002\n\u0000¨\u0006\f"}, d2 = {"Lcom/iqonic/store/utils/oauthInterceptor/OAuthInterceptor$Builder;", "", "()V", "consumerKey", "", "consumerSecret", "token", "tokenSecret", "type", "", "build", "Lcom/iqonic/store/utils/oauthInterceptor/OAuthInterceptor;", "app_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: OAuthInterceptor.kt */
    public static final class Builder {
        private String consumerKey = "";
        private String consumerSecret = "";
        private String token = "";
        private String tokenSecret = "";
        private final int type;

        public final Builder consumerKey(String str) {
            Intrinsics.checkParameterIsNotNull(str, Constants.SharedPref.CONSUMERKEY);
            this.consumerKey = str;
            return this;
        }

        public final Builder consumerSecret(String str) {
            Intrinsics.checkParameterIsNotNull(str, Constants.SharedPref.CONSUMERSECRET);
            this.consumerSecret = str;
            return this;
        }

        public final Builder tokenSecret(String str) {
            Intrinsics.checkParameterIsNotNull(str, "tokenSecret");
            this.tokenSecret = str;
            return this;
        }

        public final Builder token(String str) {
            Intrinsics.checkParameterIsNotNull(str, "token");
            this.token = str;
            return this;
        }

        public final OAuthInterceptor build() {
            return new OAuthInterceptor(this.consumerKey, this.consumerSecret, this.tokenSecret, this.token);
        }
    }

    private final String urlEncoded(String str) {
        try {
            String encode = URLEncoder.encode(str, Key.STRING_CHARSET_NAME);
            Intrinsics.checkExpressionValueIsNotNull(encode, "URLEncoder.encode(url, \"UTF-8\")");
            return encode;
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return "";
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\t\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\r"}, d2 = {"Lcom/iqonic/store/utils/oauthInterceptor/OAuthInterceptor$Companion;", "", "()V", "OAUTH_CONSUMER_KEY", "", "OAUTH_NONCE", "OAUTH_SIGNATURE", "OAUTH_SIGNATURE_METHOD", "OAUTH_SIGNATURE_METHOD_VALUE", "OAUTH_TIMESTAMP", "OAUTH_TOKEN", "OAUTH_VERSION", "OAUTH_VERSION_VALUE", "app_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: OAuthInterceptor.kt */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }
}
