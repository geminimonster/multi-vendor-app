package com.iqonic.store.utils.extensions;

import androidx.fragment.app.FragmentActivity;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Lambda;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n¢\u0006\u0002\b\u0002"}, d2 = {"<anonymous>", "", "invoke"}, k = 3, mv = {1, 1, 16})
/* compiled from: Extensions.kt */
final class ExtensionsKt$requestPermissions$1 extends Lambda implements Function0<Unit> {
    final /* synthetic */ PermissionObserver $observer;
    final /* synthetic */ Function1 $onResult;
    final /* synthetic */ String[] $permissions;
    final /* synthetic */ FragmentActivity $this_requestPermissions;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    ExtensionsKt$requestPermissions$1(FragmentActivity fragmentActivity, PermissionObserver permissionObserver, Function1 function1, String[] strArr) {
        super(0);
        this.$this_requestPermissions = fragmentActivity;
        this.$observer = permissionObserver;
        this.$onResult = function1;
        this.$permissions = strArr;
    }

    public final void invoke() {
        this.$this_requestPermissions.getLifecycle().removeObserver(this.$observer);
        this.$onResult.invoke(Boolean.valueOf(ContextExtensionsKt.isPermissionGranted(this.$this_requestPermissions, this.$permissions)));
    }
}
