package com.iqonic.store.utils.extensions;

import androidx.core.app.NotificationCompat;
import com.google.gson.Gson;
import com.iqonic.store.ShopHopApp;
import com.store.proshop.R;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import okhttp3.Request;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000%\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0003\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000*\u0001\u0000\b\n\u0018\u00002\b\u0012\u0004\u0012\u00028\u00000\u0001J\u001e\u0010\u0002\u001a\u00020\u00032\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00028\u00000\u00052\u0006\u0010\u0006\u001a\u00020\u0007H\u0016J$\u0010\b\u001a\u00020\u00032\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00028\u00000\u00052\f\u0010\t\u001a\b\u0012\u0004\u0012\u00028\u00000\nH\u0016¨\u0006\u000b"}, d2 = {"com/iqonic/store/utils/extensions/NetworkExtensionKt$callApi$4", "Lretrofit2/Callback;", "onFailure", "", "call", "Lretrofit2/Call;", "t", "", "onResponse", "response", "Lretrofit2/Response;", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: NetworkExtension.kt */
public final class NetworkExtensionKt$callApi$4 implements Callback<T> {
    final /* synthetic */ Function1 $onApiError;
    final /* synthetic */ Function1 $onApiSuccess;
    final /* synthetic */ Function0 $onNetworkError;

    NetworkExtensionKt$callApi$4(Function1 function1, Function1 function12, Function0 function0) {
        this.$onApiSuccess = function1;
        this.$onApiError = function12;
        this.$onNetworkError = function0;
    }

    public void onResponse(Call<T> call, Response<T> response) {
        Intrinsics.checkParameterIsNotNull(call, NotificationCompat.CATEGORY_CALL);
        Intrinsics.checkParameterIsNotNull(response, "response");
        if (response.isSuccessful()) {
            T body = response.body();
            if (body != null) {
                this.$onApiSuccess.invoke(body);
                Request request = call.request();
                Intrinsics.checkExpressionValueIsNotNull(request, "call.request()");
                String json = new Gson().toJson((Object) body);
                Intrinsics.checkExpressionValueIsNotNull(json, "Gson().toJson(body)");
                NetworkExtensionKt.logData$default(request, json, response.raw().receivedResponseAtMillis() - response.raw().sentRequestAtMillis(), false, 8, (Object) null);
                return;
            }
            this.$onApiError.invoke("Please try again later.");
            Request request2 = call.request();
            Intrinsics.checkExpressionValueIsNotNull(request2, "call.request()");
            NetworkExtensionKt.logData(request2, "Response body is null", response.raw().receivedResponseAtMillis() - response.raw().sentRequestAtMillis(), true);
            return;
        }
        ResponseBody errorBody = response.errorBody();
        if (errorBody == null) {
            Intrinsics.throwNpe();
        }
        Intrinsics.checkExpressionValueIsNotNull(errorBody, "response.errorBody()!!");
        String jsonMsg = NetworkExtensionKt.getJsonMsg(errorBody);
        this.$onApiError.invoke(jsonMsg);
        Request request3 = call.request();
        Intrinsics.checkExpressionValueIsNotNull(request3, "call.request()");
        NetworkExtensionKt.logData(request3, jsonMsg, response.raw().receivedResponseAtMillis() - response.raw().sentRequestAtMillis(), true);
    }

    public void onFailure(Call<T> call, Throwable th) {
        Intrinsics.checkParameterIsNotNull(call, NotificationCompat.CATEGORY_CALL);
        Intrinsics.checkParameterIsNotNull(th, "t");
        if (!ExtensionsKt.isNetworkAvailable()) {
            this.$onNetworkError.invoke();
            Request request = call.request();
            Intrinsics.checkExpressionValueIsNotNull(request, "call.request()");
            String string = ShopHopApp.Companion.getAppInstance().getResources().getString(R.string.error_no_internet);
            Intrinsics.checkExpressionValueIsNotNull(string, "getAppInstance().resourc…string.error_no_internet)");
            NetworkExtensionKt.logData$default(request, string, 0, true, 4, (Object) null);
            return;
        }
        this.$onApiError.invoke("Please try again later.");
        if (th.getMessage() != null) {
            Request request2 = call.request();
            Intrinsics.checkExpressionValueIsNotNull(request2, "call.request()");
            String message = th.getMessage();
            if (message == null) {
                Intrinsics.throwNpe();
            }
            NetworkExtensionKt.logData$default(request2, message, 0, true, 4, (Object) null);
        }
    }
}
