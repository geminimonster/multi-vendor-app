package com.iqonic.store.utils.extensions;

import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\n¢\u0006\u0002\b\u0006"}, d2 = {"<anonymous>", "", "<anonymous parameter 0>", "", "<anonymous parameter 1>", "", "invoke"}, k = 3, mv = {1, 1, 16})
/* compiled from: ListExtensions.kt */
final class ListExtensionsKt$create$7 extends Lambda implements Function2<String, Integer, Unit> {
    public static final ListExtensionsKt$create$7 INSTANCE = new ListExtensionsKt$create$7();

    ListExtensionsKt$create$7() {
        super(2);
    }

    public /* bridge */ /* synthetic */ Object invoke(Object obj, Object obj2) {
        invoke((String) obj, ((Number) obj2).intValue());
        return Unit.INSTANCE;
    }

    public final void invoke(String str, int i) {
        Intrinsics.checkParameterIsNotNull(str, "<anonymous parameter 0>");
    }
}
