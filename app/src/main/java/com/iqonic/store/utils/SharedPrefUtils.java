package com.iqonic.store.utils;

import android.content.SharedPreferences;
import com.iqonic.store.ShopHopApp;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u0006\u0010\u0007\u001a\u00020\bJ\u0018\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\f2\b\b\u0002\u0010\r\u001a\u00020\nJ\u0016\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000fJ\u0016\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0010\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u0012J\u0018\u0010\u0013\u001a\u00020\f2\u0006\u0010\u0010\u001a\u00020\f2\b\b\u0002\u0010\r\u001a\u00020\fJ\u000e\u0010\u0014\u001a\u00020\b2\u0006\u0010\u0010\u001a\u00020\fJ\u0018\u0010\u0015\u001a\u00020\b2\u0006\u0010\u0010\u001a\u00020\f2\b\u0010\u0016\u001a\u0004\u0018\u00010\u0001R\u000e\u0010\u0003\u001a\u00020\u0004X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u000e¢\u0006\u0002\n\u0000¨\u0006\u0017"}, d2 = {"Lcom/iqonic/store/utils/SharedPrefUtils;", "", "()V", "mSharedPreferences", "Landroid/content/SharedPreferences;", "mSharedPreferencesEditor", "Landroid/content/SharedPreferences$Editor;", "clear", "", "getBooleanValue", "", "keyFlag", "", "defaultValue", "getIntValue", "", "key", "getLongValue", "", "getStringValue", "removeKey", "setValue", "value", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: SharedPrefUtils.kt */
public final class SharedPrefUtils {
    private final SharedPreferences mSharedPreferences;
    private SharedPreferences.Editor mSharedPreferencesEditor;

    public SharedPrefUtils() {
        SharedPreferences sharedPreferences = ShopHopApp.Companion.getAppInstance().getSharedPreferences(Constants.myPreferences, 0);
        Intrinsics.checkExpressionValueIsNotNull(sharedPreferences, "getAppInstance().getShar…es, Context.MODE_PRIVATE)");
        this.mSharedPreferences = sharedPreferences;
        SharedPreferences.Editor edit = sharedPreferences.edit();
        Intrinsics.checkExpressionValueIsNotNull(edit, "mSharedPreferences.edit()");
        this.mSharedPreferencesEditor = edit;
        edit.apply();
    }

    public final void setValue(String str, Object obj) {
        Intrinsics.checkParameterIsNotNull(str, "key");
        boolean z = true;
        if (obj != null ? obj instanceof Integer : true) {
            SharedPreferences.Editor editor = this.mSharedPreferencesEditor;
            if (obj == null) {
                Intrinsics.throwNpe();
            }
            editor.putInt(str, ((Number) obj).intValue());
            this.mSharedPreferencesEditor.apply();
            return;
        }
        if (obj != null ? obj instanceof String : true) {
            SharedPreferences.Editor editor2 = this.mSharedPreferencesEditor;
            if (obj == null) {
                Intrinsics.throwNpe();
            }
            editor2.putString(str, (String) obj);
            this.mSharedPreferencesEditor.apply();
            return;
        }
        if (obj != null ? obj instanceof Double : true) {
            this.mSharedPreferencesEditor.putString(str, String.valueOf(obj));
            this.mSharedPreferencesEditor.apply();
            return;
        }
        if (obj != null ? obj instanceof Long : true) {
            SharedPreferences.Editor editor3 = this.mSharedPreferencesEditor;
            if (obj == null) {
                Intrinsics.throwNpe();
            }
            editor3.putLong(str, ((Number) obj).longValue());
            this.mSharedPreferencesEditor.apply();
            return;
        }
        if (obj != null) {
            z = obj instanceof Boolean;
        }
        if (z) {
            SharedPreferences.Editor editor4 = this.mSharedPreferencesEditor;
            if (obj == null) {
                Intrinsics.throwNpe();
            }
            editor4.putBoolean(str, ((Boolean) obj).booleanValue());
            this.mSharedPreferencesEditor.apply();
        }
    }

    public static /* synthetic */ String getStringValue$default(SharedPrefUtils sharedPrefUtils, String str, String str2, int i, Object obj) {
        if ((i & 2) != 0) {
            str2 = "";
        }
        return sharedPrefUtils.getStringValue(str, str2);
    }

    public final String getStringValue(String str, String str2) {
        Intrinsics.checkParameterIsNotNull(str, "key");
        Intrinsics.checkParameterIsNotNull(str2, "defaultValue");
        String string = this.mSharedPreferences.getString(str, str2);
        if (string == null) {
            Intrinsics.throwNpe();
        }
        return string;
    }

    public final int getIntValue(String str, int i) {
        Intrinsics.checkParameterIsNotNull(str, "key");
        return this.mSharedPreferences.getInt(str, i);
    }

    public final long getLongValue(String str, long j) {
        Intrinsics.checkParameterIsNotNull(str, "key");
        return this.mSharedPreferences.getLong(str, j);
    }

    public static /* synthetic */ boolean getBooleanValue$default(SharedPrefUtils sharedPrefUtils, String str, boolean z, int i, Object obj) {
        if ((i & 2) != 0) {
            z = false;
        }
        return sharedPrefUtils.getBooleanValue(str, z);
    }

    public final boolean getBooleanValue(String str, boolean z) {
        Intrinsics.checkParameterIsNotNull(str, "keyFlag");
        return this.mSharedPreferences.getBoolean(str, z);
    }

    public final void removeKey(String str) {
        Intrinsics.checkParameterIsNotNull(str, "key");
        this.mSharedPreferencesEditor.remove(str);
        this.mSharedPreferencesEditor.apply();
    }

    public final void clear() {
        this.mSharedPreferencesEditor.clear().apply();
    }
}
