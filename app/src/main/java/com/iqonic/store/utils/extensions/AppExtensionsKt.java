package com.iqonic.store.utils.extensions;

import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import androidx.appcompat.app.AlertDialog;
import androidx.browser.customtabs.CustomTabsIntent;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestBuilder;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.facebook.appevents.UserDataStore;
import com.facebook.internal.NativeProtocol;
import com.google.gson.Gson;
import com.iqonic.store.AppBaseActivity;
import com.iqonic.store.ShopHopApp;
import com.iqonic.store.models.Billing;
import com.iqonic.store.models.BuilderDashboard;
import com.iqonic.store.models.CountryModel;
import com.iqonic.store.models.DashBoardResponse;
import com.iqonic.store.models.RequestModel;
import com.iqonic.store.models.Shipping;
import com.iqonic.store.utils.Constants;
import com.iqonic.store.utils.SharedPrefUtils;
import com.store.proshop.R;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000º\u0001\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\f\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\t\u001a\u0006\u0010\u0000\u001a\u00020\u0001\u001a\u0010\u0010\u0002\u001a\u0004\u0018\u00010\u00032\u0006\u0010\u0004\u001a\u00020\u0003\u001a\u0010\u0010\u0005\u001a\u0004\u0018\u00010\u00032\u0006\u0010\u0004\u001a\u00020\u0003\u001aM\u0010\u0006\u001a\u00020\u00012\"\u0010\u0007\u001a\u001e\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\n0\tj\b\u0012\u0004\u0012\u00020\n`\u000b\u0012\u0004\u0012\u00020\u00010\b2!\u0010\f\u001a\u001d\u0012\u0013\u0012\u00110\u0003¢\u0006\f\b\r\u0012\b\b\u000e\u0012\u0004\b\b(\u000f\u0012\u0004\u0012\u00020\u00010\b\u001a\b\u0010\u0010\u001a\u00020\u0003H\u0007\u001a\u0006\u0010\u0011\u001a\u00020\u0003\u001a\b\u0010\u0012\u001a\u00020\u0003H\u0007\u001a\u0006\u0010\u0013\u001a\u00020\u0014\u001a\b\u0010\u0015\u001a\u00020\u0003H\u0007\u001a\u0006\u0010\u0016\u001a\u00020\u0003\u001a\u0006\u0010\u0017\u001a\u00020\u0003\u001a\u0006\u0010\u0018\u001a\u00020\u0003\u001a\u0006\u0010\u0019\u001a\u00020\u0003\u001a\u0006\u0010\u001a\u001a\u00020\u0003\u001a\u0006\u0010\u001b\u001a\u00020\u0003\u001a\u0006\u0010\u001c\u001a\u00020\u0003\u001a\u0006\u0010\u001d\u001a\u00020\u0003\u001a\u0006\u0010\u001e\u001a\u00020\u0003\u001a\b\u0010\u001f\u001a\u00020\u0003H\u0007\u001a\u0006\u0010 \u001a\u00020!\u001a\u0006\u0010\"\u001a\u00020\u0003\u001a\u0006\u0010#\u001a\u00020$\u001a\u0006\u0010%\u001a\u00020&\u001a\b\u0010'\u001a\u00020\u0003H\u0007\u001a\b\u0010(\u001a\u00020\u0003H\u0007\u001a\b\u0010)\u001a\u00020\u0003H\u0007\u001a\u0006\u0010*\u001a\u00020\u0003\u001a\u0006\u0010+\u001a\u00020\u0003\u001a\u0006\u0010,\u001a\u00020\u0003\u001a\u0006\u0010-\u001a\u00020\u0003\u001a\u0006\u0010.\u001a\u00020\u0003\u001a\u0006\u0010/\u001a\u000200\u001a\u0006\u00101\u001a\u000202\u001a*\u00103\u001a\u0004\u0018\u0001042\u0012\u00105\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00010\b2\f\u00106\u001a\b\u0012\u0004\u0012\u00020\u000107\u001a\u001a\u00108\u001a\u00020\u0001*\u0002092\u0006\u0010:\u001a\u00020;2\u0006\u0010<\u001a\u00020!\u001a\n\u0010=\u001a\u00020\u0001*\u00020>\u001a\f\u0010?\u001a\u0004\u0018\u00010@*\u00020;\u001a\f\u0010A\u001a\u0004\u0018\u00010@*\u00020;\u001a\f\u0010B\u001a\u0004\u0018\u00010@*\u00020;\u001a\u0012\u0010C\u001a\u00020\u0003*\u00020;2\u0006\u0010D\u001a\u00020\u0003\u001a\u0001\u0010E\u001a\u00020F*\u00020>2\u0006\u0010G\u001a\u00020\u00032\b\b\u0002\u0010H\u001a\u00020\u00032\b\b\u0002\u0010I\u001a\u00020\u00032\b\b\u0002\u0010J\u001a\u00020\u00032'\u0010K\u001a#\u0012\u0013\u0012\u00110M¢\u0006\f\b\r\u0012\b\b\u000e\u0012\u0004\b\b(N\u0012\u0004\u0012\u00020!\u0012\u0004\u0012\u00020\u00010L2'\u0010O\u001a#\u0012\u0013\u0012\u00110M¢\u0006\f\b\r\u0012\b\b\u000e\u0012\u0004\b\b(N\u0012\u0004\u0012\u00020!\u0012\u0004\u0012\u00020\u00010L\u001a\u0014\u0010P\u001a\u00020\u0001*\u0002092\b\b\u0001\u0010<\u001a\u00020!\u001a\u001e\u0010Q\u001a\u00020\u0001*\u00020;2\u0012\u0010R\u001a\u000e\u0012\u0004\u0012\u00020S\u0012\u0004\u0012\u00020\u00010\b\u001a\n\u0010T\u001a\u00020\u0001*\u00020>\u001a\u0012\u0010U\u001a\u00020\u0001*\u00020;2\u0006\u0010V\u001a\u00020\u0003\u001a\u0018\u0010W\u001a\u00020\u0001*\u00020>2\f\u0010X\u001a\b\u0012\u0004\u0012\u00020\u000107\u001a\n\u0010Y\u001a\u00020Z*\u00020>\u001a\n\u0010[\u001a\u00020Z*\u00020>\u001a\u001a\u0010\\\u001a\u00020\u0001*\u00020>2\u0006\u0010]\u001a\u00020\u00032\u0006\u0010^\u001a\u00020_\u001a\u0012\u0010`\u001a\u00020\u0001*\u00020>2\u0006\u0010^\u001a\u00020_\u001a\u0012\u0010a\u001a\u00020\u0001*\u00020>2\u0006\u0010^\u001a\u00020_\u001a\u0012\u0010b\u001a\u00020\u0001*\u00020>2\u0006\u0010^\u001a\u00020_\u001a\u0012\u0010c\u001a\u00020\u0001*\u00020>2\u0006\u0010^\u001a\u00020_\u001a\u0012\u0010d\u001a\u00020\u0001*\u00020>2\u0006\u0010^\u001a\u00020_\u001a\n\u0010e\u001a\u00020\u0001*\u00020f\u001a\u0012\u0010g\u001a\u00020\u0001*\u00020>2\u0006\u0010]\u001a\u00020\u0003\u001a\n\u0010h\u001a\u00020\u0001*\u00020>\u001a\n\u0010i\u001a\u00020\u0001*\u00020>\u001a\n\u0010j\u001a\u00020\u0001*\u00020>\u001a\n\u0010k\u001a\u00020\u0001*\u00020>\u001a\n\u0010l\u001a\u00020\u0001*\u00020>\u001ab\u0010m\u001a\u00020\u0001*\u00020n2\u0006\u0010o\u001a\u00020\u00032\u0006\u0010p\u001a\u00020\u00032\u0006\u0010q\u001a\u00020\u00032\u0006\u0010r\u001a\u00020\u00032\u0006\u0010s\u001a\u00020\u00032\u0006\u0010t\u001a\u00020\u00032\u0012\u0010u\u001a\u000e\u0012\u0004\u0012\u000202\u0012\u0004\u0012\u00020\u00010\b2\u0012\u0010v\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00010\b¨\u0006w"}, d2 = {"clearLoginPref", "", "convertOrderDataToLocalDate", "", "ourDate", "convertToLocalDate", "fetchCountry", "onApiSuccess", "Lkotlin/Function1;", "Ljava/util/ArrayList;", "Lcom/iqonic/store/models/CountryModel;", "Lkotlin/collections/ArrayList;", "onApiError", "Lkotlin/ParameterName;", "name", "aError", "getAccentColor", "getApiToken", "getBackgroundColor", "getBuilderDashboard", "Lcom/iqonic/store/models/BuilderDashboard;", "getButtonColor", "getCartCount", "getDefaultCurrency", "getDefaultCurrencyFormate", "getDisplayName", "getEmail", "getFirstName", "getLanguage", "getLastName", "getOrderCount", "getPrimaryColor", "getProductDetailConstant", "", "getProfile", "getSharedPrefInstance", "Lcom/iqonic/store/utils/SharedPrefUtils;", "getShippingList", "Lcom/iqonic/store/models/Shipping;", "getTextPrimaryColor", "getTextSecondaryColor", "getTextTitleColor", "getUserFullName", "getUserId", "getUserName", "getUserProfile", "getWishListCount", "getbillingList", "Lcom/iqonic/store/models/Billing;", "isLoggedIn", "", "startOTPTimer", "Landroid/os/CountDownTimer;", "onTimerTick", "onTimerFinished", "Lkotlin/Function0;", "displayBlankImage", "Landroid/widget/ImageView;", "aContext", "Landroid/content/Context;", "aPlaceHolderImage", "fetchAndStoreCartData", "Landroid/app/Activity;", "fontBold", "Landroid/graphics/Typeface;", "fontRegular", "fontSemiBold", "fromJson", "file", "getAlertDialog", "Landroidx/appcompat/app/AlertDialog;", "aMsgText", "aTitleText", "aPositiveText", "aNegativeText", "onPositiveClick", "Lkotlin/Function2;", "Landroid/content/DialogInterface;", "dialog", "onNegativeClick", "loadImageFromDrawable", "mainContent", "onSuccess", "Lcom/iqonic/store/models/DashBoardResponse;", "makeTransparentStatusBar", "openCustomTab", "url", "openNetworkDialog", "onClick", "productLayoutParams", "Landroid/widget/RelativeLayout$LayoutParams;", "productLayoutParamsForDealOffer", "registerBroadCastReceiver", "action", "receiver", "Landroid/content/BroadcastReceiver;", "registerCartCountChangeReceiver", "registerCartReceiver", "registerOrderCountChangeReceiver", "registerProfileUpdateReceiver", "registerWishListReceiver", "rvItemAnimation", "Landroidx/recyclerview/widget/RecyclerView;", "sendBroadcast", "sendCartBroadcast", "sendCartCountChangeBroadcast", "sendOrderCountChangeBroadcast", "sendProfileUpdateBroadcast", "sendWishlistBroadcast", "socialLogin", "Lcom/iqonic/store/AppBaseActivity;", "email", "accessToken", "firstName", "lastName", "loginType", "photoURL", "onResult", "onError", "app_release"}, k = 2, mv = {1, 1, 16})
/* compiled from: AppExtensions.kt */
public final class AppExtensionsKt {
    public static final boolean isLoggedIn() {
        return SharedPrefUtils.getBooleanValue$default(getSharedPrefInstance(), Constants.SharedPref.IS_LOGGED_IN, false, 2, (Object) null);
    }

    public static final String getUserId() {
        return SharedPrefUtils.getStringValue$default(getSharedPrefInstance(), "user_id", (String) null, 2, (Object) null);
    }

    public static final String getDefaultCurrency() {
        return SharedPrefUtils.getStringValue$default(getSharedPrefInstance(), Constants.SharedPref.DEFAULT_CURRENCY, (String) null, 2, (Object) null);
    }

    public static final String getDefaultCurrencyFormate() {
        return SharedPrefUtils.getStringValue$default(getSharedPrefInstance(), Constants.SharedPref.DEFAULT_CURRENCY_FORMATE, (String) null, 2, (Object) null);
    }

    public static final String getLanguage() {
        return SharedPrefUtils.getStringValue$default(getSharedPrefInstance(), Constants.SharedPref.LANGUAGE, (String) null, 2, (Object) null);
    }

    public static final String getUserName() {
        return SharedPrefUtils.getStringValue$default(getSharedPrefInstance(), "user_username", (String) null, 2, (Object) null);
    }

    public static final String getFirstName() {
        return SharedPrefUtils.getStringValue$default(getSharedPrefInstance(), Constants.SharedPref.USER_FIRST_NAME, (String) null, 2, (Object) null);
    }

    public static final String getDisplayName() {
        return SharedPrefUtils.getStringValue$default(getSharedPrefInstance(), Constants.SharedPref.USER_DISPLAY_NAME, (String) null, 2, (Object) null);
    }

    public static final String getLastName() {
        return SharedPrefUtils.getStringValue$default(getSharedPrefInstance(), Constants.SharedPref.USER_LAST_NAME, (String) null, 2, (Object) null);
    }

    public static final String getEmail() {
        return SharedPrefUtils.getStringValue$default(getSharedPrefInstance(), Constants.SharedPref.USER_EMAIL, (String) null, 2, (Object) null);
    }

    public static final String getCartCount() {
        return String.valueOf(getSharedPrefInstance().getIntValue(Constants.SharedPref.KEY_CART_COUNT, 0));
    }

    public static final String getWishListCount() {
        return String.valueOf(getSharedPrefInstance().getIntValue(Constants.SharedPref.KEY_WISHLIST_COUNT, 0));
    }

    public static final String getOrderCount() {
        return String.valueOf(getSharedPrefInstance().getIntValue(Constants.SharedPref.KEY_ORDER_COUNT, 0));
    }

    public static final String getApiToken() {
        return SharedPrefUtils.getStringValue$default(getSharedPrefInstance(), Constants.SharedPref.USER_TOKEN, (String) null, 2, (Object) null);
    }

    public static final String getUserProfile() {
        return SharedPrefUtils.getStringValue$default(getSharedPrefInstance(), Constants.SharedPref.USER_PROFILE, (String) null, 2, (Object) null);
    }

    public static final String getPrimaryColor() {
        if (getSharedPrefInstance().getIntValue(Constants.SharedPref.MODE, 1) == 1) {
            return SharedPrefUtils.getStringValue$default(getSharedPrefInstance(), Constants.SharedPref.PRIMARYCOLOR, (String) null, 2, (Object) null);
        }
        String string = ShopHopApp.Companion.getAppInstance().getResources().getString(R.color.colorPrimary);
        Intrinsics.checkExpressionValueIsNotNull(string, "getAppInstance().resourc…ing(R.color.colorPrimary)");
        return string;
    }

    public static final String getAccentColor() {
        if (getSharedPrefInstance().getIntValue(Constants.SharedPref.MODE, 1) == 1) {
            return SharedPrefUtils.getStringValue$default(getSharedPrefInstance(), Constants.SharedPref.ACCENTCOLOR, (String) null, 2, (Object) null);
        }
        String string = ShopHopApp.Companion.getAppInstance().getResources().getString(R.color.colorAccent);
        Intrinsics.checkExpressionValueIsNotNull(string, "getAppInstance().resourc…ring(R.color.colorAccent)");
        return string;
    }

    public static final String getButtonColor() {
        if (getSharedPrefInstance().getIntValue(Constants.SharedPref.MODE, 1) == 1) {
            return SharedPrefUtils.getStringValue$default(getSharedPrefInstance(), Constants.SharedPref.PRIMARYCOLOR, (String) null, 2, (Object) null);
        }
        String string = ShopHopApp.Companion.getAppInstance().getResources().getString(R.color.colorPrimary);
        Intrinsics.checkExpressionValueIsNotNull(string, "getAppInstance().resourc…ing(R.color.colorPrimary)");
        return string;
    }

    public static final String getTextPrimaryColor() {
        if (getSharedPrefInstance().getIntValue(Constants.SharedPref.MODE, 1) == 1) {
            return SharedPrefUtils.getStringValue$default(getSharedPrefInstance(), Constants.SharedPref.TEXTPRIMARYCOLOR, (String) null, 2, (Object) null);
        }
        String string = ShopHopApp.Companion.getAppInstance().getResources().getString(R.color.textColorPrimary);
        Intrinsics.checkExpressionValueIsNotNull(string, "getAppInstance().resourc…R.color.textColorPrimary)");
        return string;
    }

    public static final String getTextSecondaryColor() {
        if (getSharedPrefInstance().getIntValue(Constants.SharedPref.MODE, 1) == 1) {
            return SharedPrefUtils.getStringValue$default(getSharedPrefInstance(), Constants.SharedPref.TEXTSECONDARYCOLOR, (String) null, 2, (Object) null);
        }
        String string = ShopHopApp.Companion.getAppInstance().getResources().getString(R.color.textColorSecondary);
        Intrinsics.checkExpressionValueIsNotNull(string, "getAppInstance().resourc…color.textColorSecondary)");
        return string;
    }

    public static final String getBackgroundColor() {
        if (getSharedPrefInstance().getIntValue(Constants.SharedPref.MODE, 1) == 1) {
            return SharedPrefUtils.getStringValue$default(getSharedPrefInstance(), Constants.SharedPref.BACKGROUNDCOLOR, (String) null, 2, (Object) null);
        }
        String string = ShopHopApp.Companion.getAppInstance().getResources().getString(R.color.colorScreenBackground);
        Intrinsics.checkExpressionValueIsNotNull(string, "getAppInstance().resourc…or.colorScreenBackground)");
        return string;
    }

    public static final String getTextTitleColor() {
        if (getSharedPrefInstance().getIntValue(Constants.SharedPref.MODE, 1) == 1) {
            String string = ShopHopApp.Companion.getAppInstance().getResources().getString(R.color.common_white);
            Intrinsics.checkExpressionValueIsNotNull(string, "getAppInstance().resourc…ing(R.color.common_white)");
            return string;
        }
        String string2 = ShopHopApp.Companion.getAppInstance().getResources().getString(R.color.colorAccent);
        Intrinsics.checkExpressionValueIsNotNull(string2, "getAppInstance().resourc…ring(R.color.colorAccent)");
        return string2;
    }

    public static final int getProductDetailConstant() {
        return getSharedPrefInstance().getIntValue(Constants.SharedPref.KEY_PRODUCT_DETAIL, 0);
    }

    public static final String getUserFullName() {
        if (!isLoggedIn()) {
            return "Guest User";
        }
        return StringExtensionsKt.toCamelCase(SharedPrefUtils.getStringValue$default(getSharedPrefInstance(), Constants.SharedPref.USER_FIRST_NAME, (String) null, 2, (Object) null) + " " + SharedPrefUtils.getStringValue$default(getSharedPrefInstance(), Constants.SharedPref.USER_LAST_NAME, (String) null, 2, (Object) null));
    }

    public static final void openCustomTab(Context context, String str) {
        Intrinsics.checkParameterIsNotNull(context, "$this$openCustomTab");
        Intrinsics.checkParameterIsNotNull(str, "url");
        new CustomTabsIntent.Builder().build().launchUrl(context, Uri.parse(str));
    }

    public static final String getProfile() {
        return SharedPrefUtils.getStringValue$default(getSharedPrefInstance(), Constants.SharedPref.USER_PROFILE, (String) null, 2, (Object) null);
    }

    public static final void fetchAndStoreCartData(Activity activity) {
        Intrinsics.checkParameterIsNotNull(activity, "$this$fetchAndStoreCartData");
        NetworkExtensionKt.getRestApiImpl$default((String) null, 1, (Object) null).getCart(new AppExtensionsKt$fetchAndStoreCartData$1(activity), new AppExtensionsKt$fetchAndStoreCartData$2(activity));
    }

    public static final String convertOrderDataToLocalDate(String str) {
        Intrinsics.checkParameterIsNotNull(str, "ourDate");
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.000000", Locale.US);
            simpleDateFormat.setTimeZone(TimeZone.getTimeZone("Etc/UTC"));
            Date parse = simpleDateFormat.parse(str);
            Intrinsics.checkExpressionValueIsNotNull(parse, "formatter.parse(ourDate)");
            SimpleDateFormat simpleDateFormat2 = new SimpleDateFormat("dd-MM-yyyy hh:mm a");
            simpleDateFormat2.setTimeZone(TimeZone.getDefault());
            return simpleDateFormat2.format(parse);
        } catch (Exception e) {
            e.printStackTrace();
            return "00-00-0000 00:00";
        }
    }

    public static final String convertToLocalDate(String str) {
        Intrinsics.checkParameterIsNotNull(str, "ourDate");
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
            simpleDateFormat.setTimeZone(TimeZone.getTimeZone("Etc/UTC"));
            Date parse = simpleDateFormat.parse(str);
            Intrinsics.checkExpressionValueIsNotNull(parse, "formatter.parse(ourDate)");
            SimpleDateFormat simpleDateFormat2 = new SimpleDateFormat("dd-MM-yyyy hh:mm a");
            simpleDateFormat2.setTimeZone(TimeZone.getDefault());
            return simpleDateFormat2.format(parse);
        } catch (Exception e) {
            e.printStackTrace();
            return "00-00-0000 00:00";
        }
    }

    public static final void clearLoginPref() {
        getSharedPrefInstance().removeKey(Constants.SharedPref.IS_LOGGED_IN);
        getSharedPrefInstance().removeKey("user_id");
        getSharedPrefInstance().removeKey(Constants.SharedPref.USER_DISPLAY_NAME);
        getSharedPrefInstance().removeKey(Constants.SharedPref.USER_EMAIL);
        getSharedPrefInstance().removeKey(Constants.SharedPref.USER_NICE_NAME);
        getSharedPrefInstance().removeKey(Constants.SharedPref.USER_TOKEN);
        getSharedPrefInstance().removeKey(Constants.SharedPref.USER_FIRST_NAME);
        getSharedPrefInstance().removeKey(Constants.SharedPref.USER_LAST_NAME);
        getSharedPrefInstance().removeKey(Constants.SharedPref.USER_PROFILE);
        getSharedPrefInstance().removeKey("user_username");
        getSharedPrefInstance().removeKey("user_username");
        getSharedPrefInstance().removeKey(Constants.SharedPref.KEY_USER_ADDRESS);
        getSharedPrefInstance().removeKey(Constants.SharedPref.KEY_CART_COUNT);
        getSharedPrefInstance().removeKey(Constants.SharedPref.KEY_ORDER_COUNT);
        getSharedPrefInstance().removeKey(Constants.SharedPref.KEY_WISHLIST_COUNT);
    }

    public static final SharedPrefUtils getSharedPrefInstance() {
        SharedPrefUtils sharedPrefUtils;
        if (ShopHopApp.Companion.getSharedPrefUtils() == null) {
            ShopHopApp.Companion.setSharedPrefUtils(new SharedPrefUtils());
            sharedPrefUtils = ShopHopApp.Companion.getSharedPrefUtils();
            if (sharedPrefUtils == null) {
                Intrinsics.throwNpe();
            }
        } else {
            sharedPrefUtils = ShopHopApp.Companion.getSharedPrefUtils();
            if (sharedPrefUtils == null) {
                Intrinsics.throwNpe();
            }
        }
        return sharedPrefUtils;
    }

    public static final void rvItemAnimation(RecyclerView recyclerView) {
        Intrinsics.checkParameterIsNotNull(recyclerView, "$this$rvItemAnimation");
        recyclerView.setLayoutAnimation(AnimationUtils.loadLayoutAnimation(recyclerView.getContext(), R.anim.layout_animation_fall_down));
    }

    public static final void displayBlankImage(ImageView imageView, Context context, int i) {
        Intrinsics.checkParameterIsNotNull(imageView, "$this$displayBlankImage");
        Intrinsics.checkParameterIsNotNull(context, "aContext");
        ((RequestBuilder) Glide.with(context).load(Integer.valueOf(i)).diskCacheStrategy(DiskCacheStrategy.ALL)).into(imageView);
    }

    public static final Typeface fontSemiBold(Context context) {
        Intrinsics.checkParameterIsNotNull(context, "$this$fontSemiBold");
        return Typeface.createFromAsset(context.getAssets(), context.getString(R.string.font_SemiBold));
    }

    public static final Typeface fontBold(Context context) {
        Intrinsics.checkParameterIsNotNull(context, "$this$fontBold");
        return Typeface.createFromAsset(context.getAssets(), context.getString(R.string.font_Bold));
    }

    public static final Typeface fontRegular(Context context) {
        Intrinsics.checkParameterIsNotNull(context, "$this$fontRegular");
        return Typeface.createFromAsset(context.getAssets(), context.getString(R.string.font_regular));
    }

    public static final void makeTransparentStatusBar(Activity activity) {
        Intrinsics.checkParameterIsNotNull(activity, "$this$makeTransparentStatusBar");
        Window window = activity.getWindow();
        window.addFlags(Integer.MIN_VALUE);
        window.clearFlags(67108864);
        if (Build.VERSION.SDK_INT >= 23) {
            Intrinsics.checkExpressionValueIsNotNull(window, "window");
            window.setStatusBarColor(activity.getColor(R.color.item_background));
            return;
        }
        Intrinsics.checkExpressionValueIsNotNull(window, "window");
        window.setStatusBarColor(ContextCompat.getColor(activity, R.color.item_background));
    }

    public static /* synthetic */ AlertDialog getAlertDialog$default(Activity activity, String str, String str2, String str3, String str4, Function2 function2, Function2 function22, int i, Object obj) {
        if ((i & 2) != 0) {
            str2 = activity.getString(R.string.lbl_dialog_title);
            Intrinsics.checkExpressionValueIsNotNull(str2, "getString(R.string.lbl_dialog_title)");
        }
        String str5 = str2;
        if ((i & 4) != 0) {
            str3 = activity.getString(R.string.lbl_yes);
            Intrinsics.checkExpressionValueIsNotNull(str3, "getString(R.string.lbl_yes)");
        }
        String str6 = str3;
        if ((i & 8) != 0) {
            str4 = activity.getString(R.string.lbl_no);
            Intrinsics.checkExpressionValueIsNotNull(str4, "getString(R.string.lbl_no)");
        }
        return getAlertDialog(activity, str, str5, str6, str4, function2, function22);
    }

    public static final AlertDialog getAlertDialog(Activity activity, String str, String str2, String str3, String str4, Function2<? super DialogInterface, ? super Integer, Unit> function2, Function2<? super DialogInterface, ? super Integer, Unit> function22) {
        Intrinsics.checkParameterIsNotNull(activity, "$this$getAlertDialog");
        Intrinsics.checkParameterIsNotNull(str, "aMsgText");
        Intrinsics.checkParameterIsNotNull(str2, "aTitleText");
        Intrinsics.checkParameterIsNotNull(str3, "aPositiveText");
        Intrinsics.checkParameterIsNotNull(str4, "aNegativeText");
        Intrinsics.checkParameterIsNotNull(function2, "onPositiveClick");
        Intrinsics.checkParameterIsNotNull(function22, "onNegativeClick");
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle((CharSequence) str2);
        builder.setMessage((CharSequence) str);
        builder.setPositiveButton((CharSequence) str3, (DialogInterface.OnClickListener) new AppExtensionsKt$getAlertDialog$1(function2));
        builder.setNegativeButton((CharSequence) str4, (DialogInterface.OnClickListener) new AppExtensionsKt$getAlertDialog$2(function22));
        AlertDialog create = builder.create();
        Intrinsics.checkExpressionValueIsNotNull(create, "builder.create()");
        return create;
    }

    public static final RelativeLayout.LayoutParams productLayoutParams(Activity activity) {
        Intrinsics.checkParameterIsNotNull(activity, "$this$productLayoutParams");
        int displayWidth = (int) (((double) ViewExtensionsKt.getDisplayWidth(activity)) / 4.2d);
        return new RelativeLayout.LayoutParams(displayWidth, (displayWidth / 12) + displayWidth);
    }

    public static final RelativeLayout.LayoutParams productLayoutParamsForDealOffer(Activity activity) {
        Intrinsics.checkParameterIsNotNull(activity, "$this$productLayoutParamsForDealOffer");
        int displayWidth = (int) (((double) ViewExtensionsKt.getDisplayWidth(activity)) / 3.2d);
        return new RelativeLayout.LayoutParams(displayWidth, (displayWidth / 10) + displayWidth);
    }

    public static final CountDownTimer startOTPTimer(Function1<? super String, Unit> function1, Function0<Unit> function0) {
        Intrinsics.checkParameterIsNotNull(function1, "onTimerTick");
        Intrinsics.checkParameterIsNotNull(function0, "onTimerFinished");
        return new AppExtensionsKt$startOTPTimer$1(function1, function0, 60000, 1000);
    }

    public static final void sendCartCountChangeBroadcast(Activity activity) {
        Intrinsics.checkParameterIsNotNull(activity, "$this$sendCartCountChangeBroadcast");
        sendBroadcast(activity, Constants.AppBroadcasts.CART_COUNT_CHANGE);
    }

    public static final void sendProfileUpdateBroadcast(Activity activity) {
        Intrinsics.checkParameterIsNotNull(activity, "$this$sendProfileUpdateBroadcast");
        sendBroadcast(activity, Constants.AppBroadcasts.PROFILE_UPDATE);
    }

    public static final void sendWishlistBroadcast(Activity activity) {
        Intrinsics.checkParameterIsNotNull(activity, "$this$sendWishlistBroadcast");
        sendBroadcast(activity, Constants.AppBroadcasts.WISHLIST_UPDATE);
    }

    public static final void sendCartBroadcast(Activity activity) {
        Intrinsics.checkParameterIsNotNull(activity, "$this$sendCartBroadcast");
        sendBroadcast(activity, Constants.AppBroadcasts.CARTITEM_UPDATE);
    }

    public static final void registerCartReceiver(Activity activity, BroadcastReceiver broadcastReceiver) {
        Intrinsics.checkParameterIsNotNull(activity, "$this$registerCartReceiver");
        Intrinsics.checkParameterIsNotNull(broadcastReceiver, "receiver");
        registerBroadCastReceiver(activity, Constants.AppBroadcasts.CARTITEM_UPDATE, broadcastReceiver);
    }

    public static final void registerCartCountChangeReceiver(Activity activity, BroadcastReceiver broadcastReceiver) {
        Intrinsics.checkParameterIsNotNull(activity, "$this$registerCartCountChangeReceiver");
        Intrinsics.checkParameterIsNotNull(broadcastReceiver, "receiver");
        registerBroadCastReceiver(activity, Constants.AppBroadcasts.CART_COUNT_CHANGE, broadcastReceiver);
    }

    public static final void registerProfileUpdateReceiver(Activity activity, BroadcastReceiver broadcastReceiver) {
        Intrinsics.checkParameterIsNotNull(activity, "$this$registerProfileUpdateReceiver");
        Intrinsics.checkParameterIsNotNull(broadcastReceiver, "receiver");
        registerBroadCastReceiver(activity, Constants.AppBroadcasts.PROFILE_UPDATE, broadcastReceiver);
    }

    public static final void registerWishListReceiver(Activity activity, BroadcastReceiver broadcastReceiver) {
        Intrinsics.checkParameterIsNotNull(activity, "$this$registerWishListReceiver");
        Intrinsics.checkParameterIsNotNull(broadcastReceiver, "receiver");
        registerBroadCastReceiver(activity, Constants.AppBroadcasts.WISHLIST_UPDATE, broadcastReceiver);
    }

    public static final void sendOrderCountChangeBroadcast(Activity activity) {
        Intrinsics.checkParameterIsNotNull(activity, "$this$sendOrderCountChangeBroadcast");
        sendBroadcast(activity, Constants.AppBroadcasts.ORDER_COUNT_CHANGE);
    }

    public static final void registerOrderCountChangeReceiver(Activity activity, BroadcastReceiver broadcastReceiver) {
        Intrinsics.checkParameterIsNotNull(activity, "$this$registerOrderCountChangeReceiver");
        Intrinsics.checkParameterIsNotNull(broadcastReceiver, "receiver");
        registerBroadCastReceiver(activity, Constants.AppBroadcasts.ORDER_COUNT_CHANGE, broadcastReceiver);
    }

    public static final void sendBroadcast(Activity activity, String str) {
        Intrinsics.checkParameterIsNotNull(activity, "$this$sendBroadcast");
        Intrinsics.checkParameterIsNotNull(str, NativeProtocol.WEB_DIALOG_ACTION);
        Intent intent = new Intent();
        intent.setAction(str);
        activity.sendBroadcast(intent);
    }

    public static final void registerBroadCastReceiver(Activity activity, String str, BroadcastReceiver broadcastReceiver) {
        Intrinsics.checkParameterIsNotNull(activity, "$this$registerBroadCastReceiver");
        Intrinsics.checkParameterIsNotNull(str, NativeProtocol.WEB_DIALOG_ACTION);
        Intrinsics.checkParameterIsNotNull(broadcastReceiver, "receiver");
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(str);
        activity.registerReceiver(broadcastReceiver, intentFilter);
    }

    public static final Billing getbillingList() {
        String stringValue = getSharedPrefInstance().getStringValue(Constants.SharedPref.BILLING, "");
        if (stringValue.length() == 0) {
            return new Billing();
        }
        Object fromJson = new Gson().fromJson(stringValue, new AppExtensionsKt$getbillingList$1().getType());
        Intrinsics.checkExpressionValueIsNotNull(fromJson, "Gson().fromJson(string, …Token<Billing>() {}.type)");
        return (Billing) fromJson;
    }

    public static final Shipping getShippingList() {
        String stringValue = getSharedPrefInstance().getStringValue(Constants.SharedPref.SHIPPING, "");
        if (stringValue.length() == 0) {
            return new Shipping();
        }
        Object fromJson = new Gson().fromJson(stringValue, new AppExtensionsKt$getShippingList$1().getType());
        Intrinsics.checkExpressionValueIsNotNull(fromJson, "Gson().fromJson(string, …oken<Shipping>() {}.type)");
        return (Shipping) fromJson;
    }

    public static final void loadImageFromDrawable(ImageView imageView, int i) {
        Intrinsics.checkParameterIsNotNull(imageView, "$this$loadImageFromDrawable");
        ((RequestBuilder) Glide.with((Context) ShopHopApp.Companion.getAppInstance()).load(Integer.valueOf(i)).diskCacheStrategy(DiskCacheStrategy.NONE)).into(imageView);
    }

    public static final void fetchCountry(Function1<? super ArrayList<CountryModel>, Unit> function1, Function1<? super String, Unit> function12) {
        Intrinsics.checkParameterIsNotNull(function1, "onApiSuccess");
        Intrinsics.checkParameterIsNotNull(function12, "onApiError");
        String stringValue = getSharedPrefInstance().getStringValue(Constants.SharedPref.COUNTRY, "");
        Log.e(UserDataStore.COUNTRY, stringValue);
        if (stringValue.length() == 0) {
            NetworkExtensionKt.getRestApiImpl$default((String) null, 1, (Object) null).listAllCountry(new AppExtensionsKt$fetchCountry$1(function1), new AppExtensionsKt$fetchCountry$2(function12));
            return;
        }
        Object fromJson = new Gson().fromJson(stringValue, new AppExtensionsKt$fetchCountry$3().getType());
        Intrinsics.checkExpressionValueIsNotNull(fromJson, "Gson().fromJson(string, …CountryModel>>() {}.type)");
        function1.invoke(fromJson);
    }

    public static final void openNetworkDialog(Activity activity, Function0<Unit> function0) {
        Dialog noInternetDialog;
        RelativeLayout relativeLayout;
        Window window;
        Intrinsics.checkParameterIsNotNull(activity, "$this$openNetworkDialog");
        Intrinsics.checkParameterIsNotNull(function0, "onClick");
        try {
            if (ShopHopApp.Companion.getNoInternetDialog() == null) {
                ShopHopApp.Companion.setNoInternetDialog(new Dialog(activity, R.style.FullScreenDialog));
                Dialog noInternetDialog2 = ShopHopApp.Companion.getNoInternetDialog();
                if (noInternetDialog2 != null) {
                    noInternetDialog2.setContentView(R.layout.layout_network);
                }
                Dialog noInternetDialog3 = ShopHopApp.Companion.getNoInternetDialog();
                if (noInternetDialog3 != null) {
                    noInternetDialog3.setCanceledOnTouchOutside(false);
                }
                Dialog noInternetDialog4 = ShopHopApp.Companion.getNoInternetDialog();
                if (noInternetDialog4 != null) {
                    noInternetDialog4.setCancelable(false);
                }
                Dialog noInternetDialog5 = ShopHopApp.Companion.getNoInternetDialog();
                if (!(noInternetDialog5 == null || (window = noInternetDialog5.getWindow()) == null)) {
                    window.setLayout(-1, -1);
                }
                Dialog noInternetDialog6 = ShopHopApp.Companion.getNoInternetDialog();
                if (!(noInternetDialog6 == null || (relativeLayout = (RelativeLayout) noInternetDialog6.findViewById(com.iqonic.store.R.id.rlMain)) == null)) {
                    View view = relativeLayout;
                    view.setOnClickListener(new AppExtensionsKt$openNetworkDialog$$inlined$onClick$1(view, function0));
                }
            }
            if (!activity.isFinishing()) {
                Dialog noInternetDialog7 = ShopHopApp.Companion.getNoInternetDialog();
                if (noInternetDialog7 == null) {
                    Intrinsics.throwNpe();
                }
                if (!noInternetDialog7.isShowing() && (noInternetDialog = ShopHopApp.Companion.getNoInternetDialog()) != null) {
                    noInternetDialog.show();
                }
            }
        } catch (Exception unused) {
        }
    }

    public static final void socialLogin(AppBaseActivity appBaseActivity, String str, String str2, String str3, String str4, String str5, String str6, Function1<? super Boolean, Unit> function1, Function1<? super String, Unit> function12) {
        Intrinsics.checkParameterIsNotNull(appBaseActivity, "$this$socialLogin");
        Intrinsics.checkParameterIsNotNull(str, "email");
        Intrinsics.checkParameterIsNotNull(str2, "accessToken");
        Intrinsics.checkParameterIsNotNull(str3, "firstName");
        Intrinsics.checkParameterIsNotNull(str4, "lastName");
        Intrinsics.checkParameterIsNotNull(str5, "loginType");
        Intrinsics.checkParameterIsNotNull(str6, "photoURL");
        Intrinsics.checkParameterIsNotNull(function1, "onResult");
        Intrinsics.checkParameterIsNotNull(function12, "onError");
        RequestModel requestModel = new RequestModel();
        requestModel.setUserEmail(str);
        requestModel.setAccessToken(str2);
        requestModel.setFirstName(str3);
        requestModel.setLastName(str4);
        requestModel.setLoginType(str5);
        requestModel.setPhotoURL(str6);
        appBaseActivity.showProgress(true);
        NetworkExtensionKt.getRestApiImpl$default((String) null, 1, (Object) null).socialLogin(requestModel, new AppExtensionsKt$socialLogin$1(appBaseActivity, str2, function12), new AppExtensionsKt$socialLogin$2(appBaseActivity));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0043, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0044, code lost:
        kotlin.io.CloseableKt.closeFinally(r0, r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0047, code lost:
        throw r2;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final java.lang.String fromJson(android.content.Context r1, java.lang.String r2) {
        /*
            java.lang.String r0 = "$this$fromJson"
            kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(r1, r0)
            java.lang.String r0 = "file"
            kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(r2, r0)
            android.content.res.AssetManager r1 = r1.getAssets()
            java.io.InputStream r1 = r1.open(r2)
            java.lang.String r2 = "assets.open(file)"
            kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r1, r2)
            java.nio.charset.Charset r2 = kotlin.text.Charsets.UTF_8
            java.io.InputStreamReader r0 = new java.io.InputStreamReader
            r0.<init>(r1, r2)
            java.io.Reader r0 = (java.io.Reader) r0
            boolean r1 = r0 instanceof java.io.BufferedReader
            if (r1 == 0) goto L_0x0027
            java.io.BufferedReader r0 = (java.io.BufferedReader) r0
            goto L_0x002f
        L_0x0027:
            java.io.BufferedReader r1 = new java.io.BufferedReader
            r2 = 8192(0x2000, float:1.14794E-41)
            r1.<init>(r0, r2)
            r0 = r1
        L_0x002f:
            java.io.Closeable r0 = (java.io.Closeable) r0
            r1 = 0
            java.lang.Throwable r1 = (java.lang.Throwable) r1
            r2 = r0
            java.io.BufferedReader r2 = (java.io.BufferedReader) r2     // Catch:{ all -> 0x0041 }
            java.io.Reader r2 = (java.io.Reader) r2     // Catch:{ all -> 0x0041 }
            java.lang.String r2 = kotlin.io.TextStreamsKt.readText(r2)     // Catch:{ all -> 0x0041 }
            kotlin.io.CloseableKt.closeFinally(r0, r1)
            return r2
        L_0x0041:
            r1 = move-exception
            throw r1     // Catch:{ all -> 0x0043 }
        L_0x0043:
            r2 = move-exception
            kotlin.io.CloseableKt.closeFinally(r0, r1)
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.iqonic.store.utils.extensions.AppExtensionsKt.fromJson(android.content.Context, java.lang.String):java.lang.String");
    }

    public static final void mainContent(Context context, Function1<? super DashBoardResponse, Unit> function1) {
        Intrinsics.checkParameterIsNotNull(context, "$this$mainContent");
        Intrinsics.checkParameterIsNotNull(function1, "onSuccess");
        Object fromJson = new Gson().fromJson(fromJson(context, "builder.json"), DashBoardResponse.class);
        Intrinsics.checkExpressionValueIsNotNull(fromJson, "Gson().fromJson(fromJson…oardResponse::class.java)");
        function1.invoke(fromJson);
    }

    public static final BuilderDashboard getBuilderDashboard() {
        String stringValue = getSharedPrefInstance().getStringValue(Constants.SharedPref.DASHBOARDDATA, "");
        if (stringValue.length() == 0) {
            return new BuilderDashboard();
        }
        Object fromJson = new Gson().fromJson(stringValue, new AppExtensionsKt$getBuilderDashboard$1().getType());
        Intrinsics.checkExpressionValueIsNotNull(fromJson, "Gson().fromJson(string, …lderDashboard>() {}.type)");
        return (BuilderDashboard) fromJson;
    }
}
