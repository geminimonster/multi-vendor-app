package com.iqonic.store.utils;

import java.text.SimpleDateFormat;
import java.util.Locale;
import kotlin.Metadata;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\f\bÆ\u0002\u0018\u00002\u00020\u0001:\u000b\u000f\u0010\u0011\u0012\u0013\u0014\u0015\u0016\u0017\u0018\u0019B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0011\u0010\u0003\u001a\u00020\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006R\u000e\u0010\u0007\u001a\u00020\bXT¢\u0006\u0002\n\u0000R\u0011\u0010\t\u001a\u00020\u0004¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u0006R\u0011\u0010\u000b\u001a\u00020\u0004¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\u0006R\u000e\u0010\r\u001a\u00020\u000eXT¢\u0006\u0002\n\u0000¨\u0006\u001a"}, d2 = {"Lcom/iqonic/store/utils/Constants;", "", "()V", "DD_MMM_YYYY", "Ljava/text/SimpleDateFormat;", "getDD_MMM_YYYY", "()Ljava/text/SimpleDateFormat;", "EXTRA_ADD_AMOUNT", "", "FULL_DATE_FORMATTER", "getFULL_DATE_FORMATTER", "YYYY_MM_DD", "getYYYY_MM_DD", "myPreferences", "", "AppBroadcasts", "DateFormatCodes", "KeyIntent", "OrderStatus", "PAYMENT_METHOD", "RequestCode", "SharedPref", "THEME", "TotalItem", "viewAllCode", "viewName", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: Constants.kt */
public final class Constants {
    private static final SimpleDateFormat DD_MMM_YYYY = new SimpleDateFormat("dd MMM yyyy", Locale.ENGLISH);
    public static final int EXTRA_ADD_AMOUNT = 55;
    private static final SimpleDateFormat FULL_DATE_FORMATTER = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.ENGLISH);
    public static final Constants INSTANCE = new Constants();
    private static final SimpleDateFormat YYYY_MM_DD = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
    public static final String myPreferences = "MyPreferences";

    private Constants() {
    }

    public final SimpleDateFormat getFULL_DATE_FORMATTER() {
        return FULL_DATE_FORMATTER;
    }

    public final SimpleDateFormat getDD_MMM_YYYY() {
        return DD_MMM_YYYY;
    }

    public final SimpleDateFormat getYYYY_MM_DD() {
        return YYYY_MM_DD;
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\f\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\u0010"}, d2 = {"Lcom/iqonic/store/utils/Constants$RequestCode;", "", "()V", "ACCOUNT", "", "ADD_ADDRESS", "COUPON_CODE", "EDIT_PROFILE", "LOGIN", "ORDER_CANCEL", "ORDER_SUMMARY", "ROZORPAY_PAYMENT", "SIGN_IN", "STRIPE_PAYMENT", "WEB_PAYMENT", "WISHLIST", "app_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: Constants.kt */
    public static final class RequestCode {
        public static final int ACCOUNT = 203;
        public static final int ADD_ADDRESS = 201;
        public static final int COUPON_CODE = 202;
        public static final int EDIT_PROFILE = 209;
        public static final RequestCode INSTANCE = new RequestCode();
        public static final int LOGIN = 211;
        public static final int ORDER_CANCEL = 206;
        public static final int ORDER_SUMMARY = 205;
        public static final int ROZORPAY_PAYMENT = 204;
        public static final int SIGN_IN = 206;
        public static final int STRIPE_PAYMENT = 207;
        public static final int WEB_PAYMENT = 208;
        public static final int WISHLIST = 210;

        private RequestCode() {
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b5\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0017\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0018\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0019\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u001a\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u001b\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u001c\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u001d\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u001e\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u001f\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010 \u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010!\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\"\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010#\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010$\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010%\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010&\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010'\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010(\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010)\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010*\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010+\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010,\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010-\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010.\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010/\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u00100\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u00101\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u00102\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u00103\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u00104\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u00105\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u00106\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u00107\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u00108\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u00069"}, d2 = {"Lcom/iqonic/store/utils/Constants$SharedPref;", "", "()V", "ACCENTCOLOR", "", "APPURL", "BACKGROUNDCOLOR", "BILLING", "CONSUMERKEY", "CONSUMERSECRET", "CONTACT", "COPYRIGHT_TEXT", "COUNTRY", "DASHBOARDDATA", "DEFAULT_CURRENCY", "DEFAULT_CURRENCY_FORMATE", "ENABLE_COUPONS", "FACEBOOK", "INSTAGRAM", "IS_LOGGED_IN", "IS_SOCIAL_LOGIN", "KEY_CART_COUNT", "KEY_DASHBOARD", "KEY_ORDER_COUNT", "KEY_PRODUCT_DETAIL", "KEY_USER_ADDRESS", "KEY_USER_CART", "KEY_WISHLIST_COUNT", "LANGUAGE", "MODE", "PAYMENT_METHOD", "PRIMARYCOLOR", "PRIVACY_POLICY", "SHIPPING", "SHOW_SWIPE", "TERM_CONDITION", "TEXTPRIMARYCOLOR", "TEXTSECONDARYCOLOR", "THEME", "THEME_COLOR", "TWITTER", "USER_COUNTRY_CODE", "USER_DISPLAY_NAME", "USER_EMAIL", "USER_FIRST_NAME", "USER_ID", "USER_LAST_NAME", "USER_NICE_NAME", "USER_PASSWORD", "USER_PICODE", "USER_PROFILE", "USER_ROLE", "USER_STATE_CODE", "USER_STATE_NAME", "USER_TOKEN", "USER_USERNAME", "WHATSAPP", "app_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: Constants.kt */
    public static final class SharedPref {
        public static final String ACCENTCOLOR = "accentColor";
        public static final String APPURL = "appurl";
        public static final String BACKGROUNDCOLOR = "backgroundColor";
        public static final String BILLING = "user_billing";
        public static final String CONSUMERKEY = "consumerKey";
        public static final String CONSUMERSECRET = "consumerSecret";
        public static final String CONTACT = "contact";
        public static final String COPYRIGHT_TEXT = "copyright_text";
        public static final String COUNTRY = "COUNTRY";
        public static final String DASHBOARDDATA = "dashboardData";
        public static final String DEFAULT_CURRENCY = "default_currency";
        public static final String DEFAULT_CURRENCY_FORMATE = "default_currency_formate";
        public static final String ENABLE_COUPONS = "enable_coupons";
        public static final String FACEBOOK = "facebook";
        public static final String INSTAGRAM = "instagram";
        public static final SharedPref INSTANCE = new SharedPref();
        public static final String IS_LOGGED_IN = "isLoggedIn";
        public static final String IS_SOCIAL_LOGIN = "is_social_login";
        public static final String KEY_CART_COUNT = "cart_count";
        public static final String KEY_DASHBOARD = "key_dashboard";
        public static final String KEY_ORDER_COUNT = "order_count";
        public static final String KEY_PRODUCT_DETAIL = "key_product_detail";
        public static final String KEY_USER_ADDRESS = "user_address";
        public static final String KEY_USER_CART = "user_cart";
        public static final String KEY_WISHLIST_COUNT = "wishlist_count";
        public static final String LANGUAGE = "selected_language";
        public static final String MODE = "mode";
        public static final String PAYMENT_METHOD = "payment_method";
        public static final String PRIMARYCOLOR = "primaryColor";
        public static final String PRIVACY_POLICY = "privacy_policy";
        public static final String SHIPPING = "user_shipping";
        public static final String SHOW_SWIPE = "showswipe";
        public static final String TERM_CONDITION = "term_condition";
        public static final String TEXTPRIMARYCOLOR = "textPrimaryColor";
        public static final String TEXTSECONDARYCOLOR = "textSecondaryColor";
        public static final String THEME = "selected_theme";
        public static final String THEME_COLOR = "theme_color";
        public static final String TWITTER = "twitter";
        public static final String USER_COUNTRY_CODE = "user_country_code";
        public static final String USER_DISPLAY_NAME = "user_display_name";
        public static final String USER_EMAIL = "user_email";
        public static final String USER_FIRST_NAME = "user_first_name";
        public static final String USER_ID = "user_id";
        public static final String USER_LAST_NAME = "user_last_name";
        public static final String USER_NICE_NAME = "user_nicename";
        public static final String USER_PASSWORD = "user_password";
        public static final String USER_PICODE = "user_pincode";
        public static final String USER_PROFILE = "user_profile";
        public static final String USER_ROLE = "user_username";
        public static final String USER_STATE_CODE = "user_state_code";
        public static final String USER_STATE_NAME = "user_state_name";
        public static final String USER_TOKEN = "user_token";
        public static final String USER_USERNAME = "user_username";
        public static final String WHATSAPP = "whatsapp";

        private SharedPref() {
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0012\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\u0016"}, d2 = {"Lcom/iqonic/store/utils/Constants$KeyIntent;", "", "()V", "ADDRESS_ID", "", "CHECKOUT_URL", "COUPON_CODE", "DATA", "DISCOUNT", "EXTERNAL_URL", "KEYID", "ORDER_DATA", "PRICE", "PRODUCTDATA", "PRODUCT_ID", "SHIPPING", "SHIPPINGDATA", "SHOW_PAGINATION", "SPECIAL_PRODUCT_KEY", "SUBTOTAL", "TITLE", "VIEWALLID", "app_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: Constants.kt */
    public static final class KeyIntent {
        public static final String ADDRESS_ID = "address_id";
        public static final String CHECKOUT_URL = "checkoutUrl";
        public static final String COUPON_CODE = "CouponCode";
        public static final String DATA = "data";
        public static final String DISCOUNT = "DISCOUNT";
        public static final String EXTERNAL_URL = "external_url";
        public static final KeyIntent INSTANCE = new KeyIntent();
        public static final String KEYID = "key_id";
        public static final String ORDER_DATA = "orderData";
        public static final String PRICE = "price";
        public static final String PRODUCTDATA = "productdata";
        public static final String PRODUCT_ID = "product_id";
        public static final String SHIPPING = "SHIPPING";
        public static final String SHIPPINGDATA = "shippingdata";
        public static final String SHOW_PAGINATION = "show_pagination";
        public static final String SPECIAL_PRODUCT_KEY = "special_product_key";
        public static final String SUBTOTAL = "SUBTOTAL";
        public static final String TITLE = "title";
        public static final String VIEWALLID = "viewallid";

        private KeyIntent() {
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\u0007"}, d2 = {"Lcom/iqonic/store/utils/Constants$OrderStatus;", "", "()V", "CANCELED", "", "COMPLETED", "REFUNDED", "app_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: Constants.kt */
    public static final class OrderStatus {
        public static final String CANCELED = "cancelled";
        public static final String COMPLETED = "completed";
        public static final OrderStatus INSTANCE = new OrderStatus();
        public static final String REFUNDED = "refunded";

        private OrderStatus() {
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0007\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\u000b"}, d2 = {"Lcom/iqonic/store/utils/Constants$viewAllCode;", "", "()V", "BESTSELLING", "", "CATEGORY", "FEATURED", "NEWEST", "OTHER", "SALE", "SPECIAL_PRODUCT", "app_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: Constants.kt */
    public static final class viewAllCode {
        public static final int BESTSELLING = 107;
        public static final int CATEGORY = 104;
        public static final int FEATURED = 103;
        public static final viewAllCode INSTANCE = new viewAllCode();
        public static final int NEWEST = 102;
        public static final int OTHER = 105;
        public static final int SALE = 106;
        public static final int SPECIAL_PRODUCT = 108;

        private viewAllCode() {
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0005\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\t"}, d2 = {"Lcom/iqonic/store/utils/Constants$AppBroadcasts;", "", "()V", "CARTITEM_UPDATE", "", "CART_COUNT_CHANGE", "ORDER_COUNT_CHANGE", "PROFILE_UPDATE", "WISHLIST_UPDATE", "app_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: Constants.kt */
    public static final class AppBroadcasts {
        public static final String CARTITEM_UPDATE = "app.broadcast.OnCartItemUpdated";
        public static final String CART_COUNT_CHANGE = "app.broadcast.setCartCount";
        public static final AppBroadcasts INSTANCE = new AppBroadcasts();
        public static final String ORDER_COUNT_CHANGE = "app.broadcast.OnOrderCountChanged";
        public static final String PROFILE_UPDATE = "app.broadcast.OnProfileUpdated";
        public static final String WISHLIST_UPDATE = "app.broadcast.OnWishListUpdated";

        private AppBroadcasts() {
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\u0006"}, d2 = {"Lcom/iqonic/store/utils/Constants$DateFormatCodes;", "", "()V", "YMD", "", "YMD_HMS", "app_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: Constants.kt */
    public static final class DateFormatCodes {
        public static final DateFormatCodes INSTANCE = new DateFormatCodes();
        public static final int YMD = 1;
        public static final int YMD_HMS = 0;

        private DateFormatCodes() {
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\u0006"}, d2 = {"Lcom/iqonic/store/utils/Constants$PAYMENT_METHOD;", "", "()V", "PAYMENT_METHOD_NATIVE", "", "PAYMENT_METHOD_WEB", "app_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: Constants.kt */
    public static final class PAYMENT_METHOD {
        public static final PAYMENT_METHOD INSTANCE = new PAYMENT_METHOD();
        public static final String PAYMENT_METHOD_NATIVE = "native";
        public static final String PAYMENT_METHOD_WEB = "webview";

        private PAYMENT_METHOD() {
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\u0006"}, d2 = {"Lcom/iqonic/store/utils/Constants$THEME;", "", "()V", "DARK", "", "LIGHT", "app_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: Constants.kt */
    public static final class THEME {
        public static final int DARK = 1;
        public static final THEME INSTANCE = new THEME();
        public static final int LIGHT = 2;

        private THEME() {
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\u0007"}, d2 = {"Lcom/iqonic/store/utils/Constants$TotalItem;", "", "()V", "TOTAL_CATEGORY_PER_PAGE", "", "TOTAL_ITEM_PER_PAGE", "TOTAL_SUB_CATEGORY_PER_PAGE", "app_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: Constants.kt */
    public static final class TotalItem {
        public static final TotalItem INSTANCE = new TotalItem();
        public static final int TOTAL_CATEGORY_PER_PAGE = 20;
        public static final int TOTAL_ITEM_PER_PAGE = 20;
        public static final int TOTAL_SUB_CATEGORY_PER_PAGE = 50;

        private TotalItem() {
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\t\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\r"}, d2 = {"Lcom/iqonic/store/utils/Constants$viewName;", "", "()V", "VIEW_BANNER", "", "VIEW_BEST_SELLING", "VIEW_DEAL_OF_THE_DAY", "VIEW_FEATURED", "VIEW_NEWEST", "VIEW_OFFER", "VIEW_SALE", "VIEW_SUGGESTED_FOR_YOU", "VIEW_YOU_MAY_LIKE", "app_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: Constants.kt */
    public static final class viewName {
        public static final viewName INSTANCE = new viewName();
        public static final String VIEW_BANNER = "slider";
        public static final String VIEW_BEST_SELLING = "best_selling_product";
        public static final String VIEW_DEAL_OF_THE_DAY = "deal_of_the_day";
        public static final String VIEW_FEATURED = "feature_products";
        public static final String VIEW_NEWEST = "newest_product";
        public static final String VIEW_OFFER = "offer";
        public static final String VIEW_SALE = "sale_product";
        public static final String VIEW_SUGGESTED_FOR_YOU = "suggested_for_you";
        public static final String VIEW_YOU_MAY_LIKE = "you_may_like";

        private viewName() {
        }
    }
}
