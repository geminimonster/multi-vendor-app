package com.iqonic.store.utils.photoview;

import android.view.MotionEvent;

public interface OnSingleFlingListener {
    boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent2, float f, float f2);
}
