package com.iqonic.store.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001d\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000*\u0001\u0000\b\n\u0018\u00002\u00020\u0001J\u0018\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0007H\u0016¨\u0006\b"}, d2 = {"com/iqonic/store/utils/BroadcastReceiverExt$broadcastReceiver$1", "Landroid/content/BroadcastReceiver;", "onReceive", "", "context", "Landroid/content/Context;", "intent", "Landroid/content/Intent;", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: BroadcastReceiverExt.kt */
public final class BroadcastReceiverExt$broadcastReceiver$1 extends BroadcastReceiver {
    final /* synthetic */ BroadcastReceiverExt this$0;

    BroadcastReceiverExt$broadcastReceiver$1(BroadcastReceiverExt broadcastReceiverExt) {
        this.this$0 = broadcastReceiverExt;
    }

    public void onReceive(Context context, Intent intent) {
        Intrinsics.checkParameterIsNotNull(context, "context");
        Intrinsics.checkParameterIsNotNull(intent, "intent");
        for (Instructions instructions : this.this$0.instructions) {
            if (instructions.matches(intent)) {
                instructions.execution().invoke(intent);
                return;
            }
        }
    }
}
