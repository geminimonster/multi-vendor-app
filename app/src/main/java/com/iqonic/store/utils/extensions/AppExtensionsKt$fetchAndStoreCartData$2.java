package com.iqonic.store.utils.extensions;

import android.app.Activity;
import com.iqonic.store.utils.Constants;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n¢\u0006\u0002\b\u0004"}, d2 = {"<anonymous>", "", "it", "", "invoke"}, k = 3, mv = {1, 1, 16})
/* compiled from: AppExtensions.kt */
final class AppExtensionsKt$fetchAndStoreCartData$2 extends Lambda implements Function1<String, Unit> {
    final /* synthetic */ Activity $this_fetchAndStoreCartData;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    AppExtensionsKt$fetchAndStoreCartData$2(Activity activity) {
        super(1);
        this.$this_fetchAndStoreCartData = activity;
    }

    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        invoke((String) obj);
        return Unit.INSTANCE;
    }

    public final void invoke(String str) {
        Intrinsics.checkParameterIsNotNull(str, "it");
        AppExtensionsKt.getSharedPrefInstance().setValue(Constants.SharedPref.KEY_CART_COUNT, 0);
        AppExtensionsKt.sendCartCountChangeBroadcast(this.$this_fetchAndStoreCartData);
    }
}
