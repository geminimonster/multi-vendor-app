package com.iqonic.store.utils.swipereveallayout;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import androidx.core.view.GestureDetectorCompat;
import androidx.core.view.ViewCompat;
import androidx.customview.widget.ViewDragHelper;
import com.iqonic.store.R;

public class SwipeRevealLayout extends ViewGroup {
    public static final int DRAG_EDGE_BOTTOM = 8;
    public static final int DRAG_EDGE_LEFT = 1;
    public static final int DRAG_EDGE_RIGHT = 2;
    public static final int DRAG_EDGE_TOP = 4;
    public static final int MODE_NORMAL = 0;
    public static final int MODE_SAME_LEVEL = 1;
    protected static final int STATE_CLOSE = 0;
    protected static final int STATE_CLOSING = 1;
    protected static final int STATE_DRAGGING = 4;
    protected static final int STATE_OPEN = 2;
    protected static final int STATE_OPENING = 3;
    private static final int DEFAULT_MIN_DIST_REQUEST_DISALLOW_PARENT = 1;
    private static final int DEFAULT_MIN_FLING_VELOCITY = 300;
    /* access modifiers changed from: private */
    public volatile boolean mAborted = false;
    /* access modifiers changed from: private */
    public int mDragEdge = 1;
    /* access modifiers changed from: private */
    public ViewDragHelper mDragHelper;
    /* access modifiers changed from: private */
    public DragStateChangeListener mDragStateChangeListener;
    /* access modifiers changed from: private */
    public volatile boolean mIsScrolling = false;
    /* access modifiers changed from: private */
    public int mLastMainLeft = 0;
    /* access modifiers changed from: private */
    public int mLastMainTop = 0;
    /* access modifiers changed from: private */
    public volatile boolean mLockDrag = false;
    private boolean mIsOpenBeforeInit = false;
    /* access modifiers changed from: private */
    public View mMainView;
    /* access modifiers changed from: private */
    public int mMinDistRequestDisallowParent = 0;
    /* access modifiers changed from: private */
    public int mMinFlingVelocity = DEFAULT_MIN_FLING_VELOCITY;
    /* access modifiers changed from: private */
    public int mMode = 0;
    /* access modifiers changed from: private */
    public Rect mRectMainClose = new Rect();
    /* access modifiers changed from: private */
    public Rect mRectMainOpen = new Rect();
    /* access modifiers changed from: private */
    public View mSecondaryView;
    private final GestureDetector.OnGestureListener mGestureListener = new GestureDetector.SimpleOnGestureListener() {
        boolean hasDisallowed = false;

        public boolean onDown(MotionEvent motionEvent) {
            boolean unused = SwipeRevealLayout.this.mIsScrolling = false;
            this.hasDisallowed = false;
            return true;
        }

        public boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent2, float f, float f2) {
            boolean unused = SwipeRevealLayout.this.mIsScrolling = true;
            return false;
        }

        public boolean onScroll(MotionEvent motionEvent, MotionEvent motionEvent2, float f, float f2) {
            boolean z = true;
            boolean unused = SwipeRevealLayout.this.mIsScrolling = true;
            if (SwipeRevealLayout.this.getParent() != null) {
                if (!this.hasDisallowed) {
                    boolean z2 = SwipeRevealLayout.this.getDistToClosestEdge() >= SwipeRevealLayout.this.mMinDistRequestDisallowParent;
                    if (z2) {
                        this.hasDisallowed = true;
                    }
                    z = z2;
                }
                SwipeRevealLayout.this.getParent().requestDisallowInterceptTouchEvent(z);
            }
            return false;
        }
    };
    private int mOnLayoutCount = 0;
    /* access modifiers changed from: private */
    public int mState = 0;
    /* access modifiers changed from: private */
    public SwipeListener mSwipeListener;
    private float mDragDist = 0.0f;
    private GestureDetectorCompat mGestureDetector;
    private float mPrevX = -1.0f;
    private float mPrevY = -1.0f;
    private Rect mRectSecClose = new Rect();
    private Rect mRectSecOpen = new Rect();
    private final ViewDragHelper.Callback mDragHelperCallback = new ViewDragHelper.Callback() {
        public boolean tryCaptureView(View view, int i) {
            boolean unused = SwipeRevealLayout.this.mAborted = false;
            if (SwipeRevealLayout.this.mLockDrag) {
                return false;
            }
            SwipeRevealLayout.this.mDragHelper.captureChildView(SwipeRevealLayout.this.mMainView, i);
            return false;
        }

        public int clampViewPositionVertical(View view, int i, int i2) {
            int access$700 = SwipeRevealLayout.this.mDragEdge;
            if (access$700 == 4) {
                return Math.max(Math.min(i, SwipeRevealLayout.this.mRectMainClose.top + SwipeRevealLayout.this.mSecondaryView.getHeight()), SwipeRevealLayout.this.mRectMainClose.top);
            }
            if (access$700 != 8) {
                return view.getTop();
            }
            return Math.max(Math.min(i, SwipeRevealLayout.this.mRectMainClose.top), SwipeRevealLayout.this.mRectMainClose.top - SwipeRevealLayout.this.mSecondaryView.getHeight());
        }

        public int clampViewPositionHorizontal(View view, int i, int i2) {
            int access$700 = SwipeRevealLayout.this.mDragEdge;
            if (access$700 == 1) {
                return Math.max(Math.min(i, SwipeRevealLayout.this.mRectMainClose.left + SwipeRevealLayout.this.mSecondaryView.getWidth()), SwipeRevealLayout.this.mRectMainClose.left);
            }
            if (access$700 != 2) {
                return view.getLeft();
            }
            return Math.max(Math.min(i, SwipeRevealLayout.this.mRectMainClose.left), SwipeRevealLayout.this.mRectMainClose.left - SwipeRevealLayout.this.mSecondaryView.getWidth());
        }

        public void onViewReleased(View view, float f, float f2) {
            int i = (int) f;
            boolean z = false;
            boolean z2 = SwipeRevealLayout.this.pxToDp(i) >= SwipeRevealLayout.this.mMinFlingVelocity;
            boolean z3 = SwipeRevealLayout.this.pxToDp(i) <= (-SwipeRevealLayout.this.mMinFlingVelocity);
            int i2 = (int) f2;
            boolean z4 = SwipeRevealLayout.this.pxToDp(i2) <= (-SwipeRevealLayout.this.mMinFlingVelocity);
            if (SwipeRevealLayout.this.pxToDp(i2) >= SwipeRevealLayout.this.mMinFlingVelocity) {
                z = true;
            }
            int access$1200 = SwipeRevealLayout.this.getHalfwayPivotHorizontal();
            int access$1300 = SwipeRevealLayout.this.getHalfwayPivotVertical();
            int access$700 = SwipeRevealLayout.this.mDragEdge;
            if (access$700 != 1) {
                if (access$700 != 2) {
                    if (access$700 != 4) {
                        if (access$700 == 8) {
                            if (z4) {
                                SwipeRevealLayout.this.open(true);
                            } else if (z) {
                                SwipeRevealLayout.this.close(true);
                            } else if (SwipeRevealLayout.this.mMainView.getBottom() < access$1300) {
                                SwipeRevealLayout.this.open(true);
                            } else {
                                SwipeRevealLayout.this.close(true);
                            }
                        }
                    } else if (z4) {
                        SwipeRevealLayout.this.close(true);
                    } else if (z) {
                        SwipeRevealLayout.this.open(true);
                    } else if (SwipeRevealLayout.this.mMainView.getTop() < access$1300) {
                        SwipeRevealLayout.this.close(true);
                    } else {
                        SwipeRevealLayout.this.open(true);
                    }
                } else if (z2) {
                    SwipeRevealLayout.this.close(true);
                } else if (z3) {
                    SwipeRevealLayout.this.open(true);
                } else if (SwipeRevealLayout.this.mMainView.getRight() < access$1200) {
                    SwipeRevealLayout.this.open(true);
                } else {
                    SwipeRevealLayout.this.close(true);
                }
            } else if (z2) {
                SwipeRevealLayout.this.open(true);
            } else if (z3) {
                SwipeRevealLayout.this.close(true);
            } else if (SwipeRevealLayout.this.mMainView.getLeft() < access$1200) {
                SwipeRevealLayout.this.close(true);
            } else {
                SwipeRevealLayout.this.open(true);
            }
        }

        public void onEdgeDragStarted(int i, int i2) {
            super.onEdgeDragStarted(i, i2);
            if (!SwipeRevealLayout.this.mLockDrag) {
                boolean z = false;
                boolean z2 = SwipeRevealLayout.this.mDragEdge == 2 && i == 1;
                boolean z3 = SwipeRevealLayout.this.mDragEdge == 1 && i == 2;
                boolean z4 = SwipeRevealLayout.this.mDragEdge == 8 && i == 4;
                if (SwipeRevealLayout.this.mDragEdge == 4 && i == 8) {
                    z = true;
                }
                if (z2 || z3 || z4 || z) {
                    SwipeRevealLayout.this.mDragHelper.captureChildView(SwipeRevealLayout.this.mMainView, i2);
                }
            }
        }

        public void onViewPositionChanged(View view, int i, int i2, int i3, int i4) {
            super.onViewPositionChanged(view, i, i2, i3, i4);
            boolean z = true;
            if (SwipeRevealLayout.this.mMode == 1) {
                if (SwipeRevealLayout.this.mDragEdge == 1 || SwipeRevealLayout.this.mDragEdge == 2) {
                    SwipeRevealLayout.this.mSecondaryView.offsetLeftAndRight(i3);
                } else {
                    SwipeRevealLayout.this.mSecondaryView.offsetTopAndBottom(i4);
                }
            }
            if (SwipeRevealLayout.this.mMainView.getLeft() == SwipeRevealLayout.this.mLastMainLeft && SwipeRevealLayout.this.mMainView.getTop() == SwipeRevealLayout.this.mLastMainTop) {
                z = false;
            }
            if (SwipeRevealLayout.this.mSwipeListener != null && z) {
                if (SwipeRevealLayout.this.mMainView.getLeft() == SwipeRevealLayout.this.mRectMainClose.left && SwipeRevealLayout.this.mMainView.getTop() == SwipeRevealLayout.this.mRectMainClose.top) {
                    SwipeRevealLayout.this.mSwipeListener.onClosed(SwipeRevealLayout.this);
                } else if (SwipeRevealLayout.this.mMainView.getLeft() == SwipeRevealLayout.this.mRectMainOpen.left && SwipeRevealLayout.this.mMainView.getTop() == SwipeRevealLayout.this.mRectMainOpen.top) {
                    SwipeRevealLayout.this.mSwipeListener.onOpened(SwipeRevealLayout.this);
                } else {
                    SwipeRevealLayout.this.mSwipeListener.onSlide(SwipeRevealLayout.this, getSlideOffset());
                }
            }
            SwipeRevealLayout swipeRevealLayout = SwipeRevealLayout.this;
            int unused = swipeRevealLayout.mLastMainLeft = swipeRevealLayout.mMainView.getLeft();
            SwipeRevealLayout swipeRevealLayout2 = SwipeRevealLayout.this;
            int unused2 = swipeRevealLayout2.mLastMainTop = swipeRevealLayout2.mMainView.getTop();
            ViewCompat.postInvalidateOnAnimation(SwipeRevealLayout.this);
        }

        private float getSlideOffset() {
            float left;
            int width;
            int access$700 = SwipeRevealLayout.this.mDragEdge;
            if (access$700 == 1) {
                left = (float) (SwipeRevealLayout.this.mMainView.getLeft() - SwipeRevealLayout.this.mRectMainClose.left);
                width = SwipeRevealLayout.this.mSecondaryView.getWidth();
            } else if (access$700 == 2) {
                left = (float) (SwipeRevealLayout.this.mRectMainClose.left - SwipeRevealLayout.this.mMainView.getLeft());
                width = SwipeRevealLayout.this.mSecondaryView.getWidth();
            } else if (access$700 == 4) {
                left = (float) (SwipeRevealLayout.this.mMainView.getTop() - SwipeRevealLayout.this.mRectMainClose.top);
                width = SwipeRevealLayout.this.mSecondaryView.getHeight();
            } else if (access$700 != 8) {
                return 0.0f;
            } else {
                left = (float) (SwipeRevealLayout.this.mRectMainClose.top - SwipeRevealLayout.this.mMainView.getTop());
                width = SwipeRevealLayout.this.mSecondaryView.getHeight();
            }
            return left / ((float) width);
        }

        public void onViewDragStateChanged(int i) {
            super.onViewDragStateChanged(i);
            int access$1900 = SwipeRevealLayout.this.mState;
            if (i != 0) {
                if (i == 1) {
                    int unused = SwipeRevealLayout.this.mState = 4;
                }
            } else if (SwipeRevealLayout.this.mDragEdge == 1 || SwipeRevealLayout.this.mDragEdge == 2) {
                if (SwipeRevealLayout.this.mMainView.getLeft() == SwipeRevealLayout.this.mRectMainClose.left) {
                    int unused2 = SwipeRevealLayout.this.mState = 0;
                } else {
                    int unused3 = SwipeRevealLayout.this.mState = 2;
                }
            } else if (SwipeRevealLayout.this.mMainView.getTop() == SwipeRevealLayout.this.mRectMainClose.top) {
                int unused4 = SwipeRevealLayout.this.mState = 0;
            } else {
                int unused5 = SwipeRevealLayout.this.mState = 2;
            }
            if (SwipeRevealLayout.this.mDragStateChangeListener != null && !SwipeRevealLayout.this.mAborted && access$1900 != SwipeRevealLayout.this.mState) {
                SwipeRevealLayout.this.mDragStateChangeListener.onDragStateChanged(SwipeRevealLayout.this.mState);
            }
        }
    };

    public SwipeRevealLayout(Context context) {
        super(context);
        init(context, (AttributeSet) null);
    }

    public SwipeRevealLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        init(context, attributeSet);
    }

    public SwipeRevealLayout(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    public static String getStateString(int i) {
        return i != 0 ? i != 1 ? i != 2 ? i != 3 ? i != 4 ? "undefined" : "state_dragging" : "state_opening" : "state_open" : "state_closing" : "state_close";
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        this.mGestureDetector.onTouchEvent(motionEvent);
        this.mDragHelper.processTouchEvent(motionEvent);
        return true;
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        if (isDragLocked()) {
            return super.onInterceptTouchEvent(motionEvent);
        }
        this.mDragHelper.processTouchEvent(motionEvent);
        this.mGestureDetector.onTouchEvent(motionEvent);
        accumulateDragDist(motionEvent);
        boolean couldBecomeClick = couldBecomeClick(motionEvent);
        boolean z = this.mDragHelper.getViewDragState() == 2;
        boolean z2 = this.mDragHelper.getViewDragState() == 0 && this.mIsScrolling;
        this.mPrevX = motionEvent.getX();
        this.mPrevY = motionEvent.getY();
        if (couldBecomeClick || (!z && !z2)) {
            return false;
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void onFinishInflate() {
        super.onFinishInflate();
        if (getChildCount() >= 2) {
            this.mSecondaryView = getChildAt(0);
            this.mMainView = getChildAt(1);
        } else if (getChildCount() == 1) {
            this.mMainView = getChildAt(0);
        }
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        boolean z2;
        boolean z3;
        int i5;
        int i6;
        int i7;
        int i8;
        int i9 = 0;
        this.mAborted = false;
        int i10 = 0;
        while (i10 < getChildCount()) {
            View childAt = getChildAt(i10);
            int paddingLeft = getPaddingLeft();
            int max = Math.max((i3 - getPaddingRight()) - i, i9);
            int paddingTop = getPaddingTop();
            int max2 = Math.max((i4 - getPaddingBottom()) - i2, i9);
            int measuredHeight = childAt.getMeasuredHeight();
            int measuredWidth = childAt.getMeasuredWidth();
            ViewGroup.LayoutParams layoutParams = childAt.getLayoutParams();
            if (layoutParams != null) {
                z2 = layoutParams.height == -1 || layoutParams.height == -1;
                z3 = layoutParams.width == -1 || layoutParams.width == -1;
            } else {
                z3 = false;
                z2 = false;
            }
            if (z2) {
                measuredHeight = max2 - paddingTop;
                layoutParams.height = measuredHeight;
            }
            if (z3) {
                measuredWidth = max - paddingLeft;
                layoutParams.width = measuredWidth;
            }
            int i11 = this.mDragEdge;
            if (i11 == 1) {
                i8 = Math.min(getPaddingLeft(), max);
                i7 = Math.min(getPaddingTop(), max2);
                i6 = Math.min(measuredWidth + getPaddingLeft(), max);
                i5 = Math.min(measuredHeight + getPaddingTop(), max2);
            } else if (i11 == 2) {
                i8 = Math.max(((i3 - measuredWidth) - getPaddingRight()) - i, paddingLeft);
                i7 = Math.min(getPaddingTop(), max2);
                i6 = Math.max((i3 - getPaddingRight()) - i, paddingLeft);
                i5 = Math.min(measuredHeight + getPaddingTop(), max2);
            } else if (i11 == 4) {
                i8 = Math.min(getPaddingLeft(), max);
                i7 = Math.min(getPaddingTop(), max2);
                i6 = Math.min(measuredWidth + getPaddingLeft(), max);
                i5 = Math.min(measuredHeight + getPaddingTop(), max2);
            } else if (i11 != 8) {
                i8 = 0;
                i7 = 0;
                i6 = 0;
                i5 = 0;
            } else {
                i8 = Math.min(getPaddingLeft(), max);
                i7 = Math.max(((i4 - measuredHeight) - getPaddingBottom()) - i2, paddingTop);
                i6 = Math.min(measuredWidth + getPaddingLeft(), max);
                i5 = Math.max((i4 - getPaddingBottom()) - i2, paddingTop);
            }
            childAt.layout(i8, i7, i6, i5);
            i10++;
            i9 = 0;
        }
        if (this.mMode == 1) {
            int i12 = this.mDragEdge;
            if (i12 == 1) {
                View view = this.mSecondaryView;
                view.offsetLeftAndRight(-view.getWidth());
            } else if (i12 == 2) {
                View view2 = this.mSecondaryView;
                view2.offsetLeftAndRight(view2.getWidth());
            } else if (i12 == 4) {
                View view3 = this.mSecondaryView;
                view3.offsetTopAndBottom(-view3.getHeight());
            } else if (i12 == 8) {
                View view4 = this.mSecondaryView;
                view4.offsetTopAndBottom(view4.getHeight());
            }
        }
        initRects();
        if (this.mIsOpenBeforeInit) {
            open(false);
        } else {
            close(false);
        }
        this.mLastMainLeft = this.mMainView.getLeft();
        this.mLastMainTop = this.mMainView.getTop();
        this.mOnLayoutCount++;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        if (getChildCount() >= 2) {
            ViewGroup.LayoutParams layoutParams = getLayoutParams();
            int mode = View.MeasureSpec.getMode(i);
            int mode2 = View.MeasureSpec.getMode(i2);
            int i3 = 0;
            int i4 = 0;
            for (int i5 = 0; i5 < getChildCount(); i5++) {
                View childAt = getChildAt(i5);
                measureChild(childAt, i, i2);
                i3 = Math.max(childAt.getMeasuredWidth(), i3);
                i4 = Math.max(childAt.getMeasuredHeight(), i4);
            }
            int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(i3, mode);
            int makeMeasureSpec2 = View.MeasureSpec.makeMeasureSpec(i4, mode2);
            int size = View.MeasureSpec.getSize(makeMeasureSpec);
            int size2 = View.MeasureSpec.getSize(makeMeasureSpec2);
            for (int i6 = 0; i6 < getChildCount(); i6++) {
                View childAt2 = getChildAt(i6);
                ViewGroup.LayoutParams layoutParams2 = childAt2.getLayoutParams();
                if (layoutParams2 != null) {
                    if (layoutParams2.height == -1) {
                        childAt2.setMinimumHeight(size2);
                    }
                    if (layoutParams2.width == -1) {
                        childAt2.setMinimumWidth(size);
                    }
                }
                measureChild(childAt2, makeMeasureSpec, makeMeasureSpec2);
                i3 = Math.max(childAt2.getMeasuredWidth(), i3);
                i4 = Math.max(childAt2.getMeasuredHeight(), i4);
            }
            int paddingLeft = i3 + getPaddingLeft() + getPaddingRight();
            int paddingTop = i4 + getPaddingTop() + getPaddingBottom();
            if (mode != 1073741824) {
                if (layoutParams.width == -1) {
                    paddingLeft = size;
                }
                if (mode != Integer.MIN_VALUE || paddingLeft <= size) {
                    size = paddingLeft;
                }
            }
            if (mode2 != 1073741824) {
                if (layoutParams.height == -1) {
                    paddingTop = size2;
                }
                if (mode2 != Integer.MIN_VALUE || paddingTop <= size2) {
                    size2 = paddingTop;
                }
            }
            setMeasuredDimension(size, size2);
            return;
        }
        throw new RuntimeException("Layout must have two children");
    }

    public void computeScroll() {
        if (this.mDragHelper.continueSettling(true)) {
            ViewCompat.postInvalidateOnAnimation(this);
        }
    }

    public void open(boolean z) {
        this.mIsOpenBeforeInit = true;
        this.mAborted = false;
        if (z) {
            this.mState = 3;
            this.mDragHelper.smoothSlideViewTo(this.mMainView, this.mRectMainOpen.left, this.mRectMainOpen.top);
            DragStateChangeListener dragStateChangeListener = this.mDragStateChangeListener;
            if (dragStateChangeListener != null) {
                dragStateChangeListener.onDragStateChanged(this.mState);
            }
        } else {
            this.mState = 2;
            this.mDragHelper.abort();
            this.mMainView.layout(this.mRectMainOpen.left, this.mRectMainOpen.top, this.mRectMainOpen.right, this.mRectMainOpen.bottom);
            this.mSecondaryView.layout(this.mRectSecOpen.left, this.mRectSecOpen.top, this.mRectSecOpen.right, this.mRectSecOpen.bottom);
        }
        ViewCompat.postInvalidateOnAnimation(this);
    }

    public void close(boolean z) {
        this.mIsOpenBeforeInit = false;
        this.mAborted = false;
        if (z) {
            this.mState = 1;
            this.mDragHelper.smoothSlideViewTo(this.mMainView, this.mRectMainClose.left, this.mRectMainClose.top);
            DragStateChangeListener dragStateChangeListener = this.mDragStateChangeListener;
            if (dragStateChangeListener != null) {
                dragStateChangeListener.onDragStateChanged(this.mState);
            }
        } else {
            this.mState = 0;
            this.mDragHelper.abort();
            this.mMainView.layout(this.mRectMainClose.left, this.mRectMainClose.top, this.mRectMainClose.right, this.mRectMainClose.bottom);
            this.mSecondaryView.layout(this.mRectSecClose.left, this.mRectSecClose.top, this.mRectSecClose.right, this.mRectSecClose.bottom);
        }
        ViewCompat.postInvalidateOnAnimation(this);
    }

    public int getMinFlingVelocity() {
        return this.mMinFlingVelocity;
    }

    public void setMinFlingVelocity(int i) {
        this.mMinFlingVelocity = i;
    }

    public int getDragEdge() {
        return this.mDragEdge;
    }

    public void setDragEdge(int i) {
        this.mDragEdge = i;
    }

    public void setSwipeListener(SwipeListener swipeListener) {
        this.mSwipeListener = swipeListener;
    }

    public void setLockDrag(boolean z) {
        this.mLockDrag = z;
    }

    public boolean isDragLocked() {
        return this.mLockDrag;
    }

    public boolean isOpened() {
        return this.mState == 2;
    }

    public boolean isClosed() {
        return this.mState == 0;
    }

    /* access modifiers changed from: package-private */
    public void setDragStateChangeListener(DragStateChangeListener dragStateChangeListener) {
        this.mDragStateChangeListener = dragStateChangeListener;
    }

    /* access modifiers changed from: protected */
    public void abort() {
        this.mAborted = true;
        this.mDragHelper.abort();
    }

    /* access modifiers changed from: protected */
    public boolean shouldRequestLayout() {
        return this.mOnLayoutCount < 2;
    }

    private int getMainOpenLeft() {
        int i = this.mDragEdge;
        if (i == 1) {
            return this.mRectMainClose.left + this.mSecondaryView.getWidth();
        }
        if (i == 2) {
            return this.mRectMainClose.left - this.mSecondaryView.getWidth();
        }
        if (i == 4) {
            return this.mRectMainClose.left;
        }
        if (i != 8) {
            return 0;
        }
        return this.mRectMainClose.left;
    }

    private int getMainOpenTop() {
        int i = this.mDragEdge;
        if (i == 1) {
            return this.mRectMainClose.top;
        }
        if (i == 2) {
            return this.mRectMainClose.top;
        }
        if (i == 4) {
            return this.mRectMainClose.top + this.mSecondaryView.getHeight();
        }
        if (i != 8) {
            return 0;
        }
        return this.mRectMainClose.top - this.mSecondaryView.getHeight();
    }

    private int getSecOpenLeft() {
        int i;
        if (this.mMode == 0 || (i = this.mDragEdge) == 8 || i == 4) {
            return this.mRectSecClose.left;
        }
        if (i == 1) {
            return this.mRectSecClose.left + this.mSecondaryView.getWidth();
        }
        return this.mRectSecClose.left - this.mSecondaryView.getWidth();
    }

    private int getSecOpenTop() {
        int i;
        if (this.mMode == 0 || (i = this.mDragEdge) == 1 || i == 2) {
            return this.mRectSecClose.top;
        }
        if (i == 4) {
            return this.mRectSecClose.top + this.mSecondaryView.getHeight();
        }
        return this.mRectSecClose.top - this.mSecondaryView.getHeight();
    }

    private void initRects() {
        this.mRectMainClose.set(this.mMainView.getLeft(), this.mMainView.getTop(), this.mMainView.getRight(), this.mMainView.getBottom());
        this.mRectSecClose.set(this.mSecondaryView.getLeft(), this.mSecondaryView.getTop(), this.mSecondaryView.getRight(), this.mSecondaryView.getBottom());
        this.mRectMainOpen.set(getMainOpenLeft(), getMainOpenTop(), getMainOpenLeft() + this.mMainView.getWidth(), getMainOpenTop() + this.mMainView.getHeight());
        this.mRectSecOpen.set(getSecOpenLeft(), getSecOpenTop(), getSecOpenLeft() + this.mSecondaryView.getWidth(), getSecOpenTop() + this.mSecondaryView.getHeight());
    }

    private boolean couldBecomeClick(MotionEvent motionEvent) {
        return isInMainView(motionEvent) && !shouldInitiateADrag();
    }

    private boolean isInMainView(MotionEvent motionEvent) {
        float x = motionEvent.getX();
        float y = motionEvent.getY();
        boolean z = ((float) this.mMainView.getTop()) <= y && y <= ((float) this.mMainView.getBottom());
        boolean z2 = ((float) this.mMainView.getLeft()) <= x && x <= ((float) this.mMainView.getRight());
        if (!z || !z2) {
            return false;
        }
        return true;
    }

    private boolean shouldInitiateADrag() {
        return this.mDragDist >= ((float) this.mDragHelper.getTouchSlop());
    }

    private void accumulateDragDist(MotionEvent motionEvent) {
        float f;
        if (motionEvent.getAction() == 0) {
            this.mDragDist = 0.0f;
            return;
        }
        boolean z = true;
        if (!(getDragEdge() == 1 || getDragEdge() == 2)) {
            z = false;
        }
        if (z) {
            f = Math.abs(motionEvent.getX() - this.mPrevX);
        } else {
            f = Math.abs(motionEvent.getY() - this.mPrevY);
        }
        this.mDragDist += f;
    }

    private void init(Context context, AttributeSet attributeSet) {
        if (!(attributeSet == null || context == null)) {
            TypedArray obtainStyledAttributes = context.getTheme().obtainStyledAttributes(attributeSet, R.styleable.SwipeRevealLayout, 0, 0);
            this.mDragEdge = obtainStyledAttributes.getInteger(0, 1);
            this.mMinFlingVelocity = obtainStyledAttributes.getInteger(1, DEFAULT_MIN_FLING_VELOCITY);
            this.mMode = obtainStyledAttributes.getInteger(3, 0);
            this.mMinDistRequestDisallowParent = obtainStyledAttributes.getDimensionPixelSize(2, dpToPx(1));
        }
        ViewDragHelper create = ViewDragHelper.create(this, 1.0f, this.mDragHelperCallback);
        this.mDragHelper = create;
        create.setEdgeTrackingEnabled(15);
        this.mGestureDetector = new GestureDetectorCompat(context, this.mGestureListener);
    }

    /* access modifiers changed from: private */
    public int getDistToClosestEdge() {
        int i = this.mDragEdge;
        if (i == 1) {
            return Math.min(this.mMainView.getLeft() - this.mRectMainClose.left, (this.mRectMainClose.left + this.mSecondaryView.getWidth()) - this.mMainView.getLeft());
        } else if (i == 2) {
            return Math.min(this.mMainView.getRight() - (this.mRectMainClose.right - this.mSecondaryView.getWidth()), this.mRectMainClose.right - this.mMainView.getRight());
        } else if (i == 4) {
            int height = this.mRectMainClose.top + this.mSecondaryView.getHeight();
            return Math.min(this.mMainView.getBottom() - height, height - this.mMainView.getTop());
        } else if (i != 8) {
            return 0;
        } else {
            return Math.min(this.mRectMainClose.bottom - this.mMainView.getBottom(), this.mMainView.getBottom() - (this.mRectMainClose.bottom - this.mSecondaryView.getHeight()));
        }
    }

    /* access modifiers changed from: private */
    public int getHalfwayPivotHorizontal() {
        if (this.mDragEdge == 1) {
            return this.mRectMainClose.left + (this.mSecondaryView.getWidth() / 2);
        }
        return this.mRectMainClose.right - (this.mSecondaryView.getWidth() / 2);
    }

    /* access modifiers changed from: private */
    public int getHalfwayPivotVertical() {
        if (this.mDragEdge == 4) {
            return this.mRectMainClose.top + (this.mSecondaryView.getHeight() / 2);
        }
        return this.mRectMainClose.bottom - (this.mSecondaryView.getHeight() / 2);
    }

    /* access modifiers changed from: private */
    public int pxToDp(int i) {
        return (int) (((float) i) / (((float) getContext().getResources().getDisplayMetrics().densityDpi) / 160.0f));
    }

    private int dpToPx(int i) {
        return (int) (((float) i) * (((float) getContext().getResources().getDisplayMetrics().densityDpi) / 160.0f));
    }

    interface DragStateChangeListener {
        void onDragStateChanged(int i);
    }

    public interface SwipeListener {
        void onClosed(SwipeRevealLayout swipeRevealLayout);

        void onOpened(SwipeRevealLayout swipeRevealLayout);

        void onSlide(SwipeRevealLayout swipeRevealLayout, float f);
    }

    public static class SimpleSwipeListener implements SwipeListener {
        public void onClosed(SwipeRevealLayout swipeRevealLayout) {
        }

        public void onOpened(SwipeRevealLayout swipeRevealLayout) {
        }

        public void onSlide(SwipeRevealLayout swipeRevealLayout, float f) {
        }
    }
}
