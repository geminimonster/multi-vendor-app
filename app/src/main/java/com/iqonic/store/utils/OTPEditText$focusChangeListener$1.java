package com.iqonic.store.utils;

import android.view.View;
import android.widget.EditText;
import kotlin.Metadata;
import kotlin.TypeCastException;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u00032\u0006\u0010\u0005\u001a\u00020\u0006H\n¢\u0006\u0002\b\u0007"}, d2 = {"<anonymous>", "", "v", "Landroid/view/View;", "kotlin.jvm.PlatformType", "hasFocus", "", "onFocusChange"}, k = 3, mv = {1, 1, 16})
/* compiled from: OTPEditText.kt */
final class OTPEditText$focusChangeListener$1 implements View.OnFocusChangeListener {
    final /* synthetic */ OTPEditText this$0;

    OTPEditText$focusChangeListener$1(OTPEditText oTPEditText) {
        this.this$0 = oTPEditText;
    }

    public final void onFocusChange(View view, boolean z) {
        if (!z) {
            return;
        }
        if (view != null) {
            ((EditText) view).setBackground(this.this$0.getTransaparant());
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type android.widget.EditText");
    }
}
