package com.iqonic.store.utils.swipereveallayout;

import android.os.Bundle;
import com.iqonic.store.utils.swipereveallayout.SwipeRevealLayout;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class ViewBinderHelper {
    private static final String BUNDLE_MAP_KEY = "ViewBinderHelper_Bundle_Map_Key";
    /* access modifiers changed from: private */
    public Map<String, Integer> mapStates = Collections.synchronizedMap(new HashMap());
    /* access modifiers changed from: private */
    public volatile boolean openOnlyOne = false;
    private Set<String> lockedSwipeSet = Collections.synchronizedSet(new HashSet());
    private Map<String, SwipeRevealLayout> mapLayouts = Collections.synchronizedMap(new HashMap());
    private final Object stateChangeLock = new Object();

    public void bind(final SwipeRevealLayout swipeRevealLayout, final String str) {
        if (swipeRevealLayout.shouldRequestLayout()) {
            swipeRevealLayout.requestLayout();
        }
        this.mapLayouts.values().remove(swipeRevealLayout);
        this.mapLayouts.put(str, swipeRevealLayout);
        swipeRevealLayout.abort();
        swipeRevealLayout.setDragStateChangeListener(new SwipeRevealLayout.DragStateChangeListener() {
            public void onDragStateChanged(int i) {
                ViewBinderHelper.this.mapStates.put(str, Integer.valueOf(i));
                if (ViewBinderHelper.this.openOnlyOne) {
                    ViewBinderHelper.this.closeOthers(str, swipeRevealLayout);
                }
            }
        });
        if (!this.mapStates.containsKey(str)) {
            this.mapStates.put(str, 0);
            swipeRevealLayout.close(false);
        } else {
            int intValue = this.mapStates.get(str).intValue();
            if (intValue == 0 || intValue == 1 || intValue == 4) {
                swipeRevealLayout.close(false);
            } else {
                swipeRevealLayout.open(false);
            }
        }
        swipeRevealLayout.setLockDrag(this.lockedSwipeSet.contains(str));
    }

    public void saveStates(Bundle bundle) {
        if (bundle != null) {
            Bundle bundle2 = new Bundle();
            for (Map.Entry next : this.mapStates.entrySet()) {
                bundle2.putInt((String) next.getKey(), ((Integer) next.getValue()).intValue());
            }
            bundle.putBundle(BUNDLE_MAP_KEY, bundle2);
        }
    }

    public void restoreStates(Bundle bundle) {
        if (bundle != null && bundle.containsKey(BUNDLE_MAP_KEY)) {
            HashMap hashMap = new HashMap();
            Bundle bundle2 = bundle.getBundle(BUNDLE_MAP_KEY);
            Set<String> keySet = bundle2.keySet();
            if (keySet != null) {
                for (String str : keySet) {
                    hashMap.put(str, Integer.valueOf(bundle2.getInt(str)));
                }
            }
            this.mapStates = hashMap;
        }
    }

    public void lockSwipe(String... strArr) {
        setLockSwipe(true, strArr);
    }

    public void unlockSwipe(String... strArr) {
        setLockSwipe(false, strArr);
    }

    public void setOpenOnlyOne(boolean z) {
        this.openOnlyOne = z;
    }

    public void openLayout(String str) {
        synchronized (this.stateChangeLock) {
            this.mapStates.put(str, 2);
            if (this.mapLayouts.containsKey(str)) {
                this.mapLayouts.get(str).open(true);
            } else if (this.openOnlyOne) {
                closeOthers(str, this.mapLayouts.get(str));
            }
        }
    }

    public void closeLayout(String str) {
        synchronized (this.stateChangeLock) {
            this.mapStates.put(str, 0);
            if (this.mapLayouts.containsKey(str)) {
                this.mapLayouts.get(str).close(true);
            }
        }
    }

    /* access modifiers changed from: private */
    public void closeOthers(String str, SwipeRevealLayout swipeRevealLayout) {
        synchronized (this.stateChangeLock) {
            if (getOpenCount() > 1) {
                for (Map.Entry next : this.mapStates.entrySet()) {
                    if (!((String) next.getKey()).equals(str)) {
                        next.setValue(0);
                    }
                }
                for (SwipeRevealLayout next2 : this.mapLayouts.values()) {
                    if (next2 != swipeRevealLayout) {
                        next2.close(true);
                    }
                }
            }
        }
    }

    private void setLockSwipe(boolean z, String... strArr) {
        if (strArr != null && strArr.length != 0) {
            if (z) {
                this.lockedSwipeSet.addAll(Arrays.asList(strArr));
            } else {
                this.lockedSwipeSet.removeAll(Arrays.asList(strArr));
            }
            for (String str : strArr) {
                SwipeRevealLayout swipeRevealLayout = this.mapLayouts.get(str);
                if (swipeRevealLayout != null) {
                    swipeRevealLayout.setLockDrag(z);
                }
            }
        }
    }

    private int getOpenCount() {
        int i = 0;
        for (Integer intValue : this.mapStates.values()) {
            int intValue2 = intValue.intValue();
            if (intValue2 == 2 || intValue2 == 3) {
                i++;
            }
        }
        return i;
    }
}
