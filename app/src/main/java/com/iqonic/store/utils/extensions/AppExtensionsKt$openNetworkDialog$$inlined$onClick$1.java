package com.iqonic.store.utils.extensions;

import android.app.Dialog;
import android.view.View;
import android.widget.RelativeLayout;
import com.iqonic.store.ShopHopApp;
import com.store.proshop.R;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\b\u0003\n\u0002\b\u0004\u0010\u0000\u001a\u00020\u0001\"\b\b\u0000\u0010\u0002*\u00020\u00032\u000e\u0010\u0004\u001a\n \u0005*\u0004\u0018\u00010\u00030\u0003H\n¢\u0006\u0002\b\u0006¨\u0006\u0007"}, d2 = {"<anonymous>", "", "T", "Landroid/view/View;", "it", "kotlin.jvm.PlatformType", "onClick", "com/iqonic/store/utils/extensions/ExtensionsKt$onClick$1"}, k = 3, mv = {1, 1, 16})
/* compiled from: Extensions.kt */
public final class AppExtensionsKt$openNetworkDialog$$inlined$onClick$1 implements View.OnClickListener {
    final /* synthetic */ Function0 $onClick$inlined;
    final /* synthetic */ View $this_onClick;

    public AppExtensionsKt$openNetworkDialog$$inlined$onClick$1(View view, Function0 function0) {
        this.$this_onClick = view;
        this.$onClick$inlined = function0;
    }

    public final void onClick(View view) {
        RelativeLayout relativeLayout = (RelativeLayout) this.$this_onClick;
        if (!ExtensionsKt.isNetworkAvailable()) {
            String string = ShopHopApp.Companion.getAppInstance().getString(R.string.error_no_internet);
            Intrinsics.checkExpressionValueIsNotNull(string, "getAppInstance().getStri…string.error_no_internet)");
            ViewExtensionsKt.snackBarError(relativeLayout, string);
            return;
        }
        Dialog noInternetDialog = ShopHopApp.Companion.getNoInternetDialog();
        if (noInternetDialog != null) {
            noInternetDialog.dismiss();
        }
        this.$onClick$inlined.invoke();
    }
}
