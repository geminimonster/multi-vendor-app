package com.iqonic.store.utils.extensions;

import com.google.gson.Gson;
import com.iqonic.store.models.CountryModel;
import com.iqonic.store.utils.Constants;
import java.util.ArrayList;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0016\u0010\u0002\u001a\u0012\u0012\u0004\u0012\u00020\u00040\u0003j\b\u0012\u0004\u0012\u00020\u0004`\u0005H\n¢\u0006\u0002\b\u0006"}, d2 = {"<anonymous>", "", "its", "Ljava/util/ArrayList;", "Lcom/iqonic/store/models/CountryModel;", "Lkotlin/collections/ArrayList;", "invoke"}, k = 3, mv = {1, 1, 16})
/* compiled from: AppExtensions.kt */
final class AppExtensionsKt$fetchCountry$1 extends Lambda implements Function1<ArrayList<CountryModel>, Unit> {
    final /* synthetic */ Function1 $onApiSuccess;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    AppExtensionsKt$fetchCountry$1(Function1 function1) {
        super(1);
        this.$onApiSuccess = function1;
    }

    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        invoke((ArrayList<CountryModel>) (ArrayList) obj);
        return Unit.INSTANCE;
    }

    public final void invoke(ArrayList<CountryModel> arrayList) {
        Intrinsics.checkParameterIsNotNull(arrayList, "its");
        AppExtensionsKt.getSharedPrefInstance().setValue(Constants.SharedPref.COUNTRY, new Gson().toJson((Object) arrayList));
        this.$onApiSuccess.invoke(arrayList);
    }
}
