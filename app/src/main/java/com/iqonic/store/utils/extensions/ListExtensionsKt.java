package com.iqonic.store.utils.extensions;

import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Spinner;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;
import com.google.firebase.analytics.FirebaseAnalytics;
import java.util.List;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.functions.Function3;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000h\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0011\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u001aM\u0010\u0000\u001a\u00020\u0001*\u00020\u00022\b\b\u0001\u0010\u0003\u001a\u00020\u00042\b\b\u0001\u0010\u0005\u001a\u00020\u00042\f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\b0\u00072\u001a\b\u0002\u0010\t\u001a\u0014\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00010\n¢\u0006\u0002\u0010\u000b\u001aH\u0010\u0000\u001a\u00020\u0001*\u00020\u00022\b\b\u0001\u0010\u0003\u001a\u00020\u00042\b\b\u0001\u0010\u0005\u001a\u00020\u00042\f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\b0\f2\u001a\b\u0002\u0010\t\u001a\u0014\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00010\n\u001aM\u0010\u0000\u001a\u00020\u0001*\u00020\r2\b\b\u0001\u0010\u0003\u001a\u00020\u00042\b\b\u0001\u0010\u0005\u001a\u00020\u00042\f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\b0\u00072\u001a\b\u0002\u0010\t\u001a\u0014\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00010\n¢\u0006\u0002\u0010\u000e\u001aH\u0010\u0000\u001a\u00020\u0001*\u00020\r2\b\b\u0001\u0010\u0003\u001a\u00020\u00042\b\b\u0001\u0010\u0005\u001a\u00020\u00042\f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\b0\f2\u001a\b\u0002\u0010\t\u001a\u0014\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00010\n\u001aº\u0001\u0010\u0000\u001a\u00020\u0001\"\u0004\b\u0000\u0010\u000f*\u00020\u00102\b\b\u0002\u0010\u0011\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u00042\f\u0010\u0006\u001a\b\u0012\u0004\u0012\u0002H\u000f0\u00072\u0006\u0010\u0012\u001a\u00020\u00132#\u0010\u0014\u001a\u001f\u0012\u0004\u0012\u00020\u0016\u0012\u0004\u0012\u0002H\u000f\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00010\u0015¢\u0006\u0002\b\u00172\u001a\b\u0002\u0010\u0018\u001a\u0014\u0012\u0004\u0012\u0002H\u000f\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00010\n2\u001a\b\u0002\u0010\u0019\u001a\u0014\u0012\u0004\u0012\u0002H\u000f\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00010\n2\u000e\b\u0002\u0010\u001a\u001a\b\u0012\u0004\u0012\u00020\u00010\u001b2\u000e\b\u0002\u0010\u001c\u001a\b\u0012\u0004\u0012\u00020\u00010\u001b¢\u0006\u0002\u0010\u001d\u001a\u0002\u0010\u0000\u001a\u00020\u0001\"\u0004\b\u0000\u0010\u000f*\u00020\u00102\b\b\u0002\u0010\u0011\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u00042\f\u0010\u0006\u001a\b\u0012\u0004\u0012\u0002H\u000f0\f2\u0006\u0010\u0012\u001a\u00020\u00132A\u0010\u0014\u001a=\u0012\u0004\u0012\u00020\u0016\u0012\u0013\u0012\u0011H\u000f¢\u0006\f\b\u001e\u0012\b\b\u001f\u0012\u0004\b\b( \u0012\u0013\u0012\u00110\u0004¢\u0006\f\b\u001e\u0012\b\b\u001f\u0012\u0004\b\b(!\u0012\u0004\u0012\u00020\u00010\u0015¢\u0006\u0002\b\u001728\b\u0002\u0010\u0018\u001a2\u0012\u0013\u0012\u0011H\u000f¢\u0006\f\b\u001e\u0012\b\b\u001f\u0012\u0004\b\b( \u0012\u0013\u0012\u00110\u0004¢\u0006\f\b\u001e\u0012\b\b\u001f\u0012\u0004\b\b(!\u0012\u0004\u0012\u00020\u00010\n28\b\u0002\u0010\u0019\u001a2\u0012\u0013\u0012\u0011H\u000f¢\u0006\f\b\u001e\u0012\b\b\u001f\u0012\u0004\b\b( \u0012\u0013\u0012\u00110\u0004¢\u0006\f\b\u001e\u0012\b\b\u001f\u0012\u0004\b\b(!\u0012\u0004\u0012\u00020\u00010\n2\u000e\b\u0002\u0010\u001a\u001a\b\u0012\u0004\u0012\u00020\u00010\u001b2\u000e\b\u0002\u0010\u001c\u001a\b\u0012\u0004\u0012\u00020\u00010\u001b\u001a\u001e\u0010\"\u001a\u00020\u0001*\u00020#2\u0012\u0010\"\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00010$¨\u0006%"}, d2 = {"create", "", "Landroid/widget/AutoCompleteTextView;", "itemLayout", "", "textViewId", "items", "", "", "onItemSelected", "Lkotlin/Function2;", "(Landroid/widget/AutoCompleteTextView;II[Ljava/lang/String;Lkotlin/jvm/functions/Function2;)V", "", "Landroid/widget/Spinner;", "(Landroid/widget/Spinner;II[Ljava/lang/String;Lkotlin/jvm/functions/Function2;)V", "T", "Landroidx/recyclerview/widget/RecyclerView;", "itemSize", "layoutMgr", "Landroidx/recyclerview/widget/RecyclerView$LayoutManager;", "onBindView", "Lkotlin/Function3;", "Landroid/view/View;", "Lkotlin/ExtensionFunctionType;", "itemClick", "itemLongClick", "onScrollTop", "Lkotlin/Function0;", "onScrollBottom", "(Landroidx/recyclerview/widget/RecyclerView;II[Ljava/lang/Object;Landroidx/recyclerview/widget/RecyclerView$LayoutManager;Lkotlin/jvm/functions/Function3;Lkotlin/jvm/functions/Function2;Lkotlin/jvm/functions/Function2;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V", "Lkotlin/ParameterName;", "name", "item", "position", "onPageSelected", "Landroidx/viewpager/widget/ViewPager;", "Lkotlin/Function1;", "app_release"}, k = 2, mv = {1, 1, 16})
/* compiled from: ListExtensions.kt */
public final class ListExtensionsKt {
    public static /* synthetic */ void create$default(Spinner spinner, int i, int i2, String[] strArr, Function2 function2, int i3, Object obj) {
        if ((i3 & 8) != 0) {
            function2 = ListExtensionsKt$create$1.INSTANCE;
        }
        create(spinner, i, i2, strArr, (Function2<? super String, ? super Integer, Unit>) function2);
    }

    public static final void create(Spinner spinner, int i, int i2, String[] strArr, Function2<? super String, ? super Integer, Unit> function2) {
        Intrinsics.checkParameterIsNotNull(spinner, "$this$create");
        Intrinsics.checkParameterIsNotNull(strArr, FirebaseAnalytics.Param.ITEMS);
        Intrinsics.checkParameterIsNotNull(function2, "onItemSelected");
        spinner.setAdapter(new ArrayAdapter(spinner.getContext(), i, i2, strArr));
        spinner.setOnItemSelectedListener(new ListExtensionsKt$create$2(function2, strArr));
    }

    public static /* synthetic */ void create$default(Spinner spinner, int i, int i2, List list, Function2 function2, int i3, Object obj) {
        if ((i3 & 8) != 0) {
            function2 = ListExtensionsKt$create$3.INSTANCE;
        }
        create(spinner, i, i2, (List<String>) list, (Function2<? super String, ? super Integer, Unit>) function2);
    }

    public static final void create(Spinner spinner, int i, int i2, List<String> list, Function2<? super String, ? super Integer, Unit> function2) {
        Intrinsics.checkParameterIsNotNull(spinner, "$this$create");
        Intrinsics.checkParameterIsNotNull(list, FirebaseAnalytics.Param.ITEMS);
        Intrinsics.checkParameterIsNotNull(function2, "onItemSelected");
        spinner.setAdapter(new ArrayAdapter(spinner.getContext(), i, i2, list));
        spinner.setOnItemSelectedListener(new ListExtensionsKt$create$4(function2, list));
    }

    public static /* synthetic */ void create$default(AutoCompleteTextView autoCompleteTextView, int i, int i2, String[] strArr, Function2 function2, int i3, Object obj) {
        if ((i3 & 8) != 0) {
            function2 = ListExtensionsKt$create$5.INSTANCE;
        }
        create(autoCompleteTextView, i, i2, strArr, (Function2<? super String, ? super Integer, Unit>) function2);
    }

    public static final void create(AutoCompleteTextView autoCompleteTextView, int i, int i2, String[] strArr, Function2<? super String, ? super Integer, Unit> function2) {
        Intrinsics.checkParameterIsNotNull(autoCompleteTextView, "$this$create");
        Intrinsics.checkParameterIsNotNull(strArr, FirebaseAnalytics.Param.ITEMS);
        Intrinsics.checkParameterIsNotNull(function2, "onItemSelected");
        autoCompleteTextView.setAdapter(new ArrayAdapter(autoCompleteTextView.getContext(), i, i2, strArr));
        autoCompleteTextView.setOnItemSelectedListener(new ListExtensionsKt$create$6(function2, strArr));
    }

    public static /* synthetic */ void create$default(AutoCompleteTextView autoCompleteTextView, int i, int i2, List list, Function2 function2, int i3, Object obj) {
        if ((i3 & 8) != 0) {
            function2 = ListExtensionsKt$create$7.INSTANCE;
        }
        create(autoCompleteTextView, i, i2, (List<String>) list, (Function2<? super String, ? super Integer, Unit>) function2);
    }

    public static final void create(AutoCompleteTextView autoCompleteTextView, int i, int i2, List<String> list, Function2<? super String, ? super Integer, Unit> function2) {
        Intrinsics.checkParameterIsNotNull(autoCompleteTextView, "$this$create");
        Intrinsics.checkParameterIsNotNull(list, FirebaseAnalytics.Param.ITEMS);
        Intrinsics.checkParameterIsNotNull(function2, "onItemSelected");
        autoCompleteTextView.setAdapter(new ArrayAdapter(autoCompleteTextView.getContext(), i, i2, list));
        autoCompleteTextView.setOnItemSelectedListener(new ListExtensionsKt$create$8(function2, list));
    }

    public static /* synthetic */ void create$default(RecyclerView recyclerView, int i, int i2, Object[] objArr, RecyclerView.LayoutManager layoutManager, Function3 function3, Function2 function2, Function2 function22, Function0 function0, Function0 function02, int i3, Object obj) {
        int i4 = i3;
        create(recyclerView, (i4 & 1) != 0 ? 0 : i, i2, (T[]) objArr, layoutManager, function3, (i4 & 32) != 0 ? ListExtensionsKt$create$9.INSTANCE : function2, (i4 & 64) != 0 ? ListExtensionsKt$create$10.INSTANCE : function22, (Function0<Unit>) (i4 & 128) != 0 ? ListExtensionsKt$create$11.INSTANCE : function0, (Function0<Unit>) (i4 & 256) != 0 ? ListExtensionsKt$create$12.INSTANCE : function02);
    }

    public static final <T> void create(RecyclerView recyclerView, int i, int i2, T[] tArr, RecyclerView.LayoutManager layoutManager, Function3<? super View, ? super T, ? super Integer, Unit> function3, Function2<? super T, ? super Integer, Unit> function2, Function2<? super T, ? super Integer, Unit> function22, Function0<Unit> function0, Function0<Unit> function02) {
        RecyclerView recyclerView2 = recyclerView;
        RecyclerView.LayoutManager layoutManager2 = layoutManager;
        Function0<Unit> function03 = function0;
        Function0<Unit> function04 = function02;
        Intrinsics.checkParameterIsNotNull(recyclerView, "$this$create");
        Intrinsics.checkParameterIsNotNull(tArr, FirebaseAnalytics.Param.ITEMS);
        Intrinsics.checkParameterIsNotNull(layoutManager2, "layoutMgr");
        Function3<? super View, ? super T, ? super Integer, Unit> function32 = function3;
        Intrinsics.checkParameterIsNotNull(function32, "onBindView");
        Function2<? super T, ? super Integer, Unit> function23 = function2;
        Intrinsics.checkParameterIsNotNull(function23, "itemClick");
        Function2<? super T, ? super Integer, Unit> function24 = function22;
        Intrinsics.checkParameterIsNotNull(function24, "itemLongClick");
        Intrinsics.checkParameterIsNotNull(function03, "onScrollTop");
        Intrinsics.checkParameterIsNotNull(function04, "onScrollBottom");
        recyclerView.setAdapter(new RecyclerAdapter(i2, tArr, i, function32, function23, function24));
        recyclerView.setLayoutManager(layoutManager2);
        recyclerView.addOnScrollListener(new ListExtensionsKt$create$13(function03, function04));
    }

    public static /* synthetic */ void create$default(RecyclerView recyclerView, int i, int i2, List list, RecyclerView.LayoutManager layoutManager, Function3 function3, Function2 function2, Function2 function22, Function0 function0, Function0 function02, int i3, Object obj) {
        int i4 = i3;
        create(recyclerView, (i4 & 1) != 0 ? 0 : i, i2, list, layoutManager, function3, (i4 & 32) != 0 ? ListExtensionsKt$create$14.INSTANCE : function2, (i4 & 64) != 0 ? ListExtensionsKt$create$15.INSTANCE : function22, (Function0<Unit>) (i4 & 128) != 0 ? ListExtensionsKt$create$16.INSTANCE : function0, (Function0<Unit>) (i4 & 256) != 0 ? ListExtensionsKt$create$17.INSTANCE : function02);
    }

    public static final <T> void create(RecyclerView recyclerView, int i, int i2, List<T> list, RecyclerView.LayoutManager layoutManager, Function3<? super View, ? super T, ? super Integer, Unit> function3, Function2<? super T, ? super Integer, Unit> function2, Function2<? super T, ? super Integer, Unit> function22, Function0<Unit> function0, Function0<Unit> function02) {
        RecyclerView recyclerView2 = recyclerView;
        RecyclerView.LayoutManager layoutManager2 = layoutManager;
        Function0<Unit> function03 = function0;
        Function0<Unit> function04 = function02;
        Intrinsics.checkParameterIsNotNull(recyclerView, "$this$create");
        Intrinsics.checkParameterIsNotNull(list, FirebaseAnalytics.Param.ITEMS);
        Intrinsics.checkParameterIsNotNull(layoutManager2, "layoutMgr");
        Function3<? super View, ? super T, ? super Integer, Unit> function32 = function3;
        Intrinsics.checkParameterIsNotNull(function32, "onBindView");
        Function2<? super T, ? super Integer, Unit> function23 = function2;
        Intrinsics.checkParameterIsNotNull(function23, "itemClick");
        Function2<? super T, ? super Integer, Unit> function24 = function22;
        Intrinsics.checkParameterIsNotNull(function24, "itemLongClick");
        Intrinsics.checkParameterIsNotNull(function03, "onScrollTop");
        Intrinsics.checkParameterIsNotNull(function04, "onScrollBottom");
        recyclerView.setAdapter(new RecyclerAdapter(i, i2, list, function32, function23, function24));
        recyclerView.setLayoutManager(layoutManager2);
        recyclerView.addOnScrollListener(new ListExtensionsKt$create$18(function03, function04));
    }

    public static final void onPageSelected(ViewPager viewPager, Function1<? super Integer, Unit> function1) {
        Intrinsics.checkParameterIsNotNull(viewPager, "$this$onPageSelected");
        Intrinsics.checkParameterIsNotNull(function1, "onPageSelected");
        viewPager.addOnPageChangeListener(new ListExtensionsKt$onPageSelected$1(function1));
    }
}
