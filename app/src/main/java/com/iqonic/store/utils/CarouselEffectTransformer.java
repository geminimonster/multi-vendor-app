package com.iqonic.store.utils;

import android.content.Context;
import android.view.View;
import androidx.core.view.ViewCompat;
import androidx.viewpager.widget.ViewPager;

public class CarouselEffectTransformer implements ViewPager.PageTransformer {
    private int maxTranslateOffsetX;
    private ViewPager viewPager;

    public CarouselEffectTransformer(Context context) {
        this.maxTranslateOffsetX = dp2px(context, 180.0f);
    }

    public void transformPage(View view, float f) {
        if (this.viewPager == null) {
            this.viewPager = (ViewPager) view.getParent();
        }
        float left = (((float) (((view.getLeft() - this.viewPager.getScrollX()) + (view.getMeasuredWidth() / 2)) - (this.viewPager.getMeasuredWidth() / 2))) * 0.38f) / ((float) this.viewPager.getMeasuredWidth());
        float abs = 1.0f - Math.abs(left);
        if (abs > 0.0f) {
            view.setScaleX(abs);
            view.setScaleY(abs);
            view.setTranslationX(((float) (-this.maxTranslateOffsetX)) * left);
        }
        ViewCompat.setElevation(view, abs);
    }

    private int dp2px(Context context, float f) {
        return (int) ((f * context.getResources().getDisplayMetrics().density) + 0.5f);
    }
}
