package com.iqonic.store.utils.extensions;

import android.content.DialogInterface;
import com.iqonic.store.AppBaseActivity;
import com.iqonic.store.models.BaseResponse;
import com.iqonic.store.models.RequestModel;
import com.store.proshop.R;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\n¢\u0006\u0002\b\u0006"}, d2 = {"<anonymous>", "", "dialog", "Landroid/content/DialogInterface;", "i", "", "invoke"}, k = 3, mv = {1, 1, 16})
/* compiled from: NetworkExtension.kt */
final class NetworkExtensionKt$removeCartItem$1 extends Lambda implements Function2<DialogInterface, Integer, Unit> {
    final /* synthetic */ Function1 $onApiSuccess;
    final /* synthetic */ RequestModel $requestModel;
    final /* synthetic */ AppBaseActivity $this_removeCartItem;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    NetworkExtensionKt$removeCartItem$1(AppBaseActivity appBaseActivity, RequestModel requestModel, Function1 function1) {
        super(2);
        this.$this_removeCartItem = appBaseActivity;
        this.$requestModel = requestModel;
        this.$onApiSuccess = function1;
    }

    public /* bridge */ /* synthetic */ Object invoke(Object obj, Object obj2) {
        invoke((DialogInterface) obj, ((Number) obj2).intValue());
        return Unit.INSTANCE;
    }

    public final void invoke(DialogInterface dialogInterface, int i) {
        Intrinsics.checkParameterIsNotNull(dialogInterface, "dialog");
        this.$this_removeCartItem.showProgress(true);
        if (ExtensionsKt.isNetworkAvailable()) {
            NetworkExtensionKt.getRestApiImpl$default((String) null, 1, (Object) null).removeCartItem(this.$requestModel, new Function1<BaseResponse, Unit>(this) {
                final /* synthetic */ NetworkExtensionKt$removeCartItem$1 this$0;

                {
                    this.this$0 = r1;
                }

                public /* bridge */ /* synthetic */ Object invoke(Object obj) {
                    invoke((BaseResponse) obj);
                    return Unit.INSTANCE;
                }

                public final void invoke(BaseResponse baseResponse) {
                    Intrinsics.checkParameterIsNotNull(baseResponse, "it");
                    this.this$0.$this_removeCartItem.showProgress(false);
                    String string = this.this$0.$this_removeCartItem.getString(R.string.success);
                    Intrinsics.checkExpressionValueIsNotNull(string, "getString(R.string.success)");
                    ExtensionsKt.snackBar$default(this.this$0.$this_removeCartItem, string, 0, 2, (Object) null);
                    this.this$0.$onApiSuccess.invoke(baseResponse);
                }
            }, new Function1<String, Unit>(this) {
                final /* synthetic */ NetworkExtensionKt$removeCartItem$1 this$0;

                {
                    this.this$0 = r1;
                }

                public /* bridge */ /* synthetic */ Object invoke(Object obj) {
                    invoke((String) obj);
                    return Unit.INSTANCE;
                }

                public final void invoke(String str) {
                    Intrinsics.checkParameterIsNotNull(str, "it");
                    this.this$0.$this_removeCartItem.showProgress(false);
                    ExtensionsKt.snackBarError(this.this$0.$this_removeCartItem, str);
                }
            });
            return;
        }
        this.$this_removeCartItem.showProgress(false);
        ExtensionsKt.noInternetSnackBar(this.$this_removeCartItem);
    }
}
