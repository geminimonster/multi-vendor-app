package com.iqonic.store.utils;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.LocaleList;
import com.iqonic.store.utils.extensions.AppExtensionsKt;
import java.util.LinkedHashSet;
import java.util.Locale;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u0000 \u00142\u00020\u0001:\u0001\u0014B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u0010\u0010\t\u001a\u00020\n2\u0006\u0010\u0005\u001a\u00020\u0006H\u0003J\u000e\u0010\u000b\u001a\u00020\u00032\u0006\u0010\f\u001a\u00020\u0003J\u0018\u0010\r\u001a\u00020\n2\u0006\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u0011H\u0003J\u0016\u0010\u0012\u001a\u00020\u00032\u0006\u0010\f\u001a\u00020\u00032\u0006\u0010\u0005\u001a\u00020\u0006J\u001a\u0010\u0013\u001a\u00020\u00032\u0006\u0010\u0002\u001a\u00020\u00032\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006H\u0002R\u0013\u0010\u0005\u001a\u0004\u0018\u00010\u00068F¢\u0006\u0006\u001a\u0004\b\u0007\u0010\b¨\u0006\u0015"}, d2 = {"Lcom/iqonic/store/utils/LocaleManager;", "", "context", "Landroid/content/Context;", "(Landroid/content/Context;)V", "language", "", "getLanguage", "()Ljava/lang/String;", "persistLanguage", "", "setLocale", "c", "setLocaleForApi24", "config", "Landroid/content/res/Configuration;", "target", "Ljava/util/Locale;", "setNewLocale", "updateResources", "Companion", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: LocaleManager.kt */
public final class LocaleManager {
    public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
    /* access modifiers changed from: private */
    public static final String LANGUAGE_ENGLISH = LANGUAGE_ENGLISH;
    private static final String LANGUAGE_KEY = LANGUAGE_KEY;
    /* access modifiers changed from: private */
    public static final String LANGUAGE_RUSSIAN = LANGUAGE_RUSSIAN;
    /* access modifiers changed from: private */
    public static final String LANGUAGE_UKRAINIAN = LANGUAGE_UKRAINIAN;

    public LocaleManager(Context context) {
        Intrinsics.checkParameterIsNotNull(context, "context");
    }

    public final String getLanguage() {
        return AppExtensionsKt.getSharedPrefInstance().getStringValue(LANGUAGE_KEY, LANGUAGE_ENGLISH);
    }

    public final Context setLocale(Context context) {
        Intrinsics.checkParameterIsNotNull(context, "c");
        return updateResources(context, getLanguage());
    }

    public final Context setNewLocale(Context context, String str) {
        Intrinsics.checkParameterIsNotNull(context, "c");
        Intrinsics.checkParameterIsNotNull(str, "language");
        persistLanguage(str);
        return updateResources(context, str);
    }

    private final void persistLanguage(String str) {
        AppExtensionsKt.getSharedPrefInstance().setValue(LANGUAGE_KEY, str);
    }

    private final Context updateResources(Context context, String str) {
        Locale locale = new Locale(str);
        Locale.setDefault(locale);
        Resources resources = context.getResources();
        Intrinsics.checkExpressionValueIsNotNull(resources, "res");
        Configuration configuration = new Configuration(resources.getConfiguration());
        if (Build.VERSION.SDK_INT >= 24) {
            setLocaleForApi24(configuration, locale);
            Context createConfigurationContext = context.createConfigurationContext(configuration);
            Intrinsics.checkExpressionValueIsNotNull(createConfigurationContext, "context.createConfigurationContext(config)");
            return createConfigurationContext;
        }
        configuration.setLocale(locale);
        Context createConfigurationContext2 = context.createConfigurationContext(configuration);
        Intrinsics.checkExpressionValueIsNotNull(createConfigurationContext2, "context.createConfigurationContext(config)");
        return createConfigurationContext2;
    }

    private final void setLocaleForApi24(Configuration configuration, Locale locale) {
        LinkedHashSet linkedHashSet = new LinkedHashSet();
        linkedHashSet.add(locale);
        LocaleList localeList = LocaleList.getDefault();
        Intrinsics.checkExpressionValueIsNotNull(localeList, "LocaleList.getDefault()");
        int size = localeList.size();
        for (int i = 0; i < size; i++) {
            linkedHashSet.add(localeList.get(i));
        }
        Object[] array = linkedHashSet.toArray(new Locale[0]);
        if (array != null) {
            Locale[] localeArr = (Locale[]) array;
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\b\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u000e\u0010\f\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000fR\u0014\u0010\u0003\u001a\u00020\u0004XD¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006R\u000e\u0010\u0007\u001a\u00020\u0004XD¢\u0006\u0002\n\u0000R\u0014\u0010\b\u001a\u00020\u0004XD¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\u0006R\u0014\u0010\n\u001a\u00020\u0004XD¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\u0006¨\u0006\u0010"}, d2 = {"Lcom/iqonic/store/utils/LocaleManager$Companion;", "", "()V", "LANGUAGE_ENGLISH", "", "getLANGUAGE_ENGLISH", "()Ljava/lang/String;", "LANGUAGE_KEY", "LANGUAGE_RUSSIAN", "getLANGUAGE_RUSSIAN", "LANGUAGE_UKRAINIAN", "getLANGUAGE_UKRAINIAN", "getLocale", "Ljava/util/Locale;", "res", "Landroid/content/res/Resources;", "app_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: LocaleManager.kt */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        public final String getLANGUAGE_ENGLISH() {
            return LocaleManager.LANGUAGE_ENGLISH;
        }

        public final String getLANGUAGE_UKRAINIAN() {
            return LocaleManager.LANGUAGE_UKRAINIAN;
        }

        public final String getLANGUAGE_RUSSIAN() {
            return LocaleManager.LANGUAGE_RUSSIAN;
        }

        public final Locale getLocale(Resources resources) {
            Locale locale;
            String str;
            Intrinsics.checkParameterIsNotNull(resources, "res");
            Configuration configuration = resources.getConfiguration();
            if (Build.VERSION.SDK_INT >= 24) {
                Intrinsics.checkExpressionValueIsNotNull(configuration, "config");
                locale = configuration.getLocales().get(0);
                str = "config.locales.get(0)";
            } else {
                locale = configuration.locale;
                str = "config.locale";
            }
            Intrinsics.checkExpressionValueIsNotNull(locale, str);
            return locale;
        }
    }
}
