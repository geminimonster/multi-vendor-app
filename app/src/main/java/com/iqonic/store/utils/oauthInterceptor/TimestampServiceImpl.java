package com.iqonic.store.utils.oauthInterceptor;

import java.util.Random;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\t\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0003\u0018\u00002\u00020\u0001:\u0001\u0012B\u0005¢\u0006\u0002\u0010\u0002J\u0015\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0007\u001a\u00020\bH\u0000¢\u0006\u0002\b\u0011R\u0014\u0010\u0003\u001a\u00020\u00048VX\u0004¢\u0006\u0006\u001a\u0004\b\u0005\u0010\u0006R\u000e\u0010\u0007\u001a\u00020\bX\u000e¢\u0006\u0002\n\u0000R\u0014\u0010\t\u001a\u00020\u00048VX\u0004¢\u0006\u0006\u001a\u0004\b\n\u0010\u0006R\u0014\u0010\u000b\u001a\u00020\f8BX\u0004¢\u0006\u0006\u001a\u0004\b\r\u0010\u000e¨\u0006\u0013"}, d2 = {"Lcom/iqonic/store/utils/oauthInterceptor/TimestampServiceImpl;", "Lcom/iqonic/store/utils/oauthInterceptor/TimestampService;", "()V", "nonce", "", "getNonce", "()Ljava/lang/String;", "timer", "Lcom/iqonic/store/utils/oauthInterceptor/TimestampServiceImpl$Timer;", "timestampInSeconds", "getTimestampInSeconds", "ts", "", "getTs", "()J", "setTimer", "", "setTimer$app_release", "Timer", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: TimestampServiceImpl.kt */
public final class TimestampServiceImpl implements TimestampService {
    private Timer timer = new Timer();

    public String getNonce() {
        return String.valueOf(getTs() + ((long) this.timer.getRandomInteger()));
    }

    public String getTimestampInSeconds() {
        return String.valueOf(getTs());
    }

    private final long getTs() {
        return this.timer.getMilis() / ((long) 1000);
    }

    public final void setTimer$app_release(Timer timer2) {
        Intrinsics.checkParameterIsNotNull(timer2, "timer");
        this.timer = timer2;
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\b\u0000\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002R\u0011\u0010\u0003\u001a\u00020\u00048F¢\u0006\u0006\u001a\u0004\b\u0005\u0010\u0006R\u000e\u0010\u0007\u001a\u00020\bX\u0004¢\u0006\u0002\n\u0000R\u0011\u0010\t\u001a\u00020\n8F¢\u0006\u0006\u001a\u0004\b\u000b\u0010\f¨\u0006\r"}, d2 = {"Lcom/iqonic/store/utils/oauthInterceptor/TimestampServiceImpl$Timer;", "", "()V", "milis", "", "getMilis", "()J", "rand", "Ljava/util/Random;", "randomInteger", "", "getRandomInteger", "()I", "app_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: TimestampServiceImpl.kt */
    public static final class Timer {
        private final Random rand = new Random();

        public final long getMilis() {
            return System.currentTimeMillis();
        }

        public final int getRandomInteger() {
            return this.rand.nextInt();
        }
    }
}
