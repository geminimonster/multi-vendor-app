package com.iqonic.store.utils.scratch;

import android.graphics.Bitmap;
import java.nio.ByteBuffer;

public class BitmapUtils {
    public static float compareEquivalance(Bitmap bitmap, Bitmap bitmap2) {
        if (bitmap == null || bitmap2 == null || bitmap.getWidth() != bitmap2.getWidth() || bitmap.getHeight() != bitmap2.getHeight()) {
            return 0.0f;
        }
        ByteBuffer allocate = ByteBuffer.allocate(bitmap.getHeight() * bitmap.getRowBytes());
        bitmap.copyPixelsToBuffer(allocate);
        ByteBuffer allocate2 = ByteBuffer.allocate(bitmap2.getHeight() * bitmap2.getRowBytes());
        bitmap2.copyPixelsToBuffer(allocate2);
        byte[] array = allocate.array();
        byte[] array2 = allocate2.array();
        int length = array.length;
        int i = 0;
        for (int i2 = 0; i2 < length; i2++) {
            if (array[i2] == array2[i2]) {
                i++;
            }
        }
        return ((float) i) / ((float) length);
    }

    public static float getTransparentPixelPercent(Bitmap bitmap) {
        if (bitmap == null) {
            return 0.0f;
        }
        ByteBuffer allocate = ByteBuffer.allocate(bitmap.getHeight() * bitmap.getRowBytes());
        bitmap.copyPixelsToBuffer(allocate);
        int i = 0;
        for (byte b : allocate.array()) {
            if (b == 0) {
                i++;
            }
        }
        return ((float) i) / ((float) r0);
    }
}
