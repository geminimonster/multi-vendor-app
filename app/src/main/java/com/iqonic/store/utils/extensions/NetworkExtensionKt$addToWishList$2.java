package com.iqonic.store.utils.extensions;

import com.iqonic.store.AppBaseActivity;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n¢\u0006\u0002\b\u0004"}, d2 = {"<anonymous>", "", "it", "", "invoke"}, k = 3, mv = {1, 1, 16})
/* compiled from: NetworkExtension.kt */
final class NetworkExtensionKt$addToWishList$2 extends Lambda implements Function1<String, Unit> {
    final /* synthetic */ Function1 $onSuccess;
    final /* synthetic */ AppBaseActivity $this_addToWishList;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    NetworkExtensionKt$addToWishList$2(AppBaseActivity appBaseActivity, Function1 function1) {
        super(1);
        this.$this_addToWishList = appBaseActivity;
        this.$onSuccess = function1;
    }

    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        invoke((String) obj);
        return Unit.INSTANCE;
    }

    public final void invoke(String str) {
        Intrinsics.checkParameterIsNotNull(str, "it");
        ExtensionsKt.snackBarError(this.$this_addToWishList, str);
        NetworkExtensionKt.fetchAndStoreWishListData(this.$this_addToWishList, AnonymousClass1.INSTANCE);
        this.$onSuccess.invoke(false);
    }
}
