package com.iqonic.store.utils.scratch;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.Shader;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.util.AttributeSet;
import android.view.MotionEvent;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.core.content.ContextCompat;
import androidx.core.internal.view.SupportMenu;
import com.store.proshop.R;

public class ScratchTextView extends AppCompatTextView {
    public static final float STROKE_WIDTH = 12.0f;
    private static final float TOUCH_TOLERANCE = 4.0f;
    /* access modifiers changed from: private */
    public IRevealListener mRevealListener;
    private Canvas mCanvas;
    /* access modifiers changed from: private */
    public float mRevealPercent;
    /* access modifiers changed from: private */
    public Bitmap mScratchBitmap;
    private Path mErasePath;
    private Paint mGradientBgPaint;
    private Paint mBitmapPaint;
    private BitmapDrawable mDrawable;
    private Paint mErasePaint;
    private int mThreadCount = 0;
    private Path mTouchPath;
    private float mX;
    private float mY;

    public ScratchTextView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        init();
    }

    public ScratchTextView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        init();
    }

    public ScratchTextView(Context context) {
        super(context);
        init();
    }

    static /* synthetic */ int access$110(ScratchTextView scratchTextView) {
        int i = scratchTextView.mThreadCount;
        scratchTextView.mThreadCount = i - 1;
        return i;
    }

    private static int[] getTextDimens(String str, Paint paint) {
        int length = str.length();
        Rect rect = new Rect();
        paint.getTextBounds(str, 0, length, rect);
        return new int[]{rect.left + rect.width(), rect.bottom + rect.height()};
    }

    public void setStrokeWidth(int i) {
        this.mErasePaint.setStrokeWidth(((float) i) * 12.0f);
    }

    private void init() {
        this.mTouchPath = new Path();
        Paint paint = new Paint();
        this.mErasePaint = paint;
        paint.setAntiAlias(true);
        this.mErasePaint.setDither(true);
        this.mErasePaint.setColor(SupportMenu.CATEGORY_MASK);
        this.mErasePaint.setStyle(Paint.Style.STROKE);
        this.mErasePaint.setStrokeJoin(Paint.Join.BEVEL);
        this.mErasePaint.setStrokeCap(Paint.Cap.ROUND);
        this.mErasePaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
        setStrokeWidth(6);
        this.mGradientBgPaint = new Paint();
        this.mErasePath = new Path();
        this.mBitmapPaint = new Paint(4);
        BitmapDrawable bitmapDrawable = new BitmapDrawable(getResources(), BitmapFactory.decodeResource(getResources(), R.drawable.ic_scratch_pattern));
        this.mDrawable = bitmapDrawable;
        bitmapDrawable.setTileModeXY(Shader.TileMode.REPEAT, Shader.TileMode.REPEAT);
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        super.onSizeChanged(i, i2, i3, i4);
        this.mScratchBitmap = Bitmap.createBitmap(i, i2, Bitmap.Config.ARGB_8888);
        this.mCanvas = new Canvas(this.mScratchBitmap);
        Rect rect = new Rect(0, 0, this.mScratchBitmap.getWidth(), this.mScratchBitmap.getHeight());
        this.mDrawable.setBounds(rect);
        this.mGradientBgPaint.setShader(new LinearGradient(0.0f, 0.0f, 0.0f, (float) getHeight(), ContextCompat.getColor(getContext(), R.color.scratch_start_gradient), ContextCompat.getColor(getContext(), R.color.scratch_end_gradient), Shader.TileMode.MIRROR));
        this.mCanvas.drawRect(rect, this.mGradientBgPaint);
        this.mDrawable.draw(this.mCanvas);
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawBitmap(this.mScratchBitmap, 0.0f, 0.0f, this.mBitmapPaint);
        canvas.drawPath(this.mErasePath, this.mErasePaint);
    }

    private void touch_start(float f, float f2) {
        this.mErasePath.reset();
        this.mErasePath.moveTo(f, f2);
        this.mX = f;
        this.mY = f2;
    }

    private void touch_move(float f, float f2) {
        float abs = Math.abs(f - this.mX);
        float abs2 = Math.abs(f2 - this.mY);
        if (abs >= 4.0f || abs2 >= 4.0f) {
            Path path = this.mErasePath;
            float f3 = this.mX;
            float f4 = this.mY;
            path.quadTo(f3, f4, (f + f3) / 2.0f, (f2 + f4) / 2.0f);
            this.mX = f;
            this.mY = f2;
            drawPath();
        }
        this.mTouchPath.reset();
        this.mTouchPath.addCircle(this.mX, this.mY, 30.0f, Path.Direction.CW);
    }

    private void drawPath() {
        this.mErasePath.lineTo(this.mX, this.mY);
        this.mCanvas.drawPath(this.mErasePath, this.mErasePaint);
        this.mTouchPath.reset();
        this.mErasePath.reset();
        this.mErasePath.moveTo(this.mX, this.mY);
        checkRevealed();
    }

    public void reveal() {
        int[] textBounds = getTextBounds(1.5f);
        int i = textBounds[0];
        int i2 = textBounds[1];
        int i3 = textBounds[2];
        int i4 = textBounds[3];
        Paint paint = new Paint();
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
        this.mCanvas.drawRect((float) i, (float) i2, (float) i3, (float) i4, paint);
        checkRevealed();
        invalidate();
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        float x = motionEvent.getX();
        float y = motionEvent.getY();
        int action = motionEvent.getAction();
        if (action == 0) {
            touch_start(x, y);
            invalidate();
        } else if (action == 1) {
            drawPath();
            invalidate();
        } else if (action == 2) {
            touch_move(x, y);
            invalidate();
        }
        return true;
    }

    public int getColor() {
        return this.mErasePaint.getColor();
    }

    public void setRevealListener(IRevealListener iRevealListener) {
        this.mRevealListener = iRevealListener;
    }

    public boolean isRevealed() {
        return this.mRevealPercent == 1.0f;
    }

    private void checkRevealed() {
        if (!isRevealed() && this.mRevealListener != null) {
            int[] textBounds = getTextBounds();
            int i = textBounds[0];
            int i2 = textBounds[1];
            int i3 = textBounds[2] - i;
            int i4 = textBounds[3] - i2;
            int i5 = this.mThreadCount;
            if (i5 <= 1) {
                this.mThreadCount = i5 + 1;
                new AsyncTask<Integer, Void, Float>() {
                    /* access modifiers changed from: protected */
                    public Float doInBackground(Integer... numArr) {
                        try {
                            return Float.valueOf(BitmapUtils.getTransparentPixelPercent(Bitmap.createBitmap(ScratchTextView.this.mScratchBitmap, numArr[0].intValue(), numArr[1].intValue(), numArr[2].intValue(), numArr[3].intValue())));
                        } finally {
                            ScratchTextView.access$110(ScratchTextView.this);
                        }
                    }

                    public void onPostExecute(Float f) {
                        if (!ScratchTextView.this.isRevealed()) {
                            float access$200 = ScratchTextView.this.mRevealPercent;
                            float unused = ScratchTextView.this.mRevealPercent = f.floatValue();
                            if (access$200 != f.floatValue()) {
                                ScratchTextView.this.mRevealListener.onRevealPercentChangedListener(ScratchTextView.this, f.floatValue());
                            }
                            if (ScratchTextView.this.isRevealed()) {
                                ScratchTextView.this.mRevealListener.onRevealed(ScratchTextView.this);
                            }
                        }
                    }
                }.execute(new Integer[]{Integer.valueOf(i), Integer.valueOf(i2), Integer.valueOf(i3), Integer.valueOf(i4)});
            }
        }
    }

    private int[] getTextBounds() {
        return getTextBounds(1.0f);
    }

    private int[] getTextBounds(float f) {
        int paddingLeft = getPaddingLeft();
        int paddingTop = getPaddingTop();
        int paddingRight = getPaddingRight();
        int paddingBottom = getPaddingBottom();
        int width = getWidth();
        int height = getHeight();
        int i = width / 2;
        int i2 = height / 2;
        int[] textDimens = getTextDimens(getText().toString(), getPaint());
        int i3 = textDimens[0];
        int i4 = textDimens[1];
        int lineCount = getLineCount();
        int i5 = i4 * lineCount;
        int i6 = i3 / lineCount;
        int i7 = i5 > height ? height - (paddingBottom + paddingTop) : (int) (((float) i5) * f);
        int i8 = i6 > width ? width - (paddingLeft + paddingRight) : (int) (((float) i6) * f);
        int gravity = getGravity();
        if ((gravity & 3) != 3) {
            paddingLeft = (gravity & 5) == 5 ? (width - paddingRight) - i8 : (gravity & 1) == 1 ? i - (i8 / 2) : 0;
        }
        if ((gravity & 48) != 48) {
            if ((gravity & 80) == 80) {
                paddingTop = (height - paddingBottom) - i7;
            } else {
                paddingTop = (gravity & 16) == 16 ? i2 - (i7 / 2) : 0;
            }
        }
        return new int[]{paddingLeft, paddingTop, paddingLeft + i8, paddingTop + i7};
    }

    public interface IRevealListener {
        void onRevealPercentChangedListener(ScratchTextView scratchTextView, float f);

        void onRevealed(ScratchTextView scratchTextView);
    }
}
