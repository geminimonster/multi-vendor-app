package com.iqonic.store.utils;

import android.animation.ValueAnimator;
import android.view.ViewGroup;
import android.widget.TextView;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n¢\u0006\u0002\b\u0005"}, d2 = {"<anonymous>", "", "animation", "Landroid/animation/ValueAnimator;", "kotlin.jvm.PlatformType", "onAnimationUpdate"}, k = 3, mv = {1, 1, 16})
/* compiled from: ExpandableTextView.kt */
final class ExpandableTextView$expand$1 implements ValueAnimator.AnimatorUpdateListener {
    final /* synthetic */ ExpandableTextView this$0;

    ExpandableTextView$expand$1(ExpandableTextView expandableTextView) {
        this.this$0 = expandableTextView;
    }

    public final void onAnimationUpdate(ValueAnimator valueAnimator) {
        TextView content = this.this$0.getContent();
        if (content == null) {
            Intrinsics.throwNpe();
        }
        ViewGroup.LayoutParams layoutParams = content.getLayoutParams();
        Intrinsics.checkExpressionValueIsNotNull(valueAnimator, "animation");
        Object animatedValue = valueAnimator.getAnimatedValue();
        if (animatedValue != null) {
            layoutParams.height = ((Integer) animatedValue).intValue() + 70;
            TextView content2 = this.this$0.getContent();
            if (content2 == null) {
                Intrinsics.throwNpe();
            }
            content2.setLayoutParams(layoutParams);
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type kotlin.Int");
    }
}
