package com.iqonic.store.utils.extensions;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import androidx.core.content.ContextCompat;
import com.facebook.internal.NativeProtocol;
import com.google.firebase.analytics.FirebaseAnalytics;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000J\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\u0011\n\u0002\b\u0003\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u001a\u0012\u0010\u0000\u001a\u00020\u0001*\u00020\u00022\u0006\u0010\u0003\u001a\u00020\u0004\u001a\u0014\u0010\u0005\u001a\u00020\u0004*\u00020\u00022\b\b\u0002\u0010\u0006\u001a\u00020\u0004\u001a\n\u0010\u0007\u001a\u00020\b*\u00020\u0002\u001a\n\u0010\t\u001a\u00020\n*\u00020\u0002\u001a\n\u0010\u000b\u001a\u00020\f*\u00020\u0002\u001a\u001d\u0010\r\u001a\u00020\f*\u00020\u00022\f\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\u00040\u000f¢\u0006\u0002\u0010\u0010\u001a@\u0010\u0011\u001a\u00020\u0001\"\n\b\u0000\u0010\u0012\u0018\u0001*\u00020\u0013*\u00020\u00022\n\b\u0002\u0010\u0014\u001a\u0004\u0018\u00010\u00152\u0019\b\n\u0010\u0016\u001a\u0013\u0012\u0004\u0012\u00020\u0018\u0012\u0004\u0012\u00020\u00010\u0017¢\u0006\u0002\b\u0019H\b¨\u0006\u001a"}, d2 = {"dialNumber", "", "Landroid/content/Context;", "number", "", "getAppVersionName", "pName", "getConnectivityManager", "Landroid/net/ConnectivityManager;", "getLocationManager", "Landroid/location/LocationManager;", "isGPSEnable", "", "isPermissionGranted", "permissions", "", "(Landroid/content/Context;[Ljava/lang/String;)Z", "launchActivity", "T", "", "options", "Landroid/os/Bundle;", "init", "Lkotlin/Function1;", "Landroid/content/Intent;", "Lkotlin/ExtensionFunctionType;", "app_release"}, k = 2, mv = {1, 1, 16})
/* compiled from: ContextExtensions.kt */
public final class ContextExtensionsKt {
    public static final boolean isGPSEnable(Context context) {
        Intrinsics.checkParameterIsNotNull(context, "$this$isGPSEnable");
        return getLocationManager(context).isProviderEnabled("gps");
    }

    public static final LocationManager getLocationManager(Context context) {
        Intrinsics.checkParameterIsNotNull(context, "$this$getLocationManager");
        Object systemService = context.getSystemService(FirebaseAnalytics.Param.LOCATION);
        if (systemService != null) {
            return (LocationManager) systemService;
        }
        throw new TypeCastException("null cannot be cast to non-null type android.location.LocationManager");
    }

    public static final ConnectivityManager getConnectivityManager(Context context) {
        Intrinsics.checkParameterIsNotNull(context, "$this$getConnectivityManager");
        Object systemService = context.getSystemService("connectivity");
        if (systemService != null) {
            return (ConnectivityManager) systemService;
        }
        throw new TypeCastException("null cannot be cast to non-null type android.net.ConnectivityManager");
    }

    public static /* synthetic */ void launchActivity$default(Context context, Bundle bundle, Function1 function1, int i, Object obj) {
        if ((i & 1) != 0) {
            bundle = null;
        }
        if ((i & 2) != 0) {
            function1 = ContextExtensionsKt$launchActivity$1.INSTANCE;
        }
        Intrinsics.checkParameterIsNotNull(context, "$this$launchActivity");
        Intrinsics.checkParameterIsNotNull(function1, "init");
        Intrinsics.reifiedOperationMarker(4, "T");
        Intent intent = new Intent(context, Object.class);
        function1.invoke(intent);
        if (Build.VERSION.SDK_INT >= 16) {
            context.startActivity(intent, bundle);
        } else {
            context.startActivity(intent);
        }
    }

    public static final String getAppVersionName(Context context, String str) throws PackageManager.NameNotFoundException {
        Intrinsics.checkParameterIsNotNull(context, "$this$getAppVersionName");
        Intrinsics.checkParameterIsNotNull(str, "pName");
        String str2 = context.getPackageManager().getPackageInfo(str, 0).versionName;
        Intrinsics.checkExpressionValueIsNotNull(str2, "packageManager.getPackag…nfo(pName, 0).versionName");
        return str2;
    }

    public static /* synthetic */ String getAppVersionName$default(Context context, String str, int i, Object obj) throws PackageManager.NameNotFoundException {
        if ((i & 1) != 0) {
            str = context.getPackageName();
            Intrinsics.checkExpressionValueIsNotNull(str, "packageName");
        }
        return getAppVersionName(context, str);
    }

    public static final void dialNumber(Context context, String str) {
        Intrinsics.checkParameterIsNotNull(context, "$this$dialNumber");
        Intrinsics.checkParameterIsNotNull(str, "number");
        context.startActivity(new Intent("android.intent.action.DIAL", Uri.parse("tel:" + str)));
    }

    public static final /* synthetic */ <T> void launchActivity(Context context, Bundle bundle, Function1<? super Intent, Unit> function1) {
        Intrinsics.checkParameterIsNotNull(context, "$this$launchActivity");
        Intrinsics.checkParameterIsNotNull(function1, "init");
        Intrinsics.reifiedOperationMarker(4, "T");
        Intent intent = new Intent(context, Object.class);
        function1.invoke(intent);
        if (Build.VERSION.SDK_INT >= 16) {
            context.startActivity(intent, bundle);
        } else {
            context.startActivity(intent);
        }
    }

    public static final boolean isPermissionGranted(Context context, String[] strArr) {
        Intrinsics.checkParameterIsNotNull(context, "$this$isPermissionGranted");
        Intrinsics.checkParameterIsNotNull(strArr, NativeProtocol.RESULT_ARGS_PERMISSIONS);
        for (String checkSelfPermission : strArr) {
            if (ContextCompat.checkSelfPermission(context, checkSelfPermission) != 0) {
                return false;
            }
        }
        return true;
    }
}
