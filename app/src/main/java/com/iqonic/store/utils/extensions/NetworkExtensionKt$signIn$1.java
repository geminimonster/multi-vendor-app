package com.iqonic.store.utils.extensions;

import com.iqonic.store.AppBaseActivity;
import com.iqonic.store.models.loginResponse;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n¢\u0006\u0002\b\u0004"}, d2 = {"<anonymous>", "", "it", "Lcom/iqonic/store/models/loginResponse;", "invoke"}, k = 3, mv = {1, 1, 16})
/* compiled from: NetworkExtension.kt */
final class NetworkExtensionKt$signIn$1 extends Lambda implements Function1<loginResponse, Unit> {
    final /* synthetic */ Function1 $onApiSuccess;
    final /* synthetic */ Function1 $onError;
    final /* synthetic */ String $password;
    final /* synthetic */ AppBaseActivity $this_signIn;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    NetworkExtensionKt$signIn$1(AppBaseActivity appBaseActivity, String str, Function1 function1, Function1 function12) {
        super(1);
        this.$this_signIn = appBaseActivity;
        this.$password = str;
        this.$onError = function1;
        this.$onApiSuccess = function12;
    }

    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        invoke((loginResponse) obj);
        return Unit.INSTANCE;
    }

    public final void invoke(loginResponse loginresponse) {
        Intrinsics.checkParameterIsNotNull(loginresponse, "it");
        this.$this_signIn.showProgress(false);
        NetworkExtensionKt.saveLoginResponse(this.$this_signIn, loginresponse, false, this.$password, this.$onError);
        this.$onApiSuccess.invoke(loginresponse);
    }
}
