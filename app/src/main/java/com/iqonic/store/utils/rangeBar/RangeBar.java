package com.iqonic.store.utils.rangeBar;

import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import androidx.core.view.ViewCompat;
import com.iqonic.store.R;
import java.util.ArrayList;
import java.util.HashMap;

public class RangeBar extends View {
    public static final float DEFAULT_MAX_PIN_FONT_SP = 24.0f;
    public static final float DEFAULT_MIN_PIN_FONT_SP = 8.0f;
    public static final float DEFAULT_TICK_LABEL_FONT_SP = 4.0f;
    private static final int DEFAULT_BAR_COLOR = -3355444;
    private static final float DEFAULT_BAR_PADDING_BOTTOM_DP = 24.0f;
    private static final float DEFAULT_BAR_WEIGHT_DP = 2.0f;
    private static final float DEFAULT_CIRCLE_BOUNDARY_SIZE_DP = 0.0f;
    private static final float DEFAULT_CIRCLE_SIZE_DP = 5.0f;
    private static final int DEFAULT_CONNECTING_LINE_COLOR = -12627531;
    private static final float DEFAULT_CONNECTING_LINE_WEIGHT_DP = 4.0f;
    private static final float DEFAULT_EXPANDED_PIN_RADIUS_DP = 12.0f;
    private static final int DEFAULT_PIN_COLOR = -12627531;
    private static final float DEFAULT_PIN_PADDING_DP = 16.0f;
    private static final int DEFAULT_TEXT_COLOR = -1;
    private static final int DEFAULT_TICK_COLOR = -16777216;
    private static final float DEFAULT_TICK_END = 10.0f;
    private static final float DEFAULT_TICK_HEIGHT_DP = 1.0f;
    private static final float DEFAULT_TICK_INTERVAL = 1.0f;
    private static final String DEFAULT_TICK_LABEL = "";
    private static final int DEFAULT_TICK_LABEL_COLOR = -3355444;
    private static final int DEFAULT_TICK_LABEL_SELECTED_COLOR = -16777216;
    private static final float DEFAULT_TICK_START = 0.0f;
    private static final String TAG = "RangeBar";
    private final DisplayMetrics mDisplayMetrices;
    /* access modifiers changed from: private */
    public float mPinPadding;
    /* access modifiers changed from: private */
    public float mThumbRadiusDP = 12.0f;
    private boolean drawTicks;
    private int mActiveBarColor;
    private int mActiveCircleBoundaryColor;
    private int mActiveCircleColor;
    private int mActiveCircleColorLeft;
    private int mActiveCircleColorRight;
    private int mActiveConnectingLineColor;
    private int mActiveTickLabelColor;
    private int mActiveTickLabelSelectedColor;
    private ArrayList<Integer> mActiveConnectingLineColors;
    private ArrayList<Integer> mActiveTickColors;
    private int mActiveTickDefaultColor;
    private boolean mArePinsTemporary;
    private Bar mBar;
    private int mBarColor = -3355444;
    private float mBarPaddingBottom;
    private float mBarWeight = DEFAULT_BAR_WEIGHT_DP;
    private int mCircleColorLeft;
    private int mCircleColorRight;
    private float mCircleSize = DEFAULT_CIRCLE_SIZE_DP;
    private ConnectingLine mConnectingLine;
    private int mCircleBoundaryColor = -12627531;
    private float mCircleBoundarySize = 0.0f;
    private int mCircleColor = -12627531;
    private ArrayList<Integer> mConnectingLineColors = new ArrayList<>();
    private int mDiffX;
    private int mDiffY;
    private float mConnectingLineWeight = 4.0f;
    private int mDefaultHeight;
    private int mDefaultWidth;
    private boolean mDragging;
    private float mExpandedPinRadius = 12.0f;
    private boolean mFirstSetTickCount = true;
    private IRangeBarFormatter mFormatter;
    private boolean mIsBarRounded = false;
    private float mLastX;
    private float mLastY;
    private boolean mIsInScrollingContainer;
    private boolean mIsRangeBar;
    private int mLeftIndex;
    private PinView mLeftThumb;
    private OnRangeBarChangeListener mListener;
    private float mMaxPinFont = 24.0f;
    private float mMinPinFont = 8.0f;
    private boolean mOnlyOnDrag;
    private int mPinColor = -12627531;
    private PinTextFormatter mPinTextFormatter;
    private OnRangeBarTextListener mPinTextListener;
    private int mRightIndex;
    private PinView mRightThumb;
    private int mTextColor = -1;
    private CharSequence[] mTickBottomLabels;
    private ArrayList<Integer> mTickColors = new ArrayList<>();
    private int mTickCount;
    private int mTickDefaultColor = ViewCompat.MEASURED_STATE_MASK;
    private String mTickDefaultLabel = "";
    private float mTickEnd = DEFAULT_TICK_END;
    private float mTickHeight = 1.0f;
    private float mTickInterval = 1.0f;
    private int mTickLabelColor = -3355444;
    private int mTickLabelSelectedColor = ViewCompat.MEASURED_STATE_MASK;
    private float mTickLabelSize = 4.0f;
    private HashMap<Float, String> mTickMap;
    private float mTickStart = 0.0f;
    private CharSequence[] mTickTopLabels;

    public RangeBar(Context context) {
        super(context);
        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        this.mDisplayMetrices = displayMetrics;
        this.mDefaultWidth = (int) TypedValue.applyDimension(1, 250.0f, displayMetrics);
        this.mDefaultHeight = (int) TypedValue.applyDimension(1, 75.0f, this.mDisplayMetrices);
        this.mTickCount = ((int) ((this.mTickEnd - this.mTickStart) / this.mTickInterval)) + 1;
        this.mIsRangeBar = true;
        this.mPinPadding = DEFAULT_PIN_PADDING_DP;
        this.mBarPaddingBottom = 24.0f;
        this.mActiveConnectingLineColors = new ArrayList<>();
        this.mActiveTickColors = new ArrayList<>();
        this.drawTicks = true;
        this.mArePinsTemporary = true;
        this.mOnlyOnDrag = false;
        this.mDragging = false;
        this.mIsInScrollingContainer = false;
        this.mPinTextFormatter = new PinTextFormatter() {
            public String getText(String str) {
                return str.length() > 4 ? str.substring(0, 4) : str;
            }
        };
    }

    public RangeBar(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        this.mDisplayMetrices = displayMetrics;
        this.mDefaultWidth = (int) TypedValue.applyDimension(1, 250.0f, displayMetrics);
        this.mDefaultHeight = (int) TypedValue.applyDimension(1, 75.0f, this.mDisplayMetrices);
        this.mTickCount = ((int) ((this.mTickEnd - this.mTickStart) / this.mTickInterval)) + 1;
        this.mIsRangeBar = true;
        this.mPinPadding = DEFAULT_PIN_PADDING_DP;
        this.mBarPaddingBottom = 24.0f;
        this.mActiveConnectingLineColors = new ArrayList<>();
        this.mActiveTickColors = new ArrayList<>();
        this.drawTicks = true;
        this.mArePinsTemporary = true;
        this.mOnlyOnDrag = false;
        this.mDragging = false;
        this.mIsInScrollingContainer = false;
        this.mPinTextFormatter = new PinTextFormatter() {
            public String getText(String str) {
                return str.length() > 4 ? str.substring(0, 4) : str;
            }
        };
        rangeBarInit(context, attributeSet);
    }

    public RangeBar(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        this.mDisplayMetrices = displayMetrics;
        this.mDefaultWidth = (int) TypedValue.applyDimension(1, 250.0f, displayMetrics);
        this.mDefaultHeight = (int) TypedValue.applyDimension(1, 75.0f, this.mDisplayMetrices);
        this.mTickCount = ((int) ((this.mTickEnd - this.mTickStart) / this.mTickInterval)) + 1;
        this.mIsRangeBar = true;
        this.mPinPadding = DEFAULT_PIN_PADDING_DP;
        this.mBarPaddingBottom = 24.0f;
        this.mActiveConnectingLineColors = new ArrayList<>();
        this.mActiveTickColors = new ArrayList<>();
        this.drawTicks = true;
        this.mArePinsTemporary = true;
        this.mOnlyOnDrag = false;
        this.mDragging = false;
        this.mIsInScrollingContainer = false;
        this.mPinTextFormatter = new PinTextFormatter() {
            public String getText(String str) {
                return str.length() > 4 ? str.substring(0, 4) : str;
            }
        };
        rangeBarInit(context, attributeSet);
    }

    private boolean isValidTickCount(int i) {
        return i > 1;
    }

    public Parcelable onSaveInstanceState() {
        Bundle bundle = new Bundle();
        bundle.putParcelable("instanceState", super.onSaveInstanceState());
        bundle.putInt("TICK_COUNT", this.mTickCount);
        bundle.putFloat("TICK_START", this.mTickStart);
        bundle.putFloat("TICK_END", this.mTickEnd);
        bundle.putFloat("TICK_INTERVAL", this.mTickInterval);
        bundle.putInt("TICK_COLOR", this.mTickDefaultColor);
        bundle.putIntegerArrayList("TICK_COLORS", this.mTickColors);
        bundle.putInt("TICK_LABEL_COLOR", this.mTickLabelColor);
        bundle.putInt("TICK_LABEL_SELECTED_COLOR", this.mTickLabelSelectedColor);
        bundle.putCharSequenceArray("TICK_TOP_LABELS", this.mTickTopLabels);
        bundle.putCharSequenceArray("TICK_BOTTOM_LABELS", this.mTickBottomLabels);
        bundle.putString("TICK_DEFAULT_LABEL", this.mTickDefaultLabel);
        bundle.putFloat("TICK_HEIGHT_DP", this.mTickHeight);
        bundle.putFloat("BAR_WEIGHT", this.mBarWeight);
        bundle.putBoolean("BAR_ROUNDED", this.mIsBarRounded);
        bundle.putInt("BAR_COLOR", this.mBarColor);
        bundle.putFloat("CONNECTING_LINE_WEIGHT", this.mConnectingLineWeight);
        bundle.putIntegerArrayList("CONNECTING_LINE_COLOR", this.mConnectingLineColors);
        bundle.putFloat("CIRCLE_SIZE", this.mCircleSize);
        bundle.putInt("CIRCLE_COLOR", this.mCircleColor);
        bundle.putInt("CIRCLE_COLOR_LEFT", this.mCircleColorLeft);
        bundle.putInt("CIRCLE_COLOR_RIGHT", this.mCircleColorRight);
        bundle.putInt("CIRCLE_BOUNDARY_COLOR", this.mCircleBoundaryColor);
        bundle.putFloat("CIRCLE_BOUNDARY_WIDTH", this.mCircleBoundarySize);
        bundle.putFloat("THUMB_RADIUS_DP", this.mThumbRadiusDP);
        bundle.putFloat("EXPANDED_PIN_RADIUS_DP", this.mExpandedPinRadius);
        bundle.putFloat("PIN_PADDING", this.mPinPadding);
        bundle.putFloat("BAR_PADDING_BOTTOM", this.mBarPaddingBottom);
        bundle.putBoolean("IS_RANGE_BAR", this.mIsRangeBar);
        bundle.putBoolean("IS_ONLY_ON_DRAG", this.mOnlyOnDrag);
        bundle.putBoolean("ARE_PINS_TEMPORARY", this.mArePinsTemporary);
        bundle.putInt("LEFT_INDEX", this.mLeftIndex);
        bundle.putInt("RIGHT_INDEX", this.mRightIndex);
        bundle.putBoolean("FIRST_SET_TICK_COUNT", this.mFirstSetTickCount);
        bundle.putFloat("MIN_PIN_FONT", this.mMinPinFont);
        bundle.putFloat("MAX_PIN_FONT", this.mMaxPinFont);
        return bundle;
    }

    public void onRestoreInstanceState(Parcelable parcelable) {
        if (parcelable instanceof Bundle) {
            Bundle bundle = (Bundle) parcelable;
            this.mTickCount = bundle.getInt("TICK_COUNT");
            this.mTickStart = bundle.getFloat("TICK_START");
            this.mTickEnd = bundle.getFloat("TICK_END");
            this.mTickInterval = bundle.getFloat("TICK_INTERVAL");
            this.mTickDefaultColor = bundle.getInt("TICK_COLOR");
            this.mTickColors = bundle.getIntegerArrayList("TICK_COLORS");
            this.mTickLabelColor = bundle.getInt("TICK_LABEL_COLOR");
            this.mTickLabelSelectedColor = bundle.getInt("TICK_LABEL_SELECTED_COLOR");
            this.mTickTopLabels = bundle.getCharSequenceArray("TICK_TOP_LABELS");
            this.mTickBottomLabels = bundle.getCharSequenceArray("TICK_BOTTOM_LABELS");
            this.mTickDefaultLabel = bundle.getString("TICK_DEFAULT_LABEL");
            this.mTickHeight = bundle.getFloat("TICK_HEIGHT_DP");
            this.mBarWeight = bundle.getFloat("BAR_WEIGHT");
            this.mIsBarRounded = bundle.getBoolean("BAR_ROUNDED", false);
            this.mBarColor = bundle.getInt("BAR_COLOR");
            this.mCircleSize = bundle.getFloat("CIRCLE_SIZE");
            this.mCircleColor = bundle.getInt("CIRCLE_COLOR");
            this.mCircleColorLeft = bundle.getInt("CIRCLE_COLOR_LEFT");
            this.mCircleColorRight = bundle.getInt("CIRCLE_COLOR_RIGHT");
            this.mCircleBoundaryColor = bundle.getInt("CIRCLE_BOUNDARY_COLOR");
            this.mCircleBoundarySize = bundle.getFloat("CIRCLE_BOUNDARY_WIDTH");
            this.mConnectingLineWeight = bundle.getFloat("CONNECTING_LINE_WEIGHT");
            this.mConnectingLineColors = bundle.getIntegerArrayList("CONNECTING_LINE_COLOR");
            this.mThumbRadiusDP = bundle.getFloat("THUMB_RADIUS_DP");
            this.mExpandedPinRadius = bundle.getFloat("EXPANDED_PIN_RADIUS_DP");
            this.mPinPadding = bundle.getFloat("PIN_PADDING");
            this.mBarPaddingBottom = bundle.getFloat("BAR_PADDING_BOTTOM");
            this.mIsRangeBar = bundle.getBoolean("IS_RANGE_BAR");
            this.mOnlyOnDrag = bundle.getBoolean("IS_ONLY_ON_DRAG");
            this.mArePinsTemporary = bundle.getBoolean("ARE_PINS_TEMPORARY");
            this.mLeftIndex = bundle.getInt("LEFT_INDEX");
            this.mRightIndex = bundle.getInt("RIGHT_INDEX");
            this.mFirstSetTickCount = bundle.getBoolean("FIRST_SET_TICK_COUNT");
            this.mMinPinFont = bundle.getFloat("MIN_PIN_FONT");
            this.mMaxPinFont = bundle.getFloat("MAX_PIN_FONT");
            setRangePinsByIndices(this.mLeftIndex, this.mRightIndex);
            super.onRestoreInstanceState(bundle.getParcelable("instanceState"));
            return;
        }
        super.onRestoreInstanceState(parcelable);
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        int mode = View.MeasureSpec.getMode(i);
        int mode2 = View.MeasureSpec.getMode(i2);
        int size = View.MeasureSpec.getSize(i);
        int size2 = View.MeasureSpec.getSize(i2);
        if (!(mode == Integer.MIN_VALUE || mode == 1073741824)) {
            size = this.mDefaultWidth;
        }
        if (mode2 == Integer.MIN_VALUE) {
            size2 = Math.min(this.mDefaultHeight, size2);
        } else if (mode2 != 1073741824) {
            size2 = this.mDefaultHeight;
        }
        setMeasuredDimension(size, size2);
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        float f;
        OnRangeBarChangeListener onRangeBarChangeListener;
        super.onSizeChanged(i, i2, i3, i4);
        Context context = getContext();
        float f2 = this.mExpandedPinRadius / this.mDisplayMetrices.density;
        float f3 = ((float) i2) - this.mBarPaddingBottom;
        if (this.mIsRangeBar) {
            PinView pinView = new PinView(context);
            this.mLeftThumb = pinView;
            pinView.setFormatter(this.mFormatter);
            this.mLeftThumb.init(context, f3, f2, this.mPinColor, this.mTextColor, this.mCircleSize, this.mCircleColorLeft, this.mCircleBoundaryColor, this.mCircleBoundarySize, this.mMinPinFont, this.mMaxPinFont, this.mArePinsTemporary);
        }
        PinView pinView2 = new PinView(context);
        this.mRightThumb = pinView2;
        pinView2.setFormatter(this.mFormatter);
        Context context2 = context;
        this.mRightThumb.init(context2, f3, f2, this.mPinColor, this.mTextColor, this.mCircleSize, this.mCircleColorRight, this.mCircleBoundaryColor, this.mCircleBoundarySize, this.mMinPinFont, this.mMaxPinFont, this.mArePinsTemporary);
        float max = Math.max(this.mExpandedPinRadius, this.mCircleSize);
        float f4 = ((float) i) - (DEFAULT_BAR_WEIGHT_DP * max);
        this.mBar = new Bar(context2, max, f3, f4, this.mTickCount, this.mTickHeight, this.mTickDefaultColor, this.mTickColors, this.mBarWeight, this.mBarColor, this.mIsBarRounded, this.mTickLabelColor, this.mTickLabelSelectedColor, this.mTickTopLabels, this.mTickBottomLabels, this.mTickDefaultLabel, this.mTickLabelSize);
        if (this.mIsRangeBar) {
            this.mLeftThumb.setX(((((float) this.mLeftIndex) / ((float) (this.mTickCount - 1))) * f4) + max);
            this.mLeftThumb.setXValue(getPinValue(this.mLeftIndex));
        }
        this.mRightThumb.setX(max + ((((float) this.mRightIndex) / ((float) (this.mTickCount - 1))) * f4));
        this.mRightThumb.setXValue(getPinValue(this.mRightIndex));
        int nearestTickIndex = this.mIsRangeBar ? this.mBar.getNearestTickIndex(this.mLeftThumb) : 0;
        int nearestTickIndex2 = this.mBar.getNearestTickIndex(this.mRightThumb);
        if ((nearestTickIndex == this.mLeftIndex && nearestTickIndex2 == this.mRightIndex) || (onRangeBarChangeListener = this.mListener) == null) {
            f = f3;
        } else {
            int i5 = this.mLeftIndex;
            f = f3;
            onRangeBarChangeListener.onRangeChangeListener(this, i5, this.mRightIndex, getPinValue(i5), getPinValue(this.mRightIndex));
        }
        this.mConnectingLine = new ConnectingLine(f, this.mConnectingLineWeight, this.mConnectingLineColors);
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        super.onLayout(z, i, i2, i3, i4);
        this.mIsInScrollingContainer = isInScrollingContainer();
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        this.mBar.draw(canvas);
        if (this.mIsRangeBar) {
            this.mConnectingLine.draw(canvas, this.mLeftThumb, this.mRightThumb);
            if (this.drawTicks) {
                this.mBar.drawTicks(canvas, this.mExpandedPinRadius, this.mRightThumb, this.mLeftThumb);
            }
            this.mLeftThumb.draw(canvas);
        } else {
            this.mConnectingLine.draw(canvas, getMarginLeft(), this.mRightThumb);
            if (this.drawTicks) {
                this.mBar.drawTicks(canvas, this.mExpandedPinRadius, this.mRightThumb);
            }
        }
        this.mRightThumb.draw(canvas);
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (!isEnabled()) {
            return false;
        }
        int action = motionEvent.getAction();
        if (action == 0) {
            this.mDiffX = 0;
            this.mDiffY = 0;
            this.mLastX = motionEvent.getX();
            this.mLastY = motionEvent.getY();
            if (!this.mIsInScrollingContainer) {
                onActionDown(motionEvent.getX(), motionEvent.getY());
            }
            return true;
        } else if (action == 1) {
            if (this.mDragging || (motionEvent.getX() == this.mLastX && motionEvent.getY() == this.mLastY)) {
                getParent().requestDisallowInterceptTouchEvent(false);
                onActionUp(motionEvent.getX(), motionEvent.getY());
            }
            return true;
        } else if (action == 2) {
            float x = motionEvent.getX();
            float y = motionEvent.getY();
            this.mDiffX = (int) (((float) this.mDiffX) + Math.abs(x - this.mLastX));
            int abs = (int) (((float) this.mDiffY) + Math.abs(y - this.mLastY));
            this.mDiffY = abs;
            this.mLastX = x;
            this.mLastY = y;
            if (this.mDragging) {
                onActionMove(motionEvent.getX());
                getParent().requestDisallowInterceptTouchEvent(true);
                if (this.mDiffX >= this.mDiffY) {
                    return true;
                }
                if (!this.mIsInScrollingContainer) {
                    getParent().requestDisallowInterceptTouchEvent(false);
                }
                return false;
            } else if (this.mDiffX <= abs) {
                return false;
            } else {
                onActionDown(motionEvent.getX(), motionEvent.getY());
                return true;
            }
        } else if (action != 3) {
            return false;
        } else {
            if (this.mDragging || (motionEvent.getX() == this.mLastX && motionEvent.getY() == this.mLastY)) {
                getParent().requestDisallowInterceptTouchEvent(false);
                onActionUp(motionEvent.getX(), motionEvent.getY());
            }
            return true;
        }
    }

    public void setOnlyOnDrag(boolean z) {
        this.mOnlyOnDrag = z;
    }

    public void setOnRangeBarChangeListener(OnRangeBarChangeListener onRangeBarChangeListener) {
        this.mListener = onRangeBarChangeListener;
    }

    public void setPinTextListener(OnRangeBarTextListener onRangeBarTextListener) {
        this.mPinTextListener = onRangeBarTextListener;
    }

    public void setFormatter(IRangeBarFormatter iRangeBarFormatter) {
        PinView pinView = this.mLeftThumb;
        if (pinView != null) {
            pinView.setFormatter(iRangeBarFormatter);
        }
        PinView pinView2 = this.mRightThumb;
        if (pinView2 != null) {
            pinView2.setFormatter(iRangeBarFormatter);
        }
        this.mFormatter = iRangeBarFormatter;
    }

    public void setDrawTicks(boolean z) {
        this.drawTicks = z;
    }

    public void setTickHeight(float f) {
        this.mTickHeight = f;
        createBar();
    }

    public void setBarWeight(float f) {
        this.mBarWeight = f;
        createBar();
    }

    public boolean isBarRounded() {
        return this.mIsBarRounded;
    }

    public void setBarRounded(boolean z) {
        this.mIsBarRounded = z;
        createBar();
    }

    public void setBarColor(int i) {
        this.mBarColor = i;
        createBar();
    }

    public void setPinColor(int i) {
        this.mPinColor = i;
        createPins();
    }

    public void setPinTextColor(int i) {
        this.mTextColor = i;
        createPins();
    }

    public void setRangeBarEnabled(boolean z) {
        this.mIsRangeBar = z;
        invalidate();
    }

    public void setTemporaryPins(boolean z) {
        this.mArePinsTemporary = z;
        invalidate();
    }

    public void setTickDefaultColor(int i) {
        this.mTickDefaultColor = i;
        setTickColors(i);
        createBar();
    }

    public void setTickLabelColor(int i) {
        this.mTickLabelColor = i;
        createBar();
    }

    public void setTickLabelSelectedColor(int i) {
        this.mTickLabelSelectedColor = i;
        createBar();
    }

    public void setSelectorColor(int i) {
        this.mCircleColor = i;
        setLeftSelectorColor(i);
        setRightSelectorColor(i);
        createPins();
    }

    public void setSelectorBoundaryColor(int i) {
        this.mCircleBoundaryColor = i;
        createPins();
    }

    public void setSelectorBoundarySize(int i) {
        this.mCircleBoundarySize = (float) i;
        createPins();
    }

    public void setConnectingLineWeight(float f) {
        this.mConnectingLineWeight = f;
        createConnectingLine();
    }

    public void setConnectingLineColor(int i) {
        this.mConnectingLineColors.clear();
        this.mConnectingLineColors.add(Integer.valueOf(i));
        createConnectingLine();
    }

    public void setConnectingLineColors(ArrayList<Integer> arrayList) {
        this.mConnectingLineColors = new ArrayList<>(arrayList);
        createConnectingLine();
    }

    public void setPinRadius(float f) {
        this.mExpandedPinRadius = f;
        createPins();
    }

    public int getLeftSelectorColor() {
        return this.mCircleColorLeft;
    }

    public void setLeftSelectorColor(int i) {
        this.mCircleColorLeft = i;
        createPins();
    }

    public int getRightSelectorColor() {
        return this.mCircleColorRight;
    }

    public void setRightSelectorColor(int i) {
        this.mCircleColorRight = i;
        createPins();
    }

    public float getTickStart() {
        return this.mTickStart;
    }

    public void setTickStart(float f) {
        int i = ((int) ((this.mTickEnd - f) / this.mTickInterval)) + 1;
        if (isValidTickCount(i)) {
            this.mTickCount = i;
            this.mTickStart = f;
            if (this.mFirstSetTickCount) {
                this.mLeftIndex = 0;
                int i2 = i - 1;
                this.mRightIndex = i2;
                OnRangeBarChangeListener onRangeBarChangeListener = this.mListener;
                if (onRangeBarChangeListener != null) {
                    onRangeBarChangeListener.onRangeChangeListener(this, 0, i2, getPinValue(0), getPinValue(this.mRightIndex));
                }
            }
            if (indexOutOfRange(this.mLeftIndex, this.mRightIndex)) {
                this.mLeftIndex = 0;
                int i3 = this.mTickCount - 1;
                this.mRightIndex = i3;
                OnRangeBarChangeListener onRangeBarChangeListener2 = this.mListener;
                if (onRangeBarChangeListener2 != null) {
                    onRangeBarChangeListener2.onRangeChangeListener(this, 0, i3, getPinValue(0), getPinValue(this.mRightIndex));
                }
            }
            createBar();
            createPins();
            return;
        }
        Log.e(TAG, "tickCount less than 2; invalid tickCount.");
        throw new IllegalArgumentException("tickCount less than 2; invalid tickCount.");
    }

    public float getTickEnd() {
        return this.mTickEnd;
    }

    public void setTickEnd(float f) {
        int i = ((int) ((f - this.mTickStart) / this.mTickInterval)) + 1;
        if (isValidTickCount(i)) {
            this.mTickCount = i;
            this.mTickEnd = f;
            if (this.mFirstSetTickCount) {
                this.mLeftIndex = 0;
                int i2 = i - 1;
                this.mRightIndex = i2;
                OnRangeBarChangeListener onRangeBarChangeListener = this.mListener;
                if (onRangeBarChangeListener != null) {
                    onRangeBarChangeListener.onRangeChangeListener(this, 0, i2, getPinValue(0), getPinValue(this.mRightIndex));
                }
            }
            if (indexOutOfRange(this.mLeftIndex, this.mRightIndex)) {
                this.mLeftIndex = 0;
                int i3 = this.mTickCount - 1;
                this.mRightIndex = i3;
                OnRangeBarChangeListener onRangeBarChangeListener2 = this.mListener;
                if (onRangeBarChangeListener2 != null) {
                    onRangeBarChangeListener2.onRangeChangeListener(this, 0, i3, getPinValue(0), getPinValue(this.mRightIndex));
                }
            }
            createBar();
            createPins();
            return;
        }
        Log.e(TAG, "tickCount less than 2; invalid tickCount.");
        throw new IllegalArgumentException("tickCount less than 2; invalid tickCount.");
    }

    public int getTickCount() {
        return this.mTickCount;
    }

    public CharSequence[] getTickTopLabels() {
        return this.mTickTopLabels;
    }

    public void setTickTopLabels(CharSequence[] charSequenceArr) {
        this.mTickTopLabels = charSequenceArr;
        createBar();
    }

    public CharSequence[] getTickBottomLabels() {
        return this.mTickBottomLabels;
    }

    public void setTickBottomLabels(CharSequence[] charSequenceArr) {
        this.mTickBottomLabels = charSequenceArr;
        createBar();
    }

    public ArrayList<Integer> getTickColors() {
        return this.mTickColors;
    }

    public void setTickColors(ArrayList<Integer> arrayList) {
        this.mTickColors = new ArrayList<>(arrayList);
        createBar();
    }

    public void setTickColors(int i) {
        for (int i2 = 0; i2 < this.mTickColors.size(); i2++) {
            this.mTickColors.set(i2, Integer.valueOf(i));
        }
        createBar();
    }

    public int getTickColor(int i) {
        return this.mTickColors.get(i).intValue();
    }

    public void setRangePinsByIndices(int i, int i2) {
        if (!indexOutOfRange(i, i2)) {
            if (this.mFirstSetTickCount) {
                this.mFirstSetTickCount = false;
            }
            this.mLeftIndex = i;
            this.mRightIndex = i2;
            createPins();
            OnRangeBarChangeListener onRangeBarChangeListener = this.mListener;
            if (onRangeBarChangeListener != null) {
                int i3 = this.mLeftIndex;
                onRangeBarChangeListener.onRangeChangeListener(this, i3, this.mRightIndex, getPinValue(i3), getPinValue(this.mRightIndex));
            }
            invalidate();
            requestLayout();
            return;
        }
        Log.e(TAG, "Pin index left " + i + ", or right " + i2 + " is out of bounds. Check that it is greater than the minimum (" + this.mTickStart + ") and less than the maximum value (" + this.mTickEnd + ")");
        throw new IllegalArgumentException("Pin index left " + i + ", or right " + i2 + " is out of bounds. Check that it is greater than the minimum (" + this.mTickStart + ") and less than the maximum value (" + this.mTickEnd + ")");
    }

    public void setSeekPinByIndex(int i) {
        if (i < 0 || i > this.mTickCount) {
            Log.e(TAG, "Pin index " + i + " is out of bounds. Check that it is greater than the minimum (" + 0 + ") and less than the maximum value (" + this.mTickCount + ")");
            throw new IllegalArgumentException("Pin index " + i + " is out of bounds. Check that it is greater than the minimum (" + 0 + ") and less than the maximum value (" + this.mTickCount + ")");
        }
        if (this.mFirstSetTickCount) {
            this.mFirstSetTickCount = false;
        }
        this.mRightIndex = i;
        createPins();
        OnRangeBarChangeListener onRangeBarChangeListener = this.mListener;
        if (onRangeBarChangeListener != null) {
            int i2 = this.mLeftIndex;
            onRangeBarChangeListener.onRangeChangeListener(this, i2, this.mRightIndex, getPinValue(i2), getPinValue(this.mRightIndex));
        }
        invalidate();
        requestLayout();
    }

    public void setRangePinsByValue(float f, float f2) {
        if (!valueOutOfRange(f, f2)) {
            if (this.mFirstSetTickCount) {
                this.mFirstSetTickCount = false;
            }
            float f3 = this.mTickStart;
            float f4 = this.mTickInterval;
            this.mLeftIndex = (int) ((f - f3) / f4);
            this.mRightIndex = (int) ((f2 - f3) / f4);
            createPins();
            OnRangeBarChangeListener onRangeBarChangeListener = this.mListener;
            if (onRangeBarChangeListener != null) {
                int i = this.mLeftIndex;
                onRangeBarChangeListener.onRangeChangeListener(this, i, this.mRightIndex, getPinValue(i), getPinValue(this.mRightIndex));
            }
            OnRangeBarChangeListener onRangeBarChangeListener2 = this.mListener;
            if (onRangeBarChangeListener2 != null) {
                onRangeBarChangeListener2.onTouchEnded(this);
            }
            invalidate();
            requestLayout();
            return;
        }
        Log.e(TAG, "Pin value left " + f + ", or right " + f2 + " is out of bounds. Check that it is greater than the minimum (" + this.mTickStart + ") and less than the maximum value (" + this.mTickEnd + ")");
        throw new IllegalArgumentException("Pin value left " + f + ", or right " + f2 + " is out of bounds. Check that it is greater than the minimum (" + this.mTickStart + ") and less than the maximum value (" + this.mTickEnd + ")");
    }

    public void setSeekPinByValue(float f) {
        if (f > this.mTickEnd || f < this.mTickStart) {
            Log.e(TAG, "Pin value " + f + " is out of bounds. Check that it is greater than the minimum (" + this.mTickStart + ") and less than the maximum value (" + this.mTickEnd + ")");
            throw new IllegalArgumentException("Pin value " + f + " is out of bounds. Check that it is greater than the minimum (" + this.mTickStart + ") and less than the maximum value (" + this.mTickEnd + ")");
        }
        if (this.mFirstSetTickCount) {
            this.mFirstSetTickCount = false;
        }
        this.mRightIndex = (int) ((f - this.mTickStart) / this.mTickInterval);
        createPins();
        OnRangeBarChangeListener onRangeBarChangeListener = this.mListener;
        if (onRangeBarChangeListener != null) {
            int i = this.mLeftIndex;
            onRangeBarChangeListener.onRangeChangeListener(this, i, this.mRightIndex, getPinValue(i), getPinValue(this.mRightIndex));
        }
        invalidate();
        requestLayout();
    }

    public boolean isRangeBar() {
        return this.mIsRangeBar;
    }

    public String getLeftPinValue() {
        return getPinValue(this.mLeftIndex);
    }

    public String getRightPinValue() {
        return getPinValue(this.mRightIndex);
    }

    public int getLeftIndex() {
        return this.mLeftIndex;
    }

    public int getRightIndex() {
        return this.mRightIndex;
    }

    public double getTickInterval() {
        return (double) this.mTickInterval;
    }

    public void setTickInterval(float f) {
        int i = ((int) ((this.mTickEnd - this.mTickStart) / f)) + 1;
        if (isValidTickCount(i)) {
            this.mTickCount = i;
            this.mTickInterval = f;
            if (this.mFirstSetTickCount) {
                this.mLeftIndex = 0;
                int i2 = i - 1;
                this.mRightIndex = i2;
                OnRangeBarChangeListener onRangeBarChangeListener = this.mListener;
                if (onRangeBarChangeListener != null) {
                    onRangeBarChangeListener.onRangeChangeListener(this, 0, i2, getPinValue(0), getPinValue(this.mRightIndex));
                }
            }
            if (indexOutOfRange(this.mLeftIndex, this.mRightIndex)) {
                this.mLeftIndex = 0;
                int i3 = this.mTickCount - 1;
                this.mRightIndex = i3;
                OnRangeBarChangeListener onRangeBarChangeListener2 = this.mListener;
                if (onRangeBarChangeListener2 != null) {
                    onRangeBarChangeListener2.onRangeChangeListener(this, 0, i3, getPinValue(0), getPinValue(this.mRightIndex));
                }
            }
            createBar();
            createPins();
            return;
        }
        Log.e(TAG, "tickCount less than 2; invalid tickCount.");
        throw new IllegalArgumentException("tickCount less than 2; invalid tickCount.");
    }

    public void setEnabled(boolean z) {
        if (!z) {
            this.mBarColor = -3355444;
            setConnectingLineColor(-3355444);
            this.mCircleColor = -3355444;
            this.mCircleColorLeft = -3355444;
            this.mCircleColorRight = -3355444;
            this.mCircleBoundaryColor = -3355444;
            this.mTickDefaultColor = -3355444;
            setTickColors(-3355444);
            this.mTickLabelColor = -3355444;
            this.mTickLabelSelectedColor = -3355444;
        } else {
            this.mBarColor = this.mActiveBarColor;
            setConnectingLineColor(this.mActiveConnectingLineColor);
            setConnectingLineColors(this.mActiveConnectingLineColors);
            this.mCircleColor = this.mActiveCircleColor;
            this.mCircleColorLeft = this.mActiveCircleColorLeft;
            this.mCircleColorRight = this.mActiveCircleColorRight;
            this.mCircleBoundaryColor = this.mActiveCircleBoundaryColor;
            this.mTickDefaultColor = this.mActiveTickDefaultColor;
            setTickColors(this.mActiveTickColors);
            this.mTickLabelColor = this.mActiveTickLabelColor;
            this.mTickLabelSelectedColor = this.mActiveTickLabelSelectedColor;
        }
        super.setEnabled(z);
        createBar();
        createPins();
        createConnectingLine();
    }

    private void rangeBarInit(Context context, AttributeSet attributeSet) {
        if (this.mTickMap == null) {
            this.mTickMap = new HashMap<>();
        }
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, R.styleable.RangeBar, 0, 0);
        try {
            float f = obtainStyledAttributes.getFloat(32, 0.0f);
            float f2 = obtainStyledAttributes.getFloat(26, DEFAULT_TICK_END);
            float f3 = obtainStyledAttributes.getFloat(28, 1.0f);
            int i = ((int) ((f2 - f) / f3)) + 1;
            if (isValidTickCount(i)) {
                this.mTickCount = i;
                this.mTickStart = f;
                this.mTickEnd = f2;
                this.mTickInterval = f3;
                this.mLeftIndex = 0;
                int i2 = i - 1;
                this.mRightIndex = i2;
                if (this.mListener != null) {
                    this.mListener.onRangeChangeListener(this, 0, i2, getPinValue(0), getPinValue(this.mRightIndex));
                }
            } else {
                Log.e(TAG, "tickCount less than 2; invalid tickCount. XML input ignored.");
            }
            this.mTickHeight = obtainStyledAttributes.getDimension(27, TypedValue.applyDimension(1, 1.0f, this.mDisplayMetrices));
            this.mBarWeight = obtainStyledAttributes.getDimension(0, TypedValue.applyDimension(1, DEFAULT_BAR_WEIGHT_DP, this.mDisplayMetrices));
            this.mCircleSize = obtainStyledAttributes.getDimension(20, TypedValue.applyDimension(1, DEFAULT_CIRCLE_SIZE_DP, this.mDisplayMetrices));
            this.mCircleBoundarySize = obtainStyledAttributes.getDimension(18, TypedValue.applyDimension(1, 0.0f, this.mDisplayMetrices));
            this.mConnectingLineWeight = obtainStyledAttributes.getDimension(3, TypedValue.applyDimension(1, 4.0f, this.mDisplayMetrices));
            this.mExpandedPinRadius = obtainStyledAttributes.getDimension(10, TypedValue.applyDimension(1, 12.0f, this.mDisplayMetrices));
            this.mPinPadding = obtainStyledAttributes.getDimension(9, TypedValue.applyDimension(1, DEFAULT_PIN_PADDING_DP, this.mDisplayMetrices));
            this.mBarPaddingBottom = obtainStyledAttributes.getDimension(14, TypedValue.applyDimension(1, 24.0f, this.mDisplayMetrices));
            this.mBarColor = obtainStyledAttributes.getColor(13, -3355444);
            this.mTextColor = obtainStyledAttributes.getColor(11, -1);
            this.mPinColor = obtainStyledAttributes.getColor(6, -12627531);
            this.mActiveBarColor = this.mBarColor;
            int color = obtainStyledAttributes.getColor(19, -12627531);
            this.mCircleColor = color;
            this.mCircleColorLeft = obtainStyledAttributes.getColor(4, color);
            this.mCircleColorRight = obtainStyledAttributes.getColor(16, this.mCircleColor);
            int color2 = obtainStyledAttributes.getColor(17, -12627531);
            this.mCircleBoundaryColor = color2;
            this.mActiveCircleColor = this.mCircleColor;
            this.mActiveCircleColorLeft = this.mCircleColorLeft;
            this.mActiveCircleColorRight = this.mCircleColorRight;
            this.mActiveCircleBoundaryColor = color2;
            int color3 = obtainStyledAttributes.getColor(24, ViewCompat.MEASURED_STATE_MASK);
            this.mTickDefaultColor = color3;
            this.mActiveTickDefaultColor = color3;
            this.mTickColors = getColors(obtainStyledAttributes.getTextArray(23), this.mTickDefaultColor);
            this.mActiveTickColors = new ArrayList<>(this.mTickColors);
            int color4 = obtainStyledAttributes.getColor(29, -3355444);
            this.mTickLabelColor = color4;
            this.mActiveTickLabelColor = color4;
            int color5 = obtainStyledAttributes.getColor(30, ViewCompat.MEASURED_STATE_MASK);
            this.mTickLabelSelectedColor = color5;
            this.mActiveTickLabelSelectedColor = color5;
            this.mTickBottomLabels = obtainStyledAttributes.getTextArray(22);
            this.mTickTopLabels = obtainStyledAttributes.getTextArray(33);
            String string = obtainStyledAttributes.getString(25);
            this.mTickDefaultLabel = string;
            if (string == null) {
                string = "";
            }
            this.mTickDefaultLabel = string;
            int color6 = obtainStyledAttributes.getColor(1, -12627531);
            this.mActiveConnectingLineColor = color6;
            CharSequence[] textArray = obtainStyledAttributes.getTextArray(2);
            if (textArray == null || textArray.length <= 0) {
                this.mConnectingLineColors.add(Integer.valueOf(color6));
            } else {
                for (CharSequence charSequence : textArray) {
                    String charSequence2 = charSequence.toString();
                    if (charSequence2.length() == 4) {
                        charSequence2 = charSequence2 + "000";
                    }
                    this.mConnectingLineColors.add(Integer.valueOf(Color.parseColor(charSequence2)));
                }
            }
            this.mActiveConnectingLineColors = new ArrayList<>(this.mConnectingLineColors);
            this.mIsRangeBar = obtainStyledAttributes.getBoolean(12, true);
            this.mArePinsTemporary = obtainStyledAttributes.getBoolean(21, true);
            this.mIsBarRounded = obtainStyledAttributes.getBoolean(15, false);
            float f4 = this.mDisplayMetrices.density;
            this.mMinPinFont = obtainStyledAttributes.getDimension(8, 8.0f * f4);
            this.mMaxPinFont = obtainStyledAttributes.getDimension(7, 24.0f * f4);
            this.mTickLabelSize = obtainStyledAttributes.getDimension(31, f4 * 4.0f);
            this.mIsRangeBar = obtainStyledAttributes.getBoolean(12, true);
            this.mOnlyOnDrag = obtainStyledAttributes.getBoolean(5, false);
        } finally {
            obtainStyledAttributes.recycle();
        }
    }

    private void createBar() {
        Bar bar = r1;
        Bar bar2 = new Bar(getContext(), getMarginLeft(), getYPos(), getBarLength(), this.mTickCount, this.mTickHeight, this.mTickDefaultColor, this.mTickColors, this.mBarWeight, this.mBarColor, this.mIsBarRounded, this.mTickLabelColor, this.mTickLabelSelectedColor, this.mTickTopLabels, this.mTickBottomLabels, this.mTickDefaultLabel, this.mTickLabelSize);
        this.mBar = bar;
        invalidate();
    }

    private void createConnectingLine() {
        this.mConnectingLine = new ConnectingLine(getYPos(), this.mConnectingLineWeight, this.mConnectingLineColors);
        invalidate();
    }

    public void setPinTextFormatter(PinTextFormatter pinTextFormatter) {
        this.mPinTextFormatter = pinTextFormatter;
    }

    private void createPins() {
        Context context = getContext();
        float yPos = getYPos();
        float f = isEnabled() ? this.mExpandedPinRadius / this.mDisplayMetrices.density : 0.0f;
        if (this.mIsRangeBar) {
            PinView pinView = new PinView(context);
            this.mLeftThumb = pinView;
            pinView.init(context, yPos, f, this.mPinColor, this.mTextColor, this.mCircleSize, this.mCircleColorLeft, this.mCircleBoundaryColor, this.mCircleBoundarySize, this.mMinPinFont, this.mMaxPinFont, this.mArePinsTemporary);
        }
        PinView pinView2 = new PinView(context);
        this.mRightThumb = pinView2;
        pinView2.init(context, yPos, f, this.mPinColor, this.mTextColor, this.mCircleSize, this.mCircleColorRight, this.mCircleBoundaryColor, this.mCircleBoundarySize, this.mMinPinFont, this.mMaxPinFont, this.mArePinsTemporary);
        float marginLeft = getMarginLeft();
        float barLength = getBarLength();
        if (this.mIsRangeBar) {
            this.mLeftThumb.setX(((((float) this.mLeftIndex) / ((float) (this.mTickCount - 1))) * barLength) + marginLeft);
            this.mLeftThumb.setXValue(getPinValue(this.mLeftIndex));
        }
        this.mRightThumb.setX(marginLeft + ((((float) this.mRightIndex) / ((float) (this.mTickCount - 1))) * barLength));
        this.mRightThumb.setXValue(getPinValue(this.mRightIndex));
        invalidate();
    }

    private float getMarginLeft() {
        return Math.max(this.mExpandedPinRadius, this.mCircleSize);
    }

    private float getYPos() {
        return ((float) getHeight()) - this.mBarPaddingBottom;
    }

    private float getBarLength() {
        return ((float) getWidth()) - (getMarginLeft() * DEFAULT_BAR_WEIGHT_DP);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:1:0x0002, code lost:
        r0 = r1.mTickCount;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean indexOutOfRange(int r2, int r3) {
        /*
            r1 = this;
            if (r2 < 0) goto L_0x000d
            int r0 = r1.mTickCount
            if (r2 >= r0) goto L_0x000d
            if (r3 < 0) goto L_0x000d
            if (r3 < r0) goto L_0x000b
            goto L_0x000d
        L_0x000b:
            r2 = 0
            goto L_0x000e
        L_0x000d:
            r2 = 1
        L_0x000e:
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.iqonic.store.utils.rangeBar.RangeBar.indexOutOfRange(int, int):boolean");
    }

    private boolean valueOutOfRange(float f, float f2) {
        float f3 = this.mTickStart;
        if (f >= f3) {
            float f4 = this.mTickEnd;
            return f > f4 || f2 < f3 || f2 > f4;
        }
    }

    private float getLeftThumbXDistance(float f) {
        if (!isRangeBar()) {
            return 0.0f;
        }
        float x = this.mLeftThumb.getX();
        if (x != this.mRightThumb.getX() || f >= x) {
            return Math.abs(x - f);
        }
        return 0.0f;
    }

    private float getRightThumbXDistance(float f) {
        return Math.abs(this.mRightThumb.getX() - f);
    }

    private void onActionDown(float f, float f2) {
        if (this.mIsRangeBar) {
            if (!this.mRightThumb.isPressed() && this.mLeftThumb.isInTargetZone(f, f2)) {
                pressPin(this.mLeftThumb);
            } else if (!this.mLeftThumb.isPressed() && this.mRightThumb.isInTargetZone(f, f2)) {
                pressPin(this.mRightThumb);
            }
        } else if (this.mRightThumb.isInTargetZone(f, f2)) {
            pressPin(this.mRightThumb);
        }
        this.mDragging = true;
        OnRangeBarChangeListener onRangeBarChangeListener = this.mListener;
        if (onRangeBarChangeListener != null) {
            onRangeBarChangeListener.onTouchStarted(this);
        }
    }

    private void onActionUp(float f, float f2) {
        if (this.mIsRangeBar && this.mLeftThumb.isPressed()) {
            releasePin(this.mLeftThumb);
        } else if (this.mRightThumb.isPressed()) {
            releasePin(this.mRightThumb);
        } else if (!this.mOnlyOnDrag) {
            if (getLeftThumbXDistance(f) >= getRightThumbXDistance(f) || !this.mIsRangeBar) {
                this.mRightThumb.setX(f);
                releasePin(this.mRightThumb);
            } else {
                this.mLeftThumb.setX(f);
                releasePin(this.mLeftThumb);
            }
            int nearestTickIndex = this.mIsRangeBar ? this.mBar.getNearestTickIndex(this.mLeftThumb) : 0;
            int nearestTickIndex2 = this.mBar.getNearestTickIndex(this.mRightThumb);
            if (!(nearestTickIndex == this.mLeftIndex && nearestTickIndex2 == this.mRightIndex)) {
                this.mLeftIndex = nearestTickIndex;
                this.mRightIndex = nearestTickIndex2;
                OnRangeBarChangeListener onRangeBarChangeListener = this.mListener;
                if (onRangeBarChangeListener != null) {
                    onRangeBarChangeListener.onRangeChangeListener(this, nearestTickIndex, nearestTickIndex2, getPinValue(nearestTickIndex), getPinValue(this.mRightIndex));
                }
            }
        }
        this.mDragging = false;
        OnRangeBarChangeListener onRangeBarChangeListener2 = this.mListener;
        if (onRangeBarChangeListener2 != null) {
            onRangeBarChangeListener2.onTouchEnded(this);
        }
    }

    private void onActionMove(float f) {
        if (this.mIsRangeBar && this.mLeftThumb.isPressed()) {
            movePin(this.mLeftThumb, f);
        } else if (this.mRightThumb.isPressed()) {
            movePin(this.mRightThumb, f);
        }
        if (this.mIsRangeBar && this.mLeftThumb.getX() > this.mRightThumb.getX()) {
            PinView pinView = this.mLeftThumb;
            this.mLeftThumb = this.mRightThumb;
            this.mRightThumb = pinView;
        }
        int i = 0;
        int nearestTickIndex = this.mIsRangeBar ? this.mBar.getNearestTickIndex(this.mLeftThumb) : 0;
        int nearestTickIndex2 = this.mBar.getNearestTickIndex(this.mRightThumb);
        int paddingLeft = getPaddingLeft();
        int right = (getRight() - getPaddingRight()) - paddingLeft;
        if (f <= ((float) paddingLeft)) {
            movePin(this.mLeftThumb, this.mBar.getLeftX());
        } else {
            if (f >= ((float) right)) {
                nearestTickIndex2 = getTickCount() - 1;
                movePin(this.mRightThumb, this.mBar.getRightX());
            }
            i = nearestTickIndex;
        }
        if (i != this.mLeftIndex || nearestTickIndex2 != this.mRightIndex) {
            this.mLeftIndex = i;
            this.mRightIndex = nearestTickIndex2;
            if (this.mIsRangeBar) {
                this.mLeftThumb.setXValue(getPinValue(i));
            }
            this.mRightThumb.setXValue(getPinValue(this.mRightIndex));
            OnRangeBarChangeListener onRangeBarChangeListener = this.mListener;
            if (onRangeBarChangeListener != null) {
                int i2 = this.mLeftIndex;
                onRangeBarChangeListener.onRangeChangeListener(this, i2, this.mRightIndex, getPinValue(i2), getPinValue(this.mRightIndex));
            }
        }
    }

    private void pressPin(final PinView pinView) {
        if (this.mFirstSetTickCount) {
            this.mFirstSetTickCount = false;
        }
        if (this.mArePinsTemporary) {
            ValueAnimator ofFloat = ValueAnimator.ofFloat(new float[]{0.0f, this.mExpandedPinRadius});
            ofFloat.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                public void onAnimationUpdate(ValueAnimator valueAnimator) {
                    float unused = RangeBar.this.mThumbRadiusDP = ((Float) valueAnimator.getAnimatedValue()).floatValue();
                    pinView.setSize(RangeBar.this.mThumbRadiusDP, RangeBar.this.mPinPadding * valueAnimator.getAnimatedFraction());
                    RangeBar.this.invalidate();
                }
            });
            ofFloat.start();
        }
        pinView.press();
    }

    private void releasePin(final PinView pinView) {
        pinView.setX(this.mBar.getNearestTickCoordinate(pinView));
        pinView.setXValue(getPinValue(this.mBar.getNearestTickIndex(pinView)));
        if (this.mArePinsTemporary) {
            ValueAnimator ofFloat = ValueAnimator.ofFloat(new float[]{this.mExpandedPinRadius, 0.0f});
            ofFloat.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                public void onAnimationUpdate(ValueAnimator valueAnimator) {
                    float unused = RangeBar.this.mThumbRadiusDP = ((Float) valueAnimator.getAnimatedValue()).floatValue();
                    pinView.setSize(RangeBar.this.mThumbRadiusDP, RangeBar.this.mPinPadding - (RangeBar.this.mPinPadding * valueAnimator.getAnimatedFraction()));
                    RangeBar.this.invalidate();
                }
            });
            ofFloat.start();
        } else {
            invalidate();
        }
        pinView.release();
    }

    private String getPinValue(int i) {
        OnRangeBarTextListener onRangeBarTextListener = this.mPinTextListener;
        if (onRangeBarTextListener != null) {
            return onRangeBarTextListener.getPinValue(this, i);
        }
        float f = i == this.mTickCount + -1 ? this.mTickEnd : (((float) i) * this.mTickInterval) + this.mTickStart;
        String str = this.mTickMap.get(Float.valueOf(f));
        if (str == null) {
            double d = (double) f;
            if (d == Math.ceil(d)) {
                str = String.valueOf((int) f);
            } else {
                str = String.valueOf(f);
            }
        }
        return this.mPinTextFormatter.getText(str);
    }

    private ArrayList<Integer> getColors(CharSequence[] charSequenceArr, int i) {
        ArrayList<Integer> arrayList = new ArrayList<>();
        if (charSequenceArr == null || charSequenceArr.length <= 0) {
            arrayList.add(Integer.valueOf(i));
        } else {
            for (CharSequence charSequence : charSequenceArr) {
                String charSequence2 = charSequence.toString();
                if (charSequence2.length() == 4) {
                    charSequence2 = charSequence2 + "000";
                }
                arrayList.add(Integer.valueOf(Color.parseColor(charSequence2)));
            }
        }
        return arrayList;
    }

    private void movePin(PinView pinView, float f) {
        if (f >= this.mBar.getLeftX() && f <= this.mBar.getRightX() && pinView != null) {
            pinView.setX(f);
            invalidate();
        }
    }

    private boolean isInScrollingContainer() {
        ViewParent parent = getParent();
        while (parent != null && (parent instanceof ViewGroup)) {
            if (((ViewGroup) parent).shouldDelayChildPressedState()) {
                return true;
            }
            parent = parent.getParent();
        }
        return false;
    }

    public interface OnRangeBarChangeListener {
        void onRangeChangeListener(RangeBar rangeBar, int i, int i2, String str, String str2);

        void onTouchEnded(RangeBar rangeBar);

        void onTouchStarted(RangeBar rangeBar);
    }

    public interface OnRangeBarTextListener {
        String getPinValue(RangeBar rangeBar, int i);
    }

    public interface PinTextFormatter {
        String getText(String str);
    }
}
