package com.iqonic.store.utils.extensions;

import com.iqonic.store.AppBaseActivity;
import com.iqonic.store.models.Order;
import java.util.ArrayList;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0016\u0010\u0002\u001a\u0012\u0012\u0004\u0012\u00020\u00040\u0003j\b\u0012\u0004\u0012\u00020\u0004`\u0005H\n¢\u0006\u0002\b\u0006"}, d2 = {"<anonymous>", "", "it", "Ljava/util/ArrayList;", "Lcom/iqonic/store/models/Order;", "Lkotlin/collections/ArrayList;", "invoke"}, k = 3, mv = {1, 1, 16})
/* compiled from: NetworkExtension.kt */
final class NetworkExtensionKt$getOrder$1 extends Lambda implements Function1<ArrayList<Order>, Unit> {
    final /* synthetic */ Function1 $onApiSuccess;
    final /* synthetic */ AppBaseActivity $this_getOrder;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    NetworkExtensionKt$getOrder$1(AppBaseActivity appBaseActivity, Function1 function1) {
        super(1);
        this.$this_getOrder = appBaseActivity;
        this.$onApiSuccess = function1;
    }

    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        invoke((ArrayList<Order>) (ArrayList) obj);
        return Unit.INSTANCE;
    }

    public final void invoke(ArrayList<Order> arrayList) {
        Intrinsics.checkParameterIsNotNull(arrayList, "it");
        this.$this_getOrder.showProgress(false);
        this.$onApiSuccess.invoke(arrayList);
    }
}
