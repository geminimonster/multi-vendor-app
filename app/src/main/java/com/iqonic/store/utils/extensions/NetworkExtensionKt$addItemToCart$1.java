package com.iqonic.store.utils.extensions;

import com.iqonic.store.AppBaseActivity;
import com.iqonic.store.models.AddCartResponse;
import com.store.proshop.R;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n¢\u0006\u0002\b\u0004"}, d2 = {"<anonymous>", "", "it", "Lcom/iqonic/store/models/AddCartResponse;", "invoke"}, k = 3, mv = {1, 1, 16})
/* compiled from: NetworkExtension.kt */
final class NetworkExtensionKt$addItemToCart$1 extends Lambda implements Function1<AddCartResponse, Unit> {
    final /* synthetic */ Function1 $onApiSuccess;
    final /* synthetic */ AppBaseActivity $this_addItemToCart;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    NetworkExtensionKt$addItemToCart$1(AppBaseActivity appBaseActivity, Function1 function1) {
        super(1);
        this.$this_addItemToCart = appBaseActivity;
        this.$onApiSuccess = function1;
    }

    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        invoke((AddCartResponse) obj);
        return Unit.INSTANCE;
    }

    public final void invoke(AddCartResponse addCartResponse) {
        Intrinsics.checkParameterIsNotNull(addCartResponse, "it");
        this.$this_addItemToCart.showProgress(false);
        AppBaseActivity appBaseActivity = this.$this_addItemToCart;
        String string = appBaseActivity.getString(R.string.success_add);
        Intrinsics.checkExpressionValueIsNotNull(string, "getString(R.string.success_add)");
        ExtensionsKt.snackBar$default(appBaseActivity, string, 0, 2, (Object) null);
        this.$onApiSuccess.invoke(addCartResponse);
    }
}
