package com.iqonic.store.utils.extensions;

import android.util.Log;
import com.iqonic.store.AppBaseActivity;
import com.iqonic.store.models.ProfileImage;
import com.iqonic.store.utils.Constants;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n¢\u0006\u0002\b\u0004"}, d2 = {"<anonymous>", "", "it", "Lcom/iqonic/store/models/ProfileImage;", "invoke"}, k = 3, mv = {1, 1, 16})
/* compiled from: NetworkExtension.kt */
final class NetworkExtensionKt$saveProfileImage$1 extends Lambda implements Function1<ProfileImage, Unit> {
    final /* synthetic */ Function1 $onApiSuccess;
    final /* synthetic */ AppBaseActivity $this_saveProfileImage;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    NetworkExtensionKt$saveProfileImage$1(AppBaseActivity appBaseActivity, Function1 function1) {
        super(1);
        this.$this_saveProfileImage = appBaseActivity;
        this.$onApiSuccess = function1;
    }

    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        invoke((ProfileImage) obj);
        return Unit.INSTANCE;
    }

    public final void invoke(ProfileImage profileImage) {
        Intrinsics.checkParameterIsNotNull(profileImage, "it");
        this.$this_saveProfileImage.showProgress(false);
        Log.e("res", profileImage.getProfile_image());
        AppExtensionsKt.getSharedPrefInstance().removeKey(Constants.SharedPref.USER_PROFILE);
        AppExtensionsKt.getSharedPrefInstance().setValue(Constants.SharedPref.USER_PROFILE, profileImage.getProfile_image());
        AppExtensionsKt.sendProfileUpdateBroadcast(this.$this_saveProfileImage);
        this.$this_saveProfileImage.setResult(-1);
        this.$this_saveProfileImage.finish();
        this.$onApiSuccess.invoke(true);
    }
}
