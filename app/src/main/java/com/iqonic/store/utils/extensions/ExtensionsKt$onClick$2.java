package com.iqonic.store.utils.extensions;

import android.view.View;
import androidx.recyclerview.widget.RecyclerView;
import kotlin.Metadata;
import kotlin.jvm.functions.Function3;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0010\u0000\u001a\u00020\u0001\"\b\b\u0000\u0010\u0002*\u00020\u00032\u000e\u0010\u0004\u001a\n \u0006*\u0004\u0018\u00010\u00050\u0005H\n¢\u0006\u0002\b\u0007"}, d2 = {"<anonymous>", "", "T", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "it", "Landroid/view/View;", "kotlin.jvm.PlatformType", "onClick"}, k = 3, mv = {1, 1, 16})
/* compiled from: Extensions.kt */
final class ExtensionsKt$onClick$2 implements View.OnClickListener {
    final /* synthetic */ Function3 $event;
    final /* synthetic */ RecyclerView.ViewHolder $this_onClick;

    ExtensionsKt$onClick$2(RecyclerView.ViewHolder viewHolder, Function3 function3) {
        this.$this_onClick = viewHolder;
        this.$event = function3;
    }

    public final void onClick(View view) {
        Function3 function3 = this.$event;
        Intrinsics.checkExpressionValueIsNotNull(view, "it");
        function3.invoke(view, Integer.valueOf(this.$this_onClick.getAdapterPosition()), Integer.valueOf(this.$this_onClick.getItemViewType()));
    }
}
