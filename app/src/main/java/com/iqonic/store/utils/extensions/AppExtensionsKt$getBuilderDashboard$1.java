package com.iqonic.store.utils.extensions;

import com.google.gson.reflect.TypeToken;
import com.iqonic.store.models.BuilderDashboard;
import kotlin.Metadata;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000f\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000*\u0001\u0000\b\n\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001¨\u0006\u0003"}, d2 = {"com/iqonic/store/utils/extensions/AppExtensionsKt$getBuilderDashboard$1", "Lcom/google/gson/reflect/TypeToken;", "Lcom/iqonic/store/models/BuilderDashboard;", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: AppExtensions.kt */
public final class AppExtensionsKt$getBuilderDashboard$1 extends TypeToken<BuilderDashboard> {
    AppExtensionsKt$getBuilderDashboard$1() {
    }
}
