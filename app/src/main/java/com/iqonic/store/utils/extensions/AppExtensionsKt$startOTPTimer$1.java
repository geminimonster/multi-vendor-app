package com.iqonic.store.utils.extensions;

import android.os.CountDownTimer;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.StringCompanionObject;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0019\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0000*\u0001\u0000\b\n\u0018\u00002\u00020\u0001J\b\u0010\u0002\u001a\u00020\u0003H\u0016J\u0010\u0010\u0004\u001a\u00020\u00032\u0006\u0010\u0005\u001a\u00020\u0006H\u0016¨\u0006\u0007"}, d2 = {"com/iqonic/store/utils/extensions/AppExtensionsKt$startOTPTimer$1", "Landroid/os/CountDownTimer;", "onFinish", "", "onTick", "millisUntilFinished", "", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: AppExtensions.kt */
public final class AppExtensionsKt$startOTPTimer$1 extends CountDownTimer {
    final /* synthetic */ Function0 $onTimerFinished;
    final /* synthetic */ Function1 $onTimerTick;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    AppExtensionsKt$startOTPTimer$1(Function1 function1, Function0 function0, long j, long j2) {
        super(j, j2);
        this.$onTimerTick = function1;
        this.$onTimerFinished = function0;
    }

    public void onTick(long j) {
        Function1 function1 = this.$onTimerTick;
        StringCompanionObject stringCompanionObject = StringCompanionObject.INSTANCE;
        String format = String.format("00 : %d", Arrays.copyOf(new Object[]{Long.valueOf(TimeUnit.MILLISECONDS.toSeconds(j) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(j)))}, 1));
        Intrinsics.checkExpressionValueIsNotNull(format, "java.lang.String.format(format, *args)");
        function1.invoke(format);
    }

    public void onFinish() {
        this.$onTimerFinished.invoke();
    }
}
