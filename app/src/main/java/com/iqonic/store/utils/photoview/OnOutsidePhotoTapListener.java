package com.iqonic.store.utils.photoview;

import android.widget.ImageView;

public interface OnOutsidePhotoTapListener {
    void onOutsidePhotoTap(ImageView imageView);
}
