package com.iqonic.store.utils.photoview;

public interface OnViewDragListener {
    void onDrag(float f, float f2);
}
