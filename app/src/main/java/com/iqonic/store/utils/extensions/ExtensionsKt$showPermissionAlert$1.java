package com.iqonic.store.utils.extensions;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.view.View;
import com.google.android.material.snackbar.Snackbar;
import kotlin.Metadata;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n¢\u0006\u0002\b\u0005"}, d2 = {"<anonymous>", "", "it", "Landroid/view/View;", "kotlin.jvm.PlatformType", "onClick"}, k = 3, mv = {1, 1, 16})
/* compiled from: Extensions.kt */
final class ExtensionsKt$showPermissionAlert$1 implements View.OnClickListener {
    final /* synthetic */ Snackbar $snackBar;
    final /* synthetic */ Activity $this_showPermissionAlert;

    ExtensionsKt$showPermissionAlert$1(Activity activity, Snackbar snackbar) {
        this.$this_showPermissionAlert = activity;
        this.$snackBar = snackbar;
    }

    public final void onClick(View view) {
        Intent intent = new Intent();
        intent.setAction("android.settings.APPLICATION_DETAILS_SETTINGS");
        intent.setData(Uri.fromParts("package", this.$this_showPermissionAlert.getPackageName(), (String) null));
        this.$this_showPermissionAlert.startActivity(intent);
        this.$snackBar.dismiss();
    }
}
