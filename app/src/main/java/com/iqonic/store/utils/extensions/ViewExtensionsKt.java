package com.iqonic.store.utils.extensions;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.inputmethod.InputMethodManager;
import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;
import com.google.android.material.snackbar.Snackbar;
import com.iqonic.store.ShopHopApp;
import com.store.proshop.R;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.jvm.internal.Intrinsics;
import kotlin.math.MathKt;

@Metadata(bv = {1, 0, 3}, d1 = {"\u00006\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0007\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0002\b\t\n\u0002\u0010\u000e\n\u0000\u001a\u0016\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00062\u0006\u0010\b\u001a\u00020\t\u001a\u0012\u0010\n\u001a\u00020\u000b*\u00020\u00022\u0006\u0010\u0007\u001a\u00020\u0006\u001a\u0012\u0010\u0007\u001a\u00020\u0006*\u00020\f2\u0006\u0010\u0007\u001a\u00020\u0006\u001a\u0014\u0010\r\u001a\u00020\u000b*\u00020\u00022\b\b\u0002\u0010\u000e\u001a\u00020\u0006\u001a\n\u0010\u000f\u001a\u00020\u0006*\u00020\f\u001a\n\u0010\u0010\u001a\u00020\u000b*\u00020\u0002\u001a\n\u0010\u0011\u001a\u00020\u000b*\u00020\u0002\u001a\n\u0010\u0012\u001a\u00020\u0013*\u00020\u0002\u001a0\u0010\u0014\u001a\u00020\u000b*\u00020\u00022\u0006\u0010\u0015\u001a\u00020\u00062\b\b\u0002\u0010\u0016\u001a\u00020\u00062\b\b\u0002\u0010\u0017\u001a\u00020\t2\b\b\u0002\u0010\u0018\u001a\u00020\u0006\u001a\n\u0010\u0019\u001a\u00020\u000b*\u00020\u0002\u001a\n\u0010\u001a\u001a\u00020\u000b*\u00020\u0002\u001a\u0012\u0010\u001b\u001a\u00020\u000b*\u00020\u00022\u0006\u0010\u001c\u001a\u00020\u001d\"\u0015\u0010\u0000\u001a\u00020\u0001*\u00020\u00028F¢\u0006\u0006\u001a\u0004\b\u0003\u0010\u0004¨\u0006\u001e"}, d2 = {"res", "Landroid/content/res/Resources;", "Landroid/view/View;", "getRes", "(Landroid/view/View;)Landroid/content/res/Resources;", "adjustAlpha", "", "color", "factor", "", "changeBackgroundTint", "", "Landroid/content/Context;", "fadeIn", "duration", "getDisplayWidth", "hide", "hideSoftKeyboard", "isVisible", "", "setStrokedBackground", "backgroundColor", "strokeColor", "alpha", "strokeWidth", "show", "showSoftKeyboard", "snackBarError", "msg", "", "app_release"}, k = 2, mv = {1, 1, 16})
/* compiled from: ViewExtensions.kt */
public final class ViewExtensionsKt {
    public static final void snackBarError(View view, String str) {
        Intrinsics.checkParameterIsNotNull(view, "$this$snackBarError");
        Intrinsics.checkParameterIsNotNull(str, NotificationCompat.CATEGORY_MESSAGE);
        Snackbar make = Snackbar.make(view, (CharSequence) str, 0);
        Intrinsics.checkExpressionValueIsNotNull(make, "Snackbar.make(this, msg, Snackbar.LENGTH_LONG)");
        View view2 = make.getView();
        Intrinsics.checkExpressionValueIsNotNull(view2, "snackBar.view");
        view2.setBackgroundColor(ContextCompat.getColor(ShopHopApp.Companion.getAppInstance(), R.color.tomato));
        make.setTextColor(-1);
        make.show();
    }

    public static final void showSoftKeyboard(View view) {
        Intrinsics.checkParameterIsNotNull(view, "$this$showSoftKeyboard");
        Object systemService = view.getContext().getSystemService("input_method");
        if (systemService != null) {
            view.requestFocus();
            ((InputMethodManager) systemService).showSoftInput(view, 0);
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type android.view.inputmethod.InputMethodManager");
    }

    public static final void hideSoftKeyboard(View view) {
        Intrinsics.checkParameterIsNotNull(view, "$this$hideSoftKeyboard");
        Object systemService = view.getContext().getSystemService("input_method");
        if (systemService != null) {
            ((InputMethodManager) systemService).hideSoftInputFromWindow(view.getWindowToken(), 0);
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type android.view.inputmethod.InputMethodManager");
    }

    public static /* synthetic */ void fadeIn$default(View view, int i, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            i = 400;
        }
        fadeIn(view, i);
    }

    public static final void fadeIn(View view, int i) {
        Intrinsics.checkParameterIsNotNull(view, "$this$fadeIn");
        view.clearAnimation();
        AlphaAnimation alphaAnimation = new AlphaAnimation(view.getAlpha(), 1.0f);
        alphaAnimation.setDuration((long) i);
        view.startAnimation(alphaAnimation);
    }

    public static final Resources getRes(View view) {
        Intrinsics.checkParameterIsNotNull(view, "$this$res");
        Resources resources = view.getResources();
        Intrinsics.checkExpressionValueIsNotNull(resources, "resources");
        return resources;
    }

    public static final void show(View view) {
        Intrinsics.checkParameterIsNotNull(view, "$this$show");
        view.setVisibility(0);
    }

    public static final void hide(View view) {
        Intrinsics.checkParameterIsNotNull(view, "$this$hide");
        view.setVisibility(8);
    }

    public static final boolean isVisible(View view) {
        Intrinsics.checkParameterIsNotNull(view, "$this$isVisible");
        return view.getVisibility() == 0;
    }

    public static final void changeBackgroundTint(View view, int i) {
        Intrinsics.checkParameterIsNotNull(view, "$this$changeBackgroundTint");
        Drawable background = view.getBackground();
        if (background != null) {
            ((GradientDrawable) background).setColor(i);
            Drawable background2 = view.getBackground();
            if (background2 != null) {
                ((GradientDrawable) background2).setStroke(0, 0);
                Drawable background3 = view.getBackground();
                Intrinsics.checkExpressionValueIsNotNull(background3, "background");
                background3.setColorFilter(new PorterDuffColorFilter(i, PorterDuff.Mode.MULTIPLY));
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type android.graphics.drawable.GradientDrawable");
        }
        throw new TypeCastException("null cannot be cast to non-null type android.graphics.drawable.GradientDrawable");
    }

    public static /* synthetic */ void setStrokedBackground$default(View view, int i, int i2, float f, int i3, int i4, Object obj) {
        if ((i4 & 2) != 0) {
            i2 = 0;
        }
        if ((i4 & 4) != 0) {
            f = 1.0f;
        }
        if ((i4 & 8) != 0) {
            i3 = 3;
        }
        setStrokedBackground(view, i, i2, f, i3);
    }

    public static final void setStrokedBackground(View view, int i, int i2, float f, int i3) {
        Intrinsics.checkParameterIsNotNull(view, "$this$setStrokedBackground");
        Drawable background = view.getBackground();
        if (background != null) {
            GradientDrawable gradientDrawable = (GradientDrawable) background;
            gradientDrawable.setStroke(i3, i2);
            gradientDrawable.setColor(adjustAlpha(i, f));
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type android.graphics.drawable.GradientDrawable");
    }

    public static final int color(Context context, int i) {
        Intrinsics.checkParameterIsNotNull(context, "$this$color");
        return ContextCompat.getColor(context, i);
    }

    public static final int adjustAlpha(int i, float f) {
        return Color.argb(MathKt.roundToInt(((float) Color.alpha(i)) * f), Color.red(i), Color.green(i), Color.blue(i));
    }

    public static final int getDisplayWidth(Context context) {
        Intrinsics.checkParameterIsNotNull(context, "$this$getDisplayWidth");
        Resources resources = context.getResources();
        Intrinsics.checkExpressionValueIsNotNull(resources, "resources");
        return resources.getDisplayMetrics().widthPixels;
    }
}
