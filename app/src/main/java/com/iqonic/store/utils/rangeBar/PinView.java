package com.iqonic.store.utils.rangeBar;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.util.TypedValue;
import android.view.View;
import androidx.core.content.ContextCompat;
import com.store.proshop.R;

class PinView extends View {
    private static final float DEFAULT_THUMB_RADIUS_DP = 14.0f;
    private static final float MINIMUM_TARGET_RADIUS_DP = 24.0f;
    private IRangeBarFormatter formatter;
    private Rect mBounds = new Rect();
    private Paint mCircleBoundaryPaint;
    private Paint mCirclePaint;
    private float mCircleRadiusPx;
    private float mDensity;
    private boolean mHasBeenPressed = false;
    private boolean mIsPressed = false;
    private float mMaxPinFont = 24.0f;
    private float mMinPinFont = 8.0f;
    private Drawable mPin;
    private float mPinPadding;
    private int mPinRadiusPx;
    private boolean mPinsAreTemporary;
    private Resources mRes;
    private float mTargetRadiusPx;
    private Paint mTextPaint;
    private float mTextYPadding;
    private String mValue;
    private float mX;
    private float mY;
    private int pinColor;

    public PinView(Context context) {
        super(context);
    }

    public void setFormatter(IRangeBarFormatter iRangeBarFormatter) {
        this.formatter = iRangeBarFormatter;
    }

    public void init(Context context, float f, float f2, int i, int i2, float f3, int i3, int i4, float f4, float f5, float f6, boolean z) {
        this.mRes = context.getResources();
        this.mPin = ContextCompat.getDrawable(context, R.drawable.rotate);
        float f7 = getResources().getDisplayMetrics().density;
        this.mDensity = f7;
        this.mMinPinFont = f5 / f7;
        this.mMaxPinFont = f6 / f7;
        this.mPinsAreTemporary = z;
        this.mPinPadding = (float) ((int) TypedValue.applyDimension(1, 15.0f, this.mRes.getDisplayMetrics()));
        this.mCircleRadiusPx = f3;
        this.mTextYPadding = (float) ((int) TypedValue.applyDimension(1, 3.5f, this.mRes.getDisplayMetrics()));
        if (f2 == -1.0f) {
            this.mPinRadiusPx = (int) TypedValue.applyDimension(1, DEFAULT_THUMB_RADIUS_DP, this.mRes.getDisplayMetrics());
        } else {
            this.mPinRadiusPx = (int) TypedValue.applyDimension(1, f2, this.mRes.getDisplayMetrics());
        }
        Paint paint = new Paint();
        this.mTextPaint = paint;
        paint.setColor(i2);
        this.mTextPaint.setAntiAlias(true);
        this.mTextPaint.setTextSize((float) ((int) TypedValue.applyDimension(2, 15.0f, this.mRes.getDisplayMetrics())));
        Paint paint2 = new Paint();
        this.mCirclePaint = paint2;
        paint2.setColor(i3);
        this.mCirclePaint.setAntiAlias(true);
        Paint paint3 = new Paint();
        this.mCircleBoundaryPaint = paint3;
        paint3.setStyle(Paint.Style.STROKE);
        this.mCircleBoundaryPaint.setColor(-1);
        this.mCircleBoundaryPaint.setStrokeWidth(5.0f);
        this.mCircleBoundaryPaint.setAntiAlias(true);
        this.pinColor = i;
        this.mTargetRadiusPx = TypedValue.applyDimension(1, (float) ((int) Math.max(24.0f, (float) this.mPinRadiusPx)), this.mRes.getDisplayMetrics());
        this.mY = f;
    }

    public float getX() {
        return this.mX;
    }

    public void setX(float f) {
        this.mX = f;
    }

    public void setXValue(String str) {
        this.mValue = str;
    }

    public boolean isPressed() {
        return this.mIsPressed;
    }

    public void press() {
        this.mIsPressed = true;
        this.mHasBeenPressed = true;
    }

    public void setSize(float f, float f2) {
        this.mPinPadding = (float) ((int) f2);
        this.mPinRadiusPx = (int) f;
        invalidate();
    }

    public void release() {
        this.mIsPressed = false;
    }

    public boolean isInTargetZone(float f, float f2) {
        return Math.abs(f - this.mX) <= this.mTargetRadiusPx && Math.abs((f2 - this.mY) + this.mPinPadding) <= this.mTargetRadiusPx;
    }

    public void draw(Canvas canvas) {
        Paint paint = this.mCircleBoundaryPaint;
        if (paint != null) {
            canvas.drawCircle(this.mX, this.mY, this.mCircleRadiusPx, paint);
        }
        canvas.drawCircle(this.mX, this.mY, this.mCircleRadiusPx, this.mCirclePaint);
        super.draw(canvas);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:4:0x001d, code lost:
        if (r4 > r3) goto L_0x0017;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void calibrateTextSize(android.graphics.Paint r2, java.lang.String r3, float r4) {
        /*
            r1 = this;
            r0 = 1092616192(0x41200000, float:10.0)
            r2.setTextSize(r0)
            float r3 = r2.measureText(r3)
            r0 = 1090519040(0x41000000, float:8.0)
            float r4 = r4 * r0
            float r4 = r4 / r3
            float r3 = r1.mDensity
            float r4 = r4 / r3
            float r3 = r1.mMinPinFont
            int r0 = (r4 > r3 ? 1 : (r4 == r3 ? 0 : -1))
            if (r0 >= 0) goto L_0x0019
        L_0x0017:
            r4 = r3
            goto L_0x0020
        L_0x0019:
            float r3 = r1.mMaxPinFont
            int r0 = (r4 > r3 ? 1 : (r4 == r3 ? 0 : -1))
            if (r0 <= 0) goto L_0x0020
            goto L_0x0017
        L_0x0020:
            float r3 = r1.mDensity
            float r4 = r4 * r3
            r2.setTextSize(r4)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.iqonic.store.utils.rangeBar.PinView.calibrateTextSize(android.graphics.Paint, java.lang.String, float):void");
    }
}
