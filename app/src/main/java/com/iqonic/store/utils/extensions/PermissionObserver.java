package com.iqonic.store.utils.extensions;

import androidx.lifecycle.LifecycleObserver;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0005\b\u0002\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002R\"\u0010\u0003\u001a\n\u0012\u0004\u0012\u00020\u0005\u0018\u00010\u0004X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0006\u0010\u0007\"\u0004\b\b\u0010\t¨\u0006\n"}, d2 = {"Lcom/iqonic/store/utils/extensions/PermissionObserver;", "Landroidx/lifecycle/LifecycleObserver;", "()V", "onResumeCallback", "Lkotlin/Function0;", "", "getOnResumeCallback", "()Lkotlin/jvm/functions/Function0;", "setOnResumeCallback", "(Lkotlin/jvm/functions/Function0;)V", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: Extensions.kt */
final class PermissionObserver implements LifecycleObserver {
    private Function0<Unit> onResumeCallback;

    public final Function0<Unit> getOnResumeCallback() {
        return this.onResumeCallback;
    }

    public final void setOnResumeCallback(Function0<Unit> function0) {
        this.onResumeCallback = function0;
    }
}
