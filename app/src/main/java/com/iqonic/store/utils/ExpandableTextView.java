package com.iqonic.store.utils;

import android.animation.TimeInterpolator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.FrameLayout;
import android.widget.TextView;
import com.google.android.material.badge.BadgeDrawable;
import com.iqonic.store.utils.extensions.ViewExtensionsKt;
import com.store.proshop.R;
import java.util.HashMap;
import java.util.Random;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000Z\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0010\t\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\u000b\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\r\n\u0002\u0010\u0002\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u000f\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004B\u0017\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006¢\u0006\u0002\u0010\u0007B\u001f\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\b\u001a\u00020\t¢\u0006\u0002\u0010\nB'\b\u0017\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\b\u001a\u00020\t\u0012\u0006\u0010\u000b\u001a\u00020\t¢\u0006\u0002\u0010\fJ\u0012\u00105\u001a\u0002062\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006H\u0002J\u0006\u00107\u001a\u000206J\u0006\u00108\u001a\u000206J\b\u00109\u001a\u00020\tH\u0002J\u0012\u0010:\u001a\u0002062\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006H\u0002J\u000e\u0010;\u001a\u0002062\u0006\u0010<\u001a\u00020=J\u0006\u0010>\u001a\u000206J\u0010\u0010>\u001a\u0002062\u0006\u00108\u001a\u00020\u001fH\u0002R\u001a\u0010\r\u001a\u00020\u000eX\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u000f\u0010\u0010\"\u0004\b\u0011\u0010\u0012R\u0010\u0010\u0013\u001a\u0004\u0018\u00010\u0014X\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\tX\u000e¢\u0006\u0002\n\u0000R\u001c\u0010\u0016\u001a\u0004\u0018\u00010\u0017X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0018\u0010\u0019\"\u0004\b\u001a\u0010\u001bR\u000e\u0010\u001c\u001a\u00020\tX\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\u001d\u001a\u0004\u0018\u00010\u0014X\u000e¢\u0006\u0002\n\u0000R\u001e\u0010 \u001a\u00020\u001f2\u0006\u0010\u001e\u001a\u00020\u001f@BX\u000e¢\u0006\b\n\u0000\u001a\u0004\b \u0010!R$\u0010#\u001a\u00020\u001f2\u0006\u0010\"\u001a\u00020\u001f@FX\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b#\u0010!\"\u0004\b$\u0010%R\u0010\u0010&\u001a\u0004\u0018\u00010\u0003X\u000e¢\u0006\u0002\n\u0000R\u0010\u0010'\u001a\u0004\u0018\u00010(X\u000e¢\u0006\u0002\n\u0000R\u001a\u0010)\u001a\u00020\tX\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b*\u0010+\"\u0004\b,\u0010-R\u001c\u0010.\u001a\u0004\u0018\u00010\u0017X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b/\u0010\u0019\"\u0004\b0\u0010\u001bR$\u00101\u001a\u00020\t2\u0006\u00101\u001a\u00020\t@FX\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b2\u0010+\"\u0004\b3\u0010-R\u000e\u00104\u001a\u00020\tX\u000e¢\u0006\u0002\n\u0000¨\u0006?"}, d2 = {"Lcom/iqonic/store/utils/ExpandableTextView;", "Landroid/widget/FrameLayout;", "context", "Landroid/content/Context;", "(Landroid/content/Context;)V", "attrs", "Landroid/util/AttributeSet;", "(Landroid/content/Context;Landroid/util/AttributeSet;)V", "defStyleAttr", "", "(Landroid/content/Context;Landroid/util/AttributeSet;I)V", "defStyleRes", "(Landroid/content/Context;Landroid/util/AttributeSet;II)V", "animationDuration", "", "getAnimationDuration", "()J", "setAnimationDuration", "(J)V", "collapseInterpolator", "Landroid/animation/TimeInterpolator;", "collapsedHeight", "content", "Landroid/widget/TextView;", "getContent", "()Landroid/widget/TextView;", "setContent", "(Landroid/widget/TextView;)V", "contentTextStyle", "expandInterpolator", "<set-?>", "", "isDefaultExpand", "()Z", "moreLessShow", "isMoreLessShow", "setMoreLessShow", "(Z)V", "mContext", "mMainLayout", "Landroid/view/View;", "maxLine", "getMaxLine", "()I", "setMaxLine", "(I)V", "moreLess", "getMoreLess", "setMoreLess", "moreLessGravity", "getMoreLessGravity", "setMoreLessGravity", "moreLessTextStyle", "applyXmlAttributes", "", "collapse", "expand", "getID", "init", "setText", "text", "", "toggle", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: ExpandableTextView.kt */
public final class ExpandableTextView extends FrameLayout {
    private HashMap _$_findViewCache;
    private long animationDuration = 300;
    private TimeInterpolator collapseInterpolator;
    private int collapsedHeight;
    private TextView content;
    private int contentTextStyle;
    private TimeInterpolator expandInterpolator;
    private boolean isDefaultExpand;
    private boolean isMoreLessShow;
    private Context mContext;
    private View mMainLayout;
    private int maxLine = Integer.MAX_VALUE;
    private TextView moreLess;
    private int moreLessGravity;
    private int moreLessTextStyle;

    public void _$_clearFindViewByIdCache() {
        HashMap hashMap = this._$_findViewCache;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    public View _$_findCachedViewById(int i) {
        if (this._$_findViewCache == null) {
            this._$_findViewCache = new HashMap();
        }
        View view = (View) this._$_findViewCache.get(Integer.valueOf(i));
        if (view != null) {
            return view;
        }
        View findViewById = findViewById(i);
        this._$_findViewCache.put(Integer.valueOf(i), findViewById);
        return findViewById;
    }

    public final TextView getContent() {
        return this.content;
    }

    public final void setContent(TextView textView) {
        this.content = textView;
    }

    public final TextView getMoreLess() {
        return this.moreLess;
    }

    public final void setMoreLess(TextView textView) {
        this.moreLess = textView;
    }

    public final int getMaxLine() {
        return this.maxLine;
    }

    public final void setMaxLine(int i) {
        this.maxLine = i;
    }

    public final long getAnimationDuration() {
        return this.animationDuration;
    }

    public final void setAnimationDuration(long j) {
        this.animationDuration = j;
    }

    public final int getMoreLessGravity() {
        return this.moreLessGravity;
    }

    public final void setMoreLessGravity(int i) {
        this.moreLessGravity = i;
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(-2, -2);
        layoutParams.gravity = BadgeDrawable.BOTTOM_END;
        TextView textView = this.moreLess;
        if (textView == null) {
            Intrinsics.throwNpe();
        }
        textView.setLayoutParams(layoutParams);
    }

    public final boolean isMoreLessShow() {
        return this.isMoreLessShow;
    }

    public final void setMoreLessShow(boolean z) {
        this.isMoreLessShow = z;
        if (z) {
            TextView textView = this.moreLess;
            if (textView == null) {
                Intrinsics.throwNpe();
            }
            ViewExtensionsKt.show(textView);
            return;
        }
        TextView textView2 = this.moreLess;
        if (textView2 == null) {
            Intrinsics.throwNpe();
        }
        ViewExtensionsKt.hide(textView2);
    }

    public final boolean isDefaultExpand() {
        return this.isDefaultExpand;
    }

    private final int getID() {
        return new Random().nextInt(Integer.MAX_VALUE);
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ExpandableTextView(Context context) {
        super(context);
        Intrinsics.checkParameterIsNotNull(context, "context");
        this.mContext = context;
        init((AttributeSet) null);
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ExpandableTextView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        Intrinsics.checkParameterIsNotNull(context, "context");
        Intrinsics.checkParameterIsNotNull(attributeSet, "attrs");
        this.mContext = context;
        init(attributeSet);
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ExpandableTextView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        Intrinsics.checkParameterIsNotNull(context, "context");
        Intrinsics.checkParameterIsNotNull(attributeSet, "attrs");
        this.mContext = context;
        init(attributeSet);
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ExpandableTextView(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
        Intrinsics.checkParameterIsNotNull(context, "context");
        Intrinsics.checkParameterIsNotNull(attributeSet, "attrs");
        this.mContext = context;
        init(attributeSet);
    }

    private final void init(AttributeSet attributeSet) {
        this.mMainLayout = View.inflate(getContext(), R.layout.expandable_text_view_layout, this);
        View findViewById = findViewById(R.id.content);
        if (findViewById != null) {
            this.content = (TextView) findViewById;
            View findViewById2 = findViewById(R.id.moreLess);
            if (findViewById2 != null) {
                this.moreLess = (TextView) findViewById2;
                TextView textView = this.content;
                if (textView == null) {
                    Intrinsics.throwNpe();
                }
                textView.setOnClickListener(new ExpandableTextView$init$1(this));
                TextView textView2 = this.moreLess;
                if (textView2 == null) {
                    Intrinsics.throwNpe();
                }
                textView2.setOnClickListener(new ExpandableTextView$init$2(this));
                TextView textView3 = this.content;
                if (textView3 == null) {
                    Intrinsics.throwNpe();
                }
                textView3.setId(getID());
                this.expandInterpolator = new AccelerateDecelerateInterpolator();
                this.collapseInterpolator = new AccelerateDecelerateInterpolator();
                applyXmlAttributes(attributeSet);
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type android.widget.TextView");
        }
        throw new TypeCastException("null cannot be cast to non-null type android.widget.TextView");
    }

    private final void applyXmlAttributes(AttributeSet attributeSet) {
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = getContext().obtainStyledAttributes(attributeSet, com.iqonic.store.R.styleable.ExpandableTextView);
            try {
                this.maxLine = 3;
                this.isDefaultExpand = true;
                TextView textView = this.content;
                if (textView == null) {
                    Intrinsics.throwNpe();
                }
                textView.setText(obtainStyledAttributes.getString(10));
                if (this.isDefaultExpand) {
                    TextView textView2 = this.content;
                    if (textView2 == null) {
                        Intrinsics.throwNpe();
                    }
                    textView2.setMaxLines(Integer.MAX_VALUE);
                    TextView textView3 = this.moreLess;
                    if (textView3 == null) {
                        Intrinsics.throwNpe();
                    }
                    textView3.setText(getResources().getString(R.string.more));
                } else {
                    TextView textView4 = this.content;
                    if (textView4 == null) {
                        Intrinsics.throwNpe();
                    }
                    textView4.setMaxLines(this.maxLine);
                    TextView textView5 = this.moreLess;
                    if (textView5 == null) {
                        Intrinsics.throwNpe();
                    }
                    textView5.setText(getResources().getString(R.string.less));
                }
                TextView textView6 = this.moreLess;
                if (textView6 == null) {
                    Intrinsics.throwNpe();
                }
                textView6.setAllCaps(false);
                setMoreLessShow(true);
                setMoreLessGravity(obtainStyledAttributes.getInt(5, 3));
                this.moreLessTextStyle = obtainStyledAttributes.getInt(9, 0);
                this.contentTextStyle = obtainStyledAttributes.getInt(0, 0);
                this.animationDuration = (long) obtainStyledAttributes.getInt(1, 300);
            } finally {
                obtainStyledAttributes.recycle();
            }
        }
    }

    public final void toggle() {
        TextView textView = this.moreLess;
        if (textView == null) {
            Intrinsics.throwNpe();
        }
        if (Intrinsics.areEqual((Object) textView.getText(), (Object) getResources().getString(R.string.more))) {
            toggle(true);
        } else {
            toggle(false);
        }
    }

    private final void toggle(boolean z) {
        if (z) {
            expand();
            TextView textView = this.moreLess;
            if (textView == null) {
                Intrinsics.throwNpe();
            }
            textView.setText(getResources().getString(R.string.less));
            TextView textView2 = this.moreLess;
            if (textView2 == null) {
                Intrinsics.throwNpe();
            }
            textView2.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_arrow_up, 0);
            return;
        }
        collapse();
        TextView textView3 = this.moreLess;
        if (textView3 == null) {
            Intrinsics.throwNpe();
        }
        textView3.setText(getResources().getString(R.string.more));
        TextView textView4 = this.moreLess;
        if (textView4 == null) {
            Intrinsics.throwNpe();
        }
        textView4.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_arrow_down, 0);
    }

    public final void setText(String str) {
        Intrinsics.checkParameterIsNotNull(str, "text");
        TextView textView = this.content;
        if (textView == null) {
            Intrinsics.throwNpe();
        }
        textView.setMaxLines(Integer.MAX_VALUE);
        TextView textView2 = this.content;
        if (textView2 == null) {
            Intrinsics.throwNpe();
        }
        textView2.setText(str);
        TextView textView3 = this.content;
        if (textView3 == null) {
            Intrinsics.throwNpe();
        }
        textView3.getViewTreeObserver().addOnGlobalLayoutListener(new ExpandableTextView$setText$1(this));
    }

    public final void expand() {
        TextView textView = this.content;
        if (textView == null) {
            Intrinsics.throwNpe();
        }
        TextView textView2 = this.content;
        if (textView2 == null) {
            Intrinsics.throwNpe();
        }
        textView.measure(View.MeasureSpec.makeMeasureSpec(textView2.getMeasuredWidth(), 1073741824), View.MeasureSpec.makeMeasureSpec(0, 0));
        TextView textView3 = this.content;
        if (textView3 == null) {
            Intrinsics.throwNpe();
        }
        this.collapsedHeight = textView3.getMeasuredHeight();
        TextView textView4 = this.content;
        if (textView4 == null) {
            Intrinsics.throwNpe();
        }
        textView4.setMaxLines(Integer.MAX_VALUE);
        TextView textView5 = this.content;
        if (textView5 == null) {
            Intrinsics.throwNpe();
        }
        TextView textView6 = this.content;
        if (textView6 == null) {
            Intrinsics.throwNpe();
        }
        textView5.measure(View.MeasureSpec.makeMeasureSpec(textView6.getMeasuredWidth(), 1073741824), View.MeasureSpec.makeMeasureSpec(0, 0));
        TextView textView7 = this.content;
        if (textView7 == null) {
            Intrinsics.throwNpe();
        }
        ValueAnimator ofInt = ValueAnimator.ofInt(new int[]{this.collapsedHeight, textView7.getMeasuredHeight()});
        ofInt.addUpdateListener(new ExpandableTextView$expand$1(this));
        ofInt.addListener(new ExpandableTextView$expand$2(this));
        Intrinsics.checkExpressionValueIsNotNull(ofInt, "valueAnimator");
        ofInt.setInterpolator(this.expandInterpolator);
        ofInt.setDuration(this.animationDuration).start();
    }

    public final void collapse() {
        TextView textView = this.content;
        if (textView == null) {
            Intrinsics.throwNpe();
        }
        ValueAnimator ofInt = ValueAnimator.ofInt(new int[]{textView.getMeasuredHeight(), this.collapsedHeight});
        ofInt.addUpdateListener(new ExpandableTextView$collapse$1(this));
        ofInt.addListener(new ExpandableTextView$collapse$2(this));
        Intrinsics.checkExpressionValueIsNotNull(ofInt, "valueAnimator");
        ofInt.setInterpolator(this.collapseInterpolator);
        ofInt.setDuration(this.animationDuration).start();
    }
}
