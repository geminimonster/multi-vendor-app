package com.iqonic.store.utils;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0017\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000*\u0001\u0000\b\n\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\u0016¨\u0006\u0006"}, d2 = {"com/iqonic/store/utils/ExpandableTextView$expand$2", "Landroid/animation/AnimatorListenerAdapter;", "onAnimationEnd", "", "animation", "Landroid/animation/Animator;", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: ExpandableTextView.kt */
public final class ExpandableTextView$expand$2 extends AnimatorListenerAdapter {
    final /* synthetic */ ExpandableTextView this$0;

    ExpandableTextView$expand$2(ExpandableTextView expandableTextView) {
        this.this$0 = expandableTextView;
    }

    public void onAnimationEnd(Animator animator) {
        Intrinsics.checkParameterIsNotNull(animator, "animation");
        TextView content = this.this$0.getContent();
        if (content == null) {
            Intrinsics.throwNpe();
        }
        ViewGroup.LayoutParams layoutParams = content.getLayoutParams();
        layoutParams.height = -2;
        if (layoutParams != null) {
            ((FrameLayout.LayoutParams) layoutParams).bottomMargin = 70;
            TextView content2 = this.this$0.getContent();
            if (content2 == null) {
                Intrinsics.throwNpe();
            }
            content2.setLayoutParams(layoutParams);
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type android.widget.FrameLayout.LayoutParams");
    }
}
