package com.iqonic.store.utils;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.core.content.ContextCompat;
import com.iqonic.store.R;
import java.util.Arrays;
import java.util.HashMap;
import kotlin.Deprecated;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.StringCompanionObject;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\r\n\u0002\u0010\u000b\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0007\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0014\n\u0002\u0018\u0002\n\u0002\b\u000e\u0018\u0000 m2\u00020\u0001:\u0001mB\u000f\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004B!\b\u0017\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\b\b\u0002\u0010\u0007\u001a\u00020\b¢\u0006\u0002\u0010\tJ\b\u0010>\u001a\u00020?H\u0002J\b\u0010@\u001a\u00020,H\u0002J\u0014\u0010A\u001a\u0004\u0018\u00010\u001f2\b\u0010B\u001a\u0004\u0018\u00010CH\u0002J\n\u0010D\u001a\u0004\u0018\u00010/H\u0016J\b\u0010E\u001a\u00020FH\u0016J\b\u0010G\u001a\u00020?H\u0002J\b\u0010H\u001a\u00020?H\u0002J\u0010\u0010I\u001a\u00020?2\u0006\u0010J\u001a\u00020KH\u0014J(\u0010L\u001a\u00020?2\u0006\u0010M\u001a\u00020\b2\u0006\u0010N\u001a\u00020\b2\u0006\u0010O\u001a\u00020\b2\u0006\u0010P\u001a\u00020\bH\u0014J\u0010\u0010Q\u001a\u00020?2\u0006\u0010R\u001a\u00020\u0016H\u0016J\u0012\u0010S\u001a\u00020?2\b\b\u0001\u0010T\u001a\u00020\bH\u0007J\u0010\u0010U\u001a\u00020?2\u0006\u0010V\u001a\u00020/H\u0016J\u0012\u0010W\u001a\u00020?2\b\b\u0001\u0010X\u001a\u00020\bH\u0007J\u0010\u0010Y\u001a\u00020?2\u0006\u0010Z\u001a\u00020\u001fH\u0016J\u0012\u0010[\u001a\u00020?2\b\u0010B\u001a\u0004\u0018\u00010CH\u0016J\u0012\u0010\\\u001a\u00020?2\b\b\u0001\u0010]\u001a\u00020\bH\u0016J\u0012\u0010^\u001a\u00020?2\b\u0010_\u001a\u0004\u0018\u00010`H\u0016J(\u0010a\u001a\u00020?2\u0006\u0010b\u001a\u00020\b2\u0006\u0010c\u001a\u00020\b2\u0006\u0010d\u001a\u00020\b2\u0006\u0010e\u001a\u00020\bH\u0016J(\u0010f\u001a\u00020?2\u0006\u0010g\u001a\u00020\b2\u0006\u0010c\u001a\u00020\b2\u0006\u0010h\u001a\u00020\b2\u0006\u0010e\u001a\u00020\bH\u0016J\u0010\u0010i\u001a\u00020?2\u0006\u0010j\u001a\u00020FH\u0016J\b\u0010k\u001a\u00020?H\u0002J\b\u0010l\u001a\u00020?H\u0002R&\u0010\n\u001a\u00020\b2\b\b\u0001\u0010\n\u001a\u00020\b8F@FX\u000e¢\u0006\f\u001a\u0004\b\u000b\u0010\f\"\u0004\b\r\u0010\u000eR$\u0010\u000f\u001a\u00020\b2\u0006\u0010\u000f\u001a\u00020\b8F@FX\u000e¢\u0006\f\u001a\u0004\b\u0010\u0010\f\"\u0004\b\u0011\u0010\u000eR&\u0010\u0012\u001a\u00020\b2\b\b\u0001\u0010\u0012\u001a\u00020\b8G@GX\u000e¢\u0006\f\u001a\u0004\b\u0013\u0010\f\"\u0004\b\u0014\u0010\u000eR$\u0010\u0017\u001a\u00020\u00162\u0006\u0010\u0015\u001a\u00020\u00168F@FX\u000e¢\u0006\f\u001a\u0004\b\u0017\u0010\u0018\"\u0004\b\u0019\u0010\u001aR$\u0010\u001c\u001a\u00020\u00162\u0006\u0010\u001b\u001a\u00020\u0016@FX\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u001c\u0010\u0018\"\u0004\b\u001d\u0010\u001aR\u0010\u0010\u001e\u001a\u0004\u0018\u00010\u001fX\u000e¢\u0006\u0002\n\u0000R\u000e\u0010 \u001a\u00020\bX\u000e¢\u0006\u0002\n\u0000R\u000e\u0010!\u001a\u00020\"X\u0004¢\u0006\u0002\n\u0000R\u0010\u0010#\u001a\u0004\u0018\u00010$X\u000e¢\u0006\u0002\n\u0000R\u000e\u0010%\u001a\u00020\bX\u000e¢\u0006\u0002\n\u0000R\u000e\u0010&\u001a\u00020\bX\u000e¢\u0006\u0002\n\u0000R\u000e\u0010'\u001a\u00020\u0016X\u000e¢\u0006\u0002\n\u0000R\u000e\u0010(\u001a\u00020\"X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010)\u001a\u00020*X\u000e¢\u0006\u0002\n\u0000R\u000e\u0010+\u001a\u00020,X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010-\u001a\u00020\bX\u000e¢\u0006\u0002\n\u0000R\u0010\u0010.\u001a\u0004\u0018\u00010/X\u000e¢\u0006\u0002\n\u0000R\u000e\u00100\u001a\u00020*X\u000e¢\u0006\u0002\n\u0000R\u000e\u00101\u001a\u00020,X\u0004¢\u0006\u0002\n\u0000R\u000e\u00102\u001a\u00020\bX\u000e¢\u0006\u0002\n\u0000R\u000e\u00103\u001a\u00020\"X\u0004¢\u0006\u0002\n\u0000R\u000e\u00104\u001a\u00020\u0016X\u000e¢\u0006\u0002\n\u0000R\u000e\u00105\u001a\u00020\u0016X\u000e¢\u0006\u0002\n\u0000R\u000e\u00106\u001a\u000207X\u0004¢\u0006\u0002\n\u0000R\u001c\u00108\u001a\u0004\u0018\u000109X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b:\u0010;\"\u0004\b<\u0010=¨\u0006n"}, d2 = {"Lcom/iqonic/store/utils/CircleImageView;", "Landroidx/appcompat/widget/AppCompatImageView;", "context", "Landroid/content/Context;", "(Landroid/content/Context;)V", "attrs", "Landroid/util/AttributeSet;", "defStyle", "", "(Landroid/content/Context;Landroid/util/AttributeSet;I)V", "borderColor", "getBorderColor", "()I", "setBorderColor", "(I)V", "borderWidth", "getBorderWidth", "setBorderWidth", "fillColor", "getFillColor", "setFillColor", "borderOverlay", "", "isBorderOverlay", "()Z", "setBorderOverlay", "(Z)V", "disableCircularTransformation", "isDisableCircularTransformation", "setDisableCircularTransformation", "mBitmap", "Landroid/graphics/Bitmap;", "mBitmapHeight", "mBitmapPaint", "Landroid/graphics/Paint;", "mBitmapShader", "Landroid/graphics/BitmapShader;", "mBitmapWidth", "mBorderColor", "mBorderOverlay", "mBorderPaint", "mBorderRadius", "", "mBorderRect", "Landroid/graphics/RectF;", "mBorderWidth", "mColorFilter", "Landroid/graphics/ColorFilter;", "mDrawableRadius", "mDrawableRect", "mFillColor", "mFillPaint", "mReady", "mSetupPending", "mShaderMatrix", "Landroid/graphics/Matrix;", "name", "", "getName", "()Ljava/lang/String;", "setName", "(Ljava/lang/String;)V", "applyColorFilter", "", "calculateBounds", "getBitmapFromDrawable", "drawable", "Landroid/graphics/drawable/Drawable;", "getColorFilter", "getScaleType", "Landroid/widget/ImageView$ScaleType;", "init", "initializeBitmap", "onDraw", "canvas", "Landroid/graphics/Canvas;", "onSizeChanged", "w", "h", "oldw", "oldh", "setAdjustViewBounds", "adjustViewBounds", "setBorderColorResource", "borderColorRes", "setColorFilter", "cf", "setFillColorResource", "fillColorRes", "setImageBitmap", "bm", "setImageDrawable", "setImageResource", "resId", "setImageURI", "uri", "Landroid/net/Uri;", "setPadding", "left", "top", "right", "bottom", "setPaddingRelative", "start", "end", "setScaleType", "scaleType", "setup", "updateShaderMatrix", "Companion", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: CircleImageView.kt */
public final class CircleImageView extends AppCompatImageView {
    private static final Bitmap.Config BITMAP_CONFIG = Bitmap.Config.ARGB_8888;
    private static final int COLORDRAWABLE_DIMENSION = 2;
    public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
    private static final int DEFAULT_BORDER_COLOR = -16777216;
    private static final boolean DEFAULT_BORDER_OVERLAY = false;
    private static final int DEFAULT_BORDER_WIDTH = 0;
    private static final int DEFAULT_FILL_COLOR = 0;
    private static final ImageView.ScaleType SCALE_TYPE = ImageView.ScaleType.CENTER_CROP;
    private HashMap _$_findViewCache;
    private boolean isDisableCircularTransformation;
    private Bitmap mBitmap;
    private int mBitmapHeight;
    private final Paint mBitmapPaint;
    private BitmapShader mBitmapShader;
    private int mBitmapWidth;
    private int mBorderColor;
    private boolean mBorderOverlay;
    private final Paint mBorderPaint;
    private float mBorderRadius;
    private final RectF mBorderRect;
    private int mBorderWidth;
    private ColorFilter mColorFilter;
    private float mDrawableRadius;
    private final RectF mDrawableRect;
    private int mFillColor;
    private final Paint mFillPaint;
    private boolean mReady;
    private boolean mSetupPending;
    private final Matrix mShaderMatrix;
    private String name;

    public CircleImageView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0, 4, (DefaultConstructorMarker) null);
    }

    public void _$_clearFindViewByIdCache() {
        HashMap hashMap = this._$_findViewCache;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    public View _$_findCachedViewById(int i) {
        if (this._$_findViewCache == null) {
            this._$_findViewCache = new HashMap();
        }
        View view = (View) this._$_findViewCache.get(Integer.valueOf(i));
        if (view != null) {
            return view;
        }
        View findViewById = findViewById(i);
        this._$_findViewCache.put(Integer.valueOf(i), findViewById);
        return findViewById;
    }

    public final String getName() {
        return this.name;
    }

    public final void setName(String str) {
        this.name = str;
    }

    public final boolean isDisableCircularTransformation() {
        return this.isDisableCircularTransformation;
    }

    public final void setDisableCircularTransformation(boolean z) {
        if (this.isDisableCircularTransformation != z) {
            this.isDisableCircularTransformation = z;
            initializeBitmap();
        }
    }

    public final int getBorderColor() {
        return this.mBorderColor;
    }

    public final void setBorderColor(int i) {
        if (i != this.mBorderColor) {
            this.mBorderColor = i;
            this.mBorderPaint.setColor(i);
            invalidate();
        }
    }

    @Deprecated(message = "Fill Color support is going to be removed in the future")
    public final int getFillColor() {
        return this.mFillColor;
    }

    @Deprecated(message = "Fill Color support is going to be removed in the future")
    public final void setFillColor(int i) {
        if (i != this.mFillColor) {
            this.mFillColor = i;
            this.mFillPaint.setColor(i);
            invalidate();
        }
    }

    public final int getBorderWidth() {
        return this.mBorderWidth;
    }

    public final void setBorderWidth(int i) {
        if (i != this.mBorderWidth) {
            this.mBorderWidth = i;
            setup();
        }
    }

    public final boolean isBorderOverlay() {
        return this.mBorderOverlay;
    }

    public final void setBorderOverlay(boolean z) {
        if (z != this.mBorderOverlay) {
            this.mBorderOverlay = z;
            setup();
        }
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CircleImageView(Context context) {
        super(context);
        Intrinsics.checkParameterIsNotNull(context, "context");
        this.mDrawableRect = new RectF();
        this.mBorderRect = new RectF();
        this.mShaderMatrix = new Matrix();
        this.mBitmapPaint = new Paint();
        this.mBorderPaint = new Paint();
        this.mFillPaint = new Paint();
        this.mBorderColor = DEFAULT_BORDER_COLOR;
        this.mBorderWidth = DEFAULT_BORDER_WIDTH;
        this.mFillColor = DEFAULT_FILL_COLOR;
        init();
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CircleImageView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        Intrinsics.checkParameterIsNotNull(context, "context");
        Intrinsics.checkParameterIsNotNull(attributeSet, "attrs");
        this.mDrawableRect = new RectF();
        this.mBorderRect = new RectF();
        this.mShaderMatrix = new Matrix();
        this.mBitmapPaint = new Paint();
        this.mBorderPaint = new Paint();
        this.mFillPaint = new Paint();
        this.mBorderColor = DEFAULT_BORDER_COLOR;
        this.mBorderWidth = DEFAULT_BORDER_WIDTH;
        this.mFillColor = DEFAULT_FILL_COLOR;
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, R.styleable.CircleImageView, i, 0);
        this.mBorderWidth = obtainStyledAttributes.getDimensionPixelSize(2, DEFAULT_BORDER_WIDTH);
        this.mBorderColor = obtainStyledAttributes.getColor(0, DEFAULT_BORDER_COLOR);
        this.mBorderOverlay = obtainStyledAttributes.getBoolean(1, DEFAULT_BORDER_OVERLAY);
        this.mFillColor = obtainStyledAttributes.getColor(3, DEFAULT_FILL_COLOR);
        this.name = obtainStyledAttributes.getString(4);
        obtainStyledAttributes.recycle();
        init();
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ CircleImageView(Context context, AttributeSet attributeSet, int i, int i2, DefaultConstructorMarker defaultConstructorMarker) {
        this(context, attributeSet, (i2 & 4) != 0 ? 0 : i);
    }

    private final void init() {
        super.setScaleType(SCALE_TYPE);
        this.mReady = true;
        if (this.mSetupPending) {
            setup();
            this.mSetupPending = false;
        }
    }

    public ImageView.ScaleType getScaleType() {
        return SCALE_TYPE;
    }

    public void setScaleType(ImageView.ScaleType scaleType) {
        Intrinsics.checkParameterIsNotNull(scaleType, "scaleType");
        if (scaleType != SCALE_TYPE) {
            StringCompanionObject stringCompanionObject = StringCompanionObject.INSTANCE;
            String format = String.format("ScaleType %s not supported.", Arrays.copyOf(new Object[]{scaleType}, 1));
            Intrinsics.checkExpressionValueIsNotNull(format, "java.lang.String.format(format, *args)");
            throw new IllegalArgumentException(format);
        }
    }

    public void setAdjustViewBounds(boolean z) {
        if (z) {
            throw new IllegalArgumentException("adjustViewBounds not supported.");
        }
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        Intrinsics.checkParameterIsNotNull(canvas, "canvas");
        if (this.isDisableCircularTransformation) {
            super.onDraw(canvas);
        } else if (this.mBitmap != null) {
            if (this.mFillColor != 0) {
                canvas.drawCircle(this.mDrawableRect.centerX(), this.mDrawableRect.centerY(), this.mDrawableRadius, this.mFillPaint);
            }
            canvas.drawCircle(this.mDrawableRect.centerX(), this.mDrawableRect.centerY(), this.mDrawableRadius, this.mBitmapPaint);
            if (this.mBorderWidth > 0) {
                canvas.drawCircle(this.mBorderRect.centerX(), this.mBorderRect.centerY(), this.mBorderRadius, this.mBorderPaint);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        super.onSizeChanged(i, i2, i3, i4);
        setup();
    }

    public void setPadding(int i, int i2, int i3, int i4) {
        super.setPadding(i, i2, i3, i4);
        setup();
    }

    public void setPaddingRelative(int i, int i2, int i3, int i4) {
        super.setPaddingRelative(i, i2, i3, i4);
        setup();
    }

    @Deprecated(message = "Use {@link #setBorderColor(int)} instead")
    public final void setBorderColorResource(int i) {
        setBorderColor(ContextCompat.getColor(getContext(), i));
    }

    @Deprecated(message = "Fill Color support is going to be removed in the future")
    public final void setFillColorResource(int i) {
        setFillColor(ContextCompat.getColor(getContext(), i));
    }

    public void setImageBitmap(Bitmap bitmap) {
        Intrinsics.checkParameterIsNotNull(bitmap, "bm");
        super.setImageBitmap(bitmap);
        initializeBitmap();
    }

    public void setImageDrawable(Drawable drawable) {
        super.setImageDrawable(drawable);
        initializeBitmap();
    }

    public void setImageResource(int i) {
        super.setImageResource(i);
        initializeBitmap();
    }

    public void setImageURI(Uri uri) {
        super.setImageURI(uri);
        initializeBitmap();
    }

    public ColorFilter getColorFilter() {
        return this.mColorFilter;
    }

    public void setColorFilter(ColorFilter colorFilter) {
        Intrinsics.checkParameterIsNotNull(colorFilter, "cf");
        if (colorFilter != this.mColorFilter) {
            this.mColorFilter = colorFilter;
            applyColorFilter();
            invalidate();
        }
    }

    private final void applyColorFilter() {
        this.mBitmapPaint.setColorFilter(this.mColorFilter);
    }

    private final Bitmap getBitmapFromDrawable(Drawable drawable) {
        Bitmap bitmap;
        if (drawable == null) {
            return null;
        }
        if (drawable instanceof BitmapDrawable) {
            return ((BitmapDrawable) drawable).getBitmap();
        }
        try {
            if (drawable instanceof ColorDrawable) {
                bitmap = Bitmap.createBitmap(COLORDRAWABLE_DIMENSION, COLORDRAWABLE_DIMENSION, BITMAP_CONFIG);
                Intrinsics.checkExpressionValueIsNotNull(bitmap, "Bitmap.createBitmap(COLO…DIMENSION, BITMAP_CONFIG)");
            } else {
                bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), BITMAP_CONFIG);
                Intrinsics.checkExpressionValueIsNotNull(bitmap, "Bitmap.createBitmap(draw…sicHeight, BITMAP_CONFIG)");
            }
            Canvas canvas = new Canvas(bitmap);
            drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
            drawable.draw(canvas);
            return bitmap;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private final void initializeBitmap() {
        if (this.isDisableCircularTransformation) {
            this.mBitmap = null;
        } else {
            this.mBitmap = getBitmapFromDrawable(getDrawable());
        }
        setup();
    }

    private final void setup() {
        int i;
        if (!this.mReady) {
            this.mSetupPending = true;
        } else if (getWidth() != 0 || getHeight() != 0) {
            if (this.mBitmap == null) {
                invalidate();
                return;
            }
            Bitmap bitmap = this.mBitmap;
            if (bitmap == null) {
                Intrinsics.throwNpe();
            }
            this.mBitmapShader = new BitmapShader(bitmap, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP);
            this.mBitmapPaint.setAntiAlias(true);
            this.mBitmapPaint.setShader(this.mBitmapShader);
            this.mBorderPaint.setStyle(Paint.Style.STROKE);
            this.mBorderPaint.setAntiAlias(true);
            this.mBorderPaint.setColor(this.mBorderColor);
            this.mBorderPaint.setStrokeWidth((float) this.mBorderWidth);
            this.mFillPaint.setStyle(Paint.Style.FILL);
            this.mFillPaint.setAntiAlias(true);
            this.mFillPaint.setColor(this.mFillColor);
            Bitmap bitmap2 = this.mBitmap;
            if (bitmap2 == null) {
                Intrinsics.throwNpe();
            }
            this.mBitmapHeight = bitmap2.getHeight();
            Bitmap bitmap3 = this.mBitmap;
            if (bitmap3 == null) {
                Intrinsics.throwNpe();
            }
            this.mBitmapWidth = bitmap3.getWidth();
            this.mBorderRect.set(calculateBounds());
            this.mBorderRadius = Math.min((this.mBorderRect.height() - ((float) this.mBorderWidth)) / 2.0f, (this.mBorderRect.width() - ((float) this.mBorderWidth)) / 2.0f);
            this.mDrawableRect.set(this.mBorderRect);
            if (!this.mBorderOverlay && (i = this.mBorderWidth) > 0) {
                this.mDrawableRect.inset(((float) i) - 1.0f, ((float) i) - 1.0f);
            }
            this.mDrawableRadius = Math.min(this.mDrawableRect.height() / 2.0f, this.mDrawableRect.width() / 2.0f);
            applyColorFilter();
            updateShaderMatrix();
            invalidate();
        }
    }

    private final RectF calculateBounds() {
        int width = (getWidth() - getPaddingLeft()) - getPaddingRight();
        int height = (getHeight() - getPaddingTop()) - getPaddingBottom();
        int min = Math.min(width, height);
        float paddingLeft = ((float) getPaddingLeft()) + (((float) (width - min)) / 2.0f);
        float paddingTop = ((float) getPaddingTop()) + (((float) (height - min)) / 2.0f);
        float f = (float) min;
        return new RectF(paddingLeft, paddingTop, paddingLeft + f, f + paddingTop);
    }

    private final void updateShaderMatrix() {
        float f;
        float f2;
        this.mShaderMatrix.set((Matrix) null);
        float f3 = 0.0f;
        if (((float) this.mBitmapWidth) * this.mDrawableRect.height() > this.mDrawableRect.width() * ((float) this.mBitmapHeight)) {
            f2 = this.mDrawableRect.height() / ((float) this.mBitmapHeight);
            f3 = (this.mDrawableRect.width() - (((float) this.mBitmapWidth) * f2)) * 0.5f;
            f = 0.0f;
        } else {
            f2 = this.mDrawableRect.width() / ((float) this.mBitmapWidth);
            f = (this.mDrawableRect.height() - (((float) this.mBitmapHeight) * f2)) * 0.5f;
        }
        this.mShaderMatrix.setScale(f2, f2);
        this.mShaderMatrix.postTranslate(((float) ((int) (f3 + 0.5f))) + this.mDrawableRect.left, ((float) ((int) (f + 0.5f))) + this.mDrawableRect.top);
        BitmapShader bitmapShader = this.mBitmapShader;
        if (bitmapShader == null) {
            Intrinsics.throwNpe();
        }
        bitmapShader.setLocalMatrix(this.mShaderMatrix);
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006XD¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0006XD¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tXD¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u0006XD¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u0006XD¢\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\rX\u0004¢\u0006\u0002\n\u0000¨\u0006\u000e"}, d2 = {"Lcom/iqonic/store/utils/CircleImageView$Companion;", "", "()V", "BITMAP_CONFIG", "Landroid/graphics/Bitmap$Config;", "COLORDRAWABLE_DIMENSION", "", "DEFAULT_BORDER_COLOR", "DEFAULT_BORDER_OVERLAY", "", "DEFAULT_BORDER_WIDTH", "DEFAULT_FILL_COLOR", "SCALE_TYPE", "Landroid/widget/ImageView$ScaleType;", "app_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: CircleImageView.kt */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }
}
