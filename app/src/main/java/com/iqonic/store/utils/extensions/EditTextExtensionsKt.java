package com.iqonic.store.utils.extensions;

import android.text.Editable;
import android.text.TextUtils;
import android.util.Patterns;
import android.widget.EditText;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import kotlin.text.Regex;
import kotlin.text.StringsKt;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001c\n\u0000\n\u0002\u0010\u000b\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\u001a\n\u0010\u0000\u001a\u00020\u0001*\u00020\u0002\u001a\n\u0010\u0003\u001a\u00020\u0001*\u00020\u0002\u001a\n\u0010\u0004\u001a\u00020\u0001*\u00020\u0002\u001a\n\u0010\u0005\u001a\u00020\u0001*\u00020\u0002\u001a\u0012\u0010\u0006\u001a\u00020\u0007*\u00020\u00022\u0006\u0010\b\u001a\u00020\t\u001a\n\u0010\n\u001a\u00020\t*\u00020\u0002\u001a\n\u0010\u000b\u001a\u00020\u0001*\u00020\u0002¨\u0006\f"}, d2 = {"checkIsEmpty", "", "Landroid/widget/EditText;", "isValidEmail", "isValidPhoneNumber", "isValidText", "showError", "", "error", "", "textToString", "validPassword", "app_release"}, k = 2, mv = {1, 1, 16})
/* compiled from: EditTextExtensions.kt */
public final class EditTextExtensionsKt {
    public static final String textToString(EditText editText) {
        Intrinsics.checkParameterIsNotNull(editText, "$this$textToString");
        return editText.getText().toString();
    }

    public static final boolean checkIsEmpty(EditText editText) {
        Intrinsics.checkParameterIsNotNull(editText, "$this$checkIsEmpty");
        return editText.getText() == null || Intrinsics.areEqual((Object) "", (Object) textToString(editText)) || StringsKt.equals(editText.getText().toString(), "null", true);
    }

    public static final boolean isValidEmail(EditText editText) {
        Intrinsics.checkParameterIsNotNull(editText, "$this$isValidEmail");
        return !TextUtils.isEmpty(editText.getText()) && Patterns.EMAIL_ADDRESS.matcher(editText.getText()).matches();
    }

    public static final boolean isValidPhoneNumber(EditText editText) {
        Intrinsics.checkParameterIsNotNull(editText, "$this$isValidPhoneNumber");
        Editable text = editText.getText();
        Intrinsics.checkExpressionValueIsNotNull(text, "text");
        return new Regex("^(((\\+?\\(91\\))|0|((00|\\+)?91))-?)?[7-9]\\d{9}$").matches(text);
    }

    public static final void showError(EditText editText, String str) {
        Intrinsics.checkParameterIsNotNull(editText, "$this$showError");
        Intrinsics.checkParameterIsNotNull(str, "error");
        editText.setError(str);
        ViewExtensionsKt.showSoftKeyboard(editText);
    }

    public static final boolean validPassword(EditText editText) {
        Intrinsics.checkParameterIsNotNull(editText, "$this$validPassword");
        return editText.getText().length() < 6;
    }

    public static final boolean isValidText(EditText editText) {
        Intrinsics.checkParameterIsNotNull(editText, "$this$isValidText");
        Editable text = editText.getText();
        Intrinsics.checkExpressionValueIsNotNull(text, "text");
        return new Regex("[a-zA-Z]+").matches(text);
    }
}
