package com.iqonic.store.utils.shimmer;

import android.graphics.drawable.Drawable;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import com.store.proshop.R;
import io.supercharge.shimmerlayout.ShimmerLayout;

public class ShimmerViewHolder extends RecyclerView.ViewHolder {
    private ShimmerLayout mShimmerLayout;

    public ShimmerViewHolder(LayoutInflater layoutInflater, ViewGroup viewGroup, int i) {
        super(layoutInflater.inflate(R.layout.viewholder_shimmer, viewGroup, false));
        ShimmerLayout shimmerLayout = (ShimmerLayout) this.itemView;
        this.mShimmerLayout = shimmerLayout;
        layoutInflater.inflate(i, shimmerLayout, true);
    }

    public void setShimmerAngle(int i) {
        this.mShimmerLayout.setShimmerAngle(i);
    }

    public void setShimmerColor(int i) {
        this.mShimmerLayout.setShimmerColor(i);
    }

    public void setShimmerMaskWidth(float f) {
        this.mShimmerLayout.setMaskWidth(f);
    }

    public void setShimmerViewHolderBackground(Drawable drawable) {
        if (drawable != null) {
            setBackground(drawable);
        }
    }

    public void setShimmerAnimationDuration(int i) {
        this.mShimmerLayout.setShimmerAnimationDuration(i);
    }

    public void setAnimationReversed(boolean z) {
        this.mShimmerLayout.setAnimationReversed(z);
    }

    public void bind() {
        this.mShimmerLayout.startShimmerAnimation();
    }

    private void setBackground(Drawable drawable) {
        if (Build.VERSION.SDK_INT > 16) {
            this.mShimmerLayout.setBackground(drawable);
        } else {
            this.mShimmerLayout.setBackgroundDrawable(drawable);
        }
    }
}
