package com.iqonic.store.utils.extensions;

import android.view.View;
import kotlin.Metadata;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\u0010\u0000\u001a\u00020\u0001\"\u0004\b\u0000\u0010\u00022\u000e\u0010\u0003\u001a\n \u0005*\u0004\u0018\u00010\u00040\u0004H\n¢\u0006\u0002\b\u0006"}, d2 = {"<anonymous>", "", "T", "it", "Landroid/view/View;", "kotlin.jvm.PlatformType", "onClick"}, k = 3, mv = {1, 1, 16})
/* compiled from: AdapterExtensions.kt */
final class RecyclerAdapter$onBindViewHolder$1 implements View.OnClickListener {
    final /* synthetic */ Object $item;
    final /* synthetic */ int $position;
    final /* synthetic */ RecyclerAdapter this$0;

    RecyclerAdapter$onBindViewHolder$1(RecyclerAdapter recyclerAdapter, Object obj, int i) {
        this.this$0 = recyclerAdapter;
        this.$item = obj;
        this.$position = i;
    }

    public final void onClick(View view) {
        this.this$0.itemClick.invoke(this.$item, Integer.valueOf(this.$position));
    }
}
