package com.iqonic.store.utils.oauthInterceptor;

import android.util.Log;
import com.facebook.internal.FacebookRequestErrorClassification;
import com.facebook.internal.NativeProtocol;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.collections.CollectionsKt;
import kotlin.jvm.internal.Intrinsics;
import kotlin.text.Typography;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000D\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\t\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\u0018\u0000 \u001d2\u00020\u0001:\u0001\u001dB\u0007\b\u0016¢\u0006\u0002\u0010\u0002B\u0015\b\u0010\u0012\f\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004¢\u0006\u0002\u0010\u0006B\u001b\b\u0016\u0012\u0012\u0010\u0007\u001a\u000e\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\t0\b¢\u0006\u0002\u0010\nJ\u0016\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\t2\u0006\u0010\u0010\u001a\u00020\tJ\u000e\u0010\u0011\u001a\u00020\u000e2\u0006\u0010\u0012\u001a\u00020\u0000J\u000e\u0010\u0013\u001a\u00020\u000e2\u0006\u0010\u0014\u001a\u00020\tJ\u0006\u0010\u0015\u001a\u00020\tJ\u0006\u0010\u0016\u001a\u00020\tJ\u000e\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00020\u0005J\u0006\u0010\u001a\u001a\u00020\u001bJ\u0006\u0010\u001c\u001a\u00020\u0000R\u001e\u0010\u0003\u001a\u0012\u0012\u0004\u0012\u00020\u00050\u000bj\b\u0012\u0004\u0012\u00020\u0005`\fX\u0004¢\u0006\u0002\n\u0000¨\u0006\u001e"}, d2 = {"Lcom/iqonic/store/utils/oauthInterceptor/ParameterList;", "", "()V", "params", "", "Lcom/iqonic/store/utils/oauthInterceptor/Parameter;", "(Ljava/util/List;)V", "map", "", "", "(Ljava/util/Map;)V", "Ljava/util/ArrayList;", "Lkotlin/collections/ArrayList;", "add", "", "key", "value", "addAll", "other", "addQuerystring", "queryString", "asFormUrlEncodedString", "asOauthBaseString", "contains", "", "param", "size", "", "sort", "Companion", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: ParameterList.kt */
public final class ParameterList {
    public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
    private static final String EMPTY_STRING = "";
    private static final String PAIR_SEPARATOR = PAIR_SEPARATOR;
    private static final String PARAM_SEPARATOR = PARAM_SEPARATOR;
    private static final char QUERY_STRING_SEPARATOR = QUERY_STRING_SEPARATOR;
    private final ArrayList<Parameter> params;

    public ParameterList() {
        this.params = new ArrayList<>();
    }

    public ParameterList(List<Parameter> list) {
        Intrinsics.checkParameterIsNotNull(list, NativeProtocol.WEB_DIALOG_PARAMS);
        this.params = new ArrayList<>(list);
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public ParameterList(Map<String, String> map) {
        this();
        Intrinsics.checkParameterIsNotNull(map, "map");
        for (Map.Entry next : map.entrySet()) {
            this.params.add(new Parameter((String) next.getKey(), (String) next.getValue()));
        }
    }

    public final void add(String str, String str2) {
        Intrinsics.checkParameterIsNotNull(str, "key");
        Intrinsics.checkParameterIsNotNull(str2, "value");
        this.params.add(new Parameter(str, str2));
    }

    public final String asOauthBaseString() {
        return OAuthEncoder.INSTANCE.encode(asFormUrlEncodedString());
    }

    public final String asFormUrlEncodedString() {
        if (this.params.size() == 0) {
            return EMPTY_STRING;
        }
        StringBuilder sb = new StringBuilder();
        Iterator<Parameter> it = this.params.iterator();
        while (it.hasNext()) {
            sb.append(Typography.amp);
            sb.append(it.next().asUrlEncodedPair());
        }
        String sb2 = sb.toString();
        Intrinsics.checkExpressionValueIsNotNull(sb2, "builder.toString()");
        if (sb2 != null) {
            String substring = sb2.substring(1);
            Intrinsics.checkExpressionValueIsNotNull(substring, "(this as java.lang.String).substring(startIndex)");
            return substring;
        }
        throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
    }

    public final void addAll(ParameterList parameterList) {
        Intrinsics.checkParameterIsNotNull(parameterList, FacebookRequestErrorClassification.KEY_OTHER);
        this.params.addAll(parameterList.params);
    }

    /* JADX WARNING: Removed duplicated region for block: B:19:0x0062  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00b8  */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x00e3  */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x00dd A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void addQuerystring(java.lang.String r10) {
        /*
            r9 = this;
            java.lang.String r0 = "queryString"
            kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(r10, r0)
            java.lang.CharSequence r10 = (java.lang.CharSequence) r10
            int r0 = r10.length()
            r1 = 1
            r2 = 0
            if (r0 <= 0) goto L_0x0011
            r0 = 1
            goto L_0x0012
        L_0x0011:
            r0 = 0
        L_0x0012:
            if (r0 == 0) goto L_0x00e9
            java.lang.String r0 = PARAM_SEPARATOR
            kotlin.text.Regex r3 = new kotlin.text.Regex
            r3.<init>((java.lang.String) r0)
            java.util.List r10 = r3.split(r10, r2)
            boolean r0 = r10.isEmpty()
            if (r0 != 0) goto L_0x0052
            int r0 = r10.size()
            java.util.ListIterator r0 = r10.listIterator(r0)
        L_0x002d:
            boolean r3 = r0.hasPrevious()
            if (r3 == 0) goto L_0x0052
            java.lang.Object r3 = r0.previous()
            java.lang.String r3 = (java.lang.String) r3
            java.lang.CharSequence r3 = (java.lang.CharSequence) r3
            int r3 = r3.length()
            if (r3 != 0) goto L_0x0043
            r3 = 1
            goto L_0x0044
        L_0x0043:
            r3 = 0
        L_0x0044:
            if (r3 != 0) goto L_0x002d
            java.lang.Iterable r10 = (java.lang.Iterable) r10
            int r0 = r0.nextIndex()
            int r0 = r0 + r1
            java.util.List r10 = kotlin.collections.CollectionsKt.take(r10, r0)
            goto L_0x0056
        L_0x0052:
            java.util.List r10 = kotlin.collections.CollectionsKt.emptyList()
        L_0x0056:
            java.util.Collection r10 = (java.util.Collection) r10
            java.lang.String[] r0 = new java.lang.String[r2]
            java.lang.Object[] r10 = r10.toArray(r0)
            java.lang.String r0 = "null cannot be cast to non-null type kotlin.Array<T>"
            if (r10 == 0) goto L_0x00e3
            java.lang.String[] r10 = (java.lang.String[]) r10
            int r3 = r10.length
            r4 = 0
        L_0x0066:
            if (r4 >= r3) goto L_0x00e9
            r5 = r10[r4]
            java.lang.CharSequence r5 = (java.lang.CharSequence) r5
            java.lang.String r6 = PAIR_SEPARATOR
            kotlin.text.Regex r7 = new kotlin.text.Regex
            r7.<init>((java.lang.String) r6)
            java.util.List r5 = r7.split(r5, r2)
            boolean r6 = r5.isEmpty()
            if (r6 != 0) goto L_0x00aa
            int r6 = r5.size()
            java.util.ListIterator r6 = r5.listIterator(r6)
        L_0x0085:
            boolean r7 = r6.hasPrevious()
            if (r7 == 0) goto L_0x00aa
            java.lang.Object r7 = r6.previous()
            java.lang.String r7 = (java.lang.String) r7
            java.lang.CharSequence r7 = (java.lang.CharSequence) r7
            int r7 = r7.length()
            if (r7 != 0) goto L_0x009b
            r7 = 1
            goto L_0x009c
        L_0x009b:
            r7 = 0
        L_0x009c:
            if (r7 != 0) goto L_0x0085
            java.lang.Iterable r5 = (java.lang.Iterable) r5
            int r6 = r6.nextIndex()
            int r6 = r6 + r1
            java.util.List r5 = kotlin.collections.CollectionsKt.take(r5, r6)
            goto L_0x00ae
        L_0x00aa:
            java.util.List r5 = kotlin.collections.CollectionsKt.emptyList()
        L_0x00ae:
            java.util.Collection r5 = (java.util.Collection) r5
            java.lang.String[] r6 = new java.lang.String[r2]
            java.lang.Object[] r5 = r5.toArray(r6)
            if (r5 == 0) goto L_0x00dd
            java.lang.String[] r5 = (java.lang.String[]) r5
            com.iqonic.store.utils.oauthInterceptor.OAuthEncoder r6 = com.iqonic.store.utils.oauthInterceptor.OAuthEncoder.INSTANCE
            r7 = r5[r2]
            java.lang.String r6 = r6.decode(r7)
            int r7 = r5.length
            if (r7 <= r1) goto L_0x00ce
            com.iqonic.store.utils.oauthInterceptor.OAuthEncoder r7 = com.iqonic.store.utils.oauthInterceptor.OAuthEncoder.INSTANCE
            r5 = r5[r1]
            java.lang.String r5 = r7.decode(r5)
            goto L_0x00d0
        L_0x00ce:
            java.lang.String r5 = EMPTY_STRING
        L_0x00d0:
            java.util.ArrayList<com.iqonic.store.utils.oauthInterceptor.Parameter> r7 = r9.params
            com.iqonic.store.utils.oauthInterceptor.Parameter r8 = new com.iqonic.store.utils.oauthInterceptor.Parameter
            r8.<init>(r6, r5)
            r7.add(r8)
            int r4 = r4 + 1
            goto L_0x0066
        L_0x00dd:
            kotlin.TypeCastException r10 = new kotlin.TypeCastException
            r10.<init>(r0)
            throw r10
        L_0x00e3:
            kotlin.TypeCastException r10 = new kotlin.TypeCastException
            r10.<init>(r0)
            throw r10
        L_0x00e9:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.iqonic.store.utils.oauthInterceptor.ParameterList.addQuerystring(java.lang.String):void");
    }

    public final boolean contains(Parameter parameter) {
        Intrinsics.checkParameterIsNotNull(parameter, "param");
        return this.params.contains(parameter);
    }

    public final int size() {
        return this.params.size();
    }

    public final ParameterList sort() {
        ParameterList parameterList = new ParameterList((List<Parameter>) this.params);
        Log.d("saoted", parameterList.params.get(0).asUrlEncodedPair());
        Log.d("saoted", parameterList.params.get(1).asUrlEncodedPair());
        CollectionsKt.sort(parameterList.params);
        Log.d("saoted", parameterList.params.get(0).asUrlEncodedPair());
        Log.d("saoted", parameterList.params.get(1).asUrlEncodedPair());
        return parameterList;
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\f\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XD¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XD¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004XD¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\bXD¢\u0006\u0002\n\u0000¨\u0006\t"}, d2 = {"Lcom/iqonic/store/utils/oauthInterceptor/ParameterList$Companion;", "", "()V", "EMPTY_STRING", "", "PAIR_SEPARATOR", "PARAM_SEPARATOR", "QUERY_STRING_SEPARATOR", "", "app_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: ParameterList.kt */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }
}
