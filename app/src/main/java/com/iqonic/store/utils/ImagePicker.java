package com.iqonic.store.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.util.Log;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;
import com.facebook.share.internal.MessengerShareContentUtility;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000b\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0002\b\b\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J,\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u00160\u00152\u0006\u0010\u0017\u001a\u00020\u00182\f\u0010\u0019\u001a\b\u0012\u0004\u0012\u00020\u00160\u00152\u0006\u0010\u001a\u001a\u00020\u0016H\u0002J\u0018\u0010\u001b\u001a\u00020\f2\u0006\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u001c\u001a\u00020\u0004H\u0002J\u001a\u0010\u001d\u001a\u0004\u0018\u00010\u00042\u0006\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u001e\u001a\u00020\u001fH\u0002J*\u0010 \u001a\u0004\u0018\u00010\u00042\u0006\u0010\u0017\u001a\u00020\u00182\u0006\u0010!\u001a\u00020\u00062\u0006\u0010\"\u001a\u00020\u00062\b\u0010#\u001a\u0004\u0018\u00010\u0016J\u001c\u0010$\u001a\u0004\u0018\u00010\u00162\b\u0010\u0017\u001a\u0004\u0018\u00010\u00182\b\u0010%\u001a\u0004\u0018\u00010\u0004J\u0016\u0010&\u001a\u00020'2\u0006\u0010\u0017\u001a\u00020\u00182\u0006\u0010(\u001a\u00020\u0004J\u0010\u0010)\u001a\u00020\f2\u0006\u0010\u0017\u001a\u00020\u0018H\u0002J,\u0010*\u001a\u00020+2\u0006\u0010,\u001a\u00020-2\u0006\u0010%\u001a\u00020\u00042\b\b\u0002\u0010!\u001a\u00020\u00062\b\b\u0002\u0010.\u001a\u00020\fH\u0007J&\u0010/\u001a\u00020+2\u0006\u00100\u001a\u0002012\u0006\u0010%\u001a\u00020\u00042\u0006\u0010!\u001a\u00020\u00062\u0006\u0010.\u001a\u00020\fJ\u001e\u00102\u001a\u00020\u00042\u0006\u0010\u0017\u001a\u00020\u00182\u0006\u00103\u001a\u0002042\u0006\u00105\u001a\u00020\u0004J\u0016\u00106\u001a\u00020+2\u0006\u0010\u0013\u001a\u00020\u00062\u0006\u0010\u0012\u001a\u00020\u0006J\u0010\u00107\u001a\u00020+2\u0006\u00108\u001a\u00020-H\u0002J\u0010\u00109\u001a\u00020+2\u0006\u00100\u001a\u000201H\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0006XT¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0006XT¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0004X\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\n\u001a\u0004\u0018\u00010\u0004X\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\fX\u000e¢\u0006\u0002\n\u0000R\u001a\u0010\r\u001a\u00020\u0006X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u000e\u0010\u000f\"\u0004\b\u0010\u0010\u0011R\u000e\u0010\u0012\u001a\u00020\u0006X\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0006X\u000e¢\u0006\u0002\n\u0000¨\u0006:"}, d2 = {"Lcom/iqonic/store/utils/ImagePicker;", "", "()V", "BASE_IMAGE_NAME", "", "DEFAULT_MIN_HEIGHT_QUALITY", "", "DEFAULT_MIN_WIDTH_QUALITY", "DEFAULT_REQUEST_CODE", "TAG", "mChooserTitle", "mGalleryOnly", "", "mPickImageRequestCode", "getMPickImageRequestCode", "()I", "setMPickImageRequestCode", "(I)V", "minHeightQuality", "minWidthQuality", "addIntentsToList", "", "Landroid/content/Intent;", "context", "Landroid/content/Context;", "list", "intent", "appManifestContainsPermission", "permission", "getFilePathFromUri", "uri", "Landroid/net/Uri;", "getImagePathFromResult", "requestCode", "resultCode", "imageReturnedIntent", "getPickImageIntent", "chooserTitle", "getTemporalFile", "Ljava/io/File;", "payload", "hasCameraAccess", "pickImage", "", "fragment", "Landroidx/fragment/app/Fragment;", "galleryOnly", "pickImageActivity", "activity", "Landroid/app/Activity;", "savePicture", "bitmap", "Landroid/graphics/Bitmap;", "imageSuffix", "setMinQuality", "startChooser", "fragmentContext", "startChoosers", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: ImagePicker.kt */
public final class ImagePicker {
    private static final String BASE_IMAGE_NAME = "i_prefix_";
    private static final int DEFAULT_MIN_HEIGHT_QUALITY = 800;
    private static final int DEFAULT_MIN_WIDTH_QUALITY = 800;
    private static final int DEFAULT_REQUEST_CODE = 234;
    public static final ImagePicker INSTANCE = new ImagePicker();
    private static final String TAG;
    private static String mChooserTitle;
    private static boolean mGalleryOnly;
    private static int mPickImageRequestCode = DEFAULT_REQUEST_CODE;
    private static int minHeightQuality = 800;
    private static int minWidthQuality = 800;

    public final void pickImage(Fragment fragment, String str) {
        pickImage$default(this, fragment, str, 0, false, 12, (Object) null);
    }

    public final void pickImage(Fragment fragment, String str, int i) {
        pickImage$default(this, fragment, str, i, false, 8, (Object) null);
    }

    static {
        String simpleName = ImagePicker.class.getSimpleName();
        Intrinsics.checkExpressionValueIsNotNull(simpleName, "ImagePicker::class.java.simpleName");
        TAG = simpleName;
    }

    private ImagePicker() {
    }

    public final int getMPickImageRequestCode() {
        return mPickImageRequestCode;
    }

    public final void setMPickImageRequestCode(int i) {
        mPickImageRequestCode = i;
    }

    public static /* synthetic */ void pickImage$default(ImagePicker imagePicker, Fragment fragment, String str, int i, boolean z, int i2, Object obj) {
        if ((i2 & 4) != 0) {
            i = DEFAULT_REQUEST_CODE;
        }
        if ((i2 & 8) != 0) {
            z = false;
        }
        imagePicker.pickImage(fragment, str, i, z);
    }

    public final void pickImage(Fragment fragment, String str, int i, boolean z) {
        Intrinsics.checkParameterIsNotNull(fragment, "fragment");
        Intrinsics.checkParameterIsNotNull(str, "chooserTitle");
        mGalleryOnly = z;
        mPickImageRequestCode = i;
        mChooserTitle = str;
        startChooser(fragment);
    }

    public final void pickImageActivity(Activity activity, String str, int i, boolean z) {
        Intrinsics.checkParameterIsNotNull(activity, "activity");
        Intrinsics.checkParameterIsNotNull(str, "chooserTitle");
        mGalleryOnly = z;
        mPickImageRequestCode = i;
        mChooserTitle = str;
        startChoosers(activity);
    }

    private final void startChoosers(Activity activity) {
        activity.startActivityForResult(getPickImageIntent(activity.getBaseContext(), mChooserTitle), mPickImageRequestCode);
    }

    private final void startChooser(Fragment fragment) {
        fragment.startActivityForResult(getPickImageIntent(fragment.getContext(), mChooserTitle), mPickImageRequestCode);
    }

    public final Intent getPickImageIntent(Context context, String str) {
        Intent intent = null;
        List arrayList = new ArrayList();
        Intent intent2 = new Intent("android.intent.action.PICK", MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        if (context == null) {
            Intrinsics.throwNpe();
        }
        List<Intent> addIntentsToList = addIntentsToList(context, arrayList, intent2);
        if (!mGalleryOnly && (!appManifestContainsPermission(context, "android.permission.CAMERA") || hasCameraAccess(context))) {
            Intent intent3 = new Intent("android.media.action.IMAGE_CAPTURE");
            intent3.putExtra("return-data", true);
            intent3.putExtra("output", FileProvider.getUriForFile(context, "com.store.proshop.provider", getTemporalFile(context, String.valueOf(mPickImageRequestCode))));
            addIntentsToList = addIntentsToList(context, addIntentsToList, intent3);
        }
        if (addIntentsToList.size() > 0) {
            intent = Intent.createChooser(addIntentsToList.remove(addIntentsToList.size() - 1), str);
            if (intent == null) {
                Intrinsics.throwNpe();
            }
            Object[] array = addIntentsToList.toArray(new Parcelable[0]);
            if (array != null) {
                intent.putExtra("android.intent.extra.INITIAL_INTENTS", (Parcelable[]) array);
            } else {
                throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
            }
        }
        return intent;
    }

    private final List<Intent> addIntentsToList(Context context, List<Intent> list, Intent intent) {
        String str = TAG;
        StringBuilder sb = new StringBuilder();
        sb.append("Adding intents of type: ");
        String action = intent.getAction();
        if (action == null) {
            Intrinsics.throwNpe();
        }
        sb.append(action);
        Log.i(str, sb.toString());
        for (ResolveInfo resolveInfo : context.getPackageManager().queryIntentActivities(intent, 0)) {
            String str2 = resolveInfo.activityInfo.packageName;
            Intent intent2 = new Intent(intent);
            intent2.setPackage(str2);
            list.add(intent2);
            String str3 = TAG;
            Log.i(str3, "App package: " + str2);
        }
        return list;
    }

    private final boolean hasCameraAccess(Context context) {
        return ContextCompat.checkSelfPermission(context, "android.permission.CAMERA") == 0;
    }

    private final boolean appManifestContainsPermission(Context context, String str) {
        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 4096);
            String[] strArr = null;
            if (packageInfo != null) {
                strArr = packageInfo.requestedPermissions;
            }
            if (strArr == null) {
                return false;
            }
            if (!(strArr.length == 0)) {
                return Arrays.asList((String[]) Arrays.copyOf(strArr, strArr.length)).contains(str);
            }
            return false;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0060, code lost:
        if (kotlin.text.StringsKt.contains$default((java.lang.CharSequence) r1, (java.lang.CharSequence) r2, false, 2, (java.lang.Object) null) != false) goto L_0x0062;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.String getImagePathFromResult(android.content.Context r5, int r6, int r7, android.content.Intent r8) {
        /*
            r4 = this;
            java.lang.String r0 = "context"
            kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(r5, r0)
            java.lang.String r0 = TAG
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "getImagePathFromResult() called with: resultCode = ["
            r1.append(r2)
            r1.append(r7)
            r2 = 93
            r1.append(r2)
            java.lang.String r1 = r1.toString()
            android.util.Log.i(r0, r1)
            r0 = 0
            r1 = r0
            android.net.Uri r1 = (android.net.Uri) r1
            r2 = -1
            if (r7 != r2) goto L_0x008e
            int r7 = mPickImageRequestCode
            if (r6 != r7) goto L_0x008e
            java.lang.String r6 = java.lang.String.valueOf(r7)
            java.io.File r6 = r4.getTemporalFile(r5, r6)
            r7 = 0
            if (r8 == 0) goto L_0x0062
            android.net.Uri r1 = r8.getData()
            if (r1 == 0) goto L_0x0062
            android.net.Uri r1 = r8.getData()
            if (r1 != 0) goto L_0x0045
            kotlin.jvm.internal.Intrinsics.throwNpe()
        L_0x0045:
            java.lang.String r1 = r1.toString()
            java.lang.String r2 = "imageReturnedIntent.data!!.toString()"
            kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r1, r2)
            java.lang.CharSequence r1 = (java.lang.CharSequence) r1
            java.lang.String r2 = r6.toString()
            java.lang.String r3 = "imageFile.toString()"
            kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r2, r3)
            java.lang.CharSequence r2 = (java.lang.CharSequence) r2
            r3 = 2
            boolean r1 = kotlin.text.StringsKt.contains$default((java.lang.CharSequence) r1, (java.lang.CharSequence) r2, (boolean) r7, (int) r3, (java.lang.Object) r0)
            if (r1 == 0) goto L_0x0063
        L_0x0062:
            r7 = 1
        L_0x0063:
            if (r7 == 0) goto L_0x006a
            java.lang.String r5 = r6.getAbsolutePath()
            return r5
        L_0x006a:
            if (r8 != 0) goto L_0x006f
            kotlin.jvm.internal.Intrinsics.throwNpe()
        L_0x006f:
            android.net.Uri r1 = r8.getData()
            java.lang.String r6 = TAG
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            r7.<init>()
            java.lang.String r8 = "selectedImage: "
            r7.append(r8)
            if (r1 != 0) goto L_0x0084
            kotlin.jvm.internal.Intrinsics.throwNpe()
        L_0x0084:
            r7.append(r1)
            java.lang.String r7 = r7.toString()
            android.util.Log.i(r6, r7)
        L_0x008e:
            if (r1 != 0) goto L_0x0091
            goto L_0x0095
        L_0x0091:
            java.lang.String r0 = r4.getFilePathFromUri(r5, r1)
        L_0x0095:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.iqonic.store.utils.ImagePicker.getImagePathFromResult(android.content.Context, int, int, android.content.Intent):java.lang.String");
    }

    private final String getFilePathFromUri(Context context, Uri uri) {
        InputStream inputStream = null;
        if (uri.getAuthority() != null) {
            try {
                InputStream openInputStream = context.getContentResolver().openInputStream(uri);
                Bitmap decodeStream = BitmapFactory.decodeStream(openInputStream);
                Intrinsics.checkExpressionValueIsNotNull(decodeStream, "bmp");
                String path = uri.getPath();
                if (path == null) {
                    Intrinsics.throwNpe();
                }
                String savePicture = savePicture(context, decodeStream, String.valueOf(path.hashCode()));
                if (openInputStream == null) {
                    try {
                        Intrinsics.throwNpe();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                openInputStream.close();
                return savePicture;
            } catch (FileNotFoundException e2) {
                e2.printStackTrace();
                if (inputStream == null) {
                    try {
                        Intrinsics.throwNpe();
                    } catch (IOException e3) {
                        e3.printStackTrace();
                    }
                }
                inputStream.close();
            } catch (Throwable th) {
                if (inputStream == null) {
                    try {
                        Intrinsics.throwNpe();
                    } catch (IOException e4) {
                        e4.printStackTrace();
                        throw th;
                    }
                }
                inputStream.close();
                throw th;
            }
        }
        return null;
    }

    /* JADX WARNING: Removed duplicated region for block: B:22:0x0066  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x006b A[SYNTHETIC, Splitter:B:24:0x006b] */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0080  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x0085 A[SYNTHETIC, Splitter:B:34:0x0085] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.String savePicture(android.content.Context r5, android.graphics.Bitmap r6, java.lang.String r7) {
        /*
            r4 = this;
            java.lang.String r0 = "context"
            kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(r5, r0)
            java.lang.String r0 = "bitmap"
            kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(r6, r0)
            java.lang.String r0 = "imageSuffix"
            kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(r7, r0)
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            r0.append(r7)
            java.lang.String r7 = ".jpeg"
            r0.append(r7)
            java.lang.String r7 = r0.toString()
            java.io.File r5 = r4.getTemporalFile(r5, r7)
            r7 = 0
            java.io.FileOutputStream r7 = (java.io.FileOutputStream) r7
            boolean r0 = r5.exists()
            if (r0 == 0) goto L_0x0030
            r5.delete()
        L_0x0030:
            java.io.FileOutputStream r0 = new java.io.FileOutputStream     // Catch:{ IOException -> 0x0059, all -> 0x0057 }
            java.lang.String r1 = r5.getPath()     // Catch:{ IOException -> 0x0059, all -> 0x0057 }
            r0.<init>(r1)     // Catch:{ IOException -> 0x0059, all -> 0x0057 }
            android.graphics.Bitmap$CompressFormat r7 = android.graphics.Bitmap.CompressFormat.JPEG     // Catch:{ IOException -> 0x0055 }
            r1 = 80
            r2 = r0
            java.io.OutputStream r2 = (java.io.OutputStream) r2     // Catch:{ IOException -> 0x0055 }
            r6.compress(r7, r1, r2)     // Catch:{ IOException -> 0x0055 }
            boolean r7 = r6.isRecycled()
            if (r7 != 0) goto L_0x004c
            r6.recycle()
        L_0x004c:
            r0.close()     // Catch:{ IOException -> 0x0050 }
            goto L_0x006e
        L_0x0050:
            r6 = move-exception
            r6.printStackTrace()
            goto L_0x006e
        L_0x0055:
            r7 = move-exception
            goto L_0x005d
        L_0x0057:
            r5 = move-exception
            goto L_0x007a
        L_0x0059:
            r0 = move-exception
            r3 = r0
            r0 = r7
            r7 = r3
        L_0x005d:
            r7.printStackTrace()     // Catch:{ all -> 0x0078 }
            boolean r7 = r6.isRecycled()
            if (r7 != 0) goto L_0x0069
            r6.recycle()
        L_0x0069:
            if (r0 == 0) goto L_0x006e
            r0.close()     // Catch:{ IOException -> 0x0050 }
        L_0x006e:
            java.lang.String r5 = r5.getAbsolutePath()
            java.lang.String r6 = "savedImage.absolutePath"
            kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r5, r6)
            return r5
        L_0x0078:
            r5 = move-exception
            r7 = r0
        L_0x007a:
            boolean r0 = r6.isRecycled()
            if (r0 != 0) goto L_0x0083
            r6.recycle()
        L_0x0083:
            if (r7 == 0) goto L_0x008d
            r7.close()     // Catch:{ IOException -> 0x0089 }
            goto L_0x008d
        L_0x0089:
            r6 = move-exception
            r6.printStackTrace()
        L_0x008d:
            throw r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.iqonic.store.utils.ImagePicker.savePicture(android.content.Context, android.graphics.Bitmap, java.lang.String):java.lang.String");
    }

    public final File getTemporalFile(Context context, String str) {
        Intrinsics.checkParameterIsNotNull(context, "context");
        Intrinsics.checkParameterIsNotNull(str, MessengerShareContentUtility.ATTACHMENT_PAYLOAD);
        File externalCacheDir = context.getExternalCacheDir();
        return new File(externalCacheDir, BASE_IMAGE_NAME + str);
    }

    public final void setMinQuality(int i, int i2) {
        minWidthQuality = i;
        minHeightQuality = i2;
    }
}
