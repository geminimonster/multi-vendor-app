package com.iqonic.store.utils.oauthInterceptor;

import java.util.regex.Pattern;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import kotlin.text.StringsKt;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u000b\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u001a\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000b2\b\u0010\f\u001a\u0004\u0018\u00010\u0004H\u0002J\u0018\u0010\r\u001a\u00020\t2\b\u0010\u000e\u001a\u0004\u0018\u00010\u00042\u0006\u0010\u000f\u001a\u00020\u0004J\u0018\u0010\u0010\u001a\u00020\t2\b\u0010\u0011\u001a\u0004\u0018\u00010\u00012\u0006\u0010\u000f\u001a\u00020\u0004J\u0016\u0010\u0012\u001a\u00020\t2\u0006\u0010\u0013\u001a\u00020\u00042\u0006\u0010\u000f\u001a\u00020\u0004J\u0016\u0010\u0014\u001a\u00020\t2\u0006\u0010\u0013\u001a\u00020\u00042\u0006\u0010\u000f\u001a\u00020\u0004J\u0010\u0010\u0015\u001a\u00020\u000b2\u0006\u0010\u0013\u001a\u00020\u0004H\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XD¢\u0006\u0002\n\u0000R\u0016\u0010\u0005\u001a\n \u0007*\u0004\u0018\u00010\u00060\u0006X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0016"}, d2 = {"Lcom/iqonic/store/utils/oauthInterceptor/Preconditions;", "", "()V", "DEFAULT_MESSAGE", "", "URL_PATTERN", "Ljava/util/regex/Pattern;", "kotlin.jvm.PlatformType", "check", "", "requirements", "", "error", "checkEmptyString", "string", "errorMsg", "checkNotNull", "object", "checkValidOAuthCallback", "url", "checkValidUrl", "isUrl", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: Preconditions.kt */
public final class Preconditions {
    private static final String DEFAULT_MESSAGE = DEFAULT_MESSAGE;
    public static final Preconditions INSTANCE = new Preconditions();
    private static final Pattern URL_PATTERN = Pattern.compile("^[a-zA-Z][a-zA-Z0-9+.-]*://\\S+");

    private Preconditions() {
    }

    public final void checkNotNull(Object obj, String str) {
        Intrinsics.checkParameterIsNotNull(str, "errorMsg");
        check(obj != null, str);
    }

    public final void checkValidUrl(String str, String str2) {
        Intrinsics.checkParameterIsNotNull(str, "url");
        Intrinsics.checkParameterIsNotNull(str2, "errorMsg");
        checkEmptyString(str, str2);
        check(isUrl(str), str2);
    }

    public final void checkValidOAuthCallback(String str, String str2) {
        Intrinsics.checkParameterIsNotNull(str, "url");
        Intrinsics.checkParameterIsNotNull(str2, "errorMsg");
        checkEmptyString(str, str2);
        String lowerCase = str.toLowerCase();
        Intrinsics.checkExpressionValueIsNotNull(lowerCase, "(this as java.lang.String).toLowerCase()");
        if (StringsKt.compareTo(lowerCase, "oob", true) != 0) {
            check(isUrl(str), str2);
        }
    }

    private final boolean isUrl(String str) {
        return URL_PATTERN.matcher(str).matches();
    }

    public final void checkEmptyString(String str, String str2) {
        Intrinsics.checkParameterIsNotNull(str2, "errorMsg");
        boolean z = false;
        if (str != null) {
            CharSequence charSequence = str;
            int length = charSequence.length() - 1;
            int i = 0;
            boolean z2 = false;
            while (i <= length) {
                boolean z3 = charSequence.charAt(!z2 ? i : length) <= ' ';
                if (!z2) {
                    if (!z3) {
                        z2 = true;
                    } else {
                        i++;
                    }
                } else if (!z3) {
                    break;
                } else {
                    length--;
                }
            }
            if (!Intrinsics.areEqual((Object) charSequence.subSequence(i, length + 1).toString(), (Object) "")) {
                z = true;
            }
        }
        check(z, str2);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0042, code lost:
        if (r2 != false) goto L_0x0044;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final void check(boolean r9, java.lang.String r10) {
        /*
            r8 = this;
            if (r10 == 0) goto L_0x0044
            r0 = r10
            java.lang.CharSequence r0 = (java.lang.CharSequence) r0
            int r1 = r0.length()
            r2 = 1
            int r1 = r1 - r2
            r3 = 0
            r4 = 0
            r5 = 0
        L_0x000e:
            if (r4 > r1) goto L_0x002f
            if (r5 != 0) goto L_0x0014
            r6 = r4
            goto L_0x0015
        L_0x0014:
            r6 = r1
        L_0x0015:
            char r6 = r0.charAt(r6)
            r7 = 32
            if (r6 > r7) goto L_0x001f
            r6 = 1
            goto L_0x0020
        L_0x001f:
            r6 = 0
        L_0x0020:
            if (r5 != 0) goto L_0x0029
            if (r6 != 0) goto L_0x0026
            r5 = 1
            goto L_0x000e
        L_0x0026:
            int r4 = r4 + 1
            goto L_0x000e
        L_0x0029:
            if (r6 != 0) goto L_0x002c
            goto L_0x002f
        L_0x002c:
            int r1 = r1 + -1
            goto L_0x000e
        L_0x002f:
            int r1 = r1 + r2
            java.lang.CharSequence r0 = r0.subSequence(r4, r1)
            java.lang.String r0 = r0.toString()
            java.lang.CharSequence r0 = (java.lang.CharSequence) r0
            int r0 = r0.length()
            if (r0 != 0) goto L_0x0041
            goto L_0x0042
        L_0x0041:
            r2 = 0
        L_0x0042:
            if (r2 == 0) goto L_0x0046
        L_0x0044:
            java.lang.String r10 = DEFAULT_MESSAGE
        L_0x0046:
            if (r9 == 0) goto L_0x0049
            return
        L_0x0049:
            java.lang.IllegalArgumentException r9 = new java.lang.IllegalArgumentException
            r9.<init>(r10)
            java.lang.Throwable r9 = (java.lang.Throwable) r9
            throw r9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.iqonic.store.utils.oauthInterceptor.Preconditions.check(boolean, java.lang.String):void");
    }
}
