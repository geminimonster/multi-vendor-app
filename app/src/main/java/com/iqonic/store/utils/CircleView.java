package com.iqonic.store.utils;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;
import com.iqonic.store.R;
import java.util.HashMap;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0006\n\u0002\u0010\u0007\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u0000 \u001c2\u00020\u0001:\u0001\u001cB!\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\b\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bB\u0019\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\b\u0010\u0004\u001a\u0004\u0018\u00010\u0005¢\u0006\u0002\u0010\tB\u000f\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\nJ\b\u0010\u0012\u001a\u00020\u000eH\u0016J\u001a\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0002\u001a\u00020\u00032\b\u0010\u0004\u001a\u0004\u0018\u00010\u0005H\u0002J\u0010\u0010\u0015\u001a\u00020\u00142\u0006\u0010\u0016\u001a\u00020\u0017H\u0014J\u000e\u0010\u0018\u001a\u00020\u00142\u0006\u0010\u000b\u001a\u00020\u0007J\u000e\u0010\u0019\u001a\u00020\u00142\u0006\u0010\r\u001a\u00020\u000eJ\u0010\u0010\u001a\u001a\u00020\u00142\u0006\u0010\u001b\u001a\u00020\u000eH\u0016R\u000e\u0010\u000b\u001a\u00020\u0007X\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\u0007X\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u000eX\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\u0010\u001a\u0004\u0018\u00010\u0011X\u000e¢\u0006\u0002\n\u0000¨\u0006\u001d"}, d2 = {"Lcom/iqonic/store/utils/CircleView;", "Landroid/view/View;", "context", "Landroid/content/Context;", "attrs", "Landroid/util/AttributeSet;", "defStyleAttr", "", "(Landroid/content/Context;Landroid/util/AttributeSet;I)V", "(Landroid/content/Context;Landroid/util/AttributeSet;)V", "(Landroid/content/Context;)V", "mCircleColor", "mGravity", "mRadius", "", "mX", "paint", "Landroid/graphics/Paint;", "getX", "initCircle", "", "onDraw", "canvas", "Landroid/graphics/Canvas;", "setCircleColor", "setRadius", "setX", "x", "Companion", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: CircleView.kt */
public final class CircleView extends View {
    public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
    private static final int DEFAULT_RADIUS = 15;
    private static final int DEFAULT_TEXT_COLOR = DEFAULT_TEXT_COLOR;
    private HashMap _$_findViewCache;
    private int mCircleColor;
    private int mGravity;
    private float mRadius = ((float) DEFAULT_RADIUS);
    private float mX;
    private Paint paint;

    public void _$_clearFindViewByIdCache() {
        HashMap hashMap = this._$_findViewCache;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    public View _$_findCachedViewById(int i) {
        if (this._$_findViewCache == null) {
            this._$_findViewCache = new HashMap();
        }
        View view = (View) this._$_findViewCache.get(Integer.valueOf(i));
        if (view != null) {
            return view;
        }
        View findViewById = findViewById(i);
        this._$_findViewCache.put(Integer.valueOf(i), findViewById);
        return findViewById;
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CircleView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        Intrinsics.checkParameterIsNotNull(context, "context");
        initCircle(context, attributeSet);
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CircleView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        Intrinsics.checkParameterIsNotNull(context, "context");
        initCircle(context, attributeSet);
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CircleView(Context context) {
        super(context);
        Intrinsics.checkParameterIsNotNull(context, "context");
    }

    private final void initCircle(Context context, AttributeSet attributeSet) {
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, R.styleable.CircleView, 0, 0);
        try {
            this.mCircleColor = obtainStyledAttributes.getColor(1, DEFAULT_TEXT_COLOR);
            this.mRadius = obtainStyledAttributes.getDimension(2, (float) DEFAULT_RADIUS);
            this.mGravity = obtainStyledAttributes.getInteger(0, 0);
            Paint paint2 = new Paint();
            this.paint = paint2;
            if (paint2 == null) {
                Intrinsics.throwNpe();
            }
            paint2.setStyle(Paint.Style.FILL);
            Paint paint3 = this.paint;
            if (paint3 == null) {
                Intrinsics.throwNpe();
            }
            paint3.setAntiAlias(true);
            Paint paint4 = this.paint;
            if (paint4 == null) {
                Intrinsics.throwNpe();
            }
            paint4.setColor(this.mCircleColor);
        } finally {
            obtainStyledAttributes.recycle();
        }
    }

    public float getX() {
        return this.mX;
    }

    public void setX(float f) {
        this.mX = f;
    }

    public final void setRadius(float f) {
        this.mRadius = f;
        invalidate();
    }

    public final void setCircleColor(int i) {
        this.mCircleColor = i;
        Paint paint2 = this.paint;
        if (paint2 == null) {
            Intrinsics.throwNpe();
        }
        paint2.setColor(i);
        invalidate();
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        Intrinsics.checkParameterIsNotNull(canvas, "canvas");
        super.onDraw(canvas);
        if (this.mGravity == 0) {
            float width = (float) (getWidth() / 2);
            float f = this.mRadius;
            float height = ((float) (getHeight() / 2)) - (((float) (getHeight() / 2)) - f);
            Paint paint2 = this.paint;
            if (paint2 == null) {
                Intrinsics.throwNpe();
            }
            canvas.drawCircle(width, height, f, paint2);
            return;
        }
        float width2 = (float) (getWidth() / 2);
        float f2 = this.mRadius;
        float height2 = ((float) (getHeight() / 2)) + (((float) (getHeight() / 2)) - f2);
        Paint paint3 = this.paint;
        if (paint3 == null) {
            Intrinsics.throwNpe();
        }
        canvas.drawCircle(width2, height2, f2, paint3);
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XD¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XD¢\u0006\u0002\n\u0000¨\u0006\u0006"}, d2 = {"Lcom/iqonic/store/utils/CircleView$Companion;", "", "()V", "DEFAULT_RADIUS", "", "DEFAULT_TEXT_COLOR", "app_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: CircleView.kt */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }
}
