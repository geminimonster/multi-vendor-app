package com.iqonic.store.utils.photoview;

public interface OnScaleChangedListener {
    void onScaleChange(float f, float f2, float f3);
}
