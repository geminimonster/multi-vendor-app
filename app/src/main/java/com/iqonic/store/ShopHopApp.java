package com.iqonic.store;

import android.app.Dialog;
import android.content.Context;
import androidx.multidex.MultiDex;
import androidx.multidex.MultiDexApplication;
import com.iqonic.store.network.RestApiImpl;
import com.iqonic.store.utils.Constants;
import com.iqonic.store.utils.LocaleManager;
import com.iqonic.store.utils.SharedPrefUtils;
import com.iqonic.store.utils.extensions.AppExtensionsKt;
import com.onesignal.OneSignal;
import com.store.proshop.R;
import io.github.inflationx.calligraphy3.CalligraphyConfig;
import io.github.inflationx.calligraphy3.CalligraphyInterceptor;
import io.github.inflationx.viewpump.ViewPump;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u0000 \b2\u00020\u0001:\u0001\bB\u0005¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0014J\b\u0010\u0007\u001a\u00020\u0004H\u0016¨\u0006\t"}, d2 = {"Lcom/iqonic/store/ShopHopApp;", "Landroidx/multidex/MultiDexApplication;", "()V", "attachBaseContext", "", "base", "Landroid/content/Context;", "onCreate", "Companion", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: ShopHopApp.kt */
public final class ShopHopApp extends MultiDexApplication {
    public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
    /* access modifiers changed from: private */
    public static ShopHopApp appInstance;
    /* access modifiers changed from: private */
    public static int appTheme;
    public static String language;
    public static LocaleManager localeManager;
    /* access modifiers changed from: private */
    public static Dialog noInternetDialog;
    /* access modifiers changed from: private */
    public static RestApiImpl restApiImpl;
    /* access modifiers changed from: private */
    public static SharedPrefUtils sharedPrefUtils;

    public void onCreate() {
        super.onCreate();
        appInstance = this;
        OneSignal.setLogLevel(OneSignal.LOG_LEVEL.VERBOSE, OneSignal.LOG_LEVEL.NONE);
        OneSignal.startInit(this).inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification).unsubscribeWhenNotificationsAreDisabled(true).init();
        SharedPrefUtils sharedPrefInstance = AppExtensionsKt.getSharedPrefInstance();
        appTheme = sharedPrefInstance.getIntValue(Constants.SharedPref.THEME, 2);
        language = sharedPrefInstance.getStringValue(Constants.SharedPref.LANGUAGE, AppExtensionsKt.getLanguage());
        ViewPump.Companion.init(ViewPump.Companion.builder().addInterceptor(new CalligraphyInterceptor(new CalligraphyConfig.Builder().setDefaultFontPath(getString(R.string.font_regular)).setFontAttrId(R.attr.fontPath).build())).build());
    }

    /* access modifiers changed from: protected */
    public void attachBaseContext(Context context) {
        Intrinsics.checkParameterIsNotNull(context, "base");
        localeManager = new LocaleManager(context);
        super.attachBaseContext(context);
        MultiDex.install(this);
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000P\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0005\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u000e\u0010)\u001a\u00020*2\u0006\u0010+\u001a\u00020,J\u000e\u0010-\u001a\u00020*2\u0006\u0010.\u001a\u00020\fJ\u0006\u0010/\u001a\u00020\u0004J\u0006\u00100\u001a\u00020\fR\u000e\u0010\u0003\u001a\u00020\u0004X.¢\u0006\u0002\n\u0000R\u001a\u0010\u0005\u001a\u00020\u0006X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0007\u0010\b\"\u0004\b\t\u0010\nR\u001a\u0010\u000b\u001a\u00020\fX.¢\u0006\u000e\n\u0000\u001a\u0004\b\r\u0010\u000e\"\u0004\b\u000f\u0010\u0010R\u001a\u0010\u0011\u001a\u00020\u0012X.¢\u0006\u000e\n\u0000\u001a\u0004\b\u0013\u0010\u0014\"\u0004\b\u0015\u0010\u0016R\u001c\u0010\u0017\u001a\u0004\u0018\u00010\u0018X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0019\u0010\u001a\"\u0004\b\u001b\u0010\u001cR\u001c\u0010\u001d\u001a\u0004\u0018\u00010\u001eX\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u001f\u0010 \"\u0004\b!\u0010\"R\u001c\u0010#\u001a\u0004\u0018\u00010$X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b%\u0010&\"\u0004\b'\u0010(¨\u00061"}, d2 = {"Lcom/iqonic/store/ShopHopApp$Companion;", "", "()V", "appInstance", "Lcom/iqonic/store/ShopHopApp;", "appTheme", "", "getAppTheme", "()I", "setAppTheme", "(I)V", "language", "", "getLanguage", "()Ljava/lang/String;", "setLanguage", "(Ljava/lang/String;)V", "localeManager", "Lcom/iqonic/store/utils/LocaleManager;", "getLocaleManager", "()Lcom/iqonic/store/utils/LocaleManager;", "setLocaleManager", "(Lcom/iqonic/store/utils/LocaleManager;)V", "noInternetDialog", "Landroid/app/Dialog;", "getNoInternetDialog", "()Landroid/app/Dialog;", "setNoInternetDialog", "(Landroid/app/Dialog;)V", "restApiImpl", "Lcom/iqonic/store/network/RestApiImpl;", "getRestApiImpl", "()Lcom/iqonic/store/network/RestApiImpl;", "setRestApiImpl", "(Lcom/iqonic/store/network/RestApiImpl;)V", "sharedPrefUtils", "Lcom/iqonic/store/utils/SharedPrefUtils;", "getSharedPrefUtils", "()Lcom/iqonic/store/utils/SharedPrefUtils;", "setSharedPrefUtils", "(Lcom/iqonic/store/utils/SharedPrefUtils;)V", "changeAppTheme", "", "isDark", "", "changeLanguage", "aLanguage", "getAppInstance", "getServerUrl", "app_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: ShopHopApp.kt */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        public final LocaleManager getLocaleManager() {
            LocaleManager access$getLocaleManager$cp = ShopHopApp.localeManager;
            if (access$getLocaleManager$cp == null) {
                Intrinsics.throwUninitializedPropertyAccessException("localeManager");
            }
            return access$getLocaleManager$cp;
        }

        public final void setLocaleManager(LocaleManager localeManager) {
            Intrinsics.checkParameterIsNotNull(localeManager, "<set-?>");
            ShopHopApp.localeManager = localeManager;
        }

        public final SharedPrefUtils getSharedPrefUtils() {
            return ShopHopApp.sharedPrefUtils;
        }

        public final void setSharedPrefUtils(SharedPrefUtils sharedPrefUtils) {
            ShopHopApp.sharedPrefUtils = sharedPrefUtils;
        }

        public final Dialog getNoInternetDialog() {
            return ShopHopApp.noInternetDialog;
        }

        public final void setNoInternetDialog(Dialog dialog) {
            ShopHopApp.noInternetDialog = dialog;
        }

        public final RestApiImpl getRestApiImpl() {
            return ShopHopApp.restApiImpl;
        }

        public final void setRestApiImpl(RestApiImpl restApiImpl) {
            ShopHopApp.restApiImpl = restApiImpl;
        }

        public final String getLanguage() {
            String access$getLanguage$cp = ShopHopApp.language;
            if (access$getLanguage$cp == null) {
                Intrinsics.throwUninitializedPropertyAccessException("language");
            }
            return access$getLanguage$cp;
        }

        public final void setLanguage(String str) {
            Intrinsics.checkParameterIsNotNull(str, "<set-?>");
            ShopHopApp.language = str;
        }

        public final int getAppTheme() {
            return ShopHopApp.appTheme;
        }

        public final void setAppTheme(int i) {
            ShopHopApp.appTheme = i;
        }

        public final ShopHopApp getAppInstance() {
            ShopHopApp access$getAppInstance$cp = ShopHopApp.appInstance;
            if (access$getAppInstance$cp == null) {
                Intrinsics.throwUninitializedPropertyAccessException("appInstance");
            }
            return access$getAppInstance$cp;
        }

        public final void changeAppTheme(boolean z) {
            SharedPrefUtils sharedPrefInstance = AppExtensionsKt.getSharedPrefInstance();
            if (z) {
                sharedPrefInstance.setValue(Constants.SharedPref.THEME, 1);
            } else {
                sharedPrefInstance.setValue(Constants.SharedPref.THEME, 2);
            }
            ShopHopApp.Companion.setAppTheme(sharedPrefInstance.getIntValue(Constants.SharedPref.THEME, 2));
        }

        public final void changeLanguage(String str) {
            Intrinsics.checkParameterIsNotNull(str, "aLanguage");
            AppExtensionsKt.getSharedPrefInstance().setValue(Constants.SharedPref.LANGUAGE, str);
            setLanguage(str);
        }

        public final String getServerUrl() {
            String string = getAppInstance().getString(R.string.base_url);
            Intrinsics.checkExpressionValueIsNotNull(string, "getAppInstance().getString(R.string.base_url)");
            return string;
        }
    }
}
