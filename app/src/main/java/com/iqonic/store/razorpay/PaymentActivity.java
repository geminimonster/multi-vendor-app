package com.iqonic.store.razorpay;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;
import com.facebook.share.internal.MessengerShareContentUtility;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.iqonic.store.utils.Constants;
import com.razorpay.Checkout;
import com.razorpay.PaymentResultListener;
import com.store.proshop.R;
import java.util.HashMap;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Reflection;
import org.json.JSONObject;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0005\u0018\u00002\u00020\u00012\u00020\u0002B\u0005¢\u0006\u0002\u0010\u0003J\u0012\u0010\u0006\u001a\u00020\u00072\b\u0010\b\u001a\u0004\u0018\u00010\tH\u0014J\u001a\u0010\n\u001a\u00020\u00072\u0006\u0010\u000b\u001a\u00020\f2\b\u0010\r\u001a\u0004\u0018\u00010\u0005H\u0016J\u0012\u0010\u000e\u001a\u00020\u00072\b\u0010\u000f\u001a\u0004\u0018\u00010\u0005H\u0016J\b\u0010\u0010\u001a\u00020\u0007H\u0002R\u000e\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0011"}, d2 = {"Lcom/iqonic/store/razorpay/PaymentActivity;", "Landroid/app/Activity;", "Lcom/razorpay/PaymentResultListener;", "()V", "TAG", "", "onCreate", "", "savedInstanceState", "Landroid/os/Bundle;", "onPaymentError", "errorCode", "", "response", "onPaymentSuccess", "razorpayPaymentId", "startPayment", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: PaymentActivity.kt */
public final class PaymentActivity extends Activity implements PaymentResultListener {
    private final String TAG = Reflection.getOrCreateKotlinClass(PaymentActivity.class).toString();
    private HashMap _$_findViewCache;

    public void _$_clearFindViewByIdCache() {
        HashMap hashMap = this._$_findViewCache;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    public View _$_findCachedViewById(int i) {
        if (this._$_findViewCache == null) {
            this._$_findViewCache = new HashMap();
        }
        View view = (View) this._$_findViewCache.get(Integer.valueOf(i));
        if (view != null) {
            return view;
        }
        View findViewById = findViewById(i);
        this._$_findViewCache.put(Integer.valueOf(i), findViewById);
        return findViewById;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.payment_razorpay_main);
        Checkout.preload(getApplicationContext());
        View findViewById = findViewById(R.id.razerPayPayment);
        Intrinsics.checkExpressionValueIsNotNull(findViewById, "findViewById(R.id.razerPayPayment)");
        ((Button) findViewById).setOnClickListener(new PaymentActivity$onCreate$1(this));
    }

    /* access modifiers changed from: private */
    public final void startPayment() {
        Activity activity = this;
        Checkout checkout = new Checkout();
        checkout.setKeyID(getString(R.string.razor_key));
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("name", "Product Name");
            jSONObject.put("description", "Demoing Charges");
            jSONObject.put(MessengerShareContentUtility.MEDIA_IMAGE, "https://s3.amazonaws.com/rzp-mobile/images/rzp.png");
            jSONObject.put(FirebaseAnalytics.Param.CURRENCY, "INR");
            jSONObject.put("amount", "100");
            JSONObject jSONObject2 = new JSONObject();
            jSONObject2.put("email", "sandipkalola1990@gmail.com");
            jSONObject2.put(Constants.SharedPref.CONTACT, "9638472777");
            jSONObject.put("prefill", jSONObject2);
            checkout.open(activity, jSONObject);
        } catch (Exception e) {
            Toast.makeText(activity, getString(R.string.lbl_error_in_payment) + e.getMessage(), 1).show();
            e.printStackTrace();
        }
    }

    public void onPaymentError(int i, String str) {
        try {
            Toast.makeText(this, getString(R.string.lbl_error_in_payment) + i + "\n" + str, 1).show();
        } catch (Exception e) {
            Log.e(this.TAG, "Exception in onPaymentSuccess", e);
        }
    }

    public void onPaymentSuccess(String str) {
        try {
            Toast.makeText(this, getString(R.string.lbl_payment_successful) + str, 1).show();
        } catch (Exception e) {
            Log.e(this.TAG, "Exception in onPaymentSuccess", e);
        }
    }
}
