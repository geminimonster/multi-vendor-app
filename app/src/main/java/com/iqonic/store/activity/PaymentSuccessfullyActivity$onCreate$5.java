package com.iqonic.store.activity;

import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n¢\u0006\u0002\b\u0004"}, d2 = {"<anonymous>", "", "it", "", "invoke"}, k = 3, mv = {1, 1, 16})
/* compiled from: PaymentSuccessfullyActivity.kt */
final class PaymentSuccessfullyActivity$onCreate$5 extends Lambda implements Function1<String, Unit> {
    public static final PaymentSuccessfullyActivity$onCreate$5 INSTANCE = new PaymentSuccessfullyActivity$onCreate$5();

    PaymentSuccessfullyActivity$onCreate$5() {
        super(1);
    }

    public final void invoke(String str) {
        Intrinsics.checkParameterIsNotNull(str, "it");
    }

    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        invoke((String) obj);
        return Unit.INSTANCE;
    }
}
