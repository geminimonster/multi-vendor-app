package com.iqonic.store.activity;

import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.appevents.AppEventsConstants;
import com.google.android.material.button.MaterialButton;
import com.iqonic.store.AppBaseActivity;
import com.iqonic.store.adapter.BaseAdapter;
import com.iqonic.store.models.ProductReviewData;
import com.iqonic.store.utils.Constants;
import com.iqonic.store.utils.extensions.AppExtensionsKt;
import com.iqonic.store.utils.extensions.ExtensionsKt;
import com.iqonic.store.utils.extensions.NetworkExtensionKt;
import com.iqonic.store.utils.extensions.StringExtensionsKt;
import com.iqonic.store.utils.extensions.ViewExtensionsKt;
import com.store.proshop.R;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.StringCompanionObject;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\b\u0010\b\u001a\u00020\tH\u0002J\u0010\u0010\n\u001a\u00020\t2\u0006\u0010\u000b\u001a\u00020\u0007H\u0002J\b\u0010\f\u001a\u00020\tH\u0002J\b\u0010\r\u001a\u00020\tH\u0002J\u0012\u0010\u000e\u001a\u00020\t2\b\u0010\u000f\u001a\u0004\u0018\u00010\u0010H\u0015J\u0016\u0010\u0011\u001a\u00020\t2\f\u0010\u0012\u001a\b\u0012\u0004\u0012\u00020\u00070\u0013H\u0002J\u0010\u0010\u0014\u001a\u00020\t2\u0006\u0010\u0015\u001a\u00020\u0016H\u0002J\u0010\u0010\u0017\u001a\u00020\t2\u0006\u0010\u000b\u001a\u00020\u0007H\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u000e¢\u0006\u0002\n\u0000R\u0014\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0018"}, d2 = {"Lcom/iqonic/store/activity/ReviewsActivity;", "Lcom/iqonic/store/AppBaseActivity;", "()V", "mPId", "", "mReviewAdapter", "Lcom/iqonic/store/adapter/BaseAdapter;", "Lcom/iqonic/store/models/ProductReviewData;", "changeColor", "", "confirmDialog", "mode", "createProductReview", "listProductReviews", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "setRating", "data", "", "showList", "isVisible", "", "updateReview", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: ReviewsActivity.kt */
public final class ReviewsActivity extends AppBaseActivity {
    private HashMap _$_findViewCache;
    /* access modifiers changed from: private */
    public int mPId;
    /* access modifiers changed from: private */
    public final BaseAdapter<ProductReviewData> mReviewAdapter = new BaseAdapter<>(R.layout.item_review, new ReviewsActivity$mReviewAdapter$1(this));

    public void _$_clearFindViewByIdCache() {
        HashMap hashMap = this._$_findViewCache;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    public View _$_findCachedViewById(int i) {
        if (this._$_findViewCache == null) {
            this._$_findViewCache = new HashMap();
        }
        View view = (View) this._$_findViewCache.get(Integer.valueOf(i));
        if (view != null) {
            return view;
        }
        View findViewById = findViewById(i);
        this._$_findViewCache.put(Integer.valueOf(i), findViewById);
        return findViewById;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_reviews);
        Toolbar toolbar = (Toolbar) _$_findCachedViewById(com.iqonic.store.R.id.toolbar);
        Intrinsics.checkExpressionValueIsNotNull(toolbar, "toolbar");
        setToolbar(toolbar);
        mAppBarColor();
        setTitle(getString(R.string.lbl_reviews));
        changeColor();
        this.mPId = getIntent().getIntExtra(Constants.KeyIntent.PRODUCT_ID, 0);
        ImageView imageView = (ImageView) _$_findCachedViewById(com.iqonic.store.R.id.ivBackground);
        Intrinsics.checkExpressionValueIsNotNull(imageView, "ivBackground");
        ViewExtensionsKt.setStrokedBackground$default(imageView, ExtensionsKt.color(this, R.color.favourite_unselected_background), ExtensionsKt.color(this, R.color.dots_color), 0.0f, 0, 12, (Object) null);
        RecyclerView recyclerView = (RecyclerView) _$_findCachedViewById(com.iqonic.store.R.id.rvReview);
        Intrinsics.checkExpressionValueIsNotNull(recyclerView, "rvReview");
        ExtensionsKt.setVerticalLayout$default(recyclerView, false, 1, (Object) null);
        RecyclerView recyclerView2 = (RecyclerView) _$_findCachedViewById(com.iqonic.store.R.id.rvReview);
        Intrinsics.checkExpressionValueIsNotNull(recyclerView2, "rvReview");
        recyclerView2.setAdapter(this.mReviewAdapter);
        listProductReviews();
        MaterialButton materialButton = (MaterialButton) _$_findCachedViewById(com.iqonic.store.R.id.btnRateNow);
        materialButton.setOnClickListener(new ReviewsActivity$onCreate$$inlined$onClick$1(materialButton, this));
        ((SeekBar) _$_findCachedViewById(com.iqonic.store.R.id.sb1Star)).setOnTouchListener(ReviewsActivity$onCreate$2.INSTANCE);
        ((SeekBar) _$_findCachedViewById(com.iqonic.store.R.id.sb2Star)).setOnTouchListener(ReviewsActivity$onCreate$3.INSTANCE);
        ((SeekBar) _$_findCachedViewById(com.iqonic.store.R.id.sb3Star)).setOnTouchListener(ReviewsActivity$onCreate$4.INSTANCE);
        ((SeekBar) _$_findCachedViewById(com.iqonic.store.R.id.sb4Star)).setOnTouchListener(ReviewsActivity$onCreate$5.INSTANCE);
        ((SeekBar) _$_findCachedViewById(com.iqonic.store.R.id.sb5Star)).setOnTouchListener(ReviewsActivity$onCreate$6.INSTANCE);
    }

    /* access modifiers changed from: private */
    public final void setRating(List<ProductReviewData> list) {
        String str;
        String str2 = "tv3Count";
        int i = 0;
        if (list.isEmpty()) {
            TextView textView = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvReviewRate);
            Intrinsics.checkExpressionValueIsNotNull(textView, "tvReviewRate");
            ViewExtensionsKt.hide(textView);
            TextView textView2 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvTotalReview);
            Intrinsics.checkExpressionValueIsNotNull(textView2, "tvTotalReview");
            textView2.setText(getString(R.string.lbl_no_reviews));
            SeekBar seekBar = (SeekBar) _$_findCachedViewById(com.iqonic.store.R.id.sb1Star);
            Intrinsics.checkExpressionValueIsNotNull(seekBar, "sb1Star");
            seekBar.setProgress(0);
            SeekBar seekBar2 = (SeekBar) _$_findCachedViewById(com.iqonic.store.R.id.sb2Star);
            Intrinsics.checkExpressionValueIsNotNull(seekBar2, "sb2Star");
            seekBar2.setProgress(0);
            SeekBar seekBar3 = (SeekBar) _$_findCachedViewById(com.iqonic.store.R.id.sb3Star);
            Intrinsics.checkExpressionValueIsNotNull(seekBar3, "sb3Star");
            seekBar3.setProgress(0);
            SeekBar seekBar4 = (SeekBar) _$_findCachedViewById(com.iqonic.store.R.id.sb4Star);
            Intrinsics.checkExpressionValueIsNotNull(seekBar4, "sb4Star");
            seekBar4.setProgress(0);
            SeekBar seekBar5 = (SeekBar) _$_findCachedViewById(com.iqonic.store.R.id.sb5Star);
            Intrinsics.checkExpressionValueIsNotNull(seekBar5, "sb5Star");
            seekBar5.setProgress(0);
            TextView textView3 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tv5Count);
            Intrinsics.checkExpressionValueIsNotNull(textView3, "tv5Count");
            textView3.setText(AppEventsConstants.EVENT_PARAM_VALUE_NO);
            TextView textView4 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tv4Count);
            Intrinsics.checkExpressionValueIsNotNull(textView4, "tv4Count");
            textView4.setText(AppEventsConstants.EVENT_PARAM_VALUE_NO);
            TextView textView5 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tv3Count);
            Intrinsics.checkExpressionValueIsNotNull(textView5, str2);
            textView5.setText(AppEventsConstants.EVENT_PARAM_VALUE_NO);
            TextView textView6 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tv2Count);
            Intrinsics.checkExpressionValueIsNotNull(textView6, "tv2Count");
            textView6.setText(AppEventsConstants.EVENT_PARAM_VALUE_NO);
            TextView textView7 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tv1Count);
            Intrinsics.checkExpressionValueIsNotNull(textView7, "tv1Count");
            textView7.setText(AppEventsConstants.EVENT_PARAM_VALUE_NO);
            return;
        }
        Iterator<ProductReviewData> it = list.iterator();
        String str3 = "tv1Count";
        String str4 = "tv2Count";
        String str5 = "tvReviewRate";
        int i2 = 0;
        int i3 = 0;
        int i4 = 0;
        int i5 = 0;
        while (true) {
            str = str2;
            if (!it.hasNext()) {
                break;
            }
            Iterator<ProductReviewData> it2 = it;
            int rating = it.next().getRating();
            if (rating == 1) {
                i4++;
            } else if (rating == 2) {
                i2++;
            } else if (rating == 3) {
                i3++;
            } else if (rating == 4) {
                i++;
            } else if (rating == 5) {
                i5++;
            }
            str2 = str;
            it = it2;
        }
        if (i5 != 0 || i != 0 || i3 != 0 || i2 != 0 || i4 != 0) {
            SeekBar seekBar6 = (SeekBar) _$_findCachedViewById(com.iqonic.store.R.id.sb1Star);
            Intrinsics.checkExpressionValueIsNotNull(seekBar6, "sb1Star");
            seekBar6.setMax(list.size());
            SeekBar seekBar7 = (SeekBar) _$_findCachedViewById(com.iqonic.store.R.id.sb2Star);
            Intrinsics.checkExpressionValueIsNotNull(seekBar7, "sb2Star");
            seekBar7.setMax(list.size());
            SeekBar seekBar8 = (SeekBar) _$_findCachedViewById(com.iqonic.store.R.id.sb3Star);
            Intrinsics.checkExpressionValueIsNotNull(seekBar8, "sb3Star");
            seekBar8.setMax(list.size());
            SeekBar seekBar9 = (SeekBar) _$_findCachedViewById(com.iqonic.store.R.id.sb4Star);
            Intrinsics.checkExpressionValueIsNotNull(seekBar9, "sb4Star");
            seekBar9.setMax(list.size());
            SeekBar seekBar10 = (SeekBar) _$_findCachedViewById(com.iqonic.store.R.id.sb5Star);
            Intrinsics.checkExpressionValueIsNotNull(seekBar10, "sb5Star");
            seekBar10.setMax(list.size());
            SeekBar seekBar11 = (SeekBar) _$_findCachedViewById(com.iqonic.store.R.id.sb1Star);
            Intrinsics.checkExpressionValueIsNotNull(seekBar11, "sb1Star");
            seekBar11.setProgress(i4);
            SeekBar seekBar12 = (SeekBar) _$_findCachedViewById(com.iqonic.store.R.id.sb2Star);
            Intrinsics.checkExpressionValueIsNotNull(seekBar12, "sb2Star");
            seekBar12.setProgress(i2);
            SeekBar seekBar13 = (SeekBar) _$_findCachedViewById(com.iqonic.store.R.id.sb3Star);
            Intrinsics.checkExpressionValueIsNotNull(seekBar13, "sb3Star");
            seekBar13.setProgress(i3);
            SeekBar seekBar14 = (SeekBar) _$_findCachedViewById(com.iqonic.store.R.id.sb4Star);
            Intrinsics.checkExpressionValueIsNotNull(seekBar14, "sb4Star");
            seekBar14.setProgress(i);
            SeekBar seekBar15 = (SeekBar) _$_findCachedViewById(com.iqonic.store.R.id.sb5Star);
            Intrinsics.checkExpressionValueIsNotNull(seekBar15, "sb5Star");
            seekBar15.setProgress(i5);
            TextView textView8 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvTotalReview);
            Intrinsics.checkExpressionValueIsNotNull(textView8, "tvTotalReview");
            StringCompanionObject stringCompanionObject = StringCompanionObject.INSTANCE;
            String format = String.format("%d " + getString(R.string.lbl_reviews), Arrays.copyOf(new Object[]{Integer.valueOf(list.size())}, 1));
            Intrinsics.checkExpressionValueIsNotNull(format, "java.lang.String.format(format, *args)");
            textView8.setText(format);
            TextView textView9 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tv5Count);
            Intrinsics.checkExpressionValueIsNotNull(textView9, "tv5Count");
            textView9.setText(String.valueOf(i5));
            TextView textView10 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tv4Count);
            Intrinsics.checkExpressionValueIsNotNull(textView10, "tv4Count");
            textView10.setText(String.valueOf(i));
            TextView textView11 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tv3Count);
            Intrinsics.checkExpressionValueIsNotNull(textView11, str);
            textView11.setText(String.valueOf(i3));
            TextView textView12 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tv2Count);
            Intrinsics.checkExpressionValueIsNotNull(textView12, str4);
            textView12.setText(String.valueOf(i2));
            TextView textView13 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tv1Count);
            Intrinsics.checkExpressionValueIsNotNull(textView13, str3);
            textView13.setText(String.valueOf(i4));
            TextView textView14 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvReviewRate);
            String str6 = str5;
            Intrinsics.checkExpressionValueIsNotNull(textView14, str6);
            ViewExtensionsKt.show(textView14);
            TextView textView15 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvReviewRate);
            Intrinsics.checkExpressionValueIsNotNull(textView15, str6);
            textView15.setText(String.valueOf((((((i5 * 5) + (i * 4)) + (i3 * 3)) + (i2 * 2)) + (i4 * 1)) / ((((i5 + i) + i3) + i2) + i4)));
        }
    }

    /* access modifiers changed from: private */
    public final void createProductReview() {
        Dialog dialog = new Dialog(this);
        Window window = dialog.getWindow();
        if (window != null) {
            window.setBackgroundDrawable(new ColorDrawable(0));
        }
        dialog.setContentView(R.layout.dialog_rate);
        dialog.setCanceledOnTouchOutside(false);
        Window window2 = dialog.getWindow();
        if (window2 != null) {
            window2.setLayout(-1, -2);
        }
        TextView textView = (TextView) dialog.findViewById(com.iqonic.store.R.id.lblReview);
        Intrinsics.checkExpressionValueIsNotNull(textView, "dialog.lblReview");
        ExtensionsKt.changeTextPrimaryColor(textView);
        EditText editText = (EditText) dialog.findViewById(com.iqonic.store.R.id.edtReview);
        Intrinsics.checkExpressionValueIsNotNull(editText, "dialog.edtReview");
        ExtensionsKt.changeTextPrimaryColor(editText);
        MaterialButton materialButton = (MaterialButton) dialog.findViewById(com.iqonic.store.R.id.btnSubmit);
        Intrinsics.checkExpressionValueIsNotNull(materialButton, "dialog.btnSubmit");
        ExtensionsKt.changeBackgroundTint(materialButton, AppExtensionsKt.getButtonColor());
        MaterialButton materialButton2 = (MaterialButton) dialog.findViewById(com.iqonic.store.R.id.btnSubmit);
        materialButton2.setOnClickListener(new ReviewsActivity$createProductReview$$inlined$onClick$1(materialButton2, this, dialog));
        View findViewById = dialog.findViewById(com.iqonic.store.R.id.viewCloseDialog);
        findViewById.setOnClickListener(new ReviewsActivity$createProductReview$$inlined$onClick$2(findViewById, dialog));
        dialog.show();
    }

    /* access modifiers changed from: private */
    public final void confirmDialog(ProductReviewData productReviewData) {
        String string = getString(R.string.msg_confirmation);
        Intrinsics.checkExpressionValueIsNotNull(string, "getString(R.string.msg_confirmation)");
        AppExtensionsKt.getAlertDialog$default(this, string, (String) null, (String) null, (String) null, new ReviewsActivity$confirmDialog$1(this, productReviewData), ReviewsActivity$confirmDialog$2.INSTANCE, 14, (Object) null).show();
    }

    /* access modifiers changed from: private */
    public final void updateReview(ProductReviewData productReviewData) {
        Dialog dialog = new Dialog(this);
        Window window = dialog.getWindow();
        if (window != null) {
            window.setBackgroundDrawable(new ColorDrawable(0));
        }
        dialog.setContentView(R.layout.dialog_rate);
        Window window2 = dialog.getWindow();
        if (window2 != null) {
            window2.setLayout(-1, -2);
        }
        ((EditText) dialog.findViewById(com.iqonic.store.R.id.edtReview)).setText(StringExtensionsKt.getHtmlString(productReviewData.getReview()));
        RatingBar ratingBar = (RatingBar) dialog.findViewById(com.iqonic.store.R.id.ratingBar);
        Intrinsics.checkExpressionValueIsNotNull(ratingBar, "dialog.ratingBar");
        ratingBar.setRating((float) productReviewData.getRating());
        TextView textView = (TextView) dialog.findViewById(com.iqonic.store.R.id.lblReview);
        Intrinsics.checkExpressionValueIsNotNull(textView, "dialog.lblReview");
        ExtensionsKt.changeTextPrimaryColor(textView);
        EditText editText = (EditText) dialog.findViewById(com.iqonic.store.R.id.edtReview);
        Intrinsics.checkExpressionValueIsNotNull(editText, "dialog.edtReview");
        ExtensionsKt.changeTextPrimaryColor(editText);
        MaterialButton materialButton = (MaterialButton) dialog.findViewById(com.iqonic.store.R.id.btnSubmit);
        Intrinsics.checkExpressionValueIsNotNull(materialButton, "dialog.btnSubmit");
        ExtensionsKt.changeBackgroundTint(materialButton, AppExtensionsKt.getButtonColor());
        View findViewById = dialog.findViewById(com.iqonic.store.R.id.viewCloseDialog);
        findViewById.setOnClickListener(new ReviewsActivity$updateReview$$inlined$onClick$1(findViewById, dialog));
        MaterialButton materialButton2 = (MaterialButton) dialog.findViewById(com.iqonic.store.R.id.btnSubmit);
        materialButton2.setOnClickListener(new ReviewsActivity$updateReview$$inlined$onClick$2(materialButton2, this, dialog, productReviewData));
        dialog.show();
    }

    /* access modifiers changed from: private */
    public final void listProductReviews() {
        showProgress(true);
        NetworkExtensionKt.listReview(this, this.mPId, new ReviewsActivity$listProductReviews$1(this));
    }

    /* access modifiers changed from: private */
    public final void showList(boolean z) {
        if (z) {
            RelativeLayout relativeLayout = (RelativeLayout) _$_findCachedViewById(com.iqonic.store.R.id.rlNoData);
            Intrinsics.checkExpressionValueIsNotNull(relativeLayout, "rlNoData");
            ViewExtensionsKt.hide(relativeLayout);
            RecyclerView recyclerView = (RecyclerView) _$_findCachedViewById(com.iqonic.store.R.id.rvReview);
            Intrinsics.checkExpressionValueIsNotNull(recyclerView, "rvReview");
            ViewExtensionsKt.show(recyclerView);
            LinearLayout linearLayout = (LinearLayout) _$_findCachedViewById(com.iqonic.store.R.id.reviewView);
            Intrinsics.checkExpressionValueIsNotNull(linearLayout, "reviewView");
            ViewExtensionsKt.show(linearLayout);
            TextView textView = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvLblReview);
            Intrinsics.checkExpressionValueIsNotNull(textView, "tvLblReview");
            ViewExtensionsKt.show(textView);
            return;
        }
        RecyclerView recyclerView2 = (RecyclerView) _$_findCachedViewById(com.iqonic.store.R.id.rvReview);
        Intrinsics.checkExpressionValueIsNotNull(recyclerView2, "rvReview");
        ViewExtensionsKt.hide(recyclerView2);
        TextView textView2 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvLblReview);
        Intrinsics.checkExpressionValueIsNotNull(textView2, "tvLblReview");
        ViewExtensionsKt.hide(textView2);
        RelativeLayout relativeLayout2 = (RelativeLayout) _$_findCachedViewById(com.iqonic.store.R.id.rlNoData);
        Intrinsics.checkExpressionValueIsNotNull(relativeLayout2, "rlNoData");
        ViewExtensionsKt.show(relativeLayout2);
        TextView textView3 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvMsg);
        Intrinsics.checkExpressionValueIsNotNull(textView3, "tvMsg");
        textView3.setText(getString(R.string.lbl_no_reviews));
    }

    private final void changeColor() {
        TextView textView = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvLblRatings);
        Intrinsics.checkExpressionValueIsNotNull(textView, "tvLblRatings");
        ExtensionsKt.changeTextPrimaryColor(textView);
        MaterialButton materialButton = (MaterialButton) _$_findCachedViewById(com.iqonic.store.R.id.btnRateNow);
        Intrinsics.checkExpressionValueIsNotNull(materialButton, "btnRateNow");
        ExtensionsKt.changePrimaryColor(materialButton);
        TextView textView2 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvReviewRate);
        Intrinsics.checkExpressionValueIsNotNull(textView2, "tvReviewRate");
        ExtensionsKt.changeTextPrimaryColor(textView2);
        TextView textView3 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvTotalReview);
        Intrinsics.checkExpressionValueIsNotNull(textView3, "tvTotalReview");
        ExtensionsKt.changeTextSecondaryColor(textView3);
        TextView textView4 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvLbl5);
        Intrinsics.checkExpressionValueIsNotNull(textView4, "tvLbl5");
        ExtensionsKt.changeTextPrimaryColor(textView4);
        TextView textView5 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tv5Count);
        Intrinsics.checkExpressionValueIsNotNull(textView5, "tv5Count");
        ExtensionsKt.changeTextSecondaryColor(textView5);
        TextView textView6 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvLbl4);
        Intrinsics.checkExpressionValueIsNotNull(textView6, "tvLbl4");
        ExtensionsKt.changeTextPrimaryColor(textView6);
        TextView textView7 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tv4Count);
        Intrinsics.checkExpressionValueIsNotNull(textView7, "tv4Count");
        ExtensionsKt.changeTextSecondaryColor(textView7);
        TextView textView8 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvLbl3);
        Intrinsics.checkExpressionValueIsNotNull(textView8, "tvLbl3");
        ExtensionsKt.changeTextPrimaryColor(textView8);
        TextView textView9 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tv3Count);
        Intrinsics.checkExpressionValueIsNotNull(textView9, "tv3Count");
        ExtensionsKt.changeTextSecondaryColor(textView9);
        TextView textView10 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvLbl2);
        Intrinsics.checkExpressionValueIsNotNull(textView10, "tvLbl2");
        ExtensionsKt.changeTextPrimaryColor(textView10);
        TextView textView11 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tv2Count);
        Intrinsics.checkExpressionValueIsNotNull(textView11, "tv2Count");
        ExtensionsKt.changeTextSecondaryColor(textView11);
        TextView textView12 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvLbl1);
        Intrinsics.checkExpressionValueIsNotNull(textView12, "tvLbl1");
        ExtensionsKt.changeTextPrimaryColor(textView12);
        TextView textView13 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tv1Count);
        Intrinsics.checkExpressionValueIsNotNull(textView13, "tv1Count");
        ExtensionsKt.changeTextSecondaryColor(textView13);
        TextView textView14 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvLblReview);
        Intrinsics.checkExpressionValueIsNotNull(textView14, "tvLblReview");
        ExtensionsKt.changeTextPrimaryColor(textView14);
        LinearLayout linearLayout = (LinearLayout) _$_findCachedViewById(com.iqonic.store.R.id.llMain);
        Intrinsics.checkExpressionValueIsNotNull(linearLayout, "llMain");
        ExtensionsKt.changeBackgroundColor(linearLayout);
    }
}
