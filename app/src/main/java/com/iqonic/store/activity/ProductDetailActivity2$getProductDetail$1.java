package com.iqonic.store.activity;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.Spinner;
import android.widget.TextView;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.RecyclerView;
import com.google.android.material.button.MaterialButton;
import com.iqonic.store.R;
import com.iqonic.store.adapter.BaseAdapter;
import com.iqonic.store.adapter.SpinnerAdapter;
import com.iqonic.store.models.Attributes;
import com.iqonic.store.models.StoreCategory;
import com.iqonic.store.models.StoreProductModel;
import com.iqonic.store.models.StoreUpSale;
import com.iqonic.store.utils.extensions.AppExtensionsKt;
import com.iqonic.store.utils.extensions.ExtensionsKt;
import com.iqonic.store.utils.extensions.StringExtensionsKt;
import com.iqonic.store.utils.extensions.ViewExtensionsKt;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.Unit;
import kotlin.collections.CollectionsKt;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.functions.Function3;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;
import kotlin.text.StringsKt;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0016\u0010\u0002\u001a\u0012\u0012\u0004\u0012\u00020\u00040\u0003j\b\u0012\u0004\u0012\u00020\u0004`\u0005H\n¢\u0006\u0002\b\u0006"}, d2 = {"<anonymous>", "", "it", "Ljava/util/ArrayList;", "Lcom/iqonic/store/models/StoreProductModel;", "Lkotlin/collections/ArrayList;", "invoke"}, k = 3, mv = {1, 1, 16})
/* compiled from: ProductDetailActivity2.kt */
final class ProductDetailActivity2$getProductDetail$1 extends Lambda implements Function1<ArrayList<StoreProductModel>, Unit> {
    final /* synthetic */ ProductDetailActivity2 this$0;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    ProductDetailActivity2$getProductDetail$1(ProductDetailActivity2 productDetailActivity2) {
        super(1);
        this.this$0 = productDetailActivity2;
    }

    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        invoke((ArrayList<StoreProductModel>) (ArrayList) obj);
        return Unit.INSTANCE;
    }

    public final void invoke(final ArrayList<StoreProductModel> arrayList) {
        Intrinsics.checkParameterIsNotNull(arrayList, "it");
        this.this$0.showProgress(false);
        NestedScrollView nestedScrollView = (NestedScrollView) this.this$0._$_findCachedViewById(R.id.scrollView);
        Intrinsics.checkExpressionValueIsNotNull(nestedScrollView, "scrollView");
        ViewExtensionsKt.show(nestedScrollView);
        ProductDetailActivity2 productDetailActivity2 = this.this$0;
        StoreProductModel storeProductModel = arrayList.get(0);
        Intrinsics.checkExpressionValueIsNotNull(storeProductModel, "it[0]");
        productDetailActivity2.viewVariableImage(storeProductModel);
        TextView textView = (TextView) this.this$0._$_findCachedViewById(R.id.tvItemProductOriginalPrice);
        Intrinsics.checkExpressionValueIsNotNull(textView, "tvItemProductOriginalPrice");
        ExtensionsKt.applyStrike(textView);
        TextView textView2 = (TextView) this.this$0._$_findCachedViewById(R.id.tvName);
        Intrinsics.checkExpressionValueIsNotNull(textView2, "tvName");
        textView2.setText(arrayList.get(0).getName());
        RatingBar ratingBar = (RatingBar) this.this$0._$_findCachedViewById(R.id.tvItemProductRating);
        Intrinsics.checkExpressionValueIsNotNull(ratingBar, "tvItemProductRating");
        String averageRating = arrayList.get(0).getAverageRating();
        if (averageRating == null) {
            Intrinsics.throwNpe();
        }
        ratingBar.setRating(Float.parseFloat(averageRating));
        TextView textView3 = (TextView) this.this$0._$_findCachedViewById(R.id.tvTotalReview);
        Intrinsics.checkExpressionValueIsNotNull(textView3, "tvTotalReview");
        StringBuilder sb = new StringBuilder();
        sb.append("(");
        String averageRating2 = arrayList.get(0).getAverageRating();
        if (averageRating2 == null) {
            Intrinsics.throwNpe();
        }
        sb.append(averageRating2);
        sb.append(")");
        textView3.setText(sb.toString());
        TextView textView4 = (TextView) this.this$0._$_findCachedViewById(R.id.tvTags);
        Intrinsics.checkExpressionValueIsNotNull(textView4, "tvTags");
        String description = arrayList.get(0).getDescription();
        Boolean bool = null;
        textView4.setText(String.valueOf(description != null ? StringExtensionsKt.getHtmlString(description) : null));
        if (arrayList.get(0).getIn_stock()) {
            MaterialButton materialButton = (MaterialButton) this.this$0._$_findCachedViewById(R.id.btnOutOfStock);
            Intrinsics.checkExpressionValueIsNotNull(materialButton, "btnOutOfStock");
            ViewExtensionsKt.hide(materialButton);
            MaterialButton materialButton2 = (MaterialButton) this.this$0._$_findCachedViewById(R.id.btnAddCard);
            Intrinsics.checkExpressionValueIsNotNull(materialButton2, "btnAddCard");
            ViewExtensionsKt.show(materialButton2);
        } else {
            MaterialButton materialButton3 = (MaterialButton) this.this$0._$_findCachedViewById(R.id.btnOutOfStock);
            Intrinsics.checkExpressionValueIsNotNull(materialButton3, "btnOutOfStock");
            ViewExtensionsKt.show(materialButton3);
            MaterialButton materialButton4 = (MaterialButton) this.this$0._$_findCachedViewById(R.id.btnAddCard);
            Intrinsics.checkExpressionValueIsNotNull(materialButton4, "btnAddCard");
            ViewExtensionsKt.hide(materialButton4);
        }
        if (arrayList.get(0).getAttributes() != null) {
            List<Attributes> attributes = arrayList.get(0).getAttributes();
            if (attributes == null) {
                Intrinsics.throwNpe();
            }
            for (Attributes next : attributes) {
                Object systemService = this.this$0.getApplicationContext().getSystemService("layout_inflater");
                if (systemService != null) {
                    View inflate = ((LayoutInflater) systemService).inflate(com.store.proshop.R.layout.view_attributes, (ViewGroup) null);
                    Intrinsics.checkExpressionValueIsNotNull(inflate, "vi.inflate(R.layout.view_attributes, null)");
                    View findViewById = inflate.findViewById(com.store.proshop.R.id.txtAttName);
                    if (findViewById != null) {
                        TextView textView5 = (TextView) findViewById;
                        ExtensionsKt.changeTextSecondaryColor(textView5);
                        textView5.setText(String.valueOf(next.getName()) + " : ");
                        final ArrayList arrayList2 = new ArrayList();
                        List<String> options = next.getOptions();
                        if (options != null) {
                            int i = 0;
                            for (Object next2 : options) {
                                int i2 = i + 1;
                                if (i < 0) {
                                    CollectionsKt.throwIndexOverflow();
                                }
                                String str = (String) next2;
                                if (str != null) {
                                    arrayList2.add(StringsKt.trim((CharSequence) str).toString());
                                    i = i2;
                                } else {
                                    throw new TypeCastException("null cannot be cast to non-null type kotlin.CharSequence");
                                }
                            }
                            Unit unit = Unit.INSTANCE;
                        }
                        this.this$0.mAttributeAdapter = new BaseAdapter(com.store.proshop.R.layout.item_attributes, new Function3<View, String, Integer, Unit>(this) {
                            final /* synthetic */ ProductDetailActivity2$getProductDetail$1 this$0;

                            {
                                this.this$0 = r1;
                            }

                            public /* bridge */ /* synthetic */ Object invoke(Object obj, Object obj2, Object obj3) {
                                invoke((View) obj, (String) obj2, ((Number) obj3).intValue());
                                return Unit.INSTANCE;
                            }

                            public final void invoke(View view, String str, int i) {
                                Intrinsics.checkParameterIsNotNull(view, "vv");
                                Intrinsics.checkParameterIsNotNull(str, "item");
                                CharSequence charSequence = str;
                                if (charSequence.length() > 0) {
                                    View findViewById = view.findViewById(com.store.proshop.R.id.tvSize);
                                    if (findViewById != null) {
                                        TextView textView = (TextView) findViewById;
                                        textView.setTypeface(AppExtensionsKt.fontRegular(this.this$0.this$0));
                                        ExtensionsKt.changeTextSecondaryColor(textView);
                                        if (arrayList2.size() - 1 == i) {
                                            textView.setText(charSequence);
                                            return;
                                        }
                                        textView.setText(str + " ,");
                                        return;
                                    }
                                    throw new TypeCastException("null cannot be cast to non-null type android.widget.TextView");
                                }
                            }
                        });
                        BaseAdapter access$getMAttributeAdapter$p = this.this$0.mAttributeAdapter;
                        if (access$getMAttributeAdapter$p != null) {
                            access$getMAttributeAdapter$p.clearItems();
                            Unit unit2 = Unit.INSTANCE;
                        }
                        BaseAdapter access$getMAttributeAdapter$p2 = this.this$0.mAttributeAdapter;
                        if (access$getMAttributeAdapter$p2 != null) {
                            access$getMAttributeAdapter$p2.addItems(arrayList2);
                            Unit unit3 = Unit.INSTANCE;
                        }
                        View findViewById2 = inflate.findViewById(com.store.proshop.R.id.rvAttributeView);
                        if (findViewById2 != null) {
                            RecyclerView recyclerView = (RecyclerView) findViewById2;
                            ExtensionsKt.setHorizontalLayout$default(recyclerView, false, 1, (Object) null);
                            recyclerView.setAdapter(this.this$0.mAttributeAdapter);
                            ((LinearLayout) this.this$0._$_findCachedViewById(R.id.llAttributeView)).addView(inflate, 0, new ViewGroup.LayoutParams(-1, -1));
                        } else {
                            throw new TypeCastException("null cannot be cast to non-null type androidx.recyclerview.widget.RecyclerView");
                        }
                    } else {
                        throw new TypeCastException("null cannot be cast to non-null type android.widget.TextView");
                    }
                } else {
                    throw new TypeCastException("null cannot be cast to non-null type android.view.LayoutInflater");
                }
            }
        }
        if (Intrinsics.areEqual((Object) arrayList.get(0).getType(), (Object) "simple")) {
            List<Attributes> attributes2 = arrayList.get(0).getAttributes();
            if (attributes2 == null) {
                Intrinsics.throwNpe();
            }
            if (!attributes2.isEmpty()) {
                TextView textView6 = (TextView) this.this$0._$_findCachedViewById(R.id.tvAvailability);
                Intrinsics.checkExpressionValueIsNotNull(textView6, "tvAvailability");
                List<Attributes> attributes3 = arrayList.get(0).getAttributes();
                if (attributes3 == null) {
                    Intrinsics.throwNpe();
                }
                textView6.setText(String.valueOf(attributes3.get(0).getName()));
            }
            LinearLayout linearLayout = (LinearLayout) this.this$0._$_findCachedViewById(R.id.llAttribute);
            Intrinsics.checkExpressionValueIsNotNull(linearLayout, "llAttribute");
            ViewExtensionsKt.hide(linearLayout);
            ProductDetailActivity2 productDetailActivity22 = this.this$0;
            StoreProductModel storeProductModel2 = arrayList.get(0);
            Intrinsics.checkExpressionValueIsNotNull(storeProductModel2, "it[0]");
            productDetailActivity22.setPriceDetail(storeProductModel2);
        } else if (Intrinsics.areEqual((Object) arrayList.get(0).getType(), (Object) "variable")) {
            LinearLayout linearLayout2 = (LinearLayout) this.this$0._$_findCachedViewById(R.id.llAttribute);
            Intrinsics.checkExpressionValueIsNotNull(linearLayout2, "llAttribute");
            ViewExtensionsKt.show(linearLayout2);
            if (arrayList.get(0).getAttributes() != null) {
                List<Attributes> attributes4 = arrayList.get(0).getAttributes();
                if (attributes4 != null) {
                    bool = Boolean.valueOf(!attributes4.isEmpty());
                }
                if (bool == null) {
                    Intrinsics.throwNpe();
                }
                if (bool.booleanValue()) {
                    ArrayList arrayList3 = new ArrayList();
                    final ArrayList arrayList4 = new ArrayList();
                    List<Integer> variations = arrayList.get(0).getVariations();
                    if (variations == null) {
                        Intrinsics.throwNpe();
                    }
                    int i3 = 0;
                    for (Object next3 : arrayList) {
                        int i4 = i3 + 1;
                        if (i3 < 0) {
                            CollectionsKt.throwIndexOverflow();
                        }
                        StoreProductModel storeProductModel3 = (StoreProductModel) next3;
                        if (i3 > 0) {
                            List<Attributes> attributes5 = arrayList.get(i3).getAttributes();
                            if (attributes5 == null) {
                                Intrinsics.throwNpe();
                            }
                            String str2 = "";
                            for (Attributes attributes6 : attributes5) {
                                if (!StringsKt.isBlank(str2)) {
                                    str2 = str2 + " - " + String.valueOf(attributes6.getOptionsString());
                                } else {
                                    str2 = String.valueOf(attributes6.getOptionsString());
                                }
                            }
                            if (storeProductModel3.getOnSale()) {
                                str2 = str2 + " [Sale]";
                            }
                            arrayList3.add(str2);
                        }
                        i3 = i4;
                    }
                    int i5 = 0;
                    for (Object next4 : variations) {
                        int i6 = i5 + 1;
                        if (i5 < 0) {
                            CollectionsKt.throwIndexOverflow();
                        }
                        arrayList4.add(Integer.valueOf(((Number) next4).intValue()));
                        i5 = i6;
                    }
                    this.this$0.mYearAdapter = new SpinnerAdapter(this.this$0, arrayList3);
                    Spinner spinner = (Spinner) this.this$0._$_findCachedViewById(R.id.spAttribute);
                    Intrinsics.checkExpressionValueIsNotNull(spinner, "spAttribute");
                    spinner.setAdapter(this.this$0.mYearAdapter);
                    Spinner spinner2 = (Spinner) this.this$0._$_findCachedViewById(R.id.spAttribute);
                    Intrinsics.checkExpressionValueIsNotNull(spinner2, "spAttribute");
                    spinner2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener(this) {
                        final /* synthetic */ ProductDetailActivity2$getProductDetail$1 this$0;

                        public void onNothingSelected(AdapterView<?> adapterView) {
                            Intrinsics.checkParameterIsNotNull(adapterView, "parent");
                        }

                        {
                            this.this$0 = r1;
                        }

                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long j) {
                            Intrinsics.checkParameterIsNotNull(adapterView, "parent");
                            Intrinsics.checkParameterIsNotNull(view, "view");
                            for (StoreProductModel storeProductModel : arrayList) {
                                Integer num = (Integer) arrayList4.get(i);
                                int id = storeProductModel.getId();
                                if (num != null && num.intValue() == id) {
                                    this.this$0.this$0.setPriceDetail(storeProductModel);
                                    TextView textView = (TextView) this.this$0.this$0._$_findCachedViewById(R.id.tvAvailability);
                                    Intrinsics.checkExpressionValueIsNotNull(textView, "tvAvailability");
                                    List<Attributes> attributes = storeProductModel.getAttributes();
                                    if (attributes == null) {
                                        Intrinsics.throwNpe();
                                    }
                                    textView.setText(String.valueOf(attributes.get(0).getName()));
                                    ArrayAdapter access$getMYearAdapter$p = this.this$0.this$0.mYearAdapter;
                                    if (access$getMYearAdapter$p == null) {
                                        Intrinsics.throwNpe();
                                    }
                                    access$getMYearAdapter$p.notifyDataSetChanged();
                                }
                            }
                        }
                    });
                }
            }
            LinearLayout linearLayout3 = (LinearLayout) this.this$0._$_findCachedViewById(R.id.llAttribute);
            Intrinsics.checkExpressionValueIsNotNull(linearLayout3, "llAttribute");
            ViewExtensionsKt.hide(linearLayout3);
        } else if (Intrinsics.areEqual((Object) arrayList.get(0).getType(), (Object) "grouped")) {
            LinearLayout linearLayout4 = (LinearLayout) this.this$0._$_findCachedViewById(R.id.llAttribute);
            Intrinsics.checkExpressionValueIsNotNull(linearLayout4, "llAttribute");
            ViewExtensionsKt.hide(linearLayout4);
            LinearLayout linearLayout5 = (LinearLayout) this.this$0._$_findCachedViewById(R.id.upcomingSale);
            Intrinsics.checkExpressionValueIsNotNull(linearLayout5, "upcomingSale");
            ViewExtensionsKt.hide(linearLayout5);
            LinearLayout linearLayout6 = (LinearLayout) this.this$0._$_findCachedViewById(R.id.groupItems);
            Intrinsics.checkExpressionValueIsNotNull(linearLayout6, "groupItems");
            ViewExtensionsKt.show(linearLayout6);
            RecyclerView recyclerView2 = (RecyclerView) this.this$0._$_findCachedViewById(R.id.extraProduct);
            Intrinsics.checkExpressionValueIsNotNull(recyclerView2, "extraProduct");
            ExtensionsKt.setVerticalLayout$default(recyclerView2, false, 1, (Object) null);
            RecyclerView recyclerView3 = (RecyclerView) this.this$0._$_findCachedViewById(R.id.extraProduct);
            Intrinsics.checkExpressionValueIsNotNull(recyclerView3, "extraProduct");
            recyclerView3.setAdapter(this.this$0.mGroupCartAdapter);
            this.this$0.mGroupCartAdapter.clearItems();
            int i7 = 0;
            for (Object next5 : arrayList) {
                int i8 = i7 + 1;
                if (i7 < 0) {
                    CollectionsKt.throwIndexOverflow();
                }
                StoreProductModel storeProductModel4 = (StoreProductModel) next5;
                if (i7 > 0) {
                    this.this$0.mGroupCartAdapter.addItem(storeProductModel4);
                }
                i7 = i8;
            }
            this.this$0.mGroupCartAdapter.notifyDataSetChanged();
        } else if (Intrinsics.areEqual((Object) arrayList.get(0).getType(), (Object) "external")) {
            LinearLayout linearLayout7 = (LinearLayout) this.this$0._$_findCachedViewById(R.id.llAttribute);
            Intrinsics.checkExpressionValueIsNotNull(linearLayout7, "llAttribute");
            ViewExtensionsKt.hide(linearLayout7);
            ProductDetailActivity2 productDetailActivity23 = this.this$0;
            StoreProductModel storeProductModel5 = arrayList.get(0);
            Intrinsics.checkExpressionValueIsNotNull(storeProductModel5, "it[0]");
            productDetailActivity23.setPriceDetail(storeProductModel5);
            this.this$0.mIsExternalProduct = true;
            MaterialButton materialButton5 = (MaterialButton) this.this$0._$_findCachedViewById(R.id.btnAddCard);
            Intrinsics.checkExpressionValueIsNotNull(materialButton5, "btnAddCard");
            ViewExtensionsKt.show(materialButton5);
            MaterialButton materialButton6 = (MaterialButton) this.this$0._$_findCachedViewById(R.id.btnAddCard);
            Intrinsics.checkExpressionValueIsNotNull(materialButton6, "btnAddCard");
            materialButton6.setText(arrayList.get(0).getButtonText());
            this.this$0.mExternalURL = String.valueOf(arrayList.get(0).getExternalUrl());
        } else {
            ExtensionsKt.toast$default((Activity) this.this$0, (int) com.store.proshop.R.string.invalid_product, 0, 2, (Object) null);
            this.this$0.finish();
        }
        if (arrayList.get(0).getPurchasable()) {
            LinearLayout linearLayout8 = (LinearLayout) this.this$0._$_findCachedViewById(R.id.banner_container);
            Intrinsics.checkExpressionValueIsNotNull(linearLayout8, "banner_container");
            ViewExtensionsKt.show(linearLayout8);
        } else if (this.this$0.mIsExternalProduct) {
            LinearLayout linearLayout9 = (LinearLayout) this.this$0._$_findCachedViewById(R.id.banner_container);
            Intrinsics.checkExpressionValueIsNotNull(linearLayout9, "banner_container");
            ViewExtensionsKt.show(linearLayout9);
        } else {
            LinearLayout linearLayout10 = (LinearLayout) this.this$0._$_findCachedViewById(R.id.banner_container);
            Intrinsics.checkExpressionValueIsNotNull(linearLayout10, "banner_container");
            ViewExtensionsKt.hide(linearLayout10);
        }
        if (Intrinsics.areEqual((Object) arrayList.get(0).getReviewsAllowed(), (Object) true)) {
            TextView textView7 = (TextView) this.this$0._$_findCachedViewById(R.id.tvAllReviews);
            Intrinsics.checkExpressionValueIsNotNull(textView7, "tvAllReviews");
            ViewExtensionsKt.show(textView7);
            LinearLayout linearLayout11 = (LinearLayout) this.this$0._$_findCachedViewById(R.id.llReviews);
            Intrinsics.checkExpressionValueIsNotNull(linearLayout11, "llReviews");
            ViewExtensionsKt.show(linearLayout11);
            TextView textView8 = (TextView) this.this$0._$_findCachedViewById(R.id.tvAllReviews);
            textView8.setOnClickListener(new ProductDetailActivity2$getProductDetail$1$$special$$inlined$onClick$1(textView8, this));
        } else {
            LinearLayout linearLayout12 = (LinearLayout) this.this$0._$_findCachedViewById(R.id.llReviews);
            Intrinsics.checkExpressionValueIsNotNull(linearLayout12, "llReviews");
            ViewExtensionsKt.hide(linearLayout12);
            TextView textView9 = (TextView) this.this$0._$_findCachedViewById(R.id.tvAllReviews);
            Intrinsics.checkExpressionValueIsNotNull(textView9, "tvAllReviews");
            ViewExtensionsKt.hide(textView9);
        }
        Collection upsellIds = arrayList.get(0).getUpsellIds();
        if (upsellIds == null || upsellIds.isEmpty()) {
            TextView textView10 = (TextView) this.this$0._$_findCachedViewById(R.id.lbl_like);
            Intrinsics.checkExpressionValueIsNotNull(textView10, "lbl_like");
            ViewExtensionsKt.hide(textView10);
            RecyclerView recyclerView4 = (RecyclerView) this.this$0._$_findCachedViewById(R.id.rvLike);
            Intrinsics.checkExpressionValueIsNotNull(recyclerView4, "rvLike");
            ViewExtensionsKt.hide(recyclerView4);
        } else {
            TextView textView11 = (TextView) this.this$0._$_findCachedViewById(R.id.lbl_like);
            Intrinsics.checkExpressionValueIsNotNull(textView11, "lbl_like");
            ViewExtensionsKt.show(textView11);
            RecyclerView recyclerView5 = (RecyclerView) this.this$0._$_findCachedViewById(R.id.rvLike);
            Intrinsics.checkExpressionValueIsNotNull(recyclerView5, "rvLike");
            ViewExtensionsKt.show(recyclerView5);
            BaseAdapter access$getMProductAdapter$p = this.this$0.mProductAdapter;
            List<StoreUpSale> upsell_id = arrayList.get(0).getUpsell_id();
            if (upsell_id == null) {
                Intrinsics.throwNpe();
            }
            access$getMProductAdapter$p.addItems(upsell_id);
        }
        Collection categories = arrayList.get(0).getCategories();
        if (categories == null || categories.isEmpty()) {
            TextView textView12 = (TextView) this.this$0._$_findCachedViewById(R.id.lblCategory);
            Intrinsics.checkExpressionValueIsNotNull(textView12, "lblCategory");
            ViewExtensionsKt.hide(textView12);
            RecyclerView recyclerView6 = (RecyclerView) this.this$0._$_findCachedViewById(R.id.rvCategory);
            Intrinsics.checkExpressionValueIsNotNull(recyclerView6, "rvCategory");
            ViewExtensionsKt.hide(recyclerView6);
        } else {
            TextView textView13 = (TextView) this.this$0._$_findCachedViewById(R.id.lblCategory);
            Intrinsics.checkExpressionValueIsNotNull(textView13, "lblCategory");
            ViewExtensionsKt.show(textView13);
            RecyclerView recyclerView7 = (RecyclerView) this.this$0._$_findCachedViewById(R.id.rvCategory);
            Intrinsics.checkExpressionValueIsNotNull(recyclerView7, "rvCategory");
            ViewExtensionsKt.show(recyclerView7);
            this.this$0.mCategoryAdapter.clearItems();
            BaseAdapter access$getMCategoryAdapter$p = this.this$0.mCategoryAdapter;
            List<StoreCategory> categories2 = arrayList.get(0).getCategories();
            if (categories2 == null) {
                Intrinsics.throwNpe();
            }
            access$getMCategoryAdapter$p.addItems(categories2);
        }
        if (arrayList.get(0).is_added_cart()) {
            if (this.this$0.mIsExternalProduct) {
                MaterialButton materialButton7 = (MaterialButton) this.this$0._$_findCachedViewById(R.id.btnAddCard);
                Intrinsics.checkExpressionValueIsNotNull(materialButton7, "btnAddCard");
                materialButton7.setText(arrayList.get(0).getButtonText());
            } else {
                this.this$0.isAddedToCart = true;
                MaterialButton materialButton8 = (MaterialButton) this.this$0._$_findCachedViewById(R.id.btnAddCard);
                Intrinsics.checkExpressionValueIsNotNull(materialButton8, "btnAddCard");
                materialButton8.setText(this.this$0.getString(com.store.proshop.R.string.lbl_remove_cart));
            }
        } else if (this.this$0.mIsExternalProduct) {
            MaterialButton materialButton9 = (MaterialButton) this.this$0._$_findCachedViewById(R.id.btnAddCard);
            Intrinsics.checkExpressionValueIsNotNull(materialButton9, "btnAddCard");
            materialButton9.setText(arrayList.get(0).getButtonText());
        } else {
            this.this$0.isAddedToCart = false;
            MaterialButton materialButton10 = (MaterialButton) this.this$0._$_findCachedViewById(R.id.btnAddCard);
            Intrinsics.checkExpressionValueIsNotNull(materialButton10, "btnAddCard");
            materialButton10.setText(this.this$0.getString(com.store.proshop.R.string.lbl_add_to_cart));
        }
        if (arrayList.get(0).is_added_wishlist()) {
            this.this$0.mIsInWishList = true;
            this.this$0.changeFavIcon(com.store.proshop.R.drawable.ic_heart_fill, AppExtensionsKt.getAccentColor());
            return;
        }
        this.this$0.mIsInWishList = false;
        this.this$0.changeFavIcon(com.store.proshop.R.drawable.ic_heart, AppExtensionsKt.getAccentColor());
    }
}
