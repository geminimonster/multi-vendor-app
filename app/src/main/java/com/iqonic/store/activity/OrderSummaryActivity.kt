package com.iqonic.store.activity

import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.RelativeLayout
import android.widget.Toast
import com.iqonic.store.AppBaseActivity
import com.iqonic.store.R
import com.iqonic.store.stripe.StripePaymentActivity
import com.iqonic.store.utils.Constants
import com.iqonic.store.utils.Constants.PAYMENT_METHOD.PAYMENT_METHOD_WEB
import com.razorpay.Checkout
import com.razorpay.PaymentResultListener
import kotlinx.android.synthetic.main.dialog_failed_transaction.*
import org.json.JSONObject
import java.text.DecimalFormat

class OrderSummaryActivity : AppBaseActivity(), PaymentResultListener {

    private val TAG: String = OrderSummaryActivity::class.toString()
    var isNativePayment = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_order_summary)
        setToolbar(toolbar)
        title = getString(R.string.order_summary)

        /**
         * Check payment method type
         */
        isNativePayment = getSharedPrefInstance().getStringValue(
            Constants.SharedPref.PAYMENT_METHOD,
            PAYMENT_METHOD_WEB
        ) != PAYMENT_METHOD_WEB

        val price = intent.getStringExtra(Constants.KeyIntent.PRICE)
        val precision = DecimalFormat("0.00")
        tvTotalCartAmount.text =
            precision.format(price.toDouble()).toString().currencyFormat()

        updateAddress()
        tvContinue.onClick {
            if (isNativePayment) {
                when {
                    rbCard.isChecked -> {
                        launchActivity<StripePaymentActivity>(Constants.RequestCode.STRIPE_PAYMENT) {
                            putExtra(
                                Constants.KeyIntent.COUPON_CODE,
                                intent.getStringExtra(Constants.KeyIntent.COUPON_CODE)
                            )
                            putExtra(
                                Constants.KeyIntent.PRICE,
                                intent.getStringExtra(Constants.KeyIntent.PRICE)
                            )
                            putExtra(
                                Constants.KeyIntent.PRODUCTDATA,
                                intent.getSerializableExtra(Constants.KeyIntent.PRODUCTDATA)
                            )
                        }
                    }
                    rbRazorpay.isChecked -> {
                        showProgress(true)
                        startPayment()
                    }
                    rbCOD.isChecked -> {
                        showProgress(true)
                        val orderRequest = OrderRequest()
                        orderRequest.payment_method = "cod"
                        orderRequest.transaction_id = ""
                        orderRequest.customer_id = getUserId().toInt()
                        orderRequest.currency = getDefaultCurrencyFormate()
                        orderRequest.status = "pending"
                        orderRequest.set_paid = false
                        createOrderRequest(orderRequest)
                    }
                }
            } else {
                showProgress(true)
                val orderRequest = OrderRequest()
                orderRequest.payment_method = "cod"
                orderRequest.transaction_id = ""
                orderRequest.customer_id = getUserId().toInt()
                orderRequest.currency = getDefaultCurrencyFormate()
                orderRequest.status = "pending"
                orderRequest.set_paid = false
                createOrderRequest(orderRequest)
            }
        }
        tvChange.onClick {
            launchActivity<AddressManagerActivity>(Constants.RequestCode.ADD_ADDRESS)
        }

        if (isNativePayment) {
            paymentView.visibility = View.VISIBLE
        } else {
            paymentView.visibility = View.GONE
        }
    }

    @SuppressLint("SetTextI18n")
    private fun updateAddress() {

        tvUsername.text = getShippingList().first_name + getShippingList().last_name
        tvUserAddress.text =
            getShippingList().address_1 + "\n" + getShippingList().city + "\n" + getShippingList().country + "\n" + getShippingList().postcode + "\n" + getShippingList().state + "\n"

        tvBillingUsername.text = getbillingList().first_name + getbillingList().last_name
        tvBillingAddress.text =
            getbillingList().address_1 + "\n" + getbillingList().city + "\n" + getbillingList().country + "\n" + getbillingList().postcode + "\n" + getbillingList().state + "\n"
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) {
                Constants.RequestCode.ADD_ADDRESS -> {
                    updateAddress()
                }
                Constants.RequestCode.STRIPE_PAYMENT -> {
                    setResult(Activity.RESULT_OK)
                    finish()
                }
                Constants.RequestCode.WEB_PAYMENT -> {
                    setResult(Activity.RESULT_OK)
                    finish()
                }
            }
        }
    }

    /**
     * Razorpay Payment
     *
     */
    private fun startPayment() {
        val activity: Activity = this
        val co = Checkout()
        co.setKeyID(getString(R.string.razor_key))
        try {
            val options = JSONObject()
            options.put("name", getString(R.string.app_name))
            options.put("description", getString(R.string.app_description))
            options.put(
                "image",
                "http://iqonic.design/wp-themes/proshop/wp-content/uploads/2020/06/logo1.png"
            )
            options.put("currency", getDefaultCurrencyFormate())
            val orignalPrice = intent.getStringExtra(Constants.KeyIntent.PRICE)
            val finalPrice = orignalPrice.toFloat() * 100.00
            options.put("amount", finalPrice.toInt().toString())

            val prefill = JSONObject()
            prefill.put("email", getEmail())
            prefill.put("contact", getbillingList().phone)

            options.put("prefill", prefill)
            co.open(activity, options)
        } catch (e: Exception) {
            showProgress(false)
            Toast.makeText(
                activity,
                getString(R.string.lbl_error_in_payment) + e.message,
                Toast.LENGTH_LONG
            ).show()
            e.printStackTrace()
        }
    }

    override fun onPaymentError(errorCode: Int, response: String?) {
        try {
            Log.e("onPaymentError", "errorCode:$errorCode \nresponse:$response")
            showPaymentFailDialog()
            showProgress(false)
        } catch (e: Exception) {
            Log.e(TAG, "Exception in onPaymentSuccess", e)
        }
    }

    private fun createOrderRequest(orderRequest: OrderRequest) {
        val billing = Billing()
        billing.address_1 = getbillingList().address_1
        billing.address_2 = getbillingList().address_2
        billing.city = getbillingList().city
        billing.country = getbillingList().country
        billing.state = getbillingList().state
        billing.phone = getbillingList().phone
        billing.first_name = getbillingList().first_name
        billing.last_name = getbillingList().last_name
        billing.email = getEmail()

        val shipping = Shipping()
        shipping.address_1 = getShippingList().address_1
        shipping.address_2 = getShippingList().address_2
        shipping.city = getShippingList().city
        shipping.country = getShippingList().country
        shipping.state = getShippingList().state
        shipping.phone = getShippingList().phone
        shipping.first_name = getShippingList().first_name
        shipping.last_name = getShippingList().last_name

        orderRequest.billing = billing
        orderRequest.shipping = shipping

        val couponCode = intent.getStringExtra(Constants.KeyIntent.COUPON_CODE)
        val orderItems: ArrayList<Line_items> =
            intent.getSerializableExtra(Constants.KeyIntent.PRODUCTDATA) as ArrayList<Line_items>
        orderRequest.line_items = orderItems

        if (couponCode.isNotEmpty()) {
            val couponLines: ArrayList<Coupon_lines> = ArrayList(1)
            val cop = Coupon_lines()
            cop.code = couponCode
            couponLines.add(cop)
            orderRequest.coupon_lines = couponLines
        }

        getRestApiImpl().createOrderRequest(request = orderRequest, onApiSuccess = {
            if (isNativePayment) {
                showProgress(false)
                launchActivity<PaymentSuccessfullyActivity>(Constants.RequestCode.ROZORPAY_PAYMENT) {
                    putExtra(Constants.KeyIntent.ORDER_DATA, it)
                }
                getSharedPrefInstance().removeKey(Constants.SharedPref.KEY_USER_CART)
                getSharedPrefInstance().removeKey(Constants.SharedPref.KEY_CART_COUNT)
                setResult(Activity.RESULT_OK)
                finish()
            } else {
                getCheckoutUrl(it.id)
            }
        }, onApiError = {
            showProgress(false)
        })
    }

    private fun getCheckoutUrl(id: Int) {
        val request = CheckoutUrlRequest()
        request.order_id = id.toString()
        getRestApiImpl().getCheckoutUrl(request = request, onApiSuccess = {
            launchActivity<WebViewActivity>(Constants.RequestCode.WEB_PAYMENT) {
                putExtra(Constants.KeyIntent.CHECKOUT_URL, it.checkout_url)
            }
        }, onApiError = {
            showProgress(false)
        })
    }

    override fun onPaymentSuccess(razorpayPaymentId: String?) {
        Log.e(TAG, " onPaymentSuccess")
        try {
            /**
             * Create Order request for successfully payment
             *
             */
            val orderRequest = OrderRequest()
            orderRequest.payment_method = "razorpay"
            orderRequest.transaction_id = razorpayPaymentId.toString()
            orderRequest.customer_id = getUserId().toInt()
            orderRequest.currency = getDefaultCurrencyFormate()
            orderRequest.status = "pending"
            orderRequest.set_paid = true

            createOrderRequest(orderRequest)
        } catch (e: Exception) {
            showProgress(false)
            Log.e(TAG, "Exception in onPaymentSuccess", e)
        }
    }

    private fun showPaymentFailDialog() {
        val changePasswordDialog = Dialog(this)
        changePasswordDialog.setCancelable(false)
        changePasswordDialog.window?.setBackgroundDrawable(ColorDrawable(0))
        changePasswordDialog.setContentView(R.layout.dialog_failed_transaction)
        changePasswordDialog.window?.setLayout(
            RelativeLayout.LayoutParams.MATCH_PARENT,
            RelativeLayout.LayoutParams.WRAP_CONTENT
        )
        changePasswordDialog.tv_close.onClick {
            changePasswordDialog.dismiss()
            finish()
        }
        changePasswordDialog.show()
    }


}