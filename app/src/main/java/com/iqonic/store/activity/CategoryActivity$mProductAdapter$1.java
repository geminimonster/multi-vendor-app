package com.iqonic.store.activity;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.iqonic.store.R;
import com.iqonic.store.models.Category;
import com.iqonic.store.utils.extensions.AppExtensionsKt;
import com.iqonic.store.utils.extensions.ExtensionsKt;
import com.iqonic.store.utils.extensions.NetworkExtensionKt;
import com.iqonic.store.utils.extensions.StringExtensionsKt;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function3;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0007H\n¢\u0006\u0002\b\b"}, d2 = {"<anonymous>", "", "view", "Landroid/view/View;", "model", "Lcom/iqonic/store/models/Category;", "<anonymous parameter 2>", "", "invoke"}, k = 3, mv = {1, 1, 16})
/* compiled from: CategoryActivity.kt */
final class CategoryActivity$mProductAdapter$1 extends Lambda implements Function3<View, Category, Integer, Unit> {
    final /* synthetic */ CategoryActivity this$0;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    CategoryActivity$mProductAdapter$1(CategoryActivity categoryActivity) {
        super(3);
        this.this$0 = categoryActivity;
    }

    public /* bridge */ /* synthetic */ Object invoke(Object obj, Object obj2, Object obj3) {
        invoke((View) obj, (Category) obj2, ((Number) obj3).intValue());
        return Unit.INSTANCE;
    }

    public final void invoke(View view, Category category, int i) {
        Intrinsics.checkParameterIsNotNull(view, "view");
        Intrinsics.checkParameterIsNotNull(category, "model");
        if (category.getImage() != null) {
            ImageView imageView = (ImageView) view.findViewById(R.id.ivProduct);
            Intrinsics.checkExpressionValueIsNotNull(imageView, "view.ivProduct");
            NetworkExtensionKt.loadImageFromUrl$default(imageView, category.getImage().getSrc(), 0, 0, 6, (Object) null);
            ImageView imageView2 = (ImageView) view.findViewById(R.id.ivProduct);
            Intrinsics.checkExpressionValueIsNotNull(imageView2, "view.ivProduct");
            imageView2.setVisibility(0);
        } else {
            ImageView imageView3 = (ImageView) view.findViewById(R.id.ivProduct);
            Intrinsics.checkExpressionValueIsNotNull(imageView3, "view.ivProduct");
            AppExtensionsKt.loadImageFromDrawable(imageView3, com.store.proshop.R.drawable.app_logo);
        }
        TextView textView = (TextView) view.findViewById(R.id.tvProductName);
        Intrinsics.checkExpressionValueIsNotNull(textView, "view.tvProductName");
        textView.setText(StringExtensionsKt.getHtmlString(category.getName()));
        TextView textView2 = (TextView) view.findViewById(R.id.tvProductName);
        Intrinsics.checkExpressionValueIsNotNull(textView2, "view.tvProductName");
        ExtensionsKt.changeTextPrimaryColor(textView2);
        view.setOnClickListener(new CategoryActivity$mProductAdapter$1$$special$$inlined$onClick$1(view, this, category));
    }
}
