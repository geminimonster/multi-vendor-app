package com.iqonic.store.activity;

import android.view.View;
import com.iqonic.store.models.Coupons;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function3;
import kotlin.jvm.internal.Lambda;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0007H\n¢\u0006\u0002\b\b"}, d2 = {"<anonymous>", "", "view", "Landroid/view/View;", "item", "Lcom/iqonic/store/models/Coupons;", "<anonymous parameter 2>", "", "invoke"}, k = 3, mv = {1, 1, 16})
/* compiled from: CouponActivity.kt */
final class CouponActivity$onCreate$1 extends Lambda implements Function3<View, Coupons, Integer, Unit> {
    final /* synthetic */ CouponActivity this$0;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    CouponActivity$onCreate$1(CouponActivity couponActivity) {
        super(3);
        this.this$0 = couponActivity;
    }

    public /* bridge */ /* synthetic */ Object invoke(Object obj, Object obj2, Object obj3) {
        invoke((View) obj, (Coupons) obj2, ((Number) obj3).intValue());
        return Unit.INSTANCE;
    }

    /* JADX WARNING: Removed duplicated region for block: B:18:0x0170  */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x0479  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void invoke(android.view.View r20, com.iqonic.store.models.Coupons r21, int r22) {
        /*
            r19 = this;
            r0 = r19
            r1 = r20
            r2 = r21
            java.lang.String r3 = "view"
            kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(r1, r3)
            java.lang.String r3 = "item"
            kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(r2, r3)
            java.lang.String r3 = r21.getDiscount_type()
            int r4 = r3.hashCode()
            r5 = -1055414492(0xffffffffc117a724, float:-9.478306)
            r6 = 2131820890(0x7f11015a, float:1.9274508E38)
            r7 = 2131820889(0x7f110159, float:1.9274506E38)
            r8 = 2131820924(0x7f11017c, float:1.9274577E38)
            java.lang.String r9 = " "
            r10 = 1
            java.lang.String r11 = "view.tvCouponName"
            r12 = 0
            if (r4 == r5) goto L_0x00bc
            r5 = -678927291(0xffffffffd7886445, float:-2.99928471E14)
            if (r4 == r5) goto L_0x007d
            r5 = 1707398411(0x65c4d50b, float:1.1618922E23)
            if (r4 == r5) goto L_0x003a
            goto L_0x010d
        L_0x003a:
            java.lang.String r4 = "fixed_cart"
            boolean r3 = r3.equals(r4)
            if (r3 == 0) goto L_0x010d
            int r3 = com.iqonic.store.R.id.tvCouponName
            android.view.View r3 = r1.findViewById(r3)
            android.widget.TextView r3 = (android.widget.TextView) r3
            kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r3, r11)
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            com.iqonic.store.activity.CouponActivity r5 = r0.this$0
            java.lang.String r5 = r5.getString(r6)
            r4.append(r5)
            java.lang.String r5 = r21.getAmount()
            java.lang.String r5 = com.iqonic.store.utils.extensions.StringExtensionsKt.currencyFormat$default(r5, r12, r10, r12)
            r4.append(r5)
            r4.append(r9)
            com.iqonic.store.activity.CouponActivity r5 = r0.this$0
            java.lang.String r5 = r5.getString(r8)
            r4.append(r5)
            java.lang.String r4 = r4.toString()
            java.lang.CharSequence r4 = (java.lang.CharSequence) r4
            r3.setText(r4)
            goto L_0x0143
        L_0x007d:
            java.lang.String r4 = "percent"
            boolean r3 = r3.equals(r4)
            if (r3 == 0) goto L_0x010d
            int r3 = com.iqonic.store.R.id.tvCouponName
            android.view.View r3 = r1.findViewById(r3)
            android.widget.TextView r3 = (android.widget.TextView) r3
            kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r3, r11)
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            com.iqonic.store.activity.CouponActivity r5 = r0.this$0
            java.lang.String r5 = r5.getString(r7)
            r4.append(r5)
            java.lang.String r5 = r21.getAmount()
            r4.append(r5)
            com.iqonic.store.activity.CouponActivity r5 = r0.this$0
            r6 = 2131820923(0x7f11017b, float:1.9274575E38)
            java.lang.String r5 = r5.getString(r6)
            r4.append(r5)
            java.lang.String r4 = r4.toString()
            java.lang.CharSequence r4 = (java.lang.CharSequence) r4
            r3.setText(r4)
            goto L_0x0143
        L_0x00bc:
            java.lang.String r4 = "fixed_product"
            boolean r3 = r3.equals(r4)
            if (r3 == 0) goto L_0x010d
            int r3 = com.iqonic.store.R.id.tvCouponName
            android.view.View r3 = r1.findViewById(r3)
            android.widget.TextView r3 = (android.widget.TextView) r3
            kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r3, r11)
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            com.iqonic.store.activity.CouponActivity r5 = r0.this$0
            java.lang.String r5 = r5.getString(r6)
            r4.append(r5)
            java.lang.String r5 = r21.getAmount()
            java.lang.String r5 = com.iqonic.store.utils.extensions.StringExtensionsKt.currencyFormat$default(r5, r12, r10, r12)
            r4.append(r5)
            r4.append(r9)
            com.iqonic.store.activity.CouponActivity r5 = r0.this$0
            java.lang.String r5 = r5.getString(r8)
            r4.append(r5)
            r4.append(r9)
            com.iqonic.store.activity.CouponActivity r5 = r0.this$0
            r6 = 2131821007(0x7f1101cf, float:1.9274745E38)
            java.lang.String r5 = r5.getString(r6)
            r4.append(r5)
            java.lang.String r4 = r4.toString()
            java.lang.CharSequence r4 = (java.lang.CharSequence) r4
            r3.setText(r4)
            goto L_0x0143
        L_0x010d:
            int r3 = com.iqonic.store.R.id.tvCouponName
            android.view.View r3 = r1.findViewById(r3)
            android.widget.TextView r3 = (android.widget.TextView) r3
            kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r3, r11)
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            com.iqonic.store.activity.CouponActivity r5 = r0.this$0
            java.lang.String r5 = r5.getString(r7)
            r4.append(r5)
            java.lang.String r5 = r21.getAmount()
            java.lang.String r5 = com.iqonic.store.utils.extensions.StringExtensionsKt.currencyFormat$default(r5, r12, r10, r12)
            r4.append(r5)
            com.iqonic.store.activity.CouponActivity r5 = r0.this$0
            java.lang.String r5 = r5.getString(r8)
            r4.append(r5)
            java.lang.String r4 = r4.toString()
            java.lang.CharSequence r4 = (java.lang.CharSequence) r4
            r3.setText(r4)
        L_0x0143:
            int r3 = com.iqonic.store.R.id.tvInfo
            android.view.View r3 = r1.findViewById(r3)
            android.widget.TextView r3 = (android.widget.TextView) r3
            java.lang.String r4 = "view.tvInfo"
            kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r3, r4)
            java.lang.String r5 = r21.getDescription()
            java.lang.CharSequence r5 = (java.lang.CharSequence) r5
            r3.setText(r5)
            int r3 = com.iqonic.store.R.id.tvCouponOffer
            android.view.View r3 = r1.findViewById(r3)
            android.widget.TextView r3 = (android.widget.TextView) r3
            java.lang.String r5 = "view.tvCouponOffer"
            kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r3, r5)
            java.lang.String r6 = r21.getCode()
            java.lang.String r7 = "null cannot be cast to non-null type java.lang.String"
            if (r6 == 0) goto L_0x0479
            java.lang.String r6 = r6.toUpperCase()
            java.lang.String r8 = "(this as java.lang.String).toUpperCase()"
            kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r6, r8)
            java.lang.CharSequence r6 = (java.lang.CharSequence) r6
            r3.setText(r6)
            java.lang.String r3 = r21.getMinimum_amount()
            float r3 = java.lang.Float.parseFloat(r3)
            r6 = 0
            float r13 = (float) r6
            java.lang.String r14 = "\nMaximum bill amount is "
            java.lang.String r15 = "view.tvMinAmountInfo"
            int r3 = (r3 > r13 ? 1 : (r3 == r13 ? 0 : -1))
            if (r3 <= 0) goto L_0x021e
            int r3 = com.iqonic.store.R.id.tvMinAmountInfo
            android.view.View r3 = r1.findViewById(r3)
            android.widget.TextView r3 = (android.widget.TextView) r3
            kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r3, r15)
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            com.iqonic.store.activity.CouponActivity r10 = r0.this$0
            r12 = 2131821019(0x7f1101db, float:1.927477E38)
            java.lang.String r10 = r10.getString(r12)
            r6.append(r10)
            java.lang.String r10 = r21.getMinimum_amount()
            r17 = r7
            r7 = 0
            r12 = 1
            java.lang.String r10 = com.iqonic.store.utils.extensions.StringExtensionsKt.currencyFormat$default(r10, r7, r12, r7)
            r6.append(r10)
            com.iqonic.store.activity.CouponActivity r7 = r0.this$0
            r10 = 2131820825(0x7f110119, float:1.9274376E38)
            java.lang.String r7 = r7.getString(r10)
            r6.append(r7)
            java.lang.String r6 = r6.toString()
            java.lang.CharSequence r6 = (java.lang.CharSequence) r6
            r3.setText(r6)
            java.lang.String r3 = r21.getMaximum_amount()
            float r3 = java.lang.Float.parseFloat(r3)
            int r3 = (r3 > r13 ? 1 : (r3 == r13 ? 0 : -1))
            if (r3 <= 0) goto L_0x029f
            int r3 = com.iqonic.store.R.id.tvMinAmountInfo
            android.view.View r3 = r1.findViewById(r3)
            android.widget.TextView r3 = (android.widget.TextView) r3
            kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r3, r15)
            java.lang.CharSequence r3 = r3.getText()
            java.lang.String r3 = r3.toString()
            int r6 = com.iqonic.store.R.id.tvMinAmountInfo
            android.view.View r6 = r1.findViewById(r6)
            android.widget.TextView r6 = (android.widget.TextView) r6
            kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r6, r15)
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            r7.<init>()
            r7.append(r3)
            r7.append(r14)
            java.lang.String r3 = r21.getMaximum_amount()
            r10 = 1
            r12 = 0
            java.lang.String r3 = com.iqonic.store.utils.extensions.StringExtensionsKt.currencyFormat$default(r3, r12, r10, r12)
            r7.append(r3)
            java.lang.String r3 = r7.toString()
            java.lang.CharSequence r3 = (java.lang.CharSequence) r3
            r6.setText(r3)
            goto L_0x029f
        L_0x021e:
            r17 = r7
            java.lang.String r3 = r21.getMaximum_amount()
            float r3 = java.lang.Float.parseFloat(r3)
            int r3 = (r3 > r13 ? 1 : (r3 == r13 ? 0 : -1))
            if (r3 <= 0) goto L_0x026c
            int r3 = com.iqonic.store.R.id.tvMinAmountInfo
            android.view.View r3 = r1.findViewById(r3)
            android.widget.TextView r3 = (android.widget.TextView) r3
            kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r3, r15)
            java.lang.CharSequence r3 = r3.getText()
            java.lang.String r3 = r3.toString()
            int r6 = com.iqonic.store.R.id.tvMinAmountInfo
            android.view.View r6 = r1.findViewById(r6)
            android.widget.TextView r6 = (android.widget.TextView) r6
            kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r6, r15)
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            r7.<init>()
            r7.append(r3)
            r7.append(r14)
            java.lang.String r3 = r21.getMaximum_amount()
            r10 = 1
            r12 = 0
            java.lang.String r3 = com.iqonic.store.utils.extensions.StringExtensionsKt.currencyFormat$default(r3, r12, r10, r12)
            r7.append(r3)
            java.lang.String r3 = r7.toString()
            java.lang.CharSequence r3 = (java.lang.CharSequence) r3
            r6.setText(r3)
            goto L_0x029f
        L_0x026c:
            int r3 = r21.getUsage_limit()
            if (r3 <= 0) goto L_0x0286
            com.iqonic.store.activity.CouponActivity r3 = r0.this$0
            r6 = 2131821018(0x7f1101da, float:1.9274767E38)
            r3.getString(r6)
            r21.getUsage_limit()
            com.iqonic.store.activity.CouponActivity r3 = r0.this$0
            r6 = 2131821016(0x7f1101d8, float:1.9274763E38)
            r3.getString(r6)
            goto L_0x029f
        L_0x0286:
            int r3 = com.iqonic.store.R.id.tvMinAmountInfo
            android.view.View r3 = r1.findViewById(r3)
            android.widget.TextView r3 = (android.widget.TextView) r3
            kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r3, r15)
            com.iqonic.store.activity.CouponActivity r6 = r0.this$0
            r7 = 2131820919(0x7f110177, float:1.9274567E38)
            java.lang.String r6 = r6.getString(r7)
            java.lang.CharSequence r6 = (java.lang.CharSequence) r6
            r3.setText(r6)
        L_0x029f:
            java.lang.String r3 = r21.getDate_expires()
            java.lang.String r7 = ""
            java.lang.String r10 = "view.tvApply"
            java.lang.String r12 = "view.lblValidTill"
            if (r3 == 0) goto L_0x0372
            java.lang.String r3 = r21.getDate_expires()
            java.lang.CharSequence r3 = (java.lang.CharSequence) r3
            boolean r3 = kotlin.text.StringsKt.isBlank(r3)
            if (r3 != 0) goto L_0x036d
            java.text.SimpleDateFormat r3 = new java.text.SimpleDateFormat
            java.lang.String r13 = "yyyy-MM-dd'T'HH:mm:ss"
            r3.<init>(r13)
            java.lang.String r13 = r21.getDate_expires()
            java.util.Date r3 = r3.parse(r13)
            java.lang.String r13 = "dateFormat.parse(item.date_expires)"
            kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r3, r13)
            java.text.SimpleDateFormat r13 = new java.text.SimpleDateFormat
            java.lang.String r14 = "MMM dd, yyyy"
            r13.<init>(r14)
            java.lang.String r13 = r13.format(r3)
            int r14 = com.iqonic.store.R.id.lblValidTill
            android.view.View r14 = r1.findViewById(r14)
            android.widget.TextView r14 = (android.widget.TextView) r14
            kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r14, r12)
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            r16 = r5
            com.iqonic.store.activity.CouponActivity r5 = r0.this$0
            r18 = r15
            r15 = 2131821020(0x7f1101dc, float:1.9274771E38)
            java.lang.String r5 = r5.getString(r15)
            r6.append(r5)
            r6.append(r9)
            r6.append(r13)
            java.lang.String r5 = r6.toString()
            java.lang.CharSequence r5 = (java.lang.CharSequence) r5
            r14.setText(r5)
            java.util.Date r5 = new java.util.Date
            r5.<init>()
            boolean r3 = r3.before(r5)
            if (r3 == 0) goto L_0x0335
            int r3 = com.iqonic.store.R.id.lblValidTill
            android.view.View r3 = r1.findViewById(r3)
            android.widget.TextView r3 = (android.widget.TextView) r3
            kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r3, r12)
            java.lang.CharSequence r7 = (java.lang.CharSequence) r7
            r3.setText(r7)
            int r3 = com.iqonic.store.R.id.tvApply
            android.view.View r3 = r1.findViewById(r3)
            android.widget.TextView r3 = (android.widget.TextView) r3
            kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r3, r10)
            r5 = 8
            r3.setVisibility(r5)
            goto L_0x03b6
        L_0x0335:
            int r3 = com.iqonic.store.R.id.lblValidTill
            android.view.View r3 = r1.findViewById(r3)
            android.widget.TextView r3 = (android.widget.TextView) r3
            kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r3, r12)
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            com.iqonic.store.activity.CouponActivity r6 = r0.this$0
            java.lang.String r6 = r6.getString(r15)
            r5.append(r6)
            r5.append(r9)
            r5.append(r13)
            java.lang.String r5 = r5.toString()
            java.lang.CharSequence r5 = (java.lang.CharSequence) r5
            r3.setText(r5)
            int r3 = com.iqonic.store.R.id.tvApply
            android.view.View r3 = r1.findViewById(r3)
            android.widget.TextView r3 = (android.widget.TextView) r3
            kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r3, r10)
            r5 = 0
            r3.setVisibility(r5)
            goto L_0x03b6
        L_0x036d:
            r16 = r5
            r18 = r15
            goto L_0x03b6
        L_0x0372:
            r16 = r5
            r18 = r15
            int r3 = r21.getUsage_limit()
            if (r3 <= 0) goto L_0x03b6
            int r3 = r21.getUsage_limit()
            int r5 = r21.getUsage_count()
            if (r3 != r5) goto L_0x03a7
            int r3 = com.iqonic.store.R.id.lblValidTill
            android.view.View r3 = r1.findViewById(r3)
            android.widget.TextView r3 = (android.widget.TextView) r3
            kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r3, r12)
            java.lang.CharSequence r7 = (java.lang.CharSequence) r7
            r3.setText(r7)
            int r3 = com.iqonic.store.R.id.tvApply
            android.view.View r3 = r1.findViewById(r3)
            android.widget.TextView r3 = (android.widget.TextView) r3
            kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r3, r10)
            r5 = 8
            r3.setVisibility(r5)
            goto L_0x03b6
        L_0x03a7:
            int r3 = com.iqonic.store.R.id.tvApply
            android.view.View r3 = r1.findViewById(r3)
            android.widget.TextView r3 = (android.widget.TextView) r3
            kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r3, r10)
            r5 = 0
            r3.setVisibility(r5)
        L_0x03b6:
            int r3 = com.iqonic.store.R.id.tvApply
            android.view.View r3 = r1.findViewById(r3)
            android.widget.TextView r3 = (android.widget.TextView) r3
            com.iqonic.store.activity.CouponActivity$onCreate$1$$special$$inlined$onClick$1 r5 = new com.iqonic.store.activity.CouponActivity$onCreate$1$$special$$inlined$onClick$1
            r5.<init>(r3, r0, r2)
            android.view.View$OnClickListener r5 = (android.view.View.OnClickListener) r5
            r3.setOnClickListener(r5)
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            int r5 = com.iqonic.store.R.id.tvCouponName
            android.view.View r5 = r1.findViewById(r5)
            android.widget.TextView r5 = (android.widget.TextView) r5
            kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r5, r11)
            java.lang.CharSequence r5 = r5.getText()
            java.lang.String r5 = r5.toString()
            r3.append(r5)
            java.lang.String r5 = "\n\nApply Coupon Code: "
            r3.append(r5)
            java.lang.String r2 = r21.getCode()
            if (r2 == 0) goto L_0x0471
            java.lang.String r2 = r2.toUpperCase()
            kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r2, r8)
            r3.append(r2)
            java.lang.String r2 = "\n\nDownload app from here: https://play.google.com/store/apps/details?id="
            r3.append(r2)
            java.lang.String r2 = "com.store.proshop"
            r3.append(r2)
            java.lang.String r2 = r3.toString()
            int r3 = com.iqonic.store.R.id.imgShare
            android.view.View r3 = r1.findViewById(r3)
            android.widget.ImageView r3 = (android.widget.ImageView) r3
            com.iqonic.store.activity.CouponActivity$onCreate$1$$special$$inlined$onClick$2 r5 = new com.iqonic.store.activity.CouponActivity$onCreate$1$$special$$inlined$onClick$2
            r5.<init>(r3, r0, r2)
            android.view.View$OnClickListener r5 = (android.view.View.OnClickListener) r5
            r3.setOnClickListener(r5)
            int r2 = com.iqonic.store.R.id.tvCouponName
            android.view.View r2 = r1.findViewById(r2)
            android.widget.TextView r2 = (android.widget.TextView) r2
            kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r2, r11)
            com.iqonic.store.utils.extensions.ExtensionsKt.changeTextPrimaryColor(r2)
            int r2 = com.iqonic.store.R.id.tvInfo
            android.view.View r2 = r1.findViewById(r2)
            android.widget.TextView r2 = (android.widget.TextView) r2
            kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r2, r4)
            com.iqonic.store.utils.extensions.ExtensionsKt.changeTextSecondaryColor(r2)
            int r2 = com.iqonic.store.R.id.tvMinAmountInfo
            android.view.View r2 = r1.findViewById(r2)
            android.widget.TextView r2 = (android.widget.TextView) r2
            r3 = r18
            kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r2, r3)
            com.iqonic.store.utils.extensions.ExtensionsKt.changeTextSecondaryColor(r2)
            int r2 = com.iqonic.store.R.id.tvCouponOffer
            android.view.View r2 = r1.findViewById(r2)
            android.widget.TextView r2 = (android.widget.TextView) r2
            r3 = r16
            kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r2, r3)
            com.iqonic.store.utils.extensions.ExtensionsKt.changeAccentColor(r2)
            int r2 = com.iqonic.store.R.id.lblValidTill
            android.view.View r2 = r1.findViewById(r2)
            android.widget.TextView r2 = (android.widget.TextView) r2
            kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r2, r12)
            com.iqonic.store.utils.extensions.ExtensionsKt.changeTextSecondaryColor(r2)
            int r2 = com.iqonic.store.R.id.tvApply
            android.view.View r1 = r1.findViewById(r2)
            android.widget.TextView r1 = (android.widget.TextView) r1
            kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r1, r10)
            com.iqonic.store.utils.extensions.ExtensionsKt.changeAccentColor(r1)
            return
        L_0x0471:
            kotlin.TypeCastException r1 = new kotlin.TypeCastException
            r2 = r17
            r1.<init>(r2)
            throw r1
        L_0x0479:
            r2 = r7
            kotlin.TypeCastException r1 = new kotlin.TypeCastException
            r1.<init>(r2)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.iqonic.store.activity.CouponActivity$onCreate$1.invoke(android.view.View, com.iqonic.store.models.Coupons, int):void");
    }
}
