package com.iqonic.store.activity;

import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.util.Base64;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.content.FileProvider;
import com.google.android.material.button.MaterialButton;
import com.iqonic.store.AppBaseActivity;
import com.iqonic.store.adapter.SpinnerAdapter;
import com.iqonic.store.models.Billing;
import com.iqonic.store.models.CountryModel;
import com.iqonic.store.models.RequestModel;
import com.iqonic.store.models.Shipping;
import com.iqonic.store.models.State;
import com.iqonic.store.utils.CircleImageView;
import com.iqonic.store.utils.ImagePicker;
import com.iqonic.store.utils.extensions.AppExtensionsKt;
import com.iqonic.store.utils.extensions.EditTextExtensionsKt;
import com.iqonic.store.utils.extensions.ExtensionsKt;
import com.iqonic.store.utils.extensions.NetworkExtensionKt;
import com.iqonic.store.utils.extensions.ViewExtensionsKt;
import com.store.proshop.R;
import com.theartofdev.edmodo.cropper.CropImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0000\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\b\u0010\u0018\u001a\u00020\u0019H\u0002J\u0010\u0010\u001a\u001a\u00020\f2\u0006\u0010\u001b\u001a\u00020\u001cH\u0002J\b\u0010\u001d\u001a\u00020\u0019H\u0002J\"\u0010\u001e\u001a\u00020\u00192\u0006\u0010\u001f\u001a\u00020 2\u0006\u0010!\u001a\u00020 2\b\u0010\"\u001a\u0004\u0018\u00010#H\u0014J \u0010$\u001a\u00020\u00192\u0016\u0010%\u001a\u0012\u0012\u0004\u0012\u00020&0\u0004j\b\u0012\u0004\u0012\u00020&`\u0006H\u0002J\u0012\u0010'\u001a\u00020\u00192\b\u0010(\u001a\u0004\u0018\u00010)H\u0015J \u0010*\u001a\u00020\u00192\u0016\u0010%\u001a\u0012\u0012\u0004\u0012\u00020&0\u0004j\b\u0012\u0004\u0012\u00020&`\u0006H\u0002J\b\u0010+\u001a\u00020\u0019H\u0002J\b\u0010,\u001a\u00020\u0019H\u0002J\b\u0010-\u001a\u00020\u0019H\u0002J\b\u0010.\u001a\u00020/H\u0002R*\u0010\u0003\u001a\u0012\u0012\u0004\u0012\u00020\u00050\u0004j\b\u0012\u0004\u0012\u00020\u0005`\u0006X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0007\u0010\b\"\u0004\b\t\u0010\nR\u0010\u0010\u000b\u001a\u0004\u0018\u00010\fX\u000e¢\u0006\u0002\n\u0000R\u0016\u0010\r\u001a\n\u0012\u0004\u0012\u00020\f\u0018\u00010\u000eX\u000e¢\u0006\u0002\n\u0000R\u001e\u0010\u000f\u001a\u0012\u0012\u0004\u0012\u00020\f0\u0004j\b\u0012\u0004\u0012\u00020\f`\u0006X\u0004¢\u0006\u0002\n\u0000R\u0016\u0010\u0010\u001a\n\u0012\u0004\u0012\u00020\f\u0018\u00010\u000eX\u000e¢\u0006\u0002\n\u0000R\u001e\u0010\u0011\u001a\u0012\u0012\u0004\u0012\u00020\f0\u0004j\b\u0012\u0004\u0012\u00020\f`\u0006X\u0004¢\u0006\u0002\n\u0000R\u0016\u0010\u0012\u001a\n\u0012\u0004\u0012\u00020\f\u0018\u00010\u000eX\u000e¢\u0006\u0002\n\u0000R\u001e\u0010\u0013\u001a\u0012\u0012\u0004\u0012\u00020\f0\u0004j\b\u0012\u0004\u0012\u00020\f`\u0006X\u0004¢\u0006\u0002\n\u0000R\u0016\u0010\u0014\u001a\n\u0012\u0004\u0012\u00020\f\u0018\u00010\u000eX\u000e¢\u0006\u0002\n\u0000R\u001e\u0010\u0015\u001a\u0012\u0012\u0004\u0012\u00020\f0\u0004j\b\u0012\u0004\u0012\u00020\f`\u0006X\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u0016\u001a\u0004\u0018\u00010\u0017X\u000e¢\u0006\u0002\n\u0000¨\u00060"}, d2 = {"Lcom/iqonic/store/activity/EditProfileActivity;", "Lcom/iqonic/store/AppBaseActivity;", "()V", "countryList", "Ljava/util/ArrayList;", "Lcom/iqonic/store/models/CountryModel;", "Lkotlin/collections/ArrayList;", "getCountryList", "()Ljava/util/ArrayList;", "setCountryList", "(Ljava/util/ArrayList;)V", "encodedImage", "", "mBillingCountryAdapter", "Landroid/widget/ArrayAdapter;", "mBillingCountryList", "mBillingStateAdapter", "mBillingStateList", "mShippingCountryAdapter", "mShippingCountryList", "mShippingStateAdapter", "mShippingStateList", "uri", "Landroid/net/Uri;", "changeColor", "", "encodeImage", "bm", "Landroid/graphics/Bitmap;", "getData", "onActivityResult", "requestCode", "", "resultCode", "data", "Landroid/content/Intent;", "onBillingCountryChanged", "its", "Lcom/iqonic/store/models/State;", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onShippingCountryChanged", "setUpListener", "updateProfile", "updateProfilePhoto", "validate", "", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: EditProfileActivity.kt */
public final class EditProfileActivity extends AppBaseActivity {
    private HashMap _$_findViewCache;
    private ArrayList<CountryModel> countryList = new ArrayList<>();
    /* access modifiers changed from: private */
    public String encodedImage;
    /* access modifiers changed from: private */
    public ArrayAdapter<String> mBillingCountryAdapter;
    /* access modifiers changed from: private */
    public final ArrayList<String> mBillingCountryList = new ArrayList<>();
    /* access modifiers changed from: private */
    public ArrayAdapter<String> mBillingStateAdapter;
    /* access modifiers changed from: private */
    public final ArrayList<String> mBillingStateList = new ArrayList<>();
    /* access modifiers changed from: private */
    public ArrayAdapter<String> mShippingCountryAdapter;
    /* access modifiers changed from: private */
    public final ArrayList<String> mShippingCountryList = new ArrayList<>();
    /* access modifiers changed from: private */
    public ArrayAdapter<String> mShippingStateAdapter;
    /* access modifiers changed from: private */
    public final ArrayList<String> mShippingStateList = new ArrayList<>();
    private Uri uri;

    public void _$_clearFindViewByIdCache() {
        HashMap hashMap = this._$_findViewCache;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    public View _$_findCachedViewById(int i) {
        if (this._$_findViewCache == null) {
            this._$_findViewCache = new HashMap();
        }
        View view = (View) this._$_findViewCache.get(Integer.valueOf(i));
        if (view != null) {
            return view;
        }
        View findViewById = findViewById(i);
        this._$_findViewCache.put(Integer.valueOf(i), findViewById);
        return findViewById;
    }

    public final ArrayList<CountryModel> getCountryList() {
        return this.countryList;
    }

    public final void setCountryList(ArrayList<CountryModel> arrayList) {
        Intrinsics.checkParameterIsNotNull(arrayList, "<set-?>");
        this.countryList = arrayList;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_edit_profile);
        Toolbar toolbar = (Toolbar) _$_findCachedViewById(com.iqonic.store.R.id.toolbar);
        Intrinsics.checkExpressionValueIsNotNull(toolbar, "toolbar");
        setToolbar(toolbar);
        setTitle(getString(R.string.lbl_edit_profile));
        mAppBarColor();
        changeColor();
        CircleImageView circleImageView = (CircleImageView) _$_findCachedViewById(com.iqonic.store.R.id.ivProfile);
        Intrinsics.checkExpressionValueIsNotNull(circleImageView, "ivProfile");
        NetworkExtensionKt.loadImageFromUrl$default(circleImageView, AppExtensionsKt.getUserProfile(), R.drawable.ic_profile, 0, 4, (Object) null);
        TextView textView = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvCountWishList);
        Intrinsics.checkExpressionValueIsNotNull(textView, "tvCountWishList");
        textView.setText(AppExtensionsKt.getWishListCount());
        TextView textView2 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvOrderCount);
        Intrinsics.checkExpressionValueIsNotNull(textView2, "tvOrderCount");
        textView2.setText(AppExtensionsKt.getOrderCount());
        ((CheckBox) _$_findCachedViewById(com.iqonic.store.R.id.cbCheck)).setOnCheckedChangeListener(new EditProfileActivity$onCreate$1(this));
        Context context = this;
        this.mShippingCountryAdapter = new SpinnerAdapter(context, this.mShippingCountryList);
        Spinner spinner = (Spinner) _$_findCachedViewById(com.iqonic.store.R.id.spCountry);
        Intrinsics.checkExpressionValueIsNotNull(spinner, "spCountry");
        spinner.setAdapter(this.mShippingCountryAdapter);
        this.mBillingCountryAdapter = new SpinnerAdapter(context, this.mBillingCountryList);
        Spinner spinner2 = (Spinner) _$_findCachedViewById(com.iqonic.store.R.id.spBillingCountry);
        Intrinsics.checkExpressionValueIsNotNull(spinner2, "spBillingCountry");
        spinner2.setAdapter(this.mBillingCountryAdapter);
        this.mBillingStateAdapter = new SpinnerAdapter(context, this.mBillingStateList);
        Spinner spinner3 = (Spinner) _$_findCachedViewById(com.iqonic.store.R.id.spBillingState);
        Intrinsics.checkExpressionValueIsNotNull(spinner3, "spBillingState");
        spinner3.setAdapter(this.mBillingStateAdapter);
        this.mShippingStateAdapter = new SpinnerAdapter(context, this.mShippingStateList);
        Spinner spinner4 = (Spinner) _$_findCachedViewById(com.iqonic.store.R.id.spState);
        Intrinsics.checkExpressionValueIsNotNull(spinner4, "spState");
        spinner4.setAdapter(this.mShippingStateAdapter);
        if (AppExtensionsKt.isLoggedIn()) {
            getData();
        }
        setUpListener();
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (i == 203) {
            CropImage.ActivityResult activityResult = CropImage.getActivityResult(intent);
            if (i2 == -1) {
                Intrinsics.checkExpressionValueIsNotNull(activityResult, "result");
                Uri uri2 = activityResult.getUri();
                ((CircleImageView) _$_findCachedViewById(com.iqonic.store.R.id.ivProfile)).setImageURI(uri2);
                Bitmap decodeStream = BitmapFactory.decodeStream(getContentResolver().openInputStream(uri2));
                Intrinsics.checkExpressionValueIsNotNull(decodeStream, "selectedImage");
                String encodeImage = encodeImage(decodeStream);
                this.encodedImage = encodeImage;
                if (encodeImage != null) {
                    updateProfilePhoto();
                }
            } else if (i2 == 204) {
                Intrinsics.checkExpressionValueIsNotNull(activityResult, "result");
                Exception error = activityResult.getError();
                if ((error != null ? error.getMessage() : null) != null) {
                    String message = error.getMessage();
                    if (message == null) {
                        Intrinsics.throwNpe();
                    }
                    ExtensionsKt.snackBar$default(this, message, 0, 2, (Object) null);
                }
            }
        } else {
            if (!(intent == null || intent.getData() == null)) {
                ((CircleImageView) _$_findCachedViewById(com.iqonic.store.R.id.ivProfile)).setImageURI(intent.getData());
            }
            Context context = this;
            String imagePathFromResult = ImagePicker.INSTANCE.getImagePathFromResult(context, i, i2, intent);
            if (imagePathFromResult != null) {
                CropImage.activity(FileProvider.getUriForFile(context, "com.store.proshop.provider", new File(imagePathFromResult))).setOutputCompressQuality(40).start(this);
            }
        }
    }

    private final String encodeImage(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
        String encodeToString = Base64.encodeToString(byteArrayOutputStream.toByteArray(), 0);
        Intrinsics.checkExpressionValueIsNotNull(encodeToString, "Base64.encodeToString(b, Base64.DEFAULT)");
        return encodeToString;
    }

    private final void setUpListener() {
        MaterialButton materialButton = (MaterialButton) _$_findCachedViewById(com.iqonic.store.R.id.btnSaveProFile);
        materialButton.setOnClickListener(new EditProfileActivity$setUpListener$$inlined$onClick$1(materialButton, this));
        CoordinatorLayout coordinatorLayout = (CoordinatorLayout) _$_findCachedViewById(com.iqonic.store.R.id.editProfileImage);
        coordinatorLayout.setOnClickListener(new EditProfileActivity$setUpListener$$inlined$onClick$2(coordinatorLayout, this));
        LinearLayout linearLayout = (LinearLayout) _$_findCachedViewById(com.iqonic.store.R.id.llOrder);
        linearLayout.setOnClickListener(new EditProfileActivity$setUpListener$$inlined$onClick$3(linearLayout, this));
        LinearLayout linearLayout2 = (LinearLayout) _$_findCachedViewById(com.iqonic.store.R.id.llWishList);
        linearLayout2.setOnClickListener(new EditProfileActivity$setUpListener$$inlined$onClick$4(linearLayout2, this));
        Spinner spinner = (Spinner) _$_findCachedViewById(com.iqonic.store.R.id.spBillingCountry);
        Intrinsics.checkExpressionValueIsNotNull(spinner, "spBillingCountry");
        spinner.setOnItemSelectedListener(new EditProfileActivity$setUpListener$5(this));
        Spinner spinner2 = (Spinner) _$_findCachedViewById(com.iqonic.store.R.id.spCountry);
        Intrinsics.checkExpressionValueIsNotNull(spinner2, "spCountry");
        spinner2.setOnItemSelectedListener(new EditProfileActivity$setUpListener$6(this));
        Spinner spinner3 = (Spinner) _$_findCachedViewById(com.iqonic.store.R.id.spBillingState);
        Intrinsics.checkExpressionValueIsNotNull(spinner3, "spBillingState");
        spinner3.setOnItemSelectedListener(new EditProfileActivity$setUpListener$7(this));
    }

    /* access modifiers changed from: private */
    public final void updateProfile() {
        Billing billing = new Billing();
        EditText editText = (EditText) _$_findCachedViewById(com.iqonic.store.R.id.edtBillingFName);
        Intrinsics.checkExpressionValueIsNotNull(editText, "edtBillingFName");
        billing.setFirst_name(EditTextExtensionsKt.textToString(editText));
        EditText editText2 = (EditText) _$_findCachedViewById(com.iqonic.store.R.id.edtBillingLName);
        Intrinsics.checkExpressionValueIsNotNull(editText2, "edtBillingLName");
        billing.setLast_name(EditTextExtensionsKt.textToString(editText2));
        EditText editText3 = (EditText) _$_findCachedViewById(com.iqonic.store.R.id.edtBillingAdd1);
        Intrinsics.checkExpressionValueIsNotNull(editText3, "edtBillingAdd1");
        billing.setAddress_1(EditTextExtensionsKt.textToString(editText3));
        EditText editText4 = (EditText) _$_findCachedViewById(com.iqonic.store.R.id.edtBillingAdd2);
        Intrinsics.checkExpressionValueIsNotNull(editText4, "edtBillingAdd2");
        billing.setAddress_2(EditTextExtensionsKt.textToString(editText4));
        EditText editText5 = (EditText) _$_findCachedViewById(com.iqonic.store.R.id.edtBillingCity);
        Intrinsics.checkExpressionValueIsNotNull(editText5, "edtBillingCity");
        billing.setCity(EditTextExtensionsKt.textToString(editText5));
        EditText editText6 = (EditText) _$_findCachedViewById(com.iqonic.store.R.id.edtBillingCompany);
        Intrinsics.checkExpressionValueIsNotNull(editText6, "edtBillingCompany");
        billing.setCompany(EditTextExtensionsKt.textToString(editText6));
        EditText editText7 = (EditText) _$_findCachedViewById(com.iqonic.store.R.id.edtBillingPinCode);
        Intrinsics.checkExpressionValueIsNotNull(editText7, "edtBillingPinCode");
        billing.setPostcode(EditTextExtensionsKt.textToString(editText7));
        Spinner spinner = (Spinner) _$_findCachedViewById(com.iqonic.store.R.id.spBillingCountry);
        Intrinsics.checkExpressionValueIsNotNull(spinner, "spBillingCountry");
        billing.setCountry(spinner.getSelectedItem().toString());
        if (!this.mBillingStateList.isEmpty()) {
            Spinner spinner2 = (Spinner) _$_findCachedViewById(com.iqonic.store.R.id.spBillingState);
            Intrinsics.checkExpressionValueIsNotNull(spinner2, "spBillingState");
            billing.setState(spinner2.getSelectedItem().toString());
        }
        EditText editText8 = (EditText) _$_findCachedViewById(com.iqonic.store.R.id.edtBillingPhone);
        Intrinsics.checkExpressionValueIsNotNull(editText8, "edtBillingPhone");
        billing.setPhone(EditTextExtensionsKt.textToString(editText8));
        EditText editText9 = (EditText) _$_findCachedViewById(com.iqonic.store.R.id.edtBillingEmail);
        Intrinsics.checkExpressionValueIsNotNull(editText9, "edtBillingEmail");
        billing.setEmail(EditTextExtensionsKt.textToString(editText9));
        Shipping shipping = new Shipping();
        EditText editText10 = (EditText) _$_findCachedViewById(com.iqonic.store.R.id.edtShippingFName);
        Intrinsics.checkExpressionValueIsNotNull(editText10, "edtShippingFName");
        shipping.setFirst_name(EditTextExtensionsKt.textToString(editText10));
        EditText editText11 = (EditText) _$_findCachedViewById(com.iqonic.store.R.id.edtShippingLName);
        Intrinsics.checkExpressionValueIsNotNull(editText11, "edtShippingLName");
        shipping.setLast_name(EditTextExtensionsKt.textToString(editText11));
        EditText editText12 = (EditText) _$_findCachedViewById(com.iqonic.store.R.id.edtShippingAdd1);
        Intrinsics.checkExpressionValueIsNotNull(editText12, "edtShippingAdd1");
        shipping.setAddress_1(EditTextExtensionsKt.textToString(editText12));
        EditText editText13 = (EditText) _$_findCachedViewById(com.iqonic.store.R.id.edtShippingCompany);
        Intrinsics.checkExpressionValueIsNotNull(editText13, "edtShippingCompany");
        shipping.setCompany(EditTextExtensionsKt.textToString(editText13));
        EditText editText14 = (EditText) _$_findCachedViewById(com.iqonic.store.R.id.edtShippingAdd2);
        Intrinsics.checkExpressionValueIsNotNull(editText14, "edtShippingAdd2");
        shipping.setAddress_2(EditTextExtensionsKt.textToString(editText14));
        EditText editText15 = (EditText) _$_findCachedViewById(com.iqonic.store.R.id.edtShippingCity);
        Intrinsics.checkExpressionValueIsNotNull(editText15, "edtShippingCity");
        shipping.setCity(EditTextExtensionsKt.textToString(editText15));
        EditText editText16 = (EditText) _$_findCachedViewById(com.iqonic.store.R.id.edtShippingPinCode);
        Intrinsics.checkExpressionValueIsNotNull(editText16, "edtShippingPinCode");
        shipping.setPostcode(EditTextExtensionsKt.textToString(editText16));
        Spinner spinner3 = (Spinner) _$_findCachedViewById(com.iqonic.store.R.id.spCountry);
        Intrinsics.checkExpressionValueIsNotNull(spinner3, "spCountry");
        shipping.setCountry(spinner3.getSelectedItem().toString());
        if (!this.mShippingStateList.isEmpty()) {
            Spinner spinner4 = (Spinner) _$_findCachedViewById(com.iqonic.store.R.id.spState);
            Intrinsics.checkExpressionValueIsNotNull(spinner4, "spState");
            shipping.setState(spinner4.getSelectedItem().toString());
        }
        RequestModel requestModel = new RequestModel();
        EditText editText17 = (EditText) _$_findCachedViewById(com.iqonic.store.R.id.edtEmail);
        Intrinsics.checkExpressionValueIsNotNull(editText17, "edtEmail");
        requestModel.setUserEmail(EditTextExtensionsKt.textToString(editText17));
        EditText editText18 = (EditText) _$_findCachedViewById(com.iqonic.store.R.id.edtFirstName);
        Intrinsics.checkExpressionValueIsNotNull(editText18, "edtFirstName");
        requestModel.setFirstName(EditTextExtensionsKt.textToString(editText18));
        EditText editText19 = (EditText) _$_findCachedViewById(com.iqonic.store.R.id.edtLastName);
        Intrinsics.checkExpressionValueIsNotNull(editText19, "edtLastName");
        requestModel.setLastName(EditTextExtensionsKt.textToString(editText19));
        requestModel.setBilling(billing);
        requestModel.setShipping(shipping);
        Uri uri2 = this.uri;
        if (uri2 != null) {
            requestModel.setImage(String.valueOf(uri2));
        }
        NetworkExtensionKt.updateCustomer(this, requestModel, new EditProfileActivity$updateProfile$1(this));
    }

    /* access modifiers changed from: private */
    public final boolean validate() {
        EditText editText = (EditText) _$_findCachedViewById(com.iqonic.store.R.id.edtFirstName);
        Intrinsics.checkExpressionValueIsNotNull(editText, "edtFirstName");
        if (EditTextExtensionsKt.checkIsEmpty(editText)) {
            EditText editText2 = (EditText) _$_findCachedViewById(com.iqonic.store.R.id.edtFirstName);
            Intrinsics.checkExpressionValueIsNotNull(editText2, "edtFirstName");
            String string = getString(R.string.error_field_required);
            Intrinsics.checkExpressionValueIsNotNull(string, "getString(R.string.error_field_required)");
            EditTextExtensionsKt.showError(editText2, string);
            return false;
        }
        EditText editText3 = (EditText) _$_findCachedViewById(com.iqonic.store.R.id.edtFirstName);
        Intrinsics.checkExpressionValueIsNotNull(editText3, "edtFirstName");
        if (!EditTextExtensionsKt.isValidText(editText3)) {
            EditText editText4 = (EditText) _$_findCachedViewById(com.iqonic.store.R.id.edtFirstName);
            Intrinsics.checkExpressionValueIsNotNull(editText4, "edtFirstName");
            String string2 = getString(R.string.error_validText);
            Intrinsics.checkExpressionValueIsNotNull(string2, "getString(R.string.error_validText)");
            EditTextExtensionsKt.showError(editText4, string2);
            return false;
        }
        EditText editText5 = (EditText) _$_findCachedViewById(com.iqonic.store.R.id.edtLastName);
        Intrinsics.checkExpressionValueIsNotNull(editText5, "edtLastName");
        if (EditTextExtensionsKt.checkIsEmpty(editText5)) {
            EditText editText6 = (EditText) _$_findCachedViewById(com.iqonic.store.R.id.edtLastName);
            Intrinsics.checkExpressionValueIsNotNull(editText6, "edtLastName");
            String string3 = getString(R.string.error_field_required);
            Intrinsics.checkExpressionValueIsNotNull(string3, "getString(R.string.error_field_required)");
            EditTextExtensionsKt.showError(editText6, string3);
            return false;
        }
        EditText editText7 = (EditText) _$_findCachedViewById(com.iqonic.store.R.id.edtLastName);
        Intrinsics.checkExpressionValueIsNotNull(editText7, "edtLastName");
        if (!EditTextExtensionsKt.isValidText(editText7)) {
            EditText editText8 = (EditText) _$_findCachedViewById(com.iqonic.store.R.id.edtLastName);
            Intrinsics.checkExpressionValueIsNotNull(editText8, "edtLastName");
            String string4 = getString(R.string.error_validText);
            Intrinsics.checkExpressionValueIsNotNull(string4, "getString(R.string.error_validText)");
            EditTextExtensionsKt.showError(editText8, string4);
            return false;
        }
        EditText editText9 = (EditText) _$_findCachedViewById(com.iqonic.store.R.id.edtEmail);
        Intrinsics.checkExpressionValueIsNotNull(editText9, "edtEmail");
        if (EditTextExtensionsKt.checkIsEmpty(editText9)) {
            EditText editText10 = (EditText) _$_findCachedViewById(com.iqonic.store.R.id.edtEmail);
            Intrinsics.checkExpressionValueIsNotNull(editText10, "edtEmail");
            String string5 = getString(R.string.error_field_required);
            Intrinsics.checkExpressionValueIsNotNull(string5, "getString(R.string.error_field_required)");
            EditTextExtensionsKt.showError(editText10, string5);
            return false;
        }
        EditText editText11 = (EditText) _$_findCachedViewById(com.iqonic.store.R.id.edtEmail);
        Intrinsics.checkExpressionValueIsNotNull(editText11, "edtEmail");
        if (!EditTextExtensionsKt.isValidEmail(editText11)) {
            EditText editText12 = (EditText) _$_findCachedViewById(com.iqonic.store.R.id.edtEmail);
            Intrinsics.checkExpressionValueIsNotNull(editText12, "edtEmail");
            String string6 = getString(R.string.error_enter_valid_email);
            Intrinsics.checkExpressionValueIsNotNull(string6, "getString(R.string.error_enter_valid_email)");
            EditTextExtensionsKt.showError(editText12, string6);
            return false;
        }
        EditText editText13 = (EditText) _$_findCachedViewById(com.iqonic.store.R.id.edtShippingFName);
        Intrinsics.checkExpressionValueIsNotNull(editText13, "edtShippingFName");
        if (EditTextExtensionsKt.checkIsEmpty(editText13)) {
            EditText editText14 = (EditText) _$_findCachedViewById(com.iqonic.store.R.id.edtShippingFName);
            Intrinsics.checkExpressionValueIsNotNull(editText14, "edtShippingFName");
            String string7 = getString(R.string.error_field_required);
            Intrinsics.checkExpressionValueIsNotNull(string7, "getString(R.string.error_field_required)");
            EditTextExtensionsKt.showError(editText14, string7);
            return false;
        }
        EditText editText15 = (EditText) _$_findCachedViewById(com.iqonic.store.R.id.edtShippingFName);
        Intrinsics.checkExpressionValueIsNotNull(editText15, "edtShippingFName");
        if (!EditTextExtensionsKt.isValidText(editText15)) {
            EditText editText16 = (EditText) _$_findCachedViewById(com.iqonic.store.R.id.edtShippingFName);
            Intrinsics.checkExpressionValueIsNotNull(editText16, "edtShippingFName");
            String string8 = getString(R.string.error_validText);
            Intrinsics.checkExpressionValueIsNotNull(string8, "getString(R.string.error_validText)");
            EditTextExtensionsKt.showError(editText16, string8);
            return false;
        }
        EditText editText17 = (EditText) _$_findCachedViewById(com.iqonic.store.R.id.edtShippingLName);
        Intrinsics.checkExpressionValueIsNotNull(editText17, "edtShippingLName");
        if (EditTextExtensionsKt.checkIsEmpty(editText17)) {
            EditText editText18 = (EditText) _$_findCachedViewById(com.iqonic.store.R.id.edtShippingLName);
            Intrinsics.checkExpressionValueIsNotNull(editText18, "edtShippingLName");
            String string9 = getString(R.string.error_field_required);
            Intrinsics.checkExpressionValueIsNotNull(string9, "getString(R.string.error_field_required)");
            EditTextExtensionsKt.showError(editText18, string9);
            return false;
        }
        EditText editText19 = (EditText) _$_findCachedViewById(com.iqonic.store.R.id.edtShippingLName);
        Intrinsics.checkExpressionValueIsNotNull(editText19, "edtShippingLName");
        if (!EditTextExtensionsKt.isValidText(editText19)) {
            EditText editText20 = (EditText) _$_findCachedViewById(com.iqonic.store.R.id.edtShippingLName);
            Intrinsics.checkExpressionValueIsNotNull(editText20, "edtShippingLName");
            String string10 = getString(R.string.error_validText);
            Intrinsics.checkExpressionValueIsNotNull(string10, "getString(R.string.error_validText)");
            EditTextExtensionsKt.showError(editText20, string10);
            return false;
        }
        EditText editText21 = (EditText) _$_findCachedViewById(com.iqonic.store.R.id.edtShippingCompany);
        Intrinsics.checkExpressionValueIsNotNull(editText21, "edtShippingCompany");
        if (EditTextExtensionsKt.checkIsEmpty(editText21)) {
            EditText editText22 = (EditText) _$_findCachedViewById(com.iqonic.store.R.id.edtShippingCompany);
            Intrinsics.checkExpressionValueIsNotNull(editText22, "edtShippingCompany");
            String string11 = getString(R.string.error_field_required);
            Intrinsics.checkExpressionValueIsNotNull(string11, "getString(R.string.error_field_required)");
            EditTextExtensionsKt.showError(editText22, string11);
            return false;
        }
        EditText editText23 = (EditText) _$_findCachedViewById(com.iqonic.store.R.id.edtShippingPinCode);
        Intrinsics.checkExpressionValueIsNotNull(editText23, "edtShippingPinCode");
        if (EditTextExtensionsKt.checkIsEmpty(editText23)) {
            EditText editText24 = (EditText) _$_findCachedViewById(com.iqonic.store.R.id.edtShippingPinCode);
            Intrinsics.checkExpressionValueIsNotNull(editText24, "edtShippingPinCode");
            String string12 = getString(R.string.error_field_required);
            Intrinsics.checkExpressionValueIsNotNull(string12, "getString(R.string.error_field_required)");
            EditTextExtensionsKt.showError(editText24, string12);
            return false;
        }
        EditText editText25 = (EditText) _$_findCachedViewById(com.iqonic.store.R.id.edtShippingAdd1);
        Intrinsics.checkExpressionValueIsNotNull(editText25, "edtShippingAdd1");
        if (EditTextExtensionsKt.checkIsEmpty(editText25)) {
            EditText editText26 = (EditText) _$_findCachedViewById(com.iqonic.store.R.id.edtShippingAdd1);
            Intrinsics.checkExpressionValueIsNotNull(editText26, "edtShippingAdd1");
            String string13 = getString(R.string.error_field_required);
            Intrinsics.checkExpressionValueIsNotNull(string13, "getString(R.string.error_field_required)");
            EditTextExtensionsKt.showError(editText26, string13);
            return false;
        }
        EditText editText27 = (EditText) _$_findCachedViewById(com.iqonic.store.R.id.edtShippingAdd2);
        Intrinsics.checkExpressionValueIsNotNull(editText27, "edtShippingAdd2");
        if (EditTextExtensionsKt.checkIsEmpty(editText27)) {
            EditText editText28 = (EditText) _$_findCachedViewById(com.iqonic.store.R.id.edtShippingAdd2);
            Intrinsics.checkExpressionValueIsNotNull(editText28, "edtShippingAdd2");
            String string14 = getString(R.string.error_field_required);
            Intrinsics.checkExpressionValueIsNotNull(string14, "getString(R.string.error_field_required)");
            EditTextExtensionsKt.showError(editText28, string14);
            return false;
        }
        EditText editText29 = (EditText) _$_findCachedViewById(com.iqonic.store.R.id.edtShippingCity);
        Intrinsics.checkExpressionValueIsNotNull(editText29, "edtShippingCity");
        if (EditTextExtensionsKt.checkIsEmpty(editText29)) {
            EditText editText30 = (EditText) _$_findCachedViewById(com.iqonic.store.R.id.edtShippingCity);
            Intrinsics.checkExpressionValueIsNotNull(editText30, "edtShippingCity");
            String string15 = getString(R.string.error_field_required);
            Intrinsics.checkExpressionValueIsNotNull(string15, "getString(R.string.error_field_required)");
            EditTextExtensionsKt.showError(editText30, string15);
            return false;
        }
        EditText editText31 = (EditText) _$_findCachedViewById(com.iqonic.store.R.id.edtBillingFName);
        Intrinsics.checkExpressionValueIsNotNull(editText31, "edtBillingFName");
        if (EditTextExtensionsKt.checkIsEmpty(editText31)) {
            EditText editText32 = (EditText) _$_findCachedViewById(com.iqonic.store.R.id.edtBillingFName);
            Intrinsics.checkExpressionValueIsNotNull(editText32, "edtBillingFName");
            String string16 = getString(R.string.error_field_required);
            Intrinsics.checkExpressionValueIsNotNull(string16, "getString(R.string.error_field_required)");
            EditTextExtensionsKt.showError(editText32, string16);
            return false;
        }
        EditText editText33 = (EditText) _$_findCachedViewById(com.iqonic.store.R.id.edtBillingFName);
        Intrinsics.checkExpressionValueIsNotNull(editText33, "edtBillingFName");
        if (!EditTextExtensionsKt.isValidText(editText33)) {
            EditText editText34 = (EditText) _$_findCachedViewById(com.iqonic.store.R.id.edtBillingFName);
            Intrinsics.checkExpressionValueIsNotNull(editText34, "edtBillingFName");
            String string17 = getString(R.string.error_validText);
            Intrinsics.checkExpressionValueIsNotNull(string17, "getString(R.string.error_validText)");
            EditTextExtensionsKt.showError(editText34, string17);
            return false;
        }
        EditText editText35 = (EditText) _$_findCachedViewById(com.iqonic.store.R.id.edtBillingLName);
        Intrinsics.checkExpressionValueIsNotNull(editText35, "edtBillingLName");
        if (EditTextExtensionsKt.checkIsEmpty(editText35)) {
            EditText editText36 = (EditText) _$_findCachedViewById(com.iqonic.store.R.id.edtBillingLName);
            Intrinsics.checkExpressionValueIsNotNull(editText36, "edtBillingLName");
            String string18 = getString(R.string.error_field_required);
            Intrinsics.checkExpressionValueIsNotNull(string18, "getString(R.string.error_field_required)");
            EditTextExtensionsKt.showError(editText36, string18);
            return false;
        }
        EditText editText37 = (EditText) _$_findCachedViewById(com.iqonic.store.R.id.edtBillingLName);
        Intrinsics.checkExpressionValueIsNotNull(editText37, "edtBillingLName");
        if (!EditTextExtensionsKt.isValidText(editText37)) {
            EditText editText38 = (EditText) _$_findCachedViewById(com.iqonic.store.R.id.edtBillingLName);
            Intrinsics.checkExpressionValueIsNotNull(editText38, "edtBillingLName");
            String string19 = getString(R.string.error_validText);
            Intrinsics.checkExpressionValueIsNotNull(string19, "getString(R.string.error_validText)");
            EditTextExtensionsKt.showError(editText38, string19);
            return false;
        }
        EditText editText39 = (EditText) _$_findCachedViewById(com.iqonic.store.R.id.edtBillingAdd1);
        Intrinsics.checkExpressionValueIsNotNull(editText39, "edtBillingAdd1");
        if (EditTextExtensionsKt.checkIsEmpty(editText39)) {
            EditText editText40 = (EditText) _$_findCachedViewById(com.iqonic.store.R.id.edtBillingAdd1);
            Intrinsics.checkExpressionValueIsNotNull(editText40, "edtBillingAdd1");
            String string20 = getString(R.string.error_field_required);
            Intrinsics.checkExpressionValueIsNotNull(string20, "getString(R.string.error_field_required)");
            EditTextExtensionsKt.showError(editText40, string20);
            return false;
        }
        EditText editText41 = (EditText) _$_findCachedViewById(com.iqonic.store.R.id.edtBillingAdd2);
        Intrinsics.checkExpressionValueIsNotNull(editText41, "edtBillingAdd2");
        if (EditTextExtensionsKt.checkIsEmpty(editText41)) {
            EditText editText42 = (EditText) _$_findCachedViewById(com.iqonic.store.R.id.edtBillingAdd2);
            Intrinsics.checkExpressionValueIsNotNull(editText42, "edtBillingAdd2");
            String string21 = getString(R.string.error_field_required);
            Intrinsics.checkExpressionValueIsNotNull(string21, "getString(R.string.error_field_required)");
            EditTextExtensionsKt.showError(editText42, string21);
            return false;
        }
        EditText editText43 = (EditText) _$_findCachedViewById(com.iqonic.store.R.id.edtBillingCity);
        Intrinsics.checkExpressionValueIsNotNull(editText43, "edtBillingCity");
        if (EditTextExtensionsKt.checkIsEmpty(editText43)) {
            EditText editText44 = (EditText) _$_findCachedViewById(com.iqonic.store.R.id.edtBillingCity);
            Intrinsics.checkExpressionValueIsNotNull(editText44, "edtBillingCity");
            String string22 = getString(R.string.error_field_required);
            Intrinsics.checkExpressionValueIsNotNull(string22, "getString(R.string.error_field_required)");
            EditTextExtensionsKt.showError(editText44, string22);
            return false;
        }
        EditText editText45 = (EditText) _$_findCachedViewById(com.iqonic.store.R.id.edtBillingPinCode);
        Intrinsics.checkExpressionValueIsNotNull(editText45, "edtBillingPinCode");
        if (EditTextExtensionsKt.checkIsEmpty(editText45)) {
            EditText editText46 = (EditText) _$_findCachedViewById(com.iqonic.store.R.id.edtBillingPinCode);
            Intrinsics.checkExpressionValueIsNotNull(editText46, "edtBillingPinCode");
            String string23 = getString(R.string.error_field_required);
            Intrinsics.checkExpressionValueIsNotNull(string23, "getString(R.string.error_field_required)");
            EditTextExtensionsKt.showError(editText46, string23);
            return false;
        }
        EditText editText47 = (EditText) _$_findCachedViewById(com.iqonic.store.R.id.edtBillingPhone);
        Intrinsics.checkExpressionValueIsNotNull(editText47, "edtBillingPhone");
        if (EditTextExtensionsKt.checkIsEmpty(editText47)) {
            EditText editText48 = (EditText) _$_findCachedViewById(com.iqonic.store.R.id.edtBillingPhone);
            Intrinsics.checkExpressionValueIsNotNull(editText48, "edtBillingPhone");
            String string24 = getString(R.string.error_field_required);
            Intrinsics.checkExpressionValueIsNotNull(string24, "getString(R.string.error_field_required)");
            EditTextExtensionsKt.showError(editText48, string24);
            return false;
        }
        EditText editText49 = (EditText) _$_findCachedViewById(com.iqonic.store.R.id.edtBillingEmail);
        Intrinsics.checkExpressionValueIsNotNull(editText49, "edtBillingEmail");
        if (EditTextExtensionsKt.checkIsEmpty(editText49)) {
            EditText editText50 = (EditText) _$_findCachedViewById(com.iqonic.store.R.id.edtBillingEmail);
            Intrinsics.checkExpressionValueIsNotNull(editText50, "edtBillingEmail");
            String string25 = getString(R.string.error_field_required);
            Intrinsics.checkExpressionValueIsNotNull(string25, "getString(R.string.error_field_required)");
            EditTextExtensionsKt.showError(editText50, string25);
            return false;
        }
        EditText editText51 = (EditText) _$_findCachedViewById(com.iqonic.store.R.id.edtBillingEmail);
        Intrinsics.checkExpressionValueIsNotNull(editText51, "edtBillingEmail");
        if (EditTextExtensionsKt.isValidEmail(editText51)) {
            return true;
        }
        EditText editText52 = (EditText) _$_findCachedViewById(com.iqonic.store.R.id.edtBillingEmail);
        Intrinsics.checkExpressionValueIsNotNull(editText52, "edtBillingEmail");
        String string26 = getString(R.string.error_enter_valid_email);
        Intrinsics.checkExpressionValueIsNotNull(string26, "getString(R.string.error_enter_valid_email)");
        EditTextExtensionsKt.showError(editText52, string26);
        return false;
    }

    private final void getData() {
        if (ExtensionsKt.isNetworkAvailable()) {
            showProgress(true);
            NetworkExtensionKt.getRestApiImpl$default((String) null, 1, (Object) null).retrieveCustomer(new EditProfileActivity$getData$1(this), new EditProfileActivity$getData$2(this));
        }
    }

    /* access modifiers changed from: private */
    public final void onBillingCountryChanged(ArrayList<State> arrayList) {
        ArrayAdapter<String> arrayAdapter;
        this.mBillingStateList.clear();
        CheckBox checkBox = (CheckBox) _$_findCachedViewById(com.iqonic.store.R.id.cbCheck);
        Intrinsics.checkExpressionValueIsNotNull(checkBox, "cbCheck");
        if (checkBox.isChecked()) {
            this.mShippingStateList.clear();
        }
        for (State state : arrayList) {
            this.mBillingStateList.add(state.getName());
            CheckBox checkBox2 = (CheckBox) _$_findCachedViewById(com.iqonic.store.R.id.cbCheck);
            Intrinsics.checkExpressionValueIsNotNull(checkBox2, "cbCheck");
            if (checkBox2.isChecked()) {
                this.mShippingStateList.add(state.getName());
            }
        }
        ArrayAdapter<String> arrayAdapter2 = this.mBillingStateAdapter;
        if (arrayAdapter2 != null) {
            arrayAdapter2.notifyDataSetChanged();
        }
        CheckBox checkBox3 = (CheckBox) _$_findCachedViewById(com.iqonic.store.R.id.cbCheck);
        Intrinsics.checkExpressionValueIsNotNull(checkBox3, "cbCheck");
        if (checkBox3.isChecked() && (arrayAdapter = this.mShippingStateAdapter) != null) {
            arrayAdapter.notifyDataSetChanged();
        }
        if (this.mBillingStateList.isEmpty()) {
            Spinner spinner = (Spinner) _$_findCachedViewById(com.iqonic.store.R.id.spBillingState);
            Intrinsics.checkExpressionValueIsNotNull(spinner, "spBillingState");
            ViewExtensionsKt.hide(spinner);
        } else {
            Spinner spinner2 = (Spinner) _$_findCachedViewById(com.iqonic.store.R.id.spBillingState);
            Intrinsics.checkExpressionValueIsNotNull(spinner2, "spBillingState");
            ViewExtensionsKt.show(spinner2);
        }
        if (this.mShippingStateList.isEmpty()) {
            Spinner spinner3 = (Spinner) _$_findCachedViewById(com.iqonic.store.R.id.spState);
            Intrinsics.checkExpressionValueIsNotNull(spinner3, "spState");
            ViewExtensionsKt.hide(spinner3);
            return;
        }
        Spinner spinner4 = (Spinner) _$_findCachedViewById(com.iqonic.store.R.id.spState);
        Intrinsics.checkExpressionValueIsNotNull(spinner4, "spState");
        ViewExtensionsKt.show(spinner4);
    }

    /* access modifiers changed from: private */
    public final void onShippingCountryChanged(ArrayList<State> arrayList) {
        this.mShippingStateList.clear();
        for (State name : arrayList) {
            this.mShippingStateList.add(name.getName());
        }
        ArrayAdapter<String> arrayAdapter = this.mShippingStateAdapter;
        if (arrayAdapter != null) {
            arrayAdapter.notifyDataSetChanged();
        }
        if (this.mShippingStateList.isEmpty()) {
            Spinner spinner = (Spinner) _$_findCachedViewById(com.iqonic.store.R.id.spState);
            Intrinsics.checkExpressionValueIsNotNull(spinner, "spState");
            ViewExtensionsKt.hide(spinner);
            return;
        }
        Spinner spinner2 = (Spinner) _$_findCachedViewById(com.iqonic.store.R.id.spState);
        Intrinsics.checkExpressionValueIsNotNull(spinner2, "spState");
        ViewExtensionsKt.show(spinner2);
    }

    private final void updateProfilePhoto() {
        showProgress(true);
        RequestModel requestModel = new RequestModel();
        requestModel.setBase64_img(this.encodedImage);
        NetworkExtensionKt.saveProfileImage(this, requestModel, new EditProfileActivity$updateProfilePhoto$1(this));
    }

    private final void changeColor() {
        TextView textView = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvUserName);
        Intrinsics.checkExpressionValueIsNotNull(textView, "tvUserName");
        ExtensionsKt.changeTextPrimaryColor(textView);
        EditText editText = (EditText) _$_findCachedViewById(com.iqonic.store.R.id.edtFirstName);
        Intrinsics.checkExpressionValueIsNotNull(editText, "edtFirstName");
        ExtensionsKt.changeTextSecondaryColor(editText);
        EditText editText2 = (EditText) _$_findCachedViewById(com.iqonic.store.R.id.edtLastName);
        Intrinsics.checkExpressionValueIsNotNull(editText2, "edtLastName");
        ExtensionsKt.changeTextSecondaryColor(editText2);
        EditText editText3 = (EditText) _$_findCachedViewById(com.iqonic.store.R.id.edtEmail);
        Intrinsics.checkExpressionValueIsNotNull(editText3, "edtEmail");
        ExtensionsKt.changeTextSecondaryColor(editText3);
        EditText editText4 = (EditText) _$_findCachedViewById(com.iqonic.store.R.id.edtBillingPhone);
        Intrinsics.checkExpressionValueIsNotNull(editText4, "edtBillingPhone");
        ExtensionsKt.changeTextSecondaryColor(editText4);
        EditText editText5 = (EditText) _$_findCachedViewById(com.iqonic.store.R.id.edtBillingEmail);
        Intrinsics.checkExpressionValueIsNotNull(editText5, "edtBillingEmail");
        ExtensionsKt.changeTextSecondaryColor(editText5);
        CheckBox checkBox = (CheckBox) _$_findCachedViewById(com.iqonic.store.R.id.cbCheck);
        Intrinsics.checkExpressionValueIsNotNull(checkBox, "cbCheck");
        ExtensionsKt.changeTextPrimaryColor(checkBox);
        TextView textView2 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.lblOrder);
        Intrinsics.checkExpressionValueIsNotNull(textView2, "lblOrder");
        ExtensionsKt.changeTextPrimaryColor(textView2);
        TextView textView3 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvCountWishList);
        Intrinsics.checkExpressionValueIsNotNull(textView3, "tvCountWishList");
        ExtensionsKt.changePrimaryColor(textView3);
        TextView textView4 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.lblWishList);
        Intrinsics.checkExpressionValueIsNotNull(textView4, "lblWishList");
        ExtensionsKt.changeTextPrimaryColor(textView4);
        TextView textView5 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvOrderCount);
        Intrinsics.checkExpressionValueIsNotNull(textView5, "tvOrderCount");
        ExtensionsKt.changePrimaryColor(textView5);
        TextView textView6 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.lblPersonal);
        Intrinsics.checkExpressionValueIsNotNull(textView6, "lblPersonal");
        ExtensionsKt.changeTextPrimaryColor(textView6);
        TextView textView7 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.lblPersonal);
        Intrinsics.checkExpressionValueIsNotNull(textView7, "lblPersonal");
        ExtensionsKt.changeTextPrimaryColor(textView7);
        TextView textView8 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.lblBilling);
        Intrinsics.checkExpressionValueIsNotNull(textView8, "lblBilling");
        ExtensionsKt.changeTextPrimaryColor(textView8);
        TextView textView9 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.lblShipping);
        Intrinsics.checkExpressionValueIsNotNull(textView9, "lblShipping");
        ExtensionsKt.changeTextPrimaryColor(textView9);
        EditText editText6 = (EditText) _$_findCachedViewById(com.iqonic.store.R.id.edtShippingFName);
        Intrinsics.checkExpressionValueIsNotNull(editText6, "edtShippingFName");
        ExtensionsKt.changeTextSecondaryColor(editText6);
        EditText editText7 = (EditText) _$_findCachedViewById(com.iqonic.store.R.id.edtBillingFName);
        Intrinsics.checkExpressionValueIsNotNull(editText7, "edtBillingFName");
        ExtensionsKt.changeTextSecondaryColor(editText7);
        EditText editText8 = (EditText) _$_findCachedViewById(com.iqonic.store.R.id.edtShippingLName);
        Intrinsics.checkExpressionValueIsNotNull(editText8, "edtShippingLName");
        ExtensionsKt.changeTextSecondaryColor(editText8);
        EditText editText9 = (EditText) _$_findCachedViewById(com.iqonic.store.R.id.edtBillingLName);
        Intrinsics.checkExpressionValueIsNotNull(editText9, "edtBillingLName");
        ExtensionsKt.changeTextSecondaryColor(editText9);
        EditText editText10 = (EditText) _$_findCachedViewById(com.iqonic.store.R.id.edtShippingCompany);
        Intrinsics.checkExpressionValueIsNotNull(editText10, "edtShippingCompany");
        ExtensionsKt.changeTextSecondaryColor(editText10);
        EditText editText11 = (EditText) _$_findCachedViewById(com.iqonic.store.R.id.edtBillingCompany);
        Intrinsics.checkExpressionValueIsNotNull(editText11, "edtBillingCompany");
        ExtensionsKt.changeTextSecondaryColor(editText11);
        EditText editText12 = (EditText) _$_findCachedViewById(com.iqonic.store.R.id.edtShippingAdd1);
        Intrinsics.checkExpressionValueIsNotNull(editText12, "edtShippingAdd1");
        ExtensionsKt.changeTextSecondaryColor(editText12);
        EditText editText13 = (EditText) _$_findCachedViewById(com.iqonic.store.R.id.edtBillingAdd1);
        Intrinsics.checkExpressionValueIsNotNull(editText13, "edtBillingAdd1");
        ExtensionsKt.changeTextSecondaryColor(editText13);
        EditText editText14 = (EditText) _$_findCachedViewById(com.iqonic.store.R.id.edtShippingAdd2);
        Intrinsics.checkExpressionValueIsNotNull(editText14, "edtShippingAdd2");
        ExtensionsKt.changeTextSecondaryColor(editText14);
        EditText editText15 = (EditText) _$_findCachedViewById(com.iqonic.store.R.id.edtBillingAdd2);
        Intrinsics.checkExpressionValueIsNotNull(editText15, "edtBillingAdd2");
        ExtensionsKt.changeTextSecondaryColor(editText15);
        EditText editText16 = (EditText) _$_findCachedViewById(com.iqonic.store.R.id.edtShippingCity);
        Intrinsics.checkExpressionValueIsNotNull(editText16, "edtShippingCity");
        ExtensionsKt.changeTextSecondaryColor(editText16);
        EditText editText17 = (EditText) _$_findCachedViewById(com.iqonic.store.R.id.edtBillingCity);
        Intrinsics.checkExpressionValueIsNotNull(editText17, "edtBillingCity");
        ExtensionsKt.changeTextSecondaryColor(editText17);
        EditText editText18 = (EditText) _$_findCachedViewById(com.iqonic.store.R.id.edtShippingPinCode);
        Intrinsics.checkExpressionValueIsNotNull(editText18, "edtShippingPinCode");
        ExtensionsKt.changeTextSecondaryColor(editText18);
        EditText editText19 = (EditText) _$_findCachedViewById(com.iqonic.store.R.id.edtBillingPinCode);
        Intrinsics.checkExpressionValueIsNotNull(editText19, "edtBillingPinCode");
        ExtensionsKt.changeTextSecondaryColor(editText19);
        RelativeLayout relativeLayout = (RelativeLayout) _$_findCachedViewById(com.iqonic.store.R.id.rlMain);
        Intrinsics.checkExpressionValueIsNotNull(relativeLayout, "rlMain");
        ExtensionsKt.changeBackgroundColor(relativeLayout);
        TextView textView10 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvEmail);
        Intrinsics.checkExpressionValueIsNotNull(textView10, "tvEmail");
        ExtensionsKt.changePrimaryColor(textView10);
        MaterialButton materialButton = (MaterialButton) _$_findCachedViewById(com.iqonic.store.R.id.btnSaveProFile);
        Intrinsics.checkExpressionValueIsNotNull(materialButton, "btnSaveProFile");
        materialButton.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor(AppExtensionsKt.getButtonColor())));
    }
}
