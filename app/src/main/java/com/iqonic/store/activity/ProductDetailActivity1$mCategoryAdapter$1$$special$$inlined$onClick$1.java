package com.iqonic.store.activity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import com.iqonic.store.models.StoreCategory;
import kotlin.Metadata;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\b\u0003\n\u0002\b\u0004\u0010\u0000\u001a\u00020\u0001\"\b\b\u0000\u0010\u0002*\u00020\u00032\u000e\u0010\u0004\u001a\n \u0005*\u0004\u0018\u00010\u00030\u0003H\n¢\u0006\u0002\b\u0006¨\u0006\u0007"}, d2 = {"<anonymous>", "", "T", "Landroid/view/View;", "it", "kotlin.jvm.PlatformType", "onClick", "com/iqonic/store/utils/extensions/ExtensionsKt$onClick$1"}, k = 3, mv = {1, 1, 16})
/* compiled from: Extensions.kt */
public final class ProductDetailActivity1$mCategoryAdapter$1$$special$$inlined$onClick$1 implements View.OnClickListener {
    final /* synthetic */ StoreCategory $model$inlined;
    final /* synthetic */ View $this_onClick;
    final /* synthetic */ ProductDetailActivity1$mCategoryAdapter$1 this$0;

    public ProductDetailActivity1$mCategoryAdapter$1$$special$$inlined$onClick$1(View view, ProductDetailActivity1$mCategoryAdapter$1 productDetailActivity1$mCategoryAdapter$1, StoreCategory storeCategory) {
        this.$this_onClick = view;
        this.this$0 = productDetailActivity1$mCategoryAdapter$1;
        this.$model$inlined = storeCategory;
    }

    public final void onClick(View view) {
        ProductDetailActivity1 productDetailActivity1 = this.this$0.this$0;
        Bundle bundle = null;
        Intent intent = new Intent(productDetailActivity1, ViewAllProductActivity.class);
        new ProductDetailActivity1$mCategoryAdapter$1$$special$$inlined$onClick$1$lambda$1(this).invoke(intent);
        if (Build.VERSION.SDK_INT >= 16) {
            productDetailActivity1.startActivityForResult(intent, -1, bundle);
        } else {
            productDetailActivity1.startActivityForResult(intent, -1);
        }
    }
}
