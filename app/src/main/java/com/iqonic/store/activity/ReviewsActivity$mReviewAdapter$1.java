package com.iqonic.store.activity;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.iqonic.store.R;
import com.iqonic.store.models.ProductReviewData;
import com.iqonic.store.utils.extensions.AppExtensionsKt;
import com.iqonic.store.utils.extensions.ExtensionsKt;
import com.iqonic.store.utils.extensions.StringExtensionsKt;
import com.iqonic.store.utils.extensions.ViewExtensionsKt;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function3;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0007H\n¢\u0006\u0002\b\b"}, d2 = {"<anonymous>", "", "view", "Landroid/view/View;", "model", "Lcom/iqonic/store/models/ProductReviewData;", "<anonymous parameter 2>", "", "invoke"}, k = 3, mv = {1, 1, 16})
/* compiled from: ReviewsActivity.kt */
final class ReviewsActivity$mReviewAdapter$1 extends Lambda implements Function3<View, ProductReviewData, Integer, Unit> {
    final /* synthetic */ ReviewsActivity this$0;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    ReviewsActivity$mReviewAdapter$1(ReviewsActivity reviewsActivity) {
        super(3);
        this.this$0 = reviewsActivity;
    }

    public /* bridge */ /* synthetic */ Object invoke(Object obj, Object obj2, Object obj3) {
        invoke((View) obj, (ProductReviewData) obj2, ((Number) obj3).intValue());
        return Unit.INSTANCE;
    }

    public final void invoke(View view, ProductReviewData productReviewData, int i) {
        Intrinsics.checkParameterIsNotNull(view, "view");
        Intrinsics.checkParameterIsNotNull(productReviewData, "model");
        TextView textView = (TextView) view.findViewById(R.id.tvProductReviewRating);
        Intrinsics.checkExpressionValueIsNotNull(textView, "view.tvProductReviewRating");
        textView.setText(String.valueOf(productReviewData.getRating()));
        TextView textView2 = (TextView) view.findViewById(R.id.tvProductReviewSubHeading);
        Intrinsics.checkExpressionValueIsNotNull(textView2, "view.tvProductReviewSubHeading");
        textView2.setText(StringExtensionsKt.getHtmlString(productReviewData.getReview()));
        TextView textView3 = (TextView) view.findViewById(R.id.tvProductReviewCmt);
        Intrinsics.checkExpressionValueIsNotNull(textView3, "view.tvProductReviewCmt");
        textView3.setText(productReviewData.getName());
        TextView textView4 = (TextView) view.findViewById(R.id.tvProductReviewDuration);
        Intrinsics.checkExpressionValueIsNotNull(textView4, "view.tvProductReviewDuration");
        textView4.setText(AppExtensionsKt.convertToLocalDate(productReviewData.getDate_created()));
        if (productReviewData.getRating() == 1) {
            TextView textView5 = (TextView) view.findViewById(R.id.tvProductReviewRating);
            Intrinsics.checkExpressionValueIsNotNull(textView5, "view.tvProductReviewRating");
            ViewExtensionsKt.changeBackgroundTint(textView5, ExtensionsKt.color(this.this$0, com.store.proshop.R.color.red));
        }
        if (productReviewData.getRating() == 2 || productReviewData.getRating() == 3) {
            TextView textView6 = (TextView) view.findViewById(R.id.tvProductReviewRating);
            Intrinsics.checkExpressionValueIsNotNull(textView6, "view.tvProductReviewRating");
            ViewExtensionsKt.changeBackgroundTint(textView6, ExtensionsKt.color(this.this$0, com.store.proshop.R.color.yellow));
        }
        if (productReviewData.getRating() == 5 || productReviewData.getRating() == 4) {
            TextView textView7 = (TextView) view.findViewById(R.id.tvProductReviewRating);
            Intrinsics.checkExpressionValueIsNotNull(textView7, "view.tvProductReviewRating");
            ViewExtensionsKt.changeBackgroundTint(textView7, ExtensionsKt.color(this.this$0, com.store.proshop.R.color.green));
        }
        ImageView imageView = (ImageView) view.findViewById(R.id.ivMenu);
        imageView.setOnClickListener(new ReviewsActivity$mReviewAdapter$1$$special$$inlined$onClick$1(imageView, this, view, productReviewData));
        if (Intrinsics.areEqual((Object) productReviewData.getEmail(), (Object) AppExtensionsKt.getEmail())) {
            ImageView imageView2 = (ImageView) view.findViewById(R.id.ivMenu);
            Intrinsics.checkExpressionValueIsNotNull(imageView2, "view.ivMenu");
            ViewExtensionsKt.show(imageView2);
        } else {
            ImageView imageView3 = (ImageView) view.findViewById(R.id.ivMenu);
            Intrinsics.checkExpressionValueIsNotNull(imageView3, "view.ivMenu");
            ViewExtensionsKt.hide(imageView3);
        }
        TextView textView8 = (TextView) view.findViewById(R.id.tvProductReviewCmt);
        Intrinsics.checkExpressionValueIsNotNull(textView8, "view.tvProductReviewCmt");
        ExtensionsKt.changeTextPrimaryColor(textView8);
        TextView textView9 = (TextView) view.findViewById(R.id.tvProductReviewSubHeading);
        Intrinsics.checkExpressionValueIsNotNull(textView9, "view.tvProductReviewSubHeading");
        ExtensionsKt.changeTextSecondaryColor(textView9);
        TextView textView10 = (TextView) view.findViewById(R.id.tvProductReviewDuration);
        Intrinsics.checkExpressionValueIsNotNull(textView10, "view.tvProductReviewDuration");
        ExtensionsKt.changeTextSecondaryColor(textView10);
    }
}
