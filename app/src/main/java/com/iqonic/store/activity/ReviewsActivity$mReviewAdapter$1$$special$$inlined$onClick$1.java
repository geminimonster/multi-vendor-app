package com.iqonic.store.activity;

import android.view.View;
import android.widget.ImageView;
import android.widget.PopupMenu;
import com.iqonic.store.R;
import com.iqonic.store.models.ProductReviewData;
import kotlin.Metadata;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\b\u0003\n\u0002\b\u0004\u0010\u0000\u001a\u00020\u0001\"\b\b\u0000\u0010\u0002*\u00020\u00032\u000e\u0010\u0004\u001a\n \u0005*\u0004\u0018\u00010\u00030\u0003H\n¢\u0006\u0002\b\u0006¨\u0006\u0007"}, d2 = {"<anonymous>", "", "T", "Landroid/view/View;", "it", "kotlin.jvm.PlatformType", "onClick", "com/iqonic/store/utils/extensions/ExtensionsKt$onClick$1"}, k = 3, mv = {1, 1, 16})
/* compiled from: Extensions.kt */
public final class ReviewsActivity$mReviewAdapter$1$$special$$inlined$onClick$1 implements View.OnClickListener {
    final /* synthetic */ ProductReviewData $model$inlined;
    final /* synthetic */ View $this_onClick;
    final /* synthetic */ View $view$inlined;
    final /* synthetic */ ReviewsActivity$mReviewAdapter$1 this$0;

    public ReviewsActivity$mReviewAdapter$1$$special$$inlined$onClick$1(View view, ReviewsActivity$mReviewAdapter$1 reviewsActivity$mReviewAdapter$1, View view2, ProductReviewData productReviewData) {
        this.$this_onClick = view;
        this.this$0 = reviewsActivity$mReviewAdapter$1;
        this.$view$inlined = view2;
        this.$model$inlined = productReviewData;
    }

    public final void onClick(View view) {
        ImageView imageView = (ImageView) this.$this_onClick;
        PopupMenu popupMenu = new PopupMenu(this.this$0.this$0, (ImageView) this.$view$inlined.findViewById(R.id.ivMenu));
        popupMenu.getMenuInflater().inflate(com.store.proshop.R.menu.menu_review, popupMenu.getMenu());
        popupMenu.setOnMenuItemClickListener(new ReviewsActivity$mReviewAdapter$1$$special$$inlined$onClick$1$lambda$1(this));
        popupMenu.show();
    }
}
