package com.iqonic.store.activity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import com.iqonic.store.utils.extensions.ExtensionsKt$launchActivity$1;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.Lambda;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n¢\u0006\u0002\b\u0002"}, d2 = {"<anonymous>", "", "invoke"}, k = 3, mv = {1, 1, 16})
/* compiled from: SplashActivity.kt */
final class SplashActivity$onCreate$1 extends Lambda implements Function0<Unit> {
    final /* synthetic */ SplashActivity this$0;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    SplashActivity$onCreate$1(SplashActivity splashActivity) {
        super(0);
        this.this$0 = splashActivity;
    }

    public final void invoke() {
        SplashActivity splashActivity = this.this$0;
        Bundle bundle = null;
        Intent intent = new Intent(splashActivity, WalkThroughActivity.class);
        ExtensionsKt$launchActivity$1.INSTANCE.invoke(intent);
        if (Build.VERSION.SDK_INT >= 16) {
            splashActivity.startActivityForResult(intent, -1, bundle);
        } else {
            splashActivity.startActivityForResult(intent, -1);
        }
        this.this$0.finish();
    }
}
