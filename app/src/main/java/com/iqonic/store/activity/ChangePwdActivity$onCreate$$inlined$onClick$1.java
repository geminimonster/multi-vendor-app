package com.iqonic.store.activity;

import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import com.iqonic.store.R;
import com.iqonic.store.models.RequestModel;
import com.iqonic.store.utils.extensions.AppExtensionsKt;
import com.iqonic.store.utils.extensions.EditTextExtensionsKt;
import com.iqonic.store.utils.extensions.ExtensionsKt;
import com.iqonic.store.utils.extensions.NetworkExtensionKt;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import kotlin.text.StringsKt;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\b\u0003\n\u0002\b\u0004\u0010\u0000\u001a\u00020\u0001\"\b\b\u0000\u0010\u0002*\u00020\u00032\u000e\u0010\u0004\u001a\n \u0005*\u0004\u0018\u00010\u00030\u0003H\n¢\u0006\u0002\b\u0006¨\u0006\u0007"}, d2 = {"<anonymous>", "", "T", "Landroid/view/View;", "it", "kotlin.jvm.PlatformType", "onClick", "com/iqonic/store/utils/extensions/ExtensionsKt$onClick$1"}, k = 3, mv = {1, 1, 16})
/* compiled from: Extensions.kt */
public final class ChangePwdActivity$onCreate$$inlined$onClick$1 implements View.OnClickListener {
    final /* synthetic */ View $this_onClick;
    final /* synthetic */ ChangePwdActivity this$0;

    public ChangePwdActivity$onCreate$$inlined$onClick$1(View view, ChangePwdActivity changePwdActivity) {
        this.$this_onClick = view;
        this.this$0 = changePwdActivity;
    }

    public final void onClick(View view) {
        TextView textView = (TextView) this.$this_onClick;
        EditText editText = (EditText) this.this$0._$_findCachedViewById(R.id.edtOldPwd);
        Intrinsics.checkExpressionValueIsNotNull(editText, "edtOldPwd");
        if (EditTextExtensionsKt.checkIsEmpty(editText)) {
            EditText editText2 = (EditText) this.this$0._$_findCachedViewById(R.id.edtOldPwd);
            Intrinsics.checkExpressionValueIsNotNull(editText2, "edtOldPwd");
            String string = this.this$0.getString(com.store.proshop.R.string.error_field_required);
            Intrinsics.checkExpressionValueIsNotNull(string, "getString(R.string.error_field_required)");
            EditTextExtensionsKt.showError(editText2, string);
            return;
        }
        EditText editText3 = (EditText) this.this$0._$_findCachedViewById(R.id.edtNewPwd);
        Intrinsics.checkExpressionValueIsNotNull(editText3, "edtNewPwd");
        if (EditTextExtensionsKt.checkIsEmpty(editText3)) {
            EditText editText4 = (EditText) this.this$0._$_findCachedViewById(R.id.edtNewPwd);
            Intrinsics.checkExpressionValueIsNotNull(editText4, "edtNewPwd");
            String string2 = this.this$0.getString(com.store.proshop.R.string.error_field_required);
            Intrinsics.checkExpressionValueIsNotNull(string2, "getString(R.string.error_field_required)");
            EditTextExtensionsKt.showError(editText4, string2);
            return;
        }
        EditText editText5 = (EditText) this.this$0._$_findCachedViewById(R.id.edtNewPwd);
        Intrinsics.checkExpressionValueIsNotNull(editText5, "edtNewPwd");
        if (EditTextExtensionsKt.validPassword(editText5)) {
            EditText editText6 = (EditText) this.this$0._$_findCachedViewById(R.id.edtNewPwd);
            Intrinsics.checkExpressionValueIsNotNull(editText6, "edtNewPwd");
            String string3 = this.this$0.getString(com.store.proshop.R.string.error_pwd_digit_required);
            Intrinsics.checkExpressionValueIsNotNull(string3, "getString(R.string.error_pwd_digit_required)");
            EditTextExtensionsKt.showError(editText6, string3);
            return;
        }
        EditText editText7 = (EditText) this.this$0._$_findCachedViewById(R.id.edtConfirmPwd);
        Intrinsics.checkExpressionValueIsNotNull(editText7, "edtConfirmPwd");
        if (EditTextExtensionsKt.checkIsEmpty(editText7)) {
            EditText editText8 = (EditText) this.this$0._$_findCachedViewById(R.id.edtConfirmPwd);
            Intrinsics.checkExpressionValueIsNotNull(editText8, "edtConfirmPwd");
            String string4 = this.this$0.getString(com.store.proshop.R.string.error_field_required);
            Intrinsics.checkExpressionValueIsNotNull(string4, "getString(R.string.error_field_required)");
            EditTextExtensionsKt.showError(editText8, string4);
            return;
        }
        EditText editText9 = (EditText) this.this$0._$_findCachedViewById(R.id.edtConfirmPwd);
        Intrinsics.checkExpressionValueIsNotNull(editText9, "edtConfirmPwd");
        if (EditTextExtensionsKt.validPassword(editText9)) {
            EditText editText10 = (EditText) this.this$0._$_findCachedViewById(R.id.edtConfirmPwd);
            Intrinsics.checkExpressionValueIsNotNull(editText10, "edtConfirmPwd");
            String string5 = this.this$0.getString(com.store.proshop.R.string.error_pwd_digit_required);
            Intrinsics.checkExpressionValueIsNotNull(string5, "getString(R.string.error_pwd_digit_required)");
            EditTextExtensionsKt.showError(editText10, string5);
            return;
        }
        EditText editText11 = (EditText) this.this$0._$_findCachedViewById(R.id.edtConfirmPwd);
        Intrinsics.checkExpressionValueIsNotNull(editText11, "edtConfirmPwd");
        String obj = editText11.getText().toString();
        EditText editText12 = (EditText) this.this$0._$_findCachedViewById(R.id.edtNewPwd);
        Intrinsics.checkExpressionValueIsNotNull(editText12, "edtNewPwd");
        if (!StringsKt.equals(obj, editText12.getText().toString(), false)) {
            EditText editText13 = (EditText) this.this$0._$_findCachedViewById(R.id.edtConfirmPwd);
            Intrinsics.checkExpressionValueIsNotNull(editText13, "edtConfirmPwd");
            String string6 = this.this$0.getString(com.store.proshop.R.string.error_password_not_matches);
            Intrinsics.checkExpressionValueIsNotNull(string6, "getString(R.string.error_password_not_matches)");
            EditTextExtensionsKt.showError(editText13, string6);
            return;
        }
        RequestModel requestModel = new RequestModel();
        EditText editText14 = (EditText) this.this$0._$_findCachedViewById(R.id.edtOldPwd);
        Intrinsics.checkExpressionValueIsNotNull(editText14, "edtOldPwd");
        requestModel.setPassword(editText14.getText().toString());
        EditText editText15 = (EditText) this.this$0._$_findCachedViewById(R.id.edtNewPwd);
        Intrinsics.checkExpressionValueIsNotNull(editText15, "edtNewPwd");
        requestModel.setNew_password(editText15.getText().toString());
        requestModel.setUsername(AppExtensionsKt.getUserName());
        this.this$0.showProgress(true);
        if (ExtensionsKt.isNetworkAvailable()) {
            this.this$0.showProgress(true);
            NetworkExtensionKt.getRestApiImpl$default((String) null, 1, (Object) null).changePwd(requestModel, new ChangePwdActivity$onCreate$$inlined$onClick$1$lambda$1(this), new ChangePwdActivity$onCreate$$inlined$onClick$1$lambda$2(this));
            return;
        }
        this.this$0.showProgress(false);
        ExtensionsKt.noInternetSnackBar(this.this$0);
    }
}
