package com.iqonic.store.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.appcompat.widget.Toolbar;
import com.iqonic.store.AppBaseActivity;
import com.iqonic.store.fragments.BaseFragment;
import com.iqonic.store.utils.extensions.AppExtensionsKt;
import com.iqonic.store.utils.extensions.ExtensionsKt;
import com.store.proshop.R;
import java.util.HashMap;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\b\u0010\u0003\u001a\u00020\u0004H\u0002J\u0012\u0010\u0005\u001a\u00020\u00042\b\u0010\u0006\u001a\u0004\u0018\u00010\u0007H\u0015¨\u0006\b"}, d2 = {"Lcom/iqonic/store/activity/ChangePwdActivity;", "Lcom/iqonic/store/AppBaseActivity;", "()V", "changeColor", "", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: ChangePwdActivity.kt */
public final class ChangePwdActivity extends AppBaseActivity {
    private HashMap _$_findViewCache;

    public void _$_clearFindViewByIdCache() {
        HashMap hashMap = this._$_findViewCache;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    public View _$_findCachedViewById(int i) {
        if (this._$_findViewCache == null) {
            this._$_findViewCache = new HashMap();
        }
        View view = (View) this._$_findViewCache.get(Integer.valueOf(i));
        if (view != null) {
            return view;
        }
        View findViewById = findViewById(i);
        this._$_findViewCache.put(Integer.valueOf(i), findViewById);
        return findViewById;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_change_pwd);
        Toolbar toolbar = (Toolbar) _$_findCachedViewById(com.iqonic.store.R.id.toolbar);
        Intrinsics.checkExpressionValueIsNotNull(toolbar, "toolbar");
        setToolbar(toolbar);
        setTitle(getString(R.string.lbl_change_pwd));
        mAppBarColor();
        changeColor();
        EditText editText = (EditText) _$_findCachedViewById(com.iqonic.store.R.id.edtOldPwd);
        Intrinsics.checkExpressionValueIsNotNull(editText, "edtOldPwd");
        editText.setTransformationMethod(BaseFragment.BiggerDotTransformation.INSTANCE);
        EditText editText2 = (EditText) _$_findCachedViewById(com.iqonic.store.R.id.edtNewPwd);
        Intrinsics.checkExpressionValueIsNotNull(editText2, "edtNewPwd");
        editText2.setTransformationMethod(BaseFragment.BiggerDotTransformation.INSTANCE);
        EditText editText3 = (EditText) _$_findCachedViewById(com.iqonic.store.R.id.edtConfirmPwd);
        Intrinsics.checkExpressionValueIsNotNull(editText3, "edtConfirmPwd");
        editText3.setTransformationMethod(BaseFragment.BiggerDotTransformation.INSTANCE);
        TextView textView = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.btnChangePassword);
        textView.setOnClickListener(new ChangePwdActivity$onCreate$$inlined$onClick$1(textView, this));
    }

    private final void changeColor() {
        TextView textView = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.btnChangePassword);
        Intrinsics.checkExpressionValueIsNotNull(textView, "btnChangePassword");
        ExtensionsKt.changeBackgroundTint(textView, AppExtensionsKt.getButtonColor());
        EditText editText = (EditText) _$_findCachedViewById(com.iqonic.store.R.id.edtOldPwd);
        Intrinsics.checkExpressionValueIsNotNull(editText, "edtOldPwd");
        ExtensionsKt.changeTextPrimaryColor(editText);
        EditText editText2 = (EditText) _$_findCachedViewById(com.iqonic.store.R.id.edtNewPwd);
        Intrinsics.checkExpressionValueIsNotNull(editText2, "edtNewPwd");
        ExtensionsKt.changeTextPrimaryColor(editText2);
        EditText editText3 = (EditText) _$_findCachedViewById(com.iqonic.store.R.id.edtConfirmPwd);
        Intrinsics.checkExpressionValueIsNotNull(editText3, "edtConfirmPwd");
        ExtensionsKt.changeTextPrimaryColor(editText3);
        LinearLayout linearLayout = (LinearLayout) _$_findCachedViewById(com.iqonic.store.R.id.llMain);
        Intrinsics.checkExpressionValueIsNotNull(linearLayout, "llMain");
        ExtensionsKt.changeBackgroundColor(linearLayout);
    }
}
