package com.iqonic.store.activity;

import com.google.gson.Gson;
import com.iqonic.store.models.CustomerData;
import com.iqonic.store.utils.Constants;
import com.iqonic.store.utils.extensions.AppExtensionsKt;
import com.iqonic.store.utils.extensions.ExtensionsKt;
import com.store.proshop.R;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n¢\u0006\u0002\b\u0004"}, d2 = {"<anonymous>", "", "it", "Lcom/iqonic/store/models/CustomerData;", "invoke"}, k = 3, mv = {1, 1, 16})
/* compiled from: EditProfileActivity.kt */
final class EditProfileActivity$updateProfile$1 extends Lambda implements Function1<CustomerData, Unit> {
    final /* synthetic */ EditProfileActivity this$0;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    EditProfileActivity$updateProfile$1(EditProfileActivity editProfileActivity) {
        super(1);
        this.this$0 = editProfileActivity;
    }

    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        invoke((CustomerData) obj);
        return Unit.INSTANCE;
    }

    public final void invoke(CustomerData customerData) {
        Intrinsics.checkParameterIsNotNull(customerData, "it");
        EditProfileActivity editProfileActivity = this.this$0;
        String string = editProfileActivity.getString(R.string.lbl_profile_saved_successfully);
        Intrinsics.checkExpressionValueIsNotNull(string, "getString(R.string.lbl_profile_saved_successfully)");
        ExtensionsKt.snackBar$default(editProfileActivity, string, 0, 2, (Object) null);
        this.this$0.showProgress(false);
        AppExtensionsKt.getSharedPrefInstance().removeKey(Constants.SharedPref.BILLING);
        AppExtensionsKt.getSharedPrefInstance().removeKey(Constants.SharedPref.SHIPPING);
        AppExtensionsKt.getSharedPrefInstance().setValue(Constants.SharedPref.BILLING, new Gson().toJson((Object) customerData.getBilling()));
        AppExtensionsKt.getSharedPrefInstance().setValue(Constants.SharedPref.SHIPPING, new Gson().toJson((Object) customerData.getShipping()));
        this.this$0.setResult(-1);
        this.this$0.finish();
    }
}
