package com.iqonic.store.activity;

import android.view.View;
import com.iqonic.store.adapter.BaseAdapter;
import com.iqonic.store.models.Term;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function3;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0007H\n¢\u0006\u0002\b\b"}, d2 = {"<anonymous>", "", "<anonymous parameter 0>", "", "<anonymous parameter 1>", "Landroid/view/View;", "model", "Lcom/iqonic/store/models/Term;", "invoke"}, k = 3, mv = {1, 1, 16})
/* compiled from: SearchActivity.kt */
final class SearchActivity$openFilterBottomSheet$1 extends Lambda implements Function3<Integer, View, Term, Unit> {
    final /* synthetic */ BaseAdapter $brandAdapter;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    SearchActivity$openFilterBottomSheet$1(BaseAdapter baseAdapter) {
        super(3);
        this.$brandAdapter = baseAdapter;
    }

    public /* bridge */ /* synthetic */ Object invoke(Object obj, Object obj2, Object obj3) {
        invoke(((Number) obj).intValue(), (View) obj2, (Term) obj3);
        return Unit.INSTANCE;
    }

    public final void invoke(int i, View view, Term term) {
        Intrinsics.checkParameterIsNotNull(view, "<anonymous parameter 1>");
        Intrinsics.checkParameterIsNotNull(term, "model");
        term.setSelected(!term.isSelected());
        this.$brandAdapter.notifyDataSetChanged();
    }
}
