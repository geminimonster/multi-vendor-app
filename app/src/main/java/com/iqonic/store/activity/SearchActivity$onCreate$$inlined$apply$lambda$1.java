package com.iqonic.store.activity;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001d\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000*\u0001\u0000\b\n\u0018\u00002\u00020\u0001J\u0018\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0007H\u0016¨\u0006\b¸\u0006\u0000"}, d2 = {"com/iqonic/store/activity/SearchActivity$onCreate$4$1", "Landroidx/recyclerview/widget/RecyclerView$OnScrollListener;", "onScrollStateChanged", "", "recyclerView", "Landroidx/recyclerview/widget/RecyclerView;", "newState", "", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: SearchActivity.kt */
public final class SearchActivity$onCreate$$inlined$apply$lambda$1 extends RecyclerView.OnScrollListener {
    final /* synthetic */ SearchActivity this$0;

    SearchActivity$onCreate$$inlined$apply$lambda$1(SearchActivity searchActivity) {
        this.this$0 = searchActivity;
    }

    public void onScrollStateChanged(RecyclerView recyclerView, int i) {
        Intrinsics.checkParameterIsNotNull(recyclerView, "recyclerView");
        super.onScrollStateChanged(recyclerView, i);
        RecyclerView.LayoutManager layoutManager = recyclerView.getLayoutManager();
        Integer valueOf = layoutManager != null ? Integer.valueOf(layoutManager.getItemCount()) : null;
        RecyclerView.LayoutManager layoutManager2 = recyclerView.getLayoutManager();
        if (layoutManager2 != null) {
            int findLastCompletelyVisibleItemPosition = ((GridLayoutManager) layoutManager2).findLastCompletelyVisibleItemPosition();
            if (findLastCompletelyVisibleItemPosition != 0 && !this.this$0.mIsLoading && valueOf != null && valueOf.intValue() - 1 == findLastCompletelyVisibleItemPosition && this.this$0.totalPages > this.this$0.mPage) {
                this.this$0.mIsLoading = true;
                SearchActivity searchActivity = this.this$0;
                searchActivity.mPage = searchActivity.mPage + 1;
                this.this$0.searchRequest.setPage(this.this$0.mPage);
                this.this$0.loadProducts();
                return;
            }
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type androidx.recyclerview.widget.GridLayoutManager");
    }
}
