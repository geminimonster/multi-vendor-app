package com.iqonic.store.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import androidx.appcompat.widget.Toolbar;
import androidx.viewpager.widget.ViewPager;
import com.iqonic.store.AppBaseActivity;
import com.iqonic.store.models.Image;
import com.iqonic.store.models.StoreProductModel;
import com.iqonic.store.utils.dotsindicator.DotsIndicator;
import com.iqonic.store.utils.extensions.ExtensionsKt;
import com.store.proshop.R;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u0012\u0010\u0007\u001a\u00020\b2\b\u0010\t\u001a\u0004\u0018\u00010\nH\u0015R\u001e\u0010\u0003\u001a\u0012\u0012\u0004\u0012\u00020\u00050\u0004j\b\u0012\u0004\u0012\u00020\u0005`\u0006X\u0004¢\u0006\u0002\n\u0000¨\u0006\u000b"}, d2 = {"Lcom/iqonic/store/activity/ZoomImageActivity;", "Lcom/iqonic/store/AppBaseActivity;", "()V", "mImages", "Ljava/util/ArrayList;", "", "Lkotlin/collections/ArrayList;", "onCreate", "", "savedInstanceState", "Landroid/os/Bundle;", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: ZoomImageActivity.kt */
public final class ZoomImageActivity extends AppBaseActivity {
    private HashMap _$_findViewCache;
    private final ArrayList<String> mImages = new ArrayList<>();

    public void _$_clearFindViewByIdCache() {
        HashMap hashMap = this._$_findViewCache;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    public View _$_findCachedViewById(int i) {
        if (this._$_findViewCache == null) {
            this._$_findViewCache = new HashMap();
        }
        View view = (View) this._$_findViewCache.get(Integer.valueOf(i));
        if (view != null) {
            return view;
        }
        View findViewById = findViewById(i);
        this._$_findViewCache.put(Integer.valueOf(i), findViewById);
        return findViewById;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_zoom_image);
        Intent intent = getIntent();
        Serializable serializableExtra = intent != null ? intent.getSerializableExtra("data") : null;
        if (serializableExtra != null) {
            StoreProductModel storeProductModel = (StoreProductModel) serializableExtra;
            Toolbar toolbar = (Toolbar) _$_findCachedViewById(com.iqonic.store.R.id.toolbar);
            Intrinsics.checkExpressionValueIsNotNull(toolbar, "toolbar");
            setToolbar(toolbar);
            setTitle(getString(R.string.lbl_images));
            mAppBarColor();
            LinearLayout linearLayout = (LinearLayout) _$_findCachedViewById(com.iqonic.store.R.id.llMain);
            Intrinsics.checkExpressionValueIsNotNull(linearLayout, "llMain");
            ExtensionsKt.changeBackgroundColor(linearLayout);
            List<Image> images = storeProductModel.getImages();
            if (images == null) {
                Intrinsics.throwNpe();
            }
            int size = images.size();
            for (int i = 0; i < size; i++) {
                List<Image> images2 = storeProductModel.getImages();
                if (images2 == null) {
                    Intrinsics.throwNpe();
                }
                String src = images2.get(i).getSrc();
                ArrayList<String> arrayList = this.mImages;
                if (src == null) {
                    Intrinsics.throwNpe();
                }
                arrayList.add(src);
            }
            ImageAdapter imageAdapter = new ImageAdapter(this.mImages);
            ViewPager viewPager = (ViewPager) _$_findCachedViewById(com.iqonic.store.R.id.productViewPager);
            Intrinsics.checkExpressionValueIsNotNull(viewPager, "productViewPager");
            viewPager.setAdapter(imageAdapter);
            ((DotsIndicator) _$_findCachedViewById(com.iqonic.store.R.id.dots)).attachViewPager((ViewPager) _$_findCachedViewById(com.iqonic.store.R.id.productViewPager));
            ((DotsIndicator) _$_findCachedViewById(com.iqonic.store.R.id.dots)).setDotDrawable(R.drawable.bg_circle_primary, R.drawable.black_dot);
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.iqonic.store.models.StoreProductModel");
    }
}
