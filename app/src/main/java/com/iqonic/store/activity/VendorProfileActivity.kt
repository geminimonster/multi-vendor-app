package com.iqonic.store.activity

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.GridLayoutManager
import com.google.android.material.appbar.AppBarLayout
import com.iqonic.store.AppBaseActivity
import com.iqonic.store.R
import com.iqonic.store.adapter.BaseAdapter
import com.iqonic.store.models.RequestModel
import com.iqonic.store.models.StoreProductModel
import com.iqonic.store.utils.Constants
import com.iqonic.store.utils.extensions.*
import kotlinx.android.synthetic.main.activity_vender_profile.*
import kotlinx.android.synthetic.main.item_viewproductgrid.view.*
import kotlin.math.abs

class VendorProfileActivity : AppBaseActivity() {

    var vendorId = 0


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_vender_profile)
        setToolbar(toolbar)
        vendorId = intent.getIntExtra(Constants.KeyIntent.VENDER_ID, 0)
        toolbar_layout.setCollapsedTitleTextAppearance(R.style.CollapsedAppBar)
        app_bar.addOnOffsetChangedListener(AppBarLayout.OnOffsetChangedListener { _, verticalOffset ->
            if (abs(verticalOffset) - app_bar.totalScrollRange == 0) {
                toolbar_layout.title = tvName.text
            } else {
                toolbar_layout.title = ""
            }
        })
        getVendorDetails()
    }

    private fun getVendorDetails() {
        if (isNetworkAvailable()) {
            showProgress(true)
            getRestApiImpl().getVendorDetails(vendorId, onApiSuccess = {
                tvName.text = it.store_name
                imgShopImage.loadImageFromUrl(it.banner)
                civImgVender.loadImageFromUrl(it.gravatar)
                tvFullName.text = it.first_name + " " + it.last_name
                if (it.phone != null) {
                    tvMobileNumber.text = it.phone
                    tvMobileNumber.visibility = View.VISIBLE
                } else {
                    tvMobileNumber.visibility = View.VISIBLE
                }

                var addressText = ""
                if (it.address != null) {
                    if (!it.address.street_1.isBlank() && addressText.isBlank()) {
                        addressText = it.address.street_1
                    }
                    if (!it.address.street_2.isBlank()) {
                        if (addressText.isBlank()) {
                            addressText = it.address.street_2
                        } else {
                            addressText += ", " + it.address.street_2
                        }
                    }

                    if (!it.address.city!!.isBlank()) {
                        if (addressText.isBlank()) {
                            addressText = it.address.city!!
                        } else {
                            addressText += ", " + it.address.city!!
                        }
                    }
                    if (!it.address.zip.isBlank()) {
                        if (addressText.isBlank()) {
                            addressText = it.address.zip
                        } else {
                            addressText += " - " + it.address.zip
                        }
                    }
                    if (!it.address.state!!.isBlank()) {
                        if (addressText.isBlank()) {
                            addressText = it.address.state!!
                        } else {
                            addressText += ", " + it.address.state!!
                        }
                    }
                    if (!it.address.country.isBlank()) {
                        if (addressText.isBlank()) {
                            addressText = it.address.country
                        } else {
                            addressText += ", " + it.address.country
                        }
                    }
                    tvAddress.visibility = View.VISIBLE
                    tvAddress.text = addressText
                } else {
                    tvAddress.visibility = View.GONE
                }
                getVendorProduct()
                scrollView.visibility = View.VISIBLE
            }, onApiError = {
                showProgress(false)
                snackBarError(it)
            })
        } else {
            showProgress(false)
            noInternetSnackBar()
        }
    }

    private fun getVendorProduct() {
        val linearLayoutManager = GridLayoutManager(this, 2)
        extraProduct.layoutManager = linearLayoutManager
        extraProduct.apply {
            extraProduct.rvItemAnimation()
            extraProduct.adapter = mProductAdapter

            getRestApiImpl().getVendorProduct(vendorId, onApiSuccess = {
                mProductAdapter.addItems(it)
                mProductAdapter.notifyDataSetChanged()
                showProgress(false)
            }, onApiError = {
                showProgress(false)
                snackBarError(it)
            })
            extraProduct.rvItemAnimation()
        }
    }

    private val mProductAdapter =
        BaseAdapter<StoreProductModel>(R.layout.item_viewproductgrid, onBind = { view, model, _ ->

            if (model.images!!.isNotEmpty()) {
                view.ivProduct.loadImageFromUrl(model.images!![0].src!!)
            }

            view.tvProductName.text = model.name
            if (model.salePrice!!.isEmpty()) {
                view.tvDiscountPrice.text = model.price!!.currencyFormat()
                view.tvOriginalPrice.visibility = View.GONE
                view.tvOriginalPrice.text = ""
                view.tvSale.hide()
            } else {
                if (model.onSale) {
                    view.tvDiscountPrice.text = model.salePrice?.currencyFormat()
                    view.tvSale.show()
                    view.tvOriginalPrice.applyStrike()
                    view.tvOriginalPrice.text = model.regularPrice?.currencyFormat()
                    view.tvOriginalPrice.visibility = View.VISIBLE
                } else {
                    view.tvDiscountPrice.text = model.regularPrice?.currencyFormat()
                    view.tvOriginalPrice.text = ""
                    view.tvOriginalPrice.visibility = View.GONE
                    view.tvSale.hide()
                }
            }
            if (model.attributes!!.isNotEmpty()) {
                view.tvProductWeight.text = model.attributes?.get(0)?.options!![0]
            }
            if (model.purchasable) {
                if (model.in_stock!!) {
                    view.tvAdd.show()
                } else {
                    view.tvAdd.hide()
                }
            } else {
                view.tvAdd.hide()
            }

            view.tvAdd.onClick {
                addCart(model)
            }
            view.onClick {
                launchActivity<ProductDetailActivityNew> {
                    putExtra(Constants.KeyIntent.PRODUCT_ID, model.id)
                }
            }
        })

    private fun addCart(model: StoreProductModel) {
        if (isLoggedIn()) {
            val requestModel = RequestModel()
            if (model.type == "variable") {
                requestModel.pro_id = model.variations!![0]
            } else {
                requestModel.pro_id = model.id
            }
            requestModel.quantity = 1
            addItemToCart(requestModel, onApiSuccess = {
            })
        } else launchActivity<SignInUpActivity> { }
    }


}
