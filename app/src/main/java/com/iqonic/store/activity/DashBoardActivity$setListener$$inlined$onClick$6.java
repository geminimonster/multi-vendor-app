package com.iqonic.store.activity;

import android.view.View;
import android.widget.TextView;
import com.iqonic.store.ShopHopApp;
import com.iqonic.store.utils.Constants;
import com.iqonic.store.utils.extensions.AppExtensionsKt;
import com.iqonic.store.utils.extensions.ExtensionsKt;
import kotlin.Metadata;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\b\u0003\n\u0002\b\u0004\u0010\u0000\u001a\u00020\u0001\"\b\b\u0000\u0010\u0002*\u00020\u00032\u000e\u0010\u0004\u001a\n \u0005*\u0004\u0018\u00010\u00030\u0003H\n¢\u0006\u0002\b\u0006¨\u0006\u0007"}, d2 = {"<anonymous>", "", "T", "Landroid/view/View;", "it", "kotlin.jvm.PlatformType", "onClick", "com/iqonic/store/utils/extensions/ExtensionsKt$onClick$1"}, k = 3, mv = {1, 1, 16})
/* compiled from: Extensions.kt */
public final class DashBoardActivity$setListener$$inlined$onClick$6 implements View.OnClickListener {
    final /* synthetic */ View $this_onClick;
    final /* synthetic */ DashBoardActivity this$0;

    public DashBoardActivity$setListener$$inlined$onClick$6(View view, DashBoardActivity dashBoardActivity) {
        this.$this_onClick = view;
        this.this$0 = dashBoardActivity;
    }

    public final void onClick(View view) {
        TextView textView = (TextView) this.$this_onClick;
        if (this.this$0.mModeFlag) {
            ShopHopApp.Companion.changeAppTheme(false);
            ExtensionsKt.switchToDarkMode(this.this$0, false);
            AppExtensionsKt.getSharedPrefInstance().removeKey(Constants.SharedPref.MODE);
            AppExtensionsKt.getSharedPrefInstance().setValue(Constants.SharedPref.MODE, 1);
        } else {
            ShopHopApp.Companion.changeAppTheme(true);
            ExtensionsKt.switchToDarkMode(this.this$0, true);
            AppExtensionsKt.getSharedPrefInstance().removeKey(Constants.SharedPref.MODE);
            AppExtensionsKt.getSharedPrefInstance().setValue(Constants.SharedPref.MODE, 2);
        }
        this.this$0.closeDrawer();
    }
}
