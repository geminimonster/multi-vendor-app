package com.iqonic.store.activity;

import android.view.MenuItem;
import android.widget.PopupMenu;
import com.store.proshop.R;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\b\u0003\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n¢\u0006\u0002\b\u0005¨\u0006\u0006"}, d2 = {"<anonymous>", "", "item", "Landroid/view/MenuItem;", "kotlin.jvm.PlatformType", "onMenuItemClick", "com/iqonic/store/activity/ReviewsActivity$mReviewAdapter$1$1$1"}, k = 3, mv = {1, 1, 16})
/* compiled from: ReviewsActivity.kt */
final class ReviewsActivity$mReviewAdapter$1$$special$$inlined$onClick$1$lambda$1 implements PopupMenu.OnMenuItemClickListener {
    final /* synthetic */ ReviewsActivity$mReviewAdapter$1$$special$$inlined$onClick$1 this$0;

    ReviewsActivity$mReviewAdapter$1$$special$$inlined$onClick$1$lambda$1(ReviewsActivity$mReviewAdapter$1$$special$$inlined$onClick$1 reviewsActivity$mReviewAdapter$1$$special$$inlined$onClick$1) {
        this.this$0 = reviewsActivity$mReviewAdapter$1$$special$$inlined$onClick$1;
    }

    public final boolean onMenuItemClick(MenuItem menuItem) {
        if (menuItem == null) {
            Intrinsics.throwNpe();
        }
        switch (menuItem.getItemId()) {
            case R.id.nav_delete:
                this.this$0.this$0.this$0.confirmDialog(this.this$0.$model$inlined);
                return true;
            case R.id.nav_update:
                this.this$0.this$0.this$0.updateReview(this.this$0.$model$inlined);
                return true;
            default:
                return true;
        }
    }
}
