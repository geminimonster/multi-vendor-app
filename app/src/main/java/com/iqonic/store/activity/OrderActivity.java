package com.iqonic.store.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.RecyclerView;
import com.iqonic.store.AppBaseActivity;
import com.iqonic.store.adapter.BaseAdapter;
import com.iqonic.store.models.Order;
import com.iqonic.store.utils.extensions.ExtensionsKt;
import com.iqonic.store.utils.extensions.NetworkExtensionKt;
import com.store.proshop.R;
import java.util.HashMap;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\b\u0010\u0006\u001a\u00020\u0007H\u0002J\"\u0010\b\u001a\u00020\u00072\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\n2\b\u0010\f\u001a\u0004\u0018\u00010\rH\u0014J\u0012\u0010\u000e\u001a\u00020\u00072\b\u0010\u000f\u001a\u0004\u0018\u00010\u0010H\u0015R\u0016\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u00048\u0002X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0011"}, d2 = {"Lcom/iqonic/store/activity/OrderActivity;", "Lcom/iqonic/store/AppBaseActivity;", "()V", "mOrderAdapter", "Lcom/iqonic/store/adapter/BaseAdapter;", "Lcom/iqonic/store/models/Order;", "getOrderList", "", "onActivityResult", "requestCode", "", "resultCode", "data", "Landroid/content/Intent;", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: OrderActivity.kt */
public final class OrderActivity extends AppBaseActivity {
    private HashMap _$_findViewCache;
    /* access modifiers changed from: private */
    public final BaseAdapter<Order> mOrderAdapter = new BaseAdapter<>(R.layout.item_orderlist, new OrderActivity$mOrderAdapter$1(this));

    public void _$_clearFindViewByIdCache() {
        HashMap hashMap = this._$_findViewCache;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    public View _$_findCachedViewById(int i) {
        if (this._$_findViewCache == null) {
            this._$_findViewCache = new HashMap();
        }
        View view = (View) this._$_findViewCache.get(Integer.valueOf(i));
        if (view != null) {
            return view;
        }
        View findViewById = findViewById(i);
        this._$_findViewCache.put(Integer.valueOf(i), findViewById);
        return findViewById;
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (i2 == -1 && i == 206) {
            getOrderList();
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_order);
        setTitle(getString(R.string.lbl_my_orders));
        Toolbar toolbar = (Toolbar) _$_findCachedViewById(com.iqonic.store.R.id.toolbar);
        Intrinsics.checkExpressionValueIsNotNull(toolbar, "toolbar");
        setToolbar(toolbar);
        mAppBarColor();
        RelativeLayout relativeLayout = (RelativeLayout) _$_findCachedViewById(com.iqonic.store.R.id.rlMain);
        Intrinsics.checkExpressionValueIsNotNull(relativeLayout, "rlMain");
        ExtensionsKt.changeBackgroundColor(relativeLayout);
        RecyclerView recyclerView = (RecyclerView) _$_findCachedViewById(com.iqonic.store.R.id.rvOrder);
        Intrinsics.checkExpressionValueIsNotNull(recyclerView, "rvOrder");
        disableHardwareRendering(recyclerView);
        RecyclerView recyclerView2 = (RecyclerView) _$_findCachedViewById(com.iqonic.store.R.id.rvOrder);
        Intrinsics.checkExpressionValueIsNotNull(recyclerView2, "rvOrder");
        recyclerView2.setAdapter(this.mOrderAdapter);
        getOrderList();
    }

    private final void getOrderList() {
        NetworkExtensionKt.getOrder(this, new OrderActivity$getOrderList$1(this));
    }
}
