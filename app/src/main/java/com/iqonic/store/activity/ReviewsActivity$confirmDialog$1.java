package com.iqonic.store.activity;

import android.content.DialogInterface;
import com.iqonic.store.models.ProductReviewData;
import com.iqonic.store.utils.extensions.ExtensionsKt;
import com.iqonic.store.utils.extensions.NetworkExtensionKt;
import com.store.proshop.R;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\n¢\u0006\u0002\b\u0006"}, d2 = {"<anonymous>", "", "dialog", "Landroid/content/DialogInterface;", "i", "", "invoke"}, k = 3, mv = {1, 1, 16})
/* compiled from: ReviewsActivity.kt */
final class ReviewsActivity$confirmDialog$1 extends Lambda implements Function2<DialogInterface, Integer, Unit> {
    final /* synthetic */ ProductReviewData $mode;
    final /* synthetic */ ReviewsActivity this$0;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    ReviewsActivity$confirmDialog$1(ReviewsActivity reviewsActivity, ProductReviewData productReviewData) {
        super(2);
        this.this$0 = reviewsActivity;
        this.$mode = productReviewData;
    }

    public /* bridge */ /* synthetic */ Object invoke(Object obj, Object obj2) {
        invoke((DialogInterface) obj, ((Number) obj2).intValue());
        return Unit.INSTANCE;
    }

    public final void invoke(DialogInterface dialogInterface, int i) {
        Intrinsics.checkParameterIsNotNull(dialogInterface, "dialog");
        if (ExtensionsKt.isNetworkAvailable()) {
            this.this$0.showProgress(true);
            NetworkExtensionKt.getRestApiImpl$default((String) null, 1, (Object) null).DeleteProductReview(this.$mode.getId(), new Function1<ProductReviewData, Unit>(this) {
                final /* synthetic */ ReviewsActivity$confirmDialog$1 this$0;

                {
                    this.this$0 = r1;
                }

                public /* bridge */ /* synthetic */ Object invoke(Object obj) {
                    invoke((ProductReviewData) obj);
                    return Unit.INSTANCE;
                }

                public final void invoke(ProductReviewData productReviewData) {
                    Intrinsics.checkParameterIsNotNull(productReviewData, "it");
                    this.this$0.this$0.showProgress(false);
                    ReviewsActivity reviewsActivity = this.this$0.this$0;
                    String string = this.this$0.this$0.getString(R.string.success);
                    Intrinsics.checkExpressionValueIsNotNull(string, "getString(R.string.success)");
                    ExtensionsKt.snackBar$default(reviewsActivity, string, 0, 2, (Object) null);
                    this.this$0.this$0.listProductReviews();
                }
            }, new Function1<String, Unit>(this) {
                final /* synthetic */ ReviewsActivity$confirmDialog$1 this$0;

                {
                    this.this$0 = r1;
                }

                public /* bridge */ /* synthetic */ Object invoke(Object obj) {
                    invoke((String) obj);
                    return Unit.INSTANCE;
                }

                public final void invoke(String str) {
                    Intrinsics.checkParameterIsNotNull(str, "it");
                    this.this$0.this$0.showProgress(false);
                    ExtensionsKt.snackBarError(this.this$0.this$0, str);
                }
            });
            return;
        }
        this.this$0.showProgress(false);
        dialogInterface.dismiss();
        ExtensionsKt.noInternetSnackBar(this.this$0);
    }
}
