package com.iqonic.store.activity;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import com.iqonic.store.AppBaseActivity;
import com.iqonic.store.ShopHopApp;
import com.iqonic.store.fragments.HomeFragment1;
import com.iqonic.store.fragments.HomeFragment2;
import com.iqonic.store.fragments.ViewAllProductFragment;
import com.iqonic.store.fragments.WishListFragment;
import com.iqonic.store.utils.BroadcastReceiverExt;
import com.iqonic.store.utils.CircleImageView;
import com.iqonic.store.utils.Constants;
import com.iqonic.store.utils.extensions.AppExtensionsKt;
import com.iqonic.store.utils.extensions.ExtensionsKt;
import com.iqonic.store.utils.extensions.NetworkExtensionKt;
import com.iqonic.store.utils.extensions.ViewExtensionsKt;
import com.store.proshop.R;
import java.util.HashMap;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000J\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u000b\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\b\u0010\u0010\u001a\u00020\u0011H\u0002J\b\u0010\u0012\u001a\u00020\u0011H\u0002J\b\u0010\u0013\u001a\u00020\u0011H\u0002J\b\u0010\u0014\u001a\u00020\u0011H\u0002J\u0010\u0010\u0015\u001a\u00020\u00112\u0006\u0010\u0016\u001a\u00020\u0006H\u0002J\b\u0010\u0017\u001a\u00020\u0011H\u0002J\u0006\u0010\u0018\u001a\u00020\u0011J\"\u0010\u0019\u001a\u00020\u00112\u0006\u0010\u001a\u001a\u00020\u000e2\u0006\u0010\u001b\u001a\u00020\u000e2\b\u0010\u001c\u001a\u0004\u0018\u00010\u001dH\u0014J\b\u0010\u001e\u001a\u00020\u0011H\u0016J\u0012\u0010\u001f\u001a\u00020\u00112\b\u0010 \u001a\u0004\u0018\u00010!H\u0015J\b\u0010\"\u001a\u00020\u0011H\u0014J\b\u0010#\u001a\u00020\u0011H\u0002J\b\u0010$\u001a\u00020\u0011H\u0002J\b\u0010%\u001a\u00020\u0011H\u0002J\b\u0010&\u001a\u00020\u0011H\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X.¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\bX\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\fX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\u000f\u001a\u0004\u0018\u00010\u0006X\u000e¢\u0006\u0002\n\u0000¨\u0006'"}, d2 = {"Lcom/iqonic/store/activity/DashBoardActivity;", "Lcom/iqonic/store/AppBaseActivity;", "()V", "count", "", "mHomeFragment", "Landroidx/fragment/app/Fragment;", "mModeFlag", "", "mViewAllProductFragment", "Lcom/iqonic/store/fragments/ViewAllProductFragment;", "mWishListFragment", "Lcom/iqonic/store/fragments/WishListFragment;", "selectedDashboard", "", "selectedFragment", "cartCount", "", "changeColor", "changeProfile", "closeDrawer", "loadFragment", "aFragment", "loadHomeFragment", "loadWishlistFragment", "onActivityResult", "requestCode", "resultCode", "data", "Landroid/content/Intent;", "onBackPressed", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onResume", "setCartCountFromPref", "setListener", "setUpDrawerToggle", "setUserInfo", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: DashBoardActivity.kt */
public final class DashBoardActivity extends AppBaseActivity {
    private HashMap _$_findViewCache;
    private String count = "";
    /* access modifiers changed from: private */
    public Fragment mHomeFragment;
    /* access modifiers changed from: private */
    public boolean mModeFlag;
    private final ViewAllProductFragment mViewAllProductFragment = new ViewAllProductFragment();
    private final WishListFragment mWishListFragment = new WishListFragment();
    private int selectedDashboard;
    private Fragment selectedFragment;

    public void _$_clearFindViewByIdCache() {
        HashMap hashMap = this._$_findViewCache;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    public View _$_findCachedViewById(int i) {
        if (this._$_findViewCache == null) {
            this._$_findViewCache = new HashMap();
        }
        View view = (View) this._$_findViewCache.get(Integer.valueOf(i));
        if (view != null) {
            return view;
        }
        View findViewById = findViewById(i);
        this._$_findViewCache.put(Integer.valueOf(i), findViewById);
        return findViewById;
    }

    public static final /* synthetic */ Fragment access$getMHomeFragment$p(DashBoardActivity dashBoardActivity) {
        Fragment fragment = dashBoardActivity.mHomeFragment;
        if (fragment == null) {
            Intrinsics.throwUninitializedPropertyAccessException("mHomeFragment");
        }
        return fragment;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_dashboard);
        this.mModeFlag = ShopHopApp.Companion.getAppTheme() == 1;
        int intValue = AppExtensionsKt.getSharedPrefInstance().getIntValue(Constants.SharedPref.KEY_DASHBOARD, 0);
        this.selectedDashboard = intValue;
        if (intValue == 0) {
            this.mHomeFragment = new HomeFragment1();
        } else if (intValue == 1) {
            this.mHomeFragment = new HomeFragment2();
        }
        Toolbar toolbar = (Toolbar) _$_findCachedViewById(com.iqonic.store.R.id.toolbar);
        Intrinsics.checkExpressionValueIsNotNull(toolbar, "toolbar");
        setToolbar(toolbar);
        setUpDrawerToggle();
        setListener();
        mAppBarColor();
        changeColor();
        if (AppExtensionsKt.isLoggedIn()) {
            cartCount();
            setCartCountFromPref();
        }
        setUserInfo();
        TextView textView = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvHome);
        textView.setOnClickListener(new DashBoardActivity$onCreate$$inlined$onClick$1(textView, this));
        new BroadcastReceiverExt(this, new DashBoardActivity$onCreate$2(this));
        TextView textView2 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvWishlist);
        textView2.setOnClickListener(new DashBoardActivity$onCreate$$inlined$onClick$2(textView2, this));
        TextView textView3 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvCategories);
        textView3.setOnClickListener(new DashBoardActivity$onCreate$$inlined$onClick$3(textView3, this));
        TextView textView4 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvCart);
        textView4.setOnClickListener(new DashBoardActivity$onCreate$$inlined$onClick$4(textView4, this));
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (i2 != -1) {
            return;
        }
        if (i == 203) {
            loadWishlistFragment();
        } else if (i == 211) {
            setUserInfo();
        }
    }

    /* access modifiers changed from: private */
    public final void setUserInfo() {
        TextView textView = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.txtDisplayName);
        Intrinsics.checkExpressionValueIsNotNull(textView, "txtDisplayName");
        textView.setText(AppExtensionsKt.getDisplayName());
        TextView textView2 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.txtDisplayEmail);
        Intrinsics.checkExpressionValueIsNotNull(textView2, "txtDisplayEmail");
        textView2.setText(AppExtensionsKt.getEmail());
        if (AppExtensionsKt.isLoggedIn()) {
            TextView textView3 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvSignIn);
            Intrinsics.checkExpressionValueIsNotNull(textView3, "tvSignIn");
            textView3.setText(getString(R.string.btn_sign_out));
            TextView textView4 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvCart);
            Intrinsics.checkExpressionValueIsNotNull(textView4, "tvCart");
            ViewExtensionsKt.show(textView4);
            TextView textView5 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvWishlist);
            Intrinsics.checkExpressionValueIsNotNull(textView5, "tvWishlist");
            ViewExtensionsKt.show(textView5);
            TextView textView6 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvOrder);
            Intrinsics.checkExpressionValueIsNotNull(textView6, "tvOrder");
            ViewExtensionsKt.show(textView6);
            TextView textView7 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvChangePwd);
            Intrinsics.checkExpressionValueIsNotNull(textView7, "tvChangePwd");
            ViewExtensionsKt.show(textView7);
            RelativeLayout relativeLayout = (RelativeLayout) _$_findCachedViewById(com.iqonic.store.R.id.rlProfile);
            Intrinsics.checkExpressionValueIsNotNull(relativeLayout, "rlProfile");
            ViewExtensionsKt.show(relativeLayout);
        } else {
            TextView textView8 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvSignIn);
            Intrinsics.checkExpressionValueIsNotNull(textView8, "tvSignIn");
            textView8.setText(getString(R.string.lbl_sign_in_link));
            TextView textView9 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvCart);
            Intrinsics.checkExpressionValueIsNotNull(textView9, "tvCart");
            ViewExtensionsKt.hide(textView9);
            TextView textView10 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvWishlist);
            Intrinsics.checkExpressionValueIsNotNull(textView10, "tvWishlist");
            ViewExtensionsKt.hide(textView10);
            TextView textView11 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvOrder);
            Intrinsics.checkExpressionValueIsNotNull(textView11, "tvOrder");
            ViewExtensionsKt.hide(textView11);
            TextView textView12 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvChangePwd);
            Intrinsics.checkExpressionValueIsNotNull(textView12, "tvChangePwd");
            ViewExtensionsKt.hide(textView12);
            RelativeLayout relativeLayout2 = (RelativeLayout) _$_findCachedViewById(com.iqonic.store.R.id.rlProfile);
            Intrinsics.checkExpressionValueIsNotNull(relativeLayout2, "rlProfile");
            ViewExtensionsKt.hide(relativeLayout2);
        }
        if (this.mModeFlag) {
            TextView textView13 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvMode);
            Intrinsics.checkExpressionValueIsNotNull(textView13, "tvMode");
            textView13.setText(getString(R.string.lbl_light_mode));
            return;
        }
        TextView textView14 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvMode);
        Intrinsics.checkExpressionValueIsNotNull(textView14, "tvMode");
        textView14.setText(getString(R.string.lbl_night_mode));
    }

    /* access modifiers changed from: private */
    public final void closeDrawer() {
        if (((DrawerLayout) _$_findCachedViewById(com.iqonic.store.R.id.drawerLayout)).isDrawerOpen((View) (LinearLayout) _$_findCachedViewById(com.iqonic.store.R.id.llLeftDrawer))) {
            ((DrawerLayout) _$_findCachedViewById(com.iqonic.store.R.id.drawerLayout)).closeDrawer((View) (LinearLayout) _$_findCachedViewById(com.iqonic.store.R.id.llLeftDrawer));
        }
    }

    private final void setUpDrawerToggle() {
        DashBoardActivity$setUpDrawerToggle$toggle$1 dashBoardActivity$setUpDrawerToggle$toggle$1 = new DashBoardActivity$setUpDrawerToggle$toggle$1(this, this, (DrawerLayout) _$_findCachedViewById(com.iqonic.store.R.id.drawerLayout), R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        ((DrawerLayout) _$_findCachedViewById(com.iqonic.store.R.id.drawerLayout)).setScrimColor(0);
        DrawerLayout drawerLayout = (DrawerLayout) _$_findCachedViewById(com.iqonic.store.R.id.drawerLayout);
        Intrinsics.checkExpressionValueIsNotNull(drawerLayout, "drawerLayout");
        drawerLayout.setDrawerElevation(0.0f);
        dashBoardActivity$setUpDrawerToggle$toggle$1.setDrawerIndicatorEnabled(false);
        ((Toolbar) _$_findCachedViewById(com.iqonic.store.R.id.toolbar)).setNavigationIcon((int) R.drawable.ic_drawer);
        Toolbar toolbar = (Toolbar) _$_findCachedViewById(com.iqonic.store.R.id.toolbar);
        Intrinsics.checkExpressionValueIsNotNull(toolbar, "toolbar");
        Drawable navigationIcon = toolbar.getNavigationIcon();
        if (navigationIcon == null) {
            Intrinsics.throwNpe();
        }
        navigationIcon.setColorFilter(Color.parseColor(AppExtensionsKt.getTextTitleColor()), PorterDuff.Mode.SRC_ATOP);
        ((Toolbar) _$_findCachedViewById(com.iqonic.store.R.id.toolbar)).setNavigationOnClickListener(new DashBoardActivity$setUpDrawerToggle$1(this));
        ((DrawerLayout) _$_findCachedViewById(com.iqonic.store.R.id.drawerLayout)).addDrawerListener(dashBoardActivity$setUpDrawerToggle$toggle$1);
        dashBoardActivity$setUpDrawerToggle$toggle$1.syncState();
    }

    private final void setListener() {
        loadHomeFragment();
        RelativeLayout relativeLayout = (RelativeLayout) _$_findCachedViewById(com.iqonic.store.R.id.rlProfile);
        relativeLayout.setOnClickListener(new DashBoardActivity$setListener$$inlined$onClick$1(relativeLayout, this));
        TextView textView = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvOrder);
        textView.setOnClickListener(new DashBoardActivity$setListener$$inlined$onClick$2(textView, this));
        TextView textView2 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvAbout);
        textView2.setOnClickListener(new DashBoardActivity$setListener$$inlined$onClick$3(textView2, this));
        TextView textView3 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvChangePwd);
        textView3.setOnClickListener(new DashBoardActivity$setListener$$inlined$onClick$4(textView3, this));
        TextView textView4 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvSignIn);
        textView4.setOnClickListener(new DashBoardActivity$setListener$$inlined$onClick$5(textView4, this));
        TextView textView5 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvMode);
        textView5.setOnClickListener(new DashBoardActivity$setListener$$inlined$onClick$6(textView5, this));
    }

    /* access modifiers changed from: private */
    public final void loadFragment(Fragment fragment) {
        Fragment fragment2 = this.selectedFragment;
        if (fragment2 != null) {
            if (!Intrinsics.areEqual((Object) fragment2, (Object) fragment)) {
                Fragment fragment3 = this.selectedFragment;
                if (fragment3 == null) {
                    Intrinsics.throwNpe();
                }
                ExtensionsKt.hideFragment(this, fragment3);
            } else {
                return;
            }
        }
        if (fragment.isAdded()) {
            ExtensionsKt.showFragment(this, fragment);
        } else {
            ExtensionsKt.addFragment(this, fragment, R.id.container);
        }
        this.selectedFragment = fragment;
    }

    private final void loadHomeFragment() {
        Fragment fragment = this.mHomeFragment;
        if (fragment == null) {
            Intrinsics.throwUninitializedPropertyAccessException("mHomeFragment");
        }
        loadFragment(fragment);
        setTitle(getString(R.string.home));
        Fragment fragment2 = this.mHomeFragment;
        if (fragment2 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("mHomeFragment");
        }
        if (fragment2 instanceof HomeFragment1) {
            Fragment fragment3 = this.mHomeFragment;
            if (fragment3 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("mHomeFragment");
            }
            if (fragment3 != null) {
                HomeFragment1 homeFragment1 = (HomeFragment1) fragment3;
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type com.iqonic.store.fragments.HomeFragment1");
        }
        Fragment fragment4 = this.mHomeFragment;
        if (fragment4 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("mHomeFragment");
        }
        if (fragment4 instanceof HomeFragment2) {
            Fragment fragment5 = this.mHomeFragment;
            if (fragment5 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("mHomeFragment");
            }
            if (fragment5 != null) {
                HomeFragment2 homeFragment2 = (HomeFragment2) fragment5;
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type com.iqonic.store.fragments.HomeFragment2");
        }
    }

    public final void loadWishlistFragment() {
        loadFragment(this.mWishListFragment);
        setTitle(getString(R.string.lbl_wish_list));
    }

    public void onBackPressed() {
        if (((DrawerLayout) _$_findCachedViewById(com.iqonic.store.R.id.drawerLayout)).isDrawerOpen((int) GravityCompat.START)) {
            ((DrawerLayout) _$_findCachedViewById(com.iqonic.store.R.id.drawerLayout)).closeDrawer((int) GravityCompat.START);
            return;
        }
        Fragment fragment = this.mHomeFragment;
        if (fragment == null) {
            Intrinsics.throwUninitializedPropertyAccessException("mHomeFragment");
        }
        if (!fragment.isVisible()) {
            loadHomeFragment();
        } else if (this.mViewAllProductFragment.isVisible()) {
            loadHomeFragment();
        } else {
            super.onBackPressed();
        }
    }

    private final void cartCount() {
        setCartCountFromPref();
        AppExtensionsKt.sendCartCountChangeBroadcast(this);
    }

    /* access modifiers changed from: private */
    public final void setCartCountFromPref() {
        this.count = AppExtensionsKt.getCartCount();
        Fragment fragment = this.mHomeFragment;
        if (fragment == null) {
            Intrinsics.throwUninitializedPropertyAccessException("mHomeFragment");
        }
        if (fragment instanceof HomeFragment1) {
            Fragment fragment2 = this.mHomeFragment;
            if (fragment2 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("mHomeFragment");
            }
            if (fragment2 != null) {
                HomeFragment1 homeFragment1 = (HomeFragment1) fragment2;
                Fragment fragment3 = this.mHomeFragment;
                if (fragment3 == null) {
                    Intrinsics.throwUninitializedPropertyAccessException("mHomeFragment");
                }
                if (fragment3 != null) {
                    ((HomeFragment1) fragment3).setCartCount();
                    return;
                }
                throw new TypeCastException("null cannot be cast to non-null type com.iqonic.store.fragments.HomeFragment1");
            }
            throw new TypeCastException("null cannot be cast to non-null type com.iqonic.store.fragments.HomeFragment1");
        }
        Fragment fragment4 = this.mHomeFragment;
        if (fragment4 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("mHomeFragment");
        }
        if (fragment4 instanceof HomeFragment2) {
            Fragment fragment5 = this.mHomeFragment;
            if (fragment5 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("mHomeFragment");
            }
            if (fragment5 != null) {
                HomeFragment2 homeFragment2 = (HomeFragment2) fragment5;
                Fragment fragment6 = this.mHomeFragment;
                if (fragment6 == null) {
                    Intrinsics.throwUninitializedPropertyAccessException("mHomeFragment");
                }
                if (fragment6 != null) {
                    ((HomeFragment2) fragment6).setCartCount();
                    return;
                }
                throw new TypeCastException("null cannot be cast to non-null type com.iqonic.store.fragments.HomeFragment2");
            }
            throw new TypeCastException("null cannot be cast to non-null type com.iqonic.store.fragments.HomeFragment2");
        }
    }

    /* access modifiers changed from: private */
    public final void changeProfile() {
        if (AppExtensionsKt.isLoggedIn()) {
            CircleImageView circleImageView = (CircleImageView) _$_findCachedViewById(com.iqonic.store.R.id.civProfile);
            Intrinsics.checkExpressionValueIsNotNull(circleImageView, "civProfile");
            NetworkExtensionKt.loadImageFromUrl$default(circleImageView, AppExtensionsKt.getUserProfile(), R.drawable.ic_profile, 0, 4, (Object) null);
        }
    }

    private final void changeColor() {
        TextView textView = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.txtDisplayName);
        Intrinsics.checkExpressionValueIsNotNull(textView, "txtDisplayName");
        ExtensionsKt.changeTextPrimaryColor(textView);
        TextView textView2 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.txtDisplayEmail);
        Intrinsics.checkExpressionValueIsNotNull(textView2, "txtDisplayEmail");
        ExtensionsKt.changeTextPrimaryColor(textView2);
        TextView textView3 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvHome);
        Intrinsics.checkExpressionValueIsNotNull(textView3, "tvHome");
        ExtensionsKt.changeTextPrimaryColor(textView3);
        TextView textView4 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvCart);
        Intrinsics.checkExpressionValueIsNotNull(textView4, "tvCart");
        ExtensionsKt.changeTextPrimaryColor(textView4);
        TextView textView5 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvWishlist);
        Intrinsics.checkExpressionValueIsNotNull(textView5, "tvWishlist");
        ExtensionsKt.changeTextPrimaryColor(textView5);
        TextView textView6 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvCategories);
        Intrinsics.checkExpressionValueIsNotNull(textView6, "tvCategories");
        ExtensionsKt.changeTextPrimaryColor(textView6);
        TextView textView7 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvOrder);
        Intrinsics.checkExpressionValueIsNotNull(textView7, "tvOrder");
        ExtensionsKt.changeTextPrimaryColor(textView7);
        TextView textView8 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvChangePwd);
        Intrinsics.checkExpressionValueIsNotNull(textView8, "tvChangePwd");
        ExtensionsKt.changeTextPrimaryColor(textView8);
        TextView textView9 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvMode);
        Intrinsics.checkExpressionValueIsNotNull(textView9, "tvMode");
        ExtensionsKt.changeTextPrimaryColor(textView9);
        TextView textView10 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvSignIn);
        Intrinsics.checkExpressionValueIsNotNull(textView10, "tvSignIn");
        ExtensionsKt.changeTextPrimaryColor(textView10);
        TextView textView11 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvAbout);
        Intrinsics.checkExpressionValueIsNotNull(textView11, "tvAbout");
        ExtensionsKt.changeTextPrimaryColor(textView11);
        TextView textView12 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvHome);
        Intrinsics.checkExpressionValueIsNotNull(textView12, "tvHome");
        ExtensionsKt.setTextViewDrawableColor(textView12, AppExtensionsKt.getTextPrimaryColor());
        TextView textView13 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvCart);
        Intrinsics.checkExpressionValueIsNotNull(textView13, "tvCart");
        ExtensionsKt.setTextViewDrawableColor(textView13, AppExtensionsKt.getTextPrimaryColor());
        TextView textView14 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvWishlist);
        Intrinsics.checkExpressionValueIsNotNull(textView14, "tvWishlist");
        ExtensionsKt.setTextViewDrawableColor(textView14, AppExtensionsKt.getTextPrimaryColor());
        TextView textView15 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvCategories);
        Intrinsics.checkExpressionValueIsNotNull(textView15, "tvCategories");
        ExtensionsKt.setTextViewDrawableColor(textView15, AppExtensionsKt.getTextPrimaryColor());
        TextView textView16 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvOrder);
        Intrinsics.checkExpressionValueIsNotNull(textView16, "tvOrder");
        ExtensionsKt.setTextViewDrawableColor(textView16, AppExtensionsKt.getTextPrimaryColor());
        TextView textView17 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvChangePwd);
        Intrinsics.checkExpressionValueIsNotNull(textView17, "tvChangePwd");
        ExtensionsKt.setTextViewDrawableColor(textView17, AppExtensionsKt.getTextPrimaryColor());
        TextView textView18 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvMode);
        Intrinsics.checkExpressionValueIsNotNull(textView18, "tvMode");
        ExtensionsKt.setTextViewDrawableColor(textView18, AppExtensionsKt.getTextPrimaryColor());
        TextView textView19 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvSignIn);
        Intrinsics.checkExpressionValueIsNotNull(textView19, "tvSignIn");
        ExtensionsKt.setTextViewDrawableColor(textView19, AppExtensionsKt.getTextPrimaryColor());
        TextView textView20 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvAbout);
        Intrinsics.checkExpressionValueIsNotNull(textView20, "tvAbout");
        ExtensionsKt.setTextViewDrawableColor(textView20, AppExtensionsKt.getTextPrimaryColor());
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (this.selectedDashboard != AppExtensionsKt.getSharedPrefInstance().getIntValue(Constants.SharedPref.KEY_DASHBOARD, 0)) {
            recreate();
        }
    }
}
