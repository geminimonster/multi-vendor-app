package com.iqonic.store.activity

import android.os.Bundle
import com.iqonic.store.AppBaseActivity
import com.iqonic.store.R
import com.iqonic.store.models.CreateOrderNotes
import com.iqonic.store.models.CreateOrderResponse
import com.iqonic.store.utils.Constants
import com.iqonic.store.utils.extensions.convertToLocalDate
import com.iqonic.store.utils.extensions.currencyFormat
import com.iqonic.store.utils.extensions.getRestApiImpl

class PaymentSuccessfullyActivity : AppBaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_success_transaction)

        val response: CreateOrderResponse =
            intent.getSerializableExtra(Constants.KeyIntent.ORDER_DATA)

        amount.text = response.total.toString().currencyFormat()
        transactionId.text = response.transaction_id
        orderId.text = response.order_key
        paidVia.text = response.payment_method
        transactionDate.text = convertToLocalDate(response.date_created)
        imgBack.onClick {
            finish()
        }
        tvDone.onClick { finish() }

        /**
         * Place Order Notes
         *
         */
        showProgress(true)
        var notes = CreateOrderNotes()
        notes.customer_note = true
        notes.note = "{\n" +
                "\"status\":\"Ordered\",\n" +
                "\"message\":\"Your order has been placed.\"\n" +
                "} "

        getRestApiImpl().addOrderNotes(response.id, request = notes, onApiSuccess = {
            showProgress(false)
        }, onApiError = {
            showProgress(false)
        })

    }

}
