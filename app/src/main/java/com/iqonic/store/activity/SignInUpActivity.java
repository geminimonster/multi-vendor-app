package com.iqonic.store.activity;

import android.os.Bundle;
import android.view.View;
import com.iqonic.store.AppBaseActivity;
import com.iqonic.store.fragments.SignInFragment;
import com.iqonic.store.fragments.SignUpFragment;
import com.iqonic.store.utils.extensions.ExtensionsKt;
import com.iqonic.store.utils.extensions.ViewExtensionsKt;
import com.store.proshop.R;
import java.util.HashMap;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u0006\u0010\u0007\u001a\u00020\bJ\u0006\u0010\t\u001a\u00020\bJ\b\u0010\n\u001a\u00020\bH\u0016J\u0012\u0010\u000b\u001a\u00020\b2\b\u0010\f\u001a\u0004\u0018\u00010\rH\u0014R\u000e\u0010\u0003\u001a\u00020\u0004X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0004¢\u0006\u0002\n\u0000¨\u0006\u000e"}, d2 = {"Lcom/iqonic/store/activity/SignInUpActivity;", "Lcom/iqonic/store/AppBaseActivity;", "()V", "mSignInFragment", "Lcom/iqonic/store/fragments/SignInFragment;", "mSignUpFragment", "Lcom/iqonic/store/fragments/SignUpFragment;", "loadSignInFragment", "", "loadSignUpFragment", "onBackPressed", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: SignInUpActivity.kt */
public final class SignInUpActivity extends AppBaseActivity {
    private HashMap _$_findViewCache;
    private final SignInFragment mSignInFragment = new SignInFragment();
    private final SignUpFragment mSignUpFragment = new SignUpFragment();

    public void _$_clearFindViewByIdCache() {
        HashMap hashMap = this._$_findViewCache;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    public View _$_findCachedViewById(int i) {
        if (this._$_findViewCache == null) {
            this._$_findViewCache = new HashMap();
        }
        View view = (View) this._$_findViewCache.get(Integer.valueOf(i));
        if (view != null) {
            return view;
        }
        View findViewById = findViewById(i);
        this._$_findViewCache.put(Integer.valueOf(i), findViewById);
        return findViewById;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_sign_in_up);
        loadSignInFragment();
    }

    public final void loadSignUpFragment() {
        if (this.mSignUpFragment.isAdded()) {
            ExtensionsKt.replaceFragment(this, this.mSignUpFragment, R.id.fragmentContainer);
            View findViewById = findViewById(R.id.fragmentContainer);
            Intrinsics.checkExpressionValueIsNotNull(findViewById, "findViewById<FrameLayout>(R.id.fragmentContainer)");
            ViewExtensionsKt.fadeIn(findViewById, 500);
            return;
        }
        ExtensionsKt.addFragment(this, this.mSignUpFragment, R.id.fragmentContainer);
    }

    public final void loadSignInFragment() {
        if (this.mSignInFragment.isAdded()) {
            ExtensionsKt.replaceFragment(this, this.mSignInFragment, R.id.fragmentContainer);
            View findViewById = findViewById(R.id.fragmentContainer);
            Intrinsics.checkExpressionValueIsNotNull(findViewById, "findViewById<FrameLayout>(R.id.fragmentContainer)");
            ViewExtensionsKt.fadeIn(findViewById, 500);
            return;
        }
        ExtensionsKt.addFragment(this, this.mSignInFragment, R.id.fragmentContainer);
    }

    public void onBackPressed() {
        if (this.mSignUpFragment.isVisible()) {
            ExtensionsKt.removeFragment(this, this.mSignUpFragment);
        } else {
            super.onBackPressed();
        }
    }
}
