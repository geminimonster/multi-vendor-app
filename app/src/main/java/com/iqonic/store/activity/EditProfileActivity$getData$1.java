package com.iqonic.store.activity;

import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import com.google.gson.Gson;
import com.iqonic.store.R;
import com.iqonic.store.models.CountryModel;
import com.iqonic.store.models.CustomerData;
import com.iqonic.store.models.State;
import com.iqonic.store.utils.Constants;
import com.iqonic.store.utils.SharedPrefUtils;
import com.iqonic.store.utils.extensions.AppExtensionsKt;
import com.iqonic.store.utils.extensions.StringExtensionsKt;
import com.iqonic.store.utils.extensions.ViewExtensionsKt;
import java.util.ArrayList;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.collections.CollectionsKt;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n¢\u0006\u0002\b\u0004"}, d2 = {"<anonymous>", "", "it", "Lcom/iqonic/store/models/CustomerData;", "invoke"}, k = 3, mv = {1, 1, 16})
/* compiled from: EditProfileActivity.kt */
final class EditProfileActivity$getData$1 extends Lambda implements Function1<CustomerData, Unit> {
    final /* synthetic */ EditProfileActivity this$0;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    EditProfileActivity$getData$1(EditProfileActivity editProfileActivity) {
        super(1);
        this.this$0 = editProfileActivity;
    }

    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        invoke((CustomerData) obj);
        return Unit.INSTANCE;
    }

    public final void invoke(final CustomerData customerData) {
        Intrinsics.checkParameterIsNotNull(customerData, "it");
        boolean z = false;
        this.this$0.showProgress(false);
        AppExtensionsKt.getSharedPrefInstance().setValue(Constants.SharedPref.SHOW_SWIPE, true);
        ((EditText) this.this$0._$_findCachedViewById(R.id.edtFirstName)).setText(SharedPrefUtils.getStringValue$default(AppExtensionsKt.getSharedPrefInstance(), Constants.SharedPref.USER_FIRST_NAME, (String) null, 2, (Object) null));
        ((EditText) this.this$0._$_findCachedViewById(R.id.edtLastName)).setText(SharedPrefUtils.getStringValue$default(AppExtensionsKt.getSharedPrefInstance(), Constants.SharedPref.USER_LAST_NAME, (String) null, 2, (Object) null));
        if (customerData.getFirst_name().length() > 0) {
            AppExtensionsKt.getSharedPrefInstance().setValue(Constants.SharedPref.USER_LAST_NAME, customerData.getFirst_name());
        }
        if (customerData.getLast_name().length() > 0) {
            z = true;
        }
        if (z) {
            AppExtensionsKt.getSharedPrefInstance().setValue(Constants.SharedPref.USER_LAST_NAME, customerData.getLast_name());
        }
        AppExtensionsKt.getSharedPrefInstance().setValue("user_username", customerData.getRole());
        AppExtensionsKt.getSharedPrefInstance().setValue(Constants.SharedPref.BILLING, new Gson().toJson((Object) customerData.getBilling()));
        AppExtensionsKt.getSharedPrefInstance().setValue(Constants.SharedPref.SHIPPING, new Gson().toJson((Object) customerData.getShipping()));
        AppExtensionsKt.getSharedPrefInstance().setValue(Constants.SharedPref.USER_PICODE, customerData.getShipping().getPostcode());
        TextView textView = (TextView) this.this$0._$_findCachedViewById(R.id.tvUserName);
        Intrinsics.checkExpressionValueIsNotNull(textView, "tvUserName");
        textView.setText(customerData.getUsername());
        TextView textView2 = (TextView) this.this$0._$_findCachedViewById(R.id.tvEmail);
        Intrinsics.checkExpressionValueIsNotNull(textView2, "tvEmail");
        textView2.setText(AppExtensionsKt.getEmail());
        ((EditText) this.this$0._$_findCachedViewById(R.id.edtEmail)).setText(AppExtensionsKt.getEmail());
        EditText editText = (EditText) this.this$0._$_findCachedViewById(R.id.edtFirstName);
        Intrinsics.checkExpressionValueIsNotNull(editText, "edtFirstName");
        ((EditText) this.this$0._$_findCachedViewById(R.id.edtFirstName)).setSelection(editText.getText().length());
        ((EditText) this.this$0._$_findCachedViewById(R.id.edtBillingFName)).setText(customerData.getBilling().getFirst_name());
        ((EditText) this.this$0._$_findCachedViewById(R.id.edtBillingLName)).setText(customerData.getBilling().getLast_name());
        ((EditText) this.this$0._$_findCachedViewById(R.id.edtBillingAdd1)).setText(customerData.getBilling().getAddress_1());
        ((EditText) this.this$0._$_findCachedViewById(R.id.edtBillingAdd2)).setText(customerData.getBilling().getAddress_2());
        ((EditText) this.this$0._$_findCachedViewById(R.id.edtBillingCompany)).setText(customerData.getBilling().getCompany());
        ((EditText) this.this$0._$_findCachedViewById(R.id.edtBillingCity)).setText(customerData.getBilling().getCity());
        ((EditText) this.this$0._$_findCachedViewById(R.id.edtBillingPinCode)).setText(customerData.getBilling().getPostcode());
        ((EditText) this.this$0._$_findCachedViewById(R.id.edtBillingPhone)).setText(customerData.getBilling().getPhone());
        ((EditText) this.this$0._$_findCachedViewById(R.id.edtBillingEmail)).setText(customerData.getBilling().getEmail());
        ((EditText) this.this$0._$_findCachedViewById(R.id.edtShippingFName)).setText(customerData.getShipping().getFirst_name());
        ((EditText) this.this$0._$_findCachedViewById(R.id.edtShippingLName)).setText(customerData.getShipping().getLast_name());
        ((EditText) this.this$0._$_findCachedViewById(R.id.edtShippingAdd1)).setText(customerData.getShipping().getAddress_1());
        ((EditText) this.this$0._$_findCachedViewById(R.id.edtShippingCompany)).setText(customerData.getShipping().getCompany());
        ((EditText) this.this$0._$_findCachedViewById(R.id.edtShippingAdd2)).setText(customerData.getShipping().getAddress_2());
        ((EditText) this.this$0._$_findCachedViewById(R.id.edtShippingCity)).setText(customerData.getShipping().getCity());
        ((EditText) this.this$0._$_findCachedViewById(R.id.edtShippingPinCode)).setText(customerData.getShipping().getPostcode());
        AppExtensionsKt.fetchCountry(new Function1<ArrayList<CountryModel>, Unit>(this) {
            final /* synthetic */ EditProfileActivity$getData$1 this$0;

            {
                this.this$0 = r1;
            }

            public /* bridge */ /* synthetic */ Object invoke(Object obj) {
                invoke((ArrayList<CountryModel>) (ArrayList) obj);
                return Unit.INSTANCE;
            }

            public final void invoke(ArrayList<CountryModel> arrayList) {
                Intrinsics.checkParameterIsNotNull(arrayList, "its");
                this.this$0.this$0.getCountryList().clear();
                this.this$0.this$0.getCountryList().addAll(arrayList);
                this.this$0.this$0.mShippingCountryList.clear();
                this.this$0.this$0.mBillingCountryList.clear();
                this.this$0.this$0.mBillingStateList.clear();
                this.this$0.this$0.mShippingStateList.clear();
                int i = 0;
                int i2 = 0;
                int i3 = 0;
                int i4 = 0;
                for (Object next : arrayList) {
                    int i5 = i3 + 1;
                    if (i3 < 0) {
                        CollectionsKt.throwIndexOverflow();
                    }
                    CountryModel countryModel = (CountryModel) next;
                    this.this$0.this$0.mShippingCountryList.add(StringExtensionsKt.getHtmlString(countryModel.getName()).toString());
                    this.this$0.this$0.mBillingCountryList.add(StringExtensionsKt.getHtmlString(countryModel.getName()).toString());
                    if (Intrinsics.areEqual((Object) customerData.getBilling().getCountry(), (Object) countryModel.getName())) {
                        i2 = i3;
                    }
                    if (Intrinsics.areEqual((Object) customerData.getShipping().getCountry(), (Object) countryModel.getName())) {
                        i4 = i3;
                    }
                    i3 = i5;
                }
                int i6 = 0;
                int i7 = 0;
                for (Object next2 : this.this$0.this$0.getCountryList().get(i2).getStates()) {
                    int i8 = i6 + 1;
                    if (i6 < 0) {
                        CollectionsKt.throwIndexOverflow();
                    }
                    State state = (State) next2;
                    if (Intrinsics.areEqual((Object) customerData.getBilling().getState(), (Object) state.getName())) {
                        i7 = i6;
                    }
                    this.this$0.this$0.mBillingStateList.add(state.getName());
                    i6 = i8;
                }
                int i9 = 0;
                for (Object next3 : this.this$0.this$0.getCountryList().get(i4).getStates()) {
                    int i10 = i + 1;
                    if (i < 0) {
                        CollectionsKt.throwIndexOverflow();
                    }
                    State state2 = (State) next3;
                    if (Intrinsics.areEqual((Object) customerData.getShipping().getState(), (Object) state2.getName())) {
                        i9 = i;
                    }
                    this.this$0.this$0.mShippingStateList.add(state2.getName());
                    i = i10;
                }
                ArrayAdapter access$getMBillingCountryAdapter$p = this.this$0.this$0.mBillingCountryAdapter;
                if (access$getMBillingCountryAdapter$p != null) {
                    access$getMBillingCountryAdapter$p.notifyDataSetChanged();
                }
                ArrayAdapter access$getMShippingCountryAdapter$p = this.this$0.this$0.mShippingCountryAdapter;
                if (access$getMShippingCountryAdapter$p != null) {
                    access$getMShippingCountryAdapter$p.notifyDataSetChanged();
                }
                ArrayAdapter access$getMBillingStateAdapter$p = this.this$0.this$0.mBillingStateAdapter;
                if (access$getMBillingStateAdapter$p != null) {
                    access$getMBillingStateAdapter$p.notifyDataSetChanged();
                }
                ArrayAdapter access$getMShippingStateAdapter$p = this.this$0.this$0.mShippingStateAdapter;
                if (access$getMShippingStateAdapter$p != null) {
                    access$getMShippingStateAdapter$p.notifyDataSetChanged();
                }
                ((Spinner) this.this$0.this$0._$_findCachedViewById(R.id.spBillingCountry)).setSelection(i2);
                ((Spinner) this.this$0.this$0._$_findCachedViewById(R.id.spBillingState)).setSelection(i7);
                ((Spinner) this.this$0.this$0._$_findCachedViewById(R.id.spCountry)).setSelection(i4);
                ((Spinner) this.this$0.this$0._$_findCachedViewById(R.id.spState)).setSelection(i9);
                if (this.this$0.this$0.mBillingStateList.isEmpty()) {
                    Spinner spinner = (Spinner) this.this$0.this$0._$_findCachedViewById(R.id.spBillingState);
                    Intrinsics.checkExpressionValueIsNotNull(spinner, "spBillingState");
                    ViewExtensionsKt.hide(spinner);
                }
                if (this.this$0.this$0.mShippingStateList.isEmpty()) {
                    Spinner spinner2 = (Spinner) this.this$0.this$0._$_findCachedViewById(R.id.spState);
                    Intrinsics.checkExpressionValueIsNotNull(spinner2, "spState");
                    ViewExtensionsKt.hide(spinner2);
                }
            }
        }, new Function1<String, Unit>(this) {
            final /* synthetic */ EditProfileActivity$getData$1 this$0;

            {
                this.this$0 = r1;
            }

            public /* bridge */ /* synthetic */ Object invoke(Object obj) {
                invoke((String) obj);
                return Unit.INSTANCE;
            }

            public final void invoke(String str) {
                Intrinsics.checkParameterIsNotNull(str, "it");
                this.this$0.this$0.showProgress(false);
            }
        });
    }
}
