package com.iqonic.store.activity;

import com.iqonic.store.utils.extensions.ExtensionsKt;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n¢\u0006\u0002\b\u0004"}, d2 = {"<anonymous>", "", "it", "", "invoke"}, k = 3, mv = {1, 1, 16})
/* compiled from: ProductDetailActivity2.kt */
final class ProductDetailActivity2$getProductDetail$2 extends Lambda implements Function1<String, Unit> {
    final /* synthetic */ ProductDetailActivity2 this$0;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    ProductDetailActivity2$getProductDetail$2(ProductDetailActivity2 productDetailActivity2) {
        super(1);
        this.this$0 = productDetailActivity2;
    }

    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        invoke((String) obj);
        return Unit.INSTANCE;
    }

    public final void invoke(String str) {
        Intrinsics.checkParameterIsNotNull(str, "it");
        this.this$0.showProgress(false);
        ExtensionsKt.snackBar$default(this.this$0, str, 0, 2, (Object) null);
    }
}
