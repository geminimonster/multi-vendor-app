package com.iqonic.store.activity;

import com.iqonic.store.adapter.BaseAdapter;
import com.iqonic.store.models.Coupons;
import java.util.ArrayList;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0016\u0010\u0002\u001a\u0012\u0012\u0004\u0012\u00020\u00040\u0003j\b\u0012\u0004\u0012\u00020\u0004`\u0005H\n¢\u0006\u0002\b\u0006"}, d2 = {"<anonymous>", "", "it", "Ljava/util/ArrayList;", "Lcom/iqonic/store/models/Coupons;", "Lkotlin/collections/ArrayList;", "invoke"}, k = 3, mv = {1, 1, 16})
/* compiled from: CouponActivity.kt */
final class CouponActivity$onCreate$2 extends Lambda implements Function1<ArrayList<Coupons>, Unit> {
    final /* synthetic */ CouponActivity this$0;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    CouponActivity$onCreate$2(CouponActivity couponActivity) {
        super(1);
        this.this$0 = couponActivity;
    }

    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        invoke((ArrayList<Coupons>) (ArrayList) obj);
        return Unit.INSTANCE;
    }

    public final void invoke(ArrayList<Coupons> arrayList) {
        Intrinsics.checkParameterIsNotNull(arrayList, "it");
        BaseAdapter access$getCouponsAdapter$p = this.this$0.couponsAdapter;
        if (access$getCouponsAdapter$p == null) {
            Intrinsics.throwNpe();
        }
        access$getCouponsAdapter$p.clearItems();
        BaseAdapter access$getCouponsAdapter$p2 = this.this$0.couponsAdapter;
        if (access$getCouponsAdapter$p2 == null) {
            Intrinsics.throwNpe();
        }
        access$getCouponsAdapter$p2.addItems(arrayList);
        BaseAdapter access$getCouponsAdapter$p3 = this.this$0.couponsAdapter;
        if (access$getCouponsAdapter$p3 == null) {
            Intrinsics.throwNpe();
        }
        access$getCouponsAdapter$p3.notifyDataSetChanged();
    }
}
