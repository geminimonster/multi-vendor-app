package com.iqonic.store.activity;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.iqonic.store.R;
import com.iqonic.store.models.Category;
import com.iqonic.store.utils.extensions.ExtensionsKt;
import com.iqonic.store.utils.extensions.NetworkExtensionKt;
import com.iqonic.store.utils.extensions.StringExtensionsKt;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function3;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0007H\n¢\u0006\u0002\b\b"}, d2 = {"<anonymous>", "", "view", "Landroid/view/View;", "model", "Lcom/iqonic/store/models/Category;", "<anonymous parameter 2>", "", "invoke"}, k = 3, mv = {1, 1, 16})
/* compiled from: SubCategoryActivity.kt */
final class SubCategoryActivity$mSubCategoryAdapter$1 extends Lambda implements Function3<View, Category, Integer, Unit> {
    final /* synthetic */ SubCategoryActivity this$0;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    SubCategoryActivity$mSubCategoryAdapter$1(SubCategoryActivity subCategoryActivity) {
        super(3);
        this.this$0 = subCategoryActivity;
    }

    public /* bridge */ /* synthetic */ Object invoke(Object obj, Object obj2, Object obj3) {
        invoke((View) obj, (Category) obj2, ((Number) obj3).intValue());
        return Unit.INSTANCE;
    }

    public final void invoke(View view, Category category, int i) {
        Intrinsics.checkParameterIsNotNull(view, "view");
        Intrinsics.checkParameterIsNotNull(category, "model");
        TextView textView = (TextView) view.findViewById(R.id.tvSubCategory);
        Intrinsics.checkExpressionValueIsNotNull(textView, "view.tvSubCategory");
        textView.setText(StringExtensionsKt.getHtmlString(category.getName()));
        if (category.getImage() != null) {
            if (category.getImage().getSrc().length() > 0) {
                ImageView imageView = (ImageView) view.findViewById(R.id.ivProducts);
                Intrinsics.checkExpressionValueIsNotNull(imageView, "view.ivProducts");
                NetworkExtensionKt.loadImageFromUrl$default(imageView, category.getImage().getSrc(), 0, 0, 6, (Object) null);
            }
        }
        TextView textView2 = (TextView) view.findViewById(R.id.tvSubCategory);
        Intrinsics.checkExpressionValueIsNotNull(textView2, "view.tvSubCategory");
        ExtensionsKt.changeTextSecondaryColor(textView2);
        view.setOnClickListener(new SubCategoryActivity$mSubCategoryAdapter$1$$special$$inlined$onClick$1(view, this, category));
    }
}
