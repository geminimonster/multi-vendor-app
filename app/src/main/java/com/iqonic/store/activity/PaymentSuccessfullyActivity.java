package com.iqonic.store.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.google.android.material.button.MaterialButton;
import com.iqonic.store.AppBaseActivity;
import com.iqonic.store.models.CreateOrderNotes;
import com.iqonic.store.models.CreateOrderResponse;
import com.iqonic.store.utils.BroadcastReceiverExt;
import com.iqonic.store.utils.Constants;
import com.iqonic.store.utils.extensions.AppExtensionsKt;
import com.iqonic.store.utils.extensions.ExtensionsKt;
import com.iqonic.store.utils.extensions.NetworkExtensionKt;
import com.iqonic.store.utils.extensions.StringExtensionsKt;
import com.store.proshop.R;
import java.io.Serializable;
import java.util.HashMap;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\b\u0010\u0003\u001a\u00020\u0004H\u0002J\u0012\u0010\u0005\u001a\u00020\u00042\b\u0010\u0006\u001a\u0004\u0018\u00010\u0007H\u0014¨\u0006\b"}, d2 = {"Lcom/iqonic/store/activity/PaymentSuccessfullyActivity;", "Lcom/iqonic/store/AppBaseActivity;", "()V", "changeColor", "", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: PaymentSuccessfullyActivity.kt */
public final class PaymentSuccessfullyActivity extends AppBaseActivity {
    private HashMap _$_findViewCache;

    public void _$_clearFindViewByIdCache() {
        HashMap hashMap = this._$_findViewCache;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    public View _$_findCachedViewById(int i) {
        if (this._$_findViewCache == null) {
            this._$_findViewCache = new HashMap();
        }
        View view = (View) this._$_findViewCache.get(Integer.valueOf(i));
        if (view != null) {
            return view;
        }
        View findViewById = findViewById(i);
        this._$_findViewCache.put(Integer.valueOf(i), findViewById);
        return findViewById;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_success_transaction);
        changeColor();
        Serializable serializableExtra = getIntent().getSerializableExtra(Constants.KeyIntent.ORDER_DATA);
        if (serializableExtra != null) {
            CreateOrderResponse createOrderResponse = (CreateOrderResponse) serializableExtra;
            TextView textView = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.amount);
            Intrinsics.checkExpressionValueIsNotNull(textView, "amount");
            textView.setText(StringExtensionsKt.currencyFormat$default(String.valueOf(createOrderResponse.getTotal()), (String) null, 1, (Object) null));
            TextView textView2 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.transactionId);
            Intrinsics.checkExpressionValueIsNotNull(textView2, "transactionId");
            textView2.setText(createOrderResponse.getTransaction_id());
            TextView textView3 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.orderId);
            Intrinsics.checkExpressionValueIsNotNull(textView3, "orderId");
            textView3.setText(createOrderResponse.getOrder_key());
            TextView textView4 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.paidVia);
            Intrinsics.checkExpressionValueIsNotNull(textView4, "paidVia");
            textView4.setText(createOrderResponse.getPayment_method());
            TextView textView5 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.transactionDate);
            Intrinsics.checkExpressionValueIsNotNull(textView5, "transactionDate");
            textView5.setText(AppExtensionsKt.convertToLocalDate(createOrderResponse.getDate_created()));
            ImageView imageView = (ImageView) _$_findCachedViewById(com.iqonic.store.R.id.imgBack);
            imageView.setOnClickListener(new PaymentSuccessfullyActivity$onCreate$$inlined$onClick$1(imageView, this));
            new BroadcastReceiverExt(this, PaymentSuccessfullyActivity$onCreate$2.INSTANCE);
            MaterialButton materialButton = (MaterialButton) _$_findCachedViewById(com.iqonic.store.R.id.tvDone);
            materialButton.setOnClickListener(new PaymentSuccessfullyActivity$onCreate$$inlined$onClick$2(materialButton, this));
            showProgress(true);
            CreateOrderNotes createOrderNotes = new CreateOrderNotes((String) null, false, 3, (DefaultConstructorMarker) null);
            createOrderNotes.setCustomer_note(true);
            createOrderNotes.setNote("{\n\"status\":\"Ordered\",\n\"message\":\"Your order has been placed.\"\n} ");
            NetworkExtensionKt.getRestApiImpl$default((String) null, 1, (Object) null).addOrderNotes(createOrderResponse.getId(), createOrderNotes, new PaymentSuccessfullyActivity$onCreate$4(this), PaymentSuccessfullyActivity$onCreate$5.INSTANCE);
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.iqonic.store.models.CreateOrderResponse");
    }

    private final void changeColor() {
        TextView textView = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.lblOrderPlaceSuccessfully);
        Intrinsics.checkExpressionValueIsNotNull(textView, "lblOrderPlaceSuccessfully");
        ExtensionsKt.changeTextPrimaryColor(textView);
        TextView textView2 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.lblTotalAmount);
        Intrinsics.checkExpressionValueIsNotNull(textView2, "lblTotalAmount");
        ExtensionsKt.changeTextSecondaryColor(textView2);
        TextView textView3 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.amount);
        Intrinsics.checkExpressionValueIsNotNull(textView3, "amount");
        ExtensionsKt.changeTextPrimaryColor(textView3);
        TextView textView4 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.lblTransaction);
        Intrinsics.checkExpressionValueIsNotNull(textView4, "lblTransaction");
        ExtensionsKt.changeTextSecondaryColor(textView4);
        TextView textView5 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.transactionId);
        Intrinsics.checkExpressionValueIsNotNull(textView5, "transactionId");
        ExtensionsKt.changeTextPrimaryColor(textView5);
        TextView textView6 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.lblOrderId);
        Intrinsics.checkExpressionValueIsNotNull(textView6, "lblOrderId");
        ExtensionsKt.changeTextSecondaryColor(textView6);
        TextView textView7 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.orderId);
        Intrinsics.checkExpressionValueIsNotNull(textView7, "orderId");
        ExtensionsKt.changeTextPrimaryColor(textView7);
        TextView textView8 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.lblPaymentThrough);
        Intrinsics.checkExpressionValueIsNotNull(textView8, "lblPaymentThrough");
        ExtensionsKt.changeTextSecondaryColor(textView8);
        TextView textView9 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.paidVia);
        Intrinsics.checkExpressionValueIsNotNull(textView9, "paidVia");
        ExtensionsKt.changeTextPrimaryColor(textView9);
        TextView textView10 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.lblTransactionFee);
        Intrinsics.checkExpressionValueIsNotNull(textView10, "lblTransactionFee");
        ExtensionsKt.changeTextSecondaryColor(textView10);
        TextView textView11 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.transactionDate);
        Intrinsics.checkExpressionValueIsNotNull(textView11, "transactionDate");
        ExtensionsKt.changeTextPrimaryColor(textView11);
        MaterialButton materialButton = (MaterialButton) _$_findCachedViewById(com.iqonic.store.R.id.tvDone);
        Intrinsics.checkExpressionValueIsNotNull(materialButton, "tvDone");
        ExtensionsKt.changeBackgroundTint(materialButton, AppExtensionsKt.getButtonColor());
        RelativeLayout relativeLayout = (RelativeLayout) _$_findCachedViewById(com.iqonic.store.R.id.rlMain);
        Intrinsics.checkExpressionValueIsNotNull(relativeLayout, "rlMain");
        ExtensionsKt.changeBackgroundColor(relativeLayout);
    }
}
