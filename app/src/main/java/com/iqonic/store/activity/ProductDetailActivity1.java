package com.iqonic.store.activity;

import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.widget.NestedScrollView;
import androidx.viewpager.widget.ViewPager;
import com.facebook.appevents.AppEventsConstants;
import com.google.android.material.button.MaterialButton;
import com.iqonic.store.AppBaseActivity;
import com.iqonic.store.adapter.BaseAdapter;
import com.iqonic.store.adapter.ProductImageAdapter;
import com.iqonic.store.models.Image;
import com.iqonic.store.models.RequestModel;
import com.iqonic.store.models.StoreCategory;
import com.iqonic.store.models.StoreProductModel;
import com.iqonic.store.models.StoreUpSale;
import com.iqonic.store.utils.dotsindicator.DotsIndicator;
import com.iqonic.store.utils.extensions.AppExtensionsKt;
import com.iqonic.store.utils.extensions.ExtensionsKt;
import com.iqonic.store.utils.extensions.ExtensionsKt$launchActivity$1;
import com.iqonic.store.utils.extensions.NetworkExtensionKt;
import com.iqonic.store.utils.extensions.StringExtensionsKt;
import com.iqonic.store.utils.extensions.ViewExtensionsKt;
import com.store.proshop.R;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.StringCompanionObject;
import kotlin.text.StringsKt;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000r\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0010\u0007\n\u0002\b\f\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\r\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\b\u0010\u001c\u001a\u00020\u001dH\u0002J\u0010\u0010\u001e\u001a\u00020\u001d2\u0006\u0010\u001f\u001a\u00020\u0016H\u0002J\u001c\u0010 \u001a\u00020!2\b\u0010\"\u001a\u0004\u0018\u00010\u00042\b\u0010#\u001a\u0004\u0018\u00010\u0004H\u0002J\b\u0010$\u001a\u00020\u001dH\u0002J\u001a\u0010%\u001a\u00020\u001d2\u0006\u0010&\u001a\u00020\u00162\b\b\u0002\u0010'\u001a\u00020\u0004H\u0002J\b\u0010(\u001a\u00020\u001dH\u0003J\b\u0010)\u001a\u00020\u001dH\u0002J\u0010\u0010*\u001a\u00020\u001d2\u0006\u0010+\u001a\u00020\u0018H\u0002J\u0012\u0010,\u001a\u00020\u001d2\b\u0010-\u001a\u0004\u0018\u00010.H\u0014J\u0012\u0010/\u001a\u00020\u00062\b\u00100\u001a\u0004\u0018\u000101H\u0016J\b\u00102\u001a\u00020\u001dH\u0002J\u0010\u00103\u001a\u00020\u001d2\u0006\u00104\u001a\u00020\rH\u0003J\b\u00105\u001a\u00020\u001dH\u0002J\b\u00106\u001a\u00020\u001dH\u0002J\b\u00107\u001a\u00020\u001dH\u0002J\u0010\u00108\u001a\u00020\u001d2\u0006\u00104\u001a\u00020\rH\u0003J\"\u00109\u001a\u00020\u001d2\u0006\u0010:\u001a\u00020\u00142\u0006\u0010+\u001a\u00020\u00182\b\b\u0002\u0010;\u001a\u00020\u0006H\u0002J\u0010\u0010<\u001a\u00020\u001d2\u0006\u00104\u001a\u00020\rH\u0003J\u0010\u0010=\u001a\u00020\u001d2\u0006\u00104\u001a\u00020\rH\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u000e¢\u0006\u0002\n\u0000R\u0016\u0010\u0007\u001a\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010\bX\u000e¢\u0006\u0002\n\u0000R\u0014\u0010\t\u001a\b\u0012\u0004\u0012\u00020\n0\bX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u0004X\u000e¢\u0006\u0002\n\u0000R\u0014\u0010\f\u001a\b\u0012\u0004\u0012\u00020\r0\bX\u0004¢\u0006\u0002\n\u0000R\u001e\u0010\u000e\u001a\u0012\u0012\u0004\u0012\u00020\u00040\u000fj\b\u0012\u0004\u0012\u00020\u0004`\u0010X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0006X\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0006X\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0014X.¢\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u0016X\u000e¢\u0006\u0002\n\u0000R\u0014\u0010\u0017\u001a\b\u0012\u0004\u0012\u00020\u00180\bX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0019\u001a\u00020\u0004X\u000e¢\u0006\u0002\n\u0000R\u0016\u0010\u001a\u001a\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u001bX\u000e¢\u0006\u0002\n\u0000¨\u0006>"}, d2 = {"Lcom/iqonic/store/activity/ProductDetailActivity1;", "Lcom/iqonic/store/AppBaseActivity;", "()V", "image", "", "isAddedToCart", "", "mAttributeAdapter", "Lcom/iqonic/store/adapter/BaseAdapter;", "mCategoryAdapter", "Lcom/iqonic/store/models/StoreCategory;", "mExternalURL", "mGroupCartAdapter", "Lcom/iqonic/store/models/StoreProductModel;", "mImages", "Ljava/util/ArrayList;", "Lkotlin/collections/ArrayList;", "mIsExternalProduct", "mIsInWishList", "mMenuCart", "Landroid/view/View;", "mPId", "", "mProductAdapter", "Lcom/iqonic/store/models/StoreUpSale;", "mQuantity", "mYearAdapter", "Landroid/widget/ArrayAdapter;", "addItemToCart", "", "addItemToCartGroupItem", "id", "calculateDiscount", "", "salePrice", "regularPrice", "changeColor", "changeFavIcon", "drawable", "iconTint", "getProductDetail", "loadApis", "mAddCart", "model", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onCreateOptionsMenu", "menu", "Landroid/view/Menu;", "onFavouriteClick", "onSaleOffer", "its", "removeCartItem", "setCartCount", "setCartCountFromPref", "setPriceDetail", "setProductItem1", "view", "params", "showUpComingSale", "viewVariableImage", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: ProductDetailActivity1.kt */
public final class ProductDetailActivity1 extends AppBaseActivity {
    private HashMap _$_findViewCache;
    private String image = "";
    /* access modifiers changed from: private */
    public boolean isAddedToCart;
    /* access modifiers changed from: private */
    public BaseAdapter<String> mAttributeAdapter;
    /* access modifiers changed from: private */
    public final BaseAdapter<StoreCategory> mCategoryAdapter = new BaseAdapter<>(R.layout.item_category, new ProductDetailActivity1$mCategoryAdapter$1(this));
    /* access modifiers changed from: private */
    public String mExternalURL = "";
    /* access modifiers changed from: private */
    public final BaseAdapter<StoreProductModel> mGroupCartAdapter = new BaseAdapter<>(R.layout.item_group, new ProductDetailActivity1$mGroupCartAdapter$1(this));
    private final ArrayList<String> mImages = new ArrayList<>();
    /* access modifiers changed from: private */
    public boolean mIsExternalProduct;
    /* access modifiers changed from: private */
    public boolean mIsInWishList;
    private View mMenuCart;
    private int mPId;
    /* access modifiers changed from: private */
    public final BaseAdapter<StoreUpSale> mProductAdapter = new BaseAdapter<>(R.layout.item_home_dashboard1, new ProductDetailActivity1$mProductAdapter$1(this));
    private String mQuantity = "1";
    /* access modifiers changed from: private */
    public ArrayAdapter<String> mYearAdapter;

    public void _$_clearFindViewByIdCache() {
        HashMap hashMap = this._$_findViewCache;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    public View _$_findCachedViewById(int i) {
        if (this._$_findViewCache == null) {
            this._$_findViewCache = new HashMap();
        }
        View view = (View) this._$_findViewCache.get(Integer.valueOf(i));
        if (view != null) {
            return view;
        }
        View findViewById = findViewById(i);
        this._$_findViewCache.put(Integer.valueOf(i), findViewById);
        return findViewById;
    }

    static /* synthetic */ void setProductItem1$default(ProductDetailActivity1 productDetailActivity1, View view, StoreUpSale storeUpSale, boolean z, int i, Object obj) {
        if ((i & 4) != 0) {
            z = false;
        }
        productDetailActivity1.setProductItem1(view, storeUpSale, z);
    }

    private final void setProductItem1(View view, StoreUpSale storeUpSale, boolean z) {
        if (!z) {
            ImageView imageView = (ImageView) view.findViewById(com.iqonic.store.R.id.ivProduct);
            Intrinsics.checkExpressionValueIsNotNull(imageView, "view.ivProduct");
            imageView.setLayoutParams(AppExtensionsKt.productLayoutParams(this));
        } else {
            ImageView imageView2 = (ImageView) view.findViewById(com.iqonic.store.R.id.ivProduct);
            Intrinsics.checkExpressionValueIsNotNull(imageView2, "view.ivProduct");
            imageView2.setLayoutParams(AppExtensionsKt.productLayoutParamsForDealOffer(this));
        }
        List<Image> images = storeUpSale.getImages();
        if (images == null) {
            Intrinsics.throwNpe();
        }
        boolean z2 = false;
        String src = images.get(0).getSrc();
        if (src == null) {
            Intrinsics.throwNpe();
        }
        if (src.length() > 0) {
            ImageView imageView3 = (ImageView) view.findViewById(com.iqonic.store.R.id.ivProduct);
            Intrinsics.checkExpressionValueIsNotNull(imageView3, "view.ivProduct");
            List<Image> images2 = storeUpSale.getImages();
            if (images2 == null) {
                Intrinsics.throwNpe();
            }
            String src2 = images2.get(0).getSrc();
            if (src2 == null) {
                Intrinsics.throwNpe();
            }
            NetworkExtensionKt.loadImageFromUrl$default(imageView3, src2, 0, 0, 6, (Object) null);
            List<Image> images3 = storeUpSale.getImages();
            if (images3 == null) {
                Intrinsics.throwNpe();
            }
            String src3 = images3.get(0).getSrc();
            if (src3 == null) {
                Intrinsics.throwNpe();
            }
            this.image = src3;
        }
        String name = storeUpSale.getName();
        if (name == null) {
            Intrinsics.throwNpe();
        }
        List split$default = StringsKt.split$default((CharSequence) name, new String[]{","}, false, 0, 6, (Object) null);
        TextView textView = (TextView) view.findViewById(com.iqonic.store.R.id.tvProductName);
        Intrinsics.checkExpressionValueIsNotNull(textView, "view.tvProductName");
        textView.setText((CharSequence) split$default.get(0));
        TextView textView2 = (TextView) view.findViewById(com.iqonic.store.R.id.tvProductWeight);
        Intrinsics.checkExpressionValueIsNotNull(textView2, "view.tvProductWeight");
        ExtensionsKt.changeAccentColor(textView2);
        TextView textView3 = (TextView) view.findViewById(com.iqonic.store.R.id.tvProductName);
        Intrinsics.checkExpressionValueIsNotNull(textView3, "view.tvProductName");
        ExtensionsKt.changeTextPrimaryColor(textView3);
        TextView textView4 = (TextView) view.findViewById(com.iqonic.store.R.id.tvOriginalPrice);
        Intrinsics.checkExpressionValueIsNotNull(textView4, "view.tvOriginalPrice");
        ExtensionsKt.changeTextSecondaryColor(textView4);
        TextView textView5 = (TextView) view.findViewById(com.iqonic.store.R.id.tvDiscountPrice);
        Intrinsics.checkExpressionValueIsNotNull(textView5, "view.tvDiscountPrice");
        ExtensionsKt.changeTextPrimaryColor(textView5);
        TextView textView6 = (TextView) view.findViewById(com.iqonic.store.R.id.tvAdd);
        Intrinsics.checkExpressionValueIsNotNull(textView6, "view.tvAdd");
        ExtensionsKt.changeBackgroundTint(textView6, AppExtensionsKt.getAccentColor());
        String sale_price = storeUpSale.getSale_price();
        if (sale_price == null) {
            Intrinsics.throwNpe();
        }
        if (sale_price.length() > 0) {
            TextView textView7 = (TextView) view.findViewById(com.iqonic.store.R.id.tvSaleLabel);
            Intrinsics.checkExpressionValueIsNotNull(textView7, "view.tvSaleLabel");
            textView7.setVisibility(0);
            TextView textView8 = (TextView) view.findViewById(com.iqonic.store.R.id.tvDiscountPrice);
            Intrinsics.checkExpressionValueIsNotNull(textView8, "view.tvDiscountPrice");
            String sale_price2 = storeUpSale.getSale_price();
            if (sale_price2 == null) {
                Intrinsics.throwNpe();
            }
            textView8.setText(StringExtensionsKt.currencyFormat$default(sale_price2, (String) null, 1, (Object) null));
            TextView textView9 = (TextView) view.findViewById(com.iqonic.store.R.id.tvOriginalPrice);
            Intrinsics.checkExpressionValueIsNotNull(textView9, "view.tvOriginalPrice");
            ExtensionsKt.applyStrike(textView9);
            TextView textView10 = (TextView) view.findViewById(com.iqonic.store.R.id.tvOriginalPrice);
            Intrinsics.checkExpressionValueIsNotNull(textView10, "view.tvOriginalPrice");
            String regular_price = storeUpSale.getRegular_price();
            if (regular_price == null) {
                Intrinsics.throwNpe();
            }
            textView10.setText(StringExtensionsKt.currencyFormat$default(regular_price, (String) null, 1, (Object) null));
            TextView textView11 = (TextView) view.findViewById(com.iqonic.store.R.id.tvOriginalPrice);
            Intrinsics.checkExpressionValueIsNotNull(textView11, "view.tvOriginalPrice");
            textView11.setVisibility(0);
        } else {
            TextView textView12 = (TextView) view.findViewById(com.iqonic.store.R.id.tvSaleLabel);
            Intrinsics.checkExpressionValueIsNotNull(textView12, "view.tvSaleLabel");
            textView12.setVisibility(8);
            TextView textView13 = (TextView) view.findViewById(com.iqonic.store.R.id.tvOriginalPrice);
            Intrinsics.checkExpressionValueIsNotNull(textView13, "view.tvOriginalPrice");
            textView13.setVisibility(0);
            String regular_price2 = storeUpSale.getRegular_price();
            if (regular_price2 == null) {
                Intrinsics.throwNpe();
            }
            if (regular_price2.length() == 0) {
                z2 = true;
            }
            if (z2) {
                TextView textView14 = (TextView) view.findViewById(com.iqonic.store.R.id.tvOriginalPrice);
                Intrinsics.checkExpressionValueIsNotNull(textView14, "view.tvOriginalPrice");
                textView14.setText("");
                TextView textView15 = (TextView) view.findViewById(com.iqonic.store.R.id.tvDiscountPrice);
                Intrinsics.checkExpressionValueIsNotNull(textView15, "view.tvDiscountPrice");
                String price = storeUpSale.getPrice();
                if (price == null) {
                    Intrinsics.throwNpe();
                }
                textView15.setText(StringExtensionsKt.currencyFormat$default(price, (String) null, 1, (Object) null));
            } else {
                TextView textView16 = (TextView) view.findViewById(com.iqonic.store.R.id.tvOriginalPrice);
                Intrinsics.checkExpressionValueIsNotNull(textView16, "view.tvOriginalPrice");
                textView16.setText("");
                TextView textView17 = (TextView) view.findViewById(com.iqonic.store.R.id.tvDiscountPrice);
                Intrinsics.checkExpressionValueIsNotNull(textView17, "view.tvDiscountPrice");
                String regular_price3 = storeUpSale.getRegular_price();
                if (regular_price3 == null) {
                    Intrinsics.throwNpe();
                }
                textView17.setText(StringExtensionsKt.currencyFormat$default(regular_price3, (String) null, 1, (Object) null));
            }
        }
        view.setOnClickListener(new ProductDetailActivity1$setProductItem1$$inlined$onClick$1(view, this, storeUpSale));
        TextView textView18 = (TextView) view.findViewById(com.iqonic.store.R.id.tvAdd);
        textView18.setOnClickListener(new ProductDetailActivity1$setProductItem1$$inlined$onClick$2(textView18, this, storeUpSale));
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:2:0x0023, code lost:
        r6 = r6.getExtras();
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onCreate(android.os.Bundle r6) {
        /*
            r5 = this;
            super.onCreate(r6)
            r6 = 2131492902(0x7f0c0026, float:1.860927E38)
            r5.setContentView((int) r6)
            int r6 = com.iqonic.store.R.id.toolbar
            android.view.View r6 = r5._$_findCachedViewById(r6)
            androidx.appcompat.widget.Toolbar r6 = (androidx.appcompat.widget.Toolbar) r6
            java.lang.String r0 = "toolbar"
            kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r6, r0)
            r5.setDetailToolbar(r6)
            r5.changeColor()
            android.content.Intent r6 = r5.getIntent()
            r0 = 0
            if (r6 == 0) goto L_0x0030
            android.os.Bundle r6 = r6.getExtras()
            if (r6 == 0) goto L_0x0030
            java.lang.String r1 = "data"
            java.lang.Object r6 = r6.get(r1)
            goto L_0x0031
        L_0x0030:
            r6 = r0
        L_0x0031:
            r1 = 2
            java.lang.String r2 = "product_id"
            r3 = 0
            if (r6 != 0) goto L_0x0055
            android.content.Intent r6 = r5.getIntent()
            if (r6 == 0) goto L_0x0048
            android.os.Bundle r6 = r6.getExtras()
            if (r6 == 0) goto L_0x0048
            java.lang.Object r6 = r6.get(r2)
            goto L_0x0049
        L_0x0048:
            r6 = r0
        L_0x0049:
            if (r6 != 0) goto L_0x0055
            r6 = 2131820734(0x7f1100be, float:1.9274191E38)
            com.iqonic.store.utils.extensions.ExtensionsKt.toast$default((android.app.Activity) r5, (int) r6, (int) r3, (int) r1, (java.lang.Object) r0)
            r5.finish()
            return
        L_0x0055:
            android.content.Intent r6 = r5.getIntent()
            if (r6 == 0) goto L_0x0064
            int r6 = r6.getIntExtra(r2, r3)
            java.lang.Integer r6 = java.lang.Integer.valueOf(r6)
            goto L_0x0065
        L_0x0064:
            r6 = r0
        L_0x0065:
            if (r6 != 0) goto L_0x006a
            kotlin.jvm.internal.Intrinsics.throwNpe()
        L_0x006a:
            int r6 = r6.intValue()
            r5.mPId = r6
            com.iqonic.store.utils.BroadcastReceiverExt r6 = new com.iqonic.store.utils.BroadcastReceiverExt
            r2 = r5
            android.content.Context r2 = (android.content.Context) r2
            com.iqonic.store.activity.ProductDetailActivity1$onCreate$1 r4 = new com.iqonic.store.activity.ProductDetailActivity1$onCreate$1
            r4.<init>(r5)
            kotlin.jvm.functions.Function1 r4 = (kotlin.jvm.functions.Function1) r4
            r6.<init>(r2, r4)
            int r6 = com.iqonic.store.R.id.rvLike
            android.view.View r6 = r5._$_findCachedViewById(r6)
            androidx.recyclerview.widget.RecyclerView r6 = (androidx.recyclerview.widget.RecyclerView) r6
            r4 = 1
            if (r6 == 0) goto L_0x008d
            com.iqonic.store.utils.extensions.ExtensionsKt.setHorizontalLayout$default(r6, r3, r4, r0)
        L_0x008d:
            int r6 = com.iqonic.store.R.id.rvLike
            android.view.View r6 = r5._$_findCachedViewById(r6)
            androidx.recyclerview.widget.RecyclerView r6 = (androidx.recyclerview.widget.RecyclerView) r6
            if (r6 == 0) goto L_0x009e
            com.iqonic.store.adapter.BaseAdapter<com.iqonic.store.models.StoreUpSale> r0 = r5.mProductAdapter
            androidx.recyclerview.widget.RecyclerView$Adapter r0 = (androidx.recyclerview.widget.RecyclerView.Adapter) r0
            r6.setAdapter(r0)
        L_0x009e:
            int r6 = com.iqonic.store.R.id.rvCategory
            android.view.View r6 = r5._$_findCachedViewById(r6)
            androidx.recyclerview.widget.RecyclerView r6 = (androidx.recyclerview.widget.RecyclerView) r6
            androidx.recyclerview.widget.GridLayoutManager r0 = new androidx.recyclerview.widget.GridLayoutManager
            r0.<init>(r2, r1)
            androidx.recyclerview.widget.RecyclerView$LayoutManager r0 = (androidx.recyclerview.widget.RecyclerView.LayoutManager) r0
            r6.setLayoutManager(r0)
            r6.setHasFixedSize(r4)
            com.iqonic.store.adapter.BaseAdapter<com.iqonic.store.models.StoreCategory> r0 = r5.mCategoryAdapter
            androidx.recyclerview.widget.RecyclerView$Adapter r0 = (androidx.recyclerview.widget.RecyclerView.Adapter) r0
            r6.setAdapter(r0)
            r5.getProductDetail()
            boolean r6 = com.iqonic.store.utils.extensions.AppExtensionsKt.isLoggedIn()
            if (r6 == 0) goto L_0x00c6
            r5.loadApis()
        L_0x00c6:
            int r6 = com.iqonic.store.R.id.tvItemProductOriginalPrice
            android.view.View r6 = r5._$_findCachedViewById(r6)
            android.widget.TextView r6 = (android.widget.TextView) r6
            java.lang.String r0 = "tvItemProductOriginalPrice"
            kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r6, r0)
            com.iqonic.store.utils.extensions.ExtensionsKt.applyStrike(r6)
            int r6 = com.iqonic.store.R.id.btnAddCard
            android.view.View r6 = r5._$_findCachedViewById(r6)
            com.google.android.material.button.MaterialButton r6 = (com.google.android.material.button.MaterialButton) r6
            com.iqonic.store.activity.ProductDetailActivity1$onCreate$$inlined$onClick$1 r0 = new com.iqonic.store.activity.ProductDetailActivity1$onCreate$$inlined$onClick$1
            r0.<init>(r6, r5)
            android.view.View$OnClickListener r0 = (android.view.View.OnClickListener) r0
            r6.setOnClickListener(r0)
            int r6 = com.iqonic.store.R.id.llReviews
            android.view.View r6 = r5._$_findCachedViewById(r6)
            android.widget.LinearLayout r6 = (android.widget.LinearLayout) r6
            com.iqonic.store.activity.ProductDetailActivity1$onCreate$$inlined$onClick$2 r0 = new com.iqonic.store.activity.ProductDetailActivity1$onCreate$$inlined$onClick$2
            r0.<init>(r6, r5)
            android.view.View$OnClickListener r0 = (android.view.View.OnClickListener) r0
            r6.setOnClickListener(r0)
            int r6 = com.iqonic.store.R.id.toolbar_layout
            android.view.View r6 = r5._$_findCachedViewById(r6)
            com.google.android.material.appbar.CollapsingToolbarLayout r6 = (com.google.android.material.appbar.CollapsingToolbarLayout) r6
            r0 = 2131886317(0x7f1200ed, float:1.940721E38)
            r6.setCollapsedTitleTextAppearance(r0)
            int r6 = com.iqonic.store.R.id.toolbar_layout
            android.view.View r6 = r5._$_findCachedViewById(r6)
            com.google.android.material.appbar.CollapsingToolbarLayout r6 = (com.google.android.material.appbar.CollapsingToolbarLayout) r6
            java.lang.String r0 = com.iqonic.store.utils.extensions.AppExtensionsKt.getPrimaryColor()
            int r0 = android.graphics.Color.parseColor(r0)
            r6.setContentScrimColor(r0)
            int r6 = com.iqonic.store.R.id.app_bar
            android.view.View r6 = r5._$_findCachedViewById(r6)
            com.google.android.material.appbar.AppBarLayout r6 = (com.google.android.material.appbar.AppBarLayout) r6
            com.iqonic.store.activity.ProductDetailActivity1$onCreate$5 r0 = new com.iqonic.store.activity.ProductDetailActivity1$onCreate$5
            r0.<init>(r5)
            com.google.android.material.appbar.AppBarLayout$OnOffsetChangedListener r0 = (com.google.android.material.appbar.AppBarLayout.OnOffsetChangedListener) r0
            r6.addOnOffsetChangedListener((com.google.android.material.appbar.AppBarLayout.OnOffsetChangedListener) r0)
            int r6 = com.iqonic.store.R.id.ivFavourite
            android.view.View r6 = r5._$_findCachedViewById(r6)
            android.widget.ImageView r6 = (android.widget.ImageView) r6
            com.iqonic.store.activity.ProductDetailActivity1$onCreate$$inlined$onClick$3 r0 = new com.iqonic.store.activity.ProductDetailActivity1$onCreate$$inlined$onClick$3
            r0.<init>(r6, r5)
            android.view.View$OnClickListener r0 = (android.view.View.OnClickListener) r0
            r6.setOnClickListener(r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.iqonic.store.activity.ProductDetailActivity1.onCreate(android.os.Bundle):void");
    }

    /* access modifiers changed from: private */
    public final void mAddCart(StoreUpSale storeUpSale) {
        if (AppExtensionsKt.isLoggedIn()) {
            RequestModel requestModel = new RequestModel();
            requestModel.setPro_id(storeUpSale.getId());
            requestModel.setQuantity(1);
            NetworkExtensionKt.addItemToCart(this, requestModel, ProductDetailActivity1$mAddCart$1.INSTANCE);
            return;
        }
        Bundle bundle = null;
        Intent intent = new Intent(this, SignInUpActivity.class);
        ProductDetailActivity1$mAddCart$2.INSTANCE.invoke(intent);
        if (Build.VERSION.SDK_INT >= 16) {
            startActivityForResult(intent, -1, bundle);
        } else {
            startActivityForResult(intent, -1);
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_dashboard, menu);
        if (menu == null) {
            Intrinsics.throwNpe();
        }
        MenuItem findItem = menu.findItem(R.id.action_cart);
        Intrinsics.checkExpressionValueIsNotNull(findItem, "menu!!.findItem(R.id.action_cart)");
        MenuItem findItem2 = menu.findItem(R.id.action_search);
        Intrinsics.checkExpressionValueIsNotNull(findItem2, "menu.findItem(R.id.action_search)");
        findItem.setVisible(true);
        findItem2.setVisible(false);
        View actionView = findItem.getActionView();
        Intrinsics.checkExpressionValueIsNotNull(actionView, "menuWishItem.actionView");
        this.mMenuCart = actionView;
        View actionView2 = findItem.getActionView();
        actionView2.setOnClickListener(new ProductDetailActivity1$onCreateOptionsMenu$$inlined$onClick$1(actionView2, this));
        setCartCount();
        return super.onCreateOptionsMenu(menu);
    }

    /* access modifiers changed from: private */
    public final void setCartCountFromPref() {
        if (AppExtensionsKt.isLoggedIn()) {
            String cartCount = AppExtensionsKt.getCartCount();
            View view = this.mMenuCart;
            if (view == null) {
                Intrinsics.throwUninitializedPropertyAccessException("mMenuCart");
            }
            ImageView imageView = (ImageView) view.findViewById(com.iqonic.store.R.id.ivCart);
            if (imageView != null) {
                ExtensionsKt.changeBackgroundImageTint(imageView, AppExtensionsKt.getAccentColor());
            }
            View view2 = this.mMenuCart;
            if (view2 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("mMenuCart");
            }
            TextView textView = (TextView) view2.findViewById(com.iqonic.store.R.id.tvNotificationCount);
            if (textView != null) {
                ExtensionsKt.changeTint(textView, AppExtensionsKt.getAccentColor());
            }
            View view3 = this.mMenuCart;
            if (view3 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("mMenuCart");
            }
            TextView textView2 = (TextView) view3.findViewById(com.iqonic.store.R.id.tvNotificationCount);
            if (textView2 != null) {
                textView2.setText(cartCount);
            }
            if (StringExtensionsKt.checkIsEmpty(cartCount) || Intrinsics.areEqual((Object) cartCount, (Object) AppEventsConstants.EVENT_PARAM_VALUE_NO)) {
                View view4 = this.mMenuCart;
                if (view4 == null) {
                    Intrinsics.throwUninitializedPropertyAccessException("mMenuCart");
                }
                TextView textView3 = (TextView) view4.findViewById(com.iqonic.store.R.id.tvNotificationCount);
                if (textView3 != null) {
                    ViewExtensionsKt.hide(textView3);
                    return;
                }
                return;
            }
            View view5 = this.mMenuCart;
            if (view5 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("mMenuCart");
            }
            TextView textView4 = (TextView) view5.findViewById(com.iqonic.store.R.id.tvNotificationCount);
            if (textView4 != null) {
                ViewExtensionsKt.show(textView4);
            }
        }
    }

    /* access modifiers changed from: private */
    public final void addItemToCart() {
        RequestModel requestModel = new RequestModel();
        requestModel.setPro_id(Integer.valueOf(this.mPId));
        requestModel.setQuantity(Integer.valueOf(Integer.parseInt(this.mQuantity)));
        NetworkExtensionKt.addItemToCart(this, requestModel, new ProductDetailActivity1$addItemToCart$1(this));
    }

    /* access modifiers changed from: private */
    public final void addItemToCartGroupItem(int i) {
        RequestModel requestModel = new RequestModel();
        requestModel.setPro_id(Integer.valueOf(i));
        requestModel.setQuantity(Integer.valueOf(Integer.parseInt(this.mQuantity)));
        NetworkExtensionKt.addItemToCart(this, requestModel, new ProductDetailActivity1$addItemToCartGroupItem$1(this));
    }

    private final void setCartCount() {
        String cartCount = AppExtensionsKt.getCartCount();
        View view = this.mMenuCart;
        if (view == null) {
            Intrinsics.throwUninitializedPropertyAccessException("mMenuCart");
        }
        TextView textView = (TextView) view.findViewById(com.iqonic.store.R.id.tvNotificationCount);
        Intrinsics.checkExpressionValueIsNotNull(textView, "mMenuCart.tvNotificationCount");
        textView.setText(cartCount);
        if (StringExtensionsKt.checkIsEmpty(cartCount) || Intrinsics.areEqual((Object) cartCount, (Object) AppEventsConstants.EVENT_PARAM_VALUE_NO)) {
            View view2 = this.mMenuCart;
            if (view2 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("mMenuCart");
            }
            TextView textView2 = (TextView) view2.findViewById(com.iqonic.store.R.id.tvNotificationCount);
            Intrinsics.checkExpressionValueIsNotNull(textView2, "mMenuCart.tvNotificationCount");
            ViewExtensionsKt.hide(textView2);
            return;
        }
        View view3 = this.mMenuCart;
        if (view3 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("mMenuCart");
        }
        TextView textView3 = (TextView) view3.findViewById(com.iqonic.store.R.id.tvNotificationCount);
        Intrinsics.checkExpressionValueIsNotNull(textView3, "mMenuCart.tvNotificationCount");
        ViewExtensionsKt.show(textView3);
    }

    /* access modifiers changed from: private */
    public final void removeCartItem() {
        RequestModel requestModel = new RequestModel();
        requestModel.setPro_id(Integer.valueOf(this.mPId));
        NetworkExtensionKt.removeCartItem(this, requestModel, new ProductDetailActivity1$removeCartItem$1(this));
    }

    private final void loadApis() {
        if (ExtensionsKt.isNetworkAvailable()) {
            AppExtensionsKt.fetchAndStoreCartData(this);
        }
    }

    private final void getProductDetail() {
        NestedScrollView nestedScrollView = (NestedScrollView) _$_findCachedViewById(com.iqonic.store.R.id.scrollView);
        Intrinsics.checkExpressionValueIsNotNull(nestedScrollView, "scrollView");
        nestedScrollView.setVisibility(8);
        if (ExtensionsKt.isNetworkAvailable()) {
            showProgress(true);
            NetworkExtensionKt.getRestApiImpl$default((String) null, 1, (Object) null).productDetail(this.mPId, new ProductDetailActivity1$getProductDetail$1(this), new ProductDetailActivity1$getProductDetail$2(this));
        }
    }

    private final float calculateDiscount(String str, String str2) {
        if (str == null) {
            Intrinsics.throwNpe();
        }
        float parseFloat = Float.parseFloat(str) * 100.0f;
        if (str2 == null) {
            Intrinsics.throwNpe();
        }
        return 100.0f - (parseFloat / Float.parseFloat(str2));
    }

    /* access modifiers changed from: private */
    public final void viewVariableImage(StoreProductModel storeProductModel) {
        Image image2;
        String src;
        this.mImages.clear();
        List<Image> images = storeProductModel.getImages();
        if (images == null) {
            Intrinsics.throwNpe();
        }
        int size = images.size();
        for (int i = 0; i < size; i++) {
            List<Image> images2 = storeProductModel.getImages();
            if (!(images2 == null || (image2 = images2.get(i)) == null || (src = image2.getSrc()) == null)) {
                this.mImages.add(src);
            }
        }
        ProductImageAdapter productImageAdapter = new ProductImageAdapter(this.mImages);
        ViewPager viewPager = (ViewPager) _$_findCachedViewById(com.iqonic.store.R.id.productViewPager);
        Intrinsics.checkExpressionValueIsNotNull(viewPager, "productViewPager");
        viewPager.setAdapter(productImageAdapter);
        ((DotsIndicator) _$_findCachedViewById(com.iqonic.store.R.id.dots)).attachViewPager((ViewPager) _$_findCachedViewById(com.iqonic.store.R.id.productViewPager));
        ((DotsIndicator) _$_findCachedViewById(com.iqonic.store.R.id.dots)).setDotDrawable(R.drawable.bg_circle_primary, R.drawable.black_dot);
        productImageAdapter.setListener(new ProductDetailActivity1$viewVariableImage$2(this, storeProductModel));
    }

    /* access modifiers changed from: private */
    public final void setPriceDetail(StoreProductModel storeProductModel) {
        this.mPId = storeProductModel.getId();
        String str = null;
        if (storeProductModel.getOnSale()) {
            TextView textView = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvPrice);
            Intrinsics.checkExpressionValueIsNotNull(textView, "tvPrice");
            String price = storeProductModel.getPrice();
            textView.setText(price != null ? StringExtensionsKt.currencyFormat$default(price, (String) null, 1, (Object) null) : null);
            TextView textView2 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvSaleLabel);
            Intrinsics.checkExpressionValueIsNotNull(textView2, "tvSaleLabel");
            ViewExtensionsKt.show(textView2);
            TextView textView3 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvItemProductOriginalPrice);
            Intrinsics.checkExpressionValueIsNotNull(textView3, "tvItemProductOriginalPrice");
            ExtensionsKt.applyStrike(textView3);
            TextView textView4 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvItemProductOriginalPrice);
            Intrinsics.checkExpressionValueIsNotNull(textView4, "tvItemProductOriginalPrice");
            String regularPrice = storeProductModel.getRegularPrice();
            if (regularPrice != null) {
                str = StringExtensionsKt.currencyFormat$default(regularPrice, (String) null, 1, (Object) null);
            }
            textView4.setText(str);
            LinearLayout linearLayout = (LinearLayout) _$_findCachedViewById(com.iqonic.store.R.id.upcomingSale);
            Intrinsics.checkExpressionValueIsNotNull(linearLayout, "upcomingSale");
            linearLayout.setVisibility(8);
            float calculateDiscount = calculateDiscount(storeProductModel.getSalePrice(), storeProductModel.getRegularPrice());
            if (((double) calculateDiscount) > 0.0d) {
                TextView textView5 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvSaleDiscount);
                Intrinsics.checkExpressionValueIsNotNull(textView5, "tvSaleDiscount");
                textView5.setVisibility(0);
                TextView textView6 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvSaleDiscount);
                Intrinsics.checkExpressionValueIsNotNull(textView6, "tvSaleDiscount");
                StringBuilder sb = new StringBuilder();
                StringCompanionObject stringCompanionObject = StringCompanionObject.INSTANCE;
                String format = String.format("%.2f", Arrays.copyOf(new Object[]{Float.valueOf(calculateDiscount)}, 1));
                Intrinsics.checkExpressionValueIsNotNull(format, "java.lang.String.format(format, *args)");
                sb.append(format);
                sb.append(getString(R.string.lbl_off));
                textView6.setText(sb.toString());
            }
            onSaleOffer(storeProductModel);
            return;
        }
        TextView textView7 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvSaleDiscount);
        Intrinsics.checkExpressionValueIsNotNull(textView7, "tvSaleDiscount");
        textView7.setVisibility(4);
        TextView textView8 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvItemProductOriginalPrice);
        Intrinsics.checkExpressionValueIsNotNull(textView8, "tvItemProductOriginalPrice");
        textView8.setText("");
        TextView textView9 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvPrice);
        Intrinsics.checkExpressionValueIsNotNull(textView9, "tvPrice");
        String regularPrice2 = storeProductModel.getRegularPrice();
        if (regularPrice2 != null) {
            str = StringExtensionsKt.currencyFormat$default(regularPrice2, (String) null, 1, (Object) null);
        }
        textView9.setText(str);
        TextView textView10 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvSaleLabel);
        Intrinsics.checkExpressionValueIsNotNull(textView10, "tvSaleLabel");
        ViewExtensionsKt.hide(textView10);
        showUpComingSale(storeProductModel);
        TextView textView11 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvSaleOffer);
        Intrinsics.checkExpressionValueIsNotNull(textView11, "tvSaleOffer");
        textView11.setVisibility(8);
    }

    private final void onSaleOffer(StoreProductModel storeProductModel) {
        if (!Intrinsics.areEqual(storeProductModel.getDateOnSaleFrom(), (Object) "")) {
            TextView textView = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvSaleOffer);
            Intrinsics.checkExpressionValueIsNotNull(textView, "tvSaleOffer");
            textView.setVisibility(0);
            try {
                Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(String.valueOf(storeProductModel.getDateOnSaleTo()) + " 23:59:59");
                Intrinsics.checkExpressionValueIsNotNull(parse, "dateFormat.parse(endTime)");
                long time = parse.getTime() - new Date().getTime();
                new ProductDetailActivity1$onSaleOffer$1(this, time, time, 1000).start();
            } catch (ParseException e) {
                e.printStackTrace();
            }
        } else {
            TextView textView2 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvSaleOffer);
            Intrinsics.checkExpressionValueIsNotNull(textView2, "tvSaleOffer");
            textView2.setVisibility(8);
        }
    }

    private final void showUpComingSale(StoreProductModel storeProductModel) {
        if (!Intrinsics.areEqual(storeProductModel.getDateOnSaleFrom(), (Object) "")) {
            LinearLayout linearLayout = (LinearLayout) _$_findCachedViewById(com.iqonic.store.R.id.upcomingSale);
            Intrinsics.checkExpressionValueIsNotNull(linearLayout, "upcomingSale");
            linearLayout.setVisibility(0);
            TextView textView = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvUpcomingSale);
            Intrinsics.checkExpressionValueIsNotNull(textView, "tvUpcomingSale");
            textView.setText(getString(R.string.lbl_sale_start_from) + " " + storeProductModel.getDateOnSaleFrom() + " " + getString(R.string.lbl_to) + " " + storeProductModel.getDateOnSaleTo() + ". " + getString(R.string.lbl_ge_amazing_discounts_on_the_products));
            return;
        }
        LinearLayout linearLayout2 = (LinearLayout) _$_findCachedViewById(com.iqonic.store.R.id.upcomingSale);
        Intrinsics.checkExpressionValueIsNotNull(linearLayout2, "upcomingSale");
        linearLayout2.setVisibility(8);
    }

    /* access modifiers changed from: private */
    public final void onFavouriteClick() {
        if (this.mIsInWishList) {
            changeFavIcon(R.drawable.ic_heart, AppExtensionsKt.getPrimaryColor());
            ImageView imageView = (ImageView) _$_findCachedViewById(com.iqonic.store.R.id.ivFavourite);
            Intrinsics.checkExpressionValueIsNotNull(imageView, "ivFavourite");
            imageView.setClickable(false);
            RequestModel requestModel = new RequestModel();
            requestModel.setPro_id(Integer.valueOf(this.mPId));
            NetworkExtensionKt.removeFromWishList(this, requestModel, new ProductDetailActivity1$onFavouriteClick$1(this));
        } else if (AppExtensionsKt.isLoggedIn()) {
            changeFavIcon(R.drawable.ic_heart_fill, AppExtensionsKt.getPrimaryColor());
            ImageView imageView2 = (ImageView) _$_findCachedViewById(com.iqonic.store.R.id.ivFavourite);
            Intrinsics.checkExpressionValueIsNotNull(imageView2, "ivFavourite");
            imageView2.setClickable(false);
            RequestModel requestModel2 = new RequestModel();
            requestModel2.setPro_id(Integer.valueOf(this.mPId));
            NetworkExtensionKt.addToWishList(this, requestModel2, new ProductDetailActivity1$onFavouriteClick$2(this));
        } else {
            Bundle bundle = null;
            Intent intent = new Intent(this, SignInUpActivity.class);
            ExtensionsKt$launchActivity$1.INSTANCE.invoke(intent);
            if (Build.VERSION.SDK_INT >= 16) {
                startActivityForResult(intent, -1, bundle);
            } else {
                startActivityForResult(intent, -1);
            }
        }
    }

    static /* synthetic */ void changeFavIcon$default(ProductDetailActivity1 productDetailActivity1, int i, String str, int i2, Object obj) {
        if ((i2 & 2) != 0) {
            str = AppExtensionsKt.getAccentColor();
        }
        productDetailActivity1.changeFavIcon(i, str);
    }

    /* access modifiers changed from: private */
    public final void changeFavIcon(int i, String str) {
        ((ImageView) _$_findCachedViewById(com.iqonic.store.R.id.ivFavourite)).setImageResource(i);
        ImageView imageView = (ImageView) _$_findCachedViewById(com.iqonic.store.R.id.ivFavourite);
        Intrinsics.checkExpressionValueIsNotNull(imageView, "ivFavourite");
        ExtensionsKt.changeBackgroundImageTint(imageView, str);
    }

    private final void changeColor() {
        TextView textView = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvAvailability);
        Intrinsics.checkExpressionValueIsNotNull(textView, "tvAvailability");
        ExtensionsKt.changeAccentColor(textView);
        TextView textView2 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvSaleDiscount);
        Intrinsics.checkExpressionValueIsNotNull(textView2, "tvSaleDiscount");
        ExtensionsKt.changeTextPrimaryColor(textView2);
        TextView textView3 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvName);
        Intrinsics.checkExpressionValueIsNotNull(textView3, "tvName");
        ExtensionsKt.changeTextPrimaryColor(textView3);
        TextView textView4 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvPrice);
        Intrinsics.checkExpressionValueIsNotNull(textView4, "tvPrice");
        ExtensionsKt.changeAccentColor(textView4);
        TextView textView5 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvItemProductOriginalPrice);
        Intrinsics.checkExpressionValueIsNotNull(textView5, "tvItemProductOriginalPrice");
        ExtensionsKt.changeTextSecondaryColor(textView5);
        TextView textView6 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvSaleOffer);
        Intrinsics.checkExpressionValueIsNotNull(textView6, "tvSaleOffer");
        ExtensionsKt.changeAccentColor(textView6);
        TextView textView7 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.lblProductInclude);
        Intrinsics.checkExpressionValueIsNotNull(textView7, "lblProductInclude");
        ExtensionsKt.changeTextPrimaryColor(textView7);
        TextView textView8 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.lblAvailable);
        Intrinsics.checkExpressionValueIsNotNull(textView8, "lblAvailable");
        ExtensionsKt.changeTextPrimaryColor(textView8);
        TextView textView9 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.lblAdditionInformation);
        Intrinsics.checkExpressionValueIsNotNull(textView9, "lblAdditionInformation");
        ExtensionsKt.changeTextPrimaryColor(textView9);
        TextView textView10 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.lblUpcomingSale);
        Intrinsics.checkExpressionValueIsNotNull(textView10, "lblUpcomingSale");
        ExtensionsKt.changeTextPrimaryColor(textView10);
        TextView textView11 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvUpcomingSale);
        Intrinsics.checkExpressionValueIsNotNull(textView11, "tvUpcomingSale");
        ExtensionsKt.changeTextSecondaryColor(textView11);
        TextView textView12 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.lblDescription);
        Intrinsics.checkExpressionValueIsNotNull(textView12, "lblDescription");
        ExtensionsKt.changeTextPrimaryColor(textView12);
        TextView textView13 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvTags);
        Intrinsics.checkExpressionValueIsNotNull(textView13, "tvTags");
        ExtensionsKt.changeTextSecondaryColor(textView13);
        TextView textView14 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.lblCategory);
        Intrinsics.checkExpressionValueIsNotNull(textView14, "lblCategory");
        ExtensionsKt.changeTextPrimaryColor(textView14);
        TextView textView15 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.lblCategory);
        Intrinsics.checkExpressionValueIsNotNull(textView15, "lblCategory");
        ExtensionsKt.changeTextPrimaryColor(textView15);
        TextView textView16 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.lbl_like);
        Intrinsics.checkExpressionValueIsNotNull(textView16, "lbl_like");
        ExtensionsKt.changeTextPrimaryColor(textView16);
        TextView textView17 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvAllReviews);
        Intrinsics.checkExpressionValueIsNotNull(textView17, "tvAllReviews");
        ExtensionsKt.changeTextPrimaryColor(textView17);
        MaterialButton materialButton = (MaterialButton) _$_findCachedViewById(com.iqonic.store.R.id.btnAddCard);
        Intrinsics.checkExpressionValueIsNotNull(materialButton, "btnAddCard");
        materialButton.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor(AppExtensionsKt.getButtonColor())));
        CoordinatorLayout coordinatorLayout = (CoordinatorLayout) _$_findCachedViewById(com.iqonic.store.R.id.htab_maincontent);
        Intrinsics.checkExpressionValueIsNotNull(coordinatorLayout, "htab_maincontent");
        ExtensionsKt.changeBackgroundColor(coordinatorLayout);
    }
}
