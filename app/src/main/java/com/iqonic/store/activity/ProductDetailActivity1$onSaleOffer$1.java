package com.iqonic.store.activity;

import android.os.CountDownTimer;
import android.widget.TextView;
import com.iqonic.store.R;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0019\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0000*\u0001\u0000\b\n\u0018\u00002\u00020\u0001J\b\u0010\u0002\u001a\u00020\u0003H\u0016J\u0010\u0010\u0004\u001a\u00020\u00032\u0006\u0010\u0005\u001a\u00020\u0006H\u0017¨\u0006\u0007"}, d2 = {"com/iqonic/store/activity/ProductDetailActivity1$onSaleOffer$1", "Landroid/os/CountDownTimer;", "onFinish", "", "onTick", "millisUntilFinished", "", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: ProductDetailActivity1.kt */
public final class ProductDetailActivity1$onSaleOffer$1 extends CountDownTimer {
    final /* synthetic */ long $different;
    final /* synthetic */ ProductDetailActivity1 this$0;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    ProductDetailActivity1$onSaleOffer$1(ProductDetailActivity1 productDetailActivity1, long j, long j2, long j3) {
        super(j2, j3);
        this.this$0 = productDetailActivity1;
        this.$different = j;
    }

    public void onTick(long j) {
        String.valueOf(j / ((long) 1000));
        long j2 = (long) 60;
        long j3 = j2 * 1000;
        long j4 = j2 * j3;
        long j5 = ((long) 24) * j4;
        long j6 = j / j5;
        long j7 = j % j5;
        long j8 = j7 / j4;
        long j9 = j7 % j4;
        long j10 = j9 / j3;
        long j11 = (j9 % j3) / 1000;
        if (j6 > 0) {
            TextView textView = (TextView) this.this$0._$_findCachedViewById(R.id.tvSaleOffer);
            Intrinsics.checkExpressionValueIsNotNull(textView, "tvSaleOffer");
            textView.setText(this.this$0.getString(com.store.proshop.R.string.lbl_special_price_ends_in_less_then) + " " + j6 + this.this$0.getString(com.store.proshop.R.string.lbl_d) + " " + j8 + this.this$0.getString(com.store.proshop.R.string.lbl_h) + " " + j10 + this.this$0.getString(com.store.proshop.R.string.lbl_m) + " " + j11 + this.this$0.getString(com.store.proshop.R.string.lbl_s));
            return;
        }
        TextView textView2 = (TextView) this.this$0._$_findCachedViewById(R.id.tvSaleOffer);
        Intrinsics.checkExpressionValueIsNotNull(textView2, "tvSaleOffer");
        textView2.setText(this.this$0.getString(com.store.proshop.R.string.lbl_special_price_ends_in_less_then) + " " + j8 + this.this$0.getString(com.store.proshop.R.string.lbl_h) + " " + j10 + this.this$0.getString(com.store.proshop.R.string.lbl_m) + " " + j11 + this.this$0.getString(com.store.proshop.R.string.lbl_s));
    }

    public void onFinish() {
        TextView textView = (TextView) this.this$0._$_findCachedViewById(R.id.tvSaleOffer);
        Intrinsics.checkExpressionValueIsNotNull(textView, "tvSaleOffer");
        textView.setVisibility(8);
    }
}
