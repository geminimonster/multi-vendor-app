package com.iqonic.store.activity;

import android.content.DialogInterface;
import android.view.View;
import android.widget.Spinner;
import com.iqonic.store.R;
import com.iqonic.store.models.BaseResponse;
import com.iqonic.store.models.CancelOrderRequest;
import com.iqonic.store.models.CreateOrderNotes;
import com.iqonic.store.utils.extensions.ExtensionsKt;
import com.iqonic.store.utils.extensions.NetworkExtensionKt;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u00032\u0006\u0010\u0005\u001a\u00020\u0006H\n¢\u0006\u0002\b\u0007"}, d2 = {"<anonymous>", "", "dialog", "Landroid/content/DialogInterface;", "kotlin.jvm.PlatformType", "<anonymous parameter 1>", "", "onClick"}, k = 3, mv = {1, 1, 16})
/* compiled from: OrderDescriptionActivity.kt */
final class OrderDescriptionActivity$orderCancelPopup$1 implements DialogInterface.OnClickListener {
    final /* synthetic */ View $customLayout;
    final /* synthetic */ OrderDescriptionActivity this$0;

    OrderDescriptionActivity$orderCancelPopup$1(OrderDescriptionActivity orderDescriptionActivity, View view) {
        this.this$0 = orderDescriptionActivity;
        this.$customLayout = view;
    }

    public final void onClick(final DialogInterface dialogInterface, int i) {
        if (ExtensionsKt.isNetworkAvailable()) {
            CancelOrderRequest cancelOrderRequest = new CancelOrderRequest((String) null, (String) null, 3, (DefaultConstructorMarker) null);
            cancelOrderRequest.setStatus("cancelled");
            Spinner spinner = (Spinner) this.$customLayout.findViewById(R.id.spinner);
            Intrinsics.checkExpressionValueIsNotNull(spinner, "customLayout.spinner");
            cancelOrderRequest.setCustomer_note(spinner.getSelectedItem().toString());
            this.this$0.showProgress(true);
            NetworkExtensionKt.getRestApiImpl$default((String) null, 1, (Object) null).cancelOrder(OrderDescriptionActivity.access$getOrderModel$p(this.this$0).getId(), cancelOrderRequest, new Function1<BaseResponse, Unit>(this) {
                final /* synthetic */ OrderDescriptionActivity$orderCancelPopup$1 this$0;

                {
                    this.this$0 = r1;
                }

                public /* bridge */ /* synthetic */ Object invoke(Object obj) {
                    invoke((BaseResponse) obj);
                    return Unit.INSTANCE;
                }

                public final void invoke(BaseResponse baseResponse) {
                    Intrinsics.checkParameterIsNotNull(baseResponse, "it");
                    CreateOrderNotes createOrderNotes = new CreateOrderNotes((String) null, false, 3, (DefaultConstructorMarker) null);
                    createOrderNotes.setCustomer_note(true);
                    StringBuilder sb = new StringBuilder();
                    sb.append("{\n\"status\":\"Cancelled\",\n\"message\":\"Order Canceled by you due to ");
                    Spinner spinner = (Spinner) this.this$0.$customLayout.findViewById(R.id.spinner);
                    Intrinsics.checkExpressionValueIsNotNull(spinner, "customLayout.spinner");
                    sb.append(spinner.getSelectedItem().toString());
                    sb.append(".\"\n");
                    sb.append("} ");
                    createOrderNotes.setNote(sb.toString());
                    NetworkExtensionKt.getRestApiImpl$default((String) null, 1, (Object) null).addOrderNotes(OrderDescriptionActivity.access$getOrderModel$p(this.this$0.this$0).getId(), createOrderNotes, new Function1<BaseResponse, Unit>(this) {
                        final /* synthetic */ AnonymousClass1 this$0;

                        {
                            this.this$0 = r1;
                        }

                        public /* bridge */ /* synthetic */ Object invoke(Object obj) {
                            invoke((BaseResponse) obj);
                            return Unit.INSTANCE;
                        }

                        public final void invoke(BaseResponse baseResponse) {
                            Intrinsics.checkParameterIsNotNull(baseResponse, "it");
                            this.this$0.this$0.this$0.showProgress(false);
                            this.this$0.this$0.this$0.setResult(-1);
                            this.this$0.this$0.this$0.finish();
                        }
                    }, new Function1<String, Unit>(this) {
                        final /* synthetic */ AnonymousClass1 this$0;

                        {
                            this.this$0 = r1;
                        }

                        public /* bridge */ /* synthetic */ Object invoke(Object obj) {
                            invoke((String) obj);
                            return Unit.INSTANCE;
                        }

                        public final void invoke(String str) {
                            Intrinsics.checkParameterIsNotNull(str, "it");
                            this.this$0.this$0.this$0.showProgress(false);
                        }
                    });
                    dialogInterface.dismiss();
                }
            }, new Function1<String, Unit>(this) {
                final /* synthetic */ OrderDescriptionActivity$orderCancelPopup$1 this$0;

                {
                    this.this$0 = r1;
                }

                public /* bridge */ /* synthetic */ Object invoke(Object obj) {
                    invoke((String) obj);
                    return Unit.INSTANCE;
                }

                public final void invoke(String str) {
                    Intrinsics.checkParameterIsNotNull(str, "it");
                    dialogInterface.dismiss();
                    this.this$0.this$0.showProgress(false);
                    ExtensionsKt.snackBarError(this.this$0.this$0, str);
                }
            });
            return;
        }
        this.this$0.showProgress(false);
        ExtensionsKt.noInternetSnackBar(this.this$0);
        dialogInterface.dismiss();
    }
}
