package com.iqonic.store.activity;

import android.app.Activity;
import com.iqonic.store.models.ProductReviewData;
import com.iqonic.store.utils.extensions.ExtensionsKt;
import com.store.proshop.R;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n¢\u0006\u0002\b\u0004¨\u0006\u0005"}, d2 = {"<anonymous>", "", "it", "Lcom/iqonic/store/models/ProductReviewData;", "invoke", "com/iqonic/store/activity/ReviewsActivity$createProductReview$1$1"}, k = 3, mv = {1, 1, 16})
/* compiled from: ReviewsActivity.kt */
final class ReviewsActivity$createProductReview$$inlined$onClick$1$lambda$1 extends Lambda implements Function1<ProductReviewData, Unit> {
    final /* synthetic */ ReviewsActivity$createProductReview$$inlined$onClick$1 this$0;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    ReviewsActivity$createProductReview$$inlined$onClick$1$lambda$1(ReviewsActivity$createProductReview$$inlined$onClick$1 reviewsActivity$createProductReview$$inlined$onClick$1) {
        super(1);
        this.this$0 = reviewsActivity$createProductReview$$inlined$onClick$1;
    }

    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        invoke((ProductReviewData) obj);
        return Unit.INSTANCE;
    }

    public final void invoke(ProductReviewData productReviewData) {
        Intrinsics.checkParameterIsNotNull(productReviewData, "it");
        this.this$0.this$0.showProgress(false);
        ExtensionsKt.toast$default((Activity) this.this$0.this$0, (int) R.string.success_add, 0, 2, (Object) null);
        this.this$0.$dialog$inlined.dismiss();
        this.this$0.this$0.listProductReviews();
    }
}
