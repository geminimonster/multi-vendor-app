package com.iqonic.store.activity;

import android.os.Bundle;
import android.view.View;
import androidx.appcompat.widget.Toolbar;
import com.iqonic.store.AppBaseActivity;
import com.iqonic.store.fragments.ViewAllProductFragment;
import com.iqonic.store.utils.BroadcastReceiverExt;
import com.iqonic.store.utils.Constants;
import com.iqonic.store.utils.extensions.ExtensionsKt;
import com.iqonic.store.utils.extensions.StringExtensionsKt;
import com.store.proshop.R;
import java.util.HashMap;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u0012\u0010\u0005\u001a\u00020\u00062\b\u0010\u0007\u001a\u0004\u0018\u00010\bH\u0015R\u0010\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u000e¢\u0006\u0002\n\u0000¨\u0006\t"}, d2 = {"Lcom/iqonic/store/activity/ViewAllProductActivity;", "Lcom/iqonic/store/AppBaseActivity;", "()V", "mFragment", "Lcom/iqonic/store/fragments/ViewAllProductFragment;", "onCreate", "", "savedInstanceState", "Landroid/os/Bundle;", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: ViewAllProductActivity.kt */
public final class ViewAllProductActivity extends AppBaseActivity {
    private HashMap _$_findViewCache;
    /* access modifiers changed from: private */
    public ViewAllProductFragment mFragment;

    public void _$_clearFindViewByIdCache() {
        HashMap hashMap = this._$_findViewCache;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    public View _$_findCachedViewById(int i) {
        if (this._$_findViewCache == null) {
            this._$_findViewCache = new HashMap();
        }
        View view = (View) this._$_findViewCache.get(Integer.valueOf(i));
        if (view != null) {
            return view;
        }
        View findViewById = findViewById(i);
        this._$_findViewCache.put(Integer.valueOf(i), findViewById);
        return findViewById;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        ViewAllProductFragment viewAllProductFragment;
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_view_all);
        Toolbar toolbar = (Toolbar) _$_findCachedViewById(com.iqonic.store.R.id.toolbar);
        Intrinsics.checkExpressionValueIsNotNull(toolbar, "toolbar");
        setToolbar(toolbar);
        new BroadcastReceiverExt(this, new ViewAllProductActivity$onCreate$1(this));
        String stringExtra = getIntent().getStringExtra("title");
        setTitle(stringExtra != null ? StringExtensionsKt.getHtmlString(stringExtra) : null);
        mAppBarColor();
        int intExtra = getIntent().getIntExtra(Constants.KeyIntent.VIEWALLID, 0);
        int intExtra2 = getIntent().getIntExtra(Constants.KeyIntent.KEYID, -1);
        String stringExtra2 = getIntent().getStringExtra(Constants.KeyIntent.SPECIAL_PRODUCT_KEY);
        if (stringExtra2 != null) {
            viewAllProductFragment = ViewAllProductFragment.Companion.getNewInstance$default(ViewAllProductFragment.Companion, intExtra, intExtra2, false, stringExtra2, 4, (Object) null);
        } else {
            viewAllProductFragment = ViewAllProductFragment.Companion.getNewInstance$default(ViewAllProductFragment.Companion, intExtra, intExtra2, false, (String) null, 12, (Object) null);
        }
        this.mFragment = viewAllProductFragment;
        if (viewAllProductFragment == null) {
            Intrinsics.throwNpe();
        }
        ExtensionsKt.addFragment(this, viewAllProductFragment, R.id.fragmentContainer);
    }
}
