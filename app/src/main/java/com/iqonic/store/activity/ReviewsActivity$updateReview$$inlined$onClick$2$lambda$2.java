package com.iqonic.store.activity;

import com.google.android.material.button.MaterialButton;
import com.iqonic.store.utils.extensions.ViewExtensionsKt;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n¢\u0006\u0002\b\u0004¨\u0006\u0005"}, d2 = {"<anonymous>", "", "it", "", "invoke", "com/iqonic/store/activity/ReviewsActivity$updateReview$2$2"}, k = 3, mv = {1, 1, 16})
/* compiled from: ReviewsActivity.kt */
final class ReviewsActivity$updateReview$$inlined$onClick$2$lambda$2 extends Lambda implements Function1<String, Unit> {
    final /* synthetic */ MaterialButton $this_onClick;
    final /* synthetic */ ReviewsActivity$updateReview$$inlined$onClick$2 this$0;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    ReviewsActivity$updateReview$$inlined$onClick$2$lambda$2(MaterialButton materialButton, ReviewsActivity$updateReview$$inlined$onClick$2 reviewsActivity$updateReview$$inlined$onClick$2) {
        super(1);
        this.$this_onClick = materialButton;
        this.this$0 = reviewsActivity$updateReview$$inlined$onClick$2;
    }

    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        invoke((String) obj);
        return Unit.INSTANCE;
    }

    public final void invoke(String str) {
        Intrinsics.checkParameterIsNotNull(str, "it");
        this.this$0.this$0.showProgress(false);
        this.this$0.$dialog$inlined.dismiss();
        ViewExtensionsKt.snackBarError(this.$this_onClick, str);
    }
}
