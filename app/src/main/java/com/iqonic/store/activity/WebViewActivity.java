package com.iqonic.store.activity;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import androidx.appcompat.widget.Toolbar;
import com.iqonic.store.AppBaseActivity;
import com.iqonic.store.utils.BroadcastReceiverExt;
import com.iqonic.store.utils.Constants;
import com.store.proshop.R;
import java.io.File;
import java.util.HashMap;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Ref;

@Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\b\u0010\r\u001a\u00020\u000eH\u0002J\b\u0010\u000f\u001a\u00020\u000eH\u0002J\b\u0010\u0010\u001a\u00020\u000eH\u0002J\u0012\u0010\u0011\u001a\u00020\u000e2\b\u0010\u0012\u001a\u0004\u0018\u00010\u0013H\u0015J\b\u0010\u0014\u001a\u00020\u000eH\u0002R\u001a\u0010\u0003\u001a\u00020\u0004X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\bR\u000e\u0010\t\u001a\u00020\nX\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\fX\u000e¢\u0006\u0002\n\u0000¨\u0006\u0015"}, d2 = {"Lcom/iqonic/store/activity/WebViewActivity;", "Lcom/iqonic/store/AppBaseActivity;", "()V", "mIsError", "", "getMIsError", "()Z", "setMIsError", "(Z)V", "mWebViewClient", "Landroid/webkit/WebViewClient;", "webChromeClient", "Landroid/webkit/WebChromeClient;", "configureWebClient", "", "configureWebView", "initChromeClient", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "setupWebView", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: WebViewActivity.kt */
public final class WebViewActivity extends AppBaseActivity {
    private HashMap _$_findViewCache;
    private boolean mIsError;
    private WebViewClient mWebViewClient = new WebViewClient();
    private WebChromeClient webChromeClient = new WebChromeClient();

    public void _$_clearFindViewByIdCache() {
        HashMap hashMap = this._$_findViewCache;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    public View _$_findCachedViewById(int i) {
        if (this._$_findViewCache == null) {
            this._$_findViewCache = new HashMap();
        }
        View view = (View) this._$_findViewCache.get(Integer.valueOf(i));
        if (view != null) {
            return view;
        }
        View findViewById = findViewById(i);
        this._$_findViewCache.put(Integer.valueOf(i), findViewById);
        return findViewById;
    }

    public final boolean getMIsError() {
        return this.mIsError;
    }

    public final void setMIsError(boolean z) {
        this.mIsError = z;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_webview_payment);
        Toolbar toolbar = (Toolbar) _$_findCachedViewById(com.iqonic.store.R.id.toolbar);
        Intrinsics.checkExpressionValueIsNotNull(toolbar, "toolbar");
        setToolbar(toolbar);
        mAppBarColor();
        setupWebView();
        setTitle(getString(R.string.lbl_payment));
        ((WebView) _$_findCachedViewById(com.iqonic.store.R.id.webView)).loadUrl(getIntent().getStringExtra(Constants.KeyIntent.CHECKOUT_URL));
        new BroadcastReceiverExt(this, WebViewActivity$onCreate$1.INSTANCE);
    }

    private final void setupWebView() {
        showProgress(true);
        configureWebView();
        configureWebClient();
        initChromeClient();
    }

    private final void initChromeClient() {
        Ref.ObjectRef objectRef = new Ref.ObjectRef();
        objectRef.element = (View) null;
        Ref.ObjectRef objectRef2 = new Ref.ObjectRef();
        objectRef2.element = (WebChromeClient.CustomViewCallback) null;
        Ref.IntRef intRef = new Ref.IntRef();
        intRef.element = 0;
        Ref.IntRef intRef2 = new Ref.IntRef();
        intRef2.element = 0;
        this.webChromeClient = new WebViewActivity$initChromeClient$1(this, objectRef, intRef2, intRef, objectRef2);
        WebView webView = (WebView) _$_findCachedViewById(com.iqonic.store.R.id.webView);
        Intrinsics.checkExpressionValueIsNotNull(webView, "webView");
        webView.setWebChromeClient(this.webChromeClient);
    }

    private final void configureWebClient() {
        this.mWebViewClient = new WebViewActivity$configureWebClient$1(this);
        WebView webView = (WebView) _$_findCachedViewById(com.iqonic.store.R.id.webView);
        Intrinsics.checkExpressionValueIsNotNull(webView, "webView");
        webView.setWebViewClient(this.mWebViewClient);
    }

    private final void configureWebView() {
        try {
            WebView webView = (WebView) _$_findCachedViewById(com.iqonic.store.R.id.webView);
            Intrinsics.checkExpressionValueIsNotNull(webView, "webView");
            WebSettings settings = webView.getSettings();
            settings.setJavaScriptCanOpenWindowsAutomatically(true);
            settings.setJavaScriptEnabled(true);
            settings.setUseWideViewPort(true);
            settings.setLoadWithOverviewMode(true);
            settings.setDatabaseEnabled(true);
            settings.setDomStorageEnabled(true);
            settings.setAllowFileAccess(true);
            settings.setAllowContentAccess(true);
            WebView webView2 = (WebView) _$_findCachedViewById(com.iqonic.store.R.id.webView);
            Intrinsics.checkExpressionValueIsNotNull(webView2, "webView");
            webView2.setFocusable(true);
            WebView webView3 = (WebView) _$_findCachedViewById(com.iqonic.store.R.id.webView);
            Intrinsics.checkExpressionValueIsNotNull(webView3, "webView");
            webView3.setFocusableInTouchMode(true);
            WebView webView4 = (WebView) _$_findCachedViewById(com.iqonic.store.R.id.webView);
            Intrinsics.checkExpressionValueIsNotNull(webView4, "webView");
            webView4.setScrollContainer(false);
            WebView webView5 = (WebView) _$_findCachedViewById(com.iqonic.store.R.id.webView);
            Intrinsics.checkExpressionValueIsNotNull(webView5, "webView");
            webView5.setVerticalScrollBarEnabled(false);
            WebView webView6 = (WebView) _$_findCachedViewById(com.iqonic.store.R.id.webView);
            Intrinsics.checkExpressionValueIsNotNull(webView6, "webView");
            webView6.setHorizontalScrollBarEnabled(false);
            settings.setAppCacheEnabled(false);
            settings.setGeolocationEnabled(true);
            StringBuilder sb = new StringBuilder();
            Context applicationContext = getApplicationContext();
            Intrinsics.checkExpressionValueIsNotNull(applicationContext, "applicationContext");
            File filesDir = applicationContext.getFilesDir();
            Intrinsics.checkExpressionValueIsNotNull(filesDir, "applicationContext.filesDir");
            sb.append(filesDir.getAbsolutePath());
            sb.append("/cache");
            settings.setAppCachePath(sb.toString());
            settings.setCacheMode(2);
            settings.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.NORMAL);
            settings.setLoadsImagesAutomatically(true);
            settings.setLoadWithOverviewMode(true);
            if (Build.VERSION.SDK_INT >= 21) {
                settings.setMixedContentMode(0);
            }
            settings.setSupportMultipleWindows(false);
            settings.setBuiltInZoomControls(false);
            settings.setSupportZoom(false);
            settings.setUserAgentString("Mozilla/5.0 (Linux; Android 5.0; SM-G900P Build/LRX21T) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.0.0 Mobile Safari/537.36");
        } catch (Exception e) {
            e.printStackTrace();
        }
        ((WebView) _$_findCachedViewById(com.iqonic.store.R.id.webView)).clearHistory();
        ((WebView) _$_findCachedViewById(com.iqonic.store.R.id.webView)).clearCache(true);
    }
}
