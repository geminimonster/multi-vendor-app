package com.iqonic.store.activity;

import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Spinner;
import com.iqonic.store.R;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u00032\u0006\u0010\u0005\u001a\u00020\u0006H\n¢\u0006\u0002\b\u0007"}, d2 = {"<anonymous>", "", "<anonymous parameter 0>", "Landroid/widget/CompoundButton;", "kotlin.jvm.PlatformType", "isChecked", "", "onCheckedChanged"}, k = 3, mv = {1, 1, 16})
/* compiled from: EditProfileActivity.kt */
final class EditProfileActivity$onCreate$1 implements CompoundButton.OnCheckedChangeListener {
    final /* synthetic */ EditProfileActivity this$0;

    EditProfileActivity$onCreate$1(EditProfileActivity editProfileActivity) {
        this.this$0 = editProfileActivity;
    }

    public final void onCheckedChanged(CompoundButton compoundButton, boolean z) {
        if (z) {
            EditText editText = (EditText) this.this$0._$_findCachedViewById(R.id.edtBillingFName);
            Intrinsics.checkExpressionValueIsNotNull(editText, "edtBillingFName");
            ((EditText) this.this$0._$_findCachedViewById(R.id.edtShippingFName)).setText(editText.getText().toString());
            EditText editText2 = (EditText) this.this$0._$_findCachedViewById(R.id.edtBillingLName);
            Intrinsics.checkExpressionValueIsNotNull(editText2, "edtBillingLName");
            ((EditText) this.this$0._$_findCachedViewById(R.id.edtShippingLName)).setText(editText2.getText().toString());
            EditText editText3 = (EditText) this.this$0._$_findCachedViewById(R.id.edtBillingCompany);
            Intrinsics.checkExpressionValueIsNotNull(editText3, "edtBillingCompany");
            ((EditText) this.this$0._$_findCachedViewById(R.id.edtShippingCompany)).setText(editText3.getText().toString());
            EditText editText4 = (EditText) this.this$0._$_findCachedViewById(R.id.edtBillingAdd1);
            Intrinsics.checkExpressionValueIsNotNull(editText4, "edtBillingAdd1");
            ((EditText) this.this$0._$_findCachedViewById(R.id.edtShippingAdd1)).setText(editText4.getText().toString());
            EditText editText5 = (EditText) this.this$0._$_findCachedViewById(R.id.edtBillingAdd2);
            Intrinsics.checkExpressionValueIsNotNull(editText5, "edtBillingAdd2");
            ((EditText) this.this$0._$_findCachedViewById(R.id.edtShippingAdd2)).setText(editText5.getText().toString());
            EditText editText6 = (EditText) this.this$0._$_findCachedViewById(R.id.edtBillingCity);
            Intrinsics.checkExpressionValueIsNotNull(editText6, "edtBillingCity");
            ((EditText) this.this$0._$_findCachedViewById(R.id.edtShippingCity)).setText(editText6.getText().toString());
            EditText editText7 = (EditText) this.this$0._$_findCachedViewById(R.id.edtBillingPinCode);
            Intrinsics.checkExpressionValueIsNotNull(editText7, "edtBillingPinCode");
            ((EditText) this.this$0._$_findCachedViewById(R.id.edtShippingPinCode)).setText(editText7.getText().toString());
            Spinner spinner = (Spinner) this.this$0._$_findCachedViewById(R.id.spBillingState);
            Intrinsics.checkExpressionValueIsNotNull(spinner, "spBillingState");
            ((Spinner) this.this$0._$_findCachedViewById(R.id.spState)).setSelection(spinner.getSelectedItemPosition());
            Spinner spinner2 = (Spinner) this.this$0._$_findCachedViewById(R.id.spBillingCountry);
            Intrinsics.checkExpressionValueIsNotNull(spinner2, "spBillingCountry");
            ((Spinner) this.this$0._$_findCachedViewById(R.id.spCountry)).setSelection(spinner2.getSelectedItemPosition());
            return;
        }
        EditText editText8 = (EditText) this.this$0._$_findCachedViewById(R.id.edtShippingFName);
        Intrinsics.checkExpressionValueIsNotNull(editText8, "edtShippingFName");
        editText8.getText().clear();
        EditText editText9 = (EditText) this.this$0._$_findCachedViewById(R.id.edtShippingLName);
        Intrinsics.checkExpressionValueIsNotNull(editText9, "edtShippingLName");
        editText9.getText().clear();
        EditText editText10 = (EditText) this.this$0._$_findCachedViewById(R.id.edtShippingCompany);
        Intrinsics.checkExpressionValueIsNotNull(editText10, "edtShippingCompany");
        editText10.getText().clear();
        EditText editText11 = (EditText) this.this$0._$_findCachedViewById(R.id.edtShippingAdd1);
        Intrinsics.checkExpressionValueIsNotNull(editText11, "edtShippingAdd1");
        editText11.getText().clear();
        EditText editText12 = (EditText) this.this$0._$_findCachedViewById(R.id.edtShippingAdd2);
        Intrinsics.checkExpressionValueIsNotNull(editText12, "edtShippingAdd2");
        editText12.getText().clear();
        EditText editText13 = (EditText) this.this$0._$_findCachedViewById(R.id.edtShippingCity);
        Intrinsics.checkExpressionValueIsNotNull(editText13, "edtShippingCity");
        editText13.getText().clear();
        EditText editText14 = (EditText) this.this$0._$_findCachedViewById(R.id.edtShippingPinCode);
        Intrinsics.checkExpressionValueIsNotNull(editText14, "edtShippingPinCode");
        editText14.getText().clear();
    }
}
