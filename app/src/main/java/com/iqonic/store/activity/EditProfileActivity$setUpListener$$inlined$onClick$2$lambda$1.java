package com.iqonic.store.activity;

import androidx.coordinatorlayout.widget.CoordinatorLayout;
import com.iqonic.store.utils.extensions.ExtensionsKt;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n¢\u0006\u0002\b\u0004¨\u0006\u0005"}, d2 = {"<anonymous>", "", "it", "", "invoke", "com/iqonic/store/activity/EditProfileActivity$setUpListener$2$1"}, k = 3, mv = {1, 1, 16})
/* compiled from: EditProfileActivity.kt */
final class EditProfileActivity$setUpListener$$inlined$onClick$2$lambda$1 extends Lambda implements Function1<Boolean, Unit> {
    final /* synthetic */ CoordinatorLayout $this_onClick;
    final /* synthetic */ EditProfileActivity$setUpListener$$inlined$onClick$2 this$0;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    EditProfileActivity$setUpListener$$inlined$onClick$2$lambda$1(CoordinatorLayout coordinatorLayout, EditProfileActivity$setUpListener$$inlined$onClick$2 editProfileActivity$setUpListener$$inlined$onClick$2) {
        super(1);
        this.$this_onClick = coordinatorLayout;
        this.this$0 = editProfileActivity$setUpListener$$inlined$onClick$2;
    }

    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        invoke(((Boolean) obj).booleanValue());
        return Unit.INSTANCE;
    }

    public final void invoke(boolean z) {
        if (z) {
            CropImage.activity().setAspectRatio(1, 1).setGuidelines(CropImageView.Guidelines.OFF).setRequestedSize(300, 300).setOutputCompressQuality(40).start(this.this$0.this$0);
            return;
        }
        EditProfileActivity editProfileActivity = this.this$0.this$0;
        CoordinatorLayout coordinatorLayout = this.$this_onClick;
        Intrinsics.checkExpressionValueIsNotNull(coordinatorLayout, "this");
        ExtensionsKt.showPermissionAlert(editProfileActivity, coordinatorLayout);
    }
}
