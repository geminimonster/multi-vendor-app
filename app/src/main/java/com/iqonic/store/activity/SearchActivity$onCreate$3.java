package com.iqonic.store.activity;

import androidx.appcompat.widget.SearchView;
import kotlin.Metadata;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0000\n\u0002\u0010\u000b\n\u0000\u0010\u0000\u001a\u00020\u0001H\n¢\u0006\u0002\b\u0002"}, d2 = {"<anonymous>", "", "onClose"}, k = 3, mv = {1, 1, 16})
/* compiled from: SearchActivity.kt */
final class SearchActivity$onCreate$3 implements SearchView.OnCloseListener {
    final /* synthetic */ SearchActivity this$0;

    SearchActivity$onCreate$3(SearchActivity searchActivity) {
        this.this$0 = searchActivity;
    }

    public final boolean onClose() {
        this.this$0.mSearchQuery = "";
        this.this$0.mPage = 1;
        this.this$0.searchRequest.setText(this.this$0.mSearchQuery);
        this.this$0.searchRequest.setPage(this.this$0.mPage);
        return true;
    }
}
