package com.iqonic.store.activity;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001d\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000*\u0001\u0000\b\n\u0018\u00002\u00020\u0001J\u0018\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0007H\u0016¨\u0006\b¸\u0006\u0000"}, d2 = {"com/iqonic/store/activity/SubCategoryActivity$onCreate$2$1", "Landroidx/recyclerview/widget/RecyclerView$OnScrollListener;", "onScrollStateChanged", "", "recyclerView", "Landroidx/recyclerview/widget/RecyclerView;", "newState", "", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: SubCategoryActivity.kt */
public final class SubCategoryActivity$onCreate$$inlined$apply$lambda$1 extends RecyclerView.OnScrollListener {
    final /* synthetic */ SubCategoryActivity this$0;

    SubCategoryActivity$onCreate$$inlined$apply$lambda$1(SubCategoryActivity subCategoryActivity) {
        this.this$0 = subCategoryActivity;
    }

    public void onScrollStateChanged(RecyclerView recyclerView, int i) {
        int i2;
        Intrinsics.checkParameterIsNotNull(recyclerView, "recyclerView");
        super.onScrollStateChanged(recyclerView, i);
        RecyclerView.LayoutManager layoutManager = recyclerView.getLayoutManager();
        Integer valueOf = layoutManager != null ? Integer.valueOf(layoutManager.getItemCount()) : null;
        if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {
            RecyclerView.LayoutManager layoutManager2 = recyclerView.getLayoutManager();
            if (layoutManager2 != null) {
                i2 = ((LinearLayoutManager) layoutManager2).findLastCompletelyVisibleItemPosition();
            } else {
                throw new TypeCastException("null cannot be cast to non-null type androidx.recyclerview.widget.LinearLayoutManager");
            }
        } else if (recyclerView.getLayoutManager() instanceof GridLayoutManager) {
            RecyclerView.LayoutManager layoutManager3 = recyclerView.getLayoutManager();
            if (layoutManager3 != null) {
                i2 = ((GridLayoutManager) layoutManager3).findLastCompletelyVisibleItemPosition();
            } else {
                throw new TypeCastException("null cannot be cast to non-null type androidx.recyclerview.widget.GridLayoutManager");
            }
        } else {
            i2 = 0;
        }
        if (Intrinsics.areEqual((Object) this.this$0.isLastPage, (Object) false) && i2 != 0 && !this.this$0.mIsLoading && valueOf != null && valueOf.intValue() - 1 == i2) {
            this.this$0.mIsLoading = true;
            SubCategoryActivity subCategoryActivity = this.this$0;
            subCategoryActivity.countLoadMore = subCategoryActivity.countLoadMore + 1;
            this.this$0.data.put("page", Integer.valueOf(this.this$0.countLoadMore));
            this.this$0.data.put("per_page", 20);
            this.this$0.data.put("category", Integer.valueOf(this.this$0.mCategoryId));
            SubCategoryActivity subCategoryActivity2 = this.this$0;
            subCategoryActivity2.loadCategory(subCategoryActivity2.data);
        }
    }
}
