package com.iqonic.store.activity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.RadioButton;
import com.google.android.material.button.MaterialButton;
import com.iqonic.store.R;
import com.iqonic.store.models.Billing;
import com.iqonic.store.models.OrderRequest;
import com.iqonic.store.models.Shipping;
import com.iqonic.store.stripe.StripePaymentActivity;
import com.iqonic.store.utils.Constants;
import com.iqonic.store.utils.extensions.AppExtensionsKt;
import java.util.ArrayList;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\b\u0003\n\u0002\b\u0004\u0010\u0000\u001a\u00020\u0001\"\b\b\u0000\u0010\u0002*\u00020\u00032\u000e\u0010\u0004\u001a\n \u0005*\u0004\u0018\u00010\u00030\u0003H\n¢\u0006\u0002\b\u0006¨\u0006\u0007"}, d2 = {"<anonymous>", "", "T", "Landroid/view/View;", "it", "kotlin.jvm.PlatformType", "onClick", "com/iqonic/store/utils/extensions/ExtensionsKt$onClick$1"}, k = 3, mv = {1, 1, 16})
/* compiled from: Extensions.kt */
public final class OrderSummaryActivity$onCreate$$inlined$onClick$1 implements View.OnClickListener {
    final /* synthetic */ View $this_onClick;
    final /* synthetic */ OrderSummaryActivity this$0;

    public OrderSummaryActivity$onCreate$$inlined$onClick$1(View view, OrderSummaryActivity orderSummaryActivity) {
        this.$this_onClick = view;
        this.this$0 = orderSummaryActivity;
    }

    public final void onClick(View view) {
        MaterialButton materialButton = (MaterialButton) this.$this_onClick;
        if (this.this$0.isNativePayment) {
            RadioButton radioButton = (RadioButton) this.this$0._$_findCachedViewById(R.id.rbCard);
            Intrinsics.checkExpressionValueIsNotNull(radioButton, "rbCard");
            if (radioButton.isChecked()) {
                OrderSummaryActivity orderSummaryActivity = this.this$0;
                Bundle bundle = null;
                Intent intent = new Intent(orderSummaryActivity, StripePaymentActivity.class);
                new OrderSummaryActivity$onCreate$$inlined$onClick$1$lambda$1(this).invoke(intent);
                if (Build.VERSION.SDK_INT >= 16) {
                    orderSummaryActivity.startActivityForResult(intent, Constants.RequestCode.STRIPE_PAYMENT, bundle);
                } else {
                    orderSummaryActivity.startActivityForResult(intent, Constants.RequestCode.STRIPE_PAYMENT);
                }
            } else {
                RadioButton radioButton2 = (RadioButton) this.this$0._$_findCachedViewById(R.id.rbRazorpay);
                Intrinsics.checkExpressionValueIsNotNull(radioButton2, "rbRazorpay");
                if (radioButton2.isChecked()) {
                    this.this$0.showProgress(true);
                    this.this$0.startPayment();
                    return;
                }
                RadioButton radioButton3 = (RadioButton) this.this$0._$_findCachedViewById(R.id.rbCOD);
                Intrinsics.checkExpressionValueIsNotNull(radioButton3, "rbCOD");
                if (radioButton3.isChecked()) {
                    this.this$0.showProgress(true);
                    OrderRequest orderRequest = new OrderRequest((String) null, (String) null, 0, (String) null, (String) null, false, (Billing) null, (Shipping) null, (ArrayList) null, (ArrayList) null, (ArrayList) null, 2047, (DefaultConstructorMarker) null);
                    orderRequest.setPayment_method("cod");
                    orderRequest.setTransaction_id("");
                    orderRequest.setCustomer_id(Integer.parseInt(AppExtensionsKt.getUserId()));
                    orderRequest.setCurrency(AppExtensionsKt.getDefaultCurrencyFormate());
                    orderRequest.setStatus("pending");
                    orderRequest.setSet_paid(false);
                    this.this$0.createOrderRequest(orderRequest);
                }
            }
        } else {
            this.this$0.showProgress(true);
            OrderRequest orderRequest2 = new OrderRequest((String) null, (String) null, 0, (String) null, (String) null, false, (Billing) null, (Shipping) null, (ArrayList) null, (ArrayList) null, (ArrayList) null, 2047, (DefaultConstructorMarker) null);
            orderRequest2.setPayment_method("cod");
            orderRequest2.setTransaction_id("");
            orderRequest2.setCustomer_id(Integer.parseInt(AppExtensionsKt.getUserId()));
            orderRequest2.setCurrency(AppExtensionsKt.getDefaultCurrencyFormate());
            orderRequest2.setStatus("pending");
            orderRequest2.setSet_paid(false);
            this.this$0.createOrderRequest(orderRequest2);
        }
    }
}
