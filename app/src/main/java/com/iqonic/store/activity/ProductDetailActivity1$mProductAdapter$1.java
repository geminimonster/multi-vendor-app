package com.iqonic.store.activity;

import android.view.View;
import com.iqonic.store.models.StoreUpSale;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function3;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0007H\n¢\u0006\u0002\b\b"}, d2 = {"<anonymous>", "", "view", "Landroid/view/View;", "model", "Lcom/iqonic/store/models/StoreUpSale;", "<anonymous parameter 2>", "", "invoke"}, k = 3, mv = {1, 1, 16})
/* compiled from: ProductDetailActivity1.kt */
final class ProductDetailActivity1$mProductAdapter$1 extends Lambda implements Function3<View, StoreUpSale, Integer, Unit> {
    final /* synthetic */ ProductDetailActivity1 this$0;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    ProductDetailActivity1$mProductAdapter$1(ProductDetailActivity1 productDetailActivity1) {
        super(3);
        this.this$0 = productDetailActivity1;
    }

    public /* bridge */ /* synthetic */ Object invoke(Object obj, Object obj2, Object obj3) {
        invoke((View) obj, (StoreUpSale) obj2, ((Number) obj3).intValue());
        return Unit.INSTANCE;
    }

    public final void invoke(View view, StoreUpSale storeUpSale, int i) {
        Intrinsics.checkParameterIsNotNull(view, "view");
        Intrinsics.checkParameterIsNotNull(storeUpSale, "model");
        ProductDetailActivity1.setProductItem1$default(this.this$0, view, storeUpSale, false, 4, (Object) null);
    }
}
