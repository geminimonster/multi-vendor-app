package com.iqonic.store.activity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;
import com.iqonic.store.utils.extensions.AppExtensionsKt;
import com.iqonic.store.utils.extensions.ExtensionsKt$launchActivity$1;
import kotlin.Metadata;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\b\u0003\n\u0002\b\u0004\u0010\u0000\u001a\u00020\u0001\"\b\b\u0000\u0010\u0002*\u00020\u00032\u000e\u0010\u0004\u001a\n \u0005*\u0004\u0018\u00010\u00030\u0003H\n¢\u0006\u0002\b\u0006¨\u0006\u0007"}, d2 = {"<anonymous>", "", "T", "Landroid/view/View;", "it", "kotlin.jvm.PlatformType", "onClick", "com/iqonic/store/utils/extensions/ExtensionsKt$onClick$1"}, k = 3, mv = {1, 1, 16})
/* compiled from: Extensions.kt */
public final class ProductDetailActivity2$onCreate$$inlined$onClick$4 implements View.OnClickListener {
    final /* synthetic */ View $this_onClick;
    final /* synthetic */ ProductDetailActivity2 this$0;

    public ProductDetailActivity2$onCreate$$inlined$onClick$4(View view, ProductDetailActivity2 productDetailActivity2) {
        this.$this_onClick = view;
        this.this$0 = productDetailActivity2;
    }

    public final void onClick(View view) {
        RelativeLayout relativeLayout = (RelativeLayout) this.$this_onClick;
        if (AppExtensionsKt.isLoggedIn()) {
            ProductDetailActivity2 productDetailActivity2 = this.this$0;
            Bundle bundle = null;
            Intent intent = new Intent(productDetailActivity2, MyCartActivity.class);
            ExtensionsKt$launchActivity$1.INSTANCE.invoke(intent);
            if (Build.VERSION.SDK_INT >= 16) {
                productDetailActivity2.startActivityForResult(intent, -1, bundle);
            } else {
                productDetailActivity2.startActivityForResult(intent, -1);
            }
        } else {
            ProductDetailActivity2 productDetailActivity22 = this.this$0;
            Bundle bundle2 = null;
            Intent intent2 = new Intent(productDetailActivity22, SignInUpActivity.class);
            ProductDetailActivity2$onCreate$6$1.INSTANCE.invoke(intent2);
            if (Build.VERSION.SDK_INT >= 16) {
                productDetailActivity22.startActivityForResult(intent2, -1, bundle2);
            } else {
                productDetailActivity22.startActivityForResult(intent2, -1);
            }
        }
    }
}
