package com.iqonic.store.activity;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.iqonic.store.AppBaseActivity;
import com.iqonic.store.adapter.BaseAdapter;
import com.iqonic.store.models.RequestModel;
import com.iqonic.store.models.SearchRequest;
import com.iqonic.store.models.StoreProductModel;
import com.iqonic.store.models.Term;
import com.iqonic.store.utils.extensions.AppExtensionsKt;
import com.iqonic.store.utils.extensions.ExtensionsKt;
import com.iqonic.store.utils.extensions.NetworkExtensionKt;
import com.iqonic.store.utils.extensions.StringExtensionsKt;
import com.iqonic.store.utils.rangeBar.RangeBar;
import com.store.proshop.R;
import java.util.ArrayList;
import java.util.HashMap;
import kotlin.Metadata;
import kotlin.collections.ArraysKt;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000d\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00020\u0007H\u0002J\b\u0010\u001a\u001a\u00020\u0018H\u0002J\b\u0010\u001b\u001a\u00020\u0018H\u0002J\u0012\u0010\u001c\u001a\u00020\u00182\b\u0010\u001d\u001a\u0004\u0018\u00010\u001eH\u0015J\u0012\u0010\u001f\u001a\u00020\u00042\b\u0010 \u001a\u0004\u0018\u00010!H\u0016J\u0010\u0010\"\u001a\u00020\u00042\u0006\u0010#\u001a\u00020$H\u0016J\b\u0010%\u001a\u00020\u0018H\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u000e¢\u0006\u0002\n\u0000R\u0014\u0010\b\u001a\b\u0012\u0004\u0012\u00020\n0\tX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\fX\u000e¢\u0006\u0002\n\u0000R\u001e\u0010\r\u001a\u0012\u0012\u0004\u0012\u00020\u00070\u000ej\b\u0012\u0004\u0012\u00020\u0007`\u000fX\u000e¢\u0006\u0002\n\u0000R\u001e\u0010\u0010\u001a\u0012\u0012\u0004\u0012\u00020\f0\u000ej\b\u0012\u0004\u0012\u00020\f`\u000fX\u000e¢\u0006\u0002\n\u0000R\u001e\u0010\u0011\u001a\u0012\u0012\u0004\u0012\u00020\f0\u000ej\b\u0012\u0004\u0012\u00020\f`\u000fX\u000e¢\u0006\u0002\n\u0000R\u001e\u0010\u0012\u001a\u0012\u0012\u0004\u0012\u00020\u00130\u000ej\b\u0012\u0004\u0012\u00020\u0013`\u000fX\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0015X\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\u0007X\u000e¢\u0006\u0002\n\u0000¨\u0006&"}, d2 = {"Lcom/iqonic/store/activity/SearchActivity;", "Lcom/iqonic/store/AppBaseActivity;", "()V", "mIsFilterDataLoaded", "", "mIsLoading", "mPage", "", "mProductAdapter", "Lcom/iqonic/store/adapter/BaseAdapter;", "Lcom/iqonic/store/models/StoreProductModel;", "mSearchQuery", "", "mSelectedPrice", "Ljava/util/ArrayList;", "Lkotlin/collections/ArrayList;", "mSelectedSlug", "mSelectedTermId", "mTerms", "Lcom/iqonic/store/models/Term;", "searchRequest", "Lcom/iqonic/store/models/SearchRequest;", "totalPages", "addCart", "", "modelId", "getProductAttribute", "loadProducts", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onCreateOptionsMenu", "menu", "Landroid/view/Menu;", "onOptionsItemSelected", "item", "Landroid/view/MenuItem;", "openFilterBottomSheet", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: SearchActivity.kt */
public final class SearchActivity extends AppBaseActivity {
    private HashMap _$_findViewCache;
    /* access modifiers changed from: private */
    public boolean mIsFilterDataLoaded;
    /* access modifiers changed from: private */
    public boolean mIsLoading;
    /* access modifiers changed from: private */
    public int mPage = 1;
    /* access modifiers changed from: private */
    public final BaseAdapter<StoreProductModel> mProductAdapter = new BaseAdapter<>(R.layout.item_viewproductgrid, new SearchActivity$mProductAdapter$1(this));
    /* access modifiers changed from: private */
    public String mSearchQuery = "";
    /* access modifiers changed from: private */
    public ArrayList<Integer> mSelectedPrice = new ArrayList<>();
    /* access modifiers changed from: private */
    public ArrayList<String> mSelectedSlug = new ArrayList<>();
    /* access modifiers changed from: private */
    public ArrayList<String> mSelectedTermId = new ArrayList<>();
    /* access modifiers changed from: private */
    public ArrayList<Term> mTerms = new ArrayList<>();
    /* access modifiers changed from: private */
    public SearchRequest searchRequest = new SearchRequest();
    /* access modifiers changed from: private */
    public int totalPages;

    public void _$_clearFindViewByIdCache() {
        HashMap hashMap = this._$_findViewCache;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    public View _$_findCachedViewById(int i) {
        if (this._$_findViewCache == null) {
            this._$_findViewCache = new HashMap();
        }
        View view = (View) this._$_findViewCache.get(Integer.valueOf(i));
        if (view != null) {
            return view;
        }
        View findViewById = findViewById(i);
        this._$_findViewCache.put(Integer.valueOf(i), findViewById);
        return findViewById;
    }

    /* access modifiers changed from: private */
    public final void addCart(int i) {
        if (AppExtensionsKt.isLoggedIn()) {
            RequestModel requestModel = new RequestModel();
            requestModel.setPro_id(Integer.valueOf(i));
            requestModel.setQuantity(1);
            NetworkExtensionKt.addItemToCart(this, requestModel, SearchActivity$addCart$1.INSTANCE);
            return;
        }
        Bundle bundle = null;
        Intent intent = new Intent(this, SignInUpActivity.class);
        SearchActivity$addCart$2.INSTANCE.invoke(intent);
        if (Build.VERSION.SDK_INT >= 16) {
            startActivityForResult(intent, -1, bundle);
        } else {
            startActivityForResult(intent, -1);
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_search);
        Toolbar toolbar = (Toolbar) _$_findCachedViewById(com.iqonic.store.R.id.toolbar);
        Intrinsics.checkExpressionValueIsNotNull(toolbar, "toolbar");
        toolbar.setTitle((CharSequence) "");
        Toolbar toolbar2 = (Toolbar) _$_findCachedViewById(com.iqonic.store.R.id.toolbar);
        Intrinsics.checkExpressionValueIsNotNull(toolbar2, "toolbar");
        Drawable navigationIcon = toolbar2.getNavigationIcon();
        if (navigationIcon == null) {
            Intrinsics.throwNpe();
        }
        navigationIcon.setColorFilter(getResources().getColor(R.color.colorBackArrow), PorterDuff.Mode.SRC_ATOP);
        ((Toolbar) _$_findCachedViewById(com.iqonic.store.R.id.toolbar)).setNavigationOnClickListener(new SearchActivity$onCreate$1(this));
        Toolbar toolbar3 = (Toolbar) _$_findCachedViewById(com.iqonic.store.R.id.toolbar);
        Intrinsics.checkExpressionValueIsNotNull(toolbar3, "toolbar");
        setToolbar(toolbar3);
        mAppBarColor();
        LinearLayout linearLayout = (LinearLayout) _$_findCachedViewById(com.iqonic.store.R.id.llMain);
        Intrinsics.checkExpressionValueIsNotNull(linearLayout, "llMain");
        ExtensionsKt.changeBackgroundColor(linearLayout);
        ((SearchView) _$_findCachedViewById(com.iqonic.store.R.id.searchView)).setOnQueryTextListener(new SearchActivity$onCreate$2(this));
        ((SearchView) _$_findCachedViewById(com.iqonic.store.R.id.searchView)).setOnCloseListener(new SearchActivity$onCreate$3(this));
        ((SearchView) _$_findCachedViewById(com.iqonic.store.R.id.searchView)).onActionViewExpanded();
        RecyclerView recyclerView = (RecyclerView) _$_findCachedViewById(com.iqonic.store.R.id.aSearch_rvSearch);
        recyclerView.setAdapter(this.mProductAdapter);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 2));
        recyclerView.addOnScrollListener(new SearchActivity$onCreate$$inlined$apply$lambda$1(this));
        getProductAttribute();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_search, menu);
        if (menu == null) {
            Intrinsics.throwNpe();
        }
        MenuItem findItem = menu.findItem(R.id.action_filter);
        Drawable drawable = getResources().getDrawable(R.drawable.ic_filter);
        drawable.setColorFilter(Color.parseColor(AppExtensionsKt.getAccentColor()), PorterDuff.Mode.SRC_IN);
        Intrinsics.checkExpressionValueIsNotNull(findItem, "item");
        findItem.setIcon(drawable);
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        Intrinsics.checkParameterIsNotNull(menuItem, "item");
        int itemId = menuItem.getItemId();
        if (itemId == 16908332) {
            onBackPressed();
            return true;
        } else if (itemId != R.id.action_filter) {
            return super.onOptionsItemSelected(menuItem);
        } else {
            if (!this.mIsFilterDataLoaded) {
                return true;
            }
            openFilterBottomSheet();
            return true;
        }
    }

    /* access modifiers changed from: private */
    public final void loadProducts() {
        if (ExtensionsKt.isNetworkAvailable()) {
            showProgress(true);
            NetworkExtensionKt.getRestApiImpl$default((String) null, 1, (Object) null).search(this.searchRequest, new SearchActivity$loadProducts$1(this), new SearchActivity$loadProducts$2(this));
        }
    }

    private final void getProductAttribute() {
        if (ExtensionsKt.isNetworkAvailable()) {
            showProgress(true);
            NetworkExtensionKt.getRestApiImpl$default((String) null, 1, (Object) null).listAllProductAttribute(new SearchActivity$getProductAttribute$1(this), new SearchActivity$getProductAttribute$2(this));
        }
    }

    private final void openFilterBottomSheet() {
        BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(this);
        bottomSheetDialog.setContentView((int) R.layout.layout_filter);
        String[] strArr = {StringExtensionsKt.currencyFormat$default("1", (String) null, 1, (Object) null), StringExtensionsKt.currencyFormat$default("100", (String) null, 1, (Object) null), StringExtensionsKt.currencyFormat$default("500", (String) null, 1, (Object) null), StringExtensionsKt.currencyFormat$default("700", (String) null, 1, (Object) null), StringExtensionsKt.currencyFormat$default("1000", (String) null, 1, (Object) null), StringExtensionsKt.currencyFormat$default("2000", (String) null, 1, (Object) null), StringExtensionsKt.currencyFormat$default("5000", (String) null, 1, (Object) null), StringExtensionsKt.currencyFormat$default("7000", (String) null, 1, (Object) null), StringExtensionsKt.currencyFormat$default("10000", (String) null, 1, (Object) null), StringExtensionsKt.currencyFormat$default("15000", (String) null, 1, (Object) null), StringExtensionsKt.currencyFormat$default("20000", (String) null, 1, (Object) null)};
        String[] strArr2 = {"1", "100", "500", "700", "1000", "2000", "5000", "7000", "10000", "15000", "20000"};
        Dialog dialog = bottomSheetDialog;
        TextView textView = (TextView) dialog.findViewById(com.iqonic.store.R.id.lblFilter);
        Intrinsics.checkExpressionValueIsNotNull(textView, "filterDialog.lblFilter");
        ExtensionsKt.changeTextPrimaryColor(textView);
        TextView textView2 = (TextView) dialog.findViewById(com.iqonic.store.R.id.tvAttributesName);
        Intrinsics.checkExpressionValueIsNotNull(textView2, "filterDialog.tvAttributesName");
        ExtensionsKt.changeTint(textView2, AppExtensionsKt.getAccentColor());
        TextView textView3 = (TextView) dialog.findViewById(com.iqonic.store.R.id.tvApply);
        Intrinsics.checkExpressionValueIsNotNull(textView3, "filterDialog.tvApply");
        ExtensionsKt.changeTint(textView3, AppExtensionsKt.getPrimaryColor());
        TextView textView4 = (TextView) dialog.findViewById(com.iqonic.store.R.id.lblMin);
        Intrinsics.checkExpressionValueIsNotNull(textView4, "filterDialog.lblMin");
        ExtensionsKt.changeTextPrimaryColor(textView4);
        TextView textView5 = (TextView) dialog.findViewById(com.iqonic.store.R.id.lblMax);
        Intrinsics.checkExpressionValueIsNotNull(textView5, "filterDialog.lblMax");
        ExtensionsKt.changeTextPrimaryColor(textView5);
        ImageView imageView = (ImageView) dialog.findViewById(com.iqonic.store.R.id.ivClose);
        Intrinsics.checkExpressionValueIsNotNull(imageView, "filterDialog.ivClose");
        ExtensionsKt.changeBackgroundImageTint(imageView, AppExtensionsKt.getPrimaryColor());
        RangeBar rangeBar = (RangeBar) dialog.findViewById(com.iqonic.store.R.id.rangebar1);
        Intrinsics.checkExpressionValueIsNotNull(rangeBar, "filterDialog.rangebar1");
        rangeBar.setTickTopLabels((CharSequence[]) strArr);
        if (this.mSelectedPrice.size() == 2) {
            ((RangeBar) dialog.findViewById(com.iqonic.store.R.id.rangebar1)).setRangePinsByValue((float) ArraysKt.indexOf((T[]) strArr2, String.valueOf(this.mSelectedPrice.get(0).intValue())), (float) ArraysKt.indexOf((T[]) strArr2, String.valueOf(this.mSelectedPrice.get(1).intValue())));
        }
        BaseAdapter baseAdapter = new BaseAdapter(R.layout.item_filter_brand, new SearchActivity$openFilterBottomSheet$brandAdapter$1(this));
        baseAdapter.setOnItemClick(new SearchActivity$openFilterBottomSheet$1(baseAdapter));
        RecyclerView recyclerView = (RecyclerView) dialog.findViewById(com.iqonic.store.R.id.rcvBrands);
        ExtensionsKt.setVerticalLayout$default(recyclerView, false, 1, (Object) null);
        recyclerView.setAdapter(baseAdapter);
        baseAdapter.clearItems();
        baseAdapter.addItems(this.mTerms);
        TextView textView6 = (TextView) dialog.findViewById(com.iqonic.store.R.id.tvApply);
        textView6.setOnClickListener(new SearchActivity$openFilterBottomSheet$$inlined$onClick$1(textView6, this, strArr2, bottomSheetDialog));
        TextView textView7 = (TextView) dialog.findViewById(com.iqonic.store.R.id.tvReset);
        textView7.setOnClickListener(new SearchActivity$openFilterBottomSheet$$inlined$onClick$2(textView7, this, bottomSheetDialog));
        TextView textView8 = (TextView) dialog.findViewById(com.iqonic.store.R.id.tvSelectAll);
        textView8.setOnClickListener(new SearchActivity$openFilterBottomSheet$$inlined$onClick$3(textView8, this, baseAdapter));
        ImageView imageView2 = (ImageView) dialog.findViewById(com.iqonic.store.R.id.ivClose);
        imageView2.setOnClickListener(new SearchActivity$openFilterBottomSheet$$inlined$onClick$4(imageView2, bottomSheetDialog));
        bottomSheetDialog.show();
    }
}
