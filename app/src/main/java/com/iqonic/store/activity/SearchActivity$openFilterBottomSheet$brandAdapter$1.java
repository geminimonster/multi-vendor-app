package com.iqonic.store.activity;

import android.graphics.Color;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.iqonic.store.R;
import com.iqonic.store.models.Term;
import com.iqonic.store.utils.extensions.AppExtensionsKt;
import com.iqonic.store.utils.extensions.ExtensionsKt;
import com.iqonic.store.utils.extensions.ViewExtensionsKt;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function3;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0007H\n¢\u0006\u0002\b\b"}, d2 = {"<anonymous>", "", "view", "Landroid/view/View;", "model", "Lcom/iqonic/store/models/Term;", "<anonymous parameter 2>", "", "invoke"}, k = 3, mv = {1, 1, 16})
/* compiled from: SearchActivity.kt */
final class SearchActivity$openFilterBottomSheet$brandAdapter$1 extends Lambda implements Function3<View, Term, Integer, Unit> {
    final /* synthetic */ SearchActivity this$0;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    SearchActivity$openFilterBottomSheet$brandAdapter$1(SearchActivity searchActivity) {
        super(3);
        this.this$0 = searchActivity;
    }

    public /* bridge */ /* synthetic */ Object invoke(Object obj, Object obj2, Object obj3) {
        invoke((View) obj, (Term) obj2, ((Number) obj3).intValue());
        return Unit.INSTANCE;
    }

    public final void invoke(View view, Term term, int i) {
        Intrinsics.checkParameterIsNotNull(view, "view");
        Intrinsics.checkParameterIsNotNull(term, "model");
        if (term.isParent()) {
            TextView textView = (TextView) view.findViewById(R.id.tvAttributesName);
            Intrinsics.checkExpressionValueIsNotNull(textView, "view.tvAttributesName");
            textView.setVisibility(0);
            RelativeLayout relativeLayout = (RelativeLayout) view.findViewById(R.id.termsView);
            Intrinsics.checkExpressionValueIsNotNull(relativeLayout, "view.termsView");
            relativeLayout.setVisibility(8);
            TextView textView2 = (TextView) view.findViewById(R.id.tvAttributesName);
            Intrinsics.checkExpressionValueIsNotNull(textView2, "view.tvAttributesName");
            textView2.setText(term.getName());
        } else {
            RelativeLayout relativeLayout2 = (RelativeLayout) view.findViewById(R.id.termsView);
            Intrinsics.checkExpressionValueIsNotNull(relativeLayout2, "view.termsView");
            relativeLayout2.setVisibility(0);
            TextView textView3 = (TextView) view.findViewById(R.id.tvAttributesName);
            Intrinsics.checkExpressionValueIsNotNull(textView3, "view.tvAttributesName");
            textView3.setVisibility(8);
            TextView textView4 = (TextView) view.findViewById(R.id.tvBrandName);
            Intrinsics.checkExpressionValueIsNotNull(textView4, "view.tvBrandName");
            textView4.setText(term.getName());
            if (term.isSelected()) {
                TextView textView5 = (TextView) view.findViewById(R.id.tvBrandName);
                Intrinsics.checkExpressionValueIsNotNull(textView5, "view.tvBrandName");
                ExtensionsKt.changePrimaryColor(textView5);
                ((ImageView) view.findViewById(R.id.ivSelect)).setImageResource(com.store.proshop.R.drawable.ic_check);
                ImageView imageView = (ImageView) view.findViewById(R.id.ivSelect);
                Intrinsics.checkExpressionValueIsNotNull(imageView, "view.ivSelect");
                ExtensionsKt.changeBackgroundImageTint(imageView, AppExtensionsKt.getPrimaryColor());
                ImageView imageView2 = (ImageView) view.findViewById(R.id.ivSelect);
                Intrinsics.checkExpressionValueIsNotNull(imageView2, "view.ivSelect");
                ViewExtensionsKt.setStrokedBackground$default(imageView2, Color.parseColor(AppExtensionsKt.getAccentColor()), Color.parseColor(AppExtensionsKt.getAccentColor()), 0.4f, 0, 8, (Object) null);
            } else {
                TextView textView6 = (TextView) view.findViewById(R.id.tvBrandName);
                Intrinsics.checkExpressionValueIsNotNull(textView6, "view.tvBrandName");
                ExtensionsKt.changeTextSecondaryColor(textView6);
                ((ImageView) view.findViewById(R.id.ivSelect)).setImageResource(0);
                ImageView imageView3 = (ImageView) view.findViewById(R.id.ivSelect);
                Intrinsics.checkExpressionValueIsNotNull(imageView3, "view.ivSelect");
                ViewExtensionsKt.setStrokedBackground$default(imageView3, ExtensionsKt.color(this.this$0, com.store.proshop.R.color.checkbox_color), 0, 0.0f, 0, 14, (Object) null);
            }
        }
        TextView textView7 = (TextView) view.findViewById(R.id.tvAttributesName);
        Intrinsics.checkExpressionValueIsNotNull(textView7, "view.tvAttributesName");
        ExtensionsKt.changeTint(textView7, AppExtensionsKt.getAccentColor());
    }
}
