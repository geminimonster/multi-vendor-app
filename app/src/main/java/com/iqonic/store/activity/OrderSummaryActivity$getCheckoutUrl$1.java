package com.iqonic.store.activity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import com.iqonic.store.models.CheckoutResponse;
import com.iqonic.store.utils.Constants;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n¢\u0006\u0002\b\u0004"}, d2 = {"<anonymous>", "", "it", "Lcom/iqonic/store/models/CheckoutResponse;", "invoke"}, k = 3, mv = {1, 1, 16})
/* compiled from: OrderSummaryActivity.kt */
final class OrderSummaryActivity$getCheckoutUrl$1 extends Lambda implements Function1<CheckoutResponse, Unit> {
    final /* synthetic */ OrderSummaryActivity this$0;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    OrderSummaryActivity$getCheckoutUrl$1(OrderSummaryActivity orderSummaryActivity) {
        super(1);
        this.this$0 = orderSummaryActivity;
    }

    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        invoke((CheckoutResponse) obj);
        return Unit.INSTANCE;
    }

    public final void invoke(final CheckoutResponse checkoutResponse) {
        Intrinsics.checkParameterIsNotNull(checkoutResponse, "it");
        this.this$0.showProgress(false);
        OrderSummaryActivity orderSummaryActivity = this.this$0;
        Bundle bundle = null;
        Intent intent = new Intent(orderSummaryActivity, WebViewActivity.class);
        new Function1<Intent, Unit>() {
            public /* bridge */ /* synthetic */ Object invoke(Object obj) {
                invoke((Intent) obj);
                return Unit.INSTANCE;
            }

            public final void invoke(Intent intent) {
                Intrinsics.checkParameterIsNotNull(intent, "$receiver");
                intent.putExtra(Constants.KeyIntent.CHECKOUT_URL, checkoutResponse.getCheckout_url());
            }
        }.invoke(intent);
        if (Build.VERSION.SDK_INT >= 16) {
            orderSummaryActivity.startActivityForResult(intent, Constants.RequestCode.WEB_PAYMENT, bundle);
        } else {
            orderSummaryActivity.startActivityForResult(intent, Constants.RequestCode.WEB_PAYMENT);
        }
    }
}
