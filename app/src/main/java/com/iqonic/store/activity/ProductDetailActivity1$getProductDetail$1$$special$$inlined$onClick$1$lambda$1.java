package com.iqonic.store.activity;

import android.content.Intent;
import com.iqonic.store.utils.Constants;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\u0010\u0000\u001a\u00020\u0001*\u00020\u0002H\n¢\u0006\u0002\b\u0003¨\u0006\u0004"}, d2 = {"<anonymous>", "", "Landroid/content/Intent;", "invoke", "com/iqonic/store/activity/ProductDetailActivity1$getProductDetail$1$7$1"}, k = 3, mv = {1, 1, 16})
/* compiled from: ProductDetailActivity1.kt */
final class ProductDetailActivity1$getProductDetail$1$$special$$inlined$onClick$1$lambda$1 extends Lambda implements Function1<Intent, Unit> {
    final /* synthetic */ ProductDetailActivity1$getProductDetail$1$$special$$inlined$onClick$1 this$0;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    ProductDetailActivity1$getProductDetail$1$$special$$inlined$onClick$1$lambda$1(ProductDetailActivity1$getProductDetail$1$$special$$inlined$onClick$1 productDetailActivity1$getProductDetail$1$$special$$inlined$onClick$1) {
        super(1);
        this.this$0 = productDetailActivity1$getProductDetail$1$$special$$inlined$onClick$1;
    }

    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        invoke((Intent) obj);
        return Unit.INSTANCE;
    }

    public final void invoke(Intent intent) {
        Intrinsics.checkParameterIsNotNull(intent, "$receiver");
        Intent intent2 = this.this$0.this$0.this$0.getIntent();
        Integer valueOf = intent2 != null ? Integer.valueOf(intent2.getIntExtra(Constants.KeyIntent.PRODUCT_ID, 0)) : null;
        if (valueOf == null) {
            Intrinsics.throwNpe();
        }
        intent.putExtra(Constants.KeyIntent.PRODUCT_ID, valueOf.intValue());
    }
}
