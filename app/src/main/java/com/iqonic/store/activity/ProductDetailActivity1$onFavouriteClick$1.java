package com.iqonic.store.activity;

import android.widget.ImageView;
import com.iqonic.store.R;
import com.iqonic.store.utils.extensions.AppExtensionsKt;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n¢\u0006\u0002\b\u0004"}, d2 = {"<anonymous>", "", "it", "", "invoke"}, k = 3, mv = {1, 1, 16})
/* compiled from: ProductDetailActivity1.kt */
final class ProductDetailActivity1$onFavouriteClick$1 extends Lambda implements Function1<Boolean, Unit> {
    final /* synthetic */ ProductDetailActivity1 this$0;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    ProductDetailActivity1$onFavouriteClick$1(ProductDetailActivity1 productDetailActivity1) {
        super(1);
        this.this$0 = productDetailActivity1;
    }

    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        invoke(((Boolean) obj).booleanValue());
        return Unit.INSTANCE;
    }

    public final void invoke(boolean z) {
        ImageView imageView = (ImageView) this.this$0._$_findCachedViewById(R.id.ivFavourite);
        Intrinsics.checkExpressionValueIsNotNull(imageView, "ivFavourite");
        imageView.setClickable(true);
        this.this$0.mIsInWishList = false;
        if (z) {
            this.this$0.changeFavIcon(com.store.proshop.R.drawable.ic_heart, AppExtensionsKt.getPrimaryColor());
        } else {
            this.this$0.changeFavIcon(com.store.proshop.R.drawable.ic_heart_fill, AppExtensionsKt.getPrimaryColor());
        }
    }
}
