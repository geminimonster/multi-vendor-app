package com.iqonic.store.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.iqonic.store.AppBaseActivity;
import com.iqonic.store.adapter.BaseAdapter;
import com.iqonic.store.models.Coupons;
import com.iqonic.store.utils.extensions.AppExtensionsKt;
import com.iqonic.store.utils.extensions.ExtensionsKt;
import com.iqonic.store.utils.extensions.NetworkExtensionKt;
import com.store.proshop.R;
import java.util.HashMap;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\b\u0010\u0006\u001a\u00020\u0007H\u0002J\u0012\u0010\b\u001a\u00020\u00072\b\u0010\t\u001a\u0004\u0018\u00010\nH\u0015R\u0016\u0010\u0003\u001a\n\u0012\u0004\u0012\u00020\u0005\u0018\u00010\u0004X\u000e¢\u0006\u0002\n\u0000¨\u0006\u000b"}, d2 = {"Lcom/iqonic/store/activity/CouponActivity;", "Lcom/iqonic/store/AppBaseActivity;", "()V", "couponsAdapter", "Lcom/iqonic/store/adapter/BaseAdapter;", "Lcom/iqonic/store/models/Coupons;", "changeColor", "", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: CouponActivity.kt */
public final class CouponActivity extends AppBaseActivity {
    private HashMap _$_findViewCache;
    /* access modifiers changed from: private */
    public BaseAdapter<Coupons> couponsAdapter;

    public void _$_clearFindViewByIdCache() {
        HashMap hashMap = this._$_findViewCache;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    public View _$_findCachedViewById(int i) {
        if (this._$_findViewCache == null) {
            this._$_findViewCache = new HashMap();
        }
        View view = (View) this._$_findViewCache.get(Integer.valueOf(i));
        if (view != null) {
            return view;
        }
        View findViewById = findViewById(i);
        this._$_findViewCache.put(Integer.valueOf(i), findViewById);
        return findViewById;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_coupon);
        Toolbar toolbar = (Toolbar) _$_findCachedViewById(com.iqonic.store.R.id.toolbar);
        Intrinsics.checkExpressionValueIsNotNull(toolbar, "toolbar");
        setToolbar(toolbar);
        setTitle(getString(R.string.lbl_available_coupon));
        mAppBarColor();
        changeColor();
        this.couponsAdapter = new BaseAdapter<>(R.layout.item_coupon, new CouponActivity$onCreate$1(this));
        RecyclerView recyclerView = (RecyclerView) _$_findCachedViewById(com.iqonic.store.R.id.rvCoupons);
        Intrinsics.checkExpressionValueIsNotNull(recyclerView, "rvCoupons");
        recyclerView.setLayoutManager(new LinearLayoutManager(this, 1, false));
        RecyclerView recyclerView2 = (RecyclerView) _$_findCachedViewById(com.iqonic.store.R.id.rvCoupons);
        Intrinsics.checkExpressionValueIsNotNull(recyclerView2, "rvCoupons");
        recyclerView2.setAdapter(this.couponsAdapter);
        NetworkExtensionKt.listCoupons(this, new CouponActivity$onCreate$2(this));
        RecyclerView recyclerView3 = (RecyclerView) _$_findCachedViewById(com.iqonic.store.R.id.rvCoupons);
        Intrinsics.checkExpressionValueIsNotNull(recyclerView3, "rvCoupons");
        AppExtensionsKt.rvItemAnimation(recyclerView3);
    }

    private final void changeColor() {
        LinearLayout linearLayout = (LinearLayout) _$_findCachedViewById(com.iqonic.store.R.id.llMain);
        Intrinsics.checkExpressionValueIsNotNull(linearLayout, "llMain");
        ExtensionsKt.changeBackgroundColor(linearLayout);
    }
}
