package com.iqonic.store.activity;

import com.iqonic.store.models.AddCartResponse;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n¢\u0006\u0002\b\u0004"}, d2 = {"<anonymous>", "", "it", "Lcom/iqonic/store/models/AddCartResponse;", "invoke"}, k = 3, mv = {1, 1, 16})
/* compiled from: SearchActivity.kt */
final class SearchActivity$addCart$1 extends Lambda implements Function1<AddCartResponse, Unit> {
    public static final SearchActivity$addCart$1 INSTANCE = new SearchActivity$addCart$1();

    SearchActivity$addCart$1() {
        super(1);
    }

    public final void invoke(AddCartResponse addCartResponse) {
        Intrinsics.checkParameterIsNotNull(addCartResponse, "it");
    }

    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        invoke((AddCartResponse) obj);
        return Unit.INSTANCE;
    }
}
