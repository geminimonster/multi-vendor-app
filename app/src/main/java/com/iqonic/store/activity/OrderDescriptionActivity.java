package com.iqonic.store.activity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.RecyclerView;
import com.iqonic.store.AppBaseActivity;
import com.iqonic.store.adapter.BaseAdapter;
import com.iqonic.store.models.Order;
import com.iqonic.store.models.OrderNotes;
import com.iqonic.store.utils.Constants;
import com.iqonic.store.utils.extensions.AppExtensionsKt;
import com.iqonic.store.utils.extensions.ExtensionsKt;
import com.iqonic.store.utils.extensions.NetworkExtensionKt;
import com.store.proshop.R;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.jvm.internal.Intrinsics;
import org.json.JSONException;
import org.json.JSONObject;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0006\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\b\u0010\u000b\u001a\u00020\fH\u0002J\u0010\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0010H\u0002J\u0012\u0010\u0011\u001a\u00020\f2\b\u0010\u0012\u001a\u0004\u0018\u00010\u0013H\u0015J\b\u0010\u0014\u001a\u00020\fH\u0002J\b\u0010\u0015\u001a\u00020\fH\u0003R\u001e\u0010\u0003\u001a\u0012\u0012\u0004\u0012\u00020\u00050\u0004j\b\u0012\u0004\u0012\u00020\u0005`\u0006X\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\bX.¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u000e¢\u0006\u0002\n\u0000¨\u0006\u0016"}, d2 = {"Lcom/iqonic/store/activity/OrderDescriptionActivity;", "Lcom/iqonic/store/AppBaseActivity;", "()V", "mOrderNoteList", "Ljava/util/ArrayList;", "Lcom/iqonic/store/models/OrderNotes;", "Lkotlin/collections/ArrayList;", "orderModel", "Lcom/iqonic/store/models/Order;", "totalAmt", "", "changeColor", "", "isJSONValid", "", "test", "", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "orderCancelPopup", "setDetail", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: OrderDescriptionActivity.kt */
public final class OrderDescriptionActivity extends AppBaseActivity {
    private HashMap _$_findViewCache;
    /* access modifiers changed from: private */
    public ArrayList<OrderNotes> mOrderNoteList = new ArrayList<>();
    /* access modifiers changed from: private */
    public Order orderModel;
    /* access modifiers changed from: private */
    public double totalAmt;

    public void _$_clearFindViewByIdCache() {
        HashMap hashMap = this._$_findViewCache;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    public View _$_findCachedViewById(int i) {
        if (this._$_findViewCache == null) {
            this._$_findViewCache = new HashMap();
        }
        View view = (View) this._$_findViewCache.get(Integer.valueOf(i));
        if (view != null) {
            return view;
        }
        View findViewById = findViewById(i);
        this._$_findViewCache.put(Integer.valueOf(i), findViewById);
        return findViewById;
    }

    public static final /* synthetic */ Order access$getOrderModel$p(OrderDescriptionActivity orderDescriptionActivity) {
        Order order = orderDescriptionActivity.orderModel;
        if (order == null) {
            Intrinsics.throwUninitializedPropertyAccessException("orderModel");
        }
        return order;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_orderdescription);
        Serializable serializableExtra = getIntent().getSerializableExtra("data");
        if (serializableExtra != null) {
            this.orderModel = (Order) serializableExtra;
            StringBuilder sb = new StringBuilder();
            sb.append(getString(R.string.lbl_order_title));
            Order order = this.orderModel;
            if (order == null) {
                Intrinsics.throwUninitializedPropertyAccessException("orderModel");
            }
            sb.append(String.valueOf(order.getId()));
            sb.append(" ");
            sb.append(getString(R.string.lbl_details));
            setTitle(sb.toString());
            Toolbar toolbar = (Toolbar) _$_findCachedViewById(com.iqonic.store.R.id.toolbar);
            Intrinsics.checkExpressionValueIsNotNull(toolbar, "toolbar");
            setToolbar(toolbar);
            mAppBarColor();
            changeColor();
            BaseAdapter baseAdapter = new BaseAdapter(R.layout.item_order, new OrderDescriptionActivity$onCreate$mOrderItemAdapter$1(this));
            BaseAdapter baseAdapter2 = new BaseAdapter(R.layout.item_track, new OrderDescriptionActivity$onCreate$mOrderNotesAdapter$1(this));
            RecyclerView recyclerView = (RecyclerView) _$_findCachedViewById(com.iqonic.store.R.id.rvOrderItems);
            Intrinsics.checkExpressionValueIsNotNull(recyclerView, "rvOrderItems");
            ExtensionsKt.setVerticalLayout$default(recyclerView, false, 1, (Object) null);
            RecyclerView recyclerView2 = (RecyclerView) _$_findCachedViewById(com.iqonic.store.R.id.rvOrderItems);
            Intrinsics.checkExpressionValueIsNotNull(recyclerView2, "rvOrderItems");
            recyclerView2.setAdapter(baseAdapter);
            Order order2 = this.orderModel;
            if (order2 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("orderModel");
            }
            baseAdapter.addItems(order2.getLine_items());
            setDetail();
            RecyclerView recyclerView3 = (RecyclerView) _$_findCachedViewById(com.iqonic.store.R.id.rvOrderNotes);
            Intrinsics.checkExpressionValueIsNotNull(recyclerView3, "rvOrderNotes");
            ExtensionsKt.setVerticalLayout$default(recyclerView3, false, 1, (Object) null);
            RecyclerView recyclerView4 = (RecyclerView) _$_findCachedViewById(com.iqonic.store.R.id.rvOrderNotes);
            Intrinsics.checkExpressionValueIsNotNull(recyclerView4, "rvOrderNotes");
            recyclerView4.setAdapter(baseAdapter2);
            Order order3 = this.orderModel;
            if (order3 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("orderModel");
            }
            NetworkExtensionKt.getOrderTracking(this, order3.getId(), new OrderDescriptionActivity$onCreate$1(this, baseAdapter2));
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm a");
            Order order4 = this.orderModel;
            if (order4 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("orderModel");
            }
            Date parse = simpleDateFormat.parse(AppExtensionsKt.convertOrderDataToLocalDate(order4.getDate_created().getDate()));
            Calendar instance = Calendar.getInstance();
            Intrinsics.checkExpressionValueIsNotNull(instance, "Calendar.getInstance()");
            instance.setTime(parse);
            instance.add(10, 1);
            Date parse2 = simpleDateFormat.parse(simpleDateFormat.format(new Date()));
            Intrinsics.checkExpressionValueIsNotNull(parse2, "sdf.parse(sdf.format(Date()))");
            Order order5 = this.orderModel;
            if (order5 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("orderModel");
            }
            if (!Intrinsics.areEqual((Object) order5.getStatus(), (Object) Constants.OrderStatus.COMPLETED)) {
                Order order6 = this.orderModel;
                if (order6 == null) {
                    Intrinsics.throwUninitializedPropertyAccessException("orderModel");
                }
                if (!Intrinsics.areEqual((Object) order6.getStatus(), (Object) Constants.OrderStatus.REFUNDED)) {
                    Order order7 = this.orderModel;
                    if (order7 == null) {
                        Intrinsics.throwUninitializedPropertyAccessException("orderModel");
                    }
                    if (!Intrinsics.areEqual((Object) order7.getStatus(), (Object) "cancelled")) {
                        if (parse2.before(instance.getTime())) {
                            ImageView imageView = (ImageView) _$_findCachedViewById(com.iqonic.store.R.id.imgMore);
                            Intrinsics.checkExpressionValueIsNotNull(imageView, "imgMore");
                            imageView.setVisibility(0);
                            TextView textView = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.cancelOrder);
                            Intrinsics.checkExpressionValueIsNotNull(textView, "cancelOrder");
                            textView.setVisibility(0);
                        } else {
                            TextView textView2 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.cancelOrder);
                            Intrinsics.checkExpressionValueIsNotNull(textView2, "cancelOrder");
                            textView2.setVisibility(8);
                            ImageView imageView2 = (ImageView) _$_findCachedViewById(com.iqonic.store.R.id.imgMore);
                            Intrinsics.checkExpressionValueIsNotNull(imageView2, "imgMore");
                            imageView2.setVisibility(8);
                        }
                        TextView textView3 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.cancelOrder);
                        textView3.setOnClickListener(new OrderDescriptionActivity$onCreate$$inlined$onClick$1(textView3, this));
                        return;
                    }
                }
            }
            TextView textView4 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.cancelOrder);
            Intrinsics.checkExpressionValueIsNotNull(textView4, "cancelOrder");
            textView4.setVisibility(8);
            ImageView imageView3 = (ImageView) _$_findCachedViewById(com.iqonic.store.R.id.imgMore);
            Intrinsics.checkExpressionValueIsNotNull(imageView3, "imgMore");
            imageView3.setVisibility(8);
            TextView textView32 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.cancelOrder);
            textView32.setOnClickListener(new OrderDescriptionActivity$onCreate$$inlined$onClick$1(textView32, this));
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.iqonic.store.models.Order");
    }

    /* access modifiers changed from: private */
    public final void orderCancelPopup() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle((CharSequence) getString(R.string.lbl_cancel_order));
        View inflate = getLayoutInflater().inflate(R.layout.dialog_order_cancel, (ViewGroup) null);
        Intrinsics.checkExpressionValueIsNotNull(inflate, "layoutInflater.inflate(R…ialog_order_cancel, null)");
        ArrayList arrayList = new ArrayList();
        arrayList.add(getString(R.string.order_cancel_1));
        arrayList.add(getString(R.string.order_cancel_2));
        arrayList.add(getString(R.string.order_cancel_3));
        arrayList.add(getString(R.string.order_cancel_4));
        arrayList.add(getString(R.string.order_cancel_5));
        arrayList.add(getString(R.string.order_cancel_6));
        ArrayAdapter arrayAdapter = new ArrayAdapter(getApplicationContext(), R.layout.spinner_items, arrayList);
        Spinner spinner = (Spinner) inflate.findViewById(com.iqonic.store.R.id.spinner);
        Intrinsics.checkExpressionValueIsNotNull(spinner, "customLayout.spinner");
        spinner.setAdapter(arrayAdapter);
        builder.setView(inflate);
        builder.setPositiveButton((CharSequence) getString(R.string.lbl_cancel_order), (DialogInterface.OnClickListener) new OrderDescriptionActivity$orderCancelPopup$1(this, inflate));
        builder.setNegativeButton((CharSequence) getString(R.string.lbl_close), (DialogInterface.OnClickListener) OrderDescriptionActivity$orderCancelPopup$2.INSTANCE);
        builder.show();
    }

    /* access modifiers changed from: private */
    public final boolean isJSONValid(String str) {
        try {
            new JSONObject(str);
            return true;
        } catch (JSONException unused) {
            return false;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:21:0x0036 A[Catch:{ ParseException -> 0x012a }] */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00a1 A[Catch:{ ParseException -> 0x012a }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final void setDetail() {
        /*
            r13 = this;
            java.lang.String r0 = " "
            java.lang.String r1 = "orderModel"
            r2 = 0
            r3 = 1
            com.iqonic.store.models.Order r4 = r13.orderModel     // Catch:{ ParseException -> 0x012a }
            if (r4 != 0) goto L_0x000d
            kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException(r1)     // Catch:{ ParseException -> 0x012a }
        L_0x000d:
            com.iqonic.store.models.DatePaid r4 = r4.getDate_paid()     // Catch:{ ParseException -> 0x012a }
            r5 = 2131821014(0x7f1101d6, float:1.927476E38)
            java.lang.String r6 = "tvOrderId"
            if (r4 == 0) goto L_0x00f8
            com.iqonic.store.models.Order r4 = r13.orderModel     // Catch:{ ParseException -> 0x012a }
            if (r4 != 0) goto L_0x001f
            kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException(r1)     // Catch:{ ParseException -> 0x012a }
        L_0x001f:
            java.lang.String r4 = r4.getTransaction_id()     // Catch:{ ParseException -> 0x012a }
            java.lang.CharSequence r4 = (java.lang.CharSequence) r4     // Catch:{ ParseException -> 0x012a }
            if (r4 == 0) goto L_0x0030
            boolean r4 = kotlin.text.StringsKt.isBlank(r4)     // Catch:{ ParseException -> 0x012a }
            if (r4 == 0) goto L_0x002e
            goto L_0x0030
        L_0x002e:
            r4 = 0
            goto L_0x0031
        L_0x0030:
            r4 = 1
        L_0x0031:
            r7 = 2131820933(0x7f110185, float:1.9274595E38)
            if (r4 == 0) goto L_0x00a1
            int r4 = com.iqonic.store.R.id.tvOrderId     // Catch:{ ParseException -> 0x012a }
            android.view.View r4 = r13._$_findCachedViewById(r4)     // Catch:{ ParseException -> 0x012a }
            android.widget.TextView r4 = (android.widget.TextView) r4     // Catch:{ ParseException -> 0x012a }
            kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r4, r6)     // Catch:{ ParseException -> 0x012a }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ ParseException -> 0x012a }
            r6.<init>()     // Catch:{ ParseException -> 0x012a }
            java.lang.String r5 = r13.getString(r5)     // Catch:{ ParseException -> 0x012a }
            r6.append(r5)     // Catch:{ ParseException -> 0x012a }
            r6.append(r0)     // Catch:{ ParseException -> 0x012a }
            com.iqonic.store.models.Order r5 = r13.orderModel     // Catch:{ ParseException -> 0x012a }
            if (r5 != 0) goto L_0x0057
            kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException(r1)     // Catch:{ ParseException -> 0x012a }
        L_0x0057:
            java.lang.String r5 = r5.getPayment_method()     // Catch:{ ParseException -> 0x012a }
            r6.append(r5)     // Catch:{ ParseException -> 0x012a }
            java.lang.String r5 = " ("
            r6.append(r5)     // Catch:{ ParseException -> 0x012a }
            com.iqonic.store.models.Order r5 = r13.orderModel     // Catch:{ ParseException -> 0x012a }
            if (r5 != 0) goto L_0x006a
            kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException(r1)     // Catch:{ ParseException -> 0x012a }
        L_0x006a:
            java.lang.String r5 = r5.getTransaction_id()     // Catch:{ ParseException -> 0x012a }
            r6.append(r5)     // Catch:{ ParseException -> 0x012a }
            java.lang.String r5 = "). "
            r6.append(r5)     // Catch:{ ParseException -> 0x012a }
            java.lang.String r5 = r13.getString(r7)     // Catch:{ ParseException -> 0x012a }
            r6.append(r5)     // Catch:{ ParseException -> 0x012a }
            r6.append(r0)     // Catch:{ ParseException -> 0x012a }
            com.iqonic.store.models.Order r5 = r13.orderModel     // Catch:{ ParseException -> 0x012a }
            if (r5 != 0) goto L_0x0087
            kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException(r1)     // Catch:{ ParseException -> 0x012a }
        L_0x0087:
            com.iqonic.store.models.DatePaid r5 = r5.getDate_paid()     // Catch:{ ParseException -> 0x012a }
            java.lang.String r5 = r5.getDate()     // Catch:{ ParseException -> 0x012a }
            java.lang.String r5 = com.iqonic.store.utils.extensions.AppExtensionsKt.convertOrderDataToLocalDate(r5)     // Catch:{ ParseException -> 0x012a }
            r6.append(r5)     // Catch:{ ParseException -> 0x012a }
            java.lang.String r5 = r6.toString()     // Catch:{ ParseException -> 0x012a }
            java.lang.CharSequence r5 = (java.lang.CharSequence) r5     // Catch:{ ParseException -> 0x012a }
            r4.setText(r5)     // Catch:{ ParseException -> 0x012a }
            goto L_0x012e
        L_0x00a1:
            int r4 = com.iqonic.store.R.id.tvOrderId     // Catch:{ ParseException -> 0x012a }
            android.view.View r4 = r13._$_findCachedViewById(r4)     // Catch:{ ParseException -> 0x012a }
            android.widget.TextView r4 = (android.widget.TextView) r4     // Catch:{ ParseException -> 0x012a }
            kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r4, r6)     // Catch:{ ParseException -> 0x012a }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ ParseException -> 0x012a }
            r6.<init>()     // Catch:{ ParseException -> 0x012a }
            java.lang.String r5 = r13.getString(r5)     // Catch:{ ParseException -> 0x012a }
            r6.append(r5)     // Catch:{ ParseException -> 0x012a }
            r6.append(r0)     // Catch:{ ParseException -> 0x012a }
            com.iqonic.store.models.Order r5 = r13.orderModel     // Catch:{ ParseException -> 0x012a }
            if (r5 != 0) goto L_0x00c2
            kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException(r1)     // Catch:{ ParseException -> 0x012a }
        L_0x00c2:
            java.lang.String r5 = r5.getPayment_method()     // Catch:{ ParseException -> 0x012a }
            r6.append(r5)     // Catch:{ ParseException -> 0x012a }
            java.lang.String r5 = ". "
            r6.append(r5)     // Catch:{ ParseException -> 0x012a }
            java.lang.String r5 = r13.getString(r7)     // Catch:{ ParseException -> 0x012a }
            r6.append(r5)     // Catch:{ ParseException -> 0x012a }
            r6.append(r0)     // Catch:{ ParseException -> 0x012a }
            com.iqonic.store.models.Order r5 = r13.orderModel     // Catch:{ ParseException -> 0x012a }
            if (r5 != 0) goto L_0x00df
            kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException(r1)     // Catch:{ ParseException -> 0x012a }
        L_0x00df:
            com.iqonic.store.models.DatePaid r5 = r5.getDate_paid()     // Catch:{ ParseException -> 0x012a }
            java.lang.String r5 = r5.getDate()     // Catch:{ ParseException -> 0x012a }
            java.lang.String r5 = com.iqonic.store.utils.extensions.AppExtensionsKt.convertOrderDataToLocalDate(r5)     // Catch:{ ParseException -> 0x012a }
            r6.append(r5)     // Catch:{ ParseException -> 0x012a }
            java.lang.String r5 = r6.toString()     // Catch:{ ParseException -> 0x012a }
            java.lang.CharSequence r5 = (java.lang.CharSequence) r5     // Catch:{ ParseException -> 0x012a }
            r4.setText(r5)     // Catch:{ ParseException -> 0x012a }
            goto L_0x012e
        L_0x00f8:
            int r4 = com.iqonic.store.R.id.tvOrderId     // Catch:{ ParseException -> 0x012a }
            android.view.View r4 = r13._$_findCachedViewById(r4)     // Catch:{ ParseException -> 0x012a }
            android.widget.TextView r4 = (android.widget.TextView) r4     // Catch:{ ParseException -> 0x012a }
            kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r4, r6)     // Catch:{ ParseException -> 0x012a }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ ParseException -> 0x012a }
            r6.<init>()     // Catch:{ ParseException -> 0x012a }
            java.lang.String r5 = r13.getString(r5)     // Catch:{ ParseException -> 0x012a }
            r6.append(r5)     // Catch:{ ParseException -> 0x012a }
            r6.append(r0)     // Catch:{ ParseException -> 0x012a }
            com.iqonic.store.models.Order r5 = r13.orderModel     // Catch:{ ParseException -> 0x012a }
            if (r5 != 0) goto L_0x0119
            kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException(r1)     // Catch:{ ParseException -> 0x012a }
        L_0x0119:
            java.lang.String r5 = r5.getPayment_method()     // Catch:{ ParseException -> 0x012a }
            r6.append(r5)     // Catch:{ ParseException -> 0x012a }
            java.lang.String r5 = r6.toString()     // Catch:{ ParseException -> 0x012a }
            java.lang.CharSequence r5 = (java.lang.CharSequence) r5     // Catch:{ ParseException -> 0x012a }
            r4.setText(r5)     // Catch:{ ParseException -> 0x012a }
            goto L_0x012e
        L_0x012a:
            r4 = move-exception
            r4.printStackTrace()
        L_0x012e:
            int r4 = com.iqonic.store.R.id.tvProductName
            android.view.View r4 = r13._$_findCachedViewById(r4)
            android.widget.TextView r4 = (android.widget.TextView) r4
            java.lang.String r5 = "tvProductName"
            kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r4, r5)
            com.iqonic.store.models.Order r5 = r13.orderModel
            if (r5 != 0) goto L_0x0142
            kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException(r1)
        L_0x0142:
            java.util.List r5 = r5.getLine_items()
            java.lang.Object r5 = r5.get(r2)
            com.iqonic.store.models.LineItems r5 = (com.iqonic.store.models.LineItems) r5
            java.lang.String r5 = r5.getName()
            java.lang.CharSequence r5 = (java.lang.CharSequence) r5
            r4.setText(r5)
            int r4 = com.iqonic.store.R.id.tvProductSellerName
            android.view.View r4 = r13._$_findCachedViewById(r4)
            android.widget.TextView r4 = (android.widget.TextView) r4
            java.lang.String r5 = "tvProductSellerName"
            kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r4, r5)
            com.iqonic.store.models.Order r5 = r13.orderModel
            if (r5 != 0) goto L_0x0169
            kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException(r1)
        L_0x0169:
            com.iqonic.store.models.Shipping r5 = r5.getShipping()
            java.lang.String r5 = r5.getFirst_name()
            java.lang.CharSequence r5 = (java.lang.CharSequence) r5
            r4.setText(r5)
            int r4 = com.iqonic.store.R.id.tvPrice
            android.view.View r4 = r13._$_findCachedViewById(r4)
            android.widget.TextView r4 = (android.widget.TextView) r4
            java.lang.String r5 = "tvPrice"
            kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r4, r5)
            com.iqonic.store.models.Order r5 = r13.orderModel
            if (r5 != 0) goto L_0x018a
            kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException(r1)
        L_0x018a:
            java.util.List r5 = r5.getLine_items()
            java.lang.Object r5 = r5.get(r2)
            com.iqonic.store.models.LineItems r5 = (com.iqonic.store.models.LineItems) r5
            java.lang.String r5 = r5.getTotal()
            r6 = 0
            java.lang.String r5 = com.iqonic.store.utils.extensions.StringExtensionsKt.currencyFormat$default(r5, r6, r3, r6)
            java.lang.CharSequence r5 = (java.lang.CharSequence) r5
            r4.setText(r5)
            int r4 = com.iqonic.store.R.id.ivProduct
            android.view.View r4 = r13._$_findCachedViewById(r4)
            r7 = r4
            android.widget.ImageView r7 = (android.widget.ImageView) r7
            java.lang.String r4 = "ivProduct"
            kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r7, r4)
            com.iqonic.store.models.Order r4 = r13.orderModel
            if (r4 != 0) goto L_0x01b7
            kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException(r1)
        L_0x01b7:
            java.util.List r4 = r4.getLine_items()
            java.lang.Object r4 = r4.get(r2)
            com.iqonic.store.models.LineItems r4 = (com.iqonic.store.models.LineItems) r4
            java.util.List r4 = r4.getProduct_images()
            java.lang.Object r4 = r4.get(r2)
            com.iqonic.store.models.ProductImage r4 = (com.iqonic.store.models.ProductImage) r4
            java.lang.String r8 = r4.getSrc()
            r9 = 0
            r10 = 0
            r11 = 6
            r12 = 0
            com.iqonic.store.utils.extensions.NetworkExtensionKt.loadImageFromUrl$default(r7, r8, r9, r10, r11, r12)
            int r4 = com.iqonic.store.R.id.tvUsername
            android.view.View r4 = r13._$_findCachedViewById(r4)
            android.widget.TextView r4 = (android.widget.TextView) r4
            java.lang.String r5 = "tvUsername"
            kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r4, r5)
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            com.iqonic.store.models.Order r7 = r13.orderModel
            if (r7 != 0) goto L_0x01ef
            kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException(r1)
        L_0x01ef:
            com.iqonic.store.models.Shipping r7 = r7.getShipping()
            java.lang.String r7 = r7.getFirst_name()
            r5.append(r7)
            r5.append(r0)
            com.iqonic.store.models.Order r7 = r13.orderModel
            if (r7 != 0) goto L_0x0204
            kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException(r1)
        L_0x0204:
            com.iqonic.store.models.Shipping r7 = r7.getShipping()
            java.lang.String r7 = r7.getLast_name()
            r5.append(r7)
            java.lang.String r5 = r5.toString()
            java.lang.CharSequence r5 = (java.lang.CharSequence) r5
            r4.setText(r5)
            com.iqonic.store.models.Order r4 = r13.orderModel
            if (r4 != 0) goto L_0x021f
            kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException(r1)
        L_0x021f:
            com.iqonic.store.models.Shipping r4 = r4.getShipping()
            java.lang.String r4 = r4.getAddress_1()
            java.lang.CharSequence r4 = (java.lang.CharSequence) r4
            boolean r4 = kotlin.text.StringsKt.isBlank(r4)
            r4 = r4 ^ r3
            java.lang.String r5 = ""
            java.lang.String r7 = "\n"
            if (r4 == 0) goto L_0x0270
            r4 = r5
            java.lang.CharSequence r4 = (java.lang.CharSequence) r4
            boolean r4 = kotlin.text.StringsKt.isBlank(r4)
            r4 = r4 ^ r3
            if (r4 == 0) goto L_0x0260
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            r4.append(r5)
            r4.append(r7)
            com.iqonic.store.models.Order r8 = r13.orderModel
            if (r8 != 0) goto L_0x0250
            kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException(r1)
        L_0x0250:
            com.iqonic.store.models.Shipping r8 = r8.getShipping()
            java.lang.String r8 = r8.getAddress_1()
            r4.append(r8)
            java.lang.String r4 = r4.toString()
            goto L_0x0271
        L_0x0260:
            com.iqonic.store.models.Order r4 = r13.orderModel
            if (r4 != 0) goto L_0x0267
            kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException(r1)
        L_0x0267:
            com.iqonic.store.models.Shipping r4 = r4.getShipping()
            java.lang.String r4 = r4.getAddress_1()
            goto L_0x0271
        L_0x0270:
            r4 = r5
        L_0x0271:
            com.iqonic.store.models.Order r8 = r13.orderModel
            if (r8 != 0) goto L_0x0278
            kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException(r1)
        L_0x0278:
            com.iqonic.store.models.Shipping r8 = r8.getShipping()
            java.lang.String r8 = r8.getAddress_2()
            java.lang.CharSequence r8 = (java.lang.CharSequence) r8
            boolean r8 = kotlin.text.StringsKt.isBlank(r8)
            r8 = r8 ^ r3
            if (r8 == 0) goto L_0x02c4
            r8 = r4
            java.lang.CharSequence r8 = (java.lang.CharSequence) r8
            boolean r8 = kotlin.text.StringsKt.isBlank(r8)
            r8 = r8 ^ r3
            if (r8 == 0) goto L_0x02b5
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            r8.<init>()
            r8.append(r4)
            r8.append(r7)
            com.iqonic.store.models.Order r4 = r13.orderModel
            if (r4 != 0) goto L_0x02a5
            kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException(r1)
        L_0x02a5:
            com.iqonic.store.models.Shipping r4 = r4.getShipping()
            java.lang.String r4 = r4.getAddress_2()
            r8.append(r4)
            java.lang.String r4 = r8.toString()
            goto L_0x02c4
        L_0x02b5:
            com.iqonic.store.models.Order r4 = r13.orderModel
            if (r4 != 0) goto L_0x02bc
            kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException(r1)
        L_0x02bc:
            com.iqonic.store.models.Shipping r4 = r4.getShipping()
            java.lang.String r4 = r4.getAddress_2()
        L_0x02c4:
            com.iqonic.store.models.Order r8 = r13.orderModel
            if (r8 != 0) goto L_0x02cb
            kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException(r1)
        L_0x02cb:
            com.iqonic.store.models.Shipping r8 = r8.getShipping()
            java.lang.String r8 = r8.getCity()
            java.lang.CharSequence r8 = (java.lang.CharSequence) r8
            int r8 = r8.length()
            if (r8 <= 0) goto L_0x02dd
            r8 = 1
            goto L_0x02de
        L_0x02dd:
            r8 = 0
        L_0x02de:
            if (r8 == 0) goto L_0x031b
            r8 = r4
            java.lang.CharSequence r8 = (java.lang.CharSequence) r8
            boolean r8 = kotlin.text.StringsKt.isBlank(r8)
            r8 = r8 ^ r3
            if (r8 == 0) goto L_0x030c
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            r8.<init>()
            r8.append(r4)
            r8.append(r7)
            com.iqonic.store.models.Order r4 = r13.orderModel
            if (r4 != 0) goto L_0x02fc
            kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException(r1)
        L_0x02fc:
            com.iqonic.store.models.Shipping r4 = r4.getShipping()
            java.lang.String r4 = r4.getCity()
            r8.append(r4)
            java.lang.String r4 = r8.toString()
            goto L_0x031b
        L_0x030c:
            com.iqonic.store.models.Order r4 = r13.orderModel
            if (r4 != 0) goto L_0x0313
            kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException(r1)
        L_0x0313:
            com.iqonic.store.models.Shipping r4 = r4.getShipping()
            java.lang.String r4 = r4.getCity()
        L_0x031b:
            com.iqonic.store.models.Order r8 = r13.orderModel
            if (r8 != 0) goto L_0x0322
            kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException(r1)
        L_0x0322:
            com.iqonic.store.models.Shipping r8 = r8.getShipping()
            java.lang.String r8 = r8.getPostcode()
            java.lang.CharSequence r8 = (java.lang.CharSequence) r8
            int r8 = r8.length()
            if (r8 <= 0) goto L_0x0334
            r8 = 1
            goto L_0x0335
        L_0x0334:
            r8 = 0
        L_0x0335:
            java.lang.String r9 = " - "
            if (r8 == 0) goto L_0x0374
            r8 = r4
            java.lang.CharSequence r8 = (java.lang.CharSequence) r8
            boolean r8 = kotlin.text.StringsKt.isBlank(r8)
            r8 = r8 ^ r3
            if (r8 == 0) goto L_0x0365
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            r8.<init>()
            r8.append(r4)
            r8.append(r9)
            com.iqonic.store.models.Order r4 = r13.orderModel
            if (r4 != 0) goto L_0x0355
            kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException(r1)
        L_0x0355:
            com.iqonic.store.models.Shipping r4 = r4.getShipping()
            java.lang.String r4 = r4.getPostcode()
            r8.append(r4)
            java.lang.String r4 = r8.toString()
            goto L_0x0374
        L_0x0365:
            com.iqonic.store.models.Order r4 = r13.orderModel
            if (r4 != 0) goto L_0x036c
            kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException(r1)
        L_0x036c:
            com.iqonic.store.models.Shipping r4 = r4.getShipping()
            java.lang.String r4 = r4.getPostcode()
        L_0x0374:
            com.iqonic.store.models.Order r8 = r13.orderModel
            if (r8 != 0) goto L_0x037b
            kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException(r1)
        L_0x037b:
            com.iqonic.store.models.Shipping r8 = r8.getShipping()
            java.lang.String r8 = r8.getState()
            java.lang.CharSequence r8 = (java.lang.CharSequence) r8
            int r8 = r8.length()
            if (r8 <= 0) goto L_0x038d
            r8 = 1
            goto L_0x038e
        L_0x038d:
            r8 = 0
        L_0x038e:
            if (r8 == 0) goto L_0x03cb
            r8 = r4
            java.lang.CharSequence r8 = (java.lang.CharSequence) r8
            boolean r8 = kotlin.text.StringsKt.isBlank(r8)
            r8 = r8 ^ r3
            if (r8 == 0) goto L_0x03bc
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            r8.<init>()
            r8.append(r4)
            r8.append(r7)
            com.iqonic.store.models.Order r4 = r13.orderModel
            if (r4 != 0) goto L_0x03ac
            kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException(r1)
        L_0x03ac:
            com.iqonic.store.models.Shipping r4 = r4.getShipping()
            java.lang.String r4 = r4.getState()
            r8.append(r4)
            java.lang.String r4 = r8.toString()
            goto L_0x03cb
        L_0x03bc:
            com.iqonic.store.models.Order r4 = r13.orderModel
            if (r4 != 0) goto L_0x03c3
            kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException(r1)
        L_0x03c3:
            com.iqonic.store.models.Shipping r4 = r4.getShipping()
            java.lang.String r4 = r4.getState()
        L_0x03cb:
            com.iqonic.store.models.Order r8 = r13.orderModel
            if (r8 != 0) goto L_0x03d2
            kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException(r1)
        L_0x03d2:
            com.iqonic.store.models.Shipping r8 = r8.getShipping()
            java.lang.String r8 = r8.getCountry()
            java.lang.CharSequence r8 = (java.lang.CharSequence) r8
            int r8 = r8.length()
            if (r8 <= 0) goto L_0x03e3
            r2 = 1
        L_0x03e3:
            if (r2 == 0) goto L_0x0421
            r2 = r4
            java.lang.CharSequence r2 = (java.lang.CharSequence) r2
            boolean r2 = kotlin.text.StringsKt.isBlank(r2)
            r2 = r2 ^ r3
            if (r2 == 0) goto L_0x0411
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            r2.append(r4)
            r2.append(r7)
            com.iqonic.store.models.Order r4 = r13.orderModel
            if (r4 != 0) goto L_0x0401
            kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException(r1)
        L_0x0401:
            com.iqonic.store.models.Shipping r4 = r4.getShipping()
            java.lang.String r4 = r4.getCountry()
            r2.append(r4)
            java.lang.String r2 = r2.toString()
            goto L_0x0420
        L_0x0411:
            com.iqonic.store.models.Order r2 = r13.orderModel
            if (r2 != 0) goto L_0x0418
            kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException(r1)
        L_0x0418:
            com.iqonic.store.models.Shipping r2 = r2.getShipping()
            java.lang.String r2 = r2.getCountry()
        L_0x0420:
            r4 = r2
        L_0x0421:
            com.iqonic.store.models.Order r2 = r13.orderModel
            if (r2 != 0) goto L_0x0428
            kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException(r1)
        L_0x0428:
            com.iqonic.store.models.Shipping r2 = r2.getShipping()
            java.lang.String r2 = r2.getPhone()
            java.lang.CharSequence r2 = (java.lang.CharSequence) r2
            boolean r2 = kotlin.text.StringsKt.isBlank(r2)
            r2 = r2 ^ r3
            r8 = 2131820944(0x7f110190, float:1.9274617E38)
            if (r2 == 0) goto L_0x047f
            r2 = r4
            java.lang.CharSequence r2 = (java.lang.CharSequence) r2
            boolean r2 = kotlin.text.StringsKt.isBlank(r2)
            r2 = r2 ^ r3
            if (r2 == 0) goto L_0x046f
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            r2.append(r4)
            r2.append(r7)
            java.lang.String r4 = r13.getString(r8)
            r2.append(r4)
            com.iqonic.store.models.Order r4 = r13.orderModel
            if (r4 != 0) goto L_0x045f
            kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException(r1)
        L_0x045f:
            com.iqonic.store.models.Shipping r4 = r4.getShipping()
            java.lang.String r4 = r4.getPhone()
            r2.append(r4)
            java.lang.String r2 = r2.toString()
            goto L_0x047e
        L_0x046f:
            com.iqonic.store.models.Order r2 = r13.orderModel
            if (r2 != 0) goto L_0x0476
            kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException(r1)
        L_0x0476:
            com.iqonic.store.models.Shipping r2 = r2.getShipping()
            java.lang.String r2 = r2.getPhone()
        L_0x047e:
            r4 = r2
        L_0x047f:
            com.iqonic.store.models.Order r2 = r13.orderModel
            if (r2 != 0) goto L_0x0486
            kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException(r1)
        L_0x0486:
            com.iqonic.store.models.Billing r2 = r2.getBilling()
            java.lang.String r2 = r2.getPhone()
            java.lang.CharSequence r2 = (java.lang.CharSequence) r2
            boolean r2 = kotlin.text.StringsKt.isBlank(r2)
            r2 = r2 ^ r3
            if (r2 == 0) goto L_0x04da
            r2 = r4
            java.lang.CharSequence r2 = (java.lang.CharSequence) r2
            boolean r2 = kotlin.text.StringsKt.isBlank(r2)
            r2 = r2 ^ r3
            if (r2 == 0) goto L_0x04ca
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            r2.append(r4)
            r2.append(r7)
            java.lang.String r4 = r13.getString(r8)
            r2.append(r4)
            com.iqonic.store.models.Order r4 = r13.orderModel
            if (r4 != 0) goto L_0x04ba
            kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException(r1)
        L_0x04ba:
            com.iqonic.store.models.Billing r4 = r4.getBilling()
            java.lang.String r4 = r4.getPhone()
            r2.append(r4)
            java.lang.String r2 = r2.toString()
            goto L_0x04d9
        L_0x04ca:
            com.iqonic.store.models.Order r2 = r13.orderModel
            if (r2 != 0) goto L_0x04d1
            kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException(r1)
        L_0x04d1:
            com.iqonic.store.models.Billing r2 = r2.getBilling()
            java.lang.String r2 = r2.getPhone()
        L_0x04d9:
            r4 = r2
        L_0x04da:
            int r2 = com.iqonic.store.R.id.tvUserAddress
            android.view.View r2 = r13._$_findCachedViewById(r2)
            android.widget.TextView r2 = (android.widget.TextView) r2
            java.lang.String r8 = "tvUserAddress"
            kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r2, r8)
            java.lang.CharSequence r4 = (java.lang.CharSequence) r4
            r2.setText(r4)
            int r2 = com.iqonic.store.R.id.tvBillingUsername
            android.view.View r2 = r13._$_findCachedViewById(r2)
            android.widget.TextView r2 = (android.widget.TextView) r2
            java.lang.String r4 = "tvBillingUsername"
            kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r2, r4)
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            com.iqonic.store.models.Order r8 = r13.orderModel
            if (r8 != 0) goto L_0x0505
            kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException(r1)
        L_0x0505:
            com.iqonic.store.models.Billing r8 = r8.getBilling()
            java.lang.String r8 = r8.getFirst_name()
            r4.append(r8)
            r4.append(r0)
            com.iqonic.store.models.Order r0 = r13.orderModel
            if (r0 != 0) goto L_0x051a
            kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException(r1)
        L_0x051a:
            com.iqonic.store.models.Billing r0 = r0.getBilling()
            java.lang.String r0 = r0.getLast_name()
            r4.append(r0)
            java.lang.String r0 = r4.toString()
            java.lang.CharSequence r0 = (java.lang.CharSequence) r0
            r2.setText(r0)
            com.iqonic.store.models.Order r0 = r13.orderModel
            if (r0 != 0) goto L_0x0535
            kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException(r1)
        L_0x0535:
            com.iqonic.store.models.Billing r0 = r0.getBilling()
            java.lang.String r0 = r0.getAddress_1()
            java.lang.CharSequence r0 = (java.lang.CharSequence) r0
            boolean r0 = kotlin.text.StringsKt.isBlank(r0)
            r0 = r0 ^ r3
            if (r0 == 0) goto L_0x0582
            r0 = r5
            java.lang.CharSequence r0 = (java.lang.CharSequence) r0
            boolean r0 = kotlin.text.StringsKt.isBlank(r0)
            r0 = r0 ^ r3
            if (r0 == 0) goto L_0x0572
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            r0.append(r5)
            r0.append(r7)
            com.iqonic.store.models.Order r2 = r13.orderModel
            if (r2 != 0) goto L_0x0562
            kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException(r1)
        L_0x0562:
            com.iqonic.store.models.Billing r2 = r2.getBilling()
            java.lang.String r2 = r2.getAddress_1()
            r0.append(r2)
            java.lang.String r0 = r0.toString()
            goto L_0x0581
        L_0x0572:
            com.iqonic.store.models.Order r0 = r13.orderModel
            if (r0 != 0) goto L_0x0579
            kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException(r1)
        L_0x0579:
            com.iqonic.store.models.Billing r0 = r0.getBilling()
            java.lang.String r0 = r0.getAddress_1()
        L_0x0581:
            r5 = r0
        L_0x0582:
            com.iqonic.store.models.Order r0 = r13.orderModel
            if (r0 != 0) goto L_0x0589
            kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException(r1)
        L_0x0589:
            com.iqonic.store.models.Billing r0 = r0.getBilling()
            java.lang.String r0 = r0.getAddress_2()
            java.lang.CharSequence r0 = (java.lang.CharSequence) r0
            boolean r0 = kotlin.text.StringsKt.isBlank(r0)
            r0 = r0 ^ r3
            if (r0 == 0) goto L_0x05d6
            r0 = r5
            java.lang.CharSequence r0 = (java.lang.CharSequence) r0
            boolean r0 = kotlin.text.StringsKt.isBlank(r0)
            r0 = r0 ^ r3
            if (r0 == 0) goto L_0x05c6
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            r0.append(r5)
            r0.append(r7)
            com.iqonic.store.models.Order r2 = r13.orderModel
            if (r2 != 0) goto L_0x05b6
            kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException(r1)
        L_0x05b6:
            com.iqonic.store.models.Billing r2 = r2.getBilling()
            java.lang.String r2 = r2.getAddress_2()
            r0.append(r2)
            java.lang.String r0 = r0.toString()
            goto L_0x05d5
        L_0x05c6:
            com.iqonic.store.models.Order r0 = r13.orderModel
            if (r0 != 0) goto L_0x05cd
            kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException(r1)
        L_0x05cd:
            com.iqonic.store.models.Billing r0 = r0.getBilling()
            java.lang.String r0 = r0.getAddress_2()
        L_0x05d5:
            r5 = r0
        L_0x05d6:
            com.iqonic.store.models.Order r0 = r13.orderModel
            if (r0 != 0) goto L_0x05dd
            kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException(r1)
        L_0x05dd:
            com.iqonic.store.models.Billing r0 = r0.getBilling()
            java.lang.String r0 = r0.getCity()
            java.lang.CharSequence r0 = (java.lang.CharSequence) r0
            boolean r0 = kotlin.text.StringsKt.isBlank(r0)
            r0 = r0 ^ r3
            if (r0 == 0) goto L_0x062a
            r0 = r5
            java.lang.CharSequence r0 = (java.lang.CharSequence) r0
            boolean r0 = kotlin.text.StringsKt.isBlank(r0)
            r0 = r0 ^ r3
            if (r0 == 0) goto L_0x061a
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            r0.append(r5)
            r0.append(r7)
            com.iqonic.store.models.Order r2 = r13.orderModel
            if (r2 != 0) goto L_0x060a
            kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException(r1)
        L_0x060a:
            com.iqonic.store.models.Billing r2 = r2.getBilling()
            java.lang.String r2 = r2.getCity()
            r0.append(r2)
            java.lang.String r0 = r0.toString()
            goto L_0x0629
        L_0x061a:
            com.iqonic.store.models.Order r0 = r13.orderModel
            if (r0 != 0) goto L_0x0621
            kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException(r1)
        L_0x0621:
            com.iqonic.store.models.Billing r0 = r0.getBilling()
            java.lang.String r0 = r0.getCity()
        L_0x0629:
            r5 = r0
        L_0x062a:
            com.iqonic.store.models.Order r0 = r13.orderModel
            if (r0 != 0) goto L_0x0631
            kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException(r1)
        L_0x0631:
            com.iqonic.store.models.Billing r0 = r0.getBilling()
            java.lang.String r0 = r0.getPostcode()
            java.lang.CharSequence r0 = (java.lang.CharSequence) r0
            boolean r0 = kotlin.text.StringsKt.isBlank(r0)
            r0 = r0 ^ r3
            if (r0 == 0) goto L_0x067e
            r0 = r5
            java.lang.CharSequence r0 = (java.lang.CharSequence) r0
            boolean r0 = kotlin.text.StringsKt.isBlank(r0)
            r0 = r0 ^ r3
            if (r0 == 0) goto L_0x066e
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            r0.append(r5)
            r0.append(r9)
            com.iqonic.store.models.Order r2 = r13.orderModel
            if (r2 != 0) goto L_0x065e
            kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException(r1)
        L_0x065e:
            com.iqonic.store.models.Billing r2 = r2.getBilling()
            java.lang.String r2 = r2.getPostcode()
            r0.append(r2)
            java.lang.String r0 = r0.toString()
            goto L_0x067d
        L_0x066e:
            com.iqonic.store.models.Order r0 = r13.orderModel
            if (r0 != 0) goto L_0x0675
            kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException(r1)
        L_0x0675:
            com.iqonic.store.models.Billing r0 = r0.getBilling()
            java.lang.String r0 = r0.getPostcode()
        L_0x067d:
            r5 = r0
        L_0x067e:
            com.iqonic.store.models.Order r0 = r13.orderModel
            if (r0 != 0) goto L_0x0685
            kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException(r1)
        L_0x0685:
            com.iqonic.store.models.Billing r0 = r0.getBilling()
            java.lang.String r0 = r0.getState()
            java.lang.CharSequence r0 = (java.lang.CharSequence) r0
            boolean r0 = kotlin.text.StringsKt.isBlank(r0)
            r0 = r0 ^ r3
            if (r0 == 0) goto L_0x06d2
            r0 = r5
            java.lang.CharSequence r0 = (java.lang.CharSequence) r0
            boolean r0 = kotlin.text.StringsKt.isBlank(r0)
            r0 = r0 ^ r3
            if (r0 == 0) goto L_0x06c2
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            r0.append(r5)
            r0.append(r7)
            com.iqonic.store.models.Order r2 = r13.orderModel
            if (r2 != 0) goto L_0x06b2
            kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException(r1)
        L_0x06b2:
            com.iqonic.store.models.Billing r2 = r2.getBilling()
            java.lang.String r2 = r2.getState()
            r0.append(r2)
            java.lang.String r0 = r0.toString()
            goto L_0x06d1
        L_0x06c2:
            com.iqonic.store.models.Order r0 = r13.orderModel
            if (r0 != 0) goto L_0x06c9
            kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException(r1)
        L_0x06c9:
            com.iqonic.store.models.Billing r0 = r0.getBilling()
            java.lang.String r0 = r0.getState()
        L_0x06d1:
            r5 = r0
        L_0x06d2:
            com.iqonic.store.models.Order r0 = r13.orderModel
            if (r0 != 0) goto L_0x06d9
            kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException(r1)
        L_0x06d9:
            com.iqonic.store.models.Billing r0 = r0.getBilling()
            java.lang.String r0 = r0.getCountry()
            java.lang.CharSequence r0 = (java.lang.CharSequence) r0
            boolean r0 = kotlin.text.StringsKt.isBlank(r0)
            r0 = r0 ^ r3
            if (r0 == 0) goto L_0x0726
            r0 = r5
            java.lang.CharSequence r0 = (java.lang.CharSequence) r0
            boolean r0 = kotlin.text.StringsKt.isBlank(r0)
            r0 = r0 ^ r3
            if (r0 == 0) goto L_0x0716
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            r0.append(r5)
            r0.append(r7)
            com.iqonic.store.models.Order r2 = r13.orderModel
            if (r2 != 0) goto L_0x0706
            kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException(r1)
        L_0x0706:
            com.iqonic.store.models.Billing r2 = r2.getBilling()
            java.lang.String r2 = r2.getCountry()
            r0.append(r2)
            java.lang.String r0 = r0.toString()
            goto L_0x0725
        L_0x0716:
            com.iqonic.store.models.Order r0 = r13.orderModel
            if (r0 != 0) goto L_0x071d
            kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException(r1)
        L_0x071d:
            com.iqonic.store.models.Billing r0 = r0.getBilling()
            java.lang.String r0 = r0.getCountry()
        L_0x0725:
            r5 = r0
        L_0x0726:
            int r0 = com.iqonic.store.R.id.tvBillingUserAddress
            android.view.View r0 = r13._$_findCachedViewById(r0)
            android.widget.TextView r0 = (android.widget.TextView) r0
            java.lang.String r2 = "tvBillingUserAddress"
            kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r0, r2)
            java.lang.CharSequence r5 = (java.lang.CharSequence) r5
            r0.setText(r5)
            int r0 = com.iqonic.store.R.id.tvListPrice
            android.view.View r0 = r13._$_findCachedViewById(r0)
            android.widget.TextView r0 = (android.widget.TextView) r0
            java.lang.String r2 = "tvListPrice"
            kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r0, r2)
            java.lang.String r2 = "0"
            java.lang.String r4 = com.iqonic.store.utils.extensions.StringExtensionsKt.currencyFormat$default(r2, r6, r3, r6)
            java.lang.CharSequence r4 = (java.lang.CharSequence) r4
            r0.setText(r4)
            int r0 = com.iqonic.store.R.id.tvSellingPrice
            android.view.View r0 = r13._$_findCachedViewById(r0)
            android.widget.TextView r0 = (android.widget.TextView) r0
            java.lang.String r4 = "tvSellingPrice"
            kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r0, r4)
            java.lang.String r4 = com.iqonic.store.utils.extensions.StringExtensionsKt.currencyFormat$default(r2, r6, r3, r6)
            java.lang.CharSequence r4 = (java.lang.CharSequence) r4
            r0.setText(r4)
            int r0 = com.iqonic.store.R.id.tvExtraDiscount
            android.view.View r0 = r13._$_findCachedViewById(r0)
            android.widget.TextView r0 = (android.widget.TextView) r0
            java.lang.String r4 = "tvExtraDiscount"
            kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r0, r4)
            com.iqonic.store.models.Order r4 = r13.orderModel
            if (r4 != 0) goto L_0x077a
            kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException(r1)
        L_0x077a:
            java.lang.String r4 = r4.getDiscount_total()
            java.lang.String r4 = com.iqonic.store.utils.extensions.StringExtensionsKt.currencyFormat$default(r4, r6, r3, r6)
            java.lang.CharSequence r4 = (java.lang.CharSequence) r4
            r0.setText(r4)
            int r0 = com.iqonic.store.R.id.tvSpecialPrice
            android.view.View r0 = r13._$_findCachedViewById(r0)
            android.widget.TextView r0 = (android.widget.TextView) r0
            java.lang.String r4 = "tvSpecialPrice"
            kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r0, r4)
            java.lang.String r2 = com.iqonic.store.utils.extensions.StringExtensionsKt.currencyFormat$default(r2, r6, r3, r6)
            java.lang.CharSequence r2 = (java.lang.CharSequence) r2
            r0.setText(r2)
            int r0 = com.iqonic.store.R.id.tvShippingFee
            android.view.View r0 = r13._$_findCachedViewById(r0)
            android.widget.TextView r0 = (android.widget.TextView) r0
            java.lang.String r2 = "tvShippingFee"
            kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r0, r2)
            com.iqonic.store.models.Order r2 = r13.orderModel
            if (r2 != 0) goto L_0x07b1
            kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException(r1)
        L_0x07b1:
            java.lang.String r2 = r2.getShipping_total()
            java.lang.String r2 = com.iqonic.store.utils.extensions.StringExtensionsKt.currencyFormat$default(r2, r6, r3, r6)
            java.lang.CharSequence r2 = (java.lang.CharSequence) r2
            r0.setText(r2)
            int r0 = com.iqonic.store.R.id.tvTotalAmount
            android.view.View r0 = r13._$_findCachedViewById(r0)
            android.widget.TextView r0 = (android.widget.TextView) r0
            java.lang.String r2 = "tvTotalAmount"
            kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r0, r2)
            com.iqonic.store.models.Order r2 = r13.orderModel
            if (r2 != 0) goto L_0x07d2
            kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException(r1)
        L_0x07d2:
            java.lang.String r1 = r2.getTotal()
            java.lang.String r1 = com.iqonic.store.utils.extensions.StringExtensionsKt.currencyFormat$default(r1, r6, r3, r6)
            java.lang.CharSequence r1 = (java.lang.CharSequence) r1
            r0.setText(r1)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.iqonic.store.activity.OrderDescriptionActivity.setDetail():void");
    }

    private final void changeColor() {
        TextView textView = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.cancelOrder);
        Intrinsics.checkExpressionValueIsNotNull(textView, "cancelOrder");
        ExtensionsKt.changePrimaryColor(textView);
        TextView textView2 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.lblShippingDetail);
        Intrinsics.checkExpressionValueIsNotNull(textView2, "lblShippingDetail");
        ExtensionsKt.changeTextPrimaryColor(textView2);
        TextView textView3 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.lblItemOrder);
        Intrinsics.checkExpressionValueIsNotNull(textView3, "lblItemOrder");
        ExtensionsKt.changeTextPrimaryColor(textView3);
        TextView textView4 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.lblPriceDetail);
        Intrinsics.checkExpressionValueIsNotNull(textView4, "lblPriceDetail");
        ExtensionsKt.changeTextPrimaryColor(textView4);
        TextView textView5 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.lblExtraDiscount);
        Intrinsics.checkExpressionValueIsNotNull(textView5, "lblExtraDiscount");
        ExtensionsKt.changeTextSecondaryColor(textView5);
        TextView textView6 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.lblSpecialPrice);
        Intrinsics.checkExpressionValueIsNotNull(textView6, "lblSpecialPrice");
        ExtensionsKt.changeTextSecondaryColor(textView6);
        TextView textView7 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.lblShippingFee);
        Intrinsics.checkExpressionValueIsNotNull(textView7, "lblShippingFee");
        ExtensionsKt.changeTextSecondaryColor(textView7);
        TextView textView8 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.lblTotalAmount);
        Intrinsics.checkExpressionValueIsNotNull(textView8, "lblTotalAmount");
        ExtensionsKt.changeTextPrimaryColor(textView8);
        TextView textView9 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvOrderId);
        Intrinsics.checkExpressionValueIsNotNull(textView9, "tvOrderId");
        ExtensionsKt.changeTextSecondaryColor(textView9);
        TextView textView10 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvProductName);
        Intrinsics.checkExpressionValueIsNotNull(textView10, "tvProductName");
        ExtensionsKt.changeTextPrimaryColor(textView10);
        TextView textView11 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvProductSellerName);
        Intrinsics.checkExpressionValueIsNotNull(textView11, "tvProductSellerName");
        ExtensionsKt.changeTextPrimaryColor(textView11);
        TextView textView12 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvPrice);
        Intrinsics.checkExpressionValueIsNotNull(textView12, "tvPrice");
        ExtensionsKt.changeTextPrimaryColor(textView12);
        TextView textView13 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvUsername);
        Intrinsics.checkExpressionValueIsNotNull(textView13, "tvUsername");
        ExtensionsKt.changeTextPrimaryColor(textView13);
        TextView textView14 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvUserAddress);
        Intrinsics.checkExpressionValueIsNotNull(textView14, "tvUserAddress");
        ExtensionsKt.changeTextSecondaryColor(textView14);
        TextView textView15 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.lblAmount);
        Intrinsics.checkExpressionValueIsNotNull(textView15, "lblAmount");
        ExtensionsKt.changeTextSecondaryColor(textView15);
        TextView textView16 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvTotalPrice);
        Intrinsics.checkExpressionValueIsNotNull(textView16, "tvTotalPrice");
        ExtensionsKt.changeTextPrimaryColor(textView16);
        TextView textView17 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvListPrice);
        Intrinsics.checkExpressionValueIsNotNull(textView17, "tvListPrice");
        ExtensionsKt.changeTextPrimaryColor(textView17);
        TextView textView18 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvSellingPrice);
        Intrinsics.checkExpressionValueIsNotNull(textView18, "tvSellingPrice");
        ExtensionsKt.changeTextPrimaryColor(textView18);
        TextView textView19 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvExtraDiscount);
        Intrinsics.checkExpressionValueIsNotNull(textView19, "tvExtraDiscount");
        ExtensionsKt.changeTextPrimaryColor(textView19);
        TextView textView20 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvSpecialPrice);
        Intrinsics.checkExpressionValueIsNotNull(textView20, "tvSpecialPrice");
        ExtensionsKt.changeTextPrimaryColor(textView20);
        TextView textView21 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvShippingFee);
        Intrinsics.checkExpressionValueIsNotNull(textView21, "tvShippingFee");
        ExtensionsKt.changeTextPrimaryColor(textView21);
        TextView textView22 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvTotalAmount);
        Intrinsics.checkExpressionValueIsNotNull(textView22, "tvTotalAmount");
        ExtensionsKt.changeTextPrimaryColor(textView22);
        RelativeLayout relativeLayout = (RelativeLayout) _$_findCachedViewById(com.iqonic.store.R.id.rlMain);
        Intrinsics.checkExpressionValueIsNotNull(relativeLayout, "rlMain");
        ExtensionsKt.changeBackgroundColor(relativeLayout);
    }
}
