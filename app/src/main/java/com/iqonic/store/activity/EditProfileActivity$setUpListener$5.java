package com.iqonic.store.activity;

import android.view.View;
import android.widget.AdapterView;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000+\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\t\n\u0002\b\u0002*\u0001\u0000\b\n\u0018\u00002\u00020\u0001J,\u0010\u0002\u001a\u00020\u00032\n\u0010\u0004\u001a\u0006\u0012\u0002\b\u00030\u00052\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000bH\u0016J\u0014\u0010\f\u001a\u00020\u00032\n\u0010\u0004\u001a\u0006\u0012\u0002\b\u00030\u0005H\u0016¨\u0006\r"}, d2 = {"com/iqonic/store/activity/EditProfileActivity$setUpListener$5", "Landroid/widget/AdapterView$OnItemSelectedListener;", "onItemSelected", "", "parent", "Landroid/widget/AdapterView;", "view", "Landroid/view/View;", "position", "", "id", "", "onNothingSelected", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: EditProfileActivity.kt */
public final class EditProfileActivity$setUpListener$5 implements AdapterView.OnItemSelectedListener {
    final /* synthetic */ EditProfileActivity this$0;

    public void onNothingSelected(AdapterView<?> adapterView) {
        Intrinsics.checkParameterIsNotNull(adapterView, "parent");
    }

    EditProfileActivity$setUpListener$5(EditProfileActivity editProfileActivity) {
        this.this$0 = editProfileActivity;
    }

    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long j) {
        Intrinsics.checkParameterIsNotNull(adapterView, "parent");
        Intrinsics.checkParameterIsNotNull(view, "view");
        EditProfileActivity editProfileActivity = this.this$0;
        editProfileActivity.onBillingCountryChanged(editProfileActivity.getCountryList().get(i).getStates());
    }
}
