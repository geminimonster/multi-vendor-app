package com.iqonic.store.activity;

import com.google.gson.Gson;
import com.iqonic.store.ShopHopApp;
import com.iqonic.store.models.AppSetup;
import com.iqonic.store.models.BuilderDashboard;
import com.iqonic.store.models.BuilderDetail;
import com.iqonic.store.models.DashBoardResponse;
import com.iqonic.store.utils.Constants;
import com.iqonic.store.utils.SharedPrefUtils;
import com.iqonic.store.utils.extensions.AppExtensionsKt;
import com.store.proshop.R;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n¢\u0006\u0002\b\u0004"}, d2 = {"<anonymous>", "", "it", "Lcom/iqonic/store/models/DashBoardResponse;", "invoke"}, k = 3, mv = {1, 1, 16})
/* compiled from: SplashActivity.kt */
final class SplashActivity$onCreate$2 extends Lambda implements Function1<DashBoardResponse, Unit> {
    final /* synthetic */ SplashActivity this$0;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    SplashActivity$onCreate$2(SplashActivity splashActivity) {
        super(1);
        this.this$0 = splashActivity;
    }

    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        invoke((DashBoardResponse) obj);
        return Unit.INSTANCE;
    }

    public final void invoke(DashBoardResponse dashBoardResponse) {
        Intrinsics.checkParameterIsNotNull(dashBoardResponse, "it");
        this.this$0.mAppData = dashBoardResponse;
        SharedPrefUtils sharedPrefInstance = AppExtensionsKt.getSharedPrefInstance();
        Gson gson = new Gson();
        BuilderDashboard dashboard = dashBoardResponse.getDashboard();
        if (dashboard == null) {
            Intrinsics.throwNpe();
        }
        sharedPrefInstance.setValue(Constants.SharedPref.DASHBOARDDATA, gson.toJson((Object) dashboard));
        boolean z = false;
        if (Intrinsics.areEqual((Object) dashBoardResponse.getDashboard().getLayout(), (Object) "layout1")) {
            AppExtensionsKt.getSharedPrefInstance().removeKey(Constants.SharedPref.KEY_DASHBOARD);
            AppExtensionsKt.getSharedPrefInstance().setValue(Constants.SharedPref.KEY_DASHBOARD, 0);
        } else {
            AppExtensionsKt.getSharedPrefInstance().removeKey(Constants.SharedPref.KEY_DASHBOARD);
            AppExtensionsKt.getSharedPrefInstance().setValue(Constants.SharedPref.KEY_DASHBOARD, 1);
        }
        BuilderDetail productdetailview = dashBoardResponse.getProductdetailview();
        if (productdetailview == null) {
            Intrinsics.throwNpe();
        }
        if (Intrinsics.areEqual((Object) productdetailview.getLayout(), (Object) "layout1")) {
            AppExtensionsKt.getSharedPrefInstance().removeKey(Constants.SharedPref.KEY_PRODUCT_DETAIL);
            AppExtensionsKt.getSharedPrefInstance().setValue(Constants.SharedPref.KEY_PRODUCT_DETAIL, 0);
        } else {
            AppExtensionsKt.getSharedPrefInstance().removeKey(Constants.SharedPref.KEY_PRODUCT_DETAIL);
            AppExtensionsKt.getSharedPrefInstance().setValue(Constants.SharedPref.KEY_PRODUCT_DETAIL, 1);
        }
        AppSetup appSetup = SplashActivity.access$getMAppData$p(this.this$0).getAppSetup();
        if (appSetup == null) {
            Intrinsics.throwNpe();
        }
        String consumerKey = appSetup.getConsumerKey();
        if (consumerKey == null) {
            Intrinsics.throwNpe();
        }
        if (consumerKey.length() > 0) {
            AppExtensionsKt.getSharedPrefInstance().removeKey(Constants.SharedPref.CONSUMERKEY);
            SharedPrefUtils sharedPrefInstance2 = AppExtensionsKt.getSharedPrefInstance();
            AppSetup appSetup2 = SplashActivity.access$getMAppData$p(this.this$0).getAppSetup();
            if (appSetup2 == null) {
                Intrinsics.throwNpe();
            }
            sharedPrefInstance2.setValue(Constants.SharedPref.CONSUMERKEY, appSetup2.getConsumerKey());
        } else {
            AppExtensionsKt.getSharedPrefInstance().removeKey(Constants.SharedPref.CONSUMERKEY);
            AppExtensionsKt.getSharedPrefInstance().setValue(Constants.SharedPref.CONSUMERKEY, ShopHopApp.Companion.getAppInstance().getString(R.string.consumerKey));
        }
        AppSetup appSetup3 = SplashActivity.access$getMAppData$p(this.this$0).getAppSetup();
        if (appSetup3 == null) {
            Intrinsics.throwNpe();
        }
        String consumerSecret = appSetup3.getConsumerSecret();
        if (consumerSecret == null) {
            Intrinsics.throwNpe();
        }
        if (consumerSecret.length() > 0) {
            AppExtensionsKt.getSharedPrefInstance().removeKey(Constants.SharedPref.CONSUMERSECRET);
            SharedPrefUtils sharedPrefInstance3 = AppExtensionsKt.getSharedPrefInstance();
            AppSetup appSetup4 = SplashActivity.access$getMAppData$p(this.this$0).getAppSetup();
            if (appSetup4 == null) {
                Intrinsics.throwNpe();
            }
            sharedPrefInstance3.setValue(Constants.SharedPref.CONSUMERSECRET, appSetup4.getConsumerSecret());
        } else {
            AppExtensionsKt.getSharedPrefInstance().removeKey(Constants.SharedPref.CONSUMERSECRET);
            AppExtensionsKt.getSharedPrefInstance().setValue(Constants.SharedPref.CONSUMERSECRET, ShopHopApp.Companion.getAppInstance().getString(R.string.consumerSecret));
        }
        AppSetup appSetup5 = SplashActivity.access$getMAppData$p(this.this$0).getAppSetup();
        if (appSetup5 == null) {
            Intrinsics.throwNpe();
        }
        String primaryColor = appSetup5.getPrimaryColor();
        if (primaryColor == null) {
            Intrinsics.throwNpe();
        }
        if (primaryColor.length() > 0) {
            AppExtensionsKt.getSharedPrefInstance().removeKey(Constants.SharedPref.PRIMARYCOLOR);
            SharedPrefUtils sharedPrefInstance4 = AppExtensionsKt.getSharedPrefInstance();
            AppSetup appSetup6 = SplashActivity.access$getMAppData$p(this.this$0).getAppSetup();
            if (appSetup6 == null) {
                Intrinsics.throwNpe();
            }
            String primaryColor2 = appSetup6.getPrimaryColor();
            if (primaryColor2 == null) {
                Intrinsics.throwNpe();
            }
            sharedPrefInstance4.setValue(Constants.SharedPref.PRIMARYCOLOR, primaryColor2);
        } else {
            AppExtensionsKt.getSharedPrefInstance().removeKey(Constants.SharedPref.PRIMARYCOLOR);
            AppExtensionsKt.getSharedPrefInstance().setValue(Constants.SharedPref.PRIMARYCOLOR, this.this$0.getResources().getString(R.color.colorPrimary));
        }
        AppSetup appSetup7 = SplashActivity.access$getMAppData$p(this.this$0).getAppSetup();
        if (appSetup7 == null) {
            Intrinsics.throwNpe();
        }
        String secondaryColor = appSetup7.getSecondaryColor();
        if (secondaryColor == null) {
            Intrinsics.throwNpe();
        }
        if (secondaryColor.length() > 0) {
            AppExtensionsKt.getSharedPrefInstance().removeKey(Constants.SharedPref.ACCENTCOLOR);
            SharedPrefUtils sharedPrefInstance5 = AppExtensionsKt.getSharedPrefInstance();
            AppSetup appSetup8 = SplashActivity.access$getMAppData$p(this.this$0).getAppSetup();
            if (appSetup8 == null) {
                Intrinsics.throwNpe();
            }
            String secondaryColor2 = appSetup8.getSecondaryColor();
            if (secondaryColor2 == null) {
                Intrinsics.throwNpe();
            }
            sharedPrefInstance5.setValue(Constants.SharedPref.ACCENTCOLOR, secondaryColor2);
        } else {
            AppExtensionsKt.getSharedPrefInstance().removeKey(Constants.SharedPref.ACCENTCOLOR);
            AppExtensionsKt.getSharedPrefInstance().setValue(Constants.SharedPref.ACCENTCOLOR, this.this$0.getResources().getString(R.color.colorAccent));
        }
        AppSetup appSetup9 = SplashActivity.access$getMAppData$p(this.this$0).getAppSetup();
        if (appSetup9 == null) {
            Intrinsics.throwNpe();
        }
        String textPrimaryColor = appSetup9.getTextPrimaryColor();
        if (textPrimaryColor == null) {
            Intrinsics.throwNpe();
        }
        if (textPrimaryColor.length() > 0) {
            AppExtensionsKt.getSharedPrefInstance().removeKey(Constants.SharedPref.TEXTPRIMARYCOLOR);
            SharedPrefUtils sharedPrefInstance6 = AppExtensionsKt.getSharedPrefInstance();
            AppSetup appSetup10 = SplashActivity.access$getMAppData$p(this.this$0).getAppSetup();
            if (appSetup10 == null) {
                Intrinsics.throwNpe();
            }
            String textPrimaryColor2 = appSetup10.getTextPrimaryColor();
            if (textPrimaryColor2 == null) {
                Intrinsics.throwNpe();
            }
            sharedPrefInstance6.setValue(Constants.SharedPref.TEXTPRIMARYCOLOR, textPrimaryColor2);
        } else {
            AppExtensionsKt.getSharedPrefInstance().removeKey(Constants.SharedPref.TEXTPRIMARYCOLOR);
            AppExtensionsKt.getSharedPrefInstance().setValue(Constants.SharedPref.TEXTPRIMARYCOLOR, this.this$0.getResources().getString(R.color.textColorPrimary));
        }
        AppSetup appSetup11 = SplashActivity.access$getMAppData$p(this.this$0).getAppSetup();
        if (appSetup11 == null) {
            Intrinsics.throwNpe();
        }
        String textSecondaryColor = appSetup11.getTextSecondaryColor();
        if (textSecondaryColor == null) {
            Intrinsics.throwNpe();
        }
        if (textSecondaryColor.length() > 0) {
            AppExtensionsKt.getSharedPrefInstance().removeKey(Constants.SharedPref.TEXTSECONDARYCOLOR);
            SharedPrefUtils sharedPrefInstance7 = AppExtensionsKt.getSharedPrefInstance();
            AppSetup appSetup12 = SplashActivity.access$getMAppData$p(this.this$0).getAppSetup();
            if (appSetup12 == null) {
                Intrinsics.throwNpe();
            }
            String textSecondaryColor2 = appSetup12.getTextSecondaryColor();
            if (textSecondaryColor2 == null) {
                Intrinsics.throwNpe();
            }
            sharedPrefInstance7.setValue(Constants.SharedPref.TEXTSECONDARYCOLOR, textSecondaryColor2);
        } else {
            AppExtensionsKt.getSharedPrefInstance().removeKey(Constants.SharedPref.TEXTSECONDARYCOLOR);
            AppExtensionsKt.getSharedPrefInstance().setValue(Constants.SharedPref.TEXTSECONDARYCOLOR, this.this$0.getResources().getString(R.color.textColorSecondary));
        }
        AppSetup appSetup13 = SplashActivity.access$getMAppData$p(this.this$0).getAppSetup();
        if (appSetup13 == null) {
            Intrinsics.throwNpe();
        }
        String backgroundColor = appSetup13.getBackgroundColor();
        if (backgroundColor == null) {
            Intrinsics.throwNpe();
        }
        if (backgroundColor.length() > 0) {
            AppExtensionsKt.getSharedPrefInstance().removeKey(Constants.SharedPref.BACKGROUNDCOLOR);
            SharedPrefUtils sharedPrefInstance8 = AppExtensionsKt.getSharedPrefInstance();
            AppSetup appSetup14 = SplashActivity.access$getMAppData$p(this.this$0).getAppSetup();
            if (appSetup14 == null) {
                Intrinsics.throwNpe();
            }
            String backgroundColor2 = appSetup14.getBackgroundColor();
            if (backgroundColor2 == null) {
                Intrinsics.throwNpe();
            }
            sharedPrefInstance8.setValue(Constants.SharedPref.BACKGROUNDCOLOR, backgroundColor2);
        } else {
            AppExtensionsKt.getSharedPrefInstance().removeKey(Constants.SharedPref.BACKGROUNDCOLOR);
            AppExtensionsKt.getSharedPrefInstance().setValue(Constants.SharedPref.BACKGROUNDCOLOR, this.this$0.getResources().getString(R.color.colorScreenBackground));
        }
        AppSetup appSetup15 = SplashActivity.access$getMAppData$p(this.this$0).getAppSetup();
        if (appSetup15 == null) {
            Intrinsics.throwNpe();
        }
        String appUrl = appSetup15.getAppUrl();
        if (appUrl == null) {
            Intrinsics.throwNpe();
        }
        if (appUrl.length() > 0) {
            z = true;
        }
        if (z) {
            AppExtensionsKt.getSharedPrefInstance().removeKey(Constants.SharedPref.APPURL);
            SharedPrefUtils sharedPrefInstance9 = AppExtensionsKt.getSharedPrefInstance();
            AppSetup appSetup16 = SplashActivity.access$getMAppData$p(this.this$0).getAppSetup();
            if (appSetup16 == null) {
                Intrinsics.throwNpe();
            }
            String appUrl2 = appSetup16.getAppUrl();
            if (appUrl2 == null) {
                Intrinsics.throwNpe();
            }
            sharedPrefInstance9.setValue(Constants.SharedPref.APPURL, appUrl2);
            return;
        }
        AppExtensionsKt.getSharedPrefInstance().removeKey(Constants.SharedPref.APPURL);
        AppExtensionsKt.getSharedPrefInstance().setValue(Constants.SharedPref.APPURL, ShopHopApp.Companion.getAppInstance().getString(R.string.base_url));
    }
}
