package com.iqonic.store.activity;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.iqonic.store.R;
import com.iqonic.store.models.LineItems;
import com.iqonic.store.utils.extensions.ExtensionsKt;
import com.iqonic.store.utils.extensions.NetworkExtensionKt;
import com.iqonic.store.utils.extensions.StringExtensionsKt;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function3;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0007H\n¢\u0006\u0002\b\b"}, d2 = {"<anonymous>", "", "view", "Landroid/view/View;", "model", "Lcom/iqonic/store/models/LineItems;", "<anonymous parameter 2>", "", "invoke"}, k = 3, mv = {1, 1, 16})
/* compiled from: OrderDescriptionActivity.kt */
final class OrderDescriptionActivity$onCreate$mOrderItemAdapter$1 extends Lambda implements Function3<View, LineItems, Integer, Unit> {
    final /* synthetic */ OrderDescriptionActivity this$0;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    OrderDescriptionActivity$onCreate$mOrderItemAdapter$1(OrderDescriptionActivity orderDescriptionActivity) {
        super(3);
        this.this$0 = orderDescriptionActivity;
    }

    public /* bridge */ /* synthetic */ Object invoke(Object obj, Object obj2, Object obj3) {
        invoke((View) obj, (LineItems) obj2, ((Number) obj3).intValue());
        return Unit.INSTANCE;
    }

    public final void invoke(View view, LineItems lineItems, int i) {
        Intrinsics.checkParameterIsNotNull(view, "view");
        Intrinsics.checkParameterIsNotNull(lineItems, "model");
        TextView textView = (TextView) view.findViewById(R.id.tvProductName);
        Intrinsics.checkExpressionValueIsNotNull(textView, "view.tvProductName");
        textView.setText(lineItems.getName());
        TextView textView2 = (TextView) view.findViewById(R.id.tvProductName);
        Intrinsics.checkExpressionValueIsNotNull(textView2, "view.tvProductName");
        ExtensionsKt.changeTextPrimaryColor(textView2);
        TextView textView3 = (TextView) view.findViewById(R.id.tvPrice);
        Intrinsics.checkExpressionValueIsNotNull(textView3, "view.tvPrice");
        textView3.setText(StringExtensionsKt.currencyFormat(lineItems.getSubtotal(), OrderDescriptionActivity.access$getOrderModel$p(this.this$0).getCurrency()));
        TextView textView4 = (TextView) view.findViewById(R.id.tvPrice);
        Intrinsics.checkExpressionValueIsNotNull(textView4, "view.tvPrice");
        ExtensionsKt.changeTextSecondaryColor(textView4);
        TextView textView5 = (TextView) view.findViewById(R.id.tvOriginalPrice);
        Intrinsics.checkExpressionValueIsNotNull(textView5, "view.tvOriginalPrice");
        textView5.setText(" Qty: " + String.valueOf(lineItems.getQuantity()));
        TextView textView6 = (TextView) view.findViewById(R.id.tvOriginalPrice);
        Intrinsics.checkExpressionValueIsNotNull(textView6, "view.tvOriginalPrice");
        textView6.setVisibility(0);
        TextView textView7 = (TextView) view.findViewById(R.id.tvOriginalPrice);
        Intrinsics.checkExpressionValueIsNotNull(textView7, "view.tvOriginalPrice");
        ExtensionsKt.changeTextSecondaryColor(textView7);
        OrderDescriptionActivity orderDescriptionActivity = this.this$0;
        orderDescriptionActivity.totalAmt = orderDescriptionActivity.totalAmt + ((double) Float.parseFloat(lineItems.getSubtotal()));
        if (lineItems.getProduct_images().get(0).getSrc().length() > 0) {
            ImageView imageView = (ImageView) view.findViewById(R.id.ivProduct);
            Intrinsics.checkExpressionValueIsNotNull(imageView, "view.ivProduct");
            NetworkExtensionKt.loadImageFromUrl$default(imageView, lineItems.getProduct_images().get(0).getSrc(), 0, 0, 6, (Object) null);
        }
        TextView textView8 = (TextView) this.this$0._$_findCachedViewById(R.id.tvTotalPrice);
        Intrinsics.checkExpressionValueIsNotNull(textView8, "tvTotalPrice");
        textView8.setText(StringExtensionsKt.currencyFormat$default(String.valueOf(this.this$0.totalAmt), (String) null, 1, (Object) null));
        view.setOnClickListener(new OrderDescriptionActivity$onCreate$mOrderItemAdapter$1$$special$$inlined$onClick$1(view, this, lineItems));
    }
}
