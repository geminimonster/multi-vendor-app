package com.iqonic.store.activity;

import android.util.Log;
import android.view.View;
import android.widget.TextView;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.gson.Gson;
import com.iqonic.store.R;
import com.iqonic.store.models.Term;
import com.iqonic.store.utils.rangeBar.RangeBar;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\b\u0003\n\u0002\b\u0004\u0010\u0000\u001a\u00020\u0001\"\b\b\u0000\u0010\u0002*\u00020\u00032\u000e\u0010\u0004\u001a\n \u0005*\u0004\u0018\u00010\u00030\u0003H\n¢\u0006\u0002\b\u0006¨\u0006\u0007"}, d2 = {"<anonymous>", "", "T", "Landroid/view/View;", "it", "kotlin.jvm.PlatformType", "onClick", "com/iqonic/store/utils/extensions/ExtensionsKt$onClick$1"}, k = 3, mv = {1, 1, 16})
/* compiled from: Extensions.kt */
public final class SearchActivity$openFilterBottomSheet$$inlined$onClick$1 implements View.OnClickListener {
    final /* synthetic */ BottomSheetDialog $filterDialog$inlined;
    final /* synthetic */ String[] $priceArray2$inlined;
    final /* synthetic */ View $this_onClick;
    final /* synthetic */ SearchActivity this$0;

    public SearchActivity$openFilterBottomSheet$$inlined$onClick$1(View view, SearchActivity searchActivity, String[] strArr, BottomSheetDialog bottomSheetDialog) {
        this.$this_onClick = view;
        this.this$0 = searchActivity;
        this.$priceArray2$inlined = strArr;
        this.$filterDialog$inlined = bottomSheetDialog;
    }

    public final void onClick(View view) {
        TextView textView = (TextView) this.$this_onClick;
        this.this$0.mSelectedSlug.clear();
        this.this$0.mSelectedTermId.clear();
        this.this$0.mSelectedPrice.clear();
        HashMap hashMap = new HashMap();
        int i = 0;
        for (Object next : this.this$0.mTerms) {
            int i2 = i + 1;
            if (i < 0) {
                CollectionsKt.throwIndexOverflow();
            }
            Term term = (Term) next;
            if (term.isSelected()) {
                if (hashMap.containsKey(term.getTaxonomy())) {
                    ArrayList arrayList = (ArrayList) hashMap.get(term.getTaxonomy());
                    if (arrayList != null) {
                        arrayList.add(Integer.valueOf(term.getTerm_id()));
                    }
                } else {
                    ArrayList arrayList2 = new ArrayList();
                    arrayList2.add(Integer.valueOf(term.getTerm_id()));
                    hashMap.put(term.getTaxonomy(), arrayList2);
                }
                this.this$0.mSelectedSlug.add(term.getSlug());
                this.this$0.mSelectedTermId.add(String.valueOf(term.getTerm_id()));
            }
            i = i2;
        }
        ArrayList access$getMSelectedPrice$p = this.this$0.mSelectedPrice;
        String[] strArr = this.$priceArray2$inlined;
        RangeBar rangeBar = (RangeBar) this.$filterDialog$inlined.findViewById(R.id.rangebar1);
        Intrinsics.checkExpressionValueIsNotNull(rangeBar, "filterDialog.rangebar1");
        String leftPinValue = rangeBar.getLeftPinValue();
        Intrinsics.checkExpressionValueIsNotNull(leftPinValue, "filterDialog.rangebar1.leftPinValue");
        access$getMSelectedPrice$p.add(Integer.valueOf(Integer.parseInt(strArr[Integer.parseInt(leftPinValue)])));
        ArrayList access$getMSelectedPrice$p2 = this.this$0.mSelectedPrice;
        String[] strArr2 = this.$priceArray2$inlined;
        RangeBar rangeBar2 = (RangeBar) this.$filterDialog$inlined.findViewById(R.id.rangebar1);
        Intrinsics.checkExpressionValueIsNotNull(rangeBar2, "filterDialog.rangebar1");
        String rightPinValue = rangeBar2.getRightPinValue();
        Intrinsics.checkExpressionValueIsNotNull(rightPinValue, "filterDialog.rangebar1.rightPinValue");
        access$getMSelectedPrice$p2.add(Integer.valueOf(Integer.parseInt(strArr2[Integer.parseInt(rightPinValue)])));
        ArrayList arrayList3 = new ArrayList();
        Set<String> keySet = hashMap.keySet();
        Intrinsics.checkExpressionValueIsNotNull(keySet, "map.keys");
        for (String str : keySet) {
            HashMap hashMap2 = new HashMap();
            Intrinsics.checkExpressionValueIsNotNull(str, "it");
            hashMap2.put(str, hashMap.get(str));
            arrayList3.add(hashMap2);
        }
        this.this$0.mPage = 1;
        this.this$0.searchRequest.setPage(this.this$0.mPage);
        this.this$0.searchRequest.setAttribute(arrayList3);
        this.this$0.searchRequest.setPrice(this.this$0.mSelectedPrice);
        Log.e("selected params", new Gson().toJson((Object) this.this$0.searchRequest).toString());
        this.this$0.loadProducts();
        this.$filterDialog$inlined.dismiss();
    }
}
