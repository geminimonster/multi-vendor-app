package com.iqonic.store.activity;

import com.iqonic.store.models.BaseResponse;
import com.iqonic.store.utils.extensions.ExtensionsKt;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n¢\u0006\u0002\b\u0004¨\u0006\u0005"}, d2 = {"<anonymous>", "", "it", "Lcom/iqonic/store/models/BaseResponse;", "invoke", "com/iqonic/store/activity/ChangePwdActivity$onCreate$1$1"}, k = 3, mv = {1, 1, 16})
/* compiled from: ChangePwdActivity.kt */
final class ChangePwdActivity$onCreate$$inlined$onClick$1$lambda$1 extends Lambda implements Function1<BaseResponse, Unit> {
    final /* synthetic */ ChangePwdActivity$onCreate$$inlined$onClick$1 this$0;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    ChangePwdActivity$onCreate$$inlined$onClick$1$lambda$1(ChangePwdActivity$onCreate$$inlined$onClick$1 changePwdActivity$onCreate$$inlined$onClick$1) {
        super(1);
        this.this$0 = changePwdActivity$onCreate$$inlined$onClick$1;
    }

    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        invoke((BaseResponse) obj);
        return Unit.INSTANCE;
    }

    public final void invoke(BaseResponse baseResponse) {
        Intrinsics.checkParameterIsNotNull(baseResponse, "it");
        this.this$0.this$0.showProgress(false);
        ChangePwdActivity changePwdActivity = this.this$0.this$0;
        String message = baseResponse.getMessage();
        if (message == null) {
            Intrinsics.throwNpe();
        }
        ExtensionsKt.snackBar$default(changePwdActivity, message, 0, 2, (Object) null);
    }
}
