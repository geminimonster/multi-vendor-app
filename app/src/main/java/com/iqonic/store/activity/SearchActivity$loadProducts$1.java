package com.iqonic.store.activity;

import android.widget.RelativeLayout;
import androidx.recyclerview.widget.RecyclerView;
import com.iqonic.store.R;
import com.iqonic.store.adapter.BaseAdapter;
import com.iqonic.store.models.StoreProductModel;
import com.iqonic.store.models.StoreProductModelNew;
import com.iqonic.store.utils.extensions.ViewExtensionsKt;
import java.util.ArrayList;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n¢\u0006\u0002\b\u0004"}, d2 = {"<anonymous>", "", "it", "Lcom/iqonic/store/models/StoreProductModelNew;", "invoke"}, k = 3, mv = {1, 1, 16})
/* compiled from: SearchActivity.kt */
final class SearchActivity$loadProducts$1 extends Lambda implements Function1<StoreProductModelNew, Unit> {
    final /* synthetic */ SearchActivity this$0;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    SearchActivity$loadProducts$1(SearchActivity searchActivity) {
        super(1);
        this.this$0 = searchActivity;
    }

    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        invoke((StoreProductModelNew) obj);
        return Unit.INSTANCE;
    }

    public final void invoke(StoreProductModelNew storeProductModelNew) {
        Intrinsics.checkParameterIsNotNull(storeProductModelNew, "it");
        this.this$0.showProgress(false);
        if (this.this$0.mPage == 1) {
            this.this$0.mProductAdapter.clearItems();
        }
        this.this$0.mIsLoading = false;
        this.this$0.totalPages = storeProductModelNew.getNumOfPages();
        if (storeProductModelNew.getData() != null) {
            BaseAdapter access$getMProductAdapter$p = this.this$0.mProductAdapter;
            ArrayList<StoreProductModel> data = storeProductModelNew.getData();
            if (data == null) {
                Intrinsics.throwNpe();
            }
            access$getMProductAdapter$p.addMoreItems(data);
        }
        if (this.this$0.mProductAdapter.getItemCount() == 0) {
            RelativeLayout relativeLayout = (RelativeLayout) this.this$0._$_findCachedViewById(R.id.rlNoData);
            Intrinsics.checkExpressionValueIsNotNull(relativeLayout, "rlNoData");
            ViewExtensionsKt.show(relativeLayout);
            RecyclerView recyclerView = (RecyclerView) this.this$0._$_findCachedViewById(R.id.aSearch_rvSearch);
            Intrinsics.checkExpressionValueIsNotNull(recyclerView, "aSearch_rvSearch");
            ViewExtensionsKt.hide(recyclerView);
            return;
        }
        RelativeLayout relativeLayout2 = (RelativeLayout) this.this$0._$_findCachedViewById(R.id.rlNoData);
        Intrinsics.checkExpressionValueIsNotNull(relativeLayout2, "rlNoData");
        ViewExtensionsKt.hide(relativeLayout2);
        RecyclerView recyclerView2 = (RecyclerView) this.this$0._$_findCachedViewById(R.id.aSearch_rvSearch);
        Intrinsics.checkExpressionValueIsNotNull(recyclerView2, "aSearch_rvSearch");
        ViewExtensionsKt.show(recyclerView2);
    }
}
