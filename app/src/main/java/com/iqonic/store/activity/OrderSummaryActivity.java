package com.iqonic.store.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.widget.Toolbar;
import com.facebook.appevents.AppEventsConstants;
import com.facebook.share.internal.MessengerShareContentUtility;
import com.google.android.material.button.MaterialButton;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.iqonic.store.AppBaseActivity;
import com.iqonic.store.models.Billing;
import com.iqonic.store.models.CheckoutUrlRequest;
import com.iqonic.store.models.CouponLines;
import com.iqonic.store.models.Coupons;
import com.iqonic.store.models.Method;
import com.iqonic.store.models.OrderRequest;
import com.iqonic.store.models.Shipping;
import com.iqonic.store.models.ShippingLines;
import com.iqonic.store.network.RestApiImpl;
import com.iqonic.store.utils.Constants;
import com.iqonic.store.utils.extensions.AppExtensionsKt;
import com.iqonic.store.utils.extensions.ExtensionsKt;
import com.iqonic.store.utils.extensions.NetworkExtensionKt;
import com.iqonic.store.utils.extensions.StringExtensionsKt;
import com.iqonic.store.utils.extensions.ViewExtensionsKt;
import com.razorpay.Checkout;
import com.razorpay.PaymentResultListener;
import com.store.proshop.R;
import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Reflection;
import org.json.JSONObject;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000P\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\t\u0018\u00002\u00020\u00012\u00020\u0002B\u0005¢\u0006\u0002\u0010\u0003J\b\u0010\u000f\u001a\u00020\u0010H\u0002J\u0010\u0010\u0011\u001a\u00020\u00102\u0006\u0010\u0012\u001a\u00020\u0013H\u0002J\b\u0010\u0014\u001a\u00020\u0010H\u0002J\u0010\u0010\u0015\u001a\u00020\u00102\u0006\u0010\u0016\u001a\u00020\u000bH\u0002J\"\u0010\u0017\u001a\u00020\u00102\u0006\u0010\u0018\u001a\u00020\u000b2\u0006\u0010\u0019\u001a\u00020\u000b2\b\u0010\u001a\u001a\u0004\u0018\u00010\u001bH\u0014J\u0012\u0010\u001c\u001a\u00020\u00102\b\u0010\u001d\u001a\u0004\u0018\u00010\u001eH\u0015J\u001a\u0010\u001f\u001a\u00020\u00102\u0006\u0010 \u001a\u00020\u000b2\b\u0010!\u001a\u0004\u0018\u00010\u0005H\u0016J\u0012\u0010\"\u001a\u00020\u00102\b\u0010#\u001a\u0004\u0018\u00010\u0005H\u0016J\b\u0010$\u001a\u00020\u0010H\u0002J\b\u0010%\u001a\u00020\u0010H\u0002J\b\u0010&\u001a\u00020\u0010H\u0003R\u000e\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\b\u001a\u0004\u0018\u00010\tX\u000e¢\u0006\u0002\n\u0000R\u0012\u0010\n\u001a\u0004\u0018\u00010\u000bX\u000e¢\u0006\u0004\n\u0002\u0010\fR\u0010\u0010\r\u001a\u0004\u0018\u00010\u000eX\u000e¢\u0006\u0002\n\u0000¨\u0006'"}, d2 = {"Lcom/iqonic/store/activity/OrderSummaryActivity;", "Lcom/iqonic/store/AppBaseActivity;", "Lcom/razorpay/PaymentResultListener;", "()V", "TAG", "", "isNativePayment", "", "mCoupons", "Lcom/iqonic/store/models/Coupons;", "orderId", "", "Ljava/lang/Integer;", "shippingItems", "Lcom/iqonic/store/models/Method;", "changeColor", "", "createOrderRequest", "orderRequest", "Lcom/iqonic/store/models/OrderRequest;", "deleteOrder", "getCheckoutUrl", "id", "onActivityResult", "requestCode", "resultCode", "data", "Landroid/content/Intent;", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onPaymentError", "errorCode", "response", "onPaymentSuccess", "razorpayPaymentId", "showPaymentFailDialog", "startPayment", "updateAddress", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: OrderSummaryActivity.kt */
public final class OrderSummaryActivity extends AppBaseActivity implements PaymentResultListener {
    private final String TAG = Reflection.getOrCreateKotlinClass(OrderSummaryActivity.class).toString();
    private HashMap _$_findViewCache;
    /* access modifiers changed from: private */
    public boolean isNativePayment;
    private Coupons mCoupons;
    /* access modifiers changed from: private */
    public Integer orderId;
    private Method shippingItems;

    public void _$_clearFindViewByIdCache() {
        HashMap hashMap = this._$_findViewCache;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    public View _$_findCachedViewById(int i) {
        if (this._$_findViewCache == null) {
            this._$_findViewCache = new HashMap();
        }
        View view = (View) this._$_findViewCache.get(Integer.valueOf(i));
        if (view != null) {
            return view;
        }
        View findViewById = findViewById(i);
        this._$_findViewCache.put(Integer.valueOf(i), findViewById);
        return findViewById;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_order_summary);
        Toolbar toolbar = (Toolbar) _$_findCachedViewById(com.iqonic.store.R.id.toolbar);
        Intrinsics.checkExpressionValueIsNotNull(toolbar, "toolbar");
        setToolbar(toolbar);
        setTitle(getString(R.string.order_summary));
        mAppBarColor();
        changeColor();
        this.isNativePayment = !Intrinsics.areEqual((Object) AppExtensionsKt.getSharedPrefInstance().getStringValue("payment_method", Constants.PAYMENT_METHOD.PAYMENT_METHOD_WEB), (Object) Constants.PAYMENT_METHOD.PAYMENT_METHOD_WEB);
        this.shippingItems = (Method) getIntent().getSerializableExtra(Constants.KeyIntent.SHIPPINGDATA);
        this.mCoupons = (Coupons) getIntent().getSerializableExtra(Constants.KeyIntent.COUPON_CODE);
        String stringExtra = getIntent().getStringExtra(Constants.KeyIntent.SUBTOTAL);
        String stringExtra2 = getIntent().getStringExtra(Constants.KeyIntent.DISCOUNT);
        TextView textView = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvTotal);
        Intrinsics.checkExpressionValueIsNotNull(textView, "tvTotal");
        textView.setText(stringExtra != null ? StringExtensionsKt.currencyFormat$default(stringExtra, (String) null, 1, (Object) null) : null);
        if (stringExtra2 != null && (!Intrinsics.areEqual((Object) stringExtra2, (Object) AppEventsConstants.EVENT_PARAM_VALUE_NO))) {
            LinearLayout linearLayout = (LinearLayout) _$_findCachedViewById(com.iqonic.store.R.id.llDiscount);
            Intrinsics.checkExpressionValueIsNotNull(linearLayout, "llDiscount");
            ViewExtensionsKt.show(linearLayout);
            TextView textView2 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvDiscount);
            Intrinsics.checkExpressionValueIsNotNull(textView2, "tvDiscount");
            textView2.setText("-" + StringExtensionsKt.currencyFormat$default(stringExtra2, (String) null, 1, (Object) null));
        }
        if (this.shippingItems != null) {
            LinearLayout linearLayout2 = (LinearLayout) _$_findCachedViewById(com.iqonic.store.R.id.llShippingAmount);
            Intrinsics.checkExpressionValueIsNotNull(linearLayout2, "llShippingAmount");
            ViewExtensionsKt.show(linearLayout2);
            Method method = this.shippingItems;
            if (method == null) {
                Intrinsics.throwNpe();
            }
            if (Intrinsics.areEqual((Object) method.getId(), (Object) "free_shipping")) {
                TextView textView3 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvShipping);
                Intrinsics.checkExpressionValueIsNotNull(textView3, "tvShipping");
                textView3.setText("Free");
                ((TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvShipping)).setTextColor(ExtensionsKt.color(this, R.color.green));
            } else {
                Method method2 = this.shippingItems;
                if (method2 == null) {
                    Intrinsics.throwNpe();
                }
                if (method2.getCost().length() == 0) {
                    Method method3 = this.shippingItems;
                    if (method3 == null) {
                        Intrinsics.throwNpe();
                    }
                    method3.setCost(AppEventsConstants.EVENT_PARAM_VALUE_NO);
                }
                LinearLayout linearLayout3 = (LinearLayout) _$_findCachedViewById(com.iqonic.store.R.id.llShippingAmount);
                Intrinsics.checkExpressionValueIsNotNull(linearLayout3, "llShippingAmount");
                ViewExtensionsKt.show(linearLayout3);
                TextView textView4 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvShipping);
                Intrinsics.checkExpressionValueIsNotNull(textView4, "tvShipping");
                ExtensionsKt.changeTextSecondaryColor(textView4);
                TextView textView5 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvShipping);
                Intrinsics.checkExpressionValueIsNotNull(textView5, "tvShipping");
                Method method4 = this.shippingItems;
                if (method4 == null) {
                    Intrinsics.throwNpe();
                }
                textView5.setText(StringExtensionsKt.currencyFormat$default(method4.getCost(), (String) null, 1, (Object) null));
            }
        }
        String stringExtra3 = getIntent().getStringExtra("price");
        DecimalFormat decimalFormat = new DecimalFormat("0.00");
        TextView textView6 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvTotalCartAmount);
        Intrinsics.checkExpressionValueIsNotNull(textView6, "tvTotalCartAmount");
        Intrinsics.checkExpressionValueIsNotNull(stringExtra3, "price");
        textView6.setText(StringExtensionsKt.currencyFormat$default(decimalFormat.format(Double.parseDouble(stringExtra3)).toString(), (String) null, 1, (Object) null));
        updateAddress();
        MaterialButton materialButton = (MaterialButton) _$_findCachedViewById(com.iqonic.store.R.id.tvContinue);
        materialButton.setOnClickListener(new OrderSummaryActivity$onCreate$$inlined$onClick$1(materialButton, this));
        if (this.isNativePayment) {
            LinearLayout linearLayout4 = (LinearLayout) _$_findCachedViewById(com.iqonic.store.R.id.paymentView);
            Intrinsics.checkExpressionValueIsNotNull(linearLayout4, "paymentView");
            linearLayout4.setVisibility(0);
            return;
        }
        LinearLayout linearLayout5 = (LinearLayout) _$_findCachedViewById(com.iqonic.store.R.id.paymentView);
        Intrinsics.checkExpressionValueIsNotNull(linearLayout5, "paymentView");
        linearLayout5.setVisibility(8);
    }

    private final void updateAddress() {
        Shipping shippingList = AppExtensionsKt.getShippingList();
        TextView textView = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvUserAddress);
        Intrinsics.checkExpressionValueIsNotNull(textView, "tvUserAddress");
        textView.setText(shippingList.getFirst_name() + " " + shippingList.getLast_name() + "\n" + shippingList.getFullAddress("\n"));
        Billing billing = AppExtensionsKt.getbillingList();
        TextView textView2 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvBillingAddress);
        Intrinsics.checkExpressionValueIsNotNull(textView2, "tvBillingAddress");
        textView2.setText(billing.getFirst_name() + " " + billing.getLast_name() + "\n" + billing.getFullAddress("\n"));
        if (this.shippingItems != null) {
            TextView textView3 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvMethod);
            Intrinsics.checkExpressionValueIsNotNull(textView3, "tvMethod");
            ViewExtensionsKt.show(textView3);
            Method method = this.shippingItems;
            if (method == null) {
                Intrinsics.throwNpe();
            }
            if (!Intrinsics.areEqual((Object) method.getId(), (Object) "free_shipping")) {
                Method method2 = this.shippingItems;
                if (method2 == null) {
                    Intrinsics.throwNpe();
                }
                if (!Intrinsics.areEqual((Object) method2.getCost(), (Object) AppEventsConstants.EVENT_PARAM_VALUE_NO)) {
                    Method method3 = this.shippingItems;
                    if (method3 == null) {
                        Intrinsics.throwNpe();
                    }
                    if (!(method3.getCost().length() == 0)) {
                        TextView textView4 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvMethodName);
                        Intrinsics.checkExpressionValueIsNotNull(textView4, "tvMethodName");
                        StringBuilder sb = new StringBuilder();
                        Method method4 = this.shippingItems;
                        if (method4 == null) {
                            Intrinsics.throwNpe();
                        }
                        sb.append(method4.getMethodTitle());
                        sb.append(": ");
                        Method method5 = this.shippingItems;
                        if (method5 == null) {
                            Intrinsics.throwNpe();
                        }
                        sb.append(StringExtensionsKt.currencyFormat$default(method5.getCost(), (String) null, 1, (Object) null));
                        textView4.setText(sb.toString());
                        return;
                    }
                }
            }
            TextView textView5 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvMethodName);
            Intrinsics.checkExpressionValueIsNotNull(textView5, "tvMethodName");
            Method method6 = this.shippingItems;
            if (method6 == null) {
                Intrinsics.throwNpe();
            }
            textView5.setText(method6.getMethodTitle());
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (i2 == -1) {
            if (i == 201) {
                updateAddress();
            } else if (i == 207) {
                setResult(-1);
                finish();
            } else if (i == 208) {
                setResult(-1);
                finish();
            }
        } else if (i == 208 || i == 207) {
            deleteOrder();
        }
    }

    private final void deleteOrder() {
        RestApiImpl restApiImpl$default = NetworkExtensionKt.getRestApiImpl$default((String) null, 1, (Object) null);
        Integer num = this.orderId;
        if (num == null) {
            Intrinsics.throwNpe();
        }
        restApiImpl$default.deleteOrder(num.intValue(), OrderSummaryActivity$deleteOrder$1.INSTANCE, new OrderSummaryActivity$deleteOrder$2(this));
    }

    /* access modifiers changed from: private */
    public final void startPayment() {
        Activity activity = this;
        Checkout checkout = new Checkout();
        checkout.setKeyID(getString(R.string.razor_key));
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("name", getString(R.string.app_name));
            jSONObject.put("description", getString(R.string.app_description));
            jSONObject.put(MessengerShareContentUtility.MEDIA_IMAGE, "http://iqonic.design/wp-themes/proshop/wp-content/uploads/2020/06/logo1.png");
            jSONObject.put(FirebaseAnalytics.Param.CURRENCY, AppExtensionsKt.getDefaultCurrencyFormate());
            String stringExtra = getIntent().getStringExtra("price");
            Intrinsics.checkExpressionValueIsNotNull(stringExtra, "orignalPrice");
            jSONObject.put("amount", String.valueOf((int) (((double) Float.parseFloat(stringExtra)) * 100.0d)));
            JSONObject jSONObject2 = new JSONObject();
            jSONObject2.put("email", AppExtensionsKt.getEmail());
            jSONObject2.put(Constants.SharedPref.CONTACT, AppExtensionsKt.getbillingList().getPhone());
            jSONObject.put("prefill", jSONObject2);
            checkout.open(activity, jSONObject);
        } catch (Exception e) {
            showProgress(false);
            Toast.makeText(activity, getString(R.string.lbl_error_in_payment) + e.getMessage(), 1).show();
            e.printStackTrace();
        }
    }

    public void onPaymentError(int i, String str) {
        try {
            Log.e("onPaymentError", "errorCode:" + i + " \nresponse:" + str);
            showPaymentFailDialog();
            showProgress(false);
        } catch (Exception e) {
            Log.e(this.TAG, "Exception in onPaymentSuccess", e);
        }
    }

    /* access modifiers changed from: private */
    public final void createOrderRequest(OrderRequest orderRequest) {
        Billing billing = new Billing();
        billing.setAddress_1(AppExtensionsKt.getbillingList().getAddress_1());
        billing.setAddress_2(AppExtensionsKt.getbillingList().getAddress_2());
        billing.setCity(AppExtensionsKt.getbillingList().getCity());
        billing.setCountry(AppExtensionsKt.getbillingList().getCountry());
        billing.setState(AppExtensionsKt.getbillingList().getState());
        billing.setPhone(AppExtensionsKt.getbillingList().getPhone());
        billing.setFirst_name(AppExtensionsKt.getbillingList().getFirst_name());
        billing.setLast_name(AppExtensionsKt.getbillingList().getLast_name());
        billing.setEmail(AppExtensionsKt.getEmail());
        Shipping shipping = new Shipping();
        shipping.setAddress_1(AppExtensionsKt.getShippingList().getAddress_1());
        shipping.setAddress_2(AppExtensionsKt.getShippingList().getAddress_2());
        shipping.setCity(AppExtensionsKt.getShippingList().getCity());
        shipping.setCountry(AppExtensionsKt.getShippingList().getCountry());
        shipping.setState(AppExtensionsKt.getShippingList().getState());
        shipping.setPhone(AppExtensionsKt.getShippingList().getPhone());
        shipping.setFirst_name(AppExtensionsKt.getShippingList().getFirst_name());
        shipping.setLast_name(AppExtensionsKt.getShippingList().getLast_name());
        orderRequest.setBilling(billing);
        orderRequest.setShipping(shipping);
        Serializable serializableExtra = getIntent().getSerializableExtra(Constants.KeyIntent.PRODUCTDATA);
        if (serializableExtra != null) {
            orderRequest.setLine_items((ArrayList) serializableExtra);
            if (this.shippingItems != null) {
                ShippingLines shippingLines = new ShippingLines((String) null, (String) null, (String) null, (String) null, (String) null, (ArrayList) null, (ArrayList) null, 127, (DefaultConstructorMarker) null);
                Method method = this.shippingItems;
                if (method == null) {
                    Intrinsics.throwNpe();
                }
                shippingLines.setMethod_id(method.getId());
                Method method2 = this.shippingItems;
                if (method2 == null) {
                    Intrinsics.throwNpe();
                }
                shippingLines.setMethod_title(method2.getMethodTitle());
                Method method3 = this.shippingItems;
                if (method3 == null) {
                    Intrinsics.throwNpe();
                }
                if (Intrinsics.areEqual((Object) method3.getId(), (Object) "free_shipping")) {
                    shippingLines.setTotal(AppEventsConstants.EVENT_PARAM_VALUE_NO);
                } else {
                    Method method4 = this.shippingItems;
                    if (method4 == null) {
                        Intrinsics.throwNpe();
                    }
                    if (method4.getCost().length() == 0) {
                        Method method5 = this.shippingItems;
                        if (method5 == null) {
                            Intrinsics.throwNpe();
                        }
                        method5.setCost(AppEventsConstants.EVENT_PARAM_VALUE_NO);
                    }
                    Method method6 = this.shippingItems;
                    if (method6 == null) {
                        Intrinsics.throwNpe();
                    }
                    shippingLines.setTotal(method6.getCost());
                }
                ArrayList arrayList = new ArrayList();
                arrayList.add(shippingLines);
                orderRequest.setShipping_lines(arrayList);
            }
            if (this.mCoupons != null) {
                ArrayList arrayList2 = new ArrayList(1);
                CouponLines couponLines = new CouponLines((String) null, 1, (DefaultConstructorMarker) null);
                Coupons coupons = this.mCoupons;
                if (coupons == null) {
                    Intrinsics.throwNpe();
                }
                couponLines.setCode(coupons.getCode());
                arrayList2.add(couponLines);
                orderRequest.setCoupon_lines(arrayList2);
            }
            NetworkExtensionKt.getRestApiImpl$default((String) null, 1, (Object) null).createOrderRequest(orderRequest, new OrderSummaryActivity$createOrderRequest$1(this), new OrderSummaryActivity$createOrderRequest$2(this));
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type kotlin.collections.ArrayList<com.iqonic.store.models.Line_items> /* = java.util.ArrayList<com.iqonic.store.models.Line_items> */");
    }

    /* access modifiers changed from: private */
    public final void getCheckoutUrl(int i) {
        CheckoutUrlRequest checkoutUrlRequest = new CheckoutUrlRequest((String) null, 1, (DefaultConstructorMarker) null);
        checkoutUrlRequest.setOrder_id(String.valueOf(i));
        NetworkExtensionKt.getRestApiImpl$default((String) null, 1, (Object) null).getCheckoutUrl(checkoutUrlRequest, new OrderSummaryActivity$getCheckoutUrl$1(this), new OrderSummaryActivity$getCheckoutUrl$2(this));
    }

    public void onPaymentSuccess(String str) {
        Log.e(this.TAG, " onPaymentSuccess");
        try {
            OrderRequest orderRequest = new OrderRequest((String) null, (String) null, 0, (String) null, (String) null, false, (Billing) null, (Shipping) null, (ArrayList) null, (ArrayList) null, (ArrayList) null, 2047, (DefaultConstructorMarker) null);
            orderRequest.setPayment_method("razorpay");
            orderRequest.setTransaction_id(String.valueOf(str));
            orderRequest.setCustomer_id(Integer.parseInt(AppExtensionsKt.getUserId()));
            orderRequest.setCurrency(AppExtensionsKt.getDefaultCurrencyFormate());
            orderRequest.setStatus("pending");
            orderRequest.setSet_paid(true);
            createOrderRequest(orderRequest);
        } catch (Exception e) {
            showProgress(false);
            Log.e(this.TAG, "Exception in onPaymentSuccess", e);
        }
    }

    private final void showPaymentFailDialog() {
        Dialog dialog = new Dialog(this);
        dialog.setCancelable(false);
        Window window = dialog.getWindow();
        if (window != null) {
            window.setBackgroundDrawable(new ColorDrawable(0));
        }
        dialog.setContentView(R.layout.dialog_failed_transaction);
        Window window2 = dialog.getWindow();
        if (window2 != null) {
            window2.setLayout(-1, -2);
        }
        TextView textView = (TextView) dialog.findViewById(com.iqonic.store.R.id.tv_close);
        textView.setOnClickListener(new OrderSummaryActivity$showPaymentFailDialog$$inlined$onClick$1(textView, this, dialog));
        dialog.show();
    }

    private final void changeColor() {
        MaterialButton materialButton = (MaterialButton) _$_findCachedViewById(com.iqonic.store.R.id.tvContinue);
        Intrinsics.checkExpressionValueIsNotNull(materialButton, "tvContinue");
        materialButton.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor(AppExtensionsKt.getButtonColor())));
        TextView textView = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvAddressHeading);
        Intrinsics.checkExpressionValueIsNotNull(textView, "tvAddressHeading");
        ExtensionsKt.changeTextPrimaryColor(textView);
        TextView textView2 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvUserAddress);
        Intrinsics.checkExpressionValueIsNotNull(textView2, "tvUserAddress");
        ExtensionsKt.changeTextSecondaryColor(textView2);
        TextView textView3 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvMethod);
        Intrinsics.checkExpressionValueIsNotNull(textView3, "tvMethod");
        ExtensionsKt.changeTextSecondaryColor(textView3);
        TextView textView4 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvMethodName);
        Intrinsics.checkExpressionValueIsNotNull(textView4, "tvMethodName");
        ExtensionsKt.changeTextSecondaryColor(textView4);
        TextView textView5 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvChange);
        Intrinsics.checkExpressionValueIsNotNull(textView5, "tvChange");
        ExtensionsKt.changePrimaryColor(textView5);
        TextView textView6 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.lblBillingAddress);
        Intrinsics.checkExpressionValueIsNotNull(textView6, "lblBillingAddress");
        ExtensionsKt.changeTextPrimaryColor(textView6);
        TextView textView7 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvBillingAddress);
        Intrinsics.checkExpressionValueIsNotNull(textView7, "tvBillingAddress");
        ExtensionsKt.changeTextSecondaryColor(textView7);
        TextView textView8 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.lblPaymentMethod);
        Intrinsics.checkExpressionValueIsNotNull(textView8, "lblPaymentMethod");
        ExtensionsKt.changeTextPrimaryColor(textView8);
        RadioButton radioButton = (RadioButton) _$_findCachedViewById(com.iqonic.store.R.id.rbCard);
        Intrinsics.checkExpressionValueIsNotNull(radioButton, "rbCard");
        ExtensionsKt.changeTextSecondaryColor(radioButton);
        RadioButton radioButton2 = (RadioButton) _$_findCachedViewById(com.iqonic.store.R.id.rbRazorpay);
        Intrinsics.checkExpressionValueIsNotNull(radioButton2, "rbRazorpay");
        ExtensionsKt.changeTextSecondaryColor(radioButton2);
        RadioButton radioButton3 = (RadioButton) _$_findCachedViewById(com.iqonic.store.R.id.rbCOD);
        Intrinsics.checkExpressionValueIsNotNull(radioButton3, "rbCOD");
        ExtensionsKt.changeTextSecondaryColor(radioButton3);
        TextView textView9 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.lblSubTotal);
        Intrinsics.checkExpressionValueIsNotNull(textView9, "lblSubTotal");
        ExtensionsKt.changeTextSecondaryColor(textView9);
        TextView textView10 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvTotal);
        Intrinsics.checkExpressionValueIsNotNull(textView10, "tvTotal");
        ExtensionsKt.changeTextSecondaryColor(textView10);
        TextView textView11 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.txtDiscountlbl);
        Intrinsics.checkExpressionValueIsNotNull(textView11, "txtDiscountlbl");
        ExtensionsKt.changeTextSecondaryColor(textView11);
        TextView textView12 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvDiscount);
        Intrinsics.checkExpressionValueIsNotNull(textView12, "tvDiscount");
        ExtensionsKt.changeTextSecondaryColor(textView12);
        TextView textView13 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.txtShippinglbl);
        Intrinsics.checkExpressionValueIsNotNull(textView13, "txtShippinglbl");
        ExtensionsKt.changeTextSecondaryColor(textView13);
        TextView textView14 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvShipping);
        Intrinsics.checkExpressionValueIsNotNull(textView14, "tvShipping");
        ExtensionsKt.changeTextSecondaryColor(textView14);
        TextView textView15 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.lblTotalAmount);
        Intrinsics.checkExpressionValueIsNotNull(textView15, "lblTotalAmount");
        ExtensionsKt.changeTextPrimaryColor(textView15);
        TextView textView16 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvTotalCartAmount);
        Intrinsics.checkExpressionValueIsNotNull(textView16, "tvTotalCartAmount");
        ExtensionsKt.changeTextPrimaryColor(textView16);
        RelativeLayout relativeLayout = (RelativeLayout) _$_findCachedViewById(com.iqonic.store.R.id.rlMain);
        Intrinsics.checkExpressionValueIsNotNull(relativeLayout, "rlMain");
        ExtensionsKt.changeBackgroundColor(relativeLayout);
    }
}
