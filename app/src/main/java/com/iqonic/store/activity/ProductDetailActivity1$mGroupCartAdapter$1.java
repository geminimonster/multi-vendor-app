package com.iqonic.store.activity;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.iqonic.store.R;
import com.iqonic.store.models.Image;
import com.iqonic.store.models.StoreProductModel;
import com.iqonic.store.utils.extensions.AppExtensionsKt;
import com.iqonic.store.utils.extensions.ExtensionsKt;
import com.iqonic.store.utils.extensions.NetworkExtensionKt;
import com.iqonic.store.utils.extensions.StringExtensionsKt;
import java.util.List;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function3;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;
import kotlin.text.StringsKt;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0007H\n¢\u0006\u0002\b\b"}, d2 = {"<anonymous>", "", "view", "Landroid/view/View;", "model", "Lcom/iqonic/store/models/StoreProductModel;", "<anonymous parameter 2>", "", "invoke"}, k = 3, mv = {1, 1, 16})
/* compiled from: ProductDetailActivity1.kt */
final class ProductDetailActivity1$mGroupCartAdapter$1 extends Lambda implements Function3<View, StoreProductModel, Integer, Unit> {
    final /* synthetic */ ProductDetailActivity1 this$0;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    ProductDetailActivity1$mGroupCartAdapter$1(ProductDetailActivity1 productDetailActivity1) {
        super(3);
        this.this$0 = productDetailActivity1;
    }

    public /* bridge */ /* synthetic */ Object invoke(Object obj, Object obj2, Object obj3) {
        invoke((View) obj, (StoreProductModel) obj2, ((Number) obj3).intValue());
        return Unit.INSTANCE;
    }

    public final void invoke(View view, StoreProductModel storeProductModel, int i) {
        Intrinsics.checkParameterIsNotNull(view, "view");
        Intrinsics.checkParameterIsNotNull(storeProductModel, "model");
        TextView textView = (TextView) view.findViewById(R.id.tvProductName);
        Intrinsics.checkExpressionValueIsNotNull(textView, "view.tvProductName");
        textView.setText(storeProductModel.getName());
        TextView textView2 = (TextView) view.findViewById(R.id.tvProductName);
        Intrinsics.checkExpressionValueIsNotNull(textView2, "view.tvProductName");
        ExtensionsKt.changeTextPrimaryColor(textView2);
        TextView textView3 = (TextView) view.findViewById(R.id.tvAdd);
        Intrinsics.checkExpressionValueIsNotNull(textView3, "view.tvAdd");
        ExtensionsKt.changeBackgroundTint(textView3, AppExtensionsKt.getAccentColor());
        TextView textView4 = (TextView) view.findViewById(R.id.tvPrice);
        Intrinsics.checkExpressionValueIsNotNull(textView4, "view.tvPrice");
        ExtensionsKt.changeAccentColor(textView4);
        TextView textView5 = (TextView) view.findViewById(R.id.tvOriginalPrice);
        Intrinsics.checkExpressionValueIsNotNull(textView5, "view.tvOriginalPrice");
        ExtensionsKt.changeTextSecondaryColor(textView5);
        TextView textView6 = (TextView) view.findViewById(R.id.tvOriginalPrice);
        Intrinsics.checkExpressionValueIsNotNull(textView6, "view.tvOriginalPrice");
        ExtensionsKt.applyStrike(textView6);
        List<Image> images = storeProductModel.getImages();
        if (images == null) {
            Intrinsics.throwNpe();
        }
        String src = images.get(0).getSrc();
        if (src == null) {
            Intrinsics.throwNpe();
        }
        if (src.length() > 0) {
            ImageView imageView = (ImageView) view.findViewById(R.id.ivProduct);
            Intrinsics.checkExpressionValueIsNotNull(imageView, "view.ivProduct");
            List<Image> images2 = storeProductModel.getImages();
            if (images2 == null) {
                Intrinsics.throwNpe();
            }
            String src2 = images2.get(0).getSrc();
            if (src2 == null) {
                Intrinsics.throwNpe();
            }
            NetworkExtensionKt.loadImageFromUrl$default(imageView, src2, 0, 0, 6, (Object) null);
        }
        String str = null;
        if (storeProductModel.getOnSale()) {
            TextView textView7 = (TextView) view.findViewById(R.id.tvPrice);
            Intrinsics.checkExpressionValueIsNotNull(textView7, "view.tvPrice");
            String salePrice = storeProductModel.getSalePrice();
            textView7.setText(salePrice != null ? StringExtensionsKt.currencyFormat$default(salePrice, (String) null, 1, (Object) null) : null);
            TextView textView8 = (TextView) view.findViewById(R.id.tvOriginalPrice);
            Intrinsics.checkExpressionValueIsNotNull(textView8, "view.tvOriginalPrice");
            ExtensionsKt.applyStrike(textView8);
            TextView textView9 = (TextView) view.findViewById(R.id.tvOriginalPrice);
            Intrinsics.checkExpressionValueIsNotNull(textView9, "view.tvOriginalPrice");
            String regularPrice = storeProductModel.getRegularPrice();
            if (regularPrice != null) {
                str = StringExtensionsKt.currencyFormat$default(regularPrice, (String) null, 1, (Object) null);
            }
            textView9.setText(str);
        } else {
            TextView textView10 = (TextView) view.findViewById(R.id.tvOriginalPrice);
            Intrinsics.checkExpressionValueIsNotNull(textView10, "view.tvOriginalPrice");
            textView10.setText("");
            if (StringsKt.equals$default(storeProductModel.getRegularPrice(), "", false, 2, (Object) null)) {
                TextView textView11 = (TextView) view.findViewById(R.id.tvPrice);
                Intrinsics.checkExpressionValueIsNotNull(textView11, "view.tvPrice");
                String price = storeProductModel.getPrice();
                if (price != null) {
                    str = StringExtensionsKt.currencyFormat$default(price, (String) null, 1, (Object) null);
                }
                textView11.setText(str);
            } else {
                TextView textView12 = (TextView) view.findViewById(R.id.tvPrice);
                Intrinsics.checkExpressionValueIsNotNull(textView12, "view.tvPrice");
                String regularPrice2 = storeProductModel.getRegularPrice();
                if (regularPrice2 != null) {
                    str = StringExtensionsKt.currencyFormat$default(regularPrice2, (String) null, 1, (Object) null);
                }
                textView12.setText(str);
            }
        }
        TextView textView13 = (TextView) view.findViewById(R.id.tvAdd);
        textView13.setOnClickListener(new ProductDetailActivity1$mGroupCartAdapter$1$$special$$inlined$onClick$1(textView13, this, storeProductModel));
    }
}
