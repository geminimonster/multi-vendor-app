package com.iqonic.store.activity;

import android.graphics.Color;
import android.widget.TextView;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.iqonic.store.R;
import com.iqonic.store.utils.extensions.AppExtensionsKt;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u00032\u0006\u0010\u0005\u001a\u00020\u0006H\n¢\u0006\u0002\b\u0007"}, d2 = {"<anonymous>", "", "<anonymous parameter 0>", "Lcom/google/android/material/appbar/AppBarLayout;", "kotlin.jvm.PlatformType", "verticalOffset", "", "onOffsetChanged"}, k = 3, mv = {1, 1, 16})
/* compiled from: ProductDetailActivity1.kt */
final class ProductDetailActivity1$onCreate$5 implements AppBarLayout.OnOffsetChangedListener {
    final /* synthetic */ ProductDetailActivity1 this$0;

    ProductDetailActivity1$onCreate$5(ProductDetailActivity1 productDetailActivity1) {
        this.this$0 = productDetailActivity1;
    }

    public final void onOffsetChanged(AppBarLayout appBarLayout, int i) {
        int abs = Math.abs(i);
        AppBarLayout appBarLayout2 = (AppBarLayout) this.this$0._$_findCachedViewById(R.id.app_bar);
        Intrinsics.checkExpressionValueIsNotNull(appBarLayout2, "app_bar");
        if (abs - appBarLayout2.getTotalScrollRange() == 0) {
            CollapsingToolbarLayout collapsingToolbarLayout = (CollapsingToolbarLayout) this.this$0._$_findCachedViewById(R.id.toolbar_layout);
            Intrinsics.checkExpressionValueIsNotNull(collapsingToolbarLayout, "toolbar_layout");
            TextView textView = (TextView) this.this$0._$_findCachedViewById(R.id.tvName);
            Intrinsics.checkExpressionValueIsNotNull(textView, "tvName");
            collapsingToolbarLayout.setTitle(textView.getText());
            ((CollapsingToolbarLayout) this.this$0._$_findCachedViewById(R.id.toolbar_layout)).setExpandedTitleColor(Color.parseColor(AppExtensionsKt.getAccentColor()));
            return;
        }
        CollapsingToolbarLayout collapsingToolbarLayout2 = (CollapsingToolbarLayout) this.this$0._$_findCachedViewById(R.id.toolbar_layout);
        Intrinsics.checkExpressionValueIsNotNull(collapsingToolbarLayout2, "toolbar_layout");
        collapsingToolbarLayout2.setTitle("");
    }
}
