package com.iqonic.store.activity

import android.app.Activity
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.iqonic.store.AppBaseActivity
import com.iqonic.store.R
import com.iqonic.store.ShopHopApp
import com.iqonic.store.adapter.BaseAdapter
import com.iqonic.store.utils.Constants
import kotlinx.android.synthetic.main.menu_cart.view.*
import kotlinx.android.synthetic.main.spinner_language.view.*
import kotlin.system.exitProcess


class SettingActivity : AppBaseActivity() {
    private lateinit var mMenuCart: View
    private lateinit var lan: String
    private var codes = arrayOf(
        "en",
        "hi",
        "fr",
        "es",
        "de",
        "in",
        "af",
        "pt",
        "tr",
        "ar",
        "vi"
    )
    private var mCountryImg = intArrayOf(
        R.drawable.us,
        R.drawable.india,
        R.drawable.france,
        R.drawable.spain,
        R.drawable.germany,
        R.drawable.indonesia,
        R.drawable.south_africa,
        R.drawable.portugal,
        R.drawable.turkey,
        R.drawable.ar,
        R.drawable.vietnam

    )
    private val mCartCountChangeReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            setCartCount()
        }

    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_setting)
        title = getString(R.string.title_setting)
        setToolbar(toolbar)
        toolbar.setNavigationOnClickListener { onBackPressed() }


        lan = ShopHopApp.language
        val languages = resources.getStringArray(R.array.language)
        switchNightMode.isChecked = ShopHopApp.appTheme == Constants.THEME.DARK

        val dialog = BottomSheetDialog(this)
        dialog.setContentView(R.layout.dialog_launguage_selection)
        val languageAdapter = BaseAdapter<String>(
            R.layout.spinner_language,
            onBind = { view: View, s: String, i: Int ->
                view.ivLogo.setImageResource(mCountryImg[i])
                view.tvName.text = languages[i]
            })
        languageAdapter.onItemClick = { i: Int, view: View, s: String ->
            ivLanguage.loadImageFromDrawable(mCountryImg[i])
            tvLanguage.text = languages[i]
            dialog.dismiss()
            setNewLocale(codes[i])
        }
        dialog.listLanguage.apply {
            setVerticalLayout()
            adapter = languageAdapter
        }
        languageAdapter.addItems(languages.toCollection(ArrayList()))
        llLanguage.onClick {
            dialog.show()
        }
        codes.forEachIndexed { i: Int, s: String ->
            if (lan == s) {
                ivLanguage.loadImageFromDrawable(mCountryImg[i])
                tvLanguage.text = languages[i]
            }
        }
        switchNightMode.setOnCheckedChangeListener { _, isChecked ->
            ShopHopApp.changeAppTheme(isChecked)
            switchToDarkMode(isChecked)
        }

        registerCartCountChangeReceiver(mCartCountChangeReceiver)

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_dashboard, menu)
        val menuWishItem: MenuItem = menu!!.findItem(R.id.action_cart)
        val menuSearch: MenuItem = menu.findItem(R.id.action_search)
        menuWishItem.isVisible = true
        menuSearch.isVisible = false
        mMenuCart = menuWishItem.actionView
        mMenuCart.ivCart.setColorFilter(this.color(R.color.colorDrawableCart))
        menuWishItem.actionView.onClick {
            launchActivity<MyCartActivity> { }
        }
        setCartCount()
        return super.onCreateOptionsMenu(menu)
    }

    fun setCartCount() {
        val count = getCartCount()
        mMenuCart.tvNotificationCount.text = count
        if (count.checkIsEmpty() || count == "0") {
            mMenuCart.tvNotificationCount.hide()
        } else {
            mMenuCart.tvNotificationCount.show()
        }
    }

    override fun onDestroy() {
        unregisterReceiver(mCartCountChangeReceiver)
        super.onDestroy()
    }


    private fun setNewLocale(language: String) {
        ShopHopApp.changeLanguage(language)
        Log.e("lan", language)
        if (lan != language) {
            recreate()
            setResult(Activity.RESULT_OK)
        }
    }

    override fun onBackPressed() {
        if (lan != ShopHopApp.language) {
            launchActivityWithNewTask<DashBoardActivity>()
            exitProcess(0)
        } else {
            super.onBackPressed()
        }
    }
}


