package com.iqonic.store.activity;

import android.view.View;
import com.iqonic.store.models.StoreProductModel;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function3;
import kotlin.jvm.internal.Lambda;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0007H\n¢\u0006\u0002\b\b"}, d2 = {"<anonymous>", "", "view", "Landroid/view/View;", "model", "Lcom/iqonic/store/models/StoreProductModel;", "<anonymous parameter 2>", "", "invoke"}, k = 3, mv = {1, 1, 16})
/* compiled from: SubCategoryActivity.kt */
final class SubCategoryActivity$mProductAdapter$1 extends Lambda implements Function3<View, StoreProductModel, Integer, Unit> {
    final /* synthetic */ SubCategoryActivity this$0;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    SubCategoryActivity$mProductAdapter$1(SubCategoryActivity subCategoryActivity) {
        super(3);
        this.this$0 = subCategoryActivity;
    }

    public /* bridge */ /* synthetic */ Object invoke(Object obj, Object obj2, Object obj3) {
        invoke((View) obj, (StoreProductModel) obj2, ((Number) obj3).intValue());
        return Unit.INSTANCE;
    }

    /* JADX WARNING: Removed duplicated region for block: B:61:0x020c  */
    /* JADX WARNING: Removed duplicated region for block: B:64:0x021b  */
    /* JADX WARNING: Removed duplicated region for block: B:72:0x0255  */
    /* JADX WARNING: Removed duplicated region for block: B:75:0x026b  */
    /* JADX WARNING: Removed duplicated region for block: B:79:0x0299  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void invoke(android.view.View r10, com.iqonic.store.models.StoreProductModel r11, int r12) {
        /*
            r9 = this;
            java.lang.String r12 = "view"
            kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(r10, r12)
            java.lang.String r12 = "model"
            kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(r11, r12)
            java.util.List r12 = r11.getImages()
            if (r12 != 0) goto L_0x0014
            kotlin.jvm.internal.Intrinsics.throwNpe()
        L_0x0014:
            java.util.Collection r12 = (java.util.Collection) r12
            boolean r12 = r12.isEmpty()
            r0 = 1
            r12 = r12 ^ r0
            r1 = 0
            if (r12 == 0) goto L_0x006a
            int r12 = com.iqonic.store.R.id.ivProduct
            android.view.View r12 = r10.findViewById(r12)
            r2 = r12
            android.widget.ImageView r2 = (android.widget.ImageView) r2
            java.lang.String r12 = "view.ivProduct"
            kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r2, r12)
            java.util.List r12 = r11.getImages()
            if (r12 != 0) goto L_0x0037
            kotlin.jvm.internal.Intrinsics.throwNpe()
        L_0x0037:
            java.lang.Object r12 = r12.get(r1)
            com.iqonic.store.models.Image r12 = (com.iqonic.store.models.Image) r12
            java.lang.String r3 = r12.getSrc()
            if (r3 != 0) goto L_0x0046
            kotlin.jvm.internal.Intrinsics.throwNpe()
        L_0x0046:
            r4 = 0
            r5 = 0
            r6 = 6
            r7 = 0
            com.iqonic.store.utils.extensions.NetworkExtensionKt.loadImageFromUrl$default(r2, r3, r4, r5, r6, r7)
            com.iqonic.store.activity.SubCategoryActivity r12 = r9.this$0
            java.util.List r2 = r11.getImages()
            if (r2 != 0) goto L_0x0058
            kotlin.jvm.internal.Intrinsics.throwNpe()
        L_0x0058:
            java.lang.Object r2 = r2.get(r1)
            com.iqonic.store.models.Image r2 = (com.iqonic.store.models.Image) r2
            java.lang.String r2 = r2.getSrc()
            if (r2 != 0) goto L_0x0067
            kotlin.jvm.internal.Intrinsics.throwNpe()
        L_0x0067:
            r12.setImage(r2)
        L_0x006a:
            int r12 = com.iqonic.store.R.id.tvProductName
            android.view.View r12 = r10.findViewById(r12)
            android.widget.TextView r12 = (android.widget.TextView) r12
            java.lang.String r2 = "view.tvProductName"
            kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r12, r2)
            java.lang.String r3 = r11.getName()
            r4 = 0
            if (r3 == 0) goto L_0x0084
            android.text.Spanned r3 = com.iqonic.store.utils.extensions.StringExtensionsKt.getHtmlString(r3)
            goto L_0x0085
        L_0x0084:
            r3 = r4
        L_0x0085:
            java.lang.CharSequence r3 = (java.lang.CharSequence) r3
            r12.setText(r3)
            int r12 = com.iqonic.store.R.id.tvProductName
            android.view.View r12 = r10.findViewById(r12)
            android.widget.TextView r12 = (android.widget.TextView) r12
            kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r12, r2)
            com.iqonic.store.utils.extensions.ExtensionsKt.changeTextPrimaryColor(r12)
            java.lang.String r12 = r11.getSalePrice()
            if (r12 != 0) goto L_0x00a1
            kotlin.jvm.internal.Intrinsics.throwNpe()
        L_0x00a1:
            java.lang.CharSequence r12 = (java.lang.CharSequence) r12
            int r12 = r12.length()
            if (r12 != 0) goto L_0x00ab
            r12 = 1
            goto L_0x00ac
        L_0x00ab:
            r12 = 0
        L_0x00ac:
            r2 = 8
            java.lang.String r3 = "view.tvSaleLabel"
            java.lang.String r5 = ""
            java.lang.String r6 = "view.tvDiscountPrice"
            java.lang.String r7 = "view.tvOriginalPrice"
            if (r12 == 0) goto L_0x011f
            java.lang.String r12 = r11.getSalePrice()
            if (r12 != 0) goto L_0x00c4
            kotlin.jvm.internal.Intrinsics.throwNpe()
        L_0x00c4:
            java.lang.CharSequence r12 = (java.lang.CharSequence) r12
            int r12 = r12.length()
            if (r12 != 0) goto L_0x00ce
            r12 = 1
            goto L_0x00cf
        L_0x00ce:
            r12 = 0
        L_0x00cf:
            if (r12 == 0) goto L_0x011f
            int r12 = com.iqonic.store.R.id.tvDiscountPrice
            android.view.View r12 = r10.findViewById(r12)
            android.widget.TextView r12 = (android.widget.TextView) r12
            kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r12, r6)
            java.lang.String r8 = r11.getPrice()
            if (r8 != 0) goto L_0x00e5
            kotlin.jvm.internal.Intrinsics.throwNpe()
        L_0x00e5:
            java.lang.String r8 = com.iqonic.store.utils.extensions.StringExtensionsKt.currencyFormat$default(r8, r4, r0, r4)
            java.lang.CharSequence r8 = (java.lang.CharSequence) r8
            r12.setText(r8)
            int r12 = com.iqonic.store.R.id.tvOriginalPrice
            android.view.View r12 = r10.findViewById(r12)
            android.widget.TextView r12 = (android.widget.TextView) r12
            kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r12, r7)
            r12.setVisibility(r2)
            int r12 = com.iqonic.store.R.id.tvOriginalPrice
            android.view.View r12 = r10.findViewById(r12)
            android.widget.TextView r12 = (android.widget.TextView) r12
            kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r12, r7)
            r2 = r5
            java.lang.CharSequence r2 = (java.lang.CharSequence) r2
            r12.setText(r2)
            int r12 = com.iqonic.store.R.id.tvSaleLabel
            android.view.View r12 = r10.findViewById(r12)
            android.widget.TextView r12 = (android.widget.TextView) r12
            kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r12, r3)
            android.view.View r12 = (android.view.View) r12
            com.iqonic.store.utils.extensions.ViewExtensionsKt.hide(r12)
            goto L_0x01d5
        L_0x011f:
            boolean r12 = r11.getOnSale()
            if (r12 == 0) goto L_0x018a
            int r12 = com.iqonic.store.R.id.tvDiscountPrice
            android.view.View r12 = r10.findViewById(r12)
            android.widget.TextView r12 = (android.widget.TextView) r12
            kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r12, r6)
            java.lang.String r2 = r11.getSalePrice()
            if (r2 == 0) goto L_0x013b
            java.lang.String r2 = com.iqonic.store.utils.extensions.StringExtensionsKt.currencyFormat$default(r2, r4, r0, r4)
            goto L_0x013c
        L_0x013b:
            r2 = r4
        L_0x013c:
            java.lang.CharSequence r2 = (java.lang.CharSequence) r2
            r12.setText(r2)
            int r12 = com.iqonic.store.R.id.tvSaleLabel
            android.view.View r12 = r10.findViewById(r12)
            android.widget.TextView r12 = (android.widget.TextView) r12
            kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r12, r3)
            android.view.View r12 = (android.view.View) r12
            com.iqonic.store.utils.extensions.ViewExtensionsKt.show(r12)
            int r12 = com.iqonic.store.R.id.tvOriginalPrice
            android.view.View r12 = r10.findViewById(r12)
            android.widget.TextView r12 = (android.widget.TextView) r12
            kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r12, r7)
            com.iqonic.store.utils.extensions.ExtensionsKt.applyStrike(r12)
            int r12 = com.iqonic.store.R.id.tvOriginalPrice
            android.view.View r12 = r10.findViewById(r12)
            android.widget.TextView r12 = (android.widget.TextView) r12
            kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r12, r7)
            java.lang.String r2 = r11.getRegularPrice()
            if (r2 == 0) goto L_0x0175
            java.lang.String r2 = com.iqonic.store.utils.extensions.StringExtensionsKt.currencyFormat$default(r2, r4, r0, r4)
            goto L_0x0176
        L_0x0175:
            r2 = r4
        L_0x0176:
            java.lang.CharSequence r2 = (java.lang.CharSequence) r2
            r12.setText(r2)
            int r12 = com.iqonic.store.R.id.tvOriginalPrice
            android.view.View r12 = r10.findViewById(r12)
            android.widget.TextView r12 = (android.widget.TextView) r12
            kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r12, r7)
            r12.setVisibility(r1)
            goto L_0x01d5
        L_0x018a:
            int r12 = com.iqonic.store.R.id.tvDiscountPrice
            android.view.View r12 = r10.findViewById(r12)
            android.widget.TextView r12 = (android.widget.TextView) r12
            kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r12, r6)
            java.lang.String r8 = r11.getRegularPrice()
            if (r8 == 0) goto L_0x01a0
            java.lang.String r8 = com.iqonic.store.utils.extensions.StringExtensionsKt.currencyFormat$default(r8, r4, r0, r4)
            goto L_0x01a1
        L_0x01a0:
            r8 = r4
        L_0x01a1:
            java.lang.CharSequence r8 = (java.lang.CharSequence) r8
            r12.setText(r8)
            int r12 = com.iqonic.store.R.id.tvOriginalPrice
            android.view.View r12 = r10.findViewById(r12)
            android.widget.TextView r12 = (android.widget.TextView) r12
            kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r12, r7)
            r8 = r5
            java.lang.CharSequence r8 = (java.lang.CharSequence) r8
            r12.setText(r8)
            int r12 = com.iqonic.store.R.id.tvOriginalPrice
            android.view.View r12 = r10.findViewById(r12)
            android.widget.TextView r12 = (android.widget.TextView) r12
            kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r12, r7)
            r12.setVisibility(r2)
            int r12 = com.iqonic.store.R.id.tvSaleLabel
            android.view.View r12 = r10.findViewById(r12)
            android.widget.TextView r12 = (android.widget.TextView) r12
            kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r12, r3)
            android.view.View r12 = (android.view.View) r12
            com.iqonic.store.utils.extensions.ViewExtensionsKt.hide(r12)
        L_0x01d5:
            int r12 = com.iqonic.store.R.id.tvOriginalPrice
            android.view.View r12 = r10.findViewById(r12)
            android.widget.TextView r12 = (android.widget.TextView) r12
            kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r12, r7)
            com.iqonic.store.utils.extensions.ExtensionsKt.changeTextSecondaryColor(r12)
            int r12 = com.iqonic.store.R.id.tvDiscountPrice
            android.view.View r12 = r10.findViewById(r12)
            android.widget.TextView r12 = (android.widget.TextView) r12
            kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r12, r6)
            com.iqonic.store.utils.extensions.ExtensionsKt.changePrimaryColor(r12)
            int r12 = com.iqonic.store.R.id.tvAdd
            android.view.View r12 = r10.findViewById(r12)
            android.widget.TextView r12 = (android.widget.TextView) r12
            java.lang.String r2 = "view.tvAdd"
            kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r12, r2)
            java.lang.String r3 = com.iqonic.store.utils.extensions.AppExtensionsKt.getAccentColor()
            com.iqonic.store.utils.extensions.ExtensionsKt.changeBackgroundTint(r12, r3)
            java.util.List r12 = r11.getAttributes()
            if (r12 != 0) goto L_0x020f
            kotlin.jvm.internal.Intrinsics.throwNpe()
        L_0x020f:
            java.util.Collection r12 = (java.util.Collection) r12
            boolean r12 = r12.isEmpty()
            r12 = r12 ^ r0
            java.lang.String r0 = "view.tvProductWeight"
            if (r12 == 0) goto L_0x0255
            int r12 = com.iqonic.store.R.id.tvProductWeight
            android.view.View r12 = r10.findViewById(r12)
            android.widget.TextView r12 = (android.widget.TextView) r12
            kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r12, r0)
            java.util.List r3 = r11.getAttributes()
            if (r3 == 0) goto L_0x0238
            java.lang.Object r3 = r3.get(r1)
            com.iqonic.store.models.Attributes r3 = (com.iqonic.store.models.Attributes) r3
            if (r3 == 0) goto L_0x0238
            java.util.List r4 = r3.getOptions()
        L_0x0238:
            if (r4 != 0) goto L_0x023d
            kotlin.jvm.internal.Intrinsics.throwNpe()
        L_0x023d:
            java.lang.Object r1 = r4.get(r1)
            java.lang.CharSequence r1 = (java.lang.CharSequence) r1
            r12.setText(r1)
            int r12 = com.iqonic.store.R.id.tvProductWeight
            android.view.View r12 = r10.findViewById(r12)
            android.widget.TextView r12 = (android.widget.TextView) r12
            kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r12, r0)
            com.iqonic.store.utils.extensions.ExtensionsKt.changeAccentColor(r12)
            goto L_0x0265
        L_0x0255:
            int r12 = com.iqonic.store.R.id.tvProductWeight
            android.view.View r12 = r10.findViewById(r12)
            android.widget.TextView r12 = (android.widget.TextView) r12
            kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r12, r0)
            java.lang.CharSequence r5 = (java.lang.CharSequence) r5
            r12.setText(r5)
        L_0x0265:
            boolean r12 = r11.getPurchasable()
            if (r12 == 0) goto L_0x0299
            java.lang.String r12 = r11.getStockStatus()
            java.lang.String r0 = "instock"
            boolean r12 = kotlin.jvm.internal.Intrinsics.areEqual((java.lang.Object) r12, (java.lang.Object) r0)
            if (r12 == 0) goto L_0x0288
            int r12 = com.iqonic.store.R.id.tvAdd
            android.view.View r12 = r10.findViewById(r12)
            android.widget.TextView r12 = (android.widget.TextView) r12
            kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r12, r2)
            android.view.View r12 = (android.view.View) r12
            com.iqonic.store.utils.extensions.ViewExtensionsKt.show(r12)
            goto L_0x02a9
        L_0x0288:
            int r12 = com.iqonic.store.R.id.tvAdd
            android.view.View r12 = r10.findViewById(r12)
            android.widget.TextView r12 = (android.widget.TextView) r12
            kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r12, r2)
            android.view.View r12 = (android.view.View) r12
            com.iqonic.store.utils.extensions.ViewExtensionsKt.hide(r12)
            goto L_0x02a9
        L_0x0299:
            int r12 = com.iqonic.store.R.id.tvAdd
            android.view.View r12 = r10.findViewById(r12)
            android.widget.TextView r12 = (android.widget.TextView) r12
            kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r12, r2)
            android.view.View r12 = (android.view.View) r12
            com.iqonic.store.utils.extensions.ViewExtensionsKt.hide(r12)
        L_0x02a9:
            int r12 = com.iqonic.store.R.id.tvAdd
            android.view.View r12 = r10.findViewById(r12)
            android.widget.TextView r12 = (android.widget.TextView) r12
            com.iqonic.store.activity.SubCategoryActivity$mProductAdapter$1$$special$$inlined$onClick$1 r0 = new com.iqonic.store.activity.SubCategoryActivity$mProductAdapter$1$$special$$inlined$onClick$1
            r0.<init>(r12, r9, r11)
            android.view.View$OnClickListener r0 = (android.view.View.OnClickListener) r0
            r12.setOnClickListener(r0)
            com.iqonic.store.activity.SubCategoryActivity$mProductAdapter$1$$special$$inlined$onClick$2 r12 = new com.iqonic.store.activity.SubCategoryActivity$mProductAdapter$1$$special$$inlined$onClick$2
            r12.<init>(r10, r9, r11)
            android.view.View$OnClickListener r12 = (android.view.View.OnClickListener) r12
            r10.setOnClickListener(r12)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.iqonic.store.activity.SubCategoryActivity$mProductAdapter$1.invoke(android.view.View, com.iqonic.store.models.StoreProductModel, int):void");
    }
}
