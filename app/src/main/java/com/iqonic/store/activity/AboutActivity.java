package com.iqonic.store.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.appcompat.widget.Toolbar;
import com.iqonic.store.AppBaseActivity;
import com.iqonic.store.utils.Constants;
import com.iqonic.store.utils.SharedPrefUtils;
import com.iqonic.store.utils.extensions.AppExtensionsKt;
import com.iqonic.store.utils.extensions.ExtensionsKt;
import com.iqonic.store.utils.extensions.ViewExtensionsKt;
import com.store.proshop.R;
import java.util.HashMap;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\b\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\b\u0010\f\u001a\u00020\rH\u0002J\u0012\u0010\u000e\u001a\u00020\r2\b\u0010\u000f\u001a\u0004\u0018\u00010\u0010H\u0015R\u000e\u0010\u0003\u001a\u00020\u0004X\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004X\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004X\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0004X\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0004X\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u0004X\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u0004X\u000e¢\u0006\u0002\n\u0000¨\u0006\u0011"}, d2 = {"Lcom/iqonic/store/activity/AboutActivity;", "Lcom/iqonic/store/AppBaseActivity;", "()V", "contact", "", "copyRight", "facebook", "instagram", "privacy", "toc", "twitter", "whatsUp", "changeColor", "", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: AboutActivity.kt */
public final class AboutActivity extends AppBaseActivity {
    private HashMap _$_findViewCache;
    /* access modifiers changed from: private */
    public String contact = "";
    private String copyRight = "";
    /* access modifiers changed from: private */
    public String facebook = "";
    /* access modifiers changed from: private */
    public String instagram = "";
    /* access modifiers changed from: private */
    public String privacy = "";
    /* access modifiers changed from: private */
    public String toc = "";
    /* access modifiers changed from: private */
    public String twitter = "";
    /* access modifiers changed from: private */
    public String whatsUp = "";

    public void _$_clearFindViewByIdCache() {
        HashMap hashMap = this._$_findViewCache;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    public View _$_findCachedViewById(int i) {
        if (this._$_findViewCache == null) {
            this._$_findViewCache = new HashMap();
        }
        View view = (View) this._$_findViewCache.get(Integer.valueOf(i));
        if (view != null) {
            return view;
        }
        View findViewById = findViewById(i);
        this._$_findViewCache.put(Integer.valueOf(i), findViewById);
        return findViewById;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_about);
        Toolbar toolbar = (Toolbar) _$_findCachedViewById(com.iqonic.store.R.id.toolbar);
        Intrinsics.checkExpressionValueIsNotNull(toolbar, "toolbar");
        setToolbar(toolbar);
        setTitle(getString(R.string.lbl_about));
        mAppBarColor();
        changeColor();
        SharedPrefUtils sharedPrefInstance = AppExtensionsKt.getSharedPrefInstance();
        this.whatsUp = SharedPrefUtils.getStringValue$default(sharedPrefInstance, Constants.SharedPref.WHATSAPP, (String) null, 2, (Object) null);
        this.instagram = SharedPrefUtils.getStringValue$default(sharedPrefInstance, Constants.SharedPref.INSTAGRAM, (String) null, 2, (Object) null);
        this.twitter = SharedPrefUtils.getStringValue$default(sharedPrefInstance, Constants.SharedPref.TWITTER, (String) null, 2, (Object) null);
        this.facebook = SharedPrefUtils.getStringValue$default(sharedPrefInstance, Constants.SharedPref.FACEBOOK, (String) null, 2, (Object) null);
        this.contact = SharedPrefUtils.getStringValue$default(sharedPrefInstance, Constants.SharedPref.CONTACT, (String) null, 2, (Object) null);
        this.copyRight = SharedPrefUtils.getStringValue$default(sharedPrefInstance, Constants.SharedPref.COPYRIGHT_TEXT, (String) null, 2, (Object) null);
        this.privacy = SharedPrefUtils.getStringValue$default(sharedPrefInstance, Constants.SharedPref.PRIVACY_POLICY, (String) null, 2, (Object) null);
        this.toc = SharedPrefUtils.getStringValue$default(sharedPrefInstance, Constants.SharedPref.TERM_CONDITION, (String) null, 2, (Object) null);
        boolean z = true;
        if (this.copyRight.length() == 0) {
            TextView textView = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvCopyRight);
            Intrinsics.checkExpressionValueIsNotNull(textView, "tvCopyRight");
            ViewExtensionsKt.hide(textView);
        } else {
            TextView textView2 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvCopyRight);
            Intrinsics.checkExpressionValueIsNotNull(textView2, "tvCopyRight");
            textView2.setText(this.copyRight);
            TextView textView3 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvCopyRight);
            Intrinsics.checkExpressionValueIsNotNull(textView3, "tvCopyRight");
            ViewExtensionsKt.show(textView3);
        }
        TextView textView4 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvVersion);
        Intrinsics.checkExpressionValueIsNotNull(textView4, "tvVersion");
        textView4.setText("Version: 16.0.0.16");
        if (this.whatsUp.length() == 0) {
            ImageView imageView = (ImageView) _$_findCachedViewById(com.iqonic.store.R.id.iv_whatsapp);
            Intrinsics.checkExpressionValueIsNotNull(imageView, "iv_whatsapp");
            ViewExtensionsKt.hide(imageView);
        }
        if (this.privacy.length() == 0) {
            TextView textView5 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvPrivacyPolicy);
            Intrinsics.checkExpressionValueIsNotNull(textView5, "tvPrivacyPolicy");
            ViewExtensionsKt.hide(textView5);
        }
        if (this.toc.length() == 0) {
            TextView textView6 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvTOC);
            Intrinsics.checkExpressionValueIsNotNull(textView6, "tvTOC");
            ViewExtensionsKt.hide(textView6);
        }
        if (this.instagram.length() == 0) {
            ImageView imageView2 = (ImageView) _$_findCachedViewById(com.iqonic.store.R.id.iv_instagram);
            Intrinsics.checkExpressionValueIsNotNull(imageView2, "iv_instagram");
            ViewExtensionsKt.hide(imageView2);
        }
        if (this.twitter.length() == 0) {
            ImageView imageView3 = (ImageView) _$_findCachedViewById(com.iqonic.store.R.id.iv_twitter_sign);
            Intrinsics.checkExpressionValueIsNotNull(imageView3, "iv_twitter_sign");
            ViewExtensionsKt.hide(imageView3);
        }
        if (this.facebook.length() == 0) {
            ImageView imageView4 = (ImageView) _$_findCachedViewById(com.iqonic.store.R.id.iv_facebook);
            Intrinsics.checkExpressionValueIsNotNull(imageView4, "iv_facebook");
            ViewExtensionsKt.hide(imageView4);
        }
        if (this.contact.length() == 0) {
            ImageView imageView5 = (ImageView) _$_findCachedViewById(com.iqonic.store.R.id.iv_contact);
            Intrinsics.checkExpressionValueIsNotNull(imageView5, "iv_contact");
            ViewExtensionsKt.hide(imageView5);
        }
        LinearLayout linearLayout = (LinearLayout) _$_findCachedViewById(com.iqonic.store.R.id.llBottom);
        Intrinsics.checkExpressionValueIsNotNull(linearLayout, "llBottom");
        ViewExtensionsKt.show(linearLayout);
        ImageView imageView6 = (ImageView) _$_findCachedViewById(com.iqonic.store.R.id.iv_whatsapp);
        imageView6.setOnClickListener(new AboutActivity$onCreate$$inlined$onClick$1(imageView6, this));
        ImageView imageView7 = (ImageView) _$_findCachedViewById(com.iqonic.store.R.id.iv_instagram);
        imageView7.setOnClickListener(new AboutActivity$onCreate$$inlined$onClick$2(imageView7, this));
        ImageView imageView8 = (ImageView) _$_findCachedViewById(com.iqonic.store.R.id.iv_twitter_sign);
        imageView8.setOnClickListener(new AboutActivity$onCreate$$inlined$onClick$3(imageView8, this));
        ImageView imageView9 = (ImageView) _$_findCachedViewById(com.iqonic.store.R.id.iv_facebook);
        imageView9.setOnClickListener(new AboutActivity$onCreate$$inlined$onClick$4(imageView9, this));
        ImageView imageView10 = (ImageView) _$_findCachedViewById(com.iqonic.store.R.id.iv_contact);
        imageView10.setOnClickListener(new AboutActivity$onCreate$$inlined$onClick$5(imageView10, this));
        TextView textView7 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvPrivacyPolicy);
        textView7.setOnClickListener(new AboutActivity$onCreate$$inlined$onClick$6(textView7, this));
        TextView textView8 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvTOC);
        textView8.setOnClickListener(new AboutActivity$onCreate$$inlined$onClick$7(textView8, this));
        if (this.whatsUp.length() == 0) {
            if (this.instagram.length() == 0) {
                if (this.twitter.length() == 0) {
                    if (this.facebook.length() == 0) {
                        if (this.contact.length() != 0) {
                            z = false;
                        }
                        if (z) {
                            TextView textView9 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.llFollow);
                            Intrinsics.checkExpressionValueIsNotNull(textView9, "llFollow");
                            ViewExtensionsKt.hide(textView9);
                            return;
                        }
                    }
                }
            }
        }
        TextView textView10 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.llFollow);
        Intrinsics.checkExpressionValueIsNotNull(textView10, "llFollow");
        ViewExtensionsKt.show(textView10);
    }

    private final void changeColor() {
        TextView textView = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvAppName);
        Intrinsics.checkExpressionValueIsNotNull(textView, "tvAppName");
        ExtensionsKt.changePrimaryColor(textView);
        TextView textView2 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvVersion);
        Intrinsics.checkExpressionValueIsNotNull(textView2, "tvVersion");
        ExtensionsKt.changePrimaryColor(textView2);
        TextView textView3 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvCopyRight);
        Intrinsics.checkExpressionValueIsNotNull(textView3, "tvCopyRight");
        ExtensionsKt.changePrimaryColor(textView3);
        TextView textView4 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvTOC);
        Intrinsics.checkExpressionValueIsNotNull(textView4, "tvTOC");
        ExtensionsKt.changePrimaryColor(textView4);
        TextView textView5 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.tvPrivacyPolicy);
        Intrinsics.checkExpressionValueIsNotNull(textView5, "tvPrivacyPolicy");
        ExtensionsKt.changePrimaryColor(textView5);
        TextView textView6 = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.llFollow);
        Intrinsics.checkExpressionValueIsNotNull(textView6, "llFollow");
        ExtensionsKt.changeTextPrimaryColor(textView6);
        RelativeLayout relativeLayout = (RelativeLayout) _$_findCachedViewById(com.iqonic.store.R.id.rlMain);
        Intrinsics.checkExpressionValueIsNotNull(relativeLayout, "rlMain");
        ExtensionsKt.changeBackgroundColor(relativeLayout);
    }
}
