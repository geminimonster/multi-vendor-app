package com.iqonic.store.activity;

import android.content.Intent;
import com.iqonic.store.utils.Builder;
import com.iqonic.store.utils.Constants;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001*\u00020\u0002H\n¢\u0006\u0002\b\u0003"}, d2 = {"<anonymous>", "", "Lcom/iqonic/store/utils/Builder;", "invoke"}, k = 3, mv = {1, 1, 16})
/* compiled from: MyCartActivity.kt */
final class MyCartActivity$onCreate$1 extends Lambda implements Function1<Builder, Unit> {
    final /* synthetic */ MyCartActivity this$0;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    MyCartActivity$onCreate$1(MyCartActivity myCartActivity) {
        super(1);
        this.this$0 = myCartActivity;
    }

    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        invoke((Builder) obj);
        return Unit.INSTANCE;
    }

    public final void invoke(Builder builder) {
        Intrinsics.checkParameterIsNotNull(builder, "$receiver");
        builder.onAction(Constants.AppBroadcasts.CARTITEM_UPDATE, new Function1<Intent, Unit>(this) {
            final /* synthetic */ MyCartActivity$onCreate$1 this$0;

            {
                this.this$0 = r1;
            }

            public /* bridge */ /* synthetic */ Object invoke(Object obj) {
                invoke((Intent) obj);
                return Unit.INSTANCE;
            }

            public final void invoke(Intent intent) {
                Intrinsics.checkParameterIsNotNull(intent, "it");
                this.this$0.this$0.myCartFragment.invalidateCartLayout();
            }
        });
    }
}
