package com.iqonic.store.activity;

import android.app.Activity;
import android.app.Dialog;
import android.view.View;
import android.widget.EditText;
import android.widget.RatingBar;
import com.google.android.material.button.MaterialButton;
import com.iqonic.store.R;
import com.iqonic.store.models.RequestModel;
import com.iqonic.store.utils.extensions.AppExtensionsKt;
import com.iqonic.store.utils.extensions.EditTextExtensionsKt;
import com.iqonic.store.utils.extensions.ExtensionsKt;
import com.iqonic.store.utils.extensions.NetworkExtensionKt;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\b\u0003\n\u0002\b\u0004\u0010\u0000\u001a\u00020\u0001\"\b\b\u0000\u0010\u0002*\u00020\u00032\u000e\u0010\u0004\u001a\n \u0005*\u0004\u0018\u00010\u00030\u0003H\n¢\u0006\u0002\b\u0006¨\u0006\u0007"}, d2 = {"<anonymous>", "", "T", "Landroid/view/View;", "it", "kotlin.jvm.PlatformType", "onClick", "com/iqonic/store/utils/extensions/ExtensionsKt$onClick$1"}, k = 3, mv = {1, 1, 16})
/* compiled from: Extensions.kt */
public final class ReviewsActivity$createProductReview$$inlined$onClick$1 implements View.OnClickListener {
    final /* synthetic */ Dialog $dialog$inlined;
    final /* synthetic */ View $this_onClick;
    final /* synthetic */ ReviewsActivity this$0;

    public ReviewsActivity$createProductReview$$inlined$onClick$1(View view, ReviewsActivity reviewsActivity, Dialog dialog) {
        this.$this_onClick = view;
        this.this$0 = reviewsActivity;
        this.$dialog$inlined = dialog;
    }

    public final void onClick(View view) {
        MaterialButton materialButton = (MaterialButton) this.$this_onClick;
        EditText editText = (EditText) this.$dialog$inlined.findViewById(R.id.edtReview);
        Intrinsics.checkExpressionValueIsNotNull(editText, "dialog.edtReview");
        if (EditTextExtensionsKt.textToString(editText).length() > 0) {
            RequestModel requestModel = new RequestModel();
            requestModel.setProduct_id(Integer.valueOf(this.this$0.mPId));
            requestModel.setReviewer(AppExtensionsKt.getFirstName() + " " + AppExtensionsKt.getLastName());
            requestModel.setReviewer_email(AppExtensionsKt.getEmail());
            EditText editText2 = (EditText) this.$dialog$inlined.findViewById(R.id.edtReview);
            Intrinsics.checkExpressionValueIsNotNull(editText2, "dialog.edtReview");
            requestModel.setReview(EditTextExtensionsKt.textToString(editText2));
            RatingBar ratingBar = (RatingBar) this.$dialog$inlined.findViewById(R.id.ratingBar);
            Intrinsics.checkExpressionValueIsNotNull(ratingBar, "dialog.ratingBar");
            requestModel.setRating(String.valueOf(ratingBar.getRating()));
            if (ExtensionsKt.isNetworkAvailable()) {
                this.this$0.showProgress(true);
                NetworkExtensionKt.getRestApiImpl$default((String) null, 1, (Object) null).createProductReview(requestModel, new ReviewsActivity$createProductReview$$inlined$onClick$1$lambda$1(this), new ReviewsActivity$createProductReview$$inlined$onClick$1$lambda$2(materialButton, this));
                return;
            }
            this.this$0.showProgress(false);
            this.$dialog$inlined.dismiss();
            ExtensionsKt.noInternetSnackBar(this.this$0);
            return;
        }
        ExtensionsKt.toast((Activity) this.this$0, "Write your review", 0);
    }
}
