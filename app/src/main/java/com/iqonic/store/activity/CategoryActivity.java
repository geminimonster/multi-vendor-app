package com.iqonic.store.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.iqonic.store.AppBaseActivity;
import com.iqonic.store.adapter.BaseAdapter;
import com.iqonic.store.models.Category;
import com.iqonic.store.utils.extensions.AppExtensionsKt;
import com.iqonic.store.utils.extensions.ExtensionsKt;
import com.iqonic.store.utils.extensions.NetworkExtensionKt;
import com.store.proshop.R;
import java.util.HashMap;
import java.util.Map;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010%\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\b\u0010\u0010\u001a\u00020\u0011H\u0002J\u001c\u0010\u0012\u001a\u00020\u00112\u0012\u0010\u0005\u001a\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u00040\u0006H\u0002J\u0012\u0010\u0013\u001a\u00020\u00112\b\u0010\u0014\u001a\u0004\u0018\u00010\u0015H\u0015R\u000e\u0010\u0003\u001a\u00020\u0004X\u000e¢\u0006\u0002\n\u0000R\u001a\u0010\u0005\u001a\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u00040\u0006X\u0004¢\u0006\u0002\n\u0000R\u0012\u0010\b\u001a\u0004\u0018\u00010\tX\u000e¢\u0006\u0004\n\u0002\u0010\nR\u000e\u0010\u000b\u001a\u00020\tX\u000e¢\u0006\u0002\n\u0000R\u0014\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u000e0\rX\u0004¢\u0006\u0002\n\u0000R\u0012\u0010\u000f\u001a\u0004\u0018\u00010\tX\u000e¢\u0006\u0004\n\u0002\u0010\n¨\u0006\u0016"}, d2 = {"Lcom/iqonic/store/activity/CategoryActivity;", "Lcom/iqonic/store/AppBaseActivity;", "()V", "countLoadMore", "", "data", "", "", "isLastPage", "", "Ljava/lang/Boolean;", "mIsLoading", "mProductAdapter", "Lcom/iqonic/store/adapter/BaseAdapter;", "Lcom/iqonic/store/models/Category;", "showPagination", "changeColor", "", "loadData", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: CategoryActivity.kt */
public final class CategoryActivity extends AppBaseActivity {
    private HashMap _$_findViewCache;
    /* access modifiers changed from: private */
    public int countLoadMore = 1;
    /* access modifiers changed from: private */
    public final Map<String, Integer> data = new HashMap();
    /* access modifiers changed from: private */
    public Boolean isLastPage = false;
    /* access modifiers changed from: private */
    public boolean mIsLoading;
    /* access modifiers changed from: private */
    public final BaseAdapter<Category> mProductAdapter = new BaseAdapter<>(R.layout.item_viewcat, new CategoryActivity$mProductAdapter$1(this));
    private Boolean showPagination = true;

    public void _$_clearFindViewByIdCache() {
        HashMap hashMap = this._$_findViewCache;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    public View _$_findCachedViewById(int i) {
        if (this._$_findViewCache == null) {
            this._$_findViewCache = new HashMap();
        }
        View view = (View) this._$_findViewCache.get(Integer.valueOf(i));
        if (view != null) {
            return view;
        }
        View findViewById = findViewById(i);
        this._$_findViewCache.put(Integer.valueOf(i), findViewById);
        return findViewById;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_category);
        Toolbar toolbar = (Toolbar) _$_findCachedViewById(com.iqonic.store.R.id.toolbar);
        Intrinsics.checkExpressionValueIsNotNull(toolbar, "toolbar");
        setToolbar(toolbar);
        setTitle(getString(R.string.lbl_category));
        mAppBarColor();
        changeColor();
        this.data.put("page", Integer.valueOf(this.countLoadMore));
        this.data.put("per_page", 20);
        loadData(this.data);
        RecyclerView recyclerView = (RecyclerView) _$_findCachedViewById(com.iqonic.store.R.id.rvNewestProduct);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 2));
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(this.mProductAdapter);
        AppExtensionsKt.rvItemAnimation(recyclerView);
        Boolean bool = this.showPagination;
        if (bool == null) {
            Intrinsics.throwNpe();
        }
        if (bool.booleanValue()) {
            recyclerView.addOnScrollListener(new CategoryActivity$onCreate$$inlined$apply$lambda$1(this));
        }
    }

    /* access modifiers changed from: private */
    public final void loadData(Map<String, Integer> map) {
        if (ExtensionsKt.isNetworkAvailable()) {
            showProgress(true);
            NetworkExtensionKt.getRestApiImpl$default((String) null, 1, (Object) null).listAllCategory(map, new CategoryActivity$loadData$1(this), new CategoryActivity$loadData$2(this));
        }
    }

    private final void changeColor() {
        LinearLayout linearLayout = (LinearLayout) _$_findCachedViewById(com.iqonic.store.R.id.llMain);
        Intrinsics.checkExpressionValueIsNotNull(linearLayout, "llMain");
        ExtensionsKt.changeBackgroundColor(linearLayout);
    }
}
