package com.iqonic.store.activity;

import com.iqonic.store.models.StoreAttribute;
import com.iqonic.store.models.StoreProductAttribute;
import com.iqonic.store.models.Term;
import java.util.List;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.collections.CollectionsKt;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n¢\u0006\u0002\b\u0004"}, d2 = {"<anonymous>", "", "it", "Lcom/iqonic/store/models/StoreProductAttribute;", "invoke"}, k = 3, mv = {1, 1, 16})
/* compiled from: SearchActivity.kt */
final class SearchActivity$getProductAttribute$1 extends Lambda implements Function1<StoreProductAttribute, Unit> {
    final /* synthetic */ SearchActivity this$0;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    SearchActivity$getProductAttribute$1(SearchActivity searchActivity) {
        super(1);
        this.this$0 = searchActivity;
    }

    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        invoke((StoreProductAttribute) obj);
        return Unit.INSTANCE;
    }

    public final void invoke(StoreProductAttribute storeProductAttribute) {
        Intrinsics.checkParameterIsNotNull(storeProductAttribute, "it");
        this.this$0.showProgress(false);
        this.this$0.mIsFilterDataLoaded = true;
        List<StoreAttribute> attribute = storeProductAttribute.getAttribute();
        if (attribute == null) {
            Intrinsics.throwNpe();
        }
        int i = 0;
        for (Object next : attribute) {
            int i2 = i + 1;
            if (i < 0) {
                CollectionsKt.throwIndexOverflow();
            }
            Term term = r8;
            Term term2 = new Term(0, (String) null, (String) null, (String) null, 0, (String) null, (String) null, 0, 0, 0, false, false, 4095, (DefaultConstructorMarker) null);
            term.setName(storeProductAttribute.getAttribute().get(i).getName());
            term.setParent(true);
            this.this$0.mTerms.add(term);
            int i3 = 0;
            for (Object next2 : ((StoreAttribute) next).getTerms()) {
                int i4 = i3 + 1;
                if (i3 < 0) {
                    CollectionsKt.throwIndexOverflow();
                }
                this.this$0.mTerms.add((Term) next2);
                i3 = i4;
            }
            i = i2;
        }
    }
}
