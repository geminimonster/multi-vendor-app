package com.iqonic.store.activity;

import android.app.Activity;
import android.view.View;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.drawerlayout.widget.DrawerLayout;
import com.iqonic.store.R;
import com.iqonic.store.ShopHopApp;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001f\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0007\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002*\u0001\u0000\b\n\u0018\u00002\u00020\u0001J\u0018\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\u0003H\u0016R\u000e\u0010\u0002\u001a\u00020\u0003XD¢\u0006\u0002\n\u0000¨\u0006\t"}, d2 = {"com/iqonic/store/activity/DashBoardActivity$setUpDrawerToggle$toggle$1", "Landroidx/appcompat/app/ActionBarDrawerToggle;", "scaleFactor", "", "onDrawerSlide", "", "drawerView", "Landroid/view/View;", "slideOffset", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: DashBoardActivity.kt */
public final class DashBoardActivity$setUpDrawerToggle$toggle$1 extends ActionBarDrawerToggle {
    private final float scaleFactor = 4.0f;
    final /* synthetic */ DashBoardActivity this$0;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    DashBoardActivity$setUpDrawerToggle$toggle$1(DashBoardActivity dashBoardActivity, Activity activity, DrawerLayout drawerLayout, int i, int i2) {
        super(activity, drawerLayout, i, i2);
        this.this$0 = dashBoardActivity;
    }

    public void onDrawerSlide(View view, float f) {
        Intrinsics.checkParameterIsNotNull(view, "drawerView");
        super.onDrawerSlide(view, f);
        float width = ((float) view.getWidth()) * f;
        String language = ShopHopApp.Companion.getLanguage();
        if (language.hashCode() == 3121 && language.equals("ar")) {
            View _$_findCachedViewById = this.this$0._$_findCachedViewById(R.id.main);
            Intrinsics.checkExpressionValueIsNotNull(_$_findCachedViewById, "main");
            _$_findCachedViewById.setTranslationX(-width);
        } else {
            View _$_findCachedViewById2 = this.this$0._$_findCachedViewById(R.id.main);
            Intrinsics.checkExpressionValueIsNotNull(_$_findCachedViewById2, "main");
            _$_findCachedViewById2.setTranslationX(width);
        }
        View _$_findCachedViewById3 = this.this$0._$_findCachedViewById(R.id.main);
        Intrinsics.checkExpressionValueIsNotNull(_$_findCachedViewById3, "main");
        float f2 = (float) 1;
        _$_findCachedViewById3.setScaleX(f2 - (f / this.scaleFactor));
        View _$_findCachedViewById4 = this.this$0._$_findCachedViewById(R.id.main);
        Intrinsics.checkExpressionValueIsNotNull(_$_findCachedViewById4, "main");
        _$_findCachedViewById4.setScaleY(f2 - (f / this.scaleFactor));
    }
}
