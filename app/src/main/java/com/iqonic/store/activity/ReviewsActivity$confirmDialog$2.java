package com.iqonic.store.activity;

import android.content.DialogInterface;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\n¢\u0006\u0002\b\u0006"}, d2 = {"<anonymous>", "", "dialog", "Landroid/content/DialogInterface;", "<anonymous parameter 1>", "", "invoke"}, k = 3, mv = {1, 1, 16})
/* compiled from: ReviewsActivity.kt */
final class ReviewsActivity$confirmDialog$2 extends Lambda implements Function2<DialogInterface, Integer, Unit> {
    public static final ReviewsActivity$confirmDialog$2 INSTANCE = new ReviewsActivity$confirmDialog$2();

    ReviewsActivity$confirmDialog$2() {
        super(2);
    }

    public /* bridge */ /* synthetic */ Object invoke(Object obj, Object obj2) {
        invoke((DialogInterface) obj, ((Number) obj2).intValue());
        return Unit.INSTANCE;
    }

    public final void invoke(DialogInterface dialogInterface, int i) {
        Intrinsics.checkParameterIsNotNull(dialogInterface, "dialog");
        dialogInterface.dismiss();
    }
}
