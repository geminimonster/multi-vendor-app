package com.iqonic.store.activity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.iqonic.store.AppBaseActivity;
import com.iqonic.store.adapter.BaseAdapter;
import com.iqonic.store.models.Category;
import com.iqonic.store.models.RequestModel;
import com.iqonic.store.models.StoreProductModel;
import com.iqonic.store.utils.Constants;
import com.iqonic.store.utils.extensions.AppExtensionsKt;
import com.iqonic.store.utils.extensions.ExtensionsKt;
import com.iqonic.store.utils.extensions.NetworkExtensionKt;
import com.store.proshop.R;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000F\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010%\n\u0002\u0010\u000e\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0019\u001a\u00020\u001a2\u0006\u0010\u001b\u001a\u00020\u0014H\u0002J\u001c\u0010\u001c\u001a\u00020\u001a2\u0012\u0010\u0005\u001a\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u00040\u0006H\u0002J\u001c\u0010\u001d\u001a\u00020\u001a2\u0012\u0010\u0005\u001a\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u00040\u0006H\u0002J\u0012\u0010\u001e\u001a\u00020\u001a2\b\u0010\u001f\u001a\u0004\u0018\u00010 H\u0015R\u000e\u0010\u0003\u001a\u00020\u0004X\u000e¢\u0006\u0002\n\u0000R\u001a\u0010\u0005\u001a\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u00040\u0006X\u0004¢\u0006\u0002\n\u0000R\u001a\u0010\b\u001a\u00020\u0007X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\t\u0010\n\"\u0004\b\u000b\u0010\fR\u0012\u0010\r\u001a\u0004\u0018\u00010\u000eX\u000e¢\u0006\u0004\n\u0002\u0010\u000fR\u000e\u0010\u0010\u001a\u00020\u0004X\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u000eX\u000e¢\u0006\u0002\n\u0000R\u0014\u0010\u0012\u001a\b\u0012\u0004\u0012\u00020\u00140\u0013X\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\u0015\u001a\b\u0012\u0004\u0012\u00020\u00160\u0013X\u0004¢\u0006\u0002\n\u0000R\u0012\u0010\u0017\u001a\u0004\u0018\u00010\u000eX\u000e¢\u0006\u0004\n\u0002\u0010\u000fR\u001a\u0010\u0018\u001a\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u00040\u0006X\u0004¢\u0006\u0002\n\u0000¨\u0006!"}, d2 = {"Lcom/iqonic/store/activity/SubCategoryActivity;", "Lcom/iqonic/store/AppBaseActivity;", "()V", "countLoadMore", "", "data", "", "", "image", "getImage", "()Ljava/lang/String;", "setImage", "(Ljava/lang/String;)V", "isLastPage", "", "Ljava/lang/Boolean;", "mCategoryId", "mIsLoading", "mProductAdapter", "Lcom/iqonic/store/adapter/BaseAdapter;", "Lcom/iqonic/store/models/StoreProductModel;", "mSubCategoryAdapter", "Lcom/iqonic/store/models/Category;", "showPagination", "subCategoryData", "addCart", "", "model", "loadCategory", "loadSubCategory", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: SubCategoryActivity.kt */
public final class SubCategoryActivity extends AppBaseActivity {
    private HashMap _$_findViewCache;
    /* access modifiers changed from: private */
    public int countLoadMore = 1;
    /* access modifiers changed from: private */
    public final Map<String, Integer> data = new HashMap();
    private String image = "";
    /* access modifiers changed from: private */
    public Boolean isLastPage = false;
    /* access modifiers changed from: private */
    public int mCategoryId;
    /* access modifiers changed from: private */
    public boolean mIsLoading;
    /* access modifiers changed from: private */
    public final BaseAdapter<StoreProductModel> mProductAdapter = new BaseAdapter<>(R.layout.item_viewproductgrid, new SubCategoryActivity$mProductAdapter$1(this));
    /* access modifiers changed from: private */
    public final BaseAdapter<Category> mSubCategoryAdapter = new BaseAdapter<>(R.layout.item_subcategory, new SubCategoryActivity$mSubCategoryAdapter$1(this));
    private Boolean showPagination = true;
    private final Map<String, Integer> subCategoryData = new HashMap();

    public void _$_clearFindViewByIdCache() {
        HashMap hashMap = this._$_findViewCache;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    public View _$_findCachedViewById(int i) {
        if (this._$_findViewCache == null) {
            this._$_findViewCache = new HashMap();
        }
        View view = (View) this._$_findViewCache.get(Integer.valueOf(i));
        if (view != null) {
            return view;
        }
        View findViewById = findViewById(i);
        this._$_findViewCache.put(Integer.valueOf(i), findViewById);
        return findViewById;
    }

    public final String getImage() {
        return this.image;
    }

    public final void setImage(String str) {
        Intrinsics.checkParameterIsNotNull(str, "<set-?>");
        this.image = str;
    }

    /* access modifiers changed from: private */
    public final void addCart(StoreProductModel storeProductModel) {
        if (AppExtensionsKt.isLoggedIn()) {
            RequestModel requestModel = new RequestModel();
            if (Intrinsics.areEqual((Object) storeProductModel.getType(), (Object) "variable")) {
                List<Integer> variations = storeProductModel.getVariations();
                if (variations == null) {
                    Intrinsics.throwNpe();
                }
                requestModel.setPro_id(variations.get(0));
            } else {
                requestModel.setPro_id(Integer.valueOf(storeProductModel.getId()));
            }
            requestModel.setQuantity(1);
            NetworkExtensionKt.addItemToCart(this, requestModel, new SubCategoryActivity$addCart$1(this));
            return;
        }
        Bundle bundle = null;
        Intent intent = new Intent(this, SignInUpActivity.class);
        SubCategoryActivity$addCart$2.INSTANCE.invoke(intent);
        if (Build.VERSION.SDK_INT >= 16) {
            startActivityForResult(intent, -1, bundle);
        } else {
            startActivityForResult(intent, -1);
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_sub_category);
        Toolbar toolbar = (Toolbar) _$_findCachedViewById(com.iqonic.store.R.id.toolbar);
        Intrinsics.checkExpressionValueIsNotNull(toolbar, "toolbar");
        setToolbar(toolbar);
        this.mCategoryId = getIntent().getIntExtra(Constants.KeyIntent.KEYID, -1);
        setTitle(getIntent().getStringExtra("title"));
        mAppBarColor();
        LinearLayout linearLayout = (LinearLayout) _$_findCachedViewById(com.iqonic.store.R.id.llMain);
        Intrinsics.checkExpressionValueIsNotNull(linearLayout, "llMain");
        ExtensionsKt.changeBackgroundColor(linearLayout);
        RecyclerView recyclerView = (RecyclerView) _$_findCachedViewById(com.iqonic.store.R.id.rvCategory);
        ExtensionsKt.setHorizontalLayout(recyclerView, false);
        recyclerView.setHasFixedSize(true);
        RecyclerView recyclerView2 = (RecyclerView) _$_findCachedViewById(com.iqonic.store.R.id.rvCategory);
        Intrinsics.checkExpressionValueIsNotNull(recyclerView2, "rvCategory");
        recyclerView2.setAdapter(this.mSubCategoryAdapter);
        RecyclerView recyclerView3 = (RecyclerView) _$_findCachedViewById(com.iqonic.store.R.id.rvCategory);
        Intrinsics.checkExpressionValueIsNotNull(recyclerView3, "rvCategory");
        AppExtensionsKt.rvItemAnimation(recyclerView3);
        this.data.put("page", Integer.valueOf(this.countLoadMore));
        this.data.put("per_page", 20);
        this.data.put("category", Integer.valueOf(this.mCategoryId));
        loadCategory(this.data);
        this.subCategoryData.put("per_page", 50);
        this.subCategoryData.put("parent", Integer.valueOf(this.mCategoryId));
        loadSubCategory(this.subCategoryData);
        RecyclerView recyclerView4 = (RecyclerView) _$_findCachedViewById(com.iqonic.store.R.id.rvNewestProduct);
        recyclerView4.setLayoutManager(new GridLayoutManager(this, 2));
        recyclerView4.setHasFixedSize(true);
        RecyclerView recyclerView5 = (RecyclerView) _$_findCachedViewById(com.iqonic.store.R.id.rvNewestProduct);
        Intrinsics.checkExpressionValueIsNotNull(recyclerView5, "rvNewestProduct");
        recyclerView5.setAdapter(this.mProductAdapter);
        RecyclerView recyclerView6 = (RecyclerView) _$_findCachedViewById(com.iqonic.store.R.id.rvNewestProduct);
        Intrinsics.checkExpressionValueIsNotNull(recyclerView6, "rvNewestProduct");
        AppExtensionsKt.rvItemAnimation(recyclerView6);
        Boolean bool = this.showPagination;
        if (bool == null) {
            Intrinsics.throwNpe();
        }
        if (bool.booleanValue()) {
            recyclerView4.addOnScrollListener(new SubCategoryActivity$onCreate$$inlined$apply$lambda$1(this));
        }
    }

    /* access modifiers changed from: private */
    public final void loadCategory(Map<String, Integer> map) {
        if (ExtensionsKt.isNetworkAvailable()) {
            showProgress(true);
            NetworkExtensionKt.getRestApiImpl$default((String) null, 1, (Object) null).listAllCategoryProduct(map, new SubCategoryActivity$loadCategory$1(this), new SubCategoryActivity$loadCategory$2(this));
            return;
        }
        showProgress(false);
        ExtensionsKt.noInternetSnackBar(this);
    }

    private final void loadSubCategory(Map<String, Integer> map) {
        if (ExtensionsKt.isNetworkAvailable()) {
            showProgress(true);
            NetworkExtensionKt.getRestApiImpl$default((String) null, 1, (Object) null).listAllCategory(map, new SubCategoryActivity$loadSubCategory$1(this), new SubCategoryActivity$loadSubCategory$2(this));
        }
    }
}
