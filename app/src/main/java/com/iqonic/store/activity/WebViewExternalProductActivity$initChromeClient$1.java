package com.iqonic.store.activity;

import android.view.View;
import android.view.Window;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.FrameLayout;
import androidx.appcompat.widget.Toolbar;
import com.iqonic.store.R;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Ref;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000-\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000*\u0001\u0000\b\n\u0018\u00002\u00020\u0001J\b\u0010\u0002\u001a\u00020\u0003H\u0016J\u001c\u0010\u0004\u001a\u00020\u00032\b\u0010\u0005\u001a\u0004\u0018\u00010\u00062\b\u0010\u0007\u001a\u0004\u0018\u00010\bH\u0016J\u001a\u0010\t\u001a\u00020\u00032\b\u0010\n\u001a\u0004\u0018\u00010\u000b2\u0006\u0010\f\u001a\u00020\rH\u0016¨\u0006\u000e"}, d2 = {"com/iqonic/store/activity/WebViewExternalProductActivity$initChromeClient$1", "Landroid/webkit/WebChromeClient;", "onHideCustomView", "", "onReceivedTitle", "view", "Landroid/webkit/WebView;", "title", "", "onShowCustomView", "paramView", "Landroid/view/View;", "paramCustomViewCallback", "Landroid/webkit/WebChromeClient$CustomViewCallback;", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: WebViewExternalProductActivity.kt */
public final class WebViewExternalProductActivity$initChromeClient$1 extends WebChromeClient {
    final /* synthetic */ Ref.ObjectRef $customView;
    final /* synthetic */ Ref.ObjectRef $customViewCallback;
    final /* synthetic */ Ref.IntRef $originalOrientation;
    final /* synthetic */ Ref.IntRef $originalSystemUiVisibility;
    final /* synthetic */ WebViewExternalProductActivity this$0;

    WebViewExternalProductActivity$initChromeClient$1(WebViewExternalProductActivity webViewExternalProductActivity, Ref.ObjectRef objectRef, Ref.IntRef intRef, Ref.IntRef intRef2, Ref.ObjectRef objectRef2) {
        this.this$0 = webViewExternalProductActivity;
        this.$customView = objectRef;
        this.$originalSystemUiVisibility = intRef;
        this.$originalOrientation = intRef2;
        this.$customViewCallback = objectRef2;
    }

    public void onReceivedTitle(WebView webView, String str) {
        super.onReceivedTitle(webView, str);
        Toolbar toolbar = (Toolbar) this.this$0._$_findCachedViewById(R.id.toolbar);
        Intrinsics.checkExpressionValueIsNotNull(toolbar, "toolbar");
        toolbar.setTitle((CharSequence) str);
    }

    public void onShowCustomView(View view, WebChromeClient.CustomViewCallback customViewCallback) {
        Intrinsics.checkParameterIsNotNull(customViewCallback, "paramCustomViewCallback");
        if (((View) this.$customView.element) != null) {
            onHideCustomView();
            return;
        }
        this.$customView.element = view;
        Ref.IntRef intRef = this.$originalSystemUiVisibility;
        Window window = this.this$0.getWindow();
        Intrinsics.checkExpressionValueIsNotNull(window, "window");
        View decorView = window.getDecorView();
        Intrinsics.checkExpressionValueIsNotNull(decorView, "window.decorView");
        intRef.element = decorView.getSystemUiVisibility();
        this.$originalOrientation.element = this.this$0.getRequestedOrientation();
        this.$customViewCallback.element = customViewCallback;
        Window window2 = this.this$0.getWindow();
        Intrinsics.checkExpressionValueIsNotNull(window2, "window");
        View decorView2 = window2.getDecorView();
        if (decorView2 != null) {
            ((FrameLayout) decorView2).addView((View) this.$customView.element, new FrameLayout.LayoutParams(-1, -1));
            Window window3 = this.this$0.getWindow();
            Intrinsics.checkExpressionValueIsNotNull(window3, "window");
            View decorView3 = window3.getDecorView();
            Intrinsics.checkExpressionValueIsNotNull(decorView3, "window.decorView");
            decorView3.setSystemUiVisibility(3846);
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type android.widget.FrameLayout");
    }

    public void onHideCustomView() {
        Window window = this.this$0.getWindow();
        Intrinsics.checkExpressionValueIsNotNull(window, "window");
        View decorView = window.getDecorView();
        if (decorView != null) {
            ((FrameLayout) decorView).removeView((View) this.$customView.element);
            this.$customView.element = (View) null;
            Window window2 = this.this$0.getWindow();
            Intrinsics.checkExpressionValueIsNotNull(window2, "window");
            View decorView2 = window2.getDecorView();
            Intrinsics.checkExpressionValueIsNotNull(decorView2, "window.decorView");
            decorView2.setSystemUiVisibility(this.$originalSystemUiVisibility.element);
            this.this$0.setRequestedOrientation(this.$originalOrientation.element);
            WebChromeClient.CustomViewCallback customViewCallback = (WebChromeClient.CustomViewCallback) this.$customViewCallback.element;
            if (customViewCallback != null) {
                customViewCallback.onCustomViewHidden();
            }
            this.$customViewCallback.element = (WebChromeClient.CustomViewCallback) null;
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type android.widget.FrameLayout");
    }
}
