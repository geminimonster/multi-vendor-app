package com.iqonic.store.activity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import com.iqonic.store.adapter.ProductImageAdapter;
import com.iqonic.store.models.StoreProductModel;
import kotlin.Metadata;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0017\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\b\n\u0000*\u0001\u0000\b\n\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\u0016¨\u0006\u0006"}, d2 = {"com/iqonic/store/activity/ProductDetailActivity1$viewVariableImage$2", "Lcom/iqonic/store/adapter/ProductImageAdapter$OnClickListener;", "onClick", "", "position", "", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: ProductDetailActivity1.kt */
public final class ProductDetailActivity1$viewVariableImage$2 implements ProductImageAdapter.OnClickListener {
    final /* synthetic */ StoreProductModel $its;
    final /* synthetic */ ProductDetailActivity1 this$0;

    ProductDetailActivity1$viewVariableImage$2(ProductDetailActivity1 productDetailActivity1, StoreProductModel storeProductModel) {
        this.this$0 = productDetailActivity1;
        this.$its = storeProductModel;
    }

    public void onClick(int i) {
        ProductDetailActivity1 productDetailActivity1 = this.this$0;
        Bundle bundle = null;
        Intent intent = new Intent(productDetailActivity1, ZoomImageActivity.class);
        new ProductDetailActivity1$viewVariableImage$2$onClick$1(this).invoke(intent);
        if (Build.VERSION.SDK_INT >= 16) {
            productDetailActivity1.startActivityForResult(intent, -1, bundle);
        } else {
            productDetailActivity1.startActivityForResult(intent, -1);
        }
    }
}
