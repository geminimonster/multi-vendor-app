package com.iqonic.store.activity;

import android.widget.RelativeLayout;
import com.iqonic.store.R;
import com.iqonic.store.models.Order;
import com.iqonic.store.utils.Constants;
import com.iqonic.store.utils.extensions.AppExtensionsKt;
import com.iqonic.store.utils.extensions.ViewExtensionsKt;
import java.util.ArrayList;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0016\u0010\u0002\u001a\u0012\u0012\u0004\u0012\u00020\u00040\u0003j\b\u0012\u0004\u0012\u00020\u0004`\u0005H\n¢\u0006\u0002\b\u0006"}, d2 = {"<anonymous>", "", "it", "Ljava/util/ArrayList;", "Lcom/iqonic/store/models/Order;", "Lkotlin/collections/ArrayList;", "invoke"}, k = 3, mv = {1, 1, 16})
/* compiled from: OrderActivity.kt */
final class OrderActivity$getOrderList$1 extends Lambda implements Function1<ArrayList<Order>, Unit> {
    final /* synthetic */ OrderActivity this$0;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    OrderActivity$getOrderList$1(OrderActivity orderActivity) {
        super(1);
        this.this$0 = orderActivity;
    }

    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        invoke((ArrayList<Order>) (ArrayList) obj);
        return Unit.INSTANCE;
    }

    public final void invoke(ArrayList<Order> arrayList) {
        Intrinsics.checkParameterIsNotNull(arrayList, "it");
        if (arrayList.size() == 0) {
            RelativeLayout relativeLayout = (RelativeLayout) this.this$0._$_findCachedViewById(R.id.rlNoData);
            Intrinsics.checkExpressionValueIsNotNull(relativeLayout, "rlNoData");
            ViewExtensionsKt.show(relativeLayout);
            return;
        }
        RelativeLayout relativeLayout2 = (RelativeLayout) this.this$0._$_findCachedViewById(R.id.rlNoData);
        Intrinsics.checkExpressionValueIsNotNull(relativeLayout2, "rlNoData");
        ViewExtensionsKt.hide(relativeLayout2);
        this.this$0.mOrderAdapter.clearItems();
        this.this$0.mOrderAdapter.addItems(arrayList);
        AppExtensionsKt.getSharedPrefInstance().setValue(Constants.SharedPref.KEY_ORDER_COUNT, Integer.valueOf(arrayList.size()));
        AppExtensionsKt.sendOrderCountChangeBroadcast(this.this$0);
    }
}
