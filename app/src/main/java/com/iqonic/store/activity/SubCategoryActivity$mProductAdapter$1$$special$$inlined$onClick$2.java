package com.iqonic.store.activity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import com.iqonic.store.models.StoreProductModel;
import com.iqonic.store.utils.extensions.AppExtensionsKt;
import kotlin.Metadata;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\b\u0003\n\u0002\b\u0004\u0010\u0000\u001a\u00020\u0001\"\b\b\u0000\u0010\u0002*\u00020\u00032\u000e\u0010\u0004\u001a\n \u0005*\u0004\u0018\u00010\u00030\u0003H\n¢\u0006\u0002\b\u0006¨\u0006\u0007"}, d2 = {"<anonymous>", "", "T", "Landroid/view/View;", "it", "kotlin.jvm.PlatformType", "onClick", "com/iqonic/store/utils/extensions/ExtensionsKt$onClick$1"}, k = 3, mv = {1, 1, 16})
/* compiled from: Extensions.kt */
public final class SubCategoryActivity$mProductAdapter$1$$special$$inlined$onClick$2 implements View.OnClickListener {
    final /* synthetic */ StoreProductModel $model$inlined;
    final /* synthetic */ View $this_onClick;
    final /* synthetic */ SubCategoryActivity$mProductAdapter$1 this$0;

    public SubCategoryActivity$mProductAdapter$1$$special$$inlined$onClick$2(View view, SubCategoryActivity$mProductAdapter$1 subCategoryActivity$mProductAdapter$1, StoreProductModel storeProductModel) {
        this.$this_onClick = view;
        this.this$0 = subCategoryActivity$mProductAdapter$1;
        this.$model$inlined = storeProductModel;
    }

    public final void onClick(View view) {
        if (AppExtensionsKt.getProductDetailConstant() == 0) {
            SubCategoryActivity subCategoryActivity = this.this$0.this$0;
            Bundle bundle = null;
            Intent intent = new Intent(subCategoryActivity, ProductDetailActivity1.class);
            new SubCategoryActivity$mProductAdapter$1$$special$$inlined$onClick$2$lambda$1(this).invoke(intent);
            if (Build.VERSION.SDK_INT >= 16) {
                subCategoryActivity.startActivityForResult(intent, -1, bundle);
            } else {
                subCategoryActivity.startActivityForResult(intent, -1);
            }
        } else {
            SubCategoryActivity subCategoryActivity2 = this.this$0.this$0;
            Bundle bundle2 = null;
            Intent intent2 = new Intent(subCategoryActivity2, ProductDetailActivity2.class);
            new SubCategoryActivity$mProductAdapter$1$$special$$inlined$onClick$2$lambda$2(this).invoke(intent2);
            if (Build.VERSION.SDK_INT >= 16) {
                subCategoryActivity2.startActivityForResult(intent2, -1, bundle2);
            } else {
                subCategoryActivity2.startActivityForResult(intent2, -1);
            }
        }
    }
}
