package com.iqonic.store.activity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import com.google.android.material.button.MaterialButton;
import com.iqonic.store.utils.extensions.AppExtensionsKt;
import com.iqonic.store.utils.extensions.ExtensionsKt$launchActivity$1;
import kotlin.Metadata;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\b\u0003\n\u0002\b\u0004\u0010\u0000\u001a\u00020\u0001\"\b\b\u0000\u0010\u0002*\u00020\u00032\u000e\u0010\u0004\u001a\n \u0005*\u0004\u0018\u00010\u00030\u0003H\n¢\u0006\u0002\b\u0006¨\u0006\u0007"}, d2 = {"<anonymous>", "", "T", "Landroid/view/View;", "it", "kotlin.jvm.PlatformType", "onClick", "com/iqonic/store/utils/extensions/ExtensionsKt$onClick$1"}, k = 3, mv = {1, 1, 16})
/* compiled from: Extensions.kt */
public final class ReviewsActivity$onCreate$$inlined$onClick$1 implements View.OnClickListener {
    final /* synthetic */ View $this_onClick;
    final /* synthetic */ ReviewsActivity this$0;

    public ReviewsActivity$onCreate$$inlined$onClick$1(View view, ReviewsActivity reviewsActivity) {
        this.$this_onClick = view;
        this.this$0 = reviewsActivity;
    }

    public final void onClick(View view) {
        MaterialButton materialButton = (MaterialButton) this.$this_onClick;
        if (AppExtensionsKt.isLoggedIn()) {
            this.this$0.createProductReview();
            return;
        }
        ReviewsActivity reviewsActivity = this.this$0;
        Bundle bundle = null;
        Intent intent = new Intent(reviewsActivity, SignInUpActivity.class);
        ExtensionsKt$launchActivity$1.INSTANCE.invoke(intent);
        if (Build.VERSION.SDK_INT >= 16) {
            reviewsActivity.startActivityForResult(intent, -1, bundle);
        } else {
            reviewsActivity.startActivityForResult(intent, -1);
        }
    }
}
