package com.iqonic.store.activity;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.iqonic.store.R;
import com.iqonic.store.models.Order;
import com.iqonic.store.utils.Constants;
import com.iqonic.store.utils.extensions.AppExtensionsKt;
import com.iqonic.store.utils.extensions.ExtensionsKt;
import com.iqonic.store.utils.extensions.NetworkExtensionKt;
import com.iqonic.store.utils.extensions.StringExtensionsKt;
import com.iqonic.store.utils.extensions.ViewExtensionsKt;
import java.text.ParseException;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function3;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;
import kotlin.math.MathKt;
import kotlin.text.StringsKt;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0007H\n¢\u0006\u0002\b\b"}, d2 = {"<anonymous>", "", "view", "Landroid/view/View;", "model", "Lcom/iqonic/store/models/Order;", "<anonymous parameter 2>", "", "invoke"}, k = 3, mv = {1, 1, 16})
/* compiled from: OrderActivity.kt */
final class OrderActivity$mOrderAdapter$1 extends Lambda implements Function3<View, Order, Integer, Unit> {
    final /* synthetic */ OrderActivity this$0;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    OrderActivity$mOrderAdapter$1(OrderActivity orderActivity) {
        super(3);
        this.this$0 = orderActivity;
    }

    public /* bridge */ /* synthetic */ Object invoke(Object obj, Object obj2, Object obj3) {
        invoke((View) obj, (Order) obj2, ((Number) obj3).intValue());
        return Unit.INSTANCE;
    }

    public final void invoke(View view, Order order, int i) {
        Intrinsics.checkParameterIsNotNull(view, "view");
        Intrinsics.checkParameterIsNotNull(order, "model");
        boolean z = true;
        if (!order.getLine_items().isEmpty()) {
            if (order.getLine_items().get(0).getProduct_images().get(0).getSrc().length() > 0) {
                ImageView imageView = (ImageView) view.findViewById(R.id.ivProduct);
                Intrinsics.checkExpressionValueIsNotNull(imageView, "view.ivProduct");
                NetworkExtensionKt.loadImageFromUrl$default(imageView, order.getLine_items().get(0).getProduct_images().get(0).getSrc(), 0, 0, 6, (Object) null);
            }
            if (order.getLine_items().size() > 1) {
                TextView textView = (TextView) view.findViewById(R.id.tvProductName);
                Intrinsics.checkExpressionValueIsNotNull(textView, "view.tvProductName");
                textView.setText(order.getLine_items().get(0).getName() + " + " + (order.getLine_items().size() - 1) + " " + this.this$0.getString(com.store.proshop.R.string.lbl_more_item));
            } else {
                TextView textView2 = (TextView) view.findViewById(R.id.tvProductName);
                Intrinsics.checkExpressionValueIsNotNull(textView2, "view.tvProductName");
                textView2.setText(order.getLine_items().get(0).getName());
            }
        } else {
            TextView textView3 = (TextView) view.findViewById(R.id.tvProductName);
            Intrinsics.checkExpressionValueIsNotNull(textView3, "view.tvProductName");
            textView3.setText(this.this$0.getString(com.store.proshop.R.string.lbl_order_title) + order.getId());
        }
        TextView textView4 = (TextView) view.findViewById(R.id.tvPrice);
        Intrinsics.checkExpressionValueIsNotNull(textView4, "view.tvPrice");
        textView4.setText(StringExtensionsKt.currencyFormat(String.valueOf(MathKt.roundToInt(Float.parseFloat(order.getTotal()) - Float.parseFloat(order.getDiscount_total()))), order.getCurrency()));
        try {
            if (order.getDate_paid() != null) {
                CharSequence transaction_id = order.getTransaction_id();
                if (transaction_id != null) {
                    if (!StringsKt.isBlank(transaction_id)) {
                        z = false;
                    }
                }
                if (z) {
                    TextView textView5 = (TextView) view.findViewById(R.id.tvInfo);
                    Intrinsics.checkExpressionValueIsNotNull(textView5, "view.tvInfo");
                    textView5.setText(this.this$0.getString(com.store.proshop.R.string.lbl_transaction_via) + " " + order.getPayment_method() + " (" + order.getTransaction_id() + "). " + this.this$0.getString(com.store.proshop.R.string.lbl_paid_on) + " " + AppExtensionsKt.convertOrderDataToLocalDate(order.getDate_paid().getDate()));
                } else {
                    TextView textView6 = (TextView) view.findViewById(R.id.tvInfo);
                    Intrinsics.checkExpressionValueIsNotNull(textView6, "view.tvInfo");
                    textView6.setText(this.this$0.getString(com.store.proshop.R.string.lbl_transaction_via) + " " + order.getPayment_method() + ". " + this.this$0.getString(com.store.proshop.R.string.lbl_paid_on) + " " + AppExtensionsKt.convertOrderDataToLocalDate(order.getDate_paid().getDate()));
                }
            } else {
                TextView textView7 = (TextView) view.findViewById(R.id.tvInfo);
                Intrinsics.checkExpressionValueIsNotNull(textView7, "view.tvInfo");
                textView7.setText(this.this$0.getString(com.store.proshop.R.string.lbl_transaction_via) + " " + order.getPayment_method());
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (Intrinsics.areEqual((Object) order.getStatus(), (Object) Constants.OrderStatus.COMPLETED)) {
            TextView textView8 = (TextView) view.findViewById(R.id.tvProductDeliveryDate);
            Intrinsics.checkExpressionValueIsNotNull(textView8, "view.tvProductDeliveryDate");
            textView8.setText(AppExtensionsKt.convertOrderDataToLocalDate(order.getDate_modified().getDate()));
        } else {
            LinearLayout linearLayout = (LinearLayout) view.findViewById(R.id.llDeliveryDate);
            Intrinsics.checkExpressionValueIsNotNull(linearLayout, "view.llDeliveryDate");
            ViewExtensionsKt.hide(linearLayout);
        }
        TextView textView9 = (TextView) view.findViewById(R.id.tvStatus);
        Intrinsics.checkExpressionValueIsNotNull(textView9, "view.tvStatus");
        textView9.setText(order.getStatus());
        RelativeLayout relativeLayout = (RelativeLayout) view.findViewById(R.id.rlMainOrder);
        relativeLayout.setOnClickListener(new OrderActivity$mOrderAdapter$1$$special$$inlined$onClick$1(relativeLayout, this, order));
        TextView textView10 = (TextView) view.findViewById(R.id.tvProductName);
        Intrinsics.checkExpressionValueIsNotNull(textView10, "view.tvProductName");
        ExtensionsKt.changeTextPrimaryColor(textView10);
        TextView textView11 = (TextView) view.findViewById(R.id.tvInfo);
        Intrinsics.checkExpressionValueIsNotNull(textView11, "view.tvInfo");
        ExtensionsKt.changeTextSecondaryColor(textView11);
        TextView textView12 = (TextView) view.findViewById(R.id.lblDelivery);
        Intrinsics.checkExpressionValueIsNotNull(textView12, "view.lblDelivery");
        ExtensionsKt.changeTextSecondaryColor(textView12);
        TextView textView13 = (TextView) view.findViewById(R.id.tvProductDeliveryDate);
        Intrinsics.checkExpressionValueIsNotNull(textView13, "view.tvProductDeliveryDate");
        ExtensionsKt.changeTextPrimaryColor(textView13);
        TextView textView14 = (TextView) view.findViewById(R.id.tvPrice);
        Intrinsics.checkExpressionValueIsNotNull(textView14, "view.tvPrice");
        ExtensionsKt.changePrimaryColor(textView14);
        TextView textView15 = (TextView) view.findViewById(R.id.tvStatus);
        Intrinsics.checkExpressionValueIsNotNull(textView15, "view.tvStatus");
        ExtensionsKt.changeTextPrimaryColor(textView15);
        TextView textView16 = (TextView) view.findViewById(R.id.tvStatus);
        Intrinsics.checkExpressionValueIsNotNull(textView16, "view.tvStatus");
        ExtensionsKt.changeTextPrimaryColor(textView16);
    }
}
