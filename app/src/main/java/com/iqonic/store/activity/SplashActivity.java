package com.iqonic.store.activity;

import android.os.Bundle;
import android.view.View;
import com.iqonic.store.AppBaseActivity;
import com.iqonic.store.models.DashBoardResponse;
import com.iqonic.store.utils.Constants;
import com.iqonic.store.utils.extensions.AppExtensionsKt;
import com.iqonic.store.utils.extensions.ExtensionsKt;
import com.store.proshop.R;
import java.util.HashMap;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u0012\u0010\u0005\u001a\u00020\u00062\b\u0010\u0007\u001a\u0004\u0018\u00010\bH\u0015R\u000e\u0010\u0003\u001a\u00020\u0004X.¢\u0006\u0002\n\u0000¨\u0006\t"}, d2 = {"Lcom/iqonic/store/activity/SplashActivity;", "Lcom/iqonic/store/AppBaseActivity;", "()V", "mAppData", "Lcom/iqonic/store/models/DashBoardResponse;", "onCreate", "", "savedInstanceState", "Landroid/os/Bundle;", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: SplashActivity.kt */
public final class SplashActivity extends AppBaseActivity {
    private HashMap _$_findViewCache;
    /* access modifiers changed from: private */
    public DashBoardResponse mAppData;

    public void _$_clearFindViewByIdCache() {
        HashMap hashMap = this._$_findViewCache;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    public View _$_findCachedViewById(int i) {
        if (this._$_findViewCache == null) {
            this._$_findViewCache = new HashMap();
        }
        View view = (View) this._$_findViewCache.get(Integer.valueOf(i));
        if (view != null) {
            return view;
        }
        View findViewById = findViewById(i);
        this._$_findViewCache.put(Integer.valueOf(i), findViewById);
        return findViewById;
    }

    public static final /* synthetic */ DashBoardResponse access$getMAppData$p(SplashActivity splashActivity) {
        DashBoardResponse dashBoardResponse = splashActivity.mAppData;
        if (dashBoardResponse == null) {
            Intrinsics.throwUninitializedPropertyAccessException("mAppData");
        }
        return dashBoardResponse;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_splash_new);
        getWindow().setFlags(512, 512);
        ExtensionsKt.runDelayed(1500, new SplashActivity$onCreate$1(this));
        AppExtensionsKt.getSharedPrefInstance().removeKey(Constants.SharedPref.MODE);
        AppExtensionsKt.getSharedPrefInstance().setValue(Constants.SharedPref.MODE, 1);
        AppExtensionsKt.mainContent(this, new SplashActivity$onCreate$2(this));
    }
}
