package com.iqonic.store.activity;

import androidx.appcompat.widget.SearchView;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0019\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003*\u0001\u0000\b\n\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\u0016J\u0012\u0010\u0006\u001a\u00020\u00032\b\u0010\u0007\u001a\u0004\u0018\u00010\u0005H\u0016¨\u0006\b"}, d2 = {"com/iqonic/store/activity/SearchActivity$onCreate$2", "Landroidx/appcompat/widget/SearchView$OnQueryTextListener;", "onQueryTextChange", "", "newText", "", "onQueryTextSubmit", "query", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: SearchActivity.kt */
public final class SearchActivity$onCreate$2 implements SearchView.OnQueryTextListener {
    final /* synthetic */ SearchActivity this$0;

    SearchActivity$onCreate$2(SearchActivity searchActivity) {
        this.this$0 = searchActivity;
    }

    public boolean onQueryTextSubmit(String str) {
        this.this$0.mPage = 1;
        SearchActivity searchActivity = this.this$0;
        if (str == null) {
            Intrinsics.throwNpe();
        }
        searchActivity.mSearchQuery = str;
        this.this$0.searchRequest.setText(this.this$0.mSearchQuery);
        this.this$0.searchRequest.setPage(this.this$0.mPage);
        this.this$0.loadProducts();
        return true;
    }

    public boolean onQueryTextChange(String str) {
        Intrinsics.checkParameterIsNotNull(str, "newText");
        if (str.length() == 0) {
            this.this$0.mSearchQuery = "";
            this.this$0.mPage = 1;
            this.this$0.searchRequest.setText((String) null);
            this.this$0.searchRequest.setPage(this.this$0.mPage);
        }
        return true;
    }
}
