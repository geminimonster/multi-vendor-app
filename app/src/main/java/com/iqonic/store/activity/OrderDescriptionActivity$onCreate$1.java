package com.iqonic.store.activity;

import android.widget.RelativeLayout;
import com.iqonic.store.R;
import com.iqonic.store.adapter.BaseAdapter;
import com.iqonic.store.models.OrderNotes;
import com.iqonic.store.utils.extensions.ViewExtensionsKt;
import java.util.ArrayList;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.collections.CollectionsKt;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0016\u0010\u0002\u001a\u0012\u0012\u0004\u0012\u00020\u00040\u0003j\b\u0012\u0004\u0012\u00020\u0004`\u0005H\n¢\u0006\u0002\b\u0006"}, d2 = {"<anonymous>", "", "it", "Ljava/util/ArrayList;", "Lcom/iqonic/store/models/OrderNotes;", "Lkotlin/collections/ArrayList;", "invoke"}, k = 3, mv = {1, 1, 16})
/* compiled from: OrderDescriptionActivity.kt */
final class OrderDescriptionActivity$onCreate$1 extends Lambda implements Function1<ArrayList<OrderNotes>, Unit> {
    final /* synthetic */ BaseAdapter $mOrderNotesAdapter;
    final /* synthetic */ OrderDescriptionActivity this$0;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    OrderDescriptionActivity$onCreate$1(OrderDescriptionActivity orderDescriptionActivity, BaseAdapter baseAdapter) {
        super(1);
        this.this$0 = orderDescriptionActivity;
        this.$mOrderNotesAdapter = baseAdapter;
    }

    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        invoke((ArrayList<OrderNotes>) (ArrayList) obj);
        return Unit.INSTANCE;
    }

    public final void invoke(ArrayList<OrderNotes> arrayList) {
        Intrinsics.checkParameterIsNotNull(arrayList, "it");
        if (arrayList.isEmpty()) {
            RelativeLayout relativeLayout = (RelativeLayout) this.this$0._$_findCachedViewById(R.id.rlNotes);
            Intrinsics.checkExpressionValueIsNotNull(relativeLayout, "rlNotes");
            ViewExtensionsKt.hide(relativeLayout);
            return;
        }
        int i = 0;
        for (Object next : arrayList) {
            int i2 = i + 1;
            if (i < 0) {
                CollectionsKt.throwIndexOverflow();
            }
            this.this$0.mOrderNoteList.add((OrderNotes) next);
            i = i2;
        }
        this.$mOrderNotesAdapter.addItems(this.this$0.mOrderNoteList);
    }
}
