package com.iqonic.store.activity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;
import com.iqonic.store.AppBaseActivity;
import com.iqonic.store.utils.Constants;
import com.iqonic.store.utils.SharedPrefUtils;
import com.iqonic.store.utils.dotsindicator.DotsIndicator;
import com.iqonic.store.utils.extensions.AppExtensionsKt;
import com.iqonic.store.utils.extensions.ExtensionsKt;
import com.store.proshop.R;
import java.util.HashMap;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001:\u0001\bB\u0005¢\u0006\u0002\u0010\u0002J\b\u0010\u0003\u001a\u00020\u0004H\u0002J\u0012\u0010\u0005\u001a\u00020\u00042\b\u0010\u0006\u001a\u0004\u0018\u00010\u0007H\u0014¨\u0006\t"}, d2 = {"Lcom/iqonic/store/activity/WalkThroughActivity;", "Lcom/iqonic/store/AppBaseActivity;", "()V", "changeColor", "", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "WalkAdapter", "app_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: WalkThroughActivity.kt */
public final class WalkThroughActivity extends AppBaseActivity {
    private HashMap _$_findViewCache;

    public void _$_clearFindViewByIdCache() {
        HashMap hashMap = this._$_findViewCache;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    public View _$_findCachedViewById(int i) {
        if (this._$_findViewCache == null) {
            this._$_findViewCache = new HashMap();
        }
        View view = (View) this._$_findViewCache.get(Integer.valueOf(i));
        if (view != null) {
            return view;
        }
        View findViewById = findViewById(i);
        this._$_findViewCache.put(Integer.valueOf(i), findViewById);
        return findViewById;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_walk_through);
        if (SharedPrefUtils.getBooleanValue$default(AppExtensionsKt.getSharedPrefInstance(), Constants.SharedPref.SHOW_SWIPE, false, 2, (Object) null)) {
            Bundle bundle2 = null;
            Intent intent = new Intent(this, DashBoardActivity.class);
            new WalkThroughActivity$onCreate$1(this).invoke(intent);
            if (Build.VERSION.SDK_INT >= 16) {
                startActivityForResult(intent, -1, bundle2);
            } else {
                startActivityForResult(intent, -1);
            }
            finish();
        }
        ExtensionsKt.makeTransaprant(this);
        WalkAdapter walkAdapter = new WalkAdapter();
        ViewPager viewPager = (ViewPager) _$_findCachedViewById(com.iqonic.store.R.id.theme3Viewpager);
        Intrinsics.checkExpressionValueIsNotNull(viewPager, "theme3Viewpager");
        viewPager.setAdapter(walkAdapter);
        ((DotsIndicator) _$_findCachedViewById(com.iqonic.store.R.id.dots)).attachViewPager((ViewPager) _$_findCachedViewById(com.iqonic.store.R.id.theme3Viewpager));
        ((DotsIndicator) _$_findCachedViewById(com.iqonic.store.R.id.dots)).setDotDrawable(R.drawable.bg_circle_primary, R.drawable.black_dot);
        TextView textView = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.btnStatShopping);
        textView.setOnClickListener(new WalkThroughActivity$onCreate$$inlined$onClick$1(textView, this));
        changeColor();
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000F\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0011\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u0015\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0004\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J \u0010\n\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u0011H\u0016J\b\u0010\u0012\u001a\u00020\u000fH\u0016J\u0018\u0010\u0013\u001a\u00020\u00112\u0006\u0010\f\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000fH\u0016J\u0018\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u0010\u001a\u00020\u0011H\u0016R\u0016\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X\u000e¢\u0006\u0004\n\u0002\u0010\u0006R\u000e\u0010\u0007\u001a\u00020\bX\u0004¢\u0006\u0002\n\u0000R\u0016\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X\u000e¢\u0006\u0004\n\u0002\u0010\u0006¨\u0006\u0018"}, d2 = {"Lcom/iqonic/store/activity/WalkThroughActivity$WalkAdapter;", "Landroidx/viewpager/widget/PagerAdapter;", "(Lcom/iqonic/store/activity/WalkThroughActivity;)V", "mHeading", "", "", "[Ljava/lang/String;", "mImg", "", "mSubHeading", "destroyItem", "", "container", "Landroid/view/ViewGroup;", "position", "", "object", "", "getCount", "instantiateItem", "isViewFromObject", "", "view", "Landroid/view/View;", "app_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: WalkThroughActivity.kt */
    public final class WalkAdapter extends PagerAdapter {
        private String[] mHeading;
        private final int[] mImg = {R.drawable.ic_walk_1, R.drawable.ic_trends, R.drawable.ic_walk_3};
        private String[] mSubHeading;

        public WalkAdapter() {
            this.mHeading = new String[]{WalkThroughActivity.this.getString(R.string.lbl_signin_up), WalkThroughActivity.this.getString(R.string.lbl_product_quality), WalkThroughActivity.this.getString(R.string.lbl_make_delicious_dishes)};
            this.mSubHeading = new String[]{WalkThroughActivity.this.getString(R.string.lbl_dummy_text1), WalkThroughActivity.this.getString(R.string.lbl_dummy_text2), WalkThroughActivity.this.getString(R.string.lbl_dummy_text3)};
        }

        public void destroyItem(ViewGroup viewGroup, int i, Object obj) {
            Intrinsics.checkParameterIsNotNull(viewGroup, "container");
            Intrinsics.checkParameterIsNotNull(obj, "object");
            viewGroup.removeView((View) obj);
        }

        public Object instantiateItem(ViewGroup viewGroup, int i) {
            Intrinsics.checkParameterIsNotNull(viewGroup, "container");
            View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.theme3_walk, viewGroup, false);
            Intrinsics.checkExpressionValueIsNotNull(inflate, "view");
            ((ImageView) inflate.findViewById(com.iqonic.store.R.id.imgWalk)).setImageResource(this.mImg[i]);
            TextView textView = (TextView) inflate.findViewById(com.iqonic.store.R.id.tvHeading);
            Intrinsics.checkExpressionValueIsNotNull(textView, "view.tvHeading");
            textView.setText(this.mHeading[i]);
            TextView textView2 = (TextView) inflate.findViewById(com.iqonic.store.R.id.tvHeading);
            Intrinsics.checkExpressionValueIsNotNull(textView2, "view.tvHeading");
            ExtensionsKt.changeAccentColor(textView2);
            TextView textView3 = (TextView) inflate.findViewById(com.iqonic.store.R.id.tvSubHeading);
            Intrinsics.checkExpressionValueIsNotNull(textView3, "view.tvSubHeading");
            textView3.setText(this.mSubHeading[i]);
            TextView textView4 = (TextView) inflate.findViewById(com.iqonic.store.R.id.tvSubHeading);
            Intrinsics.checkExpressionValueIsNotNull(textView4, "view.tvSubHeading");
            ExtensionsKt.changeTextSecondaryColor(textView4);
            viewGroup.addView(inflate);
            return inflate;
        }

        public int getCount() {
            return this.mImg.length;
        }

        public boolean isViewFromObject(View view, Object obj) {
            Intrinsics.checkParameterIsNotNull(view, "view");
            Intrinsics.checkParameterIsNotNull(obj, "object");
            return view == ((View) obj);
        }
    }

    private final void changeColor() {
        TextView textView = (TextView) _$_findCachedViewById(com.iqonic.store.R.id.btnStatShopping);
        Intrinsics.checkExpressionValueIsNotNull(textView, "btnStatShopping");
        ExtensionsKt.changeBackgroundTint(textView, AppExtensionsKt.getPrimaryColor());
    }
}
