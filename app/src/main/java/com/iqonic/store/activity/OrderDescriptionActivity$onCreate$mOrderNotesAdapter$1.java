package com.iqonic.store.activity;

import android.graphics.Color;
import android.view.View;
import android.widget.TextView;
import com.iqonic.store.R;
import com.iqonic.store.models.OrderNotes;
import com.iqonic.store.utils.CircleView;
import com.iqonic.store.utils.LineView;
import com.iqonic.store.utils.extensions.AppExtensionsKt;
import com.iqonic.store.utils.extensions.ExtensionsKt;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function3;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;
import kotlin.text.StringsKt;
import org.json.JSONException;
import org.json.JSONObject;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0007H\n¢\u0006\u0002\b\b"}, d2 = {"<anonymous>", "", "view", "Landroid/view/View;", "model", "Lcom/iqonic/store/models/OrderNotes;", "<anonymous parameter 2>", "", "invoke"}, k = 3, mv = {1, 1, 16})
/* compiled from: OrderDescriptionActivity.kt */
final class OrderDescriptionActivity$onCreate$mOrderNotesAdapter$1 extends Lambda implements Function3<View, OrderNotes, Integer, Unit> {
    final /* synthetic */ OrderDescriptionActivity this$0;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    OrderDescriptionActivity$onCreate$mOrderNotesAdapter$1(OrderDescriptionActivity orderDescriptionActivity) {
        super(3);
        this.this$0 = orderDescriptionActivity;
    }

    public /* bridge */ /* synthetic */ Object invoke(Object obj, Object obj2, Object obj3) {
        invoke((View) obj, (OrderNotes) obj2, ((Number) obj3).intValue());
        return Unit.INSTANCE;
    }

    public final void invoke(View view, OrderNotes orderNotes, int i) {
        View view2 = view;
        Intrinsics.checkParameterIsNotNull(view2, "view");
        Intrinsics.checkParameterIsNotNull(orderNotes, "model");
        try {
            String replace$default = StringsKt.replace$default(StringsKt.replace$default(orderNotes.getNote(), "“", "\"", false, 4, (Object) null), "”", "\"", false, 4, (Object) null);
            try {
                if (this.this$0.isJSONValid(replace$default)) {
                    TextView textView = (TextView) view2.findViewById(R.id.tvText1);
                    Intrinsics.checkExpressionValueIsNotNull(textView, "view.tvText1");
                    textView.setText(new JSONObject(replace$default).getString("message"));
                    TextView textView2 = (TextView) view2.findViewById(R.id.tvText2);
                    Intrinsics.checkExpressionValueIsNotNull(textView2, "view.tvText2");
                    textView2.setText(new JSONObject(replace$default).getString("status"));
                } else {
                    TextView textView3 = (TextView) view2.findViewById(R.id.tvText1);
                    Intrinsics.checkExpressionValueIsNotNull(textView3, "view.tvText1");
                    textView3.setText(replace$default);
                    TextView textView4 = (TextView) view2.findViewById(R.id.tvText2);
                    Intrinsics.checkExpressionValueIsNotNull(textView4, "view.tvText2");
                    textView4.setText("By admin");
                }
            } catch (JSONException e) {
                e = e;
                e.printStackTrace();
                TextView textView5 = (TextView) view2.findViewById(R.id.tvText1);
                Intrinsics.checkExpressionValueIsNotNull(textView5, "view.tvText1");
                ExtensionsKt.changeTextSecondaryColor(textView5);
                TextView textView6 = (TextView) view2.findViewById(R.id.tvText2);
                Intrinsics.checkExpressionValueIsNotNull(textView6, "view.tvText2");
                ExtensionsKt.changeTextPrimaryColor(textView6);
                TextView textView7 = (TextView) view2.findViewById(R.id.tvDate);
                Intrinsics.checkExpressionValueIsNotNull(textView7, "view.tvDate");
                ExtensionsKt.changeTextSecondaryColor(textView7);
                TextView textView8 = (TextView) view2.findViewById(R.id.tvDate);
                Intrinsics.checkExpressionValueIsNotNull(textView8, "view.tvDate");
                textView8.setText(AppExtensionsKt.convertToLocalDate(orderNotes.getDate_created()));
                ((LineView) view2.findViewById(R.id.ivLine)).setLineColor(Color.parseColor(AppExtensionsKt.getPrimaryColor()));
                ((CircleView) view2.findViewById(R.id.ivCircle)).setCircleColor(Color.parseColor(AppExtensionsKt.getPrimaryColor()));
            }
        } catch (JSONException e2) {
            e = e2;
            e.printStackTrace();
            TextView textView52 = (TextView) view2.findViewById(R.id.tvText1);
            Intrinsics.checkExpressionValueIsNotNull(textView52, "view.tvText1");
            ExtensionsKt.changeTextSecondaryColor(textView52);
            TextView textView62 = (TextView) view2.findViewById(R.id.tvText2);
            Intrinsics.checkExpressionValueIsNotNull(textView62, "view.tvText2");
            ExtensionsKt.changeTextPrimaryColor(textView62);
            TextView textView72 = (TextView) view2.findViewById(R.id.tvDate);
            Intrinsics.checkExpressionValueIsNotNull(textView72, "view.tvDate");
            ExtensionsKt.changeTextSecondaryColor(textView72);
            TextView textView82 = (TextView) view2.findViewById(R.id.tvDate);
            Intrinsics.checkExpressionValueIsNotNull(textView82, "view.tvDate");
            textView82.setText(AppExtensionsKt.convertToLocalDate(orderNotes.getDate_created()));
            ((LineView) view2.findViewById(R.id.ivLine)).setLineColor(Color.parseColor(AppExtensionsKt.getPrimaryColor()));
            ((CircleView) view2.findViewById(R.id.ivCircle)).setCircleColor(Color.parseColor(AppExtensionsKt.getPrimaryColor()));
        }
        TextView textView522 = (TextView) view2.findViewById(R.id.tvText1);
        Intrinsics.checkExpressionValueIsNotNull(textView522, "view.tvText1");
        ExtensionsKt.changeTextSecondaryColor(textView522);
        TextView textView622 = (TextView) view2.findViewById(R.id.tvText2);
        Intrinsics.checkExpressionValueIsNotNull(textView622, "view.tvText2");
        ExtensionsKt.changeTextPrimaryColor(textView622);
        TextView textView722 = (TextView) view2.findViewById(R.id.tvDate);
        Intrinsics.checkExpressionValueIsNotNull(textView722, "view.tvDate");
        ExtensionsKt.changeTextSecondaryColor(textView722);
        TextView textView822 = (TextView) view2.findViewById(R.id.tvDate);
        Intrinsics.checkExpressionValueIsNotNull(textView822, "view.tvDate");
        textView822.setText(AppExtensionsKt.convertToLocalDate(orderNotes.getDate_created()));
        ((LineView) view2.findViewById(R.id.ivLine)).setLineColor(Color.parseColor(AppExtensionsKt.getPrimaryColor()));
        ((CircleView) view2.findViewById(R.id.ivCircle)).setCircleColor(Color.parseColor(AppExtensionsKt.getPrimaryColor()));
    }
}
