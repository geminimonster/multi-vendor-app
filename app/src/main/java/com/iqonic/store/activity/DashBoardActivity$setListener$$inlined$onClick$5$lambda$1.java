package com.iqonic.store.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import com.iqonic.store.utils.extensions.AppExtensionsKt;
import com.iqonic.store.utils.extensions.ExtensionsKt$launchActivityWithNewTask$1;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\n¢\u0006\u0002\b\u0006¨\u0006\u0007"}, d2 = {"<anonymous>", "", "<anonymous parameter 0>", "Landroid/content/DialogInterface;", "<anonymous parameter 1>", "", "invoke", "com/iqonic/store/activity/DashBoardActivity$setListener$5$dialog$1"}, k = 3, mv = {1, 1, 16})
/* compiled from: DashBoardActivity.kt */
final class DashBoardActivity$setListener$$inlined$onClick$5$lambda$1 extends Lambda implements Function2<DialogInterface, Integer, Unit> {
    final /* synthetic */ DashBoardActivity$setListener$$inlined$onClick$5 this$0;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    DashBoardActivity$setListener$$inlined$onClick$5$lambda$1(DashBoardActivity$setListener$$inlined$onClick$5 dashBoardActivity$setListener$$inlined$onClick$5) {
        super(2);
        this.this$0 = dashBoardActivity$setListener$$inlined$onClick$5;
    }

    public /* bridge */ /* synthetic */ Object invoke(Object obj, Object obj2) {
        invoke((DialogInterface) obj, ((Number) obj2).intValue());
        return Unit.INSTANCE;
    }

    public final void invoke(DialogInterface dialogInterface, int i) {
        Intrinsics.checkParameterIsNotNull(dialogInterface, "<anonymous parameter 0>");
        AppExtensionsKt.clearLoginPref();
        DashBoardActivity dashBoardActivity = this.this$0.this$0;
        Bundle bundle = null;
        Intent intent = new Intent(dashBoardActivity, DashBoardActivity.class);
        ExtensionsKt$launchActivityWithNewTask$1.INSTANCE.invoke(intent);
        if (Build.VERSION.SDK_INT >= 16) {
            dashBoardActivity.startActivityForResult(intent, -1, bundle);
        } else {
            dashBoardActivity.startActivityForResult(intent, -1);
        }
    }
}
