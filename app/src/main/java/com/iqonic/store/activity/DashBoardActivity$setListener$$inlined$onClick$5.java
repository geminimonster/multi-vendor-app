package com.iqonic.store.activity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import com.iqonic.store.utils.extensions.AppExtensionsKt;
import com.iqonic.store.utils.extensions.ExtensionsKt$launchActivity$1;
import com.store.proshop.R;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\b\u0003\n\u0002\b\u0004\u0010\u0000\u001a\u00020\u0001\"\b\b\u0000\u0010\u0002*\u00020\u00032\u000e\u0010\u0004\u001a\n \u0005*\u0004\u0018\u00010\u00030\u0003H\n¢\u0006\u0002\b\u0006¨\u0006\u0007"}, d2 = {"<anonymous>", "", "T", "Landroid/view/View;", "it", "kotlin.jvm.PlatformType", "onClick", "com/iqonic/store/utils/extensions/ExtensionsKt$onClick$1"}, k = 3, mv = {1, 1, 16})
/* compiled from: Extensions.kt */
public final class DashBoardActivity$setListener$$inlined$onClick$5 implements View.OnClickListener {
    final /* synthetic */ View $this_onClick;
    final /* synthetic */ DashBoardActivity this$0;

    public DashBoardActivity$setListener$$inlined$onClick$5(View view, DashBoardActivity dashBoardActivity) {
        this.$this_onClick = view;
        this.this$0 = dashBoardActivity;
    }

    public final void onClick(View view) {
        TextView textView = (TextView) this.$this_onClick;
        if (AppExtensionsKt.isLoggedIn()) {
            DashBoardActivity dashBoardActivity = this.this$0;
            String string = dashBoardActivity.getString(R.string.lbl_logout_confirmation);
            Intrinsics.checkExpressionValueIsNotNull(string, "getString(R.string.lbl_logout_confirmation)");
            AppExtensionsKt.getAlertDialog$default(dashBoardActivity, string, (String) null, (String) null, (String) null, new DashBoardActivity$setListener$$inlined$onClick$5$lambda$1(this), DashBoardActivity$setListener$5$dialog$2.INSTANCE, 14, (Object) null).show();
            this.this$0.closeDrawer();
            return;
        }
        DashBoardActivity dashBoardActivity2 = this.this$0;
        Bundle bundle = null;
        Intent intent = new Intent(dashBoardActivity2, SignInUpActivity.class);
        ExtensionsKt$launchActivity$1.INSTANCE.invoke(intent);
        if (Build.VERSION.SDK_INT >= 16) {
            dashBoardActivity2.startActivityForResult(intent, 211, bundle);
        } else {
            dashBoardActivity2.startActivityForResult(intent, 211);
        }
    }
}
