package com.razorpay;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.webkit.WebView;
import com.facebook.internal.ServerProtocol;
import org.json.JSONObject;

public class AdvertisingIdUtil {
    /* access modifiers changed from: private */
    public static String a_$P$ = "permission disabled";

    interface d__1_ {
        void R$$r_(String str);
    }

    static void R$$r_(Context context, d__1_ d__1_2) {
        new G__G_(context, d__1_2).execute(new Void[0]);
    }

    static class G__G_ extends AsyncTask<Void, Void, String> {
        private Context G__G_;
        private d__1_ R$$r_;

        /* access modifiers changed from: protected */
        public final /* synthetic */ Object doInBackground(Object[] objArr) {
            return G__G_();
        }

        /* access modifiers changed from: protected */
        public final /* synthetic */ void onPostExecute(Object obj) {
            String str = (String) obj;
            super.onPostExecute(str);
            this.R$$r_.R$$r_(str);
        }

        G__G_(Context context, d__1_ d__1_) {
            this.G__G_ = context;
            this.R$$r_ = d__1_;
        }

        private String G__G_() {
            String message;
            AdvertisingIdUtil$R$$r_ advertisingIdUtil$R$$r_ = new AdvertisingIdUtil$R$$r_((byte) 0);
            Intent intent = new Intent("com.google.android.gms.ads.identifier.service.START");
            intent.setPackage("com.google.android.gms");
            if (!this.G__G_.bindService(intent, advertisingIdUtil$R$$r_, 1)) {
                return AdvertisingIdUtil.a_$P$;
            }
            try {
                message = new AdvertisingIdUtil$a_$P$(advertisingIdUtil$R$$r_.Q_$2$()).d__1_();
            } catch (Exception e) {
                message = e.getMessage();
            } catch (Throwable th) {
                this.G__G_.unbindService(advertisingIdUtil$R$$r_);
                throw th;
            }
            this.G__G_.unbindService(advertisingIdUtil$R$$r_);
            return message;
        }
    }

    /* renamed from: com.razorpay.AdvertisingIdUtil$4  reason: invalid class name */
    static /* synthetic */ class AnonymousClass4 implements t_$J_ {
        private J$$A_ D$_X_;
        private boolean E$_j$;
        private boolean G__G_;
        private WebView Q_$2$;
        private String R$$r_;
        private Context a_$P$;
        private I$_n_ d__1_;
        private boolean r$_Y_;

        AnonymousClass4() {
        }

        AnonymousClass4(Activity activity, WebView webView) {
            this.G__G_ = false;
            this.r$_Y_ = false;
            this.E$_j$ = false;
            this.a_$P$ = this.a_$P$;
            this.Q_$2$ = webView;
            if (I$_n_.d__1_ == null) {
                I$_n_.d__1_ = new I$_n_();
            }
            I$_n_ i$_n_ = I$_n_.d__1_;
            this.d__1_ = i$_n_;
            i$_n_.a_$P$.add(this);
            J$$A_ j$$a_ = new J$$A_(activity);
            this.D$_X_ = j$$a_;
            r_$Z$.a_$P$(L__R$.L__R$().l__d$(), new Callback() {
                public final void run(C__D$ c__d$) {
                    if (c__d$.d__1_() != null) {
                        try {
                            String versionFromJsonString = BaseUtils.getVersionFromJsonString(c__d$.d__1_(), J$$A_.G__G_);
                            if (!BaseUtils.getLocalVersion(J$$A_.this.Q_$2$, J$$A_.G__G_).equals(versionFromJsonString)) {
                                r_$Z$.a_$P$(L__R$.L__R$().J$_0_(), new Callback(versionFromJsonString) {
                                    public final void run(C__D$ c__d$) {
                                        String decryptFile;
                                        if (c__d$.d__1_() != null && (decryptFile = BaseUtils.decryptFile(c__d$.d__1_())) != null) {
                                            if (BaseUtils.storeFileInInternal(J$$A_.this.Q_$2$, BaseUtils.getVersionedAssetName(r3, L__R$.L__R$().g__v_()), c__d$.d__1_())) {
                                                String unused = J$$A_.this.d__1_ = decryptFile;
                                                BaseUtils.updateLocalVersion(J$$A_.this.Q_$2$, J$$A_.G__G_, r3);
                                            }
                                        }
                                    }
                                });
                            }
                        } catch (Exception e) {
                            AnalyticsUtil.reportError(e, "error", "Could not extract version from server json");
                        }
                    }
                }
            });
        }

        public final void a_$P$() {
            if (!this.E$_j$) {
                try {
                    JSONObject f$_G$ = L__R$.L__R$().f$_G$();
                    f$_G$.put("merchant_key", (Object) null);
                    f$_G$.put("otp_permission", this.G__G_);
                    JSONObject jSONObject = new JSONObject();
                    jSONObject.put("type", L__R$.d__1_);
                    jSONObject.put("version_code", L__R$.G__G_);
                    f$_G$.put(ServerProtocol.DIALOG_PARAM_SDK_VERSION, jSONObject);
                    StringBuilder sb = new StringBuilder("window.__rzp_options = ");
                    sb.append(f$_G$.toString());
                    String obj = sb.toString();
                    this.Q_$2$.loadUrl(String.format("javascript: %s", new Object[]{obj}));
                } catch (Exception unused) {
                }
                String d__1_2 = this.D$_X_.d__1_();
                this.Q_$2$.loadUrl(String.format("javascript: %s", new Object[]{d__1_2}));
                String str = this.R$$r_;
                if (str != null) {
                    String format = String.format("Magic.elfBridge.setSms(%s)", new Object[]{str});
                    this.Q_$2$.loadUrl(String.format("javascript: %s", new Object[]{format}));
                    this.R$$r_ = null;
                }
                this.E$_j$ = true;
            }
        }

        public final void G__G_() {
            this.E$_j$ = false;
        }

        public final void Q_$2$() {
            try {
                this.d__1_.a_$P$.remove(this);
            } catch (Exception unused) {
            }
            this.d__1_.G__G_((Activity) this.a_$P$);
        }

        public final void postSms(String str, String str2) {
            if (this.r$_Y_) {
                try {
                    JSONObject jSONObject = new JSONObject();
                    jSONObject.put("sender", str);
                    jSONObject.put("message", str2);
                    this.R$$r_ = jSONObject.toString();
                    String format = String.format("Magic.elfBridge.setSms(%s)", new Object[]{jSONObject.toString()});
                    this.Q_$2$.loadUrl(String.format("javascript: %s", new Object[]{format}));
                } catch (Exception unused) {
                }
            }
        }

        /* access modifiers changed from: package-private */
        public final void G__G_(boolean z) {
            this.r$_Y_ = z;
        }

        public final void setSmsPermission(boolean z) {
            this.G__G_ = z;
        }
    }
}
