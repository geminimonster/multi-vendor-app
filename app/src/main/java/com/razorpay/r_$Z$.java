package com.razorpay;

import android.os.AsyncTask;
import androidx.browser.trusted.sharing.ShareTarget;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

public final class r_$Z$ extends AsyncTask<String, Void, C__D$> {
    private String G__G_ = null;
    private Callback Q_$2$;
    private String R$$r_ = null;
    private Map<String, String> d__1_ = new HashMap();

    /* access modifiers changed from: protected */
    public final /* synthetic */ void onPostExecute(Object obj) {
        C__D$ c__d$ = (C__D$) obj;
        Callback callback = this.Q_$2$;
        if (callback != null) {
            callback.run(c__d$);
        }
    }

    private r_$Z$(Callback callback) {
        this.Q_$2$ = callback;
    }

    public static AsyncTask a_$P$(String str, Callback callback) {
        r_$Z$ r__z_ = new r_$Z$(callback);
        r__z_.G__G_ = ShareTarget.METHOD_GET;
        return r__z_.execute(new String[]{str});
    }

    static AsyncTask Q_$2$(String str, Map<String, String> map, Callback callback) {
        r_$Z$ r__z_ = new r_$Z$(callback);
        r__z_.G__G_ = ShareTarget.METHOD_GET;
        r__z_.d__1_ = map;
        return r__z_.execute(new String[]{str});
    }

    public static AsyncTask d__1_(String str, String str2, Map<String, String> map, Callback callback) {
        r_$Z$ r__z_ = new r_$Z$(callback);
        r__z_.G__G_ = ShareTarget.METHOD_POST;
        r__z_.R$$r_ = str2;
        r__z_.d__1_ = map;
        return r__z_.execute(new String[]{str});
    }

    /* access modifiers changed from: private */
    /* renamed from: Q_$2$ */
    public C__D$ doInBackground(String... strArr) {
        InputStream inputStream;
        C__D$ c__d$ = new C__D$();
        InputStream inputStream2 = null;
        try {
            HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(strArr[0]).openConnection();
            for (Map.Entry next : this.d__1_.entrySet()) {
                httpURLConnection.setRequestProperty((String) next.getKey(), (String) next.getValue());
            }
            httpURLConnection.setRequestMethod(this.G__G_);
            if (this.R$$r_ != null) {
                httpURLConnection.setDoOutput(true);
                httpURLConnection.getOutputStream().write(this.R$$r_.getBytes(StandardCharsets.UTF_8));
            }
            httpURLConnection.setConnectTimeout(15000);
            httpURLConnection.setReadTimeout(20000);
            httpURLConnection.connect();
            int responseCode = httpURLConnection.getResponseCode();
            c__d$.R$$r_(responseCode);
            if (responseCode >= 400) {
                inputStream = httpURLConnection.getErrorStream();
            } else {
                inputStream = httpURLConnection.getInputStream();
            }
            c__d$.R$$r_(httpURLConnection.getHeaderFields());
            c__d$.R$$r_(Q_$2$(inputStream));
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (Exception e) {
                    AnalyticsUtil.reportError(e, "error", e.getMessage());
                }
            }
        } catch (Exception e2) {
            e2.getLocalizedMessage();
            AnalyticsUtil.reportError(e2, "error", e2.getMessage());
            if (inputStream2 != null) {
                inputStream2.close();
            }
        } catch (Throwable th) {
            if (inputStream2 != null) {
                try {
                    inputStream2.close();
                } catch (Exception e3) {
                    AnalyticsUtil.reportError(e3, "error", e3.getMessage());
                }
            }
            throw th;
        }
        return c__d$;
    }

    private static String Q_$2$(InputStream inputStream) throws Exception {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8));
        StringBuilder sb = new StringBuilder();
        while (true) {
            String readLine = bufferedReader.readLine();
            if (readLine != null) {
                sb.append(readLine);
            } else {
                bufferedReader.close();
                return sb.toString();
            }
        }
    }
}
