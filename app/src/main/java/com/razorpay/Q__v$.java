package com.razorpay;

import android.graphics.Bitmap;
import android.webkit.WebView;
import android.webkit.WebViewClient;

final class Q__v$ extends WebViewClient {
    private l_$w$ G__G_;

    public final boolean shouldOverrideUrlLoading(WebView webView, String str) {
        return false;
    }

    public Q__v$(l_$w$ l__w_) {
        this.G__G_ = l__w_;
    }

    public final void onReceivedError(WebView webView, int i, String str, String str2) {
        this.G__G_.destroyActivity(2, str);
    }

    public final void onPageStarted(WebView webView, String str, Bitmap bitmap) {
        webView.setTag(str);
        this.G__G_.onPageStarted(2, webView, str);
    }

    public final void onPageFinished(WebView webView, String str) {
        this.G__G_.onPageFinished(2, webView, str);
    }
}
