package com.razorpay;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.RelativeLayout;

final class b_$A$ implements Animation.AnimationListener {
    private String B$$W$;
    private Context G__G_;
    private View Q_$2$;
    private int R$$r_;
    private ViewGroup a_$P$;
    private float d__1_;

    public final void onAnimationEnd(Animation animation) {
    }

    public final void onAnimationRepeat(Animation animation) {
    }

    public final void onAnimationStart(Animation animation) {
    }

    public b_$A$(Context context, ViewGroup viewGroup) {
        this(context, viewGroup, (String) null);
    }

    public b_$A$(Context context, ViewGroup viewGroup, String str) {
        int i;
        int i2;
        this.B$$W$ = str;
        this.G__G_ = context;
        this.a_$P$ = viewGroup;
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        this.d__1_ = ((float) displayMetrics.widthPixels) / displayMetrics.density;
        this.R$$r_ = (int) TypedValue.applyDimension(1, 4.0f, this.G__G_.getResources().getDisplayMetrics());
        this.Q_$2$ = new View(this.G__G_);
        this.Q_$2$.setLayoutParams(new RelativeLayout.LayoutParams(0, this.R$$r_));
        if (TextUtils.isEmpty(this.B$$W$)) {
            if (Build.VERSION.SDK_INT >= 21) {
                i2 = 16843829;
            } else {
                i2 = this.G__G_.getResources().getIdentifier("colorAccent", "attr", this.G__G_.getPackageName());
            }
            TypedValue typedValue = new TypedValue();
            if (this.G__G_.getTheme().resolveAttribute(i2, typedValue, true)) {
                i = typedValue.data;
            } else {
                i = Color.parseColor("#4aa3df");
            }
        } else {
            i = Color.parseColor(this.B$$W$);
        }
        float[] fArr = new float[3];
        Color.colorToHSV(i, fArr);
        fArr[2] = fArr[2] * 0.8f;
        int HSVToColor = Color.HSVToColor(fArr);
        GradientDrawable gradientDrawable = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[]{i, HSVToColor});
        gradientDrawable.setCornerRadius(0.0f);
        this.Q_$2$.setBackgroundDrawable(gradientDrawable);
        this.a_$P$.addView(this.Q_$2$);
    }

    /* access modifiers changed from: package-private */
    public final void R$$r_() {
        d__1_();
    }

    /* access modifiers changed from: private */
    public void Q_$2$(int i, int i2) {
        L$$C_ l$$c_ = new L$$C_(this.Q_$2$, (int) TypedValue.applyDimension(1, (float) ((int) ((this.d__1_ * ((float) i)) / 100.0f)), this.G__G_.getResources().getDisplayMetrics()));
        l$$c_.setDuration((long) i2);
        this.Q_$2$.startAnimation(l$$c_);
        l$$c_.setAnimationListener(this);
    }

    private void d__1_() {
        L$$C_ l$$c_ = new L$$C_(this.Q_$2$, (int) TypedValue.applyDimension(1, (float) ((int) this.d__1_), this.G__G_.getResources().getDisplayMetrics()));
        l$$c_.setDuration(200);
        this.Q_$2$.startAnimation(l$$c_);
        l$$c_.setAnimationListener(new Animation.AnimationListener() {
            public final void onAnimationRepeat(Animation animation) {
            }

            public final void onAnimationStart(Animation animation) {
            }

            public final void onAnimationEnd(Animation animation) {
                b_$A$.this.Q_$2$(0, 10);
            }
        });
    }

    /* access modifiers changed from: package-private */
    public final void R$$r_(int i) {
        if (i == 100) {
            d__1_();
        } else {
            Q_$2$(i, 500);
        }
    }
}
