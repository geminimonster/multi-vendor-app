package com.razorpay;

public final class BuildConfig {
    public static final String APPLICATION_ID = "com.razorpay";
    public static final String BUILD_TYPE = "release";
    public static final boolean DEBUG = false;
    public static final String FLAVOR = "";
    public static final String SDK_TYPE = "checkout";
    public static final int VERSION_CODE = 27;
    public static final String VERSION_NAME = "1.5.16";
}
