package com.razorpay;

import android.content.Context;
import java.lang.Thread;

final class B$$J$ implements Thread.UncaughtExceptionHandler {
    Context G__G_;
    Thread.UncaughtExceptionHandler a_$P$;

    B$$J$(Context context, Thread.UncaughtExceptionHandler uncaughtExceptionHandler) {
        this.a_$P$ = uncaughtExceptionHandler;
        this.G__G_ = context;
    }

    public final void uncaughtException(Thread thread, final Throwable th) {
        new Thread() {
            public final void run() {
                AnalyticsUtil.reportUncaughtException(th);
                AnalyticsUtil.saveEventsToPreferences(B$$J$.this.G__G_);
            }
        }.start();
        Thread.UncaughtExceptionHandler uncaughtExceptionHandler = this.a_$P$;
        if (uncaughtExceptionHandler != null) {
            uncaughtExceptionHandler.uncaughtException(thread, th);
        }
    }
}
