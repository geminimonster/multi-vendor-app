package com.razorpay;

import android.content.Context;
import com.bumptech.glide.load.Key;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import org.json.JSONObject;

public class r$_Y_ {
    private JSONObject B$$W$;
    private boolean D$_X_;
    private JSONObject E$_6$;
    private boolean E$_j$;
    private String G__G_;
    private String J$$A_;
    private String J$_0_;
    private int K$$z$;
    private String L__R$;
    private String O_$B_;
    private boolean O__Y_;
    private String Q_$2$;
    private boolean R$$r_;
    private boolean Y$_o$;
    private String a_$P$;
    private String b__J_;
    private String c__C_;
    private String d__1_;
    private int f$_G$;
    private String g__v_;
    private String l_$w$;
    private Boolean l__d$;
    private String r$_Y_;

    r$_Y_() {
    }

    /* access modifiers changed from: package-private */
    public final boolean Q_$2$() {
        return this.R$$r_;
    }

    public final String a_$P$() {
        return this.r$_Y_;
    }

    public final String d__1_() {
        return this.a_$P$;
    }

    public final Boolean R$$r_() {
        return Boolean.valueOf(this.E$_j$);
    }

    /* access modifiers changed from: package-private */
    public final String G__G_() {
        return this.G__G_;
    }

    /* access modifiers changed from: package-private */
    public final int E$_j$() {
        return this.K$$z$;
    }

    /* access modifiers changed from: package-private */
    public final String B$$W$() {
        return this.J$$A_;
    }

    /* access modifiers changed from: package-private */
    public final boolean b__J_() {
        return this.O__Y_;
    }

    /* access modifiers changed from: package-private */
    public final Boolean D$_X_() {
        return Boolean.valueOf(this.D$_X_);
    }

    /* access modifiers changed from: package-private */
    public final JSONObject r$_Y_() {
        return this.B$$W$;
    }

    /* access modifiers changed from: package-private */
    public final String c__C_() {
        StringBuilder sb = new StringBuilder("https://api.razorpay.com");
        sb.append(this.d__1_);
        return sb.toString();
    }

    public final String E$_6$() {
        return this.Q_$2$;
    }

    public final String l_$w$() {
        return this.l_$w$;
    }

    public final String O_$B_() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.b__J_);
        sb.append(this.O_$B_);
        return sb.toString();
    }

    public final String Y$_o$() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.b__J_);
        sb.append(this.l_$w$);
        return sb.toString();
    }

    public final JSONObject f$_G$() {
        return this.E$_6$;
    }

    /* access modifiers changed from: package-private */
    public final String g__v_() {
        return this.L__R$;
    }

    public final String l__d$() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.c__C_);
        sb.append(this.g__v_);
        return sb.toString();
    }

    public final String J$_0_() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.c__C_);
        sb.append(this.L__R$);
        return sb.toString();
    }

    public void a_$P$(JSONObject jSONObject) {
        try {
            this.K$$z$ = ((Integer) BaseUtils.getJsonValue("update_sdk_config.latest_version", jSONObject, (Object) 1)).intValue();
            this.J$$A_ = (String) BaseUtils.getJsonValue("update_sdk_config.msg", jSONObject, (Object) "");
            this.O__Y_ = ((Boolean) BaseUtils.getJsonValue("update_sdk_config.enable_alert", jSONObject, (Object) Boolean.TRUE)).booleanValue();
            this.G__G_ = (String) BaseUtils.getJsonValue("config_end_point", jSONObject, (Object) "");
            this.R$$r_ = ((Boolean) BaseUtils.getJsonValue("enable", jSONObject, (Object) "")).booleanValue();
            this.J$_0_ = (String) BaseUtils.getJsonValue("permissions.custom_message", jSONObject, (Object) "");
            this.l__d$ = Boolean.valueOf(((Boolean) BaseUtils.getJsonValue("permissions.enable_custom_message", jSONObject, (Object) Boolean.FALSE)).booleanValue());
            this.f$_G$ = ((Integer) BaseUtils.getJsonValue("permissions.max_ask_count", jSONObject, (Object) 0)).intValue();
            this.E$_j$ = ((Boolean) BaseUtils.getJsonValue("analytics.lumberjack.enable", jSONObject, (Object) Boolean.TRUE)).booleanValue();
            this.a_$P$ = (String) BaseUtils.getJsonValue("analytics.lumberjack.key", jSONObject, (Object) "");
            this.r$_Y_ = (String) BaseUtils.getJsonValue("analytics.lumberjack.end_point", jSONObject, (Object) "");
            this.Q_$2$ = (String) BaseUtils.getJsonValue("analytics.lumberjack.sdk_identifier", jSONObject, (Object) "");
            this.D$_X_ = ((Boolean) BaseUtils.getJsonValue("otpelf.enable", jSONObject, (Object) Boolean.TRUE)).booleanValue();
            this.B$$W$ = (JSONObject) BaseUtils.getJsonValue("otpelf.settings", jSONObject, (Object) new JSONObject());
            this.b__J_ = (String) BaseUtils.getJsonValue("otpelf.endpoint", jSONObject, (Object) "https://cdn.razorpay.com/static/otpelf/");
            this.O_$B_ = (String) BaseUtils.getJsonValue("otpelf.version_file_name", jSONObject, (Object) "version.json");
            this.l_$w$ = (String) BaseUtils.getJsonValue("otpelf.js_file_name", jSONObject, (Object) "otpelf.js");
            this.Y$_o$ = ((Boolean) BaseUtils.getJsonValue("magic.enable", jSONObject, (Object) Boolean.TRUE)).booleanValue();
            this.E$_6$ = (JSONObject) BaseUtils.getJsonValue("magic.settings", jSONObject, (Object) new JSONObject());
            this.c__C_ = (String) BaseUtils.getJsonValue("magic.endpoint", jSONObject, (Object) "https://cdn.razorpay.com/static/magic/");
            this.g__v_ = (String) BaseUtils.getJsonValue("magic.version_file_name", jSONObject, (Object) "version.json");
            this.L__R$ = (String) BaseUtils.getJsonValue("magic.js_file_name", jSONObject, (Object) "magic.js");
            this.d__1_ = (String) BaseUtils.getJsonValue("checkout.end_point", jSONObject, (Object) "");
        } catch (Exception unused) {
        }
    }

    static JSONObject d__1_(Context context, int i) {
        InputStream openRawResource;
        String string = U$_z$.a_$P$(context).getString("rzp_config_json", (String) null);
        if (string == null) {
            try {
                openRawResource = context.getResources().openRawResource(i);
                StringWriter stringWriter = new StringWriter();
                char[] cArr = new char[1024];
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(openRawResource, Key.STRING_CHARSET_NAME));
                while (true) {
                    int read = bufferedReader.read(cArr);
                    if (read == -1) {
                        break;
                    }
                    stringWriter.write(cArr, 0, read);
                }
                openRawResource.close();
                string = stringWriter.toString();
            } catch (Exception unused) {
                return null;
            } catch (Throwable th) {
                openRawResource.close();
                throw th;
            }
        }
        return new JSONObject(string);
    }
}
