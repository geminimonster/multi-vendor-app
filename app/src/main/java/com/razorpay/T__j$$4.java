package com.razorpay;

import android.app.Activity;

class T__j$$4 implements Callback {
    private /* synthetic */ Object Q_$2$$5c24f078;

    public T__j$$4(Object obj) {
        this.Q_$2$$5c24f078 = obj;
    }

    public final void run(C__D$ c__d$) {
        if (c__d$.d__1_() != null) {
            try {
                String versionFromJsonString = BaseUtils.getVersionFromJsonString(c__d$.d__1_(), (String) ((Class) O__Y_.d__1_(18, 18, 0)).getField("R$$r_").get((Object) null));
                if (!BaseUtils.getLocalVersion((Activity) ((Class) O__Y_.d__1_(18, 18, 0)).getDeclaredField("Q_$2$").get(this.Q_$2$$5c24f078), (String) ((Class) O__Y_.d__1_(18, 18, 0)).getField("R$$r_").get((Object) null)).equals(versionFromJsonString)) {
                    Object obj = this.Q_$2$$5c24f078;
                    Object[] objArr = new Object[2];
                    objArr[1] = versionFromJsonString;
                    objArr[0] = obj;
                    ((Class) O__Y_.d__1_(18, 18, 0)).getDeclaredMethod("R$$r_", new Class[]{(Class) O__Y_.d__1_(18, 18, 0), String.class}).invoke((Object) null, objArr);
                }
            } catch (Exception e) {
                AnalyticsUtil.reportError(e, "error", "Could not extract version from server json");
            } catch (Throwable th) {
                Throwable cause = th.getCause();
                if (cause != null) {
                    throw cause;
                }
                throw th;
            }
        }
    }
}
