package com.razorpay;

import org.json.JSONObject;

final class E$_j$ {
    E$_j$() {
    }

    static JSONObject d__1_(boolean z) {
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("otp_read", z ? 1 : 0);
            return jSONObject;
        } catch (Exception e) {
            AnalyticsUtil.reportError(e, "error", e.getMessage());
            return null;
        }
    }
}
