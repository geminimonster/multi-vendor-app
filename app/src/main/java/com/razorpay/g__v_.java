package com.razorpay;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.ResolveInfo;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.widget.LinearLayout;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

final class g__v_ {
    private static Dialog Q_$2$;

    g__v_() {
    }

    static String a_$P$(String str, String str2, String str3) {
        if (str == null) {
            return null;
        }
        if (str2 == null) {
            return str;
        }
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        String str4 = "?";
        if (str.contains(str4)) {
            str4 = "&";
        }
        sb.append(str4);
        sb.append(str2);
        String obj = sb.toString();
        if (str3 == null) {
            return obj;
        }
        StringBuilder sb2 = new StringBuilder();
        sb2.append(obj);
        sb2.append("=");
        sb2.append(str3);
        return sb2.toString();
    }

    static void G__G_(Context context, String str, String str2) {
        SharedPreferences.Editor G__G_ = U$_z$.G__G_(context);
        if (str2 == null) {
            G__G_.remove("pref_merchant_options_".concat(String.valueOf(str)));
        } else {
            G__G_.putString("pref_merchant_options_".concat(String.valueOf(str)), str2);
        }
        G__G_.apply();
    }

    static void R$$r_(Context context, String str, String str2, String str3, final g__v_$Q_$2$ g__v__q__2_) {
        new AlertDialog.Builder(context).setMessage(str).setPositiveButton(str2, new DialogInterface.OnClickListener() {
            public final void onClick(DialogInterface dialogInterface, int i) {
                g__v__q__2_.G__G_();
            }
        }).setNegativeButton(str3, new DialogInterface.OnClickListener() {
            public final void onClick(DialogInterface dialogInterface, int i) {
                g__v__q__2_.R$$r_();
            }
        }).show();
    }

    static JSONArray R$$r_(Context context) {
        List<ResolveInfo> listOfAppsWhichHandleDeepLink = BaseUtils.getListOfAppsWhichHandleDeepLink(context, "upi://pay");
        if (listOfAppsWhichHandleDeepLink == null || listOfAppsWhichHandleDeepLink.size() <= 0) {
            return null;
        }
        JSONArray jSONArray = new JSONArray();
        for (ResolveInfo d__1_ : listOfAppsWhichHandleDeepLink) {
            jSONArray.put(d__1_(context, d__1_));
        }
        return jSONArray;
    }

    private static JSONObject d__1_(Context context, ResolveInfo resolveInfo) {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("package_name", resolveInfo.activityInfo.packageName);
            jSONObject.put("app_name", BaseUtils.getAppNameOfResolveInfo(resolveInfo, context));
            jSONObject.put("app_icon", BaseUtils.getBase64FromOtherAppsResource(context, resolveInfo.activityInfo.packageName));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return jSONObject;
    }

    static void Q_$2$(Context context) {
        if (L__R$.L__R$().O__Y_() && context != null && !((Activity) context).isFinishing()) {
            Dialog dialog = Q_$2$;
            if (dialog == null || !dialog.isShowing()) {
                Dialog dialog2 = new Dialog(context);
                Q_$2$ = dialog2;
                dialog2.requestWindowFeature(1);
                Q_$2$.getWindow().setBackgroundDrawable(new ColorDrawable(0));
                Q_$2$.setContentView(R.layout.rzp_loader);
                ((CircularProgressView) Q_$2$.findViewById(R.id.progressBar)).setColor(Color.parseColor(L__R$.L__R$().K$$z$()));
                ((LinearLayout) Q_$2$.findViewById(R.id.ll_loader)).setOnClickListener(new View.OnClickListener() {
                    public final void onClick(View view) {
                        g__v_.a_$P$();
                    }
                });
                try {
                    Q_$2$.show();
                } catch (Exception unused) {
                }
            }
        }
    }

    static void a_$P$() {
        Dialog dialog = Q_$2$;
        if (dialog != null) {
            if (dialog.isShowing()) {
                try {
                    Q_$2$.dismiss();
                } catch (Exception unused) {
                }
            }
            Q_$2$ = null;
        }
    }
}
