package com.razorpay;

import android.webkit.JavascriptInterface;
import com.razorpay.Y$_o$;
import java.util.HashMap;
import java.util.Map;

public class PluginCheckoutBridge extends Y$_o$ {
    /* access modifiers changed from: private */
    public final PluginCheckoutInteractor a_$P$;

    @JavascriptInterface
    public /* bridge */ /* synthetic */ void invokePopup(String str) {
        super.invokePopup(str);
    }

    @JavascriptInterface
    public /* bridge */ /* synthetic */ void onCheckoutBackPress() {
        super.onCheckoutBackPress();
    }

    PluginCheckoutBridge(PluginCheckoutInteractor pluginCheckoutInteractor) {
        super(pluginCheckoutInteractor, 1);
        this.a_$P$ = pluginCheckoutInteractor;
    }

    @JavascriptInterface
    public void processPayment(final String str) {
        HashMap hashMap = new HashMap();
        hashMap.put("data", str);
        AnalyticsUtil.trackEvent(AnalyticsEvent.CHECKOUT_PLUGIN_PROCESS_PAYMENT_CALLED, (Map<String, Object>) hashMap);
        super.R$$r_(new Y$_o$.d__1_() {
            public final void d__1_() {
                PluginCheckoutBridge.this.a_$P$.processPayment(str);
            }
        });
    }
}
