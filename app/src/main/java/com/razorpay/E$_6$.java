package com.razorpay;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONObject;

final class E$_6$ {
    E$_6$() {
    }

    static String a_$P$(Context context, JSONArray jSONArray) {
        JSONObject jSONObject;
        String str;
        JSONArray jSONArray2 = jSONArray;
        String str2 = "";
        if (!(jSONArray2 == null || jSONArray.length() == 0)) {
            if (jSONArray.length() == 1) {
                try {
                    jSONObject = jSONArray2.getJSONObject(0);
                } catch (Exception unused) {
                }
            } else {
                if (jSONArray.length() != 1) {
                    String str3 = "{";
                    String str4 = null;
                    boolean z = false;
                    boolean z2 = true;
                    for (int i = 0; i < jSONArray.length(); i++) {
                        try {
                            JSONObject jSONObject2 = jSONArray2.getJSONObject(i);
                            if (!z2) {
                                StringBuilder sb = new StringBuilder();
                                sb.append(str3);
                                sb.append(",");
                                str3 = sb.toString();
                            } else {
                                z2 = false;
                            }
                            StringBuilder sb2 = new StringBuilder();
                            sb2.append(str3);
                            sb2.append("'");
                            sb2.append(jSONObject2.getString("card_saving_token_source"));
                            sb2.append("': '");
                            sb2.append(jSONObject2.getString("rzp_device_token"));
                            sb2.append("'");
                            str3 = sb2.toString();
                            if (str4 == null) {
                                str4 = jSONObject2.getString("rzp_device_token");
                            } else if (!str4.equals(jSONObject2.getString("rzp_device_token"))) {
                                z = true;
                            }
                        } catch (Throwable unused2) {
                        }
                    }
                    StringBuilder sb3 = new StringBuilder();
                    sb3.append(str3);
                    sb3.append("}");
                    String obj = sb3.toString();
                    if (z) {
                        HashMap hashMap = new HashMap();
                        hashMap.put("packages", obj);
                        AnalyticsUtil.trackEvent(AnalyticsEvent.MULTIPLE_TOKEN_EVENT, (Map<String, Object>) hashMap);
                        return null;
                    }
                    jSONObject = jSONArray2.getJSONObject(0);
                }
                jSONObject = null;
            }
            if (jSONObject != null) {
                try {
                    str = jSONObject.getString("rzp_device_token");
                    try {
                        str2 = jSONObject.getString("card_saving_token_source");
                    } catch (Exception unused3) {
                    }
                } catch (Exception unused4) {
                    str = str2;
                }
                U$_z$.Q_$2$(context).putString("rzp_device_token", str).apply();
                AnalyticsUtil.addProperty("device_token_source_single", new AnalyticsProperty(str2, AnalyticsProperty$Q_$2$.ORDER));
                return str;
            }
        }
        return null;
    }

    static void G__G_(Context context) throws IllegalStateException {
        if (L__R$.L__R$().h__y_() && U$_z$.d__1_(context).getString("rzp_device_token", (String) null) != null) {
            AnalyticsUtil.addProperty("device_token_source_single", new AnalyticsProperty(context.getPackageName(), AnalyticsProperty$Q_$2$.ORDER));
        } else if (Build.VERSION.SDK_INT >= 24 && L__R$.L__R$().B$$J$()) {
            Intent intent = new Intent();
            intent.setAction("rzp.device_token.share");
            context.sendOrderedBroadcast(intent, (String) null, new BroadcastReceiver() {
                public final void onReceive(Context context, Intent intent) {
                    String string;
                    Bundle resultExtras = getResultExtras(true);
                    if (resultExtras != null && (string = resultExtras.getString("device_token_info_list")) != null) {
                        try {
                            E$_6$.a_$P$(context, new JSONArray(string));
                        } catch (Exception unused) {
                        }
                    }
                }
            }, (Handler) null, -1, (String) null, (Bundle) null);
        } else if (L__R$.L__R$().H$_a_()) {
            a_$P$(context, a_$P$(context));
        }
    }

    private static JSONArray a_$P$(Context context) {
        JSONArray jSONArray = new JSONArray();
        int i = 0;
        for (ResolveInfo resolveInfo : BaseUtils.getListOfAppsWhichHandleDeepLink(context, "io.rzp://rzp.io")) {
            String str = resolveInfo.activityInfo.taskAffinity;
            i++;
            try {
                String string = U$_z$.d__1_(context.createPackageContext(str, 2)).getString("rzp_device_token", (String) null);
                if (string != null) {
                    JSONObject jSONObject = new JSONObject();
                    jSONObject.put("rzp_device_token", string);
                    jSONObject.put("card_saving_token_source", str);
                    jSONArray.put(jSONObject);
                }
            } catch (Exception e) {
                if (!(e instanceof SecurityException) || Build.VERSION.SDK_INT < 24) {
                    AnalyticsUtil.reportError(e, "critical", e.getMessage());
                } else {
                    AnalyticsUtil.trackEvent(AnalyticsEvent.SHARE_PREFERENCES_SECURITY_EXCEPTION);
                }
            }
        }
        AnalyticsUtil.addProperty("sdk_count", new AnalyticsProperty(i, AnalyticsProperty$Q_$2$.ORDER));
        AnalyticsUtil.addProperty("sdk_count_with_token", new AnalyticsProperty(jSONArray.length(), AnalyticsProperty$Q_$2$.ORDER));
        return jSONArray;
    }
}
