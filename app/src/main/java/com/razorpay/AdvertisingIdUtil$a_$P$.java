package com.razorpay;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

final class AdvertisingIdUtil$a_$P$ implements IInterface {
    private IBinder R$$r_;

    public AdvertisingIdUtil$a_$P$(IBinder iBinder) {
        this.R$$r_ = iBinder;
    }

    public final IBinder asBinder() {
        return this.R$$r_;
    }

    public final String d__1_() throws RemoteException {
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInterfaceToken("com.google.android.gms.ads.identifier.internal.IAdvertisingIdService");
            this.R$$r_.transact(1, obtain, obtain2, 0);
            obtain2.readException();
            return obtain2.readString();
        } finally {
            obtain2.recycle();
            obtain.recycle();
        }
    }
}
