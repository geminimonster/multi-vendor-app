package com.razorpay;

import java.io.Serializable;
import org.json.JSONObject;

public class PaymentData implements Serializable {
    private String G__G_;
    private String Q_$2$;
    private String R$$r_;
    private String a_$P$;
    private String b__J_;
    private String d__1_;
    private JSONObject r$_Y_ = new JSONObject();

    public String getUserEmail() {
        return this.a_$P$;
    }

    /* access modifiers changed from: package-private */
    public final void d__1_(String str) {
        this.a_$P$ = str;
    }

    public String getUserContact() {
        return this.Q_$2$;
    }

    /* access modifiers changed from: package-private */
    public final void a_$P$(String str) {
        this.Q_$2$ = str;
    }

    /* access modifiers changed from: package-private */
    public final void G__G_(String str) {
        this.G__G_ = str;
    }

    public String getPaymentId() {
        return this.G__G_;
    }

    public String getOrderId() {
        return this.d__1_;
    }

    /* access modifiers changed from: package-private */
    public final void R$$r_(String str) {
        this.d__1_ = str;
    }

    public String getSignature() {
        return this.R$$r_;
    }

    /* access modifiers changed from: package-private */
    public final void Q_$2$(String str) {
        this.R$$r_ = str;
    }

    /* access modifiers changed from: package-private */
    public final void d__1_(JSONObject jSONObject) {
        this.r$_Y_ = jSONObject;
    }

    public JSONObject getData() {
        return this.r$_Y_;
    }

    /* access modifiers changed from: package-private */
    public final void r$_Y_(String str) {
        this.b__J_ = str;
    }

    public String getExternalWallet() {
        return this.b__J_;
    }
}
