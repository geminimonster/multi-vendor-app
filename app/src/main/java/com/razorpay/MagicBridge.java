package com.razorpay;

import android.webkit.JavascriptInterface;

public class MagicBridge {
    private c__C_ d__1_;

    MagicBridge(c__C_ c__c_) {
        this.d__1_ = c__c_;
    }

    @JavascriptInterface
    public final void relay(String str) {
        this.d__1_.sendDataToWebView(1, str);
    }
}
