package com.razorpay;

import java.util.regex.Pattern;

public class OTP {
    String G__G_;
    private String R$$r_;
    private String a_$P$;

    OTP(String str, String str2, String str3) {
        this.a_$P$ = str;
        this.R$$r_ = str2;
        this.G__G_ = str3;
        if (!Pattern.compile("^\\d").matcher(this.a_$P$).find()) {
            this.a_$P$ = this.a_$P$.substring(1);
        }
        if (!Pattern.compile("\\d$").matcher(this.a_$P$).find()) {
            String str4 = this.a_$P$;
            this.a_$P$ = str4.substring(0, str4.length() - 1);
        }
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("Pin: ");
        sb.append(this.a_$P$);
        sb.append(" bank: ");
        sb.append(this.R$$r_);
        sb.append(" sender: ");
        sb.append(this.G__G_);
        return sb.toString();
    }
}
