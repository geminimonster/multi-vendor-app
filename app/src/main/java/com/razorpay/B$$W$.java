package com.razorpay;

import android.os.Bundle;
import java.util.HashMap;

abstract class B$$W$ extends b__J_ {
    B$$W$() {
    }

    public void onCreate(Bundle bundle) {
        HashMap<String, String> allPluginsFromManifest = BaseUtils.getAllPluginsFromManifest(this);
        if (allPluginsFromManifest == null || allPluginsFromManifest.size() == 0) {
            this.presenter = new Y_$B$(this, this);
            this.checkoutBridgeObject = new Y$_o$((c__C_) this.presenter, 1);
            super.onCreate(bundle);
            return;
        }
        this.presenter = new PluginOtpElfCheckoutPresenterImpl(this, this, allPluginsFromManifest);
        this.checkoutBridgeObject = new PluginCheckoutBridge((PluginCheckoutInteractor) this.presenter);
        super.onCreate(bundle);
        for (String loadClass : allPluginsFromManifest.values()) {
            try {
                RzpPlugin rzpPlugin = (RzpPlugin) RzpPlugin.class.getClassLoader().loadClass(loadClass).newInstance();
                RzpPluginCompatibilityResponse isCompatible = rzpPlugin.isCompatible(L__R$.d__1_, L__R$.G__G_, L__R$.Q_$2$);
                if (!isCompatible.isCompatible()) {
                    destroy(7, isCompatible.getErrorMessage());
                    return;
                }
                rzpPlugin.isRegistered(this);
            } catch (ClassNotFoundException | IllegalAccessException | InstantiationException e) {
                e.printStackTrace();
            }
        }
    }
}
