package com.razorpay;

import android.content.Context;
import android.content.SharedPreferences;
import android.webkit.JavascriptInterface;

final class S$_U_ {
    private SharedPreferences G__G_;
    private SharedPreferences.Editor d__1_;

    S$_U_(Context context) {
        this.G__G_ = U$_z$.E$_j$(context);
        this.d__1_ = U$_z$.R$$r_(context);
    }

    @JavascriptInterface
    public final void setString(String str, String str2) {
        try {
            this.d__1_.putString(str, str2);
            this.d__1_.commit();
        } catch (Exception unused) {
        }
    }

    @JavascriptInterface
    public final void setBoolean(String str, boolean z) {
        try {
            this.d__1_.putBoolean(str, z);
            this.d__1_.commit();
        } catch (Exception unused) {
        }
    }

    @JavascriptInterface
    public final void setInt(String str, int i) {
        try {
            this.d__1_.putInt(str, i);
            this.d__1_.commit();
        } catch (Exception unused) {
        }
    }

    @JavascriptInterface
    public final void setFloat(String str, float f) {
        try {
            this.d__1_.putFloat(str, f);
            this.d__1_.commit();
        } catch (Exception unused) {
        }
    }

    @JavascriptInterface
    public final String getString(String str) {
        try {
            return this.G__G_.getString(str, (String) null);
        } catch (Exception unused) {
            return null;
        }
    }

    @JavascriptInterface
    public final boolean getBoolean(String str) {
        try {
            return this.G__G_.getBoolean(str, false);
        } catch (Exception unused) {
            return false;
        }
    }

    @JavascriptInterface
    public final float getFloat(String str) {
        try {
            return this.G__G_.getFloat(str, 0.0f);
        } catch (Exception unused) {
            return 0.0f;
        }
    }

    @JavascriptInterface
    public final int getInt(String str) {
        try {
            return this.G__G_.getInt(str, 0);
        } catch (Exception unused) {
            return 0;
        }
    }
}
