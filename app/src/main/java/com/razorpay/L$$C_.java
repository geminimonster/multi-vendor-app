package com.razorpay;

import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Transformation;

final class L$$C_ extends Animation {
    private int G__G_;
    private View Q_$2$;
    private int R$$r_;

    public final boolean willChangeBounds() {
        return true;
    }

    L$$C_(View view, int i) {
        this.Q_$2$ = view;
        this.G__G_ = i;
        this.R$$r_ = view.getWidth();
    }

    /* access modifiers changed from: protected */
    public final void applyTransformation(float f, Transformation transformation) {
        int i = this.R$$r_;
        this.Q_$2$.getLayoutParams().width = i + ((int) (((float) (this.G__G_ - i)) * f));
        this.Q_$2$.requestLayout();
    }

    public final void initialize(int i, int i2, int i3, int i4) {
        super.initialize(i, i2, i3, i4);
    }
}
