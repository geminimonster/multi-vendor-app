package com.razorpay;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.RelativeLayout;
import android.widget.Toast;
import com.facebook.appevents.codeless.internal.Constants;
import java.util.HashMap;

class b__J_ extends Activity implements l__d$$R$$r_, t_$J_ {
    private b_$A$ B$$W$;
    private WebViewClient D$_X_;
    private WebChromeClient E$_j$;
    private WebViewClient G__G_;
    private WebView Q_$2$;
    private ViewGroup R$$r_;
    private RelativeLayout a_$P$;
    private I$_n_ b__J_;
    protected Object checkoutBridgeObject;
    private WebView d__1_;
    protected l_$w$ presenter;
    private WebChromeClient r$_Y_;

    public void postSms(String str, String str2) {
    }

    b__J_() {
    }

    private void R$$r_(int i, WebViewClient webViewClient) {
        if (i == 1) {
            this.G__G_ = webViewClient;
        } else if (i == 2) {
            this.D$_X_ = webViewClient;
        }
    }

    private void G__G_(int i, WebChromeClient webChromeClient) {
        if (i == 1) {
            this.r$_Y_ = webChromeClient;
        } else if (i == 2) {
            this.E$_j$ = webChromeClient;
        }
    }

    public void onCreate(Bundle bundle) {
        boolean z;
        U$_z$.Q_$2$(this, L__R$.Q_$2$);
        L__R$.L__R$().a_$P$(r$_Y_.d__1_(this, R.raw.rzp_config));
        BaseUtils.checkForLatestVersion(this, L__R$.G__G_);
        this.presenter.setCheckoutLoadStartAt();
        R$$r_(1, new H$_a_(this.presenter));
        R$$r_(2, new Q__v$(this.presenter));
        G__G_(1, new I$_e_(this.presenter));
        G__G_(2, new y$_O_(this.presenter));
        BaseUtils.setup();
        AnalyticsUtil.trackEvent(AnalyticsEvent.CHECKOUT_INIT);
        requestWindowFeature(1);
        super.onCreate(bundle);
        int i = 0;
        if (bundle == null) {
            bundle = getIntent().getExtras();
            z = false;
        } else {
            z = true;
        }
        if (this.presenter.setOptions(bundle, z)) {
            this.R$$r_ = (ViewGroup) findViewById(16908290);
            Object obj = this.checkoutBridgeObject;
            WebView webView = new WebView(this);
            this.d__1_ = webView;
            BaseUtils.setWebViewSettings(this, webView, false);
            this.d__1_.clearFormData();
            this.d__1_.addJavascriptInterface(obj, "CheckoutBridge");
            this.d__1_.setWebChromeClient(this.r$_Y_);
            this.d__1_.setWebViewClient(this.G__G_);
            WebView webView2 = new WebView(this);
            this.Q_$2$ = webView2;
            BaseUtils.setWebViewSettings(this, webView2, false);
            this.Q_$2$.clearFormData();
            this.Q_$2$.addJavascriptInterface(new MagicBridge((c__C_) this.presenter), "MagicBridge");
            this.Q_$2$.addJavascriptInterface(new Y$_o$((c__C_) this.presenter, 2), "CheckoutBridge");
            this.Q_$2$.setVisibility(8);
            this.Q_$2$.setWebChromeClient(this.E$_j$);
            this.Q_$2$.setWebViewClient(this.D$_X_);
            RelativeLayout relativeLayout = new RelativeLayout(this);
            this.a_$P$ = relativeLayout;
            relativeLayout.setLayoutParams(new RelativeLayout.LayoutParams(-1, -1));
            this.a_$P$.setBackgroundColor(-1);
            this.R$$r_.addView(this.a_$P$);
            this.d__1_.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
            this.Q_$2$.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
            this.a_$P$.addView(this.d__1_);
            this.a_$P$.addView(this.Q_$2$);
            String progressBarColor = this.presenter.getProgressBarColor();
            if (progressBarColor != null) {
                this.B$$W$ = new b_$A$(this, this.a_$P$, progressBarColor);
            } else {
                this.B$$W$ = new b_$A$(this, this.a_$P$);
            }
            this.presenter.setUpAddOn();
            this.presenter.loadForm("");
            this.presenter.passPrefillToSegment();
            if ((getWindow().getAttributes().flags & 1024) != 0) {
                D$_X_.a_$P$(this);
            }
            if (getResources().getBoolean(R.bool.isTablet)) {
                setFinishOnTouchOutside(false);
                WindowManager.LayoutParams attributes = getWindow().getAttributes();
                int applyDimension = (int) TypedValue.applyDimension(1, 375.0f, getResources().getDisplayMetrics());
                DisplayMetrics displayMetrics = new DisplayMetrics();
                getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
                int i2 = displayMetrics.heightPixels;
                int identifier = getResources().getIdentifier("status_bar_height", "dimen", Constants.PLATFORM);
                if (identifier > 0) {
                    i = getResources().getDimensionPixelSize(identifier);
                }
                int i3 = i2 - i;
                if (i3 > 600) {
                    i3 = 600;
                }
                attributes.height = i3;
                attributes.width = applyDimension;
                getWindow().setAttributes(attributes);
            } else {
                setRequestedOrientation(1);
            }
            this.presenter.fetchCondfig();
            this.presenter.handleCardSaving();
            if (!BaseUtils.isDeviceHaveCorrectTlsVersion()) {
                AnalyticsUtil.trackEvent(AnalyticsEvent.CHECKOUT_TLS_ERROR);
                destroy(6, "TLSv1  is not supported for security reasons");
            }
        }
    }

    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        this.presenter.saveInstanceState(bundle);
    }

    public void onBackPressed() {
        this.presenter.backPressed(new HashMap());
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        AnalyticsUtil.trackEvent(AnalyticsEvent.ACTIVITY_ONDESTROY_CALLED);
        this.presenter.cleanUpOnDestroy();
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        this.presenter.onActivityResultReceived(i, i2, intent);
    }

    public void onRequestPermissionsResult(int i, String[] strArr, int[] iArr) {
        this.presenter.onRequestPermissionsResult(i, strArr, iArr);
    }

    public void setSmsPermission(boolean z) {
        this.presenter.sendOtpPermissionCallback(z);
        I$_n_ i$_n_ = this.b__J_;
        if (i$_n_ != null) {
            try {
                i$_n_.a_$P$.remove(this);
            } catch (Exception unused) {
            }
        }
    }

    public void loadUrl(int i, String str) {
        if (i == 1) {
            this.d__1_.loadUrl(str);
        } else if (i == 2) {
            this.Q_$2$.loadUrl(str);
        }
    }

    public void addJavascriptInterfaceToPrimaryWebview(Object obj, String str) {
        this.d__1_.addJavascriptInterface(obj, str);
    }

    public void loadData(int i, String str, String str2, String str3) {
        if (i == 1) {
            this.d__1_.loadData(str, str2, str3);
        } else if (i == 2) {
            this.Q_$2$.loadData(str, str2, str3);
        }
    }

    public void loadDataWithBaseURL(int i, String str, String str2, String str3, String str4, String str5) {
        if (i == 1) {
            this.d__1_.loadDataWithBaseURL(str, str2, str3, str4, str5);
        } else if (i == 2) {
            this.Q_$2$.loadDataWithBaseURL(str, str2, str3, str4, str5);
        }
    }

    public void makeWebViewVisible(int i) {
        if (i != 1) {
            if (i == 2 && this.Q_$2$.getVisibility() == 8) {
                this.d__1_.setVisibility(8);
                this.Q_$2$.setVisibility(0);
                g__v_.a_$P$();
                AnalyticsUtil.trackEvent(AnalyticsEvent.WEB_VIEW_PRIMARY_TO_SECONDARY_SWITCH);
            }
        } else if (this.d__1_.getVisibility() == 8) {
            this.d__1_.setVisibility(0);
            this.Q_$2$.setVisibility(8);
            g__v_.a_$P$();
            AnalyticsUtil.trackEvent(AnalyticsEvent.WEB_VIEW_SECONDARY_TO_PRIMARY_SWITCH);
        }
    }

    public boolean isWebViewVisible(int i) {
        WebView webView;
        if (i == 1) {
            WebView webView2 = this.d__1_;
            if (webView2 == null || webView2.getVisibility() != 0) {
                return false;
            }
            return true;
        } else if (i == 2 && (webView = this.Q_$2$) != null && webView.getVisibility() == 0) {
            return true;
        }
        return false;
    }

    public void showToast(String str, int i) {
        Toast.makeText(this, str, i).show();
    }

    public void destroy(int i, String str) {
        Intent intent = new Intent();
        intent.putExtra("RESULT", str);
        setResult(i, intent);
        finish();
    }

    public void showProgressBar(int i) {
        b_$A$ b__a_ = this.B$$W$;
        if (b__a_ != null) {
            b__a_.R$$r_(i);
        }
    }

    public void hideProgressBar() {
        b_$A$ b__a_ = this.B$$W$;
        if (b__a_ != null) {
            b__a_.R$$r_();
        }
    }

    public void clearWebViewHistory(int i) {
        if (i == 1) {
            this.d__1_.clearHistory();
        } else if (i == 2) {
            this.Q_$2$.clearHistory();
        }
    }

    public WebView getWebView(int i) {
        if (i == 1) {
            return this.d__1_;
        }
        if (i != 2) {
            return null;
        }
        return this.Q_$2$;
    }

    public void checkSmsPermission() {
        if (I$_n_.d__1_ == null) {
            I$_n_.d__1_ = new I$_n_();
        }
        I$_n_ i$_n_ = I$_n_.d__1_;
        this.b__J_ = i$_n_;
        i$_n_.a_$P$.add(this);
        this.b__J_.a_$P$(this);
    }
}
