package com.razorpay;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.WallpaperManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.media.RingtoneManager;
import android.net.Uri;
import android.net.http.SslCertificate;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StatFs;
import android.os.SystemClock;
import android.provider.Settings;
import android.support.v4.media.session.PlaybackStateCompat;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.webkit.CookieManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import com.bumptech.glide.load.Key;
import com.facebook.appevents.AppEventsConstants;
import com.facebook.internal.AnalyticsEvents;
import com.facebook.internal.ServerProtocol;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.razorpay.AdvertisingIdUtil;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.KeyFactory;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.spec.X509EncodedKeySpec;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import javax.net.ssl.HttpsURLConnection;
import kotlin.text.Typography;
import org.bouncycastle.pqc.math.linearalgebra.Matrix;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class BaseUtils {
    private static int B$$W$ = 1;
    private static char[] G__G_ = {136, 144, 134, 127, 137, 139, 132, 138, '{', 'z', '}', 130, 133, 143};
    private static String PERMISSION_DISABLED = "permission disabled";
    private static boolean Q_$2$ = true;
    private static int R$$r_ = 22;
    private static int a_$P$ = 0;
    private static boolean d__1_ = true;
    private static String ipAddress;
    private static boolean isCompatibleWithGooglePay = true;
    private static boolean sWebViewDebuggingEnabled = J$_0_.Q_$2$.booleanValue();

    BaseUtils() {
    }

    static /* synthetic */ JSONObject access$000(HttpsURLConnection httpsURLConnection) throws IOException, JSONException {
        int i = B$$W$ + 47;
        a_$P$ = i % 128;
        char c = i % 2 != 0 ? '%' : Typography.dollar;
        JSONObject responseJson = getResponseJson(httpsURLConnection);
        if (c == '%') {
            Object obj = null;
            super.hashCode();
        }
        return responseJson;
    }

    static /* synthetic */ String access$100() {
        String str;
        int i = B$$W$ + 101;
        a_$P$ = i % 128;
        if ((i % 2 != 0 ? Typography.greater : 30) != '>') {
            str = ipAddress;
        } else {
            str = ipAddress;
            Object[] objArr = null;
            int length = objArr.length;
        }
        int i2 = a_$P$ + 105;
        B$$W$ = i2 % 128;
        if ((i2 % 2 == 0 ? 12 : '*') != 12) {
            return str;
        }
        int i3 = 71 / 0;
        return str;
    }

    static /* synthetic */ String access$102(String str) {
        int i = B$$W$ + 23;
        a_$P$ = i % 128;
        boolean z = i % 2 == 0;
        ipAddress = str;
        if (!z) {
            Object[] objArr = null;
            int length = objArr.length;
        }
        return str;
    }

    static String constructBasicAuth(String str) throws UnsupportedEncodingException {
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append(":");
        String encodeToString = Base64.encodeToString(sb.toString().getBytes(StandardCharsets.UTF_8), 2);
        int i = B$$W$ + 15;
        a_$P$ = i % 128;
        if (!(i % 2 != 0)) {
            return encodeToString;
        }
        Object obj = null;
        super.hashCode();
        return encodeToString;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x002b, code lost:
        if ((r3.checkCallingOrSelfPermission(r4) == 0 ? ')' : kotlin.text.Typography.dollar) != '$') goto L_0x002d;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static boolean hasPermission(android.content.Context r3, java.lang.String r4) {
        /*
            int r0 = B$$W$
            int r0 = r0 + 77
            int r1 = r0 % 128
            a_$P$ = r1
            int r0 = r0 % 2
            r1 = 1
            r2 = 0
            if (r0 == 0) goto L_0x001e
            int r3 = r3.checkCallingOrSelfPermission(r4)     // Catch:{ Exception -> 0x0048 }
            r4 = 35
            if (r1 != r3) goto L_0x0019
            r3 = 28
            goto L_0x001b
        L_0x0019:
            r3 = 35
        L_0x001b:
            if (r3 == r4) goto L_0x002e
            goto L_0x002d
        L_0x001e:
            int r3 = r3.checkCallingOrSelfPermission(r4)     // Catch:{ Exception -> 0x0048 }
            r4 = 36
            if (r3 != 0) goto L_0x0029
            r3 = 41
            goto L_0x002b
        L_0x0029:
            r3 = 36
        L_0x002b:
            if (r3 == r4) goto L_0x002e
        L_0x002d:
            return r1
        L_0x002e:
            int r3 = a_$P$
            int r3 = r3 + 79
            int r4 = r3 % 128
            B$$W$ = r4
            int r3 = r3 % 2
            r4 = 14
            if (r3 != 0) goto L_0x003f
            r3 = 14
            goto L_0x0041
        L_0x003f:
            r3 = 33
        L_0x0041:
            if (r3 == r4) goto L_0x0044
            return r2
        L_0x0044:
            r3 = 65
            int r3 = r3 / r2
            return r2
        L_0x0048:
            r3 = move-exception
            java.lang.String r4 = r3.getMessage()
            java.lang.String r0 = "critical"
            com.razorpay.AnalyticsUtil.reportError(r3, r0, r4)
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.razorpay.BaseUtils.hasPermission(android.content.Context, java.lang.String):boolean");
    }

    static String getKeyId(Context context) {
        String str = null;
        try {
            ApplicationInfo applicationInfo = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128);
            if (applicationInfo.metaData != null) {
                String string = applicationInfo.metaData.getString("com.razorpay.ApiKey");
                int i = a_$P$ + 61;
                B$$W$ = i % 128;
                if ((i % 2 == 0 ? 8 : '5') != '5') {
                    int i2 = 79 / 0;
                }
                return string;
            }
            int i3 = a_$P$ + 75;
            B$$W$ = i3 % 128;
            if (i3 % 2 == 0) {
                super.hashCode();
            }
            return str;
        } catch (PackageManager.NameNotFoundException e) {
            AnalyticsUtil.reportError(e, "critical", e.getMessage());
            return str;
        }
    }

    static HashMap<String, String> getAllPluginsFromManifest(Context context) {
        int i = a_$P$ + 11;
        B$$W$ = i % 128;
        int i2 = i % 2;
        try {
            ApplicationInfo applicationInfo = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128);
            if (applicationInfo.metaData == null) {
                int i3 = a_$P$ + 35;
                B$$W$ = i3 % 128;
                int i4 = i3 % 2;
                return null;
            }
            HashMap<String, String> hashMap = new HashMap<>();
            Iterator it = applicationInfo.metaData.keySet().iterator();
            while (true) {
                if ((it.hasNext() ? 7 : ')') == ')') {
                    return hashMap;
                }
                int i5 = a_$P$ + 43;
                B$$W$ = i5 % 128;
                int i6 = i5 % 2;
                String str = (String) it.next();
                if (!((str.contains("com.razorpay.plugin.") ? '7' : '8') == '8' || applicationInfo.metaData.getString(str) == null)) {
                    hashMap.put(str, applicationInfo.metaData.getString(str));
                }
            }
        } catch (PackageManager.NameNotFoundException e) {
            AnalyticsUtil.reportError(e, "critical", e.getMessage());
            return null;
        }
    }

    private static void setBaseWebViewSettings() {
        int i = a_$P$ + 1;
        B$$W$ = i % 128;
        int i2 = i % 2;
        if (!(Build.VERSION.SDK_INT < 19)) {
            int i3 = a_$P$ + 101;
            B$$W$ = i3 % 128;
            int i4 = i3 % 2;
            WebView.setWebContentsDebuggingEnabled(sWebViewDebuggingEnabled);
            int i5 = a_$P$ + 81;
            B$$W$ = i5 % 128;
            int i6 = i5 % 2;
        }
    }

    private static void enableJavaScriptInWebView(WebView webView) {
        int i = B$$W$ + 125;
        a_$P$ = i % 128;
        int i2 = i % 2;
        webView.getSettings().setJavaScriptEnabled(true);
        int i3 = B$$W$ + 7;
        a_$P$ = i3 % 128;
        int i4 = i3 % 2;
    }

    static void setWebViewSettings(Context context, WebView webView, boolean z) {
        int i = a_$P$ + 121;
        B$$W$ = i % 128;
        int i2 = i % 2;
        setBaseWebViewSettings();
        enableJavaScriptInWebView(webView);
        CookieManager.getInstance().setAcceptCookie(true);
        webView.setTag("");
        WebSettings settings = webView.getSettings();
        settings.setDomStorageEnabled(true);
        settings.setDatabaseEnabled(true);
        settings.setTextZoom(100);
        String path = context.getApplicationContext().getDir("database", 0).getPath();
        if (Build.VERSION.SDK_INT < 19) {
            settings.setDatabasePath(path);
        }
        if (Build.VERSION.SDK_INT < 24) {
            settings.setGeolocationDatabasePath(path);
        }
        if (Build.VERSION.SDK_INT >= 21) {
            int i3 = a_$P$ + 87;
            B$$W$ = i3 % 128;
            int i4 = i3 % 2;
            CookieManager.getInstance().setAcceptThirdPartyCookies(webView, true);
        }
        if (z) {
            int i5 = B$$W$ + 73;
            a_$P$ = i5 % 128;
            if (!(i5 % 2 != 0)) {
                settings.setCacheMode(2);
            } else {
                settings.setCacheMode(3);
            }
        }
        settings.setSaveFormData(false);
        webView.addJavascriptInterface(new S$_U_(context), "StorageBridge");
    }

    static boolean hasFeature(Context context, String str) {
        int i = a_$P$ + 85;
        B$$W$ = i % 128;
        int i2 = i % 2;
        boolean hasSystemFeature = context.getPackageManager().hasSystemFeature(str);
        int i3 = B$$W$ + 69;
        a_$P$ = i3 % 128;
        if (i3 % 2 == 0) {
            return hasSystemFeature;
        }
        Object obj = null;
        super.hashCode();
        return hasSystemFeature;
    }

    public static <T> T getSystemService(Context context, String str) {
        int i = B$$W$ + 67;
        a_$P$ = i % 128;
        boolean z = i % 2 == 0;
        T systemService = context.getApplicationContext().getSystemService(str);
        if (!z) {
            int i2 = 55 / 0;
        }
        return systemService;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0034, code lost:
        if (r0 != false) goto L_0x0041;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x003f, code lost:
        if (r5.equalsIgnoreCase("2G") != false) goto L_0x0041;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0053, code lost:
        if (r5.equalsIgnoreCase("3G") == false) goto L_0x0058;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0055, code lost:
        r0 = 26;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0058, code lost:
        r0 = '`';
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x005a, code lost:
        if (r0 == 26) goto L_0x006f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0062, code lost:
        if (r5.equalsIgnoreCase("4G") == false) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0064, code lost:
        r5 = B$$W$ + 7;
        a_$P$ = r5 % 128;
        r5 = r5 % 2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x006e, code lost:
        return 4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x006f, code lost:
        return 3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:?, code lost:
        return -1;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static int getNetworkType(android.content.Context r5) {
        /*
            com.razorpay.Q$$U_ r0 = getDataNetworkType(r5)
            com.razorpay.Q$$U_ r1 = com.razorpay.Q$$U_.WIFI
            r2 = 0
            r3 = 1
            if (r0 != r1) goto L_0x000c
            r1 = 0
            goto L_0x000d
        L_0x000c:
            r1 = 1
        L_0x000d:
            r4 = 2
            if (r1 == 0) goto L_0x007d
            com.razorpay.Q$$U_ r1 = com.razorpay.Q$$U_.BLUETOOTH
            if (r0 != r1) goto L_0x0015
            r2 = 1
        L_0x0015:
            if (r2 == r3) goto L_0x0073
            com.razorpay.Q$$U_ r1 = com.razorpay.Q$$U_.CELLULAR
            if (r0 != r1) goto L_0x0071
            int r0 = a_$P$
            int r0 = r0 + 43
            int r1 = r0 % 128
            B$$W$ = r1
            int r0 = r0 % r4
            java.lang.String r1 = "2G"
            if (r0 != 0) goto L_0x0037
            java.lang.String r5 = getCellularNetworkType(r5)
            boolean r0 = r5.equalsIgnoreCase(r1)
            r1 = 0
            super.hashCode()
            if (r0 == 0) goto L_0x004b
            goto L_0x0041
        L_0x0037:
            java.lang.String r5 = getCellularNetworkType(r5)
            boolean r0 = r5.equalsIgnoreCase(r1)
            if (r0 == 0) goto L_0x004b
        L_0x0041:
            int r5 = a_$P$
            int r5 = r5 + 105
            int r0 = r5 % 128
            B$$W$ = r0
            int r5 = r5 % r4
            return r4
        L_0x004b:
            java.lang.String r0 = "3G"
            boolean r0 = r5.equalsIgnoreCase(r0)
            r1 = 26
            if (r0 == 0) goto L_0x0058
            r0 = 26
            goto L_0x005a
        L_0x0058:
            r0 = 96
        L_0x005a:
            if (r0 == r1) goto L_0x006f
            java.lang.String r0 = "4G"
            boolean r5 = r5.equalsIgnoreCase(r0)
            if (r5 == 0) goto L_0x0071
            int r5 = B$$W$
            int r5 = r5 + 7
            int r0 = r5 % 128
            a_$P$ = r0
            int r5 = r5 % r4
            r5 = 4
            return r5
        L_0x006f:
            r5 = 3
            return r5
        L_0x0071:
            r5 = -1
            return r5
        L_0x0073:
            int r5 = B$$W$
            int r5 = r5 + 65
            int r0 = r5 % 128
            a_$P$ = r0
            int r5 = r5 % r4
            return r3
        L_0x007d:
            int r5 = a_$P$
            int r5 = r5 + 103
            int r0 = r5 % 128
            B$$W$ = r0
            int r5 = r5 % r4
            if (r5 != 0) goto L_0x0089
            return r3
        L_0x0089:
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.razorpay.BaseUtils.getNetworkType(android.content.Context):int");
    }

    public static String getCellularNetworkType(Context context) {
        int i = B$$W$ + 89;
        a_$P$ = i % 128;
        int i2 = i % 2;
        try {
            switch (((TelephonyManager) context.getSystemService("phone")).getNetworkType()) {
                case 1:
                case 2:
                case 4:
                case 7:
                case 11:
                    int i3 = a_$P$ + 29;
                    B$$W$ = i3 % 128;
                    if ((i3 % 2 == 0 ? 'N' : Typography.greater) != 'N') {
                        return "2G";
                    }
                    Object[] objArr = null;
                    int length = objArr.length;
                    return "2G";
                case 3:
                case 5:
                case 6:
                case 8:
                case 9:
                case 10:
                case 12:
                case 14:
                case 15:
                    return "3G";
                case 13:
                    return "4G";
            }
        } catch (Exception unused) {
        }
        return "NA";
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0045, code lost:
        r3 = r3.getNetworkOperatorName();
        r0 = B$$W$ + 21;
        a_$P$ = r0 % 128;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0055, code lost:
        if ((r0 % 2) == 0) goto L_0x005a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0057, code lost:
        r0 = 26;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x005a, code lost:
        r0 = 9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x005c, code lost:
        if (r0 == 9) goto L_0x0062;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x005e, code lost:
        r0 = 60 / 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0062, code lost:
        return r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:3:0x0018, code lost:
        if (r3 != null) goto L_0x0045;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x002a, code lost:
        if ((r3 != null ? 'K' : 'S') != 'K') goto L_0x002c;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String getCellularNetworkProviderName(android.content.Context r3) {
        /*
            int r0 = B$$W$
            int r0 = r0 + 113
            int r1 = r0 % 128
            a_$P$ = r1
            int r0 = r0 % 2
            r1 = 0
            java.lang.String r2 = "phone"
            if (r0 == 0) goto L_0x001b
            java.lang.Object r3 = getSystemService(r3, r2)
            android.telephony.TelephonyManager r3 = (android.telephony.TelephonyManager) r3
            super.hashCode()
            if (r3 == 0) goto L_0x002c
            goto L_0x0045
        L_0x001b:
            java.lang.Object r3 = getSystemService(r3, r2)
            android.telephony.TelephonyManager r3 = (android.telephony.TelephonyManager) r3
            r0 = 75
            if (r3 == 0) goto L_0x0028
            r2 = 75
            goto L_0x002a
        L_0x0028:
            r2 = 83
        L_0x002a:
            if (r2 == r0) goto L_0x0045
        L_0x002c:
            java.lang.String r3 = "unknown"
            int r0 = a_$P$
            int r0 = r0 + 61
            int r2 = r0 % 128
            B$$W$ = r2
            int r0 = r0 % 2
            r2 = 54
            if (r0 != 0) goto L_0x003f
            r0 = 55
            goto L_0x0041
        L_0x003f:
            r0 = 54
        L_0x0041:
            if (r0 == r2) goto L_0x0044
            int r0 = r1.length
        L_0x0044:
            return r3
        L_0x0045:
            java.lang.String r3 = r3.getNetworkOperatorName()
            int r0 = B$$W$
            int r0 = r0 + 21
            int r1 = r0 % 128
            a_$P$ = r1
            int r0 = r0 % 2
            r1 = 9
            if (r0 == 0) goto L_0x005a
            r0 = 26
            goto L_0x005c
        L_0x005a:
            r0 = 9
        L_0x005c:
            if (r0 == r1) goto L_0x0062
            r0 = 60
            int r0 = r0 / 0
        L_0x0062:
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.razorpay.BaseUtils.getCellularNetworkProviderName(android.content.Context):java.lang.String");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0073, code lost:
        if (r5.isConnected() != false) goto L_0x0080;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x007d, code lost:
        if (r3 != true) goto L_0x0083;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.razorpay.Q$$U_ getDataNetworkType(android.content.Context r5) {
        /*
            java.lang.String r0 = "android.permission.ACCESS_NETWORK_STATE"
            boolean r0 = hasPermission(r5, r0)
            r1 = 69
            if (r0 == 0) goto L_0x0083
            java.lang.String r0 = "connectivity"
            java.lang.Object r5 = getSystemService(r5, r0)
            android.net.ConnectivityManager r5 = (android.net.ConnectivityManager) r5
            if (r5 == 0) goto L_0x0083
            int r0 = a_$P$
            int r0 = r0 + 39
            int r2 = r0 % 128
            B$$W$ = r2
            int r0 = r0 % 2
            r0 = 1
            android.net.NetworkInfo r2 = r5.getNetworkInfo(r0)
            r3 = 0
            if (r2 == 0) goto L_0x003e
            boolean r2 = r2.isConnected()
            if (r2 == 0) goto L_0x002e
            r2 = 0
            goto L_0x002f
        L_0x002e:
            r2 = 1
        L_0x002f:
            if (r2 == r0) goto L_0x003e
            int r5 = B$$W$
            int r5 = r5 + 123
            int r0 = r5 % 128
            a_$P$ = r0
            int r5 = r5 % 2
            com.razorpay.Q$$U_ r5 = com.razorpay.Q$$U_.WIFI
            return r5
        L_0x003e:
            r2 = 7
            android.net.NetworkInfo r2 = r5.getNetworkInfo(r2)
            if (r2 == 0) goto L_0x0048
            r4 = 69
            goto L_0x0049
        L_0x0048:
            r4 = 5
        L_0x0049:
            if (r4 == r1) goto L_0x004c
            goto L_0x0055
        L_0x004c:
            boolean r2 = r2.isConnected()
            if (r2 == 0) goto L_0x0055
            com.razorpay.Q$$U_ r5 = com.razorpay.Q$$U_.BLUETOOTH
            return r5
        L_0x0055:
            android.net.NetworkInfo r5 = r5.getNetworkInfo(r3)
            if (r5 == 0) goto L_0x005d
            r2 = 0
            goto L_0x005e
        L_0x005d:
            r2 = 1
        L_0x005e:
            if (r2 == r0) goto L_0x0083
            int r2 = a_$P$
            int r2 = r2 + 21
            int r4 = r2 % 128
            B$$W$ = r4
            int r2 = r2 % 2
            if (r2 != 0) goto L_0x0076
            boolean r5 = r5.isConnected()
            r0 = 25
            int r0 = r0 / r3
            if (r5 == 0) goto L_0x0083
            goto L_0x0080
        L_0x0076:
            boolean r5 = r5.isConnected()
            if (r5 == 0) goto L_0x007d
            r3 = 1
        L_0x007d:
            if (r3 == r0) goto L_0x0080
            goto L_0x0083
        L_0x0080:
            com.razorpay.Q$$U_ r5 = com.razorpay.Q$$U_.CELLULAR
            return r5
        L_0x0083:
            com.razorpay.Q$$U_ r5 = com.razorpay.Q$$U_.UNKNOWN
            int r0 = B$$W$
            int r0 = r0 + r1
            int r1 = r0 % 128
            a_$P$ = r1
            int r0 = r0 % 2
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.razorpay.BaseUtils.getDataNetworkType(android.content.Context):com.razorpay.Q$$U_");
    }

    public static String getLocale() {
        StringBuilder sb = new StringBuilder();
        sb.append(Locale.getDefault().getLanguage());
        sb.append("-");
        sb.append(Locale.getDefault().getCountry());
        String obj = sb.toString();
        int i = a_$P$ + 69;
        B$$W$ = i % 128;
        if (i % 2 != 0) {
            return obj;
        }
        int i2 = 48 / 0;
        return obj;
    }

    static ArrayList<String> jsonStringArrayToArrayList(JSONArray jSONArray) throws Exception {
        ArrayList<String> arrayList = new ArrayList<>();
        int i = a_$P$ + 27;
        B$$W$ = i % 128;
        int i2 = i % 2;
        int i3 = 0;
        while (true) {
            if ((i3 < jSONArray.length() ? 0 : '_') != 0) {
                return arrayList;
            }
            int i4 = a_$P$ + 71;
            B$$W$ = i4 % 128;
            int i5 = i4 % 2;
            arrayList.add(jSONArray.getString(i3));
            i3++;
        }
    }

    static String getAppBuildType(Context context) {
        int i = a_$P$ + 27;
        B$$W$ = i % 128;
        if (i % 2 == 0) {
            if ((1 != (context.getApplicationInfo().flags ^ 5) ? 26 : '-') != '-') {
                return "development";
            }
        } else {
            if (!((context.getApplicationInfo().flags & 2) == 0)) {
                return "development";
            }
        }
        int i2 = B$$W$ + 47;
        a_$P$ = i2 % 128;
        int i3 = i2 % 2;
        return "production";
    }

    public static CharSequence getWebViewUserAgent(Context context) {
        CharSequence returnUndefinedIfNull = AnalyticsUtil.returnUndefinedIfNull(new WebView(context).getSettings().getUserAgentString());
        int i = a_$P$ + 71;
        B$$W$ = i % 128;
        int i2 = i % 2;
        return returnUndefinedIfNull;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0034, code lost:
        if ((r0 == null ? 19 : '@') != '@') goto L_0x0036;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static boolean isDeviceHaveCorrectTlsVersion() {
        /*
            int r0 = B$$W$
            int r0 = r0 + 111
            int r1 = r0 % 128
            a_$P$ = r1
            int r0 = r0 % 2
            r1 = 0
            if (r0 == 0) goto L_0x001f
            javax.net.ssl.SSLContext r0 = javax.net.ssl.SSLContext.getDefault()     // Catch:{ NoSuchAlgorithmException -> 0x006a }
            javax.net.ssl.SSLParameters r0 = r0.getDefaultSSLParameters()     // Catch:{ NoSuchAlgorithmException -> 0x006a }
            java.lang.String[] r0 = r0.getProtocols()     // Catch:{ NoSuchAlgorithmException -> 0x006a }
            r2 = 18
            int r2 = r2 / r1
            if (r0 != 0) goto L_0x0037
            goto L_0x0036
        L_0x001f:
            javax.net.ssl.SSLContext r0 = javax.net.ssl.SSLContext.getDefault()     // Catch:{ NoSuchAlgorithmException -> 0x006a }
            javax.net.ssl.SSLParameters r0 = r0.getDefaultSSLParameters()     // Catch:{ NoSuchAlgorithmException -> 0x006a }
            java.lang.String[] r0 = r0.getProtocols()     // Catch:{ NoSuchAlgorithmException -> 0x006a }
            r2 = 64
            if (r0 != 0) goto L_0x0032
            r3 = 19
            goto L_0x0034
        L_0x0032:
            r3 = 64
        L_0x0034:
            if (r3 == r2) goto L_0x0037
        L_0x0036:
            return r1
        L_0x0037:
            int r2 = r0.length     // Catch:{ NoSuchAlgorithmException -> 0x006a }
            r3 = 0
        L_0x0039:
            r4 = 49
            if (r3 >= r2) goto L_0x0040
            r5 = 17
            goto L_0x0042
        L_0x0040:
            r5 = 49
        L_0x0042:
            if (r5 == r4) goto L_0x006a
            r4 = r0[r3]     // Catch:{ NoSuchAlgorithmException -> 0x006a }
            java.lang.String r5 = "TLS"
            boolean r5 = r4.startsWith(r5)     // Catch:{ NoSuchAlgorithmException -> 0x006a }
            if (r5 == 0) goto L_0x0067
            java.lang.String r5 = "TLSv1"
            boolean r4 = r4.equalsIgnoreCase(r5)     // Catch:{ NoSuchAlgorithmException -> 0x006a }
            r5 = 1
            if (r4 != 0) goto L_0x0059
            r4 = 0
            goto L_0x005a
        L_0x0059:
            r4 = 1
        L_0x005a:
            if (r4 == r5) goto L_0x0067
            int r0 = a_$P$
            int r0 = r0 + 55
            int r1 = r0 % 128
            B$$W$ = r1
            int r0 = r0 % 2
            return r5
        L_0x0067:
            int r3 = r3 + 1
            goto L_0x0039
        L_0x006a:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.razorpay.BaseUtils.isDeviceHaveCorrectTlsVersion():boolean");
    }

    static void setup() {
        int i = B$$W$ + 105;
        a_$P$ = i % 128;
        int i2 = i % 2;
        AnalyticsUtil.reset();
        int i3 = B$$W$ + 117;
        a_$P$ = i3 % 128;
        if (i3 % 2 != 0) {
            Object[] objArr = null;
            int length = objArr.length;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0047, code lost:
        throw new java.lang.IllegalArgumentException();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0017, code lost:
        if (r5 >= 0) goto L_0x001c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x001a, code lost:
        if (r5 >= 0) goto L_0x001c;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static double round(double r3, int r5) {
        /*
            int r0 = a_$P$
            int r0 = r0 + 89
            int r1 = r0 % 128
            B$$W$ = r1
            int r0 = r0 % 2
            r1 = 81
            if (r0 != 0) goto L_0x0011
            r0 = 96
            goto L_0x0013
        L_0x0011:
            r0 = 81
        L_0x0013:
            r2 = 0
            if (r0 == r1) goto L_0x001a
            int r0 = r2.length
            if (r5 < 0) goto L_0x0042
            goto L_0x001c
        L_0x001a:
            if (r5 < 0) goto L_0x0042
        L_0x001c:
            java.math.BigDecimal r0 = new java.math.BigDecimal
            r0.<init>(r3)
            java.math.RoundingMode r3 = java.math.RoundingMode.HALF_UP
            java.math.BigDecimal r3 = r0.setScale(r5, r3)
            double r3 = r3.doubleValue()
            int r5 = B$$W$
            int r5 = r5 + 123
            int r0 = r5 % 128
            a_$P$ = r0
            int r5 = r5 % 2
            r0 = 41
            if (r5 == 0) goto L_0x003c
            r5 = 8
            goto L_0x003e
        L_0x003c:
            r5 = 41
        L_0x003e:
            if (r5 == r0) goto L_0x0041
            int r5 = r2.length
        L_0x0041:
            return r3
        L_0x0042:
            java.lang.IllegalArgumentException r3 = new java.lang.IllegalArgumentException
            r3.<init>()
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.razorpay.BaseUtils.round(double, int):double");
    }

    public static String nanoTimeToSecondsString(long j, int i) {
        int i2 = a_$P$ + 43;
        B$$W$ = i2 % 128;
        return String.valueOf(round((i2 % 2 == 0 ? '5' : ')') != ')' ? ((double) j) + 1.0E9d : ((double) j) / 1.0E9d, i));
    }

    static boolean isMerchantAppDebuggable(Context context) {
        int i = B$$W$ + 13;
        a_$P$ = i % 128;
        char c = i % 2 != 0 ? ']' : '/';
        int i2 = context.getApplicationInfo().flags;
        if (c != '/') {
            if (((i2 & 3) != 0 ? (char) 25 : 2) != 25) {
                return false;
            }
        } else {
            if (((i2 & 2) != 0 ? (char) 16 : 5) == 5) {
                return false;
            }
        }
        int i3 = a_$P$ + 87;
        B$$W$ = i3 % 128;
        if (i3 % 2 == 0) {
            Object[] objArr = null;
            int length = objArr.length;
        }
        return true;
    }

    static Certificate getX509Certificate(SslCertificate sslCertificate) {
        byte[] byteArray = SslCertificate.saveState(sslCertificate).getByteArray("x509-certificate");
        if ((byteArray == null ? Typography.greater : 'M') != '>') {
            try {
                Certificate generateCertificate = CertificateFactory.getInstance("X.509").generateCertificate(new ByteArrayInputStream(byteArray));
                int i = a_$P$ + 113;
                B$$W$ = i % 128;
                if (i % 2 == 0) {
                    int i2 = 69 / 0;
                }
                return generateCertificate;
            } catch (CertificateException unused) {
                return null;
            }
        } else {
            int i3 = a_$P$ + 7;
            B$$W$ = i3 % 128;
            int i4 = i3 % 2;
            return null;
        }
    }

    /* JADX WARNING: type inference failed for: r0v0, types: [java.security.PublicKey] */
    static PublicKey constructPublicKey(String str) {
        ? r0 = 0;
        try {
            boolean z = false;
            PublicKey generatePublic = KeyFactory.getInstance("RSA").generatePublic(new X509EncodedKeySpec(Base64.decode(str.getBytes(), 0)));
            int i = B$$W$ + 15;
            a_$P$ = i % 128;
            if (i % 2 != 0) {
                z = true;
            }
            if (!z) {
                return generatePublic;
            }
            int length = r0.length;
            return generatePublic;
        } catch (Exception unused) {
            return r0;
        }
    }

    private static Boolean isUserRegisteredOnTruePay(Context context) {
        try {
            boolean z = false;
            if (context.getPackageManager().getComponentEnabledSetting(new ComponentName("com.truecaller", "com.truecaller.truepay.UserRegistered")) == 1) {
                int i = a_$P$ + 55;
                B$$W$ = i % 128;
                if (i % 2 != 0) {
                }
                z = true;
            } else {
                int i2 = a_$P$ + 109;
                B$$W$ = i2 % 128;
                int i3 = i2 % 2;
            }
            return Boolean.valueOf(z);
        } catch (Exception e) {
            e.printStackTrace();
            AnalyticsUtil.reportError(e, "error", e.getMessage());
            return Boolean.FALSE;
        }
    }

    static boolean checkUpiRegisteredApp(Context context, String str) {
        int i = a_$P$ + 63;
        B$$W$ = i % 128;
        int i2 = i % 2;
        char c = 65535;
        int hashCode = str.hashCode();
        if (hashCode != -1974907610) {
            if (hashCode == 1170339061 && str.equals("com.google.android.apps.nbu.paisa.user")) {
                int i3 = a_$P$ + 41;
                B$$W$ = i3 % 128;
                int i4 = i3 % 2;
                int i5 = B$$W$ + 79;
                a_$P$ = i5 % 128;
                int i6 = i5 % 2;
                c = 1;
            }
        } else if (str.equals("com.truecaller")) {
            c = 0;
        }
        if (c == 0) {
            boolean booleanValue = isUserRegisteredOnTruePay(context).booleanValue();
            int i7 = a_$P$ + 103;
            B$$W$ = i7 % 128;
            if ((i7 % 2 == 0 ? 'J' : 26) != 26) {
                Object obj = null;
                super.hashCode();
            }
            return booleanValue;
        } else if (c == 1) {
            return isCompatibleWithGooglePay;
        } else {
            int i8 = a_$P$ + 101;
            B$$W$ = i8 % 128;
            int i9 = i8 % 2;
            return true;
        }
    }

    static HashSet<String> getSetOfPackageNamesSupportingUpi(Context context) {
        List<ResolveInfo> listOfAppsWhichHandleDeepLink = getListOfAppsWhichHandleDeepLink(context, "upi://pay");
        HashSet<String> hashSet = new HashSet<>();
        boolean z = true;
        if (listOfAppsWhichHandleDeepLink != null && listOfAppsWhichHandleDeepLink.size() > 0) {
            Iterator<ResolveInfo> it = listOfAppsWhichHandleDeepLink.iterator();
            while (true) {
                if (!(it.hasNext())) {
                    break;
                }
                try {
                    hashSet.add(it.next().activityInfo.packageName);
                } catch (Exception unused) {
                }
            }
        }
        if (!(hashSet.size() <= 0) && !checkUpiRegisteredApp(context, "com.google.android.apps.nbu.paisa.user")) {
            int i = a_$P$ + 91;
            B$$W$ = i % 128;
            int i2 = i % 2;
            hashSet.remove("com.google.android.apps.nbu.paisa.user");
        }
        if ((hashSet.size() > 0 ? 'X' : 17) != 17) {
            if (!checkUpiRegisteredApp(context, "com.truecaller")) {
                z = false;
            }
            if (!z) {
                int i3 = a_$P$ + 43;
                B$$W$ = i3 % 128;
                if (i3 % 2 == 0) {
                    hashSet.remove("com.truecaller");
                    int i4 = 30 / 0;
                } else {
                    hashSet.remove("com.truecaller");
                }
            }
        }
        int i5 = B$$W$ + 31;
        a_$P$ = i5 % 128;
        if (i5 % 2 != 0) {
            int i6 = 28 / 0;
        }
        return hashSet;
    }

    static List<ResolveInfo> getListOfAppsWhichHandleDeepLink(Context context, String str) {
        Intent intent = new Intent();
        intent.setData(Uri.parse(str));
        List<ResolveInfo> queryIntentActivities = context.getPackageManager().queryIntentActivities(intent, 131072);
        int i = a_$P$ + 19;
        B$$W$ = i % 128;
        int i2 = i % 2;
        return queryIntentActivities;
    }

    static String getAppNameOfResolveInfo(ResolveInfo resolveInfo, Context context) throws Exception {
        int i = a_$P$ + 15;
        B$$W$ = i % 128;
        boolean z = i % 2 == 0;
        String appNameOfPackageName = getAppNameOfPackageName(resolveInfo.activityInfo.packageName, context);
        if (z) {
            Object obj = null;
            super.hashCode();
        }
        return appNameOfPackageName;
    }

    static void startActivityForResult(String str, String str2, Activity activity) {
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.setData(Uri.parse(str));
        if ((str2 != null ? 18 : '`') != '`') {
            int i = B$$W$ + 93;
            a_$P$ = i % 128;
            int i2 = i % 2;
            if ((str2.length() > 0 ? 23 : ';') != ';') {
                intent.setPackage(str2);
            }
        }
        activity.startActivityForResult(intent, 99);
        int i3 = a_$P$ + 87;
        B$$W$ = i3 % 128;
        int i4 = i3 % 2;
    }

    static JSONObject getJSONFromIntentData(Intent intent) {
        Iterator it;
        JSONObject jSONObject = new JSONObject();
        if (intent != null) {
            Bundle extras = intent.getExtras();
            if ((extras != null ? 'Y' : '9') != '9') {
                int i = B$$W$ + 85;
                a_$P$ = i % 128;
                Object[] objArr = null;
                if (i % 2 != 0) {
                    it = extras.keySet().iterator();
                    super.hashCode();
                } else {
                    it = extras.keySet().iterator();
                }
                while (true) {
                    if (!it.hasNext()) {
                        break;
                    }
                    int i2 = a_$P$ + 113;
                    B$$W$ = i2 % 128;
                    if (i2 % 2 == 0) {
                        String str = (String) it.next();
                        try {
                            jSONObject.put(str, extras.get(str));
                            int length = objArr.length;
                        } catch (JSONException unused) {
                        }
                    } else {
                        String str2 = (String) it.next();
                        jSONObject.put(str2, extras.get(str2));
                    }
                    int i3 = B$$W$ + 11;
                    a_$P$ = i3 % 128;
                    int i4 = i3 % 2;
                }
            }
        }
        return jSONObject;
    }

    static String getBase64FromOtherAppsResource(Context context, String str) {
        int i = B$$W$ + 15;
        a_$P$ = i % 128;
        int i2 = i % 2;
        PackageManager packageManager = context.getPackageManager();
        try {
            ApplicationInfo applicationInfo = packageManager.getApplicationInfo(str, 128);
            String base64FromResource = getBase64FromResource(packageManager.getResourcesForApplication(applicationInfo), applicationInfo.icon);
            int i3 = B$$W$ + 87;
            a_$P$ = i3 % 128;
            int i4 = i3 % 2;
            return base64FromResource;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }

    static String getAppNameOfPackageName(String str, Context context) throws Exception {
        int i = a_$P$ + 21;
        B$$W$ = i % 128;
        int i2 = i % 2;
        try {
            PackageManager packageManager = context.getPackageManager();
            ApplicationInfo applicationInfo = packageManager.getApplicationInfo(str, 128);
            int i3 = applicationInfo.labelRes;
            Resources resourcesForApplication = packageManager.getResourcesForApplication(applicationInfo);
            boolean z = false;
            if (i3 == 0) {
                int i4 = B$$W$ + 117;
                a_$P$ = i4 % 128;
                if ((i4 % 2 != 0 ? '@' : '*') == '*') {
                    return applicationInfo.nonLocalizedLabel.toString();
                }
                int i5 = 13 / 0;
                return applicationInfo.nonLocalizedLabel.toString();
            }
            String string = resourcesForApplication.getString(i3);
            int i6 = a_$P$ + 45;
            B$$W$ = i6 % 128;
            if (i6 % 2 != 0) {
                z = true;
            }
            if (z) {
                return string;
            }
            Object obj = null;
            super.hashCode();
            return string;
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    /* JADX WARNING: type inference failed for: r0v2, types: [java.lang.String] */
    static String getBase64FromResource(Resources resources, int i) {
        Bitmap decodeResource = BitmapFactory.decodeResource(resources, i);
        if (decodeResource == null) {
            Drawable drawable = resources.getDrawable(i);
            if ((drawable != null ? '/' : 24) == '/') {
                if (drawable instanceof BitmapDrawable) {
                    int i2 = a_$P$ + 93;
                    B$$W$ = i2 % 128;
                    int i3 = i2 % 2;
                    decodeResource = ((BitmapDrawable) drawable).getBitmap();
                } else {
                    decodeResource = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
                    Canvas canvas = new Canvas(decodeResource);
                    drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
                    drawable.draw(canvas);
                }
            }
        }
        if (decodeResource != null) {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            decodeResource.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
            byte[] byteArray = byteArrayOutputStream.toByteArray();
            StringBuilder sb = new StringBuilder();
            sb.append("data:image/png;base64,");
            sb.append(Base64.encodeToString(byteArray, 2));
            return sb.toString();
        }
        int i4 = B$$W$ + 13;
        a_$P$ = i4 % 128;
        ? r0 = 0;
        if ((i4 % 2 != 0 ? 'b' : 11) != 11) {
            int length = r0.length;
        }
        return r0;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0031, code lost:
        if ((r4 == null) != true) goto L_0x0033;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0034, code lost:
        r4 = B$$W$ + 91;
        a_$P$ = r4 % 128;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0040, code lost:
        if ((r4 % 2) == 0) goto L_0x0045;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0042, code lost:
        r4 = '`';
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0045, code lost:
        r4 = '/';
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0047, code lost:
        if (r4 == '`') goto L_0x004a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0049, code lost:
        return r6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x004a, code lost:
        r4 = 38 / 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x004d, code lost:
        return r6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:6:0x0021, code lost:
        if ((r4 != null ? org.bouncycastle.pqc.math.linearalgebra.Matrix.MATRIX_TYPE_ZERO : 29) != 29) goto L_0x0033;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static java.lang.Object getJsonValue(java.lang.String r4, org.json.JSONObject r5, java.lang.Object r6) {
        /*
            int r0 = B$$W$
            int r0 = r0 + 55
            int r1 = r0 % 128
            a_$P$ = r1
            int r0 = r0 % 2
            java.lang.String r1 = "\\."
            r2 = 1
            r3 = 0
            if (r0 == 0) goto L_0x0024
            java.lang.String[] r4 = r4.split(r1)
            java.lang.Object r4 = getJsonValue((java.lang.String[]) r4, (java.lang.Object) r5, (int) r2)
            r5 = 29
            if (r4 == 0) goto L_0x001f
            r0 = 90
            goto L_0x0021
        L_0x001f:
            r0 = 29
        L_0x0021:
            if (r0 == r5) goto L_0x0034
            goto L_0x0033
        L_0x0024:
            java.lang.String[] r4 = r4.split(r1)
            java.lang.Object r4 = getJsonValue((java.lang.String[]) r4, (java.lang.Object) r5, (int) r3)
            if (r4 == 0) goto L_0x0030
            r5 = 0
            goto L_0x0031
        L_0x0030:
            r5 = 1
        L_0x0031:
            if (r5 == r2) goto L_0x0034
        L_0x0033:
            return r4
        L_0x0034:
            int r4 = B$$W$
            int r4 = r4 + 91
            int r5 = r4 % 128
            a_$P$ = r5
            int r4 = r4 % 2
            r5 = 96
            if (r4 == 0) goto L_0x0045
            r4 = 96
            goto L_0x0047
        L_0x0045:
            r4 = 47
        L_0x0047:
            if (r4 == r5) goto L_0x004a
            return r6
        L_0x004a:
            r4 = 38
            int r4 = r4 / r3
            return r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.razorpay.BaseUtils.getJsonValue(java.lang.String, org.json.JSONObject, java.lang.Object):java.lang.Object");
    }

    private static Object getJsonValue(String[] strArr, Object obj, int i) {
        Object obj2;
        while (true) {
            obj2 = null;
            if ((i == strArr.length ? ')' : '*') != ')') {
                String str = strArr[i];
                if (!(obj instanceof JSONObject)) {
                    if (!(obj instanceof JSONArray)) {
                        break;
                    }
                    JSONArray jSONArray = (JSONArray) obj;
                    if (!TextUtils.isDigitsOnly(str)) {
                        break;
                    }
                    obj = jSONArray.opt(Integer.parseInt(str));
                    i++;
                    int i2 = a_$P$ + 101;
                    B$$W$ = i2 % 128;
                    int i3 = i2 % 2;
                } else {
                    obj = ((JSONObject) obj).opt(str);
                    i++;
                }
            } else {
                int i4 = a_$P$ + 21;
                B$$W$ = i4 % 128;
                if (i4 % 2 == 0) {
                    super.hashCode();
                }
                return obj;
            }
        }
        return obj2;
    }

    static String getRandomString() {
        String bigInteger = new BigInteger(130, new SecureRandom()).toString(32);
        int i = a_$P$ + 55;
        B$$W$ = i % 128;
        if (i % 2 != 0) {
            return bigInteger;
        }
        int i2 = 95 / 0;
        return bigInteger;
    }

    public static String getFileFromInternal(Activity activity, String str, String str2) throws Exception {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(activity.openFileInput(getVersionedAssetName(getLocalVersion(activity, str2).toString(), str)), Key.STRING_CHARSET_NAME));
        StringBuilder sb = new StringBuilder();
        while (true) {
            String readLine = bufferedReader.readLine();
            if (!(readLine != null)) {
                bufferedReader.close();
                return decryptFile(sb.toString());
            }
            int i = a_$P$ + 3;
            B$$W$ = i % 128;
            int i2 = i % 2;
            sb.append(readLine);
            int i3 = a_$P$ + 61;
            B$$W$ = i3 % 128;
            int i4 = i3 % 2;
        }
    }

    static String decryptFile(String str) {
        String str2;
        try {
            B_$q$ b_$q$ = new B_$q$();
            String intern = a_$P$((int[]) null, 127, (String) null, "").intern();
            MessageDigest instance = MessageDigest.getInstance("SHA-256");
            instance.update(intern.getBytes(Key.STRING_CHARSET_NAME));
            byte[] digest = instance.digest();
            StringBuffer stringBuffer = new StringBuffer();
            int length = digest.length;
            int i = 0;
            while (true) {
                if (!(i < length)) {
                    break;
                }
                int i2 = a_$P$ + 69;
                B$$W$ = i2 % 128;
                int i3 = i2 % 2;
                stringBuffer.append(String.format("%02x", new Object[]{Byte.valueOf(digest[i])}));
                i++;
                int i4 = B$$W$ + 1;
                a_$P$ = i4 % 128;
                int i5 = i4 % 2;
            }
            if (32 > stringBuffer.toString().length()) {
                str2 = stringBuffer.toString();
            } else {
                str2 = stringBuffer.toString().substring(0, 32);
            }
            return b_$q$.G__G_(str, str2, a_$P$((int[]) null, 127, (String) null, "").intern());
        } catch (Exception e) {
            AnalyticsUtil.reportError(e, "error", "Unable to decrypt file");
            e.getMessage();
            return null;
        }
    }

    static String getVersionedAssetName(String str, String str2) {
        String replaceAll = str.replaceAll("\\.", "-");
        StringBuilder sb = new StringBuilder();
        sb.append(replaceAll);
        sb.append("-");
        sb.append(str2);
        String obj = sb.toString();
        int i = a_$P$ + 53;
        B$$W$ = i % 128;
        int i2 = i % 2;
        return obj;
    }

    public static String getLocalVersion(Activity activity, String str) {
        int i = a_$P$ + 55;
        B$$W$ = i % 128;
        boolean z = false;
        if (!(i % 2 != 0)) {
            String a_$P$2 = U$_z$.a_$P$(activity, str);
            int i2 = 61 / 0;
            if (a_$P$2 != null) {
                return a_$P$2;
            }
        } else {
            String a_$P$3 = U$_z$.a_$P$(activity, str);
            if (a_$P$3 != null) {
                z = true;
            }
            if (z) {
                return a_$P$3;
            }
        }
        String versionFromJsonString = getVersionFromJsonString("{\n  \"hash\" : \"0accf3a74ffc5382260a1f5f3057d73c\",\n  \"magic_hash\": \"ef90ca87fdffc8b76ff0b45a35518688\"\n}\n", str);
        int i3 = a_$P$ + 7;
        B$$W$ = i3 % 128;
        int i4 = i3 % 2;
        return versionFromJsonString;
    }

    /* JADX WARNING: type inference failed for: r0v0, types: [java.lang.String] */
    public static String getVersionFromJsonString(String str, String str2) {
        String str3;
        ? r0 = 0;
        try {
            JSONObject jSONObject = new JSONObject(str);
            if ((str2.equalsIgnoreCase("otpelf_version") ? ']' : '+') == ']') {
                return jSONObject.getString("hash");
            }
            if (str2.equalsIgnoreCase("magic_version")) {
                int i = a_$P$ + 95;
                B$$W$ = i % 128;
                if ((i % 2 == 0 ? '^' : Typography.quote) != '\"') {
                    str3 = jSONObject.getString("magic_hash");
                    int length = r0.length;
                } else {
                    str3 = jSONObject.getString("magic_hash");
                }
                int i2 = a_$P$ + 119;
                B$$W$ = i2 % 128;
                int i3 = i2 % 2;
                return str3;
            }
            return r0;
        } catch (Exception unused) {
        }
    }

    static void updateLocalVersion(Activity activity, String str, String str2) {
        int i = B$$W$ + 83;
        a_$P$ = i % 128;
        char c = i % 2 != 0 ? '*' : 9;
        U$_z$.G__G_(activity, str, str2);
        if (c == '*') {
            Object[] objArr = null;
            int length = objArr.length;
        }
        int i2 = a_$P$ + 79;
        B$$W$ = i2 % 128;
        if ((i2 % 2 == 0 ? 'a' : '5') == 'a') {
            int i3 = 81 / 0;
        }
    }

    static boolean storeFileInInternal(Activity activity, String str, String str2) {
        FileOutputStream openFileOutput;
        int i = B$$W$ + 73;
        a_$P$ = i % 128;
        if ((i % 2 != 0 ? Typography.less : 'P') != '<') {
            try {
                openFileOutput = activity.openFileOutput(str, 0);
                openFileOutput.write(str2.getBytes());
            } catch (Exception e) {
                AnalyticsUtil.reportError(e, "error", "Error in saving file: ".concat(String.valueOf(str)));
                return false;
            }
        } else {
            openFileOutput = activity.openFileOutput(str, 0);
            openFileOutput.write(str2.getBytes());
        }
        openFileOutput.close();
        return true;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0039, code lost:
        if (isMerchantAppDebuggable(r5) != false) goto L_0x0042;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0040, code lost:
        if (isMerchantAppDebuggable(r5) != false) goto L_0x0042;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static void checkForLatestVersion(android.content.Context r5, int r6) {
        /*
            int r0 = B$$W$
            int r0 = r0 + 23
            int r1 = r0 % 128
            a_$P$ = r1
            int r0 = r0 % 2
            com.razorpay.r$_Y_ r0 = com.razorpay.f$_G$.L__R$()
            boolean r0 = r0.b__J_()
            r1 = 53
            r2 = 21
            if (r0 == 0) goto L_0x001b
            r0 = 53
            goto L_0x001d
        L_0x001b:
            r0 = 21
        L_0x001d:
            r3 = 0
            r4 = 1
            if (r0 == r1) goto L_0x0022
            goto L_0x0062
        L_0x0022:
            int r0 = a_$P$
            int r0 = r0 + 91
            int r1 = r0 % 128
            B$$W$ = r1
            int r0 = r0 % 2
            if (r0 != 0) goto L_0x0030
            r0 = 0
            goto L_0x0031
        L_0x0030:
            r0 = 1
        L_0x0031:
            if (r0 == r4) goto L_0x003c
            boolean r0 = isMerchantAppDebuggable(r5)
            r1 = 3
            int r1 = r1 / r3
            if (r0 == 0) goto L_0x0062
            goto L_0x0042
        L_0x003c:
            boolean r0 = isMerchantAppDebuggable(r5)
            if (r0 == 0) goto L_0x0062
        L_0x0042:
            com.razorpay.r$_Y_ r0 = com.razorpay.f$_G$.L__R$()
            int r0 = r0.E$_j$()
            r1 = 70
            if (r6 >= r0) goto L_0x0050
            r2 = 70
        L_0x0050:
            if (r2 == r1) goto L_0x0053
            goto L_0x0062
        L_0x0053:
            com.razorpay.r$_Y_ r6 = com.razorpay.f$_G$.L__R$()
            java.lang.String r6 = r6.B$$W$()
            android.widget.Toast r5 = android.widget.Toast.makeText(r5, r6, r4)
            r5.show()
        L_0x0062:
            int r5 = a_$P$
            int r5 = r5 + 15
            int r6 = r5 % 128
            B$$W$ = r6
            int r5 = r5 % 2
            if (r5 != 0) goto L_0x006f
            r4 = 0
        L_0x006f:
            if (r4 == 0) goto L_0x0072
            return
        L_0x0072:
            r5 = 52
            int r5 = r5 / r3
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.razorpay.BaseUtils.checkForLatestVersion(android.content.Context, int):void");
    }

    static int dpToPixels(Context context, int i) {
        Resources resources;
        int i2;
        int i3 = a_$P$ + 3;
        B$$W$ = i3 % 128;
        if ((i3 % 2 == 0 ? (char) 3 : 24) != 3) {
            resources = context.getResources();
            i2 = 1;
        } else {
            resources = context.getResources();
            i2 = 0;
        }
        return (int) TypedValue.applyDimension(i2, (float) i, resources.getDisplayMetrics());
    }

    static int getDisplayWidth(Activity activity) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int i = displayMetrics.widthPixels;
        int i2 = B$$W$ + 121;
        a_$P$ = i2 % 128;
        if ((i2 % 2 != 0 ? (char) 6 : 10) != 10) {
            int i3 = 4 / 0;
        }
        return i;
    }

    static int getDisplayHeight(Activity activity) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int i = displayMetrics.heightPixels;
        int i2 = B$$W$ + 57;
        a_$P$ = i2 % 128;
        if (i2 % 2 == 0) {
            return i;
        }
        int i3 = 75 / 0;
        return i;
    }

    static HashMap<String, String> getMapFromJSONObject(JSONObject jSONObject) {
        HashMap<String, String> hashMap = new HashMap<>();
        try {
            Iterator<String> keys = jSONObject.keys();
            int i = B$$W$ + 41;
            a_$P$ = i % 128;
            int i2 = i % 2;
            while (true) {
                if ((keys.hasNext() ? '2' : 'M') != '2') {
                    break;
                }
                int i3 = a_$P$ + 115;
                B$$W$ = i3 % 128;
                if (i3 % 2 == 0) {
                    String next = keys.next();
                    hashMap.put(next, jSONObject.getString(next));
                    Object obj = null;
                    super.hashCode();
                } else {
                    String next2 = keys.next();
                    hashMap.put(next2, jSONObject.getString(next2));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return hashMap;
    }

    static void setCompatibleWithGooglePay(boolean z) {
        int i = a_$P$ + 93;
        B$$W$ = i % 128;
        int i2 = i % 2;
        isCompatibleWithGooglePay = z;
        int i3 = a_$P$ + 29;
        B$$W$ = i3 % 128;
        if ((i3 % 2 == 0 ? ';' : '%') == ';') {
            Object[] objArr = null;
            int length = objArr.length;
        }
    }

    static String makeUrlEncodedPayload(JSONObject jSONObject) throws JSONException {
        String format;
        StringBuilder sb = new StringBuilder();
        Iterator<String> keys = jSONObject.keys();
        while (true) {
            if (!(keys.hasNext())) {
                return sb.deleteCharAt(sb.length() - 1).toString();
            }
            int i = a_$P$ + 13;
            B$$W$ = i % 128;
            if ((i % 2 == 0 ? 10 : 'P') != 'P') {
                String next = keys.next();
                String string = jSONObject.getString(next);
                Object[] objArr = new Object[3];
                objArr[1] = next;
                objArr[0] = Uri.encode(string);
                format = String.format("%s=%s&", objArr);
            } else {
                String next2 = keys.next();
                format = String.format("%s=%s&", new Object[]{next2, Uri.encode(jSONObject.getString(next2))});
            }
            sb.append(format);
        }
    }

    static String installedApps(Context context) {
        StringBuilder sb = new StringBuilder();
        try {
            for (ApplicationInfo next : context.getPackageManager().getInstalledApplications(0)) {
                if ((next.flags & 1) == 0) {
                    int i = a_$P$ + 123;
                    B$$W$ = i % 128;
                    if (!(i % 2 == 0)) {
                        if (sb.length() != 0) {
                        }
                        sb.append(next.packageName);
                    } else {
                        int length = sb.length();
                        Object[] objArr = null;
                        int length2 = objArr.length;
                        if ((length != 0 ? Typography.dollar : 26) != '$') {
                            sb.append(next.packageName);
                        }
                    }
                    sb.append(",");
                    sb.append(next.packageName);
                }
                int i2 = B$$W$ + 55;
                a_$P$ = i2 % 128;
                int i3 = i2 % 2;
            }
            String obj = sb.toString();
            int i4 = a_$P$ + 17;
            B$$W$ = i4 % 128;
            if ((i4 % 2 == 0 ? Matrix.MATRIX_TYPE_ZERO : 'H') != 'H') {
                int i5 = 57 / 0;
            }
            return obj;
        } catch (Throwable unused) {
            return "Apps not available";
        }
    }

    static String getAndroidId(Context context) {
        int i = a_$P$ + 89;
        B$$W$ = i % 128;
        boolean z = false;
        boolean z2 = i % 2 != 0;
        String string = Settings.Secure.getString(context.getContentResolver(), "android_id");
        if (!z2) {
            int i2 = 16 / 0;
        }
        int i3 = a_$P$ + 123;
        B$$W$ = i3 % 128;
        if (i3 % 2 != 0) {
            z = true;
        }
        if (!z) {
            Object[] objArr = null;
            int length = objArr.length;
        }
        return string;
    }

    /* access modifiers changed from: package-private */
    public String getDisplayResolution(Context context) {
        int i = B$$W$ + 33;
        a_$P$ = i % 128;
        int i2 = i % 2;
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        String format = String.format(Locale.ENGLISH, "%dx%dx%d", new Object[]{Integer.valueOf(displayMetrics.widthPixels), Integer.valueOf(displayMetrics.heightPixels), Integer.valueOf(displayMetrics.densityDpi)});
        int i3 = a_$P$ + 39;
        B$$W$ = i3 % 128;
        if (!(i3 % 2 == 0)) {
            return format;
        }
        int i4 = 36 / 0;
        return format;
    }

    /* access modifiers changed from: package-private */
    public String getDeviceUptime() {
        StringBuilder sb = new StringBuilder();
        sb.append(String.valueOf(SystemClock.elapsedRealtime()));
        sb.append("ms");
        String obj = sb.toString();
        int i = a_$P$ + 47;
        B$$W$ = i % 128;
        int i2 = i % 2;
        return obj;
    }

    /* access modifiers changed from: package-private */
    public String getParentAppVersion(Context context) {
        int i = a_$P$ + 15;
        B$$W$ = i % 128;
        int i2 = i % 2;
        try {
            boolean z = false;
            String str = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
            int i3 = a_$P$ + 87;
            B$$W$ = i3 % 128;
            if (i3 % 2 != 0) {
                z = true;
            }
            if (!z) {
                Object[] objArr = null;
                int length = objArr.length;
            }
            return str;
        } catch (PackageManager.NameNotFoundException unused) {
            return "Permission Disabled";
        }
    }

    /* access modifiers changed from: package-private */
    public String getAvailableMemory(Context context) {
        ActivityManager.MemoryInfo memoryInfo = new ActivityManager.MemoryInfo();
        ((ActivityManager) context.getSystemService("activity")).getMemoryInfo(memoryInfo);
        StringBuilder sb = new StringBuilder();
        sb.append(String.valueOf(memoryInfo.availMem / PlaybackStateCompat.ACTION_SET_CAPTIONING_ENABLED));
        sb.append("MB");
        String obj = sb.toString();
        int i = B$$W$ + 51;
        a_$P$ = i % 128;
        if ((i % 2 != 0 ? (char) 6 : 28) != 6) {
            return obj;
        }
        Object obj2 = null;
        super.hashCode();
        return obj;
    }

    /* access modifiers changed from: package-private */
    public String getTotalInternalStorage() {
        StatFs statFs = new StatFs(Environment.getDataDirectory().getPath());
        String valueOf = String.valueOf(statFs.getBlockCountLong() * statFs.getBlockSizeLong());
        int i = B$$W$ + 69;
        a_$P$ = i % 128;
        if (i % 2 == 0) {
            return valueOf;
        }
        Object[] objArr = null;
        int length = objArr.length;
        return valueOf;
    }

    /* access modifiers changed from: package-private */
    public String getBatteryLevel(Context context) {
        Intent registerReceiver = context.registerReceiver((BroadcastReceiver) null, new IntentFilter("android.intent.action.BATTERY_CHANGED"));
        if (registerReceiver == null) {
            return AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN;
        }
        int i = a_$P$ + 45;
        B$$W$ = i % 128;
        return String.valueOf((i % 2 == 0 ? '^' : 6) != 6 ? (((float) registerReceiver.getIntExtra(FirebaseAnalytics.Param.LEVEL, -1)) % ((float) registerReceiver.getIntExtra("scale", -1))) - 100.0f : (((float) registerReceiver.getIntExtra(FirebaseAnalytics.Param.LEVEL, -1)) / ((float) registerReceiver.getIntExtra("scale", -1))) * 100.0f);
    }

    /* access modifiers changed from: package-private */
    public String getAccounts(Context context) {
        if (context.checkCallingOrSelfPermission("android.permission.GET_ACCOUNTS") != 0) {
            return PERMISSION_DISABLED;
        }
        StringBuilder sb = new StringBuilder();
        Account[] accounts = ((AccountManager) context.getSystemService("account")).getAccounts();
        int length = accounts.length;
        int i = 0;
        while (true) {
            if (!(i < length)) {
                return sb.toString();
            }
            int i2 = a_$P$ + 69;
            B$$W$ = i2 % 128;
            int i3 = i2 % 2;
            sb.append(accounts[i].name);
            sb.append(",");
            i++;
            int i4 = a_$P$ + 43;
            B$$W$ = i4 % 128;
            int i5 = i4 % 2;
        }
    }

    /* access modifiers changed from: package-private */
    public String getScreenOffTimeout(Context context) {
        String string = Settings.System.getString(context.getContentResolver(), "screen_off_timeout");
        StringBuilder sb = new StringBuilder();
        sb.append(string);
        sb.append("ms");
        String obj = sb.toString();
        int i = a_$P$ + 19;
        B$$W$ = i % 128;
        if (!(i % 2 == 0)) {
            return obj;
        }
        Object obj2 = null;
        super.hashCode();
        return obj;
    }

    /* access modifiers changed from: package-private */
    public String getScreenBrightnessMode(Context context) {
        int i = a_$P$ + 107;
        B$$W$ = i % 128;
        boolean z = false;
        boolean z2 = i % 2 == 0;
        Object[] objArr = null;
        String string = Settings.System.getString(context.getContentResolver(), "screen_brightness_mode");
        if (z2) {
            super.hashCode();
        }
        int i2 = a_$P$ + 97;
        B$$W$ = i2 % 128;
        if (i2 % 2 != 0) {
            z = true;
        }
        if (!z) {
            int length = objArr.length;
        }
        return string;
    }

    /* access modifiers changed from: package-private */
    public String getScreenBrightness(Context context) {
        int i = B$$W$ + 31;
        a_$P$ = i % 128;
        boolean z = i % 2 != 0;
        String string = Settings.System.getString(context.getContentResolver(), "screen_brightness");
        if (z) {
            int i2 = 19 / 0;
        }
        return string;
    }

    /* access modifiers changed from: package-private */
    public String getWallpaperID(Context context) {
        int i = B$$W$ + 53;
        a_$P$ = i % 128;
        int i2 = i % 2;
        boolean z = false;
        if (Build.VERSION.SDK_INT < 24) {
            return PERMISSION_DISABLED;
        }
        WallpaperManager instance = WallpaperManager.getInstance(context);
        int wallpaperId = instance.getWallpaperId(1);
        String format = String.format(Locale.ENGLISH, "%d-%d", new Object[]{Integer.valueOf(instance.getWallpaperId(2)), Integer.valueOf(wallpaperId)});
        int i3 = a_$P$ + 73;
        B$$W$ = i3 % 128;
        if (i3 % 2 == 0) {
            z = true;
        }
        if (!z) {
            return format;
        }
        Object obj = null;
        super.hashCode();
        return format;
    }

    /* access modifiers changed from: package-private */
    public String getRingtoneHash(Context context) {
        int i = B$$W$ + 85;
        a_$P$ = i % 128;
        int i2 = i % 2;
        String MD5Hash = MD5Hash(RingtoneManager.getRingtone(context, RingtoneManager.getActualDefaultRingtoneUri(context, 1)).getTitle(context));
        int i3 = a_$P$ + 73;
        B$$W$ = i3 % 128;
        if ((i3 % 2 == 0 ? 28 : '_') != '_') {
            int i4 = 6 / 0;
        }
        return MD5Hash;
    }

    public static String MD5Hash(String str) {
        try {
            String bigInteger = new BigInteger(1, MessageDigest.getInstance("MD5").digest(str.getBytes())).toString(16);
            while (true) {
                if (bigInteger.length() < 32) {
                    int i = B$$W$ + 57;
                    a_$P$ = i % 128;
                    if ((i % 2 != 0 ? '^' : 7) != 7) {
                        bigInteger = AppEventsConstants.EVENT_PARAM_VALUE_NO.concat(String.valueOf(bigInteger));
                        Object obj = null;
                        super.hashCode();
                    } else {
                        bigInteger = AppEventsConstants.EVENT_PARAM_VALUE_NO.concat(String.valueOf(bigInteger));
                    }
                } else {
                    int i2 = a_$P$ + 125;
                    B$$W$ = i2 % 128;
                    int i3 = i2 % 2;
                    return bigInteger;
                }
            }
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }

    /* access modifiers changed from: package-private */
    public String getSystemFontSize(Context context) {
        int i = a_$P$ + 59;
        B$$W$ = i % 128;
        boolean z = i % 2 == 0;
        String valueOf = String.valueOf(context.getResources().getConfiguration().fontScale);
        if (z) {
            Object[] objArr = null;
            int length = objArr.length;
        }
        return valueOf;
    }

    private boolean isMocked(Context context, Location location) {
        int i = B$$W$ + 25;
        a_$P$ = i % 128;
        int i2 = i % 2;
        boolean isFromMockProvider = location.isFromMockProvider();
        int i3 = a_$P$ + 67;
        B$$W$ = i3 % 128;
        int i4 = i3 % 2;
        return isFromMockProvider;
    }

    static boolean isNetworkRoaming(Context context) {
        boolean z;
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
        boolean z2 = false;
        if (telephonyManager == null) {
            return false;
        }
        int i = a_$P$ + 121;
        B$$W$ = i % 128;
        Object[] objArr = null;
        if (i % 2 == 0) {
            z = telephonyManager.isNetworkRoaming();
            int length = objArr.length;
        } else {
            z = telephonyManager.isNetworkRoaming();
        }
        int i2 = B$$W$ + 69;
        a_$P$ = i2 % 128;
        if (i2 % 2 == 0) {
            z2 = true;
        }
        if (!z2) {
            super.hashCode();
        }
        return z;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x002e, code lost:
        r4 = PERMISSION_DISABLED;
        r0 = B$$W$ + 5;
        a_$P$ = r0 % 128;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x003a, code lost:
        if ((r0 % 2) == 0) goto L_0x003d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x003d, code lost:
        r1 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x003e, code lost:
        if (r1 == false) goto L_0x0042;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0040, code lost:
        r0 = null;
        r0 = r0.length;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0042, code lost:
        return r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:3:0x0019, code lost:
        if (r4 != null) goto L_0x0029;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0027, code lost:
        if ((r4 != null) != false) goto L_0x0029;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static java.lang.String getCarrierOperatorName(android.content.Context r4) {
        /*
            int r0 = B$$W$
            int r0 = r0 + 83
            int r1 = r0 % 128
            a_$P$ = r1
            int r0 = r0 % 2
            r1 = 1
            java.lang.String r2 = "phone"
            r3 = 0
            if (r0 == 0) goto L_0x001c
            java.lang.Object r4 = r4.getSystemService(r2)
            android.telephony.TelephonyManager r4 = (android.telephony.TelephonyManager) r4
            r0 = 87
            int r0 = r0 / r3
            if (r4 == 0) goto L_0x002e
            goto L_0x0029
        L_0x001c:
            java.lang.Object r4 = r4.getSystemService(r2)
            android.telephony.TelephonyManager r4 = (android.telephony.TelephonyManager) r4
            if (r4 == 0) goto L_0x0026
            r0 = 1
            goto L_0x0027
        L_0x0026:
            r0 = 0
        L_0x0027:
            if (r0 == 0) goto L_0x002e
        L_0x0029:
            java.lang.String r4 = r4.getNetworkOperatorName()
            return r4
        L_0x002e:
            java.lang.String r4 = PERMISSION_DISABLED
            int r0 = B$$W$
            int r0 = r0 + 5
            int r2 = r0 % 128
            a_$P$ = r2
            int r0 = r0 % 2
            if (r0 == 0) goto L_0x003d
            goto L_0x003e
        L_0x003d:
            r1 = 0
        L_0x003e:
            if (r1 == 0) goto L_0x0042
            r0 = 0
            int r0 = r0.length
        L_0x0042:
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.razorpay.BaseUtils.getCarrierOperatorName(android.content.Context):java.lang.String");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0052, code lost:
        if ((android.os.Build.VERSION.SDK_INT <= 28 ? 12 : '`') != '`') goto L_0x006c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x006a, code lost:
        if (android.os.Build.VERSION.SDK_INT <= 74) goto L_0x006c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x008a, code lost:
        r0.put("sim_serial_number", PERMISSION_DISABLED);
        r0.put("build_unique_id", java.util.UUID.randomUUID().toString());
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static java.util.Map<java.lang.String, java.lang.String> getDeviceAttributes(android.content.Context r9) {
        /*
            java.util.HashMap r0 = new java.util.HashMap
            r0.<init>()
            java.lang.String r1 = "android.permission.READ_PHONE_STATE"
            int r1 = r9.checkCallingOrSelfPermission(r1)
            r2 = 35
            if (r1 != 0) goto L_0x0012
            r1 = 22
            goto L_0x0014
        L_0x0012:
            r1 = 35
        L_0x0014:
            java.lang.String r3 = "sim_serial_number"
            java.lang.String r4 = "device_id"
            if (r1 == r2) goto L_0x009d
            int r1 = a_$P$
            int r1 = r1 + 75
            int r2 = r1 % 128
            B$$W$ = r2
            r2 = 2
            int r1 = r1 % r2
            r5 = 37
            if (r1 != 0) goto L_0x002b
            r1 = 37
            goto L_0x002d
        L_0x002b:
            r1 = 85
        L_0x002d:
            r6 = 0
            java.lang.String r7 = "advertising_id"
            java.lang.String r8 = "phone"
            if (r1 == r5) goto L_0x0055
            java.lang.Object r1 = r9.getSystemService(r8)
            android.telephony.TelephonyManager r1 = (android.telephony.TelephonyManager) r1
            android.content.SharedPreferences r9 = com.razorpay.U$_z$.a_$P$(r9)
            java.lang.String r9 = r9.getString(r7, r6)
            r0.put(r4, r9)
            int r9 = android.os.Build.VERSION.SDK_INT
            r4 = 28
            r5 = 96
            if (r9 > r4) goto L_0x0050
            r9 = 12
            goto L_0x0052
        L_0x0050:
            r9 = 96
        L_0x0052:
            if (r9 == r5) goto L_0x008a
            goto L_0x006c
        L_0x0055:
            java.lang.Object r1 = r9.getSystemService(r8)
            android.telephony.TelephonyManager r1 = (android.telephony.TelephonyManager) r1
            android.content.SharedPreferences r9 = com.razorpay.U$_z$.a_$P$(r9)
            java.lang.String r9 = r9.getString(r7, r6)
            r0.put(r4, r9)
            int r9 = android.os.Build.VERSION.SDK_INT
            r4 = 74
            if (r9 > r4) goto L_0x008a
        L_0x006c:
            r9 = 55
            if (r1 == 0) goto L_0x0073
            r4 = 55
            goto L_0x0074
        L_0x0073:
            r4 = 2
        L_0x0074:
            if (r4 == r9) goto L_0x0079
            java.lang.String r9 = "null"
            goto L_0x0086
        L_0x0079:
            int r9 = a_$P$
            int r9 = r9 + 11
            int r4 = r9 % 128
            B$$W$ = r4
            int r9 = r9 % r2
            java.lang.String r9 = r1.getSimSerialNumber()
        L_0x0086:
            r0.put(r3, r9)
            goto L_0x00a7
        L_0x008a:
            java.lang.String r9 = PERMISSION_DISABLED
            r0.put(r3, r9)
            java.util.UUID r9 = java.util.UUID.randomUUID()
            java.lang.String r9 = r9.toString()
            java.lang.String r1 = "build_unique_id"
            r0.put(r1, r9)
            goto L_0x00a7
        L_0x009d:
            java.lang.String r9 = PERMISSION_DISABLED
            r0.put(r4, r9)
            java.lang.String r9 = PERMISSION_DISABLED
            r0.put(r3, r9)
        L_0x00a7:
            java.lang.String r9 = android.os.Build.MANUFACTURER
            java.lang.String r1 = "device_manufacturer"
            r0.put(r1, r9)
            java.lang.String r9 = android.os.Build.MODEL
            java.lang.String r1 = "device_model"
            r0.put(r1, r9)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.razorpay.BaseUtils.getDeviceAttributes(android.content.Context):java.util.Map");
    }

    static String getWifiSSID(Context context) {
        int i = a_$P$ + 111;
        B$$W$ = i % 128;
        int i2 = i % 2;
        if ((context.checkCallingOrSelfPermission("android.permission.ACCESS_WIFI_STATE") == 0 ? (char) 23 : 31) == 31) {
            return PERMISSION_DISABLED;
        }
        String ssid = ((WifiManager) context.getApplicationContext().getSystemService("wifi")).getConnectionInfo().getSSID();
        int i3 = a_$P$ + 89;
        B$$W$ = i3 % 128;
        if ((i3 % 2 == 0 ? '@' : 'O') != 'O') {
            int i4 = 44 / 0;
        }
        return ssid;
    }

    static String buildSerial() {
        int i = B$$W$ + 31;
        a_$P$ = i % 128;
        int i2 = i % 2;
        String str = Build.SERIAL;
        int i3 = B$$W$ + 9;
        a_$P$ = i3 % 128;
        int i4 = i3 % 2;
        return str;
    }

    static void fetchIP(final RzpJSONCallback rzpJSONCallback) {
        new Thread(new Runnable() {
            /* JADX WARNING: Missing exception handler attribute for start block: B:27:0x0060 */
            /* JADX WARNING: Removed duplicated region for block: B:24:0x005c A[SYNTHETIC, Splitter:B:24:0x005c] */
            /* JADX WARNING: Removed duplicated region for block: B:30:0x0072 A[SYNTHETIC, Splitter:B:30:0x0072] */
            /* JADX WARNING: Removed duplicated region for block: B:33:0x0078 A[Catch:{ Exception -> 0x007c }] */
            /* JADX WARNING: Removed duplicated region for block: B:40:? A[Catch:{ Exception -> 0x007c }, RETURN, SYNTHETIC] */
            /* JADX WARNING: Removed duplicated region for block: B:41:? A[Catch:{ Exception -> 0x007c }, RETURN, SYNTHETIC] */
            /* JADX WARNING: Unknown top exception splitter block from list: {B:27:0x0060=Splitter:B:27:0x0060, B:21:0x0053=Splitter:B:21:0x0053} */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final void run() {
                /*
                    r6 = this;
                    java.lang.String r0 = "warning"
                    java.lang.String r1 = "error"
                    r2 = 0
                    java.net.URL r3 = new java.net.URL     // Catch:{ SocketTimeoutException -> 0x0060, Exception -> 0x0052 }
                    java.lang.String r4 = "https://approvals-api.getsimpl.com/my-ip"
                    r3.<init>(r4)     // Catch:{ SocketTimeoutException -> 0x0060, Exception -> 0x0052 }
                    java.net.URLConnection r3 = r3.openConnection()     // Catch:{ SocketTimeoutException -> 0x0060, Exception -> 0x0052 }
                    javax.net.ssl.HttpsURLConnection r3 = (javax.net.ssl.HttpsURLConnection) r3     // Catch:{ SocketTimeoutException -> 0x0060, Exception -> 0x0052 }
                    java.lang.String r2 = "GET"
                    r3.setRequestMethod(r2)     // Catch:{ SocketTimeoutException -> 0x004e, Exception -> 0x004b, all -> 0x0048 }
                    r2 = 150(0x96, float:2.1E-43)
                    r3.setReadTimeout(r2)     // Catch:{ SocketTimeoutException -> 0x004e, Exception -> 0x004b, all -> 0x0048 }
                    r2 = 250(0xfa, float:3.5E-43)
                    r3.setConnectTimeout(r2)     // Catch:{ SocketTimeoutException -> 0x004e, Exception -> 0x004b, all -> 0x0048 }
                    int r2 = r3.getResponseCode()     // Catch:{ SocketTimeoutException -> 0x004e, Exception -> 0x004b, all -> 0x0048 }
                    r4 = 200(0xc8, float:2.8E-43)
                    if (r2 != r4) goto L_0x0034
                    org.json.JSONObject r2 = com.razorpay.BaseUtils.access$000(r3)     // Catch:{ SocketTimeoutException -> 0x004e, Exception -> 0x004b, all -> 0x0048 }
                    com.razorpay.RzpJSONCallback r4 = r2     // Catch:{ SocketTimeoutException -> 0x004e, Exception -> 0x004b, all -> 0x0048 }
                    r4.onResponse(r2)     // Catch:{ SocketTimeoutException -> 0x004e, Exception -> 0x004b, all -> 0x0048 }
                    goto L_0x0042
                L_0x0034:
                    com.razorpay.RzpJSONCallback r2 = r2     // Catch:{ SocketTimeoutException -> 0x004e, Exception -> 0x004b, all -> 0x0048 }
                    org.json.JSONObject r4 = new org.json.JSONObject     // Catch:{ SocketTimeoutException -> 0x004e, Exception -> 0x004b, all -> 0x0048 }
                    r4.<init>()     // Catch:{ SocketTimeoutException -> 0x004e, Exception -> 0x004b, all -> 0x0048 }
                    org.json.JSONObject r4 = r4.put(r1, r1)     // Catch:{ SocketTimeoutException -> 0x004e, Exception -> 0x004b, all -> 0x0048 }
                    r2.onResponse(r4)     // Catch:{ SocketTimeoutException -> 0x004e, Exception -> 0x004b, all -> 0x0048 }
                L_0x0042:
                    if (r3 == 0) goto L_0x0075
                    r3.disconnect()     // Catch:{ Exception -> 0x007c }
                    return
                L_0x0048:
                    r1 = move-exception
                    r2 = r3
                    goto L_0x0076
                L_0x004b:
                    r1 = move-exception
                    r2 = r3
                    goto L_0x0053
                L_0x004e:
                    r2 = r3
                    goto L_0x0060
                L_0x0050:
                    r1 = move-exception
                    goto L_0x0076
                L_0x0052:
                    r1 = move-exception
                L_0x0053:
                    java.lang.String r3 = r1.getMessage()     // Catch:{ all -> 0x0050 }
                    com.razorpay.AnalyticsUtil.reportError(r1, r0, r3)     // Catch:{ all -> 0x0050 }
                    if (r2 == 0) goto L_0x0075
                    r2.disconnect()     // Catch:{ Exception -> 0x007c }
                    return
                L_0x0060:
                    com.razorpay.RzpJSONCallback r3 = r2     // Catch:{ all -> 0x0050 }
                    org.json.JSONObject r4 = new org.json.JSONObject     // Catch:{ all -> 0x0050 }
                    r4.<init>()     // Catch:{ all -> 0x0050 }
                    java.lang.String r5 = "timeout"
                    org.json.JSONObject r1 = r4.put(r1, r5)     // Catch:{ all -> 0x0050 }
                    r3.onResponse(r1)     // Catch:{ all -> 0x0050 }
                    if (r2 == 0) goto L_0x0075
                    r2.disconnect()     // Catch:{ Exception -> 0x007c }
                L_0x0075:
                    return
                L_0x0076:
                    if (r2 == 0) goto L_0x007b
                    r2.disconnect()     // Catch:{ Exception -> 0x007c }
                L_0x007b:
                    throw r1     // Catch:{ Exception -> 0x007c }
                L_0x007c:
                    r1 = move-exception
                    java.lang.String r2 = r1.getMessage()
                    com.razorpay.AnalyticsUtil.reportError(r1, r0, r2)
                    return
                */
                throw new UnsupportedOperationException("Method not decompiled: com.razorpay.BaseUtils.AnonymousClass5.run():void");
            }
        }).start();
        int i = a_$P$ + 31;
        B$$W$ = i % 128;
        int i2 = i % 2;
    }

    static void getDeviceParamValues(final Context context, final RzpJSONCallback rzpJSONCallback) {
        final JSONObject jSONObject = new JSONObject();
        try {
            fetchIP(new RzpJSONCallback() {
                public final void onResponse(JSONObject jSONObject) {
                    try {
                        if (jSONObject.getString("ip") != null) {
                            BaseUtils.access$102(jSONObject.getString("ip"));
                        }
                    } catch (JSONException unused) {
                    }
                }
            });
            AdvertisingIdUtil.R$$r_(context, new AdvertisingIdUtil.d__1_() {
                public final void R$$r_(String str) {
                    try {
                        jSONObject.put("advertising_id", str);
                        jSONObject.put("is_roming", BaseUtils.isNetworkRoaming(context));
                        jSONObject.put("carrier_network", BaseUtils.getCarrierOperatorName(context));
                        jSONObject.put("carrier_Id", "null");
                        Map<String, String> deviceAttributes = BaseUtils.getDeviceAttributes(context);
                        jSONObject.put("device_Id", deviceAttributes.get("device_id"));
                        jSONObject.put("device_manufacturer", deviceAttributes.get("device_manufacturer"));
                        jSONObject.put("device_model", deviceAttributes.get("device_model"));
                        jSONObject.put("serial_number", BaseUtils.buildSerial());
                        jSONObject.put("ip_address", BaseUtils.access$100());
                        jSONObject.put("wifi_ssid", BaseUtils.getWifiSSID(context));
                        jSONObject.put("android_id", BaseUtils.getAndroidId(context));
                        jSONObject.put("safety_net basic_integrity", ServerProtocol.DIALOG_RETURN_SCOPES_TRUE);
                        jSONObject.put("safety_net_cts_profile_match", "null");
                        rzpJSONCallback.onResponse(jSONObject);
                    } catch (JSONException e) {
                        AnalyticsUtil.reportError(e, "warning", e.getMessage());
                    }
                }
            });
            int i = B$$W$ + 21;
            a_$P$ = i % 128;
            int i2 = i % 2;
        } catch (Exception e) {
            AnalyticsUtil.reportError(e, "warning", e.getMessage());
        }
    }

    private static JSONObject getResponseJson(HttpsURLConnection httpsURLConnection) throws IOException, JSONException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(httpsURLConnection.getInputStream()));
        StringBuilder sb = new StringBuilder();
        while (true) {
            String readLine = bufferedReader.readLine();
            if (!(readLine != null)) {
                bufferedReader.close();
                JSONObject jSONObject = new JSONObject(sb.toString());
                int i = a_$P$ + 1;
                B$$W$ = i % 128;
                int i2 = i % 2;
                return jSONObject;
            }
            int i3 = B$$W$ + 123;
            a_$P$ = i3 % 128;
            int i4 = i3 % 2;
            sb.append(readLine);
            int i5 = B$$W$ + 85;
            a_$P$ = i5 % 128;
            int i6 = i5 % 2;
        }
    }

    static {
        int i = B$$W$ + 53;
        a_$P$ = i % 128;
        int i2 = i % 2;
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r9v1, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v1, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v6, resolved type: char[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r9v9, resolved type: byte[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r9v10, resolved type: byte[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r9v11, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v7, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r9v12, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r9v13, resolved type: java.lang.String} */
    /* JADX WARNING: Failed to insert additional move for type inference */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static java.lang.String a_$P$(int[] r6, int r7, java.lang.String r8, java.lang.String r9) {
        /*
            r0 = 2
            if (r9 == 0) goto L_0x001d
            int r1 = B$$W$
            int r1 = r1 + 7
            int r2 = r1 % 128
            a_$P$ = r2
            int r1 = r1 % r0
            java.lang.String r2 = "ISO-8859-1"
            if (r1 == 0) goto L_0x0019
            byte[] r9 = r9.getBytes(r2)
            r1 = 0
            super.hashCode()
            goto L_0x001d
        L_0x0019:
            byte[] r9 = r9.getBytes(r2)
        L_0x001d:
            byte[] r9 = (byte[]) r9
            r1 = 72
            if (r8 == 0) goto L_0x0026
            r2 = 37
            goto L_0x0028
        L_0x0026:
            r2 = 72
        L_0x0028:
            if (r2 == r1) goto L_0x002e
            char[] r8 = r8.toCharArray()
        L_0x002e:
            char[] r8 = (char[]) r8
            char[] r1 = G__G_
            int r2 = R$$r_
            boolean r3 = d__1_
            r4 = 0
            r5 = 1
            if (r3 == 0) goto L_0x003c
            r3 = 0
            goto L_0x003d
        L_0x003c:
            r3 = 1
        L_0x003d:
            if (r3 == r5) goto L_0x0086
            int r6 = B$$W$
            int r6 = r6 + 35
            int r8 = r6 % 128
            a_$P$ = r8
            int r6 = r6 % r0
            int r6 = r9.length
            char[] r8 = new char[r6]
        L_0x004b:
            if (r4 >= r6) goto L_0x0080
            int r3 = B$$W$
            int r3 = r3 + 113
            int r5 = r3 % 128
            a_$P$ = r5
            int r3 = r3 % r0
            r5 = 40
            if (r3 == 0) goto L_0x005d
            r3 = 40
            goto L_0x005f
        L_0x005d:
            r3 = 11
        L_0x005f:
            if (r3 == r5) goto L_0x0070
            int r3 = r6 + -1
            int r3 = r3 - r4
            byte r3 = r9[r3]
            int r3 = r3 + r7
            char r3 = r1[r3]
            int r3 = r3 - r2
            char r3 = (char) r3
            r8[r4] = r3
            int r4 = r4 + 1
            goto L_0x004b
        L_0x0070:
            int r3 = r6 + 0
            int r3 = r3 >> r4
            byte r3 = r9[r3]
            int r3 = r3 * r7
            char r3 = r1[r3]
            int r3 = r3 << r2
            char r3 = (char) r3
            r8[r4] = r3
            int r4 = r4 + 13
            goto L_0x004b
        L_0x0080:
            java.lang.String r6 = new java.lang.String
            r6.<init>(r8)
            return r6
        L_0x0086:
            boolean r9 = Q_$2$
            if (r9 == 0) goto L_0x008c
            r9 = 0
            goto L_0x008d
        L_0x008c:
            r9 = 1
        L_0x008d:
            if (r9 == r5) goto L_0x00bb
            int r6 = r8.length
            char[] r9 = new char[r6]
        L_0x0092:
            r3 = 80
            if (r4 >= r6) goto L_0x0099
            r5 = 65
            goto L_0x009b
        L_0x0099:
            r5 = 80
        L_0x009b:
            if (r5 == r3) goto L_0x00b5
            int r3 = a_$P$
            int r3 = r3 + 53
            int r5 = r3 % 128
            B$$W$ = r5
            int r3 = r3 % r0
            int r3 = r6 + -1
            int r3 = r3 - r4
            char r3 = r8[r3]
            int r3 = r3 - r7
            char r3 = r1[r3]
            int r3 = r3 - r2
            char r3 = (char) r3
            r9[r4] = r3
            int r4 = r4 + 1
            goto L_0x0092
        L_0x00b5:
            java.lang.String r6 = new java.lang.String
            r6.<init>(r9)
            return r6
        L_0x00bb:
            int r8 = r6.length
            char[] r9 = new char[r8]
        L_0x00be:
            r3 = 6
            if (r4 >= r8) goto L_0x00c3
            r5 = 6
            goto L_0x00c5
        L_0x00c3:
            r5 = 52
        L_0x00c5:
            if (r5 == r3) goto L_0x00cd
            java.lang.String r6 = new java.lang.String
            r6.<init>(r9)
            return r6
        L_0x00cd:
            int r3 = a_$P$
            int r3 = r3 + 97
            int r5 = r3 % 128
            B$$W$ = r5
            int r3 = r3 % r0
            r5 = 45
            if (r3 != 0) goto L_0x00dd
            r3 = 45
            goto L_0x00de
        L_0x00dd:
            r3 = 2
        L_0x00de:
            if (r3 == r5) goto L_0x00ef
            int r3 = r8 + -1
            int r3 = r3 - r4
            r3 = r6[r3]
            int r3 = r3 - r7
            char r3 = r1[r3]
            int r3 = r3 - r2
            char r3 = (char) r3
            r9[r4] = r3
            int r4 = r4 + 1
            goto L_0x00be
        L_0x00ef:
            int r3 = r8 >> 0
            int r3 = r3 + r4
            r3 = r6[r3]
            int r3 = r3 % r7
            char r3 = r1[r3]
            int r3 = r3 - r2
            char r3 = (char) r3
            r9[r4] = r3
            int r4 = r4 + 91
            goto L_0x00be
        */
        throw new UnsupportedOperationException("Method not decompiled: com.razorpay.BaseUtils.a_$P$(int[], int, java.lang.String, java.lang.String):java.lang.String");
    }
}
