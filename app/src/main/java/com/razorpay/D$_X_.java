package com.razorpay;

import android.app.Activity;
import android.graphics.Rect;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.FrameLayout;

final class D$_X_ implements ViewTreeObserver.OnGlobalLayoutListener {
    private int G__G_;
    private FrameLayout.LayoutParams Q_$2$;
    private View a_$P$;
    private int d__1_;

    static void a_$P$(Activity activity) {
        new D$_X_(activity);
    }

    private D$_X_(Activity activity) {
        View childAt = ((FrameLayout) activity.findViewById(16908290)).getChildAt(0);
        this.a_$P$ = childAt;
        childAt.getViewTreeObserver().addOnGlobalLayoutListener(this);
        FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) this.a_$P$.getLayoutParams();
        this.Q_$2$ = layoutParams;
        this.d__1_ = layoutParams.height;
    }

    public final void onGlobalLayout() {
        Rect rect = new Rect();
        this.a_$P$.getWindowVisibleDisplayFrame(rect);
        int i = rect.bottom - rect.top;
        if (i != this.G__G_) {
            int height = this.a_$P$.getRootView().getHeight();
            if (height - i > height / 4) {
                this.Q_$2$.height = i;
            } else {
                this.Q_$2$.height = this.d__1_;
            }
            this.a_$P$.requestLayout();
            this.G__G_ = i;
        }
    }
}
