package com.razorpay;

import android.util.Base64;
import com.bumptech.glide.load.Key;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

final class B_$q$ {
    private Cipher Q_$2$ = Cipher.getInstance("AES/CBC/PKCS5Padding");
    private byte[] R$$r_ = new byte[16];
    private byte[] a_$P$ = new byte[32];

    B_$q$() throws NoSuchAlgorithmException, NoSuchPaddingException {
    }

    private String a_$P$(String str, String str2, B_$q$$R$$r_ b_$q$$R$$r_, String str3) throws UnsupportedEncodingException, InvalidKeyException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException {
        String str4;
        int length = str2.getBytes(Key.STRING_CHARSET_NAME).length;
        int length2 = str2.getBytes(Key.STRING_CHARSET_NAME).length;
        byte[] bArr = this.a_$P$;
        if (length2 > bArr.length) {
            length = bArr.length;
        }
        int length3 = str3.getBytes(Key.STRING_CHARSET_NAME).length;
        int length4 = str3.getBytes(Key.STRING_CHARSET_NAME).length;
        byte[] bArr2 = this.R$$r_;
        if (length4 > bArr2.length) {
            length3 = bArr2.length;
        }
        System.arraycopy(str2.getBytes(Key.STRING_CHARSET_NAME), 0, this.a_$P$, 0, length);
        System.arraycopy(str3.getBytes(Key.STRING_CHARSET_NAME), 0, this.R$$r_, 0, length3);
        SecretKeySpec secretKeySpec = new SecretKeySpec(this.a_$P$, "AES");
        IvParameterSpec ivParameterSpec = new IvParameterSpec(this.R$$r_);
        if (b_$q$$R$$r_.equals(B_$q$$R$$r_.ENCRYPT)) {
            this.Q_$2$.init(1, secretKeySpec, ivParameterSpec);
            str4 = Base64.encodeToString(this.Q_$2$.doFinal(str.getBytes(Key.STRING_CHARSET_NAME)), 2);
        } else {
            str4 = "";
        }
        if (!b_$q$$R$$r_.equals(B_$q$$R$$r_.DECRYPT)) {
            return str4;
        }
        this.Q_$2$.init(2, secretKeySpec, ivParameterSpec);
        return new String(this.Q_$2$.doFinal(Base64.decode(str.getBytes(), 2)));
    }

    /* access modifiers changed from: package-private */
    public final String d__1_(String str, String str2, String str3) throws InvalidKeyException, UnsupportedEncodingException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException {
        return a_$P$(str, str2, B_$q$$R$$r_.ENCRYPT, str3);
    }

    /* access modifiers changed from: package-private */
    public final String G__G_(String str, String str2, String str3) throws InvalidKeyException, UnsupportedEncodingException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException {
        return a_$P$(str, str2, B_$q$$R$$r_.DECRYPT, str3);
    }
}
