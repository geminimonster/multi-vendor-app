package com.razorpay;

import org.json.JSONObject;

public class AnalyticsProperty {
    AnalyticsProperty$Q_$2$ scope;
    Object value;

    AnalyticsProperty(int i, AnalyticsProperty$Q_$2$ analyticsProperty$Q_$2$) {
        this.value = Integer.valueOf(i);
        this.scope = analyticsProperty$Q_$2$;
    }

    AnalyticsProperty(long j, AnalyticsProperty$Q_$2$ analyticsProperty$Q_$2$) {
        this.value = Long.valueOf(j);
        this.scope = analyticsProperty$Q_$2$;
    }

    AnalyticsProperty(String str, AnalyticsProperty$Q_$2$ analyticsProperty$Q_$2$) {
        this.value = str;
        this.scope = analyticsProperty$Q_$2$;
    }

    AnalyticsProperty(boolean z, AnalyticsProperty$Q_$2$ analyticsProperty$Q_$2$) {
        this.value = Boolean.valueOf(z);
        this.scope = analyticsProperty$Q_$2$;
    }

    AnalyticsProperty(JSONObject jSONObject, AnalyticsProperty$Q_$2$ analyticsProperty$Q_$2$) {
        this.value = jSONObject;
        this.scope = analyticsProperty$Q_$2$;
    }
}
