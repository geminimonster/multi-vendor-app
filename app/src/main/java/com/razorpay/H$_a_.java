package com.razorpay;

import android.graphics.Bitmap;
import android.webkit.WebView;
import android.webkit.WebViewClient;

final class H$_a_ extends WebViewClient {
    private l_$w$ Q_$2$;

    public final boolean shouldOverrideUrlLoading(WebView webView, String str) {
        return false;
    }

    public H$_a_(l_$w$ l__w_) {
        this.Q_$2$ = l__w_;
    }

    public final void onReceivedError(WebView webView, int i, String str, String str2) {
        this.Q_$2$.destroyActivity(2, str);
    }

    public final void onPageStarted(WebView webView, String str, Bitmap bitmap) {
        webView.setTag(str);
        this.Q_$2$.onPageStarted(1, webView, str);
    }

    public final void onPageFinished(WebView webView, String str) {
        this.Q_$2$.onPageFinished(1, webView, str);
    }
}
