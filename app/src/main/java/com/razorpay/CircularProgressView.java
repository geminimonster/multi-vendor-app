package com.razorpay;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.LinearInterpolator;

class CircularProgressView extends View {
    private float B$$W$;
    private int D$_X_;
    private int E$_6$;
    /* access modifiers changed from: private */
    public float E$_j$;
    private boolean G__G_;
    private float J$$A_;
    /* access modifiers changed from: private */
    public float J$_0_;
    /* access modifiers changed from: private */
    public float L__R$;
    private int O_$B_;
    private Paint Q_$2$;
    private boolean R$$r_;
    private int Y$_o$;
    private int a_$P$ = 0;
    /* access modifiers changed from: private */
    public float b__J_;
    private int c__C_;
    private RectF d__1_;
    private AnimatorSet f$_G$;
    private ValueAnimator g__v_;
    private int l_$w$;
    private ValueAnimator l__d$;
    private float r$_Y_;

    public CircularProgressView(Context context) {
        super(context);
        R$$r_(context);
    }

    public CircularProgressView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        R$$r_(context);
    }

    public CircularProgressView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        R$$r_(context);
    }

    private void R$$r_(Context context) {
        a_$P$(context);
        this.Q_$2$ = new Paint(1);
        R$$r_();
        this.d__1_ = new RectF();
    }

    private void a_$P$(Context context) {
        getResources();
        this.B$$W$ = 0.0f;
        this.r$_Y_ = 100.0f;
        this.D$_X_ = (int) TypedValue.applyDimension(1, 3.0f, context.getResources().getDisplayMetrics());
        this.R$$r_ = true;
        this.G__G_ = true;
        this.J$$A_ = -90.0f;
        this.J$_0_ = -90.0f;
        this.O_$B_ = Color.parseColor("#4aa3df");
        this.Y$_o$ = 4000;
        this.c__C_ = 5000;
        this.E$_6$ = 500;
        this.l_$w$ = 3;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        super.onMeasure(i, i2);
        int paddingLeft = getPaddingLeft() + getPaddingRight();
        int paddingTop = getPaddingTop() + getPaddingBottom();
        int measuredWidth = getMeasuredWidth() - paddingLeft;
        int measuredHeight = getMeasuredHeight() - paddingTop;
        if (measuredWidth >= measuredHeight) {
            measuredWidth = measuredHeight;
        }
        this.a_$P$ = measuredWidth;
        setMeasuredDimension(paddingLeft + measuredWidth, measuredWidth + paddingTop);
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        super.onSizeChanged(i, i2, i3, i4);
        if (i >= i2) {
            i = i2;
        }
        this.a_$P$ = i;
        d__1_();
    }

    private void d__1_() {
        int paddingLeft = getPaddingLeft();
        int paddingTop = getPaddingTop();
        RectF rectF = this.d__1_;
        int i = this.D$_X_;
        int i2 = this.a_$P$;
        rectF.set((float) (paddingLeft + i), (float) (paddingTop + i), (float) ((i2 - paddingLeft) - i), (float) ((i2 - paddingTop) - i));
    }

    private void R$$r_() {
        this.Q_$2$.setColor(this.O_$B_);
        this.Q_$2$.setStyle(Paint.Style.STROKE);
        this.Q_$2$.setStrokeWidth((float) this.D$_X_);
        this.Q_$2$.setStrokeCap(Paint.Cap.BUTT);
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        float f = ((isInEditMode() ? this.B$$W$ : this.L__R$) / this.r$_Y_) * 360.0f;
        if (!this.R$$r_) {
            canvas.drawArc(this.d__1_, this.J$_0_, f, false, this.Q_$2$);
        } else {
            canvas.drawArc(this.d__1_, this.J$_0_ + this.E$_j$, this.b__J_, false, this.Q_$2$);
        }
    }

    public void setIndeterminate(boolean z) {
        boolean z2 = this.R$$r_ == z;
        this.R$$r_ = z;
        if (z2) {
            Q_$2$();
        }
    }

    public void setThickness(int i) {
        this.D$_X_ = i;
        R$$r_();
        d__1_();
        invalidate();
    }

    public void setColor(int i) {
        this.O_$B_ = i;
        R$$r_();
        invalidate();
    }

    public void setMaxProgress(float f) {
        this.r$_Y_ = f;
        invalidate();
    }

    public void setProgress(float f) {
        this.B$$W$ = f;
        if (!this.R$$r_) {
            ValueAnimator valueAnimator = this.g__v_;
            if (valueAnimator != null && valueAnimator.isRunning()) {
                this.g__v_.cancel();
            }
            ValueAnimator ofFloat = ValueAnimator.ofFloat(new float[]{this.L__R$, f});
            this.g__v_ = ofFloat;
            ofFloat.setDuration((long) this.E$_6$);
            this.g__v_.setInterpolator(new LinearInterpolator());
            this.g__v_.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                public final void onAnimationUpdate(ValueAnimator valueAnimator) {
                    float unused = CircularProgressView.this.L__R$ = ((Float) valueAnimator.getAnimatedValue()).floatValue();
                    CircularProgressView.this.invalidate();
                }
            });
            this.g__v_.start();
        }
        invalidate();
    }

    public final void Q_$2$() {
        ValueAnimator valueAnimator = this.l__d$;
        if (valueAnimator != null && valueAnimator.isRunning()) {
            this.l__d$.cancel();
        }
        ValueAnimator valueAnimator2 = this.g__v_;
        if (valueAnimator2 != null && valueAnimator2.isRunning()) {
            this.g__v_.cancel();
        }
        AnimatorSet animatorSet = this.f$_G$;
        if (animatorSet != null && animatorSet.isRunning()) {
            this.f$_G$.cancel();
        }
        int i = 0;
        if (!this.R$$r_) {
            float f = this.J$$A_;
            this.J$_0_ = f;
            ValueAnimator ofFloat = ValueAnimator.ofFloat(new float[]{f, f + 360.0f});
            this.l__d$ = ofFloat;
            ofFloat.setDuration((long) this.c__C_);
            this.l__d$.setInterpolator(new DecelerateInterpolator(2.0f));
            this.l__d$.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                public final void onAnimationUpdate(ValueAnimator valueAnimator) {
                    float unused = CircularProgressView.this.J$_0_ = ((Float) valueAnimator.getAnimatedValue()).floatValue();
                    CircularProgressView.this.invalidate();
                }
            });
            this.l__d$.start();
            this.L__R$ = 0.0f;
            ValueAnimator ofFloat2 = ValueAnimator.ofFloat(new float[]{0.0f, this.B$$W$});
            this.g__v_ = ofFloat2;
            ofFloat2.setDuration((long) this.E$_6$);
            this.g__v_.setInterpolator(new LinearInterpolator());
            this.g__v_.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                public final void onAnimationUpdate(ValueAnimator valueAnimator) {
                    float unused = CircularProgressView.this.L__R$ = ((Float) valueAnimator.getAnimatedValue()).floatValue();
                    CircularProgressView.this.invalidate();
                }
            });
            this.g__v_.start();
            return;
        }
        this.b__J_ = 15.0f;
        this.f$_G$ = new AnimatorSet();
        AnimatorSet animatorSet2 = null;
        while (i < this.l_$w$) {
            AnimatorSet G__G_2 = G__G_((float) i);
            AnimatorSet.Builder play = this.f$_G$.play(G__G_2);
            if (animatorSet2 != null) {
                play.after(animatorSet2);
            }
            i++;
            animatorSet2 = G__G_2;
        }
        this.f$_G$.addListener(new AnimatorListenerAdapter() {
            private boolean Q_$2$ = false;

            public final void onAnimationCancel(Animator animator) {
                this.Q_$2$ = true;
            }

            public final void onAnimationEnd(Animator animator) {
                if (!this.Q_$2$) {
                    CircularProgressView.this.Q_$2$();
                }
            }
        });
        this.f$_G$.start();
    }

    private void G__G_() {
        ValueAnimator valueAnimator = this.l__d$;
        if (valueAnimator != null) {
            valueAnimator.cancel();
            this.l__d$ = null;
        }
        ValueAnimator valueAnimator2 = this.g__v_;
        if (valueAnimator2 != null) {
            valueAnimator2.cancel();
            this.g__v_ = null;
        }
        AnimatorSet animatorSet = this.f$_G$;
        if (animatorSet != null) {
            animatorSet.cancel();
            this.f$_G$ = null;
        }
    }

    private AnimatorSet G__G_(float f) {
        int i = this.l_$w$;
        final float f2 = ((((float) (i - 1)) * 360.0f) / ((float) i)) + 15.0f;
        final float f3 = ((f2 - 15.0f) * f) - 0.049804688f;
        ValueAnimator ofFloat = ValueAnimator.ofFloat(new float[]{15.0f, f2});
        ofFloat.setDuration((long) ((this.Y$_o$ / this.l_$w$) / 2));
        ofFloat.setInterpolator(new DecelerateInterpolator(1.0f));
        ofFloat.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            public final void onAnimationUpdate(ValueAnimator valueAnimator) {
                float unused = CircularProgressView.this.b__J_ = ((Float) valueAnimator.getAnimatedValue()).floatValue();
                CircularProgressView.this.invalidate();
            }
        });
        int i2 = this.l_$w$;
        float f4 = (0.5f + f) * 720.0f;
        ValueAnimator ofFloat2 = ValueAnimator.ofFloat(new float[]{(f * 720.0f) / ((float) i2), f4 / ((float) i2)});
        ofFloat2.setDuration((long) ((this.Y$_o$ / this.l_$w$) / 2));
        ofFloat2.setInterpolator(new LinearInterpolator());
        ofFloat2.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            public final void onAnimationUpdate(ValueAnimator valueAnimator) {
                float unused = CircularProgressView.this.E$_j$ = ((Float) valueAnimator.getAnimatedValue()).floatValue();
            }
        });
        ValueAnimator ofFloat3 = ValueAnimator.ofFloat(new float[]{f3, (f3 + f2) - 15.0f});
        ofFloat3.setDuration((long) ((this.Y$_o$ / this.l_$w$) / 2));
        ofFloat3.setInterpolator(new DecelerateInterpolator(1.0f));
        ofFloat3.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            public final void onAnimationUpdate(ValueAnimator valueAnimator) {
                float unused = CircularProgressView.this.J$_0_ = ((Float) valueAnimator.getAnimatedValue()).floatValue();
                CircularProgressView circularProgressView = CircularProgressView.this;
                float unused2 = circularProgressView.b__J_ = (f2 - circularProgressView.J$_0_) + f3;
                CircularProgressView.this.invalidate();
            }
        });
        int i3 = this.l_$w$;
        ValueAnimator ofFloat4 = ValueAnimator.ofFloat(new float[]{f4 / ((float) i3), ((f + 1.0f) * 720.0f) / ((float) i3)});
        ofFloat4.setDuration((long) ((this.Y$_o$ / this.l_$w$) / 2));
        ofFloat4.setInterpolator(new LinearInterpolator());
        ofFloat4.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            public final void onAnimationUpdate(ValueAnimator valueAnimator) {
                float unused = CircularProgressView.this.E$_j$ = ((Float) valueAnimator.getAnimatedValue()).floatValue();
            }
        });
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.play(ofFloat).with(ofFloat2);
        animatorSet.play(ofFloat3).with(ofFloat4).after(ofFloat2);
        return animatorSet;
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (this.G__G_) {
            Q_$2$();
        }
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        G__G_();
    }

    public void setVisibility(int i) {
        int visibility = getVisibility();
        super.setVisibility(i);
        if (i == visibility) {
            return;
        }
        if (i == 0) {
            Q_$2$();
        } else if (i == 8 || i == 4) {
            G__G_();
        }
    }
}
