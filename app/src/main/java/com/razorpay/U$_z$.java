package com.razorpay;

import android.content.Context;
import android.content.SharedPreferences;
import org.json.JSONObject;

public final class U$_z$ {
    private static long G__G_ = 5994205106755336670L;
    private static int Q_$2$ = 0;
    private static SharedPreferences R$$r_ = null;
    private static SharedPreferences.Editor a_$P$ = null;
    private static int d__1_ = 1;

    U$_z$() {
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0020, code lost:
        if (r0 == null) goto L_0x0022;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0019, code lost:
        if (r1 != false) goto L_0x0022;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static android.content.SharedPreferences a_$P$(android.content.Context r3) {
        /*
            int r0 = d__1_
            int r0 = r0 + 55
            int r1 = r0 % 128
            Q_$2$ = r1
            int r0 = r0 % 2
            r1 = 1
            r2 = 0
            if (r0 == 0) goto L_0x0010
            r0 = 0
            goto L_0x0011
        L_0x0010:
            r0 = 1
        L_0x0011:
            if (r0 == 0) goto L_0x001c
            android.content.SharedPreferences r0 = R$$r_
            if (r0 != 0) goto L_0x0018
            goto L_0x0019
        L_0x0018:
            r1 = 0
        L_0x0019:
            if (r1 == 0) goto L_0x0034
            goto L_0x0022
        L_0x001c:
            android.content.SharedPreferences r0 = R$$r_
            r1 = 0
            int r1 = r1.length
            if (r0 != 0) goto L_0x0034
        L_0x0022:
            int r0 = Q_$2$
            int r0 = r0 + 109
            int r1 = r0 % 128
            d__1_ = r1
            int r0 = r0 % 2
            java.lang.String r0 = "rzp_preference_private"
            android.content.SharedPreferences r3 = r3.getSharedPreferences(r0, r2)
            R$$r_ = r3
        L_0x0034:
            android.content.SharedPreferences r3 = R$$r_
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.razorpay.U$_z$.a_$P$(android.content.Context):android.content.SharedPreferences");
    }

    static SharedPreferences.Editor G__G_(Context context) {
        if (a_$P$ == null) {
            if ((R$$r_ == null ? '3' : 'b') == '3') {
                int i = Q_$2$ + 89;
                d__1_ = i % 128;
                int i2 = i % 2;
                R$$r_ = context.getSharedPreferences("rzp_preference_private", 0);
            }
            a_$P$ = R$$r_.edit();
            int i3 = Q_$2$ + 11;
            d__1_ = i3 % 128;
            int i4 = i3 % 2;
        }
        return a_$P$;
    }

    static SharedPreferences d__1_(Context context) {
        int i = d__1_ + 65;
        Q_$2$ = i % 128;
        if (i % 2 != 0) {
        }
        try {
            return context.getSharedPreferences("rzp_preference_public", 1);
        } catch (Exception e) {
            AnalyticsUtil.reportError(e, "critical", e.getMessage());
            return context.getSharedPreferences("rzp_preference_public", 0);
        }
    }

    static SharedPreferences.Editor Q_$2$(Context context) {
        int i = Q_$2$ + 109;
        d__1_ = i % 128;
        boolean z = i % 2 != 0;
        SharedPreferences.Editor edit = d__1_(context).edit();
        if (!z) {
            Object[] objArr = null;
            int length = objArr.length;
        }
        int i2 = Q_$2$ + 123;
        d__1_ = i2 % 128;
        int i3 = i2 % 2;
        return edit;
    }

    static SharedPreferences.Editor R$$r_(Context context) {
        int i = d__1_ + 115;
        Q_$2$ = i % 128;
        return (!(i % 2 == 0) ? context.getSharedPreferences("rzp_preferences_storage_bridge", 1) : context.getSharedPreferences("rzp_preferences_storage_bridge", 0)).edit();
    }

    static SharedPreferences E$_j$(Context context) {
        int i = Q_$2$ + 33;
        d__1_ = i % 128;
        int i2 = i % 2;
        SharedPreferences sharedPreferences = context.getSharedPreferences("rzp_preferences_storage_bridge", 0);
        int i3 = d__1_ + 95;
        Q_$2$ = i3 % 128;
        int i4 = i3 % 2;
        return sharedPreferences;
    }

    public static void Q_$2$(Context context, String str, String str2, String str3) {
        try {
            String randomString = BaseUtils.getRandomString();
            String d__1_2 = new B_$q$().d__1_(str2, Q_$2$("嬖學༉﻿暓ൺ譈뱒▩돊闲잏㰁?뼋咗싞웰飧漖?灭螘ᐐ㟒構鸢㲲ཧ䅠뚷✚").intern(), randomString);
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("data", d__1_2);
            jSONObject.put("iv", randomString);
            jSONObject.put("sdk_version", str3);
            String jSONObject2 = jSONObject.toString();
            SharedPreferences.Editor G__G_2 = G__G_(context);
            G__G_2.putString(str, jSONObject2);
            G__G_2.commit();
            int i = Q_$2$ + 125;
            d__1_ = i % 128;
            if (i % 2 == 0) {
                Object obj = null;
                super.hashCode();
            }
        } catch (Exception e) {
            AnalyticsUtil.reportError(e, "error", "Unable to encrypt value");
        }
    }

    static String a_$P$(Context context, String str) {
        int i = d__1_ + 31;
        Q_$2$ = i % 128;
        int i2 = i % 2;
        if ((R$$r_ == null ? 'E' : 8) == 'E') {
            int i3 = d__1_ + 43;
            Q_$2$ = i3 % 128;
            if (i3 % 2 != 0) {
            }
            R$$r_ = context.getSharedPreferences("rzp_preference_private", 0);
        }
        return R$$r_.getString(str, (String) null);
    }

    public static void G__G_(Context context, String str, String str2) {
        int i = d__1_ + 25;
        Q_$2$ = i % 128;
        if ((i % 2 != 0 ? (char) 0 : 18) != 0) {
            SharedPreferences.Editor G__G_2 = G__G_(context);
            G__G_2.putString(str, str2);
            G__G_2.commit();
            return;
        }
        SharedPreferences.Editor G__G_3 = G__G_(context);
        G__G_3.putString(str, str2);
        G__G_3.commit();
        Object[] objArr = null;
        int length = objArr.length;
    }

    /* JADX WARNING: type inference failed for: r3v0, types: [java.lang.String] */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0021, code lost:
        if ((r0 != null) != true) goto L_0x0023;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0016, code lost:
        if (R$$r_ == null) goto L_0x0023;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static void Q_$2$(android.content.Context r5, java.lang.String r6) {
        /*
            int r0 = Q_$2$
            int r0 = r0 + 59
            int r1 = r0 % 128
            d__1_ = r1
            int r0 = r0 % 2
            r1 = 1
            r2 = 0
            if (r0 != 0) goto L_0x0010
            r0 = 1
            goto L_0x0011
        L_0x0010:
            r0 = 0
        L_0x0011:
            r3 = 0
            if (r0 == r1) goto L_0x0019
            android.content.SharedPreferences r0 = R$$r_
            if (r0 != 0) goto L_0x002b
            goto L_0x0023
        L_0x0019:
            android.content.SharedPreferences r0 = R$$r_
            int r4 = r3.length
            if (r0 != 0) goto L_0x0020
            r0 = 0
            goto L_0x0021
        L_0x0020:
            r0 = 1
        L_0x0021:
            if (r0 == r1) goto L_0x002b
        L_0x0023:
            java.lang.String r0 = "rzp_preference_private"
            android.content.SharedPreferences r0 = r5.getSharedPreferences(r0, r2)
            R$$r_ = r0
        L_0x002b:
            android.content.SharedPreferences r0 = R$$r_
            java.lang.String r1 = "sdk_version"
            java.lang.String r0 = r0.getString(r1, r3)
            boolean r0 = r6.equalsIgnoreCase(r0)
            if (r0 != 0) goto L_0x0065
            android.content.SharedPreferences$Editor r0 = G__G_(r5)
            java.lang.String r4 = "rzp_config_json"
            r0.putString(r4, r3)
            r0.commit()
            android.content.SharedPreferences$Editor r0 = G__G_(r5)
            java.lang.String r4 = "rzp_config_version"
            r0.putString(r4, r3)
            r0.commit()
            android.content.SharedPreferences$Editor r5 = G__G_(r5)
            r5.putString(r1, r6)
            r5.commit()
            int r5 = d__1_
            int r5 = r5 + 25
            int r6 = r5 % 128
            Q_$2$ = r6
            int r5 = r5 % 2
        L_0x0065:
            int r5 = d__1_
            int r5 = r5 + 99
            int r6 = r5 % 128
            Q_$2$ = r6
            int r5 = r5 % 2
            if (r5 == 0) goto L_0x0074
            r5 = 89
            int r5 = r5 / r2
        L_0x0074:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.razorpay.U$_z$.Q_$2$(android.content.Context, java.lang.String):void");
    }

    /* JADX WARNING: type inference failed for: r0v0, types: [java.lang.String] */
    public static String d__1_(Context context, String str, String str2) {
        ? r0 = 0;
        try {
            boolean z = false;
            if (R$$r_ == null) {
                int i = Q_$2$ + 121;
                d__1_ = i % 128;
                R$$r_ = i % 2 == 0 ? context.getSharedPreferences("rzp_preference_private", 0) : context.getSharedPreferences("rzp_preference_private", 0);
            }
            String string = R$$r_.getString(str, r0);
            if (string == null) {
                return r0;
            }
            JSONObject jSONObject = new JSONObject(string);
            B_$q$ b_$q$ = new B_$q$();
            if (str2.equals(jSONObject.getString("sdk_version"))) {
                return b_$q$.G__G_(jSONObject.getString("data"), Q_$2$("嬖學༉﻿暓ൺ譈뱒▩돊闲잏㰁?뼋咗싞웰飧漖?灭螘ᐐ㟒構鸢㲲ཧ䅠뚷✚").intern(), jSONObject.getString("iv"));
            }
            int i2 = d__1_ + 17;
            Q_$2$ = i2 % 128;
            if (i2 % 2 == 0) {
                z = true;
            }
            if (!z) {
                int length = r0.length;
            }
            return r0;
        } catch (Exception e) {
            AnalyticsUtil.reportError(e, "error", "Unable to decrypt value");
            return r0;
        }
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r9v1, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r9v7, resolved type: char[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r9v8, resolved type: java.lang.String} */
    /* JADX WARNING: Failed to insert additional move for type inference */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static java.lang.String Q_$2$(java.lang.String r9) {
        /*
            if (r9 == 0) goto L_0x0006
            char[] r9 = r9.toCharArray()
        L_0x0006:
            char[] r9 = (char[]) r9
            long r0 = G__G_
            char[] r9 = com.razorpay.d__1_.R$$r_(r0, r9)
            r0 = 4
            r1 = 4
        L_0x0010:
            int r2 = r9.length
            r3 = 0
            r4 = 1
            if (r1 >= r2) goto L_0x0017
            r2 = 0
            goto L_0x0018
        L_0x0017:
            r2 = 1
        L_0x0018:
            if (r2 == r4) goto L_0x0057
            int r2 = d__1_
            int r2 = r2 + 61
            int r5 = r2 % 128
            Q_$2$ = r5
            int r2 = r2 % 2
            if (r2 == 0) goto L_0x0027
            r3 = 1
        L_0x0027:
            if (r3 == 0) goto L_0x003f
            int r2 = r1 * 5
            char r3 = r9[r1]
            int r4 = r1 << 4
            char r4 = r9[r4]
            r3 = r3 ^ r4
            long r3 = (long) r3
            long r5 = (long) r2
            long r7 = G__G_
            long r5 = r5 / r7
            long r3 = r3 / r5
            int r2 = (int) r3
            char r2 = (char) r2
            r9[r1] = r2
            int r1 = r1 + 39
            goto L_0x0010
        L_0x003f:
            int r2 = r1 + -4
            char r3 = r9[r1]
            int r4 = r1 % 4
            char r4 = r9[r4]
            r3 = r3 ^ r4
            long r3 = (long) r3
            long r5 = (long) r2
            long r7 = G__G_
            long r5 = r5 * r7
            long r2 = r3 ^ r5
            int r3 = (int) r2
            char r2 = (char) r3
            r9[r1] = r2
            int r1 = r1 + 1
            goto L_0x0010
        L_0x0057:
            java.lang.String r1 = new java.lang.String
            int r2 = r9.length
            int r2 = r2 - r0
            r1.<init>(r9, r0, r2)
            int r9 = d__1_
            int r9 = r9 + 91
            int r0 = r9 % 128
            Q_$2$ = r0
            int r9 = r9 % 2
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.razorpay.U$_z$.Q_$2$(java.lang.String):java.lang.String");
    }
}
