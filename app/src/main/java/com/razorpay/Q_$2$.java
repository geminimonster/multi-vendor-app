package com.razorpay;

import androidx.core.view.ViewCompat;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import kotlin.UByte;
import org.bouncycastle.asn1.cmc.BodyPartID;

public final class Q_$2$ extends FilterInputStream {
    private static final short Q_$2$ = ((short) ((int) ((Math.sqrt(5.0d) - 1.0d) * Math.pow(2.0d, 15.0d))));
    private int B$$W$;
    private int D$_X_;
    private int E$_6$;
    private int E$_j$;
    private int[] G__G_ = new int[8];
    private int[] R$$r_ = new int[8];
    private int a_$P$ = 8;
    private int b__J_;
    private int[] d__1_ = new int[8];
    private int l_$w$;
    private int r$_Y_;

    public final boolean markSupported() {
        return false;
    }

    public Q_$2$(InputStream inputStream, int[] iArr, int i, byte[] bArr, int i2, int i3) throws IOException {
        super(inputStream);
        this.B$$W$ = Math.min(Math.max(i2, 5), 16);
        this.E$_j$ = i3;
        if (i3 == 3) {
            this.G__G_ = d__1_(bArr);
        }
        long j = ((((long) iArr[0]) & BodyPartID.bodyIdMax) << 32) | (BodyPartID.bodyIdMax & ((long) iArr[1]));
        if (i == 0) {
            this.b__J_ = (int) j;
            long j2 = j >> 3;
            short s = Q_$2$;
            this.r$_Y_ = (int) ((((long) s) * j2) >> 32);
            this.l_$w$ = (int) (j >> 32);
            this.E$_6$ = (int) (j2 + ((long) s));
        } else {
            int i4 = (int) j;
            this.b__J_ = i4;
            this.r$_Y_ = i4 * i;
            this.l_$w$ = i4 ^ i;
            this.E$_6$ = (int) (j >> 32);
        }
        this.D$_X_ = this.in.read();
    }

    public final int read(byte[] bArr, int i, int i2) throws IOException {
        if (i2 == 0) {
            return 0;
        }
        int read = read();
        if (read == -1) {
            return -1;
        }
        bArr[i] = (byte) read;
        int i3 = 1;
        while (i3 < i2) {
            int read2 = read();
            if (read2 == -1) {
                return i3;
            }
            bArr[i + i3] = (byte) read2;
            i3++;
        }
        return i3;
    }

    public final long skip(long j) throws IOException {
        long j2 = 0;
        while (j2 < j && read() != -1) {
            j2++;
        }
        return j2;
    }

    public final int available() throws IOException {
        return 8 - this.a_$P$;
    }

    private static int[] d__1_(byte[] bArr) {
        int length = bArr.length;
        int[] iArr = new int[length];
        for (int i = 0; i < length; i++) {
            iArr[i] = bArr[i] & UByte.MAX_VALUE;
        }
        return iArr;
    }

    public final int read() throws IOException {
        if (this.a_$P$ == 8) {
            int i = this.D$_X_;
            if (i == -1) {
                Arrays.fill(this.d__1_, -1);
            } else {
                this.d__1_[0] = i;
                for (int i2 = 1; i2 < 8; i2++) {
                    this.d__1_[i2] = this.in.read();
                }
                if (this.E$_j$ == 3) {
                    int[] iArr = this.d__1_;
                    System.arraycopy(iArr, 0, this.R$$r_, 0, iArr.length);
                }
                int[] iArr2 = this.d__1_;
                int i3 = ((iArr2[0] << 24) & ViewCompat.MEASURED_STATE_MASK) + ((iArr2[1] << 16) & 16711680) + ((iArr2[2] << 8) & 65280) + (iArr2[3] & 255);
                int i4 = (-16777216 & (iArr2[4] << 24)) + (16711680 & (iArr2[5] << 16)) + (65280 & (iArr2[6] << 8)) + (iArr2[7] & 255);
                int i5 = 0;
                while (true) {
                    int i6 = this.B$$W$;
                    if (i5 >= i6) {
                        break;
                    }
                    short s = Q_$2$;
                    i4 -= ((i3 + ((i6 - i5) * s)) ^ ((i3 << 4) + this.l_$w$)) ^ ((i3 >>> 5) + this.E$_6$);
                    i3 -= (((i4 << 4) + this.b__J_) ^ ((s * (i6 - i5)) + i4)) ^ ((i4 >>> 5) + this.r$_Y_);
                    i5++;
                }
                int[] iArr3 = this.d__1_;
                iArr3[0] = i3 >>> 24;
                iArr3[1] = (i3 >> 16) & 255;
                iArr3[2] = (i3 >> 8) & 255;
                iArr3[3] = i3 & 255;
                iArr3[4] = i4 >>> 24;
                iArr3[5] = (i4 >> 16) & 255;
                iArr3[6] = (i4 >> 8) & 255;
                iArr3[7] = i4 & 255;
                if (this.E$_j$ == 3) {
                    for (int i7 = 0; i7 < 8; i7++) {
                        int[] iArr4 = this.d__1_;
                        iArr4[i7] = (iArr4[i7] ^ this.G__G_[i7]) & 255;
                    }
                    int[] iArr5 = this.R$$r_;
                    System.arraycopy(iArr5, 0, this.G__G_, 0, iArr5.length);
                }
                int read = this.in.read();
                this.D$_X_ = read;
                if (read == -1) {
                    int[] iArr6 = this.d__1_;
                    Arrays.fill(iArr6, 8 - iArr6[7], 8, -1);
                }
            }
            this.a_$P$ = 0;
        }
        int[] iArr7 = this.d__1_;
        int i8 = this.a_$P$;
        this.a_$P$ = i8 + 1;
        return iArr7[i8];
    }
}
