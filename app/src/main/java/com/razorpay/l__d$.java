package com.razorpay;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.WindowManager;
import android.webkit.WebView;
import com.bumptech.glide.load.Key;
import com.facebook.internal.ServerProtocol;
import com.facebook.share.internal.MessengerShareContentUtility;
import com.facebook.share.internal.ShareConstants;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.iqonic.store.utils.Constants;
import com.razorpay.AdvertisingIdUtil;
import com.razorpay.Y$_o$;
import com.stripe.android.view.PaymentAuthWebView;
import java.lang.Thread;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import org.json.JSONException;
import org.json.JSONObject;

class l__d$ implements c__C_, l_$w$, Runnable {
    private int B$$W$ = 0;
    private String D$_X_ = "{}";
    private long E$_6$;
    private boolean E$_j$;
    private String G__G_;
    private int J$_0_ = 0;
    private boolean L__R$ = false;
    private long O_$B_;
    O_$B_ Q_$2$;
    private JSONObject R$$r_;
    /* access modifiers changed from: private */
    public boolean Y$_o$ = false;
    String a_$P$;
    protected Activity activity;
    /* access modifiers changed from: private */
    public String b__J_;
    private boolean c__C_ = false;
    private String d__1_;
    private String f$_G$ = null;
    private Queue<String> g__v_ = new LinkedList();
    private boolean h__y_ = false;
    private long l_$w$;
    private AdvertisingIdUtil.AnonymousClass4 l__d$ = null;
    private boolean r$_Y_ = false;
    protected l__d$$R$$r_ view;

    /* access modifiers changed from: protected */
    public void helpersReset() {
    }

    public boolean isMagicPresent() {
        return false;
    }

    public void onRequestPermissionsResult(int i, String[] strArr, int[] iArr) {
    }

    public void relay(String str) {
    }

    public void requestOtpPermission() {
    }

    public void requestSmsPermission() {
    }

    public l__d$(Activity activity2, l__d$$R$$r_ l__d__r__r_) {
        this.activity = activity2;
        this.view = l__d__r__r_;
    }

    public boolean setOptions(Bundle bundle, boolean z) {
        this.L__R$ = z;
        if (bundle == null) {
            destroyActivity(0, this.activity.getResources().getString(R.string.activity_result_invalid_parameters));
            return false;
        }
        O_$B_ o_$b_ = new O_$B_(bundle.getString("OPTIONS"));
        this.Q_$2$ = o_$b_;
        this.a_$P$ = o_$b_.a_$P$();
        int i = bundle.getInt(ShareConstants.IMAGE_URL, 0);
        this.J$_0_ = i;
        this.Q_$2$.d__1_(this.activity, i);
        this.b__J_ = bundle.getString("BODY");
        AnalyticsUtil.setup(this.activity, this.a_$P$, L__R$.d__1_, L__R$.G__G_, L__R$.Q_$2$);
        O_$B_ o_$b_2 = this.Q_$2$;
        String a_$P$2 = g__v_.a_$P$("https://api.razorpay.com/v1/checkout/public", ServerProtocol.FALLBACK_DIALOG_PARAM_VERSION, L__R$.Q_$2$);
        Map<String, String> r_$Z$ = L__R$.L__R$().r_$Z$();
        for (String next : r_$Z$.keySet()) {
            a_$P$2 = g__v_.a_$P$(a_$P$2, next, r_$Z$.get(next));
        }
        Iterator<String> it = L__R$.L__R$().T__j$().iterator();
        while (it.hasNext()) {
            String next2 = it.next();
            if (o_$b_2.Q_$2$(next2)) {
                a_$P$2 = g__v_.a_$P$(a_$P$2, next2, (String) o_$b_2.R$$r_(next2));
            }
        }
        this.G__G_ = a_$P$2;
        if (a_$P$2 == null) {
            destroyActivity(3, this.activity.getResources().getString(R.string.activity_result_invalid_url));
        }
        try {
            this.R$$r_ = new JSONObject(this.d__1_);
        } catch (Exception unused) {
        }
        if (!z) {
            this.Q_$2$.Q_$2$();
            this.d__1_ = U$_z$.a_$P$(this.activity).getString("pref_merchant_options_".concat(String.valueOf(this.a_$P$)), (String) null);
            String string = bundle.getString("FRAMEWORK");
            if (string != null) {
                AnalyticsUtil.addProperty("framework", new AnalyticsProperty(string, AnalyticsProperty$Q_$2$.ORDER));
            }
            if (bundle.getBoolean("DISABLE_FULL_SCREEN", false)) {
                Activity activity2 = this.activity;
                activity2.getWindow().addFlags(2048);
                activity2.getWindow().clearFlags(1024);
            }
            if (bundle.containsKey("PRELOAD_COMPLETE_DURATION")) {
                this.E$_6$ = bundle.getLong("PRELOAD_COMPLETE_DURATION");
            }
            if (bundle.containsKey("PRELOAD_ABORT_DURATION")) {
                this.l_$w$ = bundle.getLong("PRELOAD_ABORT_DURATION");
            }
        } else {
            this.d__1_ = bundle.getString("DASH_OPTIONS");
            if (bundle.getBoolean("DISABLE_FULL_SCREEN", false)) {
                Activity activity3 = this.activity;
                activity3.getWindow().addFlags(2048);
                activity3.getWindow().clearFlags(1024);
            }
        }
        return true;
    }

    public void onActivityResultReceived(int i, int i2, Intent intent) {
        if (i == 99) {
            JSONObject jSONFromIntentData = BaseUtils.getJSONFromIntentData(intent);
            String format = String.format("javascript: upiIntentResponse(%s)", new Object[]{jSONFromIntentData.toString()});
            HashMap hashMap = new HashMap();
            hashMap.put("result", jSONFromIntentData);
            AnalyticsUtil.trackEvent(AnalyticsEvent.NATIVE_INTENT_ONACTIVITY_RESULT, (Map<String, Object>) hashMap);
            if (this.h__y_) {
                this.view.loadUrl(1, format);
                return;
            }
            if (this.g__v_ == null) {
                this.g__v_ = new LinkedList();
            }
            this.g__v_.add(format);
        }
    }

    public void verifyGPaySdkResponse(String str) {
        String format = String.format("javascript: window.externalSDKResponse(%s)", new Object[]{str});
        if (this.h__y_) {
            this.view.loadUrl(1, format);
            return;
        }
        if (this.g__v_ == null) {
            this.g__v_ = new LinkedList();
        }
        this.g__v_.add(format);
    }

    public O_$B_ getCheckoutOptions() {
        return this.Q_$2$;
    }

    public void setUpAddOn() {
        this.l__d$ = new AdvertisingIdUtil.AnonymousClass4(this.activity, this.view.getWebView(2));
    }

    public void loadForm(String str) {
        if (this.B$$W$ != 0) {
            AnalyticsUtil.postData();
        }
        int i = this.B$$W$ + 1;
        this.B$$W$ = i;
        AnalyticsUtil.addProperty("payment_attempt", new AnalyticsProperty(i, AnalyticsProperty$Q_$2$.ORDER));
        this.E$_j$ = true;
        StringBuilder sb = new StringBuilder();
        sb.append(this.G__G_);
        sb.append(str);
        String replace = sb.toString().replace(" ", "%20");
        String str2 = this.b__J_;
        if (str2 == null || str2.isEmpty()) {
            this.view.loadUrl(1, replace);
        } else {
            this.view.loadDataWithBaseURL(1, replace, this.b__J_, "text/html", Key.STRING_CHARSET_NAME, (String) null);
        }
    }

    public void passPrefillToSegment() {
        String B$$W$2 = this.Q_$2$.B$$W$();
        if (!TextUtils.isEmpty(B$$W$2)) {
            AnalyticsUtil.addProperty("email", new AnalyticsProperty(B$$W$2, AnalyticsProperty$Q_$2$.ORDER));
        }
        String D$_X_2 = this.Q_$2$.D$_X_();
        if (!TextUtils.isEmpty(D$_X_2)) {
            AnalyticsUtil.addProperty(Constants.SharedPref.CONTACT, new AnalyticsProperty(D$_X_2, AnalyticsProperty$Q_$2$.ORDER));
        }
    }

    public void handleCardSaving() {
        AnalyticsUtil.trackEvent(AnalyticsEvent.CARD_SAVING_START);
        E$_6$.G__G_(this.activity.getApplicationContext());
    }

    public void saveInstanceState(Bundle bundle) {
        if (this.J$_0_ != 0) {
            bundle.putString("OPTIONS", this.Q_$2$.r$_Y_());
            bundle.putInt(ShareConstants.IMAGE_URL, this.J$_0_);
        } else {
            bundle.putString("OPTIONS", this.Q_$2$.G__G_());
        }
        bundle.putString("DASH_OPTIONS", this.d__1_);
        if (this.activity.getIntent() != null) {
            bundle.putBoolean("DISABLE_FULL_SCREEN", this.activity.getIntent().getBooleanExtra("DISABLE_FULL_SCREEN", false));
        }
    }

    public void setCheckoutLoadStartAt() {
        this.O_$B_ = System.nanoTime();
    }

    public void destroyActivity(int i, String str) {
        AnalyticsUtil.addProperty("destroy_resultCode", new AnalyticsProperty(String.valueOf(i), AnalyticsProperty$Q_$2$.ORDER));
        AnalyticsUtil.addProperty("destroy_result", new AnalyticsProperty(str, AnalyticsProperty$Q_$2$.ORDER));
        AnalyticsUtil.trackEvent(AnalyticsEvent.INTERNAL_DESTROY_METHOD_CALLED);
        this.view.destroy(i, str);
    }

    public void onCheckoutBackPress() {
        AnalyticsUtil.trackEvent(AnalyticsEvent.CHECKOUT_SOFT_BACK_PRESSED);
        destroyActivity(0, "Checkout BackPressed");
    }

    /* access modifiers changed from: protected */
    public void enableAddon(JSONObject jSONObject) {
        try {
            if (jSONObject.has("magic")) {
                boolean z = jSONObject.getBoolean("magic");
                this.c__C_ = z;
                if (this.l__d$ != null) {
                    this.l__d$.G__G_(z);
                }
                AnalyticsUtil.addProperty("is_magic", new AnalyticsProperty(this.c__C_, AnalyticsProperty$Q_$2$.PAYMENT));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void invokePopup(final String str) {
        this.Y$_o$ = true;
        try {
            this.activity.runOnUiThread(new Runnable() {
                public final void run() {
                    try {
                        JSONObject jSONObject = new JSONObject(str);
                        l__d$.this.enableAddon(jSONObject);
                        if (jSONObject.has(FirebaseAnalytics.Param.CONTENT)) {
                            l__d$.this.view.loadDataWithBaseURL(2, PaymentAuthWebView.PaymentAuthWebViewClient.BLANK_PAGE, jSONObject.getString(FirebaseAnalytics.Param.CONTENT), "text/html", Key.STRING_CHARSET_NAME, (String) null);
                        }
                        if (jSONObject.has("url")) {
                            l__d$.this.view.loadUrl(2, jSONObject.getString("url"));
                        }
                        if (!jSONObject.has("focus") || jSONObject.getBoolean("focus")) {
                            l__d$.this.view.makeWebViewVisible(2);
                        } else {
                            l__d$.this.view.makeWebViewVisible(1);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    AnalyticsUtil.addProperty("two_webview_flow", new AnalyticsProperty(true, AnalyticsProperty$Q_$2$.PAYMENT));
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void fetchCondfig() {
        L__R$.R$$r_ = isMagicPresent();
        L__R$.a_$P$(this.activity, this.a_$P$);
    }

    public void onProgressChanges(int i, int i2) {
        if (i == 1) {
            this.view.showProgressBar(i2);
        }
    }

    public void onPageStarted(int i, WebView webView, String str) {
        AdvertisingIdUtil.AnonymousClass4 r1;
        if (i == 1) {
            g__v_.Q_$2$(this.activity);
        } else if (i == 2 && (r1 = this.l__d$) != null && this.c__C_) {
            r1.G__G_();
        }
    }

    public void onPageFinished(int i, WebView webView, String str) {
        AdvertisingIdUtil.AnonymousClass4 r2;
        if (i == 1) {
            primaryWebviewPageFinished(str, webView);
        } else if (i == 2 && (r2 = this.l__d$) != null && this.c__C_) {
            r2.a_$P$();
        }
    }

    /* access modifiers changed from: protected */
    public void primaryWebviewPageFinished(String str, WebView webView) {
        long nanoTime = System.nanoTime();
        g__v_.a_$P$();
        this.view.hideProgressBar();
        BaseUtils.getDeviceParamValues(this.activity, new RzpJSONCallback() {
            public final void onResponse(JSONObject jSONObject) {
                l__d$.this.view.loadUrl(1, String.format("javascript: window.getDeviceDetails(%s)", new Object[]{jSONObject.toString()}));
            }
        });
        if (str.indexOf(this.G__G_) == 0) {
            if (this.B$$W$ == 1) {
                this.h__y_ = true;
                Queue<String> queue = this.g__v_;
                if (queue != null && !queue.isEmpty()) {
                    for (String loadUrl : this.g__v_) {
                        this.view.loadUrl(1, loadUrl);
                    }
                    this.g__v_.clear();
                }
                HashMap hashMap = new HashMap();
                long j = nanoTime - this.O_$B_;
                hashMap.put("checkout_load_duration", Long.valueOf(j));
                BaseUtils.nanoTimeToSecondsString(j, 2);
                long j2 = this.E$_6$;
                if (j2 > 0) {
                    hashMap.put("preload_finish_duration", Long.valueOf(j2));
                    BaseUtils.nanoTimeToSecondsString(this.E$_6$, 2);
                } else {
                    long j3 = this.l_$w$;
                    if (j3 > 0) {
                        hashMap.put("preload_abort_duration", Long.valueOf(j3));
                        BaseUtils.nanoTimeToSecondsString(this.l_$w$, 2);
                    }
                }
                long j4 = this.E$_6$ - j;
                if (j4 > 0) {
                    hashMap.put("time_shaved_off", Long.valueOf(j4));
                    BaseUtils.nanoTimeToSecondsString(j4, 2);
                }
                AnalyticsUtil.trackEvent(AnalyticsEvent.CHECKOUT_LOADED, (Map<String, Object>) hashMap);
            }
            if (this.E$_j$) {
                this.view.clearWebViewHistory(1);
                this.E$_j$ = false;
            }
        }
    }

    public String getProgressBarColor() {
        String str = null;
        try {
            if (this.Q_$2$.R$$r_() != null) {
                String string = this.Q_$2$.R$$r_().getJSONObject("theme").getString("color");
                Color.parseColor(string);
                return string;
            }
            throw new Exception("No options defined");
        } catch (Exception e) {
            if (this.R$$r_ != null) {
                str = this.R$$r_.getJSONObject("theme").getString("color");
                Color.parseColor(str);
            } else {
                throw new Exception("No dash options defined");
            }
        } catch (Exception e2) {
            AnalyticsUtil.reportError(e2, "error", e2.getMessage());
        }
        AnalyticsUtil.reportError(e, "error", e.getMessage());
        return str;
    }

    public void sendOtpPermissionCallback(final boolean z) {
        this.activity.runOnUiThread(new Runnable() {
            public final void run() {
                try {
                    JSONObject jSONObject = new JSONObject();
                    jSONObject.put("granted", z);
                    l__d$.this.view.loadUrl(1, String.format("javascript: otpPermissionCallback(%s)", new Object[]{jSONObject.toString()}));
                } catch (Exception unused) {
                }
            }
        });
    }

    /* access modifiers changed from: protected */
    public void addOnFlowEnd() {
        AdvertisingIdUtil.AnonymousClass4 r0 = this.l__d$;
        if (r0 != null) {
            r0.Q_$2$();
        }
    }

    public void cleanUpOnDestroy() {
        Q_$2$();
        addOnFlowEnd();
        Thread.UncaughtExceptionHandler defaultUncaughtExceptionHandler = Thread.getDefaultUncaughtExceptionHandler();
        if (defaultUncaughtExceptionHandler instanceof B$$J$) {
            Thread.setDefaultUncaughtExceptionHandler(((B$$J$) defaultUncaughtExceptionHandler).a_$P$);
        }
    }

    /* access modifiers changed from: private */
    public void Q_$2$() {
        if (this.f$_G$ != null && !this.r$_Y_) {
            try {
                String constructBasicAuth = BaseUtils.constructBasicAuth(this.a_$P$);
                HashMap hashMap = new HashMap();
                hashMap.put("Authorization", "Basic ".concat(String.valueOf(constructBasicAuth)));
                StringBuilder sb = new StringBuilder("https://api.razorpay.com/v1/payments/");
                sb.append(this.f$_G$);
                sb.append("/cancel?platform=android_sdk");
                r_$Z$.Q_$2$(sb.toString(), hashMap, new Callback() {
                    public final void run(C__D$ c__d$) {
                    }
                });
                this.f$_G$ = null;
            } catch (Exception unused) {
            }
        }
    }

    public void backPressed(final Map<String, Object> map) {
        AnalyticsUtil.trackEvent(AnalyticsEvent.CHECKOUT_HARD_BACK_PRESSED, map);
        WebView webView = this.view.getWebView(1);
        if ((webView.getTag() == null ? "" : webView.getTag().toString()).contains(L__R$.L__R$().c__C_()) && !this.view.isWebViewVisible(2)) {
            this.view.loadUrl(1, "javascript: window.backPressed ? window.backPressed('onCheckoutBackPress') : CheckoutBridge.onCheckoutBackPress();");
            map.put("in_checkout", ServerProtocol.DIALOG_RETURN_SCOPES_TRUE);
        } else if (!L__R$.L__R$().Q$$U_()) {
            destroyActivity(0, "BackPressed");
        } else {
            g__v_.R$$r_(this.activity, L__R$.L__R$().L$$C_(), L__R$.L__R$().I$_e_(), L__R$.L__R$().Y_$B$(), new g__v_$Q_$2$() {
                public final void G__G_() {
                    AnalyticsUtil.trackEvent(AnalyticsEvent.ALERT_PAYMENT_CONTINUE, (Map<String, Object>) map);
                }

                public final void R$$r_() {
                    AnalyticsUtil.trackEvent(AnalyticsEvent.ALERT_PAYMENT_CANCELLED, (Map<String, Object>) map);
                    if (l__d$.this.Y$_o$) {
                        l__d$.this.view.makeWebViewVisible(1);
                        l__d$.this.view.loadUrl(2, PaymentAuthWebView.PaymentAuthWebViewClient.BLANK_PAGE);
                        l__d$.this.view.loadUrl(1, "javascript: window.onpaymentcancel()");
                    } else {
                        l__d$.R$$r_(l__d$.this, "");
                        l__d$.this.Q_$2$();
                    }
                    boolean unused = l__d$.this.Y$_o$ = false;
                }
            });
        }
    }

    public void onLoad() {
        this.activity.runOnUiThread(new Runnable() {
            public final void run() {
                if (l__d$.this.b__J_ == null || l__d$.this.b__J_.isEmpty()) {
                    l__d$.this.view.loadUrl(1, "javascript: CheckoutBridge.setCheckoutBody(document.documentElement.outerHTML)");
                }
                l__d$.this.view.loadUrl(1, String.format("javascript: handleMessage(%s)", new Object[]{l__d$.this.getOptionsForHandleMessage().toString()}));
                l__d$.this.view.loadUrl(1, String.format("javascript: CheckoutBridge.sendAnalyticsData({data: %s})", new Object[]{AnalyticsUtil.getAnalyticsDataForCheckout(l__d$.this.activity).toString()}));
            }
        });
    }

    /* access modifiers changed from: protected */
    public JSONObject getOptionsForHandleMessage() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("options", this.Q_$2$.R$$r_());
            jSONObject.put("data", this.D$_X_);
            jSONObject.put("id", AnalyticsUtil.getLocalOrderId());
            jSONObject.put(Constants.KeyIntent.KEYID, this.a_$P$);
            jSONObject.put("upi_intents_data", g__v_.R$$r_(this.activity));
            JSONObject jSONObject2 = new JSONObject();
            jSONObject2.put("openedAt", System.currentTimeMillis());
            jSONObject.put("metadata", jSONObject2);
            String string = U$_z$.d__1_(this.activity.getApplicationContext()).getString("rzp_device_token", (String) null);
            if (!TextUtils.isEmpty(string)) {
                jSONObject.put("device_token", string);
            }
            jSONObject.put("sdk_popup", true);
            jSONObject.put("magic", true);
            jSONObject.put("network_type", BaseUtils.getNetworkType(this.activity));
            jSONObject.put("activity_recreated", this.L__R$);
        } catch (JSONException unused) {
        }
        return jSONObject;
    }

    public void setAppToken(String str) {
        U$_z$.G__G_(this.activity).putString("rzp_app_token", str).apply();
    }

    public void setDeviceToken(String str) {
        U$_z$.Q_$2$((Context) this.activity).putString("rzp_device_token", str).apply();
    }

    public void callNativeIntent(String str, String str2) {
        BaseUtils.startActivityForResult(str, str2, this.activity);
        HashMap hashMap = new HashMap();
        if (str == null) {
            str = "null";
        }
        hashMap.put("url", str);
        if (str2 == null) {
            str2 = "null";
        }
        hashMap.put("package_name", str2);
        AnalyticsUtil.trackEvent(AnalyticsEvent.NATIVE_INTENT_CALLED, (Map<String, Object>) hashMap);
    }

    public void setPaymentID(String str) {
        this.f$_G$ = str;
        AnalyticsUtil.addProperty("payment_id", new AnalyticsProperty(str, AnalyticsProperty$Q_$2$.PAYMENT));
        AnalyticsUtil.trackEvent(AnalyticsEvent.PAYMENT_ID_ATTACHED);
    }

    public void setCheckoutBody(String str) {
        this.b__J_ = str;
    }

    public void setMerchantOptions(String str) {
        this.d__1_ = str;
        try {
            this.R$$r_ = new JSONObject(str);
        } catch (Exception e) {
            this.R$$r_ = null;
            AnalyticsUtil.reportError(e, "critical", e.getMessage());
        }
        if (this.R$$r_ == null) {
            g__v_.G__G_(this.activity, this.a_$P$, (String) null);
        } else {
            g__v_.G__G_(this.activity, this.a_$P$, str);
        }
    }

    /* access modifiers changed from: protected */
    public void addAnalyticsData(JSONObject jSONObject) {
        AnalyticsUtil.addFilteredPropertiesFromPayload(jSONObject);
    }

    public void onSubmit(String str) {
        if (this.B$$W$ > 1) {
            AnalyticsUtil.refreshPaymentSession();
        }
        try {
            JSONObject jSONObject = new JSONObject(str);
            this.D$_X_ = str;
            addAnalyticsData(jSONObject);
            try {
                if (jSONObject.has(Constants.SharedPref.CONTACT)) {
                    Activity activity2 = this.activity;
                    String string = jSONObject.getString(Constants.SharedPref.CONTACT);
                    SharedPreferences.Editor G__G_2 = U$_z$.G__G_(activity2);
                    G__G_2.putString("rzp_user_contact", string);
                    G__G_2.commit();
                    this.Q_$2$.G__G_(Constants.SharedPref.CONTACT, jSONObject.getString(Constants.SharedPref.CONTACT));
                }
                if (jSONObject.has("email")) {
                    Activity activity3 = this.activity;
                    String string2 = jSONObject.getString("email");
                    SharedPreferences.Editor G__G_3 = U$_z$.G__G_(activity3);
                    G__G_3.putString("rzp_user_email", string2);
                    G__G_3.commit();
                    this.Q_$2$.G__G_("email", jSONObject.getString("email"));
                }
            } catch (JSONException unused) {
            }
            String string3 = jSONObject.getString(FirebaseAnalytics.Param.METHOD);
            if (!string3.equals("card") && string3.equals("wallet")) {
                String string4 = jSONObject.getString("wallet");
                if (this.Q_$2$.d__1_(string4)) {
                    JSONObject jSONObject2 = new JSONObject();
                    jSONObject2.put("external_wallet", string4);
                    AnalyticsUtil.addProperty("external_wallet", new AnalyticsProperty(string4, AnalyticsProperty$Q_$2$.ORDER));
                    AnalyticsUtil.trackEvent(AnalyticsEvent.EXTERNAL_WALLET_SELECTED);
                    onComplete(jSONObject2);
                }
            }
            AnalyticsUtil.trackEvent(AnalyticsEvent.CHECKOUT_SUBMIT);
            AnalyticsUtil.postData();
        } catch (Exception e) {
            AnalyticsUtil.reportError(e, "critical", e.getMessage());
        }
    }

    /* access modifiers changed from: protected */
    public void onComplete(JSONObject jSONObject) {
        try {
            if (jSONObject.has("error")) {
                AnalyticsUtil.addProperty("payment_status", new AnalyticsProperty("fail", AnalyticsProperty$Q_$2$.PAYMENT));
                AnalyticsUtil.addProperty(MessengerShareContentUtility.ATTACHMENT_PAYLOAD, new AnalyticsProperty(jSONObject.toString(), AnalyticsProperty$Q_$2$.PAYMENT));
                AnalyticsUtil.trackEvent(AnalyticsEvent.CHECKOUT_PAYMENT_COMPLETE);
                if (this.Y$_o$) {
                    this.view.makeWebViewVisible(1);
                }
                onError(jSONObject);
            } else if (jSONObject.has("razorpay_fund_account_id")) {
                destroyActivity(1, jSONObject.toString());
            } else if (jSONObject.has("razorpay_payment_id")) {
                String string = jSONObject.getString("razorpay_payment_id");
                this.f$_G$ = string;
                AnalyticsUtil.addProperty("payment_id", new AnalyticsProperty(string, AnalyticsProperty$Q_$2$.PAYMENT));
                AnalyticsUtil.addProperty("payment_status", new AnalyticsProperty("success", AnalyticsProperty$Q_$2$.PAYMENT));
                AnalyticsUtil.addProperty(MessengerShareContentUtility.ATTACHMENT_PAYLOAD, new AnalyticsProperty(jSONObject.toString(), AnalyticsProperty$Q_$2$.PAYMENT));
                AnalyticsUtil.trackEvent(AnalyticsEvent.CHECKOUT_PAYMENT_COMPLETE);
                this.r$_Y_ = true;
                destroyActivity(1, jSONObject.toString());
            } else if (jSONObject.has("external_wallet")) {
                destroyActivity(4, jSONObject.toString());
            } else {
                destroyActivity(0, "Post payment parsing error");
            }
        } catch (Exception e) {
            AnalyticsUtil.reportError(e, "critical", e.getMessage());
            destroyActivity(0, e.getMessage());
        }
        this.Y$_o$ = false;
    }

    /* access modifiers changed from: protected */
    public void onError(JSONObject jSONObject) {
        String str = "?";
        if (this.Y$_o$) {
            this.view.loadUrl(1, String.format("javascript: window.onComplete(%s)", new Object[]{jSONObject.toString()}));
            return;
        }
        final String str2 = "";
        try {
            if (jSONObject.has("error")) {
                StringBuilder sb = new StringBuilder();
                sb.append(str2);
                if (this.G__G_.contains(str)) {
                    str = "&";
                }
                sb.append(str);
                str2 = sb.toString();
                if (jSONObject.get("error") instanceof JSONObject) {
                    JSONObject jSONObject2 = (JSONObject) jSONObject.get("error");
                    if (jSONObject2.has("description")) {
                        StringBuilder sb2 = new StringBuilder();
                        sb2.append(str2);
                        sb2.append("error.description=");
                        sb2.append(jSONObject2.get("description"));
                        str2 = sb2.toString();
                        if (jSONObject2.has("field")) {
                            StringBuilder sb3 = new StringBuilder();
                            sb3.append(str2);
                            sb3.append("&error.field=");
                            sb3.append(jSONObject2.get("field"));
                            str2 = sb3.toString();
                        }
                    } else {
                        StringBuilder sb4 = new StringBuilder();
                        sb4.append(str2);
                        sb4.append("error=");
                        sb4.append(jSONObject2.toString());
                        str2 = sb4.toString();
                    }
                }
            }
        } catch (Exception e) {
            AnalyticsUtil.reportError(e, "critical", e.getMessage());
        } finally {
            this.activity.runOnUiThread(new Runnable() {
                public final void run() {
                    l__d$.R$$r_(l__d$.this, str2);
                }
            });
        }
    }

    public void onFault(String str) {
        destroyActivity(3, str);
    }

    public void onComplete(final String str) {
        this.activity.runOnUiThread(new Runnable() {
            public final void run() {
                try {
                    l__d$.this.onComplete(new JSONObject(str));
                } catch (Exception e) {
                    AnalyticsUtil.reportError(e, "critical", e.getMessage());
                    l__d$.this.destroyActivity(0, e.getMessage());
                }
            }
        });
    }

    public void setDimensions(final int i, final int i2) {
        if (this.activity.getResources().getBoolean(R.bool.isTablet)) {
            this.activity.runOnUiThread(new Runnable() {
                public final void run() {
                    WindowManager.LayoutParams attributes = l__d$.this.activity.getWindow().getAttributes();
                    Activity activity = l__d$.this.activity;
                    attributes.height = (int) TypedValue.applyDimension(1, (float) i2, activity.getResources().getDisplayMetrics());
                    Activity activity2 = l__d$.this.activity;
                    attributes.width = (int) TypedValue.applyDimension(1, (float) i, activity2.getResources().getDisplayMetrics());
                    l__d$.this.activity.getWindow().setAttributes(attributes);
                }
            });
        }
    }

    public void onDismiss() {
        destroyActivity(0, "Payment Cancelled");
    }

    public void onDismiss(String str) {
        destroyActivity(0, str);
    }

    public void requestExtraAnalyticsData() {
        final JSONObject extraAnalyticsPayload = AnalyticsUtil.getExtraAnalyticsPayload();
        this.activity.runOnUiThread(new Runnable() {
            public final void run() {
                try {
                    l__d$.this.view.loadUrl(1, String.format("javascript: CheckoutBridge.sendExtraAnalyticsData(%s)", new Object[]{extraAnalyticsPayload.toString()}));
                } catch (Exception unused) {
                }
            }
        });
    }

    public void onError(String str) {
        try {
            onError(new JSONObject(str));
        } catch (Exception e) {
            AnalyticsUtil.reportError(e, "critical", e.getMessage());
            this.activity.runOnUiThread(this);
        }
    }

    public void toast(final String str, final int i) {
        this.activity.runOnUiThread(new Runnable() {
            public final void run() {
                l__d$.this.view.showToast(str, i);
            }
        });
    }

    public void showAlertDialog(final String str, final String str2, final String str3) {
        this.activity.runOnUiThread(new Object() {
            public final void run() {
                g__v_.R$$r_(l__d$.this.activity, str, str3, str2, this);
            }

            public final void G__G_() {
                l__d$.this.view.loadUrl(1, String.format("javascript: CheckoutBridge.isPositiveButtonClicked({isClicked: %s})", new Object[]{Boolean.TRUE}));
            }

            public final void R$$r_() {
                l__d$.this.view.loadUrl(1, String.format("javascript: CheckoutBridge.isPositiveButtonClicked({isClicked: %s})", new Object[]{Boolean.FALSE}));
            }
        });
    }

    public void sendDataToWebView(final int i, final String str) {
        this.activity.runOnUiThread(new Runnable() {
            public final void run() {
                int i = i;
                if (i == 1) {
                    l__d$.this.view.loadUrl(1, String.format("javascript: handleRelay(%s)", new Object[]{str}));
                } else if (i == 2) {
                    l__d$.this.view.loadUrl(2, String.format("javascript: Magic.handleRelay(%s)", new Object[]{str}));
                }
            }
        });
    }

    public void isWebViewSafeOnUI(final int i, final Y$_o$.d__1_ d__1_2) {
        this.activity.runOnUiThread(new Runnable() {
            public final void run() {
                l__d$.this.R$$r_(i, d__1_2);
            }
        });
    }

    /* access modifiers changed from: private */
    public void R$$r_(int i, Y$_o$.d__1_ d__1_2) {
        l__d$$R$$r_ l__d__r__r_;
        int i2 = 1;
        if (i == 1) {
            l__d__r__r_ = this.view;
        } else {
            l__d__r__r_ = this.view;
            i2 = 2;
        }
        try {
            String host = new URL(l__d__r__r_.getWebView(i2).getTag().toString()).getHost();
            if (host != null && host.endsWith("razorpay.com")) {
                d__1_2.d__1_();
            }
        } catch (Exception unused) {
        }
    }

    public void isWebViewSafe(int i, Y$_o$.d__1_ d__1_2) {
        R$$r_(i, d__1_2);
    }

    public boolean isUserRegisteredOnUPI(String str) {
        return BaseUtils.checkUpiRegisteredApp(this.activity, str);
    }

    static /* synthetic */ void R$$r_(l__d$ l__d_, String str) {
        int i = l__d_.B$$W$;
        int B_$q$ = L__R$.L__R$().B_$q$();
        boolean z = true;
        if (!L__R$.L__R$().J$$A_() || (B_$q$ != -1 && B_$q$ <= i)) {
            z = false;
        }
        if (z) {
            l__d_.helpersReset();
            l__d_.loadForm(str);
            return;
        }
        l__d_.destroyActivity(0, "BackPressed");
    }

    public void run() {
        helpersReset();
        loadForm("");
    }
}
