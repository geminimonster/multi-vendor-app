package com.razorpay;

import androidx.core.view.ViewCompat;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import kotlin.UByte;

public final class G__G_ extends FilterInputStream {
    private int[] B$$W$ = new int[2];
    private int E$_j$;
    private final int G__G_;
    private R$$r_ Q_$2$;
    private int[] R$$r_ = new int[8];
    private int[] a_$P$ = new int[8];
    private int b__J_;
    private int[] d__1_ = new int[8];
    private int r$_Y_ = 8;

    public final boolean markSupported() {
        return false;
    }

    public G__G_(InputStream inputStream, int[] iArr, byte[] bArr, int i, boolean z, int i2) throws IOException {
        super(inputStream);
        this.G__G_ = Math.min(Math.max(i, 3), 16);
        this.E$_j$ = i2;
        if (i2 == 2) {
            System.arraycopy(a_$P$(bArr), 0, this.R$$r_, 0, 8);
        }
        this.Q_$2$ = new R$$r_(iArr, this.G__G_, true);
        this.b__J_ = this.in.read();
    }

    public final int read(byte[] bArr, int i, int i2) throws IOException {
        if (i2 == 0) {
            return 0;
        }
        int read = read();
        if (read == -1) {
            return -1;
        }
        bArr[i] = (byte) read;
        int i3 = 1;
        while (i3 < i2) {
            int read2 = read();
            if (read2 == -1) {
                return i3;
            }
            bArr[i + i3] = (byte) read2;
            i3++;
        }
        return i3;
    }

    public final long skip(long j) throws IOException {
        long j2 = 0;
        while (j2 < j && read() != -1) {
            j2++;
        }
        return j2;
    }

    public final int available() throws IOException {
        return 8 - this.r$_Y_;
    }

    private static int[] a_$P$(byte[] bArr) {
        int length = bArr.length;
        int[] iArr = new int[length];
        for (int i = 0; i < length; i++) {
            iArr[i] = bArr[i] & UByte.MAX_VALUE;
        }
        return iArr;
    }

    public final int read() throws IOException {
        if (this.r$_Y_ == 8) {
            int i = this.b__J_;
            if (i == -1) {
                Arrays.fill(this.a_$P$, -1);
            } else {
                this.a_$P$[0] = i;
                for (int i2 = 1; i2 < 8; i2++) {
                    this.a_$P$[i2] = this.in.read();
                }
                if (this.E$_j$ == 2) {
                    int[] iArr = this.a_$P$;
                    System.arraycopy(iArr, 0, this.d__1_, 0, iArr.length);
                }
                int[] iArr2 = this.a_$P$;
                a_$P$.a_$P$(((iArr2[0] << 24) & ViewCompat.MEASURED_STATE_MASK) + ((iArr2[1] << 16) & 16711680) + ((iArr2[2] << 8) & 65280) + (iArr2[3] & 255), (-16777216 & (iArr2[4] << 24)) + (16711680 & (iArr2[5] << 16)) + (65280 & (iArr2[6] << 8)) + (iArr2[7] & 255), false, this.G__G_, this.Q_$2$.G__G_, this.Q_$2$.d__1_, this.B$$W$);
                int[] iArr3 = this.B$$W$;
                int i3 = iArr3[0];
                int i4 = iArr3[1];
                int[] iArr4 = this.a_$P$;
                iArr4[0] = i3 >>> 24;
                iArr4[1] = (i3 >> 16) & 255;
                iArr4[2] = (i3 >> 8) & 255;
                iArr4[3] = i3 & 255;
                iArr4[4] = i4 >>> 24;
                iArr4[5] = (i4 >> 16) & 255;
                iArr4[6] = (i4 >> 8) & 255;
                iArr4[7] = i4 & 255;
                if (this.E$_j$ == 2) {
                    for (int i5 = 0; i5 < 8; i5++) {
                        int[] iArr5 = this.a_$P$;
                        iArr5[i5] = (iArr5[i5] ^ this.R$$r_[i5]) & 255;
                    }
                    int[] iArr6 = this.d__1_;
                    System.arraycopy(iArr6, 0, this.R$$r_, 0, iArr6.length);
                }
                int read = this.in.read();
                this.b__J_ = read;
                if (read == -1) {
                    int[] iArr7 = this.a_$P$;
                    Arrays.fill(iArr7, 8 - iArr7[7], 8, -1);
                }
            }
            this.r$_Y_ = 0;
        }
        int[] iArr8 = this.a_$P$;
        int i6 = this.r$_Y_;
        this.r$_Y_ = i6 + 1;
        return iArr8[i6];
    }
}
