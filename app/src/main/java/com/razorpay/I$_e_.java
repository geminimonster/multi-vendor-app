package com.razorpay;

import android.webkit.ConsoleMessage;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import com.stripe.android.AnalyticsDataFactory;
import java.util.HashMap;
import java.util.Map;

final class I$_e_ extends WebChromeClient {
    private l_$w$ R$$r_;

    public I$_e_(l_$w$ l__w_) {
        this.R$$r_ = l__w_;
    }

    public final void onProgressChanged(WebView webView, int i) {
        this.R$$r_.onProgressChanges(1, i);
    }

    public final boolean onConsoleMessage(ConsoleMessage consoleMessage) {
        if (consoleMessage.messageLevel() != ConsoleMessage.MessageLevel.ERROR) {
            return false;
        }
        HashMap hashMap = new HashMap();
        hashMap.put("message", consoleMessage.message());
        hashMap.put(AnalyticsDataFactory.FIELD_SOURCE_ID, consoleMessage.sourceId());
        hashMap.put("line_number", String.valueOf(consoleMessage.lineNumber()));
        AnalyticsUtil.trackEvent(AnalyticsEvent.WEB_VIEW_JS_ERROR, (Map<String, Object>) hashMap);
        consoleMessage.message();
        return false;
    }
}
