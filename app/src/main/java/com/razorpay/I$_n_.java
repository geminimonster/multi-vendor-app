package com.razorpay;

import android.app.Activity;
import android.content.IntentFilter;
import java.util.ArrayList;
import java.util.Iterator;

final class I$_n_ {
    static I$_n_ d__1_;
    private O_$v$ Q_$2$;
    ArrayList<t_$J_> a_$P$ = new ArrayList<>();

    I$_n_() {
    }

    /* access modifiers changed from: package-private */
    public final boolean a_$P$(Activity activity) {
        if (BaseUtils.hasPermission(activity, "android.permission.RECEIVE_SMS")) {
            R$$r_(true);
            Q_$2$(activity);
            AnalyticsUtil.trackEvent(AnalyticsEvent.SMS_PERMISSION_ALREADY_GRANTED);
            return true;
        }
        AnalyticsUtil.trackEvent(AnalyticsEvent.SMS_PERMISSION_ALREADY_NOT_GRANTED);
        return false;
    }

    /* access modifiers changed from: package-private */
    public final void Q_$2$(Activity activity) {
        if (this.Q_$2$ == null) {
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.setPriority(1000);
            this.Q_$2$ = new O_$v$(this);
            intentFilter.addAction("android.provider.Telephony.SMS_RECEIVED");
            activity.registerReceiver(this.Q_$2$, intentFilter);
        }
    }

    /* access modifiers changed from: package-private */
    public final void G__G_(Activity activity) {
        R$$r_(false);
        O_$v$ o_$v$ = this.Q_$2$;
        if (o_$v$ != null) {
            try {
                activity.unregisterReceiver(o_$v$);
            } catch (Exception e) {
                AnalyticsUtil.reportError(e, "critical", e.getMessage());
            }
            this.Q_$2$ = null;
        }
    }

    /* access modifiers changed from: package-private */
    public final void R$$r_(boolean z) {
        Iterator<t_$J_> it = this.a_$P$.iterator();
        while (it.hasNext()) {
            it.next().setSmsPermission(z);
        }
    }
}
