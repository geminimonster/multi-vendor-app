package com.razorpay;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;
import java.util.Iterator;

final class O_$v$ extends BroadcastReceiver {
    private I$_n_ Q_$2$;

    O_$v$() {
        this.Q_$2$ = null;
    }

    O_$v$(I$_n_ i$_n_) {
        this.Q_$2$ = i$_n_;
    }

    public final void onReceive(Context context, Intent intent) {
        Bundle extras = intent.getExtras();
        if (extras != null) {
            try {
                Object[] objArr = (Object[]) extras.get("pdus");
                if (objArr.length > 0) {
                    SmsMessage createFromPdu = SmsMessage.createFromPdu((byte[]) objArr[0]);
                    String displayOriginatingAddress = createFromPdu.getDisplayOriginatingAddress();
                    String displayMessageBody = createFromPdu.getDisplayMessageBody();
                    if (this.Q_$2$ != null) {
                        Iterator<t_$J_> it = this.Q_$2$.a_$P$.iterator();
                        while (it.hasNext()) {
                            it.next().postSms(displayOriginatingAddress, displayMessageBody);
                        }
                        return;
                    }
                    Intent intent2 = new Intent("com.razorpay.events.SMS_PROCESSED");
                    intent2.putExtra("extra_sender", displayOriginatingAddress);
                    intent2.putExtra("extra_message", displayMessageBody);
                    context.sendBroadcast(intent2);
                }
            } catch (Exception e) {
                AnalyticsUtil.reportError(e, "critical", e.getMessage());
            }
        }
    }
}
