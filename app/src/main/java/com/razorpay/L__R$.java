package com.razorpay;

import android.content.Context;
import android.net.Uri;
import android.os.Build;
import com.facebook.internal.ServerProtocol;
import com.stripe.android.AnalyticsDataFactory;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.json.JSONArray;
import org.json.JSONObject;

final class L__R$ extends r$_Y_ {
    private static String E$_j$ = "3.0.5";
    static int G__G_ = 27;
    static String Q_$2$ = "1.5.16";
    static boolean R$$r_ = true;
    private static L__R$ a_$P$ = null;
    static String d__1_ = "checkout";
    private static String r$_Y_ = "2HujvzmUo2nuRLLqhIHIV4sCEmRw9FIc";
    private ArrayList<String> B$$W$ = new ArrayList<>();
    private Map<String, String> D$_X_ = new HashMap();
    private boolean E$_6$;
    private String J$_0_;
    private boolean L__R$;
    private boolean O_$B_;
    private String Y$_o$;
    private boolean b__J_;
    private boolean c__C_;
    private String f$_G$;
    private int g__v_;
    private boolean l_$w$;
    private String l__d$;

    private L__R$() {
    }

    public static L__R$ L__R$() {
        if (a_$P$ == null) {
            L__R$ l__r$ = new L__R$();
            a_$P$ = l__r$;
            f$_G$.G__G_(l__r$);
        }
        return a_$P$;
    }

    public final boolean h__y_() {
        return this.c__C_;
    }

    public final String K$$z$() {
        return this.Y$_o$;
    }

    public final boolean O__Y_() {
        return this.E$_6$;
    }

    public final boolean J$$A_() {
        return this.l_$w$;
    }

    public final int B_$q$() {
        return this.g__v_;
    }

    public final Map<String, String> r_$Z$() {
        return this.D$_X_;
    }

    public final ArrayList<String> T__j$() {
        return this.B$$W$;
    }

    public final String Y_$B$() {
        return this.J$_0_;
    }

    public final String I$_e_() {
        return this.l__d$;
    }

    public final boolean Q$$U_() {
        return this.L__R$;
    }

    public final String L$$C_() {
        return this.f$_G$;
    }

    public final boolean B$$J$() {
        return this.b__J_;
    }

    public final boolean H$_a_() {
        return this.O_$B_;
    }

    public final void a_$P$(JSONObject jSONObject) {
        try {
            this.B$$W$ = BaseUtils.jsonStringArrayToArrayList((JSONArray) BaseUtils.getJsonValue("checkout.append_keys", jSONObject, (Object) new JSONArray()));
            JSONObject jSONObject2 = (JSONObject) BaseUtils.getJsonValue("checkout.url_config", jSONObject, (Object) new JSONObject());
            Iterator<String> keys = jSONObject2.keys();
            while (keys.hasNext()) {
                String next = keys.next();
                this.D$_X_.put(next, jSONObject2.getString(next));
            }
            this.b__J_ = ((Boolean) BaseUtils.getJsonValue("card_saving.broadcast_receiver_flow", jSONObject, (Object) Boolean.FALSE)).booleanValue();
            this.O_$B_ = ((Boolean) BaseUtils.getJsonValue("card_saving.shared_preferences_flow", jSONObject, (Object) Boolean.FALSE)).booleanValue();
            this.c__C_ = ((Boolean) BaseUtils.getJsonValue("card_saving.local", jSONObject, (Object) Boolean.FALSE)).booleanValue();
            this.Y$_o$ = (String) BaseUtils.getJsonValue("native_loader.color", jSONObject, (Object) "");
            this.E$_6$ = ((Boolean) BaseUtils.getJsonValue("native_loader.enable", jSONObject, (Object) "")).booleanValue();
            this.l_$w$ = ((Boolean) BaseUtils.getJsonValue("retry.enabled", jSONObject, (Object) Boolean.TRUE)).booleanValue();
            this.g__v_ = ((Integer) BaseUtils.getJsonValue("retry.max_count", jSONObject, (Object) -1)).intValue();
            this.f$_G$ = (String) BaseUtils.getJsonValue("back_button.alert_message", jSONObject, (Object) "");
            this.L__R$ = ((Boolean) BaseUtils.getJsonValue("back_button.enable", jSONObject, (Object) Boolean.FALSE)).booleanValue();
            this.l__d$ = (String) BaseUtils.getJsonValue("back_button.positive_text", jSONObject, (Object) "");
            this.J$_0_ = (String) BaseUtils.getJsonValue("back_button.negative_text", jSONObject, (Object) "");
        } catch (Exception e) {
            e.getMessage();
        }
        super.a_$P$(jSONObject);
    }

    static void a_$P$(Context context, String str) {
        String str2;
        if (a_$P$ == null) {
            L__R$ l__r$ = new L__R$();
            a_$P$ = l__r$;
            f$_G$.G__G_(l__r$);
        }
        if (a_$P$.Q_$2$()) {
            HashMap hashMap = new HashMap();
            hashMap.put("AuthKey", r$_Y_);
            hashMap.put("Content-type", "application/json");
            String string = U$_z$.a_$P$(context).getString("rzp_config_version", (String) null);
            if (string == null) {
                string = E$_j$;
            }
            hashMap.put("CurrentSettingVersion", string);
            if (a_$P$ == null) {
                L__R$ l__r$2 = new L__R$();
                a_$P$ = l__r$2;
                f$_G$.G__G_(l__r$2);
            }
            Uri.Builder buildUpon = Uri.parse(a_$P$.G__G_()).buildUpon();
            StringBuilder sb = new StringBuilder("android_");
            sb.append(d__1_);
            Uri.Builder appendQueryParameter = buildUpon.appendQueryParameter("tenant", sb.toString()).appendQueryParameter("sdk_version", Q_$2$).appendQueryParameter("sdk_type", d__1_).appendQueryParameter("magic_enabled", String.valueOf(R$$r_)).appendQueryParameter("sdk_version_code", String.valueOf(G__G_)).appendQueryParameter(AnalyticsDataFactory.FIELD_APP_VERSION, BuildConfig.VERSION_NAME);
            String string2 = U$_z$.a_$P$(context).getString("rzp_config_version", (String) null);
            if (string2 == null) {
                string2 = E$_j$;
            }
            Matcher matcher = Pattern.compile("^(\\d+\\.)(\\d+\\.)(\\d+)$").matcher(string2);
            if (matcher.find()) {
                str2 = matcher.replaceFirst("$1$2*");
            } else {
                str2 = null;
            }
            Uri.Builder appendQueryParameter2 = appendQueryParameter.appendQueryParameter(ServerProtocol.FALLBACK_DIALOG_PARAM_VERSION, str2);
            appendQueryParameter2.appendQueryParameter("merchant_key_id", str).appendQueryParameter("android_version", Build.VERSION.RELEASE).appendQueryParameter("device_id", U$_z$.a_$P$(context).getString("advertising_id", (String) null)).appendQueryParameter("device_manufacturer", Build.MANUFACTURER).appendQueryParameter("device_model", Build.MODEL).appendQueryParameter("network_type", BaseUtils.getDataNetworkType(context).Q_$2$()).appendQueryParameter("cellular_network_type", BaseUtils.getCellularNetworkType(context)).appendQueryParameter("cellular_network_provider", BaseUtils.getCellularNetworkProviderName(context)).appendQueryParameter("app_package_name", context.getApplicationContext().getPackageName()).appendQueryParameter("build_type", BaseUtils.getAppBuildType(context)).appendQueryParameter("magic_version_code", String.valueOf(J$_0_.a_$P$.intValue())).appendQueryParameter("rzpassist_version_code", String.valueOf(J$_0_.G__G_.intValue())).appendQueryParameter("webview_user_agent", BaseUtils.getWebViewUserAgent(context).toString());
            r_$Z$.Q_$2$(appendQueryParameter2.build().toString(), hashMap, new Callback(context) {
                private /* synthetic */ Context R$$r_;

                {
                    this.R$$r_ = r1;
                }

                public final void run(C__D$ c__d$) {
                    String str;
                    try {
                        if (c__d$.R$$r_() == 200) {
                            JSONObject jSONObject = new JSONObject(c__d$.d__1_());
                            Context context = this.R$$r_;
                            U$_z$.G__G_(context).putString("rzp_config_json", jSONObject.toString()).apply();
                            List list = c__d$.G__G_().get("Settingversion");
                            if (list != null && list.size() > 0 && (str = (String) list.get(0)) != null && !str.isEmpty()) {
                                U$_z$.G__G_(this.R$$r_).putString("rzp_config_version", str).apply();
                            }
                        }
                    } catch (Exception unused) {
                    }
                }
            });
        }
    }
}
