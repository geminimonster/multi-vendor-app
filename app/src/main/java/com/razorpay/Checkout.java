package com.razorpay;

import android.app.Activity;
import android.app.Fragment;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.TextUtils;
import android.webkit.CookieManager;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;
import com.facebook.share.internal.ShareConstants;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public class Checkout extends Fragment {
    /* access modifiers changed from: private */
    public static boolean G__G_ = false;
    public static final int INCOMPATIBLE_PLUGIN = 7;
    public static final int INVALID_OPTIONS = 3;
    public static final int NETWORK_ERROR = 2;
    public static final int PAYMENT_CANCELED = 0;
    private static long Q_$2$ = 0;
    /* access modifiers changed from: private */
    public static long R$$r_ = 0;
    public static final int RZP_REQUEST_CODE = 62442;
    public static final int TLS_ERROR = 6;
    private static PaymentData Y$_o$;
    /* access modifiers changed from: private */
    public static WebView d__1_;
    private boolean B$$W$;
    private JSONObject D$_X_;
    private boolean E$_6$;
    private int E$_j$;
    private String a_$P$;
    private Activity b__J_;
    private String r$_Y_;

    public static class Builder {
        private boolean R$$r_;
        private String a_$P$;
        private int d__1_;

        public Builder setKeyId(String str) {
            this.a_$P$ = str;
            return this;
        }

        public Builder setImage(int i) {
            this.d__1_ = i;
            return this;
        }

        public Builder disableFullscreen(boolean z) {
            this.R$$r_ = z;
            return this;
        }

        public Checkout build() {
            Checkout checkout = new Checkout();
            checkout.setFullScreenDisable(this.R$$r_);
            checkout.setImage(this.d__1_);
            String str = this.a_$P$;
            if (str != null) {
                checkout.setKeyID(str);
            }
            return checkout;
        }
    }

    @Deprecated
    public final void setPublicKey(String str) {
        this.a_$P$ = str;
    }

    public final void setImage(int i) {
        this.E$_j$ = i;
    }

    public final void setKeyID(String str) {
        setPublicKey(str);
    }

    public final void setFullScreenDisable(boolean z) {
        this.B$$W$ = z;
    }

    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:21:0x0061 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void open(android.app.Activity r7, org.json.JSONObject r8) {
        /*
            r6 = this;
            java.lang.String r0 = r6.a_$P$
            boolean r0 = android.text.TextUtils.isEmpty(r0)
            if (r0 == 0) goto L_0x000e
            java.lang.String r0 = com.razorpay.BaseUtils.getKeyId(r7)
            r6.a_$P$ = r0
        L_0x000e:
            android.content.SharedPreferences r0 = com.razorpay.U$_z$.a_$P$(r7)
            r1 = 0
            java.lang.String r2 = "advertising_id"
            java.lang.String r0 = r0.getString(r2, r1)
            if (r0 != 0) goto L_0x0023
            com.razorpay.r$_Y_$1 r0 = new com.razorpay.r$_Y_$1
            r0.<init>(r7)
            com.razorpay.AdvertisingIdUtil.R$$r_(r7, r0)
        L_0x0023:
            java.lang.String r0 = r6.a_$P$
            boolean r0 = android.text.TextUtils.isEmpty(r0)
            if (r0 != 0) goto L_0x008e
            if (r8 == 0) goto L_0x0086
            int r0 = r8.length()
            if (r0 == 0) goto L_0x0086
            java.lang.String r0 = "key"
            java.lang.String r2 = r6.a_$P$     // Catch:{ JSONException -> 0x003b }
            r8.put(r0, r2)     // Catch:{ JSONException -> 0x003b }
            goto L_0x0046
        L_0x003b:
            r0 = move-exception
            java.lang.String r2 = r0.getMessage()
            java.lang.String r3 = "warning"
            com.razorpay.AnalyticsUtil.reportError(r0, r3, r2)
        L_0x0046:
            long r2 = java.lang.System.nanoTime()
            android.webkit.WebView r0 = d__1_     // Catch:{ Exception -> 0x0061 }
            if (r0 == 0) goto L_0x0061
            android.webkit.WebView r0 = d__1_     // Catch:{ Exception -> 0x0061 }
            java.lang.Object r0 = r0.getTag()     // Catch:{ Exception -> 0x0061 }
            java.lang.Long r0 = (java.lang.Long) r0     // Catch:{ Exception -> 0x0061 }
            long r4 = r0.longValue()     // Catch:{ Exception -> 0x0061 }
            long r2 = r2 - r4
            Q_$2$ = r2     // Catch:{ Exception -> 0x0061 }
            r0 = 2
            com.razorpay.BaseUtils.nanoTimeToSecondsString(r2, r0)     // Catch:{ Exception -> 0x0061 }
        L_0x0061:
            android.webkit.WebView r0 = d__1_     // Catch:{ Exception -> 0x0066 }
            r0.stopLoading()     // Catch:{ Exception -> 0x0066 }
        L_0x0066:
            d__1_ = r1
            r6.D$_X_ = r8
            java.lang.Class r8 = r7.getClass()
            java.lang.String r8 = r8.getName()
            r6.r$_Y_ = r8
            r6.b__J_ = r7
            android.app.FragmentManager r7 = r7.getFragmentManager()
            android.app.FragmentTransaction r7 = r7.beginTransaction()
            android.app.FragmentTransaction r7 = r7.add(r6, r1)
            r7.commit()
            return
        L_0x0086:
            java.lang.RuntimeException r7 = new java.lang.RuntimeException
            java.lang.String r8 = "Checkout options cannot be null or empty"
            r7.<init>(r8)
            throw r7
        L_0x008e:
            java.lang.RuntimeException r7 = new java.lang.RuntimeException
            java.lang.String r8 = "Please set your Razorpay API key in AndroidManifest.xml"
            r7.<init>(r8)
            throw r7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.razorpay.Checkout.open(android.app.Activity, org.json.JSONObject):void");
    }

    public static void preload(Context context) {
        R$$r_ = 0;
        Q_$2$ = 0;
        G__G_ = false;
        Context applicationContext = context.getApplicationContext();
        WebView webView = new WebView(applicationContext);
        d__1_ = webView;
        BaseUtils.setWebViewSettings(applicationContext, webView, false);
        d__1_.setWebViewClient(new WebViewClient() {
            private long a_$P$;

            public final void onPageStarted(WebView webView, String str, Bitmap bitmap) {
                this.a_$P$ = System.nanoTime();
                if (Checkout.d__1_ == null) {
                    HashMap hashMap = new HashMap();
                    hashMap.put("error_location", "Checkout->Preload()->onPageStarted");
                    AnalyticsUtil.trackEvent(AnalyticsEvent.WEB_VIEW_UNEXPECTED_NULL, (Map<String, Object>) hashMap);
                    return;
                }
                Checkout.d__1_.setTag(Long.valueOf(this.a_$P$));
            }

            public final void onReceivedError(WebView webView, int i, String str, String str2) {
                Checkout.G__G_ = true;
            }

            public final void onReceivedError(WebView webView, WebResourceRequest webResourceRequest, WebResourceError webResourceError) {
                super.onReceivedError(webView, webResourceRequest, webResourceError);
                Checkout.G__G_ = true;
            }

            public final void onPageFinished(WebView webView, String str) {
                long nanoTime = System.nanoTime();
                if (!Checkout.G__G_) {
                    long unused = Checkout.R$$r_ = nanoTime - this.a_$P$;
                    BaseUtils.nanoTimeToSecondsString(Checkout.R$$r_, 2);
                }
                Checkout.Q_$2$();
            }
        });
        d__1_.setWebChromeClient(new WebChromeClient() {
            public final void onProgressChanged(WebView webView, int i) {
            }
        });
        d__1_.loadUrl("https://api.razorpay.com/v1/checkout/public");
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (this.D$_X_ != null) {
            Intent intent = new Intent();
            intent.setComponent(new ComponentName(getActivity().getPackageName(), "com.razorpay.CheckoutActivity"));
            long j = R$$r_;
            if (j > 0) {
                intent.putExtra("PRELOAD_COMPLETE_DURATION", j);
            } else {
                long j2 = Q_$2$;
                if (j2 > 0) {
                    intent.putExtra("PRELOAD_ABORT_DURATION", j2);
                }
            }
            intent.putExtra("OPTIONS", this.D$_X_.toString());
            intent.putExtra(ShareConstants.IMAGE_URL, this.E$_j$);
            intent.putExtra("DISABLE_FULL_SCREEN", this.B$$W$);
            this.D$_X_ = null;
            startActivityForResult(intent, RZP_REQUEST_CODE);
        }
    }

    public void onActivityResult(int i, int i2, Intent intent) {
        String str;
        Bundle extras;
        if (i == 62442) {
            Activity activity = getActivity();
            this.b__J_ = activity;
            this.r$_Y_ = activity.getClass().getName();
            if (intent == null || (extras = intent.getExtras()) == null) {
                str = null;
            } else {
                str = extras.getString("RESULT");
            }
            if (i2 == 0 && (str == null || !str.contains("error"))) {
                str = "Payment Cancelled";
            }
            a_$P$(this.b__J_, str);
            G__G_(i2, str);
            if (i2 == 1) {
                String paymentId = Y$_o$.getPaymentId();
                if (paymentId != null) {
                    this.E$_6$ = true;
                    onSuccess(paymentId);
                }
                if (this.E$_6$) {
                    R$$r_(1, 1);
                }
            } else if (i2 != 4) {
                this.E$_6$ = true;
                onError(i2, str);
                if (this.E$_6$) {
                    R$$r_(i2, 1);
                }
            } else if (getActivity() instanceof ExternalWalletListener) {
                try {
                    ExternalWalletListener externalWalletListener = (ExternalWalletListener) getActivity();
                    String externalWallet = Y$_o$.getExternalWallet();
                    if (!TextUtils.isEmpty(externalWallet)) {
                        externalWalletListener.onExternalWalletSelected(externalWallet, Y$_o$);
                        AnalyticsUtil.trackEvent(AnalyticsEvent.MERCHANT_EXTERNAL_WALLET_SELECTED_CALLED);
                        AnalyticsUtil.postData();
                    }
                } catch (Exception e) {
                    G__G_(this.b__J_, 4, "threw_error", e);
                }
            } else {
                G__G_(this.b__J_, 4, "dne", new Exception());
            }
            try {
                getActivity().getFragmentManager().beginTransaction().remove(this).commit();
            } catch (Exception e2) {
                AnalyticsUtil.reportError(e2, "error", e2.getMessage());
            }
        }
    }

    public void onSuccess(String str) {
        this.E$_6$ = false;
        if (!Q_$2$(str)) {
            Method method = null;
            try {
                method = Class.forName(this.r$_Y_).getMethod("onPaymentSuccess", new Class[]{String.class});
            } catch (Exception e) {
                G__G_(this.b__J_, 1, "dne", e);
            }
            try {
                Object[] objArr = {str};
                if (method != null) {
                    method.invoke(this.b__J_, objArr);
                }
                R$$r_(1, 2);
            } catch (Exception e2) {
                G__G_(this.b__J_, 1, "threw_error", e2);
            }
        }
    }

    public void onError(int i, String str) {
        this.E$_6$ = false;
        if (!Q_$2$(i, str)) {
            Method method = null;
            try {
                method = Class.forName(this.r$_Y_).getMethod("onPaymentError", new Class[]{Integer.TYPE, String.class});
            } catch (Exception e) {
                G__G_(this.b__J_, i, "dne", e);
            }
            try {
                Object[] objArr = {Integer.valueOf(i), str};
                if (method != null) {
                    method.invoke(this.b__J_, objArr);
                }
                R$$r_(i, 2);
            } catch (Exception e2) {
                G__G_(this.b__J_, i, "threw_error", e2);
            }
        }
    }

    private boolean Q_$2$(int i, String str) {
        if (getActivity() instanceof PaymentResultListener) {
            try {
                ((PaymentResultListener) getActivity()).onPaymentError(i, str);
                R$$r_(i, 3);
            } catch (Exception e) {
                G__G_(this.b__J_, i, "threw_error", e);
            }
            return true;
        } else if (!(getActivity() instanceof PaymentResultWithDataListener)) {
            return false;
        } else {
            try {
                ((PaymentResultWithDataListener) getActivity()).onPaymentError(i, str, Y$_o$);
                R$$r_(i, 3);
            } catch (Exception e2) {
                G__G_(this.b__J_, i, "threw_error", e2);
            }
            return true;
        }
    }

    private boolean Q_$2$(String str) {
        if (getActivity() instanceof PaymentResultListener) {
            try {
                ((PaymentResultListener) getActivity()).onPaymentSuccess(str);
                R$$r_(1, 3);
            } catch (Exception e) {
                G__G_(this.b__J_, 1, "threw_error", e);
            }
            return true;
        } else if (!(getActivity() instanceof PaymentResultWithDataListener)) {
            return false;
        } else {
            try {
                ((PaymentResultWithDataListener) getActivity()).onPaymentSuccess(str, Y$_o$);
                R$$r_(1, 3);
            } catch (Exception e2) {
                G__G_(this.b__J_, 1, "threw_error", e2);
            }
            return true;
        }
    }

    private static void R$$r_(int i, int i2) {
        try {
            HashMap hashMap = new HashMap();
            hashMap.put("integration_type", Integer.toString(i2));
            if (i == 1) {
                AnalyticsUtil.trackEvent(AnalyticsEvent.MERCHANT_ON_SUCCESS_CALLED, (Map<String, Object>) hashMap);
            } else {
                AnalyticsUtil.trackEvent(AnalyticsEvent.MERCHANT_ON_ERROR_CALLED, (Map<String, Object>) hashMap);
            }
            AnalyticsUtil.postData();
        } catch (Exception e) {
            AnalyticsUtil.reportError(e, "warning", e.getMessage());
        }
    }

    private static void G__G_(int i, String str) {
        try {
            AnalyticsUtil.addProperty("onActivityResult result", new AnalyticsProperty(str, AnalyticsProperty$Q_$2$.ORDER));
            AnalyticsUtil.addProperty("onActivityResult resultCode", new AnalyticsProperty(String.valueOf(i), AnalyticsProperty$Q_$2$.ORDER));
            if (i == 1) {
                AnalyticsUtil.trackEvent(AnalyticsEvent.CALLING_ON_SUCCESS);
            } else if (i == 4) {
                AnalyticsUtil.trackEvent(AnalyticsEvent.CALLING_EXTERNAL_WALLET_SELECTED);
            } else {
                AnalyticsUtil.trackEvent(AnalyticsEvent.CALLING_ON_ERROR);
            }
            AnalyticsUtil.postData();
        } catch (Exception e) {
            AnalyticsUtil.reportError(e, "warning", e.getMessage());
        }
    }

    private static void G__G_(Activity activity, int i, String str, Exception exc) {
        String str2;
        String str3;
        if (i == 1) {
            str3 = "onPaymentSuccess";
            str2 = "success";
        } else if (i == 4) {
            str3 = "onExternalWalletSelected";
            str2 = "redirected";
        } else {
            str3 = "onPaymentError";
            str2 = "error";
        }
        try {
            HashMap hashMap = new HashMap();
            hashMap.put("event_details", exc.getMessage());
            hashMap.put("event_type", exc.getMessage());
            hashMap.put("payment_status", str2);
            AnalyticsUtil.trackEvent(AnalyticsEvent.HANDOVER_ERROR, (Map<String, Object>) hashMap);
            AnalyticsUtil.postData();
        } catch (Exception e) {
            AnalyticsUtil.reportError(e, "error", e.getMessage());
        }
        if (str.equals("dne")) {
            if (i == 4) {
                Toast.makeText(activity, "Error: ExternalWalletListener probably not implemented in your activity", 0).show();
            } else {
                StringBuilder sb = new StringBuilder("Error: ");
                sb.append(str3);
                sb.append(" probably not implemented in your activity");
                Toast.makeText(activity, sb.toString(), 0).show();
            }
        } else if (str.equals("threw_error")) {
            StringBuilder sb2 = new StringBuilder("Your ");
            sb2.append(str3);
            sb2.append(" method is throwing an error. Wrap the entire code of the method inside a try catch.");
            Toast.makeText(activity, sb2.toString(), 0).show();
        }
        AnalyticsUtil.reportError(exc, "error", exc.getMessage());
    }

    private static void a_$P$(Activity activity, String str) {
        PaymentData paymentData = new PaymentData();
        Y$_o$ = paymentData;
        paymentData.a_$P$(U$_z$.a_$P$(activity).getString("rzp_user_contact", (String) null));
        Y$_o$.d__1_(U$_z$.a_$P$(activity).getString("rzp_user_email", (String) null));
        try {
            JSONObject jSONObject = new JSONObject(str);
            Y$_o$.d__1_(jSONObject);
            if (jSONObject.has("razorpay_payment_id")) {
                Y$_o$.G__G_(jSONObject.getString("razorpay_payment_id"));
            }
            if (jSONObject.has("razorpay_order_id")) {
                Y$_o$.R$$r_(jSONObject.getString("razorpay_order_id"));
            }
            if (jSONObject.has("razorpay_signature")) {
                Y$_o$.Q_$2$(jSONObject.getString("razorpay_signature"));
            }
            if (jSONObject.has("external_wallet")) {
                Y$_o$.r$_Y_(jSONObject.getString("external_wallet"));
            }
        } catch (JSONException e) {
            AnalyticsUtil.reportError(e, "error", e.getMessage());
        }
    }

    public static void handleActivityResult(Activity activity, int i, int i2, Intent intent, PaymentResultWithDataListener paymentResultWithDataListener, ExternalWalletListener externalWalletListener) {
        String str;
        Bundle extras;
        if (i == 62442) {
            if (intent == null || (extras = intent.getExtras()) == null) {
                str = null;
            } else {
                str = extras.getString("RESULT");
            }
            if (i2 == 0) {
                str = "Payment Cancelled";
            }
            a_$P$(activity, str);
            G__G_(i2, str);
            if (i2 == 1) {
                try {
                    paymentResultWithDataListener.onPaymentSuccess(Y$_o$.getPaymentId(), Y$_o$);
                } catch (Exception e) {
                    G__G_(activity, i2, "threw_error", e);
                }
            } else if (i2 != 4) {
                try {
                    paymentResultWithDataListener.onPaymentError(i2, str, Y$_o$);
                } catch (Exception e2) {
                    G__G_(activity, i2, "threw_error", e2);
                }
            } else if (externalWalletListener != null) {
                try {
                    externalWalletListener.onExternalWalletSelected(Y$_o$.getExternalWallet(), Y$_o$);
                } catch (Exception e3) {
                    G__G_(activity, i2, "threw_error", e3);
                }
            }
        }
    }

    public static void clearUserData(Context context) {
        SharedPreferences.Editor G__G_2 = U$_z$.G__G_(context);
        G__G_2.putString("rzp_user_contact", (String) null);
        G__G_2.commit();
        SharedPreferences.Editor G__G_3 = U$_z$.G__G_(context);
        G__G_3.putString("rzp_user_email", (String) null);
        G__G_3.commit();
        U$_z$.Q_$2$(context).putString("rzp_device_token", (String) null).apply();
        CookieManager.getInstance().setCookie("https://api.razorpay.com", "razorpay_api_session=");
    }

    static /* synthetic */ void Q_$2$() {
        try {
            d__1_.stopLoading();
        } catch (Exception unused) {
        }
        d__1_ = null;
    }
}
