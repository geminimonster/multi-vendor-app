package com.razorpay;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;
import java.util.concurrent.LinkedBlockingQueue;

final class AdvertisingIdUtil$R$$r_ implements ServiceConnection {
    private boolean Q_$2$;
    private final LinkedBlockingQueue<IBinder> a_$P$;

    public final void onServiceDisconnected(ComponentName componentName) {
    }

    private AdvertisingIdUtil$R$$r_() {
        this.Q_$2$ = false;
        this.a_$P$ = new LinkedBlockingQueue<>(1);
    }

    /* synthetic */ AdvertisingIdUtil$R$$r_(byte b) {
        this();
    }

    public final void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        try {
            this.a_$P$.put(iBinder);
        } catch (InterruptedException unused) {
        }
    }

    public final IBinder Q_$2$() throws InterruptedException {
        if (!this.Q_$2$) {
            this.Q_$2$ = true;
            return this.a_$P$.take();
        }
        throw new IllegalStateException();
    }
}
