package com.razorpay;

import android.webkit.JavascriptInterface;

class Y$_o$ implements d__1_ {
    /* access modifiers changed from: private */
    public boolean G__G_ = true;
    c__C_ R$$r_;
    private int d__1_;

    interface d__1_ {
        void d__1_();
    }

    Y$_o$(c__C_ c__c_, int i) {
        this.R$$r_ = c__c_;
        this.d__1_ = i;
    }

    @JavascriptInterface
    public void invokePopup(final String str) {
        this.R$$r_.invokePopup(str);
        this.R$$r_.isWebViewSafeOnUI(this.d__1_, new d__1_() {
            public final void d__1_() {
                Y$_o$.this.R$$r_.invokePopup(str);
            }
        });
    }

    @JavascriptInterface
    public void onCheckoutBackPress() {
        this.R$$r_.onCheckoutBackPress();
        this.R$$r_.isWebViewSafeOnUI(this.d__1_, new d__1_() {
            public final void d__1_() {
                Y$_o$.this.R$$r_.onCheckoutBackPress();
            }
        });
    }

    @JavascriptInterface
    public final void setAppToken(final String str) {
        this.R$$r_.setAppToken(str);
        this.R$$r_.isWebViewSafeOnUI(this.d__1_, new d__1_() {
            public final void d__1_() {
                Y$_o$.this.R$$r_.setAppToken(str);
            }
        });
    }

    @JavascriptInterface
    public final void setDeviceToken(final String str) {
        this.R$$r_.isWebViewSafeOnUI(this.d__1_, new d__1_() {
            public final void d__1_() {
                Y$_o$.this.R$$r_.setDeviceToken(str);
            }
        });
    }

    @JavascriptInterface
    public final void callNativeIntent(final String str) {
        this.R$$r_.isWebViewSafeOnUI(this.d__1_, new d__1_() {
            public final void d__1_() {
                Y$_o$.this.R$$r_.callNativeIntent(str, (String) null);
            }
        });
    }

    @JavascriptInterface
    public final void callNativeIntent(final String str, final String str2) {
        this.R$$r_.isWebViewSafeOnUI(this.d__1_, new d__1_() {
            public final void d__1_() {
                Y$_o$.this.R$$r_.callNativeIntent(str, str2);
            }
        });
    }

    @JavascriptInterface
    public final void setPaymentID(final String str) {
        this.R$$r_.isWebViewSafeOnUI(this.d__1_, new d__1_() {
            public final void d__1_() {
                Y$_o$.this.R$$r_.setPaymentID(str);
            }
        });
    }

    @JavascriptInterface
    public final void setCheckoutBody(final String str) {
        this.R$$r_.isWebViewSafeOnUI(this.d__1_, new d__1_() {
            public final void d__1_() {
                Y$_o$.this.R$$r_.setCheckoutBody(str);
            }
        });
    }

    @JavascriptInterface
    public final void setMerchantOptions(final String str) {
        this.R$$r_.isWebViewSafeOnUI(this.d__1_, new d__1_() {
            public final void d__1_() {
                Y$_o$.this.R$$r_.setMerchantOptions(str);
            }
        });
    }

    @JavascriptInterface
    public final void onsubmit(final String str) {
        this.R$$r_.isWebViewSafeOnUI(this.d__1_, new d__1_() {
            public final void d__1_() {
                Y$_o$.this.R$$r_.onSubmit(str);
            }
        });
    }

    @JavascriptInterface
    public final void onfault(final String str) {
        this.R$$r_.isWebViewSafeOnUI(this.d__1_, new d__1_() {
            public final void d__1_() {
                Y$_o$.this.R$$r_.onFault(str);
            }
        });
    }

    @JavascriptInterface
    public final void oncomplete(final String str) {
        this.R$$r_.isWebViewSafeOnUI(this.d__1_, new d__1_() {
            public final void d__1_() {
                Y$_o$.this.R$$r_.onComplete(str);
            }
        });
    }

    @JavascriptInterface
    public final void setDimensions(final int i, final int i2) {
        this.R$$r_.isWebViewSafeOnUI(this.d__1_, new d__1_() {
            public final void d__1_() {
                Y$_o$.this.R$$r_.setDimensions(i, i2);
            }
        });
    }

    @JavascriptInterface
    public final void ondismiss() {
        this.R$$r_.isWebViewSafeOnUI(this.d__1_, new d__1_() {
            public final void d__1_() {
                Y$_o$.this.R$$r_.onDismiss();
            }
        });
    }

    @JavascriptInterface
    public final void ondismiss(final String str) {
        this.R$$r_.isWebViewSafeOnUI(this.d__1_, new d__1_() {
            public final void d__1_() {
                Y$_o$.this.R$$r_.onDismiss(str);
            }
        });
    }

    @JavascriptInterface
    public final void requestExtraAnalyticsData() {
        this.R$$r_.isWebViewSafeOnUI(this.d__1_, new d__1_() {
            public final void d__1_() {
                Y$_o$.this.R$$r_.requestExtraAnalyticsData();
            }
        });
    }

    @JavascriptInterface
    public final void onerror(final String str) {
        this.R$$r_.isWebViewSafeOnUI(this.d__1_, new d__1_() {
            public final void d__1_() {
                Y$_o$.this.R$$r_.onError(str);
            }
        });
    }

    @JavascriptInterface
    public final boolean isUserRegisteredOnUPI(final String str) {
        this.R$$r_.isWebViewSafe(this.d__1_, new d__1_() {
            public final void d__1_() {
                Y$_o$ y$_o$ = Y$_o$.this;
                boolean unused = y$_o$.G__G_ = y$_o$.R$$r_.isUserRegisteredOnUPI(str);
            }
        });
        return this.G__G_;
    }

    @JavascriptInterface
    public final void relay(final String str) {
        this.R$$r_.isWebViewSafeOnUI(this.d__1_, new d__1_() {
            public final void d__1_() {
                Y$_o$.this.R$$r_.sendDataToWebView(2, str);
            }
        });
    }

    @JavascriptInterface
    public final void toast(final String str, final int i) {
        this.R$$r_.isWebViewSafeOnUI(this.d__1_, new d__1_() {
            public final void d__1_() {
                Y$_o$.this.R$$r_.toast(str, i);
            }
        });
    }

    @JavascriptInterface
    public final void showAlertDialog(final String str, final String str2, final String str3) {
        this.R$$r_.isWebViewSafeOnUI(this.d__1_, new d__1_() {
            public final void d__1_() {
                Y$_o$.this.R$$r_.showAlertDialog(str, str2, str3);
            }
        });
    }

    /* access modifiers changed from: package-private */
    public final void R$$r_(d__1_ d__1_2) {
        this.R$$r_.isWebViewSafeOnUI(this.d__1_, d__1_2);
    }

    @JavascriptInterface
    public final void onload() {
        this.R$$r_.isWebViewSafeOnUI(this.d__1_, this);
    }

    public final void d__1_() {
        this.R$$r_.onLoad();
    }
}
