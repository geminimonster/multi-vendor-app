package com.razorpay;

import com.razorpay.Y$_o$;

interface c__C_ {
    void callNativeIntent(String str, String str2);

    void invokePopup(String str);

    boolean isUserRegisteredOnUPI(String str);

    void isWebViewSafe(int i, Y$_o$.d__1_ d__1_);

    void isWebViewSafeOnUI(int i, Y$_o$.d__1_ d__1_);

    void onCheckoutBackPress();

    void onComplete(String str);

    void onDismiss();

    void onDismiss(String str);

    void onError(String str);

    void onFault(String str);

    void onLoad();

    void onSubmit(String str);

    void requestExtraAnalyticsData();

    void sendDataToWebView(int i, String str);

    void setAppToken(String str);

    void setCheckoutBody(String str);

    void setDeviceToken(String str);

    void setDimensions(int i, int i2);

    void setMerchantOptions(String str);

    void setPaymentID(String str);

    void showAlertDialog(String str, String str2, String str3);

    void toast(String str, int i);
}
