package com.razorpay;

import android.app.Activity;
import android.webkit.WebView;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

class Y_$B$ extends l__d$ {
    private RzpAssist G__G_;
    private RzpAssist R$$r_;
    private boolean d__1_ = true;

    public Y_$B$(Activity activity, l__d$$R$$r_ l__d__r__r_) {
        super(activity, l__d__r__r_);
    }

    public void setUpAddOn() {
        RzpAssist rzpAssist;
        RzpAssist rzpAssist2 = new RzpAssist(this.a_$P$, this.activity, this.view.getWebView(1), L__R$.d__1_, L__R$.G__G_, L__R$.Q_$2$);
        this.R$$r_ = rzpAssist2;
        rzpAssist2.d__1_(true);
        RzpAssist rzpAssist3 = new RzpAssist(this.a_$P$, this.activity, this.view.getWebView(2), L__R$.d__1_, L__R$.G__G_, L__R$.Q_$2$);
        this.G__G_ = rzpAssist3;
        rzpAssist3.d__1_(true);
        if (!(this.Q_$2$.d__1_() == null || (rzpAssist = this.R$$r_) == null)) {
            rzpAssist.setOtpElfPreferences(this.Q_$2$.d__1_());
        }
        super.setUpAddOn();
    }

    /* access modifiers changed from: protected */
    public void enableAddon(JSONObject jSONObject) {
        super.enableAddon(jSONObject);
        try {
            if (jSONObject.has("otpelf")) {
                boolean z = jSONObject.getBoolean("otpelf");
                this.d__1_ = z;
                if (this.G__G_ != null) {
                    this.G__G_.d__1_(z);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void onProgressChanges(int i, int i2) {
        RzpAssist rzpAssist;
        if (i == 1) {
            RzpAssist rzpAssist2 = this.R$$r_;
            if (rzpAssist2 != null) {
                rzpAssist2.onProgressChanged(i2);
            }
        } else if (i == 2 && (rzpAssist = this.G__G_) != null && this.d__1_) {
            rzpAssist.onProgressChanged(i2);
        }
        super.onProgressChanges(i, i2);
    }

    public void onPageStarted(int i, WebView webView, String str) {
        super.onPageStarted(i, webView, str);
        if (i == 1) {
            RzpAssist rzpAssist = this.R$$r_;
            if (rzpAssist != null) {
                rzpAssist.onPageStarted(webView, str);
            }
        } else if (i == 2) {
            RzpAssist rzpAssist2 = this.G__G_;
            if (rzpAssist2 != null && this.d__1_) {
                rzpAssist2.onPageStarted(webView, str);
            }
            if (this.view.isWebViewVisible(2)) {
                g__v_.Q_$2$(this.activity);
            }
        }
    }

    public void onPageFinished(int i, WebView webView, String str) {
        super.onPageFinished(i, webView, str);
        if (i == 2) {
            RzpAssist rzpAssist = this.G__G_;
            if (rzpAssist != null && this.d__1_) {
                rzpAssist.onPageFinished(webView, str);
            }
            if (this.view.isWebViewVisible(2)) {
                g__v_.a_$P$();
            }
        }
    }

    /* access modifiers changed from: protected */
    public void primaryWebviewPageFinished(String str, WebView webView) {
        super.primaryWebviewPageFinished(str, webView);
        RzpAssist rzpAssist = this.R$$r_;
        if (rzpAssist != null) {
            rzpAssist.onPageFinished(webView, str);
        }
    }

    public void onRequestPermissionsResult(int i, String[] strArr, int[] iArr) {
        super.onRequestPermissionsResult(i, strArr, iArr);
        RzpAssist rzpAssist = this.R$$r_;
        if (rzpAssist != null) {
            rzpAssist.R$$r_(i, iArr);
        }
    }

    /* access modifiers changed from: protected */
    public void addOnFlowEnd() {
        RzpAssist rzpAssist = this.R$$r_;
        if (rzpAssist != null) {
            rzpAssist.paymentFlowEnd();
        }
        RzpAssist rzpAssist2 = this.G__G_;
        if (rzpAssist2 != null) {
            rzpAssist2.paymentFlowEnd();
        }
        super.addOnFlowEnd();
    }

    public void backPressed(Map<String, Object> map) {
        RzpAssist rzpAssist = this.R$$r_;
        if (rzpAssist != null) {
            map.put("current_loading_url_primary_webview", rzpAssist.G__G_());
            map.put("last_loaded_url_primary_webview", this.R$$r_.d__1_());
        }
        RzpAssist rzpAssist2 = this.G__G_;
        if (rzpAssist2 != null) {
            map.put("current_loading_url_secondary_webview", rzpAssist2.G__G_());
            map.put("last_loaded_url_secondary_webview", this.G__G_.d__1_());
        }
        super.backPressed(map);
    }

    public void setPaymentID(String str) {
        RzpAssist rzpAssist = this.R$$r_;
        if (rzpAssist != null) {
            rzpAssist.G__G_(str);
        }
        super.setPaymentID(str);
    }

    /* access modifiers changed from: protected */
    public void addAnalyticsData(JSONObject jSONObject) {
        try {
            if (this.R$$r_ != null) {
                this.R$$r_.G__G_(jSONObject);
                jSONObject.put("razorpay_otp", this.R$$r_.Q_$2$());
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        super.addAnalyticsData(jSONObject);
    }

    /* access modifiers changed from: protected */
    public void helpersReset() {
        super.helpersReset();
        RzpAssist rzpAssist = this.R$$r_;
        if (rzpAssist != null) {
            rzpAssist.reset();
        }
        RzpAssist rzpAssist2 = this.G__G_;
        if (rzpAssist2 != null && this.d__1_) {
            rzpAssist2.reset();
        }
    }
}
