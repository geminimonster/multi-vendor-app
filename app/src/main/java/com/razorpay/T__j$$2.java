package com.razorpay;

import android.app.Activity;

class T__j$$2 implements Callback {
    private /* synthetic */ String G__G_;
    private /* synthetic */ Object a_$P$$5c24f078;

    public T__j$$2(Object obj, String str) {
        this.a_$P$$5c24f078 = obj;
        this.G__G_ = str;
    }

    public final void run(C__D$ c__d$) {
        String decryptFile;
        if (c__d$.d__1_() != null && (decryptFile = BaseUtils.decryptFile(c__d$.d__1_())) != null) {
            if (BaseUtils.storeFileInInternal((Activity) ((Class) O__Y_.d__1_(18, 18, 0)).getField("Q_$2$").get(this.a_$P$$5c24f078), BaseUtils.getVersionedAssetName(this.G__G_, f$_G$.L__R$().l_$w$()), c__d$.d__1_())) {
                Object obj = this.a_$P$$5c24f078;
                try {
                    Object[] objArr = new Object[2];
                    objArr[1] = decryptFile;
                    objArr[0] = obj;
                    ((Class) O__Y_.d__1_(18, 18, 0)).getDeclaredMethod("G__G_", new Class[]{(Class) O__Y_.d__1_(18, 18, 0), String.class}).invoke((Object) null, objArr);
                    BaseUtils.updateLocalVersion((Activity) ((Class) O__Y_.d__1_(18, 18, 0)).getField("Q_$2$").get(this.a_$P$$5c24f078), (String) ((Class) O__Y_.d__1_(18, 18, 0)).getField("R$$r_").get((Object) null), this.G__G_);
                } catch (Throwable th) {
                    Throwable cause = th.getCause();
                    if (cause != null) {
                        throw cause;
                    }
                    throw th;
                }
            }
        }
    }
}
