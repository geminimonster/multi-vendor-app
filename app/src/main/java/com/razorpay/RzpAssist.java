package com.razorpay;

import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.view.inputmethod.InputMethodManager;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.widget.Toast;
import com.facebook.internal.ServerProtocol;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONObject;

public final class RzpAssist implements t_$J_ {
    private boolean B$$W$ = false;
    /* access modifiers changed from: private */
    public Activity D$_X_;
    private Object E$_6$$5c24f078;
    /* access modifiers changed from: private */
    public boolean E$_j$ = false;
    private boolean G__G_ = false;
    private String J$_0_ = "standalone";
    private int L__R$;
    private String O_$B_;
    private String Q_$2$ = "";
    private String R$$r_ = "";
    private boolean Y$_o$ = false;
    /* access modifiers changed from: private */
    public WebView a_$P$;
    private I$_n_ b__J_;
    private JSONObject c__C_ = new JSONObject();
    private long d__1_;
    private boolean f$_G$ = false;
    /* access modifiers changed from: private */
    public boolean g__v_ = false;
    private JSONObject l_$w$ = new JSONObject();
    private String l__d$;
    private String r$_Y_;

    public RzpAssist(String str, Activity activity, WebView webView, String str2, int i, String str3) {
        Throwable cause;
        if (f$_G$.L__R$().D$_X_().booleanValue()) {
            if (str == null || str.isEmpty()) {
                throw new RuntimeException("merchantKey cannot be null or empty");
            }
            this.J$_0_ = str2;
            this.L__R$ = i;
            if (str2.equals("standalone")) {
                AnalyticsUtil.setup(activity, str, str2, i, str3);
            }
            this.a_$P$ = webView;
            this.r$_Y_ = str;
            this.D$_X_ = activity;
            try {
                Object newInstance = ((Class) O__Y_.d__1_(18, 18, 0)).getDeclaredConstructor(new Class[]{Activity.class}).newInstance(new Object[]{activity});
                this.E$_6$$5c24f078 = newInstance;
                try {
                    ((Class) O__Y_.d__1_(18, 18, 0)).getDeclaredMethod("Q_$2$", (Class[]) null).invoke(newInstance, (Object[]) null);
                    if (I$_n_.d__1_ == null) {
                        I$_n_.d__1_ = new I$_n_();
                    }
                    I$_n_ i$_n_ = I$_n_.d__1_;
                    this.b__J_ = i$_n_;
                    i$_n_.a_$P$.add(this);
                    this.b__J_.a_$P$(this.D$_X_);
                    this.a_$P$.addJavascriptInterface(this, "OTPElfBridge");
                    this.a_$P$.getSettings().setUseWideViewPort(true);
                    AnalyticsUtil.addProperty("OTPElf Version", new AnalyticsProperty(BaseUtils.getLocalVersion(activity, (String) ((Class) O__Y_.d__1_(18, 18, 0)).getField("R$$r_").get((Object) null)), AnalyticsProperty$Q_$2$.ORDER));
                } catch (Throwable th) {
                    if (cause != null) {
                        throw cause;
                    }
                    throw th;
                }
            } finally {
                cause = th.getCause();
                if (cause != null) {
                    throw cause;
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final void G__G_(JSONObject jSONObject) {
        this.c__C_ = jSONObject;
    }

    public final void setOtpElfPreferences(JSONObject jSONObject) {
        this.l_$w$ = jSONObject;
    }

    public final void onPageFinished(WebView webView, String str) {
        AnalyticsUtil.trackPageLoadEnd(str, System.nanoTime() - this.d__1_);
        this.R$$r_ = str;
        this.Q_$2$ = "";
        if (f$_G$.L__R$().D$_X_().booleanValue() && !this.f$_G$) {
            try {
                JSONObject r$_Y_2 = f$_G$.L__R$().r$_Y_();
                r$_Y_2.put("merchant_key", this.r$_Y_);
                r$_Y_2.put("otp_permission", this.G__G_);
                JSONObject jSONObject = new JSONObject();
                jSONObject.put("type", this.J$_0_);
                jSONObject.put("version_code", this.L__R$);
                r$_Y_2.put(ServerProtocol.DIALOG_PARAM_SDK_VERSION, jSONObject);
                JSONObject jSONObject2 = new JSONObject();
                jSONObject2.put("type", "rzpassist");
                jSONObject2.put("version_code", J$_0_.G__G_.intValue());
                r$_Y_2.put("plugin", jSONObject2);
                r$_Y_2.put("payment_data", this.c__C_);
                r$_Y_2.put("preferences", this.l_$w$);
                StringBuilder sb = new StringBuilder("window.__rzp_options = ");
                sb.append(r$_Y_2.toString());
                String obj = sb.toString();
                this.a_$P$.loadUrl(String.format("javascript: %s", new Object[]{obj}));
            } catch (Exception unused) {
            }
            try {
                Object invoke = ((Class) O__Y_.d__1_(18, 18, 0)).getDeclaredMethod("d__1_", (Class[]) null).invoke(this.E$_6$$5c24f078, (Object[]) null);
                this.a_$P$.loadUrl(String.format("javascript: %s", new Object[]{invoke}));
                String str2 = this.l__d$;
                if (str2 != null) {
                    String format = String.format("OTPElf.elfBridge.setSms(%s)", new Object[]{str2});
                    this.a_$P$.loadUrl(String.format("javascript: %s", new Object[]{format}));
                    this.l__d$ = null;
                }
                this.f$_G$ = true;
            } catch (Throwable th) {
                Throwable cause = th.getCause();
                if (cause != null) {
                    throw cause;
                }
                throw th;
            }
        }
    }

    public final void onProgressChanged(int i) {
        if (!f$_G$.L__R$().D$_X_().booleanValue()) {
        }
    }

    public final void onPageStarted(WebView webView, String str) {
        AnalyticsUtil.trackPageLoadStart(str);
        this.d__1_ = System.nanoTime();
        this.Q_$2$ = str;
        this.f$_G$ = false;
    }

    public final void paymentFlowEnd() {
        if (this.J$_0_.equals("standalone")) {
            AnalyticsUtil.postData();
        }
        if (f$_G$.L__R$().D$_X_().booleanValue()) {
            this.b__J_.G__G_(this.D$_X_);
            try {
                this.b__J_.a_$P$.remove(this);
            } catch (Exception unused) {
            }
        }
    }

    public final void postSms(String str, String str2) {
        if (this.Y$_o$) {
            try {
                JSONObject jSONObject = new JSONObject();
                jSONObject.put("sender", str);
                jSONObject.put("message", str2);
                String jSONObject2 = jSONObject.toString();
                this.l__d$ = jSONObject2;
                String format = String.format("OTPElf.elfBridge.setSms(%s)", new Object[]{jSONObject2});
                this.a_$P$.loadUrl(String.format("javascript: %s", new Object[]{format}));
            } catch (Exception unused) {
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final void G__G_(String str) {
        this.O_$B_ = str;
    }

    /* access modifiers changed from: package-private */
    public final void R$$r_(int i, int[] iArr) {
        I$_n_ i$_n_ = this.b__J_;
        Activity activity = this.D$_X_;
        if (i == 1) {
            if (iArr.length <= 0 || iArr[0] != 0) {
                i$_n_.R$$r_(false);
                AnalyticsUtil.trackEvent(AnalyticsEvent.SMS_PERMISSION_NOW_DENIED);
                return;
            }
            i$_n_.R$$r_(true);
            i$_n_.Q_$2$(activity);
            AnalyticsUtil.trackEvent(AnalyticsEvent.SMS_PERMISSION_NOW_GRANTED);
        }
    }

    /* access modifiers changed from: package-private */
    public final String d__1_() {
        return this.R$$r_;
    }

    /* access modifiers changed from: package-private */
    public final String G__G_() {
        return this.Q_$2$;
    }

    /* access modifiers changed from: package-private */
    public final boolean Q_$2$() {
        return this.E$_j$;
    }

    /* access modifiers changed from: package-private */
    public final void d__1_(boolean z) {
        this.Y$_o$ = z;
    }

    @JavascriptInterface
    public final void setUseWideViewPort(final boolean z) {
        this.D$_X_.runOnUiThread(new Runnable() {
            public final void run() {
                RzpAssist.this.a_$P$.getSettings().setUseWideViewPort(z);
            }
        });
    }

    @JavascriptInterface
    public final void openKeyboard() {
        this.D$_X_.runOnUiThread(new Runnable() {
            public final void run() {
                ((InputMethodManager) RzpAssist.this.D$_X_.getSystemService("input_method")).showSoftInput(RzpAssist.this.a_$P$, 0);
            }
        });
    }

    @JavascriptInterface
    public final void toast(final String str) {
        this.D$_X_.runOnUiThread(new Runnable() {
            public final void run() {
                Toast.makeText(RzpAssist.this.D$_X_, str, 1).show();
            }
        });
    }

    @JavascriptInterface
    public final void trackEvent(String str, String str2) {
        try {
            AnalyticsEvent analyticsEvent = AnalyticsEvent.JS_EVENT;
            analyticsEvent.setEventName(str);
            AnalyticsUtil.trackEvent(analyticsEvent, new JSONObject(str2));
        } catch (Exception unused) {
        }
    }

    @JavascriptInterface
    public final void trackEvent(String str) {
        AnalyticsEvent analyticsEvent = AnalyticsEvent.JS_EVENT;
        analyticsEvent.setEventName(str);
        AnalyticsUtil.trackEvent(analyticsEvent);
    }

    @JavascriptInterface
    public final void onOtpParsed(final String str) {
        this.D$_X_.runOnUiThread(new Runnable() {
            public final void run() {
                try {
                    JSONObject jSONObject = new JSONObject(str);
                    OTP otp = new OTP(jSONObject.getString("otp"), jSONObject.getString("sender"), jSONObject.getString("bank"));
                    HashMap hashMap = new HashMap();
                    hashMap.put("sender", otp.G__G_);
                    if (otp.G__G_.contains("RZRPAY")) {
                        boolean unused = RzpAssist.this.E$_j$ = true;
                        hashMap.put("razorpay_otp", Boolean.TRUE);
                    } else {
                        hashMap.put("razorpay_otp", Boolean.FALSE);
                        boolean unused2 = RzpAssist.this.g__v_ = true;
                        AnalyticsUtil.addProperty("payment_otp_received", new AnalyticsProperty(true, AnalyticsProperty$Q_$2$.PAYMENT));
                    }
                    AnalyticsUtil.trackEvent(AnalyticsEvent.OTP_RECEIVED, (Map<String, Object>) hashMap);
                } catch (Exception unused3) {
                }
            }
        });
    }

    @JavascriptInterface
    public final void copyToClipboard(String str) {
        ((ClipboardManager) this.D$_X_.getSystemService("clipboard")).setPrimaryClip(ClipData.newPlainText("rzp_clip_data", str));
    }

    public final void setSmsPermission(boolean z) {
        this.G__G_ = z;
        AnalyticsUtil.addProperty("otp_autoreading_access", new AnalyticsProperty(z, AnalyticsProperty$Q_$2$.ORDER));
    }

    public final void reset() {
        try {
            String constructBasicAuth = BaseUtils.constructBasicAuth(this.r$_Y_);
            HashMap hashMap = new HashMap();
            hashMap.put("Authorization", "Basic ".concat(String.valueOf(constructBasicAuth)));
            hashMap.put("Content-Type", "application/json");
            if (this.O_$B_ != null) {
                StringBuilder sb = new StringBuilder("https://api.razorpay.com/v1/payments/");
                sb.append(this.O_$B_);
                sb.append("/metadata");
                r_$Z$.d__1_(sb.toString(), E$_j$.d__1_(this.g__v_).toString(), hashMap, new Callback() {
                    public final void run(C__D$ c__d$) {
                    }
                });
            }
        } catch (Exception e) {
            AnalyticsUtil.reportError(e, "critical", e.getMessage());
        }
        this.R$$r_ = "";
        this.Q_$2$ = "";
        this.g__v_ = false;
    }
}
