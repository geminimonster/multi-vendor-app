package com.google.firebase.auth.internal;

import com.google.android.gms.internal.firebase_auth.zzff;
import com.google.firebase.auth.FirebaseUser;

/* compiled from: com.google.firebase:firebase-auth@@19.4.0 */
public interface zzb {
    void zza(zzff zzff, FirebaseUser firebaseUser);
}
