package com.google.firebase.auth.api.internal;

/* compiled from: com.google.firebase:firebase-auth@@19.4.0 */
public interface zzfr<T> extends zzfo {
    void zza(T t);
}
