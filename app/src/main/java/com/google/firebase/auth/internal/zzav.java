package com.google.firebase.auth.internal;

/* compiled from: com.google.firebase:firebase-auth@@19.4.0 */
public final class zzav {
    private static zzav zza = new zzav();

    private zzav() {
    }

    public static boolean zzb() {
        return true;
    }

    public static zzav zza() {
        return zza;
    }
}
