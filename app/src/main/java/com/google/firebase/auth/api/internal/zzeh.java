package com.google.firebase.auth.api.internal;

import android.os.DeadObjectException;
import com.google.android.gms.common.api.Api;

/* compiled from: com.google.firebase:firebase-auth@@19.4.0 */
public interface zzeh extends Api.Client {
    zzer zza() throws DeadObjectException;
}
