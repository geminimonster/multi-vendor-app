package com.google.android.gms.internal.firebase_auth;

import java.io.InputStream;

/* compiled from: com.google.firebase:firebase-auth@@19.4.0 */
public interface zzkb<MessageType> {
    MessageType zza(zzhh zzhh, zzht zzht) throws zzir;

    MessageType zza(InputStream inputStream, zzht zzht) throws zzir;
}
