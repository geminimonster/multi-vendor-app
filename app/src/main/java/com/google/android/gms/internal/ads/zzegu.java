package com.google.android.gms.internal.ads;

import java.nio.ByteBuffer;

/* compiled from: com.google.android.gms:play-services-ads@@19.1.0 */
public final class zzegu {
    public static final zzegu zziix = new zzegu(1.0d, 0.0d, 0.0d, 1.0d, 0.0d, 0.0d, 1.0d, 0.0d, 0.0d);
    private static final zzegu zziiy = new zzegu(0.0d, 1.0d, -1.0d, 0.0d, 0.0d, 0.0d, 1.0d, 0.0d, 0.0d);
    private static final zzegu zziiz = new zzegu(-1.0d, 0.0d, 0.0d, -1.0d, 0.0d, 0.0d, 1.0d, 0.0d, 0.0d);
    private static final zzegu zzija = new zzegu(0.0d, -1.0d, 1.0d, 0.0d, 0.0d, 0.0d, 1.0d, 0.0d, 0.0d);

    /* renamed from: a  reason: collision with root package name */
    private final double f117a;
    private final double b;
    private final double c;
    private final double d;
    private final double w;
    private final double zziit;
    private final double zziiu;
    private final double zziiv;
    private final double zziiw;

    private zzegu(double d2, double d3, double d4, double d5, double d6, double d7, double d8, double d9, double d10) {
        this.zziit = d6;
        this.zziiu = d7;
        this.w = d8;
        this.f117a = d2;
        this.b = d3;
        this.c = d4;
        this.d = d5;
        this.zziiv = d9;
        this.zziiw = d10;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        zzegu zzegu = (zzegu) obj;
        return Double.compare(zzegu.f117a, this.f117a) == 0 && Double.compare(zzegu.b, this.b) == 0 && Double.compare(zzegu.c, this.c) == 0 && Double.compare(zzegu.d, this.d) == 0 && Double.compare(zzegu.zziiv, this.zziiv) == 0 && Double.compare(zzegu.zziiw, this.zziiw) == 0 && Double.compare(zzegu.zziit, this.zziit) == 0 && Double.compare(zzegu.zziiu, this.zziiu) == 0 && Double.compare(zzegu.w, this.w) == 0;
    }

    public final int hashCode() {
        long doubleToLongBits = Double.doubleToLongBits(this.zziit);
        long doubleToLongBits2 = Double.doubleToLongBits(this.zziiu);
        long doubleToLongBits3 = Double.doubleToLongBits(this.w);
        long doubleToLongBits4 = Double.doubleToLongBits(this.f117a);
        long doubleToLongBits5 = Double.doubleToLongBits(this.b);
        long doubleToLongBits6 = Double.doubleToLongBits(this.c);
        long doubleToLongBits7 = Double.doubleToLongBits(this.d);
        long doubleToLongBits8 = Double.doubleToLongBits(this.zziiv);
        long doubleToLongBits9 = Double.doubleToLongBits(this.zziiw);
        return (((((((((((((((((int) (doubleToLongBits ^ (doubleToLongBits >>> 32))) * 31) + ((int) (doubleToLongBits2 ^ (doubleToLongBits2 >>> 32)))) * 31) + ((int) (doubleToLongBits3 ^ (doubleToLongBits3 >>> 32)))) * 31) + ((int) (doubleToLongBits4 ^ (doubleToLongBits4 >>> 32)))) * 31) + ((int) (doubleToLongBits5 ^ (doubleToLongBits5 >>> 32)))) * 31) + ((int) (doubleToLongBits6 ^ (doubleToLongBits6 >>> 32)))) * 31) + ((int) (doubleToLongBits7 ^ (doubleToLongBits7 >>> 32)))) * 31) + ((int) (doubleToLongBits8 ^ (doubleToLongBits8 >>> 32)))) * 31) + ((int) (doubleToLongBits9 ^ (doubleToLongBits9 >>> 32)));
    }

    public final String toString() {
        if (equals(zziix)) {
            return "Rotate 0°";
        }
        if (equals(zziiy)) {
            return "Rotate 90°";
        }
        if (equals(zziiz)) {
            return "Rotate 180°";
        }
        if (equals(zzija)) {
            return "Rotate 270°";
        }
        double d2 = this.zziit;
        double d3 = this.zziiu;
        double d4 = this.w;
        double d5 = this.f117a;
        double d6 = this.b;
        double d7 = this.c;
        double d8 = this.d;
        double d9 = this.zziiv;
        double d10 = this.zziiw;
        double d11 = d8;
        StringBuilder sb = new StringBuilder(260);
        sb.append("Matrix{u=");
        sb.append(d2);
        sb.append(", v=");
        sb.append(d3);
        sb.append(", w=");
        sb.append(d4);
        sb.append(", a=");
        sb.append(d5);
        sb.append(", b=");
        sb.append(d6);
        sb.append(", c=");
        sb.append(d7);
        sb.append(", d=");
        sb.append(d11);
        sb.append(", tx=");
        sb.append(d9);
        sb.append(", ty=");
        sb.append(d10);
        sb.append("}");
        return sb.toString();
    }

    public static zzegu zzn(ByteBuffer byteBuffer) {
        double zzd = zzbg.zzd(byteBuffer);
        double zzd2 = zzbg.zzd(byteBuffer);
        double zze = zzbg.zze(byteBuffer);
        return new zzegu(zzd, zzd2, zzbg.zzd(byteBuffer), zzbg.zzd(byteBuffer), zze, zzbg.zze(byteBuffer), zzbg.zze(byteBuffer), zzbg.zzd(byteBuffer), zzbg.zzd(byteBuffer));
    }
}
