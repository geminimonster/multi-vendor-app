package com.google.android.gms.internal.ads;

import com.bumptech.glide.load.Key;

/* compiled from: com.google.android.gms:play-services-ads@@19.1.0 */
final class zzajb implements Runnable {
    private final /* synthetic */ String zzdch;
    private final /* synthetic */ zzaiu zzdci;

    zzajb(zzaiu zzaiu, String str) {
        this.zzdci = zzaiu;
        this.zzdch = str;
    }

    public final void run() {
        this.zzdci.zzdce.loadData(this.zzdch, "text/html", Key.STRING_CHARSET_NAME);
    }
}
