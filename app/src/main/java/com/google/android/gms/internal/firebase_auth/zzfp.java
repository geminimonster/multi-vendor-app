package com.google.android.gms.internal.firebase_auth;

/* compiled from: com.google.firebase:firebase-auth@@19.4.0 */
final /* synthetic */ class zzfp {
    static final /* synthetic */ int[] zza;

    /* JADX WARNING: Can't wrap try/catch for region: R(14:0|1|2|3|4|5|6|7|8|9|10|11|12|14) */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x003e */
    /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0012 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001d */
    /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x0028 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0033 */
    static {
        /*
            com.google.android.gms.internal.firebase_auth.zzgm[] r0 = com.google.android.gms.internal.firebase_auth.zzgm.values()
            int r0 = r0.length
            int[] r0 = new int[r0]
            zza = r0
            com.google.android.gms.internal.firebase_auth.zzgm r1 = com.google.android.gms.internal.firebase_auth.zzgm.VERIFY_EMAIL     // Catch:{ NoSuchFieldError -> 0x0012 }
            int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0012 }
            r2 = 1
            r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0012 }
        L_0x0012:
            int[] r0 = zza     // Catch:{ NoSuchFieldError -> 0x001d }
            com.google.android.gms.internal.firebase_auth.zzgm r1 = com.google.android.gms.internal.firebase_auth.zzgm.PASSWORD_RESET     // Catch:{ NoSuchFieldError -> 0x001d }
            int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001d }
            r2 = 2
            r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001d }
        L_0x001d:
            int[] r0 = zza     // Catch:{ NoSuchFieldError -> 0x0028 }
            com.google.android.gms.internal.firebase_auth.zzgm r1 = com.google.android.gms.internal.firebase_auth.zzgm.EMAIL_SIGNIN     // Catch:{ NoSuchFieldError -> 0x0028 }
            int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0028 }
            r2 = 3
            r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0028 }
        L_0x0028:
            int[] r0 = zza     // Catch:{ NoSuchFieldError -> 0x0033 }
            com.google.android.gms.internal.firebase_auth.zzgm r1 = com.google.android.gms.internal.firebase_auth.zzgm.VERIFY_AND_CHANGE_EMAIL     // Catch:{ NoSuchFieldError -> 0x0033 }
            int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0033 }
            r2 = 4
            r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0033 }
        L_0x0033:
            int[] r0 = zza     // Catch:{ NoSuchFieldError -> 0x003e }
            com.google.android.gms.internal.firebase_auth.zzgm r1 = com.google.android.gms.internal.firebase_auth.zzgm.RECOVER_EMAIL     // Catch:{ NoSuchFieldError -> 0x003e }
            int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x003e }
            r2 = 5
            r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x003e }
        L_0x003e:
            int[] r0 = zza     // Catch:{ NoSuchFieldError -> 0x0049 }
            com.google.android.gms.internal.firebase_auth.zzgm r1 = com.google.android.gms.internal.firebase_auth.zzgm.REVERT_SECOND_FACTOR_ADDITION     // Catch:{ NoSuchFieldError -> 0x0049 }
            int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0049 }
            r2 = 6
            r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0049 }
        L_0x0049:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.firebase_auth.zzfp.<clinit>():void");
    }
}
