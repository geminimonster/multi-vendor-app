package com.google.android.gms.internal.ads;

import com.bumptech.glide.load.Key;
import java.nio.charset.Charset;
import org.bouncycastle.i18n.LocalizedMessage;

/* compiled from: com.google.android.gms:play-services-ads-lite@@19.1.0 */
public final class zzdnv {
    private static final Charset ISO_8859_1 = Charset.forName(LocalizedMessage.DEFAULT_ENCODING);
    private static final Charset US_ASCII = Charset.forName("US-ASCII");
    private static final Charset UTF_16 = Charset.forName("UTF-16");
    private static final Charset UTF_16BE = Charset.forName("UTF-16BE");
    private static final Charset UTF_16LE = Charset.forName("UTF-16LE");
    public static final Charset UTF_8 = Charset.forName(Key.STRING_CHARSET_NAME);
}
