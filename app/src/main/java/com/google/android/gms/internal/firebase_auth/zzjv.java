package com.google.android.gms.internal.firebase_auth;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.bouncycastle.asn1.cmp.PKIFailureInfo;
import sun.misc.Unsafe;

/* compiled from: com.google.firebase:firebase-auth@@19.4.0 */
final class zzjv<T> implements zzkh<T> {
    private static final int[] zza = new int[0];
    private static final Unsafe zzb = zzlf.zzc();
    private final int[] zzc;
    private final Object[] zzd;
    private final int zze;
    private final int zzf;
    private final zzjr zzg;
    private final boolean zzh;
    private final boolean zzi;
    private final boolean zzj;
    private final boolean zzk;
    private final int[] zzl;
    private final int zzm;
    private final int zzn;
    private final zzjw zzo;
    private final zzjb zzp;
    private final zzkz<?, ?> zzq;
    private final zzhv<?> zzr;
    private final zzjk zzs;

    private zzjv(int[] iArr, Object[] objArr, int i, int i2, zzjr zzjr, boolean z, boolean z2, int[] iArr2, int i3, int i4, zzjw zzjw, zzjb zzjb, zzkz<?, ?> zzkz, zzhv<?> zzhv, zzjk zzjk) {
        this.zzc = iArr;
        this.zzd = objArr;
        this.zze = i;
        this.zzf = i2;
        this.zzi = zzjr instanceof zzig;
        this.zzj = z;
        this.zzh = zzhv != null && zzhv.zza(zzjr);
        this.zzk = false;
        this.zzl = iArr2;
        this.zzm = i3;
        this.zzn = i4;
        this.zzo = zzjw;
        this.zzp = zzjb;
        this.zzq = zzkz;
        this.zzr = zzhv;
        this.zzg = zzjr;
        this.zzs = zzjk;
    }

    private static boolean zzf(int i) {
        return (i & PKIFailureInfo.duplicateCertReq) != 0;
    }

    /* JADX WARNING: Removed duplicated region for block: B:159:0x033e  */
    /* JADX WARNING: Removed duplicated region for block: B:171:0x038c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static <T> com.google.android.gms.internal.firebase_auth.zzjv<T> zza(java.lang.Class<T> r33, com.google.android.gms.internal.firebase_auth.zzjp r34, com.google.android.gms.internal.firebase_auth.zzjw r35, com.google.android.gms.internal.firebase_auth.zzjb r36, com.google.android.gms.internal.firebase_auth.zzkz<?, ?> r37, com.google.android.gms.internal.firebase_auth.zzhv<?> r38, com.google.android.gms.internal.firebase_auth.zzjk r39) {
        /*
            r0 = r34
            boolean r1 = r0 instanceof com.google.android.gms.internal.firebase_auth.zzkf
            if (r1 == 0) goto L_0x040f
            com.google.android.gms.internal.firebase_auth.zzkf r0 = (com.google.android.gms.internal.firebase_auth.zzkf) r0
            int r1 = r0.zza()
            int r2 = com.google.android.gms.internal.firebase_auth.zzig.zze.zzi
            r3 = 0
            r4 = 1
            if (r1 != r2) goto L_0x0014
            r11 = 1
            goto L_0x0015
        L_0x0014:
            r11 = 0
        L_0x0015:
            java.lang.String r1 = r0.zzd()
            int r2 = r1.length()
            char r5 = r1.charAt(r3)
            r6 = 55296(0xd800, float:7.7486E-41)
            if (r5 < r6) goto L_0x0031
            r5 = 1
        L_0x0027:
            int r7 = r5 + 1
            char r5 = r1.charAt(r5)
            if (r5 < r6) goto L_0x0032
            r5 = r7
            goto L_0x0027
        L_0x0031:
            r7 = 1
        L_0x0032:
            int r5 = r7 + 1
            char r7 = r1.charAt(r7)
            if (r7 < r6) goto L_0x0051
            r7 = r7 & 8191(0x1fff, float:1.1478E-41)
            r9 = 13
        L_0x003e:
            int r10 = r5 + 1
            char r5 = r1.charAt(r5)
            if (r5 < r6) goto L_0x004e
            r5 = r5 & 8191(0x1fff, float:1.1478E-41)
            int r5 = r5 << r9
            r7 = r7 | r5
            int r9 = r9 + 13
            r5 = r10
            goto L_0x003e
        L_0x004e:
            int r5 = r5 << r9
            r7 = r7 | r5
            r5 = r10
        L_0x0051:
            if (r7 != 0) goto L_0x005e
            int[] r7 = zza
            r14 = r7
            r7 = 0
            r9 = 0
            r10 = 0
            r12 = 0
            r13 = 0
            r15 = 0
            goto L_0x016f
        L_0x005e:
            int r7 = r5 + 1
            char r5 = r1.charAt(r5)
            if (r5 < r6) goto L_0x007d
            r5 = r5 & 8191(0x1fff, float:1.1478E-41)
            r9 = 13
        L_0x006a:
            int r10 = r7 + 1
            char r7 = r1.charAt(r7)
            if (r7 < r6) goto L_0x007a
            r7 = r7 & 8191(0x1fff, float:1.1478E-41)
            int r7 = r7 << r9
            r5 = r5 | r7
            int r9 = r9 + 13
            r7 = r10
            goto L_0x006a
        L_0x007a:
            int r7 = r7 << r9
            r5 = r5 | r7
            r7 = r10
        L_0x007d:
            int r9 = r7 + 1
            char r7 = r1.charAt(r7)
            if (r7 < r6) goto L_0x009c
            r7 = r7 & 8191(0x1fff, float:1.1478E-41)
            r10 = 13
        L_0x0089:
            int r12 = r9 + 1
            char r9 = r1.charAt(r9)
            if (r9 < r6) goto L_0x0099
            r9 = r9 & 8191(0x1fff, float:1.1478E-41)
            int r9 = r9 << r10
            r7 = r7 | r9
            int r10 = r10 + 13
            r9 = r12
            goto L_0x0089
        L_0x0099:
            int r9 = r9 << r10
            r7 = r7 | r9
            r9 = r12
        L_0x009c:
            int r10 = r9 + 1
            char r9 = r1.charAt(r9)
            if (r9 < r6) goto L_0x00bb
            r9 = r9 & 8191(0x1fff, float:1.1478E-41)
            r12 = 13
        L_0x00a8:
            int r13 = r10 + 1
            char r10 = r1.charAt(r10)
            if (r10 < r6) goto L_0x00b8
            r10 = r10 & 8191(0x1fff, float:1.1478E-41)
            int r10 = r10 << r12
            r9 = r9 | r10
            int r12 = r12 + 13
            r10 = r13
            goto L_0x00a8
        L_0x00b8:
            int r10 = r10 << r12
            r9 = r9 | r10
            r10 = r13
        L_0x00bb:
            int r12 = r10 + 1
            char r10 = r1.charAt(r10)
            if (r10 < r6) goto L_0x00da
            r10 = r10 & 8191(0x1fff, float:1.1478E-41)
            r13 = 13
        L_0x00c7:
            int r14 = r12 + 1
            char r12 = r1.charAt(r12)
            if (r12 < r6) goto L_0x00d7
            r12 = r12 & 8191(0x1fff, float:1.1478E-41)
            int r12 = r12 << r13
            r10 = r10 | r12
            int r13 = r13 + 13
            r12 = r14
            goto L_0x00c7
        L_0x00d7:
            int r12 = r12 << r13
            r10 = r10 | r12
            r12 = r14
        L_0x00da:
            int r13 = r12 + 1
            char r12 = r1.charAt(r12)
            if (r12 < r6) goto L_0x00f9
            r12 = r12 & 8191(0x1fff, float:1.1478E-41)
            r14 = 13
        L_0x00e6:
            int r15 = r13 + 1
            char r13 = r1.charAt(r13)
            if (r13 < r6) goto L_0x00f6
            r13 = r13 & 8191(0x1fff, float:1.1478E-41)
            int r13 = r13 << r14
            r12 = r12 | r13
            int r14 = r14 + 13
            r13 = r15
            goto L_0x00e6
        L_0x00f6:
            int r13 = r13 << r14
            r12 = r12 | r13
            r13 = r15
        L_0x00f9:
            int r14 = r13 + 1
            char r13 = r1.charAt(r13)
            if (r13 < r6) goto L_0x011a
            r13 = r13 & 8191(0x1fff, float:1.1478E-41)
            r15 = 13
        L_0x0105:
            int r16 = r14 + 1
            char r14 = r1.charAt(r14)
            if (r14 < r6) goto L_0x0116
            r14 = r14 & 8191(0x1fff, float:1.1478E-41)
            int r14 = r14 << r15
            r13 = r13 | r14
            int r15 = r15 + 13
            r14 = r16
            goto L_0x0105
        L_0x0116:
            int r14 = r14 << r15
            r13 = r13 | r14
            r14 = r16
        L_0x011a:
            int r15 = r14 + 1
            char r14 = r1.charAt(r14)
            if (r14 < r6) goto L_0x013d
            r14 = r14 & 8191(0x1fff, float:1.1478E-41)
            r16 = 13
        L_0x0126:
            int r17 = r15 + 1
            char r15 = r1.charAt(r15)
            if (r15 < r6) goto L_0x0138
            r15 = r15 & 8191(0x1fff, float:1.1478E-41)
            int r15 = r15 << r16
            r14 = r14 | r15
            int r16 = r16 + 13
            r15 = r17
            goto L_0x0126
        L_0x0138:
            int r15 = r15 << r16
            r14 = r14 | r15
            r15 = r17
        L_0x013d:
            int r16 = r15 + 1
            char r15 = r1.charAt(r15)
            if (r15 < r6) goto L_0x0162
            r15 = r15 & 8191(0x1fff, float:1.1478E-41)
            r3 = r16
            r16 = 13
        L_0x014b:
            int r17 = r3 + 1
            char r3 = r1.charAt(r3)
            if (r3 < r6) goto L_0x015d
            r3 = r3 & 8191(0x1fff, float:1.1478E-41)
            int r3 = r3 << r16
            r15 = r15 | r3
            int r16 = r16 + 13
            r3 = r17
            goto L_0x014b
        L_0x015d:
            int r3 = r3 << r16
            r15 = r15 | r3
            r16 = r17
        L_0x0162:
            int r3 = r15 + r13
            int r3 = r3 + r14
            int[] r3 = new int[r3]
            int r14 = r5 << 1
            int r14 = r14 + r7
            r7 = r14
            r14 = r3
            r3 = r5
            r5 = r16
        L_0x016f:
            sun.misc.Unsafe r8 = zzb
            java.lang.Object[] r16 = r0.zze()
            com.google.android.gms.internal.firebase_auth.zzjr r17 = r0.zzc()
            java.lang.Class r6 = r17.getClass()
            r17 = r5
            int r5 = r12 * 3
            int[] r5 = new int[r5]
            int r12 = r12 << r4
            java.lang.Object[] r12 = new java.lang.Object[r12]
            int r19 = r15 + r13
            r13 = r7
            r21 = r15
            r7 = r17
            r22 = r19
            r17 = 0
            r20 = 0
        L_0x0193:
            if (r7 >= r2) goto L_0x03e3
            int r23 = r7 + 1
            char r7 = r1.charAt(r7)
            r4 = 55296(0xd800, float:7.7486E-41)
            if (r7 < r4) goto L_0x01c5
            r7 = r7 & 8191(0x1fff, float:1.1478E-41)
            r4 = r23
            r23 = 13
        L_0x01a6:
            int r25 = r4 + 1
            char r4 = r1.charAt(r4)
            r26 = r2
            r2 = 55296(0xd800, float:7.7486E-41)
            if (r4 < r2) goto L_0x01bf
            r2 = r4 & 8191(0x1fff, float:1.1478E-41)
            int r2 = r2 << r23
            r7 = r7 | r2
            int r23 = r23 + 13
            r4 = r25
            r2 = r26
            goto L_0x01a6
        L_0x01bf:
            int r2 = r4 << r23
            r7 = r7 | r2
            r2 = r25
            goto L_0x01c9
        L_0x01c5:
            r26 = r2
            r2 = r23
        L_0x01c9:
            int r4 = r2 + 1
            char r2 = r1.charAt(r2)
            r23 = r4
            r4 = 55296(0xd800, float:7.7486E-41)
            if (r2 < r4) goto L_0x01fb
            r2 = r2 & 8191(0x1fff, float:1.1478E-41)
            r4 = r23
            r23 = 13
        L_0x01dc:
            int r25 = r4 + 1
            char r4 = r1.charAt(r4)
            r27 = r15
            r15 = 55296(0xd800, float:7.7486E-41)
            if (r4 < r15) goto L_0x01f5
            r4 = r4 & 8191(0x1fff, float:1.1478E-41)
            int r4 = r4 << r23
            r2 = r2 | r4
            int r23 = r23 + 13
            r4 = r25
            r15 = r27
            goto L_0x01dc
        L_0x01f5:
            int r4 = r4 << r23
            r2 = r2 | r4
            r4 = r25
            goto L_0x01ff
        L_0x01fb:
            r27 = r15
            r4 = r23
        L_0x01ff:
            r15 = r2 & 255(0xff, float:3.57E-43)
            r23 = r10
            r10 = r2 & 1024(0x400, float:1.435E-42)
            if (r10 == 0) goto L_0x020d
            int r10 = r17 + 1
            r14[r17] = r20
            r17 = r10
        L_0x020d:
            r10 = 51
            r29 = r9
            if (r15 < r10) goto L_0x02aa
            int r10 = r4 + 1
            char r4 = r1.charAt(r4)
            r9 = 55296(0xd800, float:7.7486E-41)
            if (r4 < r9) goto L_0x023c
            r4 = r4 & 8191(0x1fff, float:1.1478E-41)
            r31 = 13
        L_0x0222:
            int r32 = r10 + 1
            char r10 = r1.charAt(r10)
            if (r10 < r9) goto L_0x0237
            r9 = r10 & 8191(0x1fff, float:1.1478E-41)
            int r9 = r9 << r31
            r4 = r4 | r9
            int r31 = r31 + 13
            r10 = r32
            r9 = 55296(0xd800, float:7.7486E-41)
            goto L_0x0222
        L_0x0237:
            int r9 = r10 << r31
            r4 = r4 | r9
            r10 = r32
        L_0x023c:
            int r9 = r15 + -51
            r31 = r10
            r10 = 9
            if (r9 == r10) goto L_0x025d
            r10 = 17
            if (r9 != r10) goto L_0x0249
            goto L_0x025d
        L_0x0249:
            r10 = 12
            if (r9 != r10) goto L_0x025b
            if (r11 != 0) goto L_0x025b
            int r9 = r20 / 3
            r10 = 1
            int r9 = r9 << r10
            int r9 = r9 + r10
            int r10 = r13 + 1
            r13 = r16[r13]
            r12[r9] = r13
            r13 = r10
        L_0x025b:
            r10 = 1
            goto L_0x026a
        L_0x025d:
            int r9 = r20 / 3
            r10 = 1
            int r9 = r9 << r10
            int r9 = r9 + r10
            int r24 = r13 + 1
            r13 = r16[r13]
            r12[r9] = r13
            r13 = r24
        L_0x026a:
            int r4 = r4 << r10
            r9 = r16[r4]
            boolean r10 = r9 instanceof java.lang.reflect.Field
            if (r10 == 0) goto L_0x0274
            java.lang.reflect.Field r9 = (java.lang.reflect.Field) r9
            goto L_0x027c
        L_0x0274:
            java.lang.String r9 = (java.lang.String) r9
            java.lang.reflect.Field r9 = zza((java.lang.Class<?>) r6, (java.lang.String) r9)
            r16[r4] = r9
        L_0x027c:
            long r9 = r8.objectFieldOffset(r9)
            int r10 = (int) r9
            int r4 = r4 + 1
            r9 = r16[r4]
            r25 = r10
            boolean r10 = r9 instanceof java.lang.reflect.Field
            if (r10 == 0) goto L_0x028e
            java.lang.reflect.Field r9 = (java.lang.reflect.Field) r9
            goto L_0x0296
        L_0x028e:
            java.lang.String r9 = (java.lang.String) r9
            java.lang.reflect.Field r9 = zza((java.lang.Class<?>) r6, (java.lang.String) r9)
            r16[r4] = r9
        L_0x0296:
            long r9 = r8.objectFieldOffset(r9)
            int r4 = (int) r9
            r30 = r1
            r9 = r4
            r1 = r11
            r24 = r12
            r10 = r25
            r11 = r31
            r4 = 0
            r18 = 1
            goto L_0x03a8
        L_0x02aa:
            int r9 = r13 + 1
            r10 = r16[r13]
            java.lang.String r10 = (java.lang.String) r10
            java.lang.reflect.Field r10 = zza((java.lang.Class<?>) r6, (java.lang.String) r10)
            r13 = 9
            if (r15 == r13) goto L_0x031e
            r13 = 17
            if (r15 != r13) goto L_0x02bd
            goto L_0x031e
        L_0x02bd:
            r13 = 27
            if (r15 == r13) goto L_0x030d
            r13 = 49
            if (r15 != r13) goto L_0x02c6
            goto L_0x030d
        L_0x02c6:
            r13 = 12
            if (r15 == r13) goto L_0x02f9
            r13 = 30
            if (r15 == r13) goto L_0x02f9
            r13 = 44
            if (r15 != r13) goto L_0x02d3
            goto L_0x02f9
        L_0x02d3:
            r13 = 50
            if (r15 != r13) goto L_0x032c
            int r13 = r21 + 1
            r14[r21] = r20
            int r21 = r20 / 3
            r24 = 1
            int r21 = r21 << 1
            int r25 = r9 + 1
            r9 = r16[r9]
            r12[r21] = r9
            r9 = r2 & 2048(0x800, float:2.87E-42)
            if (r9 == 0) goto L_0x02f6
            int r21 = r21 + 1
            int r9 = r25 + 1
            r25 = r16[r25]
            r12[r21] = r25
            r21 = r13
            goto L_0x032c
        L_0x02f6:
            r21 = r13
            goto L_0x031b
        L_0x02f9:
            if (r11 != 0) goto L_0x030a
            int r13 = r20 / 3
            r24 = 1
            int r13 = r13 << 1
            int r13 = r13 + 1
            int r25 = r9 + 1
            r9 = r16[r9]
            r12[r13] = r9
            goto L_0x031b
        L_0x030a:
            r24 = 1
            goto L_0x032c
        L_0x030d:
            r24 = 1
            int r13 = r20 / 3
            int r13 = r13 << 1
            int r13 = r13 + 1
            int r25 = r9 + 1
            r9 = r16[r9]
            r12[r13] = r9
        L_0x031b:
            r13 = r25
            goto L_0x032d
        L_0x031e:
            r24 = 1
            int r13 = r20 / 3
            int r13 = r13 << 1
            int r13 = r13 + 1
            java.lang.Class r25 = r10.getType()
            r12[r13] = r25
        L_0x032c:
            r13 = r9
        L_0x032d:
            long r9 = r8.objectFieldOffset(r10)
            int r10 = (int) r9
            r9 = r2 & 4096(0x1000, float:5.74E-42)
            r25 = r13
            r13 = 4096(0x1000, float:5.74E-42)
            if (r9 != r13) goto L_0x038c
            r9 = 17
            if (r15 > r9) goto L_0x038c
            int r9 = r4 + 1
            char r4 = r1.charAt(r4)
            r13 = 55296(0xd800, float:7.7486E-41)
            if (r4 < r13) goto L_0x0364
            r4 = r4 & 8191(0x1fff, float:1.1478E-41)
            r18 = 13
        L_0x034d:
            int r28 = r9 + 1
            char r9 = r1.charAt(r9)
            if (r9 < r13) goto L_0x035f
            r9 = r9 & 8191(0x1fff, float:1.1478E-41)
            int r9 = r9 << r18
            r4 = r4 | r9
            int r18 = r18 + 13
            r9 = r28
            goto L_0x034d
        L_0x035f:
            int r9 = r9 << r18
            r4 = r4 | r9
            r9 = r28
        L_0x0364:
            r18 = 1
            int r24 = r3 << 1
            int r28 = r4 / 32
            int r24 = r24 + r28
            r13 = r16[r24]
            r30 = r1
            boolean r1 = r13 instanceof java.lang.reflect.Field
            if (r1 == 0) goto L_0x0377
            java.lang.reflect.Field r13 = (java.lang.reflect.Field) r13
            goto L_0x037f
        L_0x0377:
            java.lang.String r13 = (java.lang.String) r13
            java.lang.reflect.Field r13 = zza((java.lang.Class<?>) r6, (java.lang.String) r13)
            r16[r24] = r13
        L_0x037f:
            r1 = r11
            r24 = r12
            long r11 = r8.objectFieldOffset(r13)
            int r12 = (int) r11
            int r4 = r4 % 32
            r11 = r9
            r9 = r12
            goto L_0x0398
        L_0x038c:
            r30 = r1
            r1 = r11
            r24 = r12
            r18 = 1
            r9 = 1048575(0xfffff, float:1.469367E-39)
            r11 = r4
            r4 = 0
        L_0x0398:
            r12 = 18
            if (r15 < r12) goto L_0x03a6
            r12 = 49
            if (r15 > r12) goto L_0x03a6
            int r12 = r22 + 1
            r14[r22] = r10
            r22 = r12
        L_0x03a6:
            r13 = r25
        L_0x03a8:
            int r12 = r20 + 1
            r5[r20] = r7
            int r7 = r12 + 1
            r20 = r3
            r3 = r2 & 512(0x200, float:7.175E-43)
            if (r3 == 0) goto L_0x03b7
            r3 = 536870912(0x20000000, float:1.0842022E-19)
            goto L_0x03b8
        L_0x03b7:
            r3 = 0
        L_0x03b8:
            r2 = r2 & 256(0x100, float:3.59E-43)
            if (r2 == 0) goto L_0x03bf
            r2 = 268435456(0x10000000, float:2.5243549E-29)
            goto L_0x03c0
        L_0x03bf:
            r2 = 0
        L_0x03c0:
            r2 = r2 | r3
            int r3 = r15 << 20
            r2 = r2 | r3
            r2 = r2 | r10
            r5[r12] = r2
            int r2 = r7 + 1
            int r3 = r4 << 20
            r3 = r3 | r9
            r5[r7] = r3
            r7 = r11
            r3 = r20
            r10 = r23
            r12 = r24
            r15 = r27
            r9 = r29
            r4 = 1
            r11 = r1
            r20 = r2
            r2 = r26
            r1 = r30
            goto L_0x0193
        L_0x03e3:
            r29 = r9
            r23 = r10
            r1 = r11
            r24 = r12
            r27 = r15
            com.google.android.gms.internal.firebase_auth.zzjv r2 = new com.google.android.gms.internal.firebase_auth.zzjv
            com.google.android.gms.internal.firebase_auth.zzjr r10 = r0.zzc()
            r12 = 0
            r0 = r5
            r5 = r2
            r6 = r0
            r7 = r24
            r8 = r29
            r9 = r23
            r13 = r14
            r14 = r27
            r15 = r19
            r16 = r35
            r17 = r36
            r18 = r37
            r19 = r38
            r20 = r39
            r5.<init>(r6, r7, r8, r9, r10, r11, r12, r13, r14, r15, r16, r17, r18, r19, r20)
            return r2
        L_0x040f:
            com.google.android.gms.internal.firebase_auth.zzks r0 = (com.google.android.gms.internal.firebase_auth.zzks) r0
            int r0 = r0.zza()
            int r1 = com.google.android.gms.internal.firebase_auth.zzig.zze.zzi
            java.lang.NoSuchMethodError r0 = new java.lang.NoSuchMethodError
            r0.<init>()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.firebase_auth.zzjv.zza(java.lang.Class, com.google.android.gms.internal.firebase_auth.zzjp, com.google.android.gms.internal.firebase_auth.zzjw, com.google.android.gms.internal.firebase_auth.zzjb, com.google.android.gms.internal.firebase_auth.zzkz, com.google.android.gms.internal.firebase_auth.zzhv, com.google.android.gms.internal.firebase_auth.zzjk):com.google.android.gms.internal.firebase_auth.zzjv");
    }

    private static Field zza(Class<?> cls, String str) {
        try {
            return cls.getDeclaredField(str);
        } catch (NoSuchFieldException unused) {
            Field[] declaredFields = cls.getDeclaredFields();
            for (Field field : declaredFields) {
                if (str.equals(field.getName())) {
                    return field;
                }
            }
            String name = cls.getName();
            String arrays = Arrays.toString(declaredFields);
            StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 40 + String.valueOf(name).length() + String.valueOf(arrays).length());
            sb.append("Field ");
            sb.append(str);
            sb.append(" for ");
            sb.append(name);
            sb.append(" not found. Known fields are ");
            sb.append(arrays);
            throw new RuntimeException(sb.toString());
        }
    }

    public final T zza() {
        return this.zzo.zza(this.zzg);
    }

    /* JADX WARNING: Can't fix incorrect switch cases order */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x006a, code lost:
        if (com.google.android.gms.internal.firebase_auth.zzkj.zza(com.google.android.gms.internal.firebase_auth.zzlf.zzf(r10, r6), com.google.android.gms.internal.firebase_auth.zzlf.zzf(r11, r6)) != false) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x007e, code lost:
        if (com.google.android.gms.internal.firebase_auth.zzlf.zzb(r10, r6) == com.google.android.gms.internal.firebase_auth.zzlf.zzb(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0090, code lost:
        if (com.google.android.gms.internal.firebase_auth.zzlf.zza((java.lang.Object) r10, r6) == com.google.android.gms.internal.firebase_auth.zzlf.zza((java.lang.Object) r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x00a4, code lost:
        if (com.google.android.gms.internal.firebase_auth.zzlf.zzb(r10, r6) == com.google.android.gms.internal.firebase_auth.zzlf.zzb(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x00b6, code lost:
        if (com.google.android.gms.internal.firebase_auth.zzlf.zza((java.lang.Object) r10, r6) == com.google.android.gms.internal.firebase_auth.zzlf.zza((java.lang.Object) r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x00c8, code lost:
        if (com.google.android.gms.internal.firebase_auth.zzlf.zza((java.lang.Object) r10, r6) == com.google.android.gms.internal.firebase_auth.zzlf.zza((java.lang.Object) r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x00da, code lost:
        if (com.google.android.gms.internal.firebase_auth.zzlf.zza((java.lang.Object) r10, r6) == com.google.android.gms.internal.firebase_auth.zzlf.zza((java.lang.Object) r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x00f0, code lost:
        if (com.google.android.gms.internal.firebase_auth.zzkj.zza(com.google.android.gms.internal.firebase_auth.zzlf.zzf(r10, r6), com.google.android.gms.internal.firebase_auth.zzlf.zzf(r11, r6)) != false) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x0106, code lost:
        if (com.google.android.gms.internal.firebase_auth.zzkj.zza(com.google.android.gms.internal.firebase_auth.zzlf.zzf(r10, r6), com.google.android.gms.internal.firebase_auth.zzlf.zzf(r11, r6)) != false) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x011c, code lost:
        if (com.google.android.gms.internal.firebase_auth.zzkj.zza(com.google.android.gms.internal.firebase_auth.zzlf.zzf(r10, r6), com.google.android.gms.internal.firebase_auth.zzlf.zzf(r11, r6)) != false) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x012e, code lost:
        if (com.google.android.gms.internal.firebase_auth.zzlf.zzc(r10, r6) == com.google.android.gms.internal.firebase_auth.zzlf.zzc(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x0140, code lost:
        if (com.google.android.gms.internal.firebase_auth.zzlf.zza((java.lang.Object) r10, r6) == com.google.android.gms.internal.firebase_auth.zzlf.zza((java.lang.Object) r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x0154, code lost:
        if (com.google.android.gms.internal.firebase_auth.zzlf.zzb(r10, r6) == com.google.android.gms.internal.firebase_auth.zzlf.zzb(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:0x0165, code lost:
        if (com.google.android.gms.internal.firebase_auth.zzlf.zza((java.lang.Object) r10, r6) == com.google.android.gms.internal.firebase_auth.zzlf.zza((java.lang.Object) r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:70:0x0178, code lost:
        if (com.google.android.gms.internal.firebase_auth.zzlf.zzb(r10, r6) == com.google.android.gms.internal.firebase_auth.zzlf.zzb(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:0x018b, code lost:
        if (com.google.android.gms.internal.firebase_auth.zzlf.zzb(r10, r6) == com.google.android.gms.internal.firebase_auth.zzlf.zzb(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:78:0x01a4, code lost:
        if (java.lang.Float.floatToIntBits(com.google.android.gms.internal.firebase_auth.zzlf.zzd(r10, r6)) == java.lang.Float.floatToIntBits(com.google.android.gms.internal.firebase_auth.zzlf.zzd(r11, r6))) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:82:0x01bf, code lost:
        if (java.lang.Double.doubleToLongBits(com.google.android.gms.internal.firebase_auth.zzlf.zze(r10, r6)) == java.lang.Double.doubleToLongBits(com.google.android.gms.internal.firebase_auth.zzlf.zze(r11, r6))) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:83:0x01c1, code lost:
        r3 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0038, code lost:
        if (com.google.android.gms.internal.firebase_auth.zzkj.zza(com.google.android.gms.internal.firebase_auth.zzlf.zzf(r10, r6), com.google.android.gms.internal.firebase_auth.zzlf.zzf(r11, r6)) != false) goto L_0x01c2;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean zza(T r10, T r11) {
        /*
            r9 = this;
            int[] r0 = r9.zzc
            int r0 = r0.length
            r1 = 0
            r2 = 0
        L_0x0005:
            r3 = 1
            if (r2 >= r0) goto L_0x01c9
            int r4 = r9.zzd((int) r2)
            r5 = 1048575(0xfffff, float:1.469367E-39)
            r6 = r4 & r5
            long r6 = (long) r6
            r8 = 267386880(0xff00000, float:2.3665827E-29)
            r4 = r4 & r8
            int r4 = r4 >>> 20
            switch(r4) {
                case 0: goto L_0x01a7;
                case 1: goto L_0x018e;
                case 2: goto L_0x017b;
                case 3: goto L_0x0168;
                case 4: goto L_0x0157;
                case 5: goto L_0x0144;
                case 6: goto L_0x0132;
                case 7: goto L_0x0120;
                case 8: goto L_0x010a;
                case 9: goto L_0x00f4;
                case 10: goto L_0x00de;
                case 11: goto L_0x00cc;
                case 12: goto L_0x00ba;
                case 13: goto L_0x00a8;
                case 14: goto L_0x0094;
                case 15: goto L_0x0082;
                case 16: goto L_0x006e;
                case 17: goto L_0x0058;
                case 18: goto L_0x004a;
                case 19: goto L_0x004a;
                case 20: goto L_0x004a;
                case 21: goto L_0x004a;
                case 22: goto L_0x004a;
                case 23: goto L_0x004a;
                case 24: goto L_0x004a;
                case 25: goto L_0x004a;
                case 26: goto L_0x004a;
                case 27: goto L_0x004a;
                case 28: goto L_0x004a;
                case 29: goto L_0x004a;
                case 30: goto L_0x004a;
                case 31: goto L_0x004a;
                case 32: goto L_0x004a;
                case 33: goto L_0x004a;
                case 34: goto L_0x004a;
                case 35: goto L_0x004a;
                case 36: goto L_0x004a;
                case 37: goto L_0x004a;
                case 38: goto L_0x004a;
                case 39: goto L_0x004a;
                case 40: goto L_0x004a;
                case 41: goto L_0x004a;
                case 42: goto L_0x004a;
                case 43: goto L_0x004a;
                case 44: goto L_0x004a;
                case 45: goto L_0x004a;
                case 46: goto L_0x004a;
                case 47: goto L_0x004a;
                case 48: goto L_0x004a;
                case 49: goto L_0x004a;
                case 50: goto L_0x003c;
                case 51: goto L_0x001c;
                case 52: goto L_0x001c;
                case 53: goto L_0x001c;
                case 54: goto L_0x001c;
                case 55: goto L_0x001c;
                case 56: goto L_0x001c;
                case 57: goto L_0x001c;
                case 58: goto L_0x001c;
                case 59: goto L_0x001c;
                case 60: goto L_0x001c;
                case 61: goto L_0x001c;
                case 62: goto L_0x001c;
                case 63: goto L_0x001c;
                case 64: goto L_0x001c;
                case 65: goto L_0x001c;
                case 66: goto L_0x001c;
                case 67: goto L_0x001c;
                case 68: goto L_0x001c;
                default: goto L_0x001a;
            }
        L_0x001a:
            goto L_0x01c2
        L_0x001c:
            int r4 = r9.zze(r2)
            r4 = r4 & r5
            long r4 = (long) r4
            int r8 = com.google.android.gms.internal.firebase_auth.zzlf.zza((java.lang.Object) r10, (long) r4)
            int r4 = com.google.android.gms.internal.firebase_auth.zzlf.zza((java.lang.Object) r11, (long) r4)
            if (r8 != r4) goto L_0x01c1
            java.lang.Object r4 = com.google.android.gms.internal.firebase_auth.zzlf.zzf(r10, r6)
            java.lang.Object r5 = com.google.android.gms.internal.firebase_auth.zzlf.zzf(r11, r6)
            boolean r4 = com.google.android.gms.internal.firebase_auth.zzkj.zza((java.lang.Object) r4, (java.lang.Object) r5)
            if (r4 != 0) goto L_0x01c2
            goto L_0x01c1
        L_0x003c:
            java.lang.Object r3 = com.google.android.gms.internal.firebase_auth.zzlf.zzf(r10, r6)
            java.lang.Object r4 = com.google.android.gms.internal.firebase_auth.zzlf.zzf(r11, r6)
            boolean r3 = com.google.android.gms.internal.firebase_auth.zzkj.zza((java.lang.Object) r3, (java.lang.Object) r4)
            goto L_0x01c2
        L_0x004a:
            java.lang.Object r3 = com.google.android.gms.internal.firebase_auth.zzlf.zzf(r10, r6)
            java.lang.Object r4 = com.google.android.gms.internal.firebase_auth.zzlf.zzf(r11, r6)
            boolean r3 = com.google.android.gms.internal.firebase_auth.zzkj.zza((java.lang.Object) r3, (java.lang.Object) r4)
            goto L_0x01c2
        L_0x0058:
            boolean r4 = r9.zzc(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            java.lang.Object r4 = com.google.android.gms.internal.firebase_auth.zzlf.zzf(r10, r6)
            java.lang.Object r5 = com.google.android.gms.internal.firebase_auth.zzlf.zzf(r11, r6)
            boolean r4 = com.google.android.gms.internal.firebase_auth.zzkj.zza((java.lang.Object) r4, (java.lang.Object) r5)
            if (r4 != 0) goto L_0x01c2
            goto L_0x01c1
        L_0x006e:
            boolean r4 = r9.zzc(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            long r4 = com.google.android.gms.internal.firebase_auth.zzlf.zzb(r10, r6)
            long r6 = com.google.android.gms.internal.firebase_auth.zzlf.zzb(r11, r6)
            int r8 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r8 == 0) goto L_0x01c2
            goto L_0x01c1
        L_0x0082:
            boolean r4 = r9.zzc(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            int r4 = com.google.android.gms.internal.firebase_auth.zzlf.zza((java.lang.Object) r10, (long) r6)
            int r5 = com.google.android.gms.internal.firebase_auth.zzlf.zza((java.lang.Object) r11, (long) r6)
            if (r4 == r5) goto L_0x01c2
            goto L_0x01c1
        L_0x0094:
            boolean r4 = r9.zzc(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            long r4 = com.google.android.gms.internal.firebase_auth.zzlf.zzb(r10, r6)
            long r6 = com.google.android.gms.internal.firebase_auth.zzlf.zzb(r11, r6)
            int r8 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r8 == 0) goto L_0x01c2
            goto L_0x01c1
        L_0x00a8:
            boolean r4 = r9.zzc(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            int r4 = com.google.android.gms.internal.firebase_auth.zzlf.zza((java.lang.Object) r10, (long) r6)
            int r5 = com.google.android.gms.internal.firebase_auth.zzlf.zza((java.lang.Object) r11, (long) r6)
            if (r4 == r5) goto L_0x01c2
            goto L_0x01c1
        L_0x00ba:
            boolean r4 = r9.zzc(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            int r4 = com.google.android.gms.internal.firebase_auth.zzlf.zza((java.lang.Object) r10, (long) r6)
            int r5 = com.google.android.gms.internal.firebase_auth.zzlf.zza((java.lang.Object) r11, (long) r6)
            if (r4 == r5) goto L_0x01c2
            goto L_0x01c1
        L_0x00cc:
            boolean r4 = r9.zzc(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            int r4 = com.google.android.gms.internal.firebase_auth.zzlf.zza((java.lang.Object) r10, (long) r6)
            int r5 = com.google.android.gms.internal.firebase_auth.zzlf.zza((java.lang.Object) r11, (long) r6)
            if (r4 == r5) goto L_0x01c2
            goto L_0x01c1
        L_0x00de:
            boolean r4 = r9.zzc(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            java.lang.Object r4 = com.google.android.gms.internal.firebase_auth.zzlf.zzf(r10, r6)
            java.lang.Object r5 = com.google.android.gms.internal.firebase_auth.zzlf.zzf(r11, r6)
            boolean r4 = com.google.android.gms.internal.firebase_auth.zzkj.zza((java.lang.Object) r4, (java.lang.Object) r5)
            if (r4 != 0) goto L_0x01c2
            goto L_0x01c1
        L_0x00f4:
            boolean r4 = r9.zzc(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            java.lang.Object r4 = com.google.android.gms.internal.firebase_auth.zzlf.zzf(r10, r6)
            java.lang.Object r5 = com.google.android.gms.internal.firebase_auth.zzlf.zzf(r11, r6)
            boolean r4 = com.google.android.gms.internal.firebase_auth.zzkj.zza((java.lang.Object) r4, (java.lang.Object) r5)
            if (r4 != 0) goto L_0x01c2
            goto L_0x01c1
        L_0x010a:
            boolean r4 = r9.zzc(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            java.lang.Object r4 = com.google.android.gms.internal.firebase_auth.zzlf.zzf(r10, r6)
            java.lang.Object r5 = com.google.android.gms.internal.firebase_auth.zzlf.zzf(r11, r6)
            boolean r4 = com.google.android.gms.internal.firebase_auth.zzkj.zza((java.lang.Object) r4, (java.lang.Object) r5)
            if (r4 != 0) goto L_0x01c2
            goto L_0x01c1
        L_0x0120:
            boolean r4 = r9.zzc(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            boolean r4 = com.google.android.gms.internal.firebase_auth.zzlf.zzc(r10, r6)
            boolean r5 = com.google.android.gms.internal.firebase_auth.zzlf.zzc(r11, r6)
            if (r4 == r5) goto L_0x01c2
            goto L_0x01c1
        L_0x0132:
            boolean r4 = r9.zzc(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            int r4 = com.google.android.gms.internal.firebase_auth.zzlf.zza((java.lang.Object) r10, (long) r6)
            int r5 = com.google.android.gms.internal.firebase_auth.zzlf.zza((java.lang.Object) r11, (long) r6)
            if (r4 == r5) goto L_0x01c2
            goto L_0x01c1
        L_0x0144:
            boolean r4 = r9.zzc(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            long r4 = com.google.android.gms.internal.firebase_auth.zzlf.zzb(r10, r6)
            long r6 = com.google.android.gms.internal.firebase_auth.zzlf.zzb(r11, r6)
            int r8 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r8 == 0) goto L_0x01c2
            goto L_0x01c1
        L_0x0157:
            boolean r4 = r9.zzc(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            int r4 = com.google.android.gms.internal.firebase_auth.zzlf.zza((java.lang.Object) r10, (long) r6)
            int r5 = com.google.android.gms.internal.firebase_auth.zzlf.zza((java.lang.Object) r11, (long) r6)
            if (r4 == r5) goto L_0x01c2
            goto L_0x01c1
        L_0x0168:
            boolean r4 = r9.zzc(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            long r4 = com.google.android.gms.internal.firebase_auth.zzlf.zzb(r10, r6)
            long r6 = com.google.android.gms.internal.firebase_auth.zzlf.zzb(r11, r6)
            int r8 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r8 == 0) goto L_0x01c2
            goto L_0x01c1
        L_0x017b:
            boolean r4 = r9.zzc(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            long r4 = com.google.android.gms.internal.firebase_auth.zzlf.zzb(r10, r6)
            long r6 = com.google.android.gms.internal.firebase_auth.zzlf.zzb(r11, r6)
            int r8 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r8 == 0) goto L_0x01c2
            goto L_0x01c1
        L_0x018e:
            boolean r4 = r9.zzc(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            float r4 = com.google.android.gms.internal.firebase_auth.zzlf.zzd(r10, r6)
            int r4 = java.lang.Float.floatToIntBits(r4)
            float r5 = com.google.android.gms.internal.firebase_auth.zzlf.zzd(r11, r6)
            int r5 = java.lang.Float.floatToIntBits(r5)
            if (r4 == r5) goto L_0x01c2
            goto L_0x01c1
        L_0x01a7:
            boolean r4 = r9.zzc(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            double r4 = com.google.android.gms.internal.firebase_auth.zzlf.zze(r10, r6)
            long r4 = java.lang.Double.doubleToLongBits(r4)
            double r6 = com.google.android.gms.internal.firebase_auth.zzlf.zze(r11, r6)
            long r6 = java.lang.Double.doubleToLongBits(r6)
            int r8 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r8 == 0) goto L_0x01c2
        L_0x01c1:
            r3 = 0
        L_0x01c2:
            if (r3 != 0) goto L_0x01c5
            return r1
        L_0x01c5:
            int r2 = r2 + 3
            goto L_0x0005
        L_0x01c9:
            com.google.android.gms.internal.firebase_auth.zzkz<?, ?> r0 = r9.zzq
            java.lang.Object r0 = r0.zzb(r10)
            com.google.android.gms.internal.firebase_auth.zzkz<?, ?> r2 = r9.zzq
            java.lang.Object r2 = r2.zzb(r11)
            boolean r0 = r0.equals(r2)
            if (r0 != 0) goto L_0x01dc
            return r1
        L_0x01dc:
            boolean r0 = r9.zzh
            if (r0 == 0) goto L_0x01f1
            com.google.android.gms.internal.firebase_auth.zzhv<?> r0 = r9.zzr
            com.google.android.gms.internal.firebase_auth.zzhz r10 = r0.zza((java.lang.Object) r10)
            com.google.android.gms.internal.firebase_auth.zzhv<?> r0 = r9.zzr
            com.google.android.gms.internal.firebase_auth.zzhz r11 = r0.zza((java.lang.Object) r11)
            boolean r10 = r10.equals(r11)
            return r10
        L_0x01f1:
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.firebase_auth.zzjv.zza(java.lang.Object, java.lang.Object):boolean");
    }

    /* JADX WARNING: Can't fix incorrect switch cases order */
    /* JADX WARNING: Code restructure failed: missing block: B:73:0x01c3, code lost:
        r2 = (r2 * 53) + r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:83:0x0227, code lost:
        r2 = r2 + r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:84:0x0228, code lost:
        r1 = r1 + 3;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final int zza(T r9) {
        /*
            r8 = this;
            int[] r0 = r8.zzc
            int r0 = r0.length
            r1 = 0
            r2 = 0
        L_0x0005:
            if (r1 >= r0) goto L_0x022c
            int r3 = r8.zzd((int) r1)
            int[] r4 = r8.zzc
            r4 = r4[r1]
            r5 = 1048575(0xfffff, float:1.469367E-39)
            r5 = r5 & r3
            long r5 = (long) r5
            r7 = 267386880(0xff00000, float:2.3665827E-29)
            r3 = r3 & r7
            int r3 = r3 >>> 20
            r7 = 37
            switch(r3) {
                case 0: goto L_0x0219;
                case 1: goto L_0x020e;
                case 2: goto L_0x0203;
                case 3: goto L_0x01f8;
                case 4: goto L_0x01f1;
                case 5: goto L_0x01e6;
                case 6: goto L_0x01df;
                case 7: goto L_0x01d4;
                case 8: goto L_0x01c7;
                case 9: goto L_0x01b9;
                case 10: goto L_0x01ad;
                case 11: goto L_0x01a5;
                case 12: goto L_0x019d;
                case 13: goto L_0x0195;
                case 14: goto L_0x0189;
                case 15: goto L_0x0181;
                case 16: goto L_0x0175;
                case 17: goto L_0x016a;
                case 18: goto L_0x015e;
                case 19: goto L_0x015e;
                case 20: goto L_0x015e;
                case 21: goto L_0x015e;
                case 22: goto L_0x015e;
                case 23: goto L_0x015e;
                case 24: goto L_0x015e;
                case 25: goto L_0x015e;
                case 26: goto L_0x015e;
                case 27: goto L_0x015e;
                case 28: goto L_0x015e;
                case 29: goto L_0x015e;
                case 30: goto L_0x015e;
                case 31: goto L_0x015e;
                case 32: goto L_0x015e;
                case 33: goto L_0x015e;
                case 34: goto L_0x015e;
                case 35: goto L_0x015e;
                case 36: goto L_0x015e;
                case 37: goto L_0x015e;
                case 38: goto L_0x015e;
                case 39: goto L_0x015e;
                case 40: goto L_0x015e;
                case 41: goto L_0x015e;
                case 42: goto L_0x015e;
                case 43: goto L_0x015e;
                case 44: goto L_0x015e;
                case 45: goto L_0x015e;
                case 46: goto L_0x015e;
                case 47: goto L_0x015e;
                case 48: goto L_0x015e;
                case 49: goto L_0x015e;
                case 50: goto L_0x0152;
                case 51: goto L_0x013c;
                case 52: goto L_0x012a;
                case 53: goto L_0x0118;
                case 54: goto L_0x0106;
                case 55: goto L_0x00f8;
                case 56: goto L_0x00e6;
                case 57: goto L_0x00d8;
                case 58: goto L_0x00c6;
                case 59: goto L_0x00b2;
                case 60: goto L_0x00a0;
                case 61: goto L_0x008e;
                case 62: goto L_0x0080;
                case 63: goto L_0x0072;
                case 64: goto L_0x0064;
                case 65: goto L_0x0052;
                case 66: goto L_0x0044;
                case 67: goto L_0x0032;
                case 68: goto L_0x0020;
                default: goto L_0x001e;
            }
        L_0x001e:
            goto L_0x0228
        L_0x0020:
            boolean r3 = r8.zza(r9, (int) r4, (int) r1)
            if (r3 == 0) goto L_0x0228
            java.lang.Object r3 = com.google.android.gms.internal.firebase_auth.zzlf.zzf(r9, r5)
            int r2 = r2 * 53
            int r3 = r3.hashCode()
            goto L_0x0227
        L_0x0032:
            boolean r3 = r8.zza(r9, (int) r4, (int) r1)
            if (r3 == 0) goto L_0x0228
            int r2 = r2 * 53
            long r3 = zze(r9, r5)
            int r3 = com.google.android.gms.internal.firebase_auth.zzii.zza((long) r3)
            goto L_0x0227
        L_0x0044:
            boolean r3 = r8.zza(r9, (int) r4, (int) r1)
            if (r3 == 0) goto L_0x0228
            int r2 = r2 * 53
            int r3 = zzd(r9, r5)
            goto L_0x0227
        L_0x0052:
            boolean r3 = r8.zza(r9, (int) r4, (int) r1)
            if (r3 == 0) goto L_0x0228
            int r2 = r2 * 53
            long r3 = zze(r9, r5)
            int r3 = com.google.android.gms.internal.firebase_auth.zzii.zza((long) r3)
            goto L_0x0227
        L_0x0064:
            boolean r3 = r8.zza(r9, (int) r4, (int) r1)
            if (r3 == 0) goto L_0x0228
            int r2 = r2 * 53
            int r3 = zzd(r9, r5)
            goto L_0x0227
        L_0x0072:
            boolean r3 = r8.zza(r9, (int) r4, (int) r1)
            if (r3 == 0) goto L_0x0228
            int r2 = r2 * 53
            int r3 = zzd(r9, r5)
            goto L_0x0227
        L_0x0080:
            boolean r3 = r8.zza(r9, (int) r4, (int) r1)
            if (r3 == 0) goto L_0x0228
            int r2 = r2 * 53
            int r3 = zzd(r9, r5)
            goto L_0x0227
        L_0x008e:
            boolean r3 = r8.zza(r9, (int) r4, (int) r1)
            if (r3 == 0) goto L_0x0228
            int r2 = r2 * 53
            java.lang.Object r3 = com.google.android.gms.internal.firebase_auth.zzlf.zzf(r9, r5)
            int r3 = r3.hashCode()
            goto L_0x0227
        L_0x00a0:
            boolean r3 = r8.zza(r9, (int) r4, (int) r1)
            if (r3 == 0) goto L_0x0228
            java.lang.Object r3 = com.google.android.gms.internal.firebase_auth.zzlf.zzf(r9, r5)
            int r2 = r2 * 53
            int r3 = r3.hashCode()
            goto L_0x0227
        L_0x00b2:
            boolean r3 = r8.zza(r9, (int) r4, (int) r1)
            if (r3 == 0) goto L_0x0228
            int r2 = r2 * 53
            java.lang.Object r3 = com.google.android.gms.internal.firebase_auth.zzlf.zzf(r9, r5)
            java.lang.String r3 = (java.lang.String) r3
            int r3 = r3.hashCode()
            goto L_0x0227
        L_0x00c6:
            boolean r3 = r8.zza(r9, (int) r4, (int) r1)
            if (r3 == 0) goto L_0x0228
            int r2 = r2 * 53
            boolean r3 = zzf(r9, r5)
            int r3 = com.google.android.gms.internal.firebase_auth.zzii.zza((boolean) r3)
            goto L_0x0227
        L_0x00d8:
            boolean r3 = r8.zza(r9, (int) r4, (int) r1)
            if (r3 == 0) goto L_0x0228
            int r2 = r2 * 53
            int r3 = zzd(r9, r5)
            goto L_0x0227
        L_0x00e6:
            boolean r3 = r8.zza(r9, (int) r4, (int) r1)
            if (r3 == 0) goto L_0x0228
            int r2 = r2 * 53
            long r3 = zze(r9, r5)
            int r3 = com.google.android.gms.internal.firebase_auth.zzii.zza((long) r3)
            goto L_0x0227
        L_0x00f8:
            boolean r3 = r8.zza(r9, (int) r4, (int) r1)
            if (r3 == 0) goto L_0x0228
            int r2 = r2 * 53
            int r3 = zzd(r9, r5)
            goto L_0x0227
        L_0x0106:
            boolean r3 = r8.zza(r9, (int) r4, (int) r1)
            if (r3 == 0) goto L_0x0228
            int r2 = r2 * 53
            long r3 = zze(r9, r5)
            int r3 = com.google.android.gms.internal.firebase_auth.zzii.zza((long) r3)
            goto L_0x0227
        L_0x0118:
            boolean r3 = r8.zza(r9, (int) r4, (int) r1)
            if (r3 == 0) goto L_0x0228
            int r2 = r2 * 53
            long r3 = zze(r9, r5)
            int r3 = com.google.android.gms.internal.firebase_auth.zzii.zza((long) r3)
            goto L_0x0227
        L_0x012a:
            boolean r3 = r8.zza(r9, (int) r4, (int) r1)
            if (r3 == 0) goto L_0x0228
            int r2 = r2 * 53
            float r3 = zzc(r9, r5)
            int r3 = java.lang.Float.floatToIntBits(r3)
            goto L_0x0227
        L_0x013c:
            boolean r3 = r8.zza(r9, (int) r4, (int) r1)
            if (r3 == 0) goto L_0x0228
            int r2 = r2 * 53
            double r3 = zzb(r9, (long) r5)
            long r3 = java.lang.Double.doubleToLongBits(r3)
            int r3 = com.google.android.gms.internal.firebase_auth.zzii.zza((long) r3)
            goto L_0x0227
        L_0x0152:
            int r2 = r2 * 53
            java.lang.Object r3 = com.google.android.gms.internal.firebase_auth.zzlf.zzf(r9, r5)
            int r3 = r3.hashCode()
            goto L_0x0227
        L_0x015e:
            int r2 = r2 * 53
            java.lang.Object r3 = com.google.android.gms.internal.firebase_auth.zzlf.zzf(r9, r5)
            int r3 = r3.hashCode()
            goto L_0x0227
        L_0x016a:
            java.lang.Object r3 = com.google.android.gms.internal.firebase_auth.zzlf.zzf(r9, r5)
            if (r3 == 0) goto L_0x01c3
            int r7 = r3.hashCode()
            goto L_0x01c3
        L_0x0175:
            int r2 = r2 * 53
            long r3 = com.google.android.gms.internal.firebase_auth.zzlf.zzb(r9, r5)
            int r3 = com.google.android.gms.internal.firebase_auth.zzii.zza((long) r3)
            goto L_0x0227
        L_0x0181:
            int r2 = r2 * 53
            int r3 = com.google.android.gms.internal.firebase_auth.zzlf.zza((java.lang.Object) r9, (long) r5)
            goto L_0x0227
        L_0x0189:
            int r2 = r2 * 53
            long r3 = com.google.android.gms.internal.firebase_auth.zzlf.zzb(r9, r5)
            int r3 = com.google.android.gms.internal.firebase_auth.zzii.zza((long) r3)
            goto L_0x0227
        L_0x0195:
            int r2 = r2 * 53
            int r3 = com.google.android.gms.internal.firebase_auth.zzlf.zza((java.lang.Object) r9, (long) r5)
            goto L_0x0227
        L_0x019d:
            int r2 = r2 * 53
            int r3 = com.google.android.gms.internal.firebase_auth.zzlf.zza((java.lang.Object) r9, (long) r5)
            goto L_0x0227
        L_0x01a5:
            int r2 = r2 * 53
            int r3 = com.google.android.gms.internal.firebase_auth.zzlf.zza((java.lang.Object) r9, (long) r5)
            goto L_0x0227
        L_0x01ad:
            int r2 = r2 * 53
            java.lang.Object r3 = com.google.android.gms.internal.firebase_auth.zzlf.zzf(r9, r5)
            int r3 = r3.hashCode()
            goto L_0x0227
        L_0x01b9:
            java.lang.Object r3 = com.google.android.gms.internal.firebase_auth.zzlf.zzf(r9, r5)
            if (r3 == 0) goto L_0x01c3
            int r7 = r3.hashCode()
        L_0x01c3:
            int r2 = r2 * 53
            int r2 = r2 + r7
            goto L_0x0228
        L_0x01c7:
            int r2 = r2 * 53
            java.lang.Object r3 = com.google.android.gms.internal.firebase_auth.zzlf.zzf(r9, r5)
            java.lang.String r3 = (java.lang.String) r3
            int r3 = r3.hashCode()
            goto L_0x0227
        L_0x01d4:
            int r2 = r2 * 53
            boolean r3 = com.google.android.gms.internal.firebase_auth.zzlf.zzc(r9, r5)
            int r3 = com.google.android.gms.internal.firebase_auth.zzii.zza((boolean) r3)
            goto L_0x0227
        L_0x01df:
            int r2 = r2 * 53
            int r3 = com.google.android.gms.internal.firebase_auth.zzlf.zza((java.lang.Object) r9, (long) r5)
            goto L_0x0227
        L_0x01e6:
            int r2 = r2 * 53
            long r3 = com.google.android.gms.internal.firebase_auth.zzlf.zzb(r9, r5)
            int r3 = com.google.android.gms.internal.firebase_auth.zzii.zza((long) r3)
            goto L_0x0227
        L_0x01f1:
            int r2 = r2 * 53
            int r3 = com.google.android.gms.internal.firebase_auth.zzlf.zza((java.lang.Object) r9, (long) r5)
            goto L_0x0227
        L_0x01f8:
            int r2 = r2 * 53
            long r3 = com.google.android.gms.internal.firebase_auth.zzlf.zzb(r9, r5)
            int r3 = com.google.android.gms.internal.firebase_auth.zzii.zza((long) r3)
            goto L_0x0227
        L_0x0203:
            int r2 = r2 * 53
            long r3 = com.google.android.gms.internal.firebase_auth.zzlf.zzb(r9, r5)
            int r3 = com.google.android.gms.internal.firebase_auth.zzii.zza((long) r3)
            goto L_0x0227
        L_0x020e:
            int r2 = r2 * 53
            float r3 = com.google.android.gms.internal.firebase_auth.zzlf.zzd(r9, r5)
            int r3 = java.lang.Float.floatToIntBits(r3)
            goto L_0x0227
        L_0x0219:
            int r2 = r2 * 53
            double r3 = com.google.android.gms.internal.firebase_auth.zzlf.zze(r9, r5)
            long r3 = java.lang.Double.doubleToLongBits(r3)
            int r3 = com.google.android.gms.internal.firebase_auth.zzii.zza((long) r3)
        L_0x0227:
            int r2 = r2 + r3
        L_0x0228:
            int r1 = r1 + 3
            goto L_0x0005
        L_0x022c:
            int r2 = r2 * 53
            com.google.android.gms.internal.firebase_auth.zzkz<?, ?> r0 = r8.zzq
            java.lang.Object r0 = r0.zzb(r9)
            int r0 = r0.hashCode()
            int r2 = r2 + r0
            boolean r0 = r8.zzh
            if (r0 == 0) goto L_0x024a
            int r2 = r2 * 53
            com.google.android.gms.internal.firebase_auth.zzhv<?> r0 = r8.zzr
            com.google.android.gms.internal.firebase_auth.zzhz r9 = r0.zza((java.lang.Object) r9)
            int r9 = r9.hashCode()
            int r2 = r2 + r9
        L_0x024a:
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.firebase_auth.zzjv.zza(java.lang.Object):int");
    }

    public final void zzb(T t, T t2) {
        if (t2 != null) {
            for (int i = 0; i < this.zzc.length; i += 3) {
                int zzd2 = zzd(i);
                long j = (long) (1048575 & zzd2);
                int i2 = this.zzc[i];
                switch ((zzd2 & 267386880) >>> 20) {
                    case 0:
                        if (!zza(t2, i)) {
                            break;
                        } else {
                            zzlf.zza((Object) t, j, zzlf.zze(t2, j));
                            zzb(t, i);
                            break;
                        }
                    case 1:
                        if (!zza(t2, i)) {
                            break;
                        } else {
                            zzlf.zza((Object) t, j, zzlf.zzd(t2, j));
                            zzb(t, i);
                            break;
                        }
                    case 2:
                        if (!zza(t2, i)) {
                            break;
                        } else {
                            zzlf.zza((Object) t, j, zzlf.zzb(t2, j));
                            zzb(t, i);
                            break;
                        }
                    case 3:
                        if (!zza(t2, i)) {
                            break;
                        } else {
                            zzlf.zza((Object) t, j, zzlf.zzb(t2, j));
                            zzb(t, i);
                            break;
                        }
                    case 4:
                        if (!zza(t2, i)) {
                            break;
                        } else {
                            zzlf.zza((Object) t, j, zzlf.zza((Object) t2, j));
                            zzb(t, i);
                            break;
                        }
                    case 5:
                        if (!zza(t2, i)) {
                            break;
                        } else {
                            zzlf.zza((Object) t, j, zzlf.zzb(t2, j));
                            zzb(t, i);
                            break;
                        }
                    case 6:
                        if (!zza(t2, i)) {
                            break;
                        } else {
                            zzlf.zza((Object) t, j, zzlf.zza((Object) t2, j));
                            zzb(t, i);
                            break;
                        }
                    case 7:
                        if (!zza(t2, i)) {
                            break;
                        } else {
                            zzlf.zza((Object) t, j, zzlf.zzc(t2, j));
                            zzb(t, i);
                            break;
                        }
                    case 8:
                        if (!zza(t2, i)) {
                            break;
                        } else {
                            zzlf.zza((Object) t, j, zzlf.zzf(t2, j));
                            zzb(t, i);
                            break;
                        }
                    case 9:
                        zza(t, t2, i);
                        break;
                    case 10:
                        if (!zza(t2, i)) {
                            break;
                        } else {
                            zzlf.zza((Object) t, j, zzlf.zzf(t2, j));
                            zzb(t, i);
                            break;
                        }
                    case 11:
                        if (!zza(t2, i)) {
                            break;
                        } else {
                            zzlf.zza((Object) t, j, zzlf.zza((Object) t2, j));
                            zzb(t, i);
                            break;
                        }
                    case 12:
                        if (!zza(t2, i)) {
                            break;
                        } else {
                            zzlf.zza((Object) t, j, zzlf.zza((Object) t2, j));
                            zzb(t, i);
                            break;
                        }
                    case 13:
                        if (!zza(t2, i)) {
                            break;
                        } else {
                            zzlf.zza((Object) t, j, zzlf.zza((Object) t2, j));
                            zzb(t, i);
                            break;
                        }
                    case 14:
                        if (!zza(t2, i)) {
                            break;
                        } else {
                            zzlf.zza((Object) t, j, zzlf.zzb(t2, j));
                            zzb(t, i);
                            break;
                        }
                    case 15:
                        if (!zza(t2, i)) {
                            break;
                        } else {
                            zzlf.zza((Object) t, j, zzlf.zza((Object) t2, j));
                            zzb(t, i);
                            break;
                        }
                    case 16:
                        if (!zza(t2, i)) {
                            break;
                        } else {
                            zzlf.zza((Object) t, j, zzlf.zzb(t2, j));
                            zzb(t, i);
                            break;
                        }
                    case 17:
                        zza(t, t2, i);
                        break;
                    case 18:
                    case 19:
                    case 20:
                    case 21:
                    case 22:
                    case 23:
                    case 24:
                    case 25:
                    case 26:
                    case 27:
                    case 28:
                    case 29:
                    case 30:
                    case 31:
                    case 32:
                    case 33:
                    case 34:
                    case 35:
                    case 36:
                    case 37:
                    case 38:
                    case 39:
                    case 40:
                    case 41:
                    case 42:
                    case 43:
                    case 44:
                    case 45:
                    case 46:
                    case 47:
                    case 48:
                    case 49:
                        this.zzp.zza(t, t2, j);
                        break;
                    case 50:
                        zzkj.zza(this.zzs, t, t2, j);
                        break;
                    case 51:
                    case 52:
                    case 53:
                    case 54:
                    case 55:
                    case 56:
                    case 57:
                    case 58:
                    case 59:
                        if (!zza(t2, i2, i)) {
                            break;
                        } else {
                            zzlf.zza((Object) t, j, zzlf.zzf(t2, j));
                            zzb(t, i2, i);
                            break;
                        }
                    case 60:
                        zzb(t, t2, i);
                        break;
                    case 61:
                    case 62:
                    case 63:
                    case 64:
                    case 65:
                    case 66:
                    case 67:
                        if (!zza(t2, i2, i)) {
                            break;
                        } else {
                            zzlf.zza((Object) t, j, zzlf.zzf(t2, j));
                            zzb(t, i2, i);
                            break;
                        }
                    case 68:
                        zzb(t, t2, i);
                        break;
                }
            }
            zzkj.zza(this.zzq, t, t2);
            if (this.zzh) {
                zzkj.zza(this.zzr, t, t2);
                return;
            }
            return;
        }
        throw null;
    }

    private final void zza(T t, T t2, int i) {
        long zzd2 = (long) (zzd(i) & 1048575);
        if (zza(t2, i)) {
            Object zzf2 = zzlf.zzf(t, zzd2);
            Object zzf3 = zzlf.zzf(t2, zzd2);
            if (zzf2 != null && zzf3 != null) {
                zzlf.zza((Object) t, zzd2, zzii.zza(zzf2, zzf3));
                zzb(t, i);
            } else if (zzf3 != null) {
                zzlf.zza((Object) t, zzd2, zzf3);
                zzb(t, i);
            }
        }
    }

    private final void zzb(T t, T t2, int i) {
        int zzd2 = zzd(i);
        int i2 = this.zzc[i];
        long j = (long) (zzd2 & 1048575);
        if (zza(t2, i2, i)) {
            Object zzf2 = zzlf.zzf(t, j);
            Object zzf3 = zzlf.zzf(t2, j);
            if (zzf2 != null && zzf3 != null) {
                zzlf.zza((Object) t, j, zzii.zza(zzf2, zzf3));
                zzb(t, i2, i);
            } else if (zzf3 != null) {
                zzlf.zza((Object) t, j, zzf3);
                zzb(t, i2, i);
            }
        }
    }

    /* JADX WARNING: Can't fix incorrect switch cases order */
    /* JADX WARNING: Code restructure failed: missing block: B:397:0x0832, code lost:
        r9 = (r9 + r10) + r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:417:0x090b, code lost:
        r13 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:433:0x0953, code lost:
        r5 = r5 + r9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:470:0x09fd, code lost:
        r5 = r5 + r9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:479:0x0a1f, code lost:
        r3 = r3 + 3;
        r9 = r13;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final int zzd(T r20) {
        /*
            r19 = this;
            r0 = r19
            r1 = r20
            boolean r2 = r0.zzj
            r3 = 267386880(0xff00000, float:2.3665827E-29)
            r4 = 0
            r7 = 1
            r8 = 1048575(0xfffff, float:1.469367E-39)
            r9 = 0
            r11 = 0
            if (r2 == 0) goto L_0x04f2
            sun.misc.Unsafe r2 = zzb
            r12 = 0
            r13 = 0
        L_0x0016:
            int[] r14 = r0.zzc
            int r14 = r14.length
            if (r12 >= r14) goto L_0x04ea
            int r14 = r0.zzd((int) r12)
            r15 = r14 & r3
            int r15 = r15 >>> 20
            int[] r3 = r0.zzc
            r3 = r3[r12]
            r14 = r14 & r8
            long r5 = (long) r14
            com.google.android.gms.internal.firebase_auth.zzia r14 = com.google.android.gms.internal.firebase_auth.zzia.DOUBLE_LIST_PACKED
            int r14 = r14.zza()
            if (r15 < r14) goto L_0x0041
            com.google.android.gms.internal.firebase_auth.zzia r14 = com.google.android.gms.internal.firebase_auth.zzia.SINT64_LIST_PACKED
            int r14 = r14.zza()
            if (r15 > r14) goto L_0x0041
            int[] r14 = r0.zzc
            int r17 = r12 + 2
            r14 = r14[r17]
            r14 = r14 & r8
            goto L_0x0042
        L_0x0041:
            r14 = 0
        L_0x0042:
            switch(r15) {
                case 0: goto L_0x04d6;
                case 1: goto L_0x04ca;
                case 2: goto L_0x04ba;
                case 3: goto L_0x04aa;
                case 4: goto L_0x049a;
                case 5: goto L_0x048e;
                case 6: goto L_0x0482;
                case 7: goto L_0x0476;
                case 8: goto L_0x0458;
                case 9: goto L_0x0444;
                case 10: goto L_0x0433;
                case 11: goto L_0x0424;
                case 12: goto L_0x0415;
                case 13: goto L_0x040a;
                case 14: goto L_0x03ff;
                case 15: goto L_0x03f0;
                case 16: goto L_0x03e1;
                case 17: goto L_0x03cc;
                case 18: goto L_0x03c1;
                case 19: goto L_0x03b8;
                case 20: goto L_0x03af;
                case 21: goto L_0x03a6;
                case 22: goto L_0x039d;
                case 23: goto L_0x0394;
                case 24: goto L_0x038b;
                case 25: goto L_0x0382;
                case 26: goto L_0x0379;
                case 27: goto L_0x036c;
                case 28: goto L_0x0363;
                case 29: goto L_0x035a;
                case 30: goto L_0x0350;
                case 31: goto L_0x0346;
                case 32: goto L_0x033c;
                case 33: goto L_0x0332;
                case 34: goto L_0x0328;
                case 35: goto L_0x0308;
                case 36: goto L_0x02eb;
                case 37: goto L_0x02ce;
                case 38: goto L_0x02b1;
                case 39: goto L_0x0293;
                case 40: goto L_0x0275;
                case 41: goto L_0x0257;
                case 42: goto L_0x0239;
                case 43: goto L_0x021b;
                case 44: goto L_0x01fd;
                case 45: goto L_0x01df;
                case 46: goto L_0x01c1;
                case 47: goto L_0x01a3;
                case 48: goto L_0x0185;
                case 49: goto L_0x0177;
                case 50: goto L_0x0167;
                case 51: goto L_0x0159;
                case 52: goto L_0x014d;
                case 53: goto L_0x013d;
                case 54: goto L_0x012d;
                case 55: goto L_0x011d;
                case 56: goto L_0x0111;
                case 57: goto L_0x0105;
                case 58: goto L_0x00f9;
                case 59: goto L_0x00db;
                case 60: goto L_0x00c7;
                case 61: goto L_0x00b5;
                case 62: goto L_0x00a5;
                case 63: goto L_0x0095;
                case 64: goto L_0x0089;
                case 65: goto L_0x007d;
                case 66: goto L_0x006d;
                case 67: goto L_0x005d;
                case 68: goto L_0x0047;
                default: goto L_0x0045;
            }
        L_0x0045:
            goto L_0x04e4
        L_0x0047:
            boolean r14 = r0.zza(r1, (int) r3, (int) r12)
            if (r14 == 0) goto L_0x04e4
            java.lang.Object r5 = com.google.android.gms.internal.firebase_auth.zzlf.zzf(r1, r5)
            com.google.android.gms.internal.firebase_auth.zzjr r5 = (com.google.android.gms.internal.firebase_auth.zzjr) r5
            com.google.android.gms.internal.firebase_auth.zzkh r6 = r0.zza((int) r12)
            int r3 = com.google.android.gms.internal.firebase_auth.zzhq.zzc(r3, r5, r6)
            goto L_0x03c9
        L_0x005d:
            boolean r14 = r0.zza(r1, (int) r3, (int) r12)
            if (r14 == 0) goto L_0x04e4
            long r5 = zze(r1, r5)
            int r3 = com.google.android.gms.internal.firebase_auth.zzhq.zzf((int) r3, (long) r5)
            goto L_0x03c9
        L_0x006d:
            boolean r14 = r0.zza(r1, (int) r3, (int) r12)
            if (r14 == 0) goto L_0x04e4
            int r5 = zzd(r1, r5)
            int r3 = com.google.android.gms.internal.firebase_auth.zzhq.zzh((int) r3, (int) r5)
            goto L_0x03c9
        L_0x007d:
            boolean r5 = r0.zza(r1, (int) r3, (int) r12)
            if (r5 == 0) goto L_0x04e4
            int r3 = com.google.android.gms.internal.firebase_auth.zzhq.zzh((int) r3, (long) r9)
            goto L_0x03c9
        L_0x0089:
            boolean r5 = r0.zza(r1, (int) r3, (int) r12)
            if (r5 == 0) goto L_0x04e4
            int r3 = com.google.android.gms.internal.firebase_auth.zzhq.zzj(r3, r11)
            goto L_0x03c9
        L_0x0095:
            boolean r14 = r0.zza(r1, (int) r3, (int) r12)
            if (r14 == 0) goto L_0x04e4
            int r5 = zzd(r1, r5)
            int r3 = com.google.android.gms.internal.firebase_auth.zzhq.zzk(r3, r5)
            goto L_0x03c9
        L_0x00a5:
            boolean r14 = r0.zza(r1, (int) r3, (int) r12)
            if (r14 == 0) goto L_0x04e4
            int r5 = zzd(r1, r5)
            int r3 = com.google.android.gms.internal.firebase_auth.zzhq.zzg((int) r3, (int) r5)
            goto L_0x03c9
        L_0x00b5:
            boolean r14 = r0.zza(r1, (int) r3, (int) r12)
            if (r14 == 0) goto L_0x04e4
            java.lang.Object r5 = com.google.android.gms.internal.firebase_auth.zzlf.zzf(r1, r5)
            com.google.android.gms.internal.firebase_auth.zzgv r5 = (com.google.android.gms.internal.firebase_auth.zzgv) r5
            int r3 = com.google.android.gms.internal.firebase_auth.zzhq.zzc((int) r3, (com.google.android.gms.internal.firebase_auth.zzgv) r5)
            goto L_0x03c9
        L_0x00c7:
            boolean r14 = r0.zza(r1, (int) r3, (int) r12)
            if (r14 == 0) goto L_0x04e4
            java.lang.Object r5 = com.google.android.gms.internal.firebase_auth.zzlf.zzf(r1, r5)
            com.google.android.gms.internal.firebase_auth.zzkh r6 = r0.zza((int) r12)
            int r3 = com.google.android.gms.internal.firebase_auth.zzkj.zza((int) r3, (java.lang.Object) r5, (com.google.android.gms.internal.firebase_auth.zzkh) r6)
            goto L_0x03c9
        L_0x00db:
            boolean r14 = r0.zza(r1, (int) r3, (int) r12)
            if (r14 == 0) goto L_0x04e4
            java.lang.Object r5 = com.google.android.gms.internal.firebase_auth.zzlf.zzf(r1, r5)
            boolean r6 = r5 instanceof com.google.android.gms.internal.firebase_auth.zzgv
            if (r6 == 0) goto L_0x00f1
            com.google.android.gms.internal.firebase_auth.zzgv r5 = (com.google.android.gms.internal.firebase_auth.zzgv) r5
            int r3 = com.google.android.gms.internal.firebase_auth.zzhq.zzc((int) r3, (com.google.android.gms.internal.firebase_auth.zzgv) r5)
            goto L_0x03c9
        L_0x00f1:
            java.lang.String r5 = (java.lang.String) r5
            int r3 = com.google.android.gms.internal.firebase_auth.zzhq.zzb((int) r3, (java.lang.String) r5)
            goto L_0x03c9
        L_0x00f9:
            boolean r5 = r0.zza(r1, (int) r3, (int) r12)
            if (r5 == 0) goto L_0x04e4
            int r3 = com.google.android.gms.internal.firebase_auth.zzhq.zzb((int) r3, (boolean) r7)
            goto L_0x03c9
        L_0x0105:
            boolean r5 = r0.zza(r1, (int) r3, (int) r12)
            if (r5 == 0) goto L_0x04e4
            int r3 = com.google.android.gms.internal.firebase_auth.zzhq.zzi(r3, r11)
            goto L_0x03c9
        L_0x0111:
            boolean r5 = r0.zza(r1, (int) r3, (int) r12)
            if (r5 == 0) goto L_0x04e4
            int r3 = com.google.android.gms.internal.firebase_auth.zzhq.zzg((int) r3, (long) r9)
            goto L_0x03c9
        L_0x011d:
            boolean r14 = r0.zza(r1, (int) r3, (int) r12)
            if (r14 == 0) goto L_0x04e4
            int r5 = zzd(r1, r5)
            int r3 = com.google.android.gms.internal.firebase_auth.zzhq.zzf((int) r3, (int) r5)
            goto L_0x03c9
        L_0x012d:
            boolean r14 = r0.zza(r1, (int) r3, (int) r12)
            if (r14 == 0) goto L_0x04e4
            long r5 = zze(r1, r5)
            int r3 = com.google.android.gms.internal.firebase_auth.zzhq.zze((int) r3, (long) r5)
            goto L_0x03c9
        L_0x013d:
            boolean r14 = r0.zza(r1, (int) r3, (int) r12)
            if (r14 == 0) goto L_0x04e4
            long r5 = zze(r1, r5)
            int r3 = com.google.android.gms.internal.firebase_auth.zzhq.zzd((int) r3, (long) r5)
            goto L_0x03c9
        L_0x014d:
            boolean r5 = r0.zza(r1, (int) r3, (int) r12)
            if (r5 == 0) goto L_0x04e4
            int r3 = com.google.android.gms.internal.firebase_auth.zzhq.zzb((int) r3, (float) r4)
            goto L_0x03c9
        L_0x0159:
            boolean r5 = r0.zza(r1, (int) r3, (int) r12)
            if (r5 == 0) goto L_0x04e4
            r5 = 0
            int r3 = com.google.android.gms.internal.firebase_auth.zzhq.zzb((int) r3, (double) r5)
            goto L_0x03c9
        L_0x0167:
            com.google.android.gms.internal.firebase_auth.zzjk r14 = r0.zzs
            java.lang.Object r5 = com.google.android.gms.internal.firebase_auth.zzlf.zzf(r1, r5)
            java.lang.Object r6 = r0.zzb((int) r12)
            int r3 = r14.zza(r3, r5, r6)
            goto L_0x03c9
        L_0x0177:
            java.util.List r5 = zza((java.lang.Object) r1, (long) r5)
            com.google.android.gms.internal.firebase_auth.zzkh r6 = r0.zza((int) r12)
            int r3 = com.google.android.gms.internal.firebase_auth.zzkj.zzb((int) r3, (java.util.List<com.google.android.gms.internal.firebase_auth.zzjr>) r5, (com.google.android.gms.internal.firebase_auth.zzkh) r6)
            goto L_0x03c9
        L_0x0185:
            java.lang.Object r5 = r2.getObject(r1, r5)
            java.util.List r5 = (java.util.List) r5
            int r5 = com.google.android.gms.internal.firebase_auth.zzkj.zzc(r5)
            if (r5 <= 0) goto L_0x04e4
            boolean r6 = r0.zzk
            if (r6 == 0) goto L_0x0199
            long r14 = (long) r14
            r2.putInt(r1, r14, r5)
        L_0x0199:
            int r3 = com.google.android.gms.internal.firebase_auth.zzhq.zze((int) r3)
            int r6 = com.google.android.gms.internal.firebase_auth.zzhq.zzg((int) r5)
            goto L_0x0324
        L_0x01a3:
            java.lang.Object r5 = r2.getObject(r1, r5)
            java.util.List r5 = (java.util.List) r5
            int r5 = com.google.android.gms.internal.firebase_auth.zzkj.zzg(r5)
            if (r5 <= 0) goto L_0x04e4
            boolean r6 = r0.zzk
            if (r6 == 0) goto L_0x01b7
            long r14 = (long) r14
            r2.putInt(r1, r14, r5)
        L_0x01b7:
            int r3 = com.google.android.gms.internal.firebase_auth.zzhq.zze((int) r3)
            int r6 = com.google.android.gms.internal.firebase_auth.zzhq.zzg((int) r5)
            goto L_0x0324
        L_0x01c1:
            java.lang.Object r5 = r2.getObject(r1, r5)
            java.util.List r5 = (java.util.List) r5
            int r5 = com.google.android.gms.internal.firebase_auth.zzkj.zzi(r5)
            if (r5 <= 0) goto L_0x04e4
            boolean r6 = r0.zzk
            if (r6 == 0) goto L_0x01d5
            long r14 = (long) r14
            r2.putInt(r1, r14, r5)
        L_0x01d5:
            int r3 = com.google.android.gms.internal.firebase_auth.zzhq.zze((int) r3)
            int r6 = com.google.android.gms.internal.firebase_auth.zzhq.zzg((int) r5)
            goto L_0x0324
        L_0x01df:
            java.lang.Object r5 = r2.getObject(r1, r5)
            java.util.List r5 = (java.util.List) r5
            int r5 = com.google.android.gms.internal.firebase_auth.zzkj.zzh(r5)
            if (r5 <= 0) goto L_0x04e4
            boolean r6 = r0.zzk
            if (r6 == 0) goto L_0x01f3
            long r14 = (long) r14
            r2.putInt(r1, r14, r5)
        L_0x01f3:
            int r3 = com.google.android.gms.internal.firebase_auth.zzhq.zze((int) r3)
            int r6 = com.google.android.gms.internal.firebase_auth.zzhq.zzg((int) r5)
            goto L_0x0324
        L_0x01fd:
            java.lang.Object r5 = r2.getObject(r1, r5)
            java.util.List r5 = (java.util.List) r5
            int r5 = com.google.android.gms.internal.firebase_auth.zzkj.zzd(r5)
            if (r5 <= 0) goto L_0x04e4
            boolean r6 = r0.zzk
            if (r6 == 0) goto L_0x0211
            long r14 = (long) r14
            r2.putInt(r1, r14, r5)
        L_0x0211:
            int r3 = com.google.android.gms.internal.firebase_auth.zzhq.zze((int) r3)
            int r6 = com.google.android.gms.internal.firebase_auth.zzhq.zzg((int) r5)
            goto L_0x0324
        L_0x021b:
            java.lang.Object r5 = r2.getObject(r1, r5)
            java.util.List r5 = (java.util.List) r5
            int r5 = com.google.android.gms.internal.firebase_auth.zzkj.zzf(r5)
            if (r5 <= 0) goto L_0x04e4
            boolean r6 = r0.zzk
            if (r6 == 0) goto L_0x022f
            long r14 = (long) r14
            r2.putInt(r1, r14, r5)
        L_0x022f:
            int r3 = com.google.android.gms.internal.firebase_auth.zzhq.zze((int) r3)
            int r6 = com.google.android.gms.internal.firebase_auth.zzhq.zzg((int) r5)
            goto L_0x0324
        L_0x0239:
            java.lang.Object r5 = r2.getObject(r1, r5)
            java.util.List r5 = (java.util.List) r5
            int r5 = com.google.android.gms.internal.firebase_auth.zzkj.zzj(r5)
            if (r5 <= 0) goto L_0x04e4
            boolean r6 = r0.zzk
            if (r6 == 0) goto L_0x024d
            long r14 = (long) r14
            r2.putInt(r1, r14, r5)
        L_0x024d:
            int r3 = com.google.android.gms.internal.firebase_auth.zzhq.zze((int) r3)
            int r6 = com.google.android.gms.internal.firebase_auth.zzhq.zzg((int) r5)
            goto L_0x0324
        L_0x0257:
            java.lang.Object r5 = r2.getObject(r1, r5)
            java.util.List r5 = (java.util.List) r5
            int r5 = com.google.android.gms.internal.firebase_auth.zzkj.zzh(r5)
            if (r5 <= 0) goto L_0x04e4
            boolean r6 = r0.zzk
            if (r6 == 0) goto L_0x026b
            long r14 = (long) r14
            r2.putInt(r1, r14, r5)
        L_0x026b:
            int r3 = com.google.android.gms.internal.firebase_auth.zzhq.zze((int) r3)
            int r6 = com.google.android.gms.internal.firebase_auth.zzhq.zzg((int) r5)
            goto L_0x0324
        L_0x0275:
            java.lang.Object r5 = r2.getObject(r1, r5)
            java.util.List r5 = (java.util.List) r5
            int r5 = com.google.android.gms.internal.firebase_auth.zzkj.zzi(r5)
            if (r5 <= 0) goto L_0x04e4
            boolean r6 = r0.zzk
            if (r6 == 0) goto L_0x0289
            long r14 = (long) r14
            r2.putInt(r1, r14, r5)
        L_0x0289:
            int r3 = com.google.android.gms.internal.firebase_auth.zzhq.zze((int) r3)
            int r6 = com.google.android.gms.internal.firebase_auth.zzhq.zzg((int) r5)
            goto L_0x0324
        L_0x0293:
            java.lang.Object r5 = r2.getObject(r1, r5)
            java.util.List r5 = (java.util.List) r5
            int r5 = com.google.android.gms.internal.firebase_auth.zzkj.zze(r5)
            if (r5 <= 0) goto L_0x04e4
            boolean r6 = r0.zzk
            if (r6 == 0) goto L_0x02a7
            long r14 = (long) r14
            r2.putInt(r1, r14, r5)
        L_0x02a7:
            int r3 = com.google.android.gms.internal.firebase_auth.zzhq.zze((int) r3)
            int r6 = com.google.android.gms.internal.firebase_auth.zzhq.zzg((int) r5)
            goto L_0x0324
        L_0x02b1:
            java.lang.Object r5 = r2.getObject(r1, r5)
            java.util.List r5 = (java.util.List) r5
            int r5 = com.google.android.gms.internal.firebase_auth.zzkj.zzb(r5)
            if (r5 <= 0) goto L_0x04e4
            boolean r6 = r0.zzk
            if (r6 == 0) goto L_0x02c5
            long r14 = (long) r14
            r2.putInt(r1, r14, r5)
        L_0x02c5:
            int r3 = com.google.android.gms.internal.firebase_auth.zzhq.zze((int) r3)
            int r6 = com.google.android.gms.internal.firebase_auth.zzhq.zzg((int) r5)
            goto L_0x0324
        L_0x02ce:
            java.lang.Object r5 = r2.getObject(r1, r5)
            java.util.List r5 = (java.util.List) r5
            int r5 = com.google.android.gms.internal.firebase_auth.zzkj.zza((java.util.List<java.lang.Long>) r5)
            if (r5 <= 0) goto L_0x04e4
            boolean r6 = r0.zzk
            if (r6 == 0) goto L_0x02e2
            long r14 = (long) r14
            r2.putInt(r1, r14, r5)
        L_0x02e2:
            int r3 = com.google.android.gms.internal.firebase_auth.zzhq.zze((int) r3)
            int r6 = com.google.android.gms.internal.firebase_auth.zzhq.zzg((int) r5)
            goto L_0x0324
        L_0x02eb:
            java.lang.Object r5 = r2.getObject(r1, r5)
            java.util.List r5 = (java.util.List) r5
            int r5 = com.google.android.gms.internal.firebase_auth.zzkj.zzh(r5)
            if (r5 <= 0) goto L_0x04e4
            boolean r6 = r0.zzk
            if (r6 == 0) goto L_0x02ff
            long r14 = (long) r14
            r2.putInt(r1, r14, r5)
        L_0x02ff:
            int r3 = com.google.android.gms.internal.firebase_auth.zzhq.zze((int) r3)
            int r6 = com.google.android.gms.internal.firebase_auth.zzhq.zzg((int) r5)
            goto L_0x0324
        L_0x0308:
            java.lang.Object r5 = r2.getObject(r1, r5)
            java.util.List r5 = (java.util.List) r5
            int r5 = com.google.android.gms.internal.firebase_auth.zzkj.zzi(r5)
            if (r5 <= 0) goto L_0x04e4
            boolean r6 = r0.zzk
            if (r6 == 0) goto L_0x031c
            long r14 = (long) r14
            r2.putInt(r1, r14, r5)
        L_0x031c:
            int r3 = com.google.android.gms.internal.firebase_auth.zzhq.zze((int) r3)
            int r6 = com.google.android.gms.internal.firebase_auth.zzhq.zzg((int) r5)
        L_0x0324:
            int r3 = r3 + r6
            int r3 = r3 + r5
            goto L_0x03c9
        L_0x0328:
            java.util.List r5 = zza((java.lang.Object) r1, (long) r5)
            int r3 = com.google.android.gms.internal.firebase_auth.zzkj.zzc(r3, r5, r11)
            goto L_0x03c9
        L_0x0332:
            java.util.List r5 = zza((java.lang.Object) r1, (long) r5)
            int r3 = com.google.android.gms.internal.firebase_auth.zzkj.zzg(r3, r5, r11)
            goto L_0x03c9
        L_0x033c:
            java.util.List r5 = zza((java.lang.Object) r1, (long) r5)
            int r3 = com.google.android.gms.internal.firebase_auth.zzkj.zzi(r3, r5, r11)
            goto L_0x03c9
        L_0x0346:
            java.util.List r5 = zza((java.lang.Object) r1, (long) r5)
            int r3 = com.google.android.gms.internal.firebase_auth.zzkj.zzh(r3, r5, r11)
            goto L_0x03c9
        L_0x0350:
            java.util.List r5 = zza((java.lang.Object) r1, (long) r5)
            int r3 = com.google.android.gms.internal.firebase_auth.zzkj.zzd(r3, r5, r11)
            goto L_0x03c9
        L_0x035a:
            java.util.List r5 = zza((java.lang.Object) r1, (long) r5)
            int r3 = com.google.android.gms.internal.firebase_auth.zzkj.zzf(r3, r5, r11)
            goto L_0x03c9
        L_0x0363:
            java.util.List r5 = zza((java.lang.Object) r1, (long) r5)
            int r3 = com.google.android.gms.internal.firebase_auth.zzkj.zzb(r3, r5)
            goto L_0x03c9
        L_0x036c:
            java.util.List r5 = zza((java.lang.Object) r1, (long) r5)
            com.google.android.gms.internal.firebase_auth.zzkh r6 = r0.zza((int) r12)
            int r3 = com.google.android.gms.internal.firebase_auth.zzkj.zza((int) r3, (java.util.List<?>) r5, (com.google.android.gms.internal.firebase_auth.zzkh) r6)
            goto L_0x03c9
        L_0x0379:
            java.util.List r5 = zza((java.lang.Object) r1, (long) r5)
            int r3 = com.google.android.gms.internal.firebase_auth.zzkj.zza((int) r3, (java.util.List<?>) r5)
            goto L_0x03c9
        L_0x0382:
            java.util.List r5 = zza((java.lang.Object) r1, (long) r5)
            int r3 = com.google.android.gms.internal.firebase_auth.zzkj.zzj(r3, r5, r11)
            goto L_0x03c9
        L_0x038b:
            java.util.List r5 = zza((java.lang.Object) r1, (long) r5)
            int r3 = com.google.android.gms.internal.firebase_auth.zzkj.zzh(r3, r5, r11)
            goto L_0x03c9
        L_0x0394:
            java.util.List r5 = zza((java.lang.Object) r1, (long) r5)
            int r3 = com.google.android.gms.internal.firebase_auth.zzkj.zzi(r3, r5, r11)
            goto L_0x03c9
        L_0x039d:
            java.util.List r5 = zza((java.lang.Object) r1, (long) r5)
            int r3 = com.google.android.gms.internal.firebase_auth.zzkj.zze(r3, r5, r11)
            goto L_0x03c9
        L_0x03a6:
            java.util.List r5 = zza((java.lang.Object) r1, (long) r5)
            int r3 = com.google.android.gms.internal.firebase_auth.zzkj.zzb((int) r3, (java.util.List<java.lang.Long>) r5, (boolean) r11)
            goto L_0x03c9
        L_0x03af:
            java.util.List r5 = zza((java.lang.Object) r1, (long) r5)
            int r3 = com.google.android.gms.internal.firebase_auth.zzkj.zza((int) r3, (java.util.List<java.lang.Long>) r5, (boolean) r11)
            goto L_0x03c9
        L_0x03b8:
            java.util.List r5 = zza((java.lang.Object) r1, (long) r5)
            int r3 = com.google.android.gms.internal.firebase_auth.zzkj.zzh(r3, r5, r11)
            goto L_0x03c9
        L_0x03c1:
            java.util.List r5 = zza((java.lang.Object) r1, (long) r5)
            int r3 = com.google.android.gms.internal.firebase_auth.zzkj.zzi(r3, r5, r11)
        L_0x03c9:
            int r13 = r13 + r3
            goto L_0x04e4
        L_0x03cc:
            boolean r14 = r0.zza(r1, (int) r12)
            if (r14 == 0) goto L_0x04e4
            java.lang.Object r5 = com.google.android.gms.internal.firebase_auth.zzlf.zzf(r1, r5)
            com.google.android.gms.internal.firebase_auth.zzjr r5 = (com.google.android.gms.internal.firebase_auth.zzjr) r5
            com.google.android.gms.internal.firebase_auth.zzkh r6 = r0.zza((int) r12)
            int r3 = com.google.android.gms.internal.firebase_auth.zzhq.zzc(r3, r5, r6)
            goto L_0x03c9
        L_0x03e1:
            boolean r14 = r0.zza(r1, (int) r12)
            if (r14 == 0) goto L_0x04e4
            long r5 = com.google.android.gms.internal.firebase_auth.zzlf.zzb(r1, r5)
            int r3 = com.google.android.gms.internal.firebase_auth.zzhq.zzf((int) r3, (long) r5)
            goto L_0x03c9
        L_0x03f0:
            boolean r14 = r0.zza(r1, (int) r12)
            if (r14 == 0) goto L_0x04e4
            int r5 = com.google.android.gms.internal.firebase_auth.zzlf.zza((java.lang.Object) r1, (long) r5)
            int r3 = com.google.android.gms.internal.firebase_auth.zzhq.zzh((int) r3, (int) r5)
            goto L_0x03c9
        L_0x03ff:
            boolean r5 = r0.zza(r1, (int) r12)
            if (r5 == 0) goto L_0x04e4
            int r3 = com.google.android.gms.internal.firebase_auth.zzhq.zzh((int) r3, (long) r9)
            goto L_0x03c9
        L_0x040a:
            boolean r5 = r0.zza(r1, (int) r12)
            if (r5 == 0) goto L_0x04e4
            int r3 = com.google.android.gms.internal.firebase_auth.zzhq.zzj(r3, r11)
            goto L_0x03c9
        L_0x0415:
            boolean r14 = r0.zza(r1, (int) r12)
            if (r14 == 0) goto L_0x04e4
            int r5 = com.google.android.gms.internal.firebase_auth.zzlf.zza((java.lang.Object) r1, (long) r5)
            int r3 = com.google.android.gms.internal.firebase_auth.zzhq.zzk(r3, r5)
            goto L_0x03c9
        L_0x0424:
            boolean r14 = r0.zza(r1, (int) r12)
            if (r14 == 0) goto L_0x04e4
            int r5 = com.google.android.gms.internal.firebase_auth.zzlf.zza((java.lang.Object) r1, (long) r5)
            int r3 = com.google.android.gms.internal.firebase_auth.zzhq.zzg((int) r3, (int) r5)
            goto L_0x03c9
        L_0x0433:
            boolean r14 = r0.zza(r1, (int) r12)
            if (r14 == 0) goto L_0x04e4
            java.lang.Object r5 = com.google.android.gms.internal.firebase_auth.zzlf.zzf(r1, r5)
            com.google.android.gms.internal.firebase_auth.zzgv r5 = (com.google.android.gms.internal.firebase_auth.zzgv) r5
            int r3 = com.google.android.gms.internal.firebase_auth.zzhq.zzc((int) r3, (com.google.android.gms.internal.firebase_auth.zzgv) r5)
            goto L_0x03c9
        L_0x0444:
            boolean r14 = r0.zza(r1, (int) r12)
            if (r14 == 0) goto L_0x04e4
            java.lang.Object r5 = com.google.android.gms.internal.firebase_auth.zzlf.zzf(r1, r5)
            com.google.android.gms.internal.firebase_auth.zzkh r6 = r0.zza((int) r12)
            int r3 = com.google.android.gms.internal.firebase_auth.zzkj.zza((int) r3, (java.lang.Object) r5, (com.google.android.gms.internal.firebase_auth.zzkh) r6)
            goto L_0x03c9
        L_0x0458:
            boolean r14 = r0.zza(r1, (int) r12)
            if (r14 == 0) goto L_0x04e4
            java.lang.Object r5 = com.google.android.gms.internal.firebase_auth.zzlf.zzf(r1, r5)
            boolean r6 = r5 instanceof com.google.android.gms.internal.firebase_auth.zzgv
            if (r6 == 0) goto L_0x046e
            com.google.android.gms.internal.firebase_auth.zzgv r5 = (com.google.android.gms.internal.firebase_auth.zzgv) r5
            int r3 = com.google.android.gms.internal.firebase_auth.zzhq.zzc((int) r3, (com.google.android.gms.internal.firebase_auth.zzgv) r5)
            goto L_0x03c9
        L_0x046e:
            java.lang.String r5 = (java.lang.String) r5
            int r3 = com.google.android.gms.internal.firebase_auth.zzhq.zzb((int) r3, (java.lang.String) r5)
            goto L_0x03c9
        L_0x0476:
            boolean r5 = r0.zza(r1, (int) r12)
            if (r5 == 0) goto L_0x04e4
            int r3 = com.google.android.gms.internal.firebase_auth.zzhq.zzb((int) r3, (boolean) r7)
            goto L_0x03c9
        L_0x0482:
            boolean r5 = r0.zza(r1, (int) r12)
            if (r5 == 0) goto L_0x04e4
            int r3 = com.google.android.gms.internal.firebase_auth.zzhq.zzi(r3, r11)
            goto L_0x03c9
        L_0x048e:
            boolean r5 = r0.zza(r1, (int) r12)
            if (r5 == 0) goto L_0x04e4
            int r3 = com.google.android.gms.internal.firebase_auth.zzhq.zzg((int) r3, (long) r9)
            goto L_0x03c9
        L_0x049a:
            boolean r14 = r0.zza(r1, (int) r12)
            if (r14 == 0) goto L_0x04e4
            int r5 = com.google.android.gms.internal.firebase_auth.zzlf.zza((java.lang.Object) r1, (long) r5)
            int r3 = com.google.android.gms.internal.firebase_auth.zzhq.zzf((int) r3, (int) r5)
            goto L_0x03c9
        L_0x04aa:
            boolean r14 = r0.zza(r1, (int) r12)
            if (r14 == 0) goto L_0x04e4
            long r5 = com.google.android.gms.internal.firebase_auth.zzlf.zzb(r1, r5)
            int r3 = com.google.android.gms.internal.firebase_auth.zzhq.zze((int) r3, (long) r5)
            goto L_0x03c9
        L_0x04ba:
            boolean r14 = r0.zza(r1, (int) r12)
            if (r14 == 0) goto L_0x04e4
            long r5 = com.google.android.gms.internal.firebase_auth.zzlf.zzb(r1, r5)
            int r3 = com.google.android.gms.internal.firebase_auth.zzhq.zzd((int) r3, (long) r5)
            goto L_0x03c9
        L_0x04ca:
            boolean r5 = r0.zza(r1, (int) r12)
            if (r5 == 0) goto L_0x04e4
            int r3 = com.google.android.gms.internal.firebase_auth.zzhq.zzb((int) r3, (float) r4)
            goto L_0x03c9
        L_0x04d6:
            boolean r5 = r0.zza(r1, (int) r12)
            if (r5 == 0) goto L_0x04e4
            r5 = 0
            int r3 = com.google.android.gms.internal.firebase_auth.zzhq.zzb((int) r3, (double) r5)
            goto L_0x03c9
        L_0x04e4:
            int r12 = r12 + 3
            r3 = 267386880(0xff00000, float:2.3665827E-29)
            goto L_0x0016
        L_0x04ea:
            com.google.android.gms.internal.firebase_auth.zzkz<?, ?> r2 = r0.zzq
            int r1 = zza(r2, r1)
            int r13 = r13 + r1
            return r13
        L_0x04f2:
            sun.misc.Unsafe r2 = zzb
            r3 = 0
            r5 = 0
            r6 = 1048575(0xfffff, float:1.469367E-39)
            r12 = 0
        L_0x04fa:
            int[] r13 = r0.zzc
            int r13 = r13.length
            if (r3 >= r13) goto L_0x0a26
            int r13 = r0.zzd((int) r3)
            int[] r14 = r0.zzc
            r15 = r14[r3]
            r16 = 267386880(0xff00000, float:2.3665827E-29)
            r17 = r13 & r16
            int r4 = r17 >>> 20
            r11 = 17
            if (r4 > r11) goto L_0x0525
            int r11 = r3 + 2
            r11 = r14[r11]
            r14 = r11 & r8
            int r18 = r11 >>> 20
            int r18 = r7 << r18
            if (r14 == r6) goto L_0x0523
            long r9 = (long) r14
            int r12 = r2.getInt(r1, r9)
            r6 = r14
        L_0x0523:
            r9 = r11
            goto L_0x0544
        L_0x0525:
            boolean r9 = r0.zzk
            if (r9 == 0) goto L_0x0541
            com.google.android.gms.internal.firebase_auth.zzia r9 = com.google.android.gms.internal.firebase_auth.zzia.DOUBLE_LIST_PACKED
            int r9 = r9.zza()
            if (r4 < r9) goto L_0x0541
            com.google.android.gms.internal.firebase_auth.zzia r9 = com.google.android.gms.internal.firebase_auth.zzia.SINT64_LIST_PACKED
            int r9 = r9.zza()
            if (r4 > r9) goto L_0x0541
            int[] r9 = r0.zzc
            int r10 = r3 + 2
            r9 = r9[r10]
            r9 = r9 & r8
            goto L_0x0542
        L_0x0541:
            r9 = 0
        L_0x0542:
            r18 = 0
        L_0x0544:
            r10 = r13 & r8
            long r10 = (long) r10
            switch(r4) {
                case 0: goto L_0x0a10;
                case 1: goto L_0x0a00;
                case 2: goto L_0x09ee;
                case 3: goto L_0x09de;
                case 4: goto L_0x09ce;
                case 5: goto L_0x09c2;
                case 6: goto L_0x09b6;
                case 7: goto L_0x09ac;
                case 8: goto L_0x0990;
                case 9: goto L_0x097e;
                case 10: goto L_0x096f;
                case 11: goto L_0x0962;
                case 12: goto L_0x0955;
                case 13: goto L_0x094a;
                case 14: goto L_0x093f;
                case 15: goto L_0x0932;
                case 16: goto L_0x0925;
                case 17: goto L_0x0912;
                case 18: goto L_0x08fe;
                case 19: goto L_0x08f2;
                case 20: goto L_0x08e6;
                case 21: goto L_0x08da;
                case 22: goto L_0x08ce;
                case 23: goto L_0x08c2;
                case 24: goto L_0x08b6;
                case 25: goto L_0x08aa;
                case 26: goto L_0x089f;
                case 27: goto L_0x0890;
                case 28: goto L_0x0884;
                case 29: goto L_0x0877;
                case 30: goto L_0x086a;
                case 31: goto L_0x085d;
                case 32: goto L_0x0850;
                case 33: goto L_0x0843;
                case 34: goto L_0x0836;
                case 35: goto L_0x0816;
                case 36: goto L_0x07f9;
                case 37: goto L_0x07dc;
                case 38: goto L_0x07bf;
                case 39: goto L_0x07a1;
                case 40: goto L_0x0783;
                case 41: goto L_0x0765;
                case 42: goto L_0x0747;
                case 43: goto L_0x0729;
                case 44: goto L_0x070b;
                case 45: goto L_0x06ed;
                case 46: goto L_0x06cf;
                case 47: goto L_0x06b1;
                case 48: goto L_0x0693;
                case 49: goto L_0x0683;
                case 50: goto L_0x0673;
                case 51: goto L_0x0665;
                case 52: goto L_0x0658;
                case 53: goto L_0x0648;
                case 54: goto L_0x0638;
                case 55: goto L_0x0628;
                case 56: goto L_0x061a;
                case 57: goto L_0x060d;
                case 58: goto L_0x0601;
                case 59: goto L_0x05e3;
                case 60: goto L_0x05cf;
                case 61: goto L_0x05bd;
                case 62: goto L_0x05ad;
                case 63: goto L_0x059d;
                case 64: goto L_0x0590;
                case 65: goto L_0x0582;
                case 66: goto L_0x0572;
                case 67: goto L_0x0562;
                case 68: goto L_0x054c;
                default: goto L_0x054a;
            }
        L_0x054a:
            goto L_0x090a
        L_0x054c:
            boolean r4 = r0.zza(r1, (int) r15, (int) r3)
            if (r4 == 0) goto L_0x090a
            java.lang.Object r4 = r2.getObject(r1, r10)
            com.google.android.gms.internal.firebase_auth.zzjr r4 = (com.google.android.gms.internal.firebase_auth.zzjr) r4
            com.google.android.gms.internal.firebase_auth.zzkh r9 = r0.zza((int) r3)
            int r4 = com.google.android.gms.internal.firebase_auth.zzhq.zzc(r15, r4, r9)
            goto L_0x0909
        L_0x0562:
            boolean r4 = r0.zza(r1, (int) r15, (int) r3)
            if (r4 == 0) goto L_0x090a
            long r9 = zze(r1, r10)
            int r4 = com.google.android.gms.internal.firebase_auth.zzhq.zzf((int) r15, (long) r9)
            goto L_0x0909
        L_0x0572:
            boolean r4 = r0.zza(r1, (int) r15, (int) r3)
            if (r4 == 0) goto L_0x090a
            int r4 = zzd(r1, r10)
            int r4 = com.google.android.gms.internal.firebase_auth.zzhq.zzh((int) r15, (int) r4)
            goto L_0x0909
        L_0x0582:
            boolean r4 = r0.zza(r1, (int) r15, (int) r3)
            if (r4 == 0) goto L_0x090a
            r9 = 0
            int r4 = com.google.android.gms.internal.firebase_auth.zzhq.zzh((int) r15, (long) r9)
            goto L_0x0909
        L_0x0590:
            boolean r4 = r0.zza(r1, (int) r15, (int) r3)
            if (r4 == 0) goto L_0x090a
            r4 = 0
            int r9 = com.google.android.gms.internal.firebase_auth.zzhq.zzj(r15, r4)
            goto L_0x0953
        L_0x059d:
            boolean r4 = r0.zza(r1, (int) r15, (int) r3)
            if (r4 == 0) goto L_0x090a
            int r4 = zzd(r1, r10)
            int r4 = com.google.android.gms.internal.firebase_auth.zzhq.zzk(r15, r4)
            goto L_0x0909
        L_0x05ad:
            boolean r4 = r0.zza(r1, (int) r15, (int) r3)
            if (r4 == 0) goto L_0x090a
            int r4 = zzd(r1, r10)
            int r4 = com.google.android.gms.internal.firebase_auth.zzhq.zzg((int) r15, (int) r4)
            goto L_0x0909
        L_0x05bd:
            boolean r4 = r0.zza(r1, (int) r15, (int) r3)
            if (r4 == 0) goto L_0x090a
            java.lang.Object r4 = r2.getObject(r1, r10)
            com.google.android.gms.internal.firebase_auth.zzgv r4 = (com.google.android.gms.internal.firebase_auth.zzgv) r4
            int r4 = com.google.android.gms.internal.firebase_auth.zzhq.zzc((int) r15, (com.google.android.gms.internal.firebase_auth.zzgv) r4)
            goto L_0x0909
        L_0x05cf:
            boolean r4 = r0.zza(r1, (int) r15, (int) r3)
            if (r4 == 0) goto L_0x090a
            java.lang.Object r4 = r2.getObject(r1, r10)
            com.google.android.gms.internal.firebase_auth.zzkh r9 = r0.zza((int) r3)
            int r4 = com.google.android.gms.internal.firebase_auth.zzkj.zza((int) r15, (java.lang.Object) r4, (com.google.android.gms.internal.firebase_auth.zzkh) r9)
            goto L_0x0909
        L_0x05e3:
            boolean r4 = r0.zza(r1, (int) r15, (int) r3)
            if (r4 == 0) goto L_0x090a
            java.lang.Object r4 = r2.getObject(r1, r10)
            boolean r9 = r4 instanceof com.google.android.gms.internal.firebase_auth.zzgv
            if (r9 == 0) goto L_0x05f9
            com.google.android.gms.internal.firebase_auth.zzgv r4 = (com.google.android.gms.internal.firebase_auth.zzgv) r4
            int r4 = com.google.android.gms.internal.firebase_auth.zzhq.zzc((int) r15, (com.google.android.gms.internal.firebase_auth.zzgv) r4)
            goto L_0x0909
        L_0x05f9:
            java.lang.String r4 = (java.lang.String) r4
            int r4 = com.google.android.gms.internal.firebase_auth.zzhq.zzb((int) r15, (java.lang.String) r4)
            goto L_0x0909
        L_0x0601:
            boolean r4 = r0.zza(r1, (int) r15, (int) r3)
            if (r4 == 0) goto L_0x090a
            int r4 = com.google.android.gms.internal.firebase_auth.zzhq.zzb((int) r15, (boolean) r7)
            goto L_0x0909
        L_0x060d:
            boolean r4 = r0.zza(r1, (int) r15, (int) r3)
            if (r4 == 0) goto L_0x090a
            r4 = 0
            int r9 = com.google.android.gms.internal.firebase_auth.zzhq.zzi(r15, r4)
            goto L_0x0953
        L_0x061a:
            boolean r4 = r0.zza(r1, (int) r15, (int) r3)
            if (r4 == 0) goto L_0x090a
            r9 = 0
            int r4 = com.google.android.gms.internal.firebase_auth.zzhq.zzg((int) r15, (long) r9)
            goto L_0x0909
        L_0x0628:
            boolean r4 = r0.zza(r1, (int) r15, (int) r3)
            if (r4 == 0) goto L_0x090a
            int r4 = zzd(r1, r10)
            int r4 = com.google.android.gms.internal.firebase_auth.zzhq.zzf((int) r15, (int) r4)
            goto L_0x0909
        L_0x0638:
            boolean r4 = r0.zza(r1, (int) r15, (int) r3)
            if (r4 == 0) goto L_0x090a
            long r9 = zze(r1, r10)
            int r4 = com.google.android.gms.internal.firebase_auth.zzhq.zze((int) r15, (long) r9)
            goto L_0x0909
        L_0x0648:
            boolean r4 = r0.zza(r1, (int) r15, (int) r3)
            if (r4 == 0) goto L_0x090a
            long r9 = zze(r1, r10)
            int r4 = com.google.android.gms.internal.firebase_auth.zzhq.zzd((int) r15, (long) r9)
            goto L_0x0909
        L_0x0658:
            boolean r4 = r0.zza(r1, (int) r15, (int) r3)
            if (r4 == 0) goto L_0x090a
            r4 = 0
            int r9 = com.google.android.gms.internal.firebase_auth.zzhq.zzb((int) r15, (float) r4)
            goto L_0x0953
        L_0x0665:
            boolean r4 = r0.zza(r1, (int) r15, (int) r3)
            if (r4 == 0) goto L_0x090a
            r9 = 0
            int r4 = com.google.android.gms.internal.firebase_auth.zzhq.zzb((int) r15, (double) r9)
            goto L_0x0909
        L_0x0673:
            com.google.android.gms.internal.firebase_auth.zzjk r4 = r0.zzs
            java.lang.Object r9 = r2.getObject(r1, r10)
            java.lang.Object r10 = r0.zzb((int) r3)
            int r4 = r4.zza(r15, r9, r10)
            goto L_0x0909
        L_0x0683:
            java.lang.Object r4 = r2.getObject(r1, r10)
            java.util.List r4 = (java.util.List) r4
            com.google.android.gms.internal.firebase_auth.zzkh r9 = r0.zza((int) r3)
            int r4 = com.google.android.gms.internal.firebase_auth.zzkj.zzb((int) r15, (java.util.List<com.google.android.gms.internal.firebase_auth.zzjr>) r4, (com.google.android.gms.internal.firebase_auth.zzkh) r9)
            goto L_0x0909
        L_0x0693:
            java.lang.Object r4 = r2.getObject(r1, r10)
            java.util.List r4 = (java.util.List) r4
            int r4 = com.google.android.gms.internal.firebase_auth.zzkj.zzc(r4)
            if (r4 <= 0) goto L_0x090a
            boolean r10 = r0.zzk
            if (r10 == 0) goto L_0x06a7
            long r9 = (long) r9
            r2.putInt(r1, r9, r4)
        L_0x06a7:
            int r9 = com.google.android.gms.internal.firebase_auth.zzhq.zze((int) r15)
            int r10 = com.google.android.gms.internal.firebase_auth.zzhq.zzg((int) r4)
            goto L_0x0832
        L_0x06b1:
            java.lang.Object r4 = r2.getObject(r1, r10)
            java.util.List r4 = (java.util.List) r4
            int r4 = com.google.android.gms.internal.firebase_auth.zzkj.zzg(r4)
            if (r4 <= 0) goto L_0x090a
            boolean r10 = r0.zzk
            if (r10 == 0) goto L_0x06c5
            long r9 = (long) r9
            r2.putInt(r1, r9, r4)
        L_0x06c5:
            int r9 = com.google.android.gms.internal.firebase_auth.zzhq.zze((int) r15)
            int r10 = com.google.android.gms.internal.firebase_auth.zzhq.zzg((int) r4)
            goto L_0x0832
        L_0x06cf:
            java.lang.Object r4 = r2.getObject(r1, r10)
            java.util.List r4 = (java.util.List) r4
            int r4 = com.google.android.gms.internal.firebase_auth.zzkj.zzi(r4)
            if (r4 <= 0) goto L_0x090a
            boolean r10 = r0.zzk
            if (r10 == 0) goto L_0x06e3
            long r9 = (long) r9
            r2.putInt(r1, r9, r4)
        L_0x06e3:
            int r9 = com.google.android.gms.internal.firebase_auth.zzhq.zze((int) r15)
            int r10 = com.google.android.gms.internal.firebase_auth.zzhq.zzg((int) r4)
            goto L_0x0832
        L_0x06ed:
            java.lang.Object r4 = r2.getObject(r1, r10)
            java.util.List r4 = (java.util.List) r4
            int r4 = com.google.android.gms.internal.firebase_auth.zzkj.zzh(r4)
            if (r4 <= 0) goto L_0x090a
            boolean r10 = r0.zzk
            if (r10 == 0) goto L_0x0701
            long r9 = (long) r9
            r2.putInt(r1, r9, r4)
        L_0x0701:
            int r9 = com.google.android.gms.internal.firebase_auth.zzhq.zze((int) r15)
            int r10 = com.google.android.gms.internal.firebase_auth.zzhq.zzg((int) r4)
            goto L_0x0832
        L_0x070b:
            java.lang.Object r4 = r2.getObject(r1, r10)
            java.util.List r4 = (java.util.List) r4
            int r4 = com.google.android.gms.internal.firebase_auth.zzkj.zzd(r4)
            if (r4 <= 0) goto L_0x090a
            boolean r10 = r0.zzk
            if (r10 == 0) goto L_0x071f
            long r9 = (long) r9
            r2.putInt(r1, r9, r4)
        L_0x071f:
            int r9 = com.google.android.gms.internal.firebase_auth.zzhq.zze((int) r15)
            int r10 = com.google.android.gms.internal.firebase_auth.zzhq.zzg((int) r4)
            goto L_0x0832
        L_0x0729:
            java.lang.Object r4 = r2.getObject(r1, r10)
            java.util.List r4 = (java.util.List) r4
            int r4 = com.google.android.gms.internal.firebase_auth.zzkj.zzf(r4)
            if (r4 <= 0) goto L_0x090a
            boolean r10 = r0.zzk
            if (r10 == 0) goto L_0x073d
            long r9 = (long) r9
            r2.putInt(r1, r9, r4)
        L_0x073d:
            int r9 = com.google.android.gms.internal.firebase_auth.zzhq.zze((int) r15)
            int r10 = com.google.android.gms.internal.firebase_auth.zzhq.zzg((int) r4)
            goto L_0x0832
        L_0x0747:
            java.lang.Object r4 = r2.getObject(r1, r10)
            java.util.List r4 = (java.util.List) r4
            int r4 = com.google.android.gms.internal.firebase_auth.zzkj.zzj(r4)
            if (r4 <= 0) goto L_0x090a
            boolean r10 = r0.zzk
            if (r10 == 0) goto L_0x075b
            long r9 = (long) r9
            r2.putInt(r1, r9, r4)
        L_0x075b:
            int r9 = com.google.android.gms.internal.firebase_auth.zzhq.zze((int) r15)
            int r10 = com.google.android.gms.internal.firebase_auth.zzhq.zzg((int) r4)
            goto L_0x0832
        L_0x0765:
            java.lang.Object r4 = r2.getObject(r1, r10)
            java.util.List r4 = (java.util.List) r4
            int r4 = com.google.android.gms.internal.firebase_auth.zzkj.zzh(r4)
            if (r4 <= 0) goto L_0x090a
            boolean r10 = r0.zzk
            if (r10 == 0) goto L_0x0779
            long r9 = (long) r9
            r2.putInt(r1, r9, r4)
        L_0x0779:
            int r9 = com.google.android.gms.internal.firebase_auth.zzhq.zze((int) r15)
            int r10 = com.google.android.gms.internal.firebase_auth.zzhq.zzg((int) r4)
            goto L_0x0832
        L_0x0783:
            java.lang.Object r4 = r2.getObject(r1, r10)
            java.util.List r4 = (java.util.List) r4
            int r4 = com.google.android.gms.internal.firebase_auth.zzkj.zzi(r4)
            if (r4 <= 0) goto L_0x090a
            boolean r10 = r0.zzk
            if (r10 == 0) goto L_0x0797
            long r9 = (long) r9
            r2.putInt(r1, r9, r4)
        L_0x0797:
            int r9 = com.google.android.gms.internal.firebase_auth.zzhq.zze((int) r15)
            int r10 = com.google.android.gms.internal.firebase_auth.zzhq.zzg((int) r4)
            goto L_0x0832
        L_0x07a1:
            java.lang.Object r4 = r2.getObject(r1, r10)
            java.util.List r4 = (java.util.List) r4
            int r4 = com.google.android.gms.internal.firebase_auth.zzkj.zze(r4)
            if (r4 <= 0) goto L_0x090a
            boolean r10 = r0.zzk
            if (r10 == 0) goto L_0x07b5
            long r9 = (long) r9
            r2.putInt(r1, r9, r4)
        L_0x07b5:
            int r9 = com.google.android.gms.internal.firebase_auth.zzhq.zze((int) r15)
            int r10 = com.google.android.gms.internal.firebase_auth.zzhq.zzg((int) r4)
            goto L_0x0832
        L_0x07bf:
            java.lang.Object r4 = r2.getObject(r1, r10)
            java.util.List r4 = (java.util.List) r4
            int r4 = com.google.android.gms.internal.firebase_auth.zzkj.zzb(r4)
            if (r4 <= 0) goto L_0x090a
            boolean r10 = r0.zzk
            if (r10 == 0) goto L_0x07d3
            long r9 = (long) r9
            r2.putInt(r1, r9, r4)
        L_0x07d3:
            int r9 = com.google.android.gms.internal.firebase_auth.zzhq.zze((int) r15)
            int r10 = com.google.android.gms.internal.firebase_auth.zzhq.zzg((int) r4)
            goto L_0x0832
        L_0x07dc:
            java.lang.Object r4 = r2.getObject(r1, r10)
            java.util.List r4 = (java.util.List) r4
            int r4 = com.google.android.gms.internal.firebase_auth.zzkj.zza((java.util.List<java.lang.Long>) r4)
            if (r4 <= 0) goto L_0x090a
            boolean r10 = r0.zzk
            if (r10 == 0) goto L_0x07f0
            long r9 = (long) r9
            r2.putInt(r1, r9, r4)
        L_0x07f0:
            int r9 = com.google.android.gms.internal.firebase_auth.zzhq.zze((int) r15)
            int r10 = com.google.android.gms.internal.firebase_auth.zzhq.zzg((int) r4)
            goto L_0x0832
        L_0x07f9:
            java.lang.Object r4 = r2.getObject(r1, r10)
            java.util.List r4 = (java.util.List) r4
            int r4 = com.google.android.gms.internal.firebase_auth.zzkj.zzh(r4)
            if (r4 <= 0) goto L_0x090a
            boolean r10 = r0.zzk
            if (r10 == 0) goto L_0x080d
            long r9 = (long) r9
            r2.putInt(r1, r9, r4)
        L_0x080d:
            int r9 = com.google.android.gms.internal.firebase_auth.zzhq.zze((int) r15)
            int r10 = com.google.android.gms.internal.firebase_auth.zzhq.zzg((int) r4)
            goto L_0x0832
        L_0x0816:
            java.lang.Object r4 = r2.getObject(r1, r10)
            java.util.List r4 = (java.util.List) r4
            int r4 = com.google.android.gms.internal.firebase_auth.zzkj.zzi(r4)
            if (r4 <= 0) goto L_0x090a
            boolean r10 = r0.zzk
            if (r10 == 0) goto L_0x082a
            long r9 = (long) r9
            r2.putInt(r1, r9, r4)
        L_0x082a:
            int r9 = com.google.android.gms.internal.firebase_auth.zzhq.zze((int) r15)
            int r10 = com.google.android.gms.internal.firebase_auth.zzhq.zzg((int) r4)
        L_0x0832:
            int r9 = r9 + r10
            int r9 = r9 + r4
            goto L_0x0953
        L_0x0836:
            java.lang.Object r4 = r2.getObject(r1, r10)
            java.util.List r4 = (java.util.List) r4
            r9 = 0
            int r4 = com.google.android.gms.internal.firebase_auth.zzkj.zzc(r15, r4, r9)
            goto L_0x0909
        L_0x0843:
            r9 = 0
            java.lang.Object r4 = r2.getObject(r1, r10)
            java.util.List r4 = (java.util.List) r4
            int r4 = com.google.android.gms.internal.firebase_auth.zzkj.zzg(r15, r4, r9)
            goto L_0x0909
        L_0x0850:
            r9 = 0
            java.lang.Object r4 = r2.getObject(r1, r10)
            java.util.List r4 = (java.util.List) r4
            int r4 = com.google.android.gms.internal.firebase_auth.zzkj.zzi(r15, r4, r9)
            goto L_0x0909
        L_0x085d:
            r9 = 0
            java.lang.Object r4 = r2.getObject(r1, r10)
            java.util.List r4 = (java.util.List) r4
            int r4 = com.google.android.gms.internal.firebase_auth.zzkj.zzh(r15, r4, r9)
            goto L_0x0909
        L_0x086a:
            r9 = 0
            java.lang.Object r4 = r2.getObject(r1, r10)
            java.util.List r4 = (java.util.List) r4
            int r4 = com.google.android.gms.internal.firebase_auth.zzkj.zzd(r15, r4, r9)
            goto L_0x0909
        L_0x0877:
            r9 = 0
            java.lang.Object r4 = r2.getObject(r1, r10)
            java.util.List r4 = (java.util.List) r4
            int r4 = com.google.android.gms.internal.firebase_auth.zzkj.zzf(r15, r4, r9)
            goto L_0x0909
        L_0x0884:
            java.lang.Object r4 = r2.getObject(r1, r10)
            java.util.List r4 = (java.util.List) r4
            int r4 = com.google.android.gms.internal.firebase_auth.zzkj.zzb(r15, r4)
            goto L_0x0909
        L_0x0890:
            java.lang.Object r4 = r2.getObject(r1, r10)
            java.util.List r4 = (java.util.List) r4
            com.google.android.gms.internal.firebase_auth.zzkh r9 = r0.zza((int) r3)
            int r4 = com.google.android.gms.internal.firebase_auth.zzkj.zza((int) r15, (java.util.List<?>) r4, (com.google.android.gms.internal.firebase_auth.zzkh) r9)
            goto L_0x0909
        L_0x089f:
            java.lang.Object r4 = r2.getObject(r1, r10)
            java.util.List r4 = (java.util.List) r4
            int r4 = com.google.android.gms.internal.firebase_auth.zzkj.zza((int) r15, (java.util.List<?>) r4)
            goto L_0x0909
        L_0x08aa:
            java.lang.Object r4 = r2.getObject(r1, r10)
            java.util.List r4 = (java.util.List) r4
            r9 = 0
            int r4 = com.google.android.gms.internal.firebase_auth.zzkj.zzj(r15, r4, r9)
            goto L_0x0909
        L_0x08b6:
            r9 = 0
            java.lang.Object r4 = r2.getObject(r1, r10)
            java.util.List r4 = (java.util.List) r4
            int r4 = com.google.android.gms.internal.firebase_auth.zzkj.zzh(r15, r4, r9)
            goto L_0x0909
        L_0x08c2:
            r9 = 0
            java.lang.Object r4 = r2.getObject(r1, r10)
            java.util.List r4 = (java.util.List) r4
            int r4 = com.google.android.gms.internal.firebase_auth.zzkj.zzi(r15, r4, r9)
            goto L_0x0909
        L_0x08ce:
            r9 = 0
            java.lang.Object r4 = r2.getObject(r1, r10)
            java.util.List r4 = (java.util.List) r4
            int r4 = com.google.android.gms.internal.firebase_auth.zzkj.zze(r15, r4, r9)
            goto L_0x0909
        L_0x08da:
            r9 = 0
            java.lang.Object r4 = r2.getObject(r1, r10)
            java.util.List r4 = (java.util.List) r4
            int r4 = com.google.android.gms.internal.firebase_auth.zzkj.zzb((int) r15, (java.util.List<java.lang.Long>) r4, (boolean) r9)
            goto L_0x0909
        L_0x08e6:
            r9 = 0
            java.lang.Object r4 = r2.getObject(r1, r10)
            java.util.List r4 = (java.util.List) r4
            int r4 = com.google.android.gms.internal.firebase_auth.zzkj.zza((int) r15, (java.util.List<java.lang.Long>) r4, (boolean) r9)
            goto L_0x0909
        L_0x08f2:
            r9 = 0
            java.lang.Object r4 = r2.getObject(r1, r10)
            java.util.List r4 = (java.util.List) r4
            int r4 = com.google.android.gms.internal.firebase_auth.zzkj.zzh(r15, r4, r9)
            goto L_0x0909
        L_0x08fe:
            r9 = 0
            java.lang.Object r4 = r2.getObject(r1, r10)
            java.util.List r4 = (java.util.List) r4
            int r4 = com.google.android.gms.internal.firebase_auth.zzkj.zzi(r15, r4, r9)
        L_0x0909:
            int r5 = r5 + r4
        L_0x090a:
            r4 = 0
        L_0x090b:
            r9 = 0
            r10 = 0
            r13 = 0
            goto L_0x0a1f
        L_0x0912:
            r4 = r12 & r18
            if (r4 == 0) goto L_0x090a
            java.lang.Object r4 = r2.getObject(r1, r10)
            com.google.android.gms.internal.firebase_auth.zzjr r4 = (com.google.android.gms.internal.firebase_auth.zzjr) r4
            com.google.android.gms.internal.firebase_auth.zzkh r9 = r0.zza((int) r3)
            int r4 = com.google.android.gms.internal.firebase_auth.zzhq.zzc(r15, r4, r9)
            goto L_0x0909
        L_0x0925:
            r4 = r12 & r18
            if (r4 == 0) goto L_0x090a
            long r9 = r2.getLong(r1, r10)
            int r4 = com.google.android.gms.internal.firebase_auth.zzhq.zzf((int) r15, (long) r9)
            goto L_0x0909
        L_0x0932:
            r4 = r12 & r18
            if (r4 == 0) goto L_0x090a
            int r4 = r2.getInt(r1, r10)
            int r4 = com.google.android.gms.internal.firebase_auth.zzhq.zzh((int) r15, (int) r4)
            goto L_0x0909
        L_0x093f:
            r4 = r12 & r18
            if (r4 == 0) goto L_0x090a
            r9 = 0
            int r4 = com.google.android.gms.internal.firebase_auth.zzhq.zzh((int) r15, (long) r9)
            goto L_0x0909
        L_0x094a:
            r4 = r12 & r18
            if (r4 == 0) goto L_0x090a
            r4 = 0
            int r9 = com.google.android.gms.internal.firebase_auth.zzhq.zzj(r15, r4)
        L_0x0953:
            int r5 = r5 + r9
            goto L_0x090a
        L_0x0955:
            r4 = r12 & r18
            if (r4 == 0) goto L_0x090a
            int r4 = r2.getInt(r1, r10)
            int r4 = com.google.android.gms.internal.firebase_auth.zzhq.zzk(r15, r4)
            goto L_0x0909
        L_0x0962:
            r4 = r12 & r18
            if (r4 == 0) goto L_0x090a
            int r4 = r2.getInt(r1, r10)
            int r4 = com.google.android.gms.internal.firebase_auth.zzhq.zzg((int) r15, (int) r4)
            goto L_0x0909
        L_0x096f:
            r4 = r12 & r18
            if (r4 == 0) goto L_0x090a
            java.lang.Object r4 = r2.getObject(r1, r10)
            com.google.android.gms.internal.firebase_auth.zzgv r4 = (com.google.android.gms.internal.firebase_auth.zzgv) r4
            int r4 = com.google.android.gms.internal.firebase_auth.zzhq.zzc((int) r15, (com.google.android.gms.internal.firebase_auth.zzgv) r4)
            goto L_0x0909
        L_0x097e:
            r4 = r12 & r18
            if (r4 == 0) goto L_0x090a
            java.lang.Object r4 = r2.getObject(r1, r10)
            com.google.android.gms.internal.firebase_auth.zzkh r9 = r0.zza((int) r3)
            int r4 = com.google.android.gms.internal.firebase_auth.zzkj.zza((int) r15, (java.lang.Object) r4, (com.google.android.gms.internal.firebase_auth.zzkh) r9)
            goto L_0x0909
        L_0x0990:
            r4 = r12 & r18
            if (r4 == 0) goto L_0x090a
            java.lang.Object r4 = r2.getObject(r1, r10)
            boolean r9 = r4 instanceof com.google.android.gms.internal.firebase_auth.zzgv
            if (r9 == 0) goto L_0x09a4
            com.google.android.gms.internal.firebase_auth.zzgv r4 = (com.google.android.gms.internal.firebase_auth.zzgv) r4
            int r4 = com.google.android.gms.internal.firebase_auth.zzhq.zzc((int) r15, (com.google.android.gms.internal.firebase_auth.zzgv) r4)
            goto L_0x0909
        L_0x09a4:
            java.lang.String r4 = (java.lang.String) r4
            int r4 = com.google.android.gms.internal.firebase_auth.zzhq.zzb((int) r15, (java.lang.String) r4)
            goto L_0x0909
        L_0x09ac:
            r4 = r12 & r18
            if (r4 == 0) goto L_0x090a
            int r4 = com.google.android.gms.internal.firebase_auth.zzhq.zzb((int) r15, (boolean) r7)
            goto L_0x0909
        L_0x09b6:
            r4 = r12 & r18
            if (r4 == 0) goto L_0x090a
            r4 = 0
            int r9 = com.google.android.gms.internal.firebase_auth.zzhq.zzi(r15, r4)
            int r5 = r5 + r9
            goto L_0x090b
        L_0x09c2:
            r4 = 0
            r9 = r12 & r18
            r13 = 0
            if (r9 == 0) goto L_0x09fe
            int r9 = com.google.android.gms.internal.firebase_auth.zzhq.zzg((int) r15, (long) r13)
            goto L_0x09fd
        L_0x09ce:
            r4 = 0
            r13 = 0
            r9 = r12 & r18
            if (r9 == 0) goto L_0x09fe
            int r9 = r2.getInt(r1, r10)
            int r9 = com.google.android.gms.internal.firebase_auth.zzhq.zzf((int) r15, (int) r9)
            goto L_0x09fd
        L_0x09de:
            r4 = 0
            r13 = 0
            r9 = r12 & r18
            if (r9 == 0) goto L_0x09fe
            long r9 = r2.getLong(r1, r10)
            int r9 = com.google.android.gms.internal.firebase_auth.zzhq.zze((int) r15, (long) r9)
            goto L_0x09fd
        L_0x09ee:
            r4 = 0
            r13 = 0
            r9 = r12 & r18
            if (r9 == 0) goto L_0x09fe
            long r9 = r2.getLong(r1, r10)
            int r9 = com.google.android.gms.internal.firebase_auth.zzhq.zzd((int) r15, (long) r9)
        L_0x09fd:
            int r5 = r5 + r9
        L_0x09fe:
            r9 = 0
            goto L_0x0a0d
        L_0x0a00:
            r4 = 0
            r13 = 0
            r9 = r12 & r18
            if (r9 == 0) goto L_0x09fe
            r9 = 0
            int r10 = com.google.android.gms.internal.firebase_auth.zzhq.zzb((int) r15, (float) r9)
            int r5 = r5 + r10
        L_0x0a0d:
            r10 = 0
            goto L_0x0a1f
        L_0x0a10:
            r4 = 0
            r9 = 0
            r13 = 0
            r10 = r12 & r18
            if (r10 == 0) goto L_0x0a0d
            r10 = 0
            int r15 = com.google.android.gms.internal.firebase_auth.zzhq.zzb((int) r15, (double) r10)
            int r5 = r5 + r15
        L_0x0a1f:
            int r3 = r3 + 3
            r9 = r13
            r4 = 0
            r11 = 0
            goto L_0x04fa
        L_0x0a26:
            r4 = 0
            com.google.android.gms.internal.firebase_auth.zzkz<?, ?> r2 = r0.zzq
            int r2 = zza(r2, r1)
            int r5 = r5 + r2
            boolean r2 = r0.zzh
            if (r2 == 0) goto L_0x0a80
            com.google.android.gms.internal.firebase_auth.zzhv<?> r2 = r0.zzr
            com.google.android.gms.internal.firebase_auth.zzhz r1 = r2.zza((java.lang.Object) r1)
            r11 = 0
        L_0x0a39:
            com.google.android.gms.internal.firebase_auth.zzki<T, java.lang.Object> r2 = r1.zza
            int r2 = r2.zzc()
            if (r11 >= r2) goto L_0x0a59
            com.google.android.gms.internal.firebase_auth.zzki<T, java.lang.Object> r2 = r1.zza
            java.util.Map$Entry r2 = r2.zzb((int) r11)
            java.lang.Object r3 = r2.getKey()
            com.google.android.gms.internal.firebase_auth.zzib r3 = (com.google.android.gms.internal.firebase_auth.zzib) r3
            java.lang.Object r2 = r2.getValue()
            int r2 = com.google.android.gms.internal.firebase_auth.zzhz.zza((com.google.android.gms.internal.firebase_auth.zzib<?>) r3, (java.lang.Object) r2)
            int r4 = r4 + r2
            int r11 = r11 + 1
            goto L_0x0a39
        L_0x0a59:
            com.google.android.gms.internal.firebase_auth.zzki<T, java.lang.Object> r1 = r1.zza
            java.lang.Iterable r1 = r1.zzd()
            java.util.Iterator r1 = r1.iterator()
        L_0x0a63:
            boolean r2 = r1.hasNext()
            if (r2 == 0) goto L_0x0a7f
            java.lang.Object r2 = r1.next()
            java.util.Map$Entry r2 = (java.util.Map.Entry) r2
            java.lang.Object r3 = r2.getKey()
            com.google.android.gms.internal.firebase_auth.zzib r3 = (com.google.android.gms.internal.firebase_auth.zzib) r3
            java.lang.Object r2 = r2.getValue()
            int r2 = com.google.android.gms.internal.firebase_auth.zzhz.zza((com.google.android.gms.internal.firebase_auth.zzib<?>) r3, (java.lang.Object) r2)
            int r4 = r4 + r2
            goto L_0x0a63
        L_0x0a7f:
            int r5 = r5 + r4
        L_0x0a80:
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.firebase_auth.zzjv.zzd(java.lang.Object):int");
    }

    private static <UT, UB> int zza(zzkz<UT, UB> zzkz, T t) {
        return zzkz.zzf(zzkz.zzb(t));
    }

    private static List<?> zza(Object obj, long j) {
        return (List) zzlf.zzf(obj, j);
    }

    /* JADX WARNING: Removed duplicated region for block: B:10:0x003b  */
    /* JADX WARNING: Removed duplicated region for block: B:163:0x0513  */
    /* JADX WARNING: Removed duplicated region for block: B:178:0x0552  */
    /* JADX WARNING: Removed duplicated region for block: B:331:0x0a2a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void zza(T r14, com.google.android.gms.internal.firebase_auth.zzlw r15) throws java.io.IOException {
        /*
            r13 = this;
            int r0 = r15.zza()
            int r1 = com.google.android.gms.internal.firebase_auth.zzig.zze.zzk
            r2 = 267386880(0xff00000, float:2.3665827E-29)
            r3 = 0
            r4 = 1
            r5 = 0
            r6 = 1048575(0xfffff, float:1.469367E-39)
            if (r0 != r1) goto L_0x0529
            com.google.android.gms.internal.firebase_auth.zzkz<?, ?> r0 = r13.zzq
            zza(r0, r14, (com.google.android.gms.internal.firebase_auth.zzlw) r15)
            boolean r0 = r13.zzh
            if (r0 == 0) goto L_0x0032
            com.google.android.gms.internal.firebase_auth.zzhv<?> r0 = r13.zzr
            com.google.android.gms.internal.firebase_auth.zzhz r0 = r0.zza((java.lang.Object) r14)
            com.google.android.gms.internal.firebase_auth.zzki<T, java.lang.Object> r1 = r0.zza
            boolean r1 = r1.isEmpty()
            if (r1 != 0) goto L_0x0032
            java.util.Iterator r0 = r0.zze()
            java.lang.Object r1 = r0.next()
            java.util.Map$Entry r1 = (java.util.Map.Entry) r1
            goto L_0x0034
        L_0x0032:
            r0 = r3
            r1 = r0
        L_0x0034:
            int[] r7 = r13.zzc
            int r7 = r7.length
            int r7 = r7 + -3
        L_0x0039:
            if (r7 < 0) goto L_0x0511
            int r8 = r13.zzd((int) r7)
            int[] r9 = r13.zzc
            r9 = r9[r7]
        L_0x0043:
            if (r1 == 0) goto L_0x0061
            com.google.android.gms.internal.firebase_auth.zzhv<?> r10 = r13.zzr
            int r10 = r10.zza((java.util.Map.Entry<?, ?>) r1)
            if (r10 <= r9) goto L_0x0061
            com.google.android.gms.internal.firebase_auth.zzhv<?> r10 = r13.zzr
            r10.zza(r15, r1)
            boolean r1 = r0.hasNext()
            if (r1 == 0) goto L_0x005f
            java.lang.Object r1 = r0.next()
            java.util.Map$Entry r1 = (java.util.Map.Entry) r1
            goto L_0x0043
        L_0x005f:
            r1 = r3
            goto L_0x0043
        L_0x0061:
            r10 = r8 & r2
            int r10 = r10 >>> 20
            switch(r10) {
                case 0: goto L_0x04fe;
                case 1: goto L_0x04ee;
                case 2: goto L_0x04de;
                case 3: goto L_0x04ce;
                case 4: goto L_0x04be;
                case 5: goto L_0x04ae;
                case 6: goto L_0x049e;
                case 7: goto L_0x048d;
                case 8: goto L_0x047c;
                case 9: goto L_0x0467;
                case 10: goto L_0x0454;
                case 11: goto L_0x0443;
                case 12: goto L_0x0432;
                case 13: goto L_0x0421;
                case 14: goto L_0x0410;
                case 15: goto L_0x03ff;
                case 16: goto L_0x03ee;
                case 17: goto L_0x03d9;
                case 18: goto L_0x03c8;
                case 19: goto L_0x03b7;
                case 20: goto L_0x03a6;
                case 21: goto L_0x0395;
                case 22: goto L_0x0384;
                case 23: goto L_0x0373;
                case 24: goto L_0x0362;
                case 25: goto L_0x0351;
                case 26: goto L_0x0340;
                case 27: goto L_0x032b;
                case 28: goto L_0x031a;
                case 29: goto L_0x0309;
                case 30: goto L_0x02f8;
                case 31: goto L_0x02e7;
                case 32: goto L_0x02d6;
                case 33: goto L_0x02c5;
                case 34: goto L_0x02b4;
                case 35: goto L_0x02a3;
                case 36: goto L_0x0292;
                case 37: goto L_0x0281;
                case 38: goto L_0x0270;
                case 39: goto L_0x025f;
                case 40: goto L_0x024e;
                case 41: goto L_0x023d;
                case 42: goto L_0x022c;
                case 43: goto L_0x021b;
                case 44: goto L_0x020a;
                case 45: goto L_0x01f9;
                case 46: goto L_0x01e8;
                case 47: goto L_0x01d7;
                case 48: goto L_0x01c6;
                case 49: goto L_0x01b1;
                case 50: goto L_0x01a6;
                case 51: goto L_0x0195;
                case 52: goto L_0x0184;
                case 53: goto L_0x0173;
                case 54: goto L_0x0162;
                case 55: goto L_0x0151;
                case 56: goto L_0x0140;
                case 57: goto L_0x012f;
                case 58: goto L_0x011e;
                case 59: goto L_0x010d;
                case 60: goto L_0x00f8;
                case 61: goto L_0x00e5;
                case 62: goto L_0x00d4;
                case 63: goto L_0x00c3;
                case 64: goto L_0x00b2;
                case 65: goto L_0x00a1;
                case 66: goto L_0x0090;
                case 67: goto L_0x007f;
                case 68: goto L_0x006a;
                default: goto L_0x0068;
            }
        L_0x0068:
            goto L_0x050d
        L_0x006a:
            boolean r10 = r13.zza(r14, (int) r9, (int) r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.firebase_auth.zzlf.zzf(r14, r10)
            com.google.android.gms.internal.firebase_auth.zzkh r10 = r13.zza((int) r7)
            r15.zzb((int) r9, (java.lang.Object) r8, (com.google.android.gms.internal.firebase_auth.zzkh) r10)
            goto L_0x050d
        L_0x007f:
            boolean r10 = r13.zza(r14, (int) r9, (int) r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            long r10 = zze(r14, r10)
            r15.zze((int) r9, (long) r10)
            goto L_0x050d
        L_0x0090:
            boolean r10 = r13.zza(r14, (int) r9, (int) r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            int r8 = zzd(r14, r10)
            r15.zzf(r9, r8)
            goto L_0x050d
        L_0x00a1:
            boolean r10 = r13.zza(r14, (int) r9, (int) r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            long r10 = zze(r14, r10)
            r15.zzb((int) r9, (long) r10)
            goto L_0x050d
        L_0x00b2:
            boolean r10 = r13.zza(r14, (int) r9, (int) r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            int r8 = zzd(r14, r10)
            r15.zza((int) r9, (int) r8)
            goto L_0x050d
        L_0x00c3:
            boolean r10 = r13.zza(r14, (int) r9, (int) r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            int r8 = zzd(r14, r10)
            r15.zzb((int) r9, (int) r8)
            goto L_0x050d
        L_0x00d4:
            boolean r10 = r13.zza(r14, (int) r9, (int) r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            int r8 = zzd(r14, r10)
            r15.zze((int) r9, (int) r8)
            goto L_0x050d
        L_0x00e5:
            boolean r10 = r13.zza(r14, (int) r9, (int) r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.firebase_auth.zzlf.zzf(r14, r10)
            com.google.android.gms.internal.firebase_auth.zzgv r8 = (com.google.android.gms.internal.firebase_auth.zzgv) r8
            r15.zza((int) r9, (com.google.android.gms.internal.firebase_auth.zzgv) r8)
            goto L_0x050d
        L_0x00f8:
            boolean r10 = r13.zza(r14, (int) r9, (int) r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.firebase_auth.zzlf.zzf(r14, r10)
            com.google.android.gms.internal.firebase_auth.zzkh r10 = r13.zza((int) r7)
            r15.zza((int) r9, (java.lang.Object) r8, (com.google.android.gms.internal.firebase_auth.zzkh) r10)
            goto L_0x050d
        L_0x010d:
            boolean r10 = r13.zza(r14, (int) r9, (int) r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.firebase_auth.zzlf.zzf(r14, r10)
            zza((int) r9, (java.lang.Object) r8, (com.google.android.gms.internal.firebase_auth.zzlw) r15)
            goto L_0x050d
        L_0x011e:
            boolean r10 = r13.zza(r14, (int) r9, (int) r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            boolean r8 = zzf(r14, r10)
            r15.zza((int) r9, (boolean) r8)
            goto L_0x050d
        L_0x012f:
            boolean r10 = r13.zza(r14, (int) r9, (int) r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            int r8 = zzd(r14, r10)
            r15.zzd((int) r9, (int) r8)
            goto L_0x050d
        L_0x0140:
            boolean r10 = r13.zza(r14, (int) r9, (int) r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            long r10 = zze(r14, r10)
            r15.zzd((int) r9, (long) r10)
            goto L_0x050d
        L_0x0151:
            boolean r10 = r13.zza(r14, (int) r9, (int) r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            int r8 = zzd(r14, r10)
            r15.zzc((int) r9, (int) r8)
            goto L_0x050d
        L_0x0162:
            boolean r10 = r13.zza(r14, (int) r9, (int) r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            long r10 = zze(r14, r10)
            r15.zzc((int) r9, (long) r10)
            goto L_0x050d
        L_0x0173:
            boolean r10 = r13.zza(r14, (int) r9, (int) r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            long r10 = zze(r14, r10)
            r15.zza((int) r9, (long) r10)
            goto L_0x050d
        L_0x0184:
            boolean r10 = r13.zza(r14, (int) r9, (int) r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            float r8 = zzc(r14, r10)
            r15.zza((int) r9, (float) r8)
            goto L_0x050d
        L_0x0195:
            boolean r10 = r13.zza(r14, (int) r9, (int) r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            double r10 = zzb(r14, (long) r10)
            r15.zza((int) r9, (double) r10)
            goto L_0x050d
        L_0x01a6:
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.firebase_auth.zzlf.zzf(r14, r10)
            r13.zza((com.google.android.gms.internal.firebase_auth.zzlw) r15, (int) r9, (java.lang.Object) r8, (int) r7)
            goto L_0x050d
        L_0x01b1:
            int[] r9 = r13.zzc
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.firebase_auth.zzlf.zzf(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.firebase_auth.zzkh r10 = r13.zza((int) r7)
            com.google.android.gms.internal.firebase_auth.zzkj.zzb((int) r9, (java.util.List<?>) r8, (com.google.android.gms.internal.firebase_auth.zzlw) r15, (com.google.android.gms.internal.firebase_auth.zzkh) r10)
            goto L_0x050d
        L_0x01c6:
            int[] r9 = r13.zzc
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.firebase_auth.zzlf.zzf(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.firebase_auth.zzkj.zze(r9, r8, r15, r4)
            goto L_0x050d
        L_0x01d7:
            int[] r9 = r13.zzc
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.firebase_auth.zzlf.zzf(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.firebase_auth.zzkj.zzj(r9, r8, r15, r4)
            goto L_0x050d
        L_0x01e8:
            int[] r9 = r13.zzc
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.firebase_auth.zzlf.zzf(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.firebase_auth.zzkj.zzg(r9, r8, r15, r4)
            goto L_0x050d
        L_0x01f9:
            int[] r9 = r13.zzc
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.firebase_auth.zzlf.zzf(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.firebase_auth.zzkj.zzl(r9, r8, r15, r4)
            goto L_0x050d
        L_0x020a:
            int[] r9 = r13.zzc
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.firebase_auth.zzlf.zzf(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.firebase_auth.zzkj.zzm(r9, r8, r15, r4)
            goto L_0x050d
        L_0x021b:
            int[] r9 = r13.zzc
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.firebase_auth.zzlf.zzf(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.firebase_auth.zzkj.zzi(r9, r8, r15, r4)
            goto L_0x050d
        L_0x022c:
            int[] r9 = r13.zzc
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.firebase_auth.zzlf.zzf(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.firebase_auth.zzkj.zzn(r9, r8, r15, r4)
            goto L_0x050d
        L_0x023d:
            int[] r9 = r13.zzc
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.firebase_auth.zzlf.zzf(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.firebase_auth.zzkj.zzk(r9, r8, r15, r4)
            goto L_0x050d
        L_0x024e:
            int[] r9 = r13.zzc
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.firebase_auth.zzlf.zzf(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.firebase_auth.zzkj.zzf(r9, r8, r15, r4)
            goto L_0x050d
        L_0x025f:
            int[] r9 = r13.zzc
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.firebase_auth.zzlf.zzf(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.firebase_auth.zzkj.zzh(r9, r8, r15, r4)
            goto L_0x050d
        L_0x0270:
            int[] r9 = r13.zzc
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.firebase_auth.zzlf.zzf(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.firebase_auth.zzkj.zzd(r9, r8, r15, r4)
            goto L_0x050d
        L_0x0281:
            int[] r9 = r13.zzc
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.firebase_auth.zzlf.zzf(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.firebase_auth.zzkj.zzc(r9, r8, r15, r4)
            goto L_0x050d
        L_0x0292:
            int[] r9 = r13.zzc
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.firebase_auth.zzlf.zzf(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.firebase_auth.zzkj.zzb((int) r9, (java.util.List<java.lang.Float>) r8, (com.google.android.gms.internal.firebase_auth.zzlw) r15, (boolean) r4)
            goto L_0x050d
        L_0x02a3:
            int[] r9 = r13.zzc
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.firebase_auth.zzlf.zzf(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.firebase_auth.zzkj.zza((int) r9, (java.util.List<java.lang.Double>) r8, (com.google.android.gms.internal.firebase_auth.zzlw) r15, (boolean) r4)
            goto L_0x050d
        L_0x02b4:
            int[] r9 = r13.zzc
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.firebase_auth.zzlf.zzf(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.firebase_auth.zzkj.zze(r9, r8, r15, r5)
            goto L_0x050d
        L_0x02c5:
            int[] r9 = r13.zzc
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.firebase_auth.zzlf.zzf(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.firebase_auth.zzkj.zzj(r9, r8, r15, r5)
            goto L_0x050d
        L_0x02d6:
            int[] r9 = r13.zzc
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.firebase_auth.zzlf.zzf(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.firebase_auth.zzkj.zzg(r9, r8, r15, r5)
            goto L_0x050d
        L_0x02e7:
            int[] r9 = r13.zzc
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.firebase_auth.zzlf.zzf(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.firebase_auth.zzkj.zzl(r9, r8, r15, r5)
            goto L_0x050d
        L_0x02f8:
            int[] r9 = r13.zzc
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.firebase_auth.zzlf.zzf(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.firebase_auth.zzkj.zzm(r9, r8, r15, r5)
            goto L_0x050d
        L_0x0309:
            int[] r9 = r13.zzc
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.firebase_auth.zzlf.zzf(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.firebase_auth.zzkj.zzi(r9, r8, r15, r5)
            goto L_0x050d
        L_0x031a:
            int[] r9 = r13.zzc
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.firebase_auth.zzlf.zzf(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.firebase_auth.zzkj.zzb((int) r9, (java.util.List<com.google.android.gms.internal.firebase_auth.zzgv>) r8, (com.google.android.gms.internal.firebase_auth.zzlw) r15)
            goto L_0x050d
        L_0x032b:
            int[] r9 = r13.zzc
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.firebase_auth.zzlf.zzf(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.firebase_auth.zzkh r10 = r13.zza((int) r7)
            com.google.android.gms.internal.firebase_auth.zzkj.zza((int) r9, (java.util.List<?>) r8, (com.google.android.gms.internal.firebase_auth.zzlw) r15, (com.google.android.gms.internal.firebase_auth.zzkh) r10)
            goto L_0x050d
        L_0x0340:
            int[] r9 = r13.zzc
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.firebase_auth.zzlf.zzf(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.firebase_auth.zzkj.zza((int) r9, (java.util.List<java.lang.String>) r8, (com.google.android.gms.internal.firebase_auth.zzlw) r15)
            goto L_0x050d
        L_0x0351:
            int[] r9 = r13.zzc
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.firebase_auth.zzlf.zzf(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.firebase_auth.zzkj.zzn(r9, r8, r15, r5)
            goto L_0x050d
        L_0x0362:
            int[] r9 = r13.zzc
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.firebase_auth.zzlf.zzf(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.firebase_auth.zzkj.zzk(r9, r8, r15, r5)
            goto L_0x050d
        L_0x0373:
            int[] r9 = r13.zzc
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.firebase_auth.zzlf.zzf(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.firebase_auth.zzkj.zzf(r9, r8, r15, r5)
            goto L_0x050d
        L_0x0384:
            int[] r9 = r13.zzc
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.firebase_auth.zzlf.zzf(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.firebase_auth.zzkj.zzh(r9, r8, r15, r5)
            goto L_0x050d
        L_0x0395:
            int[] r9 = r13.zzc
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.firebase_auth.zzlf.zzf(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.firebase_auth.zzkj.zzd(r9, r8, r15, r5)
            goto L_0x050d
        L_0x03a6:
            int[] r9 = r13.zzc
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.firebase_auth.zzlf.zzf(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.firebase_auth.zzkj.zzc(r9, r8, r15, r5)
            goto L_0x050d
        L_0x03b7:
            int[] r9 = r13.zzc
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.firebase_auth.zzlf.zzf(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.firebase_auth.zzkj.zzb((int) r9, (java.util.List<java.lang.Float>) r8, (com.google.android.gms.internal.firebase_auth.zzlw) r15, (boolean) r5)
            goto L_0x050d
        L_0x03c8:
            int[] r9 = r13.zzc
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.firebase_auth.zzlf.zzf(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.firebase_auth.zzkj.zza((int) r9, (java.util.List<java.lang.Double>) r8, (com.google.android.gms.internal.firebase_auth.zzlw) r15, (boolean) r5)
            goto L_0x050d
        L_0x03d9:
            boolean r10 = r13.zza(r14, (int) r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.firebase_auth.zzlf.zzf(r14, r10)
            com.google.android.gms.internal.firebase_auth.zzkh r10 = r13.zza((int) r7)
            r15.zzb((int) r9, (java.lang.Object) r8, (com.google.android.gms.internal.firebase_auth.zzkh) r10)
            goto L_0x050d
        L_0x03ee:
            boolean r10 = r13.zza(r14, (int) r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            long r10 = com.google.android.gms.internal.firebase_auth.zzlf.zzb(r14, r10)
            r15.zze((int) r9, (long) r10)
            goto L_0x050d
        L_0x03ff:
            boolean r10 = r13.zza(r14, (int) r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            int r8 = com.google.android.gms.internal.firebase_auth.zzlf.zza((java.lang.Object) r14, (long) r10)
            r15.zzf(r9, r8)
            goto L_0x050d
        L_0x0410:
            boolean r10 = r13.zza(r14, (int) r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            long r10 = com.google.android.gms.internal.firebase_auth.zzlf.zzb(r14, r10)
            r15.zzb((int) r9, (long) r10)
            goto L_0x050d
        L_0x0421:
            boolean r10 = r13.zza(r14, (int) r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            int r8 = com.google.android.gms.internal.firebase_auth.zzlf.zza((java.lang.Object) r14, (long) r10)
            r15.zza((int) r9, (int) r8)
            goto L_0x050d
        L_0x0432:
            boolean r10 = r13.zza(r14, (int) r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            int r8 = com.google.android.gms.internal.firebase_auth.zzlf.zza((java.lang.Object) r14, (long) r10)
            r15.zzb((int) r9, (int) r8)
            goto L_0x050d
        L_0x0443:
            boolean r10 = r13.zza(r14, (int) r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            int r8 = com.google.android.gms.internal.firebase_auth.zzlf.zza((java.lang.Object) r14, (long) r10)
            r15.zze((int) r9, (int) r8)
            goto L_0x050d
        L_0x0454:
            boolean r10 = r13.zza(r14, (int) r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.firebase_auth.zzlf.zzf(r14, r10)
            com.google.android.gms.internal.firebase_auth.zzgv r8 = (com.google.android.gms.internal.firebase_auth.zzgv) r8
            r15.zza((int) r9, (com.google.android.gms.internal.firebase_auth.zzgv) r8)
            goto L_0x050d
        L_0x0467:
            boolean r10 = r13.zza(r14, (int) r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.firebase_auth.zzlf.zzf(r14, r10)
            com.google.android.gms.internal.firebase_auth.zzkh r10 = r13.zza((int) r7)
            r15.zza((int) r9, (java.lang.Object) r8, (com.google.android.gms.internal.firebase_auth.zzkh) r10)
            goto L_0x050d
        L_0x047c:
            boolean r10 = r13.zza(r14, (int) r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.firebase_auth.zzlf.zzf(r14, r10)
            zza((int) r9, (java.lang.Object) r8, (com.google.android.gms.internal.firebase_auth.zzlw) r15)
            goto L_0x050d
        L_0x048d:
            boolean r10 = r13.zza(r14, (int) r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            boolean r8 = com.google.android.gms.internal.firebase_auth.zzlf.zzc(r14, r10)
            r15.zza((int) r9, (boolean) r8)
            goto L_0x050d
        L_0x049e:
            boolean r10 = r13.zza(r14, (int) r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            int r8 = com.google.android.gms.internal.firebase_auth.zzlf.zza((java.lang.Object) r14, (long) r10)
            r15.zzd((int) r9, (int) r8)
            goto L_0x050d
        L_0x04ae:
            boolean r10 = r13.zza(r14, (int) r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            long r10 = com.google.android.gms.internal.firebase_auth.zzlf.zzb(r14, r10)
            r15.zzd((int) r9, (long) r10)
            goto L_0x050d
        L_0x04be:
            boolean r10 = r13.zza(r14, (int) r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            int r8 = com.google.android.gms.internal.firebase_auth.zzlf.zza((java.lang.Object) r14, (long) r10)
            r15.zzc((int) r9, (int) r8)
            goto L_0x050d
        L_0x04ce:
            boolean r10 = r13.zza(r14, (int) r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            long r10 = com.google.android.gms.internal.firebase_auth.zzlf.zzb(r14, r10)
            r15.zzc((int) r9, (long) r10)
            goto L_0x050d
        L_0x04de:
            boolean r10 = r13.zza(r14, (int) r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            long r10 = com.google.android.gms.internal.firebase_auth.zzlf.zzb(r14, r10)
            r15.zza((int) r9, (long) r10)
            goto L_0x050d
        L_0x04ee:
            boolean r10 = r13.zza(r14, (int) r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            float r8 = com.google.android.gms.internal.firebase_auth.zzlf.zzd(r14, r10)
            r15.zza((int) r9, (float) r8)
            goto L_0x050d
        L_0x04fe:
            boolean r10 = r13.zza(r14, (int) r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            double r10 = com.google.android.gms.internal.firebase_auth.zzlf.zze(r14, r10)
            r15.zza((int) r9, (double) r10)
        L_0x050d:
            int r7 = r7 + -3
            goto L_0x0039
        L_0x0511:
            if (r1 == 0) goto L_0x0528
            com.google.android.gms.internal.firebase_auth.zzhv<?> r14 = r13.zzr
            r14.zza(r15, r1)
            boolean r14 = r0.hasNext()
            if (r14 == 0) goto L_0x0526
            java.lang.Object r14 = r0.next()
            java.util.Map$Entry r14 = (java.util.Map.Entry) r14
            r1 = r14
            goto L_0x0511
        L_0x0526:
            r1 = r3
            goto L_0x0511
        L_0x0528:
            return
        L_0x0529:
            boolean r0 = r13.zzj
            if (r0 == 0) goto L_0x0a44
            boolean r0 = r13.zzh
            if (r0 == 0) goto L_0x054a
            com.google.android.gms.internal.firebase_auth.zzhv<?> r0 = r13.zzr
            com.google.android.gms.internal.firebase_auth.zzhz r0 = r0.zza((java.lang.Object) r14)
            com.google.android.gms.internal.firebase_auth.zzki<T, java.lang.Object> r1 = r0.zza
            boolean r1 = r1.isEmpty()
            if (r1 != 0) goto L_0x054a
            java.util.Iterator r0 = r0.zzd()
            java.lang.Object r1 = r0.next()
            java.util.Map$Entry r1 = (java.util.Map.Entry) r1
            goto L_0x054c
        L_0x054a:
            r0 = r3
            r1 = r0
        L_0x054c:
            int[] r7 = r13.zzc
            int r7 = r7.length
            r8 = 0
        L_0x0550:
            if (r8 >= r7) goto L_0x0a28
            int r9 = r13.zzd((int) r8)
            int[] r10 = r13.zzc
            r10 = r10[r8]
        L_0x055a:
            if (r1 == 0) goto L_0x0578
            com.google.android.gms.internal.firebase_auth.zzhv<?> r11 = r13.zzr
            int r11 = r11.zza((java.util.Map.Entry<?, ?>) r1)
            if (r11 > r10) goto L_0x0578
            com.google.android.gms.internal.firebase_auth.zzhv<?> r11 = r13.zzr
            r11.zza(r15, r1)
            boolean r1 = r0.hasNext()
            if (r1 == 0) goto L_0x0576
            java.lang.Object r1 = r0.next()
            java.util.Map$Entry r1 = (java.util.Map.Entry) r1
            goto L_0x055a
        L_0x0576:
            r1 = r3
            goto L_0x055a
        L_0x0578:
            r11 = r9 & r2
            int r11 = r11 >>> 20
            switch(r11) {
                case 0: goto L_0x0a15;
                case 1: goto L_0x0a05;
                case 2: goto L_0x09f5;
                case 3: goto L_0x09e5;
                case 4: goto L_0x09d5;
                case 5: goto L_0x09c5;
                case 6: goto L_0x09b5;
                case 7: goto L_0x09a4;
                case 8: goto L_0x0993;
                case 9: goto L_0x097e;
                case 10: goto L_0x096b;
                case 11: goto L_0x095a;
                case 12: goto L_0x0949;
                case 13: goto L_0x0938;
                case 14: goto L_0x0927;
                case 15: goto L_0x0916;
                case 16: goto L_0x0905;
                case 17: goto L_0x08f0;
                case 18: goto L_0x08df;
                case 19: goto L_0x08ce;
                case 20: goto L_0x08bd;
                case 21: goto L_0x08ac;
                case 22: goto L_0x089b;
                case 23: goto L_0x088a;
                case 24: goto L_0x0879;
                case 25: goto L_0x0868;
                case 26: goto L_0x0857;
                case 27: goto L_0x0842;
                case 28: goto L_0x0831;
                case 29: goto L_0x0820;
                case 30: goto L_0x080f;
                case 31: goto L_0x07fe;
                case 32: goto L_0x07ed;
                case 33: goto L_0x07dc;
                case 34: goto L_0x07cb;
                case 35: goto L_0x07ba;
                case 36: goto L_0x07a9;
                case 37: goto L_0x0798;
                case 38: goto L_0x0787;
                case 39: goto L_0x0776;
                case 40: goto L_0x0765;
                case 41: goto L_0x0754;
                case 42: goto L_0x0743;
                case 43: goto L_0x0732;
                case 44: goto L_0x0721;
                case 45: goto L_0x0710;
                case 46: goto L_0x06ff;
                case 47: goto L_0x06ee;
                case 48: goto L_0x06dd;
                case 49: goto L_0x06c8;
                case 50: goto L_0x06bd;
                case 51: goto L_0x06ac;
                case 52: goto L_0x069b;
                case 53: goto L_0x068a;
                case 54: goto L_0x0679;
                case 55: goto L_0x0668;
                case 56: goto L_0x0657;
                case 57: goto L_0x0646;
                case 58: goto L_0x0635;
                case 59: goto L_0x0624;
                case 60: goto L_0x060f;
                case 61: goto L_0x05fc;
                case 62: goto L_0x05eb;
                case 63: goto L_0x05da;
                case 64: goto L_0x05c9;
                case 65: goto L_0x05b8;
                case 66: goto L_0x05a7;
                case 67: goto L_0x0596;
                case 68: goto L_0x0581;
                default: goto L_0x057f;
            }
        L_0x057f:
            goto L_0x0a24
        L_0x0581:
            boolean r11 = r13.zza(r14, (int) r10, (int) r8)
            if (r11 == 0) goto L_0x0a24
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.firebase_auth.zzlf.zzf(r14, r11)
            com.google.android.gms.internal.firebase_auth.zzkh r11 = r13.zza((int) r8)
            r15.zzb((int) r10, (java.lang.Object) r9, (com.google.android.gms.internal.firebase_auth.zzkh) r11)
            goto L_0x0a24
        L_0x0596:
            boolean r11 = r13.zza(r14, (int) r10, (int) r8)
            if (r11 == 0) goto L_0x0a24
            r9 = r9 & r6
            long r11 = (long) r9
            long r11 = zze(r14, r11)
            r15.zze((int) r10, (long) r11)
            goto L_0x0a24
        L_0x05a7:
            boolean r11 = r13.zza(r14, (int) r10, (int) r8)
            if (r11 == 0) goto L_0x0a24
            r9 = r9 & r6
            long r11 = (long) r9
            int r9 = zzd(r14, r11)
            r15.zzf(r10, r9)
            goto L_0x0a24
        L_0x05b8:
            boolean r11 = r13.zza(r14, (int) r10, (int) r8)
            if (r11 == 0) goto L_0x0a24
            r9 = r9 & r6
            long r11 = (long) r9
            long r11 = zze(r14, r11)
            r15.zzb((int) r10, (long) r11)
            goto L_0x0a24
        L_0x05c9:
            boolean r11 = r13.zza(r14, (int) r10, (int) r8)
            if (r11 == 0) goto L_0x0a24
            r9 = r9 & r6
            long r11 = (long) r9
            int r9 = zzd(r14, r11)
            r15.zza((int) r10, (int) r9)
            goto L_0x0a24
        L_0x05da:
            boolean r11 = r13.zza(r14, (int) r10, (int) r8)
            if (r11 == 0) goto L_0x0a24
            r9 = r9 & r6
            long r11 = (long) r9
            int r9 = zzd(r14, r11)
            r15.zzb((int) r10, (int) r9)
            goto L_0x0a24
        L_0x05eb:
            boolean r11 = r13.zza(r14, (int) r10, (int) r8)
            if (r11 == 0) goto L_0x0a24
            r9 = r9 & r6
            long r11 = (long) r9
            int r9 = zzd(r14, r11)
            r15.zze((int) r10, (int) r9)
            goto L_0x0a24
        L_0x05fc:
            boolean r11 = r13.zza(r14, (int) r10, (int) r8)
            if (r11 == 0) goto L_0x0a24
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.firebase_auth.zzlf.zzf(r14, r11)
            com.google.android.gms.internal.firebase_auth.zzgv r9 = (com.google.android.gms.internal.firebase_auth.zzgv) r9
            r15.zza((int) r10, (com.google.android.gms.internal.firebase_auth.zzgv) r9)
            goto L_0x0a24
        L_0x060f:
            boolean r11 = r13.zza(r14, (int) r10, (int) r8)
            if (r11 == 0) goto L_0x0a24
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.firebase_auth.zzlf.zzf(r14, r11)
            com.google.android.gms.internal.firebase_auth.zzkh r11 = r13.zza((int) r8)
            r15.zza((int) r10, (java.lang.Object) r9, (com.google.android.gms.internal.firebase_auth.zzkh) r11)
            goto L_0x0a24
        L_0x0624:
            boolean r11 = r13.zza(r14, (int) r10, (int) r8)
            if (r11 == 0) goto L_0x0a24
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.firebase_auth.zzlf.zzf(r14, r11)
            zza((int) r10, (java.lang.Object) r9, (com.google.android.gms.internal.firebase_auth.zzlw) r15)
            goto L_0x0a24
        L_0x0635:
            boolean r11 = r13.zza(r14, (int) r10, (int) r8)
            if (r11 == 0) goto L_0x0a24
            r9 = r9 & r6
            long r11 = (long) r9
            boolean r9 = zzf(r14, r11)
            r15.zza((int) r10, (boolean) r9)
            goto L_0x0a24
        L_0x0646:
            boolean r11 = r13.zza(r14, (int) r10, (int) r8)
            if (r11 == 0) goto L_0x0a24
            r9 = r9 & r6
            long r11 = (long) r9
            int r9 = zzd(r14, r11)
            r15.zzd((int) r10, (int) r9)
            goto L_0x0a24
        L_0x0657:
            boolean r11 = r13.zza(r14, (int) r10, (int) r8)
            if (r11 == 0) goto L_0x0a24
            r9 = r9 & r6
            long r11 = (long) r9
            long r11 = zze(r14, r11)
            r15.zzd((int) r10, (long) r11)
            goto L_0x0a24
        L_0x0668:
            boolean r11 = r13.zza(r14, (int) r10, (int) r8)
            if (r11 == 0) goto L_0x0a24
            r9 = r9 & r6
            long r11 = (long) r9
            int r9 = zzd(r14, r11)
            r15.zzc((int) r10, (int) r9)
            goto L_0x0a24
        L_0x0679:
            boolean r11 = r13.zza(r14, (int) r10, (int) r8)
            if (r11 == 0) goto L_0x0a24
            r9 = r9 & r6
            long r11 = (long) r9
            long r11 = zze(r14, r11)
            r15.zzc((int) r10, (long) r11)
            goto L_0x0a24
        L_0x068a:
            boolean r11 = r13.zza(r14, (int) r10, (int) r8)
            if (r11 == 0) goto L_0x0a24
            r9 = r9 & r6
            long r11 = (long) r9
            long r11 = zze(r14, r11)
            r15.zza((int) r10, (long) r11)
            goto L_0x0a24
        L_0x069b:
            boolean r11 = r13.zza(r14, (int) r10, (int) r8)
            if (r11 == 0) goto L_0x0a24
            r9 = r9 & r6
            long r11 = (long) r9
            float r9 = zzc(r14, r11)
            r15.zza((int) r10, (float) r9)
            goto L_0x0a24
        L_0x06ac:
            boolean r11 = r13.zza(r14, (int) r10, (int) r8)
            if (r11 == 0) goto L_0x0a24
            r9 = r9 & r6
            long r11 = (long) r9
            double r11 = zzb(r14, (long) r11)
            r15.zza((int) r10, (double) r11)
            goto L_0x0a24
        L_0x06bd:
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.firebase_auth.zzlf.zzf(r14, r11)
            r13.zza((com.google.android.gms.internal.firebase_auth.zzlw) r15, (int) r10, (java.lang.Object) r9, (int) r8)
            goto L_0x0a24
        L_0x06c8:
            int[] r10 = r13.zzc
            r10 = r10[r8]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.firebase_auth.zzlf.zzf(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.firebase_auth.zzkh r11 = r13.zza((int) r8)
            com.google.android.gms.internal.firebase_auth.zzkj.zzb((int) r10, (java.util.List<?>) r9, (com.google.android.gms.internal.firebase_auth.zzlw) r15, (com.google.android.gms.internal.firebase_auth.zzkh) r11)
            goto L_0x0a24
        L_0x06dd:
            int[] r10 = r13.zzc
            r10 = r10[r8]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.firebase_auth.zzlf.zzf(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.firebase_auth.zzkj.zze(r10, r9, r15, r4)
            goto L_0x0a24
        L_0x06ee:
            int[] r10 = r13.zzc
            r10 = r10[r8]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.firebase_auth.zzlf.zzf(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.firebase_auth.zzkj.zzj(r10, r9, r15, r4)
            goto L_0x0a24
        L_0x06ff:
            int[] r10 = r13.zzc
            r10 = r10[r8]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.firebase_auth.zzlf.zzf(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.firebase_auth.zzkj.zzg(r10, r9, r15, r4)
            goto L_0x0a24
        L_0x0710:
            int[] r10 = r13.zzc
            r10 = r10[r8]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.firebase_auth.zzlf.zzf(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.firebase_auth.zzkj.zzl(r10, r9, r15, r4)
            goto L_0x0a24
        L_0x0721:
            int[] r10 = r13.zzc
            r10 = r10[r8]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.firebase_auth.zzlf.zzf(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.firebase_auth.zzkj.zzm(r10, r9, r15, r4)
            goto L_0x0a24
        L_0x0732:
            int[] r10 = r13.zzc
            r10 = r10[r8]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.firebase_auth.zzlf.zzf(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.firebase_auth.zzkj.zzi(r10, r9, r15, r4)
            goto L_0x0a24
        L_0x0743:
            int[] r10 = r13.zzc
            r10 = r10[r8]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.firebase_auth.zzlf.zzf(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.firebase_auth.zzkj.zzn(r10, r9, r15, r4)
            goto L_0x0a24
        L_0x0754:
            int[] r10 = r13.zzc
            r10 = r10[r8]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.firebase_auth.zzlf.zzf(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.firebase_auth.zzkj.zzk(r10, r9, r15, r4)
            goto L_0x0a24
        L_0x0765:
            int[] r10 = r13.zzc
            r10 = r10[r8]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.firebase_auth.zzlf.zzf(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.firebase_auth.zzkj.zzf(r10, r9, r15, r4)
            goto L_0x0a24
        L_0x0776:
            int[] r10 = r13.zzc
            r10 = r10[r8]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.firebase_auth.zzlf.zzf(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.firebase_auth.zzkj.zzh(r10, r9, r15, r4)
            goto L_0x0a24
        L_0x0787:
            int[] r10 = r13.zzc
            r10 = r10[r8]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.firebase_auth.zzlf.zzf(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.firebase_auth.zzkj.zzd(r10, r9, r15, r4)
            goto L_0x0a24
        L_0x0798:
            int[] r10 = r13.zzc
            r10 = r10[r8]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.firebase_auth.zzlf.zzf(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.firebase_auth.zzkj.zzc(r10, r9, r15, r4)
            goto L_0x0a24
        L_0x07a9:
            int[] r10 = r13.zzc
            r10 = r10[r8]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.firebase_auth.zzlf.zzf(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.firebase_auth.zzkj.zzb((int) r10, (java.util.List<java.lang.Float>) r9, (com.google.android.gms.internal.firebase_auth.zzlw) r15, (boolean) r4)
            goto L_0x0a24
        L_0x07ba:
            int[] r10 = r13.zzc
            r10 = r10[r8]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.firebase_auth.zzlf.zzf(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.firebase_auth.zzkj.zza((int) r10, (java.util.List<java.lang.Double>) r9, (com.google.android.gms.internal.firebase_auth.zzlw) r15, (boolean) r4)
            goto L_0x0a24
        L_0x07cb:
            int[] r10 = r13.zzc
            r10 = r10[r8]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.firebase_auth.zzlf.zzf(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.firebase_auth.zzkj.zze(r10, r9, r15, r5)
            goto L_0x0a24
        L_0x07dc:
            int[] r10 = r13.zzc
            r10 = r10[r8]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.firebase_auth.zzlf.zzf(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.firebase_auth.zzkj.zzj(r10, r9, r15, r5)
            goto L_0x0a24
        L_0x07ed:
            int[] r10 = r13.zzc
            r10 = r10[r8]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.firebase_auth.zzlf.zzf(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.firebase_auth.zzkj.zzg(r10, r9, r15, r5)
            goto L_0x0a24
        L_0x07fe:
            int[] r10 = r13.zzc
            r10 = r10[r8]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.firebase_auth.zzlf.zzf(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.firebase_auth.zzkj.zzl(r10, r9, r15, r5)
            goto L_0x0a24
        L_0x080f:
            int[] r10 = r13.zzc
            r10 = r10[r8]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.firebase_auth.zzlf.zzf(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.firebase_auth.zzkj.zzm(r10, r9, r15, r5)
            goto L_0x0a24
        L_0x0820:
            int[] r10 = r13.zzc
            r10 = r10[r8]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.firebase_auth.zzlf.zzf(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.firebase_auth.zzkj.zzi(r10, r9, r15, r5)
            goto L_0x0a24
        L_0x0831:
            int[] r10 = r13.zzc
            r10 = r10[r8]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.firebase_auth.zzlf.zzf(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.firebase_auth.zzkj.zzb((int) r10, (java.util.List<com.google.android.gms.internal.firebase_auth.zzgv>) r9, (com.google.android.gms.internal.firebase_auth.zzlw) r15)
            goto L_0x0a24
        L_0x0842:
            int[] r10 = r13.zzc
            r10 = r10[r8]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.firebase_auth.zzlf.zzf(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.firebase_auth.zzkh r11 = r13.zza((int) r8)
            com.google.android.gms.internal.firebase_auth.zzkj.zza((int) r10, (java.util.List<?>) r9, (com.google.android.gms.internal.firebase_auth.zzlw) r15, (com.google.android.gms.internal.firebase_auth.zzkh) r11)
            goto L_0x0a24
        L_0x0857:
            int[] r10 = r13.zzc
            r10 = r10[r8]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.firebase_auth.zzlf.zzf(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.firebase_auth.zzkj.zza((int) r10, (java.util.List<java.lang.String>) r9, (com.google.android.gms.internal.firebase_auth.zzlw) r15)
            goto L_0x0a24
        L_0x0868:
            int[] r10 = r13.zzc
            r10 = r10[r8]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.firebase_auth.zzlf.zzf(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.firebase_auth.zzkj.zzn(r10, r9, r15, r5)
            goto L_0x0a24
        L_0x0879:
            int[] r10 = r13.zzc
            r10 = r10[r8]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.firebase_auth.zzlf.zzf(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.firebase_auth.zzkj.zzk(r10, r9, r15, r5)
            goto L_0x0a24
        L_0x088a:
            int[] r10 = r13.zzc
            r10 = r10[r8]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.firebase_auth.zzlf.zzf(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.firebase_auth.zzkj.zzf(r10, r9, r15, r5)
            goto L_0x0a24
        L_0x089b:
            int[] r10 = r13.zzc
            r10 = r10[r8]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.firebase_auth.zzlf.zzf(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.firebase_auth.zzkj.zzh(r10, r9, r15, r5)
            goto L_0x0a24
        L_0x08ac:
            int[] r10 = r13.zzc
            r10 = r10[r8]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.firebase_auth.zzlf.zzf(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.firebase_auth.zzkj.zzd(r10, r9, r15, r5)
            goto L_0x0a24
        L_0x08bd:
            int[] r10 = r13.zzc
            r10 = r10[r8]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.firebase_auth.zzlf.zzf(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.firebase_auth.zzkj.zzc(r10, r9, r15, r5)
            goto L_0x0a24
        L_0x08ce:
            int[] r10 = r13.zzc
            r10 = r10[r8]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.firebase_auth.zzlf.zzf(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.firebase_auth.zzkj.zzb((int) r10, (java.util.List<java.lang.Float>) r9, (com.google.android.gms.internal.firebase_auth.zzlw) r15, (boolean) r5)
            goto L_0x0a24
        L_0x08df:
            int[] r10 = r13.zzc
            r10 = r10[r8]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.firebase_auth.zzlf.zzf(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.firebase_auth.zzkj.zza((int) r10, (java.util.List<java.lang.Double>) r9, (com.google.android.gms.internal.firebase_auth.zzlw) r15, (boolean) r5)
            goto L_0x0a24
        L_0x08f0:
            boolean r11 = r13.zza(r14, (int) r8)
            if (r11 == 0) goto L_0x0a24
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.firebase_auth.zzlf.zzf(r14, r11)
            com.google.android.gms.internal.firebase_auth.zzkh r11 = r13.zza((int) r8)
            r15.zzb((int) r10, (java.lang.Object) r9, (com.google.android.gms.internal.firebase_auth.zzkh) r11)
            goto L_0x0a24
        L_0x0905:
            boolean r11 = r13.zza(r14, (int) r8)
            if (r11 == 0) goto L_0x0a24
            r9 = r9 & r6
            long r11 = (long) r9
            long r11 = com.google.android.gms.internal.firebase_auth.zzlf.zzb(r14, r11)
            r15.zze((int) r10, (long) r11)
            goto L_0x0a24
        L_0x0916:
            boolean r11 = r13.zza(r14, (int) r8)
            if (r11 == 0) goto L_0x0a24
            r9 = r9 & r6
            long r11 = (long) r9
            int r9 = com.google.android.gms.internal.firebase_auth.zzlf.zza((java.lang.Object) r14, (long) r11)
            r15.zzf(r10, r9)
            goto L_0x0a24
        L_0x0927:
            boolean r11 = r13.zza(r14, (int) r8)
            if (r11 == 0) goto L_0x0a24
            r9 = r9 & r6
            long r11 = (long) r9
            long r11 = com.google.android.gms.internal.firebase_auth.zzlf.zzb(r14, r11)
            r15.zzb((int) r10, (long) r11)
            goto L_0x0a24
        L_0x0938:
            boolean r11 = r13.zza(r14, (int) r8)
            if (r11 == 0) goto L_0x0a24
            r9 = r9 & r6
            long r11 = (long) r9
            int r9 = com.google.android.gms.internal.firebase_auth.zzlf.zza((java.lang.Object) r14, (long) r11)
            r15.zza((int) r10, (int) r9)
            goto L_0x0a24
        L_0x0949:
            boolean r11 = r13.zza(r14, (int) r8)
            if (r11 == 0) goto L_0x0a24
            r9 = r9 & r6
            long r11 = (long) r9
            int r9 = com.google.android.gms.internal.firebase_auth.zzlf.zza((java.lang.Object) r14, (long) r11)
            r15.zzb((int) r10, (int) r9)
            goto L_0x0a24
        L_0x095a:
            boolean r11 = r13.zza(r14, (int) r8)
            if (r11 == 0) goto L_0x0a24
            r9 = r9 & r6
            long r11 = (long) r9
            int r9 = com.google.android.gms.internal.firebase_auth.zzlf.zza((java.lang.Object) r14, (long) r11)
            r15.zze((int) r10, (int) r9)
            goto L_0x0a24
        L_0x096b:
            boolean r11 = r13.zza(r14, (int) r8)
            if (r11 == 0) goto L_0x0a24
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.firebase_auth.zzlf.zzf(r14, r11)
            com.google.android.gms.internal.firebase_auth.zzgv r9 = (com.google.android.gms.internal.firebase_auth.zzgv) r9
            r15.zza((int) r10, (com.google.android.gms.internal.firebase_auth.zzgv) r9)
            goto L_0x0a24
        L_0x097e:
            boolean r11 = r13.zza(r14, (int) r8)
            if (r11 == 0) goto L_0x0a24
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.firebase_auth.zzlf.zzf(r14, r11)
            com.google.android.gms.internal.firebase_auth.zzkh r11 = r13.zza((int) r8)
            r15.zza((int) r10, (java.lang.Object) r9, (com.google.android.gms.internal.firebase_auth.zzkh) r11)
            goto L_0x0a24
        L_0x0993:
            boolean r11 = r13.zza(r14, (int) r8)
            if (r11 == 0) goto L_0x0a24
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.firebase_auth.zzlf.zzf(r14, r11)
            zza((int) r10, (java.lang.Object) r9, (com.google.android.gms.internal.firebase_auth.zzlw) r15)
            goto L_0x0a24
        L_0x09a4:
            boolean r11 = r13.zza(r14, (int) r8)
            if (r11 == 0) goto L_0x0a24
            r9 = r9 & r6
            long r11 = (long) r9
            boolean r9 = com.google.android.gms.internal.firebase_auth.zzlf.zzc(r14, r11)
            r15.zza((int) r10, (boolean) r9)
            goto L_0x0a24
        L_0x09b5:
            boolean r11 = r13.zza(r14, (int) r8)
            if (r11 == 0) goto L_0x0a24
            r9 = r9 & r6
            long r11 = (long) r9
            int r9 = com.google.android.gms.internal.firebase_auth.zzlf.zza((java.lang.Object) r14, (long) r11)
            r15.zzd((int) r10, (int) r9)
            goto L_0x0a24
        L_0x09c5:
            boolean r11 = r13.zza(r14, (int) r8)
            if (r11 == 0) goto L_0x0a24
            r9 = r9 & r6
            long r11 = (long) r9
            long r11 = com.google.android.gms.internal.firebase_auth.zzlf.zzb(r14, r11)
            r15.zzd((int) r10, (long) r11)
            goto L_0x0a24
        L_0x09d5:
            boolean r11 = r13.zza(r14, (int) r8)
            if (r11 == 0) goto L_0x0a24
            r9 = r9 & r6
            long r11 = (long) r9
            int r9 = com.google.android.gms.internal.firebase_auth.zzlf.zza((java.lang.Object) r14, (long) r11)
            r15.zzc((int) r10, (int) r9)
            goto L_0x0a24
        L_0x09e5:
            boolean r11 = r13.zza(r14, (int) r8)
            if (r11 == 0) goto L_0x0a24
            r9 = r9 & r6
            long r11 = (long) r9
            long r11 = com.google.android.gms.internal.firebase_auth.zzlf.zzb(r14, r11)
            r15.zzc((int) r10, (long) r11)
            goto L_0x0a24
        L_0x09f5:
            boolean r11 = r13.zza(r14, (int) r8)
            if (r11 == 0) goto L_0x0a24
            r9 = r9 & r6
            long r11 = (long) r9
            long r11 = com.google.android.gms.internal.firebase_auth.zzlf.zzb(r14, r11)
            r15.zza((int) r10, (long) r11)
            goto L_0x0a24
        L_0x0a05:
            boolean r11 = r13.zza(r14, (int) r8)
            if (r11 == 0) goto L_0x0a24
            r9 = r9 & r6
            long r11 = (long) r9
            float r9 = com.google.android.gms.internal.firebase_auth.zzlf.zzd(r14, r11)
            r15.zza((int) r10, (float) r9)
            goto L_0x0a24
        L_0x0a15:
            boolean r11 = r13.zza(r14, (int) r8)
            if (r11 == 0) goto L_0x0a24
            r9 = r9 & r6
            long r11 = (long) r9
            double r11 = com.google.android.gms.internal.firebase_auth.zzlf.zze(r14, r11)
            r15.zza((int) r10, (double) r11)
        L_0x0a24:
            int r8 = r8 + 3
            goto L_0x0550
        L_0x0a28:
            if (r1 == 0) goto L_0x0a3e
            com.google.android.gms.internal.firebase_auth.zzhv<?> r2 = r13.zzr
            r2.zza(r15, r1)
            boolean r1 = r0.hasNext()
            if (r1 == 0) goto L_0x0a3c
            java.lang.Object r1 = r0.next()
            java.util.Map$Entry r1 = (java.util.Map.Entry) r1
            goto L_0x0a28
        L_0x0a3c:
            r1 = r3
            goto L_0x0a28
        L_0x0a3e:
            com.google.android.gms.internal.firebase_auth.zzkz<?, ?> r0 = r13.zzq
            zza(r0, r14, (com.google.android.gms.internal.firebase_auth.zzlw) r15)
            return
        L_0x0a44:
            r13.zzb(r14, (com.google.android.gms.internal.firebase_auth.zzlw) r15)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.firebase_auth.zzjv.zza(java.lang.Object, com.google.android.gms.internal.firebase_auth.zzlw):void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:170:0x0495  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0031  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final void zzb(T r18, com.google.android.gms.internal.firebase_auth.zzlw r19) throws java.io.IOException {
        /*
            r17 = this;
            r0 = r17
            r1 = r18
            r2 = r19
            boolean r3 = r0.zzh
            if (r3 == 0) goto L_0x0023
            com.google.android.gms.internal.firebase_auth.zzhv<?> r3 = r0.zzr
            com.google.android.gms.internal.firebase_auth.zzhz r3 = r3.zza((java.lang.Object) r1)
            com.google.android.gms.internal.firebase_auth.zzki<T, java.lang.Object> r5 = r3.zza
            boolean r5 = r5.isEmpty()
            if (r5 != 0) goto L_0x0023
            java.util.Iterator r3 = r3.zzd()
            java.lang.Object r5 = r3.next()
            java.util.Map$Entry r5 = (java.util.Map.Entry) r5
            goto L_0x0025
        L_0x0023:
            r3 = 0
            r5 = 0
        L_0x0025:
            int[] r6 = r0.zzc
            int r6 = r6.length
            sun.misc.Unsafe r7 = zzb
            r10 = 0
            r11 = 1048575(0xfffff, float:1.469367E-39)
            r12 = 0
        L_0x002f:
            if (r10 >= r6) goto L_0x0493
            int r13 = r0.zzd((int) r10)
            int[] r14 = r0.zzc
            r15 = r14[r10]
            r16 = 267386880(0xff00000, float:2.3665827E-29)
            r16 = r13 & r16
            int r4 = r16 >>> 20
            boolean r9 = r0.zzj
            if (r9 != 0) goto L_0x005e
            r9 = 17
            if (r4 > r9) goto L_0x005e
            int r9 = r10 + 2
            r9 = r14[r9]
            r14 = 1048575(0xfffff, float:1.469367E-39)
            r8 = r9 & r14
            if (r8 == r11) goto L_0x0058
            long r11 = (long) r8
            int r12 = r7.getInt(r1, r11)
            r11 = r8
        L_0x0058:
            int r8 = r9 >>> 20
            r9 = 1
            int r8 = r9 << r8
            goto L_0x005f
        L_0x005e:
            r8 = 0
        L_0x005f:
            if (r5 == 0) goto L_0x007d
            com.google.android.gms.internal.firebase_auth.zzhv<?> r9 = r0.zzr
            int r9 = r9.zza((java.util.Map.Entry<?, ?>) r5)
            if (r9 > r15) goto L_0x007d
            com.google.android.gms.internal.firebase_auth.zzhv<?> r9 = r0.zzr
            r9.zza(r2, r5)
            boolean r5 = r3.hasNext()
            if (r5 == 0) goto L_0x007b
            java.lang.Object r5 = r3.next()
            java.util.Map$Entry r5 = (java.util.Map.Entry) r5
            goto L_0x005f
        L_0x007b:
            r5 = 0
            goto L_0x005f
        L_0x007d:
            r9 = 1048575(0xfffff, float:1.469367E-39)
            r13 = r13 & r9
            long r13 = (long) r13
            switch(r4) {
                case 0: goto L_0x0484;
                case 1: goto L_0x0478;
                case 2: goto L_0x046c;
                case 3: goto L_0x0460;
                case 4: goto L_0x0454;
                case 5: goto L_0x0448;
                case 6: goto L_0x043c;
                case 7: goto L_0x0430;
                case 8: goto L_0x0424;
                case 9: goto L_0x0413;
                case 10: goto L_0x0404;
                case 11: goto L_0x03f7;
                case 12: goto L_0x03ea;
                case 13: goto L_0x03dd;
                case 14: goto L_0x03d0;
                case 15: goto L_0x03c3;
                case 16: goto L_0x03b6;
                case 17: goto L_0x03a5;
                case 18: goto L_0x0395;
                case 19: goto L_0x0385;
                case 20: goto L_0x0375;
                case 21: goto L_0x0365;
                case 22: goto L_0x0355;
                case 23: goto L_0x0345;
                case 24: goto L_0x0335;
                case 25: goto L_0x0325;
                case 26: goto L_0x0316;
                case 27: goto L_0x0303;
                case 28: goto L_0x02f4;
                case 29: goto L_0x02e4;
                case 30: goto L_0x02d4;
                case 31: goto L_0x02c4;
                case 32: goto L_0x02b4;
                case 33: goto L_0x02a4;
                case 34: goto L_0x0294;
                case 35: goto L_0x0284;
                case 36: goto L_0x0274;
                case 37: goto L_0x0264;
                case 38: goto L_0x0254;
                case 39: goto L_0x0244;
                case 40: goto L_0x0234;
                case 41: goto L_0x0224;
                case 42: goto L_0x0214;
                case 43: goto L_0x0204;
                case 44: goto L_0x01f4;
                case 45: goto L_0x01e4;
                case 46: goto L_0x01d4;
                case 47: goto L_0x01c4;
                case 48: goto L_0x01b4;
                case 49: goto L_0x01a1;
                case 50: goto L_0x0198;
                case 51: goto L_0x0189;
                case 52: goto L_0x017a;
                case 53: goto L_0x016b;
                case 54: goto L_0x015c;
                case 55: goto L_0x014d;
                case 56: goto L_0x013e;
                case 57: goto L_0x012f;
                case 58: goto L_0x0120;
                case 59: goto L_0x0111;
                case 60: goto L_0x00fe;
                case 61: goto L_0x00ee;
                case 62: goto L_0x00e0;
                case 63: goto L_0x00d2;
                case 64: goto L_0x00c4;
                case 65: goto L_0x00b6;
                case 66: goto L_0x00a8;
                case 67: goto L_0x009a;
                case 68: goto L_0x0088;
                default: goto L_0x0085;
            }
        L_0x0085:
            r4 = 0
            goto L_0x048f
        L_0x0088:
            boolean r4 = r0.zza(r1, (int) r15, (int) r10)
            if (r4 == 0) goto L_0x0085
            java.lang.Object r4 = r7.getObject(r1, r13)
            com.google.android.gms.internal.firebase_auth.zzkh r8 = r0.zza((int) r10)
            r2.zzb((int) r15, (java.lang.Object) r4, (com.google.android.gms.internal.firebase_auth.zzkh) r8)
            goto L_0x0085
        L_0x009a:
            boolean r4 = r0.zza(r1, (int) r15, (int) r10)
            if (r4 == 0) goto L_0x0085
            long r13 = zze(r1, r13)
            r2.zze((int) r15, (long) r13)
            goto L_0x0085
        L_0x00a8:
            boolean r4 = r0.zza(r1, (int) r15, (int) r10)
            if (r4 == 0) goto L_0x0085
            int r4 = zzd(r1, r13)
            r2.zzf(r15, r4)
            goto L_0x0085
        L_0x00b6:
            boolean r4 = r0.zza(r1, (int) r15, (int) r10)
            if (r4 == 0) goto L_0x0085
            long r13 = zze(r1, r13)
            r2.zzb((int) r15, (long) r13)
            goto L_0x0085
        L_0x00c4:
            boolean r4 = r0.zza(r1, (int) r15, (int) r10)
            if (r4 == 0) goto L_0x0085
            int r4 = zzd(r1, r13)
            r2.zza((int) r15, (int) r4)
            goto L_0x0085
        L_0x00d2:
            boolean r4 = r0.zza(r1, (int) r15, (int) r10)
            if (r4 == 0) goto L_0x0085
            int r4 = zzd(r1, r13)
            r2.zzb((int) r15, (int) r4)
            goto L_0x0085
        L_0x00e0:
            boolean r4 = r0.zza(r1, (int) r15, (int) r10)
            if (r4 == 0) goto L_0x0085
            int r4 = zzd(r1, r13)
            r2.zze((int) r15, (int) r4)
            goto L_0x0085
        L_0x00ee:
            boolean r4 = r0.zza(r1, (int) r15, (int) r10)
            if (r4 == 0) goto L_0x0085
            java.lang.Object r4 = r7.getObject(r1, r13)
            com.google.android.gms.internal.firebase_auth.zzgv r4 = (com.google.android.gms.internal.firebase_auth.zzgv) r4
            r2.zza((int) r15, (com.google.android.gms.internal.firebase_auth.zzgv) r4)
            goto L_0x0085
        L_0x00fe:
            boolean r4 = r0.zza(r1, (int) r15, (int) r10)
            if (r4 == 0) goto L_0x0085
            java.lang.Object r4 = r7.getObject(r1, r13)
            com.google.android.gms.internal.firebase_auth.zzkh r8 = r0.zza((int) r10)
            r2.zza((int) r15, (java.lang.Object) r4, (com.google.android.gms.internal.firebase_auth.zzkh) r8)
            goto L_0x0085
        L_0x0111:
            boolean r4 = r0.zza(r1, (int) r15, (int) r10)
            if (r4 == 0) goto L_0x0085
            java.lang.Object r4 = r7.getObject(r1, r13)
            zza((int) r15, (java.lang.Object) r4, (com.google.android.gms.internal.firebase_auth.zzlw) r2)
            goto L_0x0085
        L_0x0120:
            boolean r4 = r0.zza(r1, (int) r15, (int) r10)
            if (r4 == 0) goto L_0x0085
            boolean r4 = zzf(r1, r13)
            r2.zza((int) r15, (boolean) r4)
            goto L_0x0085
        L_0x012f:
            boolean r4 = r0.zza(r1, (int) r15, (int) r10)
            if (r4 == 0) goto L_0x0085
            int r4 = zzd(r1, r13)
            r2.zzd((int) r15, (int) r4)
            goto L_0x0085
        L_0x013e:
            boolean r4 = r0.zza(r1, (int) r15, (int) r10)
            if (r4 == 0) goto L_0x0085
            long r13 = zze(r1, r13)
            r2.zzd((int) r15, (long) r13)
            goto L_0x0085
        L_0x014d:
            boolean r4 = r0.zza(r1, (int) r15, (int) r10)
            if (r4 == 0) goto L_0x0085
            int r4 = zzd(r1, r13)
            r2.zzc((int) r15, (int) r4)
            goto L_0x0085
        L_0x015c:
            boolean r4 = r0.zza(r1, (int) r15, (int) r10)
            if (r4 == 0) goto L_0x0085
            long r13 = zze(r1, r13)
            r2.zzc((int) r15, (long) r13)
            goto L_0x0085
        L_0x016b:
            boolean r4 = r0.zza(r1, (int) r15, (int) r10)
            if (r4 == 0) goto L_0x0085
            long r13 = zze(r1, r13)
            r2.zza((int) r15, (long) r13)
            goto L_0x0085
        L_0x017a:
            boolean r4 = r0.zza(r1, (int) r15, (int) r10)
            if (r4 == 0) goto L_0x0085
            float r4 = zzc(r1, r13)
            r2.zza((int) r15, (float) r4)
            goto L_0x0085
        L_0x0189:
            boolean r4 = r0.zza(r1, (int) r15, (int) r10)
            if (r4 == 0) goto L_0x0085
            double r13 = zzb(r1, (long) r13)
            r2.zza((int) r15, (double) r13)
            goto L_0x0085
        L_0x0198:
            java.lang.Object r4 = r7.getObject(r1, r13)
            r0.zza((com.google.android.gms.internal.firebase_auth.zzlw) r2, (int) r15, (java.lang.Object) r4, (int) r10)
            goto L_0x0085
        L_0x01a1:
            int[] r4 = r0.zzc
            r4 = r4[r10]
            java.lang.Object r8 = r7.getObject(r1, r13)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.firebase_auth.zzkh r13 = r0.zza((int) r10)
            com.google.android.gms.internal.firebase_auth.zzkj.zzb((int) r4, (java.util.List<?>) r8, (com.google.android.gms.internal.firebase_auth.zzlw) r2, (com.google.android.gms.internal.firebase_auth.zzkh) r13)
            goto L_0x0085
        L_0x01b4:
            int[] r4 = r0.zzc
            r4 = r4[r10]
            java.lang.Object r8 = r7.getObject(r1, r13)
            java.util.List r8 = (java.util.List) r8
            r15 = 1
            com.google.android.gms.internal.firebase_auth.zzkj.zze(r4, r8, r2, r15)
            goto L_0x0085
        L_0x01c4:
            r15 = 1
            int[] r4 = r0.zzc
            r4 = r4[r10]
            java.lang.Object r8 = r7.getObject(r1, r13)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.firebase_auth.zzkj.zzj(r4, r8, r2, r15)
            goto L_0x0085
        L_0x01d4:
            r15 = 1
            int[] r4 = r0.zzc
            r4 = r4[r10]
            java.lang.Object r8 = r7.getObject(r1, r13)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.firebase_auth.zzkj.zzg(r4, r8, r2, r15)
            goto L_0x0085
        L_0x01e4:
            r15 = 1
            int[] r4 = r0.zzc
            r4 = r4[r10]
            java.lang.Object r8 = r7.getObject(r1, r13)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.firebase_auth.zzkj.zzl(r4, r8, r2, r15)
            goto L_0x0085
        L_0x01f4:
            r15 = 1
            int[] r4 = r0.zzc
            r4 = r4[r10]
            java.lang.Object r8 = r7.getObject(r1, r13)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.firebase_auth.zzkj.zzm(r4, r8, r2, r15)
            goto L_0x0085
        L_0x0204:
            r15 = 1
            int[] r4 = r0.zzc
            r4 = r4[r10]
            java.lang.Object r8 = r7.getObject(r1, r13)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.firebase_auth.zzkj.zzi(r4, r8, r2, r15)
            goto L_0x0085
        L_0x0214:
            r15 = 1
            int[] r4 = r0.zzc
            r4 = r4[r10]
            java.lang.Object r8 = r7.getObject(r1, r13)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.firebase_auth.zzkj.zzn(r4, r8, r2, r15)
            goto L_0x0085
        L_0x0224:
            r15 = 1
            int[] r4 = r0.zzc
            r4 = r4[r10]
            java.lang.Object r8 = r7.getObject(r1, r13)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.firebase_auth.zzkj.zzk(r4, r8, r2, r15)
            goto L_0x0085
        L_0x0234:
            r15 = 1
            int[] r4 = r0.zzc
            r4 = r4[r10]
            java.lang.Object r8 = r7.getObject(r1, r13)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.firebase_auth.zzkj.zzf(r4, r8, r2, r15)
            goto L_0x0085
        L_0x0244:
            r15 = 1
            int[] r4 = r0.zzc
            r4 = r4[r10]
            java.lang.Object r8 = r7.getObject(r1, r13)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.firebase_auth.zzkj.zzh(r4, r8, r2, r15)
            goto L_0x0085
        L_0x0254:
            r15 = 1
            int[] r4 = r0.zzc
            r4 = r4[r10]
            java.lang.Object r8 = r7.getObject(r1, r13)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.firebase_auth.zzkj.zzd(r4, r8, r2, r15)
            goto L_0x0085
        L_0x0264:
            r15 = 1
            int[] r4 = r0.zzc
            r4 = r4[r10]
            java.lang.Object r8 = r7.getObject(r1, r13)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.firebase_auth.zzkj.zzc(r4, r8, r2, r15)
            goto L_0x0085
        L_0x0274:
            r15 = 1
            int[] r4 = r0.zzc
            r4 = r4[r10]
            java.lang.Object r8 = r7.getObject(r1, r13)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.firebase_auth.zzkj.zzb((int) r4, (java.util.List<java.lang.Float>) r8, (com.google.android.gms.internal.firebase_auth.zzlw) r2, (boolean) r15)
            goto L_0x0085
        L_0x0284:
            r15 = 1
            int[] r4 = r0.zzc
            r4 = r4[r10]
            java.lang.Object r8 = r7.getObject(r1, r13)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.firebase_auth.zzkj.zza((int) r4, (java.util.List<java.lang.Double>) r8, (com.google.android.gms.internal.firebase_auth.zzlw) r2, (boolean) r15)
            goto L_0x0085
        L_0x0294:
            int[] r4 = r0.zzc
            r4 = r4[r10]
            java.lang.Object r8 = r7.getObject(r1, r13)
            java.util.List r8 = (java.util.List) r8
            r15 = 0
            com.google.android.gms.internal.firebase_auth.zzkj.zze(r4, r8, r2, r15)
            goto L_0x0085
        L_0x02a4:
            r15 = 0
            int[] r4 = r0.zzc
            r4 = r4[r10]
            java.lang.Object r8 = r7.getObject(r1, r13)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.firebase_auth.zzkj.zzj(r4, r8, r2, r15)
            goto L_0x0085
        L_0x02b4:
            r15 = 0
            int[] r4 = r0.zzc
            r4 = r4[r10]
            java.lang.Object r8 = r7.getObject(r1, r13)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.firebase_auth.zzkj.zzg(r4, r8, r2, r15)
            goto L_0x0085
        L_0x02c4:
            r15 = 0
            int[] r4 = r0.zzc
            r4 = r4[r10]
            java.lang.Object r8 = r7.getObject(r1, r13)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.firebase_auth.zzkj.zzl(r4, r8, r2, r15)
            goto L_0x0085
        L_0x02d4:
            r15 = 0
            int[] r4 = r0.zzc
            r4 = r4[r10]
            java.lang.Object r8 = r7.getObject(r1, r13)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.firebase_auth.zzkj.zzm(r4, r8, r2, r15)
            goto L_0x0085
        L_0x02e4:
            r15 = 0
            int[] r4 = r0.zzc
            r4 = r4[r10]
            java.lang.Object r8 = r7.getObject(r1, r13)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.firebase_auth.zzkj.zzi(r4, r8, r2, r15)
            goto L_0x0085
        L_0x02f4:
            int[] r4 = r0.zzc
            r4 = r4[r10]
            java.lang.Object r8 = r7.getObject(r1, r13)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.firebase_auth.zzkj.zzb((int) r4, (java.util.List<com.google.android.gms.internal.firebase_auth.zzgv>) r8, (com.google.android.gms.internal.firebase_auth.zzlw) r2)
            goto L_0x0085
        L_0x0303:
            int[] r4 = r0.zzc
            r4 = r4[r10]
            java.lang.Object r8 = r7.getObject(r1, r13)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.firebase_auth.zzkh r13 = r0.zza((int) r10)
            com.google.android.gms.internal.firebase_auth.zzkj.zza((int) r4, (java.util.List<?>) r8, (com.google.android.gms.internal.firebase_auth.zzlw) r2, (com.google.android.gms.internal.firebase_auth.zzkh) r13)
            goto L_0x0085
        L_0x0316:
            int[] r4 = r0.zzc
            r4 = r4[r10]
            java.lang.Object r8 = r7.getObject(r1, r13)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.firebase_auth.zzkj.zza((int) r4, (java.util.List<java.lang.String>) r8, (com.google.android.gms.internal.firebase_auth.zzlw) r2)
            goto L_0x0085
        L_0x0325:
            int[] r4 = r0.zzc
            r4 = r4[r10]
            java.lang.Object r8 = r7.getObject(r1, r13)
            java.util.List r8 = (java.util.List) r8
            r15 = 0
            com.google.android.gms.internal.firebase_auth.zzkj.zzn(r4, r8, r2, r15)
            goto L_0x0085
        L_0x0335:
            r15 = 0
            int[] r4 = r0.zzc
            r4 = r4[r10]
            java.lang.Object r8 = r7.getObject(r1, r13)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.firebase_auth.zzkj.zzk(r4, r8, r2, r15)
            goto L_0x0085
        L_0x0345:
            r15 = 0
            int[] r4 = r0.zzc
            r4 = r4[r10]
            java.lang.Object r8 = r7.getObject(r1, r13)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.firebase_auth.zzkj.zzf(r4, r8, r2, r15)
            goto L_0x0085
        L_0x0355:
            r15 = 0
            int[] r4 = r0.zzc
            r4 = r4[r10]
            java.lang.Object r8 = r7.getObject(r1, r13)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.firebase_auth.zzkj.zzh(r4, r8, r2, r15)
            goto L_0x0085
        L_0x0365:
            r15 = 0
            int[] r4 = r0.zzc
            r4 = r4[r10]
            java.lang.Object r8 = r7.getObject(r1, r13)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.firebase_auth.zzkj.zzd(r4, r8, r2, r15)
            goto L_0x0085
        L_0x0375:
            r15 = 0
            int[] r4 = r0.zzc
            r4 = r4[r10]
            java.lang.Object r8 = r7.getObject(r1, r13)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.firebase_auth.zzkj.zzc(r4, r8, r2, r15)
            goto L_0x0085
        L_0x0385:
            r15 = 0
            int[] r4 = r0.zzc
            r4 = r4[r10]
            java.lang.Object r8 = r7.getObject(r1, r13)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.firebase_auth.zzkj.zzb((int) r4, (java.util.List<java.lang.Float>) r8, (com.google.android.gms.internal.firebase_auth.zzlw) r2, (boolean) r15)
            goto L_0x0085
        L_0x0395:
            r15 = 0
            int[] r4 = r0.zzc
            r4 = r4[r10]
            java.lang.Object r8 = r7.getObject(r1, r13)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.firebase_auth.zzkj.zza((int) r4, (java.util.List<java.lang.Double>) r8, (com.google.android.gms.internal.firebase_auth.zzlw) r2, (boolean) r15)
            goto L_0x0085
        L_0x03a5:
            r4 = 0
            r8 = r8 & r12
            if (r8 == 0) goto L_0x048f
            java.lang.Object r8 = r7.getObject(r1, r13)
            com.google.android.gms.internal.firebase_auth.zzkh r13 = r0.zza((int) r10)
            r2.zzb((int) r15, (java.lang.Object) r8, (com.google.android.gms.internal.firebase_auth.zzkh) r13)
            goto L_0x048f
        L_0x03b6:
            r4 = 0
            r8 = r8 & r12
            if (r8 == 0) goto L_0x048f
            long r13 = r7.getLong(r1, r13)
            r2.zze((int) r15, (long) r13)
            goto L_0x048f
        L_0x03c3:
            r4 = 0
            r8 = r8 & r12
            if (r8 == 0) goto L_0x048f
            int r8 = r7.getInt(r1, r13)
            r2.zzf(r15, r8)
            goto L_0x048f
        L_0x03d0:
            r4 = 0
            r8 = r8 & r12
            if (r8 == 0) goto L_0x048f
            long r13 = r7.getLong(r1, r13)
            r2.zzb((int) r15, (long) r13)
            goto L_0x048f
        L_0x03dd:
            r4 = 0
            r8 = r8 & r12
            if (r8 == 0) goto L_0x048f
            int r8 = r7.getInt(r1, r13)
            r2.zza((int) r15, (int) r8)
            goto L_0x048f
        L_0x03ea:
            r4 = 0
            r8 = r8 & r12
            if (r8 == 0) goto L_0x048f
            int r8 = r7.getInt(r1, r13)
            r2.zzb((int) r15, (int) r8)
            goto L_0x048f
        L_0x03f7:
            r4 = 0
            r8 = r8 & r12
            if (r8 == 0) goto L_0x048f
            int r8 = r7.getInt(r1, r13)
            r2.zze((int) r15, (int) r8)
            goto L_0x048f
        L_0x0404:
            r4 = 0
            r8 = r8 & r12
            if (r8 == 0) goto L_0x048f
            java.lang.Object r8 = r7.getObject(r1, r13)
            com.google.android.gms.internal.firebase_auth.zzgv r8 = (com.google.android.gms.internal.firebase_auth.zzgv) r8
            r2.zza((int) r15, (com.google.android.gms.internal.firebase_auth.zzgv) r8)
            goto L_0x048f
        L_0x0413:
            r4 = 0
            r8 = r8 & r12
            if (r8 == 0) goto L_0x048f
            java.lang.Object r8 = r7.getObject(r1, r13)
            com.google.android.gms.internal.firebase_auth.zzkh r13 = r0.zza((int) r10)
            r2.zza((int) r15, (java.lang.Object) r8, (com.google.android.gms.internal.firebase_auth.zzkh) r13)
            goto L_0x048f
        L_0x0424:
            r4 = 0
            r8 = r8 & r12
            if (r8 == 0) goto L_0x048f
            java.lang.Object r8 = r7.getObject(r1, r13)
            zza((int) r15, (java.lang.Object) r8, (com.google.android.gms.internal.firebase_auth.zzlw) r2)
            goto L_0x048f
        L_0x0430:
            r4 = 0
            r8 = r8 & r12
            if (r8 == 0) goto L_0x048f
            boolean r8 = com.google.android.gms.internal.firebase_auth.zzlf.zzc(r1, r13)
            r2.zza((int) r15, (boolean) r8)
            goto L_0x048f
        L_0x043c:
            r4 = 0
            r8 = r8 & r12
            if (r8 == 0) goto L_0x048f
            int r8 = r7.getInt(r1, r13)
            r2.zzd((int) r15, (int) r8)
            goto L_0x048f
        L_0x0448:
            r4 = 0
            r8 = r8 & r12
            if (r8 == 0) goto L_0x048f
            long r13 = r7.getLong(r1, r13)
            r2.zzd((int) r15, (long) r13)
            goto L_0x048f
        L_0x0454:
            r4 = 0
            r8 = r8 & r12
            if (r8 == 0) goto L_0x048f
            int r8 = r7.getInt(r1, r13)
            r2.zzc((int) r15, (int) r8)
            goto L_0x048f
        L_0x0460:
            r4 = 0
            r8 = r8 & r12
            if (r8 == 0) goto L_0x048f
            long r13 = r7.getLong(r1, r13)
            r2.zzc((int) r15, (long) r13)
            goto L_0x048f
        L_0x046c:
            r4 = 0
            r8 = r8 & r12
            if (r8 == 0) goto L_0x048f
            long r13 = r7.getLong(r1, r13)
            r2.zza((int) r15, (long) r13)
            goto L_0x048f
        L_0x0478:
            r4 = 0
            r8 = r8 & r12
            if (r8 == 0) goto L_0x048f
            float r8 = com.google.android.gms.internal.firebase_auth.zzlf.zzd(r1, r13)
            r2.zza((int) r15, (float) r8)
            goto L_0x048f
        L_0x0484:
            r4 = 0
            r8 = r8 & r12
            if (r8 == 0) goto L_0x048f
            double r13 = com.google.android.gms.internal.firebase_auth.zzlf.zze(r1, r13)
            r2.zza((int) r15, (double) r13)
        L_0x048f:
            int r10 = r10 + 3
            goto L_0x002f
        L_0x0493:
            if (r5 == 0) goto L_0x04aa
            com.google.android.gms.internal.firebase_auth.zzhv<?> r4 = r0.zzr
            r4.zza(r2, r5)
            boolean r4 = r3.hasNext()
            if (r4 == 0) goto L_0x04a8
            java.lang.Object r4 = r3.next()
            java.util.Map$Entry r4 = (java.util.Map.Entry) r4
            r5 = r4
            goto L_0x0493
        L_0x04a8:
            r5 = 0
            goto L_0x0493
        L_0x04aa:
            com.google.android.gms.internal.firebase_auth.zzkz<?, ?> r3 = r0.zzq
            zza(r3, r1, (com.google.android.gms.internal.firebase_auth.zzlw) r2)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.firebase_auth.zzjv.zzb(java.lang.Object, com.google.android.gms.internal.firebase_auth.zzlw):void");
    }

    private final <K, V> void zza(zzlw zzlw, int i, Object obj, int i2) throws IOException {
        if (obj != null) {
            zzlw.zza(i, this.zzs.zzf(zzb(i2)), this.zzs.zzb(obj));
        }
    }

    private static <UT, UB> void zza(zzkz<UT, UB> zzkz, T t, zzlw zzlw) throws IOException {
        zzkz.zza(zzkz.zzb(t), zzlw);
    }

    /*  JADX ERROR: StackOverflow in pass: MarkFinallyVisitor
        jadx.core.utils.exceptions.JadxOverflowException: 
        	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:47)
        	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:81)
        */
    public final void zza(T r18, com.google.android.gms.internal.firebase_auth.zzke r19, com.google.android.gms.internal.firebase_auth.zzht r20) throws java.io.IOException {
        /*
            r17 = this;
            r1 = r17
            r2 = r18
            r0 = r19
            r10 = r20
            r11 = 0
            if (r10 == 0) goto L_0x05ed
            com.google.android.gms.internal.firebase_auth.zzkz<?, ?> r12 = r1.zzq
            com.google.android.gms.internal.firebase_auth.zzhv<?> r13 = r1.zzr
            r3 = r11
            r14 = r3
        L_0x0011:
            int r4 = r19.zza()     // Catch:{ all -> 0x05d5 }
            int r5 = r1.zze     // Catch:{ all -> 0x05d5 }
            r6 = -1
            if (r4 < r5) goto L_0x003e
            int r5 = r1.zzf     // Catch:{ all -> 0x05d5 }
            if (r4 > r5) goto L_0x003e
            r5 = 0
            int[] r7 = r1.zzc     // Catch:{ all -> 0x05d5 }
            int r7 = r7.length     // Catch:{ all -> 0x05d5 }
            int r7 = r7 / 3
            int r7 = r7 + -1
        L_0x0026:
            if (r5 > r7) goto L_0x003e
            int r8 = r7 + r5
            int r8 = r8 >>> 1
            int r9 = r8 * 3
            int[] r15 = r1.zzc     // Catch:{ all -> 0x05d5 }
            r15 = r15[r9]     // Catch:{ all -> 0x05d5 }
            if (r4 != r15) goto L_0x0036
            r6 = r9
            goto L_0x003e
        L_0x0036:
            if (r4 >= r15) goto L_0x003b
            int r7 = r8 + -1
            goto L_0x0026
        L_0x003b:
            int r5 = r8 + 1
            goto L_0x0026
        L_0x003e:
            if (r6 >= 0) goto L_0x00a6
            r5 = 2147483647(0x7fffffff, float:NaN)
            if (r4 != r5) goto L_0x005c
            int r0 = r1.zzm
        L_0x0047:
            int r3 = r1.zzn
            if (r0 >= r3) goto L_0x0056
            int[] r3 = r1.zzl
            r3 = r3[r0]
            java.lang.Object r14 = r1.zza((java.lang.Object) r2, (int) r3, r14, r12)
            int r0 = r0 + 1
            goto L_0x0047
        L_0x0056:
            if (r14 == 0) goto L_0x005b
            r12.zzb((java.lang.Object) r2, r14)
        L_0x005b:
            return
        L_0x005c:
            boolean r5 = r1.zzh     // Catch:{ all -> 0x05d5 }
            if (r5 != 0) goto L_0x0062
            r5 = r11
            goto L_0x0069
        L_0x0062:
            com.google.android.gms.internal.firebase_auth.zzjr r5 = r1.zzg     // Catch:{ all -> 0x05d5 }
            java.lang.Object r4 = r13.zza(r10, r5, r4)     // Catch:{ all -> 0x05d5 }
            r5 = r4
        L_0x0069:
            if (r5 == 0) goto L_0x0080
            if (r3 != 0) goto L_0x0071
            com.google.android.gms.internal.firebase_auth.zzhz r3 = r13.zzb(r2)     // Catch:{ all -> 0x05d5 }
        L_0x0071:
            r15 = r3
            r3 = r13
            r4 = r19
            r6 = r20
            r7 = r15
            r8 = r14
            r9 = r12
            java.lang.Object r14 = r3.zza(r4, r5, r6, r7, r8, r9)     // Catch:{ all -> 0x05d5 }
            r3 = r15
            goto L_0x0011
        L_0x0080:
            r12.zza((com.google.android.gms.internal.firebase_auth.zzke) r0)     // Catch:{ all -> 0x05d5 }
            if (r14 != 0) goto L_0x0089
            java.lang.Object r14 = r12.zzc(r2)     // Catch:{ all -> 0x05d5 }
        L_0x0089:
            boolean r4 = r12.zza(r14, (com.google.android.gms.internal.firebase_auth.zzke) r0)     // Catch:{ all -> 0x05d5 }
            if (r4 != 0) goto L_0x0011
            int r0 = r1.zzm
        L_0x0091:
            int r3 = r1.zzn
            if (r0 >= r3) goto L_0x00a0
            int[] r3 = r1.zzl
            r3 = r3[r0]
            java.lang.Object r14 = r1.zza((java.lang.Object) r2, (int) r3, r14, r12)
            int r0 = r0 + 1
            goto L_0x0091
        L_0x00a0:
            if (r14 == 0) goto L_0x00a5
            r12.zzb((java.lang.Object) r2, r14)
        L_0x00a5:
            return
        L_0x00a6:
            int r5 = r1.zzd((int) r6)     // Catch:{ all -> 0x05d5 }
            r7 = 267386880(0xff00000, float:2.3665827E-29)
            r7 = r7 & r5
            int r7 = r7 >>> 20
            r8 = 1048575(0xfffff, float:1.469367E-39)
            switch(r7) {
                case 0: goto L_0x0582;
                case 1: goto L_0x0573;
                case 2: goto L_0x0564;
                case 3: goto L_0x0555;
                case 4: goto L_0x0546;
                case 5: goto L_0x0537;
                case 6: goto L_0x0528;
                case 7: goto L_0x0519;
                case 8: goto L_0x0511;
                case 9: goto L_0x04e0;
                case 10: goto L_0x04d1;
                case 11: goto L_0x04c2;
                case 12: goto L_0x04a0;
                case 13: goto L_0x0491;
                case 14: goto L_0x0482;
                case 15: goto L_0x0473;
                case 16: goto L_0x0464;
                case 17: goto L_0x0433;
                case 18: goto L_0x0426;
                case 19: goto L_0x0419;
                case 20: goto L_0x040c;
                case 21: goto L_0x03ff;
                case 22: goto L_0x03f2;
                case 23: goto L_0x03e5;
                case 24: goto L_0x03d8;
                case 25: goto L_0x03cb;
                case 26: goto L_0x03ab;
                case 27: goto L_0x039a;
                case 28: goto L_0x038d;
                case 29: goto L_0x0380;
                case 30: goto L_0x036b;
                case 31: goto L_0x035e;
                case 32: goto L_0x0351;
                case 33: goto L_0x0344;
                case 34: goto L_0x0337;
                case 35: goto L_0x032a;
                case 36: goto L_0x031d;
                case 37: goto L_0x0310;
                case 38: goto L_0x0303;
                case 39: goto L_0x02f6;
                case 40: goto L_0x02e9;
                case 41: goto L_0x02dc;
                case 42: goto L_0x02cf;
                case 43: goto L_0x02c2;
                case 44: goto L_0x02ad;
                case 45: goto L_0x02a0;
                case 46: goto L_0x0293;
                case 47: goto L_0x0286;
                case 48: goto L_0x0279;
                case 49: goto L_0x0267;
                case 50: goto L_0x0225;
                case 51: goto L_0x0213;
                case 52: goto L_0x0201;
                case 53: goto L_0x01ef;
                case 54: goto L_0x01dd;
                case 55: goto L_0x01cb;
                case 56: goto L_0x01b9;
                case 57: goto L_0x01a7;
                case 58: goto L_0x0195;
                case 59: goto L_0x018d;
                case 60: goto L_0x015c;
                case 61: goto L_0x014e;
                case 62: goto L_0x013c;
                case 63: goto L_0x0117;
                case 64: goto L_0x0105;
                case 65: goto L_0x00f3;
                case 66: goto L_0x00e1;
                case 67: goto L_0x00cf;
                case 68: goto L_0x00bd;
                default: goto L_0x00b5;
            }
        L_0x00b5:
            if (r14 != 0) goto L_0x0591
            java.lang.Object r14 = r12.zza()     // Catch:{ zziq -> 0x05ae }
            goto L_0x0591
        L_0x00bd:
            r5 = r5 & r8
            long r7 = (long) r5     // Catch:{ zziq -> 0x05ae }
            com.google.android.gms.internal.firebase_auth.zzkh r5 = r1.zza((int) r6)     // Catch:{ zziq -> 0x05ae }
            java.lang.Object r5 = r0.zzb(r5, r10)     // Catch:{ zziq -> 0x05ae }
            com.google.android.gms.internal.firebase_auth.zzlf.zza((java.lang.Object) r2, (long) r7, (java.lang.Object) r5)     // Catch:{ zziq -> 0x05ae }
            r1.zzb(r2, (int) r4, (int) r6)     // Catch:{ zziq -> 0x05ae }
            goto L_0x0011
        L_0x00cf:
            r5 = r5 & r8
            long r7 = (long) r5     // Catch:{ zziq -> 0x05ae }
            long r15 = r19.zzt()     // Catch:{ zziq -> 0x05ae }
            java.lang.Long r5 = java.lang.Long.valueOf(r15)     // Catch:{ zziq -> 0x05ae }
            com.google.android.gms.internal.firebase_auth.zzlf.zza((java.lang.Object) r2, (long) r7, (java.lang.Object) r5)     // Catch:{ zziq -> 0x05ae }
            r1.zzb(r2, (int) r4, (int) r6)     // Catch:{ zziq -> 0x05ae }
            goto L_0x0011
        L_0x00e1:
            r5 = r5 & r8
            long r7 = (long) r5     // Catch:{ zziq -> 0x05ae }
            int r5 = r19.zzs()     // Catch:{ zziq -> 0x05ae }
            java.lang.Integer r5 = java.lang.Integer.valueOf(r5)     // Catch:{ zziq -> 0x05ae }
            com.google.android.gms.internal.firebase_auth.zzlf.zza((java.lang.Object) r2, (long) r7, (java.lang.Object) r5)     // Catch:{ zziq -> 0x05ae }
            r1.zzb(r2, (int) r4, (int) r6)     // Catch:{ zziq -> 0x05ae }
            goto L_0x0011
        L_0x00f3:
            r5 = r5 & r8
            long r7 = (long) r5     // Catch:{ zziq -> 0x05ae }
            long r15 = r19.zzr()     // Catch:{ zziq -> 0x05ae }
            java.lang.Long r5 = java.lang.Long.valueOf(r15)     // Catch:{ zziq -> 0x05ae }
            com.google.android.gms.internal.firebase_auth.zzlf.zza((java.lang.Object) r2, (long) r7, (java.lang.Object) r5)     // Catch:{ zziq -> 0x05ae }
            r1.zzb(r2, (int) r4, (int) r6)     // Catch:{ zziq -> 0x05ae }
            goto L_0x0011
        L_0x0105:
            r5 = r5 & r8
            long r7 = (long) r5     // Catch:{ zziq -> 0x05ae }
            int r5 = r19.zzq()     // Catch:{ zziq -> 0x05ae }
            java.lang.Integer r5 = java.lang.Integer.valueOf(r5)     // Catch:{ zziq -> 0x05ae }
            com.google.android.gms.internal.firebase_auth.zzlf.zza((java.lang.Object) r2, (long) r7, (java.lang.Object) r5)     // Catch:{ zziq -> 0x05ae }
            r1.zzb(r2, (int) r4, (int) r6)     // Catch:{ zziq -> 0x05ae }
            goto L_0x0011
        L_0x0117:
            int r7 = r19.zzp()     // Catch:{ zziq -> 0x05ae }
            com.google.android.gms.internal.firebase_auth.zzin r9 = r1.zzc((int) r6)     // Catch:{ zziq -> 0x05ae }
            if (r9 == 0) goto L_0x012e
            boolean r9 = r9.zza(r7)     // Catch:{ zziq -> 0x05ae }
            if (r9 == 0) goto L_0x0128
            goto L_0x012e
        L_0x0128:
            java.lang.Object r14 = com.google.android.gms.internal.firebase_auth.zzkj.zza((int) r4, (int) r7, r14, r12)     // Catch:{ zziq -> 0x05ae }
            goto L_0x0011
        L_0x012e:
            r5 = r5 & r8
            long r8 = (long) r5     // Catch:{ zziq -> 0x05ae }
            java.lang.Integer r5 = java.lang.Integer.valueOf(r7)     // Catch:{ zziq -> 0x05ae }
            com.google.android.gms.internal.firebase_auth.zzlf.zza((java.lang.Object) r2, (long) r8, (java.lang.Object) r5)     // Catch:{ zziq -> 0x05ae }
            r1.zzb(r2, (int) r4, (int) r6)     // Catch:{ zziq -> 0x05ae }
            goto L_0x0011
        L_0x013c:
            r5 = r5 & r8
            long r7 = (long) r5     // Catch:{ zziq -> 0x05ae }
            int r5 = r19.zzo()     // Catch:{ zziq -> 0x05ae }
            java.lang.Integer r5 = java.lang.Integer.valueOf(r5)     // Catch:{ zziq -> 0x05ae }
            com.google.android.gms.internal.firebase_auth.zzlf.zza((java.lang.Object) r2, (long) r7, (java.lang.Object) r5)     // Catch:{ zziq -> 0x05ae }
            r1.zzb(r2, (int) r4, (int) r6)     // Catch:{ zziq -> 0x05ae }
            goto L_0x0011
        L_0x014e:
            r5 = r5 & r8
            long r7 = (long) r5     // Catch:{ zziq -> 0x05ae }
            com.google.android.gms.internal.firebase_auth.zzgv r5 = r19.zzn()     // Catch:{ zziq -> 0x05ae }
            com.google.android.gms.internal.firebase_auth.zzlf.zza((java.lang.Object) r2, (long) r7, (java.lang.Object) r5)     // Catch:{ zziq -> 0x05ae }
            r1.zzb(r2, (int) r4, (int) r6)     // Catch:{ zziq -> 0x05ae }
            goto L_0x0011
        L_0x015c:
            boolean r7 = r1.zza(r2, (int) r4, (int) r6)     // Catch:{ zziq -> 0x05ae }
            if (r7 == 0) goto L_0x0178
            r5 = r5 & r8
            long r7 = (long) r5     // Catch:{ zziq -> 0x05ae }
            java.lang.Object r5 = com.google.android.gms.internal.firebase_auth.zzlf.zzf(r2, r7)     // Catch:{ zziq -> 0x05ae }
            com.google.android.gms.internal.firebase_auth.zzkh r9 = r1.zza((int) r6)     // Catch:{ zziq -> 0x05ae }
            java.lang.Object r9 = r0.zza(r9, r10)     // Catch:{ zziq -> 0x05ae }
            java.lang.Object r5 = com.google.android.gms.internal.firebase_auth.zzii.zza((java.lang.Object) r5, (java.lang.Object) r9)     // Catch:{ zziq -> 0x05ae }
            com.google.android.gms.internal.firebase_auth.zzlf.zza((java.lang.Object) r2, (long) r7, (java.lang.Object) r5)     // Catch:{ zziq -> 0x05ae }
            goto L_0x0188
        L_0x0178:
            r5 = r5 & r8
            long r7 = (long) r5     // Catch:{ zziq -> 0x05ae }
            com.google.android.gms.internal.firebase_auth.zzkh r5 = r1.zza((int) r6)     // Catch:{ zziq -> 0x05ae }
            java.lang.Object r5 = r0.zza(r5, r10)     // Catch:{ zziq -> 0x05ae }
            com.google.android.gms.internal.firebase_auth.zzlf.zza((java.lang.Object) r2, (long) r7, (java.lang.Object) r5)     // Catch:{ zziq -> 0x05ae }
            r1.zzb(r2, (int) r6)     // Catch:{ zziq -> 0x05ae }
        L_0x0188:
            r1.zzb(r2, (int) r4, (int) r6)     // Catch:{ zziq -> 0x05ae }
            goto L_0x0011
        L_0x018d:
            r1.zza((java.lang.Object) r2, (int) r5, (com.google.android.gms.internal.firebase_auth.zzke) r0)     // Catch:{ zziq -> 0x05ae }
            r1.zzb(r2, (int) r4, (int) r6)     // Catch:{ zziq -> 0x05ae }
            goto L_0x0011
        L_0x0195:
            r5 = r5 & r8
            long r7 = (long) r5     // Catch:{ zziq -> 0x05ae }
            boolean r5 = r19.zzk()     // Catch:{ zziq -> 0x05ae }
            java.lang.Boolean r5 = java.lang.Boolean.valueOf(r5)     // Catch:{ zziq -> 0x05ae }
            com.google.android.gms.internal.firebase_auth.zzlf.zza((java.lang.Object) r2, (long) r7, (java.lang.Object) r5)     // Catch:{ zziq -> 0x05ae }
            r1.zzb(r2, (int) r4, (int) r6)     // Catch:{ zziq -> 0x05ae }
            goto L_0x0011
        L_0x01a7:
            r5 = r5 & r8
            long r7 = (long) r5     // Catch:{ zziq -> 0x05ae }
            int r5 = r19.zzj()     // Catch:{ zziq -> 0x05ae }
            java.lang.Integer r5 = java.lang.Integer.valueOf(r5)     // Catch:{ zziq -> 0x05ae }
            com.google.android.gms.internal.firebase_auth.zzlf.zza((java.lang.Object) r2, (long) r7, (java.lang.Object) r5)     // Catch:{ zziq -> 0x05ae }
            r1.zzb(r2, (int) r4, (int) r6)     // Catch:{ zziq -> 0x05ae }
            goto L_0x0011
        L_0x01b9:
            r5 = r5 & r8
            long r7 = (long) r5     // Catch:{ zziq -> 0x05ae }
            long r15 = r19.zzi()     // Catch:{ zziq -> 0x05ae }
            java.lang.Long r5 = java.lang.Long.valueOf(r15)     // Catch:{ zziq -> 0x05ae }
            com.google.android.gms.internal.firebase_auth.zzlf.zza((java.lang.Object) r2, (long) r7, (java.lang.Object) r5)     // Catch:{ zziq -> 0x05ae }
            r1.zzb(r2, (int) r4, (int) r6)     // Catch:{ zziq -> 0x05ae }
            goto L_0x0011
        L_0x01cb:
            r5 = r5 & r8
            long r7 = (long) r5     // Catch:{ zziq -> 0x05ae }
            int r5 = r19.zzh()     // Catch:{ zziq -> 0x05ae }
            java.lang.Integer r5 = java.lang.Integer.valueOf(r5)     // Catch:{ zziq -> 0x05ae }
            com.google.android.gms.internal.firebase_auth.zzlf.zza((java.lang.Object) r2, (long) r7, (java.lang.Object) r5)     // Catch:{ zziq -> 0x05ae }
            r1.zzb(r2, (int) r4, (int) r6)     // Catch:{ zziq -> 0x05ae }
            goto L_0x0011
        L_0x01dd:
            r5 = r5 & r8
            long r7 = (long) r5     // Catch:{ zziq -> 0x05ae }
            long r15 = r19.zzf()     // Catch:{ zziq -> 0x05ae }
            java.lang.Long r5 = java.lang.Long.valueOf(r15)     // Catch:{ zziq -> 0x05ae }
            com.google.android.gms.internal.firebase_auth.zzlf.zza((java.lang.Object) r2, (long) r7, (java.lang.Object) r5)     // Catch:{ zziq -> 0x05ae }
            r1.zzb(r2, (int) r4, (int) r6)     // Catch:{ zziq -> 0x05ae }
            goto L_0x0011
        L_0x01ef:
            r5 = r5 & r8
            long r7 = (long) r5     // Catch:{ zziq -> 0x05ae }
            long r15 = r19.zzg()     // Catch:{ zziq -> 0x05ae }
            java.lang.Long r5 = java.lang.Long.valueOf(r15)     // Catch:{ zziq -> 0x05ae }
            com.google.android.gms.internal.firebase_auth.zzlf.zza((java.lang.Object) r2, (long) r7, (java.lang.Object) r5)     // Catch:{ zziq -> 0x05ae }
            r1.zzb(r2, (int) r4, (int) r6)     // Catch:{ zziq -> 0x05ae }
            goto L_0x0011
        L_0x0201:
            r5 = r5 & r8
            long r7 = (long) r5     // Catch:{ zziq -> 0x05ae }
            float r5 = r19.zze()     // Catch:{ zziq -> 0x05ae }
            java.lang.Float r5 = java.lang.Float.valueOf(r5)     // Catch:{ zziq -> 0x05ae }
            com.google.android.gms.internal.firebase_auth.zzlf.zza((java.lang.Object) r2, (long) r7, (java.lang.Object) r5)     // Catch:{ zziq -> 0x05ae }
            r1.zzb(r2, (int) r4, (int) r6)     // Catch:{ zziq -> 0x05ae }
            goto L_0x0011
        L_0x0213:
            r5 = r5 & r8
            long r7 = (long) r5     // Catch:{ zziq -> 0x05ae }
            double r15 = r19.zzd()     // Catch:{ zziq -> 0x05ae }
            java.lang.Double r5 = java.lang.Double.valueOf(r15)     // Catch:{ zziq -> 0x05ae }
            com.google.android.gms.internal.firebase_auth.zzlf.zza((java.lang.Object) r2, (long) r7, (java.lang.Object) r5)     // Catch:{ zziq -> 0x05ae }
            r1.zzb(r2, (int) r4, (int) r6)     // Catch:{ zziq -> 0x05ae }
            goto L_0x0011
        L_0x0225:
            java.lang.Object r4 = r1.zzb((int) r6)     // Catch:{ zziq -> 0x05ae }
            int r5 = r1.zzd((int) r6)     // Catch:{ zziq -> 0x05ae }
            r5 = r5 & r8
            long r5 = (long) r5     // Catch:{ zziq -> 0x05ae }
            java.lang.Object r7 = com.google.android.gms.internal.firebase_auth.zzlf.zzf(r2, r5)     // Catch:{ zziq -> 0x05ae }
            if (r7 != 0) goto L_0x023f
            com.google.android.gms.internal.firebase_auth.zzjk r7 = r1.zzs     // Catch:{ zziq -> 0x05ae }
            java.lang.Object r7 = r7.zze(r4)     // Catch:{ zziq -> 0x05ae }
            com.google.android.gms.internal.firebase_auth.zzlf.zza((java.lang.Object) r2, (long) r5, (java.lang.Object) r7)     // Catch:{ zziq -> 0x05ae }
            goto L_0x0256
        L_0x023f:
            com.google.android.gms.internal.firebase_auth.zzjk r8 = r1.zzs     // Catch:{ zziq -> 0x05ae }
            boolean r8 = r8.zzc(r7)     // Catch:{ zziq -> 0x05ae }
            if (r8 == 0) goto L_0x0256
            com.google.android.gms.internal.firebase_auth.zzjk r8 = r1.zzs     // Catch:{ zziq -> 0x05ae }
            java.lang.Object r8 = r8.zze(r4)     // Catch:{ zziq -> 0x05ae }
            com.google.android.gms.internal.firebase_auth.zzjk r9 = r1.zzs     // Catch:{ zziq -> 0x05ae }
            r9.zza(r8, r7)     // Catch:{ zziq -> 0x05ae }
            com.google.android.gms.internal.firebase_auth.zzlf.zza((java.lang.Object) r2, (long) r5, (java.lang.Object) r8)     // Catch:{ zziq -> 0x05ae }
            r7 = r8
        L_0x0256:
            com.google.android.gms.internal.firebase_auth.zzjk r5 = r1.zzs     // Catch:{ zziq -> 0x05ae }
            java.util.Map r5 = r5.zza(r7)     // Catch:{ zziq -> 0x05ae }
            com.google.android.gms.internal.firebase_auth.zzjk r6 = r1.zzs     // Catch:{ zziq -> 0x05ae }
            com.google.android.gms.internal.firebase_auth.zzji r4 = r6.zzf(r4)     // Catch:{ zziq -> 0x05ae }
            r0.zza(r5, r4, (com.google.android.gms.internal.firebase_auth.zzht) r10)     // Catch:{ zziq -> 0x05ae }
            goto L_0x0011
        L_0x0267:
            r4 = r5 & r8
            long r4 = (long) r4     // Catch:{ zziq -> 0x05ae }
            com.google.android.gms.internal.firebase_auth.zzkh r6 = r1.zza((int) r6)     // Catch:{ zziq -> 0x05ae }
            com.google.android.gms.internal.firebase_auth.zzjb r7 = r1.zzp     // Catch:{ zziq -> 0x05ae }
            java.util.List r4 = r7.zza(r2, r4)     // Catch:{ zziq -> 0x05ae }
            r0.zzb(r4, r6, r10)     // Catch:{ zziq -> 0x05ae }
            goto L_0x0011
        L_0x0279:
            com.google.android.gms.internal.firebase_auth.zzjb r4 = r1.zzp     // Catch:{ zziq -> 0x05ae }
            r5 = r5 & r8
            long r5 = (long) r5     // Catch:{ zziq -> 0x05ae }
            java.util.List r4 = r4.zza(r2, r5)     // Catch:{ zziq -> 0x05ae }
            r0.zzq(r4)     // Catch:{ zziq -> 0x05ae }
            goto L_0x0011
        L_0x0286:
            com.google.android.gms.internal.firebase_auth.zzjb r4 = r1.zzp     // Catch:{ zziq -> 0x05ae }
            r5 = r5 & r8
            long r5 = (long) r5     // Catch:{ zziq -> 0x05ae }
            java.util.List r4 = r4.zza(r2, r5)     // Catch:{ zziq -> 0x05ae }
            r0.zzp(r4)     // Catch:{ zziq -> 0x05ae }
            goto L_0x0011
        L_0x0293:
            com.google.android.gms.internal.firebase_auth.zzjb r4 = r1.zzp     // Catch:{ zziq -> 0x05ae }
            r5 = r5 & r8
            long r5 = (long) r5     // Catch:{ zziq -> 0x05ae }
            java.util.List r4 = r4.zza(r2, r5)     // Catch:{ zziq -> 0x05ae }
            r0.zzo(r4)     // Catch:{ zziq -> 0x05ae }
            goto L_0x0011
        L_0x02a0:
            com.google.android.gms.internal.firebase_auth.zzjb r4 = r1.zzp     // Catch:{ zziq -> 0x05ae }
            r5 = r5 & r8
            long r5 = (long) r5     // Catch:{ zziq -> 0x05ae }
            java.util.List r4 = r4.zza(r2, r5)     // Catch:{ zziq -> 0x05ae }
            r0.zzn(r4)     // Catch:{ zziq -> 0x05ae }
            goto L_0x0011
        L_0x02ad:
            com.google.android.gms.internal.firebase_auth.zzjb r7 = r1.zzp     // Catch:{ zziq -> 0x05ae }
            r5 = r5 & r8
            long r8 = (long) r5     // Catch:{ zziq -> 0x05ae }
            java.util.List r5 = r7.zza(r2, r8)     // Catch:{ zziq -> 0x05ae }
            r0.zzm(r5)     // Catch:{ zziq -> 0x05ae }
            com.google.android.gms.internal.firebase_auth.zzin r6 = r1.zzc((int) r6)     // Catch:{ zziq -> 0x05ae }
            java.lang.Object r14 = com.google.android.gms.internal.firebase_auth.zzkj.zza(r4, r5, r6, r14, r12)     // Catch:{ zziq -> 0x05ae }
            goto L_0x0011
        L_0x02c2:
            com.google.android.gms.internal.firebase_auth.zzjb r4 = r1.zzp     // Catch:{ zziq -> 0x05ae }
            r5 = r5 & r8
            long r5 = (long) r5     // Catch:{ zziq -> 0x05ae }
            java.util.List r4 = r4.zza(r2, r5)     // Catch:{ zziq -> 0x05ae }
            r0.zzl(r4)     // Catch:{ zziq -> 0x05ae }
            goto L_0x0011
        L_0x02cf:
            com.google.android.gms.internal.firebase_auth.zzjb r4 = r1.zzp     // Catch:{ zziq -> 0x05ae }
            r5 = r5 & r8
            long r5 = (long) r5     // Catch:{ zziq -> 0x05ae }
            java.util.List r4 = r4.zza(r2, r5)     // Catch:{ zziq -> 0x05ae }
            r0.zzh(r4)     // Catch:{ zziq -> 0x05ae }
            goto L_0x0011
        L_0x02dc:
            com.google.android.gms.internal.firebase_auth.zzjb r4 = r1.zzp     // Catch:{ zziq -> 0x05ae }
            r5 = r5 & r8
            long r5 = (long) r5     // Catch:{ zziq -> 0x05ae }
            java.util.List r4 = r4.zza(r2, r5)     // Catch:{ zziq -> 0x05ae }
            r0.zzg(r4)     // Catch:{ zziq -> 0x05ae }
            goto L_0x0011
        L_0x02e9:
            com.google.android.gms.internal.firebase_auth.zzjb r4 = r1.zzp     // Catch:{ zziq -> 0x05ae }
            r5 = r5 & r8
            long r5 = (long) r5     // Catch:{ zziq -> 0x05ae }
            java.util.List r4 = r4.zza(r2, r5)     // Catch:{ zziq -> 0x05ae }
            r0.zzf(r4)     // Catch:{ zziq -> 0x05ae }
            goto L_0x0011
        L_0x02f6:
            com.google.android.gms.internal.firebase_auth.zzjb r4 = r1.zzp     // Catch:{ zziq -> 0x05ae }
            r5 = r5 & r8
            long r5 = (long) r5     // Catch:{ zziq -> 0x05ae }
            java.util.List r4 = r4.zza(r2, r5)     // Catch:{ zziq -> 0x05ae }
            r0.zze(r4)     // Catch:{ zziq -> 0x05ae }
            goto L_0x0011
        L_0x0303:
            com.google.android.gms.internal.firebase_auth.zzjb r4 = r1.zzp     // Catch:{ zziq -> 0x05ae }
            r5 = r5 & r8
            long r5 = (long) r5     // Catch:{ zziq -> 0x05ae }
            java.util.List r4 = r4.zza(r2, r5)     // Catch:{ zziq -> 0x05ae }
            r0.zzc(r4)     // Catch:{ zziq -> 0x05ae }
            goto L_0x0011
        L_0x0310:
            com.google.android.gms.internal.firebase_auth.zzjb r4 = r1.zzp     // Catch:{ zziq -> 0x05ae }
            r5 = r5 & r8
            long r5 = (long) r5     // Catch:{ zziq -> 0x05ae }
            java.util.List r4 = r4.zza(r2, r5)     // Catch:{ zziq -> 0x05ae }
            r0.zzd(r4)     // Catch:{ zziq -> 0x05ae }
            goto L_0x0011
        L_0x031d:
            com.google.android.gms.internal.firebase_auth.zzjb r4 = r1.zzp     // Catch:{ zziq -> 0x05ae }
            r5 = r5 & r8
            long r5 = (long) r5     // Catch:{ zziq -> 0x05ae }
            java.util.List r4 = r4.zza(r2, r5)     // Catch:{ zziq -> 0x05ae }
            r0.zzb(r4)     // Catch:{ zziq -> 0x05ae }
            goto L_0x0011
        L_0x032a:
            com.google.android.gms.internal.firebase_auth.zzjb r4 = r1.zzp     // Catch:{ zziq -> 0x05ae }
            r5 = r5 & r8
            long r5 = (long) r5     // Catch:{ zziq -> 0x05ae }
            java.util.List r4 = r4.zza(r2, r5)     // Catch:{ zziq -> 0x05ae }
            r0.zza(r4)     // Catch:{ zziq -> 0x05ae }
            goto L_0x0011
        L_0x0337:
            com.google.android.gms.internal.firebase_auth.zzjb r4 = r1.zzp     // Catch:{ zziq -> 0x05ae }
            r5 = r5 & r8
            long r5 = (long) r5     // Catch:{ zziq -> 0x05ae }
            java.util.List r4 = r4.zza(r2, r5)     // Catch:{ zziq -> 0x05ae }
            r0.zzq(r4)     // Catch:{ zziq -> 0x05ae }
            goto L_0x0011
        L_0x0344:
            com.google.android.gms.internal.firebase_auth.zzjb r4 = r1.zzp     // Catch:{ zziq -> 0x05ae }
            r5 = r5 & r8
            long r5 = (long) r5     // Catch:{ zziq -> 0x05ae }
            java.util.List r4 = r4.zza(r2, r5)     // Catch:{ zziq -> 0x05ae }
            r0.zzp(r4)     // Catch:{ zziq -> 0x05ae }
            goto L_0x0011
        L_0x0351:
            com.google.android.gms.internal.firebase_auth.zzjb r4 = r1.zzp     // Catch:{ zziq -> 0x05ae }
            r5 = r5 & r8
            long r5 = (long) r5     // Catch:{ zziq -> 0x05ae }
            java.util.List r4 = r4.zza(r2, r5)     // Catch:{ zziq -> 0x05ae }
            r0.zzo(r4)     // Catch:{ zziq -> 0x05ae }
            goto L_0x0011
        L_0x035e:
            com.google.android.gms.internal.firebase_auth.zzjb r4 = r1.zzp     // Catch:{ zziq -> 0x05ae }
            r5 = r5 & r8
            long r5 = (long) r5     // Catch:{ zziq -> 0x05ae }
            java.util.List r4 = r4.zza(r2, r5)     // Catch:{ zziq -> 0x05ae }
            r0.zzn(r4)     // Catch:{ zziq -> 0x05ae }
            goto L_0x0011
        L_0x036b:
            com.google.android.gms.internal.firebase_auth.zzjb r7 = r1.zzp     // Catch:{ zziq -> 0x05ae }
            r5 = r5 & r8
            long r8 = (long) r5     // Catch:{ zziq -> 0x05ae }
            java.util.List r5 = r7.zza(r2, r8)     // Catch:{ zziq -> 0x05ae }
            r0.zzm(r5)     // Catch:{ zziq -> 0x05ae }
            com.google.android.gms.internal.firebase_auth.zzin r6 = r1.zzc((int) r6)     // Catch:{ zziq -> 0x05ae }
            java.lang.Object r14 = com.google.android.gms.internal.firebase_auth.zzkj.zza(r4, r5, r6, r14, r12)     // Catch:{ zziq -> 0x05ae }
            goto L_0x0011
        L_0x0380:
            com.google.android.gms.internal.firebase_auth.zzjb r4 = r1.zzp     // Catch:{ zziq -> 0x05ae }
            r5 = r5 & r8
            long r5 = (long) r5     // Catch:{ zziq -> 0x05ae }
            java.util.List r4 = r4.zza(r2, r5)     // Catch:{ zziq -> 0x05ae }
            r0.zzl(r4)     // Catch:{ zziq -> 0x05ae }
            goto L_0x0011
        L_0x038d:
            com.google.android.gms.internal.firebase_auth.zzjb r4 = r1.zzp     // Catch:{ zziq -> 0x05ae }
            r5 = r5 & r8
            long r5 = (long) r5     // Catch:{ zziq -> 0x05ae }
            java.util.List r4 = r4.zza(r2, r5)     // Catch:{ zziq -> 0x05ae }
            r0.zzk(r4)     // Catch:{ zziq -> 0x05ae }
            goto L_0x0011
        L_0x039a:
            com.google.android.gms.internal.firebase_auth.zzkh r4 = r1.zza((int) r6)     // Catch:{ zziq -> 0x05ae }
            r5 = r5 & r8
            long r5 = (long) r5     // Catch:{ zziq -> 0x05ae }
            com.google.android.gms.internal.firebase_auth.zzjb r7 = r1.zzp     // Catch:{ zziq -> 0x05ae }
            java.util.List r5 = r7.zza(r2, r5)     // Catch:{ zziq -> 0x05ae }
            r0.zza(r5, r4, (com.google.android.gms.internal.firebase_auth.zzht) r10)     // Catch:{ zziq -> 0x05ae }
            goto L_0x0011
        L_0x03ab:
            boolean r4 = zzf(r5)     // Catch:{ zziq -> 0x05ae }
            if (r4 == 0) goto L_0x03be
            com.google.android.gms.internal.firebase_auth.zzjb r4 = r1.zzp     // Catch:{ zziq -> 0x05ae }
            r5 = r5 & r8
            long r5 = (long) r5     // Catch:{ zziq -> 0x05ae }
            java.util.List r4 = r4.zza(r2, r5)     // Catch:{ zziq -> 0x05ae }
            r0.zzj(r4)     // Catch:{ zziq -> 0x05ae }
            goto L_0x0011
        L_0x03be:
            com.google.android.gms.internal.firebase_auth.zzjb r4 = r1.zzp     // Catch:{ zziq -> 0x05ae }
            r5 = r5 & r8
            long r5 = (long) r5     // Catch:{ zziq -> 0x05ae }
            java.util.List r4 = r4.zza(r2, r5)     // Catch:{ zziq -> 0x05ae }
            r0.zzi(r4)     // Catch:{ zziq -> 0x05ae }
            goto L_0x0011
        L_0x03cb:
            com.google.android.gms.internal.firebase_auth.zzjb r4 = r1.zzp     // Catch:{ zziq -> 0x05ae }
            r5 = r5 & r8
            long r5 = (long) r5     // Catch:{ zziq -> 0x05ae }
            java.util.List r4 = r4.zza(r2, r5)     // Catch:{ zziq -> 0x05ae }
            r0.zzh(r4)     // Catch:{ zziq -> 0x05ae }
            goto L_0x0011
        L_0x03d8:
            com.google.android.gms.internal.firebase_auth.zzjb r4 = r1.zzp     // Catch:{ zziq -> 0x05ae }
            r5 = r5 & r8
            long r5 = (long) r5     // Catch:{ zziq -> 0x05ae }
            java.util.List r4 = r4.zza(r2, r5)     // Catch:{ zziq -> 0x05ae }
            r0.zzg(r4)     // Catch:{ zziq -> 0x05ae }
            goto L_0x0011
        L_0x03e5:
            com.google.android.gms.internal.firebase_auth.zzjb r4 = r1.zzp     // Catch:{ zziq -> 0x05ae }
            r5 = r5 & r8
            long r5 = (long) r5     // Catch:{ zziq -> 0x05ae }
            java.util.List r4 = r4.zza(r2, r5)     // Catch:{ zziq -> 0x05ae }
            r0.zzf(r4)     // Catch:{ zziq -> 0x05ae }
            goto L_0x0011
        L_0x03f2:
            com.google.android.gms.internal.firebase_auth.zzjb r4 = r1.zzp     // Catch:{ zziq -> 0x05ae }
            r5 = r5 & r8
            long r5 = (long) r5     // Catch:{ zziq -> 0x05ae }
            java.util.List r4 = r4.zza(r2, r5)     // Catch:{ zziq -> 0x05ae }
            r0.zze(r4)     // Catch:{ zziq -> 0x05ae }
            goto L_0x0011
        L_0x03ff:
            com.google.android.gms.internal.firebase_auth.zzjb r4 = r1.zzp     // Catch:{ zziq -> 0x05ae }
            r5 = r5 & r8
            long r5 = (long) r5     // Catch:{ zziq -> 0x05ae }
            java.util.List r4 = r4.zza(r2, r5)     // Catch:{ zziq -> 0x05ae }
            r0.zzc(r4)     // Catch:{ zziq -> 0x05ae }
            goto L_0x0011
        L_0x040c:
            com.google.android.gms.internal.firebase_auth.zzjb r4 = r1.zzp     // Catch:{ zziq -> 0x05ae }
            r5 = r5 & r8
            long r5 = (long) r5     // Catch:{ zziq -> 0x05ae }
            java.util.List r4 = r4.zza(r2, r5)     // Catch:{ zziq -> 0x05ae }
            r0.zzd(r4)     // Catch:{ zziq -> 0x05ae }
            goto L_0x0011
        L_0x0419:
            com.google.android.gms.internal.firebase_auth.zzjb r4 = r1.zzp     // Catch:{ zziq -> 0x05ae }
            r5 = r5 & r8
            long r5 = (long) r5     // Catch:{ zziq -> 0x05ae }
            java.util.List r4 = r4.zza(r2, r5)     // Catch:{ zziq -> 0x05ae }
            r0.zzb(r4)     // Catch:{ zziq -> 0x05ae }
            goto L_0x0011
        L_0x0426:
            com.google.android.gms.internal.firebase_auth.zzjb r4 = r1.zzp     // Catch:{ zziq -> 0x05ae }
            r5 = r5 & r8
            long r5 = (long) r5     // Catch:{ zziq -> 0x05ae }
            java.util.List r4 = r4.zza(r2, r5)     // Catch:{ zziq -> 0x05ae }
            r0.zza(r4)     // Catch:{ zziq -> 0x05ae }
            goto L_0x0011
        L_0x0433:
            boolean r4 = r1.zza(r2, (int) r6)     // Catch:{ zziq -> 0x05ae }
            if (r4 == 0) goto L_0x0451
            r4 = r5 & r8
            long r4 = (long) r4     // Catch:{ zziq -> 0x05ae }
            java.lang.Object r7 = com.google.android.gms.internal.firebase_auth.zzlf.zzf(r2, r4)     // Catch:{ zziq -> 0x05ae }
            com.google.android.gms.internal.firebase_auth.zzkh r6 = r1.zza((int) r6)     // Catch:{ zziq -> 0x05ae }
            java.lang.Object r6 = r0.zzb(r6, r10)     // Catch:{ zziq -> 0x05ae }
            java.lang.Object r6 = com.google.android.gms.internal.firebase_auth.zzii.zza((java.lang.Object) r7, (java.lang.Object) r6)     // Catch:{ zziq -> 0x05ae }
            com.google.android.gms.internal.firebase_auth.zzlf.zza((java.lang.Object) r2, (long) r4, (java.lang.Object) r6)     // Catch:{ zziq -> 0x05ae }
            goto L_0x0011
        L_0x0451:
            r4 = r5 & r8
            long r4 = (long) r4     // Catch:{ zziq -> 0x05ae }
            com.google.android.gms.internal.firebase_auth.zzkh r7 = r1.zza((int) r6)     // Catch:{ zziq -> 0x05ae }
            java.lang.Object r7 = r0.zzb(r7, r10)     // Catch:{ zziq -> 0x05ae }
            com.google.android.gms.internal.firebase_auth.zzlf.zza((java.lang.Object) r2, (long) r4, (java.lang.Object) r7)     // Catch:{ zziq -> 0x05ae }
            r1.zzb(r2, (int) r6)     // Catch:{ zziq -> 0x05ae }
            goto L_0x0011
        L_0x0464:
            r4 = r5 & r8
            long r4 = (long) r4     // Catch:{ zziq -> 0x05ae }
            long r7 = r19.zzt()     // Catch:{ zziq -> 0x05ae }
            com.google.android.gms.internal.firebase_auth.zzlf.zza((java.lang.Object) r2, (long) r4, (long) r7)     // Catch:{ zziq -> 0x05ae }
            r1.zzb(r2, (int) r6)     // Catch:{ zziq -> 0x05ae }
            goto L_0x0011
        L_0x0473:
            r4 = r5 & r8
            long r4 = (long) r4     // Catch:{ zziq -> 0x05ae }
            int r7 = r19.zzs()     // Catch:{ zziq -> 0x05ae }
            com.google.android.gms.internal.firebase_auth.zzlf.zza((java.lang.Object) r2, (long) r4, (int) r7)     // Catch:{ zziq -> 0x05ae }
            r1.zzb(r2, (int) r6)     // Catch:{ zziq -> 0x05ae }
            goto L_0x0011
        L_0x0482:
            r4 = r5 & r8
            long r4 = (long) r4     // Catch:{ zziq -> 0x05ae }
            long r7 = r19.zzr()     // Catch:{ zziq -> 0x05ae }
            com.google.android.gms.internal.firebase_auth.zzlf.zza((java.lang.Object) r2, (long) r4, (long) r7)     // Catch:{ zziq -> 0x05ae }
            r1.zzb(r2, (int) r6)     // Catch:{ zziq -> 0x05ae }
            goto L_0x0011
        L_0x0491:
            r4 = r5 & r8
            long r4 = (long) r4     // Catch:{ zziq -> 0x05ae }
            int r7 = r19.zzq()     // Catch:{ zziq -> 0x05ae }
            com.google.android.gms.internal.firebase_auth.zzlf.zza((java.lang.Object) r2, (long) r4, (int) r7)     // Catch:{ zziq -> 0x05ae }
            r1.zzb(r2, (int) r6)     // Catch:{ zziq -> 0x05ae }
            goto L_0x0011
        L_0x04a0:
            int r7 = r19.zzp()     // Catch:{ zziq -> 0x05ae }
            com.google.android.gms.internal.firebase_auth.zzin r9 = r1.zzc((int) r6)     // Catch:{ zziq -> 0x05ae }
            if (r9 == 0) goto L_0x04b7
            boolean r9 = r9.zza(r7)     // Catch:{ zziq -> 0x05ae }
            if (r9 == 0) goto L_0x04b1
            goto L_0x04b7
        L_0x04b1:
            java.lang.Object r14 = com.google.android.gms.internal.firebase_auth.zzkj.zza((int) r4, (int) r7, r14, r12)     // Catch:{ zziq -> 0x05ae }
            goto L_0x0011
        L_0x04b7:
            r4 = r5 & r8
            long r4 = (long) r4     // Catch:{ zziq -> 0x05ae }
            com.google.android.gms.internal.firebase_auth.zzlf.zza((java.lang.Object) r2, (long) r4, (int) r7)     // Catch:{ zziq -> 0x05ae }
            r1.zzb(r2, (int) r6)     // Catch:{ zziq -> 0x05ae }
            goto L_0x0011
        L_0x04c2:
            r4 = r5 & r8
            long r4 = (long) r4     // Catch:{ zziq -> 0x05ae }
            int r7 = r19.zzo()     // Catch:{ zziq -> 0x05ae }
            com.google.android.gms.internal.firebase_auth.zzlf.zza((java.lang.Object) r2, (long) r4, (int) r7)     // Catch:{ zziq -> 0x05ae }
            r1.zzb(r2, (int) r6)     // Catch:{ zziq -> 0x05ae }
            goto L_0x0011
        L_0x04d1:
            r4 = r5 & r8
            long r4 = (long) r4     // Catch:{ zziq -> 0x05ae }
            com.google.android.gms.internal.firebase_auth.zzgv r7 = r19.zzn()     // Catch:{ zziq -> 0x05ae }
            com.google.android.gms.internal.firebase_auth.zzlf.zza((java.lang.Object) r2, (long) r4, (java.lang.Object) r7)     // Catch:{ zziq -> 0x05ae }
            r1.zzb(r2, (int) r6)     // Catch:{ zziq -> 0x05ae }
            goto L_0x0011
        L_0x04e0:
            boolean r4 = r1.zza(r2, (int) r6)     // Catch:{ zziq -> 0x05ae }
            if (r4 == 0) goto L_0x04fe
            r4 = r5 & r8
            long r4 = (long) r4     // Catch:{ zziq -> 0x05ae }
            java.lang.Object r7 = com.google.android.gms.internal.firebase_auth.zzlf.zzf(r2, r4)     // Catch:{ zziq -> 0x05ae }
            com.google.android.gms.internal.firebase_auth.zzkh r6 = r1.zza((int) r6)     // Catch:{ zziq -> 0x05ae }
            java.lang.Object r6 = r0.zza(r6, r10)     // Catch:{ zziq -> 0x05ae }
            java.lang.Object r6 = com.google.android.gms.internal.firebase_auth.zzii.zza((java.lang.Object) r7, (java.lang.Object) r6)     // Catch:{ zziq -> 0x05ae }
            com.google.android.gms.internal.firebase_auth.zzlf.zza((java.lang.Object) r2, (long) r4, (java.lang.Object) r6)     // Catch:{ zziq -> 0x05ae }
            goto L_0x0011
        L_0x04fe:
            r4 = r5 & r8
            long r4 = (long) r4     // Catch:{ zziq -> 0x05ae }
            com.google.android.gms.internal.firebase_auth.zzkh r7 = r1.zza((int) r6)     // Catch:{ zziq -> 0x05ae }
            java.lang.Object r7 = r0.zza(r7, r10)     // Catch:{ zziq -> 0x05ae }
            com.google.android.gms.internal.firebase_auth.zzlf.zza((java.lang.Object) r2, (long) r4, (java.lang.Object) r7)     // Catch:{ zziq -> 0x05ae }
            r1.zzb(r2, (int) r6)     // Catch:{ zziq -> 0x05ae }
            goto L_0x0011
        L_0x0511:
            r1.zza((java.lang.Object) r2, (int) r5, (com.google.android.gms.internal.firebase_auth.zzke) r0)     // Catch:{ zziq -> 0x05ae }
            r1.zzb(r2, (int) r6)     // Catch:{ zziq -> 0x05ae }
            goto L_0x0011
        L_0x0519:
            r4 = r5 & r8
            long r4 = (long) r4     // Catch:{ zziq -> 0x05ae }
            boolean r7 = r19.zzk()     // Catch:{ zziq -> 0x05ae }
            com.google.android.gms.internal.firebase_auth.zzlf.zza((java.lang.Object) r2, (long) r4, (boolean) r7)     // Catch:{ zziq -> 0x05ae }
            r1.zzb(r2, (int) r6)     // Catch:{ zziq -> 0x05ae }
            goto L_0x0011
        L_0x0528:
            r4 = r5 & r8
            long r4 = (long) r4     // Catch:{ zziq -> 0x05ae }
            int r7 = r19.zzj()     // Catch:{ zziq -> 0x05ae }
            com.google.android.gms.internal.firebase_auth.zzlf.zza((java.lang.Object) r2, (long) r4, (int) r7)     // Catch:{ zziq -> 0x05ae }
            r1.zzb(r2, (int) r6)     // Catch:{ zziq -> 0x05ae }
            goto L_0x0011
        L_0x0537:
            r4 = r5 & r8
            long r4 = (long) r4     // Catch:{ zziq -> 0x05ae }
            long r7 = r19.zzi()     // Catch:{ zziq -> 0x05ae }
            com.google.android.gms.internal.firebase_auth.zzlf.zza((java.lang.Object) r2, (long) r4, (long) r7)     // Catch:{ zziq -> 0x05ae }
            r1.zzb(r2, (int) r6)     // Catch:{ zziq -> 0x05ae }
            goto L_0x0011
        L_0x0546:
            r4 = r5 & r8
            long r4 = (long) r4     // Catch:{ zziq -> 0x05ae }
            int r7 = r19.zzh()     // Catch:{ zziq -> 0x05ae }
            com.google.android.gms.internal.firebase_auth.zzlf.zza((java.lang.Object) r2, (long) r4, (int) r7)     // Catch:{ zziq -> 0x05ae }
            r1.zzb(r2, (int) r6)     // Catch:{ zziq -> 0x05ae }
            goto L_0x0011
        L_0x0555:
            r4 = r5 & r8
            long r4 = (long) r4     // Catch:{ zziq -> 0x05ae }
            long r7 = r19.zzf()     // Catch:{ zziq -> 0x05ae }
            com.google.android.gms.internal.firebase_auth.zzlf.zza((java.lang.Object) r2, (long) r4, (long) r7)     // Catch:{ zziq -> 0x05ae }
            r1.zzb(r2, (int) r6)     // Catch:{ zziq -> 0x05ae }
            goto L_0x0011
        L_0x0564:
            r4 = r5 & r8
            long r4 = (long) r4     // Catch:{ zziq -> 0x05ae }
            long r7 = r19.zzg()     // Catch:{ zziq -> 0x05ae }
            com.google.android.gms.internal.firebase_auth.zzlf.zza((java.lang.Object) r2, (long) r4, (long) r7)     // Catch:{ zziq -> 0x05ae }
            r1.zzb(r2, (int) r6)     // Catch:{ zziq -> 0x05ae }
            goto L_0x0011
        L_0x0573:
            r4 = r5 & r8
            long r4 = (long) r4     // Catch:{ zziq -> 0x05ae }
            float r7 = r19.zze()     // Catch:{ zziq -> 0x05ae }
            com.google.android.gms.internal.firebase_auth.zzlf.zza((java.lang.Object) r2, (long) r4, (float) r7)     // Catch:{ zziq -> 0x05ae }
            r1.zzb(r2, (int) r6)     // Catch:{ zziq -> 0x05ae }
            goto L_0x0011
        L_0x0582:
            r4 = r5 & r8
            long r4 = (long) r4     // Catch:{ zziq -> 0x05ae }
            double r7 = r19.zzd()     // Catch:{ zziq -> 0x05ae }
            com.google.android.gms.internal.firebase_auth.zzlf.zza((java.lang.Object) r2, (long) r4, (double) r7)     // Catch:{ zziq -> 0x05ae }
            r1.zzb(r2, (int) r6)     // Catch:{ zziq -> 0x05ae }
            goto L_0x0011
        L_0x0591:
            boolean r4 = r12.zza(r14, (com.google.android.gms.internal.firebase_auth.zzke) r0)     // Catch:{ zziq -> 0x05ae }
            if (r4 != 0) goto L_0x0011
            int r0 = r1.zzm
        L_0x0599:
            int r3 = r1.zzn
            if (r0 >= r3) goto L_0x05a8
            int[] r3 = r1.zzl
            r3 = r3[r0]
            java.lang.Object r14 = r1.zza((java.lang.Object) r2, (int) r3, r14, r12)
            int r0 = r0 + 1
            goto L_0x0599
        L_0x05a8:
            if (r14 == 0) goto L_0x05ad
            r12.zzb((java.lang.Object) r2, r14)
        L_0x05ad:
            return
        L_0x05ae:
            r12.zza((com.google.android.gms.internal.firebase_auth.zzke) r0)     // Catch:{ all -> 0x05d5 }
            if (r14 != 0) goto L_0x05b8
            java.lang.Object r4 = r12.zzc(r2)     // Catch:{ all -> 0x05d5 }
            r14 = r4
        L_0x05b8:
            boolean r4 = r12.zza(r14, (com.google.android.gms.internal.firebase_auth.zzke) r0)     // Catch:{ all -> 0x05d5 }
            if (r4 != 0) goto L_0x0011
            int r0 = r1.zzm
        L_0x05c0:
            int r3 = r1.zzn
            if (r0 >= r3) goto L_0x05cf
            int[] r3 = r1.zzl
            r3 = r3[r0]
            java.lang.Object r14 = r1.zza((java.lang.Object) r2, (int) r3, r14, r12)
            int r0 = r0 + 1
            goto L_0x05c0
        L_0x05cf:
            if (r14 == 0) goto L_0x05d4
            r12.zzb((java.lang.Object) r2, r14)
        L_0x05d4:
            return
        L_0x05d5:
            r0 = move-exception
            int r3 = r1.zzm
        L_0x05d8:
            int r4 = r1.zzn
            if (r3 >= r4) goto L_0x05e7
            int[] r4 = r1.zzl
            r4 = r4[r3]
            java.lang.Object r14 = r1.zza((java.lang.Object) r2, (int) r4, r14, r12)
            int r3 = r3 + 1
            goto L_0x05d8
        L_0x05e7:
            if (r14 == 0) goto L_0x05ec
            r12.zzb((java.lang.Object) r2, r14)
        L_0x05ec:
            throw r0
        L_0x05ed:
            throw r11
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.firebase_auth.zzjv.zza(java.lang.Object, com.google.android.gms.internal.firebase_auth.zzke, com.google.android.gms.internal.firebase_auth.zzht):void");
    }

    private final zzkh zza(int i) {
        int i2 = (i / 3) << 1;
        zzkh zzkh = (zzkh) this.zzd[i2];
        if (zzkh != null) {
            return zzkh;
        }
        zzkh zza2 = zzkd.zza().zza((Class) this.zzd[i2 + 1]);
        this.zzd[i2] = zza2;
        return zza2;
    }

    private final Object zzb(int i) {
        return this.zzd[(i / 3) << 1];
    }

    private final zzin zzc(int i) {
        return (zzin) this.zzd[((i / 3) << 1) + 1];
    }

    public final void zzb(T t) {
        int i;
        int i2 = this.zzm;
        while (true) {
            i = this.zzn;
            if (i2 >= i) {
                break;
            }
            long zzd2 = (long) (zzd(this.zzl[i2]) & 1048575);
            Object zzf2 = zzlf.zzf(t, zzd2);
            if (zzf2 != null) {
                zzlf.zza((Object) t, zzd2, this.zzs.zzd(zzf2));
            }
            i2++;
        }
        int length = this.zzl.length;
        while (i < length) {
            this.zzp.zzb(t, (long) this.zzl[i]);
            i++;
        }
        this.zzq.zzd(t);
        if (this.zzh) {
            this.zzr.zzc(t);
        }
    }

    private final <UT, UB> UB zza(Object obj, int i, UB ub, zzkz<UT, UB> zzkz) {
        zzin zzc2;
        int i2 = this.zzc[i];
        Object zzf2 = zzlf.zzf(obj, (long) (zzd(i) & 1048575));
        if (zzf2 == null || (zzc2 = zzc(i)) == null) {
            return ub;
        }
        return zza(i, i2, this.zzs.zza(zzf2), zzc2, ub, zzkz);
    }

    private final <K, V, UT, UB> UB zza(int i, int i2, Map<K, V> map, zzin zzin, UB ub, zzkz<UT, UB> zzkz) {
        zzji<?, ?> zzf2 = this.zzs.zzf(zzb(i));
        Iterator<Map.Entry<K, V>> it = map.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry next = it.next();
            if (!zzin.zza(((Integer) next.getValue()).intValue())) {
                if (ub == null) {
                    ub = zzkz.zza();
                }
                zzhd zzc2 = zzgv.zzc(zzjj.zza(zzf2, next.getKey(), next.getValue()));
                try {
                    zzjj.zza(zzc2.zzb(), zzf2, next.getKey(), next.getValue());
                    zzkz.zza(ub, i2, zzc2.zza());
                    it.remove();
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
        }
        return ub;
    }

    public final boolean zzc(T t) {
        int i;
        int i2;
        T t2 = t;
        int i3 = 1048575;
        int i4 = 0;
        int i5 = 0;
        while (true) {
            boolean z = true;
            if (i5 >= this.zzm) {
                return !this.zzh || this.zzr.zza((Object) t2).zzf();
            }
            int i6 = this.zzl[i5];
            int i7 = this.zzc[i6];
            int zzd2 = zzd(i6);
            int i8 = this.zzc[i6 + 2];
            int i9 = i8 & 1048575;
            int i10 = 1 << (i8 >>> 20);
            if (i9 != i3) {
                if (i9 != 1048575) {
                    i4 = zzb.getInt(t2, (long) i9);
                }
                i = i4;
                i2 = i9;
            } else {
                i2 = i3;
                i = i4;
            }
            if (((268435456 & zzd2) != 0) && !zza(t, i6, i2, i, i10)) {
                return false;
            }
            int i11 = (267386880 & zzd2) >>> 20;
            if (i11 != 9 && i11 != 17) {
                if (i11 != 27) {
                    if (i11 == 60 || i11 == 68) {
                        if (zza(t2, i7, i6) && !zza((Object) t2, zzd2, zza(i6))) {
                            return false;
                        }
                    } else if (i11 != 49) {
                        if (i11 != 50) {
                            continue;
                        } else {
                            Map<?, ?> zzb2 = this.zzs.zzb(zzlf.zzf(t2, (long) (zzd2 & 1048575)));
                            if (!zzb2.isEmpty()) {
                                if (this.zzs.zzf(zzb(i6)).zzc.zza() == zzlt.MESSAGE) {
                                    zzkh<?> zzkh = null;
                                    Iterator<?> it = zzb2.values().iterator();
                                    while (true) {
                                        if (!it.hasNext()) {
                                            break;
                                        }
                                        Object next = it.next();
                                        if (zzkh == null) {
                                            zzkh = zzkd.zza().zza(next.getClass());
                                        }
                                        if (!zzkh.zzc(next)) {
                                            z = false;
                                            break;
                                        }
                                    }
                                }
                            }
                            if (!z) {
                                return false;
                            }
                        }
                    }
                }
                List list = (List) zzlf.zzf(t2, (long) (zzd2 & 1048575));
                if (!list.isEmpty()) {
                    zzkh zza2 = zza(i6);
                    int i12 = 0;
                    while (true) {
                        if (i12 >= list.size()) {
                            break;
                        } else if (!zza2.zzc(list.get(i12))) {
                            z = false;
                            break;
                        } else {
                            i12++;
                        }
                    }
                }
                if (!z) {
                    return false;
                }
            } else if (zza(t, i6, i2, i, i10) && !zza((Object) t2, zzd2, zza(i6))) {
                return false;
            }
            i5++;
            i3 = i2;
            i4 = i;
        }
    }

    private static boolean zza(Object obj, int i, zzkh zzkh) {
        return zzkh.zzc(zzlf.zzf(obj, (long) (i & 1048575)));
    }

    private static void zza(int i, Object obj, zzlw zzlw) throws IOException {
        if (obj instanceof String) {
            zzlw.zza(i, (String) obj);
        } else {
            zzlw.zza(i, (zzgv) obj);
        }
    }

    private final void zza(Object obj, int i, zzke zzke) throws IOException {
        if (zzf(i)) {
            zzlf.zza(obj, (long) (i & 1048575), (Object) zzke.zzm());
        } else if (this.zzi) {
            zzlf.zza(obj, (long) (i & 1048575), (Object) zzke.zzl());
        } else {
            zzlf.zza(obj, (long) (i & 1048575), (Object) zzke.zzn());
        }
    }

    private final int zzd(int i) {
        return this.zzc[i + 1];
    }

    private final int zze(int i) {
        return this.zzc[i + 2];
    }

    private static <T> double zzb(T t, long j) {
        return ((Double) zzlf.zzf(t, j)).doubleValue();
    }

    private static <T> float zzc(T t, long j) {
        return ((Float) zzlf.zzf(t, j)).floatValue();
    }

    private static <T> int zzd(T t, long j) {
        return ((Integer) zzlf.zzf(t, j)).intValue();
    }

    private static <T> long zze(T t, long j) {
        return ((Long) zzlf.zzf(t, j)).longValue();
    }

    private static <T> boolean zzf(T t, long j) {
        return ((Boolean) zzlf.zzf(t, j)).booleanValue();
    }

    private final boolean zzc(T t, T t2, int i) {
        return zza(t, i) == zza(t2, i);
    }

    private final boolean zza(T t, int i, int i2, int i3, int i4) {
        if (i2 == 1048575) {
            return zza(t, i);
        }
        return (i3 & i4) != 0;
    }

    private final boolean zza(T t, int i) {
        int zze2 = zze(i);
        long j = (long) (zze2 & 1048575);
        if (j == 1048575) {
            int zzd2 = zzd(i);
            long j2 = (long) (zzd2 & 1048575);
            switch ((zzd2 & 267386880) >>> 20) {
                case 0:
                    return zzlf.zze(t, j2) != 0.0d;
                case 1:
                    return zzlf.zzd(t, j2) != 0.0f;
                case 2:
                    return zzlf.zzb(t, j2) != 0;
                case 3:
                    return zzlf.zzb(t, j2) != 0;
                case 4:
                    return zzlf.zza((Object) t, j2) != 0;
                case 5:
                    return zzlf.zzb(t, j2) != 0;
                case 6:
                    return zzlf.zza((Object) t, j2) != 0;
                case 7:
                    return zzlf.zzc(t, j2);
                case 8:
                    Object zzf2 = zzlf.zzf(t, j2);
                    if (zzf2 instanceof String) {
                        return !((String) zzf2).isEmpty();
                    }
                    if (zzf2 instanceof zzgv) {
                        return !zzgv.zza.equals(zzf2);
                    }
                    throw new IllegalArgumentException();
                case 9:
                    return zzlf.zzf(t, j2) != null;
                case 10:
                    return !zzgv.zza.equals(zzlf.zzf(t, j2));
                case 11:
                    return zzlf.zza((Object) t, j2) != 0;
                case 12:
                    return zzlf.zza((Object) t, j2) != 0;
                case 13:
                    return zzlf.zza((Object) t, j2) != 0;
                case 14:
                    return zzlf.zzb(t, j2) != 0;
                case 15:
                    return zzlf.zza((Object) t, j2) != 0;
                case 16:
                    return zzlf.zzb(t, j2) != 0;
                case 17:
                    return zzlf.zzf(t, j2) != null;
                default:
                    throw new IllegalArgumentException();
            }
        } else {
            return (zzlf.zza((Object) t, j) & (1 << (zze2 >>> 20))) != 0;
        }
    }

    private final void zzb(T t, int i) {
        int zze2 = zze(i);
        long j = (long) (1048575 & zze2);
        if (j != 1048575) {
            zzlf.zza((Object) t, j, (1 << (zze2 >>> 20)) | zzlf.zza((Object) t, j));
        }
    }

    private final boolean zza(T t, int i, int i2) {
        return zzlf.zza((Object) t, (long) (zze(i2) & 1048575)) == i;
    }

    private final void zzb(T t, int i, int i2) {
        zzlf.zza((Object) t, (long) (zze(i2) & 1048575), i);
    }
}
