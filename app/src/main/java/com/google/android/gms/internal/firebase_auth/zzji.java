package com.google.android.gms.internal.firebase_auth;

/* compiled from: com.google.firebase:firebase-auth@@19.4.0 */
final class zzji<K, V> {
    public final zzlq zza;
    public final K zzb;
    public final zzlq zzc;
    public final V zzd;
}
