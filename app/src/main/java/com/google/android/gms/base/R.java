package com.google.android.gms.base;

public final class R {

    public static final class attr {
        public static final int buttonSize = 2130968691;
        public static final int circleCrop = 2130968736;
        public static final int colorScheme = 2130968772;
        public static final int imageAspectRatio = 2130969015;
        public static final int imageAspectRatioAdjust = 2130969016;
        public static final int scopeUris = 2130969264;

        private attr() {
        }
    }

    public static final class color {
        public static final int common_google_signin_btn_text_dark = 2131099739;
        public static final int common_google_signin_btn_text_dark_default = 2131099740;
        public static final int common_google_signin_btn_text_dark_disabled = 2131099741;
        public static final int common_google_signin_btn_text_dark_focused = 2131099742;
        public static final int common_google_signin_btn_text_dark_pressed = 2131099743;
        public static final int common_google_signin_btn_text_light = 2131099744;
        public static final int common_google_signin_btn_text_light_default = 2131099745;
        public static final int common_google_signin_btn_text_light_disabled = 2131099746;
        public static final int common_google_signin_btn_text_light_focused = 2131099747;
        public static final int common_google_signin_btn_text_light_pressed = 2131099748;
        public static final int common_google_signin_btn_tint = 2131099749;

        private color() {
        }
    }

    public static final class drawable {
        public static final int common_full_open_on_phone = 2131230883;
        public static final int common_google_signin_btn_icon_dark = 2131230884;
        public static final int common_google_signin_btn_icon_dark_focused = 2131230885;
        public static final int common_google_signin_btn_icon_dark_normal = 2131230886;
        public static final int common_google_signin_btn_icon_dark_normal_background = 2131230887;
        public static final int common_google_signin_btn_icon_disabled = 2131230888;
        public static final int common_google_signin_btn_icon_light = 2131230889;
        public static final int common_google_signin_btn_icon_light_focused = 2131230890;
        public static final int common_google_signin_btn_icon_light_normal = 2131230891;
        public static final int common_google_signin_btn_icon_light_normal_background = 2131230892;
        public static final int common_google_signin_btn_text_dark = 2131230893;
        public static final int common_google_signin_btn_text_dark_focused = 2131230894;
        public static final int common_google_signin_btn_text_dark_normal = 2131230895;
        public static final int common_google_signin_btn_text_dark_normal_background = 2131230896;
        public static final int common_google_signin_btn_text_disabled = 2131230897;
        public static final int common_google_signin_btn_text_light = 2131230898;
        public static final int common_google_signin_btn_text_light_focused = 2131230899;
        public static final int common_google_signin_btn_text_light_normal = 2131230900;
        public static final int common_google_signin_btn_text_light_normal_background = 2131230901;
        public static final int googleg_disabled_color_18 = 2131230914;
        public static final int googleg_standard_color_18 = 2131230915;

        private drawable() {
        }
    }

    public static final class id {
        public static final int adjust_height = 2131296333;
        public static final int adjust_width = 2131296334;
        public static final int auto = 2131296345;
        public static final int dark = 2131296454;
        public static final int icon_only = 2131296568;
        public static final int light = 2131296662;
        public static final int none = 2131296745;
        public static final int standard = 2131296910;
        public static final int wide = 2131297129;

        private id() {
        }
    }

    public static final class string {
        public static final int common_google_play_services_enable_button = 2131820679;
        public static final int common_google_play_services_enable_text = 2131820680;
        public static final int common_google_play_services_enable_title = 2131820681;
        public static final int common_google_play_services_install_button = 2131820682;
        public static final int common_google_play_services_install_text = 2131820683;
        public static final int common_google_play_services_install_title = 2131820684;
        public static final int common_google_play_services_notification_channel_name = 2131820685;
        public static final int common_google_play_services_notification_ticker = 2131820686;
        public static final int common_google_play_services_unsupported_text = 2131820688;
        public static final int common_google_play_services_update_button = 2131820689;
        public static final int common_google_play_services_update_text = 2131820690;
        public static final int common_google_play_services_update_title = 2131820691;
        public static final int common_google_play_services_updating_text = 2131820692;
        public static final int common_google_play_services_wear_update_text = 2131820693;
        public static final int common_open_on_phone = 2131820694;
        public static final int common_signin_button_text = 2131820695;
        public static final int common_signin_button_text_long = 2131820696;

        private string() {
        }
    }

    public static final class styleable {
        public static final int[] LoadingImageView = {com.store.proshop.R.attr.circleCrop, com.store.proshop.R.attr.imageAspectRatio, com.store.proshop.R.attr.imageAspectRatioAdjust};
        public static final int LoadingImageView_circleCrop = 0;
        public static final int LoadingImageView_imageAspectRatio = 1;
        public static final int LoadingImageView_imageAspectRatioAdjust = 2;
        public static final int[] SignInButton = {com.store.proshop.R.attr.buttonSize, com.store.proshop.R.attr.colorScheme, com.store.proshop.R.attr.scopeUris};
        public static final int SignInButton_buttonSize = 0;
        public static final int SignInButton_colorScheme = 1;
        public static final int SignInButton_scopeUris = 2;

        private styleable() {
        }
    }

    private R() {
    }
}
