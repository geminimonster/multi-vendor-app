package com.google.android.gms.internal.firebase_auth;

import com.bumptech.glide.load.Key;
import java.nio.charset.Charset;
import org.bouncycastle.i18n.LocalizedMessage;

/* compiled from: com.google.firebase:firebase-auth@@19.4.0 */
public final class zzam {
    public static final Charset zza = Charset.forName(Key.STRING_CHARSET_NAME);
    private static final Charset zzb = Charset.forName("US-ASCII");
    private static final Charset zzc = Charset.forName(LocalizedMessage.DEFAULT_ENCODING);
    private static final Charset zzd = Charset.forName("UTF-16BE");
    private static final Charset zze = Charset.forName("UTF-16LE");
    private static final Charset zzf = Charset.forName("UTF-16");
}
