package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zztf;

/* compiled from: com.google.android.gms:play-services-ads@@19.1.0 */
public final class zzcev implements zzegz<zztf.zza.C0038zza> {
    public static zzcev zzams() {
        return zzceu.zzfwz;
    }

    public final /* synthetic */ Object get() {
        return (zztf.zza.C0038zza) zzehf.zza(zztf.zza.C0038zza.REWARD_BASED_VIDEO_AD, "Cannot return null from a non-@Nullable @Provides method");
    }
}
