package com.google.android.gms.internal.firebase_auth;

import java.util.concurrent.ExecutorService;

/* compiled from: com.google.firebase:firebase-auth@@19.4.0 */
public interface zzg {
    ExecutorService zza(int i);

    ExecutorService zza(int i, int i2);
}
