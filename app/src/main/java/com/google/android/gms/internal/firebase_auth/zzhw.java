package com.google.android.gms.internal.firebase_auth;

/* compiled from: com.google.firebase:firebase-auth@@19.4.0 */
final class zzhw {
    private static final zzhv<?> zza = new zzhx();
    private static final zzhv<?> zzb = zzc();

    private static zzhv<?> zzc() {
        try {
            return (zzhv) Class.forName("com.google.protobuf.ExtensionSchemaFull").getDeclaredConstructor(new Class[0]).newInstance(new Object[0]);
        } catch (Exception unused) {
            return null;
        }
    }

    static zzhv<?> zza() {
        return zza;
    }

    static zzhv<?> zzb() {
        zzhv<?> zzhv = zzb;
        if (zzhv != null) {
            return zzhv;
        }
        throw new IllegalStateException("Protobuf runtime is not correctly loaded.");
    }
}
