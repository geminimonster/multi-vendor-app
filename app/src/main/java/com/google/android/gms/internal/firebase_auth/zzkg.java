package com.google.android.gms.internal.firebase_auth;

/* compiled from: com.google.firebase:firebase-auth@@19.4.0 */
interface zzkg {
    <T> zzkh<T> zza(Class<T> cls);
}
