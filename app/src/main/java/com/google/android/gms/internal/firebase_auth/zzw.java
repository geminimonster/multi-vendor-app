package com.google.android.gms.internal.firebase_auth;

/* compiled from: com.google.firebase:firebase-auth@@19.4.0 */
final class zzw implements zzin {
    static final zzin zza = new zzw();

    private zzw() {
    }

    public final boolean zza(int i) {
        return zzv.zza(i) != null;
    }
}
