package com.google.android.gms.internal.firebase_auth;

/* compiled from: com.google.firebase:firebase-auth@@19.4.0 */
final class zzbi<E> extends zzbf<E> {
    private final zzbj<E> zza;

    zzbi(zzbj<E> zzbj, int i) {
        super(zzbj.size(), i);
        this.zza = zzbj;
    }

    /* access modifiers changed from: protected */
    public final E zza(int i) {
        return this.zza.get(i);
    }
}
