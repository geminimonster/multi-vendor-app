package com.google.android.gms.internal.firebase_auth;

/* compiled from: com.google.firebase:firebase-auth@@19.4.0 */
final /* synthetic */ class zzad {
    static final /* synthetic */ int[] zza;

    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x000f */
    static {
        /*
            int[] r0 = com.google.android.gms.internal.firebase_auth.zzag.zza()
            int r0 = r0.length
            int[] r0 = new int[r0]
            zza = r0
            r1 = 1
            int r2 = com.google.android.gms.internal.firebase_auth.zzag.zzc     // Catch:{ NoSuchFieldError -> 0x000f }
            int r2 = r2 - r1
            r0[r2] = r1     // Catch:{ NoSuchFieldError -> 0x000f }
        L_0x000f:
            int[] r0 = zza     // Catch:{ NoSuchFieldError -> 0x0017 }
            int r2 = com.google.android.gms.internal.firebase_auth.zzag.zza     // Catch:{ NoSuchFieldError -> 0x0017 }
            int r2 = r2 - r1
            r1 = 2
            r0[r2] = r1     // Catch:{ NoSuchFieldError -> 0x0017 }
        L_0x0017:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.firebase_auth.zzad.<clinit>():void");
    }
}
