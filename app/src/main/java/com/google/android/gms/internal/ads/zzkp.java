package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@19.1.0 */
final class zzkp {
    /* access modifiers changed from: private */
    public final int id;
    /* access modifiers changed from: private */
    public final int zzagn;
    /* access modifiers changed from: private */
    public final long zzcw;

    public zzkp(int i, long j, int i2) {
        this.id = i;
        this.zzcw = j;
        this.zzagn = i2;
    }
}
