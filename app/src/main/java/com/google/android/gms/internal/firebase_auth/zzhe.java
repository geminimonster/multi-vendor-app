package com.google.android.gms.internal.firebase_auth;

import java.util.Iterator;

/* compiled from: com.google.firebase:firebase-auth@@19.4.0 */
public interface zzhe extends Iterator<Byte> {
    byte zza();
}
