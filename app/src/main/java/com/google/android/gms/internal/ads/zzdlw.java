package com.google.android.gms.internal.ads;

import android.content.Context;
import com.stripe.android.StripePaymentController;

/* compiled from: com.google.android.gms:play-services-gass@@19.1.0 */
public final class zzdlw {
    public static zzdng zza(Context context, int i, zzgb zzgb, String str, String str2, String str3, zzdlk zzdlk) {
        return new zzdlv(context, 1, zzgb, str, str2, str3, zzdlk).zzdy(StripePaymentController.PAYMENT_REQUEST_CODE);
    }
}
