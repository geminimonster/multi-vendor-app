package com.google.android.gms.ads.impl;

public final class R {

    public static final class string {
        public static final int s1 = 2131821100;
        public static final int s2 = 2131821101;
        public static final int s3 = 2131821102;
        public static final int s4 = 2131821103;
        public static final int s5 = 2131821104;
        public static final int s6 = 2131821105;
        public static final int s7 = 2131821106;

        private string() {
        }
    }

    private R() {
    }
}
