package com.google.android.gms.internal.firebase_auth;

/* compiled from: com.google.firebase:firebase-auth@@19.4.0 */
public class zziw {
    private static final zzht zza = zzht.zza();
    private zzgv zzb;
    private volatile zzjr zzc;
    private volatile zzgv zzd;

    public int hashCode() {
        return 1;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof zziw)) {
            return false;
        }
        zziw zziw = (zziw) obj;
        zzjr zzjr = this.zzc;
        zzjr zzjr2 = zziw.zzc;
        if (zzjr == null && zzjr2 == null) {
            return zzc().equals(zziw.zzc());
        }
        if (zzjr != null && zzjr2 != null) {
            return zzjr.equals(zzjr2);
        }
        if (zzjr != null) {
            return zzjr.equals(zziw.zzb(zzjr.zzag()));
        }
        return zzb(zzjr2.zzag()).equals(zzjr2);
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(6:7|8|9|10|11|12) */
    /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0012 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final com.google.android.gms.internal.firebase_auth.zzjr zzb(com.google.android.gms.internal.firebase_auth.zzjr r2) {
        /*
            r1 = this;
            com.google.android.gms.internal.firebase_auth.zzjr r0 = r1.zzc
            if (r0 != 0) goto L_0x001d
            monitor-enter(r1)
            com.google.android.gms.internal.firebase_auth.zzjr r0 = r1.zzc     // Catch:{ all -> 0x001a }
            if (r0 == 0) goto L_0x000b
            monitor-exit(r1)     // Catch:{ all -> 0x001a }
            goto L_0x001d
        L_0x000b:
            r1.zzc = r2     // Catch:{ zzir -> 0x0012 }
            com.google.android.gms.internal.firebase_auth.zzgv r0 = com.google.android.gms.internal.firebase_auth.zzgv.zza     // Catch:{ zzir -> 0x0012 }
            r1.zzd = r0     // Catch:{ zzir -> 0x0012 }
            goto L_0x0018
        L_0x0012:
            r1.zzc = r2     // Catch:{ all -> 0x001a }
            com.google.android.gms.internal.firebase_auth.zzgv r2 = com.google.android.gms.internal.firebase_auth.zzgv.zza     // Catch:{ all -> 0x001a }
            r1.zzd = r2     // Catch:{ all -> 0x001a }
        L_0x0018:
            monitor-exit(r1)     // Catch:{ all -> 0x001a }
            goto L_0x001d
        L_0x001a:
            r2 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x001a }
            throw r2
        L_0x001d:
            com.google.android.gms.internal.firebase_auth.zzjr r2 = r1.zzc
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.firebase_auth.zziw.zzb(com.google.android.gms.internal.firebase_auth.zzjr):com.google.android.gms.internal.firebase_auth.zzjr");
    }

    public final zzjr zza(zzjr zzjr) {
        zzjr zzjr2 = this.zzc;
        this.zzb = null;
        this.zzd = null;
        this.zzc = zzjr;
        return zzjr2;
    }

    public final int zzb() {
        if (this.zzd != null) {
            return this.zzd.zza();
        }
        if (this.zzc != null) {
            return this.zzc.zzab();
        }
        return 0;
    }

    public final zzgv zzc() {
        if (this.zzd != null) {
            return this.zzd;
        }
        synchronized (this) {
            if (this.zzd != null) {
                zzgv zzgv = this.zzd;
                return zzgv;
            }
            if (this.zzc == null) {
                this.zzd = zzgv.zza;
            } else {
                this.zzd = this.zzc.zzw();
            }
            zzgv zzgv2 = this.zzd;
            return zzgv2;
        }
    }
}
