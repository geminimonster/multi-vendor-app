package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzsv;

/* compiled from: com.google.android.gms:play-services-ads@@19.1.0 */
public final class zzchl {
    public final zzsv.zza.C0037zza zzfyp;
    public final zzsv.zza.C0037zza zzfyq;
    public final zzsv.zza.C0037zza zzfyr;

    public zzchl(zzsv.zza.C0037zza zza, zzsv.zza.C0037zza zza2, zzsv.zza.C0037zza zza3) {
        this.zzfyp = zza;
        this.zzfyr = zza3;
        this.zzfyq = zza2;
    }
}
