package com.google.android.gms.internal.firebase_auth;

/* compiled from: com.google.firebase:firebase-auth@@19.4.0 */
public abstract class zzal {
    public abstract boolean zza();

    public abstract boolean zza(int i);

    public abstract int zzb();

    public abstract int zzc();
}
