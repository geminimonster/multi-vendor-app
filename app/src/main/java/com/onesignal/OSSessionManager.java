package com.onesignal;

import com.onesignal.OneSignal;
import com.onesignal.OneSignalDbContract;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

class OSSessionManager {
    private static final String DIRECT_TAG = "direct";
    private String directNotificationId;
    private JSONArray indirectNotificationIds;
    protected Session session;
    private SessionListener sessionListener;

    interface SessionListener {
        void onSessionEnding(SessionResult sessionResult);
    }

    public static class SessionResult {
        JSONArray notificationIds;
        Session session;

        SessionResult(Builder builder) {
            this.notificationIds = builder.notificationIds;
            this.session = builder.session;
        }

        public static class Builder {
            /* access modifiers changed from: private */
            public JSONArray notificationIds;
            /* access modifiers changed from: private */
            public Session session;

            public static Builder newInstance() {
                return new Builder();
            }

            private Builder() {
            }

            public Builder setNotificationIds(JSONArray jSONArray) {
                this.notificationIds = jSONArray;
                return this;
            }

            public Builder setSession(Session session2) {
                this.session = session2;
                return this;
            }

            public SessionResult build() {
                return new SessionResult(this);
            }
        }
    }

    public enum Session {
        DIRECT,
        INDIRECT,
        UNATTRIBUTED,
        DISABLED;

        public boolean isDirect() {
            return equals(DIRECT);
        }

        public boolean isIndirect() {
            return equals(INDIRECT);
        }

        public boolean isAttributed() {
            return isDirect() || isIndirect();
        }

        public boolean isUnattributed() {
            return equals(UNATTRIBUTED);
        }

        public boolean isDisabled() {
            return equals(DISABLED);
        }

        public static Session fromString(String str) {
            if (str == null || str.isEmpty()) {
                return UNATTRIBUTED;
            }
            for (Session session : values()) {
                if (session.name().equalsIgnoreCase(str)) {
                    return session;
                }
            }
            return UNATTRIBUTED;
        }
    }

    public OSSessionManager(SessionListener sessionListener2) {
        this.sessionListener = sessionListener2;
        initSessionFromCache();
    }

    private void initSessionFromCache() {
        Session cachedSession = OutcomesUtils.getCachedSession();
        this.session = cachedSession;
        if (cachedSession.isIndirect()) {
            this.indirectNotificationIds = getLastNotificationsReceivedIds();
        } else if (this.session.isDirect()) {
            this.directNotificationId = OutcomesUtils.getCachedNotificationOpenId();
        }
    }

    /* access modifiers changed from: package-private */
    public void addSessionNotificationsIds(JSONObject jSONObject) {
        if (!this.session.isUnattributed()) {
            try {
                if (this.session.isDirect()) {
                    jSONObject.put(DIRECT_TAG, true);
                    jSONObject.put(OneSignalDbContract.OutcomeEventsTable.COLUMN_NAME_NOTIFICATION_IDS, new JSONArray().put(this.directNotificationId));
                } else if (this.session.isIndirect()) {
                    jSONObject.put(DIRECT_TAG, false);
                    jSONObject.put(OneSignalDbContract.OutcomeEventsTable.COLUMN_NAME_NOTIFICATION_IDS, this.indirectNotificationIds);
                }
            } catch (JSONException e) {
                OneSignal.Log(OneSignal.LOG_LEVEL.ERROR, "Generating addNotificationId:JSON Failed.", e);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void restartSessionIfNeeded() {
        if (!OneSignal.getAppEntryState().isNotificationClick()) {
            JSONArray lastNotificationsReceivedIds = getLastNotificationsReceivedIds();
            if (lastNotificationsReceivedIds.length() > 0) {
                setSession(Session.INDIRECT, (String) null, lastNotificationsReceivedIds);
            } else {
                setSession(Session.UNATTRIBUTED, (String) null, (JSONArray) null);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public Session getSession() {
        return this.session;
    }

    /* access modifiers changed from: package-private */
    public String getDirectNotificationId() {
        return this.directNotificationId;
    }

    /* access modifiers changed from: package-private */
    public JSONArray getIndirectNotificationIds() {
        return this.indirectNotificationIds;
    }

    /* access modifiers changed from: package-private */
    public void onDirectSessionFromNotificationOpen(String str) {
        setSession(Session.DIRECT, str, (JSONArray) null);
    }

    private void setSession(Session session2, String str, JSONArray jSONArray) {
        if (willChangeSession(session2, str, jSONArray)) {
            OneSignal.LOG_LEVEL log_level = OneSignal.LOG_LEVEL.DEBUG;
            OneSignal.Log(log_level, "OSSession changed\nfrom:\nsession: " + this.session + ", directNotificationId: " + this.directNotificationId + ", indirectNotificationIds: " + this.indirectNotificationIds + "\nto:\nsession: " + session2 + ", directNotificationId: " + str + ", indirectNotificationIds: " + jSONArray);
            OutcomesUtils.cacheCurrentSession(session2);
            OutcomesUtils.cacheNotificationOpenId(str);
            this.sessionListener.onSessionEnding(getSessionResult());
            this.session = session2;
            this.directNotificationId = str;
            this.indirectNotificationIds = jSONArray;
        }
    }

    private boolean willChangeSession(Session session2, String str, JSONArray jSONArray) {
        JSONArray jSONArray2;
        String str2;
        if (!session2.equals(this.session)) {
            return true;
        }
        if (this.session.isDirect() && (str2 = this.directNotificationId) != null && !str2.equals(str)) {
            return true;
        }
        if (!this.session.isIndirect() || (jSONArray2 = this.indirectNotificationIds) == null || jSONArray2.length() <= 0 || JSONUtils.compareJSONArrays(this.indirectNotificationIds, jSONArray)) {
            return false;
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public JSONArray getLastNotificationsReceivedIds() {
        JSONArray lastNotificationsReceivedData = OutcomesUtils.getLastNotificationsReceivedData();
        JSONArray jSONArray = new JSONArray();
        long indirectAttributionWindow = ((long) (OutcomesUtils.getIndirectAttributionWindow() * 60)) * 1000;
        long currentTimeMillis = System.currentTimeMillis();
        for (int i = 0; i < lastNotificationsReceivedData.length(); i++) {
            try {
                JSONObject jSONObject = lastNotificationsReceivedData.getJSONObject(i);
                if (currentTimeMillis - jSONObject.getLong("time") <= indirectAttributionWindow) {
                    jSONArray.put(jSONObject.getString("notification_id"));
                }
            } catch (JSONException e) {
                OneSignal.Log(OneSignal.LOG_LEVEL.ERROR, "From getting notification from array:JSON Failed.", e);
            }
        }
        return jSONArray;
    }

    /* access modifiers changed from: package-private */
    public SessionResult getSessionResult() {
        if (this.session.isDirect()) {
            if (OutcomesUtils.isDirectSessionEnabled()) {
                return SessionResult.Builder.newInstance().setNotificationIds(new JSONArray().put(this.directNotificationId)).setSession(Session.DIRECT).build();
            }
        } else if (this.session.isIndirect()) {
            if (OutcomesUtils.isIndirectSessionEnabled()) {
                return SessionResult.Builder.newInstance().setNotificationIds(this.indirectNotificationIds).setSession(Session.INDIRECT).build();
            }
        } else if (OutcomesUtils.isUnattributedSessionEnabled()) {
            return SessionResult.Builder.newInstance().setSession(Session.UNATTRIBUTED).build();
        }
        return SessionResult.Builder.newInstance().setSession(Session.DISABLED).build();
    }

    /* access modifiers changed from: package-private */
    public SessionResult getIAMSessionResult() {
        if (OutcomesUtils.isUnattributedSessionEnabled()) {
            return SessionResult.Builder.newInstance().setSession(Session.UNATTRIBUTED).build();
        }
        return SessionResult.Builder.newInstance().setSession(Session.DISABLED).build();
    }

    /* access modifiers changed from: package-private */
    public void attemptSessionUpgrade() {
        if (OneSignal.getAppEntryState().isNotificationClick()) {
            setSession(Session.DIRECT, this.directNotificationId, (JSONArray) null);
        } else if (this.session.isUnattributed()) {
            JSONArray lastNotificationsReceivedIds = getLastNotificationsReceivedIds();
            if (lastNotificationsReceivedIds.length() > 0 && OneSignal.getAppEntryState().isAppOpen()) {
                setSession(Session.INDIRECT, (String) null, lastNotificationsReceivedIds);
            }
        }
    }
}
