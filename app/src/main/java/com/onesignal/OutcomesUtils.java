package com.onesignal;

import android.os.Build;
import com.onesignal.OSSessionManager;
import com.onesignal.OneSignal;
import com.onesignal.OneSignalRemoteParams;
import okhttp3.HttpUrl;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

class OutcomesUtils {
    static final String NOTIFICATIONS_IDS = "notification_ids";
    static final String NOTIFICATION_ID = "notification_id";
    static final String TIME = "time";

    OutcomesUtils() {
    }

    static void cacheCurrentSession(OSSessionManager.Session session) {
        OneSignalPrefs.saveString(OneSignalPrefs.PREFS_ONESIGNAL, OneSignalPrefs.PREFS_OS_OUTCOMES_CURRENT_SESSION, session.toString());
    }

    static OSSessionManager.Session getCachedSession() {
        return OSSessionManager.Session.fromString(OneSignalPrefs.getString(OneSignalPrefs.PREFS_ONESIGNAL, OneSignalPrefs.PREFS_OS_OUTCOMES_CURRENT_SESSION, OSSessionManager.Session.UNATTRIBUTED.toString()));
    }

    static void cacheNotificationOpenId(String str) {
        OneSignalPrefs.saveString(OneSignalPrefs.PREFS_ONESIGNAL, OneSignalPrefs.PREFS_OS_LAST_ATTRIBUTED_NOTIFICATION_OPEN, str);
    }

    static String getCachedNotificationOpenId() {
        return OneSignalPrefs.getString(OneSignalPrefs.PREFS_ONESIGNAL, OneSignalPrefs.PREFS_OS_LAST_ATTRIBUTED_NOTIFICATION_OPEN, (String) null);
    }

    static void markLastNotificationReceived(String str) {
        OneSignal.LOG_LEVEL log_level = OneSignal.LOG_LEVEL.DEBUG;
        OneSignal.Log(log_level, "Notification markLastNotificationReceived with id: " + str);
        if (str != null && !str.isEmpty()) {
            try {
                JSONArray jSONArray = new JSONArray(OneSignalPrefs.getString(OneSignalPrefs.PREFS_ONESIGNAL, OneSignalPrefs.PREFS_OS_LAST_NOTIFICATIONS_RECEIVED, HttpUrl.PATH_SEGMENT_ENCODE_SET_URI));
                jSONArray.put(new JSONObject().put("notification_id", str).put(TIME, System.currentTimeMillis()));
                int notificationLimit = getNotificationLimit();
                if (jSONArray.length() > notificationLimit) {
                    int length = jSONArray.length() - notificationLimit;
                    if (Build.VERSION.SDK_INT >= 19) {
                        for (int i = 0; i < length; i++) {
                            jSONArray.remove(i);
                        }
                    } else {
                        JSONArray jSONArray2 = new JSONArray();
                        while (length < jSONArray.length()) {
                            jSONArray2.put(jSONArray.get(length));
                            length++;
                        }
                        jSONArray = jSONArray2;
                    }
                }
                OneSignalPrefs.saveString(OneSignalPrefs.PREFS_ONESIGNAL, OneSignalPrefs.PREFS_OS_LAST_NOTIFICATIONS_RECEIVED, jSONArray.toString());
            } catch (JSONException e) {
                OneSignal.Log(OneSignal.LOG_LEVEL.ERROR, "Generating direct notification arrived:JSON Failed.", e);
            }
        }
    }

    static JSONArray getLastNotificationsReceivedData() {
        try {
            return new JSONArray(OneSignalPrefs.getString(OneSignalPrefs.PREFS_ONESIGNAL, OneSignalPrefs.PREFS_OS_LAST_NOTIFICATIONS_RECEIVED, HttpUrl.PATH_SEGMENT_ENCODE_SET_URI));
        } catch (JSONException e) {
            OneSignal.Log(OneSignal.LOG_LEVEL.ERROR, "Generating last notifications received data:JSON Failed.", e);
            return new JSONArray();
        }
    }

    static int getNotificationLimit() {
        return OneSignalPrefs.getInt(OneSignalPrefs.PREFS_ONESIGNAL, OneSignalPrefs.PREFS_OS_NOTIFICATION_LIMIT, 10);
    }

    static int getIndirectAttributionWindow() {
        return OneSignalPrefs.getInt(OneSignalPrefs.PREFS_ONESIGNAL, OneSignalPrefs.PREFS_OS_INDIRECT_ATTRIBUTION_WINDOW, 1440);
    }

    static boolean isDirectSessionEnabled() {
        return OneSignalPrefs.getBool(OneSignalPrefs.PREFS_ONESIGNAL, OneSignalPrefs.PREFS_OS_DIRECT_ENABLED, false);
    }

    static boolean isIndirectSessionEnabled() {
        return OneSignalPrefs.getBool(OneSignalPrefs.PREFS_ONESIGNAL, OneSignalPrefs.PREFS_OS_INDIRECT_ENABLED, false);
    }

    static boolean isUnattributedSessionEnabled() {
        return OneSignalPrefs.getBool(OneSignalPrefs.PREFS_ONESIGNAL, OneSignalPrefs.PREFS_OS_UNATTRIBUTED_ENABLED, false);
    }

    static void saveOutcomesParams(OneSignalRemoteParams.OutcomesParams outcomesParams) {
        OneSignalPrefs.saveBool(OneSignalPrefs.PREFS_ONESIGNAL, OneSignalPrefs.PREFS_OS_DIRECT_ENABLED, outcomesParams.directEnabled);
        OneSignalPrefs.saveBool(OneSignalPrefs.PREFS_ONESIGNAL, OneSignalPrefs.PREFS_OS_INDIRECT_ENABLED, outcomesParams.indirectEnabled);
        OneSignalPrefs.saveBool(OneSignalPrefs.PREFS_ONESIGNAL, OneSignalPrefs.PREFS_OS_UNATTRIBUTED_ENABLED, outcomesParams.unattributedEnabled);
        OneSignalPrefs.saveInt(OneSignalPrefs.PREFS_ONESIGNAL, OneSignalPrefs.PREFS_OS_NOTIFICATION_LIMIT, outcomesParams.notificationLimit);
        OneSignalPrefs.saveInt(OneSignalPrefs.PREFS_ONESIGNAL, OneSignalPrefs.PREFS_OS_INDIRECT_ATTRIBUTION_WINDOW, outcomesParams.indirectAttributionWindow);
    }
}
