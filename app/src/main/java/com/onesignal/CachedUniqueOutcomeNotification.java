package com.onesignal;

class CachedUniqueOutcomeNotification {
    private String name;
    private String notificationId;

    CachedUniqueOutcomeNotification(String str, String str2) {
        this.notificationId = str;
        this.name = str2;
    }

    /* access modifiers changed from: package-private */
    public String getNotificationId() {
        return this.notificationId;
    }

    /* access modifiers changed from: package-private */
    public String getName() {
        return this.name;
    }
}
