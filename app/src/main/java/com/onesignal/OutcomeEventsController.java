package com.onesignal;

import com.onesignal.OSSessionManager;
import com.onesignal.OneSignal;
import com.onesignal.OneSignalRestClient;
import java.util.Set;
import org.json.JSONArray;

class OutcomeEventsController {
    private static final String OS_SAVE_OUTCOMES = "OS_SAVE_OUTCOMES";
    private static final String OS_SAVE_UNIQUE_OUTCOME_NOTIFICATIONS = "OS_SAVE_UNIQUE_OUTCOME_NOTIFICATIONS";
    private static final String OS_SEND_SAVED_OUTCOMES = "OS_SEND_SAVED_OUTCOMES";
    private final OSSessionManager osSessionManager;
    /* access modifiers changed from: private */
    public final OutcomeEventsRepository outcomeEventsRepository;
    private Set<String> unattributedUniqueOutcomeEventsSentSet;

    public OutcomeEventsController(OSSessionManager oSSessionManager, OutcomeEventsRepository outcomeEventsRepository2) {
        this.osSessionManager = oSSessionManager;
        this.outcomeEventsRepository = outcomeEventsRepository2;
        initUniqueOutcomeEventsSentSets();
    }

    OutcomeEventsController(OSSessionManager oSSessionManager, OneSignalDbHelper oneSignalDbHelper) {
        this.outcomeEventsRepository = new OutcomeEventsRepository(oneSignalDbHelper);
        this.osSessionManager = oSSessionManager;
        initUniqueOutcomeEventsSentSets();
    }

    private void initUniqueOutcomeEventsSentSets() {
        this.unattributedUniqueOutcomeEventsSentSet = OSUtils.newConcurrentSet();
        Set<String> stringSet = OneSignalPrefs.getStringSet(OneSignalPrefs.PREFS_ONESIGNAL, OneSignalPrefs.PREFS_OS_UNATTRIBUTED_UNIQUE_OUTCOME_EVENTS_SENT, (Set<String>) null);
        if (stringSet != null) {
            this.unattributedUniqueOutcomeEventsSentSet.addAll(stringSet);
        }
    }

    /* access modifiers changed from: package-private */
    public void cleanOutcomes() {
        this.unattributedUniqueOutcomeEventsSentSet = OSUtils.newConcurrentSet();
        saveUnattributedUniqueOutcomeEvents();
    }

    /* access modifiers changed from: package-private */
    public void sendSavedOutcomes() {
        new Thread(new Runnable() {
            public void run() {
                Thread.currentThread().setPriority(10);
                for (OutcomeEvent access$100 : OutcomeEventsController.this.outcomeEventsRepository.getSavedOutcomeEvents()) {
                    OutcomeEventsController.this.sendSavedOutcomeEvent(access$100);
                }
            }
        }, OS_SEND_SAVED_OUTCOMES).start();
    }

    /* access modifiers changed from: package-private */
    public void sendUniqueOutcomeEvent(String str, OneSignal.OutcomeCallback outcomeCallback) {
        sendUniqueOutcomeEvent(str, this.osSessionManager.getSessionResult(), this.osSessionManager.getSession(), outcomeCallback);
    }

    /* access modifiers changed from: package-private */
    public void sendUniqueClickOutcomeEvent(String str) {
        sendUniqueOutcomeEvent(str, this.osSessionManager.getIAMSessionResult(), OSSessionManager.Session.UNATTRIBUTED, (OneSignal.OutcomeCallback) null);
    }

    /* access modifiers changed from: package-private */
    public void sendOutcomeEvent(String str, OneSignal.OutcomeCallback outcomeCallback) {
        OSSessionManager.SessionResult sessionResult = this.osSessionManager.getSessionResult();
        sendAndCreateOutcomeEvent(str, 0.0f, sessionResult.notificationIds, sessionResult.session, outcomeCallback);
    }

    /* access modifiers changed from: package-private */
    public void sendOutcomeEventWithValue(String str, float f, OneSignal.OutcomeCallback outcomeCallback) {
        OSSessionManager.SessionResult sessionResult = this.osSessionManager.getSessionResult();
        sendAndCreateOutcomeEvent(str, f, sessionResult.notificationIds, sessionResult.session, outcomeCallback);
    }

    /* access modifiers changed from: package-private */
    public void sendClickOutcomeEventWithValue(String str, float f) {
        OSSessionManager.SessionResult iAMSessionResult = this.osSessionManager.getIAMSessionResult();
        sendAndCreateOutcomeEvent(str, f, iAMSessionResult.notificationIds, iAMSessionResult.session, (OneSignal.OutcomeCallback) null);
    }

    private void sendUniqueOutcomeEvent(String str, OSSessionManager.SessionResult sessionResult, OSSessionManager.Session session, OneSignal.OutcomeCallback outcomeCallback) {
        OSSessionManager.Session session2 = sessionResult.session;
        JSONArray jSONArray = sessionResult.notificationIds;
        if (session.isAttributed()) {
            JSONArray uniqueNotificationIds = getUniqueNotificationIds(str, jSONArray);
            if (uniqueNotificationIds == null) {
                OneSignal.LOG_LEVEL log_level = OneSignal.LOG_LEVEL.DEBUG;
                OneSignal.Log(log_level, "Measure endpoint will not send because unique outcome already sent for: \nSession: " + this.osSessionManager.getSession().toString() + "\nOutcome name: " + str + "\nnotificationIds: " + jSONArray);
                if (outcomeCallback != null) {
                    outcomeCallback.onSuccess((OutcomeEvent) null);
                    return;
                }
                return;
            }
            sendAndCreateOutcomeEvent(str, 0.0f, uniqueNotificationIds, session2, outcomeCallback);
        } else if (!session.isUnattributed()) {
            OneSignal.Log(OneSignal.LOG_LEVEL.DEBUG, "Unique Outcome for current session is disabled");
        } else if (this.unattributedUniqueOutcomeEventsSentSet.contains(str)) {
            OneSignal.LOG_LEVEL log_level2 = OneSignal.LOG_LEVEL.DEBUG;
            OneSignal.Log(log_level2, "Measure endpoint will not send because unique outcome already sent for: \nSession: " + this.osSessionManager.getSession().toString() + "\nOutcome name: " + str);
            if (outcomeCallback != null) {
                outcomeCallback.onSuccess((OutcomeEvent) null);
            }
        } else {
            this.unattributedUniqueOutcomeEventsSentSet.add(str);
            sendAndCreateOutcomeEvent(str, 0.0f, (JSONArray) null, session2, outcomeCallback);
        }
    }

    /* access modifiers changed from: private */
    public void sendSavedOutcomeEvent(final OutcomeEvent outcomeEvent) {
        sendOutcomeEvent(outcomeEvent, (OneSignalRestClient.ResponseHandler) new OneSignalRestClient.ResponseHandler() {
            /* access modifiers changed from: package-private */
            public void onSuccess(String str) {
                super.onSuccess(str);
                OutcomeEventsController.this.outcomeEventsRepository.removeEvent(outcomeEvent);
            }
        });
    }

    private void sendAndCreateOutcomeEvent(String str, float f, JSONArray jSONArray, OSSessionManager.Session session, OneSignal.OutcomeCallback outcomeCallback) {
        OutcomeEvent outcomeEvent = new OutcomeEvent(session, jSONArray, str, System.currentTimeMillis() / 1000, f);
        final OSSessionManager.Session session2 = session;
        final JSONArray jSONArray2 = jSONArray;
        final String str2 = str;
        final OneSignal.OutcomeCallback outcomeCallback2 = outcomeCallback;
        final OutcomeEvent outcomeEvent2 = outcomeEvent;
        sendOutcomeEvent(outcomeEvent, (OneSignalRestClient.ResponseHandler) new OneSignalRestClient.ResponseHandler() {
            /* access modifiers changed from: package-private */
            public void onSuccess(String str) {
                super.onSuccess(str);
                if (session2.isAttributed()) {
                    OutcomeEventsController.this.saveAttributedUniqueOutcomeNotifications(jSONArray2, str2);
                } else {
                    OutcomeEventsController.this.saveUnattributedUniqueOutcomeEvents();
                }
                OneSignal.OutcomeCallback outcomeCallback = outcomeCallback2;
                if (outcomeCallback != null) {
                    outcomeCallback.onSuccess(outcomeEvent2);
                }
            }

            /* access modifiers changed from: package-private */
            public void onFailure(int i, String str, Throwable th) {
                super.onFailure(i, str, th);
                new Thread(new Runnable() {
                    public void run() {
                        Thread.currentThread().setPriority(10);
                        OutcomeEventsController.this.outcomeEventsRepository.saveOutcomeEvent(outcomeEvent2);
                    }
                }, OutcomeEventsController.OS_SAVE_OUTCOMES).start();
                OneSignal.LOG_LEVEL log_level = OneSignal.LOG_LEVEL.WARN;
                OneSignal.onesignalLog(log_level, "Sending outcome with name: " + str2 + " failed with status code: " + i + " and response: " + str + "\nOutcome event was cached and will be reattempted on app cold start");
                OneSignal.OutcomeCallback outcomeCallback = outcomeCallback2;
                if (outcomeCallback != null) {
                    outcomeCallback.onSuccess((OutcomeEvent) null);
                }
            }
        });
    }

    private void sendOutcomeEvent(OutcomeEvent outcomeEvent, OneSignalRestClient.ResponseHandler responseHandler) {
        String str = OneSignal.appId;
        int deviceType = new OSUtils().getDeviceType();
        int i = AnonymousClass5.$SwitchMap$com$onesignal$OSSessionManager$Session[outcomeEvent.getSession().ordinal()];
        if (i == 1) {
            this.outcomeEventsRepository.requestMeasureDirectOutcomeEvent(str, deviceType, outcomeEvent, responseHandler);
        } else if (i == 2) {
            this.outcomeEventsRepository.requestMeasureIndirectOutcomeEvent(str, deviceType, outcomeEvent, responseHandler);
        } else if (i == 3) {
            this.outcomeEventsRepository.requestMeasureUnattributedOutcomeEvent(str, deviceType, outcomeEvent, responseHandler);
        } else if (i == 4) {
            OneSignal.Log(OneSignal.LOG_LEVEL.VERBOSE, "Outcomes for current session are disabled");
        }
    }

    /* renamed from: com.onesignal.OutcomeEventsController$5  reason: invalid class name */
    static /* synthetic */ class AnonymousClass5 {
        static final /* synthetic */ int[] $SwitchMap$com$onesignal$OSSessionManager$Session;

        /* JADX WARNING: Can't wrap try/catch for region: R(8:0|1|2|3|4|5|6|(3:7|8|10)) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0012 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001d */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x0028 */
        static {
            /*
                com.onesignal.OSSessionManager$Session[] r0 = com.onesignal.OSSessionManager.Session.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                $SwitchMap$com$onesignal$OSSessionManager$Session = r0
                com.onesignal.OSSessionManager$Session r1 = com.onesignal.OSSessionManager.Session.DIRECT     // Catch:{ NoSuchFieldError -> 0x0012 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0012 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0012 }
            L_0x0012:
                int[] r0 = $SwitchMap$com$onesignal$OSSessionManager$Session     // Catch:{ NoSuchFieldError -> 0x001d }
                com.onesignal.OSSessionManager$Session r1 = com.onesignal.OSSessionManager.Session.INDIRECT     // Catch:{ NoSuchFieldError -> 0x001d }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001d }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001d }
            L_0x001d:
                int[] r0 = $SwitchMap$com$onesignal$OSSessionManager$Session     // Catch:{ NoSuchFieldError -> 0x0028 }
                com.onesignal.OSSessionManager$Session r1 = com.onesignal.OSSessionManager.Session.UNATTRIBUTED     // Catch:{ NoSuchFieldError -> 0x0028 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0028 }
                r2 = 3
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0028 }
            L_0x0028:
                int[] r0 = $SwitchMap$com$onesignal$OSSessionManager$Session     // Catch:{ NoSuchFieldError -> 0x0033 }
                com.onesignal.OSSessionManager$Session r1 = com.onesignal.OSSessionManager.Session.DISABLED     // Catch:{ NoSuchFieldError -> 0x0033 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0033 }
                r2 = 4
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0033 }
            L_0x0033:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.onesignal.OutcomeEventsController.AnonymousClass5.<clinit>():void");
        }
    }

    /* access modifiers changed from: private */
    public void saveAttributedUniqueOutcomeNotifications(final JSONArray jSONArray, final String str) {
        new Thread(new Runnable() {
            public void run() {
                Thread.currentThread().setPriority(10);
                OutcomeEventsController.this.outcomeEventsRepository.saveUniqueOutcomeNotifications(jSONArray, str);
            }
        }, OS_SAVE_UNIQUE_OUTCOME_NOTIFICATIONS).start();
    }

    /* access modifiers changed from: private */
    public void saveUnattributedUniqueOutcomeEvents() {
        OneSignalPrefs.saveStringSet(OneSignalPrefs.PREFS_ONESIGNAL, OneSignalPrefs.PREFS_OS_UNATTRIBUTED_UNIQUE_OUTCOME_EVENTS_SENT, this.unattributedUniqueOutcomeEventsSentSet);
    }

    private JSONArray getUniqueNotificationIds(String str, JSONArray jSONArray) {
        JSONArray notCachedUniqueOutcomeNotifications = this.outcomeEventsRepository.getNotCachedUniqueOutcomeNotifications(str, jSONArray);
        if (notCachedUniqueOutcomeNotifications.length() == 0) {
            return null;
        }
        return notCachedUniqueOutcomeNotifications;
    }
}
