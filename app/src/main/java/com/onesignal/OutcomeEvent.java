package com.onesignal;

import com.onesignal.OSSessionManager;
import com.onesignal.OneSignal;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class OutcomeEvent {
    private static final String NOTIFICATION_IDS = "notification_ids";
    private static final String OUTCOME_ID = "id";
    private static final String SESSION = "session";
    private static final String TIMESTAMP = "timestamp";
    private static final String WEIGHT = "weight";
    private String name;
    private JSONArray notificationIds;
    private OSSessionManager.Session session;
    private long timestamp;
    private Float weight;

    public OutcomeEvent(OSSessionManager.Session session2, JSONArray jSONArray, String str, long j, float f) {
        this.session = session2;
        this.notificationIds = jSONArray;
        this.name = str;
        this.timestamp = j;
        this.weight = Float.valueOf(f);
    }

    public OSSessionManager.Session getSession() {
        return this.session;
    }

    public JSONArray getNotificationIds() {
        return this.notificationIds;
    }

    public String getName() {
        return this.name;
    }

    public long getTimestamp() {
        return this.timestamp;
    }

    public float getWeight() {
        return this.weight.floatValue();
    }

    public JSONObject toJSONObject() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("session", this.session);
            jSONObject.put("notification_ids", this.notificationIds);
            jSONObject.put("id", this.name);
            jSONObject.put("timestamp", this.timestamp);
            jSONObject.put("weight", this.weight);
        } catch (JSONException e) {
            OneSignal.Log(OneSignal.LOG_LEVEL.ERROR, "Generating OutcomeEvent toJSONObject ", e);
        }
        return jSONObject;
    }

    /* access modifiers changed from: package-private */
    public JSONObject toJSONObjectForMeasure() {
        JSONObject jSONObject = new JSONObject();
        try {
            if (this.notificationIds != null && this.notificationIds.length() > 0) {
                jSONObject.put("notification_ids", this.notificationIds);
            }
            jSONObject.put("id", this.name);
            if (this.weight.floatValue() > 0.0f) {
                jSONObject.put("weight", this.weight);
            }
        } catch (JSONException e) {
            OneSignal.Log(OneSignal.LOG_LEVEL.ERROR, "Generating OutcomeEvent toJSONObject ", e);
        }
        return jSONObject;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        OutcomeEvent outcomeEvent = (OutcomeEvent) obj;
        if (!this.session.equals(outcomeEvent.session) || !this.notificationIds.equals(outcomeEvent.notificationIds) || !this.name.equals(outcomeEvent.name) || this.timestamp != outcomeEvent.timestamp || !this.weight.equals(outcomeEvent.weight)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int i;
        int i2 = 1;
        Object[] objArr = {this.session, this.notificationIds, this.name, Long.valueOf(this.timestamp), this.weight};
        for (int i3 = 0; i3 < 5; i3++) {
            Object obj = objArr[i3];
            int i4 = i2 * 31;
            if (obj == null) {
                i = 0;
            } else {
                i = obj.hashCode();
            }
            i2 = i4 + i;
        }
        return i2;
    }

    public String toString() {
        return "OutcomeEvent{session=" + this.session + ", notificationIds=" + this.notificationIds + ", name='" + this.name + '\'' + ", timestamp=" + this.timestamp + ", weight=" + this.weight + '}';
    }
}
