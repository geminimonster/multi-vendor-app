package com.onesignal;

import com.onesignal.OneSignal;
import com.onesignal.OneSignalRestClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

class OneSignalRemoteParams {
    static final int DEFAULT_INDIRECT_ATTRIBUTION_WINDOW = 1440;
    static final int DEFAULT_NOTIFICATION_LIMIT = 10;
    private static final String DIRECT_PARAM = "direct";
    private static final String ENABLED_PARAM = "enabled";
    private static final String FCM_API_KEY = "api_key";
    private static final String FCM_APP_ID = "app_id";
    private static final String FCM_PARENT_PARAM = "fcm";
    private static final String FCM_PROJECT_ID = "project_id";
    private static final int INCREASE_BETWEEN_RETRIES = 10000;
    private static final String INDIRECT_PARAM = "indirect";
    private static final int MAX_WAIT_BETWEEN_RETRIES = 90000;
    private static final int MIN_WAIT_BETWEEN_RETRIES = 30000;
    private static final String NOTIFICATION_ATTRIBUTION_PARAM = "notification_attribution";
    private static final String OUTCOME_PARAM = "outcomes";
    private static final String UNATTRIBUTED_PARAM = "unattributed";
    /* access modifiers changed from: private */
    public static int androidParamsRetries;

    interface CallBack {
        void complete(Params params);
    }

    OneSignalRemoteParams() {
    }

    static /* synthetic */ int access$008() {
        int i = androidParamsRetries;
        androidParamsRetries = i + 1;
        return i;
    }

    static class FCMParams {
        String apiKey;
        String appId;
        String projectId;

        FCMParams() {
        }
    }

    static class OutcomesParams {
        boolean directEnabled = false;
        int indirectAttributionWindow = OneSignalRemoteParams.DEFAULT_INDIRECT_ATTRIBUTION_WINDOW;
        boolean indirectEnabled = false;
        int notificationLimit = 10;
        boolean unattributedEnabled = false;

        OutcomesParams() {
        }
    }

    static class Params {
        boolean clearGroupOnSummaryClick;
        boolean enterprise;
        FCMParams fcmParams;
        boolean firebaseAnalytics;
        String googleProjectNumber;
        JSONArray notificationChannels;
        OutcomesParams outcomesParams;
        boolean receiveReceiptEnabled;
        boolean restoreTTLFilter;
        boolean useEmailAuth;

        Params() {
        }
    }

    static void makeAndroidParamsRequest(final CallBack callBack) {
        AnonymousClass1 r0 = new OneSignalRestClient.ResponseHandler() {
            /* access modifiers changed from: package-private */
            public void onFailure(int i, String str, Throwable th) {
                if (i == 403) {
                    OneSignal.Log(OneSignal.LOG_LEVEL.FATAL, "403 error getting OneSignal params, omitting further retries!");
                } else {
                    new Thread(new Runnable() {
                        public void run() {
                            int access$000 = (OneSignalRemoteParams.androidParamsRetries * OneSignalRemoteParams.INCREASE_BETWEEN_RETRIES) + OneSignalRemoteParams.MIN_WAIT_BETWEEN_RETRIES;
                            if (access$000 > OneSignalRemoteParams.MAX_WAIT_BETWEEN_RETRIES) {
                                access$000 = OneSignalRemoteParams.MAX_WAIT_BETWEEN_RETRIES;
                            }
                            OneSignal.LOG_LEVEL log_level = OneSignal.LOG_LEVEL.INFO;
                            OneSignal.Log(log_level, "Failed to get Android parameters, trying again in " + (access$000 / 1000) + " seconds.");
                            OSUtils.sleep(access$000);
                            OneSignalRemoteParams.access$008();
                            OneSignalRemoteParams.makeAndroidParamsRequest(callBack);
                        }
                    }, "OS_PARAMS_REQUEST").start();
                }
            }

            /* access modifiers changed from: package-private */
            public void onSuccess(String str) {
                OneSignalRemoteParams.processJson(str, callBack);
            }
        };
        String str = "apps/" + OneSignal.appId + "/android_params.js";
        String userId = OneSignal.getUserId();
        if (userId != null) {
            str = str + "?player_id=" + userId;
        }
        OneSignal.Log(OneSignal.LOG_LEVEL.DEBUG, "Starting request to get Android parameters.");
        OneSignalRestClient.get(str, r0, "CACHE_KEY_REMOTE_PARAMS");
    }

    /* access modifiers changed from: private */
    public static void processJson(String str, CallBack callBack) {
        try {
            final JSONObject jSONObject = new JSONObject(str);
            callBack.complete(new Params() {
                {
                    this.enterprise = jSONObject.optBoolean("enterp", false);
                    this.useEmailAuth = jSONObject.optBoolean("use_email_auth", false);
                    this.notificationChannels = jSONObject.optJSONArray("chnl_lst");
                    this.firebaseAnalytics = jSONObject.optBoolean("fba", false);
                    this.restoreTTLFilter = jSONObject.optBoolean("restore_ttl_filter", true);
                    this.googleProjectNumber = jSONObject.optString("android_sender_id", (String) null);
                    this.clearGroupOnSummaryClick = jSONObject.optBoolean("clear_group_on_summary_click", true);
                    this.receiveReceiptEnabled = jSONObject.optBoolean("receive_receipts_enable", false);
                    this.outcomesParams = new OutcomesParams();
                    if (jSONObject.has(OneSignalRemoteParams.OUTCOME_PARAM)) {
                        JSONObject optJSONObject = jSONObject.optJSONObject(OneSignalRemoteParams.OUTCOME_PARAM);
                        if (optJSONObject.has(OneSignalRemoteParams.DIRECT_PARAM)) {
                            JSONObject optJSONObject2 = optJSONObject.optJSONObject(OneSignalRemoteParams.DIRECT_PARAM);
                            this.outcomesParams.directEnabled = optJSONObject2.optBoolean(OneSignalRemoteParams.ENABLED_PARAM);
                        }
                        if (optJSONObject.has(OneSignalRemoteParams.INDIRECT_PARAM)) {
                            JSONObject optJSONObject3 = optJSONObject.optJSONObject(OneSignalRemoteParams.INDIRECT_PARAM);
                            this.outcomesParams.indirectEnabled = optJSONObject3.optBoolean(OneSignalRemoteParams.ENABLED_PARAM);
                            if (optJSONObject3.has(OneSignalRemoteParams.NOTIFICATION_ATTRIBUTION_PARAM)) {
                                JSONObject optJSONObject4 = optJSONObject3.optJSONObject(OneSignalRemoteParams.NOTIFICATION_ATTRIBUTION_PARAM);
                                this.outcomesParams.indirectAttributionWindow = optJSONObject4.optInt("minutes_since_displayed", OneSignalRemoteParams.DEFAULT_INDIRECT_ATTRIBUTION_WINDOW);
                                this.outcomesParams.notificationLimit = optJSONObject4.optInt("limit", 10);
                            }
                        }
                        if (optJSONObject.has(OneSignalRemoteParams.UNATTRIBUTED_PARAM)) {
                            JSONObject optJSONObject5 = optJSONObject.optJSONObject(OneSignalRemoteParams.UNATTRIBUTED_PARAM);
                            this.outcomesParams.unattributedEnabled = optJSONObject5.optBoolean(OneSignalRemoteParams.ENABLED_PARAM);
                        }
                    }
                    this.fcmParams = new FCMParams();
                    if (jSONObject.has("fcm")) {
                        JSONObject optJSONObject6 = jSONObject.optJSONObject("fcm");
                        this.fcmParams.apiKey = optJSONObject6.optString(OneSignalRemoteParams.FCM_API_KEY, (String) null);
                        this.fcmParams.appId = optJSONObject6.optString("app_id", (String) null);
                        this.fcmParams.projectId = optJSONObject6.optString(OneSignalRemoteParams.FCM_PROJECT_ID, (String) null);
                    }
                }
            });
        } catch (NullPointerException | JSONException e) {
            OneSignal.Log(OneSignal.LOG_LEVEL.FATAL, "Error parsing android_params!: ", e);
            OneSignal.LOG_LEVEL log_level = OneSignal.LOG_LEVEL.FATAL;
            OneSignal.Log(log_level, "Response that errored from android_params!: " + str);
        }
    }
}
