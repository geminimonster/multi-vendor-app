package com.onesignal;

public final class R {

    public static final class anim {
        public static final int onesignal_fade_in = 2130772001;
        public static final int onesignal_fade_out = 2130772002;

        private anim() {
        }
    }

    public static final class attr {
        public static final int buttonSize = 2130968691;
        public static final int cardBackgroundColor = 2130968696;
        public static final int cardCornerRadius = 2130968697;
        public static final int cardElevation = 2130968698;
        public static final int cardMaxElevation = 2130968701;
        public static final int cardPreventCornerOverlap = 2130968702;
        public static final int cardUseCompatPadding = 2130968705;
        public static final int cardViewStyle = 2130968706;
        public static final int circleCrop = 2130968736;
        public static final int colorScheme = 2130968772;
        public static final int contentPadding = 2130968801;
        public static final int contentPaddingBottom = 2130968802;
        public static final int contentPaddingLeft = 2130968803;
        public static final int contentPaddingRight = 2130968804;
        public static final int contentPaddingTop = 2130968805;
        public static final int coordinatorLayoutStyle = 2130968808;
        public static final int font = 2130968958;
        public static final int fontProviderAuthority = 2130968961;
        public static final int fontProviderCerts = 2130968962;
        public static final int fontProviderFetchStrategy = 2130968963;
        public static final int fontProviderFetchTimeout = 2130968964;
        public static final int fontProviderPackage = 2130968965;
        public static final int fontProviderQuery = 2130968966;
        public static final int fontStyle = 2130968967;
        public static final int fontWeight = 2130968969;
        public static final int imageAspectRatio = 2130969015;
        public static final int imageAspectRatioAdjust = 2130969016;
        public static final int keylines = 2130969056;
        public static final int layout_anchor = 2130969061;
        public static final int layout_anchorGravity = 2130969062;
        public static final int layout_behavior = 2130969063;
        public static final int layout_dodgeInsetEdges = 2130969107;
        public static final int layout_insetEdge = 2130969116;
        public static final int layout_keyline = 2130969117;
        public static final int scopeUris = 2130969264;
        public static final int statusBarBackground = 2130969329;

        private attr() {
        }
    }

    public static final class bool {
        public static final int abc_action_bar_embed_tabs = 2131034112;

        private bool() {
        }
    }

    public static final class color {
        public static final int browser_actions_bg_grey = 2131099687;
        public static final int browser_actions_divider_color = 2131099688;
        public static final int browser_actions_text_color = 2131099689;
        public static final int browser_actions_title_color = 2131099690;
        public static final int cardview_dark_background = 2131099694;
        public static final int cardview_light_background = 2131099695;
        public static final int cardview_shadow_end_color = 2131099696;
        public static final int cardview_shadow_start_color = 2131099697;
        public static final int common_google_signin_btn_text_dark = 2131099739;
        public static final int common_google_signin_btn_text_dark_default = 2131099740;
        public static final int common_google_signin_btn_text_dark_disabled = 2131099741;
        public static final int common_google_signin_btn_text_dark_focused = 2131099742;
        public static final int common_google_signin_btn_text_dark_pressed = 2131099743;
        public static final int common_google_signin_btn_text_light = 2131099744;
        public static final int common_google_signin_btn_text_light_default = 2131099745;
        public static final int common_google_signin_btn_text_light_disabled = 2131099746;
        public static final int common_google_signin_btn_text_light_focused = 2131099747;
        public static final int common_google_signin_btn_text_light_pressed = 2131099748;
        public static final int common_google_signin_btn_tint = 2131099749;
        public static final int notification_action_color_filter = 2131099889;
        public static final int notification_icon_bg_color = 2131099890;
        public static final int notification_material_background_media_default_color = 2131099891;
        public static final int primary_text_default_material_dark = 2131099896;
        public static final int ripple_material_light = 2131099903;
        public static final int secondary_text_default_material_dark = 2131099906;
        public static final int secondary_text_default_material_light = 2131099907;

        private color() {
        }
    }

    public static final class dimen {
        public static final int browser_actions_context_menu_max_width = 2131165313;
        public static final int browser_actions_context_menu_min_padding = 2131165314;
        public static final int cardview_compat_inset_shadow = 2131165322;
        public static final int cardview_default_elevation = 2131165323;
        public static final int cardview_default_radius = 2131165324;
        public static final int compat_button_inset_horizontal_material = 2131165342;
        public static final int compat_button_inset_vertical_material = 2131165343;
        public static final int compat_button_padding_horizontal_material = 2131165344;
        public static final int compat_button_padding_vertical_material = 2131165345;
        public static final int compat_control_corner_material = 2131165346;
        public static final int notification_action_icon_size = 2131165571;
        public static final int notification_action_text_size = 2131165572;
        public static final int notification_big_circle_margin = 2131165573;
        public static final int notification_content_margin_start = 2131165574;
        public static final int notification_large_icon_height = 2131165575;
        public static final int notification_large_icon_width = 2131165576;
        public static final int notification_main_column_padding_top = 2131165577;
        public static final int notification_media_narrow_margin = 2131165578;
        public static final int notification_right_icon_size = 2131165579;
        public static final int notification_right_side_padding_top = 2131165580;
        public static final int notification_small_icon_background_padding = 2131165581;
        public static final int notification_small_icon_size_as_large = 2131165582;
        public static final int notification_subtext_size = 2131165583;
        public static final int notification_top_pad = 2131165584;
        public static final int notification_top_pad_large_text = 2131165585;

        private dimen() {
        }
    }

    public static final class drawable {
        public static final int common_full_open_on_phone = 2131230883;
        public static final int common_google_signin_btn_icon_dark = 2131230884;
        public static final int common_google_signin_btn_icon_dark_focused = 2131230885;
        public static final int common_google_signin_btn_icon_dark_normal = 2131230886;
        public static final int common_google_signin_btn_icon_dark_normal_background = 2131230887;
        public static final int common_google_signin_btn_icon_disabled = 2131230888;
        public static final int common_google_signin_btn_icon_light = 2131230889;
        public static final int common_google_signin_btn_icon_light_focused = 2131230890;
        public static final int common_google_signin_btn_icon_light_normal = 2131230891;
        public static final int common_google_signin_btn_icon_light_normal_background = 2131230892;
        public static final int common_google_signin_btn_text_dark = 2131230893;
        public static final int common_google_signin_btn_text_dark_focused = 2131230894;
        public static final int common_google_signin_btn_text_dark_normal = 2131230895;
        public static final int common_google_signin_btn_text_dark_normal_background = 2131230896;
        public static final int common_google_signin_btn_text_disabled = 2131230897;
        public static final int common_google_signin_btn_text_light = 2131230898;
        public static final int common_google_signin_btn_text_light_focused = 2131230899;
        public static final int common_google_signin_btn_text_light_normal = 2131230900;
        public static final int common_google_signin_btn_text_light_normal_background = 2131230901;
        public static final int googleg_disabled_color_18 = 2131230914;
        public static final int googleg_standard_color_18 = 2131230915;
        public static final int ic_os_notification_fallback_white_24dp = 2131230986;
        public static final int notification_action_background = 2131231023;
        public static final int notification_bg = 2131231024;
        public static final int notification_bg_low = 2131231025;
        public static final int notification_bg_low_normal = 2131231026;
        public static final int notification_bg_low_pressed = 2131231027;
        public static final int notification_bg_normal = 2131231028;
        public static final int notification_bg_normal_pressed = 2131231029;
        public static final int notification_icon_background = 2131231030;
        public static final int notification_template_icon_bg = 2131231031;
        public static final int notification_template_icon_low_bg = 2131231032;
        public static final int notification_tile_bg = 2131231033;
        public static final int notify_panel_notification_icon_bg = 2131231034;

        private drawable() {
        }
    }

    public static final class id {
        public static final int action0 = 2131296305;
        public static final int action_container = 2131296315;
        public static final int action_divider = 2131296317;
        public static final int action_image = 2131296319;
        public static final int action_text = 2131296327;
        public static final int actions = 2131296329;
        public static final int adjust_height = 2131296333;
        public static final int adjust_width = 2131296334;
        public static final int async = 2131296341;
        public static final int auto = 2131296345;
        public static final int blocking = 2131296352;
        public static final int bottom = 2131296355;
        public static final int browser_actions_header_text = 2131296360;
        public static final int browser_actions_menu_item_icon = 2131296361;
        public static final int browser_actions_menu_item_text = 2131296362;
        public static final int browser_actions_menu_items = 2131296363;
        public static final int browser_actions_menu_view = 2131296364;
        public static final int cancel_action = 2131296385;
        public static final int chronometer = 2131296406;
        public static final int dark = 2131296454;
        public static final int end = 2131296507;
        public static final int end_padder = 2131296508;
        public static final int forever = 2131296543;
        public static final int icon = 2131296566;
        public static final int icon_group = 2131296567;
        public static final int icon_only = 2131296568;
        public static final int info = 2131296579;
        public static final int italic = 2131296584;
        public static final int left = 2131296660;
        public static final int light = 2131296662;
        public static final int line1 = 2131296663;
        public static final int line3 = 2131296664;
        public static final int media_actions = 2131296703;
        public static final int none = 2131296745;
        public static final int normal = 2131296746;
        public static final int notification_background = 2131296747;
        public static final int notification_main_column = 2131296748;
        public static final int notification_main_column_container = 2131296749;
        public static final int os_bgimage_notif_bgimage = 2131296756;
        public static final int os_bgimage_notif_bgimage_align_layout = 2131296757;
        public static final int os_bgimage_notif_bgimage_right_aligned = 2131296758;
        public static final int os_bgimage_notif_body = 2131296759;
        public static final int os_bgimage_notif_title = 2131296760;
        public static final int right = 2131296804;
        public static final int right_icon = 2131296805;
        public static final int right_side = 2131296806;
        public static final int standard = 2131296910;
        public static final int start = 2131296911;
        public static final int status_bar_latest_event_content = 2131296912;
        public static final int tag_transition_group = 2131296929;
        public static final int text = 2131296935;
        public static final int text2 = 2131296936;
        public static final int time = 2131296952;
        public static final int title = 2131296953;
        public static final int top = 2131296971;
        public static final int wide = 2131297129;

        private id() {
        }
    }

    public static final class integer {
        public static final int cancel_button_image_alpha = 2131361796;
        public static final int google_play_services_version = 2131361800;
        public static final int status_bar_notification_info_maxnum = 2131361813;

        private integer() {
        }
    }

    public static final class layout {
        public static final int browser_actions_context_menu_page = 2131492925;
        public static final int browser_actions_context_menu_row = 2131492926;
        public static final int notification_action = 2131493052;
        public static final int notification_action_tombstone = 2131493053;
        public static final int notification_media_action = 2131493054;
        public static final int notification_media_cancel_action = 2131493055;
        public static final int notification_template_big_media = 2131493056;
        public static final int notification_template_big_media_custom = 2131493057;
        public static final int notification_template_big_media_narrow = 2131493058;
        public static final int notification_template_big_media_narrow_custom = 2131493059;
        public static final int notification_template_custom_big = 2131493060;
        public static final int notification_template_icon_group = 2131493061;
        public static final int notification_template_lines_media = 2131493062;
        public static final int notification_template_media = 2131493063;
        public static final int notification_template_media_custom = 2131493064;
        public static final int notification_template_part_chronometer = 2131493065;
        public static final int notification_template_part_time = 2131493066;
        public static final int onesignal_bgimage_notif_layout = 2131493067;

        private layout() {
        }
    }

    public static final class raw {
        public static final int consumer_onesignal_keep = 2131755008;

        private raw() {
        }
    }

    public static final class string {
        public static final int common_google_play_services_enable_button = 2131820679;
        public static final int common_google_play_services_enable_text = 2131820680;
        public static final int common_google_play_services_enable_title = 2131820681;
        public static final int common_google_play_services_install_button = 2131820682;
        public static final int common_google_play_services_install_text = 2131820683;
        public static final int common_google_play_services_install_title = 2131820684;
        public static final int common_google_play_services_notification_channel_name = 2131820685;
        public static final int common_google_play_services_notification_ticker = 2131820686;
        public static final int common_google_play_services_unknown_issue = 2131820687;
        public static final int common_google_play_services_unsupported_text = 2131820688;
        public static final int common_google_play_services_update_button = 2131820689;
        public static final int common_google_play_services_update_text = 2131820690;
        public static final int common_google_play_services_update_title = 2131820691;
        public static final int common_google_play_services_updating_text = 2131820692;
        public static final int common_google_play_services_wear_update_text = 2131820693;
        public static final int common_open_on_phone = 2131820694;
        public static final int common_signin_button_text = 2131820695;
        public static final int common_signin_button_text_long = 2131820696;
        public static final int fcm_fallback_notification_channel_label = 2131820746;
        public static final int status_bar_notification_info_overflow = 2131821111;

        private string() {
        }
    }

    public static final class style {
        public static final int Base_CardView = 2131886103;
        public static final int CardView = 2131886313;
        public static final int CardView_Dark = 2131886314;
        public static final int CardView_Light = 2131886315;
        public static final int TextAppearance_Compat_Notification = 2131886501;
        public static final int TextAppearance_Compat_Notification_Info = 2131886502;
        public static final int TextAppearance_Compat_Notification_Info_Media = 2131886503;
        public static final int TextAppearance_Compat_Notification_Line2 = 2131886504;
        public static final int TextAppearance_Compat_Notification_Line2_Media = 2131886505;
        public static final int TextAppearance_Compat_Notification_Media = 2131886506;
        public static final int TextAppearance_Compat_Notification_Time = 2131886507;
        public static final int TextAppearance_Compat_Notification_Time_Media = 2131886508;
        public static final int TextAppearance_Compat_Notification_Title = 2131886509;
        public static final int TextAppearance_Compat_Notification_Title_Media = 2131886510;
        public static final int Widget_Compat_NotificationActionContainer = 2131886735;
        public static final int Widget_Compat_NotificationActionText = 2131886736;
        public static final int Widget_Support_CoordinatorLayout = 2131886839;

        private style() {
        }
    }

    public static final class styleable {
        public static final int[] CardView = {16843071, 16843072, com.store.proshop.R.attr.cardBackgroundColor, com.store.proshop.R.attr.cardCornerRadius, com.store.proshop.R.attr.cardElevation, com.store.proshop.R.attr.cardMaxElevation, com.store.proshop.R.attr.cardPreventCornerOverlap, com.store.proshop.R.attr.cardUseCompatPadding, com.store.proshop.R.attr.contentPadding, com.store.proshop.R.attr.contentPaddingBottom, com.store.proshop.R.attr.contentPaddingLeft, com.store.proshop.R.attr.contentPaddingRight, com.store.proshop.R.attr.contentPaddingTop};
        public static final int CardView_android_minHeight = 1;
        public static final int CardView_android_minWidth = 0;
        public static final int CardView_cardBackgroundColor = 2;
        public static final int CardView_cardCornerRadius = 3;
        public static final int CardView_cardElevation = 4;
        public static final int CardView_cardMaxElevation = 5;
        public static final int CardView_cardPreventCornerOverlap = 6;
        public static final int CardView_cardUseCompatPadding = 7;
        public static final int CardView_contentPadding = 8;
        public static final int CardView_contentPaddingBottom = 9;
        public static final int CardView_contentPaddingLeft = 10;
        public static final int CardView_contentPaddingRight = 11;
        public static final int CardView_contentPaddingTop = 12;
        public static final int[] CoordinatorLayout = {com.store.proshop.R.attr.keylines, com.store.proshop.R.attr.statusBarBackground};
        public static final int[] CoordinatorLayout_Layout = {16842931, com.store.proshop.R.attr.layout_anchor, com.store.proshop.R.attr.layout_anchorGravity, com.store.proshop.R.attr.layout_behavior, com.store.proshop.R.attr.layout_dodgeInsetEdges, com.store.proshop.R.attr.layout_insetEdge, com.store.proshop.R.attr.layout_keyline};
        public static final int CoordinatorLayout_Layout_android_layout_gravity = 0;
        public static final int CoordinatorLayout_Layout_layout_anchor = 1;
        public static final int CoordinatorLayout_Layout_layout_anchorGravity = 2;
        public static final int CoordinatorLayout_Layout_layout_behavior = 3;
        public static final int CoordinatorLayout_Layout_layout_dodgeInsetEdges = 4;
        public static final int CoordinatorLayout_Layout_layout_insetEdge = 5;
        public static final int CoordinatorLayout_Layout_layout_keyline = 6;
        public static final int CoordinatorLayout_keylines = 0;
        public static final int CoordinatorLayout_statusBarBackground = 1;
        public static final int[] FontFamily = {com.store.proshop.R.attr.fontProviderAuthority, com.store.proshop.R.attr.fontProviderCerts, com.store.proshop.R.attr.fontProviderFetchStrategy, com.store.proshop.R.attr.fontProviderFetchTimeout, com.store.proshop.R.attr.fontProviderPackage, com.store.proshop.R.attr.fontProviderQuery};
        public static final int[] FontFamilyFont = {16844082, 16844083, 16844095, 16844143, 16844144, com.store.proshop.R.attr.font, com.store.proshop.R.attr.fontStyle, com.store.proshop.R.attr.fontVariationSettings, com.store.proshop.R.attr.fontWeight, com.store.proshop.R.attr.ttcIndex};
        public static final int FontFamilyFont_android_font = 0;
        public static final int FontFamilyFont_android_fontStyle = 2;
        public static final int FontFamilyFont_android_fontVariationSettings = 4;
        public static final int FontFamilyFont_android_fontWeight = 1;
        public static final int FontFamilyFont_android_ttcIndex = 3;
        public static final int FontFamilyFont_font = 5;
        public static final int FontFamilyFont_fontStyle = 6;
        public static final int FontFamilyFont_fontVariationSettings = 7;
        public static final int FontFamilyFont_fontWeight = 8;
        public static final int FontFamilyFont_ttcIndex = 9;
        public static final int FontFamily_fontProviderAuthority = 0;
        public static final int FontFamily_fontProviderCerts = 1;
        public static final int FontFamily_fontProviderFetchStrategy = 2;
        public static final int FontFamily_fontProviderFetchTimeout = 3;
        public static final int FontFamily_fontProviderPackage = 4;
        public static final int FontFamily_fontProviderQuery = 5;
        public static final int[] LoadingImageView = {com.store.proshop.R.attr.circleCrop, com.store.proshop.R.attr.imageAspectRatio, com.store.proshop.R.attr.imageAspectRatioAdjust};
        public static final int LoadingImageView_circleCrop = 0;
        public static final int LoadingImageView_imageAspectRatio = 1;
        public static final int LoadingImageView_imageAspectRatioAdjust = 2;
        public static final int[] SignInButton = {com.store.proshop.R.attr.buttonSize, com.store.proshop.R.attr.colorScheme, com.store.proshop.R.attr.scopeUris};
        public static final int SignInButton_buttonSize = 0;
        public static final int SignInButton_colorScheme = 1;
        public static final int SignInButton_scopeUris = 2;

        private styleable() {
        }
    }

    private R() {
    }
}
