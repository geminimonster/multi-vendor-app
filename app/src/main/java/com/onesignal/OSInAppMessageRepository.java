package com.onesignal;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import com.onesignal.OneSignalDbContract;

class OSInAppMessageRepository {
    private static final long OS_IAM_MAX_CACHE_TIME = 15552000;
    private final OneSignalDbHelper dbHelper;

    OSInAppMessageRepository(OneSignalDbHelper oneSignalDbHelper) {
        this.dbHelper = oneSignalDbHelper;
    }

    /* access modifiers changed from: package-private */
    public synchronized void deleteOldRedisplayedInAppMessages() {
        long currentTimeMillis = (System.currentTimeMillis() / 1000) - OS_IAM_MAX_CACHE_TIME;
        this.dbHelper.getWritableDbWithRetries().delete(OneSignalDbContract.InAppMessageTable.TABLE_NAME, "last_display< ?", new String[]{String.valueOf(currentTimeMillis)});
    }

    /* access modifiers changed from: package-private */
    public synchronized void saveInAppMessage(OSInAppMessage oSInAppMessage) {
        SQLiteDatabase writableDbWithRetries = this.dbHelper.getWritableDbWithRetries();
        ContentValues contentValues = new ContentValues();
        contentValues.put(OneSignalDbContract.InAppMessageTable.COLUMN_NAME_MESSAGE_ID, oSInAppMessage.messageId);
        contentValues.put(OneSignalDbContract.InAppMessageTable.COLUMN_NAME_DISPLAY_QUANTITY, Integer.valueOf(oSInAppMessage.getDisplayStats().getDisplayQuantity()));
        contentValues.put(OneSignalDbContract.InAppMessageTable.COLUMN_NAME_LAST_DISPLAY, Long.valueOf(oSInAppMessage.getDisplayStats().getLastDisplayTime()));
        contentValues.put(OneSignalDbContract.InAppMessageTable.COLUMN_CLICK_IDS, oSInAppMessage.getClickedClickIds().toString());
        contentValues.put(OneSignalDbContract.InAppMessageTable.COLUMN_DISPLAYED_IN_SESSION, Boolean.valueOf(oSInAppMessage.isDisplayedInSession()));
        if (writableDbWithRetries.update(OneSignalDbContract.InAppMessageTable.TABLE_NAME, contentValues, "message_id = ?", new String[]{oSInAppMessage.messageId}) == 0) {
            writableDbWithRetries.insert(OneSignalDbContract.InAppMessageTable.TABLE_NAME, (String) null, contentValues);
        }
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x008a, code lost:
        if (r1.isClosed() == false) goto L_0x008c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x008c, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x00a0, code lost:
        if (r1.isClosed() == false) goto L_0x008c;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized java.util.List<com.onesignal.OSInAppMessage> getRedisplayedInAppMessages() {
        /*
            r11 = this;
            monitor-enter(r11)
            java.util.ArrayList r0 = new java.util.ArrayList     // Catch:{ all -> 0x00b1 }
            r0.<init>()     // Catch:{ all -> 0x00b1 }
            r1 = 0
            com.onesignal.OneSignalDbHelper r2 = r11.dbHelper     // Catch:{ JSONException -> 0x0092 }
            android.database.sqlite.SQLiteDatabase r3 = r2.getReadableDbWithRetries()     // Catch:{ JSONException -> 0x0092 }
            java.lang.String r4 = "in_app_message"
            r5 = 0
            r6 = 0
            r7 = 0
            r8 = 0
            r9 = 0
            r10 = 0
            android.database.Cursor r1 = r3.query(r4, r5, r6, r7, r8, r9, r10)     // Catch:{ JSONException -> 0x0092 }
            boolean r2 = r1.moveToFirst()     // Catch:{ JSONException -> 0x0092 }
            if (r2 == 0) goto L_0x0084
        L_0x001f:
            java.lang.String r2 = "message_id"
            int r2 = r1.getColumnIndex(r2)     // Catch:{ JSONException -> 0x0092 }
            java.lang.String r2 = r1.getString(r2)     // Catch:{ JSONException -> 0x0092 }
            java.lang.String r3 = "click_ids"
            int r3 = r1.getColumnIndex(r3)     // Catch:{ JSONException -> 0x0092 }
            java.lang.String r3 = r1.getString(r3)     // Catch:{ JSONException -> 0x0092 }
            java.lang.String r4 = "display_quantity"
            int r4 = r1.getColumnIndex(r4)     // Catch:{ JSONException -> 0x0092 }
            int r4 = r1.getInt(r4)     // Catch:{ JSONException -> 0x0092 }
            java.lang.String r5 = "last_display"
            int r5 = r1.getColumnIndex(r5)     // Catch:{ JSONException -> 0x0092 }
            long r5 = r1.getLong(r5)     // Catch:{ JSONException -> 0x0092 }
            java.lang.String r7 = "displayed_in_session"
            int r7 = r1.getColumnIndex(r7)     // Catch:{ JSONException -> 0x0092 }
            int r7 = r1.getInt(r7)     // Catch:{ JSONException -> 0x0092 }
            r8 = 0
            r9 = 1
            if (r7 != r9) goto L_0x0056
            goto L_0x0057
        L_0x0056:
            r9 = 0
        L_0x0057:
            org.json.JSONArray r7 = new org.json.JSONArray     // Catch:{ JSONException -> 0x0092 }
            r7.<init>(r3)     // Catch:{ JSONException -> 0x0092 }
            java.util.HashSet r3 = new java.util.HashSet     // Catch:{ JSONException -> 0x0092 }
            r3.<init>()     // Catch:{ JSONException -> 0x0092 }
        L_0x0061:
            int r10 = r7.length()     // Catch:{ JSONException -> 0x0092 }
            if (r8 >= r10) goto L_0x0071
            java.lang.String r10 = r7.getString(r8)     // Catch:{ JSONException -> 0x0092 }
            r3.add(r10)     // Catch:{ JSONException -> 0x0092 }
            int r8 = r8 + 1
            goto L_0x0061
        L_0x0071:
            com.onesignal.OSInAppMessage r7 = new com.onesignal.OSInAppMessage     // Catch:{ JSONException -> 0x0092 }
            com.onesignal.OSInAppMessageDisplayStats r8 = new com.onesignal.OSInAppMessageDisplayStats     // Catch:{ JSONException -> 0x0092 }
            r8.<init>(r4, r5)     // Catch:{ JSONException -> 0x0092 }
            r7.<init>(r2, r3, r9, r8)     // Catch:{ JSONException -> 0x0092 }
            r0.add(r7)     // Catch:{ JSONException -> 0x0092 }
            boolean r2 = r1.moveToNext()     // Catch:{ JSONException -> 0x0092 }
            if (r2 != 0) goto L_0x001f
        L_0x0084:
            if (r1 == 0) goto L_0x00a3
            boolean r2 = r1.isClosed()     // Catch:{ all -> 0x00b1 }
            if (r2 != 0) goto L_0x00a3
        L_0x008c:
            r1.close()     // Catch:{ all -> 0x00b1 }
            goto L_0x00a3
        L_0x0090:
            r0 = move-exception
            goto L_0x00a5
        L_0x0092:
            r2 = move-exception
            com.onesignal.OneSignal$LOG_LEVEL r3 = com.onesignal.OneSignal.LOG_LEVEL.ERROR     // Catch:{ all -> 0x0090 }
            java.lang.String r4 = "Generating JSONArray from iam click ids:JSON Failed."
            com.onesignal.OneSignal.Log(r3, r4, r2)     // Catch:{ all -> 0x0090 }
            if (r1 == 0) goto L_0x00a3
            boolean r2 = r1.isClosed()     // Catch:{ all -> 0x00b1 }
            if (r2 != 0) goto L_0x00a3
            goto L_0x008c
        L_0x00a3:
            monitor-exit(r11)
            return r0
        L_0x00a5:
            if (r1 == 0) goto L_0x00b0
            boolean r2 = r1.isClosed()     // Catch:{ all -> 0x00b1 }
            if (r2 != 0) goto L_0x00b0
            r1.close()     // Catch:{ all -> 0x00b1 }
        L_0x00b0:
            throw r0     // Catch:{ all -> 0x00b1 }
        L_0x00b1:
            r0 = move-exception
            monitor-exit(r11)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.onesignal.OSInAppMessageRepository.getRedisplayedInAppMessages():java.util.List");
    }
}
