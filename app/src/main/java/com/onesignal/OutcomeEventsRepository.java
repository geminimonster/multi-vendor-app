package com.onesignal;

import com.onesignal.OneSignal;
import com.onesignal.OneSignalRestClient;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

class OutcomeEventsRepository {
    private static final String APP_ID = "app_id";
    private static final String DEVICE_TYPE = "device_type";
    private static final String DIRECT = "direct";
    private final OneSignalDbHelper dbHelper;
    private final OutcomeEventsService outcomeEventsService;

    OutcomeEventsRepository(OneSignalDbHelper oneSignalDbHelper) {
        this.outcomeEventsService = new OutcomeEventsService();
        this.dbHelper = oneSignalDbHelper;
    }

    OutcomeEventsRepository(OutcomeEventsService outcomeEventsService2, OneSignalDbHelper oneSignalDbHelper) {
        this.outcomeEventsService = outcomeEventsService2;
        this.dbHelper = oneSignalDbHelper;
    }

    /* access modifiers changed from: package-private */
    public List<OutcomeEvent> getSavedOutcomeEvents() {
        return OutcomeEventsCache.getAllEventsToSend(this.dbHelper);
    }

    /* access modifiers changed from: package-private */
    public void saveOutcomeEvent(OutcomeEvent outcomeEvent) {
        OutcomeEventsCache.saveOutcomeEvent(outcomeEvent, this.dbHelper);
    }

    /* access modifiers changed from: package-private */
    public void removeEvent(OutcomeEvent outcomeEvent) {
        OutcomeEventsCache.deleteOldOutcomeEvent(outcomeEvent, this.dbHelper);
    }

    /* access modifiers changed from: package-private */
    public void requestMeasureDirectOutcomeEvent(String str, int i, OutcomeEvent outcomeEvent, OneSignalRestClient.ResponseHandler responseHandler) {
        JSONObject jSONObjectForMeasure = outcomeEvent.toJSONObjectForMeasure();
        try {
            jSONObjectForMeasure.put("app_id", str);
            jSONObjectForMeasure.put("device_type", i);
            jSONObjectForMeasure.put(DIRECT, true);
            this.outcomeEventsService.sendOutcomeEvent(jSONObjectForMeasure, responseHandler);
        } catch (JSONException e) {
            OneSignal.Log(OneSignal.LOG_LEVEL.ERROR, "Generating direct outcome:JSON Failed.", e);
        }
    }

    /* access modifiers changed from: package-private */
    public void requestMeasureIndirectOutcomeEvent(String str, int i, OutcomeEvent outcomeEvent, OneSignalRestClient.ResponseHandler responseHandler) {
        JSONObject jSONObjectForMeasure = outcomeEvent.toJSONObjectForMeasure();
        try {
            jSONObjectForMeasure.put("app_id", str);
            jSONObjectForMeasure.put("device_type", i);
            jSONObjectForMeasure.put(DIRECT, false);
            this.outcomeEventsService.sendOutcomeEvent(jSONObjectForMeasure, responseHandler);
        } catch (JSONException e) {
            OneSignal.Log(OneSignal.LOG_LEVEL.ERROR, "Generating indirect outcome:JSON Failed.", e);
        }
    }

    /* access modifiers changed from: package-private */
    public void requestMeasureUnattributedOutcomeEvent(String str, int i, OutcomeEvent outcomeEvent, OneSignalRestClient.ResponseHandler responseHandler) {
        JSONObject jSONObjectForMeasure = outcomeEvent.toJSONObjectForMeasure();
        try {
            jSONObjectForMeasure.put("app_id", str);
            jSONObjectForMeasure.put("device_type", i);
            this.outcomeEventsService.sendOutcomeEvent(jSONObjectForMeasure, responseHandler);
        } catch (JSONException e) {
            OneSignal.Log(OneSignal.LOG_LEVEL.ERROR, "Generating unattributed outcome:JSON Failed.", e);
        }
    }

    /* access modifiers changed from: package-private */
    public void saveUniqueOutcomeNotifications(JSONArray jSONArray, String str) {
        OutcomeEventsCache.saveUniqueOutcomeNotifications(jSONArray, str, this.dbHelper);
    }

    /* access modifiers changed from: package-private */
    public JSONArray getNotCachedUniqueOutcomeNotifications(String str, JSONArray jSONArray) {
        return OutcomeEventsCache.getNotCachedUniqueOutcomeNotifications(str, jSONArray, this.dbHelper);
    }
}
