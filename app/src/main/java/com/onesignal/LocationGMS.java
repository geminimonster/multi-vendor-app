package com.onesignal;

import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.Handler;
import android.os.HandlerThread;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.onesignal.AndroidSupportV4Compat;
import com.onesignal.OneSignal;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

class LocationGMS {
    private static final long BACKGROUND_UPDATE_TIME_MS = 570000;
    private static final long FOREGROUND_UPDATE_TIME_MS = 270000;
    private static final long TIME_BACKGROUND_SEC = 600;
    private static final long TIME_FOREGROUND_SEC = 300;
    /* access modifiers changed from: private */
    public static Context classContext;
    private static Thread fallbackFailThread;
    private static boolean locationCoarse;
    private static LocationHandlerThread locationHandlerThread;
    private static ConcurrentHashMap<PermissionType, LocationHandler> locationHandlers = new ConcurrentHashMap<>();
    static LocationUpdateListener locationUpdateListener;
    /* access modifiers changed from: private */
    public static GoogleApiClientCompatProxy mGoogleApiClient;
    /* access modifiers changed from: private */
    public static Location mLastLocation;
    private static final List<LocationPromptCompletionHandler> promptHandlers = new ArrayList();
    static String requestPermission;
    protected static final Object syncLock = new Object() {
    };

    interface LocationHandler {
        void complete(LocationPoint locationPoint);

        PermissionType getType();
    }

    enum PermissionType {
        STARTUP,
        PROMPT_LOCATION,
        SYNC_SERVICE
    }

    /* access modifiers changed from: private */
    public static int getApiFallbackWait() {
        return 30000;
    }

    LocationGMS() {
    }

    static class LocationPoint {
        Float accuracy;
        Boolean bg;
        Double lat;
        Double log;
        Long timeStamp;
        Integer type;

        LocationPoint() {
        }
    }

    static abstract class LocationPromptCompletionHandler implements LocationHandler {
        /* access modifiers changed from: package-private */
        public void onAnswered(boolean z) {
        }

        LocationPromptCompletionHandler() {
        }
    }

    static boolean scheduleUpdate(Context context) {
        if (!hasLocationPermission(context) || !OneSignal.shareLocation) {
            return false;
        }
        long currentTimeMillis = System.currentTimeMillis() - getLastLocationTime();
        long j = OneSignal.isForeground() ? TIME_FOREGROUND_SEC : TIME_BACKGROUND_SEC;
        Long.signum(j);
        OneSignalSyncServiceUtils.scheduleLocationUpdateTask(context, (j * 1000) - currentTimeMillis);
        return true;
    }

    private static void setLastLocationTime(long j) {
        OneSignalPrefs.saveLong(OneSignalPrefs.PREFS_ONESIGNAL, OneSignalPrefs.PREFS_OS_LAST_LOCATION_TIME, j);
    }

    private static long getLastLocationTime() {
        return OneSignalPrefs.getLong(OneSignalPrefs.PREFS_ONESIGNAL, OneSignalPrefs.PREFS_OS_LAST_LOCATION_TIME, -600000);
    }

    private static boolean hasLocationPermission(Context context) {
        return AndroidSupportV4Compat.ContextCompat.checkSelfPermission(context, "android.permission.ACCESS_FINE_LOCATION") == 0 || AndroidSupportV4Compat.ContextCompat.checkSelfPermission(context, "android.permission.ACCESS_COARSE_LOCATION") == 0;
    }

    static void addPromptHandlerIfAvailable(LocationHandler locationHandler) {
        if (locationHandler instanceof LocationPromptCompletionHandler) {
            synchronized (promptHandlers) {
                promptHandlers.add((LocationPromptCompletionHandler) locationHandler);
            }
        }
    }

    static void sendAndClearPromptHandlers(boolean z, boolean z2) {
        if (!z) {
            OneSignal.onesignalLog(OneSignal.LOG_LEVEL.DEBUG, "LocationGMS sendAndClearPromptHandlers from non prompt flow");
            return;
        }
        synchronized (promptHandlers) {
            OneSignal.onesignalLog(OneSignal.LOG_LEVEL.DEBUG, "LocationGMS calling prompt handlers");
            for (LocationPromptCompletionHandler onAnswered : promptHandlers) {
                onAnswered.onAnswered(z2);
            }
            promptHandlers.clear();
        }
    }

    static void getLocation(Context context, boolean z, LocationHandler locationHandler) {
        addPromptHandlerIfAvailable(locationHandler);
        classContext = context;
        locationHandlers.put(locationHandler.getType(), locationHandler);
        if (!OneSignal.shareLocation) {
            sendAndClearPromptHandlers(z, false);
            fireFailedComplete();
            return;
        }
        int checkSelfPermission = AndroidSupportV4Compat.ContextCompat.checkSelfPermission(context, "android.permission.ACCESS_FINE_LOCATION");
        int i = -1;
        if (checkSelfPermission == -1) {
            i = AndroidSupportV4Compat.ContextCompat.checkSelfPermission(context, "android.permission.ACCESS_COARSE_LOCATION");
            locationCoarse = true;
        }
        if (Build.VERSION.SDK_INT < 23) {
            if (checkSelfPermission == 0 || i == 0) {
                sendAndClearPromptHandlers(z, true);
                startGetLocation();
                return;
            }
            sendAndClearPromptHandlers(z, false);
            locationHandler.complete((LocationPoint) null);
        } else if (checkSelfPermission != 0) {
            try {
                List asList = Arrays.asList(context.getPackageManager().getPackageInfo(context.getPackageName(), 4096).requestedPermissions);
                if (asList.contains("android.permission.ACCESS_FINE_LOCATION")) {
                    requestPermission = "android.permission.ACCESS_FINE_LOCATION";
                } else if (!asList.contains("android.permission.ACCESS_COARSE_LOCATION")) {
                    OneSignal.onesignalLog(OneSignal.LOG_LEVEL.INFO, "Location permissions not added on AndroidManifest file");
                } else if (i != 0) {
                    requestPermission = "android.permission.ACCESS_COARSE_LOCATION";
                }
                if (requestPermission != null && z) {
                    PermissionsActivity.startPrompt();
                } else if (i == 0) {
                    sendAndClearPromptHandlers(z, true);
                    startGetLocation();
                } else {
                    sendAndClearPromptHandlers(z, false);
                    fireFailedComplete();
                }
            } catch (PackageManager.NameNotFoundException e) {
                sendAndClearPromptHandlers(z, false);
                e.printStackTrace();
            }
        } else {
            sendAndClearPromptHandlers(z, true);
            startGetLocation();
        }
    }

    static void startGetLocation() {
        if (fallbackFailThread == null) {
            try {
                synchronized (syncLock) {
                    startFallBackThread();
                    if (locationHandlerThread == null) {
                        locationHandlerThread = new LocationHandlerThread();
                    }
                    if (mGoogleApiClient != null) {
                        if (mLastLocation != null) {
                            if (mLastLocation != null) {
                                fireCompleteForLocation(mLastLocation);
                            }
                        }
                    }
                    GoogleApiClientListener googleApiClientListener = new GoogleApiClientListener();
                    GoogleApiClientCompatProxy googleApiClientCompatProxy = new GoogleApiClientCompatProxy(new GoogleApiClient.Builder(classContext).addApi(LocationServices.API).addConnectionCallbacks(googleApiClientListener).addOnConnectionFailedListener(googleApiClientListener).setHandler(locationHandlerThread.mHandler).build());
                    mGoogleApiClient = googleApiClientCompatProxy;
                    googleApiClientCompatProxy.connect();
                }
            } catch (Throwable th) {
                OneSignal.Log(OneSignal.LOG_LEVEL.WARN, "Location permission exists but there was an error initializing: ", th);
                fireFailedComplete();
            }
        }
    }

    private static void startFallBackThread() {
        Thread thread = new Thread(new Runnable() {
            public void run() {
                try {
                    Thread.sleep((long) LocationGMS.getApiFallbackWait());
                    OneSignal.Log(OneSignal.LOG_LEVEL.WARN, "Location permission exists but GoogleApiClient timed out. Maybe related to mismatch google-play aar versions.");
                    LocationGMS.fireFailedComplete();
                    LocationGMS.scheduleUpdate(LocationGMS.classContext);
                } catch (InterruptedException unused) {
                }
            }
        }, "OS_GMS_LOCATION_FALLBACK");
        fallbackFailThread = thread;
        thread.start();
    }

    static void fireFailedComplete() {
        PermissionsActivity.answered = false;
        synchronized (syncLock) {
            if (mGoogleApiClient != null) {
                mGoogleApiClient.disconnect();
            }
            mGoogleApiClient = null;
        }
        fireComplete((LocationPoint) null);
    }

    private static void fireComplete(LocationPoint locationPoint) {
        Thread thread;
        Class<LocationGMS> cls = LocationGMS.class;
        HashMap hashMap = new HashMap();
        synchronized (cls) {
            hashMap.putAll(locationHandlers);
            locationHandlers.clear();
            thread = fallbackFailThread;
        }
        for (PermissionType permissionType : hashMap.keySet()) {
            ((LocationHandler) hashMap.get(permissionType)).complete(locationPoint);
        }
        if (thread != null && !Thread.currentThread().equals(thread)) {
            thread.interrupt();
        }
        if (thread == fallbackFailThread) {
            synchronized (cls) {
                if (thread == fallbackFailThread) {
                    fallbackFailThread = null;
                }
            }
        }
        setLastLocationTime(System.currentTimeMillis());
    }

    /* access modifiers changed from: private */
    public static void fireCompleteForLocation(Location location) {
        LocationPoint locationPoint = new LocationPoint();
        locationPoint.accuracy = Float.valueOf(location.getAccuracy());
        locationPoint.bg = Boolean.valueOf(!OneSignal.isForeground());
        locationPoint.type = Integer.valueOf(locationCoarse ^ true ? 1 : 0);
        locationPoint.timeStamp = Long.valueOf(location.getTime());
        if (locationCoarse) {
            locationPoint.lat = Double.valueOf(new BigDecimal(location.getLatitude()).setScale(7, RoundingMode.HALF_UP).doubleValue());
            locationPoint.log = Double.valueOf(new BigDecimal(location.getLongitude()).setScale(7, RoundingMode.HALF_UP).doubleValue());
        } else {
            locationPoint.lat = Double.valueOf(location.getLatitude());
            locationPoint.log = Double.valueOf(location.getLongitude());
        }
        fireComplete(locationPoint);
        scheduleUpdate(classContext);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x002f, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static void onFocusChange() {
        /*
            java.lang.Object r0 = syncLock
            monitor-enter(r0)
            com.onesignal.GoogleApiClientCompatProxy r1 = mGoogleApiClient     // Catch:{ all -> 0x0030 }
            if (r1 == 0) goto L_0x002e
            com.onesignal.GoogleApiClientCompatProxy r1 = mGoogleApiClient     // Catch:{ all -> 0x0030 }
            com.google.android.gms.common.api.GoogleApiClient r1 = r1.realInstance()     // Catch:{ all -> 0x0030 }
            boolean r1 = r1.isConnected()     // Catch:{ all -> 0x0030 }
            if (r1 != 0) goto L_0x0014
            goto L_0x002e
        L_0x0014:
            com.onesignal.GoogleApiClientCompatProxy r1 = mGoogleApiClient     // Catch:{ all -> 0x0030 }
            com.google.android.gms.common.api.GoogleApiClient r1 = r1.realInstance()     // Catch:{ all -> 0x0030 }
            com.onesignal.LocationGMS$LocationUpdateListener r2 = locationUpdateListener     // Catch:{ all -> 0x0030 }
            if (r2 == 0) goto L_0x0025
            com.google.android.gms.location.FusedLocationProviderApi r2 = com.google.android.gms.location.LocationServices.FusedLocationApi     // Catch:{ all -> 0x0030 }
            com.onesignal.LocationGMS$LocationUpdateListener r3 = locationUpdateListener     // Catch:{ all -> 0x0030 }
            r2.removeLocationUpdates((com.google.android.gms.common.api.GoogleApiClient) r1, (com.google.android.gms.location.LocationListener) r3)     // Catch:{ all -> 0x0030 }
        L_0x0025:
            com.onesignal.LocationGMS$LocationUpdateListener r2 = new com.onesignal.LocationGMS$LocationUpdateListener     // Catch:{ all -> 0x0030 }
            r2.<init>(r1)     // Catch:{ all -> 0x0030 }
            locationUpdateListener = r2     // Catch:{ all -> 0x0030 }
            monitor-exit(r0)     // Catch:{ all -> 0x0030 }
            return
        L_0x002e:
            monitor-exit(r0)     // Catch:{ all -> 0x0030 }
            return
        L_0x0030:
            r1 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x0030 }
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.onesignal.LocationGMS.onFocusChange():void");
    }

    private static class GoogleApiClientListener implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
        private GoogleApiClientListener() {
        }

        /* JADX WARNING: Code restructure failed: missing block: B:18:0x004b, code lost:
            return;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void onConnected(android.os.Bundle r3) {
            /*
                r2 = this;
                java.lang.Object r3 = com.onesignal.LocationGMS.syncLock
                monitor-enter(r3)
                r0 = 0
                com.onesignal.PermissionsActivity.answered = r0     // Catch:{ all -> 0x004c }
                com.onesignal.GoogleApiClientCompatProxy r0 = com.onesignal.LocationGMS.mGoogleApiClient     // Catch:{ all -> 0x004c }
                if (r0 == 0) goto L_0x004a
                com.onesignal.GoogleApiClientCompatProxy r0 = com.onesignal.LocationGMS.mGoogleApiClient     // Catch:{ all -> 0x004c }
                com.google.android.gms.common.api.GoogleApiClient r0 = r0.realInstance()     // Catch:{ all -> 0x004c }
                if (r0 != 0) goto L_0x0017
                goto L_0x004a
            L_0x0017:
                android.location.Location r0 = com.onesignal.LocationGMS.mLastLocation     // Catch:{ all -> 0x004c }
                if (r0 != 0) goto L_0x0039
                com.onesignal.GoogleApiClientCompatProxy r0 = com.onesignal.LocationGMS.mGoogleApiClient     // Catch:{ all -> 0x004c }
                com.google.android.gms.common.api.GoogleApiClient r0 = r0.realInstance()     // Catch:{ all -> 0x004c }
                android.location.Location r0 = com.onesignal.LocationGMS.FusedLocationApiWrapper.getLastLocation(r0)     // Catch:{ all -> 0x004c }
                android.location.Location unused = com.onesignal.LocationGMS.mLastLocation = r0     // Catch:{ all -> 0x004c }
                android.location.Location r0 = com.onesignal.LocationGMS.mLastLocation     // Catch:{ all -> 0x004c }
                if (r0 == 0) goto L_0x0039
                android.location.Location r0 = com.onesignal.LocationGMS.mLastLocation     // Catch:{ all -> 0x004c }
                com.onesignal.LocationGMS.fireCompleteForLocation(r0)     // Catch:{ all -> 0x004c }
            L_0x0039:
                com.onesignal.LocationGMS$LocationUpdateListener r0 = new com.onesignal.LocationGMS$LocationUpdateListener     // Catch:{ all -> 0x004c }
                com.onesignal.GoogleApiClientCompatProxy r1 = com.onesignal.LocationGMS.mGoogleApiClient     // Catch:{ all -> 0x004c }
                com.google.android.gms.common.api.GoogleApiClient r1 = r1.realInstance()     // Catch:{ all -> 0x004c }
                r0.<init>(r1)     // Catch:{ all -> 0x004c }
                com.onesignal.LocationGMS.locationUpdateListener = r0     // Catch:{ all -> 0x004c }
                monitor-exit(r3)     // Catch:{ all -> 0x004c }
                return
            L_0x004a:
                monitor-exit(r3)     // Catch:{ all -> 0x004c }
                return
            L_0x004c:
                r0 = move-exception
                monitor-exit(r3)     // Catch:{ all -> 0x004c }
                throw r0
            */
            throw new UnsupportedOperationException("Method not decompiled: com.onesignal.LocationGMS.GoogleApiClientListener.onConnected(android.os.Bundle):void");
        }

        public void onConnectionSuspended(int i) {
            LocationGMS.fireFailedComplete();
        }

        public void onConnectionFailed(ConnectionResult connectionResult) {
            LocationGMS.fireFailedComplete();
        }
    }

    static class LocationUpdateListener implements LocationListener {
        private GoogleApiClient mGoogleApiClient;

        LocationUpdateListener(GoogleApiClient googleApiClient) {
            this.mGoogleApiClient = googleApiClient;
            long j = OneSignal.isForeground() ? LocationGMS.FOREGROUND_UPDATE_TIME_MS : LocationGMS.BACKGROUND_UPDATE_TIME_MS;
            FusedLocationApiWrapper.requestLocationUpdates(this.mGoogleApiClient, LocationRequest.create().setFastestInterval(j).setInterval(j).setMaxWaitTime((long) (((double) j) * 1.5d)).setPriority(102), this);
        }

        public void onLocationChanged(Location location) {
            Location unused = LocationGMS.mLastLocation = location;
            OneSignal.Log(OneSignal.LOG_LEVEL.INFO, "Location Change Detected");
        }
    }

    static class FusedLocationApiWrapper {
        FusedLocationApiWrapper() {
        }

        static void requestLocationUpdates(GoogleApiClient googleApiClient, LocationRequest locationRequest, LocationListener locationListener) {
            try {
                synchronized (LocationGMS.syncLock) {
                    if (googleApiClient.isConnected()) {
                        LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, locationListener);
                    }
                }
            } catch (Throwable th) {
                OneSignal.Log(OneSignal.LOG_LEVEL.WARN, "FusedLocationApi.requestLocationUpdates failed!", th);
            }
        }

        static Location getLastLocation(GoogleApiClient googleApiClient) {
            synchronized (LocationGMS.syncLock) {
                if (!googleApiClient.isConnected()) {
                    return null;
                }
                Location lastLocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
                return lastLocation;
            }
        }
    }

    private static class LocationHandlerThread extends HandlerThread {
        Handler mHandler = new Handler(getLooper());

        LocationHandlerThread() {
            super("OSH_LocationHandlerThread");
            start();
        }
    }
}
