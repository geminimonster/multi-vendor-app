package com.onesignal;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import com.onesignal.ActivityLifecycleHandler;
import com.onesignal.AndroidSupportV4Compat;

public class PermissionsActivity extends Activity {
    private static final int DELAY_TIME_CALLBACK_CALL = 500;
    private static final int REQUEST_LOCATION = 2;
    private static final String TAG = PermissionsActivity.class.getCanonicalName();
    private static ActivityLifecycleHandler.ActivityAvailableListener activityAvailableListener;
    static boolean answered;
    static boolean waiting;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        OneSignal.setAppContext(this);
        if (bundle == null || !bundle.getBoolean("android:hasCurrentPermissionsRequest", false)) {
            requestPermission();
        } else {
            waiting = true;
        }
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (OneSignal.isInitDone()) {
            requestPermission();
        }
    }

    private void requestPermission() {
        if (Build.VERSION.SDK_INT < 23) {
            finish();
            overridePendingTransition(R.anim.onesignal_fade_in, R.anim.onesignal_fade_out);
        } else if (!waiting) {
            waiting = true;
            AndroidSupportV4Compat.ActivityCompat.requestPermissions(this, new String[]{LocationGMS.requestPermission}, 2);
        }
    }

    public void onRequestPermissionsResult(int i, String[] strArr, final int[] iArr) {
        answered = true;
        waiting = false;
        if (i == 2) {
            new Handler().postDelayed(new Runnable() {
                public void run() {
                    int[] iArr = iArr;
                    boolean z = false;
                    if (iArr.length > 0 && iArr[0] == 0) {
                        z = true;
                    }
                    LocationGMS.sendAndClearPromptHandlers(true, z);
                    if (z) {
                        LocationGMS.startGetLocation();
                    } else {
                        LocationGMS.fireFailedComplete();
                    }
                }
            }, 500);
        }
        ActivityLifecycleHandler.removeActivityAvailableListener(TAG);
        finish();
        overridePendingTransition(R.anim.onesignal_fade_in, R.anim.onesignal_fade_out);
    }

    static void startPrompt() {
        if (!waiting && !answered) {
            AnonymousClass2 r0 = new ActivityLifecycleHandler.ActivityAvailableListener() {
                public void available(Activity activity) {
                    if (!activity.getClass().equals(PermissionsActivity.class)) {
                        Intent intent = new Intent(activity, PermissionsActivity.class);
                        intent.setFlags(131072);
                        activity.startActivity(intent);
                        activity.overridePendingTransition(R.anim.onesignal_fade_in, R.anim.onesignal_fade_out);
                    }
                }
            };
            activityAvailableListener = r0;
            ActivityLifecycleHandler.setActivityAvailableListener(TAG, r0);
        }
    }
}
