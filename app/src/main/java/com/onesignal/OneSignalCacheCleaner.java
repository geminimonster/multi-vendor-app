package com.onesignal;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import com.onesignal.OneSignalDbContract;

class OneSignalCacheCleaner {
    private static String OS_DELETE_OLD_CACHED_DATA = "OS_DELETE_OLD_CACHED_DATA";

    static void cleanInAppMessagingCache() {
    }

    OneSignalCacheCleaner() {
    }

    static synchronized void cleanOldCachedData(final Context context) {
        synchronized (OneSignalCacheCleaner.class) {
            new Thread(new Runnable() {
                public void run() {
                    Thread.currentThread().setPriority(10);
                    SQLiteDatabase writableDbWithRetries = OneSignalDbHelper.getInstance(context).getWritableDbWithRetries();
                    OneSignalCacheCleaner.cleanInAppMessagingCache();
                    OneSignalCacheCleaner.cleanNotificationCache(writableDbWithRetries);
                }
            }, OS_DELETE_OLD_CACHED_DATA).start();
        }
    }

    static void cleanNotificationCache(SQLiteDatabase sQLiteDatabase) {
        cleanOldNotificationData(sQLiteDatabase);
        cleanOldUniqueOutcomeEventNotificationsCache(sQLiteDatabase);
    }

    private static void cleanOldNotificationData(SQLiteDatabase sQLiteDatabase) {
        sQLiteDatabase.delete(OneSignalDbContract.NotificationTable.TABLE_NAME, "created_time < " + ((System.currentTimeMillis() / 1000) - 604800), (String[]) null);
    }

    static void cleanOldUniqueOutcomeEventNotificationsCache(SQLiteDatabase sQLiteDatabase) {
        sQLiteDatabase.delete(OneSignalDbContract.CachedUniqueOutcomeNotificationTable.TABLE_NAME, "NOT EXISTS(SELECT NULL FROM notification n WHERE n.notification_id = notification_id)", (String[]) null);
    }
}
