package com.beloo.widget.chipslayoutmanager.layouter;

import android.graphics.Rect;
import android.util.Pair;
import android.view.View;
import com.beloo.widget.chipslayoutmanager.layouter.AbstractLayouter;

class RTLDownLayouter extends AbstractLayouter {
    private boolean isPurged;

    /* access modifiers changed from: package-private */
    public boolean isReverseOrder() {
        return false;
    }

    private RTLDownLayouter(Builder builder) {
        super(builder);
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    /* access modifiers changed from: package-private */
    public void onPreLayout() {
        if (!this.rowViews.isEmpty()) {
            if (!this.isPurged) {
                this.isPurged = true;
                getCacheStorage().purgeCacheFromPosition(getLayoutManager().getPosition((View) ((Pair) this.rowViews.get(0)).second));
            }
            getCacheStorage().storeRow(this.rowViews);
        }
    }

    /* access modifiers changed from: package-private */
    public void onAfterLayout() {
        this.viewRight = getCanvasRightBorder();
        this.viewTop = this.viewBottom;
    }

    /* access modifiers changed from: package-private */
    public boolean isAttachedViewFromNewRow(View view) {
        return this.viewBottom <= getLayoutManager().getDecoratedTop(view) && getLayoutManager().getDecoratedRight(view) > this.viewRight;
    }

    /* access modifiers changed from: package-private */
    public Rect createViewRect(View view) {
        Rect rect = new Rect(this.viewRight - getCurrentViewWidth(), this.viewTop, this.viewRight, this.viewTop + getCurrentViewHeight());
        this.viewRight = rect.left;
        this.viewBottom = Math.max(this.viewBottom, rect.bottom);
        return rect;
    }

    public void onInterceptAttachView(View view) {
        this.viewTop = getLayoutManager().getDecoratedTop(view);
        this.viewRight = getLayoutManager().getDecoratedLeft(view);
        this.viewBottom = Math.max(this.viewBottom, getLayoutManager().getDecoratedBottom(view));
    }

    public int getStartRowBorder() {
        return getViewTop();
    }

    public int getEndRowBorder() {
        return getViewBottom();
    }

    public int getRowLength() {
        return getCanvasRightBorder() - this.viewRight;
    }

    public static final class Builder extends AbstractLayouter.Builder {
        private Builder() {
        }

        public RTLDownLayouter createLayouter() {
            return new RTLDownLayouter(this);
        }
    }
}
