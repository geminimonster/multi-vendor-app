package com.beloo.widget.chipslayoutmanager;

import android.content.Context;
import android.graphics.Rect;
import android.os.Parcelable;
import android.util.SparseArray;
import android.view.View;
import androidx.recyclerview.widget.RecyclerView;
import com.beloo.widget.chipslayoutmanager.DisappearingViewsManager;
import com.beloo.widget.chipslayoutmanager.ScrollingController;
import com.beloo.widget.chipslayoutmanager.anchor.AnchorViewState;
import com.beloo.widget.chipslayoutmanager.anchor.IAnchorFactory;
import com.beloo.widget.chipslayoutmanager.cache.IViewCacheStorage;
import com.beloo.widget.chipslayoutmanager.cache.ViewCacheFactory;
import com.beloo.widget.chipslayoutmanager.gravity.CenterChildGravity;
import com.beloo.widget.chipslayoutmanager.gravity.CustomGravityResolver;
import com.beloo.widget.chipslayoutmanager.gravity.IChildGravityResolver;
import com.beloo.widget.chipslayoutmanager.layouter.AbstractPositionIterator;
import com.beloo.widget.chipslayoutmanager.layouter.ColumnsStateFactory;
import com.beloo.widget.chipslayoutmanager.layouter.ICanvas;
import com.beloo.widget.chipslayoutmanager.layouter.ILayouter;
import com.beloo.widget.chipslayoutmanager.layouter.IMeasureSupporter;
import com.beloo.widget.chipslayoutmanager.layouter.IStateFactory;
import com.beloo.widget.chipslayoutmanager.layouter.LayouterFactory;
import com.beloo.widget.chipslayoutmanager.layouter.MeasureSupporter;
import com.beloo.widget.chipslayoutmanager.layouter.RowsStateFactory;
import com.beloo.widget.chipslayoutmanager.layouter.breaker.EmptyRowBreaker;
import com.beloo.widget.chipslayoutmanager.layouter.breaker.IRowBreaker;
import com.beloo.widget.chipslayoutmanager.layouter.criteria.AbstractCriteriaFactory;
import com.beloo.widget.chipslayoutmanager.layouter.criteria.InfiniteCriteriaFactory;
import com.beloo.widget.chipslayoutmanager.layouter.placer.PlacerFactory;
import com.beloo.widget.chipslayoutmanager.util.AssertionUtils;
import com.beloo.widget.chipslayoutmanager.util.LayoutManagerUtil;
import com.beloo.widget.chipslayoutmanager.util.log.IFillLogger;
import com.beloo.widget.chipslayoutmanager.util.log.Log;
import com.beloo.widget.chipslayoutmanager.util.log.LoggerFactory;
import com.beloo.widget.chipslayoutmanager.util.testing.EmptySpy;
import com.beloo.widget.chipslayoutmanager.util.testing.ISpy;
import java.util.Iterator;
import java.util.Locale;
import org.bouncycastle.crypto.tls.CipherSuite;

public class ChipsLayoutManager extends RecyclerView.LayoutManager implements IChipsLayoutManagerContract, IStateHolder, ScrollingController.IScrollerListener {
    private static final int APPROXIMATE_ADDITIONAL_ROWS_COUNT = 5;
    private static final float FAST_SCROLLING_COEFFICIENT = 2.0f;
    public static final int HORIZONTAL = 1;
    private static final int INT_ROW_SIZE_APPROXIMATELY_FOR_CACHE = 10;
    public static final int STRATEGY_CENTER = 5;
    public static final int STRATEGY_CENTER_DENSE = 6;
    public static final int STRATEGY_DEFAULT = 1;
    public static final int STRATEGY_FILL_SPACE = 4;
    public static final int STRATEGY_FILL_VIEW = 2;
    private static final String TAG = ChipsLayoutManager.class.getSimpleName();
    public static final int VERTICAL = 2;
    /* access modifiers changed from: private */
    public IAnchorFactory anchorFactory;
    /* access modifiers changed from: private */
    public AnchorViewState anchorView;
    private Integer cacheNormalizationPosition = null;
    /* access modifiers changed from: private */
    public ICanvas canvas;
    /* access modifiers changed from: private */
    public IChildGravityResolver childGravityResolver;
    private SparseArray<View> childViewPositions = new SparseArray<>();
    /* access modifiers changed from: private */
    public ChildViewsIterable childViews = new ChildViewsIterable(this);
    private ParcelableContainer container = new ParcelableContainer();
    /* access modifiers changed from: private */
    public IDisappearingViewsManager disappearingViewsManager;
    private boolean isAfterPreLayout;
    private boolean isLayoutRTL = false;
    private boolean isScrollingEnabledContract = true;
    private boolean isSmoothScrollbarEnabled = false;
    /* access modifiers changed from: private */
    public boolean isStrategyAppliedWithLastRow;
    /* access modifiers changed from: private */
    public int layoutOrientation = 1;
    private IFillLogger logger;
    /* access modifiers changed from: private */
    public Integer maxViewsInRow = null;
    private IMeasureSupporter measureSupporter;
    private int orientation;
    private PlacerFactory placerFactory = new PlacerFactory(this);
    /* access modifiers changed from: private */
    public IRowBreaker rowBreaker = new EmptyRowBreaker();
    /* access modifiers changed from: private */
    public int rowStrategy = 1;
    /* access modifiers changed from: private */
    public IScrollingController scrollingController;
    private ISpy spy = new EmptySpy();
    /* access modifiers changed from: private */
    public IStateFactory stateFactory;
    private SparseArray<View> viewCache = new SparseArray<>();
    private IViewCacheStorage viewPositionsStorage;

    public boolean supportsPredictiveItemAnimations() {
        return true;
    }

    ChipsLayoutManager(Context context) {
        this.orientation = context.getResources().getConfiguration().orientation;
        this.logger = new LoggerFactory().getFillLogger(this.viewCache);
        this.viewPositionsStorage = new ViewCacheFactory(this).createCacheStorage();
        this.measureSupporter = new MeasureSupporter(this);
        setAutoMeasureEnabled(true);
    }

    public static Builder newBuilder(Context context) {
        if (context != null) {
            ChipsLayoutManager chipsLayoutManager = new ChipsLayoutManager(context);
            chipsLayoutManager.getClass();
            return new StrategyBuilder();
        }
        throw new IllegalArgumentException("you have passed null context to builder");
    }

    public IChildGravityResolver getChildGravityResolver() {
        return this.childGravityResolver;
    }

    public void setScrollingEnabledContract(boolean z) {
        this.isScrollingEnabledContract = z;
    }

    public boolean isScrollingEnabledContract() {
        return this.isScrollingEnabledContract;
    }

    public void setMaxViewsInRow(Integer num) {
        if (num.intValue() >= 1) {
            this.maxViewsInRow = num;
            onRuntimeLayoutChanges();
            return;
        }
        throw new IllegalArgumentException("maxViewsInRow should be positive, but is = " + num);
    }

    private void onRuntimeLayoutChanges() {
        this.cacheNormalizationPosition = 0;
        this.viewPositionsStorage.purge();
        requestLayoutWithAnimations();
    }

    public Integer getMaxViewsInRow() {
        return this.maxViewsInRow;
    }

    public IRowBreaker getRowBreaker() {
        return this.rowBreaker;
    }

    public int getRowStrategyType() {
        return this.rowStrategy;
    }

    public boolean isStrategyAppliedWithLastRow() {
        return this.isStrategyAppliedWithLastRow;
    }

    public IViewCacheStorage getViewPositionsStorage() {
        return this.viewPositionsStorage;
    }

    public ICanvas getCanvas() {
        return this.canvas;
    }

    /* access modifiers changed from: package-private */
    public AnchorViewState getAnchor() {
        return this.anchorView;
    }

    /* access modifiers changed from: package-private */
    public void setSpy(ISpy iSpy) {
        this.spy = iSpy;
    }

    public class StrategyBuilder extends Builder {
        public StrategyBuilder() {
            super();
        }

        public Builder withLastRow(boolean z) {
            boolean unused = ChipsLayoutManager.this.isStrategyAppliedWithLastRow = z;
            return this;
        }
    }

    public class Builder {
        private Integer gravity;

        private Builder() {
        }

        public Builder setChildGravity(int i) {
            this.gravity = Integer.valueOf(i);
            return this;
        }

        public Builder setGravityResolver(IChildGravityResolver iChildGravityResolver) {
            AssertionUtils.assertNotNull(iChildGravityResolver, "gravity resolver couldn't be null");
            IChildGravityResolver unused = ChipsLayoutManager.this.childGravityResolver = iChildGravityResolver;
            return this;
        }

        public Builder setScrollingEnabled(boolean z) {
            ChipsLayoutManager.this.setScrollingEnabledContract(z);
            return this;
        }

        public StrategyBuilder setRowStrategy(int i) {
            int unused = ChipsLayoutManager.this.rowStrategy = i;
            return (StrategyBuilder) this;
        }

        public Builder setMaxViewsInRow(int i) {
            if (i >= 1) {
                Integer unused = ChipsLayoutManager.this.maxViewsInRow = Integer.valueOf(i);
                return this;
            }
            throw new IllegalArgumentException("maxViewsInRow should be positive, but is = " + i);
        }

        public Builder setRowBreaker(IRowBreaker iRowBreaker) {
            AssertionUtils.assertNotNull(iRowBreaker, "breaker couldn't be null");
            IRowBreaker unused = ChipsLayoutManager.this.rowBreaker = iRowBreaker;
            return this;
        }

        public Builder setOrientation(int i) {
            if (i != 1 && i != 2) {
                return this;
            }
            int unused = ChipsLayoutManager.this.layoutOrientation = i;
            return this;
        }

        public ChipsLayoutManager build() {
            if (ChipsLayoutManager.this.childGravityResolver == null) {
                Integer num = this.gravity;
                if (num != null) {
                    IChildGravityResolver unused = ChipsLayoutManager.this.childGravityResolver = new CustomGravityResolver(num.intValue());
                } else {
                    IChildGravityResolver unused2 = ChipsLayoutManager.this.childGravityResolver = new CenterChildGravity();
                }
            }
            ChipsLayoutManager chipsLayoutManager = ChipsLayoutManager.this;
            IStateFactory unused3 = chipsLayoutManager.stateFactory = chipsLayoutManager.layoutOrientation == 1 ? new RowsStateFactory(ChipsLayoutManager.this) : new ColumnsStateFactory(ChipsLayoutManager.this);
            ChipsLayoutManager chipsLayoutManager2 = ChipsLayoutManager.this;
            ICanvas unused4 = chipsLayoutManager2.canvas = chipsLayoutManager2.stateFactory.createCanvas();
            ChipsLayoutManager chipsLayoutManager3 = ChipsLayoutManager.this;
            IAnchorFactory unused5 = chipsLayoutManager3.anchorFactory = chipsLayoutManager3.stateFactory.anchorFactory();
            ChipsLayoutManager chipsLayoutManager4 = ChipsLayoutManager.this;
            IScrollingController unused6 = chipsLayoutManager4.scrollingController = chipsLayoutManager4.stateFactory.scrollingController();
            ChipsLayoutManager chipsLayoutManager5 = ChipsLayoutManager.this;
            AnchorViewState unused7 = chipsLayoutManager5.anchorView = chipsLayoutManager5.anchorFactory.createNotFound();
            ChipsLayoutManager chipsLayoutManager6 = ChipsLayoutManager.this;
            IDisappearingViewsManager unused8 = chipsLayoutManager6.disappearingViewsManager = new DisappearingViewsManager(chipsLayoutManager6.canvas, ChipsLayoutManager.this.childViews, ChipsLayoutManager.this.stateFactory);
            return ChipsLayoutManager.this;
        }
    }

    public RecyclerView.LayoutParams generateDefaultLayoutParams() {
        return new RecyclerView.LayoutParams(-2, -2);
    }

    private void requestLayoutWithAnimations() {
        LayoutManagerUtil.requestLayoutWithAnimations(this);
    }

    public void onRestoreInstanceState(Parcelable parcelable) {
        ParcelableContainer parcelableContainer = (ParcelableContainer) parcelable;
        this.container = parcelableContainer;
        this.anchorView = parcelableContainer.getAnchorViewState();
        if (this.orientation != this.container.getOrientation()) {
            int intValue = this.anchorView.getPosition().intValue();
            AnchorViewState createNotFound = this.anchorFactory.createNotFound();
            this.anchorView = createNotFound;
            createNotFound.setPosition(Integer.valueOf(intValue));
        }
        this.viewPositionsStorage.onRestoreInstanceState(this.container.getPositionsCache(this.orientation));
        this.cacheNormalizationPosition = this.container.getNormalizationPosition(this.orientation);
        String str = TAG;
        Log.d(str, "RESTORE. last cache position before cleanup = " + this.viewPositionsStorage.getLastCachePosition());
        Integer num = this.cacheNormalizationPosition;
        if (num != null) {
            this.viewPositionsStorage.purgeCacheFromPosition(num.intValue());
        }
        this.viewPositionsStorage.purgeCacheFromPosition(this.anchorView.getPosition().intValue());
        String str2 = TAG;
        Log.d(str2, "RESTORE. anchor position =" + this.anchorView.getPosition());
        String str3 = TAG;
        Log.d(str3, "RESTORE. layoutOrientation = " + this.orientation + " normalizationPos = " + this.cacheNormalizationPosition);
        String str4 = TAG;
        StringBuilder sb = new StringBuilder();
        sb.append("RESTORE. last cache position = ");
        sb.append(this.viewPositionsStorage.getLastCachePosition());
        Log.d(str4, sb.toString());
    }

    public Parcelable onSaveInstanceState() {
        this.container.putAnchorViewState(this.anchorView);
        this.container.putPositionsCache(this.orientation, this.viewPositionsStorage.onSaveInstanceState());
        this.container.putOrientation(this.orientation);
        String str = TAG;
        Log.d(str, "STORE. last cache position =" + this.viewPositionsStorage.getLastCachePosition());
        Integer num = this.cacheNormalizationPosition;
        if (num == null) {
            num = this.viewPositionsStorage.getLastCachePosition();
        }
        String str2 = TAG;
        Log.d(str2, "STORE. layoutOrientation = " + this.orientation + " normalizationPos = " + num);
        this.container.putNormalizationPosition(this.orientation, num);
        return this.container;
    }

    public int getCompletelyVisibleViewsCount() {
        Iterator<View> it = this.childViews.iterator();
        int i = 0;
        while (it.hasNext()) {
            if (this.canvas.isFullyVisible(it.next())) {
                i++;
            }
        }
        return i;
    }

    public int findFirstVisibleItemPosition() {
        if (getChildCount() == 0) {
            return -1;
        }
        return this.canvas.getMinPositionOnScreen().intValue();
    }

    public int findFirstCompletelyVisibleItemPosition() {
        Iterator<View> it = this.childViews.iterator();
        while (it.hasNext()) {
            View next = it.next();
            Rect viewRect = this.canvas.getViewRect(next);
            if (this.canvas.isFullyVisible(viewRect) && this.canvas.isInside(viewRect)) {
                return getPosition(next);
            }
        }
        return -1;
    }

    public int findLastVisibleItemPosition() {
        if (getChildCount() == 0) {
            return -1;
        }
        return this.canvas.getMaxPositionOnScreen().intValue();
    }

    public int findLastCompletelyVisibleItemPosition() {
        for (int childCount = getChildCount() - 1; childCount >= 0; childCount--) {
            View childAt = getChildAt(childCount);
            if (this.canvas.isFullyVisible(this.canvas.getViewRect(childAt)) && this.canvas.isInside(childAt)) {
                return getPosition(childAt);
            }
        }
        return -1;
    }

    /* access modifiers changed from: package-private */
    public View getChildWithPosition(int i) {
        return this.childViewPositions.get(i);
    }

    public boolean isLayoutRTL() {
        return getLayoutDirection() == 1;
    }

    public int layoutOrientation() {
        return this.layoutOrientation;
    }

    public int getItemCount() {
        return super.getItemCount() + this.disappearingViewsManager.getDeletingItemsOnScreenCount();
    }

    public void onLayoutChildren(RecyclerView.Recycler recycler, RecyclerView.State state) {
        this.spy.onLayoutChildren(recycler, state);
        String str = TAG;
        Log.d(str, "onLayoutChildren. State =" + state);
        if (getItemCount() == 0) {
            detachAndScrapAttachedViews(recycler);
            return;
        }
        Log.i("onLayoutChildren", "isPreLayout = " + state.isPreLayout(), 4);
        if (isLayoutRTL() != this.isLayoutRTL) {
            this.isLayoutRTL = isLayoutRTL();
            detachAndScrapAttachedViews(recycler);
        }
        calcRecyclerCacheSize(recycler);
        if (state.isPreLayout()) {
            int calcDisappearingViewsLength = this.disappearingViewsManager.calcDisappearingViewsLength(recycler);
            Log.d("LayoutManager", "height =" + getHeight(), 4);
            Log.d("onDeletingHeightCalc", "additional height  = " + calcDisappearingViewsLength, 4);
            AnchorViewState anchor = this.anchorFactory.getAnchor();
            this.anchorView = anchor;
            this.anchorFactory.resetRowCoordinates(anchor);
            String str2 = TAG;
            Log.w(str2, "anchor state in pre-layout = " + this.anchorView);
            detachAndScrapAttachedViews(recycler);
            AbstractCriteriaFactory createDefaultFinishingCriteriaFactory = this.stateFactory.createDefaultFinishingCriteriaFactory();
            createDefaultFinishingCriteriaFactory.setAdditionalRowsCount(5);
            createDefaultFinishingCriteriaFactory.setAdditionalLength(calcDisappearingViewsLength);
            LayouterFactory createLayouterFactory = this.stateFactory.createLayouterFactory(createDefaultFinishingCriteriaFactory, this.placerFactory.createRealPlacerFactory());
            this.logger.onBeforeLayouter(this.anchorView);
            fill(recycler, createLayouterFactory.getBackwardLayouter(this.anchorView), createLayouterFactory.getForwardLayouter(this.anchorView));
            this.isAfterPreLayout = true;
        } else {
            detachAndScrapAttachedViews(recycler);
            this.viewPositionsStorage.purgeCacheFromPosition(this.anchorView.getPosition().intValue());
            if (this.cacheNormalizationPosition != null && this.anchorView.getPosition().intValue() <= this.cacheNormalizationPosition.intValue()) {
                this.cacheNormalizationPosition = null;
            }
            AbstractCriteriaFactory createDefaultFinishingCriteriaFactory2 = this.stateFactory.createDefaultFinishingCriteriaFactory();
            createDefaultFinishingCriteriaFactory2.setAdditionalRowsCount(5);
            LayouterFactory createLayouterFactory2 = this.stateFactory.createLayouterFactory(createDefaultFinishingCriteriaFactory2, this.placerFactory.createRealPlacerFactory());
            ILayouter backwardLayouter = createLayouterFactory2.getBackwardLayouter(this.anchorView);
            ILayouter forwardLayouter = createLayouterFactory2.getForwardLayouter(this.anchorView);
            fill(recycler, backwardLayouter, forwardLayouter);
            if (this.scrollingController.normalizeGaps(recycler, (RecyclerView.State) null)) {
                Log.d(TAG, "normalize gaps");
                this.anchorView = this.anchorFactory.getAnchor();
                requestLayoutWithAnimations();
            }
            if (this.isAfterPreLayout) {
                layoutDisappearingViews(recycler, backwardLayouter, forwardLayouter);
            }
            this.isAfterPreLayout = false;
        }
        this.disappearingViewsManager.reset();
        if (!state.isMeasuring()) {
            this.measureSupporter.onSizeChanged();
        }
    }

    public void detachAndScrapAttachedViews(RecyclerView.Recycler recycler) {
        super.detachAndScrapAttachedViews(recycler);
        this.childViewPositions.clear();
    }

    private void layoutDisappearingViews(RecyclerView.Recycler recycler, ILayouter iLayouter, ILayouter iLayouter2) {
        LayouterFactory createLayouterFactory = this.stateFactory.createLayouterFactory(new InfiniteCriteriaFactory(), this.placerFactory.createDisappearingPlacerFactory());
        DisappearingViewsManager.DisappearingViewsContainer disappearingViews = this.disappearingViewsManager.getDisappearingViews(recycler);
        if (disappearingViews.size() > 0) {
            Log.d("disappearing views", "count = " + disappearingViews.size());
            Log.d("fill disappearing views", "");
            ILayouter buildForwardLayouter = createLayouterFactory.buildForwardLayouter(iLayouter2);
            for (int i = 0; i < disappearingViews.getForwardViews().size(); i++) {
                buildForwardLayouter.placeView(recycler.getViewForPosition(disappearingViews.getForwardViews().keyAt(i)));
            }
            buildForwardLayouter.layoutRow();
            ILayouter buildBackwardLayouter = createLayouterFactory.buildBackwardLayouter(iLayouter);
            for (int i2 = 0; i2 < disappearingViews.getBackwardViews().size(); i2++) {
                buildBackwardLayouter.placeView(recycler.getViewForPosition(disappearingViews.getBackwardViews().keyAt(i2)));
            }
            buildBackwardLayouter.layoutRow();
        }
    }

    private void fillCache() {
        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = getChildAt(i);
            this.viewCache.put(getPosition(childAt), childAt);
        }
    }

    private void fill(RecyclerView.Recycler recycler, ILayouter iLayouter, ILayouter iLayouter2) {
        int intValue = this.anchorView.getPosition().intValue();
        fillCache();
        for (int i = 0; i < this.viewCache.size(); i++) {
            detachView(this.viewCache.valueAt(i));
        }
        int i2 = intValue - 1;
        this.logger.onStartLayouter(i2);
        if (this.anchorView.getAnchorViewRect() != null) {
            fillWithLayouter(recycler, iLayouter, i2);
        }
        this.logger.onStartLayouter(intValue);
        fillWithLayouter(recycler, iLayouter2, intValue);
        this.logger.onAfterLayouter();
        for (int i3 = 0; i3 < this.viewCache.size(); i3++) {
            removeAndRecycleView(this.viewCache.valueAt(i3), recycler);
            this.logger.onRemovedAndRecycled(i3);
        }
        this.canvas.findBorderViews();
        buildChildWithPositionsMap();
        this.viewCache.clear();
        this.logger.onAfterRemovingViews();
    }

    private void buildChildWithPositionsMap() {
        this.childViewPositions.clear();
        Iterator<View> it = this.childViews.iterator();
        while (it.hasNext()) {
            View next = it.next();
            this.childViewPositions.put(getPosition(next), next);
        }
    }

    private void fillWithLayouter(RecyclerView.Recycler recycler, ILayouter iLayouter, int i) {
        if (i >= 0) {
            AbstractPositionIterator positionIterator = iLayouter.positionIterator();
            positionIterator.move(i);
            while (true) {
                if (!positionIterator.hasNext()) {
                    break;
                }
                int intValue = ((Integer) positionIterator.next()).intValue();
                View view = this.viewCache.get(intValue);
                if (view == null) {
                    try {
                        View viewForPosition = recycler.getViewForPosition(intValue);
                        this.logger.onItemRequested();
                        if (!iLayouter.placeView(viewForPosition)) {
                            recycler.recycleView(viewForPosition);
                            this.logger.onItemRecycled();
                            break;
                        }
                    } catch (IndexOutOfBoundsException unused) {
                    }
                } else if (!iLayouter.onAttachView(view)) {
                    break;
                } else {
                    this.viewCache.remove(intValue);
                }
            }
            this.logger.onFinishedLayouter();
            iLayouter.layoutRow();
        }
    }

    private void calcRecyclerCacheSize(RecyclerView.Recycler recycler) {
        Integer num = this.maxViewsInRow;
        recycler.setViewCacheSize((int) (((float) (num == null ? 10 : num.intValue())) * FAST_SCROLLING_COEFFICIENT));
    }

    private void performNormalizationIfNeeded() {
        if (this.cacheNormalizationPosition != null && getChildCount() > 0) {
            int position = getPosition(getChildAt(0));
            if (position < this.cacheNormalizationPosition.intValue() || (this.cacheNormalizationPosition.intValue() == 0 && this.cacheNormalizationPosition.intValue() == position)) {
                Log.d("normalization", "position = " + this.cacheNormalizationPosition + " top view position = " + position);
                String str = TAG;
                StringBuilder sb = new StringBuilder();
                sb.append("cache purged from position ");
                sb.append(position);
                Log.d(str, sb.toString());
                this.viewPositionsStorage.purgeCacheFromPosition(position);
                this.cacheNormalizationPosition = null;
                requestLayoutWithAnimations();
            }
        }
    }

    public void setMeasuredDimension(int i, int i2) {
        this.measureSupporter.measure(i, i2);
        String str = TAG;
        Log.i(str, "measured dimension = " + i2);
        super.setMeasuredDimension(this.measureSupporter.getMeasuredWidth(), this.measureSupporter.getMeasuredHeight());
    }

    public void onAdapterChanged(RecyclerView.Adapter adapter, RecyclerView.Adapter adapter2) {
        if (adapter != null && this.measureSupporter.isRegistered()) {
            try {
                this.measureSupporter.setRegistered(false);
                adapter.unregisterAdapterDataObserver((RecyclerView.AdapterDataObserver) this.measureSupporter);
            } catch (IllegalStateException unused) {
            }
        }
        if (adapter2 != null) {
            this.measureSupporter.setRegistered(true);
            adapter2.registerAdapterDataObserver((RecyclerView.AdapterDataObserver) this.measureSupporter);
        }
        removeAllViews();
    }

    public void onItemsRemoved(RecyclerView recyclerView, int i, int i2) {
        Log.d("onItemsRemoved", "starts from = " + i + ", item count = " + i2, 1);
        super.onItemsRemoved(recyclerView, i, i2);
        onLayoutUpdatedFromPosition(i);
        this.measureSupporter.onItemsRemoved(recyclerView);
    }

    public void onItemsAdded(RecyclerView recyclerView, int i, int i2) {
        Log.d("onItemsAdded", "starts from = " + i + ", item count = " + i2, 1);
        super.onItemsAdded(recyclerView, i, i2);
        onLayoutUpdatedFromPosition(i);
    }

    public void onItemsChanged(RecyclerView recyclerView) {
        Log.d("onItemsChanged", "", 1);
        super.onItemsChanged(recyclerView);
        this.viewPositionsStorage.purge();
        onLayoutUpdatedFromPosition(0);
    }

    public void onItemsUpdated(RecyclerView recyclerView, int i, int i2) {
        Log.d("onItemsUpdated", "starts from = " + i + ", item count = " + i2, 1);
        super.onItemsUpdated(recyclerView, i, i2);
        onLayoutUpdatedFromPosition(i);
    }

    public void onItemsUpdated(RecyclerView recyclerView, int i, int i2, Object obj) {
        onItemsUpdated(recyclerView, i, i2);
    }

    public void onItemsMoved(RecyclerView recyclerView, int i, int i2, int i3) {
        Log.d("onItemsMoved", String.format(Locale.US, "from = %d, to = %d, itemCount = %d", new Object[]{Integer.valueOf(i), Integer.valueOf(i2), Integer.valueOf(i3)}), 1);
        super.onItemsMoved(recyclerView, i, i2, i3);
        onLayoutUpdatedFromPosition(Math.min(i, i2));
    }

    private void onLayoutUpdatedFromPosition(int i) {
        String str = TAG;
        Log.d(str, "cache purged from position " + i);
        this.viewPositionsStorage.purgeCacheFromPosition(i);
        int startOfRow = this.viewPositionsStorage.getStartOfRow(i);
        Integer num = this.cacheNormalizationPosition;
        if (num != null) {
            startOfRow = Math.min(num.intValue(), startOfRow);
        }
        this.cacheNormalizationPosition = Integer.valueOf(startOfRow);
    }

    public void setSmoothScrollbarEnabled(boolean z) {
        this.isSmoothScrollbarEnabled = z;
    }

    public boolean isSmoothScrollbarEnabled() {
        return this.isSmoothScrollbarEnabled;
    }

    public void scrollToPosition(int i) {
        if (i >= getItemCount() || i < 0) {
            Log.e("span layout manager", "Cannot scroll to " + i + ", item count " + getItemCount());
            return;
        }
        Integer lastCachePosition = this.viewPositionsStorage.getLastCachePosition();
        Integer num = this.cacheNormalizationPosition;
        if (num == null) {
            num = lastCachePosition;
        }
        this.cacheNormalizationPosition = num;
        if (lastCachePosition != null && i < lastCachePosition.intValue()) {
            i = this.viewPositionsStorage.getStartOfRow(i);
        }
        AnchorViewState createNotFound = this.anchorFactory.createNotFound();
        this.anchorView = createNotFound;
        createNotFound.setPosition(Integer.valueOf(i));
        super.requestLayout();
    }

    public void smoothScrollToPosition(RecyclerView recyclerView, RecyclerView.State state, int i) {
        if (i >= getItemCount() || i < 0) {
            Log.e("span layout manager", "Cannot scroll to " + i + ", item count " + getItemCount());
            return;
        }
        RecyclerView.SmoothScroller createSmoothScroller = this.scrollingController.createSmoothScroller(recyclerView.getContext(), i, CipherSuite.TLS_RSA_WITH_SEED_CBC_SHA, this.anchorView);
        createSmoothScroller.setTargetPosition(i);
        startSmoothScroll(createSmoothScroller);
    }

    public boolean canScrollHorizontally() {
        return this.scrollingController.canScrollHorizontally();
    }

    public boolean canScrollVertically() {
        return this.scrollingController.canScrollVertically();
    }

    public int scrollVerticallyBy(int i, RecyclerView.Recycler recycler, RecyclerView.State state) {
        return this.scrollingController.scrollVerticallyBy(i, recycler, state);
    }

    public int scrollHorizontallyBy(int i, RecyclerView.Recycler recycler, RecyclerView.State state) {
        return this.scrollingController.scrollHorizontallyBy(i, recycler, state);
    }

    public VerticalScrollingController verticalScrollingController() {
        return new VerticalScrollingController(this, this.stateFactory, this);
    }

    public HorizontalScrollingController horizontalScrollingController() {
        return new HorizontalScrollingController(this, this.stateFactory, this);
    }

    public void onScrolled(IScrollingController iScrollingController, RecyclerView.Recycler recycler, RecyclerView.State state) {
        performNormalizationIfNeeded();
        this.anchorView = this.anchorFactory.getAnchor();
        AbstractCriteriaFactory createDefaultFinishingCriteriaFactory = this.stateFactory.createDefaultFinishingCriteriaFactory();
        createDefaultFinishingCriteriaFactory.setAdditionalRowsCount(1);
        LayouterFactory createLayouterFactory = this.stateFactory.createLayouterFactory(createDefaultFinishingCriteriaFactory, this.placerFactory.createRealPlacerFactory());
        fill(recycler, createLayouterFactory.getBackwardLayouter(this.anchorView), createLayouterFactory.getForwardLayouter(this.anchorView));
    }

    public int computeVerticalScrollOffset(RecyclerView.State state) {
        return this.scrollingController.computeVerticalScrollOffset(state);
    }

    public int computeVerticalScrollExtent(RecyclerView.State state) {
        return this.scrollingController.computeVerticalScrollExtent(state);
    }

    public int computeVerticalScrollRange(RecyclerView.State state) {
        return this.scrollingController.computeVerticalScrollRange(state);
    }

    public int computeHorizontalScrollExtent(RecyclerView.State state) {
        return this.scrollingController.computeHorizontalScrollExtent(state);
    }

    public int computeHorizontalScrollOffset(RecyclerView.State state) {
        return this.scrollingController.computeHorizontalScrollOffset(state);
    }

    public int computeHorizontalScrollRange(RecyclerView.State state) {
        return this.scrollingController.computeHorizontalScrollRange(state);
    }
}
