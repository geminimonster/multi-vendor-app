package com.beloo.widget.chipslayoutmanager.layouter.criteria;

public class InfiniteCriteriaFactory extends AbstractCriteriaFactory implements ICriteriaFactory {
    public IFinishingCriteria getBackwardFinishingCriteria() {
        return new InfiniteCriteria();
    }

    public IFinishingCriteria getForwardFinishingCriteria() {
        return new InfiniteCriteria();
    }
}
