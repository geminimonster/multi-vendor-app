package com.beloo.widget.chipslayoutmanager.layouter.placer;

import androidx.recyclerview.widget.RecyclerView;

class DisappearingPlacerFactory implements IPlacerFactory {
    private RecyclerView.LayoutManager layoutManager;

    DisappearingPlacerFactory(RecyclerView.LayoutManager layoutManager2) {
        this.layoutManager = layoutManager2;
    }

    public IPlacer getAtStartPlacer() {
        return new DisappearingViewAtStartPlacer(this.layoutManager);
    }

    public IPlacer getAtEndPlacer() {
        return new DisappearingViewAtEndPlacer(this.layoutManager);
    }
}
