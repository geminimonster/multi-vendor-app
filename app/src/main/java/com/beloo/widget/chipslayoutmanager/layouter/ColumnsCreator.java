package com.beloo.widget.chipslayoutmanager.layouter;

import android.graphics.Rect;
import androidx.recyclerview.widget.RecyclerView;
import com.beloo.widget.chipslayoutmanager.anchor.AnchorViewState;
import com.beloo.widget.chipslayoutmanager.layouter.AbstractLayouter;

class ColumnsCreator implements ILayouterCreator {
    private RecyclerView.LayoutManager layoutManager;

    ColumnsCreator(RecyclerView.LayoutManager layoutManager2) {
        this.layoutManager = layoutManager2;
    }

    public AbstractLayouter.Builder createBackwardBuilder() {
        return LeftLayouter.newBuilder();
    }

    public AbstractLayouter.Builder createForwardBuilder() {
        return RightLayouter.newBuilder();
    }

    public Rect createOffsetRectForBackwardLayouter(AnchorViewState anchorViewState) {
        Rect anchorViewRect = anchorViewState.getAnchorViewRect();
        return new Rect(anchorViewRect == null ? 0 : anchorViewRect.left, 0, anchorViewRect == null ? 0 : anchorViewRect.right, anchorViewRect == null ? 0 : anchorViewRect.top);
    }

    public Rect createOffsetRectForForwardLayouter(AnchorViewState anchorViewState) {
        Rect anchorViewRect = anchorViewState.getAnchorViewRect();
        return new Rect(anchorViewRect == null ? anchorViewState.getPosition().intValue() == 0 ? this.layoutManager.getPaddingLeft() : 0 : anchorViewRect.left, anchorViewRect == null ? this.layoutManager.getPaddingTop() : anchorViewRect.top, anchorViewRect == null ? anchorViewState.getPosition().intValue() == 0 ? this.layoutManager.getPaddingRight() : 0 : anchorViewRect.right, 0);
    }
}
