package com.beloo.widget.chipslayoutmanager.layouter;

import androidx.recyclerview.widget.RecyclerView;

public class MeasureSupporter extends RecyclerView.AdapterDataObserver implements IMeasureSupporter {
    private int autoMeasureHeight = 0;
    private int autoMeasureWidth = 0;
    private Integer beforeRemovingHeight = null;
    private Integer beforeRemovingWidth = null;
    /* access modifiers changed from: private */
    public boolean isAfterRemoving;
    private boolean isRegistered;
    /* access modifiers changed from: private */
    public RecyclerView.LayoutManager lm;
    private int measuredHeight;
    private int measuredWidth;

    public MeasureSupporter(RecyclerView.LayoutManager layoutManager) {
        this.lm = layoutManager;
    }

    public void onSizeChanged() {
        this.autoMeasureWidth = this.lm.getWidth();
        this.autoMeasureHeight = this.lm.getHeight();
    }

    /* access modifiers changed from: package-private */
    public boolean isAfterRemoving() {
        return this.isAfterRemoving;
    }

    public int getMeasuredWidth() {
        return this.measuredWidth;
    }

    private void setMeasuredWidth(int i) {
        this.measuredWidth = i;
    }

    public int getMeasuredHeight() {
        return this.measuredHeight;
    }

    public boolean isRegistered() {
        return this.isRegistered;
    }

    public void setRegistered(boolean z) {
        this.isRegistered = z;
    }

    private void setMeasuredHeight(int i) {
        this.measuredHeight = i;
    }

    public void measure(int i, int i2) {
        if (isAfterRemoving()) {
            setMeasuredWidth(Math.max(i, this.beforeRemovingWidth.intValue()));
            setMeasuredHeight(Math.max(i2, this.beforeRemovingHeight.intValue()));
            return;
        }
        setMeasuredWidth(i);
        setMeasuredHeight(i2);
    }

    public void onItemsRemoved(final RecyclerView recyclerView) {
        this.lm.postOnAnimation(new Runnable() {
            /* access modifiers changed from: private */
            public void onFinished() {
                boolean unused = MeasureSupporter.this.isAfterRemoving = false;
                MeasureSupporter.this.lm.requestLayout();
            }

            public void run() {
                if (recyclerView.getItemAnimator() != null) {
                    recyclerView.getItemAnimator().isRunning(new RecyclerView.ItemAnimator.ItemAnimatorFinishedListener() {
                        public void onAnimationsFinished() {
                            AnonymousClass1.this.onFinished();
                        }
                    });
                } else {
                    onFinished();
                }
            }
        });
    }

    public void onItemRangeRemoved(int i, int i2) {
        super.onItemRangeRemoved(i, i2);
        this.isAfterRemoving = true;
        this.beforeRemovingWidth = Integer.valueOf(this.autoMeasureWidth);
        this.beforeRemovingHeight = Integer.valueOf(this.autoMeasureHeight);
    }
}
