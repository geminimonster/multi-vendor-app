package com.beloo.widget.chipslayoutmanager;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.SparseArray;
import com.beloo.widget.chipslayoutmanager.anchor.AnchorViewState;
import com.beloo.widget.chipslayoutmanager.cache.CacheParcelableContainer;

class ParcelableContainer implements Parcelable {
    public static final Parcelable.Creator<ParcelableContainer> CREATOR = new Parcelable.Creator<ParcelableContainer>() {
        public ParcelableContainer createFromParcel(Parcel parcel) {
            return new ParcelableContainer(parcel);
        }

        public ParcelableContainer[] newArray(int i) {
            return new ParcelableContainer[i];
        }
    };
    private AnchorViewState anchorViewState;
    private SparseArray<Object> cacheNormalizationPositionMap;
    private int orientation;
    private SparseArray<Object> orientationCacheMap;

    public int describeContents() {
        return 0;
    }

    ParcelableContainer() {
        this.orientationCacheMap = new SparseArray<>();
        SparseArray<Object> sparseArray = new SparseArray<>();
        this.cacheNormalizationPositionMap = sparseArray;
        sparseArray.put(1, 0);
        this.cacheNormalizationPositionMap.put(2, 0);
    }

    /* access modifiers changed from: package-private */
    public void putAnchorViewState(AnchorViewState anchorViewState2) {
        this.anchorViewState = anchorViewState2;
    }

    /* access modifiers changed from: package-private */
    public AnchorViewState getAnchorViewState() {
        return this.anchorViewState;
    }

    /* access modifiers changed from: package-private */
    public int getOrientation() {
        return this.orientation;
    }

    /* access modifiers changed from: package-private */
    public void putOrientation(int i) {
        this.orientation = i;
    }

    private ParcelableContainer(Parcel parcel) {
        this.orientationCacheMap = new SparseArray<>();
        this.cacheNormalizationPositionMap = new SparseArray<>();
        this.anchorViewState = AnchorViewState.CREATOR.createFromParcel(parcel);
        this.orientationCacheMap = parcel.readSparseArray(CacheParcelableContainer.class.getClassLoader());
        this.cacheNormalizationPositionMap = parcel.readSparseArray(Integer.class.getClassLoader());
        this.orientation = parcel.readInt();
    }

    public void writeToParcel(Parcel parcel, int i) {
        this.anchorViewState.writeToParcel(parcel, i);
        parcel.writeSparseArray(this.orientationCacheMap);
        parcel.writeSparseArray(this.cacheNormalizationPositionMap);
        parcel.writeInt(this.orientation);
    }

    /* access modifiers changed from: package-private */
    public void putPositionsCache(int i, Parcelable parcelable) {
        this.orientationCacheMap.put(i, parcelable);
    }

    /* access modifiers changed from: package-private */
    public void putNormalizationPosition(int i, Integer num) {
        this.cacheNormalizationPositionMap.put(i, num);
    }

    /* access modifiers changed from: package-private */
    public Parcelable getPositionsCache(int i) {
        return (Parcelable) this.orientationCacheMap.get(i);
    }

    /* access modifiers changed from: package-private */
    public Integer getNormalizationPosition(int i) {
        return (Integer) this.cacheNormalizationPositionMap.get(i);
    }
}
