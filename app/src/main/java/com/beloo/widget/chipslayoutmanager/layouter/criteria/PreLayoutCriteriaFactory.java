package com.beloo.widget.chipslayoutmanager.layouter.criteria;

public class PreLayoutCriteriaFactory implements ICriteriaFactory {
    private int additionalHeight;
    private int additionalRowsCount;

    public PreLayoutCriteriaFactory(int i, int i2) {
        this.additionalHeight = i;
        this.additionalRowsCount = i2;
    }

    public IFinishingCriteria getBackwardFinishingCriteria() {
        return new CriteriaUpAdditionalHeight(new CriteriaUpLayouterFinished(), this.additionalHeight);
    }

    public IFinishingCriteria getForwardFinishingCriteria() {
        return new CriteriaAdditionalRow(new CriteriaDownAdditionalHeight(new CriteriaDownLayouterFinished(), this.additionalHeight), this.additionalRowsCount);
    }
}
