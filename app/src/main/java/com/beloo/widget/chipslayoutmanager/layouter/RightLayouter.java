package com.beloo.widget.chipslayoutmanager.layouter;

import android.graphics.Rect;
import android.util.Pair;
import android.view.View;
import com.beloo.widget.chipslayoutmanager.layouter.AbstractLayouter;

class RightLayouter extends AbstractLayouter {
    private boolean isPurged;

    /* access modifiers changed from: package-private */
    public boolean isReverseOrder() {
        return false;
    }

    private RightLayouter(Builder builder) {
        super(builder);
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    /* access modifiers changed from: package-private */
    public Rect createViewRect(View view) {
        Rect rect = new Rect(this.viewLeft, this.viewTop, this.viewLeft + getCurrentViewWidth(), this.viewTop + getCurrentViewHeight());
        this.viewBottom = rect.bottom;
        this.viewTop = this.viewBottom;
        this.viewRight = Math.max(this.viewRight, rect.right);
        return rect;
    }

    /* access modifiers changed from: package-private */
    public void onPreLayout() {
        if (!this.rowViews.isEmpty()) {
            if (!this.isPurged) {
                this.isPurged = true;
                getCacheStorage().purgeCacheFromPosition(getLayoutManager().getPosition((View) ((Pair) this.rowViews.get(0)).second));
            }
            getCacheStorage().storeRow(this.rowViews);
        }
    }

    /* access modifiers changed from: package-private */
    public void onAfterLayout() {
        this.viewLeft = getViewRight();
        this.viewTop = getCanvasTopBorder();
    }

    /* access modifiers changed from: package-private */
    public boolean isAttachedViewFromNewRow(View view) {
        return this.viewRight <= getLayoutManager().getDecoratedLeft(view) && getLayoutManager().getDecoratedTop(view) < this.viewTop;
    }

    /* access modifiers changed from: package-private */
    public void onInterceptAttachView(View view) {
        this.viewTop = getLayoutManager().getDecoratedBottom(view);
        this.viewLeft = getLayoutManager().getDecoratedLeft(view);
        this.viewRight = Math.max(this.viewRight, getLayoutManager().getDecoratedRight(view));
    }

    public int getStartRowBorder() {
        return getViewLeft();
    }

    public int getEndRowBorder() {
        return getViewRight();
    }

    public int getRowLength() {
        return this.viewTop - getCanvasTopBorder();
    }

    public static final class Builder extends AbstractLayouter.Builder {
        private Builder() {
        }

        public RightLayouter createLayouter() {
            return new RightLayouter(this);
        }
    }
}
