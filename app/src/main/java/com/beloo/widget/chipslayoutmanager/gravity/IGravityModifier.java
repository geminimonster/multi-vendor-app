package com.beloo.widget.chipslayoutmanager.gravity;

import android.graphics.Rect;

public interface IGravityModifier {
    Rect modifyChildRect(int i, int i2, Rect rect);
}
