package com.beloo.widget.chipslayoutmanager;

import android.util.SparseArray;
import android.view.View;
import androidx.recyclerview.widget.RecyclerView;
import com.beloo.widget.chipslayoutmanager.layouter.ICanvas;
import com.beloo.widget.chipslayoutmanager.layouter.IStateFactory;
import java.util.Iterator;
import java.util.List;

class DisappearingViewsManager implements IDisappearingViewsManager {
    private ICanvas canvas;
    private ChildViewsIterable childViews;
    private int deletingItemsOnScreenCount;
    private IStateFactory stateFactory;

    DisappearingViewsManager(ICanvas iCanvas, ChildViewsIterable childViewsIterable, IStateFactory iStateFactory) {
        this.canvas = iCanvas;
        this.childViews = childViewsIterable;
        this.stateFactory = iStateFactory;
    }

    class DisappearingViewsContainer {
        /* access modifiers changed from: private */
        public SparseArray<View> backwardViews = new SparseArray<>();
        /* access modifiers changed from: private */
        public SparseArray<View> forwardViews = new SparseArray<>();

        DisappearingViewsContainer() {
        }

        /* access modifiers changed from: package-private */
        public int size() {
            return this.backwardViews.size() + this.forwardViews.size();
        }

        /* access modifiers changed from: package-private */
        public SparseArray<View> getBackwardViews() {
            return this.backwardViews;
        }

        /* access modifiers changed from: package-private */
        public SparseArray<View> getForwardViews() {
            return this.forwardViews;
        }
    }

    public DisappearingViewsContainer getDisappearingViews(RecyclerView.Recycler recycler) {
        List<RecyclerView.ViewHolder> scrapList = recycler.getScrapList();
        DisappearingViewsContainer disappearingViewsContainer = new DisappearingViewsContainer();
        for (RecyclerView.ViewHolder viewHolder : scrapList) {
            View view = viewHolder.itemView;
            RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams) view.getLayoutParams();
            if (!layoutParams.isItemRemoved()) {
                if (layoutParams.getViewAdapterPosition() < this.canvas.getMinPositionOnScreen().intValue()) {
                    disappearingViewsContainer.backwardViews.put(layoutParams.getViewAdapterPosition(), view);
                } else if (layoutParams.getViewAdapterPosition() > this.canvas.getMaxPositionOnScreen().intValue()) {
                    disappearingViewsContainer.forwardViews.put(layoutParams.getViewAdapterPosition(), view);
                }
            }
        }
        return disappearingViewsContainer;
    }

    public int calcDisappearingViewsLength(RecyclerView.Recycler recycler) {
        int convertPreLayoutPositionToPostLayout;
        Integer num = Integer.MAX_VALUE;
        Integer num2 = Integer.MIN_VALUE;
        Iterator<View> it = this.childViews.iterator();
        while (true) {
            boolean z = false;
            if (!it.hasNext()) {
                break;
            }
            View next = it.next();
            RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams) next.getLayoutParams();
            if (!layoutParams.isItemRemoved() && ((convertPreLayoutPositionToPostLayout = recycler.convertPreLayoutPositionToPostLayout(layoutParams.getViewLayoutPosition())) < this.canvas.getMinPositionOnScreen().intValue() || convertPreLayoutPositionToPostLayout > this.canvas.getMaxPositionOnScreen().intValue())) {
                z = true;
            }
            if (layoutParams.isItemRemoved() || z) {
                this.deletingItemsOnScreenCount++;
                num = Integer.valueOf(Math.min(num.intValue(), this.stateFactory.getStart(next)));
                num2 = Integer.valueOf(Math.max(num2.intValue(), this.stateFactory.getEnd(next)));
            }
        }
        if (num.intValue() != Integer.MAX_VALUE) {
            return num2.intValue() - num.intValue();
        }
        return 0;
    }

    public int getDeletingItemsOnScreenCount() {
        return this.deletingItemsOnScreenCount;
    }

    public void reset() {
        this.deletingItemsOnScreenCount = 0;
    }
}
