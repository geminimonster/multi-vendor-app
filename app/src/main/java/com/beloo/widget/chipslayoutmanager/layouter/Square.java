package com.beloo.widget.chipslayoutmanager.layouter;

import android.graphics.Rect;
import android.view.View;
import androidx.recyclerview.widget.RecyclerView;
import com.beloo.widget.chipslayoutmanager.ChildViewsIterable;
import java.util.Iterator;

abstract class Square implements ICanvas {
    private View bottomView;
    private ChildViewsIterable childViews;
    private boolean isFirstItemAdded;
    private View leftView;
    RecyclerView.LayoutManager lm;
    private Integer maxPositionOnScreen;
    private Integer minPositionOnScreen;
    private View rightView;
    private View topView;

    Square(RecyclerView.LayoutManager layoutManager) {
        this.lm = layoutManager;
        this.childViews = new ChildViewsIterable(layoutManager);
    }

    public Rect getCanvasRect() {
        return new Rect(getCanvasLeftBorder(), getCanvasTopBorder(), getCanvasRightBorder(), getCanvasBottomBorder());
    }

    public Rect getViewRect(View view) {
        return new Rect(this.lm.getDecoratedLeft(view), this.lm.getDecoratedTop(view), this.lm.getDecoratedRight(view), this.lm.getDecoratedBottom(view));
    }

    public boolean isInside(Rect rect) {
        return getCanvasRect().intersect(new Rect(rect));
    }

    public boolean isInside(View view) {
        return isInside(getViewRect(view));
    }

    public boolean isFullyVisible(View view) {
        return isFullyVisible(getViewRect(view));
    }

    public boolean isFullyVisible(Rect rect) {
        return rect.top >= getCanvasTopBorder() && rect.bottom <= getCanvasBottomBorder() && rect.left >= getCanvasLeftBorder() && rect.right <= getCanvasRightBorder();
    }

    public void findBorderViews() {
        this.topView = null;
        this.bottomView = null;
        this.leftView = null;
        this.rightView = null;
        this.minPositionOnScreen = -1;
        this.maxPositionOnScreen = -1;
        this.isFirstItemAdded = false;
        if (this.lm.getChildCount() > 0) {
            View childAt = this.lm.getChildAt(0);
            this.topView = childAt;
            this.bottomView = childAt;
            this.leftView = childAt;
            this.rightView = childAt;
            Iterator<View> it = this.childViews.iterator();
            while (it.hasNext()) {
                View next = it.next();
                int position = this.lm.getPosition(next);
                if (isInside(next)) {
                    if (this.lm.getDecoratedTop(next) < this.lm.getDecoratedTop(this.topView)) {
                        this.topView = next;
                    }
                    if (this.lm.getDecoratedBottom(next) > this.lm.getDecoratedBottom(this.bottomView)) {
                        this.bottomView = next;
                    }
                    if (this.lm.getDecoratedLeft(next) < this.lm.getDecoratedLeft(this.leftView)) {
                        this.leftView = next;
                    }
                    if (this.lm.getDecoratedRight(next) > this.lm.getDecoratedRight(this.rightView)) {
                        this.rightView = next;
                    }
                    if (this.minPositionOnScreen.intValue() == -1 || position < this.minPositionOnScreen.intValue()) {
                        this.minPositionOnScreen = Integer.valueOf(position);
                    }
                    if (this.maxPositionOnScreen.intValue() == -1 || position > this.maxPositionOnScreen.intValue()) {
                        this.maxPositionOnScreen = Integer.valueOf(position);
                    }
                    if (position == 0) {
                        this.isFirstItemAdded = true;
                    }
                }
            }
        }
    }

    public View getTopView() {
        return this.topView;
    }

    public View getBottomView() {
        return this.bottomView;
    }

    public View getLeftView() {
        return this.leftView;
    }

    public View getRightView() {
        return this.rightView;
    }

    public Integer getMinPositionOnScreen() {
        return this.minPositionOnScreen;
    }

    public Integer getMaxPositionOnScreen() {
        return this.maxPositionOnScreen;
    }

    public boolean isFirstItemAdded() {
        return this.isFirstItemAdded;
    }
}
