package com.beloo.widget.chipslayoutmanager;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.SOURCE)
public @interface RowStrategy {
}
