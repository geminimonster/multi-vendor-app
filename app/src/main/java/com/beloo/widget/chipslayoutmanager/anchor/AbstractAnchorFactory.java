package com.beloo.widget.chipslayoutmanager.anchor;

import android.view.View;
import androidx.recyclerview.widget.RecyclerView;
import com.beloo.widget.chipslayoutmanager.layouter.ICanvas;

abstract class AbstractAnchorFactory implements IAnchorFactory {
    private ICanvas canvas;
    RecyclerView.LayoutManager lm;

    AbstractAnchorFactory(RecyclerView.LayoutManager layoutManager, ICanvas iCanvas) {
        this.lm = layoutManager;
        this.canvas = iCanvas;
    }

    /* access modifiers changed from: package-private */
    public ICanvas getCanvas() {
        return this.canvas;
    }

    /* access modifiers changed from: package-private */
    public AnchorViewState createAnchorState(View view) {
        return new AnchorViewState(this.lm.getPosition(view), this.canvas.getViewRect(view));
    }

    public AnchorViewState createNotFound() {
        return AnchorViewState.getNotFoundState();
    }
}
