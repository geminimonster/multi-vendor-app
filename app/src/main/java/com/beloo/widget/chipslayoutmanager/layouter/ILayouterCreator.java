package com.beloo.widget.chipslayoutmanager.layouter;

import android.graphics.Rect;
import com.beloo.widget.chipslayoutmanager.anchor.AnchorViewState;
import com.beloo.widget.chipslayoutmanager.layouter.AbstractLayouter;

interface ILayouterCreator {
    AbstractLayouter.Builder createBackwardBuilder();

    AbstractLayouter.Builder createForwardBuilder();

    Rect createOffsetRectForBackwardLayouter(AnchorViewState anchorViewState);

    Rect createOffsetRectForForwardLayouter(AnchorViewState anchorViewState);
}
