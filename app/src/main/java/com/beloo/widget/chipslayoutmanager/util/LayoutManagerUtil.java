package com.beloo.widget.chipslayoutmanager.util;

import androidx.recyclerview.widget.RecyclerView;

public class LayoutManagerUtil {
    public static void requestLayoutWithAnimations(final RecyclerView.LayoutManager layoutManager) {
        layoutManager.postOnAnimation(new Runnable() {
            public void run() {
                layoutManager.requestLayout();
                layoutManager.requestSimpleAnimationsInNextLayout();
            }
        });
    }
}
