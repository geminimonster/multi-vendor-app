package com.beloo.widget.chipslayoutmanager.gravity;

import com.beloo.widget.chipslayoutmanager.layouter.AbstractLayouter;
import com.beloo.widget.chipslayoutmanager.layouter.Item;
import java.util.List;

class StrategyDecorator implements IRowStrategy {
    private IRowStrategy rowStrategy;

    StrategyDecorator(IRowStrategy iRowStrategy) {
        this.rowStrategy = iRowStrategy;
    }

    public void applyStrategy(AbstractLayouter abstractLayouter, List<Item> list) {
        this.rowStrategy.applyStrategy(abstractLayouter, list);
    }
}
