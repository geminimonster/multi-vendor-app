package com.beloo.widget.chipslayoutmanager.layouter.criteria;

public class RowsCriteriaFactory extends AbstractCriteriaFactory implements ICriteriaFactory {
    public IFinishingCriteria getBackwardFinishingCriteria() {
        CriteriaUpLayouterFinished criteriaUpLayouterFinished = new CriteriaUpLayouterFinished();
        return this.additionalLength != 0 ? new CriteriaUpAdditionalHeight(criteriaUpLayouterFinished, this.additionalLength) : criteriaUpLayouterFinished;
    }

    public IFinishingCriteria getForwardFinishingCriteria() {
        IFinishingCriteria criteriaDownLayouterFinished = new CriteriaDownLayouterFinished();
        if (this.additionalLength != 0) {
            criteriaDownLayouterFinished = new CriteriaDownAdditionalHeight(criteriaDownLayouterFinished, this.additionalLength);
        }
        return this.additionalRowCount != 0 ? new CriteriaAdditionalRow(criteriaDownLayouterFinished, this.additionalRowCount) : criteriaDownLayouterFinished;
    }
}
