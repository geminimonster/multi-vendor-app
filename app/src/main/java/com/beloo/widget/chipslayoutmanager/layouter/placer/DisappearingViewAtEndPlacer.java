package com.beloo.widget.chipslayoutmanager.layouter.placer;

import android.view.View;
import androidx.recyclerview.widget.RecyclerView;

class DisappearingViewAtEndPlacer extends AbstractPlacer {
    DisappearingViewAtEndPlacer(RecyclerView.LayoutManager layoutManager) {
        super(layoutManager);
    }

    public void addView(View view) {
        getLayoutManager().addDisappearingView(view);
    }
}
