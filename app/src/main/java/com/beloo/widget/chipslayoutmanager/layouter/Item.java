package com.beloo.widget.chipslayoutmanager.layouter;

import android.graphics.Rect;

public class Item {
    private int viewPosition;
    private Rect viewRect;

    public Item(Rect rect, int i) {
        this.viewRect = rect;
        this.viewPosition = i;
    }

    public Rect getViewRect() {
        return this.viewRect;
    }

    public int getViewPosition() {
        return this.viewPosition;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj != null && getClass() == obj.getClass() && this.viewPosition == ((Item) obj).viewPosition) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        return this.viewPosition;
    }
}
