package com.beloo.widget.chipslayoutmanager.cache;

import androidx.recyclerview.widget.RecyclerView;

public class ViewCacheFactory {
    private RecyclerView.LayoutManager layoutManager;

    public ViewCacheFactory(RecyclerView.LayoutManager layoutManager2) {
        this.layoutManager = layoutManager2;
    }

    public IViewCacheStorage createCacheStorage() {
        return new ViewCacheStorage(this.layoutManager);
    }
}
