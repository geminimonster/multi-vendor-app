package com.beloo.widget.chipslayoutmanager.layouter;

import androidx.recyclerview.widget.RecyclerView;
import com.beloo.widget.chipslayoutmanager.gravity.IRowStrategyFactory;
import com.beloo.widget.chipslayoutmanager.layouter.breaker.IBreakerFactory;

interface IOrientationStateFactory {
    IBreakerFactory createDefaultBreaker();

    ILayouterCreator createLayouterCreator(RecyclerView.LayoutManager layoutManager);

    IRowStrategyFactory createRowStrategyFactory();
}
