package com.beloo.widget.chipslayoutmanager.util;

import android.text.TextUtils;

public class AssertionUtils {
    private AssertionUtils() {
    }

    public static <T> void assertNotNull(T t, String str) throws AssertionError {
        if (t == null) {
            throw new AssertionError(str + " can't be null.");
        }
    }

    public static <T> void assertInstanceOf(T t, Class<?> cls, String str) throws AssertionError {
        check(!cls.isInstance(t), str + " is not instance of " + cls.getName() + ".");
    }

    public static <T> void assertNotEquals(T t, T t2, String str) throws AssertionError {
        boolean z = t == t2 || t.equals(t2);
        check(z, str + " can't be equal to " + String.valueOf(t2) + ".");
    }

    public static void assertNotEmpty(String str, String str2) throws AssertionError {
        boolean z = TextUtils.isEmpty(str) || TextUtils.isEmpty(str.trim());
        check(z, str2 + " can't be empty.");
    }

    public static void check(boolean z, String str) {
        if (z) {
            throw new AssertionError(str);
        }
    }
}
