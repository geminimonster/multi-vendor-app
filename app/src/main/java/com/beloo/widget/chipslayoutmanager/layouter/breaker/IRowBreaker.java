package com.beloo.widget.chipslayoutmanager.layouter.breaker;

public interface IRowBreaker {
    boolean isItemBreakRow(int i);
}
