package com.beloo.widget.chipslayoutmanager.layouter.criteria;

import com.beloo.widget.chipslayoutmanager.layouter.AbstractLayouter;
import com.beloo.widget.chipslayoutmanager.layouter.ILayouter;
import com.beloo.widget.chipslayoutmanager.layouter.ILayouterListener;
import com.beloo.widget.chipslayoutmanager.layouter.Item;

public class CriteriaPolitePositionReached extends FinishingCriteriaDecorator implements IFinishingCriteria, ILayouterListener {
    private boolean isPositionReached;
    private int reachedPosition;

    CriteriaPolitePositionReached(AbstractLayouter abstractLayouter, IFinishingCriteria iFinishingCriteria, int i) {
        super(iFinishingCriteria);
        this.reachedPosition = i;
        abstractLayouter.addLayouterListener(this);
    }

    public boolean isFinishedLayouting(AbstractLayouter abstractLayouter) {
        return super.isFinishedLayouting(abstractLayouter) || this.isPositionReached;
    }

    public void onLayoutRow(ILayouter iLayouter) {
        if (!this.isPositionReached && iLayouter.getRowSize() != 0) {
            for (Item viewPosition : iLayouter.getCurrentRowItems()) {
                if (viewPosition.getViewPosition() == this.reachedPosition) {
                    this.isPositionReached = true;
                    return;
                }
            }
        }
    }
}
