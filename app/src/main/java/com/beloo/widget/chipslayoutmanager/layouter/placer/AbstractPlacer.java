package com.beloo.widget.chipslayoutmanager.layouter.placer;

import androidx.recyclerview.widget.RecyclerView;

abstract class AbstractPlacer implements IPlacer {
    private RecyclerView.LayoutManager layoutManager;

    AbstractPlacer(RecyclerView.LayoutManager layoutManager2) {
        this.layoutManager = layoutManager2;
    }

    public RecyclerView.LayoutManager getLayoutManager() {
        return this.layoutManager;
    }
}
