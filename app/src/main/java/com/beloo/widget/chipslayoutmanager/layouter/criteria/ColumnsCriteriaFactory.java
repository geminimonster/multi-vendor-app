package com.beloo.widget.chipslayoutmanager.layouter.criteria;

public class ColumnsCriteriaFactory extends AbstractCriteriaFactory {
    public IFinishingCriteria getBackwardFinishingCriteria() {
        CriteriaLeftLayouterFinished criteriaLeftLayouterFinished = new CriteriaLeftLayouterFinished();
        return this.additionalLength != 0 ? new CriteriaLeftAdditionalWidth(criteriaLeftLayouterFinished, this.additionalLength) : criteriaLeftLayouterFinished;
    }

    public IFinishingCriteria getForwardFinishingCriteria() {
        IFinishingCriteria criteriaRightLayouterFinished = new CriteriaRightLayouterFinished();
        if (this.additionalLength != 0) {
            criteriaRightLayouterFinished = new CriteriaRightAdditionalWidth(criteriaRightLayouterFinished, this.additionalLength);
        }
        return this.additionalRowCount != 0 ? new CriteriaAdditionalRow(criteriaRightLayouterFinished, this.additionalRowCount) : criteriaRightLayouterFinished;
    }
}
