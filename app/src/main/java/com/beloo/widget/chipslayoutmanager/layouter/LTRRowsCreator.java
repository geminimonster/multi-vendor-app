package com.beloo.widget.chipslayoutmanager.layouter;

import android.graphics.Rect;
import androidx.recyclerview.widget.RecyclerView;
import com.beloo.widget.chipslayoutmanager.anchor.AnchorViewState;
import com.beloo.widget.chipslayoutmanager.layouter.AbstractLayouter;

class LTRRowsCreator implements ILayouterCreator {
    private RecyclerView.LayoutManager layoutManager;

    LTRRowsCreator(RecyclerView.LayoutManager layoutManager2) {
        this.layoutManager = layoutManager2;
    }

    public Rect createOffsetRectForBackwardLayouter(AnchorViewState anchorViewState) {
        Rect anchorViewRect = anchorViewState.getAnchorViewRect();
        return new Rect(0, anchorViewRect == null ? 0 : anchorViewRect.top, anchorViewRect == null ? 0 : anchorViewRect.left, anchorViewRect == null ? 0 : anchorViewRect.bottom);
    }

    public Rect createOffsetRectForForwardLayouter(AnchorViewState anchorViewState) {
        Rect anchorViewRect = anchorViewState.getAnchorViewRect();
        return new Rect(anchorViewRect == null ? this.layoutManager.getPaddingLeft() : anchorViewRect.left, anchorViewRect == null ? anchorViewState.getPosition().intValue() == 0 ? this.layoutManager.getPaddingTop() : 0 : anchorViewRect.top, 0, anchorViewRect == null ? anchorViewState.getPosition().intValue() == 0 ? this.layoutManager.getPaddingBottom() : 0 : anchorViewRect.bottom);
    }

    public AbstractLayouter.Builder createBackwardBuilder() {
        return LTRUpLayouter.newBuilder();
    }

    public AbstractLayouter.Builder createForwardBuilder() {
        return LTRDownLayouter.newBuilder();
    }
}
