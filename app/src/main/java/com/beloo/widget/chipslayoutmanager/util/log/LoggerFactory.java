package com.beloo.widget.chipslayoutmanager.util.log;

import android.util.SparseArray;
import android.view.View;

public class LoggerFactory {
    public IFillLogger getFillLogger(SparseArray<View> sparseArray) {
        return new FillLogger(sparseArray);
    }
}
