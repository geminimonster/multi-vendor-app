package com.beloo.widget.chipslayoutmanager.layouter.criteria;

public interface ICriteriaFactory {
    IFinishingCriteria getBackwardFinishingCriteria();

    IFinishingCriteria getForwardFinishingCriteria();
}
