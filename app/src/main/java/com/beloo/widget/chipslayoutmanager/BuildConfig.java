package com.beloo.widget.chipslayoutmanager;

public final class BuildConfig {
    public static final String APPLICATION_ID = "com.beloo.widget.chipslayoutmanager";
    public static final String BUILD_TYPE = "release";
    public static final boolean DEBUG = false;
    public static final String FLAVOR = "";
    public static final int VERSION_CODE = 10;
    public static final String VERSION_NAME = "0.3.6";
    public static final boolean isLogEnabled = false;
}
