package com.beloo.widget.chipslayoutmanager.util.log;

import android.util.SparseArray;
import android.view.View;
import com.beloo.widget.chipslayoutmanager.anchor.AnchorViewState;
import java.util.Locale;

class FillLogger implements IFillLogger {
    private int recycledItems;
    private int recycledSize;
    private int requestedItems;
    private int startCacheSize;
    private SparseArray<View> viewCache;

    FillLogger(SparseArray<View> sparseArray) {
        this.viewCache = sparseArray;
    }

    public void onStartLayouter(int i) {
        this.requestedItems = 0;
        this.recycledItems = 0;
        this.startCacheSize = this.viewCache.size();
        Log.d("fillWithLayouter", "start position = " + i, 3);
        Log.d("fillWithLayouter", "cached items = " + this.startCacheSize, 3);
    }

    public void onItemRequested() {
        this.requestedItems++;
    }

    public void onItemRecycled() {
        this.recycledItems++;
    }

    public void onFinishedLayouter() {
        Log.d("fillWithLayouter", String.format(Locale.getDefault(), "reattached items = %d : requested items = %d recycledItems = %d", new Object[]{Integer.valueOf(this.startCacheSize - this.viewCache.size()), Integer.valueOf(this.requestedItems), Integer.valueOf(this.recycledItems)}), 3);
    }

    public void onAfterLayouter() {
        this.recycledSize = this.viewCache.size();
    }

    public void onRemovedAndRecycled(int i) {
        Log.d("fillWithLayouter", " recycle position =" + this.viewCache.keyAt(i), 3);
        this.recycledSize = this.recycledSize + 1;
    }

    public void onAfterRemovingViews() {
        Log.d("fillWithLayouter", "recycled count = " + this.recycledSize, 3);
    }

    public void onBeforeLayouter(AnchorViewState anchorViewState) {
        if (anchorViewState.getAnchorViewRect() != null) {
            Log.d("fill", "anchorPos " + anchorViewState.getPosition(), 3);
            Log.d("fill", "anchorTop " + anchorViewState.getAnchorViewRect().top, 3);
        }
    }
}
