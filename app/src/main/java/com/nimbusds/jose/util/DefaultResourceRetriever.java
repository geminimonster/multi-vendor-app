package com.nimbusds.jose.util;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.Proxy;
import java.net.URL;
import net.jcip.annotations.ThreadSafe;

@ThreadSafe
public class DefaultResourceRetriever extends AbstractRestrictedResourceRetriever implements RestrictedResourceRetriever {
    private boolean disconnectAfterUse;
    private Proxy proxy;

    public DefaultResourceRetriever() {
        this(0, 0);
    }

    public DefaultResourceRetriever(int i, int i2) {
        this(i, i2, 0);
    }

    public DefaultResourceRetriever(int i, int i2, int i3) {
        this(i, i2, i3, true);
    }

    public DefaultResourceRetriever(int i, int i2, int i3, boolean z) {
        super(i, i2, i3);
        this.disconnectAfterUse = z;
    }

    public boolean disconnectsAfterUse() {
        return this.disconnectAfterUse;
    }

    public void setDisconnectsAfterUse(boolean z) {
        this.disconnectAfterUse = z;
    }

    public Proxy getProxy() {
        return this.proxy;
    }

    public void setProxy(Proxy proxy2) {
        this.proxy = proxy2;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:24:0x006a, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x006b, code lost:
        if (r6 != null) goto L_0x006d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:?, code lost:
        r6.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0075, code lost:
        throw r2;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.nimbusds.jose.util.Resource retrieveResource(java.net.URL r6) throws java.io.IOException {
        /*
            r5 = this;
            r0 = 0
            java.net.HttpURLConnection r0 = r5.openConnection(r6)     // Catch:{ ClassCastException -> 0x0078 }
            int r6 = r5.getConnectTimeout()     // Catch:{ ClassCastException -> 0x0078 }
            r0.setConnectTimeout(r6)     // Catch:{ ClassCastException -> 0x0078 }
            int r6 = r5.getReadTimeout()     // Catch:{ ClassCastException -> 0x0078 }
            r0.setReadTimeout(r6)     // Catch:{ ClassCastException -> 0x0078 }
            int r6 = r5.getSizeLimit()     // Catch:{ ClassCastException -> 0x0078 }
            java.io.InputStream r6 = r5.getInputStream(r0, r6)     // Catch:{ ClassCastException -> 0x0078 }
            java.nio.charset.Charset r1 = java.nio.charset.StandardCharsets.UTF_8     // Catch:{ all -> 0x0068 }
            java.lang.String r1 = com.nimbusds.jose.util.IOUtils.readInputStreamToString(r6, r1)     // Catch:{ all -> 0x0068 }
            if (r6 == 0) goto L_0x0026
            r6.close()     // Catch:{ ClassCastException -> 0x0078 }
        L_0x0026:
            int r6 = r0.getResponseCode()     // Catch:{ ClassCastException -> 0x0078 }
            java.lang.String r2 = r0.getResponseMessage()     // Catch:{ ClassCastException -> 0x0078 }
            r3 = 299(0x12b, float:4.19E-43)
            if (r6 > r3) goto L_0x0049
            r3 = 200(0xc8, float:2.8E-43)
            if (r6 < r3) goto L_0x0049
            com.nimbusds.jose.util.Resource r6 = new com.nimbusds.jose.util.Resource     // Catch:{ ClassCastException -> 0x0078 }
            java.lang.String r2 = r0.getContentType()     // Catch:{ ClassCastException -> 0x0078 }
            r6.<init>(r1, r2)     // Catch:{ ClassCastException -> 0x0078 }
            boolean r1 = r5.disconnectAfterUse
            if (r1 == 0) goto L_0x0048
            if (r0 == 0) goto L_0x0048
            r0.disconnect()
        L_0x0048:
            return r6
        L_0x0049:
            java.io.IOException r1 = new java.io.IOException     // Catch:{ ClassCastException -> 0x0078 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ ClassCastException -> 0x0078 }
            r3.<init>()     // Catch:{ ClassCastException -> 0x0078 }
            java.lang.String r4 = "HTTP "
            r3.append(r4)     // Catch:{ ClassCastException -> 0x0078 }
            r3.append(r6)     // Catch:{ ClassCastException -> 0x0078 }
            java.lang.String r6 = ": "
            r3.append(r6)     // Catch:{ ClassCastException -> 0x0078 }
            r3.append(r2)     // Catch:{ ClassCastException -> 0x0078 }
            java.lang.String r6 = r3.toString()     // Catch:{ ClassCastException -> 0x0078 }
            r1.<init>(r6)     // Catch:{ ClassCastException -> 0x0078 }
            throw r1     // Catch:{ ClassCastException -> 0x0078 }
        L_0x0068:
            r1 = move-exception
            throw r1     // Catch:{ all -> 0x006a }
        L_0x006a:
            r2 = move-exception
            if (r6 == 0) goto L_0x0075
            r6.close()     // Catch:{ all -> 0x0071 }
            goto L_0x0075
        L_0x0071:
            r6 = move-exception
            r1.addSuppressed(r6)     // Catch:{ ClassCastException -> 0x0078 }
        L_0x0075:
            throw r2     // Catch:{ ClassCastException -> 0x0078 }
        L_0x0076:
            r6 = move-exception
            goto L_0x0094
        L_0x0078:
            r6 = move-exception
            java.io.IOException r1 = new java.io.IOException     // Catch:{ all -> 0x0076 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x0076 }
            r2.<init>()     // Catch:{ all -> 0x0076 }
            java.lang.String r3 = "Couldn't open HTTP(S) connection: "
            r2.append(r3)     // Catch:{ all -> 0x0076 }
            java.lang.String r3 = r6.getMessage()     // Catch:{ all -> 0x0076 }
            r2.append(r3)     // Catch:{ all -> 0x0076 }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x0076 }
            r1.<init>(r2, r6)     // Catch:{ all -> 0x0076 }
            throw r1     // Catch:{ all -> 0x0076 }
        L_0x0094:
            boolean r1 = r5.disconnectAfterUse
            if (r1 == 0) goto L_0x009d
            if (r0 == 0) goto L_0x009d
            r0.disconnect()
        L_0x009d:
            throw r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.nimbusds.jose.util.DefaultResourceRetriever.retrieveResource(java.net.URL):com.nimbusds.jose.util.Resource");
    }

    /* access modifiers changed from: protected */
    public HttpURLConnection openConnection(URL url) throws IOException {
        Proxy proxy2 = this.proxy;
        if (proxy2 != null) {
            return (HttpURLConnection) url.openConnection(proxy2);
        }
        return (HttpURLConnection) url.openConnection();
    }

    private InputStream getInputStream(HttpURLConnection httpURLConnection, int i) throws IOException {
        InputStream inputStream = httpURLConnection.getInputStream();
        return i > 0 ? new BoundedInputStream(inputStream, (long) getSizeLimit()) : inputStream;
    }
}
