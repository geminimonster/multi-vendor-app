package com.stripe.android;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Parcel;
import android.os.Parcelable;
import com.stripe.android.FingerprintDataRepository;
import kotlin.Metadata;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0006\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\b\b\u0018\u0000 \u00162\u00020\u0001:\u0002\u0016\u0017B\u000f\b\u0000\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\t\u0010\u0007\u001a\u00020\u0003HÆ\u0003J\u0013\u0010\b\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u0003HÆ\u0001J\t\u0010\t\u001a\u00020\nHÖ\u0001J\u0013\u0010\u000b\u001a\u00020\f2\b\u0010\r\u001a\u0004\u0018\u00010\u000eHÖ\u0003J\t\u0010\u000f\u001a\u00020\nHÖ\u0001J\t\u0010\u0010\u001a\u00020\u0003HÖ\u0001J\u0019\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\nHÖ\u0001R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006¨\u0006\u0018"}, d2 = {"Lcom/stripe/android/PaymentConfiguration;", "Landroid/os/Parcelable;", "publishableKey", "", "(Ljava/lang/String;)V", "getPublishableKey", "()Ljava/lang/String;", "component1", "copy", "describeContents", "", "equals", "", "other", "", "hashCode", "toString", "writeToParcel", "", "parcel", "Landroid/os/Parcel;", "flags", "Companion", "Store", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: PaymentConfiguration.kt */
public final class PaymentConfiguration implements Parcelable {
    public static final Parcelable.Creator CREATOR = new Creator();
    public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
    /* access modifiers changed from: private */
    public static PaymentConfiguration instance;
    private final String publishableKey;

    @Metadata(bv = {1, 0, 3}, k = 3, mv = {1, 1, 16})
    public static class Creator implements Parcelable.Creator {
        public final Object createFromParcel(Parcel parcel) {
            Intrinsics.checkParameterIsNotNull(parcel, "in");
            return new PaymentConfiguration(parcel.readString());
        }

        public final Object[] newArray(int i) {
            return new PaymentConfiguration[i];
        }
    }

    public static /* synthetic */ PaymentConfiguration copy$default(PaymentConfiguration paymentConfiguration, String str, int i, Object obj) {
        if ((i & 1) != 0) {
            str = paymentConfiguration.publishableKey;
        }
        return paymentConfiguration.copy(str);
    }

    @JvmStatic
    public static final PaymentConfiguration getInstance(Context context) {
        return Companion.getInstance(context);
    }

    @JvmStatic
    public static final void init(Context context, String str) {
        Companion.init(context, str);
    }

    public final String component1() {
        return this.publishableKey;
    }

    public final PaymentConfiguration copy(String str) {
        Intrinsics.checkParameterIsNotNull(str, "publishableKey");
        return new PaymentConfiguration(str);
    }

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            return (obj instanceof PaymentConfiguration) && Intrinsics.areEqual((Object) this.publishableKey, (Object) ((PaymentConfiguration) obj).publishableKey);
        }
        return true;
    }

    public int hashCode() {
        String str = this.publishableKey;
        if (str != null) {
            return str.hashCode();
        }
        return 0;
    }

    public String toString() {
        return "PaymentConfiguration(publishableKey=" + this.publishableKey + ")";
    }

    public void writeToParcel(Parcel parcel, int i) {
        Intrinsics.checkParameterIsNotNull(parcel, "parcel");
        parcel.writeString(this.publishableKey);
    }

    public PaymentConfiguration(String str) {
        Intrinsics.checkParameterIsNotNull(str, "publishableKey");
        this.publishableKey = str;
        ApiKeyValidator.Companion.get$stripe_release().requireValid(this.publishableKey);
    }

    public final String getPublishableKey() {
        return this.publishableKey;
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\b\u0002\u0018\u0000 \u000f2\u00020\u0001:\u0001\u000fB\u000f\b\u0000\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u000f\u0010\u0007\u001a\u0004\u0018\u00010\bH\u0000¢\u0006\u0002\b\tJ\u0015\u0010\n\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\rH\u0000¢\u0006\u0002\b\u000eR\u000e\u0010\u0005\u001a\u00020\u0006X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0010"}, d2 = {"Lcom/stripe/android/PaymentConfiguration$Store;", "", "context", "Landroid/content/Context;", "(Landroid/content/Context;)V", "prefs", "Landroid/content/SharedPreferences;", "load", "Lcom/stripe/android/PaymentConfiguration;", "load$stripe_release", "save", "", "publishableKey", "", "save$stripe_release", "Companion", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: PaymentConfiguration.kt */
    private static final class Store {
        @Deprecated
        public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
        private static final String KEY_PUBLISHABLE_KEY = "key_publishable_key";
        private static final String NAME = PaymentConfiguration.class.getCanonicalName();
        private final SharedPreferences prefs;

        public Store(Context context) {
            Intrinsics.checkParameterIsNotNull(context, "context");
            SharedPreferences sharedPreferences = context.getApplicationContext().getSharedPreferences(NAME, 0);
            Intrinsics.checkExpressionValueIsNotNull(sharedPreferences, "context.applicationConte…haredPreferences(NAME, 0)");
            this.prefs = sharedPreferences;
        }

        public final /* synthetic */ void save$stripe_release(String str) {
            Intrinsics.checkParameterIsNotNull(str, "publishableKey");
            this.prefs.edit().putString(KEY_PUBLISHABLE_KEY, str).apply();
        }

        public final /* synthetic */ PaymentConfiguration load$stripe_release() {
            String string = this.prefs.getString(KEY_PUBLISHABLE_KEY, (String) null);
            if (string == null) {
                return null;
            }
            Intrinsics.checkExpressionValueIsNotNull(string, "publishableKey");
            return new PaymentConfiguration(string);
        }

        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u0010\u0010\u0005\u001a\u0004\u0018\u00010\u0004X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0006"}, d2 = {"Lcom/stripe/android/PaymentConfiguration$Store$Companion;", "", "()V", "KEY_PUBLISHABLE_KEY", "", "NAME", "stripe_release"}, k = 1, mv = {1, 1, 16})
        /* compiled from: PaymentConfiguration.kt */
        private static final class Companion {
            private Companion() {
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\r\u0010\u0005\u001a\u00020\u0006H\u0000¢\u0006\u0002\b\u0007J\u0010\u0010\b\u001a\u00020\u00042\u0006\u0010\t\u001a\u00020\nH\u0007J\u0018\u0010\u000b\u001a\u00020\u00062\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\f\u001a\u00020\rH\u0007J\u0010\u0010\u000e\u001a\u00020\u00042\u0006\u0010\t\u001a\u00020\nH\u0002R\u0010\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u000e¢\u0006\u0002\n\u0000¨\u0006\u000f"}, d2 = {"Lcom/stripe/android/PaymentConfiguration$Companion;", "", "()V", "instance", "Lcom/stripe/android/PaymentConfiguration;", "clearInstance", "", "clearInstance$stripe_release", "getInstance", "context", "Landroid/content/Context;", "init", "publishableKey", "", "loadInstance", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: PaymentConfiguration.kt */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        @JvmStatic
        public final PaymentConfiguration getInstance(Context context) {
            Intrinsics.checkParameterIsNotNull(context, "context");
            PaymentConfiguration access$getInstance$cp = PaymentConfiguration.instance;
            return access$getInstance$cp != null ? access$getInstance$cp : loadInstance(context);
        }

        private final PaymentConfiguration loadInstance(Context context) {
            PaymentConfiguration load$stripe_release = new Store(context).load$stripe_release();
            if (load$stripe_release != null) {
                PaymentConfiguration.instance = load$stripe_release;
                if (load$stripe_release != null) {
                    return load$stripe_release;
                }
            }
            throw new IllegalStateException("PaymentConfiguration was not initialized. Call PaymentConfiguration.init().");
        }

        @JvmStatic
        public final void init(Context context, String str) {
            Intrinsics.checkParameterIsNotNull(context, "context");
            Intrinsics.checkParameterIsNotNull(str, "publishableKey");
            PaymentConfiguration.instance = new PaymentConfiguration(str);
            new Store(context).save$stripe_release(str);
            new FingerprintDataRepository.Default(context).refresh();
        }

        public final /* synthetic */ void clearInstance$stripe_release() {
            PaymentConfiguration.instance = null;
        }
    }
}
