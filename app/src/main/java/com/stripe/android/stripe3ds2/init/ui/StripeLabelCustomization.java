package com.stripe.android.stripe3ds2.init.ui;

import android.os.Parcel;
import android.os.Parcelable;
import com.stripe.android.stripe3ds2.exceptions.InvalidInputException;
import com.stripe.android.stripe3ds2.utils.CustomizeUtils;
import java.util.Arrays;
import java.util.Objects;
import kotlin.jvm.internal.Intrinsics;

public final class StripeLabelCustomization extends BaseCustomization implements LabelCustomization, Parcelable {
    public static final Parcelable.Creator<StripeLabelCustomization> CREATOR = new a();
    public String d;
    public String e;
    public int f;

    public static class a implements Parcelable.Creator<StripeLabelCustomization> {
        public Object createFromParcel(Parcel parcel) {
            return new StripeLabelCustomization(parcel);
        }

        public Object[] newArray(int i) {
            return new StripeLabelCustomization[i];
        }
    }

    public StripeLabelCustomization() {
    }

    public StripeLabelCustomization(Parcel parcel) {
        super(parcel);
        this.d = parcel.readString();
        this.e = parcel.readString();
        this.f = parcel.readInt();
    }

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (!(obj instanceof StripeLabelCustomization)) {
                return false;
            }
            StripeLabelCustomization stripeLabelCustomization = (StripeLabelCustomization) obj;
            if (Intrinsics.areEqual((Object) this.d, (Object) stripeLabelCustomization.d) && Intrinsics.areEqual((Object) this.e, (Object) stripeLabelCustomization.e) && this.f == stripeLabelCustomization.f) {
                return true;
            }
            return false;
        }
        return true;
    }

    public String getHeadingTextColor() {
        return this.d;
    }

    public String getHeadingTextFontName() {
        return this.e;
    }

    public int getHeadingTextFontSize() {
        return this.f;
    }

    public int hashCode() {
        Object[] objArr = {this.d, this.e, Integer.valueOf(this.f)};
        Intrinsics.checkParameterIsNotNull(objArr, "values");
        return Objects.hash(Arrays.copyOf(objArr, 3));
    }

    public void setHeadingTextColor(String str) throws InvalidInputException {
        this.d = CustomizeUtils.requireValidColor(str);
    }

    public void setHeadingTextFontName(String str) throws InvalidInputException {
        this.e = CustomizeUtils.requireValidString(str);
    }

    public void setHeadingTextFontSize(int i) throws InvalidInputException {
        this.f = CustomizeUtils.requireValidFontSize(i);
    }

    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        parcel.writeString(this.d);
        parcel.writeString(this.e);
        parcel.writeInt(this.f);
    }
}
