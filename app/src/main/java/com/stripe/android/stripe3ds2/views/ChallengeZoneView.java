package com.stripe.android.stripe3ds2.views;

import a.a.a.a.a.g;
import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import androidx.core.widget.CompoundButtonCompat;
import com.stripe.android.stripe3ds2.R;
import com.stripe.android.stripe3ds2.init.ui.ButtonCustomization;
import com.stripe.android.stripe3ds2.init.ui.LabelCustomization;
import java.util.ArrayList;
import java.util.Iterator;
import kotlin.Metadata;
import kotlin.collections.IntIterator;
import kotlin.jvm.internal.Intrinsics;
import kotlin.ranges.IntRange;
import kotlin.ranges.RangesKt;
import kotlin.text.StringsKt;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0000\u0018\u00002\u00020\u0001B%\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\b\b\u0002\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ\u000e\u0010+\u001a\u00020,2\u0006\u0010\t\u001a\u00020-J\u001c\u0010.\u001a\u00020,2\b\u0010/\u001a\u0004\u0018\u0001002\n\b\u0002\u00101\u001a\u0004\u0018\u000102J\u001c\u00103\u001a\u00020,2\b\u00104\u001a\u0004\u0018\u0001002\n\b\u0002\u00101\u001a\u0004\u0018\u000102J\u0010\u00105\u001a\u00020,2\b\b\u0001\u00106\u001a\u00020\u0007J\u0010\u00107\u001a\u00020,2\b\u00108\u001a\u0004\u0018\u000109J\u001c\u0010:\u001a\u00020,2\b\u0010;\u001a\u0004\u0018\u0001002\n\b\u0002\u0010<\u001a\u0004\u0018\u00010=J\u001c\u0010>\u001a\u00020,2\b\u0010?\u001a\u0004\u0018\u0001002\n\b\u0002\u0010<\u001a\u0004\u0018\u00010=J\u0010\u0010@\u001a\u00020,2\b\u00108\u001a\u0004\u0018\u000109J(\u0010A\u001a\u00020,2\b\u0010%\u001a\u0004\u0018\u0001002\n\b\u0002\u00101\u001a\u0004\u0018\u0001022\n\b\u0002\u0010<\u001a\u0004\u0018\u00010=R\u0014\u0010\t\u001a\u00020\nX\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0014\u0010\r\u001a\u00020\u000eX\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010R\u0014\u0010\u0011\u001a\u00020\u0012X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u0014R\u0014\u0010\u0015\u001a\u00020\u0016X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0017\u0010\u0018R\u0014\u0010\u0019\u001a\u00020\u0016X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u001a\u0010\u0018R\u0014\u0010\u001b\u001a\u00020\u001cX\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u001d\u0010\u001eR\u0014\u0010\u001f\u001a\u00020 X\u0004¢\u0006\b\n\u0000\u001a\u0004\b!\u0010\"R\u0014\u0010#\u001a\u00020\u001cX\u0004¢\u0006\b\n\u0000\u001a\u0004\b$\u0010\u001eR\u0014\u0010%\u001a\u00020\u0012X\u0004¢\u0006\b\n\u0000\u001a\u0004\b&\u0010\u0014R\u0014\u0010'\u001a\u00020(8@X\u0004¢\u0006\u0006\u001a\u0004\b)\u0010*¨\u0006B"}, d2 = {"Lcom/stripe/android/stripe3ds2/views/ChallengeZoneView;", "Landroid/widget/LinearLayout;", "context", "Landroid/content/Context;", "attrs", "Landroid/util/AttributeSet;", "defStyleAttr", "", "(Landroid/content/Context;Landroid/util/AttributeSet;I)V", "challengeEntryView", "Landroid/widget/FrameLayout;", "getChallengeEntryView$3ds2sdk_release", "()Landroid/widget/FrameLayout;", "infoHeader", "Lcom/stripe/android/stripe3ds2/views/ThreeDS2HeaderTextView;", "getInfoHeader$3ds2sdk_release", "()Lcom/stripe/android/stripe3ds2/views/ThreeDS2HeaderTextView;", "infoTextView", "Lcom/stripe/android/stripe3ds2/views/ThreeDS2TextView;", "getInfoTextView$3ds2sdk_release", "()Lcom/stripe/android/stripe3ds2/views/ThreeDS2TextView;", "resendButton", "Lcom/stripe/android/stripe3ds2/views/ThreeDS2Button;", "getResendButton$3ds2sdk_release", "()Lcom/stripe/android/stripe3ds2/views/ThreeDS2Button;", "submitButton", "getSubmitButton$3ds2sdk_release", "whitelistNoRadioButton", "Landroid/widget/RadioButton;", "getWhitelistNoRadioButton$3ds2sdk_release", "()Landroid/widget/RadioButton;", "whitelistRadioGroup", "Landroid/widget/RadioGroup;", "getWhitelistRadioGroup$3ds2sdk_release", "()Landroid/widget/RadioGroup;", "whitelistYesRadioButton", "getWhitelistYesRadioButton$3ds2sdk_release", "whitelistingLabel", "getWhitelistingLabel$3ds2sdk_release", "whitelistingSelection", "", "getWhitelistingSelection$3ds2sdk_release", "()Z", "setChallengeEntryView", "", "Landroid/view/View;", "setInfoHeaderText", "headerText", "", "labelCustomization", "Lcom/stripe/android/stripe3ds2/init/ui/LabelCustomization;", "setInfoText", "infoText", "setInfoTextIndicator", "indicatorResId", "setResendButtonClickListener", "listener", "Landroid/view/View$OnClickListener;", "setResendButtonLabel", "resendButtonLabel", "buttonCustomization", "Lcom/stripe/android/stripe3ds2/init/ui/ButtonCustomization;", "setSubmitButton", "submitButtonLabel", "setSubmitButtonClickListener", "setWhitelistingLabel", "3ds2sdk_release"}, k = 1, mv = {1, 1, 16})
public final class ChallengeZoneView extends LinearLayout {

    /* renamed from: a  reason: collision with root package name */
    public final ThreeDS2HeaderTextView f142a;
    public final ThreeDS2TextView b;
    public final ThreeDS2Button c;
    public final ThreeDS2Button d;
    public final ThreeDS2TextView e;
    public final RadioGroup f;
    public final FrameLayout g;
    public final RadioButton h;
    public final RadioButton i;

    public ChallengeZoneView(Context context) {
        this(context, (AttributeSet) null, 0, 6, (DefaultConstructorMarker) null);
    }

    public ChallengeZoneView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0, 4, (DefaultConstructorMarker) null);
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ChallengeZoneView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        Intrinsics.checkParameterIsNotNull(context, "context");
        g a2 = g.a(LayoutInflater.from(context), this);
        Intrinsics.checkExpressionValueIsNotNull(a2, "ChallengeZoneViewBinding…           this\n        )");
        ThreeDS2HeaderTextView threeDS2HeaderTextView = a2.c;
        Intrinsics.checkExpressionValueIsNotNull(threeDS2HeaderTextView, "viewBinding.czvHeader");
        this.f142a = threeDS2HeaderTextView;
        ThreeDS2TextView threeDS2TextView = a2.d;
        Intrinsics.checkExpressionValueIsNotNull(threeDS2TextView, "viewBinding.czvInfo");
        this.b = threeDS2TextView;
        ThreeDS2Button threeDS2Button = a2.f;
        Intrinsics.checkExpressionValueIsNotNull(threeDS2Button, "viewBinding.czvSubmitButton");
        this.c = threeDS2Button;
        ThreeDS2Button threeDS2Button2 = a2.e;
        Intrinsics.checkExpressionValueIsNotNull(threeDS2Button2, "viewBinding.czvResendButton");
        this.d = threeDS2Button2;
        ThreeDS2TextView threeDS2TextView2 = a2.j;
        Intrinsics.checkExpressionValueIsNotNull(threeDS2TextView2, "viewBinding.czvWhitelistingLabel");
        this.e = threeDS2TextView2;
        RadioGroup radioGroup = a2.h;
        Intrinsics.checkExpressionValueIsNotNull(radioGroup, "viewBinding.czvWhitelistRadioGroup");
        this.f = radioGroup;
        FrameLayout frameLayout = a2.b;
        Intrinsics.checkExpressionValueIsNotNull(frameLayout, "viewBinding.czvEntryView");
        this.g = frameLayout;
        RadioButton radioButton = a2.i;
        Intrinsics.checkExpressionValueIsNotNull(radioButton, "viewBinding.czvWhitelistYesButton");
        this.h = radioButton;
        RadioButton radioButton2 = a2.g;
        Intrinsics.checkExpressionValueIsNotNull(radioButton2, "viewBinding.czvWhitelistNoButton");
        this.i = radioButton2;
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ ChallengeZoneView(Context context, AttributeSet attributeSet, int i2, int i3, DefaultConstructorMarker defaultConstructorMarker) {
        this(context, (i3 & 2) != 0 ? null : attributeSet, (i3 & 4) != 0 ? 0 : i2);
    }

    public final void a(String str, ButtonCustomization buttonCustomization) {
        if (!(str == null || StringsKt.isBlank(str))) {
            this.d.setVisibility(0);
            this.d.setText(str);
            this.d.setButtonCustomization(buttonCustomization);
        }
    }

    public final void a(String str, LabelCustomization labelCustomization) {
        if (str == null || StringsKt.isBlank(str)) {
            this.f142a.setVisibility(8);
        } else {
            this.f142a.a(str, labelCustomization);
        }
    }

    public final void a(String str, LabelCustomization labelCustomization, ButtonCustomization buttonCustomization) {
        if (!(str == null || StringsKt.isBlank(str))) {
            this.e.a(str, labelCustomization);
            if (buttonCustomization != null) {
                IntRange until = RangesKt.until(0, this.f.getChildCount());
                ArrayList<RadioButton> arrayList = new ArrayList<>();
                Iterator it = until.iterator();
                while (it.hasNext()) {
                    View childAt = this.f.getChildAt(((IntIterator) it).nextInt());
                    if (!(childAt instanceof RadioButton)) {
                        childAt = null;
                    }
                    RadioButton radioButton = (RadioButton) childAt;
                    if (radioButton != null) {
                        arrayList.add(radioButton);
                    }
                }
                for (RadioButton radioButton2 : arrayList) {
                    String backgroundColor = buttonCustomization.getBackgroundColor();
                    if (!(backgroundColor == null || StringsKt.isBlank(backgroundColor))) {
                        CompoundButtonCompat.setButtonTintList(radioButton2, ColorStateList.valueOf(Color.parseColor(buttonCustomization.getBackgroundColor())));
                    }
                    String textColor = buttonCustomization.getTextColor();
                    if (!(textColor == null || StringsKt.isBlank(textColor))) {
                        radioButton2.setTextColor(Color.parseColor(buttonCustomization.getTextColor()));
                    }
                }
            }
            this.e.setVisibility(0);
            this.f.setVisibility(0);
        }
    }

    public final void b(String str, ButtonCustomization buttonCustomization) {
        if (str == null || StringsKt.isBlank(str)) {
            this.c.setVisibility(8);
            return;
        }
        this.c.setText(str);
        this.c.setButtonCustomization(buttonCustomization);
    }

    public final void b(String str, LabelCustomization labelCustomization) {
        if (str == null || StringsKt.isBlank(str)) {
            this.b.setVisibility(8);
        } else {
            this.b.a(str, labelCustomization);
        }
    }

    public final FrameLayout getChallengeEntryView$3ds2sdk_release() {
        return this.g;
    }

    public final ThreeDS2HeaderTextView getInfoHeader$3ds2sdk_release() {
        return this.f142a;
    }

    public final ThreeDS2TextView getInfoTextView$3ds2sdk_release() {
        return this.b;
    }

    public final ThreeDS2Button getResendButton$3ds2sdk_release() {
        return this.d;
    }

    public final ThreeDS2Button getSubmitButton$3ds2sdk_release() {
        return this.c;
    }

    public final RadioButton getWhitelistNoRadioButton$3ds2sdk_release() {
        return this.i;
    }

    public final RadioGroup getWhitelistRadioGroup$3ds2sdk_release() {
        return this.f;
    }

    public final RadioButton getWhitelistYesRadioButton$3ds2sdk_release() {
        return this.h;
    }

    public final ThreeDS2TextView getWhitelistingLabel$3ds2sdk_release() {
        return this.e;
    }

    public final boolean getWhitelistingSelection$3ds2sdk_release() {
        return this.f.getCheckedRadioButtonId() == R.id.czv_whitelist_yes_button;
    }

    public final void setChallengeEntryView(View view) {
        Intrinsics.checkParameterIsNotNull(view, "challengeEntryView");
        this.g.addView(view);
    }

    public final void setInfoTextIndicator(int i2) {
        this.b.setCompoundDrawablesRelativeWithIntrinsicBounds(i2, 0, 0, 0);
    }

    public final void setResendButtonClickListener(View.OnClickListener onClickListener) {
        this.d.setOnClickListener(onClickListener);
    }

    public final void setSubmitButtonClickListener(View.OnClickListener onClickListener) {
        this.c.setOnClickListener(onClickListener);
    }
}
