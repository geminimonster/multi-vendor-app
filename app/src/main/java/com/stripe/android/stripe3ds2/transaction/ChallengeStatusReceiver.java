package com.stripe.android.stripe3ds2.transaction;

import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;

@Metadata(bv = {1, 0, 3}, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\bf\u0018\u00002\u00020\u0001J\u001e\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00030\u0007H&J&\u0010\b\u001a\u00020\u00032\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u0004\u001a\u00020\u00052\f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00030\u0007H&J\u001e\u0010\u000b\u001a\u00020\u00032\u0006\u0010\f\u001a\u00020\r2\f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00030\u0007H&J\u001e\u0010\u000e\u001a\u00020\u00032\u0006\u0010\u000f\u001a\u00020\u00102\f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00030\u0007H&J\u001e\u0010\u0011\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00030\u0007H&¨\u0006\u0012"}, d2 = {"Lcom/stripe/android/stripe3ds2/transaction/ChallengeStatusReceiver;", "", "cancelled", "", "uiTypeCode", "", "onReceiverCompleted", "Lkotlin/Function0;", "completed", "completionEvent", "Lcom/stripe/android/stripe3ds2/transaction/CompletionEvent;", "protocolError", "protocolErrorEvent", "Lcom/stripe/android/stripe3ds2/transaction/ProtocolErrorEvent;", "runtimeError", "runtimeErrorEvent", "Lcom/stripe/android/stripe3ds2/transaction/RuntimeErrorEvent;", "timedout", "3ds2sdk_release"}, k = 1, mv = {1, 1, 16})
public interface ChallengeStatusReceiver {
    void cancelled(String str, Function0<Unit> function0);

    void completed(CompletionEvent completionEvent, String str, Function0<Unit> function0);

    void protocolError(ProtocolErrorEvent protocolErrorEvent, Function0<Unit> function0);

    void runtimeError(RuntimeErrorEvent runtimeErrorEvent, Function0<Unit> function0);

    void timedout(String str, Function0<Unit> function0);
}
