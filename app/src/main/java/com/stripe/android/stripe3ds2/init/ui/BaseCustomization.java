package com.stripe.android.stripe3ds2.init.ui;

import android.os.Parcel;
import android.os.Parcelable;
import com.stripe.android.stripe3ds2.exceptions.InvalidInputException;
import com.stripe.android.stripe3ds2.utils.CustomizeUtils;

public abstract class BaseCustomization implements Customization, Parcelable {

    /* renamed from: a  reason: collision with root package name */
    public String f119a;
    public String b;
    public int c;

    public BaseCustomization() {
    }

    public BaseCustomization(Parcel parcel) {
        this.f119a = parcel.readString();
        this.b = parcel.readString();
        this.c = parcel.readInt();
    }

    public String getTextColor() {
        return this.b;
    }

    public String getTextFontName() {
        return this.f119a;
    }

    public int getTextFontSize() {
        return this.c;
    }

    public void setTextColor(String str) throws InvalidInputException {
        this.b = CustomizeUtils.requireValidColor(str);
    }

    public void setTextFontName(String str) throws InvalidInputException {
        this.f119a = CustomizeUtils.requireValidString(str);
    }

    public void setTextFontSize(int i) throws InvalidInputException {
        this.c = CustomizeUtils.requireValidFontSize(i);
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.f119a);
        parcel.writeString(this.b);
        parcel.writeInt(this.c);
    }
}
