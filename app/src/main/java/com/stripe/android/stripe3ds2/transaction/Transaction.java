package com.stripe.android.stripe3ds2.transaction;

import android.app.Activity;
import android.app.ProgressDialog;
import com.stripe.android.stripe3ds2.exceptions.InvalidInputException;
import kotlin.Metadata;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000H\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\bf\u0018\u00002\u00020\u0001J\b\u0010\f\u001a\u00020\rH&J(\u0010\u000e\u001a\u00020\r2\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u0016H&J(\u0010\u000e\u001a\u00020\r2\u0006\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u0016H&J\u0010\u0010\u0019\u001a\u00020\u001a2\u0006\u0010\u001b\u001a\u00020\u0010H&R\u0012\u0010\u0002\u001a\u00020\u0003X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0004\u0010\u0005R\u001a\u0010\u0006\u001a\u0004\u0018\u00010\u0007X¦\u000e¢\u0006\f\u001a\u0004\b\b\u0010\t\"\u0004\b\n\u0010\u000b¨\u0006\u001c"}, d2 = {"Lcom/stripe/android/stripe3ds2/transaction/Transaction;", "", "authenticationRequestParameters", "Lcom/stripe/android/stripe3ds2/transaction/AuthenticationRequestParameters;", "getAuthenticationRequestParameters", "()Lcom/stripe/android/stripe3ds2/transaction/AuthenticationRequestParameters;", "initialChallengeUiType", "", "getInitialChallengeUiType", "()Ljava/lang/String;", "setInitialChallengeUiType", "(Ljava/lang/String;)V", "close", "", "doChallenge", "activity", "Landroid/app/Activity;", "challengeParameters", "Lcom/stripe/android/stripe3ds2/transaction/ChallengeParameters;", "challengeStatusReceiver", "Lcom/stripe/android/stripe3ds2/transaction/ChallengeStatusReceiver;", "timeOut", "", "host", "Lcom/stripe/android/stripe3ds2/transaction/Stripe3ds2ActivityStarterHost;", "getProgressView", "Landroid/app/ProgressDialog;", "currentActivity", "3ds2sdk_release"}, k = 1, mv = {1, 1, 16})
public interface Transaction {
    void close();

    void doChallenge(Activity activity, ChallengeParameters challengeParameters, ChallengeStatusReceiver challengeStatusReceiver, int i) throws InvalidInputException;

    void doChallenge(Stripe3ds2ActivityStarterHost stripe3ds2ActivityStarterHost, ChallengeParameters challengeParameters, ChallengeStatusReceiver challengeStatusReceiver, int i) throws InvalidInputException;

    AuthenticationRequestParameters getAuthenticationRequestParameters();

    String getInitialChallengeUiType();

    ProgressDialog getProgressView(Activity activity) throws InvalidInputException;

    void setInitialChallengeUiType(String str);
}
