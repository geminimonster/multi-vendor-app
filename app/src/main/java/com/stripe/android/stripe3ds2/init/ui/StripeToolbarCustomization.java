package com.stripe.android.stripe3ds2.init.ui;

import android.os.Parcel;
import android.os.Parcelable;
import com.stripe.android.stripe3ds2.exceptions.InvalidInputException;
import com.stripe.android.stripe3ds2.utils.CustomizeUtils;
import java.util.Arrays;
import java.util.Objects;
import kotlin.jvm.internal.Intrinsics;

public final class StripeToolbarCustomization extends BaseCustomization implements ToolbarCustomization, Parcelable {
    public static final Parcelable.Creator<StripeToolbarCustomization> CREATOR = new a();
    public String d;
    public String e;
    public String f;
    public String g;

    public static class a implements Parcelable.Creator<StripeToolbarCustomization> {
        public Object createFromParcel(Parcel parcel) {
            return new StripeToolbarCustomization(parcel);
        }

        public Object[] newArray(int i) {
            return new StripeToolbarCustomization[i];
        }
    }

    public StripeToolbarCustomization() {
    }

    public StripeToolbarCustomization(Parcel parcel) {
        super(parcel);
        this.d = parcel.readString();
        this.e = parcel.readString();
        this.f = parcel.readString();
        this.g = parcel.readString();
    }

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (!(obj instanceof StripeToolbarCustomization)) {
                return false;
            }
            StripeToolbarCustomization stripeToolbarCustomization = (StripeToolbarCustomization) obj;
            if (Intrinsics.areEqual((Object) this.d, (Object) stripeToolbarCustomization.d) && Intrinsics.areEqual((Object) this.e, (Object) stripeToolbarCustomization.e) && Intrinsics.areEqual((Object) this.f, (Object) stripeToolbarCustomization.f) && Intrinsics.areEqual((Object) this.g, (Object) stripeToolbarCustomization.g)) {
                return true;
            }
            return false;
        }
        return true;
    }

    public String getBackgroundColor() {
        return this.d;
    }

    public String getButtonText() {
        return this.g;
    }

    public String getHeaderText() {
        return this.f;
    }

    public String getStatusBarColor() {
        return this.e;
    }

    public int hashCode() {
        Object[] objArr = {this.d, this.e, this.f, this.g};
        Intrinsics.checkParameterIsNotNull(objArr, "values");
        return Objects.hash(Arrays.copyOf(objArr, 4));
    }

    public void setBackgroundColor(String str) throws InvalidInputException {
        this.d = CustomizeUtils.requireValidColor(str);
    }

    public void setButtonText(String str) throws InvalidInputException {
        this.g = CustomizeUtils.requireValidString(str);
    }

    public void setHeaderText(String str) throws InvalidInputException {
        this.f = CustomizeUtils.requireValidString(str);
    }

    public void setStatusBarColor(String str) throws InvalidInputException {
        this.e = CustomizeUtils.requireValidColor(str);
    }

    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        parcel.writeString(this.d);
        parcel.writeString(this.e);
        parcel.writeString(this.f);
        parcel.writeString(this.g);
    }
}
