package com.stripe.android.stripe3ds2.transaction;

import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0015\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\b\u0018\u00002\u00020\u0001B5\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0003\u0012\u0006\u0010\u0006\u001a\u00020\u0003\u0012\u0006\u0010\u0007\u001a\u00020\u0003\u0012\u0006\u0010\b\u001a\u00020\u0003¢\u0006\u0002\u0010\tJ\t\u0010\u0011\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0012\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0013\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0014\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0015\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0016\u001a\u00020\u0003HÆ\u0003JE\u0010\u0017\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00032\b\b\u0002\u0010\u0005\u001a\u00020\u00032\b\b\u0002\u0010\u0006\u001a\u00020\u00032\b\b\u0002\u0010\u0007\u001a\u00020\u00032\b\b\u0002\u0010\b\u001a\u00020\u0003HÆ\u0001J\u0013\u0010\u0018\u001a\u00020\u00192\b\u0010\u001a\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u001b\u001a\u00020\u001cHÖ\u0001J\t\u0010\u001d\u001a\u00020\u0003HÖ\u0001R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u0011\u0010\b\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\u000bR\u0011\u0010\u0005\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000bR\u0011\u0010\u0007\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u000bR\u0011\u0010\u0006\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u000bR\u0011\u0010\u0004\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u000b¨\u0006\u001e"}, d2 = {"Lcom/stripe/android/stripe3ds2/transaction/AuthenticationRequestParameters;", "", "deviceData", "", "sdkTransactionId", "sdkAppId", "sdkReferenceNumber", "sdkEphemeralPublicKey", "messageVersion", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", "getDeviceData", "()Ljava/lang/String;", "getMessageVersion", "getSdkAppId", "getSdkEphemeralPublicKey", "getSdkReferenceNumber", "getSdkTransactionId", "component1", "component2", "component3", "component4", "component5", "component6", "copy", "equals", "", "other", "hashCode", "", "toString", "3ds2sdk_release"}, k = 1, mv = {1, 1, 16})
public final class AuthenticationRequestParameters {
    public final String deviceData;
    public final String messageVersion;
    public final String sdkAppId;
    public final String sdkEphemeralPublicKey;
    public final String sdkReferenceNumber;
    public final String sdkTransactionId;

    public AuthenticationRequestParameters(String str, String str2, String str3, String str4, String str5, String str6) {
        Intrinsics.checkParameterIsNotNull(str, "deviceData");
        Intrinsics.checkParameterIsNotNull(str2, "sdkTransactionId");
        Intrinsics.checkParameterIsNotNull(str3, "sdkAppId");
        Intrinsics.checkParameterIsNotNull(str4, "sdkReferenceNumber");
        Intrinsics.checkParameterIsNotNull(str5, "sdkEphemeralPublicKey");
        Intrinsics.checkParameterIsNotNull(str6, "messageVersion");
        this.deviceData = str;
        this.sdkTransactionId = str2;
        this.sdkAppId = str3;
        this.sdkReferenceNumber = str4;
        this.sdkEphemeralPublicKey = str5;
        this.messageVersion = str6;
    }

    public static /* synthetic */ AuthenticationRequestParameters copy$default(AuthenticationRequestParameters authenticationRequestParameters, String str, String str2, String str3, String str4, String str5, String str6, int i, Object obj) {
        if ((i & 1) != 0) {
            str = authenticationRequestParameters.deviceData;
        }
        if ((i & 2) != 0) {
            str2 = authenticationRequestParameters.sdkTransactionId;
        }
        String str7 = str2;
        if ((i & 4) != 0) {
            str3 = authenticationRequestParameters.sdkAppId;
        }
        String str8 = str3;
        if ((i & 8) != 0) {
            str4 = authenticationRequestParameters.sdkReferenceNumber;
        }
        String str9 = str4;
        if ((i & 16) != 0) {
            str5 = authenticationRequestParameters.sdkEphemeralPublicKey;
        }
        String str10 = str5;
        if ((i & 32) != 0) {
            str6 = authenticationRequestParameters.messageVersion;
        }
        return authenticationRequestParameters.copy(str, str7, str8, str9, str10, str6);
    }

    public final String component1() {
        return this.deviceData;
    }

    public final String component2() {
        return this.sdkTransactionId;
    }

    public final String component3() {
        return this.sdkAppId;
    }

    public final String component4() {
        return this.sdkReferenceNumber;
    }

    public final String component5() {
        return this.sdkEphemeralPublicKey;
    }

    public final String component6() {
        return this.messageVersion;
    }

    public final AuthenticationRequestParameters copy(String str, String str2, String str3, String str4, String str5, String str6) {
        Intrinsics.checkParameterIsNotNull(str, "deviceData");
        Intrinsics.checkParameterIsNotNull(str2, "sdkTransactionId");
        Intrinsics.checkParameterIsNotNull(str3, "sdkAppId");
        Intrinsics.checkParameterIsNotNull(str4, "sdkReferenceNumber");
        Intrinsics.checkParameterIsNotNull(str5, "sdkEphemeralPublicKey");
        Intrinsics.checkParameterIsNotNull(str6, "messageVersion");
        return new AuthenticationRequestParameters(str, str2, str3, str4, str5, str6);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof AuthenticationRequestParameters)) {
            return false;
        }
        AuthenticationRequestParameters authenticationRequestParameters = (AuthenticationRequestParameters) obj;
        return Intrinsics.areEqual((Object) this.deviceData, (Object) authenticationRequestParameters.deviceData) && Intrinsics.areEqual((Object) this.sdkTransactionId, (Object) authenticationRequestParameters.sdkTransactionId) && Intrinsics.areEqual((Object) this.sdkAppId, (Object) authenticationRequestParameters.sdkAppId) && Intrinsics.areEqual((Object) this.sdkReferenceNumber, (Object) authenticationRequestParameters.sdkReferenceNumber) && Intrinsics.areEqual((Object) this.sdkEphemeralPublicKey, (Object) authenticationRequestParameters.sdkEphemeralPublicKey) && Intrinsics.areEqual((Object) this.messageVersion, (Object) authenticationRequestParameters.messageVersion);
    }

    public final String getDeviceData() {
        return this.deviceData;
    }

    public final String getMessageVersion() {
        return this.messageVersion;
    }

    public final String getSdkAppId() {
        return this.sdkAppId;
    }

    public final String getSdkEphemeralPublicKey() {
        return this.sdkEphemeralPublicKey;
    }

    public final String getSdkReferenceNumber() {
        return this.sdkReferenceNumber;
    }

    public final String getSdkTransactionId() {
        return this.sdkTransactionId;
    }

    public int hashCode() {
        String str = this.deviceData;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.sdkTransactionId;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.sdkAppId;
        int hashCode3 = (hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31;
        String str4 = this.sdkReferenceNumber;
        int hashCode4 = (hashCode3 + (str4 != null ? str4.hashCode() : 0)) * 31;
        String str5 = this.sdkEphemeralPublicKey;
        int hashCode5 = (hashCode4 + (str5 != null ? str5.hashCode() : 0)) * 31;
        String str6 = this.messageVersion;
        if (str6 != null) {
            i = str6.hashCode();
        }
        return hashCode5 + i;
    }

    public String toString() {
        return "AuthenticationRequestParameters(deviceData=" + this.deviceData + ", sdkTransactionId=" + this.sdkTransactionId + ", sdkAppId=" + this.sdkAppId + ", sdkReferenceNumber=" + this.sdkReferenceNumber + ", sdkEphemeralPublicKey=" + this.sdkEphemeralPublicKey + ", messageVersion=" + this.messageVersion + ")";
    }
}
