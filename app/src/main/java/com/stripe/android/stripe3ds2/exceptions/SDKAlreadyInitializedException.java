package com.stripe.android.stripe3ds2.exceptions;

import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\u00060\u0001j\u0002`\u0002B\u0011\u0012\n\u0010\u0003\u001a\u00060\u0001j\u0002`\u0002¢\u0006\u0002\u0010\u0004¨\u0006\u0005"}, d2 = {"Lcom/stripe/android/stripe3ds2/exceptions/SDKAlreadyInitializedException;", "Ljava/lang/RuntimeException;", "Lkotlin/RuntimeException;", "ex", "(Ljava/lang/RuntimeException;)V", "3ds2sdk_release"}, k = 1, mv = {1, 1, 16})
public final class SDKAlreadyInitializedException extends RuntimeException {
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SDKAlreadyInitializedException(RuntimeException runtimeException) {
        super(runtimeException.getMessage(), runtimeException.getCause());
        Intrinsics.checkParameterIsNotNull(runtimeException, "ex");
    }
}
