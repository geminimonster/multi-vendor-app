package com.stripe.android.stripe3ds2.transactions;

import a.a.a.a.d.v;
import a.a.a.a.e.d;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Base64;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.ults.listeners.ChallengeType;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import kotlin.Metadata;
import kotlin.Result;
import kotlin.ResultKt;
import kotlin.collections.CollectionsKt;
import kotlin.collections.SetsKt;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.text.Charsets;
import kotlin.text.StringsKt;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000`\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0006\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\bM\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\b\b\u0018\u0000 t2\u00020\u0001:\u0004stuvBÍ\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\n\b\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\u0007\u001a\u0004\u0018\u00010\b\u0012\b\b\u0002\u0010\t\u001a\u00020\n\u0012\n\b\u0002\u0010\u000b\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\f\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\r\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\u000e\u001a\u0004\u0018\u00010\u0003\u0012\b\b\u0002\u0010\u000f\u001a\u00020\n\u0012\u0010\b\u0002\u0010\u0010\u001a\n\u0012\u0004\u0012\u00020\u0012\u0018\u00010\u0011\u0012\n\b\u0002\u0010\u0013\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\u0014\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\u0015\u001a\u0004\u0018\u00010\u0016\u0012\u0010\b\u0002\u0010\u0017\u001a\n\u0012\u0004\u0012\u00020\u0018\u0018\u00010\u0011\u0012\u0006\u0010\u0019\u001a\u00020\u0003\u0012\n\b\u0002\u0010\u001a\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\u001b\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\u001c\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\u001d\u001a\u0004\u0018\u00010\u0016\u0012\n\b\u0002\u0010\u001e\u001a\u0004\u0018\u00010\u0003\u0012\u0006\u0010\u001f\u001a\u00020\u0003\u0012\n\b\u0002\u0010 \u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010!\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\"\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010#\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010$\u001a\u0004\u0018\u00010\u0003¢\u0006\u0002\u0010%J\t\u0010H\u001a\u00020\u0003HÆ\u0003J\u000b\u0010I\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\t\u0010J\u001a\u00020\nHÆ\u0003J\u0011\u0010K\u001a\n\u0012\u0004\u0012\u00020\u0012\u0018\u00010\u0011HÆ\u0003J\u000b\u0010L\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000b\u0010M\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000b\u0010N\u001a\u0004\u0018\u00010\u0016HÆ\u0003J\u0011\u0010O\u001a\n\u0012\u0004\u0012\u00020\u0018\u0018\u00010\u0011HÆ\u0003J\t\u0010P\u001a\u00020\u0003HÆ\u0003J\u000b\u0010Q\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000b\u0010R\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\t\u0010S\u001a\u00020\u0003HÆ\u0003J\u000b\u0010T\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000b\u0010U\u001a\u0004\u0018\u00010\u0016HÆ\u0003J\u000b\u0010V\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\t\u0010W\u001a\u00020\u0003HÆ\u0003J\u000b\u0010X\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000b\u0010Y\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000b\u0010Z\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000b\u0010[\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000b\u0010\\\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000b\u0010]\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000b\u0010^\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000b\u0010_\u001a\u0004\u0018\u00010\bHÆ\u0003J\t\u0010`\u001a\u00020\nHÆ\u0003J\u000b\u0010a\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000b\u0010b\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000b\u0010c\u001a\u0004\u0018\u00010\u0003HÆ\u0003JÙ\u0002\u0010d\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00032\n\b\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u0007\u001a\u0004\u0018\u00010\b2\b\b\u0002\u0010\t\u001a\u00020\n2\n\b\u0002\u0010\u000b\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\f\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\r\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u000e\u001a\u0004\u0018\u00010\u00032\b\b\u0002\u0010\u000f\u001a\u00020\n2\u0010\b\u0002\u0010\u0010\u001a\n\u0012\u0004\u0012\u00020\u0012\u0018\u00010\u00112\n\b\u0002\u0010\u0013\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u0014\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u0015\u001a\u0004\u0018\u00010\u00162\u0010\b\u0002\u0010\u0017\u001a\n\u0012\u0004\u0012\u00020\u0018\u0018\u00010\u00112\b\b\u0002\u0010\u0019\u001a\u00020\u00032\n\b\u0002\u0010\u001a\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u001b\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u001c\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u001d\u001a\u0004\u0018\u00010\u00162\n\b\u0002\u0010\u001e\u001a\u0004\u0018\u00010\u00032\b\b\u0002\u0010\u001f\u001a\u00020\u00032\n\b\u0002\u0010 \u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010!\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\"\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010#\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010$\u001a\u0004\u0018\u00010\u0003HÆ\u0001J\t\u0010e\u001a\u00020fHÖ\u0001J\u0013\u0010g\u001a\u00020\n2\b\u0010h\u001a\u0004\u0018\u00010iHÖ\u0003J\t\u0010j\u001a\u00020fHÖ\u0001J\u0006\u0010k\u001a\u00020lJ\t\u0010m\u001a\u00020\u0003HÖ\u0001J\u0019\u0010n\u001a\u00020o2\u0006\u0010p\u001a\u00020q2\u0006\u0010r\u001a\u00020fHÖ\u0001R\u0013\u0010\u0005\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b&\u0010'R\u0013\u0010\u0006\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b(\u0010'R\u0011\u0010\u0004\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b)\u0010'R\u0013\u0010\u000e\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b*\u0010'R\u0013\u0010\u000b\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b+\u0010'R\u0013\u0010\f\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b,\u0010'R\u0013\u0010\r\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b-\u0010'R\u0019\u0010\u0010\u001a\n\u0012\u0004\u0012\u00020\u0012\u0018\u00010\u0011¢\u0006\b\n\u0000\u001a\u0004\b.\u0010/R\u0013\u0010\u0013\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b0\u0010'R\u0013\u0010\u0014\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b1\u0010'R\u0011\u0010\t\u001a\u00020\n¢\u0006\b\n\u0000\u001a\u0004\b\t\u00102R\u0014\u00103\u001a\u00020\n8AX\u0004¢\u0006\u0006\u001a\u0004\b4\u00102R\u0013\u0010\u0015\u001a\u0004\u0018\u00010\u0016¢\u0006\b\n\u0000\u001a\u0004\b5\u00106R\u0019\u0010\u0017\u001a\n\u0012\u0004\u0012\u00020\u0018\u0018\u00010\u0011¢\u0006\b\n\u0000\u001a\u0004\b7\u0010/R\u0011\u0010\u0019\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b8\u0010'R\u0013\u0010\u001b\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b9\u0010'R\u0013\u0010\u001a\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b:\u0010'R\u0013\u0010\u001c\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b;\u0010'R\u0013\u0010\u001d\u001a\u0004\u0018\u00010\u0016¢\u0006\b\n\u0000\u001a\u0004\b<\u00106R\u0013\u0010\u001e\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b=\u0010'R\u0011\u0010\u001f\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b>\u0010'R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b?\u0010'R\u0011\u0010\u000f\u001a\u00020\n¢\u0006\b\n\u0000\u001a\u0004\b@\u00102R\u0013\u0010 \u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\bA\u0010'R\u0013\u0010$\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\bB\u0010'R\u0013\u0010\u0007\u001a\u0004\u0018\u00010\b¢\u0006\b\n\u0000\u001a\u0004\bC\u0010DR\u0013\u0010!\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\bE\u0010'R\u0013\u0010\"\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\bF\u0010'R\u0013\u0010#\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\bG\u0010'¨\u0006w"}, d2 = {"Lcom/stripe/android/stripe3ds2/transactions/ChallengeResponseData;", "Landroid/os/Parcelable;", "serverTransId", "", "acsTransId", "acsHtml", "acsHtmlRefresh", "uiType", "Lcom/stripe/android/stripe3ds2/transactions/ChallengeResponseData$UiType;", "isChallengeCompleted", "", "challengeInfoHeader", "challengeInfoLabel", "challengeInfoText", "challengeAdditionalInfoText", "shouldShowChallengeInfoTextIndicator", "challengeSelectOptions", "", "Lcom/stripe/android/stripe3ds2/transactions/ChallengeResponseData$ChallengeSelectOption;", "expandInfoLabel", "expandInfoText", "issuerImage", "Lcom/stripe/android/stripe3ds2/transactions/ChallengeResponseData$Image;", "messageExtensions", "Lcom/stripe/android/stripe3ds2/transactions/MessageExtension;", "messageVersion", "oobAppUrl", "oobAppLabel", "oobContinueLabel", "paymentSystemImage", "resendInformationLabel", "sdkTransId", "submitAuthenticationLabel", "whitelistingInfoText", "whyInfoLabel", "whyInfoText", "transStatus", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/stripe/android/stripe3ds2/transactions/ChallengeResponseData$UiType;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/util/List;Ljava/lang/String;Ljava/lang/String;Lcom/stripe/android/stripe3ds2/transactions/ChallengeResponseData$Image;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/stripe/android/stripe3ds2/transactions/ChallengeResponseData$Image;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", "getAcsHtml", "()Ljava/lang/String;", "getAcsHtmlRefresh", "getAcsTransId", "getChallengeAdditionalInfoText", "getChallengeInfoHeader", "getChallengeInfoLabel", "getChallengeInfoText", "getChallengeSelectOptions", "()Ljava/util/List;", "getExpandInfoLabel", "getExpandInfoText", "()Z", "isValidForUi", "isValidForUi$3ds2sdk_release", "getIssuerImage", "()Lcom/stripe/android/stripe3ds2/transactions/ChallengeResponseData$Image;", "getMessageExtensions", "getMessageVersion", "getOobAppLabel", "getOobAppUrl", "getOobContinueLabel", "getPaymentSystemImage", "getResendInformationLabel", "getSdkTransId", "getServerTransId", "getShouldShowChallengeInfoTextIndicator", "getSubmitAuthenticationLabel", "getTransStatus", "getUiType", "()Lcom/stripe/android/stripe3ds2/transactions/ChallengeResponseData$UiType;", "getWhitelistingInfoText", "getWhyInfoLabel", "getWhyInfoText", "component1", "component10", "component11", "component12", "component13", "component14", "component15", "component16", "component17", "component18", "component19", "component2", "component20", "component21", "component22", "component23", "component24", "component25", "component26", "component27", "component28", "component3", "component4", "component5", "component6", "component7", "component8", "component9", "copy", "describeContents", "", "equals", "other", "", "hashCode", "toJson", "Lorg/json/JSONObject;", "toString", "writeToParcel", "", "parcel", "Landroid/os/Parcel;", "flags", "ChallengeSelectOption", "Companion", "Image", "UiType", "3ds2sdk_release"}, k = 1, mv = {1, 1, 16})
public final class ChallengeResponseData implements Parcelable {
    public static final List<String> C = CollectionsKt.listOf("Y", "N");
    public static final Parcelable.Creator CREATOR = new b();
    public static final a Companion = new a();
    public static final Set<String> D = SetsKt.setOf("threeDSServerTransID", "acsCounterAtoS", "acsTransID", "challengeCompletionInd", "messageExtension", "messageType", "messageVersion", "sdkTransID", "transStatus");
    public static final String MESSAGE_TYPE = "CRes";
    public final String A;
    public final String B;

    /* renamed from: a  reason: collision with root package name */
    public final String f125a;
    public final String b;
    public final String c;
    public final String d;
    public final c e;
    public final boolean f;
    public final String g;
    public final String h;
    public final String i;
    public final String j;
    public final boolean k;
    public final List<ChallengeSelectOption> l;
    public final String m;
    public final String n;
    public final Image o;
    public final List<MessageExtension> p;
    public final String q;
    public final String r;
    public final String s;
    public final String t;
    public final Image u;
    public final String v;
    public final String w;
    public final String x;
    public final String y;
    public final String z;

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\t\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\b\b\u0018\u0000 \u001b2\u00020\u0001:\u0001\u001bB\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003¢\u0006\u0002\u0010\u0005J\t\u0010\t\u001a\u00020\u0003HÆ\u0003J\t\u0010\n\u001a\u00020\u0003HÆ\u0003J\u001d\u0010\u000b\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u0003HÆ\u0001J\t\u0010\f\u001a\u00020\rHÖ\u0001J\u0013\u0010\u000e\u001a\u00020\u000f2\b\u0010\u0010\u001a\u0004\u0018\u00010\u0011HÖ\u0003J\t\u0010\u0012\u001a\u00020\rHÖ\u0001J\b\u0010\u0013\u001a\u00020\u0014H\u0002J\t\u0010\u0015\u001a\u00020\u0003HÖ\u0001J\u0019\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00020\rHÖ\u0001R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\u0007R\u0011\u0010\u0004\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\u0007¨\u0006\u001c"}, d2 = {"Lcom/stripe/android/stripe3ds2/transactions/ChallengeResponseData$ChallengeSelectOption;", "Landroid/os/Parcelable;", "name", "", "text", "(Ljava/lang/String;Ljava/lang/String;)V", "getName", "()Ljava/lang/String;", "getText", "component1", "component2", "copy", "describeContents", "", "equals", "", "other", "", "hashCode", "toJson", "Lorg/json/JSONObject;", "toString", "writeToParcel", "", "parcel", "Landroid/os/Parcel;", "flags", "Companion", "3ds2sdk_release"}, k = 1, mv = {1, 1, 16})
    public static final class ChallengeSelectOption implements Parcelable {
        public static final Parcelable.Creator CREATOR = new b();
        public static final a Companion = new a();

        /* renamed from: a  reason: collision with root package name */
        public final String f126a;
        public final String b;

        public static final class a {
        }

        public static class b implements Parcelable.Creator {
            public final Object createFromParcel(Parcel parcel) {
                Intrinsics.checkParameterIsNotNull(parcel, "in");
                return new ChallengeSelectOption(parcel.readString(), parcel.readString());
            }

            public final Object[] newArray(int i) {
                return new ChallengeSelectOption[i];
            }
        }

        public ChallengeSelectOption(String str, String str2) {
            Intrinsics.checkParameterIsNotNull(str, "name");
            Intrinsics.checkParameterIsNotNull(str2, "text");
            this.f126a = str;
            this.b = str2;
        }

        public static final /* synthetic */ JSONObject access$toJson(ChallengeSelectOption challengeSelectOption) {
            if (challengeSelectOption != null) {
                JSONObject put = new JSONObject().put(challengeSelectOption.f126a, challengeSelectOption.b);
                Intrinsics.checkExpressionValueIsNotNull(put, "JSONObject()\n                .put(name, text)");
                return put;
            }
            throw null;
        }

        public static /* synthetic */ ChallengeSelectOption copy$default(ChallengeSelectOption challengeSelectOption, String str, String str2, int i, Object obj) {
            if ((i & 1) != 0) {
                str = challengeSelectOption.f126a;
            }
            if ((i & 2) != 0) {
                str2 = challengeSelectOption.b;
            }
            return challengeSelectOption.copy(str, str2);
        }

        public final String component1() {
            return this.f126a;
        }

        public final String component2() {
            return this.b;
        }

        public final ChallengeSelectOption copy(String str, String str2) {
            Intrinsics.checkParameterIsNotNull(str, "name");
            Intrinsics.checkParameterIsNotNull(str2, "text");
            return new ChallengeSelectOption(str, str2);
        }

        public int describeContents() {
            return 0;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof ChallengeSelectOption)) {
                return false;
            }
            ChallengeSelectOption challengeSelectOption = (ChallengeSelectOption) obj;
            return Intrinsics.areEqual((Object) this.f126a, (Object) challengeSelectOption.f126a) && Intrinsics.areEqual((Object) this.b, (Object) challengeSelectOption.b);
        }

        public final String getName() {
            return this.f126a;
        }

        public final String getText() {
            return this.b;
        }

        public int hashCode() {
            String str = this.f126a;
            int i = 0;
            int hashCode = (str != null ? str.hashCode() : 0) * 31;
            String str2 = this.b;
            if (str2 != null) {
                i = str2.hashCode();
            }
            return hashCode + i;
        }

        public String toString() {
            return "ChallengeSelectOption(name=" + this.f126a + ", text=" + this.b + ")";
        }

        public void writeToParcel(Parcel parcel, int i) {
            Intrinsics.checkParameterIsNotNull(parcel, "parcel");
            parcel.writeString(this.f126a);
            parcel.writeString(this.b);
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0011\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\b\b\u0018\u0000 &2\u00020\u0001:\u0001&B'\u0012\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0003\u0012\b\u0010\u0005\u001a\u0004\u0018\u00010\u0003¢\u0006\u0002\u0010\u0006J\u0010\u0010\r\u001a\u0004\u0018\u00010\u0003HÀ\u0003¢\u0006\u0002\b\u000eJ\u0010\u0010\u000f\u001a\u0004\u0018\u00010\u0003HÀ\u0003¢\u0006\u0002\b\u0010J\u0010\u0010\u0011\u001a\u0004\u0018\u00010\u0003HÀ\u0003¢\u0006\u0002\b\u0012J-\u0010\u0013\u001a\u00020\u00002\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u0003HÆ\u0001J\t\u0010\u0014\u001a\u00020\u0015HÖ\u0001J\u0013\u0010\u0016\u001a\u00020\u00172\b\u0010\u0018\u001a\u0004\u0018\u00010\u0019HÖ\u0003J\u0010\u0010\u001a\u001a\u0004\u0018\u00010\u00032\u0006\u0010\u001b\u001a\u00020\u0015J\t\u0010\u001c\u001a\u00020\u0015HÖ\u0001J\r\u0010\u001d\u001a\u00020\u001eH\u0000¢\u0006\u0002\b\u001fJ\t\u0010 \u001a\u00020\u0003HÖ\u0001J\u0019\u0010!\u001a\u00020\"2\u0006\u0010#\u001a\u00020$2\u0006\u0010%\u001a\u00020\u0015HÖ\u0001R\u0016\u0010\u0005\u001a\u0004\u0018\u00010\u0003X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0016\u0010\u0004\u001a\u0004\u0018\u00010\u0003X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\bR\u0013\u0010\n\u001a\u0004\u0018\u00010\u00038F¢\u0006\u0006\u001a\u0004\b\u000b\u0010\bR\u0016\u0010\u0002\u001a\u0004\u0018\u00010\u0003X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\b¨\u0006'"}, d2 = {"Lcom/stripe/android/stripe3ds2/transactions/ChallengeResponseData$Image;", "Landroid/os/Parcelable;", "mediumUrl", "", "highUrl", "extraHighUrl", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", "getExtraHighUrl$3ds2sdk_release", "()Ljava/lang/String;", "getHighUrl$3ds2sdk_release", "highestFidelityImageUrl", "getHighestFidelityImageUrl", "getMediumUrl$3ds2sdk_release", "component1", "component1$3ds2sdk_release", "component2", "component2$3ds2sdk_release", "component3", "component3$3ds2sdk_release", "copy", "describeContents", "", "equals", "", "other", "", "getUrlForDensity", "density", "hashCode", "toJson", "Lorg/json/JSONObject;", "toJson$3ds2sdk_release", "toString", "writeToParcel", "", "parcel", "Landroid/os/Parcel;", "flags", "Companion", "3ds2sdk_release"}, k = 1, mv = {1, 1, 16})
    public static final class Image implements Parcelable {
        public static final Parcelable.Creator CREATOR = new b();
        public static final a Companion = new a();

        /* renamed from: a  reason: collision with root package name */
        public final String f127a;
        public final String b;
        public final String c;

        public static final class a {
            public final Image a(JSONObject jSONObject) {
                if (jSONObject != null) {
                    return new Image(jSONObject.optString(FirebaseAnalytics.Param.MEDIUM), jSONObject.optString("high"), jSONObject.optString("extraHigh"));
                }
                return null;
            }
        }

        public static class b implements Parcelable.Creator {
            public final Object createFromParcel(Parcel parcel) {
                Intrinsics.checkParameterIsNotNull(parcel, "in");
                return new Image(parcel.readString(), parcel.readString(), parcel.readString());
            }

            public final Object[] newArray(int i) {
                return new Image[i];
            }
        }

        public Image(String str, String str2, String str3) {
            this.f127a = str;
            this.b = str2;
            this.c = str3;
        }

        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ Image(String str, String str2, String str3, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this((i & 1) != 0 ? null : str, (i & 2) != 0 ? null : str2, str3);
        }

        public static /* synthetic */ Image copy$default(Image image, String str, String str2, String str3, int i, Object obj) {
            if ((i & 1) != 0) {
                str = image.f127a;
            }
            if ((i & 2) != 0) {
                str2 = image.b;
            }
            if ((i & 4) != 0) {
                str3 = image.c;
            }
            return image.copy(str, str2, str3);
        }

        public final String component1$3ds2sdk_release() {
            return this.f127a;
        }

        public final String component2$3ds2sdk_release() {
            return this.b;
        }

        public final String component3$3ds2sdk_release() {
            return this.c;
        }

        public final Image copy(String str, String str2, String str3) {
            return new Image(str, str2, str3);
        }

        public int describeContents() {
            return 0;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Image)) {
                return false;
            }
            Image image = (Image) obj;
            return Intrinsics.areEqual((Object) this.f127a, (Object) image.f127a) && Intrinsics.areEqual((Object) this.b, (Object) image.b) && Intrinsics.areEqual((Object) this.c, (Object) image.c);
        }

        public final String getExtraHighUrl$3ds2sdk_release() {
            return this.c;
        }

        public final String getHighUrl$3ds2sdk_release() {
            return this.b;
        }

        public final String getHighestFidelityImageUrl() {
            Object obj;
            Iterator it = CollectionsKt.listOf(this.c, this.b, this.f127a).iterator();
            while (true) {
                if (!it.hasNext()) {
                    obj = null;
                    break;
                }
                obj = it.next();
                String str = (String) obj;
                if (!(str == null || StringsKt.isBlank(str))) {
                    break;
                }
            }
            return (String) obj;
        }

        public final String getMediumUrl$3ds2sdk_release() {
            return this.f127a;
        }

        public final String getUrlForDensity(int i) {
            String str = i <= 160 ? this.f127a : i >= 320 ? this.c : this.b;
            if (str == null || StringsKt.isBlank(str)) {
                str = null;
            }
            return str != null ? str : getHighestFidelityImageUrl();
        }

        public int hashCode() {
            String str = this.f127a;
            int i = 0;
            int hashCode = (str != null ? str.hashCode() : 0) * 31;
            String str2 = this.b;
            int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
            String str3 = this.c;
            if (str3 != null) {
                i = str3.hashCode();
            }
            return hashCode2 + i;
        }

        public final JSONObject toJson$3ds2sdk_release() throws JSONException {
            JSONObject put = new JSONObject().put(FirebaseAnalytics.Param.MEDIUM, this.f127a).put("high", this.b).put("extraHigh", this.c);
            Intrinsics.checkExpressionValueIsNotNull(put, "JSONObject()\n           …EXTRA_HIGH, extraHighUrl)");
            return put;
        }

        public String toString() {
            return "Image(mediumUrl=" + this.f127a + ", highUrl=" + this.b + ", extraHighUrl=" + this.c + ")";
        }

        public void writeToParcel(Parcel parcel, int i) {
            Intrinsics.checkParameterIsNotNull(parcel, "parcel");
            parcel.writeString(this.f127a);
            parcel.writeString(this.b);
            parcel.writeString(this.c);
        }
    }

    public static class b implements Parcelable.Creator {
        public final Object createFromParcel(Parcel parcel) {
            ArrayList arrayList;
            ArrayList arrayList2;
            Parcel parcel2 = parcel;
            Intrinsics.checkParameterIsNotNull(parcel2, "in");
            String readString = parcel.readString();
            String readString2 = parcel.readString();
            String readString3 = parcel.readString();
            String readString4 = parcel.readString();
            c cVar = parcel.readInt() != 0 ? (c) Enum.valueOf(c.class, parcel.readString()) : null;
            boolean z = parcel.readInt() != 0;
            String readString5 = parcel.readString();
            String readString6 = parcel.readString();
            String readString7 = parcel.readString();
            String readString8 = parcel.readString();
            boolean z2 = parcel.readInt() != 0;
            if (parcel.readInt() != 0) {
                int readInt = parcel.readInt();
                ArrayList arrayList3 = new ArrayList(readInt);
                while (readInt != 0) {
                    arrayList3.add((ChallengeSelectOption) ChallengeSelectOption.CREATOR.createFromParcel(parcel2));
                    readInt--;
                }
                arrayList = arrayList3;
            } else {
                arrayList = null;
            }
            String readString9 = parcel.readString();
            String readString10 = parcel.readString();
            Image image = parcel.readInt() != 0 ? (Image) Image.CREATOR.createFromParcel(parcel2) : null;
            if (parcel.readInt() != 0) {
                int readInt2 = parcel.readInt();
                ArrayList arrayList4 = new ArrayList(readInt2);
                while (readInt2 != 0) {
                    arrayList4.add((MessageExtension) MessageExtension.CREATOR.createFromParcel(parcel2));
                    readInt2--;
                }
                arrayList2 = arrayList4;
            } else {
                arrayList2 = null;
            }
            return new ChallengeResponseData(readString, readString2, readString3, readString4, cVar, z, readString5, readString6, readString7, readString8, z2, arrayList, readString9, readString10, image, arrayList2, parcel.readString(), parcel.readString(), parcel.readString(), parcel.readString(), parcel.readInt() != 0 ? (Image) Image.CREATOR.createFromParcel(parcel2) : null, parcel.readString(), parcel.readString(), parcel.readString(), parcel.readString(), parcel.readString(), parcel.readString(), parcel.readString());
        }

        public final Object[] newArray(int i) {
            return new ChallengeResponseData[i];
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u000e\b\u0001\u0018\u0000 \u00142\b\u0012\u0004\u0012\u00020\u00000\u0001:\u0001\u0014B!\b\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\b\b\u0002\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bR\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0014\u0010\u0006\u001a\u00020\u0007X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000ej\u0002\b\u000fj\u0002\b\u0010j\u0002\b\u0011j\u0002\b\u0012j\u0002\b\u0013¨\u0006\u0015"}, d2 = {"Lcom/stripe/android/stripe3ds2/transactions/ChallengeResponseData$UiType;", "", "code", "", "challengeType", "Lcom/ults/listeners/ChallengeType;", "requiresSubmitButton", "", "(Ljava/lang/String;ILjava/lang/String;Lcom/ults/listeners/ChallengeType;Z)V", "getChallengeType", "()Lcom/ults/listeners/ChallengeType;", "getCode", "()Ljava/lang/String;", "getRequiresSubmitButton$3ds2sdk_release", "()Z", "TEXT", "SINGLE_SELECT", "MULTI_SELECT", "OOB", "HTML", "Companion", "3ds2sdk_release"}, k = 1, mv = {1, 1, 16})
    public enum c {
        TEXT("01", ChallengeType.SINGLE_TEXT_INPUT, true),
        SINGLE_SELECT("02", ChallengeType.SINGLE_SELECT, true),
        MULTI_SELECT("03", ChallengeType.MULTI_SELECT, true),
        OOB("04", ChallengeType.OUT_OF_BAND, false),
        HTML("05", ChallengeType.HTML_UI, false);
        
        public static final a j = null;

        /* renamed from: a  reason: collision with root package name */
        public final String f128a;
        public final ChallengeType b;
        public final boolean c;

        public static final class a {
        }

        /* access modifiers changed from: public */
        static {
            j = new a();
        }

        /* access modifiers changed from: public */
        c(String str, ChallengeType challengeType, boolean z) {
            this.f128a = str;
            this.b = challengeType;
            this.c = z;
        }

        public final String a() {
            return this.f128a;
        }
    }

    public ChallengeResponseData(String str, String str2, String str3, String str4, c cVar, boolean z2, String str5, String str6, String str7, String str8, boolean z3, List<ChallengeSelectOption> list, String str9, String str10, Image image, List<MessageExtension> list2, String str11, String str12, String str13, String str14, Image image2, String str15, String str16, String str17, String str18, String str19, String str20, String str21) {
        String str22 = str11;
        String str23 = str16;
        Intrinsics.checkParameterIsNotNull(str, "serverTransId");
        Intrinsics.checkParameterIsNotNull(str2, "acsTransId");
        Intrinsics.checkParameterIsNotNull(str22, "messageVersion");
        Intrinsics.checkParameterIsNotNull(str23, "sdkTransId");
        this.f125a = str;
        this.b = str2;
        this.c = str3;
        this.d = str4;
        this.e = cVar;
        this.f = z2;
        this.g = str5;
        this.h = str6;
        this.i = str7;
        this.j = str8;
        this.k = z3;
        this.l = list;
        this.m = str9;
        this.n = str10;
        this.o = image;
        this.p = list2;
        this.q = str22;
        this.r = str12;
        this.s = str13;
        this.t = str14;
        this.u = image2;
        this.v = str15;
        this.w = str23;
        this.x = str17;
        this.y = str18;
        this.z = str19;
        this.A = str20;
        this.B = str21;
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* synthetic */ ChallengeResponseData(java.lang.String r33, java.lang.String r34, java.lang.String r35, java.lang.String r36, com.stripe.android.stripe3ds2.transactions.ChallengeResponseData.c r37, boolean r38, java.lang.String r39, java.lang.String r40, java.lang.String r41, java.lang.String r42, boolean r43, java.util.List r44, java.lang.String r45, java.lang.String r46, com.stripe.android.stripe3ds2.transactions.ChallengeResponseData.Image r47, java.util.List r48, java.lang.String r49, java.lang.String r50, java.lang.String r51, java.lang.String r52, com.stripe.android.stripe3ds2.transactions.ChallengeResponseData.Image r53, java.lang.String r54, java.lang.String r55, java.lang.String r56, java.lang.String r57, java.lang.String r58, java.lang.String r59, java.lang.String r60, int r61, kotlin.jvm.internal.DefaultConstructorMarker r62) {
        /*
            r32 = this;
            r0 = r61
            r1 = r0 & 4
            r2 = 0
            if (r1 == 0) goto L_0x0009
            r6 = r2
            goto L_0x000b
        L_0x0009:
            r6 = r35
        L_0x000b:
            r1 = r0 & 8
            if (r1 == 0) goto L_0x0011
            r7 = r2
            goto L_0x0013
        L_0x0011:
            r7 = r36
        L_0x0013:
            r1 = r0 & 16
            if (r1 == 0) goto L_0x0019
            r8 = r2
            goto L_0x001b
        L_0x0019:
            r8 = r37
        L_0x001b:
            r1 = r0 & 32
            r3 = 0
            if (r1 == 0) goto L_0x0022
            r9 = 0
            goto L_0x0024
        L_0x0022:
            r9 = r38
        L_0x0024:
            r1 = r0 & 64
            if (r1 == 0) goto L_0x002a
            r10 = r2
            goto L_0x002c
        L_0x002a:
            r10 = r39
        L_0x002c:
            r1 = r0 & 128(0x80, float:1.794E-43)
            if (r1 == 0) goto L_0x0032
            r11 = r2
            goto L_0x0034
        L_0x0032:
            r11 = r40
        L_0x0034:
            r1 = r0 & 256(0x100, float:3.59E-43)
            if (r1 == 0) goto L_0x003a
            r12 = r2
            goto L_0x003c
        L_0x003a:
            r12 = r41
        L_0x003c:
            r1 = r0 & 512(0x200, float:7.175E-43)
            if (r1 == 0) goto L_0x0042
            r13 = r2
            goto L_0x0044
        L_0x0042:
            r13 = r42
        L_0x0044:
            r1 = r0 & 1024(0x400, float:1.435E-42)
            if (r1 == 0) goto L_0x004a
            r14 = 0
            goto L_0x004c
        L_0x004a:
            r14 = r43
        L_0x004c:
            r1 = r0 & 2048(0x800, float:2.87E-42)
            if (r1 == 0) goto L_0x0052
            r15 = r2
            goto L_0x0054
        L_0x0052:
            r15 = r44
        L_0x0054:
            r1 = r0 & 4096(0x1000, float:5.74E-42)
            if (r1 == 0) goto L_0x005b
            r16 = r2
            goto L_0x005d
        L_0x005b:
            r16 = r45
        L_0x005d:
            r1 = r0 & 8192(0x2000, float:1.14794E-41)
            if (r1 == 0) goto L_0x0064
            r17 = r2
            goto L_0x0066
        L_0x0064:
            r17 = r46
        L_0x0066:
            r1 = r0 & 16384(0x4000, float:2.2959E-41)
            if (r1 == 0) goto L_0x006d
            r18 = r2
            goto L_0x006f
        L_0x006d:
            r18 = r47
        L_0x006f:
            r1 = 32768(0x8000, float:4.5918E-41)
            r1 = r1 & r0
            if (r1 == 0) goto L_0x0078
            r19 = r2
            goto L_0x007a
        L_0x0078:
            r19 = r48
        L_0x007a:
            r1 = 131072(0x20000, float:1.83671E-40)
            r1 = r1 & r0
            if (r1 == 0) goto L_0x0082
            r21 = r2
            goto L_0x0084
        L_0x0082:
            r21 = r50
        L_0x0084:
            r1 = 262144(0x40000, float:3.67342E-40)
            r1 = r1 & r0
            if (r1 == 0) goto L_0x008c
            r22 = r2
            goto L_0x008e
        L_0x008c:
            r22 = r51
        L_0x008e:
            r1 = 524288(0x80000, float:7.34684E-40)
            r1 = r1 & r0
            if (r1 == 0) goto L_0x0096
            r23 = r2
            goto L_0x0098
        L_0x0096:
            r23 = r52
        L_0x0098:
            r1 = 1048576(0x100000, float:1.469368E-39)
            r1 = r1 & r0
            if (r1 == 0) goto L_0x00a0
            r24 = r2
            goto L_0x00a2
        L_0x00a0:
            r24 = r53
        L_0x00a2:
            r1 = 2097152(0x200000, float:2.938736E-39)
            r1 = r1 & r0
            if (r1 == 0) goto L_0x00aa
            r25 = r2
            goto L_0x00ac
        L_0x00aa:
            r25 = r54
        L_0x00ac:
            r1 = 8388608(0x800000, float:1.17549435E-38)
            r1 = r1 & r0
            if (r1 == 0) goto L_0x00b4
            r27 = r2
            goto L_0x00b6
        L_0x00b4:
            r27 = r56
        L_0x00b6:
            r1 = 16777216(0x1000000, float:2.3509887E-38)
            r1 = r1 & r0
            if (r1 == 0) goto L_0x00be
            r28 = r2
            goto L_0x00c0
        L_0x00be:
            r28 = r57
        L_0x00c0:
            r1 = 33554432(0x2000000, float:9.403955E-38)
            r1 = r1 & r0
            if (r1 == 0) goto L_0x00c8
            r29 = r2
            goto L_0x00ca
        L_0x00c8:
            r29 = r58
        L_0x00ca:
            r1 = 67108864(0x4000000, float:1.5046328E-36)
            r1 = r1 & r0
            if (r1 == 0) goto L_0x00d2
            r30 = r2
            goto L_0x00d4
        L_0x00d2:
            r30 = r59
        L_0x00d4:
            r1 = 134217728(0x8000000, float:3.85186E-34)
            r0 = r0 & r1
            if (r0 == 0) goto L_0x00dc
            r31 = r2
            goto L_0x00de
        L_0x00dc:
            r31 = r60
        L_0x00de:
            r3 = r32
            r4 = r33
            r5 = r34
            r20 = r49
            r26 = r55
            r3.<init>(r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15, r16, r17, r18, r19, r20, r21, r22, r23, r24, r25, r26, r27, r28, r29, r30, r31)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.stripe.android.stripe3ds2.transactions.ChallengeResponseData.<init>(java.lang.String, java.lang.String, java.lang.String, java.lang.String, com.stripe.android.stripe3ds2.transactions.ChallengeResponseData$c, boolean, java.lang.String, java.lang.String, java.lang.String, java.lang.String, boolean, java.util.List, java.lang.String, java.lang.String, com.stripe.android.stripe3ds2.transactions.ChallengeResponseData$Image, java.util.List, java.lang.String, java.lang.String, java.lang.String, java.lang.String, com.stripe.android.stripe3ds2.transactions.ChallengeResponseData$Image, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, int, kotlin.jvm.internal.DefaultConstructorMarker):void");
    }

    public static /* synthetic */ ChallengeResponseData copy$default(ChallengeResponseData challengeResponseData, String str, String str2, String str3, String str4, c cVar, boolean z2, String str5, String str6, String str7, String str8, boolean z3, List list, String str9, String str10, Image image, List list2, String str11, String str12, String str13, String str14, Image image2, String str15, String str16, String str17, String str18, String str19, String str20, String str21, int i2, Object obj) {
        ChallengeResponseData challengeResponseData2 = challengeResponseData;
        int i3 = i2;
        return challengeResponseData.copy((i3 & 1) != 0 ? challengeResponseData2.f125a : str, (i3 & 2) != 0 ? challengeResponseData2.b : str2, (i3 & 4) != 0 ? challengeResponseData2.c : str3, (i3 & 8) != 0 ? challengeResponseData2.d : str4, (i3 & 16) != 0 ? challengeResponseData2.e : cVar, (i3 & 32) != 0 ? challengeResponseData2.f : z2, (i3 & 64) != 0 ? challengeResponseData2.g : str5, (i3 & 128) != 0 ? challengeResponseData2.h : str6, (i3 & 256) != 0 ? challengeResponseData2.i : str7, (i3 & 512) != 0 ? challengeResponseData2.j : str8, (i3 & 1024) != 0 ? challengeResponseData2.k : z3, (i3 & 2048) != 0 ? challengeResponseData2.l : list, (i3 & 4096) != 0 ? challengeResponseData2.m : str9, (i3 & 8192) != 0 ? challengeResponseData2.n : str10, (i3 & 16384) != 0 ? challengeResponseData2.o : image, (i3 & 32768) != 0 ? challengeResponseData2.p : list2, (i3 & 65536) != 0 ? challengeResponseData2.q : str11, (i3 & 131072) != 0 ? challengeResponseData2.r : str12, (i3 & 262144) != 0 ? challengeResponseData2.s : str13, (i3 & 524288) != 0 ? challengeResponseData2.t : str14, (i3 & 1048576) != 0 ? challengeResponseData2.u : image2, (i3 & 2097152) != 0 ? challengeResponseData2.v : str15, (i3 & 4194304) != 0 ? challengeResponseData2.w : str16, (i3 & 8388608) != 0 ? challengeResponseData2.x : str17, (i3 & 16777216) != 0 ? challengeResponseData2.y : str18, (i3 & 33554432) != 0 ? challengeResponseData2.z : str19, (i3 & 67108864) != 0 ? challengeResponseData2.A : str20, (i3 & 134217728) != 0 ? challengeResponseData2.B : str21);
    }

    public final String component1() {
        return this.f125a;
    }

    public final String component10() {
        return this.j;
    }

    public final boolean component11() {
        return this.k;
    }

    public final List<ChallengeSelectOption> component12() {
        return this.l;
    }

    public final String component13() {
        return this.m;
    }

    public final String component14() {
        return this.n;
    }

    public final Image component15() {
        return this.o;
    }

    public final List<MessageExtension> component16() {
        return this.p;
    }

    public final String component17() {
        return this.q;
    }

    public final String component18() {
        return this.r;
    }

    public final String component19() {
        return this.s;
    }

    public final String component2() {
        return this.b;
    }

    public final String component20() {
        return this.t;
    }

    public final Image component21() {
        return this.u;
    }

    public final String component22() {
        return this.v;
    }

    public final String component23() {
        return this.w;
    }

    public final String component24() {
        return this.x;
    }

    public final String component25() {
        return this.y;
    }

    public final String component26() {
        return this.z;
    }

    public final String component27() {
        return this.A;
    }

    public final String component28() {
        return this.B;
    }

    public final String component3() {
        return this.c;
    }

    public final String component4() {
        return this.d;
    }

    public final c component5() {
        return this.e;
    }

    public final boolean component6() {
        return this.f;
    }

    public final String component7() {
        return this.g;
    }

    public final String component8() {
        return this.h;
    }

    public final String component9() {
        return this.i;
    }

    public final ChallengeResponseData copy(String str, String str2, String str3, String str4, c cVar, boolean z2, String str5, String str6, String str7, String str8, boolean z3, List<ChallengeSelectOption> list, String str9, String str10, Image image, List<MessageExtension> list2, String str11, String str12, String str13, String str14, Image image2, String str15, String str16, String str17, String str18, String str19, String str20, String str21) {
        String str22 = str;
        Intrinsics.checkParameterIsNotNull(str22, "serverTransId");
        Intrinsics.checkParameterIsNotNull(str2, "acsTransId");
        Intrinsics.checkParameterIsNotNull(str11, "messageVersion");
        Intrinsics.checkParameterIsNotNull(str16, "sdkTransId");
        return new ChallengeResponseData(str22, str2, str3, str4, cVar, z2, str5, str6, str7, str8, z3, list, str9, str10, image, list2, str11, str12, str13, str14, image2, str15, str16, str17, str18, str19, str20, str21);
    }

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ChallengeResponseData)) {
            return false;
        }
        ChallengeResponseData challengeResponseData = (ChallengeResponseData) obj;
        return Intrinsics.areEqual((Object) this.f125a, (Object) challengeResponseData.f125a) && Intrinsics.areEqual((Object) this.b, (Object) challengeResponseData.b) && Intrinsics.areEqual((Object) this.c, (Object) challengeResponseData.c) && Intrinsics.areEqual((Object) this.d, (Object) challengeResponseData.d) && Intrinsics.areEqual((Object) this.e, (Object) challengeResponseData.e) && this.f == challengeResponseData.f && Intrinsics.areEqual((Object) this.g, (Object) challengeResponseData.g) && Intrinsics.areEqual((Object) this.h, (Object) challengeResponseData.h) && Intrinsics.areEqual((Object) this.i, (Object) challengeResponseData.i) && Intrinsics.areEqual((Object) this.j, (Object) challengeResponseData.j) && this.k == challengeResponseData.k && Intrinsics.areEqual((Object) this.l, (Object) challengeResponseData.l) && Intrinsics.areEqual((Object) this.m, (Object) challengeResponseData.m) && Intrinsics.areEqual((Object) this.n, (Object) challengeResponseData.n) && Intrinsics.areEqual((Object) this.o, (Object) challengeResponseData.o) && Intrinsics.areEqual((Object) this.p, (Object) challengeResponseData.p) && Intrinsics.areEqual((Object) this.q, (Object) challengeResponseData.q) && Intrinsics.areEqual((Object) this.r, (Object) challengeResponseData.r) && Intrinsics.areEqual((Object) this.s, (Object) challengeResponseData.s) && Intrinsics.areEqual((Object) this.t, (Object) challengeResponseData.t) && Intrinsics.areEqual((Object) this.u, (Object) challengeResponseData.u) && Intrinsics.areEqual((Object) this.v, (Object) challengeResponseData.v) && Intrinsics.areEqual((Object) this.w, (Object) challengeResponseData.w) && Intrinsics.areEqual((Object) this.x, (Object) challengeResponseData.x) && Intrinsics.areEqual((Object) this.y, (Object) challengeResponseData.y) && Intrinsics.areEqual((Object) this.z, (Object) challengeResponseData.z) && Intrinsics.areEqual((Object) this.A, (Object) challengeResponseData.A) && Intrinsics.areEqual((Object) this.B, (Object) challengeResponseData.B);
    }

    public final String getAcsHtml() {
        return this.c;
    }

    public final String getAcsHtmlRefresh() {
        return this.d;
    }

    public final String getAcsTransId() {
        return this.b;
    }

    public final String getChallengeAdditionalInfoText() {
        return this.j;
    }

    public final String getChallengeInfoHeader() {
        return this.g;
    }

    public final String getChallengeInfoLabel() {
        return this.h;
    }

    public final String getChallengeInfoText() {
        return this.i;
    }

    public final List<ChallengeSelectOption> getChallengeSelectOptions() {
        return this.l;
    }

    public final String getExpandInfoLabel() {
        return this.m;
    }

    public final String getExpandInfoText() {
        return this.n;
    }

    public final Image getIssuerImage() {
        return this.o;
    }

    public final List<MessageExtension> getMessageExtensions() {
        return this.p;
    }

    public final String getMessageVersion() {
        return this.q;
    }

    public final String getOobAppLabel() {
        return this.s;
    }

    public final String getOobAppUrl() {
        return this.r;
    }

    public final String getOobContinueLabel() {
        return this.t;
    }

    public final Image getPaymentSystemImage() {
        return this.u;
    }

    public final String getResendInformationLabel() {
        return this.v;
    }

    public final String getSdkTransId() {
        return this.w;
    }

    public final String getServerTransId() {
        return this.f125a;
    }

    public final boolean getShouldShowChallengeInfoTextIndicator() {
        return this.k;
    }

    public final String getSubmitAuthenticationLabel() {
        return this.x;
    }

    public final String getTransStatus() {
        return this.B;
    }

    public final c getUiType() {
        return this.e;
    }

    public final String getWhitelistingInfoText() {
        return this.y;
    }

    public final String getWhyInfoLabel() {
        return this.z;
    }

    public final String getWhyInfoText() {
        return this.A;
    }

    public int hashCode() {
        String str = this.f125a;
        int i2 = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.b;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.c;
        int hashCode3 = (hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31;
        String str4 = this.d;
        int hashCode4 = (hashCode3 + (str4 != null ? str4.hashCode() : 0)) * 31;
        c cVar = this.e;
        int hashCode5 = (hashCode4 + (cVar != null ? cVar.hashCode() : 0)) * 31;
        boolean z2 = this.f;
        boolean z3 = true;
        if (z2) {
            z2 = true;
        }
        int i3 = (hashCode5 + (z2 ? 1 : 0)) * 31;
        String str5 = this.g;
        int hashCode6 = (i3 + (str5 != null ? str5.hashCode() : 0)) * 31;
        String str6 = this.h;
        int hashCode7 = (hashCode6 + (str6 != null ? str6.hashCode() : 0)) * 31;
        String str7 = this.i;
        int hashCode8 = (hashCode7 + (str7 != null ? str7.hashCode() : 0)) * 31;
        String str8 = this.j;
        int hashCode9 = (hashCode8 + (str8 != null ? str8.hashCode() : 0)) * 31;
        boolean z4 = this.k;
        if (!z4) {
            z3 = z4;
        }
        int i4 = (hashCode9 + (z3 ? 1 : 0)) * 31;
        List<ChallengeSelectOption> list = this.l;
        int hashCode10 = (i4 + (list != null ? list.hashCode() : 0)) * 31;
        String str9 = this.m;
        int hashCode11 = (hashCode10 + (str9 != null ? str9.hashCode() : 0)) * 31;
        String str10 = this.n;
        int hashCode12 = (hashCode11 + (str10 != null ? str10.hashCode() : 0)) * 31;
        Image image = this.o;
        int hashCode13 = (hashCode12 + (image != null ? image.hashCode() : 0)) * 31;
        List<MessageExtension> list2 = this.p;
        int hashCode14 = (hashCode13 + (list2 != null ? list2.hashCode() : 0)) * 31;
        String str11 = this.q;
        int hashCode15 = (hashCode14 + (str11 != null ? str11.hashCode() : 0)) * 31;
        String str12 = this.r;
        int hashCode16 = (hashCode15 + (str12 != null ? str12.hashCode() : 0)) * 31;
        String str13 = this.s;
        int hashCode17 = (hashCode16 + (str13 != null ? str13.hashCode() : 0)) * 31;
        String str14 = this.t;
        int hashCode18 = (hashCode17 + (str14 != null ? str14.hashCode() : 0)) * 31;
        Image image2 = this.u;
        int hashCode19 = (hashCode18 + (image2 != null ? image2.hashCode() : 0)) * 31;
        String str15 = this.v;
        int hashCode20 = (hashCode19 + (str15 != null ? str15.hashCode() : 0)) * 31;
        String str16 = this.w;
        int hashCode21 = (hashCode20 + (str16 != null ? str16.hashCode() : 0)) * 31;
        String str17 = this.x;
        int hashCode22 = (hashCode21 + (str17 != null ? str17.hashCode() : 0)) * 31;
        String str18 = this.y;
        int hashCode23 = (hashCode22 + (str18 != null ? str18.hashCode() : 0)) * 31;
        String str19 = this.z;
        int hashCode24 = (hashCode23 + (str19 != null ? str19.hashCode() : 0)) * 31;
        String str20 = this.A;
        int hashCode25 = (hashCode24 + (str20 != null ? str20.hashCode() : 0)) * 31;
        String str21 = this.B;
        if (str21 != null) {
            i2 = str21.hashCode();
        }
        return hashCode25 + i2;
    }

    public final boolean isChallengeCompleted() {
        return this.f;
    }

    public final boolean isValidForUi$3ds2sdk_release() {
        a.a.a.a.f.c cVar;
        String str;
        List<ChallengeSelectOption> list;
        c cVar2 = this.e;
        if (cVar2 == null) {
            return true;
        }
        if (cVar2 == c.HTML) {
            cVar = a.a.a.a.f.c.f89a;
            str = this.c;
        } else if (a.a.a.a.f.c.f89a.a(this.g) && a.a.a.a.f.c.f89a.a(this.h) && a.a.a.a.f.c.f89a.a(this.i) && a.a.a.a.f.c.f89a.a(this.z) && a.a.a.a.f.c.f89a.a(this.A) && a.a.a.a.f.c.f89a.a(this.m) && a.a.a.a.f.c.f89a.a(this.n) && a.a.a.a.f.c.f89a.a(this.v)) {
            return false;
        } else {
            c cVar3 = this.e;
            if (cVar3 == c.OOB) {
                return !a.a.a.a.f.c.f89a.a(this.s) || !a.a.a.a.f.c.f89a.a(this.r) || !a.a.a.a.f.c.f89a.a(this.t);
            }
            if ((cVar3 == c.SINGLE_SELECT || cVar3 == c.MULTI_SELECT) && ((list = this.l) == null || list.isEmpty())) {
                return false;
            }
            cVar = a.a.a.a.f.c.f89a;
            str = this.x;
        }
        return !cVar.a(str);
    }

    public final JSONObject toJson() throws JSONException {
        JSONArray jSONArray;
        JSONObject put = new JSONObject().put("messageType", MESSAGE_TYPE).put("threeDSServerTransID", this.f125a).put("acsTransID", this.b).put("acsHTML", this.c).put("acsHTMLRefresh", this.d);
        c cVar = this.e;
        JSONObject jSONObject = null;
        String str = "Y";
        JSONObject put2 = put.put("acsUiType", cVar != null ? cVar.f128a : null).put("challengeCompletionInd", this.f ? str : "N").put("challengeInfoHeader", this.g).put("challengeInfoLabel", this.h).put("challengeInfoText", this.i).put("challengeAddInfo", this.j);
        ChallengeSelectOption.a aVar = ChallengeSelectOption.Companion;
        List<ChallengeSelectOption> list = this.l;
        if (aVar != null) {
            if (list == null) {
                jSONArray = null;
            } else {
                jSONArray = new JSONArray();
                for (ChallengeSelectOption access$toJson : list) {
                    jSONArray.put(ChallengeSelectOption.access$toJson(access$toJson));
                }
            }
            JSONObject put3 = put2.put("challengeSelectInfo", jSONArray).put("expandInfoLabel", this.m).put("expandInfoText", this.n);
            Image image = this.o;
            JSONObject put4 = put3.put("issuerImage", image != null ? image.toJson$3ds2sdk_release() : null).put("messageExtension", MessageExtension.Companion.a(this.p)).put("messageVersion", this.q).put("oobAppURL", this.r).put("oobAppLabel", this.s).put("oobContinueLabel", this.t);
            Image image2 = this.u;
            if (image2 != null) {
                jSONObject = image2.toJson$3ds2sdk_release();
            }
            JSONObject put5 = put4.put("psImage", jSONObject).put("resendInformationLabel", this.v).put("sdkTransID", this.w).put("submitAuthenticationLabel", this.x).put("whitelistingInfoText", this.y).put("whyInfoLabel", this.z).put("whyInfoText", this.A).put("transStatus", this.B);
            if (!this.f) {
                if (!this.k) {
                    str = "N";
                }
                put5.put("challengeInfoTextIndicator", str);
            }
            Intrinsics.checkExpressionValueIsNotNull(put5, "JSONObject()\n           …          }\n            }");
            return put5;
        }
        throw null;
    }

    public String toString() {
        return "ChallengeResponseData(serverTransId=" + this.f125a + ", acsTransId=" + this.b + ", acsHtml=" + this.c + ", acsHtmlRefresh=" + this.d + ", uiType=" + this.e + ", isChallengeCompleted=" + this.f + ", challengeInfoHeader=" + this.g + ", challengeInfoLabel=" + this.h + ", challengeInfoText=" + this.i + ", challengeAdditionalInfoText=" + this.j + ", shouldShowChallengeInfoTextIndicator=" + this.k + ", challengeSelectOptions=" + this.l + ", expandInfoLabel=" + this.m + ", expandInfoText=" + this.n + ", issuerImage=" + this.o + ", messageExtensions=" + this.p + ", messageVersion=" + this.q + ", oobAppUrl=" + this.r + ", oobAppLabel=" + this.s + ", oobContinueLabel=" + this.t + ", paymentSystemImage=" + this.u + ", resendInformationLabel=" + this.v + ", sdkTransId=" + this.w + ", submitAuthenticationLabel=" + this.x + ", whitelistingInfoText=" + this.y + ", whyInfoLabel=" + this.z + ", whyInfoText=" + this.A + ", transStatus=" + this.B + ")";
    }

    public void writeToParcel(Parcel parcel, int i2) {
        Intrinsics.checkParameterIsNotNull(parcel, "parcel");
        parcel.writeString(this.f125a);
        parcel.writeString(this.b);
        parcel.writeString(this.c);
        parcel.writeString(this.d);
        c cVar = this.e;
        if (cVar != null) {
            parcel.writeInt(1);
            parcel.writeString(cVar.name());
        } else {
            parcel.writeInt(0);
        }
        parcel.writeInt(this.f ? 1 : 0);
        parcel.writeString(this.g);
        parcel.writeString(this.h);
        parcel.writeString(this.i);
        parcel.writeString(this.j);
        parcel.writeInt(this.k ? 1 : 0);
        List<ChallengeSelectOption> list = this.l;
        if (list != null) {
            parcel.writeInt(1);
            parcel.writeInt(list.size());
            for (ChallengeSelectOption writeToParcel : list) {
                writeToParcel.writeToParcel(parcel, 0);
            }
        } else {
            parcel.writeInt(0);
        }
        parcel.writeString(this.m);
        parcel.writeString(this.n);
        Image image = this.o;
        if (image != null) {
            parcel.writeInt(1);
            image.writeToParcel(parcel, 0);
        } else {
            parcel.writeInt(0);
        }
        List<MessageExtension> list2 = this.p;
        if (list2 != null) {
            parcel.writeInt(1);
            parcel.writeInt(list2.size());
            for (MessageExtension writeToParcel2 : list2) {
                writeToParcel2.writeToParcel(parcel, 0);
            }
        } else {
            parcel.writeInt(0);
        }
        parcel.writeString(this.q);
        parcel.writeString(this.r);
        parcel.writeString(this.s);
        parcel.writeString(this.t);
        Image image2 = this.u;
        if (image2 != null) {
            parcel.writeInt(1);
            image2.writeToParcel(parcel, 0);
        } else {
            parcel.writeInt(0);
        }
        parcel.writeString(this.v);
        parcel.writeString(this.w);
        parcel.writeString(this.x);
        parcel.writeString(this.y);
        parcel.writeString(this.z);
        parcel.writeString(this.A);
        parcel.writeString(this.B);
    }

    public static final class a {
        public final ChallengeResponseData a(JSONObject jSONObject) throws a.a.a.a.e.b {
            ChallengeResponseData challengeResponseData;
            JSONArray jSONArray;
            c cVar;
            ArrayList arrayList;
            JSONArray jSONArray2;
            Object obj;
            v vVar;
            JSONObject jSONObject2 = jSONObject;
            Intrinsics.checkParameterIsNotNull(jSONObject2, "cresJson");
            Intrinsics.checkParameterIsNotNull(jSONObject2, "cresJson");
            boolean z = true;
            if (!(!Intrinsics.areEqual((Object) ChallengeResponseData.MESSAGE_TYPE, (Object) jSONObject2.optString("messageType")))) {
                boolean a2 = a(jSONObject2, "challengeCompletionInd", true);
                String uuid = a(jSONObject2, "sdkTransID").toString();
                Intrinsics.checkExpressionValueIsNotNull(uuid, "getTransactionId(cresJso…_SDK_TRANS_ID).toString()");
                String uuid2 = a(jSONObject2, "threeDSServerTransID").toString();
                Intrinsics.checkExpressionValueIsNotNull(uuid2, "getTransactionId(cresJso…RVER_TRANS_ID).toString()");
                String uuid3 = a(jSONObject2, "acsTransID").toString();
                Intrinsics.checkExpressionValueIsNotNull(uuid3, "getTransactionId(cresJso…_ACS_TRANS_ID).toString()");
                String c = c(jSONObject);
                List<MessageExtension> b = b(jSONObject);
                if (a2) {
                    Intrinsics.checkParameterIsNotNull(jSONObject2, "cresJson");
                    Iterator<String> keys = jSONObject.keys();
                    while (keys.hasNext()) {
                        String next = keys.next();
                        if (!ChallengeResponseData.D.contains(next)) {
                            int i = d.InvalidMessageReceived.f87a;
                            throw new a.a.a.a.e.b(i, "Message is not final CRes", "Invalid data element for final CRes: " + next);
                        }
                    }
                    Intrinsics.checkParameterIsNotNull(jSONObject2, "cresJson");
                    String optString = jSONObject2.optString("transStatus");
                    if (a.a.a.a.f.c.f89a.a(optString)) {
                        throw a.a.a.a.e.b.d.b("transStatus");
                    } else if (v.c != null) {
                        if (optString != null) {
                            v[] values = v.values();
                            int length = values.length;
                            int i2 = 0;
                            while (true) {
                                if (i2 >= length) {
                                    break;
                                }
                                v vVar2 = values[i2];
                                if (Intrinsics.areEqual((Object) vVar2.f77a, (Object) optString)) {
                                    vVar = vVar2;
                                    break;
                                }
                                i2++;
                            }
                        }
                        vVar = null;
                        if (vVar != null) {
                            challengeResponseData = new ChallengeResponseData(uuid2, uuid3, (String) null, (String) null, (c) null, a2, (String) null, (String) null, (String) null, (String) null, false, (List) null, (String) null, (String) null, (Image) null, b, c, (String) null, (String) null, (String) null, (Image) null, (String) null, uuid, (String) null, (String) null, (String) null, (String) null, vVar.f77a, 129925084, (DefaultConstructorMarker) null);
                        } else {
                            throw a.a.a.a.e.b.d.a("transStatus");
                        }
                    } else {
                        throw null;
                    }
                } else {
                    String str = uuid;
                    int i3 = 0;
                    boolean a3 = a(jSONObject2, "challengeInfoTextIndicator", false);
                    Intrinsics.checkParameterIsNotNull(jSONObject2, "cresJson");
                    String string = jSONObject2.has("resendInformationLabel") ? jSONObject2.getString("resendInformationLabel") : null;
                    if (string != null) {
                        if (string.length() != 0) {
                            z = false;
                        }
                        if (z) {
                            throw a.a.a.a.e.b.d.a("resendInformationLabel");
                        }
                    }
                    Intrinsics.checkParameterIsNotNull(jSONObject2, "cresJson");
                    JSONObject jSONObject3 = jSONObject2.has("challengeSelectInfo") ? jSONObject2 : null;
                    if (jSONObject3 != null) {
                        a aVar = ChallengeResponseData.Companion;
                        try {
                            Result.Companion companion = Result.Companion;
                            obj = Result.m4constructorimpl(jSONObject3.getJSONArray("challengeSelectInfo"));
                        } catch (Throwable th) {
                            Result.Companion companion2 = Result.Companion;
                            obj = Result.m4constructorimpl(ResultKt.createFailure(th));
                        }
                        if (Result.m7exceptionOrNullimpl(obj) == null) {
                            jSONArray = (JSONArray) obj;
                        } else {
                            throw a.a.a.a.e.b.d.a("challengeSelectInfo");
                        }
                    } else {
                        jSONArray = null;
                    }
                    Intrinsics.checkParameterIsNotNull(jSONObject2, "cresJson");
                    String optString2 = jSONObject2.optString("acsUiType");
                    if (a.a.a.a.f.c.f89a.a(optString2)) {
                        throw a.a.a.a.e.b.d.b("acsUiType");
                    } else if (c.j != null) {
                        c[] values2 = c.values();
                        int length2 = values2.length;
                        int i4 = 0;
                        while (true) {
                            if (i4 >= length2) {
                                cVar = null;
                                break;
                            }
                            c cVar2 = values2[i4];
                            if (Intrinsics.areEqual((Object) optString2, (Object) cVar2.f128a)) {
                                cVar = cVar2;
                                break;
                            }
                            i4++;
                        }
                        if (cVar != null) {
                            Intrinsics.checkParameterIsNotNull(jSONObject2, "cresJson");
                            Intrinsics.checkParameterIsNotNull(cVar, "uiType");
                            String string2 = jSONObject2.has("submitAuthenticationLabel") ? jSONObject2.getString("submitAuthenticationLabel") : null;
                            if (!a.a.a.a.f.c.f89a.a(string2) || !cVar.c) {
                                Intrinsics.checkParameterIsNotNull(jSONObject2, "cresJson");
                                Intrinsics.checkParameterIsNotNull(cVar, "uiType");
                                String string3 = jSONObject2.has("acsHTML") ? jSONObject2.getString("acsHTML") : null;
                                if (!a.a.a.a.f.c.f89a.a(string3) || cVar != c.HTML) {
                                    String a4 = a(string3);
                                    Intrinsics.checkParameterIsNotNull(jSONObject2, "cresJson");
                                    Intrinsics.checkParameterIsNotNull(cVar, "uiType");
                                    String optString3 = jSONObject2.optString("oobContinueLabel");
                                    if (a.a.a.a.f.c.f89a.a(optString3) && cVar == c.OOB) {
                                        throw a.a.a.a.e.b.d.b("oobContinueLabel");
                                    } else if (ChallengeSelectOption.Companion != null) {
                                        if (jSONArray == null) {
                                            arrayList = null;
                                        } else {
                                            ArrayList arrayList2 = new ArrayList();
                                            int length3 = jSONArray.length();
                                            while (i3 < length3) {
                                                JSONObject optJSONObject = jSONArray.optJSONObject(i3);
                                                if (optJSONObject != null) {
                                                    String next2 = optJSONObject.keys().next();
                                                    String optString4 = optJSONObject.optString(next2);
                                                    jSONArray2 = jSONArray;
                                                    Intrinsics.checkExpressionValueIsNotNull(next2, "name");
                                                    Intrinsics.checkExpressionValueIsNotNull(optString4, "text");
                                                    arrayList2.add(new ChallengeSelectOption(next2, optString4));
                                                } else {
                                                    jSONArray2 = jSONArray;
                                                }
                                                i3++;
                                                jSONArray = jSONArray2;
                                            }
                                            arrayList = arrayList2;
                                        }
                                        ChallengeResponseData challengeResponseData2 = r5;
                                        ChallengeResponseData challengeResponseData3 = new ChallengeResponseData(uuid2, uuid3, a4, a(jSONObject2.optString("acsHTMLRefresh")), cVar, a2, jSONObject2.optString("challengeInfoHeader"), jSONObject2.optString("challengeInfoLabel"), jSONObject2.optString("challengeInfoText"), jSONObject2.optString("challengeAddInfo"), a3, arrayList, jSONObject2.optString("expandInfoLabel"), jSONObject2.optString("expandInfoText"), Image.Companion.a(jSONObject2.optJSONObject("issuerImage")), b, c, jSONObject2.optString("oobAppURL"), jSONObject2.optString("oobAppLabel"), optString3, Image.Companion.a(jSONObject2.optJSONObject("psImage")), string, str, string2, jSONObject2.optString("whitelistingInfoText"), jSONObject2.optString("whyInfoLabel"), jSONObject2.optString("whyInfoText"), "");
                                        challengeResponseData = challengeResponseData2;
                                    } else {
                                        throw null;
                                    }
                                } else {
                                    throw a.a.a.a.e.b.d.b("acsHTML");
                                }
                            } else {
                                throw a.a.a.a.e.b.d.b("submitAuthenticationLabel");
                            }
                        } else {
                            throw a.a.a.a.e.b.d.a("acsUiType");
                        }
                    } else {
                        throw null;
                    }
                }
                if (challengeResponseData.isValidForUi$3ds2sdk_release()) {
                    return challengeResponseData;
                }
                throw a.a.a.a.e.b.d.b("UI fields missing");
            }
            throw new a.a.a.a.e.b(d.InvalidMessageReceived.f87a, "Message is not CRes", "Invalid Message Type");
        }

        public final String a(String str) {
            Object obj;
            String str2 = null;
            if (str == null) {
                return null;
            }
            a aVar = ChallengeResponseData.Companion;
            try {
                Result.Companion companion = Result.Companion;
                byte[] decode = Base64.decode(str, 8);
                Intrinsics.checkExpressionValueIsNotNull(decode, "Base64.decode(encodedHtml, Base64.URL_SAFE)");
                obj = Result.m4constructorimpl(new String(decode, Charsets.UTF_8));
            } catch (Throwable th) {
                Result.Companion companion2 = Result.Companion;
                obj = Result.m4constructorimpl(ResultKt.createFailure(th));
            }
            if (!Result.m10isFailureimpl(obj)) {
                str2 = obj;
            }
            return str2;
        }

        public final UUID a(JSONObject jSONObject, String str) throws a.a.a.a.e.b {
            Intrinsics.checkParameterIsNotNull(jSONObject, "cresJson");
            Intrinsics.checkParameterIsNotNull(str, "fieldName");
            String optString = jSONObject.optString(str);
            if (!a.a.a.a.f.c.f89a.a(optString)) {
                try {
                    Result.Companion companion = Result.Companion;
                    UUID fromString = UUID.fromString(optString);
                    Intrinsics.checkExpressionValueIsNotNull(fromString, "UUID.fromString(transId)");
                    return fromString;
                } catch (Throwable th) {
                    Result.Companion companion2 = Result.Companion;
                    if (Result.m7exceptionOrNullimpl(Result.m4constructorimpl(ResultKt.createFailure(th))) == null) {
                        throw null;
                    }
                    throw a.a.a.a.e.b.d.a(str);
                }
            } else {
                throw a.a.a.a.e.b.d.b(str);
            }
        }

        public final List<MessageExtension> b(JSONObject jSONObject) throws a.a.a.a.e.b {
            Intrinsics.checkParameterIsNotNull(jSONObject, "cresJson");
            List<MessageExtension> a2 = MessageExtension.Companion.a(jSONObject.optJSONArray("messageExtension"));
            if (a2 != null) {
                ArrayList arrayList = new ArrayList();
                Iterator<T> it = a2.iterator();
                while (true) {
                    boolean z = true;
                    if (!it.hasNext()) {
                        break;
                    }
                    T next = it.next();
                    MessageExtension messageExtension = (MessageExtension) next;
                    if (!messageExtension.getCriticalityIndicator() || messageExtension.isProcessable()) {
                        z = false;
                    }
                    if (z) {
                        arrayList.add(next);
                    }
                }
                if (!arrayList.isEmpty()) {
                    d dVar = d.UnrecognizedCriticalMessageExtensions;
                    String joinToString$default = CollectionsKt.joinToString$default(arrayList, ",", (CharSequence) null, (CharSequence) null, 0, (CharSequence) null, (Function1) null, 62, (Object) null);
                    Intrinsics.checkParameterIsNotNull(dVar, "protocolError");
                    Intrinsics.checkParameterIsNotNull(joinToString$default, "detail");
                    throw new a.a.a.a.e.b(dVar.f87a, dVar.b, joinToString$default);
                }
            }
            return a2;
        }

        public final String c(JSONObject jSONObject) throws a.a.a.a.e.b {
            Intrinsics.checkParameterIsNotNull(jSONObject, "cresJson");
            String optString = jSONObject.optString("messageVersion");
            Intrinsics.checkExpressionValueIsNotNull(optString, "it");
            if (!(!StringsKt.isBlank(optString))) {
                optString = null;
            }
            if (optString != null) {
                return optString;
            }
            throw a.a.a.a.e.b.d.b("messageVersion");
        }

        public final boolean a(JSONObject jSONObject, String str, boolean z) throws a.a.a.a.e.b {
            String str2;
            Intrinsics.checkParameterIsNotNull(jSONObject, "cresJson");
            Intrinsics.checkParameterIsNotNull(str, "fieldName");
            if (!z) {
                str2 = jSONObject.has(str) ? jSONObject.getString(str) : null;
            } else if (jSONObject.has(str)) {
                str2 = jSONObject.getString(str);
            } else {
                throw a.a.a.a.e.b.d.b(str);
            }
            if (str2 == null || ChallengeResponseData.C.contains(str2)) {
                return Intrinsics.areEqual((Object) "Y", (Object) str2);
            }
            throw ((!z || !a.a.a.a.f.c.f89a.a(str2)) ? a.a.a.a.e.b.d.a(str) : a.a.a.a.e.b.d.b(str));
        }
    }
}
