package com.stripe.android.stripe3ds2.init.ui;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.TypedValue;
import androidx.appcompat.R;
import androidx.appcompat.view.ContextThemeWrapper;
import androidx.core.content.ContextCompat;
import com.stripe.android.stripe3ds2.exceptions.InvalidInputException;
import com.stripe.android.stripe3ds2.init.ui.UiCustomization;
import com.stripe.android.stripe3ds2.utils.CustomizeUtils;
import java.util.Arrays;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import kotlin.jvm.internal.Intrinsics;

public final class StripeUiCustomization implements UiCustomization, Parcelable {
    public static final Parcelable.Creator<StripeUiCustomization> CREATOR = new a();

    /* renamed from: a  reason: collision with root package name */
    public ToolbarCustomization f120a;
    public LabelCustomization b;
    public TextBoxCustomization c;
    public final Map<UiCustomization.ButtonType, ButtonCustomization> d;
    public final Map<String, ButtonCustomization> e;
    public String f;

    public static class a implements Parcelable.Creator<StripeUiCustomization> {
        public Object createFromParcel(Parcel parcel) {
            return new StripeUiCustomization(parcel);
        }

        public Object[] newArray(int i) {
            return new StripeUiCustomization[i];
        }
    }

    public StripeUiCustomization() {
        this.d = new EnumMap(UiCustomization.ButtonType.class);
        this.e = new HashMap();
    }

    public StripeUiCustomization(Activity activity) {
        this();
        Context a2 = a(activity, R.attr.actionBarTheme);
        String a3 = a((Context) activity, Build.VERSION.SDK_INT >= 21 ? 16843827 : R.attr.colorPrimary);
        String a4 = a((Context) activity, Build.VERSION.SDK_INT >= 21 ? 16843828 : R.attr.colorPrimaryDark);
        String a5 = a(a2, 16842806);
        String a6 = a((Context) activity, 16842904);
        String a7 = a((Context) activity, Build.VERSION.SDK_INT >= 21 ? 16843829 : R.attr.colorAccent);
        String a8 = a((Context) activity, 16842906);
        this.f120a = new StripeToolbarCustomization();
        this.b = new StripeLabelCustomization();
        StripeTextBoxCustomization stripeTextBoxCustomization = new StripeTextBoxCustomization();
        this.c = stripeTextBoxCustomization;
        if (a8 != null) {
            stripeTextBoxCustomization.setHintTextColor(a8);
        }
        StripeButtonCustomization stripeButtonCustomization = new StripeButtonCustomization();
        StripeButtonCustomization stripeButtonCustomization2 = new StripeButtonCustomization();
        if (a5 != null) {
            this.f120a.setTextColor(a5);
            stripeButtonCustomization.setTextColor(a5);
        }
        if (a3 != null) {
            this.f120a.setBackgroundColor(a3);
        }
        if (a4 != null) {
            this.f120a.setStatusBarColor(a4);
        }
        if (a6 != null) {
            this.b.setTextColor(a6);
            this.b.setHeadingTextColor(a6);
            stripeButtonCustomization2.setTextColor(a6);
            this.c.setTextColor(a6);
        }
        if (a7 != null) {
            setAccentColor(a7);
            StripeButtonCustomization stripeButtonCustomization3 = new StripeButtonCustomization();
            stripeButtonCustomization3.setTextColor(a7);
            setButtonCustomization((ButtonCustomization) stripeButtonCustomization3, UiCustomization.ButtonType.RESEND);
            stripeButtonCustomization2.setBackgroundColor(a7);
        }
        setButtonCustomization((ButtonCustomization) stripeButtonCustomization, UiCustomization.ButtonType.CANCEL);
        setButtonCustomization((ButtonCustomization) stripeButtonCustomization2, UiCustomization.ButtonType.NEXT);
        setButtonCustomization((ButtonCustomization) stripeButtonCustomization2, UiCustomization.ButtonType.CONTINUE);
        setButtonCustomization((ButtonCustomization) stripeButtonCustomization2, UiCustomization.ButtonType.SUBMIT);
        setButtonCustomization((ButtonCustomization) stripeButtonCustomization2, UiCustomization.ButtonType.SELECT);
    }

    public StripeUiCustomization(Parcel parcel) {
        Class<StripeUiCustomization> cls = StripeUiCustomization.class;
        this.f = parcel.readString();
        this.f120a = (ToolbarCustomization) parcel.readParcelable(StripeToolbarCustomization.class.getClassLoader());
        this.b = (LabelCustomization) parcel.readParcelable(StripeLabelCustomization.class.getClassLoader());
        this.c = (TextBoxCustomization) parcel.readParcelable(StripeTextBoxCustomization.class.getClassLoader());
        this.d = new HashMap();
        Bundle readBundle = parcel.readBundle(cls.getClassLoader());
        if (readBundle != null) {
            for (String str : readBundle.keySet()) {
                ButtonCustomization buttonCustomization = (ButtonCustomization) readBundle.getParcelable(str);
                if (buttonCustomization != null) {
                    this.d.put(UiCustomization.ButtonType.valueOf(str), buttonCustomization);
                }
            }
        }
        this.e = new HashMap();
        Bundle readBundle2 = parcel.readBundle(cls.getClassLoader());
        if (readBundle2 != null) {
            for (String str2 : readBundle2.keySet()) {
                ButtonCustomization buttonCustomization2 = (ButtonCustomization) readBundle2.getParcelable(str2);
                if (buttonCustomization2 != null) {
                    this.e.put(str2, buttonCustomization2);
                }
            }
        }
    }

    public static StripeUiCustomization createWithAppTheme(Activity activity) {
        return new StripeUiCustomization(activity);
    }

    public final Context a(Activity activity, int i) {
        TypedValue typedValue = new TypedValue();
        return activity.getTheme().resolveAttribute(i, typedValue, true) ? new ContextThemeWrapper((Context) activity, typedValue.resourceId) : activity;
    }

    public final String a(Context context, int i) {
        TypedValue typedValue = new TypedValue();
        if (!context.getTheme().resolveAttribute(i, typedValue, true)) {
            return null;
        }
        int i2 = typedValue.resourceId;
        return CustomizeUtils.colorIntToHex(i2 != 0 ? ContextCompat.getColor(context, i2) : typedValue.data);
    }

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (!(obj instanceof StripeUiCustomization)) {
                return false;
            }
            StripeUiCustomization stripeUiCustomization = (StripeUiCustomization) obj;
            if (Intrinsics.areEqual((Object) this.f120a, (Object) stripeUiCustomization.f120a) && Intrinsics.areEqual((Object) this.f, (Object) stripeUiCustomization.f) && Intrinsics.areEqual((Object) this.b, (Object) stripeUiCustomization.b) && Intrinsics.areEqual((Object) this.c, (Object) stripeUiCustomization.c) && Intrinsics.areEqual((Object) this.d, (Object) stripeUiCustomization.d) && Intrinsics.areEqual((Object) this.e, (Object) stripeUiCustomization.e)) {
                return true;
            }
            return false;
        }
        return true;
    }

    public String getAccentColor() {
        return this.f;
    }

    public ButtonCustomization getButtonCustomization(UiCustomization.ButtonType buttonType) throws InvalidInputException {
        return this.d.get(buttonType);
    }

    public ButtonCustomization getButtonCustomization(String str) throws InvalidInputException {
        return this.e.get(str);
    }

    public LabelCustomization getLabelCustomization() {
        return this.b;
    }

    public TextBoxCustomization getTextBoxCustomization() {
        return this.c;
    }

    public ToolbarCustomization getToolbarCustomization() {
        return this.f120a;
    }

    public int hashCode() {
        Object[] objArr = {this.f120a, this.f, this.b, this.c, this.d, this.e};
        Intrinsics.checkParameterIsNotNull(objArr, "values");
        return Objects.hash(Arrays.copyOf(objArr, 6));
    }

    public void setAccentColor(String str) {
        this.f = CustomizeUtils.requireValidColor(str);
    }

    public void setButtonCustomization(ButtonCustomization buttonCustomization, UiCustomization.ButtonType buttonType) throws InvalidInputException {
        this.d.put(buttonType, buttonCustomization);
    }

    public void setButtonCustomization(ButtonCustomization buttonCustomization, String str) throws InvalidInputException {
        this.e.put(str, buttonCustomization);
    }

    public void setLabelCustomization(LabelCustomization labelCustomization) throws InvalidInputException {
        this.b = labelCustomization;
    }

    public void setTextBoxCustomization(TextBoxCustomization textBoxCustomization) throws InvalidInputException {
        this.c = textBoxCustomization;
    }

    public void setToolbarCustomization(ToolbarCustomization toolbarCustomization) throws InvalidInputException {
        this.f120a = toolbarCustomization;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.f);
        parcel.writeParcelable((StripeToolbarCustomization) this.f120a, 0);
        parcel.writeParcelable((StripeLabelCustomization) this.b, 0);
        parcel.writeParcelable((StripeTextBoxCustomization) this.c, 0);
        Bundle bundle = new Bundle();
        for (Map.Entry next : this.d.entrySet()) {
            bundle.putParcelable(((UiCustomization.ButtonType) next.getKey()).name(), (StripeButtonCustomization) next.getValue());
        }
        parcel.writeBundle(bundle);
        Bundle bundle2 = new Bundle();
        for (Map.Entry next2 : this.e.entrySet()) {
            bundle2.putParcelable((String) next2.getKey(), (StripeButtonCustomization) next2.getValue());
        }
        parcel.writeBundle(bundle2);
    }
}
