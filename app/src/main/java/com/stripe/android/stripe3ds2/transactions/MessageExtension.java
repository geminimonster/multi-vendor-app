package com.stripe.android.stripe3ds2.transactions;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt;
import kotlin.collections.IntIterator;
import kotlin.collections.MapsKt;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.internal.Intrinsics;
import kotlin.ranges.IntRange;
import kotlin.ranges.RangesKt;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000F\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010$\n\u0002\b\u000e\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\b\b\u0018\u0000 %2\u00020\u0001:\u0001%B7\b\u0000\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0016\b\u0002\u0010\u0007\u001a\u0010\u0012\u0004\u0012\u00020\u0003\u0012\u0006\u0012\u0004\u0018\u00010\u00030\b¢\u0006\u0002\u0010\tJ\t\u0010\u0010\u001a\u00020\u0003HÆ\u0003J\u000e\u0010\u0011\u001a\u00020\u0003HÀ\u0003¢\u0006\u0002\b\u0012J\t\u0010\u0013\u001a\u00020\u0006HÆ\u0003J\u0017\u0010\u0014\u001a\u0010\u0012\u0004\u0012\u00020\u0003\u0012\u0006\u0012\u0004\u0018\u00010\u00030\bHÂ\u0003J?\u0010\u0015\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00032\b\b\u0002\u0010\u0005\u001a\u00020\u00062\u0016\b\u0002\u0010\u0007\u001a\u0010\u0012\u0004\u0012\u00020\u0003\u0012\u0006\u0012\u0004\u0018\u00010\u00030\bHÆ\u0001J\t\u0010\u0016\u001a\u00020\u0017HÖ\u0001J\u0013\u0010\u0018\u001a\u00020\u00062\b\u0010\u0019\u001a\u0004\u0018\u00010\u001aHÖ\u0003J\t\u0010\u001b\u001a\u00020\u0017HÖ\u0001J\r\u0010\u001c\u001a\u00020\u001dH\u0000¢\u0006\u0002\b\u001eJ\t\u0010\u001f\u001a\u00020\u0003HÖ\u0001J\u0019\u0010 \u001a\u00020!2\u0006\u0010\"\u001a\u00020#2\u0006\u0010$\u001a\u00020\u0017HÖ\u0001R\u0011\u0010\u0005\u001a\u00020\u0006¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u001c\u0010\u0007\u001a\u0010\u0012\u0004\u0012\u00020\u0003\u0012\u0006\u0012\u0004\u0018\u00010\u00030\bX\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\u0004\u001a\u00020\u0003X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\rR\u0011\u0010\u000e\u001a\u00020\u00068F¢\u0006\u0006\u001a\u0004\b\u000e\u0010\u000bR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\r¨\u0006&"}, d2 = {"Lcom/stripe/android/stripe3ds2/transactions/MessageExtension;", "Landroid/os/Parcelable;", "name", "", "id", "criticalityIndicator", "", "data", "", "(Ljava/lang/String;Ljava/lang/String;ZLjava/util/Map;)V", "getCriticalityIndicator", "()Z", "getId$3ds2sdk_release", "()Ljava/lang/String;", "isProcessable", "getName", "component1", "component2", "component2$3ds2sdk_release", "component3", "component4", "copy", "describeContents", "", "equals", "other", "", "hashCode", "toJson", "Lorg/json/JSONObject;", "toJson$3ds2sdk_release", "toString", "writeToParcel", "", "parcel", "Landroid/os/Parcel;", "flags", "Companion", "3ds2sdk_release"}, k = 1, mv = {1, 1, 16})
public final class MessageExtension implements Parcelable {
    public static final Parcelable.Creator CREATOR = new b();
    public static final a Companion = new a();
    public static final String FIELD_CRITICALITY_INDICATOR = "criticalityIndicator";
    public static final String FIELD_DATA = "data";
    public static final String FIELD_ID = "id";
    public static final String FIELD_NAME = "name";
    public static final List<String> e = CollectionsKt.emptyList();

    /* renamed from: a  reason: collision with root package name */
    public final String f129a;
    public final String b;
    public final boolean c;
    public final Map<String, String> d;

    public static final class a {
        @JvmStatic
        public final List<MessageExtension> a(JSONArray jSONArray) throws a.a.a.a.e.b {
            JSONArray jSONArray2 = jSONArray;
            if (jSONArray2 == null) {
                return null;
            }
            IntRange until = RangesKt.until(0, jSONArray.length());
            ArrayList<JSONObject> arrayList = new ArrayList<>();
            Iterator it = until.iterator();
            while (it.hasNext()) {
                JSONObject optJSONObject = jSONArray2.optJSONObject(((IntIterator) it).nextInt());
                if (optJSONObject != null) {
                    arrayList.add(optJSONObject);
                }
            }
            ArrayList arrayList2 = new ArrayList(CollectionsKt.collectionSizeOrDefault(arrayList, 10));
            for (JSONObject jSONObject : arrayList) {
                if (MessageExtension.Companion != null) {
                    String optString = jSONObject.optString("name");
                    if (optString.length() <= 64) {
                        String optString2 = jSONObject.optString("id");
                        if (optString2.length() <= 64) {
                            HashMap hashMap = new HashMap();
                            JSONObject optJSONObject2 = jSONObject.optJSONObject("data");
                            if (optJSONObject2 != null) {
                                Iterator<String> keys = optJSONObject2.keys();
                                while (keys.hasNext()) {
                                    String next = keys.next();
                                    String optString3 = optJSONObject2.optString(next);
                                    if (optString3.length() <= 8059) {
                                        Intrinsics.checkExpressionValueIsNotNull(next, "key");
                                        Intrinsics.checkExpressionValueIsNotNull(optString3, "value");
                                        hashMap.put(next, optString3);
                                    } else {
                                        throw a.a.a.a.e.b.d.a("messageExtension.data.value");
                                    }
                                }
                                continue;
                            }
                            Intrinsics.checkExpressionValueIsNotNull(optString, "name");
                            Intrinsics.checkExpressionValueIsNotNull(optString2, "id");
                            arrayList2.add(new MessageExtension(optString, optString2, jSONObject.optBoolean(MessageExtension.FIELD_CRITICALITY_INDICATOR), hashMap));
                        } else {
                            throw a.a.a.a.e.b.d.a("messageExtension.id");
                        }
                    } else {
                        throw a.a.a.a.e.b.d.a("messageExtension.name");
                    }
                } else {
                    throw null;
                }
            }
            if (arrayList2.size() <= 10) {
                return arrayList2;
            }
            throw a.a.a.a.e.b.d.a("messageExtensions");
        }

        @JvmStatic
        public final JSONArray a(List<MessageExtension> list) throws JSONException {
            if (list == null) {
                return null;
            }
            JSONArray jSONArray = new JSONArray();
            for (MessageExtension json$3ds2sdk_release : list) {
                jSONArray.put(json$3ds2sdk_release.toJson$3ds2sdk_release());
            }
            return jSONArray;
        }
    }

    public static class b implements Parcelable.Creator {
        public final Object createFromParcel(Parcel parcel) {
            Intrinsics.checkParameterIsNotNull(parcel, "in");
            String readString = parcel.readString();
            String readString2 = parcel.readString();
            boolean z = parcel.readInt() != 0;
            int readInt = parcel.readInt();
            LinkedHashMap linkedHashMap = new LinkedHashMap(readInt);
            while (readInt != 0) {
                linkedHashMap.put(parcel.readString(), parcel.readString());
                readInt--;
            }
            return new MessageExtension(readString, readString2, z, linkedHashMap);
        }

        public final Object[] newArray(int i) {
            return new MessageExtension[i];
        }
    }

    public MessageExtension(String str, String str2, boolean z, Map<String, String> map) {
        Intrinsics.checkParameterIsNotNull(str, "name");
        Intrinsics.checkParameterIsNotNull(str2, "id");
        Intrinsics.checkParameterIsNotNull(map, "data");
        this.f129a = str;
        this.b = str2;
        this.c = z;
        this.d = map;
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ MessageExtension(String str, String str2, boolean z, Map map, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(str, str2, z, (i & 8) != 0 ? MapsKt.emptyMap() : map);
    }

    public static /* synthetic */ MessageExtension copy$default(MessageExtension messageExtension, String str, String str2, boolean z, Map<String, String> map, int i, Object obj) {
        if ((i & 1) != 0) {
            str = messageExtension.f129a;
        }
        if ((i & 2) != 0) {
            str2 = messageExtension.b;
        }
        if ((i & 4) != 0) {
            z = messageExtension.c;
        }
        if ((i & 8) != 0) {
            map = messageExtension.d;
        }
        return messageExtension.copy(str, str2, z, map);
    }

    @JvmStatic
    public static final List<MessageExtension> fromJson(JSONArray jSONArray) throws a.a.a.a.e.b {
        return Companion.a(jSONArray);
    }

    @JvmStatic
    public static final JSONArray toJsonArray(List<MessageExtension> list) throws JSONException {
        return Companion.a(list);
    }

    public final String component1() {
        return this.f129a;
    }

    public final String component2$3ds2sdk_release() {
        return this.b;
    }

    public final boolean component3() {
        return this.c;
    }

    public final MessageExtension copy(String str, String str2, boolean z, Map<String, String> map) {
        Intrinsics.checkParameterIsNotNull(str, "name");
        Intrinsics.checkParameterIsNotNull(str2, "id");
        Intrinsics.checkParameterIsNotNull(map, "data");
        return new MessageExtension(str, str2, z, map);
    }

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof MessageExtension)) {
            return false;
        }
        MessageExtension messageExtension = (MessageExtension) obj;
        return Intrinsics.areEqual((Object) this.f129a, (Object) messageExtension.f129a) && Intrinsics.areEqual((Object) this.b, (Object) messageExtension.b) && this.c == messageExtension.c && Intrinsics.areEqual((Object) this.d, (Object) messageExtension.d);
    }

    public final boolean getCriticalityIndicator() {
        return this.c;
    }

    public final String getId$3ds2sdk_release() {
        return this.b;
    }

    public final String getName() {
        return this.f129a;
    }

    public int hashCode() {
        String str = this.f129a;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.b;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        boolean z = this.c;
        if (z) {
            z = true;
        }
        int i2 = (hashCode2 + (z ? 1 : 0)) * 31;
        Map<String, String> map = this.d;
        if (map != null) {
            i = map.hashCode();
        }
        return i2 + i;
    }

    public final boolean isProcessable() {
        return e.contains(this.f129a);
    }

    public final JSONObject toJson$3ds2sdk_release() throws JSONException {
        JSONObject put = new JSONObject().put("name", this.f129a).put("id", this.b).put(FIELD_CRITICALITY_INDICATOR, this.c).put("data", new JSONObject(this.d));
        Intrinsics.checkExpressionValueIsNotNull(put, "JSONObject()\n           …D_DATA, JSONObject(data))");
        return put;
    }

    public String toString() {
        return "MessageExtension(name=" + this.f129a + ", id=" + this.b + ", criticalityIndicator=" + this.c + ", data=" + this.d + ")";
    }

    public void writeToParcel(Parcel parcel, int i) {
        Intrinsics.checkParameterIsNotNull(parcel, "parcel");
        parcel.writeString(this.f129a);
        parcel.writeString(this.b);
        parcel.writeInt(this.c ? 1 : 0);
        Map<String, String> map = this.d;
        parcel.writeInt(map.size());
        for (Map.Entry<String, String> next : map.entrySet()) {
            parcel.writeString(next.getKey());
            parcel.writeString(next.getValue());
        }
    }
}
