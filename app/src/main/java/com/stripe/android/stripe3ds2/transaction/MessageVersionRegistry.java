package com.stripe.android.stripe3ds2.transaction;

import java.util.Set;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt;
import kotlin.collections.SetsKt;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0003\u0018\u0000 \n2\u00020\u0001:\u0001\nB\u0005¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0007\u001a\u00020\b2\b\u0010\t\u001a\u0004\u0018\u00010\u0004R\u0011\u0010\u0003\u001a\u00020\u00048F¢\u0006\u0006\u001a\u0004\b\u0005\u0010\u0006¨\u0006\u000b"}, d2 = {"Lcom/stripe/android/stripe3ds2/transaction/MessageVersionRegistry;", "", "()V", "current", "", "getCurrent", "()Ljava/lang/String;", "isSupported", "", "messageVersion", "Companion", "3ds2sdk_release"}, k = 1, mv = {1, 1, 16})
public final class MessageVersionRegistry {
    @Deprecated
    public static final a Companion = new a();

    /* renamed from: a  reason: collision with root package name */
    public static final Set<String> f123a = SetsKt.setOf("2.1.0");

    public static final class a {
    }

    public final String getCurrent() {
        return "2.1.0";
    }

    public final boolean isSupported(String str) {
        return CollectionsKt.contains(f123a, str);
    }
}
