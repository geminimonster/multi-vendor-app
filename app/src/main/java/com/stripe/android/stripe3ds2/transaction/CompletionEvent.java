package com.stripe.android.stripe3ds2.transaction;

import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b\t\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\b\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003¢\u0006\u0002\u0010\u0005J\t\u0010\t\u001a\u00020\u0003HÆ\u0003J\t\u0010\n\u001a\u00020\u0003HÆ\u0003J\u001d\u0010\u000b\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u0003HÆ\u0001J\u0013\u0010\f\u001a\u00020\r2\b\u0010\u000e\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u000f\u001a\u00020\u0010HÖ\u0001J\t\u0010\u0011\u001a\u00020\u0003HÖ\u0001R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\u0007R\u0011\u0010\u0004\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\u0007¨\u0006\u0012"}, d2 = {"Lcom/stripe/android/stripe3ds2/transaction/CompletionEvent;", "", "sdkTransactionId", "", "transactionStatus", "(Ljava/lang/String;Ljava/lang/String;)V", "getSdkTransactionId", "()Ljava/lang/String;", "getTransactionStatus", "component1", "component2", "copy", "equals", "", "other", "hashCode", "", "toString", "3ds2sdk_release"}, k = 1, mv = {1, 1, 16})
public final class CompletionEvent {
    public final String sdkTransactionId;
    public final String transactionStatus;

    public CompletionEvent(String str, String str2) {
        Intrinsics.checkParameterIsNotNull(str, "sdkTransactionId");
        Intrinsics.checkParameterIsNotNull(str2, "transactionStatus");
        this.sdkTransactionId = str;
        this.transactionStatus = str2;
    }

    public static /* synthetic */ CompletionEvent copy$default(CompletionEvent completionEvent, String str, String str2, int i, Object obj) {
        if ((i & 1) != 0) {
            str = completionEvent.sdkTransactionId;
        }
        if ((i & 2) != 0) {
            str2 = completionEvent.transactionStatus;
        }
        return completionEvent.copy(str, str2);
    }

    public final String component1() {
        return this.sdkTransactionId;
    }

    public final String component2() {
        return this.transactionStatus;
    }

    public final CompletionEvent copy(String str, String str2) {
        Intrinsics.checkParameterIsNotNull(str, "sdkTransactionId");
        Intrinsics.checkParameterIsNotNull(str2, "transactionStatus");
        return new CompletionEvent(str, str2);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof CompletionEvent)) {
            return false;
        }
        CompletionEvent completionEvent = (CompletionEvent) obj;
        return Intrinsics.areEqual((Object) this.sdkTransactionId, (Object) completionEvent.sdkTransactionId) && Intrinsics.areEqual((Object) this.transactionStatus, (Object) completionEvent.transactionStatus);
    }

    public final String getSdkTransactionId() {
        return this.sdkTransactionId;
    }

    public final String getTransactionStatus() {
        return this.transactionStatus;
    }

    public int hashCode() {
        String str = this.sdkTransactionId;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.transactionStatus;
        if (str2 != null) {
            i = str2.hashCode();
        }
        return hashCode + i;
    }

    public String toString() {
        return "CompletionEvent(sdkTransactionId=" + this.sdkTransactionId + ", transactionStatus=" + this.transactionStatus + ")";
    }
}
