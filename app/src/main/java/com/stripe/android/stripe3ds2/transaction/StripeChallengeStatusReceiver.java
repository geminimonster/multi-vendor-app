package com.stripe.android.stripe3ds2.transaction;

import a.a.a.a.d.o;
import a.a.a.a.f.b;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000F\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u0016\u0018\u00002\u00020\u0001B\u0007\b\u0016¢\u0006\u0002\u0010\u0002B\u0019\b\u0000\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\b\b\u0002\u0010\u0005\u001a\u00020\u0006¢\u0006\u0002\u0010\u0007J\u001e\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000b2\f\u0010\f\u001a\b\u0012\u0004\u0012\u00020\t0\rH\u0016J&\u0010\u000e\u001a\u00020\t2\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010\n\u001a\u00020\u000b2\f\u0010\f\u001a\b\u0012\u0004\u0012\u00020\t0\rH\u0016J\u001e\u0010\u0011\u001a\u00020\t2\u0006\u0010\u0012\u001a\u00020\u00132\f\u0010\f\u001a\b\u0012\u0004\u0012\u00020\t0\rH\u0016J\u001e\u0010\u0014\u001a\u00020\t2\u0006\u0010\u0015\u001a\u00020\u00162\f\u0010\f\u001a\b\u0012\u0004\u0012\u00020\t0\rH\u0016J\b\u0010\u0017\u001a\u00020\tH\u0002J\u001e\u0010\u0018\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000b2\f\u0010\f\u001a\b\u0012\u0004\u0012\u00020\t0\rH\u0016R\u000e\u0010\u0003\u001a\u00020\u0004X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0019"}, d2 = {"Lcom/stripe/android/stripe3ds2/transaction/StripeChallengeStatusReceiver;", "Lcom/stripe/android/stripe3ds2/transaction/ChallengeStatusReceiver;", "()V", "imageCache", "Lcom/stripe/android/stripe3ds2/utils/ImageCache;", "logger", "Lcom/stripe/android/stripe3ds2/transaction/Logger;", "(Lcom/stripe/android/stripe3ds2/utils/ImageCache;Lcom/stripe/android/stripe3ds2/transaction/Logger;)V", "cancelled", "", "uiTypeCode", "", "onReceiverCompleted", "Lkotlin/Function0;", "completed", "completionEvent", "Lcom/stripe/android/stripe3ds2/transaction/CompletionEvent;", "protocolError", "protocolErrorEvent", "Lcom/stripe/android/stripe3ds2/transaction/ProtocolErrorEvent;", "runtimeError", "runtimeErrorEvent", "Lcom/stripe/android/stripe3ds2/transaction/RuntimeErrorEvent;", "statusReceived", "timedout", "3ds2sdk_release"}, k = 1, mv = {1, 1, 16})
public class StripeChallengeStatusReceiver implements ChallengeStatusReceiver {

    /* renamed from: a  reason: collision with root package name */
    public final b f124a;
    public final o b;

    public StripeChallengeStatusReceiver() {
        this(b.a.c, (o) null, 2, (DefaultConstructorMarker) null);
    }

    public StripeChallengeStatusReceiver(b bVar, o oVar) {
        Intrinsics.checkParameterIsNotNull(bVar, "imageCache");
        Intrinsics.checkParameterIsNotNull(oVar, "logger");
        this.f124a = bVar;
        this.b = oVar;
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ StripeChallengeStatusReceiver(b bVar, o oVar, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(bVar, (i & 2) != 0 ? o.f58a.a() : oVar);
    }

    public void cancelled(String str, Function0<Unit> function0) {
        Intrinsics.checkParameterIsNotNull(str, "uiTypeCode");
        Intrinsics.checkParameterIsNotNull(function0, "onReceiverCompleted");
        this.b.a("StripeChallengeStatusReceiver#cancelled()");
        this.f124a.a();
    }

    public void completed(CompletionEvent completionEvent, String str, Function0<Unit> function0) {
        Intrinsics.checkParameterIsNotNull(completionEvent, "completionEvent");
        Intrinsics.checkParameterIsNotNull(str, "uiTypeCode");
        Intrinsics.checkParameterIsNotNull(function0, "onReceiverCompleted");
        this.b.a("StripeChallengeStatusReceiver#completed()");
        this.f124a.a();
    }

    public void protocolError(ProtocolErrorEvent protocolErrorEvent, Function0<Unit> function0) {
        Intrinsics.checkParameterIsNotNull(protocolErrorEvent, "protocolErrorEvent");
        Intrinsics.checkParameterIsNotNull(function0, "onReceiverCompleted");
        this.b.a("StripeChallengeStatusReceiver#protocolError()");
        this.f124a.a();
    }

    public void runtimeError(RuntimeErrorEvent runtimeErrorEvent, Function0<Unit> function0) {
        Intrinsics.checkParameterIsNotNull(runtimeErrorEvent, "runtimeErrorEvent");
        Intrinsics.checkParameterIsNotNull(function0, "onReceiverCompleted");
        this.b.a("StripeChallengeStatusReceiver#runtimeError()");
        this.f124a.a();
    }

    public void timedout(String str, Function0<Unit> function0) {
        Intrinsics.checkParameterIsNotNull(str, "uiTypeCode");
        Intrinsics.checkParameterIsNotNull(function0, "onReceiverCompleted");
        this.b.a("StripeChallengeStatusReceiver#timedout()");
        this.f124a.a();
    }
}
