package com.stripe.android.stripe3ds2.views;

import a.a.a.a.a.j;
import a.a.a.a.g.m;
import a.a.a.a.g.q;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelStoreOwner;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import com.stripe.android.stripe3ds2.init.ui.StripeUiCustomization;
import com.stripe.android.stripe3ds2.init.ui.ToolbarCustomization;
import com.stripe.android.stripe3ds2.utils.CustomizeUtils;
import com.ults.listeners.SdkChallengeInterface;
import kotlin.Lazy;
import kotlin.LazyKt;
import kotlin.Metadata;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u0000 \u001c2\u00020\u0001:\u0003\u001b\u001c\u001dB\u0005¢\u0006\u0002\u0010\u0002J\b\u0010\u0015\u001a\u00020\u0016H\u0016J\u0012\u0010\u0017\u001a\u00020\u00162\b\u0010\u0018\u001a\u0004\u0018\u00010\u0019H\u0014J\b\u0010\u001a\u001a\u00020\u0016H\u0014R\u0010\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u000e¢\u0006\u0002\n\u0000R\u001b\u0010\u0005\u001a\u00020\u00068BX\u0002¢\u0006\f\n\u0004\b\t\u0010\n\u001a\u0004\b\u0007\u0010\bR\u001b\u0010\u000b\u001a\u00020\f8BX\u0002¢\u0006\f\n\u0004\b\u000f\u0010\n\u001a\u0004\b\r\u0010\u000eR\u001b\u0010\u0010\u001a\u00020\u00118BX\u0002¢\u0006\f\n\u0004\b\u0014\u0010\n\u001a\u0004\b\u0012\u0010\u0013¨\u0006\u001e"}, d2 = {"Lcom/stripe/android/stripe3ds2/views/ChallengeProgressDialogActivity;", "Landroidx/appcompat/app/AppCompatActivity;", "()V", "dialogBroadcastReceiver", "Lcom/stripe/android/stripe3ds2/views/ChallengeProgressDialogActivity$DialogBroadcastReceiver;", "localBroadcastManager", "Landroidx/localbroadcastmanager/content/LocalBroadcastManager;", "getLocalBroadcastManager", "()Landroidx/localbroadcastmanager/content/LocalBroadcastManager;", "localBroadcastManager$delegate", "Lkotlin/Lazy;", "viewBinding", "Lcom/stripe/android/stripe3ds2/databinding/ProgressViewLayoutBinding;", "getViewBinding", "()Lcom/stripe/android/stripe3ds2/databinding/ProgressViewLayoutBinding;", "viewBinding$delegate", "viewModel", "Lcom/stripe/android/stripe3ds2/views/ChallengeProgressDialogActivity$ChallengeProgressDialogActivityViewModel;", "getViewModel", "()Lcom/stripe/android/stripe3ds2/views/ChallengeProgressDialogActivity$ChallengeProgressDialogActivityViewModel;", "viewModel$delegate", "onBackPressed", "", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onStop", "ChallengeProgressDialogActivityViewModel", "Companion", "DialogBroadcastReceiver", "3ds2sdk_release"}, k = 1, mv = {1, 1, 16})
public final class ChallengeProgressDialogActivity extends AppCompatActivity {
    public static final Companion Companion = new Companion((DefaultConstructorMarker) null);

    /* renamed from: a  reason: collision with root package name */
    public final Lazy f135a = LazyKt.lazy(new c(this));
    public b b;
    public final Lazy c = LazyKt.lazy(new e(this));
    public final Lazy d = LazyKt.lazy(new f(this));

    @Metadata(bv = {1, 0, 3}, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0018\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u0004H\u0007J(\u0010\u0007\u001a\u00020\b2\u0006\u0010\f\u001a\u00020\r2\u0006\u0010\u000b\u001a\u00020\u00042\u0006\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u0011H\u0007R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\u0012"}, d2 = {"Lcom/stripe/android/stripe3ds2/views/ChallengeProgressDialogActivity$Companion;", "", "()V", "EXTRA_CANCELABLE", "", "EXTRA_DIRECTORY_SERVER_NAME", "EXTRA_UI_CUSTOMIZATION", "show", "", "activity", "Landroid/app/Activity;", "directoryServerName", "context", "Landroid/content/Context;", "cancelable", "", "uiCustomization", "Lcom/stripe/android/stripe3ds2/init/ui/StripeUiCustomization;", "3ds2sdk_release"}, k = 1, mv = {1, 1, 16})
    public static final class Companion {
        public Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        @JvmStatic
        public final void show(Activity activity, String str) {
            Intrinsics.checkParameterIsNotNull(activity, "activity");
            Intrinsics.checkParameterIsNotNull(str, "directoryServerName");
            StripeUiCustomization createWithAppTheme = StripeUiCustomization.createWithAppTheme(activity);
            Intrinsics.checkExpressionValueIsNotNull(createWithAppTheme, "StripeUiCustomization.createWithAppTheme(activity)");
            show(activity, str, false, createWithAppTheme);
        }

        @JvmStatic
        public final void show(Context context, String str, boolean z, StripeUiCustomization stripeUiCustomization) {
            Intrinsics.checkParameterIsNotNull(context, "context");
            Intrinsics.checkParameterIsNotNull(str, "directoryServerName");
            Intrinsics.checkParameterIsNotNull(stripeUiCustomization, "uiCustomization");
            context.startActivity(new Intent(context, ChallengeProgressDialogActivity.class).putExtra("extra_directory_server_name", str).putExtra("extra_cancelable", z).putExtra("extra_ui_customization", stripeUiCustomization));
        }
    }

    public static final class a extends ViewModel {

        /* renamed from: a  reason: collision with root package name */
        public final MutableLiveData<Boolean> f136a = new MutableLiveData<>();
    }

    public static final class b extends BroadcastReceiver {

        /* renamed from: a  reason: collision with root package name */
        public final MutableLiveData<Boolean> f137a;

        public b(MutableLiveData<Boolean> mutableLiveData) {
            Intrinsics.checkParameterIsNotNull(mutableLiveData, "finishLiveData");
            this.f137a = mutableLiveData;
        }

        public void onReceive(Context context, Intent intent) {
            Intrinsics.checkParameterIsNotNull(context, "context");
            Intrinsics.checkParameterIsNotNull(intent, "intent");
            this.f137a.setValue(true);
        }
    }

    public static final class c extends Lambda implements Function0<LocalBroadcastManager> {

        /* renamed from: a  reason: collision with root package name */
        public final /* synthetic */ ChallengeProgressDialogActivity f138a;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(ChallengeProgressDialogActivity challengeProgressDialogActivity) {
            super(0);
            this.f138a = challengeProgressDialogActivity;
        }

        public Object invoke() {
            return LocalBroadcastManager.getInstance(this.f138a);
        }
    }

    public static final class d<T> implements Observer<Boolean> {

        /* renamed from: a  reason: collision with root package name */
        public final /* synthetic */ ChallengeProgressDialogActivity f139a;

        public d(ChallengeProgressDialogActivity challengeProgressDialogActivity) {
            this.f139a = challengeProgressDialogActivity;
        }

        public void onChanged(Object obj) {
            Boolean bool = (Boolean) obj;
            Intrinsics.checkExpressionValueIsNotNull(bool, "shouldFinish");
            if (bool.booleanValue()) {
                this.f139a.finish();
            }
        }
    }

    public static final class e extends Lambda implements Function0<j> {

        /* renamed from: a  reason: collision with root package name */
        public final /* synthetic */ ChallengeProgressDialogActivity f140a;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(ChallengeProgressDialogActivity challengeProgressDialogActivity) {
            super(0);
            this.f140a = challengeProgressDialogActivity;
        }

        public Object invoke() {
            return j.a(this.f140a.getLayoutInflater());
        }
    }

    public static final class f extends Lambda implements Function0<a> {

        /* renamed from: a  reason: collision with root package name */
        public final /* synthetic */ ChallengeProgressDialogActivity f141a;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(ChallengeProgressDialogActivity challengeProgressDialogActivity) {
            super(0);
            this.f141a = challengeProgressDialogActivity;
        }

        public Object invoke() {
            return (a) new ViewModelProvider((ViewModelStoreOwner) this.f141a, (ViewModelProvider.Factory) new ViewModelProvider.NewInstanceFactory()).get(a.class);
        }
    }

    @JvmStatic
    public static final void show(Activity activity, String str) {
        Companion.show(activity, str);
    }

    @JvmStatic
    public static final void show(Context context, String str, boolean z, StripeUiCustomization stripeUiCustomization) {
        Companion.show(context, str, z, stripeUiCustomization);
    }

    public final j a() {
        return (j) this.c.getValue();
    }

    public void onBackPressed() {
        if (getIntent().getBooleanExtra("extra_cancelable", false)) {
            super.onBackPressed();
        }
    }

    public void onCreate(Bundle bundle) {
        ToolbarCustomization toolbarCustomization;
        super.onCreate(bundle);
        setContentView((View) a().f15a);
        ((a) this.d.getValue()).f136a.observe(this, new d(this));
        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.hide();
        }
        StripeUiCustomization stripeUiCustomization = (StripeUiCustomization) getIntent().getParcelableExtra("extra_ui_customization");
        if (!(stripeUiCustomization == null || (toolbarCustomization = stripeUiCustomization.getToolbarCustomization()) == null)) {
            m.a aVar = m.b;
            Intrinsics.checkExpressionValueIsNotNull(toolbarCustomization, "toolbarCustomization");
            aVar.a(this, toolbarCustomization);
        }
        q.a a2 = q.a.e.a(getIntent().getStringExtra("extra_directory_server_name"));
        ImageView imageView = a().b;
        Intrinsics.checkExpressionValueIsNotNull(imageView, "viewBinding.brandLogo");
        imageView.setImageDrawable(ContextCompat.getDrawable(this, a2.b));
        imageView.setContentDescription(getString(a2.c));
        imageView.setVisibility(0);
        ProgressBar progressBar = a().c;
        Intrinsics.checkExpressionValueIsNotNull(progressBar, "viewBinding.progressBar");
        CustomizeUtils.INSTANCE.applyProgressBarColor$3ds2sdk_release(progressBar, stripeUiCustomization);
        b bVar = new b(((a) this.d.getValue()).f136a);
        this.b = bVar;
        ((LocalBroadcastManager) this.f135a.getValue()).registerReceiver(bVar, new IntentFilter(SdkChallengeInterface.UL_HANDLE_CHALLENGE_ACTION));
    }

    public void onStop() {
        b bVar = this.b;
        if (bVar != null) {
            ((LocalBroadcastManager) this.f135a.getValue()).unregisterReceiver(bVar);
        }
        this.b = null;
        super.onStop();
    }
}
