package com.stripe.android.stripe3ds2.service;

import a.a.a.a.b.d;
import a.a.a.a.b.e;
import a.a.a.a.b.g;
import a.a.a.a.b.h;
import a.a.a.a.b.i;
import a.a.a.a.b.k;
import a.a.a.a.c.c;
import a.a.a.a.c.l;
import a.a.a.a.c.n;
import a.a.a.a.d.j;
import a.a.a.a.d.o;
import a.a.a.a.d.s;
import a.a.a.a.d.u;
import a.a.a.a.d.z;
import a.a.a.a.f.b;
import a.a.a.a.g.q;
import android.content.Context;
import android.content.Intent;
import android.os.Parcel;
import android.os.Parcelable;
import com.stripe.android.stripe3ds2.exceptions.InvalidInputException;
import com.stripe.android.stripe3ds2.exceptions.SDKAlreadyInitializedException;
import com.stripe.android.stripe3ds2.exceptions.SDKNotInitializedException;
import com.stripe.android.stripe3ds2.exceptions.SDKRuntimeException;
import com.stripe.android.stripe3ds2.init.Warning;
import com.stripe.android.stripe3ds2.init.ui.StripeUiCustomization;
import com.stripe.android.stripe3ds2.init.ui.UiCustomization;
import com.stripe.android.stripe3ds2.transaction.MessageVersionRegistry;
import com.stripe.android.stripe3ds2.transaction.Transaction;
import java.io.InputStream;
import java.security.KeyFactory;
import java.security.PublicKey;
import java.security.cert.Certificate;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.security.spec.X509EncodedKeySpec;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicBoolean;
import javax.net.ssl.SSLSocketFactory;
import kotlin.Metadata;
import kotlin.Result;
import kotlin.ResultKt;
import kotlin.collections.CollectionsKt;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u0000 C2\u00020\u0001:\u0001CB%\b\u0017\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\b\b\u0002\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bB+\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\t\u001a\u00020\n\u0012\b\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\b\b\u0002\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\u000bBC\b\u0012\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\f\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u0012\u0006\u0010\u0010\u001a\u00020\u0011\u0012\u0006\u0010\t\u001a\u00020\n\u0012\b\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\b\b\u0002\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\u0012BG\b\u0011\u0012\u0006\u0010\u0013\u001a\u00020\u0014\u0012\u0006\u0010\u0015\u001a\u00020\u0016\u0012\u0006\u0010\u0017\u001a\u00020\u0018\u0012\u0006\u0010\u0019\u001a\u00020\u001a\u0012\u0006\u0010\u001b\u001a\u00020\u001c\u0012\u0006\u0010\f\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u0012\u0006\u0010\u0010\u001a\u00020\u0011¢\u0006\u0002\u0010\u001dJ\b\u0010.\u001a\u00020/H\u0016J\u0010\u00100\u001a\u00020\"2\u0006\u0010!\u001a\u00020\"H\u0002J\u001a\u00101\u001a\u0002022\u0006\u00103\u001a\u00020\n2\b\u00104\u001a\u0004\u0018\u00010\nH\u0016J*\u00101\u001a\u0002022\u0006\u00103\u001a\u00020\n2\b\u00104\u001a\u0004\u0018\u00010\n2\u0006\u00105\u001a\u00020\u00072\u0006\u00106\u001a\u00020\nH\u0016J\\\u00101\u001a\u0002022\u0006\u00103\u001a\u00020\n2\b\u00104\u001a\u0004\u0018\u00010\n2\u0006\u00105\u001a\u00020\u00072\u0006\u00106\u001a\u00020\n2\f\u00107\u001a\b\u0012\u0004\u0012\u0002080*2\u0006\u00109\u001a\u00020:2\b\u0010;\u001a\u0004\u0018\u00010\n2\b\u0010<\u001a\u0004\u0018\u00010=2\u0006\u0010>\u001a\u00020?H\u0016J\u0012\u0010@\u001a\u00020/2\b\u0010!\u001a\u0004\u0018\u00010AH\u0016J\b\u0010B\u001a\u00020/H\u0002R\u000e\u0010\u000e\u001a\u00020\u000fX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\rX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0014X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u001b\u001a\u00020\u001cX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0019\u001a\u00020\u001aX\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\u001e\u001a\u00020\n8VX\u0004¢\u0006\u0006\u001a\u0004\b\u001f\u0010 R\u000e\u0010\u0015\u001a\u00020\u0016X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0017\u001a\u00020\u0018X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0004¢\u0006\u0002\n\u0000R&\u0010!\u001a\u0004\u0018\u00010\"8\u0000@\u0000X\u000e¢\u0006\u0014\n\u0000\u0012\u0004\b#\u0010$\u001a\u0004\b%\u0010&\"\u0004\b'\u0010(R\u001a\u0010)\u001a\b\u0012\u0004\u0012\u00020+0*8VX\u0004¢\u0006\u0006\u001a\u0004\b,\u0010-¨\u0006D"}, d2 = {"Lcom/stripe/android/stripe3ds2/service/StripeThreeDs2ServiceImpl;", "Lcom/stripe/android/stripe3ds2/service/StripeThreeDs2Service;", "context", "Landroid/content/Context;", "sslSocketFactory", "Ljavax/net/ssl/SSLSocketFactory;", "enableLogging", "", "(Landroid/content/Context;Ljavax/net/ssl/SSLSocketFactory;Z)V", "sdkReferenceNumber", "", "(Landroid/content/Context;Ljava/lang/String;Ljavax/net/ssl/SSLSocketFactory;Z)V", "imageCache", "Lcom/stripe/android/stripe3ds2/utils/ImageCache;", "challengeStatusReceiverProvider", "Lcom/stripe/android/stripe3ds2/transaction/ChallengeStatusReceiverProvider;", "transactionTimerProvider", "Lcom/stripe/android/stripe3ds2/transaction/TransactionTimerProvider;", "(Landroid/content/Context;Lcom/stripe/android/stripe3ds2/utils/ImageCache;Lcom/stripe/android/stripe3ds2/transaction/ChallengeStatusReceiverProvider;Lcom/stripe/android/stripe3ds2/transaction/TransactionTimerProvider;Ljava/lang/String;Ljavax/net/ssl/SSLSocketFactory;Z)V", "isInitialized", "Ljava/util/concurrent/atomic/AtomicBoolean;", "securityChecker", "Lcom/stripe/android/stripe3ds2/init/SecurityChecker;", "transactionFactory", "Lcom/stripe/android/stripe3ds2/transaction/TransactionFactory;", "publicKeyFactory", "Lcom/stripe/android/stripe3ds2/security/PublicKeyFactory;", "messageVersionRegistry", "Lcom/stripe/android/stripe3ds2/transaction/MessageVersionRegistry;", "(Ljava/util/concurrent/atomic/AtomicBoolean;Lcom/stripe/android/stripe3ds2/init/SecurityChecker;Lcom/stripe/android/stripe3ds2/transaction/TransactionFactory;Lcom/stripe/android/stripe3ds2/security/PublicKeyFactory;Lcom/stripe/android/stripe3ds2/transaction/MessageVersionRegistry;Lcom/stripe/android/stripe3ds2/utils/ImageCache;Lcom/stripe/android/stripe3ds2/transaction/ChallengeStatusReceiverProvider;Lcom/stripe/android/stripe3ds2/transaction/TransactionTimerProvider;)V", "sdkVersion", "getSdkVersion", "()Ljava/lang/String;", "uiCustomization", "Lcom/stripe/android/stripe3ds2/init/ui/StripeUiCustomization;", "uiCustomization$annotations", "()V", "getUiCustomization$3ds2sdk_release", "()Lcom/stripe/android/stripe3ds2/init/ui/StripeUiCustomization;", "setUiCustomization$3ds2sdk_release", "(Lcom/stripe/android/stripe3ds2/init/ui/StripeUiCustomization;)V", "warnings", "", "Lcom/stripe/android/stripe3ds2/init/Warning;", "getWarnings", "()Ljava/util/List;", "cleanup", "", "copyUiCustomization", "createTransaction", "Lcom/stripe/android/stripe3ds2/transaction/Transaction;", "directoryServerID", "messageVersion", "isLiveMode", "directoryServerName", "rootCerts", "Ljava/security/cert/X509Certificate;", "dsPublicKey", "Ljava/security/PublicKey;", "keyId", "challengeCompletionIntent", "Landroid/content/Intent;", "challengeCompletionRequestCode", "", "initialize", "Lcom/stripe/android/stripe3ds2/init/ui/UiCustomization;", "requireInitialization", "Companion", "3ds2sdk_release"}, k = 1, mv = {1, 1, 16})
public final class StripeThreeDs2ServiceImpl implements StripeThreeDs2Service {
    @Deprecated
    public static final a Companion = new a();

    /* renamed from: a  reason: collision with root package name */
    public StripeUiCustomization f121a;
    public final AtomicBoolean b;
    public final k c;
    public final u d;
    public final l e;
    public final MessageVersionRegistry f;
    public final b g;
    public final j h;
    public final z i;

    public static final class a {
    }

    public StripeThreeDs2ServiceImpl(Context context) {
        this(context, (SSLSocketFactory) null, false, 6, (DefaultConstructorMarker) null);
    }

    public StripeThreeDs2ServiceImpl(Context context, b bVar, j jVar, z zVar, String str, SSLSocketFactory sSLSocketFactory, boolean z) {
        Context context2 = context;
        SSLSocketFactory sSLSocketFactory2 = sSLSocketFactory;
        this.b = new AtomicBoolean(false);
        this.c = new k.a((List) null, 1);
        this.g = bVar;
        this.h = jVar;
        this.i = zVar;
        n nVar = new n();
        this.f = new MessageVersionRegistry();
        this.e = new l(context2);
        o.a aVar = o.f58a;
        o b2 = z ? aVar.b() : aVar.a();
        g gVar = new g(context2);
        Context applicationContext = context.getApplicationContext();
        Intrinsics.checkExpressionValueIsNotNull(applicationContext, "context.applicationContext");
        this.d = new u.a(new a.a.a.a.d.b((a.a.a.a.b.a) new a.a.a.a.b.b(applicationContext, gVar), (d) new e(gVar), this.c, (a.a.a.a.c.e) nVar, (a.a.a.a.f.d<h>) new i(context2), this.f, str), nVar, this.f, str, b2, (q) null, (a.a.a.a.d.n) null, (j) null, (Intent) null, 0, 992);
        if (sSLSocketFactory2 != null) {
            s.c.a(sSLSocketFactory2);
        }
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public StripeThreeDs2ServiceImpl(Context context, String str, SSLSocketFactory sSLSocketFactory, boolean z) {
        this(context, b.a.c, j.a.b, z.a.b, str, sSLSocketFactory, z);
        Intrinsics.checkParameterIsNotNull(context, "context");
        Intrinsics.checkParameterIsNotNull(str, "sdkReferenceNumber");
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ StripeThreeDs2ServiceImpl(Context context, String str, SSLSocketFactory sSLSocketFactory, boolean z, int i2, DefaultConstructorMarker defaultConstructorMarker) {
        this(context, str, sSLSocketFactory, (i2 & 8) != 0 ? false : z);
    }

    public StripeThreeDs2ServiceImpl(Context context, SSLSocketFactory sSLSocketFactory) {
        this(context, sSLSocketFactory, false, 4, (DefaultConstructorMarker) null);
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public StripeThreeDs2ServiceImpl(Context context, SSLSocketFactory sSLSocketFactory, boolean z) {
        this(context, "3DS_LOA_SDK_STIN_020100_00142", sSLSocketFactory, z);
        Intrinsics.checkParameterIsNotNull(context, "context");
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ StripeThreeDs2ServiceImpl(Context context, SSLSocketFactory sSLSocketFactory, boolean z, int i2, DefaultConstructorMarker defaultConstructorMarker) {
        this(context, (i2 & 2) != 0 ? null : sSLSocketFactory, (i2 & 4) != 0 ? false : z);
    }

    public StripeThreeDs2ServiceImpl(AtomicBoolean atomicBoolean, k kVar, u uVar, l lVar, MessageVersionRegistry messageVersionRegistry, b bVar, j jVar, z zVar) {
        Intrinsics.checkParameterIsNotNull(atomicBoolean, "isInitialized");
        Intrinsics.checkParameterIsNotNull(kVar, "securityChecker");
        Intrinsics.checkParameterIsNotNull(uVar, "transactionFactory");
        Intrinsics.checkParameterIsNotNull(lVar, "publicKeyFactory");
        Intrinsics.checkParameterIsNotNull(messageVersionRegistry, "messageVersionRegistry");
        Intrinsics.checkParameterIsNotNull(bVar, "imageCache");
        Intrinsics.checkParameterIsNotNull(jVar, "challengeStatusReceiverProvider");
        Intrinsics.checkParameterIsNotNull(zVar, "transactionTimerProvider");
        this.b = atomicBoolean;
        this.c = kVar;
        this.d = uVar;
        this.e = lVar;
        this.f = messageVersionRegistry;
        this.g = bVar;
        this.h = jVar;
        this.i = zVar;
    }

    public static /* synthetic */ void uiCustomization$annotations() {
    }

    public final void a() {
        if (!this.b.get()) {
            throw new SDKNotInitializedException(new RuntimeException());
        }
    }

    public void cleanup() throws SDKNotInitializedException {
        a();
        this.g.a();
        this.h.a();
        this.i.a();
    }

    public Transaction createTransaction(String str, String str2) {
        Intrinsics.checkParameterIsNotNull(str, "directoryServerID");
        return createTransaction(str, str2, true, "visa");
    }

    public Transaction createTransaction(String str, String str2, boolean z, String str3) throws InvalidInputException, SDKNotInitializedException, SDKRuntimeException {
        PublicKey publicKey;
        Object obj;
        Object obj2;
        Intrinsics.checkParameterIsNotNull(str, "directoryServerID");
        Intrinsics.checkParameterIsNotNull(str3, "directoryServerName");
        l lVar = this.e;
        if (lVar != null) {
            Intrinsics.checkParameterIsNotNull(str, "directoryServerId");
            c a2 = c.g.a(str);
            if (a2.a()) {
                String str4 = a2.c;
                try {
                    Result.Companion companion = Result.Companion;
                    CertificateFactory instance = CertificateFactory.getInstance("X.509");
                    InputStream open = lVar.f32a.getAssets().open(str4);
                    Intrinsics.checkExpressionValueIsNotNull(open, "context.assets.open(fileName)");
                    obj2 = Result.m4constructorimpl(instance.generateCertificate(open));
                } catch (Throwable th) {
                    Result.Companion companion2 = Result.Companion;
                    obj2 = Result.m4constructorimpl(ResultKt.createFailure(th));
                }
                Throwable r1 = Result.m7exceptionOrNullimpl(obj2);
                if (r1 == null) {
                    Intrinsics.checkExpressionValueIsNotNull(obj2, "runCatching {\n          …eException(it))\n        }");
                    publicKey = ((Certificate) obj2).getPublicKey();
                    Intrinsics.checkExpressionValueIsNotNull(publicKey, "generateCertificate(dire…erver.fileName).publicKey");
                } else {
                    throw new SDKRuntimeException(new RuntimeException(r1));
                }
            } else {
                String str5 = a2.c;
                a.a.a.a.c.a aVar = a2.b;
                try {
                    Result.Companion companion3 = Result.Companion;
                    obj = Result.m4constructorimpl(KeyFactory.getInstance(aVar.f26a).generatePublic(new X509EncodedKeySpec(lVar.a(str5))));
                } catch (Throwable th2) {
                    Result.Companion companion4 = Result.Companion;
                    obj = Result.m4constructorimpl(ResultKt.createFailure(th2));
                }
                Throwable r12 = Result.m7exceptionOrNullimpl(obj);
                if (r12 == null) {
                    Intrinsics.checkExpressionValueIsNotNull(obj, "runCatching {\n          …eException(it))\n        }");
                    publicKey = (PublicKey) obj;
                } else {
                    throw new SDKRuntimeException(new RuntimeException(r12));
                }
            }
            return createTransaction(str, str2, z, str3, CollectionsKt.emptyList(), publicKey, (String) null, (Intent) null, 0);
        }
        throw null;
    }

    public Transaction createTransaction(String str, String str2, boolean z, String str3, List<? extends X509Certificate> list, PublicKey publicKey, String str4, Intent intent, int i2) throws InvalidInputException, SDKNotInitializedException, SDKRuntimeException {
        String str5 = str2;
        String str6 = str3;
        Intrinsics.checkParameterIsNotNull(str, "directoryServerID");
        Intrinsics.checkParameterIsNotNull(str6, "directoryServerName");
        Intrinsics.checkParameterIsNotNull(list, "rootCerts");
        Intrinsics.checkParameterIsNotNull(publicKey, "dsPublicKey");
        a();
        if (!this.f.isSupported(str5)) {
            StringBuilder sb = new StringBuilder();
            sb.append("Message version is unsupported: ");
            if (str5 == null) {
                str5 = "";
            }
            sb.append(str5);
            throw new InvalidInputException(new RuntimeException(sb.toString()));
        }
        String uuid = UUID.randomUUID().toString();
        Intrinsics.checkExpressionValueIsNotNull(uuid, "UUID.randomUUID().toString()");
        return this.d.a(str, list, publicKey, str4, uuid, this.f121a, z, q.a.e.a(str6), intent, i2);
    }

    public String getSdkVersion() throws SDKNotInitializedException, SDKRuntimeException {
        a();
        return "1.0.0";
    }

    public final StripeUiCustomization getUiCustomization$3ds2sdk_release() {
        return this.f121a;
    }

    public List<Warning> getWarnings() {
        return this.c.getWarnings();
    }

    public void initialize(UiCustomization uiCustomization) throws InvalidInputException, SDKAlreadyInitializedException, SDKRuntimeException {
        StripeUiCustomization stripeUiCustomization;
        if (this.b.compareAndSet(false, true)) {
            if (uiCustomization instanceof StripeUiCustomization) {
                StripeUiCustomization stripeUiCustomization2 = (StripeUiCustomization) uiCustomization;
                Parcelable.Creator<StripeUiCustomization> creator = StripeUiCustomization.CREATOR;
                Intrinsics.checkExpressionValueIsNotNull(creator, "StripeUiCustomization.CREATOR");
                Intrinsics.checkParameterIsNotNull(stripeUiCustomization2, "source");
                Intrinsics.checkParameterIsNotNull(creator, "creator");
                Parcel obtain = Parcel.obtain();
                Intrinsics.checkExpressionValueIsNotNull(obtain, "Parcel.obtain()");
                stripeUiCustomization2.writeToParcel(obtain, stripeUiCustomization2.describeContents());
                obtain.setDataPosition(0);
                StripeUiCustomization createFromParcel = creator.createFromParcel(obtain);
                Intrinsics.checkExpressionValueIsNotNull(createFromParcel, "creator.createFromParcel(parcel)");
                Parcelable parcelable = createFromParcel;
                Intrinsics.checkExpressionValueIsNotNull(parcelable, "ParcelUtils.copy(uiCusto…eUiCustomization.CREATOR)");
                stripeUiCustomization = (StripeUiCustomization) parcelable;
            } else if (uiCustomization == null) {
                stripeUiCustomization = null;
            } else {
                throw new InvalidInputException(new RuntimeException("UiCustomization must be an instance of StripeUiCustomization"));
            }
            this.f121a = stripeUiCustomization;
            return;
        }
        throw new SDKAlreadyInitializedException(new RuntimeException());
    }

    public final void setUiCustomization$3ds2sdk_release(StripeUiCustomization stripeUiCustomization) {
        this.f121a = stripeUiCustomization;
    }
}
