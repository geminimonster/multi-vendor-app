package com.stripe.android.stripe3ds2.utils;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.BlendMode;
import android.graphics.BlendModeColorFilter;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.text.SpannableString;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.TypefaceSpan;
import android.util.TypedValue;
import android.view.Window;
import android.widget.ProgressBar;
import androidx.appcompat.app.AppCompatActivity;
import com.stripe.android.stripe3ds2.exceptions.InvalidInputException;
import com.stripe.android.stripe3ds2.init.ui.Customization;
import com.stripe.android.stripe3ds2.init.ui.UiCustomization;
import java.util.Arrays;
import java.util.Locale;
import kotlin.Metadata;
import kotlin.Result;
import kotlin.ResultKt;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.StringCompanionObject;
import kotlin.text.StringsKt;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000P\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0007\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\r\n\u0002\u0018\u0002\n\u0002\b\u0002\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u001f\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\b\u0010\t\u001a\u0004\u0018\u00010\nH\u0000¢\u0006\u0002\b\u000bJ\u001e\u0010\f\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0013J\u0012\u0010\u0014\u001a\u00020\u00112\b\b\u0001\u0010\u0015\u001a\u00020\u0016H\u0007J\u0017\u0010\u0017\u001a\u00020\u00162\b\b\u0001\u0010\u0015\u001a\u00020\u0016H\u0001¢\u0006\u0002\b\u0018J\u001f\u0010\u0017\u001a\u00020\u00162\b\b\u0001\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0019\u001a\u00020\u0004H\u0001¢\u0006\u0002\b\u0018J\u0010\u0010\u001a\u001a\u00020\u00112\u0006\u0010\u001b\u001a\u00020\u0011H\u0007J\u0010\u0010\u001c\u001a\u00020\u00162\u0006\u0010\u001d\u001a\u00020\u0016H\u0007J\u0010\u0010\u001e\u001a\u00020\u00162\u0006\u0010\u001f\u001a\u00020\u0016H\u0007J\u0010\u0010 \u001a\u00020\u00112\u0006\u0010!\u001a\u00020\u0011H\u0007J\u0018\u0010\"\u001a\u00020\u00062\u0006\u0010#\u001a\u00020$2\b\b\u0001\u0010%\u001a\u00020\u0016R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006&"}, d2 = {"Lcom/stripe/android/stripe3ds2/utils/CustomizeUtils;", "", "()V", "DEFAULT_DARKEN_FACTOR", "", "applyProgressBarColor", "", "progressBar", "Landroid/widget/ProgressBar;", "uiCustomization", "Lcom/stripe/android/stripe3ds2/init/ui/UiCustomization;", "applyProgressBarColor$3ds2sdk_release", "buildStyledText", "Landroid/text/SpannableString;", "context", "Landroid/content/Context;", "text", "", "customization", "Lcom/stripe/android/stripe3ds2/init/ui/Customization;", "colorIntToHex", "color", "", "darken", "darken$3ds2sdk_release", "factor", "requireValidColor", "hexColor", "requireValidDimension", "dimension", "requireValidFontSize", "fontSize", "requireValidString", "string", "setStatusBarColor", "activity", "Landroidx/appcompat/app/AppCompatActivity;", "statusBarColor", "3ds2sdk_release"}, k = 1, mv = {1, 1, 16})
public final class CustomizeUtils {
    public static final CustomizeUtils INSTANCE = new CustomizeUtils();

    @JvmStatic
    public static final String colorIntToHex(int i) {
        int alpha = Color.alpha(i);
        int blue = Color.blue(i);
        int green = Color.green(i);
        int red = Color.red(i);
        StringBuilder sb = new StringBuilder();
        sb.append("#");
        StringCompanionObject stringCompanionObject = StringCompanionObject.INSTANCE;
        Locale locale = Locale.ENGLISH;
        Intrinsics.checkExpressionValueIsNotNull(locale, "Locale.ENGLISH");
        String format = String.format(locale, "%02X", Arrays.copyOf(new Object[]{Integer.valueOf(alpha)}, 1));
        Intrinsics.checkExpressionValueIsNotNull(format, "java.lang.String.format(locale, format, *args)");
        sb.append(format);
        StringCompanionObject stringCompanionObject2 = StringCompanionObject.INSTANCE;
        Locale locale2 = Locale.ENGLISH;
        Intrinsics.checkExpressionValueIsNotNull(locale2, "Locale.ENGLISH");
        String format2 = String.format(locale2, "%02X", Arrays.copyOf(new Object[]{Integer.valueOf(red)}, 1));
        Intrinsics.checkExpressionValueIsNotNull(format2, "java.lang.String.format(locale, format, *args)");
        sb.append(format2);
        StringCompanionObject stringCompanionObject3 = StringCompanionObject.INSTANCE;
        Locale locale3 = Locale.ENGLISH;
        Intrinsics.checkExpressionValueIsNotNull(locale3, "Locale.ENGLISH");
        String format3 = String.format(locale3, "%02X", Arrays.copyOf(new Object[]{Integer.valueOf(green)}, 1));
        Intrinsics.checkExpressionValueIsNotNull(format3, "java.lang.String.format(locale, format, *args)");
        sb.append(format3);
        StringCompanionObject stringCompanionObject4 = StringCompanionObject.INSTANCE;
        Locale locale4 = Locale.ENGLISH;
        Intrinsics.checkExpressionValueIsNotNull(locale4, "Locale.ENGLISH");
        String format4 = String.format(locale4, "%02X", Arrays.copyOf(new Object[]{Integer.valueOf(blue)}, 1));
        Intrinsics.checkExpressionValueIsNotNull(format4, "java.lang.String.format(locale, format, *args)");
        sb.append(format4);
        return sb.toString();
    }

    @JvmStatic
    public static final String requireValidColor(String str) throws InvalidInputException {
        Object obj;
        Intrinsics.checkParameterIsNotNull(str, "hexColor");
        try {
            Result.Companion companion = Result.Companion;
            Color.parseColor(str);
            obj = Result.m4constructorimpl(str);
        } catch (Throwable th) {
            Result.Companion companion2 = Result.Companion;
            obj = Result.m4constructorimpl(ResultKt.createFailure(th));
        }
        if (Result.m7exceptionOrNullimpl(obj) == null) {
            return (String) obj;
        }
        throw new InvalidInputException(new RuntimeException("Unable to parse color: " + str));
    }

    @JvmStatic
    public static final int requireValidDimension(int i) throws InvalidInputException {
        if (i >= 0) {
            return i;
        }
        throw new InvalidInputException(new RuntimeException("Dimension must be greater or equal to 0"));
    }

    @JvmStatic
    public static final int requireValidFontSize(int i) throws InvalidInputException {
        if (i > 0) {
            return i;
        }
        throw new InvalidInputException(new RuntimeException("Font size must be greater than 0"));
    }

    @JvmStatic
    public static final String requireValidString(String str) throws InvalidInputException {
        Intrinsics.checkParameterIsNotNull(str, "string");
        if (!StringsKt.isBlank(str)) {
            return str;
        }
        throw new InvalidInputException(new RuntimeException("String must not be null or empty"));
    }

    public final void applyProgressBarColor$3ds2sdk_release(ProgressBar progressBar, UiCustomization uiCustomization) {
        String accentColor;
        Intrinsics.checkParameterIsNotNull(progressBar, "progressBar");
        if (uiCustomization != null && (accentColor = uiCustomization.getAccentColor()) != null) {
            int parseColor = Color.parseColor(accentColor);
            if (Build.VERSION.SDK_INT >= 21) {
                progressBar.setIndeterminateTintList(ColorStateList.valueOf(parseColor));
                return;
            }
            Drawable mutate = progressBar.getIndeterminateDrawable().mutate();
            Intrinsics.checkExpressionValueIsNotNull(mutate, "progressBar.indeterminateDrawable.mutate()");
            if (Build.VERSION.SDK_INT >= 29) {
                mutate.setColorFilter(new BlendModeColorFilter(parseColor, BlendMode.SRC_IN));
            } else {
                mutate.setColorFilter(parseColor, PorterDuff.Mode.SRC_IN);
            }
            progressBar.setIndeterminateDrawable(mutate);
        }
    }

    public final SpannableString buildStyledText(Context context, String str, Customization customization) {
        Intrinsics.checkParameterIsNotNull(context, "context");
        Intrinsics.checkParameterIsNotNull(str, "text");
        Intrinsics.checkParameterIsNotNull(customization, "customization");
        SpannableString spannableString = new SpannableString(str);
        String textColor = customization.getTextColor();
        if (textColor != null) {
            spannableString.setSpan(new ForegroundColorSpan(Color.parseColor(textColor)), 0, spannableString.length(), 0);
        }
        Integer valueOf = Integer.valueOf(customization.getTextFontSize());
        if (!(valueOf.intValue() > 0)) {
            valueOf = null;
        }
        if (valueOf != null) {
            Resources resources = context.getResources();
            Intrinsics.checkExpressionValueIsNotNull(resources, "context.resources");
            spannableString.setSpan(new AbsoluteSizeSpan((int) TypedValue.applyDimension(2, (float) valueOf.intValue(), resources.getDisplayMetrics())), 0, spannableString.length(), 0);
        }
        String textFontName = customization.getTextFontName();
        if (textFontName != null) {
            spannableString.setSpan(new TypefaceSpan(textFontName), 0, spannableString.length(), 0);
        }
        return spannableString;
    }

    public final int darken$3ds2sdk_release(int i) {
        return darken$3ds2sdk_release(i, 0.8f);
    }

    public final int darken$3ds2sdk_release(int i, float f) {
        return Color.argb(Color.alpha(i), Math.min(Math.max((int) (((float) Color.red(i)) * f), 0), 255), Math.min(Math.max((int) (((float) Color.green(i)) * f), 0), 255), Math.min(Math.max((int) (((float) Color.blue(i)) * f), 0), 255));
    }

    public final void setStatusBarColor(AppCompatActivity appCompatActivity, int i) {
        Intrinsics.checkParameterIsNotNull(appCompatActivity, "activity");
        if (Build.VERSION.SDK_INT >= 21) {
            Window window = appCompatActivity.getWindow();
            Intrinsics.checkExpressionValueIsNotNull(window, "activity.window");
            window.setStatusBarColor(i);
        }
    }
}
