package com.stripe.android.stripe3ds2.views;

import a.a.a.a.d.c;
import a.a.a.a.d.d;
import a.a.a.a.d.f;
import a.a.a.a.d.k;
import a.a.a.a.d.y;
import a.a.a.a.d.z;
import a.a.a.a.g.h;
import a.a.a.a.g.i;
import a.a.a.a.g.j;
import a.a.a.a.g.l;
import a.a.a.a.g.m;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcelable;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.ScrollView;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelStoreOwner;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import com.stripe.android.stripe3ds2.R;
import com.stripe.android.stripe3ds2.exceptions.InvalidInputException;
import com.stripe.android.stripe3ds2.init.ui.StripeUiCustomization;
import com.stripe.android.stripe3ds2.transactions.ChallengeResponseData;
import com.ults.listeners.BaseSdkChallenge;
import com.ults.listeners.ChallengeType;
import com.ults.listeners.SdkChallengeInterface;
import com.ults.listeners.challenges.MultiSelectChallenge;
import com.ults.listeners.challenges.SingleSelectChallenge;
import com.ults.listeners.challenges.TextChallenge;
import com.ults.listeners.challenges.WebChallenge;
import java.io.Serializable;
import java.util.List;
import kotlin.Lazy;
import kotlin.LazyKt;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0011\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0010\u000e\n\u0002\b\u0002\u0018\u0000 A2\u00020\u00012\u00020\u00022\u00020\u00032\u00020\u00042\u00020\u00052\u00020\u0006:\u0001AB\u0005¢\u0006\u0002\u0010\u0007J\b\u0010\u001f\u001a\u00020 H\u0016J\b\u0010!\u001a\u00020 H\u0016J\r\u0010\"\u001a\u00020 H\u0000¢\u0006\u0002\b#J\b\u0010$\u001a\u00020 H\u0016J\n\u0010%\u001a\u0004\u0018\u00010&H\u0016J\u0017\u0010'\u001a\f\u0012\u0006\u0012\u0004\u0018\u00010)\u0018\u00010(H\u0016¢\u0006\u0002\u0010*J\b\u0010+\u001a\u00020,H\u0016J\n\u0010-\u001a\u0004\u0018\u00010.H\u0016J\b\u0010/\u001a\u00020 H\u0016J\u0012\u00100\u001a\u00020 2\b\u00101\u001a\u0004\u0018\u000102H\u0014J\b\u00103\u001a\u00020 H\u0014J\b\u00104\u001a\u00020 H\u0016J\b\u00105\u001a\u00020 H\u0014J\b\u00106\u001a\u00020 H\u0014J\u0010\u00107\u001a\u00020 2\u0006\u00108\u001a\u000202H\u0014J\u0010\u00109\u001a\u00020 2\u0006\u0010:\u001a\u00020;H\u0016J\u0010\u0010<\u001a\u00020 2\u0006\u0010=\u001a\u00020;H\u0016J\u0010\u0010>\u001a\u00020 2\u0006\u0010?\u001a\u00020@H\u0016R\u001b\u0010\b\u001a\u00020\t8BX\u0002¢\u0006\f\n\u0004\b\f\u0010\r\u001a\u0004\b\n\u0010\u000bR$\u0010\u000e\u001a\u00020\u000f8\u0000@\u0000X\u000e¢\u0006\u0014\n\u0000\u0012\u0004\b\u0010\u0010\u0007\u001a\u0004\b\u0011\u0010\u0012\"\u0004\b\u0013\u0010\u0014R\u001b\u0010\u0015\u001a\u00020\u00168@X\u0002¢\u0006\f\n\u0004\b\u0019\u0010\r\u001a\u0004\b\u0017\u0010\u0018R\u001b\u0010\u001a\u001a\u00020\u001b8BX\u0002¢\u0006\f\n\u0004\b\u001e\u0010\r\u001a\u0004\b\u001c\u0010\u001d¨\u0006B"}, d2 = {"Lcom/stripe/android/stripe3ds2/views/ChallengeActivity;", "Landroidx/appcompat/app/AppCompatActivity;", "Lcom/ults/listeners/challenges/TextChallenge;", "Lcom/ults/listeners/challenges/SingleSelectChallenge;", "Lcom/ults/listeners/challenges/MultiSelectChallenge;", "Lcom/ults/listeners/challenges/WebChallenge;", "Lcom/ults/listeners/SdkChallengeInterface;", "()V", "presenter", "Lcom/stripe/android/stripe3ds2/views/ChallengePresenter;", "getPresenter", "()Lcom/stripe/android/stripe3ds2/views/ChallengePresenter;", "presenter$delegate", "Lkotlin/Lazy;", "refreshUi", "", "refreshUi$annotations", "getRefreshUi$3ds2sdk_release", "()Z", "setRefreshUi$3ds2sdk_release", "(Z)V", "viewBinding", "Lcom/stripe/android/stripe3ds2/databinding/ChallengeActivityBinding;", "getViewBinding$3ds2sdk_release", "()Lcom/stripe/android/stripe3ds2/databinding/ChallengeActivityBinding;", "viewBinding$delegate", "viewModel", "Lcom/stripe/android/stripe3ds2/views/ChallengeActivityViewModel;", "getViewModel", "()Lcom/stripe/android/stripe3ds2/views/ChallengeActivityViewModel;", "viewModel$delegate", "clickCancelButton", "", "clickSubmitButton", "dismissKeyboard", "dismissKeyboard$3ds2sdk_release", "expandTextsBeforeScreenshot", "getChallengeType", "Lcom/ults/listeners/ChallengeType;", "getCheckboxesOrdered", "", "Landroid/widget/CheckBox;", "()[Landroid/widget/CheckBox;", "getCurrentChallenge", "Lcom/ults/listeners/BaseSdkChallenge;", "getWebView", "Landroid/webkit/WebView;", "onBackPressed", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onDestroy", "onLowMemory", "onPause", "onResume", "onSaveInstanceState", "outState", "onTrimMemory", "level", "", "selectObject", "i", "typeTextChallengeValue", "s", "", "Companion", "3ds2sdk_release"}, k = 1, mv = {1, 1, 16})
public final class ChallengeActivity extends AppCompatActivity implements TextChallenge, SingleSelectChallenge, MultiSelectChallenge, WebChallenge, SdkChallengeInterface {

    /* renamed from: a  reason: collision with root package name */
    public boolean f131a;
    public final Lazy b = LazyKt.lazy(new a(this));
    public final Lazy c = LazyKt.lazy(new b(this));
    public final Lazy d = LazyKt.lazy(new c(this));

    public static final class a extends Lambda implements Function0<a.a.a.a.g.c> {

        /* renamed from: a  reason: collision with root package name */
        public final /* synthetic */ ChallengeActivity f132a;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(ChallengeActivity challengeActivity) {
            super(0);
            this.f132a = challengeActivity;
        }

        public Object invoke() {
            i.a aVar = i.i;
            Intent intent = this.f132a.getIntent();
            Intrinsics.checkExpressionValueIsNotNull(intent, "intent");
            Bundle extras = intent.getExtras();
            if (extras != null) {
                ChallengeResponseData challengeResponseData = (ChallengeResponseData) extras.getParcelable("extra_cres_data");
                if (challengeResponseData != null) {
                    Intrinsics.checkExpressionValueIsNotNull(challengeResponseData, "extras.getParcelable<Cha…sponseData is required\"))");
                    Serializable serializable = extras.getSerializable("extra_creq_data");
                    if (serializable != null) {
                        a.a.a.a.e.a aVar2 = (a.a.a.a.e.a) serializable;
                        StripeUiCustomization stripeUiCustomization = (StripeUiCustomization) extras.getParcelable("extra_ui_customization");
                        if (stripeUiCustomization != null) {
                            Intrinsics.checkExpressionValueIsNotNull(stripeUiCustomization, "extras.getParcelable<Str…tomization is required\"))");
                            Serializable serializable2 = extras.getSerializable("extra_creq_executor_config");
                            if (serializable2 != null) {
                                f.a aVar3 = (f.a) serializable2;
                                Serializable serializable3 = extras.getSerializable("extra_creq_executor_factory");
                                if (serializable3 != null) {
                                    f.b bVar = (f.b) serializable3;
                                    Serializable serializable4 = extras.getSerializable("extra_error_executor_factory");
                                    if (serializable4 != null) {
                                        k.a aVar4 = (k.a) serializable4;
                                        Parcelable parcelable = extras.getParcelable("extra_challenge_completion_intent");
                                        String str = null;
                                        if (!(parcelable instanceof Intent)) {
                                            parcelable = null;
                                        }
                                        i iVar = new i(challengeResponseData, aVar2, stripeUiCustomization, aVar3, bVar, aVar4, (Intent) parcelable, extras.getInt("extra_challenge_completion_request_code", 0));
                                        ChallengeActivity challengeActivity = this.f132a;
                                        f.b bVar2 = iVar.e;
                                        k.a aVar5 = iVar.f;
                                        a.a.a.a.g.a aVar6 = (a.a.a.a.g.a) challengeActivity.d.getValue();
                                        Intrinsics.checkParameterIsNotNull(challengeActivity, "activity");
                                        Intrinsics.checkParameterIsNotNull(iVar, "args");
                                        Intrinsics.checkParameterIsNotNull(bVar2, "creqExecutorFactory");
                                        Intrinsics.checkParameterIsNotNull(aVar5, "errorExecutorFactory");
                                        Intrinsics.checkParameterIsNotNull(aVar6, "viewModel");
                                        ChallengeResponseData d = iVar.d();
                                        StripeUiCustomization e = iVar.e();
                                        h hVar = new h(challengeActivity, iVar.e());
                                        a.a.a.a.e.a b = iVar.b();
                                        ChallengeResponseData.c uiType = iVar.d().getUiType();
                                        if (uiType != null) {
                                            str = uiType.a();
                                        }
                                        return new a.a.a.a.g.c(challengeActivity, aVar6, d, e, hVar, new d.a(challengeActivity, b, str != null ? str : "", iVar.e(), bVar2, iVar.c(), aVar5, iVar.a()), z.a.b.b(iVar.b().a()), iVar.a(), (m) null, (a.a.a.a.g.b) null, (a.a.a.a.f.b) null, 1792);
                                    }
                                    throw new TypeCastException("null cannot be cast to non-null type com.stripe.android.stripe3ds2.transaction.ErrorRequestExecutor.Factory");
                                }
                                throw new TypeCastException("null cannot be cast to non-null type com.stripe.android.stripe3ds2.transaction.ChallengeRequestExecutor.Factory");
                            }
                            throw new TypeCastException("null cannot be cast to non-null type com.stripe.android.stripe3ds2.transaction.ChallengeRequestExecutor.Config");
                        }
                        throw new InvalidInputException(new RuntimeException("UiCustomization is required"));
                    }
                    throw new TypeCastException("null cannot be cast to non-null type com.stripe.android.stripe3ds2.transactions.ChallengeRequestData");
                }
                throw new InvalidInputException(new RuntimeException("ChallengeResponseData is required"));
            }
            throw new InvalidInputException(new RuntimeException("Intent extras required"));
        }
    }

    public static final class b extends Lambda implements Function0<a.a.a.a.a.b> {

        /* renamed from: a  reason: collision with root package name */
        public final /* synthetic */ ChallengeActivity f133a;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(ChallengeActivity challengeActivity) {
            super(0);
            this.f133a = challengeActivity;
        }

        public Object invoke() {
            String str;
            View inflate = this.f133a.getLayoutInflater().inflate(R.layout.challenge_activity, (ViewGroup) null, false);
            BrandZoneView brandZoneView = (BrandZoneView) inflate.findViewById(R.id.ca_brand_zone);
            if (brandZoneView != null) {
                ChallengeZoneView challengeZoneView = (ChallengeZoneView) inflate.findViewById(R.id.ca_challenge_zone);
                if (challengeZoneView != null) {
                    InformationZoneView informationZoneView = (InformationZoneView) inflate.findViewById(R.id.ca_information_zone);
                    if (informationZoneView != null) {
                        return new a.a.a.a.a.b((ScrollView) inflate, brandZoneView, challengeZoneView, informationZoneView);
                    }
                    str = "caInformationZone";
                } else {
                    str = "caChallengeZone";
                }
            } else {
                str = "caBrandZone";
            }
            throw new NullPointerException("Missing required view with ID: ".concat(str));
        }
    }

    public static final class c extends Lambda implements Function0<a.a.a.a.g.a> {

        /* renamed from: a  reason: collision with root package name */
        public final /* synthetic */ ChallengeActivity f134a;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(ChallengeActivity challengeActivity) {
            super(0);
            this.f134a = challengeActivity;
        }

        public Object invoke() {
            ChallengeActivity challengeActivity = this.f134a;
            return (a.a.a.a.g.a) new ViewModelProvider((ViewModelStoreOwner) challengeActivity, (ViewModelProvider.Factory) new ViewModelProvider.AndroidViewModelFactory(challengeActivity.getApplication())).get(a.a.a.a.g.a.class);
        }
    }

    public final void a() {
        Object systemService = getSystemService("input_method");
        IBinder iBinder = null;
        if (!(systemService instanceof InputMethodManager)) {
            systemService = null;
        }
        InputMethodManager inputMethodManager = (InputMethodManager) systemService;
        if (inputMethodManager != null && inputMethodManager.isAcceptingText()) {
            View currentFocus = getCurrentFocus();
            if (currentFocus != null) {
                iBinder = currentFocus.getWindowToken();
            }
            inputMethodManager.hideSoftInputFromWindow(iBinder, 0);
        }
    }

    public final a.a.a.a.g.c b() {
        return (a.a.a.a.g.c) this.b.getValue();
    }

    public final a.a.a.a.a.b c() {
        return (a.a.a.a.a.b) this.c.getValue();
    }

    public void clickCancelButton() {
        a.a.a.a.g.c b2 = b();
        if (b2 != null) {
            b2.m.a(c.a.f37a);
            return;
        }
        throw null;
    }

    public void clickSubmitButton() {
        b().b();
    }

    public void expandTextsBeforeScreenshot() {
        InformationZoneView informationZoneView = b().f92a;
        informationZoneView.i.setRotation(180.0f);
        informationZoneView.e.setRotation(180.0f);
        informationZoneView.g.setVisibility(0);
        informationZoneView.c.setVisibility(0);
    }

    public ChallengeType getChallengeType() {
        ChallengeResponseData.c uiType = b().j.getUiType();
        if (uiType != null) {
            return uiType.b;
        }
        return null;
    }

    public Object[] getCheckboxesOrdered() {
        j jVar = b().e;
        List<CheckBox> checkBoxes = jVar != null ? jVar.getCheckBoxes() : null;
        if (checkBoxes == null) {
            return null;
        }
        Object[] array = checkBoxes.toArray(new CheckBox[0]);
        if (array != null) {
            return (CheckBox[]) array;
        }
        throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
    }

    public BaseSdkChallenge getCurrentChallenge() {
        return this;
    }

    public Object getWebView() {
        l lVar = b().f;
        if (lVar != null) {
            return lVar.getWebView();
        }
        return null;
    }

    public void onBackPressed() {
        b().m.a(c.a.f37a);
    }

    /* JADX WARNING: Removed duplicated region for block: B:22:0x010f  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0177  */
    /* JADX WARNING: Removed duplicated region for block: B:27:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onCreate(android.os.Bundle r6) {
        /*
            r5 = this;
            super.onCreate(r6)
            android.view.Window r0 = r5.getWindow()
            r1 = 8192(0x2000, float:1.14794E-41)
            r0.setFlags(r1, r1)
            a.a.a.a.a.b r0 = r5.c()
            android.widget.ScrollView r0 = r0.f7a
            r5.setContentView((android.view.View) r0)
            r0 = 0
            if (r6 == 0) goto L_0x001f
            java.lang.String r1 = "refresh_ui"
            boolean r6 = r6.getBoolean(r1, r0)
            goto L_0x0020
        L_0x001f:
            r6 = 0
        L_0x0020:
            r5.f131a = r6
            a.a.a.a.g.c r6 = r5.b()
            a.a.a.a.d.y r1 = r6.n
            a.a.a.a.g.c$a r2 = new a.a.a.a.g.c$a
            com.stripe.android.stripe3ds2.views.ChallengeActivity r3 = r6.h
            android.content.Intent r4 = r6.o
            r2.<init>(r3, r4)
            r1.a(r2)
            a.a.a.a.g.m r1 = r6.p
            com.stripe.android.stripe3ds2.init.ui.StripeUiCustomization r2 = r6.k
            com.stripe.android.stripe3ds2.init.ui.ToolbarCustomization r2 = r2.getToolbarCustomization()
            com.stripe.android.stripe3ds2.init.ui.StripeUiCustomization r3 = r6.k
            com.stripe.android.stripe3ds2.init.ui.UiCustomization$ButtonType r4 = com.stripe.android.stripe3ds2.init.ui.UiCustomization.ButtonType.CANCEL
            com.stripe.android.stripe3ds2.init.ui.ButtonCustomization r3 = r3.getButtonCustomization((com.stripe.android.stripe3ds2.init.ui.UiCustomization.ButtonType) r4)
            com.stripe.android.stripe3ds2.views.ThreeDS2Button r1 = r1.a(r2, r3)
            if (r1 == 0) goto L_0x0052
            a.a.a.a.g.f r2 = new a.a.a.a.g.f
            r2.<init>(r6, r1)
            r1.setOnClickListener(r2)
        L_0x0052:
            r6.d()
            a.a.a.a.g.k r1 = r6.d
            if (r1 == 0) goto L_0x0085
            com.stripe.android.stripe3ds2.views.ChallengeZoneView r2 = r6.b
            r2.setChallengeEntryView(r1)
            com.stripe.android.stripe3ds2.views.ChallengeZoneView r1 = r6.b
            com.stripe.android.stripe3ds2.transactions.ChallengeResponseData r2 = r6.j
            java.lang.String r2 = r2.getSubmitAuthenticationLabel()
            com.stripe.android.stripe3ds2.init.ui.StripeUiCustomization r3 = r6.k
            com.stripe.android.stripe3ds2.init.ui.UiCustomization$ButtonType r4 = com.stripe.android.stripe3ds2.init.ui.UiCustomization.ButtonType.SUBMIT
        L_0x006a:
            com.stripe.android.stripe3ds2.init.ui.ButtonCustomization r3 = r3.getButtonCustomization((com.stripe.android.stripe3ds2.init.ui.UiCustomization.ButtonType) r4)
            r1.b((java.lang.String) r2, (com.stripe.android.stripe3ds2.init.ui.ButtonCustomization) r3)
            com.stripe.android.stripe3ds2.views.ChallengeZoneView r1 = r6.b
            com.stripe.android.stripe3ds2.transactions.ChallengeResponseData r2 = r6.j
            java.lang.String r2 = r2.getResendInformationLabel()
            com.stripe.android.stripe3ds2.init.ui.StripeUiCustomization r3 = r6.k
            com.stripe.android.stripe3ds2.init.ui.UiCustomization$ButtonType r4 = com.stripe.android.stripe3ds2.init.ui.UiCustomization.ButtonType.RESEND
            com.stripe.android.stripe3ds2.init.ui.ButtonCustomization r3 = r3.getButtonCustomization((com.stripe.android.stripe3ds2.init.ui.UiCustomization.ButtonType) r4)
            r1.a((java.lang.String) r2, (com.stripe.android.stripe3ds2.init.ui.ButtonCustomization) r3)
            goto L_0x00e3
        L_0x0085:
            a.a.a.a.g.j r1 = r6.e
            if (r1 == 0) goto L_0x009b
            com.stripe.android.stripe3ds2.views.ChallengeZoneView r2 = r6.b
            r2.setChallengeEntryView(r1)
            com.stripe.android.stripe3ds2.views.ChallengeZoneView r1 = r6.b
            com.stripe.android.stripe3ds2.transactions.ChallengeResponseData r2 = r6.j
            java.lang.String r2 = r2.getSubmitAuthenticationLabel()
            com.stripe.android.stripe3ds2.init.ui.StripeUiCustomization r3 = r6.k
            com.stripe.android.stripe3ds2.init.ui.UiCustomization$ButtonType r4 = com.stripe.android.stripe3ds2.init.ui.UiCustomization.ButtonType.NEXT
            goto L_0x006a
        L_0x009b:
            a.a.a.a.g.l r1 = r6.f
            if (r1 == 0) goto L_0x00c6
            com.stripe.android.stripe3ds2.views.ChallengeZoneView r2 = r6.b
            r2.setChallengeEntryView(r1)
            com.stripe.android.stripe3ds2.views.ChallengeZoneView r1 = r6.b
            r2 = 0
            r1.a((java.lang.String) r2, (com.stripe.android.stripe3ds2.init.ui.LabelCustomization) r2)
            com.stripe.android.stripe3ds2.views.ChallengeZoneView r1 = r6.b
            r1.b((java.lang.String) r2, (com.stripe.android.stripe3ds2.init.ui.LabelCustomization) r2)
            com.stripe.android.stripe3ds2.views.ChallengeZoneView r1 = r6.b
            r1.b((java.lang.String) r2, (com.stripe.android.stripe3ds2.init.ui.ButtonCustomization) r2)
            a.a.a.a.g.l r1 = r6.f
            a.a.a.a.g.g r2 = new a.a.a.a.g.g
            r2.<init>(r6)
            r1.setOnClickListener(r2)
            com.stripe.android.stripe3ds2.views.BrandZoneView r1 = r6.c
            r2 = 8
            r1.setVisibility(r2)
            goto L_0x00e3
        L_0x00c6:
            com.stripe.android.stripe3ds2.transactions.ChallengeResponseData r1 = r6.j
            com.stripe.android.stripe3ds2.transactions.ChallengeResponseData$c r1 = r1.getUiType()
            com.stripe.android.stripe3ds2.transactions.ChallengeResponseData$c r2 = com.stripe.android.stripe3ds2.transactions.ChallengeResponseData.c.OOB
            if (r1 != r2) goto L_0x00e3
            com.stripe.android.stripe3ds2.views.ChallengeZoneView r1 = r6.b
            com.stripe.android.stripe3ds2.transactions.ChallengeResponseData r2 = r6.j
            java.lang.String r2 = r2.getOobContinueLabel()
            com.stripe.android.stripe3ds2.init.ui.StripeUiCustomization r3 = r6.k
            com.stripe.android.stripe3ds2.init.ui.UiCustomization$ButtonType r4 = com.stripe.android.stripe3ds2.init.ui.UiCustomization.ButtonType.CONTINUE
            com.stripe.android.stripe3ds2.init.ui.ButtonCustomization r3 = r3.getButtonCustomization((com.stripe.android.stripe3ds2.init.ui.UiCustomization.ButtonType) r4)
            r1.b((java.lang.String) r2, (com.stripe.android.stripe3ds2.init.ui.ButtonCustomization) r3)
        L_0x00e3:
            com.stripe.android.stripe3ds2.views.ChallengeZoneView r1 = r6.b
            com.stripe.android.stripe3ds2.transactions.ChallengeResponseData r2 = r6.j
            java.lang.String r2 = r2.getChallengeInfoHeader()
            com.stripe.android.stripe3ds2.init.ui.StripeUiCustomization r3 = r6.k
            com.stripe.android.stripe3ds2.init.ui.LabelCustomization r3 = r3.getLabelCustomization()
            r1.a((java.lang.String) r2, (com.stripe.android.stripe3ds2.init.ui.LabelCustomization) r3)
            com.stripe.android.stripe3ds2.views.ChallengeZoneView r1 = r6.b
            com.stripe.android.stripe3ds2.transactions.ChallengeResponseData r2 = r6.j
            java.lang.String r2 = r2.getChallengeInfoText()
            com.stripe.android.stripe3ds2.init.ui.StripeUiCustomization r3 = r6.k
            com.stripe.android.stripe3ds2.init.ui.LabelCustomization r3 = r3.getLabelCustomization()
            r1.b((java.lang.String) r2, (com.stripe.android.stripe3ds2.init.ui.LabelCustomization) r3)
            com.stripe.android.stripe3ds2.views.ChallengeZoneView r1 = r6.b
            com.stripe.android.stripe3ds2.transactions.ChallengeResponseData r2 = r6.j
            boolean r2 = r2.getShouldShowChallengeInfoTextIndicator()
            if (r2 == 0) goto L_0x0111
            int r0 = com.stripe.android.stripe3ds2.R.drawable.ic_indicator
        L_0x0111:
            r1.setInfoTextIndicator(r0)
            com.stripe.android.stripe3ds2.views.ChallengeZoneView r0 = r6.b
            com.stripe.android.stripe3ds2.transactions.ChallengeResponseData r1 = r6.j
            java.lang.String r1 = r1.getWhitelistingInfoText()
            com.stripe.android.stripe3ds2.init.ui.StripeUiCustomization r2 = r6.k
            com.stripe.android.stripe3ds2.init.ui.LabelCustomization r2 = r2.getLabelCustomization()
            com.stripe.android.stripe3ds2.init.ui.StripeUiCustomization r3 = r6.k
            com.stripe.android.stripe3ds2.init.ui.UiCustomization$ButtonType r4 = com.stripe.android.stripe3ds2.init.ui.UiCustomization.ButtonType.SELECT
            com.stripe.android.stripe3ds2.init.ui.ButtonCustomization r3 = r3.getButtonCustomization((com.stripe.android.stripe3ds2.init.ui.UiCustomization.ButtonType) r4)
            r0.a(r1, r2, r3)
            com.stripe.android.stripe3ds2.views.ChallengeZoneView r0 = r6.b
            a.a.a.a.g.d r1 = new a.a.a.a.g.d
            r1.<init>(r6)
            r0.setSubmitButtonClickListener(r1)
            com.stripe.android.stripe3ds2.views.ChallengeZoneView r0 = r6.b
            a.a.a.a.g.e r1 = new a.a.a.a.g.e
            r1.<init>(r6)
            r0.setResendButtonClickListener(r1)
            com.stripe.android.stripe3ds2.views.InformationZoneView r0 = r6.f92a
            com.stripe.android.stripe3ds2.transactions.ChallengeResponseData r1 = r6.j
            java.lang.String r1 = r1.getWhyInfoLabel()
            com.stripe.android.stripe3ds2.transactions.ChallengeResponseData r2 = r6.j
            java.lang.String r2 = r2.getWhyInfoText()
            com.stripe.android.stripe3ds2.init.ui.StripeUiCustomization r3 = r6.k
            com.stripe.android.stripe3ds2.init.ui.LabelCustomization r3 = r3.getLabelCustomization()
            r0.b(r1, r2, r3)
            com.stripe.android.stripe3ds2.views.InformationZoneView r0 = r6.f92a
            com.stripe.android.stripe3ds2.transactions.ChallengeResponseData r1 = r6.j
            java.lang.String r1 = r1.getExpandInfoLabel()
            com.stripe.android.stripe3ds2.transactions.ChallengeResponseData r2 = r6.j
            java.lang.String r2 = r2.getExpandInfoText()
            com.stripe.android.stripe3ds2.init.ui.StripeUiCustomization r3 = r6.k
            com.stripe.android.stripe3ds2.init.ui.LabelCustomization r3 = r3.getLabelCustomization()
            r0.a(r1, r2, r3)
            com.stripe.android.stripe3ds2.init.ui.StripeUiCustomization r0 = r6.k
            java.lang.String r0 = r0.getAccentColor()
            if (r0 == 0) goto L_0x0180
            com.stripe.android.stripe3ds2.views.InformationZoneView r6 = r6.f92a
            int r0 = android.graphics.Color.parseColor(r0)
            r6.setToggleColor$3ds2sdk_release(r0)
        L_0x0180:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.stripe.android.stripe3ds2.views.ChallengeActivity.onCreate(android.os.Bundle):void");
    }

    public void onDestroy() {
        a.a.a.a.g.c b2 = b();
        ProgressDialog progressDialog = b2.g;
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
        b2.g = null;
        b2.n.a((y.b) null);
        super.onDestroy();
    }

    public void onLowMemory() {
        b().q.a();
        super.onLowMemory();
    }

    public void onPause() {
        this.f131a = true;
        a();
        super.onPause();
    }

    public void onResume() {
        super.onResume();
        if (this.f131a) {
            b().c();
        } else {
            LocalBroadcastManager.getInstance(this).sendBroadcast(new Intent().setAction(SdkChallengeInterface.UL_HANDLE_CHALLENGE_ACTION));
        }
    }

    public void onSaveInstanceState(Bundle bundle) {
        Intrinsics.checkParameterIsNotNull(bundle, "outState");
        super.onSaveInstanceState(bundle);
        bundle.putBoolean("refresh_ui", true);
    }

    public void onTrimMemory(int i) {
        b().q.a();
        super.onTrimMemory(i);
    }

    public void selectObject(int i) {
        j jVar = b().e;
        if (jVar != null) {
            jVar.a(i);
        }
    }

    public void typeTextChallengeValue(String str) {
        Intrinsics.checkParameterIsNotNull(str, "s");
        a.a.a.a.g.c b2 = b();
        if (b2 != null) {
            Intrinsics.checkParameterIsNotNull(str, "text");
            a.a.a.a.g.k kVar = b2.d;
            if (kVar != null) {
                kVar.setTextEntry$3ds2sdk_release(str);
                return;
            }
            return;
        }
        throw null;
    }
}
