package com.stripe.android.stripe3ds2.transaction;

import android.content.Intent;
import android.os.Bundle;
import com.onesignal.OneSignalDbContract;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\u0018\u0000 \u00112\u00020\u0001:\u0001\u0011B\u0019\b\u0000\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0002\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J'\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\b2\u0006\u0010\n\u001a\u00020\u000b2\b\b\u0002\u0010\f\u001a\u00020\rH\u0000¢\u0006\u0002\b\u000eJ\u0016\u0010\u000f\u001a\u00020\u00102\u0006\u0010\t\u001a\u00020\b2\u0006\u0010\n\u001a\u00020\u000bR\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0012"}, d2 = {"Lcom/stripe/android/stripe3ds2/transaction/ChallengeCompletionIntentStarter;", "", "host", "Lcom/stripe/android/stripe3ds2/transaction/Stripe3ds2ActivityStarterHost;", "requestCode", "", "(Lcom/stripe/android/stripe3ds2/transaction/Stripe3ds2ActivityStarterHost;I)V", "createIntent", "Landroid/content/Intent;", "intent", "outcome", "Lcom/stripe/android/stripe3ds2/transaction/ChallengeFlowOutcome;", "isRequestingResult", "", "createIntent$3ds2sdk_release", "start", "", "Companion", "3ds2sdk_release"}, k = 1, mv = {1, 1, 16})
public final class ChallengeCompletionIntentStarter {
    public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
    public static final String EXTRA_OUTCOME = "extra_outcome";

    /* renamed from: a  reason: collision with root package name */
    public final Stripe3ds2ActivityStarterHost f122a;
    public final int b;

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\u0005"}, d2 = {"Lcom/stripe/android/stripe3ds2/transaction/ChallengeCompletionIntentStarter$Companion;", "", "()V", "EXTRA_OUTCOME", "", "3ds2sdk_release"}, k = 1, mv = {1, 1, 16})
    public static final class Companion {
        public Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public ChallengeCompletionIntentStarter(Stripe3ds2ActivityStarterHost stripe3ds2ActivityStarterHost, int i) {
        Intrinsics.checkParameterIsNotNull(stripe3ds2ActivityStarterHost, "host");
        this.f122a = stripe3ds2ActivityStarterHost;
        this.b = i;
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ ChallengeCompletionIntentStarter(Stripe3ds2ActivityStarterHost stripe3ds2ActivityStarterHost, int i, int i2, DefaultConstructorMarker defaultConstructorMarker) {
        this(stripe3ds2ActivityStarterHost, (i2 & 2) != 0 ? 0 : i);
    }

    public static /* synthetic */ Intent createIntent$3ds2sdk_release$default(ChallengeCompletionIntentStarter challengeCompletionIntentStarter, Intent intent, ChallengeFlowOutcome challengeFlowOutcome, boolean z, int i, Object obj) {
        if ((i & 4) != 0) {
            z = false;
        }
        return challengeCompletionIntentStarter.createIntent$3ds2sdk_release(intent, challengeFlowOutcome, z);
    }

    public final Intent createIntent$3ds2sdk_release(Intent intent, ChallengeFlowOutcome challengeFlowOutcome, boolean z) {
        Intrinsics.checkParameterIsNotNull(intent, "intent");
        Intrinsics.checkParameterIsNotNull(challengeFlowOutcome, OneSignalDbContract.OutcomeEventsTable.TABLE_NAME);
        Intent component = new Intent().setComponent(intent.getComponent());
        Bundle extras = intent.getExtras();
        if (extras == null) {
            extras = new Bundle();
        }
        Intent putExtra = component.putExtras(extras).putExtra(EXTRA_OUTCOME, challengeFlowOutcome.ordinal());
        if (!z) {
            putExtra.addFlags(33554432);
        }
        Intrinsics.checkExpressionValueIsNotNull(putExtra, "Intent()\n            .se…          }\n            }");
        return putExtra;
    }

    public final void start(Intent intent, ChallengeFlowOutcome challengeFlowOutcome) {
        Intrinsics.checkParameterIsNotNull(intent, "intent");
        Intrinsics.checkParameterIsNotNull(challengeFlowOutcome, OneSignalDbContract.OutcomeEventsTable.TABLE_NAME);
        if (this.b > 0) {
            this.f122a.startActivityForResult$3ds2sdk_release(createIntent$3ds2sdk_release(intent, challengeFlowOutcome, true), this.b);
        } else {
            this.f122a.startActivity$3ds2sdk_release(createIntent$3ds2sdk_release$default(this, intent, challengeFlowOutcome, false, 4, (Object) null));
        }
    }
}
