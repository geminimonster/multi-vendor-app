package com.stripe.android.stripe3ds2.init.ui;

import android.os.Parcel;
import android.os.Parcelable;
import com.stripe.android.stripe3ds2.exceptions.InvalidInputException;
import com.stripe.android.stripe3ds2.utils.CustomizeUtils;
import java.util.Arrays;
import java.util.Objects;
import kotlin.jvm.internal.Intrinsics;

public final class StripeButtonCustomization extends BaseCustomization implements ButtonCustomization, Parcelable {
    public static final Parcelable.Creator<StripeButtonCustomization> CREATOR = new a();
    public String d;
    public int e;

    public static class a implements Parcelable.Creator<StripeButtonCustomization> {
        public Object createFromParcel(Parcel parcel) {
            return new StripeButtonCustomization(parcel);
        }

        public Object[] newArray(int i) {
            return new StripeButtonCustomization[i];
        }
    }

    public StripeButtonCustomization() {
    }

    public StripeButtonCustomization(Parcel parcel) {
        super(parcel);
        this.d = parcel.readString();
        this.e = parcel.readInt();
    }

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (!(obj instanceof StripeButtonCustomization)) {
                return false;
            }
            StripeButtonCustomization stripeButtonCustomization = (StripeButtonCustomization) obj;
            if (Intrinsics.areEqual((Object) this.d, (Object) stripeButtonCustomization.d) && this.e == stripeButtonCustomization.e) {
                return true;
            }
            return false;
        }
        return true;
    }

    public String getBackgroundColor() {
        return this.d;
    }

    public int getCornerRadius() {
        return this.e;
    }

    public int hashCode() {
        Object[] objArr = {this.d, Integer.valueOf(this.e)};
        Intrinsics.checkParameterIsNotNull(objArr, "values");
        return Objects.hash(Arrays.copyOf(objArr, 2));
    }

    public void setBackgroundColor(String str) throws InvalidInputException {
        this.d = CustomizeUtils.requireValidColor(str);
    }

    public void setCornerRadius(int i) throws InvalidInputException {
        this.e = CustomizeUtils.requireValidDimension(i);
    }

    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        parcel.writeString(this.d);
        parcel.writeInt(this.e);
    }
}
