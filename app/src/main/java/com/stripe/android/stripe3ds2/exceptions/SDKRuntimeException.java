package com.stripe.android.stripe3ds2.exceptions;

import androidx.core.app.NotificationCompat;
import kotlin.Metadata;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u0000 \u00052\u00060\u0001j\u0002`\u0002:\u0001\u0005B\u0011\u0012\n\u0010\u0003\u001a\u00060\u0001j\u0002`\u0002¢\u0006\u0002\u0010\u0004¨\u0006\u0006"}, d2 = {"Lcom/stripe/android/stripe3ds2/exceptions/SDKRuntimeException;", "Ljava/lang/RuntimeException;", "Lkotlin/RuntimeException;", "ex", "(Ljava/lang/RuntimeException;)V", "Companion", "3ds2sdk_release"}, k = 1, mv = {1, 1, 16})
public final class SDKRuntimeException extends RuntimeException {
    public static final Companion Companion = new Companion((DefaultConstructorMarker) null);

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0003\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0007J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0007\u001a\u00020\bH\u0007¨\u0006\t"}, d2 = {"Lcom/stripe/android/stripe3ds2/exceptions/SDKRuntimeException$Companion;", "", "()V", "create", "Lcom/stripe/android/stripe3ds2/exceptions/SDKRuntimeException;", "msg", "", "throwable", "", "3ds2sdk_release"}, k = 1, mv = {1, 1, 16})
    public static final class Companion {
        public Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        @JvmStatic
        public final SDKRuntimeException create(String str) {
            Intrinsics.checkParameterIsNotNull(str, NotificationCompat.CATEGORY_MESSAGE);
            return new SDKRuntimeException(new RuntimeException(str));
        }

        @JvmStatic
        public final SDKRuntimeException create(Throwable th) {
            Intrinsics.checkParameterIsNotNull(th, "throwable");
            return new SDKRuntimeException(new RuntimeException(th));
        }
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SDKRuntimeException(RuntimeException runtimeException) {
        super(runtimeException.getMessage(), runtimeException.getCause());
        Intrinsics.checkParameterIsNotNull(runtimeException, "ex");
    }

    @JvmStatic
    public static final SDKRuntimeException create(String str) {
        return Companion.create(str);
    }

    @JvmStatic
    public static final SDKRuntimeException create(Throwable th) {
        return Companion.create(th);
    }
}
