package com.stripe.android.stripe3ds2.transaction;

import android.app.Activity;
import android.content.Intent;
import androidx.fragment.app.Fragment;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0002\b\u0007\u0018\u00002\u00020\u0001B\u000f\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004B\u000f\b\u0016\u0012\u0006\u0010\u0005\u001a\u00020\u0006¢\u0006\u0002\u0010\u0007B\u0019\b\u0002\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003¢\u0006\u0002\u0010\bJ\u0015\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000eH\u0000¢\u0006\u0002\b\u000fJ\u001d\u0010\u0010\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\u0011\u001a\u00020\u0012H\u0000¢\u0006\u0002\b\u0013R\u0014\u0010\u0005\u001a\u00020\u0006X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0010\u0010\u0002\u001a\u0004\u0018\u00010\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0014"}, d2 = {"Lcom/stripe/android/stripe3ds2/transaction/Stripe3ds2ActivityStarterHost;", "", "fragment", "Landroidx/fragment/app/Fragment;", "(Landroidx/fragment/app/Fragment;)V", "activity", "Landroid/app/Activity;", "(Landroid/app/Activity;)V", "(Landroid/app/Activity;Landroidx/fragment/app/Fragment;)V", "getActivity$3ds2sdk_release", "()Landroid/app/Activity;", "startActivity", "", "intent", "Landroid/content/Intent;", "startActivity$3ds2sdk_release", "startActivityForResult", "requestCode", "", "startActivityForResult$3ds2sdk_release", "3ds2sdk_release"}, k = 1, mv = {1, 1, 16})
public final class Stripe3ds2ActivityStarterHost {
    public final Activity activity;
    public final Fragment fragment;

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public Stripe3ds2ActivityStarterHost(Activity activity2) {
        this(activity2, (Fragment) null);
        Intrinsics.checkParameterIsNotNull(activity2, "activity");
    }

    public Stripe3ds2ActivityStarterHost(Activity activity2, Fragment fragment2) {
        this.activity = activity2;
        this.fragment = fragment2;
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public Stripe3ds2ActivityStarterHost(androidx.fragment.app.Fragment r3) {
        /*
            r2 = this;
            java.lang.String r0 = "fragment"
            kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(r3, r0)
            androidx.fragment.app.FragmentActivity r0 = r3.requireActivity()
            java.lang.String r1 = "fragment.requireActivity()"
            kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r0, r1)
            r2.<init>(r0, r3)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.stripe.android.stripe3ds2.transaction.Stripe3ds2ActivityStarterHost.<init>(androidx.fragment.app.Fragment):void");
    }

    public final Activity getActivity$3ds2sdk_release() {
        return this.activity;
    }

    public final void startActivity$3ds2sdk_release(Intent intent) {
        Intrinsics.checkParameterIsNotNull(intent, "intent");
        Fragment fragment2 = this.fragment;
        if (fragment2 == null) {
            this.activity.startActivity(intent);
        } else if (fragment2.isAdded()) {
            this.fragment.startActivity(intent);
        }
    }

    public final void startActivityForResult$3ds2sdk_release(Intent intent, int i) {
        Intrinsics.checkParameterIsNotNull(intent, "intent");
        Fragment fragment2 = this.fragment;
        if (fragment2 == null) {
            this.activity.startActivityForResult(intent, i);
        } else if (fragment2.isAdded()) {
            this.fragment.startActivityForResult(intent, i);
        }
    }
}
