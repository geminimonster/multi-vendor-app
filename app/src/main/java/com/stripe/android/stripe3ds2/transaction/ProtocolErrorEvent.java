package com.stripe.android.stripe3ds2.transaction;

import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\b\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\t\u0010\u000b\u001a\u00020\u0003HÆ\u0003J\t\u0010\f\u001a\u00020\u0005HÆ\u0003J\u001d\u0010\r\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u0005HÆ\u0001J\u0013\u0010\u000e\u001a\u00020\u000f2\b\u0010\u0010\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0011\u001a\u00020\u0012HÖ\u0001J\t\u0010\u0013\u001a\u00020\u0003HÖ\u0001R\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n¨\u0006\u0014"}, d2 = {"Lcom/stripe/android/stripe3ds2/transaction/ProtocolErrorEvent;", "", "sdkTransactionId", "", "errorMessage", "Lcom/stripe/android/stripe3ds2/transaction/ErrorMessage;", "(Ljava/lang/String;Lcom/stripe/android/stripe3ds2/transaction/ErrorMessage;)V", "getErrorMessage", "()Lcom/stripe/android/stripe3ds2/transaction/ErrorMessage;", "getSdkTransactionId", "()Ljava/lang/String;", "component1", "component2", "copy", "equals", "", "other", "hashCode", "", "toString", "3ds2sdk_release"}, k = 1, mv = {1, 1, 16})
public final class ProtocolErrorEvent {
    public final ErrorMessage errorMessage;
    public final String sdkTransactionId;

    public ProtocolErrorEvent(String str, ErrorMessage errorMessage2) {
        Intrinsics.checkParameterIsNotNull(str, "sdkTransactionId");
        Intrinsics.checkParameterIsNotNull(errorMessage2, "errorMessage");
        this.sdkTransactionId = str;
        this.errorMessage = errorMessage2;
    }

    public static /* synthetic */ ProtocolErrorEvent copy$default(ProtocolErrorEvent protocolErrorEvent, String str, ErrorMessage errorMessage2, int i, Object obj) {
        if ((i & 1) != 0) {
            str = protocolErrorEvent.sdkTransactionId;
        }
        if ((i & 2) != 0) {
            errorMessage2 = protocolErrorEvent.errorMessage;
        }
        return protocolErrorEvent.copy(str, errorMessage2);
    }

    public final String component1() {
        return this.sdkTransactionId;
    }

    public final ErrorMessage component2() {
        return this.errorMessage;
    }

    public final ProtocolErrorEvent copy(String str, ErrorMessage errorMessage2) {
        Intrinsics.checkParameterIsNotNull(str, "sdkTransactionId");
        Intrinsics.checkParameterIsNotNull(errorMessage2, "errorMessage");
        return new ProtocolErrorEvent(str, errorMessage2);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ProtocolErrorEvent)) {
            return false;
        }
        ProtocolErrorEvent protocolErrorEvent = (ProtocolErrorEvent) obj;
        return Intrinsics.areEqual((Object) this.sdkTransactionId, (Object) protocolErrorEvent.sdkTransactionId) && Intrinsics.areEqual((Object) this.errorMessage, (Object) protocolErrorEvent.errorMessage);
    }

    public final ErrorMessage getErrorMessage() {
        return this.errorMessage;
    }

    public final String getSdkTransactionId() {
        return this.sdkTransactionId;
    }

    public int hashCode() {
        String str = this.sdkTransactionId;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        ErrorMessage errorMessage2 = this.errorMessage;
        if (errorMessage2 != null) {
            i = errorMessage2.hashCode();
        }
        return hashCode + i;
    }

    public String toString() {
        return "ProtocolErrorEvent(sdkTransactionId=" + this.sdkTransactionId + ", errorMessage=" + this.errorMessage + ")";
    }
}
