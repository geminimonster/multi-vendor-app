package com.stripe.android.stripe3ds2.init.ui;

import android.os.Parcel;
import android.os.Parcelable;
import com.stripe.android.stripe3ds2.exceptions.InvalidInputException;
import com.stripe.android.stripe3ds2.utils.CustomizeUtils;
import java.util.Arrays;
import java.util.Objects;
import kotlin.jvm.internal.Intrinsics;

public final class StripeTextBoxCustomization extends BaseCustomization implements TextBoxCustomization, Parcelable {
    public static final Parcelable.Creator<StripeTextBoxCustomization> CREATOR = new a();
    public int d;
    public String e;
    public int f;
    public String g;

    public static class a implements Parcelable.Creator<StripeTextBoxCustomization> {
        public Object createFromParcel(Parcel parcel) {
            return new StripeTextBoxCustomization(parcel);
        }

        public Object[] newArray(int i) {
            return new StripeTextBoxCustomization[i];
        }
    }

    public StripeTextBoxCustomization() {
    }

    public StripeTextBoxCustomization(Parcel parcel) {
        super(parcel);
        this.d = parcel.readInt();
        this.e = parcel.readString();
        this.f = parcel.readInt();
        this.g = parcel.readString();
    }

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (!(obj instanceof StripeTextBoxCustomization)) {
                return false;
            }
            StripeTextBoxCustomization stripeTextBoxCustomization = (StripeTextBoxCustomization) obj;
            if (this.d == stripeTextBoxCustomization.d && Intrinsics.areEqual((Object) this.e, (Object) stripeTextBoxCustomization.e) && this.f == stripeTextBoxCustomization.f && Intrinsics.areEqual((Object) this.g, (Object) stripeTextBoxCustomization.g)) {
                return true;
            }
            return false;
        }
        return true;
    }

    public String getBorderColor() {
        return this.e;
    }

    public int getBorderWidth() {
        return this.d;
    }

    public int getCornerRadius() {
        return this.f;
    }

    public String getHintTextColor() {
        return this.g;
    }

    public int hashCode() {
        Object[] objArr = {Integer.valueOf(this.d), this.e, Integer.valueOf(this.f), this.g};
        Intrinsics.checkParameterIsNotNull(objArr, "values");
        return Objects.hash(Arrays.copyOf(objArr, 4));
    }

    public void setBorderColor(String str) throws InvalidInputException {
        this.e = CustomizeUtils.requireValidColor(str);
    }

    public void setBorderWidth(int i) throws InvalidInputException {
        this.d = CustomizeUtils.requireValidDimension(i);
    }

    public void setCornerRadius(int i) throws InvalidInputException {
        this.f = CustomizeUtils.requireValidDimension(i);
    }

    public void setHintTextColor(String str) throws InvalidInputException {
        this.g = CustomizeUtils.requireValidColor(str);
    }

    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        parcel.writeInt(this.d);
        parcel.writeString(this.e);
        parcel.writeInt(this.f);
        parcel.writeString(this.g);
    }
}
