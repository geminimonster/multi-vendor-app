package com.stripe.android.stripe3ds2.views;

import a.a.a.a.a.i;
import a.a.a.a.g.p;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.res.ColorStateList;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.appcompat.widget.AppCompatImageView;
import com.stripe.android.stripe3ds2.init.ui.LabelCustomization;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import kotlin.text.StringsKt;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000d\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\f\n\u0002\u0018\u0002\n\u0002\b\r\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0000\u0018\u00002\u00020\u0001B%\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\b\b\u0002\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ\u0006\u00101\u001a\u000202J&\u00103\u001a\u0002022\b\u00104\u001a\u0004\u0018\u0001052\b\u00106\u001a\u0004\u0018\u0001052\n\b\u0002\u00107\u001a\u0004\u0018\u000108J&\u00109\u001a\u0002022\b\u0010:\u001a\u0004\u0018\u0001052\b\u0010;\u001a\u0004\u0018\u0001052\n\b\u0002\u00107\u001a\u0004\u0018\u000108J \u0010<\u001a\u0002022\u0006\u0010=\u001a\u00020>2\u0006\u0010?\u001a\u00020@2\u0006\u0010A\u001a\u00020>H\u0002R\u000e\u0010\t\u001a\u00020\u0007X\u0004¢\u0006\u0002\n\u0000R\u0012\u0010\n\u001a\u00020\u00078\u0002@\u0002X\u000e¢\u0006\u0002\n\u0000R\u001c\u0010\u000b\u001a\u00020\f8\u0000X\u0004¢\u0006\u000e\n\u0000\u0012\u0004\b\r\u0010\u000e\u001a\u0004\b\u000f\u0010\u0010R\u001c\u0010\u0011\u001a\u00020\u00128\u0000X\u0004¢\u0006\u000e\n\u0000\u0012\u0004\b\u0013\u0010\u000e\u001a\u0004\b\u0014\u0010\u0015R\u001c\u0010\u0016\u001a\u00020\u00178\u0000X\u0004¢\u0006\u000e\n\u0000\u0012\u0004\b\u0018\u0010\u000e\u001a\u0004\b\u0019\u0010\u001aR\u001c\u0010\u001b\u001a\u00020\u00178\u0000X\u0004¢\u0006\u000e\n\u0000\u0012\u0004\b\u001c\u0010\u000e\u001a\u0004\b\u001d\u0010\u001aR\u001e\u0010\u001e\u001a\u00020\u00078\u0000@\u0000X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u001f\u0010 \"\u0004\b!\u0010\"R\u000e\u0010#\u001a\u00020$X\u0004¢\u0006\u0002\n\u0000R\u001c\u0010%\u001a\u00020\f8\u0000X\u0004¢\u0006\u000e\n\u0000\u0012\u0004\b&\u0010\u000e\u001a\u0004\b'\u0010\u0010R\u001c\u0010(\u001a\u00020\u00128\u0000X\u0004¢\u0006\u000e\n\u0000\u0012\u0004\b)\u0010\u000e\u001a\u0004\b*\u0010\u0015R\u001c\u0010+\u001a\u00020\u00178\u0000X\u0004¢\u0006\u000e\n\u0000\u0012\u0004\b,\u0010\u000e\u001a\u0004\b-\u0010\u001aR\u001c\u0010.\u001a\u00020\u00178\u0000X\u0004¢\u0006\u000e\n\u0000\u0012\u0004\b/\u0010\u000e\u001a\u0004\b0\u0010\u001a¨\u0006B"}, d2 = {"Lcom/stripe/android/stripe3ds2/views/InformationZoneView;", "Landroid/widget/FrameLayout;", "context", "Landroid/content/Context;", "attrs", "Landroid/util/AttributeSet;", "defStyleAttr", "", "(Landroid/content/Context;Landroid/util/AttributeSet;I)V", "animationDuration", "defaultColor", "expandArrow", "Landroidx/appcompat/widget/AppCompatImageView;", "expandArrow$annotations", "()V", "getExpandArrow$3ds2sdk_release", "()Landroidx/appcompat/widget/AppCompatImageView;", "expandContainer", "Landroid/widget/LinearLayout;", "expandContainer$annotations", "getExpandContainer$3ds2sdk_release", "()Landroid/widget/LinearLayout;", "expandLabel", "Lcom/stripe/android/stripe3ds2/views/ThreeDS2TextView;", "expandLabel$annotations", "getExpandLabel$3ds2sdk_release", "()Lcom/stripe/android/stripe3ds2/views/ThreeDS2TextView;", "expandText", "expandText$annotations", "getExpandText$3ds2sdk_release", "toggleColor", "getToggleColor$3ds2sdk_release", "()I", "setToggleColor$3ds2sdk_release", "(I)V", "viewBinding", "Lcom/stripe/android/stripe3ds2/databinding/InformationZoneViewBinding;", "whyArrow", "whyArrow$annotations", "getWhyArrow$3ds2sdk_release", "whyContainer", "whyContainer$annotations", "getWhyContainer$3ds2sdk_release", "whyLabel", "whyLabel$annotations", "getWhyLabel$3ds2sdk_release", "whyText", "whyText$annotations", "getWhyText$3ds2sdk_release", "expandViews", "", "setExpandInfo", "expandInfoLabel", "", "expandInfoText", "labelCustomization", "Lcom/stripe/android/stripe3ds2/init/ui/LabelCustomization;", "setWhyInfo", "whyInfoLabel", "whyInfoText", "toggleView", "arrow", "Landroid/view/View;", "label", "Landroid/widget/TextView;", "detailsView", "3ds2sdk_release"}, k = 1, mv = {1, 1, 16})
public final class InformationZoneView extends FrameLayout {

    /* renamed from: a  reason: collision with root package name */
    public final i f143a;
    public final ThreeDS2TextView b;
    public final ThreeDS2TextView c;
    public final LinearLayout d;
    public final AppCompatImageView e;
    public final ThreeDS2TextView f;
    public final ThreeDS2TextView g;
    public final LinearLayout h;
    public final AppCompatImageView i;
    public int j;
    public int k;
    public final int l;

    public static final class a implements View.OnClickListener {

        /* renamed from: a  reason: collision with root package name */
        public final /* synthetic */ InformationZoneView f144a;

        public a(InformationZoneView informationZoneView) {
            this.f144a = informationZoneView;
        }

        public final void onClick(View view) {
            InformationZoneView informationZoneView = this.f144a;
            InformationZoneView.a(informationZoneView, informationZoneView.getWhyArrow$3ds2sdk_release(), this.f144a.getWhyLabel$3ds2sdk_release(), this.f144a.getWhyText$3ds2sdk_release());
        }
    }

    public static final class b implements View.OnClickListener {

        /* renamed from: a  reason: collision with root package name */
        public final /* synthetic */ InformationZoneView f145a;

        public b(InformationZoneView informationZoneView) {
            this.f145a = informationZoneView;
        }

        public final void onClick(View view) {
            InformationZoneView informationZoneView = this.f145a;
            InformationZoneView.a(informationZoneView, informationZoneView.getExpandArrow$3ds2sdk_release(), this.f145a.getExpandLabel$3ds2sdk_release(), this.f145a.getExpandText$3ds2sdk_release());
        }
    }

    public InformationZoneView(Context context) {
        this(context, (AttributeSet) null, 0, 6, (DefaultConstructorMarker) null);
    }

    public InformationZoneView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0, 4, (DefaultConstructorMarker) null);
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public InformationZoneView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        Intrinsics.checkParameterIsNotNull(context, "context");
        i a2 = i.a(LayoutInflater.from(context), this, true);
        Intrinsics.checkExpressionValueIsNotNull(a2, "InformationZoneViewBindi… this,\n        true\n    )");
        this.f143a = a2;
        ThreeDS2TextView threeDS2TextView = a2.h;
        Intrinsics.checkExpressionValueIsNotNull(threeDS2TextView, "viewBinding.whyLabel");
        this.b = threeDS2TextView;
        ThreeDS2TextView threeDS2TextView2 = this.f143a.i;
        Intrinsics.checkExpressionValueIsNotNull(threeDS2TextView2, "viewBinding.whyText");
        this.c = threeDS2TextView2;
        LinearLayout linearLayout = this.f143a.g;
        Intrinsics.checkExpressionValueIsNotNull(linearLayout, "viewBinding.whyContainer");
        this.d = linearLayout;
        AppCompatImageView appCompatImageView = this.f143a.f;
        Intrinsics.checkExpressionValueIsNotNull(appCompatImageView, "viewBinding.whyArrow");
        this.e = appCompatImageView;
        ThreeDS2TextView threeDS2TextView3 = this.f143a.d;
        Intrinsics.checkExpressionValueIsNotNull(threeDS2TextView3, "viewBinding.expandLabel");
        this.f = threeDS2TextView3;
        ThreeDS2TextView threeDS2TextView4 = this.f143a.e;
        Intrinsics.checkExpressionValueIsNotNull(threeDS2TextView4, "viewBinding.expandText");
        this.g = threeDS2TextView4;
        LinearLayout linearLayout2 = this.f143a.c;
        Intrinsics.checkExpressionValueIsNotNull(linearLayout2, "viewBinding.expandContainer");
        this.h = linearLayout2;
        AppCompatImageView appCompatImageView2 = this.f143a.b;
        Intrinsics.checkExpressionValueIsNotNull(appCompatImageView2, "viewBinding.expandArrow");
        this.i = appCompatImageView2;
        this.l = getResources().getInteger(17694720);
        this.d.setOnClickListener(new a(this));
        this.h.setOnClickListener(new b(this));
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ InformationZoneView(Context context, AttributeSet attributeSet, int i2, int i3, DefaultConstructorMarker defaultConstructorMarker) {
        this(context, (i3 & 2) != 0 ? null : attributeSet, (i3 & 4) != 0 ? 0 : i2);
    }

    public static final /* synthetic */ void a(InformationZoneView informationZoneView, View view, TextView textView, View view2) {
        if (informationZoneView != null) {
            int i2 = 8;
            boolean z = view2.getVisibility() == 8;
            ObjectAnimator ofFloat = ObjectAnimator.ofFloat(view, "rotation", new float[]{(float) (z ? 180 : 0)});
            Intrinsics.checkExpressionValueIsNotNull(ofFloat, "arrowAnimator");
            ofFloat.setDuration((long) informationZoneView.l);
            ofFloat.start();
            textView.setEnabled(z);
            view.setEnabled(z);
            if (informationZoneView.j != 0) {
                if (informationZoneView.k == 0) {
                    ColorStateList textColors = textView.getTextColors();
                    Intrinsics.checkExpressionValueIsNotNull(textColors, "label.textColors");
                    informationZoneView.k = textColors.getDefaultColor();
                }
                textView.setTextColor(z ? informationZoneView.j : informationZoneView.k);
            }
            if (z) {
                i2 = 0;
            }
            view2.setVisibility(i2);
            if (z) {
                view2.postDelayed(new p(view2), (long) informationZoneView.l);
                return;
            }
            return;
        }
        throw null;
    }

    public final void a(String str, String str2, LabelCustomization labelCustomization) {
        if (!(str == null || StringsKt.isBlank(str))) {
            this.f.a(str, labelCustomization);
            this.h.setVisibility(0);
            this.g.a(str2, labelCustomization);
        }
    }

    public final void b(String str, String str2, LabelCustomization labelCustomization) {
        if (!(str == null || StringsKt.isBlank(str))) {
            this.b.a(str, labelCustomization);
            this.d.setVisibility(0);
            this.c.a(str2, labelCustomization);
        }
    }

    public final AppCompatImageView getExpandArrow$3ds2sdk_release() {
        return this.i;
    }

    public final LinearLayout getExpandContainer$3ds2sdk_release() {
        return this.h;
    }

    public final ThreeDS2TextView getExpandLabel$3ds2sdk_release() {
        return this.f;
    }

    public final ThreeDS2TextView getExpandText$3ds2sdk_release() {
        return this.g;
    }

    public final int getToggleColor$3ds2sdk_release() {
        return this.j;
    }

    public final AppCompatImageView getWhyArrow$3ds2sdk_release() {
        return this.e;
    }

    public final LinearLayout getWhyContainer$3ds2sdk_release() {
        return this.d;
    }

    public final ThreeDS2TextView getWhyLabel$3ds2sdk_release() {
        return this.b;
    }

    public final ThreeDS2TextView getWhyText$3ds2sdk_release() {
        return this.c;
    }

    public final void setToggleColor$3ds2sdk_release(int i2) {
        this.j = i2;
    }
}
