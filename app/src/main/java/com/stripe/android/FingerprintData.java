package com.stripe.android;

import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.json.JSONObject;

@Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\t\n\u0002\b\u000b\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\b\b\u0018\u0000 \u001a2\u00020\u0001:\u0001\u001aB\u001b\u0012\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\b\b\u0002\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\u0010\u0010\u000b\u001a\u0004\u0018\u00010\u0003HÀ\u0003¢\u0006\u0002\b\fJ\u000e\u0010\r\u001a\u00020\u0005HÀ\u0003¢\u0006\u0002\b\u000eJ\u001f\u0010\u000f\u001a\u00020\u00002\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u0005HÆ\u0001J\u0013\u0010\u0010\u001a\u00020\u00112\b\u0010\u0012\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0013\u001a\u00020\u0014HÖ\u0001J\u000e\u0010\u0015\u001a\u00020\u00112\u0006\u0010\u0016\u001a\u00020\u0005J\u0006\u0010\u0017\u001a\u00020\u0018J\t\u0010\u0019\u001a\u00020\u0003HÖ\u0001R\u0016\u0010\u0002\u001a\u0004\u0018\u00010\u0003X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0014\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n¨\u0006\u001b"}, d2 = {"Lcom/stripe/android/FingerprintData;", "", "guid", "", "timestamp", "", "(Ljava/lang/String;J)V", "getGuid$stripe_release", "()Ljava/lang/String;", "getTimestamp$stripe_release", "()J", "component1", "component1$stripe_release", "component2", "component2$stripe_release", "copy", "equals", "", "other", "hashCode", "", "isExpired", "currentTime", "toJson", "Lorg/json/JSONObject;", "toString", "Companion", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: FingerprintData.kt */
public final class FingerprintData {
    public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
    private static final String KEY_GUID = "guid";
    private static final String KEY_TIMESTAMP = "timestamp";
    private static final long TTL = TimeUnit.MINUTES.toMillis(30);
    private final String guid;
    private final long timestamp;

    public FingerprintData() {
        this((String) null, 0, 3, (DefaultConstructorMarker) null);
    }

    public static /* synthetic */ FingerprintData copy$default(FingerprintData fingerprintData, String str, long j, int i, Object obj) {
        if ((i & 1) != 0) {
            str = fingerprintData.guid;
        }
        if ((i & 2) != 0) {
            j = fingerprintData.timestamp;
        }
        return fingerprintData.copy(str, j);
    }

    public final String component1$stripe_release() {
        return this.guid;
    }

    public final long component2$stripe_release() {
        return this.timestamp;
    }

    public final FingerprintData copy(String str, long j) {
        return new FingerprintData(str, j);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof FingerprintData)) {
            return false;
        }
        FingerprintData fingerprintData = (FingerprintData) obj;
        return Intrinsics.areEqual((Object) this.guid, (Object) fingerprintData.guid) && this.timestamp == fingerprintData.timestamp;
    }

    public int hashCode() {
        String str = this.guid;
        int hashCode = str != null ? str.hashCode() : 0;
        long j = this.timestamp;
        return (hashCode * 31) + ((int) (j ^ (j >>> 32)));
    }

    public String toString() {
        return "FingerprintData(guid=" + this.guid + ", timestamp=" + this.timestamp + ")";
    }

    public FingerprintData(String str, long j) {
        this.guid = str;
        this.timestamp = j;
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ FingerprintData(String str, long j, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this((i & 1) != 0 ? null : str, (i & 2) != 0 ? 0 : j);
    }

    public final String getGuid$stripe_release() {
        return this.guid;
    }

    public final long getTimestamp$stripe_release() {
        return this.timestamp;
    }

    public final JSONObject toJson() {
        JSONObject put = new JSONObject().put(KEY_GUID, this.guid).put("timestamp", this.timestamp);
        Intrinsics.checkExpressionValueIsNotNull(put, "JSONObject()\n           …KEY_TIMESTAMP, timestamp)");
        return put;
    }

    public final boolean isExpired(long j) {
        return j - this.timestamp > TTL;
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u000e\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000bR\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0004¢\u0006\u0002\n\u0000¨\u0006\f"}, d2 = {"Lcom/stripe/android/FingerprintData$Companion;", "", "()V", "KEY_GUID", "", "KEY_TIMESTAMP", "TTL", "", "fromJson", "Lcom/stripe/android/FingerprintData;", "json", "Lorg/json/JSONObject;", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: FingerprintData.kt */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        public final FingerprintData fromJson(JSONObject jSONObject) {
            Intrinsics.checkParameterIsNotNull(jSONObject, "json");
            return new FingerprintData(jSONObject.optString(FingerprintData.KEY_GUID), jSONObject.optLong("timestamp", -1));
        }
    }
}
