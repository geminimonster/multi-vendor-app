package com.stripe.android;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import com.stripe.android.stripe3ds2.init.ui.StripeToolbarCustomization;
import com.stripe.android.view.AuthActivityStarter;
import com.stripe.android.view.PaymentAuthWebViewActivity;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0004\b\u0000\u0018\u0000 \f2\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0002\u000b\fB\u0017\b\u0000\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006¢\u0006\u0002\u0010\u0007J\u0010\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u0002H\u0016R\u000e\u0010\u0003\u001a\u00020\u0004X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0004¢\u0006\u0002\n\u0000¨\u0006\r"}, d2 = {"Lcom/stripe/android/PaymentAuthWebViewStarter;", "Lcom/stripe/android/view/AuthActivityStarter;", "Lcom/stripe/android/PaymentAuthWebViewStarter$Args;", "host", "Lcom/stripe/android/view/AuthActivityStarter$Host;", "requestCode", "", "(Lcom/stripe/android/view/AuthActivityStarter$Host;I)V", "start", "", "args", "Args", "Companion", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: PaymentAuthWebViewStarter.kt */
public final class PaymentAuthWebViewStarter implements AuthActivityStarter<Args> {
    public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
    public static final String EXTRA_ARGS = "extra_args";
    private final AuthActivityStarter.Host host;
    private final int requestCode;

    public PaymentAuthWebViewStarter(AuthActivityStarter.Host host2, int i) {
        Intrinsics.checkParameterIsNotNull(host2, "host");
        this.host = host2;
        this.requestCode = i;
    }

    public void start(Args args) {
        Intrinsics.checkParameterIsNotNull(args, "args");
        Bundle bundle = new Bundle();
        bundle.putParcelable(EXTRA_ARGS, args);
        this.host.startActivityForResult$stripe_release(PaymentAuthWebViewActivity.class, bundle, this.requestCode);
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0013\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\b\b\u0018\u00002\u00020\u0001BC\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\n\b\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u0003\u0012\b\b\u0002\u0010\u0006\u001a\u00020\u0007\u0012\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\t\u0012\n\b\u0002\u0010\n\u001a\u0004\u0018\u00010\u0003¢\u0006\u0002\u0010\u000bJ\t\u0010\u0015\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0016\u001a\u00020\u0003HÆ\u0003J\u000b\u0010\u0017\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\t\u0010\u0018\u001a\u00020\u0007HÆ\u0003J\u000b\u0010\u0019\u001a\u0004\u0018\u00010\tHÆ\u0003J\u000b\u0010\u001a\u001a\u0004\u0018\u00010\u0003HÆ\u0003JK\u0010\u001b\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00032\n\b\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u00032\b\b\u0002\u0010\u0006\u001a\u00020\u00072\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\t2\n\b\u0002\u0010\n\u001a\u0004\u0018\u00010\u0003HÆ\u0001J\t\u0010\u001c\u001a\u00020\u001dHÖ\u0001J\u0013\u0010\u001e\u001a\u00020\u00072\b\u0010\u001f\u001a\u0004\u0018\u00010 HÖ\u0003J\t\u0010!\u001a\u00020\u001dHÖ\u0001J\t\u0010\"\u001a\u00020\u0003HÖ\u0001J\u0019\u0010#\u001a\u00020$2\u0006\u0010%\u001a\u00020&2\u0006\u0010'\u001a\u00020\u001dHÖ\u0001R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\rR\u0011\u0010\u0006\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u000fR\u0013\u0010\u0005\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\rR\u0013\u0010\n\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\rR\u0013\u0010\b\u001a\u0004\u0018\u00010\t¢\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u0013R\u0011\u0010\u0004\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0014\u0010\r¨\u0006("}, d2 = {"Lcom/stripe/android/PaymentAuthWebViewStarter$Args;", "Landroid/os/Parcelable;", "clientSecret", "", "url", "returnUrl", "enableLogging", "", "toolbarCustomization", "Lcom/stripe/android/stripe3ds2/init/ui/StripeToolbarCustomization;", "stripeAccountId", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLcom/stripe/android/stripe3ds2/init/ui/StripeToolbarCustomization;Ljava/lang/String;)V", "getClientSecret", "()Ljava/lang/String;", "getEnableLogging", "()Z", "getReturnUrl", "getStripeAccountId", "getToolbarCustomization", "()Lcom/stripe/android/stripe3ds2/init/ui/StripeToolbarCustomization;", "getUrl", "component1", "component2", "component3", "component4", "component5", "component6", "copy", "describeContents", "", "equals", "other", "", "hashCode", "toString", "writeToParcel", "", "parcel", "Landroid/os/Parcel;", "flags", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: PaymentAuthWebViewStarter.kt */
    public static final class Args implements Parcelable {
        public static final Parcelable.Creator CREATOR = new Creator();
        private final String clientSecret;
        private final boolean enableLogging;
        private final String returnUrl;
        private final String stripeAccountId;
        private final StripeToolbarCustomization toolbarCustomization;
        private final String url;

        @Metadata(bv = {1, 0, 3}, k = 3, mv = {1, 1, 16})
        public static class Creator implements Parcelable.Creator {
            public final Object createFromParcel(Parcel parcel) {
                Intrinsics.checkParameterIsNotNull(parcel, "in");
                return new Args(parcel.readString(), parcel.readString(), parcel.readString(), parcel.readInt() != 0, parcel.readInt() != 0 ? StripeToolbarCustomization.CREATOR.createFromParcel(parcel) : null, parcel.readString());
            }

            public final Object[] newArray(int i) {
                return new Args[i];
            }
        }

        public static /* synthetic */ Args copy$default(Args args, String str, String str2, String str3, boolean z, StripeToolbarCustomization stripeToolbarCustomization, String str4, int i, Object obj) {
            if ((i & 1) != 0) {
                str = args.clientSecret;
            }
            if ((i & 2) != 0) {
                str2 = args.url;
            }
            String str5 = str2;
            if ((i & 4) != 0) {
                str3 = args.returnUrl;
            }
            String str6 = str3;
            if ((i & 8) != 0) {
                z = args.enableLogging;
            }
            boolean z2 = z;
            if ((i & 16) != 0) {
                stripeToolbarCustomization = args.toolbarCustomization;
            }
            StripeToolbarCustomization stripeToolbarCustomization2 = stripeToolbarCustomization;
            if ((i & 32) != 0) {
                str4 = args.stripeAccountId;
            }
            return args.copy(str, str5, str6, z2, stripeToolbarCustomization2, str4);
        }

        public final String component1() {
            return this.clientSecret;
        }

        public final String component2() {
            return this.url;
        }

        public final String component3() {
            return this.returnUrl;
        }

        public final boolean component4() {
            return this.enableLogging;
        }

        public final StripeToolbarCustomization component5() {
            return this.toolbarCustomization;
        }

        public final String component6() {
            return this.stripeAccountId;
        }

        public final Args copy(String str, String str2, String str3, boolean z, StripeToolbarCustomization stripeToolbarCustomization, String str4) {
            Intrinsics.checkParameterIsNotNull(str, "clientSecret");
            Intrinsics.checkParameterIsNotNull(str2, "url");
            return new Args(str, str2, str3, z, stripeToolbarCustomization, str4);
        }

        public int describeContents() {
            return 0;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Args)) {
                return false;
            }
            Args args = (Args) obj;
            return Intrinsics.areEqual((Object) this.clientSecret, (Object) args.clientSecret) && Intrinsics.areEqual((Object) this.url, (Object) args.url) && Intrinsics.areEqual((Object) this.returnUrl, (Object) args.returnUrl) && this.enableLogging == args.enableLogging && Intrinsics.areEqual((Object) this.toolbarCustomization, (Object) args.toolbarCustomization) && Intrinsics.areEqual((Object) this.stripeAccountId, (Object) args.stripeAccountId);
        }

        public int hashCode() {
            String str = this.clientSecret;
            int i = 0;
            int hashCode = (str != null ? str.hashCode() : 0) * 31;
            String str2 = this.url;
            int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
            String str3 = this.returnUrl;
            int hashCode3 = (hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31;
            boolean z = this.enableLogging;
            if (z) {
                z = true;
            }
            int i2 = (hashCode3 + (z ? 1 : 0)) * 31;
            StripeToolbarCustomization stripeToolbarCustomization = this.toolbarCustomization;
            int hashCode4 = (i2 + (stripeToolbarCustomization != null ? stripeToolbarCustomization.hashCode() : 0)) * 31;
            String str4 = this.stripeAccountId;
            if (str4 != null) {
                i = str4.hashCode();
            }
            return hashCode4 + i;
        }

        public String toString() {
            return "Args(clientSecret=" + this.clientSecret + ", url=" + this.url + ", returnUrl=" + this.returnUrl + ", enableLogging=" + this.enableLogging + ", toolbarCustomization=" + this.toolbarCustomization + ", stripeAccountId=" + this.stripeAccountId + ")";
        }

        public void writeToParcel(Parcel parcel, int i) {
            Intrinsics.checkParameterIsNotNull(parcel, "parcel");
            parcel.writeString(this.clientSecret);
            parcel.writeString(this.url);
            parcel.writeString(this.returnUrl);
            parcel.writeInt(this.enableLogging ? 1 : 0);
            StripeToolbarCustomization stripeToolbarCustomization = this.toolbarCustomization;
            if (stripeToolbarCustomization != null) {
                parcel.writeInt(1);
                stripeToolbarCustomization.writeToParcel(parcel, 0);
            } else {
                parcel.writeInt(0);
            }
            parcel.writeString(this.stripeAccountId);
        }

        public Args(String str, String str2, String str3, boolean z, StripeToolbarCustomization stripeToolbarCustomization, String str4) {
            Intrinsics.checkParameterIsNotNull(str, "clientSecret");
            Intrinsics.checkParameterIsNotNull(str2, "url");
            this.clientSecret = str;
            this.url = str2;
            this.returnUrl = str3;
            this.enableLogging = z;
            this.toolbarCustomization = stripeToolbarCustomization;
            this.stripeAccountId = str4;
        }

        public final String getClientSecret() {
            return this.clientSecret;
        }

        public final String getUrl() {
            return this.url;
        }

        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ Args(String str, String str2, String str3, boolean z, StripeToolbarCustomization stripeToolbarCustomization, String str4, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this(str, str2, (i & 4) != 0 ? null : str3, (i & 8) != 0 ? false : z, (i & 16) != 0 ? null : stripeToolbarCustomization, (i & 32) != 0 ? null : str4);
        }

        public final String getReturnUrl() {
            return this.returnUrl;
        }

        public final boolean getEnableLogging() {
            return this.enableLogging;
        }

        public final StripeToolbarCustomization getToolbarCustomization() {
            return this.toolbarCustomization;
        }

        public final String getStripeAccountId() {
            return this.stripeAccountId;
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\u0005"}, d2 = {"Lcom/stripe/android/PaymentAuthWebViewStarter$Companion;", "", "()V", "EXTRA_ARGS", "", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: PaymentAuthWebViewStarter.kt */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }
}
