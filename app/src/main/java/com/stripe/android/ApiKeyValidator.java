package com.stripe.android;

import kotlin.Metadata;
import kotlin.jvm.JvmStatic;
import kotlin.text.StringsKt;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\b\u0000\u0018\u0000 \u00062\u00020\u0001:\u0001\u0006B\u0005¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\b\u0010\u0005\u001a\u0004\u0018\u00010\u0004¨\u0006\u0007"}, d2 = {"Lcom/stripe/android/ApiKeyValidator;", "", "()V", "requireValid", "", "apiKey", "Companion", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: ApiKeyValidator.kt */
public final class ApiKeyValidator {
    public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
    /* access modifiers changed from: private */
    public static final ApiKeyValidator DEFAULT = new ApiKeyValidator();

    @JvmStatic
    public static final ApiKeyValidator get$stripe_release() {
        return Companion.get$stripe_release();
    }

    public final String requireValid(String str) {
        CharSequence charSequence = str;
        if (!(!(charSequence == null || StringsKt.isBlank(charSequence)))) {
            throw new IllegalArgumentException("Invalid Publishable Key: You must use a valid Stripe API key to make a Stripe API request. For more info, see https://stripe.com/docs/keys".toString());
        } else if (!StringsKt.startsWith$default(str, "sk_", false, 2, (Object) null)) {
            return str;
        } else {
            throw new IllegalArgumentException("Invalid Publishable Key: You are using a secret key instead of a publishable one. For more info, see https://stripe.com/docs/keys".toString());
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\r\u0010\u0005\u001a\u00020\u0004H\u0001¢\u0006\u0002\b\u0006R\u000e\u0010\u0003\u001a\u00020\u0004X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0007"}, d2 = {"Lcom/stripe/android/ApiKeyValidator$Companion;", "", "()V", "DEFAULT", "Lcom/stripe/android/ApiKeyValidator;", "get", "get$stripe_release", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: ApiKeyValidator.kt */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        @JvmStatic
        public final ApiKeyValidator get$stripe_release() {
            return ApiKeyValidator.DEFAULT;
        }
    }
}
