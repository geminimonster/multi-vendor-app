package com.stripe.android;

import android.os.Build;
import android.util.DisplayMetrics;
import java.math.BigDecimal;
import java.math.MathContext;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import kotlin.TuplesKt;
import kotlin.collections.MapsKt;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010$\n\u0002\b\u0007\b\u0000\u0018\u0000 \u00172\u00020\u0001:\u0001\u0017B\u000f\b\u0010\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004B1\b\u0001\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\b\u0012\b\u0010\t\u001a\u0004\u0018\u00010\b\u0012\u0006\u0010\n\u001a\u00020\b\u0012\u0006\u0010\u000b\u001a\u00020\f¢\u0006\u0002\u0010\rJ\u0014\u0010\u0010\u001a\u000e\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\u00010\u0011H\u0002J\u0019\u0010\u0012\u001a\u000e\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\u00010\u0011H\u0000¢\u0006\u0002\b\u0013J\u0014\u0010\u0014\u001a\u000e\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\u00010\u0011H\u0002J\u001c\u0010\u0015\u001a\u000e\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\u00010\u00112\u0006\u0010\u0016\u001a\u00020\bH\u0002R\u000e\u0010\u000e\u001a\u00020\bX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\fX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\bX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\bX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\bX\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\t\u001a\u0004\u0018\u00010\bX\u0004¢\u0006\u0002\n\u0000¨\u0006\u0018"}, d2 = {"Lcom/stripe/android/FingerprintRequestParamsFactory;", "", "context", "Landroid/content/Context;", "(Landroid/content/Context;)V", "displayMetrics", "Landroid/util/DisplayMetrics;", "packageName", "", "versionName", "timeZone", "clientFingerprintDataStore", "Lcom/stripe/android/ClientFingerprintDataStore;", "(Landroid/util/DisplayMetrics;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/stripe/android/ClientFingerprintDataStore;)V", "androidVersionString", "screen", "createFirstMap", "", "createParams", "createParams$stripe_release", "createSecondMap", "createValueMap", "value", "Companion", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: FingerprintRequestParamsFactory.kt */
public final class FingerprintRequestParamsFactory {
    @Deprecated
    public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
    private final String androidVersionString;
    private final ClientFingerprintDataStore clientFingerprintDataStore;
    private final DisplayMetrics displayMetrics;
    private final String packageName;
    private final String screen;
    private final String timeZone;
    private final String versionName;

    public FingerprintRequestParamsFactory(DisplayMetrics displayMetrics2, String str, String str2, String str3, ClientFingerprintDataStore clientFingerprintDataStore2) {
        Intrinsics.checkParameterIsNotNull(displayMetrics2, "displayMetrics");
        Intrinsics.checkParameterIsNotNull(str, "packageName");
        Intrinsics.checkParameterIsNotNull(str3, "timeZone");
        Intrinsics.checkParameterIsNotNull(clientFingerprintDataStore2, "clientFingerprintDataStore");
        this.displayMetrics = displayMetrics2;
        this.packageName = str;
        this.versionName = str2;
        this.timeZone = str3;
        this.clientFingerprintDataStore = clientFingerprintDataStore2;
        this.screen = this.displayMetrics.widthPixels + "w_" + this.displayMetrics.heightPixels + "h_" + this.displayMetrics.densityDpi + "dpi";
        this.androidVersionString = "Android " + Build.VERSION.RELEASE + ' ' + Build.VERSION.CODENAME + ' ' + Build.VERSION.SDK_INT;
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public FingerprintRequestParamsFactory(android.content.Context r9) {
        /*
            r8 = this;
            java.lang.String r0 = "context"
            kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(r9, r0)
            android.content.res.Resources r0 = r9.getResources()
            java.lang.String r1 = "context.resources"
            kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r0, r1)
            android.util.DisplayMetrics r3 = r0.getDisplayMetrics()
            java.lang.String r0 = "context.resources.displayMetrics"
            kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r3, r0)
            java.lang.String r0 = r9.getPackageName()
            if (r0 == 0) goto L_0x001e
            goto L_0x0020
        L_0x001e:
            java.lang.String r0 = ""
        L_0x0020:
            r4 = r0
            com.stripe.android.utils.ContextUtils r0 = com.stripe.android.utils.ContextUtils.INSTANCE
            android.content.pm.PackageInfo r0 = r0.getPackageInfo$stripe_release(r9)
            r1 = 0
            if (r0 == 0) goto L_0x002e
            java.lang.String r0 = r0.versionName
            r5 = r0
            goto L_0x002f
        L_0x002e:
            r5 = r1
        L_0x002f:
            com.stripe.android.FingerprintRequestParamsFactory$Companion r0 = Companion
            java.lang.String r6 = r0.createTimezone()
            com.stripe.android.ClientFingerprintDataStore$Default r0 = new com.stripe.android.ClientFingerprintDataStore$Default
            r2 = 2
            r0.<init>(r9, r1, r2, r1)
            r7 = r0
            com.stripe.android.ClientFingerprintDataStore r7 = (com.stripe.android.ClientFingerprintDataStore) r7
            r2 = r8
            r2.<init>(r3, r4, r5, r6, r7)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.stripe.android.FingerprintRequestParamsFactory.<init>(android.content.Context):void");
    }

    public final /* synthetic */ Map<String, Object> createParams$stripe_release() {
        return MapsKt.mapOf(TuplesKt.to("v2", 1), TuplesKt.to("tag", BuildConfig.VERSION_NAME), TuplesKt.to("src", "android-sdk"), TuplesKt.to("a", createFirstMap()), TuplesKt.to("b", createSecondMap()));
    }

    private final Map<String, Object> createFirstMap() {
        String locale = Locale.getDefault().toString();
        Intrinsics.checkExpressionValueIsNotNull(locale, "Locale.getDefault().toString()");
        return MapsKt.mapOf(TuplesKt.to("c", createValueMap(locale)), TuplesKt.to("d", createValueMap(this.androidVersionString)), TuplesKt.to("f", createValueMap(this.screen)), TuplesKt.to("g", createValueMap(this.timeZone)));
    }

    private final Map<String, Object> createSecondMap() {
        Map mapOf = MapsKt.mapOf(TuplesKt.to("d", this.clientFingerprintDataStore.getMuid()), TuplesKt.to("e", this.clientFingerprintDataStore.getSid()), TuplesKt.to("k", this.packageName), TuplesKt.to("o", Build.VERSION.RELEASE), TuplesKt.to("p", Integer.valueOf(Build.VERSION.SDK_INT)), TuplesKt.to("q", Build.MANUFACTURER), TuplesKt.to("r", Build.BRAND), TuplesKt.to("s", Build.MODEL), TuplesKt.to("t", Build.TAGS));
        String str = this.versionName;
        Map mapOf2 = str != null ? MapsKt.mapOf(TuplesKt.to("l", str)) : null;
        if (mapOf2 == null) {
            mapOf2 = MapsKt.emptyMap();
        }
        return MapsKt.plus(mapOf, mapOf2);
    }

    private final Map<String, Object> createValueMap(String str) {
        return MapsKt.mapOf(TuplesKt.to("v", str));
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\b\u0010\u0003\u001a\u00020\u0004H\u0002¨\u0006\u0005"}, d2 = {"Lcom/stripe/android/FingerprintRequestParamsFactory$Companion;", "", "()V", "createTimezone", "", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: FingerprintRequestParamsFactory.kt */
    private static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        /* access modifiers changed from: private */
        public final String createTimezone() {
            TimeUnit timeUnit = TimeUnit.MINUTES;
            TimeZone timeZone = TimeZone.getDefault();
            Intrinsics.checkExpressionValueIsNotNull(timeZone, "TimeZone.getDefault()");
            int convert = (int) timeUnit.convert((long) timeZone.getRawOffset(), TimeUnit.MILLISECONDS);
            if (convert % 60 == 0) {
                return String.valueOf(convert / 60);
            }
            String bigDecimal = new BigDecimal(convert).setScale(2, 6).divide(new BigDecimal(60), new MathContext(2)).setScale(2, 6).toString();
            Intrinsics.checkExpressionValueIsNotNull(bigDecimal, "decHours.toString()");
            return bigDecimal;
        }
    }
}
