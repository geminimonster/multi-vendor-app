package com.stripe.android;

import android.os.Handler;
import com.stripe.android.CustomerSessionRunnableFactory;
import com.stripe.android.EphemeralOperation;
import com.stripe.android.exception.StripeException;
import com.stripe.android.model.Customer;
import kotlin.Metadata;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0011\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002*\u0001\u0000\b\n\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001J\n\u0010\u0003\u001a\u0004\u0018\u00010\u0002H\u0016¨\u0006\u0004"}, d2 = {"com/stripe/android/CustomerSessionRunnableFactory$createUpdateCustomerRunnable$1", "Lcom/stripe/android/CustomerSessionRunnableFactory$CustomerSessionRunnable;", "Lcom/stripe/android/model/Customer;", "createMessageObject", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: CustomerSessionRunnableFactory.kt */
public final class CustomerSessionRunnableFactory$createUpdateCustomerRunnable$1 extends CustomerSessionRunnableFactory.CustomerSessionRunnable<Customer> {
    final /* synthetic */ EphemeralKey $key;
    final /* synthetic */ EphemeralOperation.RetrieveKey $operation;
    final /* synthetic */ CustomerSessionRunnableFactory this$0;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    CustomerSessionRunnableFactory$createUpdateCustomerRunnable$1(CustomerSessionRunnableFactory customerSessionRunnableFactory, EphemeralKey ephemeralKey, EphemeralOperation.RetrieveKey retrieveKey, Handler handler, CustomerSessionRunnableFactory.ResultType resultType, String str) {
        super(handler, resultType, str);
        this.this$0 = customerSessionRunnableFactory;
        this.$key = ephemeralKey;
        this.$operation = retrieveKey;
    }

    /* renamed from: createMessageObject */
    public Customer createMessageObject$stripe_release() throws StripeException {
        return this.this$0.retrieveCustomerWithKey(this.$key, this.$operation.getProductUsage$stripe_release());
    }
}
