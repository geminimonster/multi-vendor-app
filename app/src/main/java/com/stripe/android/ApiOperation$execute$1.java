package com.stripe.android;

import kotlin.Metadata;
import kotlin.Unit;
import kotlin.coroutines.Continuation;
import kotlin.coroutines.jvm.internal.DebugMetadata;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.Intrinsics;
import kotlinx.coroutines.CoroutineScope;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0010\u0000\u001a\u00020\u0001\"\u0004\b\u0000\u0010\u0002*\u00020\u0003H@¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"<anonymous>", "", "ResultType", "Lkotlinx/coroutines/CoroutineScope;", "invoke", "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;"}, k = 3, mv = {1, 1, 16})
@DebugMetadata(c = "com.stripe.android.ApiOperation$execute$1", f = "ApiOperation.kt", i = {0, 1, 1}, l = {24, 41}, m = "invokeSuspend", n = {"$this$launch", "$this$launch", "result"}, s = {"L$0", "L$0", "L$1"})
/* compiled from: ApiOperation.kt */
final class ApiOperation$execute$1 extends SuspendLambda implements Function2<CoroutineScope, Continuation<? super Unit>, Object> {
    Object L$0;
    Object L$1;
    Object L$2;
    Object L$3;
    int label;
    private CoroutineScope p$;
    final /* synthetic */ ApiOperation this$0;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    ApiOperation$execute$1(ApiOperation apiOperation, Continuation continuation) {
        super(2, continuation);
        this.this$0 = apiOperation;
    }

    public final Continuation<Unit> create(Object obj, Continuation<?> continuation) {
        Intrinsics.checkParameterIsNotNull(continuation, "completion");
        ApiOperation$execute$1 apiOperation$execute$1 = new ApiOperation$execute$1(this.this$0, continuation);
        apiOperation$execute$1.p$ = (CoroutineScope) obj;
        return apiOperation$execute$1;
    }

    public final Object invoke(Object obj, Object obj2) {
        return ((ApiOperation$execute$1) create(obj, (Continuation) obj2)).invokeSuspend(Unit.INSTANCE);
    }

    /* JADX WARNING: Removed duplicated region for block: B:46:0x00fb A[RETURN] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object invokeSuspend(java.lang.Object r18) {
        /*
            r17 = this;
            r1 = r17
            java.lang.Object r2 = kotlin.coroutines.intrinsics.IntrinsicsKt.getCOROUTINE_SUSPENDED()
            int r0 = r1.label
            r3 = 1
            r4 = 0
            r5 = 2
            if (r0 == 0) goto L_0x004a
            if (r0 == r3) goto L_0x0026
            if (r0 != r5) goto L_0x001e
            java.lang.Object r0 = r1.L$1
            kotlin.jvm.internal.Ref$ObjectRef r0 = (kotlin.jvm.internal.Ref.ObjectRef) r0
            java.lang.Object r0 = r1.L$0
            kotlinx.coroutines.CoroutineScope r0 = (kotlinx.coroutines.CoroutineScope) r0
            kotlin.ResultKt.throwOnFailure(r18)
            goto L_0x00fc
        L_0x001e:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r2 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r2)
            throw r0
        L_0x0026:
            java.lang.Object r0 = r1.L$3
            kotlin.jvm.internal.Ref$ObjectRef r0 = (kotlin.jvm.internal.Ref.ObjectRef) r0
            java.lang.Object r3 = r1.L$2
            kotlin.jvm.internal.Ref$ObjectRef r3 = (kotlin.jvm.internal.Ref.ObjectRef) r3
            java.lang.Object r6 = r1.L$1
            kotlin.jvm.internal.Ref$ObjectRef r6 = (kotlin.jvm.internal.Ref.ObjectRef) r6
            java.lang.Object r7 = r1.L$0
            kotlinx.coroutines.CoroutineScope r7 = (kotlinx.coroutines.CoroutineScope) r7
            kotlin.ResultKt.throwOnFailure(r18)     // Catch:{ StripeException -> 0x0047, JSONException -> 0x0044, IOException -> 0x0041, IllegalArgumentException -> 0x003f }
            r8 = r7
            r7 = r6
            r6 = r0
            r0 = r18
            goto L_0x006c
        L_0x003f:
            r0 = move-exception
            goto L_0x0084
        L_0x0041:
            r0 = move-exception
            goto L_0x00a6
        L_0x0044:
            r0 = move-exception
            goto L_0x00bb
        L_0x0047:
            r0 = move-exception
            goto L_0x00d1
        L_0x004a:
            kotlin.ResultKt.throwOnFailure(r18)
            kotlinx.coroutines.CoroutineScope r7 = r1.p$
            kotlin.jvm.internal.Ref$ObjectRef r6 = new kotlin.jvm.internal.Ref$ObjectRef
            r6.<init>()
            kotlin.Result$Companion r0 = kotlin.Result.Companion     // Catch:{ StripeException -> 0x00cf, JSONException -> 0x00b9, IOException -> 0x00a4, IllegalArgumentException -> 0x0082 }
            com.stripe.android.ApiOperation r0 = r1.this$0     // Catch:{ StripeException -> 0x00cf, JSONException -> 0x00b9, IOException -> 0x00a4, IllegalArgumentException -> 0x0082 }
            r1.L$0 = r7     // Catch:{ StripeException -> 0x00cf, JSONException -> 0x00b9, IOException -> 0x00a4, IllegalArgumentException -> 0x0082 }
            r1.L$1 = r6     // Catch:{ StripeException -> 0x00cf, JSONException -> 0x00b9, IOException -> 0x00a4, IllegalArgumentException -> 0x0082 }
            r1.L$2 = r6     // Catch:{ StripeException -> 0x00cf, JSONException -> 0x00b9, IOException -> 0x00a4, IllegalArgumentException -> 0x0082 }
            r1.L$3 = r6     // Catch:{ StripeException -> 0x00cf, JSONException -> 0x00b9, IOException -> 0x00a4, IllegalArgumentException -> 0x0082 }
            r1.label = r3     // Catch:{ StripeException -> 0x00cf, JSONException -> 0x00b9, IOException -> 0x00a4, IllegalArgumentException -> 0x0082 }
            java.lang.Object r0 = r0.getResult$stripe_release(r1)     // Catch:{ StripeException -> 0x00cf, JSONException -> 0x00b9, IOException -> 0x00a4, IllegalArgumentException -> 0x0082 }
            if (r0 != r2) goto L_0x0069
            return r2
        L_0x0069:
            r3 = r6
            r8 = r7
            r7 = r3
        L_0x006c:
            java.lang.Object r0 = kotlin.Result.m4constructorimpl(r0)     // Catch:{ StripeException -> 0x007e, JSONException -> 0x007a, IOException -> 0x0076, IllegalArgumentException -> 0x0072 }
            goto L_0x00e0
        L_0x0072:
            r0 = move-exception
            r6 = r7
            r7 = r8
            goto L_0x0084
        L_0x0076:
            r0 = move-exception
            r6 = r7
            r7 = r8
            goto L_0x00a6
        L_0x007a:
            r0 = move-exception
            r6 = r7
            r7 = r8
            goto L_0x00bb
        L_0x007e:
            r0 = move-exception
            r6 = r7
            r7 = r8
            goto L_0x00d1
        L_0x0082:
            r0 = move-exception
            r3 = r6
        L_0x0084:
            kotlin.Result$Companion r8 = kotlin.Result.Companion
            com.stripe.android.exception.InvalidRequestException r8 = new com.stripe.android.exception.InvalidRequestException
            r10 = 0
            r11 = 0
            r12 = 0
            java.lang.String r13 = r0.getMessage()
            r14 = r0
            java.lang.Throwable r14 = (java.lang.Throwable) r14
            r15 = 7
            r16 = 0
            r9 = r8
            r9.<init>(r10, r11, r12, r13, r14, r15, r16)
            java.lang.Throwable r8 = (java.lang.Throwable) r8
            java.lang.Object r0 = kotlin.ResultKt.createFailure(r8)
            java.lang.Object r0 = kotlin.Result.m4constructorimpl(r0)
            goto L_0x00dd
        L_0x00a4:
            r0 = move-exception
            r3 = r6
        L_0x00a6:
            kotlin.Result$Companion r8 = kotlin.Result.Companion
            com.stripe.android.exception.APIConnectionException$Companion r8 = com.stripe.android.exception.APIConnectionException.Companion
            com.stripe.android.exception.APIConnectionException r0 = com.stripe.android.exception.APIConnectionException.Companion.create$stripe_release$default(r8, r0, r4, r5, r4)
            java.lang.Throwable r0 = (java.lang.Throwable) r0
            java.lang.Object r0 = kotlin.ResultKt.createFailure(r0)
            java.lang.Object r0 = kotlin.Result.m4constructorimpl(r0)
            goto L_0x00dd
        L_0x00b9:
            r0 = move-exception
            r3 = r6
        L_0x00bb:
            kotlin.Result$Companion r8 = kotlin.Result.Companion
            com.stripe.android.exception.APIException r8 = new com.stripe.android.exception.APIException
            java.lang.Exception r0 = (java.lang.Exception) r0
            r8.<init>(r0)
            java.lang.Throwable r8 = (java.lang.Throwable) r8
            java.lang.Object r0 = kotlin.ResultKt.createFailure(r8)
            java.lang.Object r0 = kotlin.Result.m4constructorimpl(r0)
            goto L_0x00dd
        L_0x00cf:
            r0 = move-exception
            r3 = r6
        L_0x00d1:
            kotlin.Result$Companion r8 = kotlin.Result.Companion
            java.lang.Throwable r0 = (java.lang.Throwable) r0
            java.lang.Object r0 = kotlin.ResultKt.createFailure(r0)
            java.lang.Object r0 = kotlin.Result.m4constructorimpl(r0)
        L_0x00dd:
            r8 = r7
            r7 = r6
            r6 = r3
        L_0x00e0:
            r6.element = r0
            kotlinx.coroutines.MainCoroutineDispatcher r0 = kotlinx.coroutines.Dispatchers.getMain()
            kotlin.coroutines.CoroutineContext r0 = (kotlin.coroutines.CoroutineContext) r0
            com.stripe.android.ApiOperation$execute$1$1 r3 = new com.stripe.android.ApiOperation$execute$1$1
            r3.<init>(r1, r7, r4)
            kotlin.jvm.functions.Function2 r3 = (kotlin.jvm.functions.Function2) r3
            r1.L$0 = r8
            r1.L$1 = r7
            r1.label = r5
            java.lang.Object r0 = kotlinx.coroutines.BuildersKt.withContext(r0, r3, r1)
            if (r0 != r2) goto L_0x00fc
            return r2
        L_0x00fc:
            kotlin.Unit r0 = kotlin.Unit.INSTANCE
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.stripe.android.ApiOperation$execute$1.invokeSuspend(java.lang.Object):java.lang.Object");
    }
}
