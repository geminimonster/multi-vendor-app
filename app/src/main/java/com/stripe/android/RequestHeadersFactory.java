package com.stripe.android;

import android.os.Build;
import com.facebook.appevents.codeless.internal.Constants;
import com.stripe.android.ApiRequest;
import java.util.Locale;
import java.util.Map;
import kotlin.Metadata;
import kotlin.TuplesKt;
import kotlin.collections.CollectionsKt;
import kotlin.collections.MapsKt;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.text.Charsets;
import kotlin.text.StringsKt;
import org.json.JSONObject;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\u0010\u000e\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b0\u0018\u0000 \r2\u00020\u0001:\u0003\f\r\u000eB\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0012\u0010\u000b\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00050\u0004R\u001e\u0010\u0003\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00050\u0004X¤\u0004¢\u0006\u0006\u001a\u0004\b\u0006\u0010\u0007R\u0012\u0010\b\u001a\u00020\u0005X¤\u0004¢\u0006\u0006\u001a\u0004\b\t\u0010\n\u0001\u0002\u000f\u0010¨\u0006\u0011"}, d2 = {"Lcom/stripe/android/RequestHeadersFactory;", "", "()V", "extraHeaders", "", "", "getExtraHeaders", "()Ljava/util/Map;", "userAgent", "getUserAgent", "()Ljava/lang/String;", "create", "Api", "Companion", "Default", "Lcom/stripe/android/RequestHeadersFactory$Api;", "Lcom/stripe/android/RequestHeadersFactory$Default;", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: RequestHeadersFactory.kt */
public abstract class RequestHeadersFactory {
    private static final String CHARSET = Charsets.UTF_8.name();
    public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
    private static final String HEADER_ACCEPT_CHARSET = "Accept-Charset";
    private static final String HEADER_USER_AGENT = "User-Agent";

    /* access modifiers changed from: protected */
    public abstract Map<String, String> getExtraHeaders();

    /* access modifiers changed from: protected */
    public abstract String getUserAgent();

    private RequestHeadersFactory() {
    }

    public /* synthetic */ RequestHeadersFactory(DefaultConstructorMarker defaultConstructorMarker) {
        this();
    }

    public final Map<String, String> create() {
        return MapsKt.plus(getExtraHeaders(), (Map<String, String>) MapsKt.mapOf(TuplesKt.to(HEADER_USER_AGENT, getUserAgent()), TuplesKt.to(HEADER_ACCEPT_CHARSET, CHARSET)));
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0010$\n\u0002\b\n\u0018\u0000 \u00182\u00020\u0001:\u0001\u0018BM\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\b\b\u0002\u0010\u0006\u001a\u00020\u0007\u0012\u0014\b\u0002\u0010\b\u001a\u000e\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\n0\t\u0012\b\b\u0002\u0010\u000b\u001a\u00020\n\u0012\b\b\u0002\u0010\f\u001a\u00020\n¢\u0006\u0002\u0010\rJ\u0014\u0010\u0017\u001a\u00020\n2\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005H\u0002R\u000e\u0010\u000b\u001a\u00020\nX\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u0004\u001a\u0004\u0018\u00010\u0005X\u0004¢\u0006\u0002\n\u0000R \u0010\u000e\u001a\u000e\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\n0\u000f8TX\u0004¢\u0006\u0006\u001a\u0004\b\u0010\u0010\u0011R\u0016\u0010\u0012\u001a\u0004\u0018\u00010\n8BX\u0004¢\u0006\u0006\u001a\u0004\b\u0013\u0010\u0014R\u000e\u0010\u0006\u001a\u00020\u0007X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\nX\u0004¢\u0006\u0002\n\u0000R\u001a\u0010\b\u001a\u000e\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\n0\tX\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\u0015\u001a\u00020\n8TX\u0004¢\u0006\u0006\u001a\u0004\b\u0016\u0010\u0014¨\u0006\u0019"}, d2 = {"Lcom/stripe/android/RequestHeadersFactory$Api;", "Lcom/stripe/android/RequestHeadersFactory;", "options", "Lcom/stripe/android/ApiRequest$Options;", "appInfo", "Lcom/stripe/android/AppInfo;", "locale", "Ljava/util/Locale;", "systemPropertySupplier", "Lkotlin/Function1;", "", "apiVersion", "sdkVersion", "(Lcom/stripe/android/ApiRequest$Options;Lcom/stripe/android/AppInfo;Ljava/util/Locale;Lkotlin/jvm/functions/Function1;Ljava/lang/String;Ljava/lang/String;)V", "extraHeaders", "", "getExtraHeaders", "()Ljava/util/Map;", "languageTag", "getLanguageTag", "()Ljava/lang/String;", "userAgent", "getUserAgent", "createStripeClientUserAgent", "Companion", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: RequestHeadersFactory.kt */
    public static final class Api extends RequestHeadersFactory {
        @Deprecated
        public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
        private static final Function1<String, String> DEFAULT_SYSTEM_PROPERTY_SUPPLIER = RequestHeadersFactory$Api$Companion$DEFAULT_SYSTEM_PROPERTY_SUPPLIER$1.INSTANCE;
        private static final String PROP_USER_AGENT = "http.agent";
        private final String apiVersion;
        private final AppInfo appInfo;
        private final Locale locale;
        private final ApiRequest.Options options;
        private final String sdkVersion;
        private final Function1<String, String> systemPropertySupplier;

        /* JADX WARNING: Illegal instructions before constructor call */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public /* synthetic */ Api(com.stripe.android.ApiRequest.Options r8, com.stripe.android.AppInfo r9, java.util.Locale r10, kotlin.jvm.functions.Function1<java.lang.String, java.lang.String> r11, java.lang.String r12, java.lang.String r13, int r14, kotlin.jvm.internal.DefaultConstructorMarker r15) {
            /*
                r7 = this;
                r15 = r14 & 2
                if (r15 == 0) goto L_0x0007
                r9 = 0
                com.stripe.android.AppInfo r9 = (com.stripe.android.AppInfo) r9
            L_0x0007:
                r2 = r9
                r9 = r14 & 4
                if (r9 == 0) goto L_0x0015
                java.util.Locale r10 = java.util.Locale.getDefault()
                java.lang.String r9 = "Locale.getDefault()"
                kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r10, r9)
            L_0x0015:
                r3 = r10
                r9 = r14 & 8
                if (r9 == 0) goto L_0x001c
                kotlin.jvm.functions.Function1<java.lang.String, java.lang.String> r11 = DEFAULT_SYSTEM_PROPERTY_SUPPLIER
            L_0x001c:
                r4 = r11
                r9 = r14 & 16
                if (r9 == 0) goto L_0x002b
                com.stripe.android.ApiVersion$Companion r9 = com.stripe.android.ApiVersion.Companion
                com.stripe.android.ApiVersion r9 = r9.get$stripe_release()
                java.lang.String r12 = r9.getCode$stripe_release()
            L_0x002b:
                r5 = r12
                r9 = r14 & 32
                if (r9 == 0) goto L_0x0032
                java.lang.String r13 = "AndroidBindings/14.5.0"
            L_0x0032:
                r6 = r13
                r0 = r7
                r1 = r8
                r0.<init>(r1, r2, r3, r4, r5, r6)
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.stripe.android.RequestHeadersFactory.Api.<init>(com.stripe.android.ApiRequest$Options, com.stripe.android.AppInfo, java.util.Locale, kotlin.jvm.functions.Function1, java.lang.String, java.lang.String, int, kotlin.jvm.internal.DefaultConstructorMarker):void");
        }

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Api(ApiRequest.Options options2, AppInfo appInfo2, Locale locale2, Function1<? super String, String> function1, String str, String str2) {
            super((DefaultConstructorMarker) null);
            Intrinsics.checkParameterIsNotNull(options2, "options");
            Intrinsics.checkParameterIsNotNull(locale2, "locale");
            Intrinsics.checkParameterIsNotNull(function1, "systemPropertySupplier");
            Intrinsics.checkParameterIsNotNull(str, "apiVersion");
            Intrinsics.checkParameterIsNotNull(str2, "sdkVersion");
            this.options = options2;
            this.appInfo = appInfo2;
            this.locale = locale2;
            this.systemPropertySupplier = function1;
            this.apiVersion = str;
            this.sdkVersion = str2;
        }

        private final String getLanguageTag() {
            String locale2 = this.locale.toString();
            Intrinsics.checkExpressionValueIsNotNull(locale2, "locale.toString()");
            String replace$default = StringsKt.replace$default(locale2, "_", "-", false, 4, (Object) null);
            if (!StringsKt.isBlank(replace$default)) {
                return replace$default;
            }
            return null;
        }

        /* access modifiers changed from: protected */
        public String getUserAgent() {
            String[] strArr = new String[2];
            strArr[0] = RequestHeadersFactory.Companion.getUserAgent$stripe_release(this.sdkVersion);
            AppInfo appInfo2 = this.appInfo;
            strArr[1] = appInfo2 != null ? appInfo2.toUserAgent$stripe_release() : null;
            return CollectionsKt.joinToString$default(CollectionsKt.listOfNotNull((T[]) strArr), " ", (CharSequence) null, (CharSequence) null, 0, (CharSequence) null, (Function1) null, 62, (Object) null);
        }

        /* access modifiers changed from: protected */
        public Map<String, String> getExtraHeaders() {
            Map mapOf = MapsKt.mapOf(TuplesKt.to("Accept", "application/json"), TuplesKt.to(ApiRequest.HEADER_STRIPE_CLIENT_USER_AGENT, createStripeClientUserAgent(this.appInfo)), TuplesKt.to("Stripe-Version", this.apiVersion), TuplesKt.to("Authorization", "Bearer " + this.options.getApiKey()));
            String stripeAccount$stripe_release = this.options.getStripeAccount$stripe_release();
            Map map = null;
            Map mapOf2 = stripeAccount$stripe_release != null ? MapsKt.mapOf(TuplesKt.to("Stripe-Account", stripeAccount$stripe_release)) : null;
            if (mapOf2 == null) {
                mapOf2 = MapsKt.emptyMap();
            }
            Map plus = MapsKt.plus(mapOf, mapOf2);
            String idempotencyKey$stripe_release = this.options.getIdempotencyKey$stripe_release();
            Map mapOf3 = idempotencyKey$stripe_release != null ? MapsKt.mapOf(TuplesKt.to("Idempotency-Key", idempotencyKey$stripe_release)) : null;
            if (mapOf3 == null) {
                mapOf3 = MapsKt.emptyMap();
            }
            Map plus2 = MapsKt.plus(plus, mapOf3);
            String languageTag = getLanguageTag();
            if (languageTag != null) {
                map = MapsKt.mapOf(TuplesKt.to("Accept-Language", languageTag));
            }
            if (map == null) {
                map = MapsKt.emptyMap();
            }
            return MapsKt.plus(plus2, map);
        }

        static /* synthetic */ String createStripeClientUserAgent$default(Api api, AppInfo appInfo2, int i, Object obj) {
            if ((i & 1) != 0) {
                appInfo2 = null;
            }
            return api.createStripeClientUserAgent(appInfo2);
        }

        private final String createStripeClientUserAgent(AppInfo appInfo2) {
            Map mapOf = MapsKt.mapOf(TuplesKt.to("os.name", Constants.PLATFORM), TuplesKt.to("os.version", String.valueOf(Build.VERSION.SDK_INT)), TuplesKt.to("bindings.version", BuildConfig.VERSION_NAME), TuplesKt.to("lang", "Java"), TuplesKt.to("publisher", "Stripe"), TuplesKt.to(PROP_USER_AGENT, this.systemPropertySupplier.invoke(PROP_USER_AGENT)));
            Map<String, Map<String, String>> createClientHeaders$stripe_release = appInfo2 != null ? appInfo2.createClientHeaders$stripe_release() : null;
            if (createClientHeaders$stripe_release == null) {
                createClientHeaders$stripe_release = MapsKt.emptyMap();
            }
            String jSONObject = new JSONObject(MapsKt.plus(mapOf, (Map) createClientHeaders$stripe_release)).toString();
            Intrinsics.checkExpressionValueIsNotNull(jSONObject, "JSONObject(\n            …\n            ).toString()");
            return jSONObject;
        }

        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u001a\u0010\u0003\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00050\u0004X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0005XT¢\u0006\u0002\n\u0000¨\u0006\u0007"}, d2 = {"Lcom/stripe/android/RequestHeadersFactory$Api$Companion;", "", "()V", "DEFAULT_SYSTEM_PROPERTY_SUPPLIER", "Lkotlin/Function1;", "", "PROP_USER_AGENT", "stripe_release"}, k = 1, mv = {1, 1, 16})
        /* compiled from: RequestHeadersFactory.kt */
        private static final class Companion {
            private Companion() {
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\u0010\u000e\n\u0002\b\b\u0018\u00002\u00020\u0001B%\u0012\u0014\b\u0002\u0010\u0002\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00040\u0003\u0012\b\b\u0002\u0010\u0005\u001a\u00020\u0004¢\u0006\u0002\u0010\u0006R \u0010\u0002\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00040\u0003X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0014\u0010\t\u001a\u00020\u0004X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000b¨\u0006\f"}, d2 = {"Lcom/stripe/android/RequestHeadersFactory$Default;", "Lcom/stripe/android/RequestHeadersFactory;", "extraHeaders", "", "", "sdkVersion", "(Ljava/util/Map;Ljava/lang/String;)V", "getExtraHeaders", "()Ljava/util/Map;", "userAgent", "getUserAgent", "()Ljava/lang/String;", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: RequestHeadersFactory.kt */
    public static final class Default extends RequestHeadersFactory {
        private final Map<String, String> extraHeaders;
        private final String userAgent;

        public Default() {
            this((Map) null, (String) null, 3, (DefaultConstructorMarker) null);
        }

        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ Default(Map map, String str, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this((i & 1) != 0 ? MapsKt.emptyMap() : map, (i & 2) != 0 ? Stripe.VERSION : str);
        }

        /* access modifiers changed from: protected */
        public Map<String, String> getExtraHeaders() {
            return this.extraHeaders;
        }

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Default(Map<String, String> map, String str) {
            super((DefaultConstructorMarker) null);
            Intrinsics.checkParameterIsNotNull(map, "extraHeaders");
            Intrinsics.checkParameterIsNotNull(str, "sdkVersion");
            this.extraHeaders = map;
            this.userAgent = RequestHeadersFactory.Companion.getUserAgent$stripe_release(str);
        }

        /* access modifiers changed from: protected */
        public String getUserAgent() {
            return this.userAgent;
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0007\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0017\u0010\b\u001a\u00020\u00042\b\b\u0002\u0010\t\u001a\u00020\u0004H\u0000¢\u0006\u0002\b\nR\u0016\u0010\u0003\u001a\n \u0005*\u0004\u0018\u00010\u00040\u0004X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\u000b"}, d2 = {"Lcom/stripe/android/RequestHeadersFactory$Companion;", "", "()V", "CHARSET", "", "kotlin.jvm.PlatformType", "HEADER_ACCEPT_CHARSET", "HEADER_USER_AGENT", "getUserAgent", "sdkVersion", "getUserAgent$stripe_release", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: RequestHeadersFactory.kt */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        public static /* synthetic */ String getUserAgent$stripe_release$default(Companion companion, String str, int i, Object obj) {
            if ((i & 1) != 0) {
                str = Stripe.VERSION;
            }
            return companion.getUserAgent$stripe_release(str);
        }

        public final String getUserAgent$stripe_release(String str) {
            Intrinsics.checkParameterIsNotNull(str, "sdkVersion");
            return "Stripe/v1 " + str;
        }
    }
}
