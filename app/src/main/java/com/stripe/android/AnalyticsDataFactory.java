package com.stripe.android;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import com.facebook.internal.AnalyticsEvents;
import com.facebook.internal.NativeProtocol;
import com.stripe.android.model.PaymentMethod;
import com.stripe.android.stripe3ds2.transaction.ErrorMessage;
import com.stripe.android.stripe3ds2.transaction.ProtocolErrorEvent;
import com.stripe.android.stripe3ds2.transaction.RuntimeErrorEvent;
import java.lang.annotation.RetentionPolicy;
import java.util.Collection;
import java.util.Map;
import java.util.Set;
import kotlin.Metadata;
import kotlin.TuplesKt;
import kotlin.annotation.AnnotationRetention;
import kotlin.annotation.Retention;
import kotlin.collections.CollectionsKt;
import kotlin.collections.MapsKt;
import kotlin.collections.SetsKt;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000^\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010$\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\"\n\u0002\b\u001a\n\u0002\u0018\u0002\n\u0002\b\u000e\n\u0002\u0010\r\n\u0002\b\u0003\b\u0000\u0018\u0000 G2\u00020\u0001:\u0002GHB\u0017\b\u0010\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006B+\b\u0001\u0012\b\u0010\u0007\u001a\u0004\u0018\u00010\b\u0012\b\u0010\t\u001a\u0004\u0018\u00010\n\u0012\u0006\u0010\u000b\u001a\u00020\u0005\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\fJ)\u0010\r\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00010\u000e2\u0006\u0010\u000f\u001a\u00020\u00052\u0006\u0010\u0010\u001a\u00020\u0011H\u0000¢\u0006\u0002\b\u0012J)\u0010\r\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00010\u000e2\u0006\u0010\u000f\u001a\u00020\u00052\u0006\u0010\u0013\u001a\u00020\u0014H\u0000¢\u0006\u0002\b\u0012J1\u0010\u0015\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00010\u000e2\u0006\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u000f\u001a\u00020\u00052\u0006\u0010\u0018\u001a\u00020\u0005H\u0000¢\u0006\u0002\b\u0019J3\u0010\u001a\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00010\u000e2\u0010\b\u0002\u0010\u001b\u001a\n\u0012\u0004\u0012\u00020\u0005\u0018\u00010\u001c2\u0006\u0010\u001d\u001a\u00020\u0005H\u0000¢\u0006\u0002\b\u001eJ\u0019\u0010\u001f\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00010\u000eH\u0000¢\u0006\u0002\b J)\u0010!\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00010\u000e2\u000e\u0010\u001b\u001a\n\u0012\u0004\u0012\u00020\u0005\u0018\u00010\u001cH\u0000¢\u0006\u0002\b\"J)\u0010#\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00010\u000e2\u0006\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u000f\u001a\u00020\u0005H\u0000¢\u0006\u0002\b$J+\u0010%\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00010\u000e2\u0006\u0010\u0016\u001a\u00020\u00172\b\u0010&\u001a\u0004\u0018\u00010\u0005H\u0000¢\u0006\u0002\b'J)\u0010(\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00010\u000e2\u000e\u0010\u001b\u001a\n\u0012\u0004\u0012\u00020\u0005\u0018\u00010\u001cH\u0000¢\u0006\u0002\b)J)\u0010*\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00010\u000e2\u000e\u0010\u001b\u001a\n\u0012\u0004\u0012\u00020\u0005\u0018\u00010\u001cH\u0000¢\u0006\u0002\b+Jc\u0010,\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00010\u000e2\u0006\u0010\u0016\u001a\u00020\u00172\u0010\b\u0002\u0010\u001b\u001a\n\u0012\u0004\u0012\u00020\u0005\u0018\u00010\u001c2\n\b\u0002\u0010\u001d\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010-\u001a\u0004\u0018\u00010\u00052\u0016\b\u0002\u0010.\u001a\u0010\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u0001\u0018\u00010\u000eH\u0000¢\u0006\u0002\b/J%\u00100\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00010\u000e2\n\b\u0002\u00101\u001a\u0004\u0018\u00010\u0005H\u0000¢\u0006\u0002\b2J!\u00103\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00010\u000e2\u0006\u0010\u000f\u001a\u00020\u0005H\u0000¢\u0006\u0002\b4J=\u00105\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00010\u000e2\b\u00106\u001a\u0004\u0018\u00010\u00052\b\u00101\u001a\u0004\u0018\u0001072\u000e\u0010\u001b\u001a\n\u0012\u0004\u0012\u00020\u0005\u0018\u00010\u001cH\u0000¢\u0006\u0002\b8J+\u00109\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00010\u000e2\b\u00101\u001a\u0004\u0018\u00010\u00052\u0006\u0010\u000f\u001a\u00020\u0005H\u0000¢\u0006\u0002\b:J!\u0010;\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00010\u000e2\u0006\u0010\u000f\u001a\u00020\u0005H\u0000¢\u0006\u0002\b<J3\u0010=\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00010\u000e2\u0006\u0010\u001d\u001a\u00020\u00052\u0010\b\u0002\u0010\u001b\u001a\n\u0012\u0004\u0012\u00020\u0005\u0018\u00010\u001cH\u0000¢\u0006\u0002\b>J!\u0010?\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00010\u000e2\u0006\u0010&\u001a\u00020\u0005H\u0000¢\u0006\u0002\b@J\u001c\u0010A\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00010\u000e2\u0006\u0010\u0016\u001a\u00020\u0017H\u0002J1\u0010B\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00010\u000e2\u000e\u0010\u001b\u001a\n\u0012\u0004\u0012\u00020\u0005\u0018\u00010\u001c2\u0006\u0010-\u001a\u00020\u0005H\u0000¢\u0006\u0002\bCJ,\u0010D\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00050\u000e2\n\b\u0002\u0010\u001d\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010-\u001a\u0004\u0018\u00010\u0005H\u0002J\u001a\u0010E\u001a\u00020F2\b\u0010\t\u001a\u0004\u0018\u00010\n2\u0006\u0010\u0007\u001a\u00020\bH\u0002R\u0010\u0010\t\u001a\u0004\u0018\u00010\nX\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u0007\u001a\u0004\u0018\u00010\bX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000¨\u0006I"}, d2 = {"Lcom/stripe/android/AnalyticsDataFactory;", "", "context", "Landroid/content/Context;", "publishableKey", "", "(Landroid/content/Context;Ljava/lang/String;)V", "packageManager", "Landroid/content/pm/PackageManager;", "packageInfo", "Landroid/content/pm/PackageInfo;", "packageName", "(Landroid/content/pm/PackageManager;Landroid/content/pm/PackageInfo;Ljava/lang/String;Ljava/lang/String;)V", "create3ds2ChallengeErrorParams", "", "intentId", "protocolErrorEvent", "Lcom/stripe/android/stripe3ds2/transaction/ProtocolErrorEvent;", "create3ds2ChallengeErrorParams$stripe_release", "runtimeErrorEvent", "Lcom/stripe/android/stripe3ds2/transaction/RuntimeErrorEvent;", "create3ds2ChallengeParams", "event", "Lcom/stripe/android/AnalyticsEvent;", "uiTypeCode", "create3ds2ChallengeParams$stripe_release", "createAddSourceParams", "productUsageTokens", "", "sourceType", "createAddSourceParams$stripe_release", "createAppDataParams", "createAppDataParams$stripe_release", "createAttachPaymentMethodParams", "createAttachPaymentMethodParams$stripe_release", "createAuthParams", "createAuthParams$stripe_release", "createAuthSourceParams", "sourceId", "createAuthSourceParams$stripe_release", "createDeleteSourceParams", "createDeleteSourceParams$stripe_release", "createDetachPaymentMethodParams", "createDetachPaymentMethodParams$stripe_release", "createParams", "tokenType", "extraParams", "createParams$stripe_release", "createPaymentIntentConfirmationParams", "paymentMethodType", "createPaymentIntentConfirmationParams$stripe_release", "createPaymentIntentRetrieveParams", "createPaymentIntentRetrieveParams$stripe_release", "createPaymentMethodCreationParams", "paymentMethodId", "Lcom/stripe/android/model/PaymentMethod$Type;", "createPaymentMethodCreationParams$stripe_release", "createSetupIntentConfirmationParams", "createSetupIntentConfirmationParams$stripe_release", "createSetupIntentRetrieveParams", "createSetupIntentRetrieveParams$stripe_release", "createSourceCreationParams", "createSourceCreationParams$stripe_release", "createSourceRetrieveParams", "createSourceRetrieveParams$stripe_release", "createStandardParams", "createTokenCreationParams", "createTokenCreationParams$stripe_release", "createTokenTypeParam", "getAppName", "", "Companion", "ThreeDS2UiType", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: AnalyticsDataFactory.kt */
public final class AnalyticsDataFactory {
    private static final String ANALYTICS_NAME = "stripe_android";
    private static final String ANALYTICS_PREFIX = "analytics";
    public static final String ANALYTICS_UA = "analytics.stripe_android-1.0";
    private static final String ANALYTICS_VERSION = "1.0";
    public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
    private static final String DEVICE_TYPE = (Build.MANUFACTURER + '_' + Build.BRAND + '_' + Build.MODEL);
    public static final String FIELD_3DS2_UI_TYPE = "3ds2_ui_type";
    public static final String FIELD_ANALYTICS_UA = "analytics_ua";
    public static final String FIELD_APP_NAME = "app_name";
    public static final String FIELD_APP_VERSION = "app_version";
    public static final String FIELD_BINDINGS_VERSION = "bindings_version";
    public static final String FIELD_DEVICE_TYPE = "device_type";
    public static final String FIELD_ERROR_DATA = "error";
    public static final String FIELD_EVENT = "event";
    public static final String FIELD_INTENT_ID = "intent_id";
    public static final String FIELD_OS_NAME = "os_name";
    public static final String FIELD_OS_RELEASE = "os_release";
    public static final String FIELD_OS_VERSION = "os_version";
    public static final String FIELD_PAYMENT_METHOD_ID = "payment_method_id";
    public static final String FIELD_PAYMENT_METHOD_TYPE = "payment_method_type";
    public static final String FIELD_PRODUCT_USAGE = "product_usage";
    public static final String FIELD_PUBLISHABLE_KEY = "publishable_key";
    public static final String FIELD_SOURCE_ID = "source_id";
    public static final String FIELD_SOURCE_TYPE = "source_type";
    public static final String FIELD_TOKEN_TYPE = "token_type";
    /* access modifiers changed from: private */
    public static final /* synthetic */ Set<String> VALID_PARAM_FIELDS = SetsKt.setOf(FIELD_ANALYTICS_UA, "app_name", FIELD_APP_VERSION, FIELD_BINDINGS_VERSION, FIELD_DEVICE_TYPE, "event", FIELD_OS_VERSION, FIELD_OS_NAME, FIELD_OS_RELEASE, FIELD_PRODUCT_USAGE, FIELD_PUBLISHABLE_KEY, FIELD_SOURCE_TYPE, FIELD_TOKEN_TYPE);
    private final PackageInfo packageInfo;
    private final PackageManager packageManager;
    private final String packageName;
    private final String publishableKey;

    public AnalyticsDataFactory(PackageManager packageManager2, PackageInfo packageInfo2, String str, String str2) {
        Intrinsics.checkParameterIsNotNull(str, "packageName");
        Intrinsics.checkParameterIsNotNull(str2, "publishableKey");
        this.packageManager = packageManager2;
        this.packageInfo = packageInfo2;
        this.packageName = str;
        this.publishableKey = str2;
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public AnalyticsDataFactory(android.content.Context r5, java.lang.String r6) {
        /*
            r4 = this;
            java.lang.String r0 = "context"
            kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(r5, r0)
            java.lang.String r0 = "publishableKey"
            kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(r6, r0)
            android.content.Context r0 = r5.getApplicationContext()
            java.lang.String r1 = "context.applicationContext"
            kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r0, r1)
            android.content.pm.PackageManager r0 = r0.getPackageManager()
            com.stripe.android.utils.ContextUtils r2 = com.stripe.android.utils.ContextUtils.INSTANCE
            android.content.Context r3 = r5.getApplicationContext()
            kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r3, r1)
            android.content.pm.PackageInfo r2 = r2.getPackageInfo$stripe_release(r3)
            android.content.Context r5 = r5.getApplicationContext()
            kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r5, r1)
            java.lang.String r5 = r5.getPackageName()
            if (r5 == 0) goto L_0x0032
            goto L_0x0034
        L_0x0032:
            java.lang.String r5 = ""
        L_0x0034:
            r4.<init>(r0, r2, r5, r6)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.stripe.android.AnalyticsDataFactory.<init>(android.content.Context, java.lang.String):void");
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u001b\n\u0002\b\u0002\b\u0002\u0018\u0000 \u00022\u00020\u0001:\u0001\u0002B\u0000¨\u0006\u0003"}, d2 = {"Lcom/stripe/android/AnalyticsDataFactory$ThreeDS2UiType;", "", "Companion", "stripe_release"}, k = 1, mv = {1, 1, 16})
    @Retention(AnnotationRetention.SOURCE)
    @java.lang.annotation.Retention(RetentionPolicy.SOURCE)
    /* compiled from: AnalyticsDataFactory.kt */
    private @interface ThreeDS2UiType {
        public static final Companion Companion = Companion.$$INSTANCE;
        public static final String HTML = "html";
        public static final String MULTI_SELECT = "multi_select";
        public static final String NONE = "none";
        public static final String OOB = "oob";
        public static final String SINGLE_SELECT = "single_select";
        public static final String TEXT = "text";

        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0006\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\n"}, d2 = {"Lcom/stripe/android/AnalyticsDataFactory$ThreeDS2UiType$Companion;", "", "()V", "HTML", "", "MULTI_SELECT", "NONE", "OOB", "SINGLE_SELECT", "TEXT", "stripe_release"}, k = 1, mv = {1, 1, 16})
        /* compiled from: AnalyticsDataFactory.kt */
        public static final class Companion {
            static final /* synthetic */ Companion $$INSTANCE = new Companion();
            public static final String HTML = "html";
            public static final String MULTI_SELECT = "multi_select";
            public static final String NONE = "none";
            public static final String OOB = "oob";
            public static final String SINGLE_SELECT = "single_select";
            public static final String TEXT = "text";

            private Companion() {
            }
        }
    }

    public final /* synthetic */ Map<String, Object> createAuthParams$stripe_release(AnalyticsEvent analyticsEvent, String str) {
        Intrinsics.checkParameterIsNotNull(analyticsEvent, "event");
        Intrinsics.checkParameterIsNotNull(str, "intentId");
        return createParams$stripe_release$default(this, analyticsEvent, (Set) null, (String) null, (String) null, Companion.createIntentParam(str), 14, (Object) null);
    }

    public final /* synthetic */ Map<String, Object> createAuthSourceParams$stripe_release(AnalyticsEvent analyticsEvent, String str) {
        Intrinsics.checkParameterIsNotNull(analyticsEvent, "event");
        return createParams$stripe_release$default(this, analyticsEvent, (Set) null, (String) null, (String) null, str != null ? MapsKt.mapOf(TuplesKt.to(FIELD_SOURCE_ID, str)) : null, 14, (Object) null);
    }

    public final /* synthetic */ Map<String, Object> create3ds2ChallengeParams$stripe_release(AnalyticsEvent analyticsEvent, String str, String str2) {
        Intrinsics.checkParameterIsNotNull(analyticsEvent, "event");
        Intrinsics.checkParameterIsNotNull(str, "intentId");
        Intrinsics.checkParameterIsNotNull(str2, "uiTypeCode");
        return createParams$stripe_release$default(this, analyticsEvent, (Set) null, (String) null, (String) null, MapsKt.plus(Companion.createIntentParam(str), TuplesKt.to(FIELD_3DS2_UI_TYPE, Companion.get3ds2UiType(str2))), 14, (Object) null);
    }

    public final /* synthetic */ Map<String, Object> create3ds2ChallengeErrorParams$stripe_release(String str, RuntimeErrorEvent runtimeErrorEvent) {
        Intrinsics.checkParameterIsNotNull(str, "intentId");
        Intrinsics.checkParameterIsNotNull(runtimeErrorEvent, "runtimeErrorEvent");
        return createParams$stripe_release$default(this, AnalyticsEvent.Auth3ds2ChallengeErrored, (Set) null, (String) null, (String) null, MapsKt.plus(Companion.createIntentParam(str), TuplesKt.to("error", MapsKt.mapOf(TuplesKt.to("type", "runtime_error_event"), TuplesKt.to(NativeProtocol.BRIDGE_ARG_ERROR_CODE, runtimeErrorEvent.getErrorCode()), TuplesKt.to(AnalyticsEvents.PARAMETER_SHARE_ERROR_MESSAGE, runtimeErrorEvent.getErrorMessage())))), 14, (Object) null);
    }

    public final /* synthetic */ Map<String, Object> create3ds2ChallengeErrorParams$stripe_release(String str, ProtocolErrorEvent protocolErrorEvent) {
        Intrinsics.checkParameterIsNotNull(str, "intentId");
        Intrinsics.checkParameterIsNotNull(protocolErrorEvent, "protocolErrorEvent");
        ErrorMessage errorMessage = protocolErrorEvent.getErrorMessage();
        Map mapOf = MapsKt.mapOf(TuplesKt.to("type", "protocol_error_event"), TuplesKt.to("sdk_trans_id", protocolErrorEvent.getSdkTransactionId()), TuplesKt.to(NativeProtocol.BRIDGE_ARG_ERROR_CODE, errorMessage.getErrorCode()), TuplesKt.to(NativeProtocol.BRIDGE_ARG_ERROR_DESCRIPTION, errorMessage.getErrorDescription()), TuplesKt.to("error_details", errorMessage.getErrorDetails()), TuplesKt.to("trans_id", errorMessage.getTransactionId()));
        return createParams$stripe_release$default(this, AnalyticsEvent.Auth3ds2ChallengeErrored, (Set) null, (String) null, (String) null, MapsKt.plus(Companion.createIntentParam(str), TuplesKt.to("error", mapOf)), 14, (Object) null);
    }

    public final /* synthetic */ Map<String, Object> createTokenCreationParams$stripe_release(Set<String> set, String str) {
        Intrinsics.checkParameterIsNotNull(str, "tokenType");
        return createParams$stripe_release$default(this, AnalyticsEvent.TokenCreate, set, (String) null, str, (Map) null, 20, (Object) null);
    }

    public final /* synthetic */ Map<String, Object> createPaymentMethodCreationParams$stripe_release(String str, PaymentMethod.Type type, Set<String> set) {
        return createParams$stripe_release$default(this, AnalyticsEvent.PaymentMethodCreate, set, type != null ? type.code : null, (String) null, str != null ? MapsKt.mapOf(TuplesKt.to(FIELD_PAYMENT_METHOD_ID, str)) : null, 8, (Object) null);
    }

    public static /* synthetic */ Map createSourceCreationParams$stripe_release$default(AnalyticsDataFactory analyticsDataFactory, String str, Set set, int i, Object obj) {
        if ((i & 2) != 0) {
            set = null;
        }
        return analyticsDataFactory.createSourceCreationParams$stripe_release(str, set);
    }

    public final /* synthetic */ Map<String, Object> createSourceCreationParams$stripe_release(String str, Set<String> set) {
        Intrinsics.checkParameterIsNotNull(str, "sourceType");
        return createParams$stripe_release$default(this, AnalyticsEvent.SourceCreate, set, str, (String) null, (Map) null, 24, (Object) null);
    }

    public final /* synthetic */ Map<String, Object> createSourceRetrieveParams$stripe_release(String str) {
        Intrinsics.checkParameterIsNotNull(str, "sourceId");
        return createParams$stripe_release$default(this, AnalyticsEvent.SourceRetrieve, (Set) null, (String) null, (String) null, MapsKt.mapOf(TuplesKt.to(FIELD_SOURCE_ID, str)), 14, (Object) null);
    }

    public static /* synthetic */ Map createAddSourceParams$stripe_release$default(AnalyticsDataFactory analyticsDataFactory, Set set, String str, int i, Object obj) {
        if ((i & 1) != 0) {
            set = null;
        }
        return analyticsDataFactory.createAddSourceParams$stripe_release(set, str);
    }

    public final /* synthetic */ Map<String, Object> createAddSourceParams$stripe_release(Set<String> set, String str) {
        Intrinsics.checkParameterIsNotNull(str, "sourceType");
        return createParams$stripe_release$default(this, AnalyticsEvent.CustomerAddSource, set, str, (String) null, (Map) null, 24, (Object) null);
    }

    public final /* synthetic */ Map<String, Object> createDeleteSourceParams$stripe_release(Set<String> set) {
        return createParams$stripe_release$default(this, AnalyticsEvent.CustomerDeleteSource, set, (String) null, (String) null, (Map) null, 28, (Object) null);
    }

    public final /* synthetic */ Map<String, Object> createAttachPaymentMethodParams$stripe_release(Set<String> set) {
        return createParams$stripe_release$default(this, AnalyticsEvent.CustomerAttachPaymentMethod, set, (String) null, (String) null, (Map) null, 28, (Object) null);
    }

    public final /* synthetic */ Map<String, Object> createDetachPaymentMethodParams$stripe_release(Set<String> set) {
        return createParams$stripe_release$default(this, AnalyticsEvent.CustomerDetachPaymentMethod, set, (String) null, (String) null, (Map) null, 28, (Object) null);
    }

    public static /* synthetic */ Map createPaymentIntentConfirmationParams$stripe_release$default(AnalyticsDataFactory analyticsDataFactory, String str, int i, Object obj) {
        if ((i & 1) != 0) {
            str = null;
        }
        return analyticsDataFactory.createPaymentIntentConfirmationParams$stripe_release(str);
    }

    public final /* synthetic */ Map<String, Object> createPaymentIntentConfirmationParams$stripe_release(String str) {
        return createParams$stripe_release$default(this, AnalyticsEvent.PaymentIntentConfirm, (Set) null, str, (String) null, (Map) null, 26, (Object) null);
    }

    public final /* synthetic */ Map<String, Object> createPaymentIntentRetrieveParams$stripe_release(String str) {
        Intrinsics.checkParameterIsNotNull(str, "intentId");
        return createParams$stripe_release$default(this, AnalyticsEvent.PaymentIntentRetrieve, (Set) null, (String) null, (String) null, Companion.createIntentParam(str), 14, (Object) null);
    }

    public final /* synthetic */ Map<String, Object> createSetupIntentConfirmationParams$stripe_release(String str, String str2) {
        Intrinsics.checkParameterIsNotNull(str2, "intentId");
        AnalyticsEvent analyticsEvent = AnalyticsEvent.SetupIntentConfirm;
        Map access$createIntentParam = Companion.createIntentParam(str2);
        Map mapOf = str != null ? MapsKt.mapOf(TuplesKt.to(FIELD_PAYMENT_METHOD_TYPE, str)) : null;
        if (mapOf == null) {
            mapOf = MapsKt.emptyMap();
        }
        return createParams$stripe_release$default(this, analyticsEvent, (Set) null, (String) null, (String) null, MapsKt.plus(access$createIntentParam, mapOf), 14, (Object) null);
    }

    public final /* synthetic */ Map<String, Object> createSetupIntentRetrieveParams$stripe_release(String str) {
        Intrinsics.checkParameterIsNotNull(str, "intentId");
        return createParams$stripe_release$default(this, AnalyticsEvent.SetupIntentRetrieve, (Set) null, (String) null, (String) null, Companion.createIntentParam(str), 14, (Object) null);
    }

    public static /* synthetic */ Map createParams$stripe_release$default(AnalyticsDataFactory analyticsDataFactory, AnalyticsEvent analyticsEvent, Set set, String str, String str2, Map map, int i, Object obj) {
        if ((i & 2) != 0) {
            set = null;
        }
        Set set2 = set;
        if ((i & 4) != 0) {
            str = null;
        }
        String str3 = str;
        if ((i & 8) != 0) {
            str2 = null;
        }
        String str4 = str2;
        if ((i & 16) != 0) {
            map = null;
        }
        return analyticsDataFactory.createParams$stripe_release(analyticsEvent, set2, str3, str4, map);
    }

    public final /* synthetic */ Map<String, Object> createParams$stripe_release(AnalyticsEvent analyticsEvent, Set<String> set, String str, String str2, Map<String, ? extends Object> map) {
        Intrinsics.checkParameterIsNotNull(analyticsEvent, "event");
        Map<String, Object> plus = MapsKt.plus(createStandardParams(analyticsEvent), createAppDataParams$stripe_release());
        Collection collection = set;
        Map map2 = null;
        if (collection == null || collection.isEmpty()) {
            set = null;
        }
        Map mapOf = set != null ? MapsKt.mapOf(TuplesKt.to(FIELD_PRODUCT_USAGE, CollectionsKt.toList(set))) : null;
        if (mapOf == null) {
            mapOf = MapsKt.emptyMap();
        }
        Map<String, Object> plus2 = MapsKt.plus(plus, (Map<String, Object>) mapOf);
        if (str != null) {
            map2 = MapsKt.mapOf(TuplesKt.to(FIELD_SOURCE_TYPE, str));
        }
        if (map2 == null) {
            map2 = MapsKt.emptyMap();
        }
        Map<String, String> plus3 = MapsKt.plus(MapsKt.plus(plus2, (Map<String, Object>) map2), (Map<String, Object>) createTokenTypeParam(str, str2));
        if (map == null) {
            map = MapsKt.emptyMap();
        }
        return MapsKt.plus(plus3, (Map<String, String>) map);
    }

    static /* synthetic */ Map createTokenTypeParam$default(AnalyticsDataFactory analyticsDataFactory, String str, String str2, int i, Object obj) {
        if ((i & 1) != 0) {
            str = null;
        }
        if ((i & 2) != 0) {
            str2 = null;
        }
        return analyticsDataFactory.createTokenTypeParam(str, str2);
    }

    private final Map<String, String> createTokenTypeParam(String str, String str2) {
        Map<String, String> map = null;
        if (str2 == null) {
            str2 = str == null ? "unknown" : null;
        }
        if (str2 != null) {
            map = MapsKt.mapOf(TuplesKt.to(FIELD_TOKEN_TYPE, str2));
        }
        return map != null ? map : MapsKt.emptyMap();
    }

    private final Map<String, Object> createStandardParams(AnalyticsEvent analyticsEvent) {
        return MapsKt.mapOf(TuplesKt.to(FIELD_ANALYTICS_UA, ANALYTICS_UA), TuplesKt.to("event", analyticsEvent.toString()), TuplesKt.to(FIELD_PUBLISHABLE_KEY, this.publishableKey), TuplesKt.to(FIELD_OS_NAME, Build.VERSION.CODENAME), TuplesKt.to(FIELD_OS_RELEASE, Build.VERSION.RELEASE), TuplesKt.to(FIELD_OS_VERSION, Integer.valueOf(Build.VERSION.SDK_INT)), TuplesKt.to(FIELD_DEVICE_TYPE, DEVICE_TYPE), TuplesKt.to(FIELD_BINDINGS_VERSION, BuildConfig.VERSION_NAME));
    }

    public final Map<String, Object> createAppDataParams$stripe_release() {
        PackageInfo packageInfo2;
        PackageManager packageManager2 = this.packageManager;
        if (packageManager2 == null || (packageInfo2 = this.packageInfo) == null) {
            return MapsKt.emptyMap();
        }
        return MapsKt.mapOf(TuplesKt.to("app_name", getAppName(packageInfo2, packageManager2)), TuplesKt.to(FIELD_APP_VERSION, Integer.valueOf(this.packageInfo.versionCode)));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:2:0x0003, code lost:
        r2 = r2.applicationInfo;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final java.lang.CharSequence getAppName(android.content.pm.PackageInfo r2, android.content.pm.PackageManager r3) {
        /*
            r1 = this;
            r0 = 0
            if (r2 == 0) goto L_0x000c
            android.content.pm.ApplicationInfo r2 = r2.applicationInfo
            if (r2 == 0) goto L_0x000c
            java.lang.CharSequence r2 = r2.loadLabel(r3)
            goto L_0x000d
        L_0x000c:
            r2 = r0
        L_0x000d:
            if (r2 == 0) goto L_0x0018
            boolean r3 = kotlin.text.StringsKt.isBlank(r2)
            if (r3 == 0) goto L_0x0016
            goto L_0x0018
        L_0x0016:
            r3 = 0
            goto L_0x0019
        L_0x0018:
            r3 = 1
        L_0x0019:
            if (r3 != 0) goto L_0x001c
            r0 = r2
        L_0x001c:
            if (r0 == 0) goto L_0x001f
            goto L_0x0024
        L_0x001f:
            java.lang.String r2 = r1.packageName
            r0 = r2
            java.lang.CharSequence r0 = (java.lang.CharSequence) r0
        L_0x0024:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.stripe.android.AnalyticsDataFactory.getAppName(android.content.pm.PackageInfo, android.content.pm.PackageManager):java.lang.CharSequence");
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0018\n\u0002\u0010\"\n\u0002\b\u0003\n\u0002\u0010$\n\u0002\b\u0004\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u001c\u0010 \u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00040!2\u0006\u0010\"\u001a\u00020\u0004H\u0002J\u0010\u0010#\u001a\u00020\u00042\u0006\u0010$\u001a\u00020\u0004H\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0004X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0017\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0018\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0019\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u001a\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u001b\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u001a\u0010\u001c\u001a\b\u0012\u0004\u0012\u00020\u00040\u001dX\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u001e\u0010\u001f¨\u0006%"}, d2 = {"Lcom/stripe/android/AnalyticsDataFactory$Companion;", "", "()V", "ANALYTICS_NAME", "", "ANALYTICS_PREFIX", "ANALYTICS_UA", "ANALYTICS_VERSION", "DEVICE_TYPE", "FIELD_3DS2_UI_TYPE", "FIELD_ANALYTICS_UA", "FIELD_APP_NAME", "FIELD_APP_VERSION", "FIELD_BINDINGS_VERSION", "FIELD_DEVICE_TYPE", "FIELD_ERROR_DATA", "FIELD_EVENT", "FIELD_INTENT_ID", "FIELD_OS_NAME", "FIELD_OS_RELEASE", "FIELD_OS_VERSION", "FIELD_PAYMENT_METHOD_ID", "FIELD_PAYMENT_METHOD_TYPE", "FIELD_PRODUCT_USAGE", "FIELD_PUBLISHABLE_KEY", "FIELD_SOURCE_ID", "FIELD_SOURCE_TYPE", "FIELD_TOKEN_TYPE", "VALID_PARAM_FIELDS", "", "getVALID_PARAM_FIELDS$stripe_release", "()Ljava/util/Set;", "createIntentParam", "", "intentId", "get3ds2UiType", "uiTypeCode", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: AnalyticsDataFactory.kt */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        public final Set<String> getVALID_PARAM_FIELDS$stripe_release() {
            return AnalyticsDataFactory.VALID_PARAM_FIELDS;
        }

        /* access modifiers changed from: private */
        public final String get3ds2UiType(String str) {
            switch (str.hashCode()) {
                case 1537:
                    if (str.equals("01")) {
                        return "text";
                    }
                    break;
                case 1538:
                    if (str.equals("02")) {
                        return "single_select";
                    }
                    break;
                case 1539:
                    if (str.equals("03")) {
                        return "multi_select";
                    }
                    break;
                case 1540:
                    if (str.equals("04")) {
                        return "oob";
                    }
                    break;
                case 1541:
                    if (str.equals("05")) {
                        return "html";
                    }
                    break;
            }
            return "none";
        }

        /* access modifiers changed from: private */
        public final Map<String, String> createIntentParam(String str) {
            return MapsKt.mapOf(TuplesKt.to(AnalyticsDataFactory.FIELD_INTENT_ID, str));
        }
    }
}
