package com.stripe.android;

import android.content.Context;
import java.util.Map;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\b\u0000\u0018\u00002\u0010\u0012\u0006\u0012\u0004\u0018\u00010\u0002\u0012\u0004\u0012\u00020\u00030\u0001B\u000f\b\u0010\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006B\u000f\b\u0001\u0012\u0006\u0010\u0007\u001a\u00020\b¢\u0006\u0002\u0010\tJ\u0012\u0010\n\u001a\u00020\u00032\b\u0010\u000b\u001a\u0004\u0018\u00010\u0002H\u0016R\u000e\u0010\u0007\u001a\u00020\bX\u0004¢\u0006\u0002\n\u0000¨\u0006\f"}, d2 = {"Lcom/stripe/android/FingerprintRequestFactory;", "Lcom/stripe/android/Factory1;", "", "Lcom/stripe/android/FingerprintRequest;", "context", "Landroid/content/Context;", "(Landroid/content/Context;)V", "fingerprintRequestParamsFactory", "Lcom/stripe/android/FingerprintRequestParamsFactory;", "(Lcom/stripe/android/FingerprintRequestParamsFactory;)V", "create", "arg", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: FingerprintRequestFactory.kt */
public final class FingerprintRequestFactory implements Factory1<String, FingerprintRequest> {
    private final FingerprintRequestParamsFactory fingerprintRequestParamsFactory;

    public FingerprintRequestFactory(FingerprintRequestParamsFactory fingerprintRequestParamsFactory2) {
        Intrinsics.checkParameterIsNotNull(fingerprintRequestParamsFactory2, "fingerprintRequestParamsFactory");
        this.fingerprintRequestParamsFactory = fingerprintRequestParamsFactory2;
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public FingerprintRequestFactory(Context context) {
        this(new FingerprintRequestParamsFactory(context));
        Intrinsics.checkParameterIsNotNull(context, "context");
    }

    public FingerprintRequest create(String str) {
        Map<String, Object> createParams$stripe_release = this.fingerprintRequestParamsFactory.createParams$stripe_release();
        if (str == null) {
            str = "";
        }
        return new FingerprintRequest(createParams$stripe_release, str);
    }
}
