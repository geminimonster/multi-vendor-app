package com.stripe.android;

import java.util.Calendar;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0000\n\u0002\u0010\t\n\u0000\u0010\u0000\u001a\u00020\u0001H\n¢\u0006\u0002\b\u0002"}, d2 = {"<anonymous>", "", "invoke"}, k = 3, mv = {1, 1, 16})
/* compiled from: FingerprintRequestExecutor.kt */
final class FingerprintRequestExecutor$Default$timestampSupplier$1 extends Lambda implements Function0<Long> {
    public static final FingerprintRequestExecutor$Default$timestampSupplier$1 INSTANCE = new FingerprintRequestExecutor$Default$timestampSupplier$1();

    FingerprintRequestExecutor$Default$timestampSupplier$1() {
        super(0);
    }

    public final long invoke() {
        Calendar instance = Calendar.getInstance();
        Intrinsics.checkExpressionValueIsNotNull(instance, "Calendar.getInstance()");
        return instance.getTimeInMillis();
    }
}
