package com.stripe.android;

import com.stripe.android.AnalyticsRequestExecutor;
import kotlin.Metadata;
import kotlin.Result;
import kotlin.ResultKt;
import kotlin.Unit;
import kotlin.coroutines.Continuation;
import kotlin.coroutines.intrinsics.IntrinsicsKt;
import kotlin.coroutines.jvm.internal.Boxing;
import kotlin.coroutines.jvm.internal.DebugMetadata;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.Intrinsics;
import kotlinx.coroutines.CoroutineScope;
import kotlinx.coroutines.CoroutineScopeKt;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\u0010\u0000\u001a\u00020\u0001*\u00020\u0002H@¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"<anonymous>", "", "Lkotlinx/coroutines/CoroutineScope;", "invoke", "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;"}, k = 3, mv = {1, 1, 16})
@DebugMetadata(c = "com.stripe.android.AnalyticsRequestExecutor$Default$executeAsync$1", f = "AnalyticsRequestExecutor.kt", i = {0, 0}, l = {47}, m = "invokeSuspend", n = {"$this$launch", "$this$runCatching"}, s = {"L$0", "L$1"})
/* compiled from: AnalyticsRequestExecutor.kt */
final class AnalyticsRequestExecutor$Default$executeAsync$1 extends SuspendLambda implements Function2<CoroutineScope, Continuation<? super Unit>, Object> {
    final /* synthetic */ AnalyticsRequest $request;
    Object L$0;
    Object L$1;
    int label;
    private CoroutineScope p$;
    final /* synthetic */ AnalyticsRequestExecutor.Default this$0;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    AnalyticsRequestExecutor$Default$executeAsync$1(AnalyticsRequestExecutor.Default defaultR, AnalyticsRequest analyticsRequest, Continuation continuation) {
        super(2, continuation);
        this.this$0 = defaultR;
        this.$request = analyticsRequest;
    }

    public final Continuation<Unit> create(Object obj, Continuation<?> continuation) {
        Intrinsics.checkParameterIsNotNull(continuation, "completion");
        AnalyticsRequestExecutor$Default$executeAsync$1 analyticsRequestExecutor$Default$executeAsync$1 = new AnalyticsRequestExecutor$Default$executeAsync$1(this.this$0, this.$request, continuation);
        analyticsRequestExecutor$Default$executeAsync$1.p$ = (CoroutineScope) obj;
        return analyticsRequestExecutor$Default$executeAsync$1;
    }

    public final Object invoke(Object obj, Object obj2) {
        return ((AnalyticsRequestExecutor$Default$executeAsync$1) create(obj, (Continuation) obj2)).invokeSuspend(Unit.INSTANCE);
    }

    public final Object invokeSuspend(Object obj) {
        Object obj2;
        Object coroutine_suspended = IntrinsicsKt.getCOROUTINE_SUSPENDED();
        int i = this.label;
        if (i == 0) {
            ResultKt.throwOnFailure(obj);
            CoroutineScope coroutineScope = this.p$;
            Result.Companion companion = Result.Companion;
            this.L$0 = coroutineScope;
            this.L$1 = coroutineScope;
            this.label = 1;
            obj = CoroutineScopeKt.coroutineScope(new AnalyticsRequestExecutor$Default$executeAsync$1$invokeSuspend$$inlined$runCatching$lambda$1((Continuation) null, this), this);
            if (obj == coroutine_suspended) {
                return coroutine_suspended;
            }
        } else if (i == 1) {
            CoroutineScope coroutineScope2 = (CoroutineScope) this.L$1;
            CoroutineScope coroutineScope3 = (CoroutineScope) this.L$0;
            try {
                ResultKt.throwOnFailure(obj);
            } catch (Throwable th) {
                Result.Companion companion2 = Result.Companion;
                obj2 = Result.m4constructorimpl(ResultKt.createFailure(th));
            }
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        obj2 = Result.m4constructorimpl(Boxing.boxInt(((Number) obj).intValue()));
        Throwable r5 = Result.m7exceptionOrNullimpl(obj2);
        if (r5 != null) {
            Result.Companion companion3 = Result.Companion;
            this.this$0.logger.error("Exception while making analytics request", r5);
            Result.m4constructorimpl(Unit.INSTANCE);
        }
        return Unit.INSTANCE;
    }
}
