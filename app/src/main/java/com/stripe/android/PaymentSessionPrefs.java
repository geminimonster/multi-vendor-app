package com.stripe.android;

import android.content.Context;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\b`\u0018\u0000 \b2\u00020\u0001:\u0001\bJ\u0014\u0010\u0002\u001a\u0004\u0018\u00010\u00032\b\u0010\u0004\u001a\u0004\u0018\u00010\u0003H&J\u001a\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0004\u001a\u00020\u00032\b\u0010\u0007\u001a\u0004\u0018\u00010\u0003H&¨\u0006\t"}, d2 = {"Lcom/stripe/android/PaymentSessionPrefs;", "", "getPaymentMethodId", "", "customerId", "savePaymentMethodId", "", "paymentMethodId", "Companion", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: PaymentSessionPrefs.kt */
public interface PaymentSessionPrefs {
    public static final Companion Companion = Companion.$$INSTANCE;

    String getPaymentMethodId(String str);

    void savePaymentMethodId(String str, String str2);

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0015\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bH\u0000¢\u0006\u0002\b\tJ\u0012\u0010\n\u001a\u00020\u00042\b\u0010\u000b\u001a\u0004\u0018\u00010\u0004H\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\f"}, d2 = {"Lcom/stripe/android/PaymentSessionPrefs$Companion;", "", "()V", "PREF_FILE", "", "create", "Lcom/stripe/android/PaymentSessionPrefs;", "context", "Landroid/content/Context;", "create$stripe_release", "getPaymentMethodKey", "customerId", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: PaymentSessionPrefs.kt */
    public static final class Companion {
        static final /* synthetic */ Companion $$INSTANCE = new Companion();
        private static final String PREF_FILE = "PaymentSessionPrefs";

        private Companion() {
        }

        /* access modifiers changed from: private */
        public final String getPaymentMethodKey(String str) {
            return "customer[" + str + "].payment_method";
        }

        public final /* synthetic */ PaymentSessionPrefs create$stripe_release(Context context) {
            Intrinsics.checkParameterIsNotNull(context, "context");
            return new PaymentSessionPrefs$Companion$create$1(this, context.getSharedPreferences(PREF_FILE, 0));
        }
    }
}
