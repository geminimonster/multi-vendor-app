package com.stripe.android;

import android.content.Context;
import android.content.SharedPreferences;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.stripe.android.FingerprintData;
import kotlin.Metadata;
import kotlin.Result;
import kotlin.ResultKt;
import kotlin.jvm.internal.Intrinsics;
import org.json.JSONObject;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\b`\u0018\u00002\u00020\u0001:\u0001\bJ\u000e\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003H&J\u0010\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0004H&¨\u0006\t"}, d2 = {"Lcom/stripe/android/FingerprintDataStore;", "", "get", "Landroidx/lifecycle/LiveData;", "Lcom/stripe/android/FingerprintData;", "save", "", "fingerprintData", "Default", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: FingerprintDataStore.kt */
public interface FingerprintDataStore {
    LiveData<FingerprintData> get();

    void save(FingerprintData fingerprintData);

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0018\u0000 \u000e2\u00020\u0001:\u0001\u000eB\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u000e\u0010\b\u001a\b\u0012\u0004\u0012\u00020\n0\tH\u0016J\u0010\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\nH\u0016R\u0016\u0010\u0005\u001a\n \u0007*\u0004\u0018\u00010\u00060\u0006X\u0004¢\u0006\u0002\n\u0000¨\u0006\u000f"}, d2 = {"Lcom/stripe/android/FingerprintDataStore$Default;", "Lcom/stripe/android/FingerprintDataStore;", "context", "Landroid/content/Context;", "(Landroid/content/Context;)V", "prefs", "Landroid/content/SharedPreferences;", "kotlin.jvm.PlatformType", "get", "Landroidx/lifecycle/LiveData;", "Lcom/stripe/android/FingerprintData;", "save", "", "fingerprintData", "Companion", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: FingerprintDataStore.kt */
    public static final class Default implements FingerprintDataStore {
        @Deprecated
        public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
        private static final String KEY_DATA = "key_fingerprint_data";
        private static final String PREF_FILE = "FingerprintDataRepository";
        private final SharedPreferences prefs;

        public Default(Context context) {
            Intrinsics.checkParameterIsNotNull(context, "context");
            this.prefs = context.getSharedPreferences(PREF_FILE, 0);
        }

        public LiveData<FingerprintData> get() {
            FingerprintData fingerprintData;
            try {
                Result.Companion companion = Result.Companion;
                FingerprintData.Companion companion2 = FingerprintData.Companion;
                String string = this.prefs.getString(KEY_DATA, (String) null);
                if (string == null) {
                    string = "";
                }
                fingerprintData = Result.m4constructorimpl(companion2.fromJson(new JSONObject(string)));
            } catch (Throwable th) {
                Result.Companion companion3 = Result.Companion;
                fingerprintData = Result.m4constructorimpl(ResultKt.createFailure(th));
            }
            FingerprintData fingerprintData2 = new FingerprintData((String) null, 0, 3, (DefaultConstructorMarker) null);
            if (Result.m10isFailureimpl(fingerprintData)) {
                fingerprintData = fingerprintData2;
            }
            return new MutableLiveData<>(fingerprintData);
        }

        public void save(FingerprintData fingerprintData) {
            Intrinsics.checkParameterIsNotNull(fingerprintData, "fingerprintData");
            this.prefs.edit().putString(KEY_DATA, fingerprintData.toJson().toString()).apply();
        }

        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\u0006"}, d2 = {"Lcom/stripe/android/FingerprintDataStore$Default$Companion;", "", "()V", "KEY_DATA", "", "PREF_FILE", "stripe_release"}, k = 1, mv = {1, 1, 16})
        /* compiled from: FingerprintDataStore.kt */
        private static final class Companion {
            private Companion() {
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }
        }
    }
}
