package com.stripe.android.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import androidx.viewbinding.ViewBinding;
import com.google.android.material.textfield.TextInputLayout;
import com.stripe.android.R;

public final class CountryAutocompleteViewBinding implements ViewBinding {
    public final AutoCompleteTextView countryAutocomplete;
    public final TextInputLayout countryTextInputLayout;
    private final View rootView;

    private CountryAutocompleteViewBinding(View view, AutoCompleteTextView autoCompleteTextView, TextInputLayout textInputLayout) {
        this.rootView = view;
        this.countryAutocomplete = autoCompleteTextView;
        this.countryTextInputLayout = textInputLayout;
    }

    public View getRoot() {
        return this.rootView;
    }

    public static CountryAutocompleteViewBinding inflate(LayoutInflater layoutInflater, ViewGroup viewGroup) {
        if (viewGroup != null) {
            layoutInflater.inflate(R.layout.country_autocomplete_view, viewGroup);
            return bind(viewGroup);
        }
        throw new NullPointerException("parent");
    }

    public static CountryAutocompleteViewBinding bind(View view) {
        int i = R.id.country_autocomplete;
        AutoCompleteTextView autoCompleteTextView = (AutoCompleteTextView) view.findViewById(i);
        if (autoCompleteTextView != null) {
            i = R.id.country_text_input_layout;
            TextInputLayout textInputLayout = (TextInputLayout) view.findViewById(i);
            if (textInputLayout != null) {
                return new CountryAutocompleteViewBinding(view, autoCompleteTextView, textInputLayout);
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }
}
