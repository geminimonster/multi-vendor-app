package com.stripe.android.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.viewbinding.ViewBinding;
import com.stripe.android.R;

public final class CardBrandViewBinding implements ViewBinding {
    public final ImageView icon;
    private final View rootView;

    private CardBrandViewBinding(View view, ImageView imageView) {
        this.rootView = view;
        this.icon = imageView;
    }

    public View getRoot() {
        return this.rootView;
    }

    public static CardBrandViewBinding inflate(LayoutInflater layoutInflater, ViewGroup viewGroup) {
        if (viewGroup != null) {
            layoutInflater.inflate(R.layout.card_brand_view, viewGroup);
            return bind(viewGroup);
        }
        throw new NullPointerException("parent");
    }

    public static CardBrandViewBinding bind(View view) {
        int i = R.id.icon;
        ImageView imageView = (ImageView) view.findViewById(i);
        if (imageView != null) {
            return new CardBrandViewBinding(view, imageView);
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }
}
