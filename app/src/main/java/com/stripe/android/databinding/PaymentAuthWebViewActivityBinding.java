package com.stripe.android.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import androidx.appcompat.widget.Toolbar;
import androidx.viewbinding.ViewBinding;
import com.stripe.android.R;
import com.stripe.android.view.PaymentAuthWebView;

public final class PaymentAuthWebViewActivityBinding implements ViewBinding {
    public final PaymentAuthWebView authWebView;
    public final FrameLayout authWebViewContainer;
    public final ProgressBar authWebViewProgressBar;
    private final RelativeLayout rootView;
    public final Toolbar toolbar;

    private PaymentAuthWebViewActivityBinding(RelativeLayout relativeLayout, PaymentAuthWebView paymentAuthWebView, FrameLayout frameLayout, ProgressBar progressBar, Toolbar toolbar2) {
        this.rootView = relativeLayout;
        this.authWebView = paymentAuthWebView;
        this.authWebViewContainer = frameLayout;
        this.authWebViewProgressBar = progressBar;
        this.toolbar = toolbar2;
    }

    public RelativeLayout getRoot() {
        return this.rootView;
    }

    public static PaymentAuthWebViewActivityBinding inflate(LayoutInflater layoutInflater) {
        return inflate(layoutInflater, (ViewGroup) null, false);
    }

    public static PaymentAuthWebViewActivityBinding inflate(LayoutInflater layoutInflater, ViewGroup viewGroup, boolean z) {
        View inflate = layoutInflater.inflate(R.layout.payment_auth_web_view_activity, viewGroup, false);
        if (z) {
            viewGroup.addView(inflate);
        }
        return bind(inflate);
    }

    public static PaymentAuthWebViewActivityBinding bind(View view) {
        int i = R.id.auth_web_view;
        PaymentAuthWebView paymentAuthWebView = (PaymentAuthWebView) view.findViewById(i);
        if (paymentAuthWebView != null) {
            i = R.id.auth_web_view_container;
            FrameLayout frameLayout = (FrameLayout) view.findViewById(i);
            if (frameLayout != null) {
                i = R.id.auth_web_view_progress_bar;
                ProgressBar progressBar = (ProgressBar) view.findViewById(i);
                if (progressBar != null) {
                    i = R.id.toolbar;
                    Toolbar toolbar2 = (Toolbar) view.findViewById(i);
                    if (toolbar2 != null) {
                        return new PaymentAuthWebViewActivityBinding((RelativeLayout) view, paymentAuthWebView, frameLayout, progressBar, toolbar2);
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }
}
