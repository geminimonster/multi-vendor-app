package com.stripe.android.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.viewbinding.ViewBinding;
import com.google.android.material.textfield.TextInputLayout;
import com.stripe.android.R;
import com.stripe.android.view.BecsDebitAccountNumberEditText;
import com.stripe.android.view.BecsDebitBsbEditText;
import com.stripe.android.view.BecsDebitMandateAcceptanceTextView;
import com.stripe.android.view.EmailEditText;
import com.stripe.android.view.IconTextInputLayout;
import com.stripe.android.view.StripeEditText;

public final class BecsDebitWidgetBinding implements ViewBinding {
    public final BecsDebitAccountNumberEditText accountNumberEditText;
    public final TextInputLayout accountNumberTextInputLayout;
    public final BecsDebitBsbEditText bsbEditText;
    public final IconTextInputLayout bsbTextInputLayout;
    public final EmailEditText emailEditText;
    public final TextInputLayout emailTextInputLayout;
    public final BecsDebitMandateAcceptanceTextView mandateAcceptanceTextView;
    public final StripeEditText nameEditText;
    public final TextInputLayout nameTextInputLayout;
    private final View rootView;

    private BecsDebitWidgetBinding(View view, BecsDebitAccountNumberEditText becsDebitAccountNumberEditText, TextInputLayout textInputLayout, BecsDebitBsbEditText becsDebitBsbEditText, IconTextInputLayout iconTextInputLayout, EmailEditText emailEditText2, TextInputLayout textInputLayout2, BecsDebitMandateAcceptanceTextView becsDebitMandateAcceptanceTextView, StripeEditText stripeEditText, TextInputLayout textInputLayout3) {
        this.rootView = view;
        this.accountNumberEditText = becsDebitAccountNumberEditText;
        this.accountNumberTextInputLayout = textInputLayout;
        this.bsbEditText = becsDebitBsbEditText;
        this.bsbTextInputLayout = iconTextInputLayout;
        this.emailEditText = emailEditText2;
        this.emailTextInputLayout = textInputLayout2;
        this.mandateAcceptanceTextView = becsDebitMandateAcceptanceTextView;
        this.nameEditText = stripeEditText;
        this.nameTextInputLayout = textInputLayout3;
    }

    public View getRoot() {
        return this.rootView;
    }

    public static BecsDebitWidgetBinding inflate(LayoutInflater layoutInflater, ViewGroup viewGroup) {
        if (viewGroup != null) {
            layoutInflater.inflate(R.layout.becs_debit_widget, viewGroup);
            return bind(viewGroup);
        }
        throw new NullPointerException("parent");
    }

    public static BecsDebitWidgetBinding bind(View view) {
        int i = R.id.account_number_edit_text;
        BecsDebitAccountNumberEditText becsDebitAccountNumberEditText = (BecsDebitAccountNumberEditText) view.findViewById(i);
        if (becsDebitAccountNumberEditText != null) {
            i = R.id.account_number_text_input_layout;
            TextInputLayout textInputLayout = (TextInputLayout) view.findViewById(i);
            if (textInputLayout != null) {
                i = R.id.bsb_edit_text;
                BecsDebitBsbEditText becsDebitBsbEditText = (BecsDebitBsbEditText) view.findViewById(i);
                if (becsDebitBsbEditText != null) {
                    i = R.id.bsb_text_input_layout;
                    IconTextInputLayout iconTextInputLayout = (IconTextInputLayout) view.findViewById(i);
                    if (iconTextInputLayout != null) {
                        i = R.id.email_edit_text;
                        EmailEditText emailEditText2 = (EmailEditText) view.findViewById(i);
                        if (emailEditText2 != null) {
                            i = R.id.email_text_input_layout;
                            TextInputLayout textInputLayout2 = (TextInputLayout) view.findViewById(i);
                            if (textInputLayout2 != null) {
                                i = R.id.mandate_acceptance_text_view;
                                BecsDebitMandateAcceptanceTextView becsDebitMandateAcceptanceTextView = (BecsDebitMandateAcceptanceTextView) view.findViewById(i);
                                if (becsDebitMandateAcceptanceTextView != null) {
                                    i = R.id.name_edit_text;
                                    StripeEditText stripeEditText = (StripeEditText) view.findViewById(i);
                                    if (stripeEditText != null) {
                                        i = R.id.name_text_input_layout;
                                        TextInputLayout textInputLayout3 = (TextInputLayout) view.findViewById(i);
                                        if (textInputLayout3 != null) {
                                            return new BecsDebitWidgetBinding(view, becsDebitAccountNumberEditText, textInputLayout, becsDebitBsbEditText, iconTextInputLayout, emailEditText2, textInputLayout2, becsDebitMandateAcceptanceTextView, stripeEditText, textInputLayout3);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }
}
