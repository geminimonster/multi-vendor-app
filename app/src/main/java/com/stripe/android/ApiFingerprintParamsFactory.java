package com.stripe.android;

import android.content.Context;
import com.stripe.android.ClientFingerprintDataStore;
import java.util.Map;
import kotlin.Metadata;
import kotlin.TuplesKt;
import kotlin.collections.MapsKt;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\u0010\u000e\n\u0002\b\u0003\b\u0000\u0018\u0000 \f2\u00020\u0001:\u0001\fB\u000f\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004B\r\u0012\u0006\u0010\u0005\u001a\u00020\u0006¢\u0006\u0002\u0010\u0007J\u001c\u0010\b\u001a\u000e\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\n0\t2\b\u0010\u000b\u001a\u0004\u0018\u00010\nR\u000e\u0010\u0005\u001a\u00020\u0006X\u0004¢\u0006\u0002\n\u0000¨\u0006\r"}, d2 = {"Lcom/stripe/android/ApiFingerprintParamsFactory;", "", "context", "Landroid/content/Context;", "(Landroid/content/Context;)V", "store", "Lcom/stripe/android/ClientFingerprintDataStore;", "(Lcom/stripe/android/ClientFingerprintDataStore;)V", "createParams", "", "", "guid", "Companion", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: ApiFingerprintParamsFactory.kt */
public final class ApiFingerprintParamsFactory {
    @Deprecated
    public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
    private static final String FIELD_GUID = "guid";
    private static final String FIELD_MUID = "muid";
    private final ClientFingerprintDataStore store;

    public ApiFingerprintParamsFactory(ClientFingerprintDataStore clientFingerprintDataStore) {
        Intrinsics.checkParameterIsNotNull(clientFingerprintDataStore, "store");
        this.store = clientFingerprintDataStore;
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public ApiFingerprintParamsFactory(Context context) {
        this((ClientFingerprintDataStore) new ClientFingerprintDataStore.Default(context, (Function0) null, 2, (DefaultConstructorMarker) null));
        Intrinsics.checkParameterIsNotNull(context, "context");
    }

    public final Map<String, String> createParams(String str) {
        Map mapOf = MapsKt.mapOf(TuplesKt.to(FIELD_MUID, this.store.getMuid()));
        Map mapOf2 = str != null ? MapsKt.mapOf(TuplesKt.to(FIELD_GUID, str)) : null;
        if (mapOf2 == null) {
            mapOf2 = MapsKt.emptyMap();
        }
        return MapsKt.plus(mapOf, mapOf2);
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\u0006"}, d2 = {"Lcom/stripe/android/ApiFingerprintParamsFactory$Companion;", "", "()V", "FIELD_GUID", "", "FIELD_MUID", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: ApiFingerprintParamsFactory.kt */
    private static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }
}
