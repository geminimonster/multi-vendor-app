package com.stripe.android;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.stripe.android.RequestHeadersFactory;
import com.stripe.android.StripeRequest;
import com.stripe.android.exception.InvalidRequestException;
import java.io.UnsupportedEncodingException;
import java.util.Locale;
import java.util.Map;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000T\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010$\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0011\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0005\b\b\u0018\u0000 42\u00020\u0001:\u0003456Bk\b\u0000\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0014\b\u0002\u0010\u0006\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0002\b\u0003\u0018\u00010\u0007\u0012\u0006\u0010\b\u001a\u00020\t\u0012\n\b\u0002\u0010\n\u001a\u0004\u0018\u00010\u000b\u0012\u0014\b\u0002\u0010\f\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00050\r\u0012\b\b\u0002\u0010\u000e\u001a\u00020\u0005\u0012\b\b\u0002\u0010\u000f\u001a\u00020\u0005¢\u0006\u0002\u0010\u0010J\t\u0010#\u001a\u00020\u0003HÆ\u0003J\t\u0010$\u001a\u00020\u0005HÆ\u0003J\u0015\u0010%\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0002\b\u0003\u0018\u00010\u0007HÆ\u0003J\u000e\u0010&\u001a\u00020\tHÀ\u0003¢\u0006\u0002\b'J\u000b\u0010(\u001a\u0004\u0018\u00010\u000bHÂ\u0003J\u0015\u0010)\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00050\rHÂ\u0003J\t\u0010*\u001a\u00020\u0005HÂ\u0003J\t\u0010+\u001a\u00020\u0005HÂ\u0003Js\u0010,\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\u0014\b\u0002\u0010\u0006\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0002\b\u0003\u0018\u00010\u00072\b\b\u0002\u0010\b\u001a\u00020\t2\n\b\u0002\u0010\n\u001a\u0004\u0018\u00010\u000b2\u0014\b\u0002\u0010\f\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00050\r2\b\b\u0002\u0010\u000e\u001a\u00020\u00052\b\b\u0002\u0010\u000f\u001a\u00020\u0005HÆ\u0001J\u0013\u0010-\u001a\u00020.2\b\u0010/\u001a\u0004\u0018\u000100HÖ\u0003J\t\u00101\u001a\u000202HÖ\u0001J\b\u00103\u001a\u00020\u0005H\u0016R\u000e\u0010\u000e\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\n\u001a\u0004\u0018\u00010\u000bX\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u0012R\u0014\u0010\u0013\u001a\u00020\u00058TX\u0004¢\u0006\u0006\u001a\u0004\b\u0014\u0010\u0012R\u0014\u0010\u0015\u001a\u00020\u0016X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0017\u0010\u0018R\u0014\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0019\u0010\u001aR\u0014\u0010\u001b\u001a\u00020\u001cX\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u001d\u0010\u001eR\u0014\u0010\b\u001a\u00020\tX\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u001f\u0010 R \u0010\u0006\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0002\b\u0003\u0018\u00010\u0007X\u0004¢\u0006\b\n\u0000\u001a\u0004\b!\u0010\"R\u000e\u0010\u000f\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000R\u001a\u0010\f\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00050\rX\u0004¢\u0006\u0002\n\u0000¨\u00067"}, d2 = {"Lcom/stripe/android/ApiRequest;", "Lcom/stripe/android/StripeRequest;", "method", "Lcom/stripe/android/StripeRequest$Method;", "baseUrl", "", "params", "", "options", "Lcom/stripe/android/ApiRequest$Options;", "appInfo", "Lcom/stripe/android/AppInfo;", "systemPropertySupplier", "Lkotlin/Function1;", "apiVersion", "sdkVersion", "(Lcom/stripe/android/StripeRequest$Method;Ljava/lang/String;Ljava/util/Map;Lcom/stripe/android/ApiRequest$Options;Lcom/stripe/android/AppInfo;Lkotlin/jvm/functions/Function1;Ljava/lang/String;Ljava/lang/String;)V", "getBaseUrl", "()Ljava/lang/String;", "body", "getBody", "headersFactory", "Lcom/stripe/android/RequestHeadersFactory$Api;", "getHeadersFactory", "()Lcom/stripe/android/RequestHeadersFactory$Api;", "getMethod", "()Lcom/stripe/android/StripeRequest$Method;", "mimeType", "Lcom/stripe/android/StripeRequest$MimeType;", "getMimeType", "()Lcom/stripe/android/StripeRequest$MimeType;", "getOptions$stripe_release", "()Lcom/stripe/android/ApiRequest$Options;", "getParams", "()Ljava/util/Map;", "component1", "component2", "component3", "component4", "component4$stripe_release", "component5", "component6", "component7", "component8", "copy", "equals", "", "other", "", "hashCode", "", "toString", "Companion", "Factory", "Options", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: ApiRequest.kt */
public final class ApiRequest extends StripeRequest {
    public static final String API_HOST = "https://api.stripe.com";
    public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
    public static final String HEADER_STRIPE_CLIENT_USER_AGENT = "X-Stripe-Client-User-Agent";
    private final String apiVersion;
    private final AppInfo appInfo;
    private final String baseUrl;
    private final RequestHeadersFactory.Api headersFactory;
    private final StripeRequest.Method method;
    private final StripeRequest.MimeType mimeType;
    private final Options options;
    private final Map<String, ?> params;
    private final String sdkVersion;
    private final Function1<String, String> systemPropertySupplier;

    private final AppInfo component5() {
        return this.appInfo;
    }

    private final Function1<String, String> component6() {
        return this.systemPropertySupplier;
    }

    private final String component7() {
        return this.apiVersion;
    }

    private final String component8() {
        return this.sdkVersion;
    }

    public static /* synthetic */ ApiRequest copy$default(ApiRequest apiRequest, StripeRequest.Method method2, String str, Map map, Options options2, AppInfo appInfo2, Function1 function1, String str2, String str3, int i, Object obj) {
        ApiRequest apiRequest2 = apiRequest;
        int i2 = i;
        return apiRequest.copy((i2 & 1) != 0 ? apiRequest.getMethod() : method2, (i2 & 2) != 0 ? apiRequest.getBaseUrl() : str, (i2 & 4) != 0 ? apiRequest.getParams() : map, (i2 & 8) != 0 ? apiRequest2.options : options2, (i2 & 16) != 0 ? apiRequest2.appInfo : appInfo2, (i2 & 32) != 0 ? apiRequest2.systemPropertySupplier : function1, (i2 & 64) != 0 ? apiRequest2.apiVersion : str2, (i2 & 128) != 0 ? apiRequest2.sdkVersion : str3);
    }

    public final StripeRequest.Method component1() {
        return getMethod();
    }

    public final String component2() {
        return getBaseUrl();
    }

    public final Map<String, ?> component3() {
        return getParams();
    }

    public final Options component4$stripe_release() {
        return this.options;
    }

    public final ApiRequest copy(StripeRequest.Method method2, String str, Map<String, ?> map, Options options2, AppInfo appInfo2, Function1<? super String, String> function1, String str2, String str3) {
        Intrinsics.checkParameterIsNotNull(method2, FirebaseAnalytics.Param.METHOD);
        Intrinsics.checkParameterIsNotNull(str, "baseUrl");
        Intrinsics.checkParameterIsNotNull(options2, "options");
        Function1<? super String, String> function12 = function1;
        Intrinsics.checkParameterIsNotNull(function12, "systemPropertySupplier");
        String str4 = str2;
        Intrinsics.checkParameterIsNotNull(str4, "apiVersion");
        String str5 = str3;
        Intrinsics.checkParameterIsNotNull(str5, "sdkVersion");
        return new ApiRequest(method2, str, map, options2, appInfo2, function12, str4, str5);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ApiRequest)) {
            return false;
        }
        ApiRequest apiRequest = (ApiRequest) obj;
        return Intrinsics.areEqual((Object) getMethod(), (Object) apiRequest.getMethod()) && Intrinsics.areEqual((Object) getBaseUrl(), (Object) apiRequest.getBaseUrl()) && Intrinsics.areEqual((Object) getParams(), (Object) apiRequest.getParams()) && Intrinsics.areEqual((Object) this.options, (Object) apiRequest.options) && Intrinsics.areEqual((Object) this.appInfo, (Object) apiRequest.appInfo) && Intrinsics.areEqual((Object) this.systemPropertySupplier, (Object) apiRequest.systemPropertySupplier) && Intrinsics.areEqual((Object) this.apiVersion, (Object) apiRequest.apiVersion) && Intrinsics.areEqual((Object) this.sdkVersion, (Object) apiRequest.sdkVersion);
    }

    public int hashCode() {
        StripeRequest.Method method2 = getMethod();
        int i = 0;
        int hashCode = (method2 != null ? method2.hashCode() : 0) * 31;
        String baseUrl2 = getBaseUrl();
        int hashCode2 = (hashCode + (baseUrl2 != null ? baseUrl2.hashCode() : 0)) * 31;
        Map<String, ?> params2 = getParams();
        int hashCode3 = (hashCode2 + (params2 != null ? params2.hashCode() : 0)) * 31;
        Options options2 = this.options;
        int hashCode4 = (hashCode3 + (options2 != null ? options2.hashCode() : 0)) * 31;
        AppInfo appInfo2 = this.appInfo;
        int hashCode5 = (hashCode4 + (appInfo2 != null ? appInfo2.hashCode() : 0)) * 31;
        Function1<String, String> function1 = this.systemPropertySupplier;
        int hashCode6 = (hashCode5 + (function1 != null ? function1.hashCode() : 0)) * 31;
        String str = this.apiVersion;
        int hashCode7 = (hashCode6 + (str != null ? str.hashCode() : 0)) * 31;
        String str2 = this.sdkVersion;
        if (str2 != null) {
            i = str2.hashCode();
        }
        return hashCode7 + i;
    }

    public StripeRequest.Method getMethod() {
        return this.method;
    }

    public String getBaseUrl() {
        return this.baseUrl;
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* synthetic */ ApiRequest(com.stripe.android.StripeRequest.Method r13, java.lang.String r14, java.util.Map r15, com.stripe.android.ApiRequest.Options r16, com.stripe.android.AppInfo r17, kotlin.jvm.functions.Function1 r18, java.lang.String r19, java.lang.String r20, int r21, kotlin.jvm.internal.DefaultConstructorMarker r22) {
        /*
            r12 = this;
            r0 = r21
            r1 = r0 & 4
            r2 = 0
            if (r1 == 0) goto L_0x000c
            r1 = r2
            java.util.Map r1 = (java.util.Map) r1
            r6 = r1
            goto L_0x000d
        L_0x000c:
            r6 = r15
        L_0x000d:
            r1 = r0 & 16
            if (r1 == 0) goto L_0x0016
            r1 = r2
            com.stripe.android.AppInfo r1 = (com.stripe.android.AppInfo) r1
            r8 = r1
            goto L_0x0018
        L_0x0016:
            r8 = r17
        L_0x0018:
            r1 = r0 & 32
            if (r1 == 0) goto L_0x0024
            com.stripe.android.StripeRequest$Companion r1 = com.stripe.android.StripeRequest.Companion
            kotlin.jvm.functions.Function1 r1 = r1.getDEFAULT_SYSTEM_PROPERTY_SUPPLIER$stripe_release()
            r9 = r1
            goto L_0x0026
        L_0x0024:
            r9 = r18
        L_0x0026:
            r1 = r0 & 64
            if (r1 == 0) goto L_0x0036
            com.stripe.android.ApiVersion$Companion r1 = com.stripe.android.ApiVersion.Companion
            com.stripe.android.ApiVersion r1 = r1.get$stripe_release()
            java.lang.String r1 = r1.getCode$stripe_release()
            r10 = r1
            goto L_0x0038
        L_0x0036:
            r10 = r19
        L_0x0038:
            r0 = r0 & 128(0x80, float:1.794E-43)
            if (r0 == 0) goto L_0x0040
            java.lang.String r0 = "AndroidBindings/14.5.0"
            r11 = r0
            goto L_0x0042
        L_0x0040:
            r11 = r20
        L_0x0042:
            r3 = r12
            r4 = r13
            r5 = r14
            r7 = r16
            r3.<init>(r4, r5, r6, r7, r8, r9, r10, r11)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.stripe.android.ApiRequest.<init>(com.stripe.android.StripeRequest$Method, java.lang.String, java.util.Map, com.stripe.android.ApiRequest$Options, com.stripe.android.AppInfo, kotlin.jvm.functions.Function1, java.lang.String, java.lang.String, int, kotlin.jvm.internal.DefaultConstructorMarker):void");
    }

    public Map<String, ?> getParams() {
        return this.params;
    }

    public final Options getOptions$stripe_release() {
        return this.options;
    }

    public ApiRequest(StripeRequest.Method method2, String str, Map<String, ?> map, Options options2, AppInfo appInfo2, Function1<? super String, String> function1, String str2, String str3) {
        Function1<? super String, String> function12 = function1;
        String str4 = str2;
        String str5 = str3;
        Intrinsics.checkParameterIsNotNull(method2, FirebaseAnalytics.Param.METHOD);
        Intrinsics.checkParameterIsNotNull(str, "baseUrl");
        Intrinsics.checkParameterIsNotNull(options2, "options");
        Intrinsics.checkParameterIsNotNull(function12, "systemPropertySupplier");
        Intrinsics.checkParameterIsNotNull(str4, "apiVersion");
        Intrinsics.checkParameterIsNotNull(str5, "sdkVersion");
        this.method = method2;
        this.baseUrl = str;
        this.params = map;
        this.options = options2;
        this.appInfo = appInfo2;
        this.systemPropertySupplier = function12;
        this.apiVersion = str4;
        this.sdkVersion = str5;
        this.mimeType = StripeRequest.MimeType.Form;
        this.headersFactory = new RequestHeadersFactory.Api(this.options, this.appInfo, (Locale) null, this.systemPropertySupplier, this.apiVersion, this.sdkVersion, 4, (DefaultConstructorMarker) null);
    }

    public StripeRequest.MimeType getMimeType() {
        return this.mimeType;
    }

    public RequestHeadersFactory.Api getHeadersFactory() {
        return this.headersFactory;
    }

    /* access modifiers changed from: protected */
    public String getBody() throws UnsupportedEncodingException, InvalidRequestException {
        return getQuery();
    }

    public String toString() {
        return getMethod().getCode() + ' ' + getBaseUrl();
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u000e\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\b\b\u0018\u00002\u00020\u0001B'\b\u0000\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u0003¢\u0006\u0002\u0010\u0006J\t\u0010\u000b\u001a\u00020\u0003HÆ\u0003J\u0010\u0010\f\u001a\u0004\u0018\u00010\u0003HÀ\u0003¢\u0006\u0002\b\rJ\u0010\u0010\u000e\u001a\u0004\u0018\u00010\u0003HÀ\u0003¢\u0006\u0002\b\u000fJ+\u0010\u0010\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u0003HÆ\u0001J\t\u0010\u0011\u001a\u00020\u0012HÖ\u0001J\u0013\u0010\u0013\u001a\u00020\u00142\b\u0010\u0015\u001a\u0004\u0018\u00010\u0016HÖ\u0003J\t\u0010\u0017\u001a\u00020\u0012HÖ\u0001J\t\u0010\u0018\u001a\u00020\u0003HÖ\u0001J\u0019\u0010\u0019\u001a\u00020\u001a2\u0006\u0010\u001b\u001a\u00020\u001c2\u0006\u0010\u001d\u001a\u00020\u0012HÖ\u0001R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0016\u0010\u0005\u001a\u0004\u0018\u00010\u0003X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\bR\u0016\u0010\u0004\u001a\u0004\u0018\u00010\u0003X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\b¨\u0006\u001e"}, d2 = {"Lcom/stripe/android/ApiRequest$Options;", "Landroid/os/Parcelable;", "apiKey", "", "stripeAccount", "idempotencyKey", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", "getApiKey", "()Ljava/lang/String;", "getIdempotencyKey$stripe_release", "getStripeAccount$stripe_release", "component1", "component2", "component2$stripe_release", "component3", "component3$stripe_release", "copy", "describeContents", "", "equals", "", "other", "", "hashCode", "toString", "writeToParcel", "", "parcel", "Landroid/os/Parcel;", "flags", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: ApiRequest.kt */
    public static final class Options implements Parcelable {
        public static final Parcelable.Creator CREATOR = new Creator();
        private final String apiKey;
        private final String idempotencyKey;
        private final String stripeAccount;

        @Metadata(bv = {1, 0, 3}, k = 3, mv = {1, 1, 16})
        public static class Creator implements Parcelable.Creator {
            public final Object createFromParcel(Parcel parcel) {
                Intrinsics.checkParameterIsNotNull(parcel, "in");
                return new Options(parcel.readString(), parcel.readString(), parcel.readString());
            }

            public final Object[] newArray(int i) {
                return new Options[i];
            }
        }

        public static /* synthetic */ Options copy$default(Options options, String str, String str2, String str3, int i, Object obj) {
            if ((i & 1) != 0) {
                str = options.apiKey;
            }
            if ((i & 2) != 0) {
                str2 = options.stripeAccount;
            }
            if ((i & 4) != 0) {
                str3 = options.idempotencyKey;
            }
            return options.copy(str, str2, str3);
        }

        public final String component1() {
            return this.apiKey;
        }

        public final String component2$stripe_release() {
            return this.stripeAccount;
        }

        public final String component3$stripe_release() {
            return this.idempotencyKey;
        }

        public final Options copy(String str, String str2, String str3) {
            Intrinsics.checkParameterIsNotNull(str, "apiKey");
            return new Options(str, str2, str3);
        }

        public int describeContents() {
            return 0;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Options)) {
                return false;
            }
            Options options = (Options) obj;
            return Intrinsics.areEqual((Object) this.apiKey, (Object) options.apiKey) && Intrinsics.areEqual((Object) this.stripeAccount, (Object) options.stripeAccount) && Intrinsics.areEqual((Object) this.idempotencyKey, (Object) options.idempotencyKey);
        }

        public int hashCode() {
            String str = this.apiKey;
            int i = 0;
            int hashCode = (str != null ? str.hashCode() : 0) * 31;
            String str2 = this.stripeAccount;
            int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
            String str3 = this.idempotencyKey;
            if (str3 != null) {
                i = str3.hashCode();
            }
            return hashCode2 + i;
        }

        public String toString() {
            return "Options(apiKey=" + this.apiKey + ", stripeAccount=" + this.stripeAccount + ", idempotencyKey=" + this.idempotencyKey + ")";
        }

        public void writeToParcel(Parcel parcel, int i) {
            Intrinsics.checkParameterIsNotNull(parcel, "parcel");
            parcel.writeString(this.apiKey);
            parcel.writeString(this.stripeAccount);
            parcel.writeString(this.idempotencyKey);
        }

        public Options(String str, String str2, String str3) {
            Intrinsics.checkParameterIsNotNull(str, "apiKey");
            this.apiKey = str;
            this.stripeAccount = str2;
            this.idempotencyKey = str3;
            new ApiKeyValidator().requireValid(this.apiKey);
        }

        public final String getApiKey() {
            return this.apiKey;
        }

        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ Options(String str, String str2, String str3, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this(str, (i & 2) != 0 ? null : str2, (i & 4) != 0 ? null : str3);
        }

        public final String getStripeAccount$stripe_release() {
            return this.stripeAccount;
        }

        public final String getIdempotencyKey$stripe_release() {
            return this.idempotencyKey;
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\b\u0002\u0018\u00002\u00020\u0001B%\u0012\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\b\b\u0002\u0010\u0004\u001a\u00020\u0005\u0012\b\b\u0002\u0010\u0006\u001a\u00020\u0005¢\u0006\u0002\u0010\u0007J\u0016\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u00052\u0006\u0010\u000b\u001a\u00020\fJ,\u0010\r\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u00052\u0006\u0010\u000b\u001a\u00020\f2\u0014\b\u0002\u0010\u000e\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0002\b\u0003\u0018\u00010\u000fJ,\u0010\u0010\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u00052\u0006\u0010\u000b\u001a\u00020\f2\u0014\b\u0002\u0010\u000e\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0002\b\u0003\u0018\u00010\u000fR\u000e\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u0002\u001a\u0004\u0018\u00010\u0003X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0011"}, d2 = {"Lcom/stripe/android/ApiRequest$Factory;", "", "appInfo", "Lcom/stripe/android/AppInfo;", "apiVersion", "", "sdkVersion", "(Lcom/stripe/android/AppInfo;Ljava/lang/String;Ljava/lang/String;)V", "createDelete", "Lcom/stripe/android/ApiRequest;", "url", "options", "Lcom/stripe/android/ApiRequest$Options;", "createGet", "params", "", "createPost", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: ApiRequest.kt */
    public static final class Factory {
        private final String apiVersion;
        private final AppInfo appInfo;
        private final String sdkVersion;

        public Factory() {
            this((AppInfo) null, (String) null, (String) null, 7, (DefaultConstructorMarker) null);
        }

        public Factory(AppInfo appInfo2, String str, String str2) {
            Intrinsics.checkParameterIsNotNull(str, "apiVersion");
            Intrinsics.checkParameterIsNotNull(str2, "sdkVersion");
            this.appInfo = appInfo2;
            this.apiVersion = str;
            this.sdkVersion = str2;
        }

        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ Factory(AppInfo appInfo2, String str, String str2, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this((i & 1) != 0 ? null : appInfo2, (i & 2) != 0 ? ApiVersion.Companion.get$stripe_release().getCode$stripe_release() : str, (i & 4) != 0 ? Stripe.VERSION : str2);
        }

        public static /* synthetic */ ApiRequest createGet$default(Factory factory, String str, Options options, Map map, int i, Object obj) {
            if ((i & 4) != 0) {
                map = null;
            }
            return factory.createGet(str, options, map);
        }

        public final ApiRequest createGet(String str, Options options, Map<String, ?> map) {
            Intrinsics.checkParameterIsNotNull(str, "url");
            Intrinsics.checkParameterIsNotNull(options, "options");
            return new ApiRequest(StripeRequest.Method.GET, str, map, options, this.appInfo, (Function1) null, this.apiVersion, this.sdkVersion, 32, (DefaultConstructorMarker) null);
        }

        public static /* synthetic */ ApiRequest createPost$default(Factory factory, String str, Options options, Map map, int i, Object obj) {
            if ((i & 4) != 0) {
                map = null;
            }
            return factory.createPost(str, options, map);
        }

        public final ApiRequest createPost(String str, Options options, Map<String, ?> map) {
            Intrinsics.checkParameterIsNotNull(str, "url");
            Intrinsics.checkParameterIsNotNull(options, "options");
            return new ApiRequest(StripeRequest.Method.POST, str, map, options, this.appInfo, (Function1) null, this.apiVersion, this.sdkVersion, 32, (DefaultConstructorMarker) null);
        }

        public final ApiRequest createDelete(String str, Options options) {
            Intrinsics.checkParameterIsNotNull(str, "url");
            Intrinsics.checkParameterIsNotNull(options, "options");
            return new ApiRequest(StripeRequest.Method.DELETE, str, (Map) null, options, this.appInfo, (Function1) null, this.apiVersion, this.sdkVersion, 36, (DefaultConstructorMarker) null);
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\u0006"}, d2 = {"Lcom/stripe/android/ApiRequest$Companion;", "", "()V", "API_HOST", "", "HEADER_STRIPE_CLIENT_USER_AGENT", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: ApiRequest.kt */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }
}
