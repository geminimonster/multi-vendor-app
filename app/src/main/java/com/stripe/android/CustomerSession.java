package com.stripe.android;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import com.google.android.gms.common.internal.ServiceSpecificExtraArgs;
import com.stripe.android.EphemeralKeyManager;
import com.stripe.android.EphemeralOperation;
import com.stripe.android.exception.StripeException;
import com.stripe.android.model.Customer;
import com.stripe.android.model.PaymentMethod;
import com.stripe.android.model.ShippingInformation;
import com.stripe.android.model.Source;
import java.lang.ref.WeakReference;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import kotlin.collections.SetsKt;
import kotlin.coroutines.CoroutineContext;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.Intrinsics;
import kotlinx.coroutines.CoroutineDispatcher;
import kotlinx.coroutines.ExecutorsKt;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000¶\u0001\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\f\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010%\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\"\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\r\u0018\u0000 c2\u00020\u0001:\n_`abcdefghBY\b\u0001\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\b\u0010\b\u001a\u0004\u0018\u00010\u0007\u0012\b\b\u0002\u0010\t\u001a\u00020\n\u0012\b\b\u0002\u0010\u000b\u001a\u00020\f\u0012\u0012\b\u0002\u0010\r\u001a\f\u0012\u0004\u0012\u00020\u000f0\u000ej\u0002`\u0010\u0012\u0006\u0010\u0011\u001a\u00020\u0012¢\u0006\u0002\u0010\u0013J\u001e\u0010*\u001a\u00020+2\u0006\u0010,\u001a\u00020\u00072\u0006\u0010-\u001a\u00020\u00072\u0006\u0010.\u001a\u00020/J3\u0010*\u001a\u00020+2\u0006\u0010,\u001a\u00020\u00072\u0006\u0010-\u001a\u00020\u00072\f\u00100\u001a\b\u0012\u0004\u0012\u00020\u0007012\u0006\u0010.\u001a\u00020/H\u0000¢\u0006\u0002\b2J\u0016\u00103\u001a\u00020+2\u0006\u00104\u001a\u00020\u00072\u0006\u0010.\u001a\u000205J+\u00103\u001a\u00020+2\u0006\u00104\u001a\u00020\u00072\f\u00100\u001a\b\u0012\u0004\u0012\u00020\u0007012\u0006\u0010.\u001a\u000205H\u0000¢\u0006\u0002\b6J\r\u00107\u001a\u00020+H\u0000¢\u0006\u0002\b8J\b\u00109\u001a\u00020:H\u0002J\u0016\u0010;\u001a\u00020+2\u0006\u0010,\u001a\u00020\u00072\u0006\u0010.\u001a\u00020/J+\u0010;\u001a\u00020+2\u0006\u0010,\u001a\u00020\u00072\f\u00100\u001a\b\u0012\u0004\u0012\u00020\u0007012\u0006\u0010.\u001a\u00020/H\u0000¢\u0006\u0002\b<J\u0016\u0010=\u001a\u00020+2\u0006\u00104\u001a\u00020\u00072\u0006\u0010.\u001a\u000205J+\u0010=\u001a\u00020+2\u0006\u00104\u001a\u00020\u00072\f\u00100\u001a\b\u0012\u0004\u0012\u00020\u0007012\u0006\u0010.\u001a\u000205H\u0000¢\u0006\u0002\b>J#\u0010?\u001a\u0004\u0018\u0001H@\"\n\b\u0000\u0010@*\u0004\u0018\u00010)2\u0006\u0010A\u001a\u00020\u0007H\u0002¢\u0006\u0002\u0010BJ\u0016\u0010C\u001a\u00020+2\u0006\u0010D\u001a\u00020E2\u0006\u0010.\u001a\u00020FJA\u0010C\u001a\u00020+2\u0006\u0010D\u001a\u00020E2\n\b\u0001\u0010G\u001a\u0004\u0018\u00010H2\n\b\u0002\u0010I\u001a\u0004\u0018\u00010\u00072\n\b\u0002\u0010J\u001a\u0004\u0018\u00010\u00072\u0006\u0010.\u001a\u00020FH\u0007¢\u0006\u0002\u0010KJQ\u0010C\u001a\u00020+2\u0006\u0010D\u001a\u00020E2\n\b\u0003\u0010G\u001a\u0004\u0018\u00010H2\n\b\u0002\u0010I\u001a\u0004\u0018\u00010\u00072\n\b\u0002\u0010J\u001a\u0004\u0018\u00010\u00072\f\u00100\u001a\b\u0012\u0004\u0012\u00020\u0007012\u0006\u0010.\u001a\u00020FH\u0000¢\u0006\u0004\bL\u0010MJ\u0018\u0010N\u001a\u00020+2\u0006\u0010A\u001a\u00020\u00072\u0006\u0010O\u001a\u00020PH\u0002J\u000e\u0010Q\u001a\u00020+2\u0006\u0010.\u001a\u00020RJ#\u0010Q\u001a\u00020+2\f\u00100\u001a\b\u0012\u0004\u0012\u00020\u0007012\u0006\u0010.\u001a\u00020RH\u0000¢\u0006\u0002\bSJ\u001e\u0010T\u001a\u00020+2\u0006\u0010,\u001a\u00020\u00072\u0006\u0010-\u001a\u00020\u00072\u0006\u0010.\u001a\u00020RJ3\u0010T\u001a\u00020+2\u0006\u0010,\u001a\u00020\u00072\u0006\u0010-\u001a\u00020\u00072\f\u00100\u001a\b\u0012\u0004\u0012\u00020\u0007012\u0006\u0010.\u001a\u00020RH\u0000¢\u0006\u0002\bUJ\u0016\u0010V\u001a\u00020+2\u0006\u0010W\u001a\u00020X2\u0006\u0010.\u001a\u00020RJ+\u0010V\u001a\u00020+2\u0006\u0010W\u001a\u00020X2\f\u00100\u001a\b\u0012\u0004\u0012\u00020\u0007012\u0006\u0010.\u001a\u00020RH\u0000¢\u0006\u0002\bYJ\u001a\u0010Z\u001a\u00020+2\u0006\u0010[\u001a\u00020\\2\b\u0010.\u001a\u0004\u0018\u00010)H\u0002J\u000e\u0010]\u001a\u00020+2\u0006\u0010.\u001a\u00020RJ#\u0010]\u001a\u00020+2\f\u00100\u001a\b\u0012\u0004\u0012\u00020\u0007012\u0006\u0010.\u001a\u00020RH\u0000¢\u0006\u0002\b^R\u0013\u0010\u0014\u001a\u0004\u0018\u00010\u00158F¢\u0006\u0006\u001a\u0004\b\u0016\u0010\u0017R\u0014\u0010\u0018\u001a\u00020\u00198BX\u0004¢\u0006\u0006\u001a\u0004\b\u001a\u0010\u001bR\u001c\u0010\u001c\u001a\u0004\u0018\u00010\u0015X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u001d\u0010\u0017\"\u0004\b\u001e\u0010\u001fR\u001a\u0010 \u001a\u00020\u000fX\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b!\u0010\"\"\u0004\b#\u0010$R\u000e\u0010%\u001a\u00020&X\u0004¢\u0006\u0002\n\u0000R\u001c\u0010'\u001a\u0010\u0012\u0004\u0012\u00020\u0007\u0012\u0006\u0012\u0004\u0018\u00010)0(X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\fX\u0004¢\u0006\u0002\n\u0000R\u0018\u0010\r\u001a\f\u0012\u0004\u0012\u00020\u000f0\u000ej\u0002`\u0010X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0004¢\u0006\u0002\n\u0000¨\u0006i"}, d2 = {"Lcom/stripe/android/CustomerSession;", "", "context", "Landroid/content/Context;", "stripeRepository", "Lcom/stripe/android/StripeRepository;", "publishableKey", "", "stripeAccountId", "workDispatcher", "Lkotlinx/coroutines/CoroutineDispatcher;", "operationIdFactory", "Lcom/stripe/android/OperationIdFactory;", "timeSupplier", "Lkotlin/Function0;", "", "Lcom/stripe/android/TimeSupplier;", "ephemeralKeyManagerFactory", "Lcom/stripe/android/EphemeralKeyManager$Factory;", "(Landroid/content/Context;Lcom/stripe/android/StripeRepository;Ljava/lang/String;Ljava/lang/String;Lkotlinx/coroutines/CoroutineDispatcher;Lcom/stripe/android/OperationIdFactory;Lkotlin/jvm/functions/Function0;Lcom/stripe/android/EphemeralKeyManager$Factory;)V", "cachedCustomer", "Lcom/stripe/android/model/Customer;", "getCachedCustomer", "()Lcom/stripe/android/model/Customer;", "canUseCachedCustomer", "", "getCanUseCachedCustomer", "()Z", "customer", "getCustomer$stripe_release", "setCustomer$stripe_release", "(Lcom/stripe/android/model/Customer;)V", "customerCacheTime", "getCustomerCacheTime$stripe_release", "()J", "setCustomerCacheTime$stripe_release", "(J)V", "ephemeralKeyManager", "Lcom/stripe/android/EphemeralKeyManager;", "listeners", "", "Lcom/stripe/android/CustomerSession$RetrievalListener;", "addCustomerSource", "", "sourceId", "sourceType", "listener", "Lcom/stripe/android/CustomerSession$SourceRetrievalListener;", "productUsage", "", "addCustomerSource$stripe_release", "attachPaymentMethod", "paymentMethodId", "Lcom/stripe/android/CustomerSession$PaymentMethodRetrievalListener;", "attachPaymentMethod$stripe_release", "cancel", "cancel$stripe_release", "createHandler", "Landroid/os/Handler;", "deleteCustomerSource", "deleteCustomerSource$stripe_release", "detachPaymentMethod", "detachPaymentMethod$stripe_release", "getListener", "L", "operationId", "(Ljava/lang/String;)Lcom/stripe/android/CustomerSession$RetrievalListener;", "getPaymentMethods", "paymentMethodType", "Lcom/stripe/android/model/PaymentMethod$Type;", "Lcom/stripe/android/CustomerSession$PaymentMethodsRetrievalListener;", "limit", "", "endingBefore", "startingAfter", "(Lcom/stripe/android/model/PaymentMethod$Type;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Lcom/stripe/android/CustomerSession$PaymentMethodsRetrievalListener;)V", "getPaymentMethods$stripe_release", "(Lcom/stripe/android/model/PaymentMethod$Type;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;Lcom/stripe/android/CustomerSession$PaymentMethodsRetrievalListener;)V", "handleRetrievalError", "exception", "Lcom/stripe/android/exception/StripeException;", "retrieveCurrentCustomer", "Lcom/stripe/android/CustomerSession$CustomerRetrievalListener;", "retrieveCurrentCustomer$stripe_release", "setCustomerDefaultSource", "setCustomerDefaultSource$stripe_release", "setCustomerShippingInformation", "shippingInformation", "Lcom/stripe/android/model/ShippingInformation;", "setCustomerShippingInformation$stripe_release", "startOperation", "operation", "Lcom/stripe/android/EphemeralOperation;", "updateCurrentCustomer", "updateCurrentCustomer$stripe_release", "ActivityCustomerRetrievalListener", "ActivityPaymentMethodRetrievalListener", "ActivityPaymentMethodsRetrievalListener", "ActivitySourceRetrievalListener", "Companion", "CustomerRetrievalListener", "PaymentMethodRetrievalListener", "PaymentMethodsRetrievalListener", "RetrievalListener", "SourceRetrievalListener", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: CustomerSession.kt */
public final class CustomerSession {
    private static final long CUSTOMER_CACHE_DURATION_MILLISECONDS = TimeUnit.MINUTES.toMillis(1);
    public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
    private static final int KEEP_ALIVE_TIME = 2;
    /* access modifiers changed from: private */
    public static final TimeUnit KEEP_ALIVE_TIME_UNIT = TimeUnit.SECONDS;
    private static final int THREAD_POOL_SIZE = 3;
    /* access modifiers changed from: private */
    public static /* synthetic */ CustomerSession instance;
    private /* synthetic */ Customer customer;
    private /* synthetic */ long customerCacheTime;
    private final EphemeralKeyManager ephemeralKeyManager;
    private final Map<String, RetrievalListener> listeners;
    private final OperationIdFactory operationIdFactory;
    /* access modifiers changed from: private */
    public final Function0<Long> timeSupplier;
    private final CoroutineDispatcher workDispatcher;

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\bf\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&¨\u0006\u0006"}, d2 = {"Lcom/stripe/android/CustomerSession$CustomerRetrievalListener;", "Lcom/stripe/android/CustomerSession$RetrievalListener;", "onCustomerRetrieved", "", "customer", "Lcom/stripe/android/model/Customer;", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: CustomerSession.kt */
    public interface CustomerRetrievalListener extends RetrievalListener {
        void onCustomerRetrieved(Customer customer);
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\bf\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&¨\u0006\u0006"}, d2 = {"Lcom/stripe/android/CustomerSession$PaymentMethodRetrievalListener;", "Lcom/stripe/android/CustomerSession$RetrievalListener;", "onPaymentMethodRetrieved", "", "paymentMethod", "Lcom/stripe/android/model/PaymentMethod;", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: CustomerSession.kt */
    public interface PaymentMethodRetrievalListener extends RetrievalListener {
        void onPaymentMethodRetrieved(PaymentMethod paymentMethod);
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\bf\u0018\u00002\u00020\u0001J\u0016\u0010\u0002\u001a\u00020\u00032\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005H&¨\u0006\u0007"}, d2 = {"Lcom/stripe/android/CustomerSession$PaymentMethodsRetrievalListener;", "Lcom/stripe/android/CustomerSession$RetrievalListener;", "onPaymentMethodsRetrieved", "", "paymentMethods", "", "Lcom/stripe/android/model/PaymentMethod;", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: CustomerSession.kt */
    public interface PaymentMethodsRetrievalListener extends RetrievalListener {
        void onPaymentMethodsRetrieved(List<PaymentMethod> list);
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\bf\u0018\u00002\u00020\u0001J\"\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00072\b\u0010\b\u001a\u0004\u0018\u00010\tH&¨\u0006\n"}, d2 = {"Lcom/stripe/android/CustomerSession$RetrievalListener;", "", "onError", "", "errorCode", "", "errorMessage", "", "stripeError", "Lcom/stripe/android/StripeError;", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: CustomerSession.kt */
    public interface RetrievalListener {
        void onError(int i, String str, StripeError stripeError);
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\bf\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&¨\u0006\u0006"}, d2 = {"Lcom/stripe/android/CustomerSession$SourceRetrievalListener;", "Lcom/stripe/android/CustomerSession$RetrievalListener;", "onSourceRetrieved", "", "source", "Lcom/stripe/android/model/Source;", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: CustomerSession.kt */
    public interface SourceRetrievalListener extends RetrievalListener {
        void onSourceRetrieved(Source source);
    }

    @JvmStatic
    public static final void cancelCallbacks() {
        Companion.cancelCallbacks();
    }

    @JvmStatic
    public static final void endCustomerSession() {
        Companion.endCustomerSession();
    }

    @JvmStatic
    public static final CustomerSession getInstance() {
        return Companion.getInstance();
    }

    @JvmStatic
    public static final void initCustomerSession(Context context, EphemeralKeyProvider ephemeralKeyProvider) {
        Companion.initCustomerSession$default(Companion, context, ephemeralKeyProvider, (String) null, false, 12, (Object) null);
    }

    @JvmStatic
    public static final void initCustomerSession(Context context, EphemeralKeyProvider ephemeralKeyProvider, String str) {
        Companion.initCustomerSession$default(Companion, context, ephemeralKeyProvider, str, false, 8, (Object) null);
    }

    @JvmStatic
    public static final void initCustomerSession(Context context, EphemeralKeyProvider ephemeralKeyProvider, String str, boolean z) {
        Companion.initCustomerSession(context, ephemeralKeyProvider, str, z);
    }

    @JvmStatic
    public static final void initCustomerSession(Context context, EphemeralKeyProvider ephemeralKeyProvider, boolean z) {
        Companion.initCustomerSession(context, ephemeralKeyProvider, z);
    }

    public final void getPaymentMethods(PaymentMethod.Type type, Integer num, PaymentMethodsRetrievalListener paymentMethodsRetrievalListener) {
        getPaymentMethods$default(this, type, num, (String) null, (String) null, paymentMethodsRetrievalListener, 12, (Object) null);
    }

    public final void getPaymentMethods(PaymentMethod.Type type, Integer num, String str, PaymentMethodsRetrievalListener paymentMethodsRetrievalListener) {
        getPaymentMethods$default(this, type, num, str, (String) null, paymentMethodsRetrievalListener, 8, (Object) null);
    }

    public CustomerSession(Context context, StripeRepository stripeRepository, String str, String str2, CoroutineDispatcher coroutineDispatcher, OperationIdFactory operationIdFactory2, Function0<Long> function0, EphemeralKeyManager.Factory factory) {
        Intrinsics.checkParameterIsNotNull(context, "context");
        Intrinsics.checkParameterIsNotNull(stripeRepository, "stripeRepository");
        Intrinsics.checkParameterIsNotNull(str, "publishableKey");
        Intrinsics.checkParameterIsNotNull(coroutineDispatcher, "workDispatcher");
        Intrinsics.checkParameterIsNotNull(operationIdFactory2, "operationIdFactory");
        Intrinsics.checkParameterIsNotNull(function0, "timeSupplier");
        Intrinsics.checkParameterIsNotNull(factory, "ephemeralKeyManagerFactory");
        this.workDispatcher = coroutineDispatcher;
        this.operationIdFactory = operationIdFactory2;
        this.timeSupplier = function0;
        this.listeners = new LinkedHashMap();
        this.ephemeralKeyManager = (EphemeralKeyManager) factory.create(new CustomerSessionEphemeralKeyManagerListener(new CustomerSessionRunnableFactory(stripeRepository, createHandler(), str, str2), this.workDispatcher, this.listeners));
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ CustomerSession(Context context, StripeRepository stripeRepository, String str, String str2, CoroutineDispatcher coroutineDispatcher, OperationIdFactory operationIdFactory2, Function0 function0, EphemeralKeyManager.Factory factory, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(context, stripeRepository, str, str2, (i & 16) != 0 ? Companion.createCoroutineDispatcher() : coroutineDispatcher, (i & 32) != 0 ? new StripeOperationIdFactory() : operationIdFactory2, (i & 64) != 0 ? AnonymousClass1.INSTANCE : function0, factory);
    }

    public final long getCustomerCacheTime$stripe_release() {
        return this.customerCacheTime;
    }

    public final void setCustomerCacheTime$stripe_release(long j) {
        this.customerCacheTime = j;
    }

    public final Customer getCustomer$stripe_release() {
        return this.customer;
    }

    public final void setCustomer$stripe_release(Customer customer2) {
        this.customer = customer2;
    }

    private final Handler createHandler() {
        return new CustomerSessionHandler(new CustomerSession$createHandler$1(this));
    }

    public final void retrieveCurrentCustomer(CustomerRetrievalListener customerRetrievalListener) {
        Intrinsics.checkParameterIsNotNull(customerRetrievalListener, ServiceSpecificExtraArgs.CastExtraArgs.LISTENER);
        retrieveCurrentCustomer$stripe_release(SetsKt.emptySet(), customerRetrievalListener);
    }

    public final /* synthetic */ void retrieveCurrentCustomer$stripe_release(Set<String> set, CustomerRetrievalListener customerRetrievalListener) {
        Intrinsics.checkParameterIsNotNull(set, "productUsage");
        Intrinsics.checkParameterIsNotNull(customerRetrievalListener, ServiceSpecificExtraArgs.CastExtraArgs.LISTENER);
        Customer cachedCustomer = getCachedCustomer();
        if (cachedCustomer != null) {
            customerRetrievalListener.onCustomerRetrieved(cachedCustomer);
        } else {
            updateCurrentCustomer$stripe_release(set, customerRetrievalListener);
        }
    }

    public final void updateCurrentCustomer(CustomerRetrievalListener customerRetrievalListener) {
        Intrinsics.checkParameterIsNotNull(customerRetrievalListener, ServiceSpecificExtraArgs.CastExtraArgs.LISTENER);
        updateCurrentCustomer$stripe_release(SetsKt.emptySet(), customerRetrievalListener);
    }

    public final /* synthetic */ void updateCurrentCustomer$stripe_release(Set<String> set, CustomerRetrievalListener customerRetrievalListener) {
        Intrinsics.checkParameterIsNotNull(set, "productUsage");
        Intrinsics.checkParameterIsNotNull(customerRetrievalListener, ServiceSpecificExtraArgs.CastExtraArgs.LISTENER);
        this.customer = null;
        startOperation(new EphemeralOperation.RetrieveKey(this.operationIdFactory.create(), set), customerRetrievalListener);
    }

    public final Customer getCachedCustomer() {
        Customer customer2 = this.customer;
        if (getCanUseCachedCustomer()) {
            return customer2;
        }
        return null;
    }

    public final void addCustomerSource(String str, String str2, SourceRetrievalListener sourceRetrievalListener) {
        Intrinsics.checkParameterIsNotNull(str, "sourceId");
        Intrinsics.checkParameterIsNotNull(str2, "sourceType");
        Intrinsics.checkParameterIsNotNull(sourceRetrievalListener, ServiceSpecificExtraArgs.CastExtraArgs.LISTENER);
        addCustomerSource$stripe_release(str, str2, SetsKt.emptySet(), sourceRetrievalListener);
    }

    public final /* synthetic */ void addCustomerSource$stripe_release(String str, String str2, Set<String> set, SourceRetrievalListener sourceRetrievalListener) {
        Intrinsics.checkParameterIsNotNull(str, "sourceId");
        Intrinsics.checkParameterIsNotNull(str2, "sourceType");
        Intrinsics.checkParameterIsNotNull(set, "productUsage");
        Intrinsics.checkParameterIsNotNull(sourceRetrievalListener, ServiceSpecificExtraArgs.CastExtraArgs.LISTENER);
        startOperation(new EphemeralOperation.Customer.AddSource(str, str2, this.operationIdFactory.create(), set), sourceRetrievalListener);
    }

    public final void deleteCustomerSource(String str, SourceRetrievalListener sourceRetrievalListener) {
        Intrinsics.checkParameterIsNotNull(str, "sourceId");
        Intrinsics.checkParameterIsNotNull(sourceRetrievalListener, ServiceSpecificExtraArgs.CastExtraArgs.LISTENER);
        deleteCustomerSource$stripe_release(str, SetsKt.emptySet(), sourceRetrievalListener);
    }

    public final /* synthetic */ void deleteCustomerSource$stripe_release(String str, Set<String> set, SourceRetrievalListener sourceRetrievalListener) {
        Intrinsics.checkParameterIsNotNull(str, "sourceId");
        Intrinsics.checkParameterIsNotNull(set, "productUsage");
        Intrinsics.checkParameterIsNotNull(sourceRetrievalListener, ServiceSpecificExtraArgs.CastExtraArgs.LISTENER);
        startOperation(new EphemeralOperation.Customer.DeleteSource(str, this.operationIdFactory.create(), set), sourceRetrievalListener);
    }

    public final void attachPaymentMethod(String str, PaymentMethodRetrievalListener paymentMethodRetrievalListener) {
        Intrinsics.checkParameterIsNotNull(str, "paymentMethodId");
        Intrinsics.checkParameterIsNotNull(paymentMethodRetrievalListener, ServiceSpecificExtraArgs.CastExtraArgs.LISTENER);
        attachPaymentMethod$stripe_release(str, SetsKt.emptySet(), paymentMethodRetrievalListener);
    }

    public final /* synthetic */ void attachPaymentMethod$stripe_release(String str, Set<String> set, PaymentMethodRetrievalListener paymentMethodRetrievalListener) {
        Intrinsics.checkParameterIsNotNull(str, "paymentMethodId");
        Intrinsics.checkParameterIsNotNull(set, "productUsage");
        Intrinsics.checkParameterIsNotNull(paymentMethodRetrievalListener, ServiceSpecificExtraArgs.CastExtraArgs.LISTENER);
        startOperation(new EphemeralOperation.Customer.AttachPaymentMethod(str, this.operationIdFactory.create(), set), paymentMethodRetrievalListener);
    }

    public final void detachPaymentMethod(String str, PaymentMethodRetrievalListener paymentMethodRetrievalListener) {
        Intrinsics.checkParameterIsNotNull(str, "paymentMethodId");
        Intrinsics.checkParameterIsNotNull(paymentMethodRetrievalListener, ServiceSpecificExtraArgs.CastExtraArgs.LISTENER);
        detachPaymentMethod$stripe_release(str, SetsKt.emptySet(), paymentMethodRetrievalListener);
    }

    public final /* synthetic */ void detachPaymentMethod$stripe_release(String str, Set<String> set, PaymentMethodRetrievalListener paymentMethodRetrievalListener) {
        Intrinsics.checkParameterIsNotNull(str, "paymentMethodId");
        Intrinsics.checkParameterIsNotNull(set, "productUsage");
        Intrinsics.checkParameterIsNotNull(paymentMethodRetrievalListener, ServiceSpecificExtraArgs.CastExtraArgs.LISTENER);
        startOperation(new EphemeralOperation.Customer.DetachPaymentMethod(str, this.operationIdFactory.create(), set), paymentMethodRetrievalListener);
    }

    public static /* synthetic */ void getPaymentMethods$default(CustomerSession customerSession, PaymentMethod.Type type, Integer num, String str, String str2, PaymentMethodsRetrievalListener paymentMethodsRetrievalListener, int i, Object obj) {
        if ((i & 4) != 0) {
            str = null;
        }
        String str3 = str;
        if ((i & 8) != 0) {
            str2 = null;
        }
        customerSession.getPaymentMethods(type, num, str3, str2, paymentMethodsRetrievalListener);
    }

    public final void getPaymentMethods(PaymentMethod.Type type, Integer num, String str, String str2, PaymentMethodsRetrievalListener paymentMethodsRetrievalListener) {
        Intrinsics.checkParameterIsNotNull(type, "paymentMethodType");
        Intrinsics.checkParameterIsNotNull(paymentMethodsRetrievalListener, ServiceSpecificExtraArgs.CastExtraArgs.LISTENER);
        getPaymentMethods$stripe_release(type, num, str, str2, SetsKt.emptySet(), paymentMethodsRetrievalListener);
    }

    public static /* synthetic */ void getPaymentMethods$stripe_release$default(CustomerSession customerSession, PaymentMethod.Type type, Integer num, String str, String str2, Set set, PaymentMethodsRetrievalListener paymentMethodsRetrievalListener, int i, Object obj) {
        customerSession.getPaymentMethods$stripe_release(type, (i & 2) != 0 ? null : num, (i & 4) != 0 ? null : str, (i & 8) != 0 ? null : str2, set, paymentMethodsRetrievalListener);
    }

    public final /* synthetic */ void getPaymentMethods$stripe_release(PaymentMethod.Type type, Integer num, String str, String str2, Set<String> set, PaymentMethodsRetrievalListener paymentMethodsRetrievalListener) {
        Intrinsics.checkParameterIsNotNull(type, "paymentMethodType");
        Intrinsics.checkParameterIsNotNull(set, "productUsage");
        Intrinsics.checkParameterIsNotNull(paymentMethodsRetrievalListener, ServiceSpecificExtraArgs.CastExtraArgs.LISTENER);
        startOperation(new EphemeralOperation.Customer.GetPaymentMethods(type, num, str, str2, this.operationIdFactory.create(), set), paymentMethodsRetrievalListener);
    }

    public final void getPaymentMethods(PaymentMethod.Type type, PaymentMethodsRetrievalListener paymentMethodsRetrievalListener) {
        Intrinsics.checkParameterIsNotNull(type, "paymentMethodType");
        Intrinsics.checkParameterIsNotNull(paymentMethodsRetrievalListener, ServiceSpecificExtraArgs.CastExtraArgs.LISTENER);
        getPaymentMethods$stripe_release$default(this, type, (Integer) null, (String) null, (String) null, SetsKt.emptySet(), paymentMethodsRetrievalListener, 14, (Object) null);
    }

    public final void setCustomerShippingInformation(ShippingInformation shippingInformation, CustomerRetrievalListener customerRetrievalListener) {
        Intrinsics.checkParameterIsNotNull(shippingInformation, "shippingInformation");
        Intrinsics.checkParameterIsNotNull(customerRetrievalListener, ServiceSpecificExtraArgs.CastExtraArgs.LISTENER);
        setCustomerShippingInformation$stripe_release(shippingInformation, SetsKt.emptySet(), customerRetrievalListener);
    }

    public final /* synthetic */ void setCustomerShippingInformation$stripe_release(ShippingInformation shippingInformation, Set<String> set, CustomerRetrievalListener customerRetrievalListener) {
        Intrinsics.checkParameterIsNotNull(shippingInformation, "shippingInformation");
        Intrinsics.checkParameterIsNotNull(set, "productUsage");
        Intrinsics.checkParameterIsNotNull(customerRetrievalListener, ServiceSpecificExtraArgs.CastExtraArgs.LISTENER);
        startOperation(new EphemeralOperation.Customer.UpdateShipping(shippingInformation, this.operationIdFactory.create(), set), customerRetrievalListener);
    }

    public final void setCustomerDefaultSource(String str, String str2, CustomerRetrievalListener customerRetrievalListener) {
        Intrinsics.checkParameterIsNotNull(str, "sourceId");
        Intrinsics.checkParameterIsNotNull(str2, "sourceType");
        Intrinsics.checkParameterIsNotNull(customerRetrievalListener, ServiceSpecificExtraArgs.CastExtraArgs.LISTENER);
        setCustomerDefaultSource$stripe_release(str, str2, SetsKt.emptySet(), customerRetrievalListener);
    }

    public final /* synthetic */ void setCustomerDefaultSource$stripe_release(String str, String str2, Set<String> set, CustomerRetrievalListener customerRetrievalListener) {
        Intrinsics.checkParameterIsNotNull(str, "sourceId");
        Intrinsics.checkParameterIsNotNull(str2, "sourceType");
        Intrinsics.checkParameterIsNotNull(set, "productUsage");
        Intrinsics.checkParameterIsNotNull(customerRetrievalListener, ServiceSpecificExtraArgs.CastExtraArgs.LISTENER);
        startOperation(new EphemeralOperation.Customer.UpdateDefaultSource(str, str2, this.operationIdFactory.create(), set), customerRetrievalListener);
    }

    private final void startOperation(EphemeralOperation ephemeralOperation, RetrievalListener retrievalListener) {
        this.listeners.put(ephemeralOperation.getId$stripe_release(), retrievalListener);
        this.ephemeralKeyManager.retrieveEphemeralKey$stripe_release(ephemeralOperation);
    }

    private final boolean getCanUseCachedCustomer() {
        return this.customer != null && this.timeSupplier.invoke().longValue() - this.customerCacheTime < CUSTOMER_CACHE_DURATION_MILLISECONDS;
    }

    /* access modifiers changed from: private */
    public final void handleRetrievalError(String str, StripeException stripeException) {
        RetrievalListener remove = this.listeners.remove(str);
        if (remove != null) {
            String localizedMessage = stripeException.getLocalizedMessage();
            if (localizedMessage == null) {
                localizedMessage = "";
            }
            remove.onError(stripeException.getStatusCode(), localizedMessage, stripeException.getStripeError());
        }
    }

    public final /* synthetic */ void cancel$stripe_release() {
        this.listeners.clear();
        JobKt__JobKt.cancelChildren$default((CoroutineContext) this.workDispatcher, (CancellationException) null, 1, (Object) null);
    }

    /* access modifiers changed from: private */
    public final <L extends RetrievalListener> L getListener(String str) {
        return (RetrievalListener) this.listeners.remove(str);
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\b&\u0018\u0000*\n\b\u0000\u0010\u0001*\u0004\u0018\u00010\u00022\u00020\u0003B\r\u0012\u0006\u0010\u0004\u001a\u00028\u0000¢\u0006\u0002\u0010\u0005R\u0016\u0010\u0004\u001a\u0004\u0018\u00018\u00008DX\u0004¢\u0006\u0006\u001a\u0004\b\u0006\u0010\u0007R\u0014\u0010\b\u001a\b\u0012\u0004\u0012\u00028\u00000\tX\u0004¢\u0006\u0002\n\u0000¨\u0006\n"}, d2 = {"Lcom/stripe/android/CustomerSession$ActivityCustomerRetrievalListener;", "A", "Landroid/app/Activity;", "Lcom/stripe/android/CustomerSession$CustomerRetrievalListener;", "activity", "(Landroid/app/Activity;)V", "getActivity", "()Landroid/app/Activity;", "activityRef", "Ljava/lang/ref/WeakReference;", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: CustomerSession.kt */
    public static abstract class ActivityCustomerRetrievalListener<A extends Activity> implements CustomerRetrievalListener {
        private final WeakReference<A> activityRef;

        public ActivityCustomerRetrievalListener(A a2) {
            this.activityRef = new WeakReference<>(a2);
        }

        /* access modifiers changed from: protected */
        public final A getActivity() {
            return (Activity) this.activityRef.get();
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\b&\u0018\u0000*\n\b\u0000\u0010\u0001*\u0004\u0018\u00010\u00022\u00020\u0003B\r\u0012\u0006\u0010\u0004\u001a\u00028\u0000¢\u0006\u0002\u0010\u0005R\u0016\u0010\u0004\u001a\u0004\u0018\u00018\u00008DX\u0004¢\u0006\u0006\u001a\u0004\b\u0006\u0010\u0007R\u0014\u0010\b\u001a\b\u0012\u0004\u0012\u00028\u00000\tX\u0004¢\u0006\u0002\n\u0000¨\u0006\n"}, d2 = {"Lcom/stripe/android/CustomerSession$ActivityPaymentMethodsRetrievalListener;", "A", "Landroid/app/Activity;", "Lcom/stripe/android/CustomerSession$PaymentMethodsRetrievalListener;", "activity", "(Landroid/app/Activity;)V", "getActivity", "()Landroid/app/Activity;", "activityRef", "Ljava/lang/ref/WeakReference;", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: CustomerSession.kt */
    public static abstract class ActivityPaymentMethodsRetrievalListener<A extends Activity> implements PaymentMethodsRetrievalListener {
        private final WeakReference<A> activityRef;

        public ActivityPaymentMethodsRetrievalListener(A a2) {
            this.activityRef = new WeakReference<>(a2);
        }

        /* access modifiers changed from: protected */
        public final A getActivity() {
            return (Activity) this.activityRef.get();
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\b&\u0018\u0000*\n\b\u0000\u0010\u0001*\u0004\u0018\u00010\u00022\u00020\u0003B\r\u0012\u0006\u0010\u0004\u001a\u00028\u0000¢\u0006\u0002\u0010\u0005R\u0016\u0010\u0004\u001a\u0004\u0018\u00018\u00008DX\u0004¢\u0006\u0006\u001a\u0004\b\u0006\u0010\u0007R\u0014\u0010\b\u001a\b\u0012\u0004\u0012\u00028\u00000\tX\u0004¢\u0006\u0002\n\u0000¨\u0006\n"}, d2 = {"Lcom/stripe/android/CustomerSession$ActivitySourceRetrievalListener;", "A", "Landroid/app/Activity;", "Lcom/stripe/android/CustomerSession$SourceRetrievalListener;", "activity", "(Landroid/app/Activity;)V", "getActivity", "()Landroid/app/Activity;", "activityRef", "Ljava/lang/ref/WeakReference;", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: CustomerSession.kt */
    public static abstract class ActivitySourceRetrievalListener<A extends Activity> implements SourceRetrievalListener {
        private final WeakReference<A> activityRef;

        public ActivitySourceRetrievalListener(A a2) {
            this.activityRef = new WeakReference<>(a2);
        }

        /* access modifiers changed from: protected */
        public final A getActivity() {
            return (Activity) this.activityRef.get();
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\b&\u0018\u0000*\n\b\u0000\u0010\u0001*\u0004\u0018\u00010\u00022\u00020\u0003B\r\u0012\u0006\u0010\u0004\u001a\u00028\u0000¢\u0006\u0002\u0010\u0005R\u0016\u0010\u0004\u001a\u0004\u0018\u00018\u00008DX\u0004¢\u0006\u0006\u001a\u0004\b\u0006\u0010\u0007R\u0014\u0010\b\u001a\b\u0012\u0004\u0012\u00028\u00000\tX\u0004¢\u0006\u0002\n\u0000¨\u0006\n"}, d2 = {"Lcom/stripe/android/CustomerSession$ActivityPaymentMethodRetrievalListener;", "A", "Landroid/app/Activity;", "Lcom/stripe/android/CustomerSession$PaymentMethodRetrievalListener;", "activity", "(Landroid/app/Activity;)V", "getActivity", "()Landroid/app/Activity;", "activityRef", "Ljava/lang/ref/WeakReference;", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: CustomerSession.kt */
    public static abstract class ActivityPaymentMethodRetrievalListener<A extends Activity> implements PaymentMethodRetrievalListener {
        private final WeakReference<A> activityRef;

        public ActivityPaymentMethodRetrievalListener(A a2) {
            this.activityRef = new WeakReference<>(a2);
        }

        /* access modifiers changed from: protected */
        public final A getActivity() {
            return (Activity) this.activityRef.get();
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000P\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\t\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\b\u0010\u0010\u001a\u00020\u0011H\u0007J\r\u0010\u0012\u001a\u00020\u0011H\u0001¢\u0006\u0002\b\u0013J\b\u0010\u0014\u001a\u00020\u0015H\u0002J\b\u0010\u0016\u001a\u00020\u0011H\u0007J\b\u0010\u0017\u001a\u00020\u000bH\u0007J \u0010\u0018\u001a\u00020\u00112\u0006\u0010\u0019\u001a\u00020\u001a2\u0006\u0010\u001b\u001a\u00020\u001c2\u0006\u0010\u001d\u001a\u00020\u001eH\u0007J.\u0010\u0018\u001a\u00020\u00112\u0006\u0010\u0019\u001a\u00020\u001a2\u0006\u0010\u001b\u001a\u00020\u001c2\n\b\u0002\u0010\u001f\u001a\u0004\u0018\u00010 2\b\b\u0002\u0010\u001d\u001a\u00020\u001eH\u0007R\u000e\u0010\u0003\u001a\u00020\u0004X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\bX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0006XT¢\u0006\u0002\n\u0000R\u001c\u0010\n\u001a\u0004\u0018\u00010\u000bX\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\f\u0010\r\"\u0004\b\u000e\u0010\u000f¨\u0006!"}, d2 = {"Lcom/stripe/android/CustomerSession$Companion;", "", "()V", "CUSTOMER_CACHE_DURATION_MILLISECONDS", "", "KEEP_ALIVE_TIME", "", "KEEP_ALIVE_TIME_UNIT", "Ljava/util/concurrent/TimeUnit;", "THREAD_POOL_SIZE", "instance", "Lcom/stripe/android/CustomerSession;", "getInstance$stripe_release", "()Lcom/stripe/android/CustomerSession;", "setInstance$stripe_release", "(Lcom/stripe/android/CustomerSession;)V", "cancelCallbacks", "", "clearInstance", "clearInstance$stripe_release", "createCoroutineDispatcher", "Lkotlinx/coroutines/CoroutineDispatcher;", "endCustomerSession", "getInstance", "initCustomerSession", "context", "Landroid/content/Context;", "ephemeralKeyProvider", "Lcom/stripe/android/EphemeralKeyProvider;", "shouldPrefetchEphemeralKey", "", "stripeAccountId", "", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: CustomerSession.kt */
    public static final class Companion {
        @JvmStatic
        public final void initCustomerSession(Context context, EphemeralKeyProvider ephemeralKeyProvider) {
            initCustomerSession$default(this, context, ephemeralKeyProvider, (String) null, false, 12, (Object) null);
        }

        @JvmStatic
        public final void initCustomerSession(Context context, EphemeralKeyProvider ephemeralKeyProvider, String str) {
            initCustomerSession$default(this, context, ephemeralKeyProvider, str, false, 8, (Object) null);
        }

        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        public static /* synthetic */ void initCustomerSession$default(Companion companion, Context context, EphemeralKeyProvider ephemeralKeyProvider, String str, boolean z, int i, Object obj) {
            if ((i & 4) != 0) {
                str = null;
            }
            if ((i & 8) != 0) {
                z = true;
            }
            companion.initCustomerSession(context, ephemeralKeyProvider, str, z);
        }

        @JvmStatic
        public final void initCustomerSession(Context context, EphemeralKeyProvider ephemeralKeyProvider, String str, boolean z) {
            CustomerSession customerSession;
            Context context2 = context;
            EphemeralKeyProvider ephemeralKeyProvider2 = ephemeralKeyProvider;
            Intrinsics.checkParameterIsNotNull(context2, "context");
            Intrinsics.checkParameterIsNotNull(ephemeralKeyProvider2, "ephemeralKeyProvider");
            StripeOperationIdFactory stripeOperationIdFactory = new StripeOperationIdFactory();
            Function0 function0 = CustomerSession$Companion$initCustomerSession$timeSupplier$1.INSTANCE;
            OperationIdFactory operationIdFactory = stripeOperationIdFactory;
            EphemeralKeyManager.Factory.Default defaultR = new EphemeralKeyManager.Factory.Default(ephemeralKeyProvider2, z, operationIdFactory, function0);
            String publishableKey = PaymentConfiguration.Companion.getInstance(context2).getPublishableKey();
            Context context3 = context;
            Companion companion = this;
            String str2 = publishableKey;
            String str3 = str;
            OperationIdFactory operationIdFactory2 = operationIdFactory;
            Function0 function02 = function0;
            new CustomerSession(context3, new StripeApiRepository(context3, publishableKey, Stripe.Companion.getAppInfo(), (Logger) null, (ApiRequestExecutor) null, (AnalyticsRequestExecutor) null, (FingerprintDataRepository) null, (ApiFingerprintParamsFactory) null, (AnalyticsDataFactory) null, (FingerprintParamsUtils) null, (CoroutineContext) null, (String) null, (String) null, 8184, (DefaultConstructorMarker) null), str2, str3, companion.createCoroutineDispatcher(), operationIdFactory2, function02, defaultR);
            companion.setInstance$stripe_release(customerSession);
        }

        @JvmStatic
        public final void initCustomerSession(Context context, EphemeralKeyProvider ephemeralKeyProvider, boolean z) {
            Intrinsics.checkParameterIsNotNull(context, "context");
            Intrinsics.checkParameterIsNotNull(ephemeralKeyProvider, "ephemeralKeyProvider");
            initCustomerSession(context, ephemeralKeyProvider, (String) null, z);
        }

        public final CustomerSession getInstance$stripe_release() {
            return CustomerSession.instance;
        }

        public final void setInstance$stripe_release(CustomerSession customerSession) {
            CustomerSession.instance = customerSession;
        }

        @JvmStatic
        public final CustomerSession getInstance() {
            CustomerSession instance$stripe_release = getInstance$stripe_release();
            if (instance$stripe_release != null) {
                return instance$stripe_release;
            }
            throw new IllegalStateException("Attempted to get instance of CustomerSession without initialization.".toString());
        }

        @JvmStatic
        public final void endCustomerSession() {
            clearInstance$stripe_release();
        }

        public final /* synthetic */ void clearInstance$stripe_release() {
            Companion companion = this;
            companion.cancelCallbacks();
            companion.setInstance$stripe_release((CustomerSession) null);
        }

        @JvmStatic
        public final void cancelCallbacks() {
            CustomerSession instance$stripe_release = getInstance$stripe_release();
            if (instance$stripe_release != null) {
                instance$stripe_release.cancel$stripe_release();
            }
        }

        /* access modifiers changed from: private */
        public final CoroutineDispatcher createCoroutineDispatcher() {
            return ExecutorsKt.from((ExecutorService) new ThreadPoolExecutor(3, 3, (long) 2, CustomerSession.KEEP_ALIVE_TIME_UNIT, new LinkedBlockingQueue()));
        }
    }
}
