package com.stripe.android;

import android.os.Handler;
import com.stripe.android.ApiRequest;
import com.stripe.android.CustomerSessionRunnableFactory;
import com.stripe.android.EphemeralOperation;
import com.stripe.android.exception.StripeException;
import com.stripe.android.model.ListPaymentMethodsParams;
import com.stripe.android.model.PaymentMethod;
import java.util.List;
import kotlin.Metadata;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0015\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002*\u0001\u0000\b\n\u0018\u00002\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00030\u00020\u0001J\u000e\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002H\u0016¨\u0006\u0005"}, d2 = {"com/stripe/android/CustomerSessionRunnableFactory$createGetPaymentMethodsRunnable$1", "Lcom/stripe/android/CustomerSessionRunnableFactory$CustomerSessionRunnable;", "", "Lcom/stripe/android/model/PaymentMethod;", "createMessageObject", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: CustomerSessionRunnableFactory.kt */
public final class CustomerSessionRunnableFactory$createGetPaymentMethodsRunnable$1 extends CustomerSessionRunnableFactory.CustomerSessionRunnable<List<? extends PaymentMethod>> {
    final /* synthetic */ EphemeralKey $key;
    final /* synthetic */ EphemeralOperation.Customer.GetPaymentMethods $operation;
    final /* synthetic */ CustomerSessionRunnableFactory this$0;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    CustomerSessionRunnableFactory$createGetPaymentMethodsRunnable$1(CustomerSessionRunnableFactory customerSessionRunnableFactory, EphemeralKey ephemeralKey, EphemeralOperation.Customer.GetPaymentMethods getPaymentMethods, Handler handler, CustomerSessionRunnableFactory.ResultType resultType, String str) {
        super(handler, resultType, str);
        this.this$0 = customerSessionRunnableFactory;
        this.$key = ephemeralKey;
        this.$operation = getPaymentMethods;
    }

    /* renamed from: createMessageObject */
    public List<PaymentMethod> createMessageObject$stripe_release() throws StripeException {
        return this.this$0.stripeRepository.getPaymentMethods(new ListPaymentMethodsParams(this.$key.getObjectId$stripe_release(), this.$operation.getType$stripe_release(), this.$operation.getLimit$stripe_release(), this.$operation.getEndingBefore$stripe_release(), this.$operation.getStartingAfter$stripe_release()), this.this$0.publishableKey, this.$operation.getProductUsage$stripe_release(), new ApiRequest.Options(this.$key.getSecret(), this.this$0.stripeAccountId, (String) null, 4, (DefaultConstructorMarker) null));
    }
}
