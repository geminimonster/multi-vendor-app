package com.stripe.android;

import com.stripe.android.model.PaymentMethod;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLParameters;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import kotlin.Metadata;
import kotlin.Result;
import kotlin.ResultKt;
import kotlin.TypeCastException;
import kotlin.collections.CollectionsKt;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.SpreadBuilder;

@Metadata(bv = {1, 0, 3}, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0011\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0010\b\u0000\u0018\u0000  2\u00020\u0001:\u0001 B\u0007\b\u0010¢\u0006\u0002\u0010\u0002B\u0015\b\u0012\u0012\f\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004¢\u0006\u0002\u0010\u0006B\u0015\u0012\u0006\u0010\u0007\u001a\u00020\b\u0012\u0006\u0010\t\u001a\u00020\b¢\u0006\u0002\u0010\nJ\u001a\u0010\f\u001a\u0004\u0018\u00010\r2\u0006\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u0011H\u0016J*\u0010\f\u001a\u0004\u0018\u00010\r2\u0006\u0010\u0012\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0013\u001a\u00020\u000f2\u0006\u0010\u0014\u001a\u00020\u0011H\u0016J*\u0010\f\u001a\u0004\u0018\u00010\r2\u0006\u0010\u0015\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u00052\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0016\u001a\u00020\bH\u0016J\u001a\u0010\f\u001a\u0004\u0018\u00010\r2\u0006\u0010\u000e\u001a\u00020\u00052\u0006\u0010\u0010\u001a\u00020\u0011H\u0016J*\u0010\f\u001a\u0004\u0018\u00010\r2\u0006\u0010\u000e\u001a\u00020\u00052\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0017\u001a\u00020\u000f2\u0006\u0010\u0014\u001a\u00020\u0011H\u0016J\u0010\u0010\u0018\u001a\u00020\r2\u0006\u0010\u0019\u001a\u00020\rH\u0002J\u0013\u0010\u001a\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004H\u0016¢\u0006\u0002\u0010\u001bJ\u001f\u0010\u001c\u001a\b\u0012\u0004\u0012\u00020\u00050\u00042\f\u0010\u001d\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004¢\u0006\u0002\u0010\u001eJ\u0013\u0010\u001f\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004H\u0016¢\u0006\u0002\u0010\u001bR\u000e\u0010\u000b\u001a\u00020\u0001X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\bX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\bX\u0004¢\u0006\u0002\n\u0000¨\u0006!"}, d2 = {"Lcom/stripe/android/StripeSSLSocketFactory;", "Ljavax/net/ssl/SSLSocketFactory;", "()V", "supportedProtocols", "", "", "([Ljava/lang/String;)V", "tlsv11Supported", "", "tlsv12Supported", "(ZZ)V", "internalFactory", "createSocket", "Ljava/net/Socket;", "host", "Ljava/net/InetAddress;", "port", "", "address", "localAddress", "localPort", "s", "autoClose", "localHost", "fixupSocket", "sock", "getDefaultCipherSuites", "()[Ljava/lang/String;", "getEnabledProtocols", "enabledProtocols", "([Ljava/lang/String;)[Ljava/lang/String;", "getSupportedCipherSuites", "Companion", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: StripeSSLSocketFactory.kt */
public final class StripeSSLSocketFactory extends SSLSocketFactory {
    @Deprecated
    public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
    private static final String TLS_V11_PROTO = "TLSv1.1";
    private static final String TLS_V12_PROTO = "TLSv1.2";
    private final SSLSocketFactory internalFactory;
    private final boolean tlsv11Supported;
    private final boolean tlsv12Supported;

    public StripeSSLSocketFactory(boolean z, boolean z2) {
        this.tlsv11Supported = z;
        this.tlsv12Supported = z2;
        SSLSocketFactory defaultSSLSocketFactory = HttpsURLConnection.getDefaultSSLSocketFactory();
        Intrinsics.checkExpressionValueIsNotNull(defaultSSLSocketFactory, "HttpsURLConnection.getDefaultSSLSocketFactory()");
        this.internalFactory = defaultSSLSocketFactory;
    }

    public StripeSSLSocketFactory() {
        this(Companion.getSUPPORTED_PROTOCOLS());
    }

    public String[] getDefaultCipherSuites() {
        String[] defaultCipherSuites = this.internalFactory.getDefaultCipherSuites();
        Intrinsics.checkExpressionValueIsNotNull(defaultCipherSuites, "internalFactory.defaultCipherSuites");
        return defaultCipherSuites;
    }

    public String[] getSupportedCipherSuites() {
        String[] supportedCipherSuites = this.internalFactory.getSupportedCipherSuites();
        Intrinsics.checkExpressionValueIsNotNull(supportedCipherSuites, "internalFactory.supportedCipherSuites");
        return supportedCipherSuites;
    }

    public Socket createSocket(Socket socket, String str, int i, boolean z) throws IOException {
        Intrinsics.checkParameterIsNotNull(socket, "s");
        Intrinsics.checkParameterIsNotNull(str, "host");
        Socket createSocket = this.internalFactory.createSocket(socket, str, i, z);
        Intrinsics.checkExpressionValueIsNotNull(createSocket, "internalFactory.createSo…s, host, port, autoClose)");
        return fixupSocket(createSocket);
    }

    public Socket createSocket(String str, int i) throws IOException {
        Intrinsics.checkParameterIsNotNull(str, "host");
        Socket createSocket = this.internalFactory.createSocket(str, i);
        Intrinsics.checkExpressionValueIsNotNull(createSocket, "internalFactory.createSocket(host, port)");
        return fixupSocket(createSocket);
    }

    public Socket createSocket(String str, int i, InetAddress inetAddress, int i2) throws IOException {
        Intrinsics.checkParameterIsNotNull(str, "host");
        Intrinsics.checkParameterIsNotNull(inetAddress, "localHost");
        Socket createSocket = this.internalFactory.createSocket(str, i, inetAddress, i2);
        Intrinsics.checkExpressionValueIsNotNull(createSocket, "internalFactory.createSo…rt, localHost, localPort)");
        return fixupSocket(createSocket);
    }

    public Socket createSocket(InetAddress inetAddress, int i) throws IOException {
        Intrinsics.checkParameterIsNotNull(inetAddress, "host");
        Socket createSocket = this.internalFactory.createSocket(inetAddress, i);
        Intrinsics.checkExpressionValueIsNotNull(createSocket, "internalFactory.createSocket(host, port)");
        return fixupSocket(createSocket);
    }

    public Socket createSocket(InetAddress inetAddress, int i, InetAddress inetAddress2, int i2) throws IOException {
        Intrinsics.checkParameterIsNotNull(inetAddress, PaymentMethod.BillingDetails.PARAM_ADDRESS);
        Intrinsics.checkParameterIsNotNull(inetAddress2, "localAddress");
        Socket createSocket = this.internalFactory.createSocket(inetAddress, i, inetAddress2, i2);
        Intrinsics.checkExpressionValueIsNotNull(createSocket, "internalFactory.createSo… localAddress, localPort)");
        return fixupSocket(createSocket);
    }

    private final Socket fixupSocket(Socket socket) {
        if (socket instanceof SSLSocket) {
            SSLSocket sSLSocket = (SSLSocket) socket;
            String[] enabledProtocols = sSLSocket.getEnabledProtocols();
            Intrinsics.checkExpressionValueIsNotNull(enabledProtocols, "sock.enabledProtocols");
            sSLSocket.setEnabledProtocols(getEnabledProtocols(enabledProtocols));
        }
        return socket;
    }

    public final String[] getEnabledProtocols(String[] strArr) {
        Intrinsics.checkParameterIsNotNull(strArr, "enabledProtocols");
        SpreadBuilder spreadBuilder = new SpreadBuilder(3);
        spreadBuilder.addSpread(strArr);
        String str = null;
        spreadBuilder.add(this.tlsv11Supported ? TLS_V11_PROTO : null);
        if (this.tlsv12Supported) {
            str = TLS_V12_PROTO;
        }
        spreadBuilder.add(str);
        Object[] array = CollectionsKt.toSet(CollectionsKt.listOfNotNull((T[]) (String[]) spreadBuilder.toArray(new String[spreadBuilder.size()]))).toArray(new String[0]);
        if (array != null) {
            return (String[]) array;
        }
        throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0011\n\u0002\u0010\u000e\n\u0002\b\u0005\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u001a\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u00048BX\u0004¢\u0006\u0006\u001a\u0004\b\u0006\u0010\u0007R\u000e\u0010\b\u001a\u00020\u0005XT¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0005XT¢\u0006\u0002\n\u0000¨\u0006\n"}, d2 = {"Lcom/stripe/android/StripeSSLSocketFactory$Companion;", "", "()V", "SUPPORTED_PROTOCOLS", "", "", "getSUPPORTED_PROTOCOLS", "()[Ljava/lang/String;", "TLS_V11_PROTO", "TLS_V12_PROTO", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: StripeSSLSocketFactory.kt */
    private static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        /* access modifiers changed from: private */
        public final String[] getSUPPORTED_PROTOCOLS() {
            String[] strArr;
            Companion unused = StripeSSLSocketFactory.Companion;
            try {
                Result.Companion companion = Result.Companion;
                SSLContext sSLContext = SSLContext.getDefault();
                Intrinsics.checkExpressionValueIsNotNull(sSLContext, "SSLContext.getDefault()");
                SSLParameters supportedSSLParameters = sSLContext.getSupportedSSLParameters();
                Intrinsics.checkExpressionValueIsNotNull(supportedSSLParameters, "SSLContext.getDefault().supportedSSLParameters");
                strArr = Result.m4constructorimpl(supportedSSLParameters.getProtocols());
            } catch (Throwable th) {
                Result.Companion companion2 = Result.Companion;
                strArr = Result.m4constructorimpl(ResultKt.createFailure(th));
            }
            String[] strArr2 = new String[0];
            if (Result.m10isFailureimpl(strArr)) {
                strArr = strArr2;
            }
            Intrinsics.checkExpressionValueIsNotNull(strArr, "runCatching {\n          …etOrDefault(emptyArray())");
            return (String[]) strArr;
        }
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private StripeSSLSocketFactory(java.lang.String[] r8) {
        /*
            r7 = this;
            int r0 = r8.length
            r1 = 0
            r2 = 0
        L_0x0003:
            r3 = 1
            if (r2 >= r0) goto L_0x0015
            r4 = r8[r2]
            java.lang.String r5 = "TLSv1.1"
            boolean r4 = kotlin.jvm.internal.Intrinsics.areEqual((java.lang.Object) r4, (java.lang.Object) r5)
            if (r4 == 0) goto L_0x0012
            r0 = 1
            goto L_0x0016
        L_0x0012:
            int r2 = r2 + 1
            goto L_0x0003
        L_0x0015:
            r0 = 0
        L_0x0016:
            int r2 = r8.length
            r4 = 0
        L_0x0018:
            if (r4 >= r2) goto L_0x0029
            r5 = r8[r4]
            java.lang.String r6 = "TLSv1.2"
            boolean r5 = kotlin.jvm.internal.Intrinsics.areEqual((java.lang.Object) r5, (java.lang.Object) r6)
            if (r5 == 0) goto L_0x0026
            r1 = 1
            goto L_0x0029
        L_0x0026:
            int r4 = r4 + 1
            goto L_0x0018
        L_0x0029:
            r7.<init>(r0, r1)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.stripe.android.StripeSSLSocketFactory.<init>(java.lang.String[]):void");
    }
}
