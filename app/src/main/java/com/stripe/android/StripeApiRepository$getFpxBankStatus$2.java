package com.stripe.android;

import androidx.lifecycle.LiveDataScope;
import com.stripe.android.ApiRequest;
import com.stripe.android.model.FpxBankStatuses;
import com.stripe.android.model.parsers.FpxBankStatusesJsonParser;
import kotlin.Metadata;
import kotlin.ResultKt;
import kotlin.TuplesKt;
import kotlin.Unit;
import kotlin.collections.MapsKt;
import kotlin.coroutines.Continuation;
import kotlin.coroutines.intrinsics.IntrinsicsKt;
import kotlin.coroutines.jvm.internal.DebugMetadata;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\u0010\u0000\u001a\u00020\u0001*\b\u0012\u0004\u0012\u00020\u00030\u0002H@¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"<anonymous>", "", "Landroidx/lifecycle/LiveDataScope;", "Lcom/stripe/android/model/FpxBankStatuses;", "invoke", "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;"}, k = 3, mv = {1, 1, 16})
@DebugMetadata(c = "com.stripe.android.StripeApiRepository$getFpxBankStatus$2", f = "StripeApiRepository.kt", i = {0, 0}, l = {814}, m = "invokeSuspend", n = {"$this$liveData", "it"}, s = {"L$0", "L$1"})
/* compiled from: StripeApiRepository.kt */
final class StripeApiRepository$getFpxBankStatus$2 extends SuspendLambda implements Function2<LiveDataScope<FpxBankStatuses>, Continuation<? super Unit>, Object> {
    final /* synthetic */ ApiRequest.Options $options;
    Object L$0;
    Object L$1;
    int label;
    private LiveDataScope p$;
    final /* synthetic */ StripeApiRepository this$0;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    StripeApiRepository$getFpxBankStatus$2(StripeApiRepository stripeApiRepository, ApiRequest.Options options, Continuation continuation) {
        super(2, continuation);
        this.this$0 = stripeApiRepository;
        this.$options = options;
    }

    public final Continuation<Unit> create(Object obj, Continuation<?> continuation) {
        Intrinsics.checkParameterIsNotNull(continuation, "completion");
        StripeApiRepository$getFpxBankStatus$2 stripeApiRepository$getFpxBankStatus$2 = new StripeApiRepository$getFpxBankStatus$2(this.this$0, this.$options, continuation);
        stripeApiRepository$getFpxBankStatus$2.p$ = (LiveDataScope) obj;
        return stripeApiRepository$getFpxBankStatus$2;
    }

    public final Object invoke(Object obj, Object obj2) {
        return ((StripeApiRepository$getFpxBankStatus$2) create(obj, (Continuation) obj2)).invokeSuspend(Unit.INSTANCE);
    }

    public final Object invokeSuspend(Object obj) {
        Object coroutine_suspended = IntrinsicsKt.getCOROUTINE_SUSPENDED();
        int i = this.label;
        if (i == 0) {
            ResultKt.throwOnFailure(obj);
            LiveDataScope liveDataScope = this.p$;
            StripeApiRepository stripeApiRepository = this.this$0;
            StripeResponse makeApiRequest$stripe_release = stripeApiRepository.makeApiRequest$stripe_release(stripeApiRepository.apiRequestFactory.createGet(StripeApiRepository.Companion.getApiUrl("fpx/bank_statuses"), this.$options, MapsKt.mapOf(TuplesKt.to("account_holder_type", "individual"))));
            FpxBankStatuses parse = new FpxBankStatusesJsonParser().parse(makeApiRequest$stripe_release.getResponseJson$stripe_release());
            this.L$0 = liveDataScope;
            this.L$1 = makeApiRequest$stripe_release;
            this.label = 1;
            if (liveDataScope.emit(parse, this) == coroutine_suspended) {
                return coroutine_suspended;
            }
        } else if (i == 1) {
            StripeResponse stripeResponse = (StripeResponse) this.L$1;
            LiveDataScope liveDataScope2 = (LiveDataScope) this.L$0;
            ResultKt.throwOnFailure(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return Unit.INSTANCE;
    }
}
