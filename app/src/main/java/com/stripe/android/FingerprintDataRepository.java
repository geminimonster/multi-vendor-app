package com.stripe.android;

import android.content.Context;
import com.stripe.android.FingerprintDataStore;
import com.stripe.android.FingerprintRequestExecutor;
import kotlin.Metadata;
import kotlin.coroutines.Continuation;
import kotlin.coroutines.CoroutineContext;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.Intrinsics;
import kotlinx.coroutines.CoroutineDispatcher;
import kotlinx.coroutines.CoroutineScope;
import kotlinx.coroutines.CoroutineScopeKt;
import kotlinx.coroutines.CoroutineStart;
import kotlinx.coroutines.Dispatchers;
import kotlinx.coroutines.Job;
import kotlinx.coroutines.SupervisorKt;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\b`\u0018\u00002\u00020\u0001:\u0001\bJ\n\u0010\u0002\u001a\u0004\u0018\u00010\u0003H&J\b\u0010\u0004\u001a\u00020\u0005H&J\u0010\u0010\u0006\u001a\u00020\u00052\u0006\u0010\u0007\u001a\u00020\u0003H&¨\u0006\t"}, d2 = {"Lcom/stripe/android/FingerprintDataRepository;", "", "get", "Lcom/stripe/android/FingerprintData;", "refresh", "", "save", "fingerprintData", "Default", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: FingerprintDataRepository.kt */
public interface FingerprintDataRepository {
    FingerprintData get();

    void refresh();

    void save(FingerprintData fingerprintData);

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000F\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0018\u00002\u00020\u0001B\u000f\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004B)\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\b\u0012\b\b\u0002\u0010\t\u001a\u00020\n\u0012\b\b\u0002\u0010\u000b\u001a\u00020\f¢\u0006\u0002\u0010\rJ\n\u0010\u0013\u001a\u0004\u0018\u00010\u000fH\u0016J\b\u0010\u0014\u001a\u00020\u0015H\u0016J\u0010\u0010\u0016\u001a\u00020\u00152\u0006\u0010\u0017\u001a\u00020\u000fH\u0016R\u0010\u0010\u000e\u001a\u0004\u0018\u00010\u000fX\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\fX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\bX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u00120\u0011X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0018"}, d2 = {"Lcom/stripe/android/FingerprintDataRepository$Default;", "Lcom/stripe/android/FingerprintDataRepository;", "context", "Landroid/content/Context;", "(Landroid/content/Context;)V", "store", "Lcom/stripe/android/FingerprintDataStore;", "fingerprintRequestFactory", "Lcom/stripe/android/FingerprintRequestFactory;", "fingerprintRequestExecutor", "Lcom/stripe/android/FingerprintRequestExecutor;", "coroutineScope", "Lkotlinx/coroutines/CoroutineScope;", "(Lcom/stripe/android/FingerprintDataStore;Lcom/stripe/android/FingerprintRequestFactory;Lcom/stripe/android/FingerprintRequestExecutor;Lkotlinx/coroutines/CoroutineScope;)V", "cachedFingerprintData", "Lcom/stripe/android/FingerprintData;", "timestampSupplier", "Lkotlin/Function0;", "", "get", "refresh", "", "save", "fingerprintData", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: FingerprintDataRepository.kt */
    public static final class Default implements FingerprintDataRepository {
        /* access modifiers changed from: private */
        public FingerprintData cachedFingerprintData;
        private final CoroutineScope coroutineScope;
        /* access modifiers changed from: private */
        public final FingerprintRequestExecutor fingerprintRequestExecutor;
        /* access modifiers changed from: private */
        public final FingerprintRequestFactory fingerprintRequestFactory;
        /* access modifiers changed from: private */
        public final FingerprintDataStore store;
        /* access modifiers changed from: private */
        public final Function0<Long> timestampSupplier;

        public Default(FingerprintDataStore fingerprintDataStore, FingerprintRequestFactory fingerprintRequestFactory2, FingerprintRequestExecutor fingerprintRequestExecutor2, CoroutineScope coroutineScope2) {
            Intrinsics.checkParameterIsNotNull(fingerprintDataStore, "store");
            Intrinsics.checkParameterIsNotNull(fingerprintRequestFactory2, "fingerprintRequestFactory");
            Intrinsics.checkParameterIsNotNull(fingerprintRequestExecutor2, "fingerprintRequestExecutor");
            Intrinsics.checkParameterIsNotNull(coroutineScope2, "coroutineScope");
            this.store = fingerprintDataStore;
            this.fingerprintRequestFactory = fingerprintRequestFactory2;
            this.fingerprintRequestExecutor = fingerprintRequestExecutor2;
            this.coroutineScope = coroutineScope2;
            this.timestampSupplier = FingerprintDataRepository$Default$timestampSupplier$1.INSTANCE;
        }

        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ Default(FingerprintDataStore fingerprintDataStore, FingerprintRequestFactory fingerprintRequestFactory2, FingerprintRequestExecutor fingerprintRequestExecutor2, CoroutineScope coroutineScope2, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this(fingerprintDataStore, fingerprintRequestFactory2, (i & 4) != 0 ? new FingerprintRequestExecutor.Default((CoroutineDispatcher) null, (ConnectionFactory) null, 3, (DefaultConstructorMarker) null) : fingerprintRequestExecutor2, (i & 8) != 0 ? CoroutineScopeKt.CoroutineScope(Dispatchers.getMain().plus(SupervisorKt.SupervisorJob$default((Job) null, 1, (Object) null))) : coroutineScope2);
        }

        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public Default(Context context) {
            this(new FingerprintDataStore.Default(context), new FingerprintRequestFactory(context), (FingerprintRequestExecutor) null, (CoroutineScope) null, 12, (DefaultConstructorMarker) null);
            Intrinsics.checkParameterIsNotNull(context, "context");
        }

        public void refresh() {
            if (Stripe.Companion.getAdvancedFraudSignalsEnabled()) {
                Job unused = BuildersKt__Builders_commonKt.launch$default(this.coroutineScope, (CoroutineContext) null, (CoroutineStart) null, new FingerprintDataRepository$Default$refresh$1(this, (Continuation) null), 3, (Object) null);
            }
        }

        public FingerprintData get() {
            FingerprintData fingerprintData = this.cachedFingerprintData;
            if (Stripe.Companion.getAdvancedFraudSignalsEnabled()) {
                return fingerprintData;
            }
            return null;
        }

        public void save(FingerprintData fingerprintData) {
            Intrinsics.checkParameterIsNotNull(fingerprintData, "fingerprintData");
            this.cachedFingerprintData = fingerprintData;
            this.store.save(fingerprintData);
        }
    }
}
