package com.stripe.android;

import com.facebook.internal.NativeProtocol;
import com.stripe.android.ApiRequest;
import com.stripe.android.RequestHeadersFactory;
import com.stripe.android.StripeRequest;
import java.util.Locale;
import java.util.Map;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000N\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\r\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0004\b\b\u0018\u0000 ,2\u00020\u0001:\u0002,-B+\u0012\u0010\u0010\u0002\u001a\f\u0012\u0004\u0012\u00020\u0004\u0012\u0002\b\u00030\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\n\b\u0002\u0010\u0007\u001a\u0004\u0018\u00010\b¢\u0006\u0002\u0010\tJ\u0013\u0010\u001f\u001a\f\u0012\u0004\u0012\u00020\u0004\u0012\u0002\b\u00030\u0003HÆ\u0003J\u000e\u0010 \u001a\u00020\u0006HÀ\u0003¢\u0006\u0002\b!J\u0010\u0010\"\u001a\u0004\u0018\u00010\bHÀ\u0003¢\u0006\u0002\b#J3\u0010$\u001a\u00020\u00002\u0012\b\u0002\u0010\u0002\u001a\f\u0012\u0004\u0012\u00020\u0004\u0012\u0002\b\u00030\u00032\b\b\u0002\u0010\u0005\u001a\u00020\u00062\n\b\u0002\u0010\u0007\u001a\u0004\u0018\u00010\bHÆ\u0001J\u0013\u0010%\u001a\u00020&2\b\u0010'\u001a\u0004\u0018\u00010(HÖ\u0003J\t\u0010)\u001a\u00020*HÖ\u0001J\t\u0010+\u001a\u00020\u0004HÖ\u0001R\u0016\u0010\u0007\u001a\u0004\u0018\u00010\bX\u0004¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u0014\u0010\f\u001a\u00020\u0004XD¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000eR\u0014\u0010\u000f\u001a\u00020\u0010X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u0012R\u0014\u0010\u0013\u001a\u00020\u0014X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0015\u0010\u0016R\u0014\u0010\u0017\u001a\u00020\u0018X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0019\u0010\u001aR\u0014\u0010\u0005\u001a\u00020\u0006X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u001b\u0010\u001cR\u001e\u0010\u0002\u001a\f\u0012\u0004\u0012\u00020\u0004\u0012\u0002\b\u00030\u0003X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u001d\u0010\u001e¨\u0006."}, d2 = {"Lcom/stripe/android/AnalyticsRequest;", "Lcom/stripe/android/StripeRequest;", "params", "", "", "options", "Lcom/stripe/android/ApiRequest$Options;", "appInfo", "Lcom/stripe/android/AppInfo;", "(Ljava/util/Map;Lcom/stripe/android/ApiRequest$Options;Lcom/stripe/android/AppInfo;)V", "getAppInfo$stripe_release", "()Lcom/stripe/android/AppInfo;", "baseUrl", "getBaseUrl", "()Ljava/lang/String;", "headersFactory", "Lcom/stripe/android/RequestHeadersFactory;", "getHeadersFactory", "()Lcom/stripe/android/RequestHeadersFactory;", "method", "Lcom/stripe/android/StripeRequest$Method;", "getMethod", "()Lcom/stripe/android/StripeRequest$Method;", "mimeType", "Lcom/stripe/android/StripeRequest$MimeType;", "getMimeType", "()Lcom/stripe/android/StripeRequest$MimeType;", "getOptions$stripe_release", "()Lcom/stripe/android/ApiRequest$Options;", "getParams", "()Ljava/util/Map;", "component1", "component2", "component2$stripe_release", "component3", "component3$stripe_release", "copy", "equals", "", "other", "", "hashCode", "", "toString", "Companion", "Factory", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: AnalyticsRequest.kt */
public final class AnalyticsRequest extends StripeRequest {
    public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
    public static final String HOST = "https://q.stripe.com";
    private final AppInfo appInfo;
    private final String baseUrl;
    private final RequestHeadersFactory headersFactory;
    private final StripeRequest.Method method;
    private final StripeRequest.MimeType mimeType;
    private final ApiRequest.Options options;
    private final Map<String, ?> params;

    public static /* synthetic */ AnalyticsRequest copy$default(AnalyticsRequest analyticsRequest, Map<String, ?> map, ApiRequest.Options options2, AppInfo appInfo2, int i, Object obj) {
        if ((i & 1) != 0) {
            map = analyticsRequest.getParams();
        }
        if ((i & 2) != 0) {
            options2 = analyticsRequest.options;
        }
        if ((i & 4) != 0) {
            appInfo2 = analyticsRequest.appInfo;
        }
        return analyticsRequest.copy(map, options2, appInfo2);
    }

    public final Map<String, ?> component1() {
        return getParams();
    }

    public final ApiRequest.Options component2$stripe_release() {
        return this.options;
    }

    public final AppInfo component3$stripe_release() {
        return this.appInfo;
    }

    public final AnalyticsRequest copy(Map<String, ?> map, ApiRequest.Options options2, AppInfo appInfo2) {
        Intrinsics.checkParameterIsNotNull(map, NativeProtocol.WEB_DIALOG_PARAMS);
        Intrinsics.checkParameterIsNotNull(options2, "options");
        return new AnalyticsRequest(map, options2, appInfo2);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof AnalyticsRequest)) {
            return false;
        }
        AnalyticsRequest analyticsRequest = (AnalyticsRequest) obj;
        return Intrinsics.areEqual((Object) getParams(), (Object) analyticsRequest.getParams()) && Intrinsics.areEqual((Object) this.options, (Object) analyticsRequest.options) && Intrinsics.areEqual((Object) this.appInfo, (Object) analyticsRequest.appInfo);
    }

    public int hashCode() {
        Map<String, ?> params2 = getParams();
        int i = 0;
        int hashCode = (params2 != null ? params2.hashCode() : 0) * 31;
        ApiRequest.Options options2 = this.options;
        int hashCode2 = (hashCode + (options2 != null ? options2.hashCode() : 0)) * 31;
        AppInfo appInfo2 = this.appInfo;
        if (appInfo2 != null) {
            i = appInfo2.hashCode();
        }
        return hashCode2 + i;
    }

    public String toString() {
        return "AnalyticsRequest(params=" + getParams() + ", options=" + this.options + ", appInfo=" + this.appInfo + ")";
    }

    public Map<String, ?> getParams() {
        return this.params;
    }

    public final ApiRequest.Options getOptions$stripe_release() {
        return this.options;
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ AnalyticsRequest(Map map, ApiRequest.Options options2, AppInfo appInfo2, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(map, options2, (i & 4) != 0 ? null : appInfo2);
    }

    public final AppInfo getAppInfo$stripe_release() {
        return this.appInfo;
    }

    public AnalyticsRequest(Map<String, ?> map, ApiRequest.Options options2, AppInfo appInfo2) {
        Intrinsics.checkParameterIsNotNull(map, NativeProtocol.WEB_DIALOG_PARAMS);
        Intrinsics.checkParameterIsNotNull(options2, "options");
        this.params = map;
        this.options = options2;
        this.appInfo = appInfo2;
        this.method = StripeRequest.Method.GET;
        this.baseUrl = HOST;
        this.mimeType = StripeRequest.MimeType.Form;
        this.headersFactory = new RequestHeadersFactory.Api(this.options, this.appInfo, (Locale) null, (Function1) null, (String) null, (String) null, 60, (DefaultConstructorMarker) null);
    }

    public StripeRequest.Method getMethod() {
        return this.method;
    }

    public String getBaseUrl() {
        return this.baseUrl;
    }

    public StripeRequest.MimeType getMimeType() {
        return this.mimeType;
    }

    public RequestHeadersFactory getHeadersFactory() {
        return this.headersFactory;
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u000f\u0012\b\b\u0002\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J3\u0010\u0005\u001a\u00020\u00062\u0010\u0010\u0007\u001a\f\u0012\u0004\u0012\u00020\t\u0012\u0002\b\u00030\b2\u0006\u0010\n\u001a\u00020\u000b2\n\b\u0002\u0010\f\u001a\u0004\u0018\u00010\rH\u0000¢\u0006\u0002\b\u000eR\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\u000f"}, d2 = {"Lcom/stripe/android/AnalyticsRequest$Factory;", "", "logger", "Lcom/stripe/android/Logger;", "(Lcom/stripe/android/Logger;)V", "create", "Lcom/stripe/android/AnalyticsRequest;", "params", "", "", "options", "Lcom/stripe/android/ApiRequest$Options;", "appInfo", "Lcom/stripe/android/AppInfo;", "create$stripe_release", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: AnalyticsRequest.kt */
    public static final class Factory {
        private final Logger logger;

        public Factory() {
            this((Logger) null, 1, (DefaultConstructorMarker) null);
        }

        public Factory(Logger logger2) {
            Intrinsics.checkParameterIsNotNull(logger2, "logger");
            this.logger = logger2;
        }

        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ Factory(Logger logger2, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this((i & 1) != 0 ? Logger.Companion.noop$stripe_release() : logger2);
        }

        public static /* synthetic */ AnalyticsRequest create$stripe_release$default(Factory factory, Map map, ApiRequest.Options options, AppInfo appInfo, int i, Object obj) {
            if ((i & 4) != 0) {
                appInfo = null;
            }
            return factory.create$stripe_release(map, options, appInfo);
        }

        public final /* synthetic */ AnalyticsRequest create$stripe_release(Map<String, ?> map, ApiRequest.Options options, AppInfo appInfo) {
            Intrinsics.checkParameterIsNotNull(map, NativeProtocol.WEB_DIALOG_PARAMS);
            Intrinsics.checkParameterIsNotNull(options, "options");
            Logger logger2 = this.logger;
            logger2.info("Event: " + map.get("event"));
            return new AnalyticsRequest(map, options, appInfo);
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\u0005"}, d2 = {"Lcom/stripe/android/AnalyticsRequest$Companion;", "", "()V", "HOST", "", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: AnalyticsRequest.kt */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }
}
