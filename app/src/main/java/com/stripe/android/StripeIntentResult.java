package com.stripe.android;

import android.os.Parcel;
import android.os.Parcelable;
import com.stripe.android.model.PaymentMethod;
import com.stripe.android.model.StripeIntent;
import com.stripe.android.model.StripeModel;
import java.lang.annotation.RetentionPolicy;
import java.util.Objects;
import java.util.Set;
import kotlin.Metadata;
import kotlin.annotation.AnnotationRetention;
import kotlin.annotation.Retention;
import kotlin.collections.CollectionsKt;
import kotlin.collections.SetsKt;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u000b\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\b&\u0018\u0000 \u001f*\b\b\u0000\u0010\u0001*\u00020\u00022\u00020\u0003:\u0002\u001f B\u0017\b\u0000\u0012\u0006\u0010\u0004\u001a\u00028\u0000\u0012\u0006\u0010\u0005\u001a\u00020\u0006¢\u0006\u0002\u0010\u0007J\b\u0010\u000f\u001a\u00020\u0006H\u0016J\u001a\u0010\u0010\u001a\u00020\u00062\b\u0010\u0011\u001a\u0004\u0018\u00010\u00122\u0006\u0010\u0005\u001a\u00020\u0006H\u0002J\u0013\u0010\u0013\u001a\u00020\u00142\b\u0010\u0015\u001a\u0004\u0018\u00010\u0016H\u0002J\b\u0010\u0017\u001a\u00020\u0006H\u0016J\u0014\u0010\u0018\u001a\u00020\u00142\n\u0010\u0019\u001a\u0006\u0012\u0002\b\u00030\u0000H\u0002J\u0018\u0010\u001a\u001a\u00020\u001b2\u0006\u0010\u001c\u001a\u00020\u001d2\u0006\u0010\u001e\u001a\u00020\u0006H\u0016R\u0013\u0010\u0004\u001a\u00028\u0000¢\u0006\n\n\u0002\u0010\n\u001a\u0004\b\b\u0010\tR\u0019\u0010\u0005\u001a\u00020\u00068F¢\u0006\u000e\n\u0000\u0012\u0004\b\u000b\u0010\f\u001a\u0004\b\r\u0010\u000e¨\u0006!"}, d2 = {"Lcom/stripe/android/StripeIntentResult;", "T", "Lcom/stripe/android/model/StripeIntent;", "Lcom/stripe/android/model/StripeModel;", "intent", "outcome", "", "(Lcom/stripe/android/model/StripeIntent;I)V", "getIntent", "()Lcom/stripe/android/model/StripeIntent;", "Lcom/stripe/android/model/StripeIntent;", "outcome$annotations", "()V", "getOutcome", "()I", "describeContents", "determineOutcome", "stripeIntentStatus", "Lcom/stripe/android/model/StripeIntent$Status;", "equals", "", "other", "", "hashCode", "typedEquals", "setupIntentResult", "writeToParcel", "", "dest", "Landroid/os/Parcel;", "flags", "Companion", "Outcome", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: StripeIntentResult.kt */
public abstract class StripeIntentResult<T extends StripeIntent> implements StripeModel {
    @Deprecated
    public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
    private static final Set<PaymentMethod.Type> PROCESSING_IS_SUCCESS = SetsKt.setOf(PaymentMethod.Type.SepaDebit, PaymentMethod.Type.BacsDebit, PaymentMethod.Type.AuBecsDebit, PaymentMethod.Type.Sofort);
    private final T intent;
    private final int outcome;

    @Metadata(bv = {1, 0, 3}, k = 3, mv = {1, 1, 16})
    public final /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;

        static {
            int[] iArr = new int[StripeIntent.Status.values().length];
            $EnumSwitchMapping$0 = iArr;
            iArr[StripeIntent.Status.RequiresAction.ordinal()] = 1;
            $EnumSwitchMapping$0[StripeIntent.Status.Canceled.ordinal()] = 2;
            $EnumSwitchMapping$0[StripeIntent.Status.RequiresPaymentMethod.ordinal()] = 3;
            $EnumSwitchMapping$0[StripeIntent.Status.Succeeded.ordinal()] = 4;
            $EnumSwitchMapping$0[StripeIntent.Status.RequiresCapture.ordinal()] = 5;
            $EnumSwitchMapping$0[StripeIntent.Status.RequiresConfirmation.ordinal()] = 6;
            $EnumSwitchMapping$0[StripeIntent.Status.Processing.ordinal()] = 7;
        }
    }

    public static /* synthetic */ void outcome$annotations() {
    }

    public int describeContents() {
        return 0;
    }

    public StripeIntentResult(T t, int i) {
        Intrinsics.checkParameterIsNotNull(t, "intent");
        this.intent = t;
        this.outcome = determineOutcome(t.getStatus(), i);
    }

    public final T getIntent() {
        return this.intent;
    }

    public final int getOutcome() {
        return this.outcome;
    }

    private final int determineOutcome(StripeIntent.Status status, int i) {
        if (i != 0) {
            return i;
        }
        if (status == null) {
            return 0;
        }
        switch (WhenMappings.$EnumSwitchMapping$0[status.ordinal()]) {
            case 1:
            case 2:
                return 3;
            case 3:
                return 2;
            case 4:
            case 5:
            case 6:
                return 1;
            case 7:
                Iterable iterable = PROCESSING_IS_SUCCESS;
                PaymentMethod paymentMethod = this.intent.getPaymentMethod();
                return CollectionsKt.contains(iterable, paymentMethod != null ? paymentMethod.type : null) ? 1 : 0;
            default:
                return 0;
        }
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof StripeIntentResult) {
            return typedEquals((StripeIntentResult) obj);
        }
        return false;
    }

    private final boolean typedEquals(StripeIntentResult<?> stripeIntentResult) {
        return Intrinsics.areEqual((Object) this.intent, (Object) stripeIntentResult.intent) && this.outcome == stripeIntentResult.outcome;
    }

    public int hashCode() {
        return Objects.hash(new Object[]{this.intent, Integer.valueOf(this.outcome)});
    }

    public void writeToParcel(Parcel parcel, int i) {
        Intrinsics.checkParameterIsNotNull(parcel, "dest");
        parcel.writeParcelable((Parcelable) this.intent, i);
        parcel.writeInt(this.outcome);
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u001b\n\u0002\b\u0002\b\u0002\u0018\u0000 \u00022\u00020\u0001:\u0001\u0002B\u0000¨\u0006\u0003"}, d2 = {"Lcom/stripe/android/StripeIntentResult$Outcome;", "", "Companion", "stripe_release"}, k = 1, mv = {1, 1, 16})
    @Retention(AnnotationRetention.SOURCE)
    @java.lang.annotation.Retention(RetentionPolicy.SOURCE)
    /* compiled from: StripeIntentResult.kt */
    public @interface Outcome {
        public static final int CANCELED = 3;
        public static final Companion Companion = Companion.$$INSTANCE;
        public static final int FAILED = 2;
        public static final int SUCCEEDED = 1;
        public static final int TIMEDOUT = 4;
        public static final int UNKNOWN = 0;

        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0005\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\t"}, d2 = {"Lcom/stripe/android/StripeIntentResult$Outcome$Companion;", "", "()V", "CANCELED", "", "FAILED", "SUCCEEDED", "TIMEDOUT", "UNKNOWN", "stripe_release"}, k = 1, mv = {1, 1, 16})
        /* compiled from: StripeIntentResult.kt */
        public static final class Companion {
            static final /* synthetic */ Companion $$INSTANCE = new Companion();
            public static final int CANCELED = 3;
            public static final int FAILED = 2;
            public static final int SUCCEEDED = 1;
            public static final int TIMEDOUT = 4;
            public static final int UNKNOWN = 0;

            private Companion() {
            }
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0014\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0006"}, d2 = {"Lcom/stripe/android/StripeIntentResult$Companion;", "", "()V", "PROCESSING_IS_SUCCESS", "", "Lcom/stripe/android/model/PaymentMethod$Type;", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: StripeIntentResult.kt */
    private static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }
}
