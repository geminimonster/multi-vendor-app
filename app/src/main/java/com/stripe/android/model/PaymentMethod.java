package com.stripe.android.model;

import android.os.Parcel;
import android.os.Parcelable;
import com.stripe.android.ObjectBuilder;
import com.stripe.android.model.parsers.PaymentMethodJsonParser;
import com.stripe.android.model.wallets.Wallet;
import java.lang.annotation.RetentionPolicy;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import kotlin.Metadata;
import kotlin.TuplesKt;
import kotlin.annotation.AnnotationRetention;
import kotlin.annotation.Retention;
import kotlin.collections.MapsKt;
import kotlin.collections.SetsKt;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.internal.Intrinsics;
import org.json.JSONObject;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010$\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0015\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u000e\b\b\u0018\u0000 E2\u00020\u0001:\f?@ABCDEFGHIJB½\u0001\b\u0000\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\b\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\b\u0010\b\u001a\u0004\u0018\u00010\t\u0012\n\b\u0002\u0010\n\u001a\u0004\u0018\u00010\u000b\u0012\n\b\u0002\u0010\f\u001a\u0004\u0018\u00010\u0003\u0012\u0016\b\u0002\u0010\r\u001a\u0010\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0003\u0018\u00010\u000e\u0012\n\b\u0002\u0010\u000f\u001a\u0004\u0018\u00010\u0010\u0012\n\b\u0002\u0010\u0011\u001a\u0004\u0018\u00010\u0012\u0012\n\b\u0002\u0010\u0013\u001a\u0004\u0018\u00010\u0014\u0012\n\b\u0002\u0010\u0015\u001a\u0004\u0018\u00010\u0016\u0012\n\b\u0002\u0010\u0017\u001a\u0004\u0018\u00010\u0018\u0012\n\b\u0002\u0010\u0019\u001a\u0004\u0018\u00010\u001a\u0012\n\b\u0002\u0010\u001b\u001a\u0004\u0018\u00010\u001c\u0012\n\b\u0002\u0010\u001d\u001a\u0004\u0018\u00010\u001e¢\u0006\u0002\u0010\u001fJ\u000b\u0010!\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000b\u0010\"\u001a\u0004\u0018\u00010\u0014HÆ\u0003J\u000b\u0010#\u001a\u0004\u0018\u00010\u0016HÆ\u0003J\u000b\u0010$\u001a\u0004\u0018\u00010\u0018HÆ\u0003J\u000b\u0010%\u001a\u0004\u0018\u00010\u001aHÆ\u0003J\u000b\u0010&\u001a\u0004\u0018\u00010\u001cHÆ\u0003J\u000b\u0010'\u001a\u0004\u0018\u00010\u001eHÆ\u0003J\u0010\u0010(\u001a\u0004\u0018\u00010\u0005HÆ\u0003¢\u0006\u0002\u0010)J\t\u0010*\u001a\u00020\u0007HÆ\u0003J\u000b\u0010+\u001a\u0004\u0018\u00010\tHÆ\u0003J\u000b\u0010,\u001a\u0004\u0018\u00010\u000bHÆ\u0003J\u000b\u0010-\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u0017\u0010.\u001a\u0010\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0003\u0018\u00010\u000eHÆ\u0003J\u000b\u0010/\u001a\u0004\u0018\u00010\u0010HÆ\u0003J\u000b\u00100\u001a\u0004\u0018\u00010\u0012HÆ\u0003JÌ\u0001\u00101\u001a\u00020\u00002\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u00072\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\t2\n\b\u0002\u0010\n\u001a\u0004\u0018\u00010\u000b2\n\b\u0002\u0010\f\u001a\u0004\u0018\u00010\u00032\u0016\b\u0002\u0010\r\u001a\u0010\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0003\u0018\u00010\u000e2\n\b\u0002\u0010\u000f\u001a\u0004\u0018\u00010\u00102\n\b\u0002\u0010\u0011\u001a\u0004\u0018\u00010\u00122\n\b\u0002\u0010\u0013\u001a\u0004\u0018\u00010\u00142\n\b\u0002\u0010\u0015\u001a\u0004\u0018\u00010\u00162\n\b\u0002\u0010\u0017\u001a\u0004\u0018\u00010\u00182\n\b\u0002\u0010\u0019\u001a\u0004\u0018\u00010\u001a2\n\b\u0002\u0010\u001b\u001a\u0004\u0018\u00010\u001c2\n\b\u0002\u0010\u001d\u001a\u0004\u0018\u00010\u001eHÆ\u0001¢\u0006\u0002\u00102J\t\u00103\u001a\u000204HÖ\u0001J\u0013\u00105\u001a\u00020\u00072\b\u00106\u001a\u0004\u0018\u000107HÖ\u0003J\t\u00108\u001a\u000204HÖ\u0001J\t\u00109\u001a\u00020\u0003HÖ\u0001J\u0019\u0010:\u001a\u00020;2\u0006\u0010<\u001a\u00020=2\u0006\u0010>\u001a\u000204HÖ\u0001R\u0012\u0010\u0019\u001a\u0004\u0018\u00010\u001a8\u0006X\u0004¢\u0006\u0002\n\u0000R\u0012\u0010\u001b\u001a\u0004\u0018\u00010\u001c8\u0006X\u0004¢\u0006\u0002\n\u0000R\u0012\u0010\n\u001a\u0004\u0018\u00010\u000b8\u0006X\u0004¢\u0006\u0002\n\u0000R\u0012\u0010\u000f\u001a\u0004\u0018\u00010\u00108\u0006X\u0004¢\u0006\u0002\n\u0000R\u0012\u0010\u0011\u001a\u0004\u0018\u00010\u00128\u0006X\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\u0004\u001a\u0004\u0018\u00010\u00058\u0006X\u0004¢\u0006\u0004\n\u0002\u0010 R\u0012\u0010\f\u001a\u0004\u0018\u00010\u00038\u0006X\u0004¢\u0006\u0002\n\u0000R\u0012\u0010\u0013\u001a\u0004\u0018\u00010\u00148\u0006X\u0004¢\u0006\u0002\n\u0000R\u0012\u0010\u0002\u001a\u0004\u0018\u00010\u00038\u0006X\u0004¢\u0006\u0002\n\u0000R\u0012\u0010\u0015\u001a\u0004\u0018\u00010\u00168\u0006X\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u0006\u001a\u00020\u00078\u0006X\u0004¢\u0006\u0002\n\u0000R\u001e\u0010\r\u001a\u0010\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0003\u0018\u00010\u000e8\u0006X\u0004¢\u0006\u0002\n\u0000R\u0012\u0010\u0017\u001a\u0004\u0018\u00010\u00188\u0006X\u0004¢\u0006\u0002\n\u0000R\u0012\u0010\u001d\u001a\u0004\u0018\u00010\u001e8\u0006X\u0004¢\u0006\u0002\n\u0000R\u0012\u0010\b\u001a\u0004\u0018\u00010\t8\u0006X\u0004¢\u0006\u0002\n\u0000¨\u0006K"}, d2 = {"Lcom/stripe/android/model/PaymentMethod;", "Lcom/stripe/android/model/StripeModel;", "id", "", "created", "", "liveMode", "", "type", "Lcom/stripe/android/model/PaymentMethod$Type;", "billingDetails", "Lcom/stripe/android/model/PaymentMethod$BillingDetails;", "customerId", "metadata", "", "card", "Lcom/stripe/android/model/PaymentMethod$Card;", "cardPresent", "Lcom/stripe/android/model/PaymentMethod$CardPresent;", "fpx", "Lcom/stripe/android/model/PaymentMethod$Fpx;", "ideal", "Lcom/stripe/android/model/PaymentMethod$Ideal;", "sepaDebit", "Lcom/stripe/android/model/PaymentMethod$SepaDebit;", "auBecsDebit", "Lcom/stripe/android/model/PaymentMethod$AuBecsDebit;", "bacsDebit", "Lcom/stripe/android/model/PaymentMethod$BacsDebit;", "sofort", "Lcom/stripe/android/model/PaymentMethod$Sofort;", "(Ljava/lang/String;Ljava/lang/Long;ZLcom/stripe/android/model/PaymentMethod$Type;Lcom/stripe/android/model/PaymentMethod$BillingDetails;Ljava/lang/String;Ljava/util/Map;Lcom/stripe/android/model/PaymentMethod$Card;Lcom/stripe/android/model/PaymentMethod$CardPresent;Lcom/stripe/android/model/PaymentMethod$Fpx;Lcom/stripe/android/model/PaymentMethod$Ideal;Lcom/stripe/android/model/PaymentMethod$SepaDebit;Lcom/stripe/android/model/PaymentMethod$AuBecsDebit;Lcom/stripe/android/model/PaymentMethod$BacsDebit;Lcom/stripe/android/model/PaymentMethod$Sofort;)V", "Ljava/lang/Long;", "component1", "component10", "component11", "component12", "component13", "component14", "component15", "component2", "()Ljava/lang/Long;", "component3", "component4", "component5", "component6", "component7", "component8", "component9", "copy", "(Ljava/lang/String;Ljava/lang/Long;ZLcom/stripe/android/model/PaymentMethod$Type;Lcom/stripe/android/model/PaymentMethod$BillingDetails;Ljava/lang/String;Ljava/util/Map;Lcom/stripe/android/model/PaymentMethod$Card;Lcom/stripe/android/model/PaymentMethod$CardPresent;Lcom/stripe/android/model/PaymentMethod$Fpx;Lcom/stripe/android/model/PaymentMethod$Ideal;Lcom/stripe/android/model/PaymentMethod$SepaDebit;Lcom/stripe/android/model/PaymentMethod$AuBecsDebit;Lcom/stripe/android/model/PaymentMethod$BacsDebit;Lcom/stripe/android/model/PaymentMethod$Sofort;)Lcom/stripe/android/model/PaymentMethod;", "describeContents", "", "equals", "other", "", "hashCode", "toString", "writeToParcel", "", "parcel", "Landroid/os/Parcel;", "flags", "AuBecsDebit", "BacsDebit", "BillingDetails", "Builder", "Card", "CardPresent", "Companion", "Fpx", "Ideal", "SepaDebit", "Sofort", "Type", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: PaymentMethod.kt */
public final class PaymentMethod implements StripeModel {
    public static final Parcelable.Creator CREATOR = new Creator();
    public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
    public final AuBecsDebit auBecsDebit;
    public final BacsDebit bacsDebit;
    public final BillingDetails billingDetails;
    public final Card card;
    public final CardPresent cardPresent;
    public final Long created;
    public final String customerId;
    public final Fpx fpx;
    public final String id;
    public final Ideal ideal;
    public final boolean liveMode;
    public final Map<String, String> metadata;
    public final SepaDebit sepaDebit;
    public final Sofort sofort;
    public final Type type;

    @Metadata(bv = {1, 0, 3}, k = 3, mv = {1, 1, 16})
    public static class Creator implements Parcelable.Creator {
        public final Object createFromParcel(Parcel parcel) {
            LinkedHashMap linkedHashMap;
            Parcel parcel2 = parcel;
            Intrinsics.checkParameterIsNotNull(parcel2, "in");
            String readString = parcel.readString();
            Long valueOf = parcel.readInt() != 0 ? Long.valueOf(parcel.readLong()) : null;
            boolean z = parcel.readInt() != 0;
            Type type = parcel.readInt() != 0 ? (Type) Enum.valueOf(Type.class, parcel.readString()) : null;
            BillingDetails billingDetails = parcel.readInt() != 0 ? (BillingDetails) BillingDetails.CREATOR.createFromParcel(parcel2) : null;
            String readString2 = parcel.readString();
            if (parcel.readInt() != 0) {
                int readInt = parcel.readInt();
                linkedHashMap = new LinkedHashMap(readInt);
                while (readInt != 0) {
                    linkedHashMap.put(parcel.readString(), parcel.readString());
                    readInt--;
                }
            } else {
                linkedHashMap = null;
            }
            return new PaymentMethod(readString, valueOf, z, type, billingDetails, readString2, linkedHashMap, parcel.readInt() != 0 ? (Card) Card.CREATOR.createFromParcel(parcel2) : null, parcel.readInt() != 0 ? (CardPresent) CardPresent.CREATOR.createFromParcel(parcel2) : null, parcel.readInt() != 0 ? (Fpx) Fpx.CREATOR.createFromParcel(parcel2) : null, parcel.readInt() != 0 ? (Ideal) Ideal.CREATOR.createFromParcel(parcel2) : null, parcel.readInt() != 0 ? (SepaDebit) SepaDebit.CREATOR.createFromParcel(parcel2) : null, parcel.readInt() != 0 ? (AuBecsDebit) AuBecsDebit.CREATOR.createFromParcel(parcel2) : null, parcel.readInt() != 0 ? (BacsDebit) BacsDebit.CREATOR.createFromParcel(parcel2) : null, parcel.readInt() != 0 ? (Sofort) Sofort.CREATOR.createFromParcel(parcel2) : null);
        }

        public final Object[] newArray(int i) {
            return new PaymentMethod[i];
        }
    }

    public static /* synthetic */ PaymentMethod copy$default(PaymentMethod paymentMethod, String str, Long l, boolean z, Type type2, BillingDetails billingDetails2, String str2, Map map, Card card2, CardPresent cardPresent2, Fpx fpx2, Ideal ideal2, SepaDebit sepaDebit2, AuBecsDebit auBecsDebit2, BacsDebit bacsDebit2, Sofort sofort2, int i, Object obj) {
        PaymentMethod paymentMethod2 = paymentMethod;
        int i2 = i;
        return paymentMethod.copy((i2 & 1) != 0 ? paymentMethod2.id : str, (i2 & 2) != 0 ? paymentMethod2.created : l, (i2 & 4) != 0 ? paymentMethod2.liveMode : z, (i2 & 8) != 0 ? paymentMethod2.type : type2, (i2 & 16) != 0 ? paymentMethod2.billingDetails : billingDetails2, (i2 & 32) != 0 ? paymentMethod2.customerId : str2, (i2 & 64) != 0 ? paymentMethod2.metadata : map, (i2 & 128) != 0 ? paymentMethod2.card : card2, (i2 & 256) != 0 ? paymentMethod2.cardPresent : cardPresent2, (i2 & 512) != 0 ? paymentMethod2.fpx : fpx2, (i2 & 1024) != 0 ? paymentMethod2.ideal : ideal2, (i2 & 2048) != 0 ? paymentMethod2.sepaDebit : sepaDebit2, (i2 & 4096) != 0 ? paymentMethod2.auBecsDebit : auBecsDebit2, (i2 & 8192) != 0 ? paymentMethod2.bacsDebit : bacsDebit2, (i2 & 16384) != 0 ? paymentMethod2.sofort : sofort2);
    }

    @JvmStatic
    public static final PaymentMethod fromJson(JSONObject jSONObject) {
        return Companion.fromJson(jSONObject);
    }

    public final String component1() {
        return this.id;
    }

    public final Fpx component10() {
        return this.fpx;
    }

    public final Ideal component11() {
        return this.ideal;
    }

    public final SepaDebit component12() {
        return this.sepaDebit;
    }

    public final AuBecsDebit component13() {
        return this.auBecsDebit;
    }

    public final BacsDebit component14() {
        return this.bacsDebit;
    }

    public final Sofort component15() {
        return this.sofort;
    }

    public final Long component2() {
        return this.created;
    }

    public final boolean component3() {
        return this.liveMode;
    }

    public final Type component4() {
        return this.type;
    }

    public final BillingDetails component5() {
        return this.billingDetails;
    }

    public final String component6() {
        return this.customerId;
    }

    public final Map<String, String> component7() {
        return this.metadata;
    }

    public final Card component8() {
        return this.card;
    }

    public final CardPresent component9() {
        return this.cardPresent;
    }

    public final PaymentMethod copy(String str, Long l, boolean z, Type type2, BillingDetails billingDetails2, String str2, Map<String, String> map, Card card2, CardPresent cardPresent2, Fpx fpx2, Ideal ideal2, SepaDebit sepaDebit2, AuBecsDebit auBecsDebit2, BacsDebit bacsDebit2, Sofort sofort2) {
        return new PaymentMethod(str, l, z, type2, billingDetails2, str2, map, card2, cardPresent2, fpx2, ideal2, sepaDebit2, auBecsDebit2, bacsDebit2, sofort2);
    }

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof PaymentMethod)) {
            return false;
        }
        PaymentMethod paymentMethod = (PaymentMethod) obj;
        return Intrinsics.areEqual((Object) this.id, (Object) paymentMethod.id) && Intrinsics.areEqual((Object) this.created, (Object) paymentMethod.created) && this.liveMode == paymentMethod.liveMode && Intrinsics.areEqual((Object) this.type, (Object) paymentMethod.type) && Intrinsics.areEqual((Object) this.billingDetails, (Object) paymentMethod.billingDetails) && Intrinsics.areEqual((Object) this.customerId, (Object) paymentMethod.customerId) && Intrinsics.areEqual((Object) this.metadata, (Object) paymentMethod.metadata) && Intrinsics.areEqual((Object) this.card, (Object) paymentMethod.card) && Intrinsics.areEqual((Object) this.cardPresent, (Object) paymentMethod.cardPresent) && Intrinsics.areEqual((Object) this.fpx, (Object) paymentMethod.fpx) && Intrinsics.areEqual((Object) this.ideal, (Object) paymentMethod.ideal) && Intrinsics.areEqual((Object) this.sepaDebit, (Object) paymentMethod.sepaDebit) && Intrinsics.areEqual((Object) this.auBecsDebit, (Object) paymentMethod.auBecsDebit) && Intrinsics.areEqual((Object) this.bacsDebit, (Object) paymentMethod.bacsDebit) && Intrinsics.areEqual((Object) this.sofort, (Object) paymentMethod.sofort);
    }

    public int hashCode() {
        String str = this.id;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        Long l = this.created;
        int hashCode2 = (hashCode + (l != null ? l.hashCode() : 0)) * 31;
        boolean z = this.liveMode;
        if (z) {
            z = true;
        }
        int i2 = (hashCode2 + (z ? 1 : 0)) * 31;
        Type type2 = this.type;
        int hashCode3 = (i2 + (type2 != null ? type2.hashCode() : 0)) * 31;
        BillingDetails billingDetails2 = this.billingDetails;
        int hashCode4 = (hashCode3 + (billingDetails2 != null ? billingDetails2.hashCode() : 0)) * 31;
        String str2 = this.customerId;
        int hashCode5 = (hashCode4 + (str2 != null ? str2.hashCode() : 0)) * 31;
        Map<String, String> map = this.metadata;
        int hashCode6 = (hashCode5 + (map != null ? map.hashCode() : 0)) * 31;
        Card card2 = this.card;
        int hashCode7 = (hashCode6 + (card2 != null ? card2.hashCode() : 0)) * 31;
        CardPresent cardPresent2 = this.cardPresent;
        int hashCode8 = (hashCode7 + (cardPresent2 != null ? cardPresent2.hashCode() : 0)) * 31;
        Fpx fpx2 = this.fpx;
        int hashCode9 = (hashCode8 + (fpx2 != null ? fpx2.hashCode() : 0)) * 31;
        Ideal ideal2 = this.ideal;
        int hashCode10 = (hashCode9 + (ideal2 != null ? ideal2.hashCode() : 0)) * 31;
        SepaDebit sepaDebit2 = this.sepaDebit;
        int hashCode11 = (hashCode10 + (sepaDebit2 != null ? sepaDebit2.hashCode() : 0)) * 31;
        AuBecsDebit auBecsDebit2 = this.auBecsDebit;
        int hashCode12 = (hashCode11 + (auBecsDebit2 != null ? auBecsDebit2.hashCode() : 0)) * 31;
        BacsDebit bacsDebit2 = this.bacsDebit;
        int hashCode13 = (hashCode12 + (bacsDebit2 != null ? bacsDebit2.hashCode() : 0)) * 31;
        Sofort sofort2 = this.sofort;
        if (sofort2 != null) {
            i = sofort2.hashCode();
        }
        return hashCode13 + i;
    }

    public String toString() {
        return "PaymentMethod(id=" + this.id + ", created=" + this.created + ", liveMode=" + this.liveMode + ", type=" + this.type + ", billingDetails=" + this.billingDetails + ", customerId=" + this.customerId + ", metadata=" + this.metadata + ", card=" + this.card + ", cardPresent=" + this.cardPresent + ", fpx=" + this.fpx + ", ideal=" + this.ideal + ", sepaDebit=" + this.sepaDebit + ", auBecsDebit=" + this.auBecsDebit + ", bacsDebit=" + this.bacsDebit + ", sofort=" + this.sofort + ")";
    }

    public void writeToParcel(Parcel parcel, int i) {
        Intrinsics.checkParameterIsNotNull(parcel, "parcel");
        parcel.writeString(this.id);
        Long l = this.created;
        if (l != null) {
            parcel.writeInt(1);
            parcel.writeLong(l.longValue());
        } else {
            parcel.writeInt(0);
        }
        parcel.writeInt(this.liveMode ? 1 : 0);
        Type type2 = this.type;
        if (type2 != null) {
            parcel.writeInt(1);
            parcel.writeString(type2.name());
        } else {
            parcel.writeInt(0);
        }
        BillingDetails billingDetails2 = this.billingDetails;
        if (billingDetails2 != null) {
            parcel.writeInt(1);
            billingDetails2.writeToParcel(parcel, 0);
        } else {
            parcel.writeInt(0);
        }
        parcel.writeString(this.customerId);
        Map<String, String> map = this.metadata;
        if (map != null) {
            parcel.writeInt(1);
            parcel.writeInt(map.size());
            for (Map.Entry<String, String> next : map.entrySet()) {
                parcel.writeString(next.getKey());
                parcel.writeString(next.getValue());
            }
        } else {
            parcel.writeInt(0);
        }
        Card card2 = this.card;
        if (card2 != null) {
            parcel.writeInt(1);
            card2.writeToParcel(parcel, 0);
        } else {
            parcel.writeInt(0);
        }
        CardPresent cardPresent2 = this.cardPresent;
        if (cardPresent2 != null) {
            parcel.writeInt(1);
            cardPresent2.writeToParcel(parcel, 0);
        } else {
            parcel.writeInt(0);
        }
        Fpx fpx2 = this.fpx;
        if (fpx2 != null) {
            parcel.writeInt(1);
            fpx2.writeToParcel(parcel, 0);
        } else {
            parcel.writeInt(0);
        }
        Ideal ideal2 = this.ideal;
        if (ideal2 != null) {
            parcel.writeInt(1);
            ideal2.writeToParcel(parcel, 0);
        } else {
            parcel.writeInt(0);
        }
        SepaDebit sepaDebit2 = this.sepaDebit;
        if (sepaDebit2 != null) {
            parcel.writeInt(1);
            sepaDebit2.writeToParcel(parcel, 0);
        } else {
            parcel.writeInt(0);
        }
        AuBecsDebit auBecsDebit2 = this.auBecsDebit;
        if (auBecsDebit2 != null) {
            parcel.writeInt(1);
            auBecsDebit2.writeToParcel(parcel, 0);
        } else {
            parcel.writeInt(0);
        }
        BacsDebit bacsDebit2 = this.bacsDebit;
        if (bacsDebit2 != null) {
            parcel.writeInt(1);
            bacsDebit2.writeToParcel(parcel, 0);
        } else {
            parcel.writeInt(0);
        }
        Sofort sofort2 = this.sofort;
        if (sofort2 != null) {
            parcel.writeInt(1);
            sofort2.writeToParcel(parcel, 0);
            return;
        }
        parcel.writeInt(0);
    }

    public PaymentMethod(String str, Long l, boolean z, Type type2, BillingDetails billingDetails2, String str2, Map<String, String> map, Card card2, CardPresent cardPresent2, Fpx fpx2, Ideal ideal2, SepaDebit sepaDebit2, AuBecsDebit auBecsDebit2, BacsDebit bacsDebit2, Sofort sofort2) {
        this.id = str;
        this.created = l;
        this.liveMode = z;
        this.type = type2;
        this.billingDetails = billingDetails2;
        this.customerId = str2;
        this.metadata = map;
        this.card = card2;
        this.cardPresent = cardPresent2;
        this.fpx = fpx2;
        this.ideal = ideal2;
        this.sepaDebit = sepaDebit2;
        this.auBecsDebit = auBecsDebit2;
        this.bacsDebit = bacsDebit2;
        this.sofort = sofort2;
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* synthetic */ PaymentMethod(java.lang.String r20, java.lang.Long r21, boolean r22, com.stripe.android.model.PaymentMethod.Type r23, com.stripe.android.model.PaymentMethod.BillingDetails r24, java.lang.String r25, java.util.Map r26, com.stripe.android.model.PaymentMethod.Card r27, com.stripe.android.model.PaymentMethod.CardPresent r28, com.stripe.android.model.PaymentMethod.Fpx r29, com.stripe.android.model.PaymentMethod.Ideal r30, com.stripe.android.model.PaymentMethod.SepaDebit r31, com.stripe.android.model.PaymentMethod.AuBecsDebit r32, com.stripe.android.model.PaymentMethod.BacsDebit r33, com.stripe.android.model.PaymentMethod.Sofort r34, int r35, kotlin.jvm.internal.DefaultConstructorMarker r36) {
        /*
            r19 = this;
            r0 = r35
            r1 = r0 & 16
            r2 = 0
            if (r1 == 0) goto L_0x000c
            r1 = r2
            com.stripe.android.model.PaymentMethod$BillingDetails r1 = (com.stripe.android.model.PaymentMethod.BillingDetails) r1
            r8 = r1
            goto L_0x000e
        L_0x000c:
            r8 = r24
        L_0x000e:
            r1 = r0 & 32
            if (r1 == 0) goto L_0x0017
            r1 = r2
            java.lang.String r1 = (java.lang.String) r1
            r9 = r1
            goto L_0x0019
        L_0x0017:
            r9 = r25
        L_0x0019:
            r1 = r0 & 64
            if (r1 == 0) goto L_0x0022
            r1 = r2
            java.util.Map r1 = (java.util.Map) r1
            r10 = r1
            goto L_0x0024
        L_0x0022:
            r10 = r26
        L_0x0024:
            r1 = r0 & 128(0x80, float:1.794E-43)
            if (r1 == 0) goto L_0x002d
            r1 = r2
            com.stripe.android.model.PaymentMethod$Card r1 = (com.stripe.android.model.PaymentMethod.Card) r1
            r11 = r1
            goto L_0x002f
        L_0x002d:
            r11 = r27
        L_0x002f:
            r1 = r0 & 256(0x100, float:3.59E-43)
            if (r1 == 0) goto L_0x0038
            r1 = r2
            com.stripe.android.model.PaymentMethod$CardPresent r1 = (com.stripe.android.model.PaymentMethod.CardPresent) r1
            r12 = r1
            goto L_0x003a
        L_0x0038:
            r12 = r28
        L_0x003a:
            r1 = r0 & 512(0x200, float:7.175E-43)
            if (r1 == 0) goto L_0x0043
            r1 = r2
            com.stripe.android.model.PaymentMethod$Fpx r1 = (com.stripe.android.model.PaymentMethod.Fpx) r1
            r13 = r1
            goto L_0x0045
        L_0x0043:
            r13 = r29
        L_0x0045:
            r1 = r0 & 1024(0x400, float:1.435E-42)
            if (r1 == 0) goto L_0x004e
            r1 = r2
            com.stripe.android.model.PaymentMethod$Ideal r1 = (com.stripe.android.model.PaymentMethod.Ideal) r1
            r14 = r1
            goto L_0x0050
        L_0x004e:
            r14 = r30
        L_0x0050:
            r1 = r0 & 2048(0x800, float:2.87E-42)
            if (r1 == 0) goto L_0x0059
            r1 = r2
            com.stripe.android.model.PaymentMethod$SepaDebit r1 = (com.stripe.android.model.PaymentMethod.SepaDebit) r1
            r15 = r1
            goto L_0x005b
        L_0x0059:
            r15 = r31
        L_0x005b:
            r1 = r0 & 4096(0x1000, float:5.74E-42)
            if (r1 == 0) goto L_0x0065
            r1 = r2
            com.stripe.android.model.PaymentMethod$AuBecsDebit r1 = (com.stripe.android.model.PaymentMethod.AuBecsDebit) r1
            r16 = r1
            goto L_0x0067
        L_0x0065:
            r16 = r32
        L_0x0067:
            r1 = r0 & 8192(0x2000, float:1.14794E-41)
            if (r1 == 0) goto L_0x0071
            r1 = r2
            com.stripe.android.model.PaymentMethod$BacsDebit r1 = (com.stripe.android.model.PaymentMethod.BacsDebit) r1
            r17 = r1
            goto L_0x0073
        L_0x0071:
            r17 = r33
        L_0x0073:
            r0 = r0 & 16384(0x4000, float:2.2959E-41)
            if (r0 == 0) goto L_0x007d
            r0 = r2
            com.stripe.android.model.PaymentMethod$Sofort r0 = (com.stripe.android.model.PaymentMethod.Sofort) r0
            r18 = r0
            goto L_0x007f
        L_0x007d:
            r18 = r34
        L_0x007f:
            r3 = r19
            r4 = r20
            r5 = r21
            r6 = r22
            r7 = r23
            r3.<init>(r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15, r16, r17, r18)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.stripe.android.model.PaymentMethod.<init>(java.lang.String, java.lang.Long, boolean, com.stripe.android.model.PaymentMethod$Type, com.stripe.android.model.PaymentMethod$BillingDetails, java.lang.String, java.util.Map, com.stripe.android.model.PaymentMethod$Card, com.stripe.android.model.PaymentMethod$CardPresent, com.stripe.android.model.PaymentMethod$Fpx, com.stripe.android.model.PaymentMethod$Ideal, com.stripe.android.model.PaymentMethod$SepaDebit, com.stripe.android.model.PaymentMethod$AuBecsDebit, com.stripe.android.model.PaymentMethod$BacsDebit, com.stripe.android.model.PaymentMethod$Sofort, int, kotlin.jvm.internal.DefaultConstructorMarker):void");
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0011\b\u0001\u0018\u0000 \u001e2\b\u0012\u0004\u0012\u00020\u00000\u00012\u00020\u0002:\u0001\u001eB\u0017\b\u0002\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006¢\u0006\u0002\u0010\u0007J\t\u0010\b\u001a\u00020\tHÖ\u0001J\b\u0010\n\u001a\u00020\u0004H\u0016J\u0019\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\tHÖ\u0001R\u0010\u0010\u0003\u001a\u00020\u00048\u0006X\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u0005\u001a\u00020\u00068\u0006X\u0004¢\u0006\u0002\n\u0000j\u0002\b\u0010j\u0002\b\u0011j\u0002\b\u0012j\u0002\b\u0013j\u0002\b\u0014j\u0002\b\u0015j\u0002\b\u0016j\u0002\b\u0017j\u0002\b\u0018j\u0002\b\u0019j\u0002\b\u001aj\u0002\b\u001bj\u0002\b\u001cj\u0002\b\u001d¨\u0006\u001f"}, d2 = {"Lcom/stripe/android/model/PaymentMethod$Type;", "", "Landroid/os/Parcelable;", "code", "", "isReusable", "", "(Ljava/lang/String;ILjava/lang/String;Z)V", "describeContents", "", "toString", "writeToParcel", "", "parcel", "Landroid/os/Parcel;", "flags", "Card", "CardPresent", "Fpx", "Ideal", "SepaDebit", "AuBecsDebit", "BacsDebit", "Sofort", "P24", "Bancontact", "Giropay", "Eps", "Oxxo", "Alipay", "Companion", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: PaymentMethod.kt */
    public enum Type implements Parcelable {
        Card("card", true),
        CardPresent("card_present", false),
        Fpx("fpx", false),
        Ideal("ideal", false),
        SepaDebit("sepa_debit", false),
        AuBecsDebit("au_becs_debit", true),
        BacsDebit("bacs_debit", true),
        Sofort("sofort", false),
        P24("p24", false),
        Bancontact("bancontact", false),
        Giropay("giropay", false),
        Eps("eps", false),
        Oxxo("oxxo", false),
        Alipay("alipay", false);
        
        public static final Parcelable.Creator CREATOR = null;
        public static final Companion Companion = null;
        public final String code;
        public final boolean isReusable;

        @Metadata(bv = {1, 0, 3}, k = 3, mv = {1, 1, 16})
        public static class Creator implements Parcelable.Creator {
            public final Object createFromParcel(Parcel parcel) {
                Intrinsics.checkParameterIsNotNull(parcel, "in");
                return (Type) Enum.valueOf(Type.class, parcel.readString());
            }

            public final Object[] newArray(int i) {
                return new Type[i];
            }
        }

        public int describeContents() {
            return 0;
        }

        public void writeToParcel(Parcel parcel, int i) {
            Intrinsics.checkParameterIsNotNull(parcel, "parcel");
            parcel.writeString(name());
        }

        private Type(String str, boolean z) {
            this.code = str;
            this.isReusable = z;
        }

        static {
            Companion = new Companion((DefaultConstructorMarker) null);
            CREATOR = new Creator();
        }

        public String toString() {
            return this.code;
        }

        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0019\u0010\u0003\u001a\u0004\u0018\u00010\u00042\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006H\u0000¢\u0006\u0002\b\u0007¨\u0006\b"}, d2 = {"Lcom/stripe/android/model/PaymentMethod$Type$Companion;", "", "()V", "fromCode", "Lcom/stripe/android/model/PaymentMethod$Type;", "code", "", "fromCode$stripe_release", "stripe_release"}, k = 1, mv = {1, 1, 16})
        /* compiled from: PaymentMethod.kt */
        public static final class Companion {
            private Companion() {
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }

            public final /* synthetic */ Type fromCode$stripe_release(String str) {
                for (Type type : Type.values()) {
                    if (Intrinsics.areEqual((Object) type.code, (Object) str)) {
                        return type;
                    }
                }
                return null;
            }
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000j\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010$\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0012\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0003J\b\u0010\"\u001a\u00020\u0002H\u0016J\u0010\u0010#\u001a\u00020\u00002\b\u0010\u0004\u001a\u0004\u0018\u00010\u0005J\u0010\u0010$\u001a\u00020\u00002\b\u0010\u0006\u001a\u0004\u0018\u00010\u0007J\u0010\u0010%\u001a\u00020\u00002\b\u0010\b\u001a\u0004\u0018\u00010\tJ\u0010\u0010&\u001a\u00020\u00002\b\u0010\n\u001a\u0004\u0018\u00010\u000bJ\u0010\u0010'\u001a\u00020\u00002\b\u0010\f\u001a\u0004\u0018\u00010\rJ\u0015\u0010(\u001a\u00020\u00002\b\u0010\u000e\u001a\u0004\u0018\u00010\u000f¢\u0006\u0002\u0010)J\u0010\u0010*\u001a\u00020\u00002\b\u0010\u0011\u001a\u0004\u0018\u00010\u0012J\u0010\u0010+\u001a\u00020\u00002\b\u0010\u0013\u001a\u0004\u0018\u00010\u0014J\u0010\u0010,\u001a\u00020\u00002\b\u0010\u0015\u001a\u0004\u0018\u00010\u0012J\u0010\u0010-\u001a\u00020\u00002\b\u0010\u0016\u001a\u0004\u0018\u00010\u0017J\u000e\u0010.\u001a\u00020\u00002\u0006\u0010\u0018\u001a\u00020\u0019J\u001c\u0010/\u001a\u00020\u00002\u0014\u0010\u001a\u001a\u0010\u0012\u0004\u0012\u00020\u0012\u0012\u0004\u0012\u00020\u0012\u0018\u00010\u001bJ\u0010\u00100\u001a\u00020\u00002\b\u0010\u001c\u001a\u0004\u0018\u00010\u001dJ\u0010\u00101\u001a\u00020\u00002\b\u0010\u001e\u001a\u0004\u0018\u00010\u001fJ\u0010\u00102\u001a\u00020\u00002\b\u0010 \u001a\u0004\u0018\u00010!R\u0010\u0010\u0004\u001a\u0004\u0018\u00010\u0005X\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\u0006\u001a\u0004\u0018\u00010\u0007X\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\b\u001a\u0004\u0018\u00010\tX\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\n\u001a\u0004\u0018\u00010\u000bX\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\f\u001a\u0004\u0018\u00010\rX\u000e¢\u0006\u0002\n\u0000R\u0012\u0010\u000e\u001a\u0004\u0018\u00010\u000fX\u000e¢\u0006\u0004\n\u0002\u0010\u0010R\u0010\u0010\u0011\u001a\u0004\u0018\u00010\u0012X\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\u0013\u001a\u0004\u0018\u00010\u0014X\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\u0015\u001a\u0004\u0018\u00010\u0012X\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\u0016\u001a\u0004\u0018\u00010\u0017X\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0018\u001a\u00020\u0019X\u000e¢\u0006\u0002\n\u0000R\u001c\u0010\u001a\u001a\u0010\u0012\u0004\u0012\u00020\u0012\u0012\u0004\u0012\u00020\u0012\u0018\u00010\u001bX\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\u001c\u001a\u0004\u0018\u00010\u001dX\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\u001e\u001a\u0004\u0018\u00010\u001fX\u000e¢\u0006\u0002\n\u0000R\u0010\u0010 \u001a\u0004\u0018\u00010!X\u000e¢\u0006\u0002\n\u0000¨\u00063"}, d2 = {"Lcom/stripe/android/model/PaymentMethod$Builder;", "Lcom/stripe/android/ObjectBuilder;", "Lcom/stripe/android/model/PaymentMethod;", "()V", "auBecsDebit", "Lcom/stripe/android/model/PaymentMethod$AuBecsDebit;", "bacsDebit", "Lcom/stripe/android/model/PaymentMethod$BacsDebit;", "billingDetails", "Lcom/stripe/android/model/PaymentMethod$BillingDetails;", "card", "Lcom/stripe/android/model/PaymentMethod$Card;", "cardPresent", "Lcom/stripe/android/model/PaymentMethod$CardPresent;", "created", "", "Ljava/lang/Long;", "customerId", "", "fpx", "Lcom/stripe/android/model/PaymentMethod$Fpx;", "id", "ideal", "Lcom/stripe/android/model/PaymentMethod$Ideal;", "liveMode", "", "metadata", "", "sepaDebit", "Lcom/stripe/android/model/PaymentMethod$SepaDebit;", "sofort", "Lcom/stripe/android/model/PaymentMethod$Sofort;", "type", "Lcom/stripe/android/model/PaymentMethod$Type;", "build", "setAuBecsDebit", "setBacsDebit", "setBillingDetails", "setCard", "setCardPresent", "setCreated", "(Ljava/lang/Long;)Lcom/stripe/android/model/PaymentMethod$Builder;", "setCustomerId", "setFpx", "setId", "setIdeal", "setLiveMode", "setMetadata", "setSepaDebit", "setSofort", "setType", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: PaymentMethod.kt */
    public static final class Builder implements ObjectBuilder<PaymentMethod> {
        private AuBecsDebit auBecsDebit;
        private BacsDebit bacsDebit;
        private BillingDetails billingDetails;
        private Card card;
        private CardPresent cardPresent;
        private Long created;
        private String customerId;
        private Fpx fpx;
        private String id;
        private Ideal ideal;
        private boolean liveMode;
        private Map<String, String> metadata;
        private SepaDebit sepaDebit;
        private Sofort sofort;
        private Type type;

        public final Builder setId(String str) {
            Builder builder = this;
            builder.id = str;
            return builder;
        }

        public final Builder setCreated(Long l) {
            Builder builder = this;
            builder.created = l;
            return builder;
        }

        public final Builder setLiveMode(boolean z) {
            Builder builder = this;
            builder.liveMode = z;
            return builder;
        }

        public final Builder setMetadata(Map<String, String> map) {
            Builder builder = this;
            builder.metadata = map;
            return builder;
        }

        public final Builder setType(Type type2) {
            Builder builder = this;
            builder.type = type2;
            return builder;
        }

        public final Builder setBillingDetails(BillingDetails billingDetails2) {
            Builder builder = this;
            builder.billingDetails = billingDetails2;
            return builder;
        }

        public final Builder setCard(Card card2) {
            Builder builder = this;
            builder.card = card2;
            return builder;
        }

        public final Builder setCardPresent(CardPresent cardPresent2) {
            Builder builder = this;
            builder.cardPresent = cardPresent2;
            return builder;
        }

        public final Builder setCustomerId(String str) {
            Builder builder = this;
            builder.customerId = str;
            return builder;
        }

        public final Builder setIdeal(Ideal ideal2) {
            Builder builder = this;
            builder.ideal = ideal2;
            return builder;
        }

        public final Builder setFpx(Fpx fpx2) {
            Builder builder = this;
            builder.fpx = fpx2;
            return builder;
        }

        public final Builder setSepaDebit(SepaDebit sepaDebit2) {
            Builder builder = this;
            builder.sepaDebit = sepaDebit2;
            return builder;
        }

        public final Builder setAuBecsDebit(AuBecsDebit auBecsDebit2) {
            Builder builder = this;
            builder.auBecsDebit = auBecsDebit2;
            return builder;
        }

        public final Builder setBacsDebit(BacsDebit bacsDebit2) {
            Builder builder = this;
            builder.bacsDebit = bacsDebit2;
            return builder;
        }

        public final Builder setSofort(Sofort sofort2) {
            Builder builder = this;
            builder.sofort = sofort2;
            return builder;
        }

        public PaymentMethod build() {
            return new PaymentMethod(this.id, this.created, this.liveMode, this.type, this.billingDetails, this.customerId, this.metadata, this.card, this.cardPresent, this.fpx, this.ideal, this.sepaDebit, this.auBecsDebit, this.bacsDebit, this.sofort);
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000L\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\t\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\b\b\u0018\u0000 !2\u00020\u00012\u00020\u0002:\u0002 !B7\b\u0007\u0012\n\b\u0002\u0010\u0003\u001a\u0004\u0018\u00010\u0004\u0012\n\b\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u0012\n\b\u0002\u0010\u0007\u001a\u0004\u0018\u00010\u0006\u0012\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\u0006¢\u0006\u0002\u0010\tJ\u000b\u0010\n\u001a\u0004\u0018\u00010\u0004HÆ\u0003J\u000b\u0010\u000b\u001a\u0004\u0018\u00010\u0006HÆ\u0003J\u000b\u0010\f\u001a\u0004\u0018\u00010\u0006HÆ\u0003J\u000b\u0010\r\u001a\u0004\u0018\u00010\u0006HÆ\u0003J9\u0010\u000e\u001a\u00020\u00002\n\b\u0002\u0010\u0003\u001a\u0004\u0018\u00010\u00042\n\b\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u00062\n\b\u0002\u0010\u0007\u001a\u0004\u0018\u00010\u00062\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\u0006HÆ\u0001J\t\u0010\u000f\u001a\u00020\u0010HÖ\u0001J\u0013\u0010\u0011\u001a\u00020\u00122\b\u0010\u0013\u001a\u0004\u0018\u00010\u0014HÖ\u0003J\t\u0010\u0015\u001a\u00020\u0010HÖ\u0001J\u0006\u0010\u0016\u001a\u00020\u0017J\u0014\u0010\u0018\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00140\u0019H\u0016J\t\u0010\u001a\u001a\u00020\u0006HÖ\u0001J\u0019\u0010\u001b\u001a\u00020\u001c2\u0006\u0010\u001d\u001a\u00020\u001e2\u0006\u0010\u001f\u001a\u00020\u0010HÖ\u0001R\u0012\u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0006X\u0004¢\u0006\u0002\n\u0000R\u0012\u0010\u0005\u001a\u0004\u0018\u00010\u00068\u0006X\u0004¢\u0006\u0002\n\u0000R\u0012\u0010\u0007\u001a\u0004\u0018\u00010\u00068\u0006X\u0004¢\u0006\u0002\n\u0000R\u0012\u0010\b\u001a\u0004\u0018\u00010\u00068\u0006X\u0004¢\u0006\u0002\n\u0000¨\u0006\""}, d2 = {"Lcom/stripe/android/model/PaymentMethod$BillingDetails;", "Lcom/stripe/android/model/StripeModel;", "Lcom/stripe/android/model/StripeParamsModel;", "address", "Lcom/stripe/android/model/Address;", "email", "", "name", "phone", "(Lcom/stripe/android/model/Address;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", "component1", "component2", "component3", "component4", "copy", "describeContents", "", "equals", "", "other", "", "hashCode", "toBuilder", "Lcom/stripe/android/model/PaymentMethod$BillingDetails$Builder;", "toParamMap", "", "toString", "writeToParcel", "", "parcel", "Landroid/os/Parcel;", "flags", "Builder", "Companion", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: PaymentMethod.kt */
    public static final class BillingDetails implements StripeModel, StripeParamsModel {
        public static final Parcelable.Creator CREATOR = new Creator();
        public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
        public static final String PARAM_ADDRESS = "address";
        public static final String PARAM_EMAIL = "email";
        public static final String PARAM_NAME = "name";
        public static final String PARAM_PHONE = "phone";
        public final Address address;
        public final String email;
        public final String name;
        public final String phone;

        @Metadata(bv = {1, 0, 3}, k = 3, mv = {1, 1, 16})
        public static class Creator implements Parcelable.Creator {
            public final Object createFromParcel(Parcel parcel) {
                Intrinsics.checkParameterIsNotNull(parcel, "in");
                return new BillingDetails(parcel.readInt() != 0 ? (Address) Address.CREATOR.createFromParcel(parcel) : null, parcel.readString(), parcel.readString(), parcel.readString());
            }

            public final Object[] newArray(int i) {
                return new BillingDetails[i];
            }
        }

        public BillingDetails() {
            this((Address) null, (String) null, (String) null, (String) null, 15, (DefaultConstructorMarker) null);
        }

        public BillingDetails(Address address2) {
            this(address2, (String) null, (String) null, (String) null, 14, (DefaultConstructorMarker) null);
        }

        public BillingDetails(Address address2, String str) {
            this(address2, str, (String) null, (String) null, 12, (DefaultConstructorMarker) null);
        }

        public BillingDetails(Address address2, String str, String str2) {
            this(address2, str, str2, (String) null, 8, (DefaultConstructorMarker) null);
        }

        public static /* synthetic */ BillingDetails copy$default(BillingDetails billingDetails, Address address2, String str, String str2, String str3, int i, Object obj) {
            if ((i & 1) != 0) {
                address2 = billingDetails.address;
            }
            if ((i & 2) != 0) {
                str = billingDetails.email;
            }
            if ((i & 4) != 0) {
                str2 = billingDetails.name;
            }
            if ((i & 8) != 0) {
                str3 = billingDetails.phone;
            }
            return billingDetails.copy(address2, str, str2, str3);
        }

        public final Address component1() {
            return this.address;
        }

        public final String component2() {
            return this.email;
        }

        public final String component3() {
            return this.name;
        }

        public final String component4() {
            return this.phone;
        }

        public final BillingDetails copy(Address address2, String str, String str2, String str3) {
            return new BillingDetails(address2, str, str2, str3);
        }

        public int describeContents() {
            return 0;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof BillingDetails)) {
                return false;
            }
            BillingDetails billingDetails = (BillingDetails) obj;
            return Intrinsics.areEqual((Object) this.address, (Object) billingDetails.address) && Intrinsics.areEqual((Object) this.email, (Object) billingDetails.email) && Intrinsics.areEqual((Object) this.name, (Object) billingDetails.name) && Intrinsics.areEqual((Object) this.phone, (Object) billingDetails.phone);
        }

        public int hashCode() {
            Address address2 = this.address;
            int i = 0;
            int hashCode = (address2 != null ? address2.hashCode() : 0) * 31;
            String str = this.email;
            int hashCode2 = (hashCode + (str != null ? str.hashCode() : 0)) * 31;
            String str2 = this.name;
            int hashCode3 = (hashCode2 + (str2 != null ? str2.hashCode() : 0)) * 31;
            String str3 = this.phone;
            if (str3 != null) {
                i = str3.hashCode();
            }
            return hashCode3 + i;
        }

        public String toString() {
            return "BillingDetails(address=" + this.address + ", email=" + this.email + ", name=" + this.name + ", phone=" + this.phone + ")";
        }

        public void writeToParcel(Parcel parcel, int i) {
            Intrinsics.checkParameterIsNotNull(parcel, "parcel");
            Address address2 = this.address;
            if (address2 != null) {
                parcel.writeInt(1);
                address2.writeToParcel(parcel, 0);
            } else {
                parcel.writeInt(0);
            }
            parcel.writeString(this.email);
            parcel.writeString(this.name);
            parcel.writeString(this.phone);
        }

        public BillingDetails(Address address2, String str, String str2, String str3) {
            this.address = address2;
            this.email = str;
            this.name = str2;
            this.phone = str3;
        }

        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ BillingDetails(Address address2, String str, String str2, String str3, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this((i & 1) != 0 ? null : address2, (i & 2) != 0 ? null : str, (i & 4) != 0 ? null : str2, (i & 8) != 0 ? null : str3);
        }

        public final Builder toBuilder() {
            return new Builder().setAddress(this.address).setEmail(this.email).setName(this.name).setPhone(this.phone);
        }

        public Map<String, Object> toParamMap() {
            Map emptyMap = MapsKt.emptyMap();
            Address address2 = this.address;
            Map map = null;
            Map<K, V> mapOf = address2 != null ? MapsKt.mapOf(TuplesKt.to(PARAM_ADDRESS, address2.toParamMap())) : null;
            if (mapOf == null) {
                mapOf = MapsKt.emptyMap();
            }
            Map<K, V> plus = MapsKt.plus(emptyMap, (Map) mapOf);
            String str = this.email;
            Map mapOf2 = str != null ? MapsKt.mapOf(TuplesKt.to("email", str)) : null;
            if (mapOf2 == null) {
                mapOf2 = MapsKt.emptyMap();
            }
            Map<K, V> plus2 = MapsKt.plus(plus, (Map<K, V>) mapOf2);
            String str2 = this.name;
            Map mapOf3 = str2 != null ? MapsKt.mapOf(TuplesKt.to("name", str2)) : null;
            if (mapOf3 == null) {
                mapOf3 = MapsKt.emptyMap();
            }
            Map<K, V> plus3 = MapsKt.plus(plus2, (Map<K, V>) mapOf3);
            String str3 = this.phone;
            if (str3 != null) {
                map = MapsKt.mapOf(TuplesKt.to("phone", str3));
            }
            if (map == null) {
                map = MapsKt.emptyMap();
            }
            return MapsKt.plus(plus3, (Map<K, V>) map);
        }

        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\b\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0003J\b\u0010\n\u001a\u00020\u0002H\u0016J\u0010\u0010\u000b\u001a\u00020\u00002\b\u0010\u0004\u001a\u0004\u0018\u00010\u0005J\u0010\u0010\f\u001a\u00020\u00002\b\u0010\u0006\u001a\u0004\u0018\u00010\u0007J\u0010\u0010\r\u001a\u00020\u00002\b\u0010\b\u001a\u0004\u0018\u00010\u0007J\u0010\u0010\u000e\u001a\u00020\u00002\b\u0010\t\u001a\u0004\u0018\u00010\u0007R\u0010\u0010\u0004\u001a\u0004\u0018\u00010\u0005X\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\u0006\u001a\u0004\u0018\u00010\u0007X\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\b\u001a\u0004\u0018\u00010\u0007X\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\t\u001a\u0004\u0018\u00010\u0007X\u000e¢\u0006\u0002\n\u0000¨\u0006\u000f"}, d2 = {"Lcom/stripe/android/model/PaymentMethod$BillingDetails$Builder;", "Lcom/stripe/android/ObjectBuilder;", "Lcom/stripe/android/model/PaymentMethod$BillingDetails;", "()V", "address", "Lcom/stripe/android/model/Address;", "email", "", "name", "phone", "build", "setAddress", "setEmail", "setName", "setPhone", "stripe_release"}, k = 1, mv = {1, 1, 16})
        /* compiled from: PaymentMethod.kt */
        public static final class Builder implements ObjectBuilder<BillingDetails> {
            private Address address;
            private String email;
            private String name;
            private String phone;

            public final Builder setAddress(Address address2) {
                Builder builder = this;
                builder.address = address2;
                return builder;
            }

            public final Builder setEmail(String str) {
                Builder builder = this;
                builder.email = str;
                return builder;
            }

            public final Builder setName(String str) {
                Builder builder = this;
                builder.name = str;
                return builder;
            }

            public final Builder setPhone(String str) {
                Builder builder = this;
                builder.phone = str;
                return builder;
            }

            public BillingDetails build() {
                return new BillingDetails(this.address, this.email, this.name, this.phone);
            }
        }

        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u000e\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000bR\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\f"}, d2 = {"Lcom/stripe/android/model/PaymentMethod$BillingDetails$Companion;", "", "()V", "PARAM_ADDRESS", "", "PARAM_EMAIL", "PARAM_NAME", "PARAM_PHONE", "create", "Lcom/stripe/android/model/PaymentMethod$BillingDetails;", "shippingInformation", "Lcom/stripe/android/model/ShippingInformation;", "stripe_release"}, k = 1, mv = {1, 1, 16})
        /* compiled from: PaymentMethod.kt */
        public static final class Companion {
            private Companion() {
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }

            public final BillingDetails create(ShippingInformation shippingInformation) {
                Intrinsics.checkParameterIsNotNull(shippingInformation, "shippingInformation");
                return new BillingDetails(shippingInformation.getAddress(), (String) null, shippingInformation.getName(), shippingInformation.getPhone(), 2, (DefaultConstructorMarker) null);
            }
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000P\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0012\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\b\b\u0018\u00002\u00020\u0001:\u0005./012B\b\u0000\u0012\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\u0007\u001a\u0004\u0018\u00010\b\u0012\n\b\u0002\u0010\t\u001a\u0004\u0018\u00010\b\u0012\n\b\u0002\u0010\n\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\u000b\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\f\u001a\u0004\u0018\u00010\r\u0012\n\b\u0002\u0010\u000e\u001a\u0004\u0018\u00010\u000f\u0012\n\b\u0002\u0010\u0010\u001a\u0004\u0018\u00010\u0011¢\u0006\u0002\u0010\u0012J\u000b\u0010\u0014\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u0010\u0010\u0015\u001a\u0004\u0018\u00010\u0011HÀ\u0003¢\u0006\u0002\b\u0016J\u000b\u0010\u0017\u001a\u0004\u0018\u00010\u0005HÆ\u0003J\u000b\u0010\u0018\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u0010\u0010\u0019\u001a\u0004\u0018\u00010\bHÆ\u0003¢\u0006\u0002\u0010\u001aJ\u0010\u0010\u001b\u001a\u0004\u0018\u00010\bHÆ\u0003¢\u0006\u0002\u0010\u001aJ\u000b\u0010\u001c\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000b\u0010\u001d\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000b\u0010\u001e\u001a\u0004\u0018\u00010\rHÆ\u0003J\u000b\u0010\u001f\u001a\u0004\u0018\u00010\u000fHÆ\u0003J\u0001\u0010 \u001a\u00020\u00002\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u0007\u001a\u0004\u0018\u00010\b2\n\b\u0002\u0010\t\u001a\u0004\u0018\u00010\b2\n\b\u0002\u0010\n\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u000b\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\f\u001a\u0004\u0018\u00010\r2\n\b\u0002\u0010\u000e\u001a\u0004\u0018\u00010\u000f2\n\b\u0002\u0010\u0010\u001a\u0004\u0018\u00010\u0011HÆ\u0001¢\u0006\u0002\u0010!J\t\u0010\"\u001a\u00020\bHÖ\u0001J\u0013\u0010#\u001a\u00020$2\b\u0010%\u001a\u0004\u0018\u00010&HÖ\u0003J\t\u0010'\u001a\u00020\bHÖ\u0001J\t\u0010(\u001a\u00020\u0003HÖ\u0001J\u0019\u0010)\u001a\u00020*2\u0006\u0010+\u001a\u00020,2\u0006\u0010-\u001a\u00020\bHÖ\u0001R\u0012\u0010\u0002\u001a\u0004\u0018\u00010\u00038\u0006X\u0004¢\u0006\u0002\n\u0000R\u0012\u0010\u0004\u001a\u0004\u0018\u00010\u00058\u0006X\u0004¢\u0006\u0002\n\u0000R\u0012\u0010\u0006\u001a\u0004\u0018\u00010\u00038\u0006X\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\u0007\u001a\u0004\u0018\u00010\b8\u0006X\u0004¢\u0006\u0004\n\u0002\u0010\u0013R\u0014\u0010\t\u001a\u0004\u0018\u00010\b8\u0006X\u0004¢\u0006\u0004\n\u0002\u0010\u0013R\u0012\u0010\n\u001a\u0004\u0018\u00010\u00038\u0006X\u0004¢\u0006\u0002\n\u0000R\u0012\u0010\u000b\u001a\u0004\u0018\u00010\u00038\u0006X\u0004¢\u0006\u0002\n\u0000R\u0012\u0010\u0010\u001a\u0004\u0018\u00010\u00118\u0000X\u0004¢\u0006\u0002\n\u0000R\u0012\u0010\f\u001a\u0004\u0018\u00010\r8\u0006X\u0004¢\u0006\u0002\n\u0000R\u0012\u0010\u000e\u001a\u0004\u0018\u00010\u000f8\u0006X\u0004¢\u0006\u0002\n\u0000¨\u00063"}, d2 = {"Lcom/stripe/android/model/PaymentMethod$Card;", "Lcom/stripe/android/model/StripeModel;", "brand", "", "checks", "Lcom/stripe/android/model/PaymentMethod$Card$Checks;", "country", "expiryMonth", "", "expiryYear", "funding", "last4", "threeDSecureUsage", "Lcom/stripe/android/model/PaymentMethod$Card$ThreeDSecureUsage;", "wallet", "Lcom/stripe/android/model/wallets/Wallet;", "networks", "Lcom/stripe/android/model/PaymentMethod$Card$Networks;", "(Ljava/lang/String;Lcom/stripe/android/model/PaymentMethod$Card$Checks;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Lcom/stripe/android/model/PaymentMethod$Card$ThreeDSecureUsage;Lcom/stripe/android/model/wallets/Wallet;Lcom/stripe/android/model/PaymentMethod$Card$Networks;)V", "Ljava/lang/Integer;", "component1", "component10", "component10$stripe_release", "component2", "component3", "component4", "()Ljava/lang/Integer;", "component5", "component6", "component7", "component8", "component9", "copy", "(Ljava/lang/String;Lcom/stripe/android/model/PaymentMethod$Card$Checks;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Lcom/stripe/android/model/PaymentMethod$Card$ThreeDSecureUsage;Lcom/stripe/android/model/wallets/Wallet;Lcom/stripe/android/model/PaymentMethod$Card$Networks;)Lcom/stripe/android/model/PaymentMethod$Card;", "describeContents", "equals", "", "other", "", "hashCode", "toString", "writeToParcel", "", "parcel", "Landroid/os/Parcel;", "flags", "Brand", "Builder", "Checks", "Networks", "ThreeDSecureUsage", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: PaymentMethod.kt */
    public static final class Card implements StripeModel {
        public static final Parcelable.Creator CREATOR = new Creator();
        public final String brand;
        public final Checks checks;
        public final String country;
        public final Integer expiryMonth;
        public final Integer expiryYear;
        public final String funding;
        public final String last4;
        public final Networks networks;
        public final ThreeDSecureUsage threeDSecureUsage;
        public final Wallet wallet;

        @Metadata(bv = {1, 0, 3}, k = 3, mv = {1, 1, 16})
        public static class Creator implements Parcelable.Creator {
            public final Object createFromParcel(Parcel parcel) {
                Intrinsics.checkParameterIsNotNull(parcel, "in");
                return new Card(parcel.readString(), parcel.readInt() != 0 ? (Checks) Checks.CREATOR.createFromParcel(parcel) : null, parcel.readString(), parcel.readInt() != 0 ? Integer.valueOf(parcel.readInt()) : null, parcel.readInt() != 0 ? Integer.valueOf(parcel.readInt()) : null, parcel.readString(), parcel.readString(), parcel.readInt() != 0 ? (ThreeDSecureUsage) ThreeDSecureUsage.CREATOR.createFromParcel(parcel) : null, (Wallet) parcel.readParcelable(Card.class.getClassLoader()), parcel.readInt() != 0 ? (Networks) Networks.CREATOR.createFromParcel(parcel) : null);
            }

            public final Object[] newArray(int i) {
                return new Card[i];
            }
        }

        public Card() {
            this((String) null, (Checks) null, (String) null, (Integer) null, (Integer) null, (String) null, (String) null, (ThreeDSecureUsage) null, (Wallet) null, (Networks) null, 1023, (DefaultConstructorMarker) null);
        }

        public static /* synthetic */ Card copy$default(Card card, String str, Checks checks2, String str2, Integer num, Integer num2, String str3, String str4, ThreeDSecureUsage threeDSecureUsage2, Wallet wallet2, Networks networks2, int i, Object obj) {
            Card card2 = card;
            int i2 = i;
            return card.copy((i2 & 1) != 0 ? card2.brand : str, (i2 & 2) != 0 ? card2.checks : checks2, (i2 & 4) != 0 ? card2.country : str2, (i2 & 8) != 0 ? card2.expiryMonth : num, (i2 & 16) != 0 ? card2.expiryYear : num2, (i2 & 32) != 0 ? card2.funding : str3, (i2 & 64) != 0 ? card2.last4 : str4, (i2 & 128) != 0 ? card2.threeDSecureUsage : threeDSecureUsage2, (i2 & 256) != 0 ? card2.wallet : wallet2, (i2 & 512) != 0 ? card2.networks : networks2);
        }

        public final String component1() {
            return this.brand;
        }

        public final Networks component10$stripe_release() {
            return this.networks;
        }

        public final Checks component2() {
            return this.checks;
        }

        public final String component3() {
            return this.country;
        }

        public final Integer component4() {
            return this.expiryMonth;
        }

        public final Integer component5() {
            return this.expiryYear;
        }

        public final String component6() {
            return this.funding;
        }

        public final String component7() {
            return this.last4;
        }

        public final ThreeDSecureUsage component8() {
            return this.threeDSecureUsage;
        }

        public final Wallet component9() {
            return this.wallet;
        }

        public final Card copy(String str, Checks checks2, String str2, Integer num, Integer num2, String str3, String str4, ThreeDSecureUsage threeDSecureUsage2, Wallet wallet2, Networks networks2) {
            return new Card(str, checks2, str2, num, num2, str3, str4, threeDSecureUsage2, wallet2, networks2);
        }

        public int describeContents() {
            return 0;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Card)) {
                return false;
            }
            Card card = (Card) obj;
            return Intrinsics.areEqual((Object) this.brand, (Object) card.brand) && Intrinsics.areEqual((Object) this.checks, (Object) card.checks) && Intrinsics.areEqual((Object) this.country, (Object) card.country) && Intrinsics.areEqual((Object) this.expiryMonth, (Object) card.expiryMonth) && Intrinsics.areEqual((Object) this.expiryYear, (Object) card.expiryYear) && Intrinsics.areEqual((Object) this.funding, (Object) card.funding) && Intrinsics.areEqual((Object) this.last4, (Object) card.last4) && Intrinsics.areEqual((Object) this.threeDSecureUsage, (Object) card.threeDSecureUsage) && Intrinsics.areEqual((Object) this.wallet, (Object) card.wallet) && Intrinsics.areEqual((Object) this.networks, (Object) card.networks);
        }

        public int hashCode() {
            String str = this.brand;
            int i = 0;
            int hashCode = (str != null ? str.hashCode() : 0) * 31;
            Checks checks2 = this.checks;
            int hashCode2 = (hashCode + (checks2 != null ? checks2.hashCode() : 0)) * 31;
            String str2 = this.country;
            int hashCode3 = (hashCode2 + (str2 != null ? str2.hashCode() : 0)) * 31;
            Integer num = this.expiryMonth;
            int hashCode4 = (hashCode3 + (num != null ? num.hashCode() : 0)) * 31;
            Integer num2 = this.expiryYear;
            int hashCode5 = (hashCode4 + (num2 != null ? num2.hashCode() : 0)) * 31;
            String str3 = this.funding;
            int hashCode6 = (hashCode5 + (str3 != null ? str3.hashCode() : 0)) * 31;
            String str4 = this.last4;
            int hashCode7 = (hashCode6 + (str4 != null ? str4.hashCode() : 0)) * 31;
            ThreeDSecureUsage threeDSecureUsage2 = this.threeDSecureUsage;
            int hashCode8 = (hashCode7 + (threeDSecureUsage2 != null ? threeDSecureUsage2.hashCode() : 0)) * 31;
            Wallet wallet2 = this.wallet;
            int hashCode9 = (hashCode8 + (wallet2 != null ? wallet2.hashCode() : 0)) * 31;
            Networks networks2 = this.networks;
            if (networks2 != null) {
                i = networks2.hashCode();
            }
            return hashCode9 + i;
        }

        public String toString() {
            return "Card(brand=" + this.brand + ", checks=" + this.checks + ", country=" + this.country + ", expiryMonth=" + this.expiryMonth + ", expiryYear=" + this.expiryYear + ", funding=" + this.funding + ", last4=" + this.last4 + ", threeDSecureUsage=" + this.threeDSecureUsage + ", wallet=" + this.wallet + ", networks=" + this.networks + ")";
        }

        public void writeToParcel(Parcel parcel, int i) {
            Intrinsics.checkParameterIsNotNull(parcel, "parcel");
            parcel.writeString(this.brand);
            Checks checks2 = this.checks;
            if (checks2 != null) {
                parcel.writeInt(1);
                checks2.writeToParcel(parcel, 0);
            } else {
                parcel.writeInt(0);
            }
            parcel.writeString(this.country);
            Integer num = this.expiryMonth;
            if (num != null) {
                parcel.writeInt(1);
                parcel.writeInt(num.intValue());
            } else {
                parcel.writeInt(0);
            }
            Integer num2 = this.expiryYear;
            if (num2 != null) {
                parcel.writeInt(1);
                parcel.writeInt(num2.intValue());
            } else {
                parcel.writeInt(0);
            }
            parcel.writeString(this.funding);
            parcel.writeString(this.last4);
            ThreeDSecureUsage threeDSecureUsage2 = this.threeDSecureUsage;
            if (threeDSecureUsage2 != null) {
                parcel.writeInt(1);
                threeDSecureUsage2.writeToParcel(parcel, 0);
            } else {
                parcel.writeInt(0);
            }
            parcel.writeParcelable(this.wallet, i);
            Networks networks2 = this.networks;
            if (networks2 != null) {
                parcel.writeInt(1);
                networks2.writeToParcel(parcel, 0);
                return;
            }
            parcel.writeInt(0);
        }

        public Card(String str, Checks checks2, String str2, Integer num, Integer num2, String str3, String str4, ThreeDSecureUsage threeDSecureUsage2, Wallet wallet2, Networks networks2) {
            this.brand = str;
            this.checks = checks2;
            this.country = str2;
            this.expiryMonth = num;
            this.expiryYear = num2;
            this.funding = str3;
            this.last4 = str4;
            this.threeDSecureUsage = threeDSecureUsage2;
            this.wallet = wallet2;
            this.networks = networks2;
        }

        /* JADX WARNING: Illegal instructions before constructor call */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public /* synthetic */ Card(java.lang.String r12, com.stripe.android.model.PaymentMethod.Card.Checks r13, java.lang.String r14, java.lang.Integer r15, java.lang.Integer r16, java.lang.String r17, java.lang.String r18, com.stripe.android.model.PaymentMethod.Card.ThreeDSecureUsage r19, com.stripe.android.model.wallets.Wallet r20, com.stripe.android.model.PaymentMethod.Card.Networks r21, int r22, kotlin.jvm.internal.DefaultConstructorMarker r23) {
            /*
                r11 = this;
                r0 = r22
                r1 = r0 & 1
                r2 = 0
                if (r1 == 0) goto L_0x000b
                r1 = r2
                java.lang.String r1 = (java.lang.String) r1
                goto L_0x000c
            L_0x000b:
                r1 = r12
            L_0x000c:
                r3 = r0 & 2
                if (r3 == 0) goto L_0x0014
                r3 = r2
                com.stripe.android.model.PaymentMethod$Card$Checks r3 = (com.stripe.android.model.PaymentMethod.Card.Checks) r3
                goto L_0x0015
            L_0x0014:
                r3 = r13
            L_0x0015:
                r4 = r0 & 4
                if (r4 == 0) goto L_0x001d
                r4 = r2
                java.lang.String r4 = (java.lang.String) r4
                goto L_0x001e
            L_0x001d:
                r4 = r14
            L_0x001e:
                r5 = r0 & 8
                if (r5 == 0) goto L_0x0026
                r5 = r2
                java.lang.Integer r5 = (java.lang.Integer) r5
                goto L_0x0027
            L_0x0026:
                r5 = r15
            L_0x0027:
                r6 = r0 & 16
                if (r6 == 0) goto L_0x002f
                r6 = r2
                java.lang.Integer r6 = (java.lang.Integer) r6
                goto L_0x0031
            L_0x002f:
                r6 = r16
            L_0x0031:
                r7 = r0 & 32
                if (r7 == 0) goto L_0x0039
                r7 = r2
                java.lang.String r7 = (java.lang.String) r7
                goto L_0x003b
            L_0x0039:
                r7 = r17
            L_0x003b:
                r8 = r0 & 64
                if (r8 == 0) goto L_0x0043
                r8 = r2
                java.lang.String r8 = (java.lang.String) r8
                goto L_0x0045
            L_0x0043:
                r8 = r18
            L_0x0045:
                r9 = r0 & 128(0x80, float:1.794E-43)
                if (r9 == 0) goto L_0x004d
                r9 = r2
                com.stripe.android.model.PaymentMethod$Card$ThreeDSecureUsage r9 = (com.stripe.android.model.PaymentMethod.Card.ThreeDSecureUsage) r9
                goto L_0x004f
            L_0x004d:
                r9 = r19
            L_0x004f:
                r10 = r0 & 256(0x100, float:3.59E-43)
                if (r10 == 0) goto L_0x0057
                r10 = r2
                com.stripe.android.model.wallets.Wallet r10 = (com.stripe.android.model.wallets.Wallet) r10
                goto L_0x0059
            L_0x0057:
                r10 = r20
            L_0x0059:
                r0 = r0 & 512(0x200, float:7.175E-43)
                if (r0 == 0) goto L_0x0061
                r0 = r2
                com.stripe.android.model.PaymentMethod$Card$Networks r0 = (com.stripe.android.model.PaymentMethod.Card.Networks) r0
                goto L_0x0063
            L_0x0061:
                r0 = r21
            L_0x0063:
                r12 = r11
                r13 = r1
                r14 = r3
                r15 = r4
                r16 = r5
                r17 = r6
                r18 = r7
                r19 = r8
                r20 = r9
                r21 = r10
                r22 = r0
                r12.<init>(r13, r14, r15, r16, r17, r18, r19, r20, r21, r22)
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.stripe.android.model.PaymentMethod.Card.<init>(java.lang.String, com.stripe.android.model.PaymentMethod$Card$Checks, java.lang.String, java.lang.Integer, java.lang.Integer, java.lang.String, java.lang.String, com.stripe.android.model.PaymentMethod$Card$ThreeDSecureUsage, com.stripe.android.model.wallets.Wallet, com.stripe.android.model.PaymentMethod$Card$Networks, int, kotlin.jvm.internal.DefaultConstructorMarker):void");
        }

        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u001b\n\u0002\b\u0002\b\u0002\u0018\u0000 \u00022\u00020\u0001:\u0001\u0002B\u0000¨\u0006\u0003"}, d2 = {"Lcom/stripe/android/model/PaymentMethod$Card$Brand;", "", "Companion", "stripe_release"}, k = 1, mv = {1, 1, 16})
        @Retention(AnnotationRetention.SOURCE)
        @java.lang.annotation.Retention(RetentionPolicy.SOURCE)
        /* compiled from: PaymentMethod.kt */
        public @interface Brand {
            public static final String AMERICAN_EXPRESS = "amex";
            public static final Companion Companion = Companion.$$INSTANCE;
            public static final String DINERS_CLUB = "diners";
            public static final String DISCOVER = "discover";
            public static final String JCB = "jcb";
            public static final String MASTERCARD = "mastercard";
            public static final String UNIONPAY = "unionpay";
            public static final String UNKNOWN = "unknown";
            public static final String VISA = "visa";

            @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\b\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\f"}, d2 = {"Lcom/stripe/android/model/PaymentMethod$Card$Brand$Companion;", "", "()V", "AMERICAN_EXPRESS", "", "DINERS_CLUB", "DISCOVER", "JCB", "MASTERCARD", "UNIONPAY", "UNKNOWN", "VISA", "stripe_release"}, k = 1, mv = {1, 1, 16})
            /* compiled from: PaymentMethod.kt */
            public static final class Companion {
                static final /* synthetic */ Companion $$INSTANCE = new Companion();
                public static final String AMERICAN_EXPRESS = "amex";
                public static final String DINERS_CLUB = "diners";
                public static final String DISCOVER = "discover";
                public static final String JCB = "jcb";
                public static final String MASTERCARD = "mastercard";
                public static final String UNIONPAY = "unionpay";
                public static final String UNKNOWN = "unknown";
                public static final String VISA = "visa";

                private Companion() {
                }
            }
        }

        @Metadata(bv = {1, 0, 3}, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\f\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0003J\b\u0010\u0013\u001a\u00020\u0002H\u0016J\u0010\u0010\u0014\u001a\u00020\u00002\b\u0010\u0004\u001a\u0004\u0018\u00010\u0005J\u0010\u0010\u0015\u001a\u00020\u00002\b\u0010\u0006\u001a\u0004\u0018\u00010\u0007J\u0010\u0010\u0016\u001a\u00020\u00002\b\u0010\b\u001a\u0004\u0018\u00010\u0005J\u0015\u0010\u0017\u001a\u00020\u00002\b\u0010\t\u001a\u0004\u0018\u00010\n¢\u0006\u0002\u0010\u0018J\u0015\u0010\u0019\u001a\u00020\u00002\b\u0010\f\u001a\u0004\u0018\u00010\n¢\u0006\u0002\u0010\u0018J\u0010\u0010\u001a\u001a\u00020\u00002\b\u0010\r\u001a\u0004\u0018\u00010\u0005J\u0010\u0010\u001b\u001a\u00020\u00002\b\u0010\u000e\u001a\u0004\u0018\u00010\u0005J\u0010\u0010\u001c\u001a\u00020\u00002\b\u0010\u000f\u001a\u0004\u0018\u00010\u0010J\u0010\u0010\u001d\u001a\u00020\u00002\b\u0010\u0011\u001a\u0004\u0018\u00010\u0012R\u0010\u0010\u0004\u001a\u0004\u0018\u00010\u0005X\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\u0006\u001a\u0004\u0018\u00010\u0007X\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\b\u001a\u0004\u0018\u00010\u0005X\u000e¢\u0006\u0002\n\u0000R\u0012\u0010\t\u001a\u0004\u0018\u00010\nX\u000e¢\u0006\u0004\n\u0002\u0010\u000bR\u0012\u0010\f\u001a\u0004\u0018\u00010\nX\u000e¢\u0006\u0004\n\u0002\u0010\u000bR\u0010\u0010\r\u001a\u0004\u0018\u00010\u0005X\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\u000e\u001a\u0004\u0018\u00010\u0005X\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\u000f\u001a\u0004\u0018\u00010\u0010X\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\u0011\u001a\u0004\u0018\u00010\u0012X\u000e¢\u0006\u0002\n\u0000¨\u0006\u001e"}, d2 = {"Lcom/stripe/android/model/PaymentMethod$Card$Builder;", "Lcom/stripe/android/ObjectBuilder;", "Lcom/stripe/android/model/PaymentMethod$Card;", "()V", "brand", "", "checks", "Lcom/stripe/android/model/PaymentMethod$Card$Checks;", "country", "expiryMonth", "", "Ljava/lang/Integer;", "expiryYear", "funding", "last4", "threeDSecureUsage", "Lcom/stripe/android/model/PaymentMethod$Card$ThreeDSecureUsage;", "wallet", "Lcom/stripe/android/model/wallets/Wallet;", "build", "setBrand", "setChecks", "setCountry", "setExpiryMonth", "(Ljava/lang/Integer;)Lcom/stripe/android/model/PaymentMethod$Card$Builder;", "setExpiryYear", "setFunding", "setLast4", "setThreeDSecureUsage", "setWallet", "stripe_release"}, k = 1, mv = {1, 1, 16})
        /* compiled from: PaymentMethod.kt */
        public static final class Builder implements ObjectBuilder<Card> {
            private String brand;
            private Checks checks;
            private String country;
            private Integer expiryMonth;
            private Integer expiryYear;
            private String funding;
            private String last4;
            private ThreeDSecureUsage threeDSecureUsage;
            private Wallet wallet;

            public final Builder setBrand(String str) {
                Builder builder = this;
                builder.brand = str;
                return builder;
            }

            public final Builder setChecks(Checks checks2) {
                Builder builder = this;
                builder.checks = checks2;
                return builder;
            }

            public final Builder setCountry(String str) {
                Builder builder = this;
                builder.country = str;
                return builder;
            }

            public final Builder setExpiryMonth(Integer num) {
                Builder builder = this;
                builder.expiryMonth = num;
                return builder;
            }

            public final Builder setExpiryYear(Integer num) {
                Builder builder = this;
                builder.expiryYear = num;
                return builder;
            }

            public final Builder setFunding(String str) {
                Builder builder = this;
                builder.funding = str;
                return builder;
            }

            public final Builder setLast4(String str) {
                Builder builder = this;
                builder.last4 = str;
                return builder;
            }

            public final Builder setThreeDSecureUsage(ThreeDSecureUsage threeDSecureUsage2) {
                Builder builder = this;
                builder.threeDSecureUsage = threeDSecureUsage2;
                return builder;
            }

            public final Builder setWallet(Wallet wallet2) {
                Builder builder = this;
                builder.wallet = wallet2;
                return builder;
            }

            public Card build() {
                return new Card(this.brand, this.checks, this.country, this.expiryMonth, this.expiryYear, this.funding, this.last4, this.threeDSecureUsage, this.wallet, (Networks) null, 512, (DefaultConstructorMarker) null);
            }
        }

        @Metadata(bv = {1, 0, 3}, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\b\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\b\b\u0018\u00002\u00020\u0001B%\b\u0000\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\b\u0010\u0004\u001a\u0004\u0018\u00010\u0003\u0012\b\u0010\u0005\u001a\u0004\u0018\u00010\u0003¢\u0006\u0002\u0010\u0006J\u000b\u0010\u0007\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000b\u0010\b\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000b\u0010\t\u001a\u0004\u0018\u00010\u0003HÆ\u0003J-\u0010\n\u001a\u00020\u00002\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u0003HÆ\u0001J\t\u0010\u000b\u001a\u00020\fHÖ\u0001J\u0013\u0010\r\u001a\u00020\u000e2\b\u0010\u000f\u001a\u0004\u0018\u00010\u0010HÖ\u0003J\t\u0010\u0011\u001a\u00020\fHÖ\u0001J\t\u0010\u0012\u001a\u00020\u0003HÖ\u0001J\u0019\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\fHÖ\u0001R\u0012\u0010\u0002\u001a\u0004\u0018\u00010\u00038\u0006X\u0004¢\u0006\u0002\n\u0000R\u0012\u0010\u0004\u001a\u0004\u0018\u00010\u00038\u0006X\u0004¢\u0006\u0002\n\u0000R\u0012\u0010\u0005\u001a\u0004\u0018\u00010\u00038\u0006X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0018"}, d2 = {"Lcom/stripe/android/model/PaymentMethod$Card$Checks;", "Lcom/stripe/android/model/StripeModel;", "addressLine1Check", "", "addressPostalCodeCheck", "cvcCheck", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", "component1", "component2", "component3", "copy", "describeContents", "", "equals", "", "other", "", "hashCode", "toString", "writeToParcel", "", "parcel", "Landroid/os/Parcel;", "flags", "stripe_release"}, k = 1, mv = {1, 1, 16})
        /* compiled from: PaymentMethod.kt */
        public static final class Checks implements StripeModel {
            public static final Parcelable.Creator CREATOR = new Creator();
            public final String addressLine1Check;
            public final String addressPostalCodeCheck;
            public final String cvcCheck;

            @Metadata(bv = {1, 0, 3}, k = 3, mv = {1, 1, 16})
            public static class Creator implements Parcelable.Creator {
                public final Object createFromParcel(Parcel parcel) {
                    Intrinsics.checkParameterIsNotNull(parcel, "in");
                    return new Checks(parcel.readString(), parcel.readString(), parcel.readString());
                }

                public final Object[] newArray(int i) {
                    return new Checks[i];
                }
            }

            public static /* synthetic */ Checks copy$default(Checks checks, String str, String str2, String str3, int i, Object obj) {
                if ((i & 1) != 0) {
                    str = checks.addressLine1Check;
                }
                if ((i & 2) != 0) {
                    str2 = checks.addressPostalCodeCheck;
                }
                if ((i & 4) != 0) {
                    str3 = checks.cvcCheck;
                }
                return checks.copy(str, str2, str3);
            }

            public final String component1() {
                return this.addressLine1Check;
            }

            public final String component2() {
                return this.addressPostalCodeCheck;
            }

            public final String component3() {
                return this.cvcCheck;
            }

            public final Checks copy(String str, String str2, String str3) {
                return new Checks(str, str2, str3);
            }

            public int describeContents() {
                return 0;
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof Checks)) {
                    return false;
                }
                Checks checks = (Checks) obj;
                return Intrinsics.areEqual((Object) this.addressLine1Check, (Object) checks.addressLine1Check) && Intrinsics.areEqual((Object) this.addressPostalCodeCheck, (Object) checks.addressPostalCodeCheck) && Intrinsics.areEqual((Object) this.cvcCheck, (Object) checks.cvcCheck);
            }

            public int hashCode() {
                String str = this.addressLine1Check;
                int i = 0;
                int hashCode = (str != null ? str.hashCode() : 0) * 31;
                String str2 = this.addressPostalCodeCheck;
                int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
                String str3 = this.cvcCheck;
                if (str3 != null) {
                    i = str3.hashCode();
                }
                return hashCode2 + i;
            }

            public String toString() {
                return "Checks(addressLine1Check=" + this.addressLine1Check + ", addressPostalCodeCheck=" + this.addressPostalCodeCheck + ", cvcCheck=" + this.cvcCheck + ")";
            }

            public void writeToParcel(Parcel parcel, int i) {
                Intrinsics.checkParameterIsNotNull(parcel, "parcel");
                parcel.writeString(this.addressLine1Check);
                parcel.writeString(this.addressPostalCodeCheck);
                parcel.writeString(this.cvcCheck);
            }

            public Checks(String str, String str2, String str3) {
                this.addressLine1Check = str;
                this.addressPostalCodeCheck = str2;
                this.cvcCheck = str3;
            }
        }

        @Metadata(bv = {1, 0, 3}, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\b\b\u0018\u00002\u00020\u0001B\u000f\b\u0000\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\t\u0010\u0005\u001a\u00020\u0003HÆ\u0003J\u0013\u0010\u0006\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u0003HÆ\u0001J\t\u0010\u0007\u001a\u00020\bHÖ\u0001J\u0013\u0010\t\u001a\u00020\u00032\b\u0010\n\u001a\u0004\u0018\u00010\u000bHÖ\u0003J\t\u0010\f\u001a\u00020\bHÖ\u0001J\t\u0010\r\u001a\u00020\u000eHÖ\u0001J\u0019\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\bHÖ\u0001R\u0010\u0010\u0002\u001a\u00020\u00038\u0006X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0014"}, d2 = {"Lcom/stripe/android/model/PaymentMethod$Card$ThreeDSecureUsage;", "Lcom/stripe/android/model/StripeModel;", "isSupported", "", "(Z)V", "component1", "copy", "describeContents", "", "equals", "other", "", "hashCode", "toString", "", "writeToParcel", "", "parcel", "Landroid/os/Parcel;", "flags", "stripe_release"}, k = 1, mv = {1, 1, 16})
        /* compiled from: PaymentMethod.kt */
        public static final class ThreeDSecureUsage implements StripeModel {
            public static final Parcelable.Creator CREATOR = new Creator();
            public final boolean isSupported;

            @Metadata(bv = {1, 0, 3}, k = 3, mv = {1, 1, 16})
            public static class Creator implements Parcelable.Creator {
                public final Object createFromParcel(Parcel parcel) {
                    Intrinsics.checkParameterIsNotNull(parcel, "in");
                    return new ThreeDSecureUsage(parcel.readInt() != 0);
                }

                public final Object[] newArray(int i) {
                    return new ThreeDSecureUsage[i];
                }
            }

            public static /* synthetic */ ThreeDSecureUsage copy$default(ThreeDSecureUsage threeDSecureUsage, boolean z, int i, Object obj) {
                if ((i & 1) != 0) {
                    z = threeDSecureUsage.isSupported;
                }
                return threeDSecureUsage.copy(z);
            }

            public final boolean component1() {
                return this.isSupported;
            }

            public final ThreeDSecureUsage copy(boolean z) {
                return new ThreeDSecureUsage(z);
            }

            public int describeContents() {
                return 0;
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof ThreeDSecureUsage) && this.isSupported == ((ThreeDSecureUsage) obj).isSupported;
                }
                return true;
            }

            public int hashCode() {
                boolean z = this.isSupported;
                if (z) {
                    return 1;
                }
                return z ? 1 : 0;
            }

            public String toString() {
                return "ThreeDSecureUsage(isSupported=" + this.isSupported + ")";
            }

            public void writeToParcel(Parcel parcel, int i) {
                Intrinsics.checkParameterIsNotNull(parcel, "parcel");
                parcel.writeInt(this.isSupported ? 1 : 0);
            }

            public ThreeDSecureUsage(boolean z) {
                this.isSupported = z;
            }
        }

        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\"\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\b\b\u0018\u00002\u00020\u0001B+\u0012\u000e\b\u0002\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003\u0012\b\b\u0002\u0010\u0005\u001a\u00020\u0006\u0012\n\b\u0002\u0010\u0007\u001a\u0004\u0018\u00010\u0004¢\u0006\u0002\u0010\bJ\u000f\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003HÂ\u0003J\t\u0010\n\u001a\u00020\u0006HÂ\u0003J\u000b\u0010\u000b\u001a\u0004\u0018\u00010\u0004HÂ\u0003J/\u0010\f\u001a\u00020\u00002\u000e\b\u0002\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u00032\b\b\u0002\u0010\u0005\u001a\u00020\u00062\n\b\u0002\u0010\u0007\u001a\u0004\u0018\u00010\u0004HÆ\u0001J\t\u0010\r\u001a\u00020\u000eHÖ\u0001J\u0013\u0010\u000f\u001a\u00020\u00062\b\u0010\u0010\u001a\u0004\u0018\u00010\u0011HÖ\u0003J\t\u0010\u0012\u001a\u00020\u000eHÖ\u0001J\t\u0010\u0013\u001a\u00020\u0004HÖ\u0001J\u0019\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u000eHÖ\u0001R\u0014\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003X\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u0007\u001a\u0004\u0018\u00010\u0004X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0019"}, d2 = {"Lcom/stripe/android/model/PaymentMethod$Card$Networks;", "Lcom/stripe/android/model/StripeModel;", "available", "", "", "selectionMandatory", "", "preferred", "(Ljava/util/Set;ZLjava/lang/String;)V", "component1", "component2", "component3", "copy", "describeContents", "", "equals", "other", "", "hashCode", "toString", "writeToParcel", "", "parcel", "Landroid/os/Parcel;", "flags", "stripe_release"}, k = 1, mv = {1, 1, 16})
        /* compiled from: PaymentMethod.kt */
        public static final class Networks implements StripeModel {
            public static final Parcelable.Creator CREATOR = new Creator();
            private final Set<String> available;
            private final String preferred;
            private final boolean selectionMandatory;

            @Metadata(bv = {1, 0, 3}, k = 3, mv = {1, 1, 16})
            public static class Creator implements Parcelable.Creator {
                public final Object createFromParcel(Parcel parcel) {
                    Intrinsics.checkParameterIsNotNull(parcel, "in");
                    int readInt = parcel.readInt();
                    LinkedHashSet linkedHashSet = new LinkedHashSet(readInt);
                    while (readInt != 0) {
                        linkedHashSet.add(parcel.readString());
                        readInt--;
                    }
                    return new Networks(linkedHashSet, parcel.readInt() != 0, parcel.readString());
                }

                public final Object[] newArray(int i) {
                    return new Networks[i];
                }
            }

            public Networks() {
                this((Set) null, false, (String) null, 7, (DefaultConstructorMarker) null);
            }

            private final Set<String> component1() {
                return this.available;
            }

            private final boolean component2() {
                return this.selectionMandatory;
            }

            private final String component3() {
                return this.preferred;
            }

            public static /* synthetic */ Networks copy$default(Networks networks, Set<String> set, boolean z, String str, int i, Object obj) {
                if ((i & 1) != 0) {
                    set = networks.available;
                }
                if ((i & 2) != 0) {
                    z = networks.selectionMandatory;
                }
                if ((i & 4) != 0) {
                    str = networks.preferred;
                }
                return networks.copy(set, z, str);
            }

            public final Networks copy(Set<String> set, boolean z, String str) {
                Intrinsics.checkParameterIsNotNull(set, "available");
                return new Networks(set, z, str);
            }

            public int describeContents() {
                return 0;
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof Networks)) {
                    return false;
                }
                Networks networks = (Networks) obj;
                return Intrinsics.areEqual((Object) this.available, (Object) networks.available) && this.selectionMandatory == networks.selectionMandatory && Intrinsics.areEqual((Object) this.preferred, (Object) networks.preferred);
            }

            public int hashCode() {
                Set<String> set = this.available;
                int i = 0;
                int hashCode = (set != null ? set.hashCode() : 0) * 31;
                boolean z = this.selectionMandatory;
                if (z) {
                    z = true;
                }
                int i2 = (hashCode + (z ? 1 : 0)) * 31;
                String str = this.preferred;
                if (str != null) {
                    i = str.hashCode();
                }
                return i2 + i;
            }

            public String toString() {
                return "Networks(available=" + this.available + ", selectionMandatory=" + this.selectionMandatory + ", preferred=" + this.preferred + ")";
            }

            public void writeToParcel(Parcel parcel, int i) {
                Intrinsics.checkParameterIsNotNull(parcel, "parcel");
                Set<String> set = this.available;
                parcel.writeInt(set.size());
                for (String writeString : set) {
                    parcel.writeString(writeString);
                }
                parcel.writeInt(this.selectionMandatory ? 1 : 0);
                parcel.writeString(this.preferred);
            }

            public Networks(Set<String> set, boolean z, String str) {
                Intrinsics.checkParameterIsNotNull(set, "available");
                this.available = set;
                this.selectionMandatory = z;
                this.preferred = str;
            }

            /* JADX INFO: this call moved to the top of the method (can break code semantics) */
            public /* synthetic */ Networks(Set set, boolean z, String str, int i, DefaultConstructorMarker defaultConstructorMarker) {
                this((i & 1) != 0 ? SetsKt.emptySet() : set, (i & 2) != 0 ? false : z, (i & 4) != 0 ? null : str);
            }
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\b\b\u0018\u0000 \u00142\u00020\u0001:\u0001\u0014B\u0011\b\u0000\u0012\b\b\u0002\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\t\u0010\u0005\u001a\u00020\u0003HÂ\u0003J\u0013\u0010\u0006\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u0003HÆ\u0001J\t\u0010\u0007\u001a\u00020\bHÖ\u0001J\u0013\u0010\t\u001a\u00020\u00032\b\u0010\n\u001a\u0004\u0018\u00010\u000bHÖ\u0003J\t\u0010\f\u001a\u00020\bHÖ\u0001J\t\u0010\r\u001a\u00020\u000eHÖ\u0001J\u0019\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\bHÖ\u0001R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0015"}, d2 = {"Lcom/stripe/android/model/PaymentMethod$CardPresent;", "Lcom/stripe/android/model/StripeModel;", "ignore", "", "(Z)V", "component1", "copy", "describeContents", "", "equals", "other", "", "hashCode", "toString", "", "writeToParcel", "", "parcel", "Landroid/os/Parcel;", "flags", "Companion", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: PaymentMethod.kt */
    public static final class CardPresent implements StripeModel {
        public static final Parcelable.Creator CREATOR = new Creator();
        public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
        /* access modifiers changed from: private */
        public static final /* synthetic */ CardPresent EMPTY = new CardPresent(false, 1, (DefaultConstructorMarker) null);
        private final boolean ignore;

        @Metadata(bv = {1, 0, 3}, k = 3, mv = {1, 1, 16})
        public static class Creator implements Parcelable.Creator {
            public final Object createFromParcel(Parcel parcel) {
                Intrinsics.checkParameterIsNotNull(parcel, "in");
                return new CardPresent(parcel.readInt() != 0);
            }

            public final Object[] newArray(int i) {
                return new CardPresent[i];
            }
        }

        public CardPresent() {
            this(false, 1, (DefaultConstructorMarker) null);
        }

        private final boolean component1() {
            return this.ignore;
        }

        public static /* synthetic */ CardPresent copy$default(CardPresent cardPresent, boolean z, int i, Object obj) {
            if ((i & 1) != 0) {
                z = cardPresent.ignore;
            }
            return cardPresent.copy(z);
        }

        public final CardPresent copy(boolean z) {
            return new CardPresent(z);
        }

        public int describeContents() {
            return 0;
        }

        public boolean equals(Object obj) {
            if (this != obj) {
                return (obj instanceof CardPresent) && this.ignore == ((CardPresent) obj).ignore;
            }
            return true;
        }

        public int hashCode() {
            boolean z = this.ignore;
            if (z) {
                return 1;
            }
            return z ? 1 : 0;
        }

        public String toString() {
            return "CardPresent(ignore=" + this.ignore + ")";
        }

        public void writeToParcel(Parcel parcel, int i) {
            Intrinsics.checkParameterIsNotNull(parcel, "parcel");
            parcel.writeInt(this.ignore ? 1 : 0);
        }

        public CardPresent(boolean z) {
            this.ignore = z;
        }

        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ CardPresent(boolean z, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this((i & 1) != 0 ? true : z);
        }

        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0014\u0010\u0003\u001a\u00020\u0004X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006¨\u0006\u0007"}, d2 = {"Lcom/stripe/android/model/PaymentMethod$CardPresent$Companion;", "", "()V", "EMPTY", "Lcom/stripe/android/model/PaymentMethod$CardPresent;", "getEMPTY$stripe_release", "()Lcom/stripe/android/model/PaymentMethod$CardPresent;", "stripe_release"}, k = 1, mv = {1, 1, 16})
        /* compiled from: PaymentMethod.kt */
        public static final class Companion {
            private Companion() {
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }

            public final CardPresent getEMPTY$stripe_release() {
                return CardPresent.EMPTY;
            }
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0006\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\b\b\u0018\u00002\u00020\u0001B\u001b\b\u0000\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\b\u0010\u0004\u001a\u0004\u0018\u00010\u0003¢\u0006\u0002\u0010\u0005J\u000b\u0010\u0006\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000b\u0010\u0007\u001a\u0004\u0018\u00010\u0003HÆ\u0003J!\u0010\b\u001a\u00020\u00002\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0003HÆ\u0001J\t\u0010\t\u001a\u00020\nHÖ\u0001J\u0013\u0010\u000b\u001a\u00020\f2\b\u0010\r\u001a\u0004\u0018\u00010\u000eHÖ\u0003J\t\u0010\u000f\u001a\u00020\nHÖ\u0001J\t\u0010\u0010\u001a\u00020\u0003HÖ\u0001J\u0019\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\nHÖ\u0001R\u0012\u0010\u0002\u001a\u0004\u0018\u00010\u00038\u0006X\u0004¢\u0006\u0002\n\u0000R\u0012\u0010\u0004\u001a\u0004\u0018\u00010\u00038\u0006X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0016"}, d2 = {"Lcom/stripe/android/model/PaymentMethod$Ideal;", "Lcom/stripe/android/model/StripeModel;", "bank", "", "bankIdentifierCode", "(Ljava/lang/String;Ljava/lang/String;)V", "component1", "component2", "copy", "describeContents", "", "equals", "", "other", "", "hashCode", "toString", "writeToParcel", "", "parcel", "Landroid/os/Parcel;", "flags", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: PaymentMethod.kt */
    public static final class Ideal implements StripeModel {
        public static final Parcelable.Creator CREATOR = new Creator();
        public final String bank;
        public final String bankIdentifierCode;

        @Metadata(bv = {1, 0, 3}, k = 3, mv = {1, 1, 16})
        public static class Creator implements Parcelable.Creator {
            public final Object createFromParcel(Parcel parcel) {
                Intrinsics.checkParameterIsNotNull(parcel, "in");
                return new Ideal(parcel.readString(), parcel.readString());
            }

            public final Object[] newArray(int i) {
                return new Ideal[i];
            }
        }

        public static /* synthetic */ Ideal copy$default(Ideal ideal, String str, String str2, int i, Object obj) {
            if ((i & 1) != 0) {
                str = ideal.bank;
            }
            if ((i & 2) != 0) {
                str2 = ideal.bankIdentifierCode;
            }
            return ideal.copy(str, str2);
        }

        public final String component1() {
            return this.bank;
        }

        public final String component2() {
            return this.bankIdentifierCode;
        }

        public final Ideal copy(String str, String str2) {
            return new Ideal(str, str2);
        }

        public int describeContents() {
            return 0;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Ideal)) {
                return false;
            }
            Ideal ideal = (Ideal) obj;
            return Intrinsics.areEqual((Object) this.bank, (Object) ideal.bank) && Intrinsics.areEqual((Object) this.bankIdentifierCode, (Object) ideal.bankIdentifierCode);
        }

        public int hashCode() {
            String str = this.bank;
            int i = 0;
            int hashCode = (str != null ? str.hashCode() : 0) * 31;
            String str2 = this.bankIdentifierCode;
            if (str2 != null) {
                i = str2.hashCode();
            }
            return hashCode + i;
        }

        public String toString() {
            return "Ideal(bank=" + this.bank + ", bankIdentifierCode=" + this.bankIdentifierCode + ")";
        }

        public void writeToParcel(Parcel parcel, int i) {
            Intrinsics.checkParameterIsNotNull(parcel, "parcel");
            parcel.writeString(this.bank);
            parcel.writeString(this.bankIdentifierCode);
        }

        public Ideal(String str, String str2) {
            this.bank = str;
            this.bankIdentifierCode = str2;
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0006\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\b\b\u0018\u00002\u00020\u0001B\u001b\b\u0000\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\b\u0010\u0004\u001a\u0004\u0018\u00010\u0003¢\u0006\u0002\u0010\u0005J\u000b\u0010\u0006\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000b\u0010\u0007\u001a\u0004\u0018\u00010\u0003HÆ\u0003J!\u0010\b\u001a\u00020\u00002\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0003HÆ\u0001J\t\u0010\t\u001a\u00020\nHÖ\u0001J\u0013\u0010\u000b\u001a\u00020\f2\b\u0010\r\u001a\u0004\u0018\u00010\u000eHÖ\u0003J\t\u0010\u000f\u001a\u00020\nHÖ\u0001J\t\u0010\u0010\u001a\u00020\u0003HÖ\u0001J\u0019\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\nHÖ\u0001R\u0012\u0010\u0004\u001a\u0004\u0018\u00010\u00038\u0006X\u0004¢\u0006\u0002\n\u0000R\u0012\u0010\u0002\u001a\u0004\u0018\u00010\u00038\u0006X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0016"}, d2 = {"Lcom/stripe/android/model/PaymentMethod$Fpx;", "Lcom/stripe/android/model/StripeModel;", "bank", "", "accountHolderType", "(Ljava/lang/String;Ljava/lang/String;)V", "component1", "component2", "copy", "describeContents", "", "equals", "", "other", "", "hashCode", "toString", "writeToParcel", "", "parcel", "Landroid/os/Parcel;", "flags", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: PaymentMethod.kt */
    public static final class Fpx implements StripeModel {
        public static final Parcelable.Creator CREATOR = new Creator();
        public final String accountHolderType;
        public final String bank;

        @Metadata(bv = {1, 0, 3}, k = 3, mv = {1, 1, 16})
        public static class Creator implements Parcelable.Creator {
            public final Object createFromParcel(Parcel parcel) {
                Intrinsics.checkParameterIsNotNull(parcel, "in");
                return new Fpx(parcel.readString(), parcel.readString());
            }

            public final Object[] newArray(int i) {
                return new Fpx[i];
            }
        }

        public static /* synthetic */ Fpx copy$default(Fpx fpx, String str, String str2, int i, Object obj) {
            if ((i & 1) != 0) {
                str = fpx.bank;
            }
            if ((i & 2) != 0) {
                str2 = fpx.accountHolderType;
            }
            return fpx.copy(str, str2);
        }

        public final String component1() {
            return this.bank;
        }

        public final String component2() {
            return this.accountHolderType;
        }

        public final Fpx copy(String str, String str2) {
            return new Fpx(str, str2);
        }

        public int describeContents() {
            return 0;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Fpx)) {
                return false;
            }
            Fpx fpx = (Fpx) obj;
            return Intrinsics.areEqual((Object) this.bank, (Object) fpx.bank) && Intrinsics.areEqual((Object) this.accountHolderType, (Object) fpx.accountHolderType);
        }

        public int hashCode() {
            String str = this.bank;
            int i = 0;
            int hashCode = (str != null ? str.hashCode() : 0) * 31;
            String str2 = this.accountHolderType;
            if (str2 != null) {
                i = str2.hashCode();
            }
            return hashCode + i;
        }

        public String toString() {
            return "Fpx(bank=" + this.bank + ", accountHolderType=" + this.accountHolderType + ")";
        }

        public void writeToParcel(Parcel parcel, int i) {
            Intrinsics.checkParameterIsNotNull(parcel, "parcel");
            parcel.writeString(this.bank);
            parcel.writeString(this.accountHolderType);
        }

        public Fpx(String str, String str2) {
            this.bank = str;
            this.accountHolderType = str2;
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\f\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\b\b\u0018\u00002\u00020\u0001B9\b\u0000\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\b\u0010\u0004\u001a\u0004\u0018\u00010\u0003\u0012\b\u0010\u0005\u001a\u0004\u0018\u00010\u0003\u0012\b\u0010\u0006\u001a\u0004\u0018\u00010\u0003\u0012\b\u0010\u0007\u001a\u0004\u0018\u00010\u0003¢\u0006\u0002\u0010\bJ\u000b\u0010\t\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000b\u0010\n\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000b\u0010\u000b\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000b\u0010\f\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000b\u0010\r\u001a\u0004\u0018\u00010\u0003HÆ\u0003JE\u0010\u000e\u001a\u00020\u00002\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u0007\u001a\u0004\u0018\u00010\u0003HÆ\u0001J\t\u0010\u000f\u001a\u00020\u0010HÖ\u0001J\u0013\u0010\u0011\u001a\u00020\u00122\b\u0010\u0013\u001a\u0004\u0018\u00010\u0014HÖ\u0003J\t\u0010\u0015\u001a\u00020\u0010HÖ\u0001J\t\u0010\u0016\u001a\u00020\u0003HÖ\u0001J\u0019\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00020\u001a2\u0006\u0010\u001b\u001a\u00020\u0010HÖ\u0001R\u0012\u0010\u0002\u001a\u0004\u0018\u00010\u00038\u0006X\u0004¢\u0006\u0002\n\u0000R\u0012\u0010\u0004\u001a\u0004\u0018\u00010\u00038\u0006X\u0004¢\u0006\u0002\n\u0000R\u0012\u0010\u0005\u001a\u0004\u0018\u00010\u00038\u0006X\u0004¢\u0006\u0002\n\u0000R\u0012\u0010\u0006\u001a\u0004\u0018\u00010\u00038\u0006X\u0004¢\u0006\u0002\n\u0000R\u0012\u0010\u0007\u001a\u0004\u0018\u00010\u00038\u0006X\u0004¢\u0006\u0002\n\u0000¨\u0006\u001c"}, d2 = {"Lcom/stripe/android/model/PaymentMethod$SepaDebit;", "Lcom/stripe/android/model/StripeModel;", "bankCode", "", "branchCode", "country", "fingerprint", "last4", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", "component1", "component2", "component3", "component4", "component5", "copy", "describeContents", "", "equals", "", "other", "", "hashCode", "toString", "writeToParcel", "", "parcel", "Landroid/os/Parcel;", "flags", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: PaymentMethod.kt */
    public static final class SepaDebit implements StripeModel {
        public static final Parcelable.Creator CREATOR = new Creator();
        public final String bankCode;
        public final String branchCode;
        public final String country;
        public final String fingerprint;
        public final String last4;

        @Metadata(bv = {1, 0, 3}, k = 3, mv = {1, 1, 16})
        public static class Creator implements Parcelable.Creator {
            public final Object createFromParcel(Parcel parcel) {
                Intrinsics.checkParameterIsNotNull(parcel, "in");
                return new SepaDebit(parcel.readString(), parcel.readString(), parcel.readString(), parcel.readString(), parcel.readString());
            }

            public final Object[] newArray(int i) {
                return new SepaDebit[i];
            }
        }

        public static /* synthetic */ SepaDebit copy$default(SepaDebit sepaDebit, String str, String str2, String str3, String str4, String str5, int i, Object obj) {
            if ((i & 1) != 0) {
                str = sepaDebit.bankCode;
            }
            if ((i & 2) != 0) {
                str2 = sepaDebit.branchCode;
            }
            String str6 = str2;
            if ((i & 4) != 0) {
                str3 = sepaDebit.country;
            }
            String str7 = str3;
            if ((i & 8) != 0) {
                str4 = sepaDebit.fingerprint;
            }
            String str8 = str4;
            if ((i & 16) != 0) {
                str5 = sepaDebit.last4;
            }
            return sepaDebit.copy(str, str6, str7, str8, str5);
        }

        public final String component1() {
            return this.bankCode;
        }

        public final String component2() {
            return this.branchCode;
        }

        public final String component3() {
            return this.country;
        }

        public final String component4() {
            return this.fingerprint;
        }

        public final String component5() {
            return this.last4;
        }

        public final SepaDebit copy(String str, String str2, String str3, String str4, String str5) {
            return new SepaDebit(str, str2, str3, str4, str5);
        }

        public int describeContents() {
            return 0;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof SepaDebit)) {
                return false;
            }
            SepaDebit sepaDebit = (SepaDebit) obj;
            return Intrinsics.areEqual((Object) this.bankCode, (Object) sepaDebit.bankCode) && Intrinsics.areEqual((Object) this.branchCode, (Object) sepaDebit.branchCode) && Intrinsics.areEqual((Object) this.country, (Object) sepaDebit.country) && Intrinsics.areEqual((Object) this.fingerprint, (Object) sepaDebit.fingerprint) && Intrinsics.areEqual((Object) this.last4, (Object) sepaDebit.last4);
        }

        public int hashCode() {
            String str = this.bankCode;
            int i = 0;
            int hashCode = (str != null ? str.hashCode() : 0) * 31;
            String str2 = this.branchCode;
            int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
            String str3 = this.country;
            int hashCode3 = (hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31;
            String str4 = this.fingerprint;
            int hashCode4 = (hashCode3 + (str4 != null ? str4.hashCode() : 0)) * 31;
            String str5 = this.last4;
            if (str5 != null) {
                i = str5.hashCode();
            }
            return hashCode4 + i;
        }

        public String toString() {
            return "SepaDebit(bankCode=" + this.bankCode + ", branchCode=" + this.branchCode + ", country=" + this.country + ", fingerprint=" + this.fingerprint + ", last4=" + this.last4 + ")";
        }

        public void writeToParcel(Parcel parcel, int i) {
            Intrinsics.checkParameterIsNotNull(parcel, "parcel");
            parcel.writeString(this.bankCode);
            parcel.writeString(this.branchCode);
            parcel.writeString(this.country);
            parcel.writeString(this.fingerprint);
            parcel.writeString(this.last4);
        }

        public SepaDebit(String str, String str2, String str3, String str4, String str5) {
            this.bankCode = str;
            this.branchCode = str2;
            this.country = str3;
            this.fingerprint = str4;
            this.last4 = str5;
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\b\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\b\b\u0018\u00002\u00020\u0001B%\b\u0000\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\b\u0010\u0004\u001a\u0004\u0018\u00010\u0003\u0012\b\u0010\u0005\u001a\u0004\u0018\u00010\u0003¢\u0006\u0002\u0010\u0006J\u000b\u0010\u0007\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000b\u0010\b\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000b\u0010\t\u001a\u0004\u0018\u00010\u0003HÆ\u0003J-\u0010\n\u001a\u00020\u00002\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u0003HÆ\u0001J\t\u0010\u000b\u001a\u00020\fHÖ\u0001J\u0013\u0010\r\u001a\u00020\u000e2\b\u0010\u000f\u001a\u0004\u0018\u00010\u0010HÖ\u0003J\t\u0010\u0011\u001a\u00020\fHÖ\u0001J\t\u0010\u0012\u001a\u00020\u0003HÖ\u0001J\u0019\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\fHÖ\u0001R\u0012\u0010\u0002\u001a\u0004\u0018\u00010\u00038\u0006X\u0004¢\u0006\u0002\n\u0000R\u0012\u0010\u0004\u001a\u0004\u0018\u00010\u00038\u0006X\u0004¢\u0006\u0002\n\u0000R\u0012\u0010\u0005\u001a\u0004\u0018\u00010\u00038\u0006X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0018"}, d2 = {"Lcom/stripe/android/model/PaymentMethod$AuBecsDebit;", "Lcom/stripe/android/model/StripeModel;", "bsbNumber", "", "fingerprint", "last4", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", "component1", "component2", "component3", "copy", "describeContents", "", "equals", "", "other", "", "hashCode", "toString", "writeToParcel", "", "parcel", "Landroid/os/Parcel;", "flags", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: PaymentMethod.kt */
    public static final class AuBecsDebit implements StripeModel {
        public static final Parcelable.Creator CREATOR = new Creator();
        public final String bsbNumber;
        public final String fingerprint;
        public final String last4;

        @Metadata(bv = {1, 0, 3}, k = 3, mv = {1, 1, 16})
        public static class Creator implements Parcelable.Creator {
            public final Object createFromParcel(Parcel parcel) {
                Intrinsics.checkParameterIsNotNull(parcel, "in");
                return new AuBecsDebit(parcel.readString(), parcel.readString(), parcel.readString());
            }

            public final Object[] newArray(int i) {
                return new AuBecsDebit[i];
            }
        }

        public static /* synthetic */ AuBecsDebit copy$default(AuBecsDebit auBecsDebit, String str, String str2, String str3, int i, Object obj) {
            if ((i & 1) != 0) {
                str = auBecsDebit.bsbNumber;
            }
            if ((i & 2) != 0) {
                str2 = auBecsDebit.fingerprint;
            }
            if ((i & 4) != 0) {
                str3 = auBecsDebit.last4;
            }
            return auBecsDebit.copy(str, str2, str3);
        }

        public final String component1() {
            return this.bsbNumber;
        }

        public final String component2() {
            return this.fingerprint;
        }

        public final String component3() {
            return this.last4;
        }

        public final AuBecsDebit copy(String str, String str2, String str3) {
            return new AuBecsDebit(str, str2, str3);
        }

        public int describeContents() {
            return 0;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof AuBecsDebit)) {
                return false;
            }
            AuBecsDebit auBecsDebit = (AuBecsDebit) obj;
            return Intrinsics.areEqual((Object) this.bsbNumber, (Object) auBecsDebit.bsbNumber) && Intrinsics.areEqual((Object) this.fingerprint, (Object) auBecsDebit.fingerprint) && Intrinsics.areEqual((Object) this.last4, (Object) auBecsDebit.last4);
        }

        public int hashCode() {
            String str = this.bsbNumber;
            int i = 0;
            int hashCode = (str != null ? str.hashCode() : 0) * 31;
            String str2 = this.fingerprint;
            int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
            String str3 = this.last4;
            if (str3 != null) {
                i = str3.hashCode();
            }
            return hashCode2 + i;
        }

        public String toString() {
            return "AuBecsDebit(bsbNumber=" + this.bsbNumber + ", fingerprint=" + this.fingerprint + ", last4=" + this.last4 + ")";
        }

        public void writeToParcel(Parcel parcel, int i) {
            Intrinsics.checkParameterIsNotNull(parcel, "parcel");
            parcel.writeString(this.bsbNumber);
            parcel.writeString(this.fingerprint);
            parcel.writeString(this.last4);
        }

        public AuBecsDebit(String str, String str2, String str3) {
            this.bsbNumber = str;
            this.fingerprint = str2;
            this.last4 = str3;
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\b\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\b\b\u0018\u00002\u00020\u0001B%\b\u0000\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\b\u0010\u0004\u001a\u0004\u0018\u00010\u0003\u0012\b\u0010\u0005\u001a\u0004\u0018\u00010\u0003¢\u0006\u0002\u0010\u0006J\u000b\u0010\u0007\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000b\u0010\b\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000b\u0010\t\u001a\u0004\u0018\u00010\u0003HÆ\u0003J-\u0010\n\u001a\u00020\u00002\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u0003HÆ\u0001J\t\u0010\u000b\u001a\u00020\fHÖ\u0001J\u0013\u0010\r\u001a\u00020\u000e2\b\u0010\u000f\u001a\u0004\u0018\u00010\u0010HÖ\u0003J\t\u0010\u0011\u001a\u00020\fHÖ\u0001J\t\u0010\u0012\u001a\u00020\u0003HÖ\u0001J\u0019\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\fHÖ\u0001R\u0012\u0010\u0002\u001a\u0004\u0018\u00010\u00038\u0006X\u0004¢\u0006\u0002\n\u0000R\u0012\u0010\u0004\u001a\u0004\u0018\u00010\u00038\u0006X\u0004¢\u0006\u0002\n\u0000R\u0012\u0010\u0005\u001a\u0004\u0018\u00010\u00038\u0006X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0018"}, d2 = {"Lcom/stripe/android/model/PaymentMethod$BacsDebit;", "Lcom/stripe/android/model/StripeModel;", "fingerprint", "", "last4", "sortCode", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", "component1", "component2", "component3", "copy", "describeContents", "", "equals", "", "other", "", "hashCode", "toString", "writeToParcel", "", "parcel", "Landroid/os/Parcel;", "flags", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: PaymentMethod.kt */
    public static final class BacsDebit implements StripeModel {
        public static final Parcelable.Creator CREATOR = new Creator();
        public final String fingerprint;
        public final String last4;
        public final String sortCode;

        @Metadata(bv = {1, 0, 3}, k = 3, mv = {1, 1, 16})
        public static class Creator implements Parcelable.Creator {
            public final Object createFromParcel(Parcel parcel) {
                Intrinsics.checkParameterIsNotNull(parcel, "in");
                return new BacsDebit(parcel.readString(), parcel.readString(), parcel.readString());
            }

            public final Object[] newArray(int i) {
                return new BacsDebit[i];
            }
        }

        public static /* synthetic */ BacsDebit copy$default(BacsDebit bacsDebit, String str, String str2, String str3, int i, Object obj) {
            if ((i & 1) != 0) {
                str = bacsDebit.fingerprint;
            }
            if ((i & 2) != 0) {
                str2 = bacsDebit.last4;
            }
            if ((i & 4) != 0) {
                str3 = bacsDebit.sortCode;
            }
            return bacsDebit.copy(str, str2, str3);
        }

        public final String component1() {
            return this.fingerprint;
        }

        public final String component2() {
            return this.last4;
        }

        public final String component3() {
            return this.sortCode;
        }

        public final BacsDebit copy(String str, String str2, String str3) {
            return new BacsDebit(str, str2, str3);
        }

        public int describeContents() {
            return 0;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof BacsDebit)) {
                return false;
            }
            BacsDebit bacsDebit = (BacsDebit) obj;
            return Intrinsics.areEqual((Object) this.fingerprint, (Object) bacsDebit.fingerprint) && Intrinsics.areEqual((Object) this.last4, (Object) bacsDebit.last4) && Intrinsics.areEqual((Object) this.sortCode, (Object) bacsDebit.sortCode);
        }

        public int hashCode() {
            String str = this.fingerprint;
            int i = 0;
            int hashCode = (str != null ? str.hashCode() : 0) * 31;
            String str2 = this.last4;
            int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
            String str3 = this.sortCode;
            if (str3 != null) {
                i = str3.hashCode();
            }
            return hashCode2 + i;
        }

        public String toString() {
            return "BacsDebit(fingerprint=" + this.fingerprint + ", last4=" + this.last4 + ", sortCode=" + this.sortCode + ")";
        }

        public void writeToParcel(Parcel parcel, int i) {
            Intrinsics.checkParameterIsNotNull(parcel, "parcel");
            parcel.writeString(this.fingerprint);
            parcel.writeString(this.last4);
            parcel.writeString(this.sortCode);
        }

        public BacsDebit(String str, String str2, String str3) {
            this.fingerprint = str;
            this.last4 = str2;
            this.sortCode = str3;
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\b\b\u0018\u00002\u00020\u0001B\u0011\b\u0000\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003¢\u0006\u0002\u0010\u0004J\u000b\u0010\u0005\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u0015\u0010\u0006\u001a\u00020\u00002\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u0003HÆ\u0001J\t\u0010\u0007\u001a\u00020\bHÖ\u0001J\u0013\u0010\t\u001a\u00020\n2\b\u0010\u000b\u001a\u0004\u0018\u00010\fHÖ\u0003J\t\u0010\r\u001a\u00020\bHÖ\u0001J\t\u0010\u000e\u001a\u00020\u0003HÖ\u0001J\u0019\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\bHÖ\u0001R\u0012\u0010\u0002\u001a\u0004\u0018\u00010\u00038\u0006X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0014"}, d2 = {"Lcom/stripe/android/model/PaymentMethod$Sofort;", "Lcom/stripe/android/model/StripeModel;", "country", "", "(Ljava/lang/String;)V", "component1", "copy", "describeContents", "", "equals", "", "other", "", "hashCode", "toString", "writeToParcel", "", "parcel", "Landroid/os/Parcel;", "flags", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: PaymentMethod.kt */
    public static final class Sofort implements StripeModel {
        public static final Parcelable.Creator CREATOR = new Creator();
        public final String country;

        @Metadata(bv = {1, 0, 3}, k = 3, mv = {1, 1, 16})
        public static class Creator implements Parcelable.Creator {
            public final Object createFromParcel(Parcel parcel) {
                Intrinsics.checkParameterIsNotNull(parcel, "in");
                return new Sofort(parcel.readString());
            }

            public final Object[] newArray(int i) {
                return new Sofort[i];
            }
        }

        public static /* synthetic */ Sofort copy$default(Sofort sofort, String str, int i, Object obj) {
            if ((i & 1) != 0) {
                str = sofort.country;
            }
            return sofort.copy(str);
        }

        public final String component1() {
            return this.country;
        }

        public final Sofort copy(String str) {
            return new Sofort(str);
        }

        public int describeContents() {
            return 0;
        }

        public boolean equals(Object obj) {
            if (this != obj) {
                return (obj instanceof Sofort) && Intrinsics.areEqual((Object) this.country, (Object) ((Sofort) obj).country);
            }
            return true;
        }

        public int hashCode() {
            String str = this.country;
            if (str != null) {
                return str.hashCode();
            }
            return 0;
        }

        public String toString() {
            return "Sofort(country=" + this.country + ")";
        }

        public void writeToParcel(Parcel parcel, int i) {
            Intrinsics.checkParameterIsNotNull(parcel, "parcel");
            parcel.writeString(this.country);
        }

        public Sofort(String str) {
            this.country = str;
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0014\u0010\u0003\u001a\u0004\u0018\u00010\u00042\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006H\u0007¨\u0006\u0007"}, d2 = {"Lcom/stripe/android/model/PaymentMethod$Companion;", "", "()V", "fromJson", "Lcom/stripe/android/model/PaymentMethod;", "paymentMethod", "Lorg/json/JSONObject;", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: PaymentMethod.kt */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        @JvmStatic
        public final PaymentMethod fromJson(JSONObject jSONObject) {
            if (jSONObject != null) {
                return new PaymentMethodJsonParser().parse(jSONObject);
            }
            return null;
        }
    }
}
