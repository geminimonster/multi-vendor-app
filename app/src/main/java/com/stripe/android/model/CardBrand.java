package com.stripe.android.model;

import com.facebook.internal.AnalyticsEvents;
import com.stripe.android.R;
import java.util.ArrayList;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import kotlin.Metadata;
import kotlin.TuplesKt;
import kotlin.TypeCastException;
import kotlin.collections.CollectionsKt;
import kotlin.collections.MapsKt;
import kotlin.collections.SetsKt;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.text.StringsKt;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\"\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\b\u001c\n\u0002\u0010\u0011\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u000e\b\u0001\u0018\u0000 <2\b\u0012\u0004\u0012\u00020\u00000\u0001:\u0001<B³\u0001\b\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\b\b\u0001\u0010\u0005\u001a\u00020\u0006\u0012\b\b\u0003\u0010\u0007\u001a\u00020\u0006\u0012\b\b\u0003\u0010\b\u001a\u00020\u0006\u0012\u000e\b\u0002\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00060\n\u0012\b\b\u0002\u0010\u000b\u001a\u00020\u0006\u0012\n\b\u0002\u0010\f\u001a\u0004\u0018\u00010\r\u0012\u0014\b\u0002\u0010\u000e\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\r0\u000f\u0012\u000e\b\u0002\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u00060\n\u0012\u0014\b\u0002\u0010\u0011\u001a\u000e\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\u00060\u000f\u0012\u001a\b\u0002\u0010\u0012\u001a\u0014\u0012\u0004\u0012\u00020\r\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00060\n0\u000f¢\u0006\u0002\u0010\u0013J\u000e\u0010%\u001a\u00020\u00032\u0006\u0010&\u001a\u00020\u0003J\u000e\u0010'\u001a\u00020\u00062\u0006\u0010&\u001a\u00020\u0003J\u000e\u0010(\u001a\u00020\u00062\u0006\u0010&\u001a\u00020\u0003J\u0012\u0010)\u001a\u0004\u0018\u00010\r2\u0006\u0010&\u001a\u00020\u0003H\u0002J\u0014\u0010*\u001a\b\u0012\u0004\u0012\u00020\u00060\n2\u0006\u0010&\u001a\u00020\u0003J\u001b\u0010+\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00030,2\u0006\u0010&\u001a\u00020\u0003¢\u0006\u0002\u0010-J\u0010\u0010.\u001a\u00020/2\b\u00100\u001a\u0004\u0018\u00010\u0003J\u0010\u00101\u001a\u00020/2\b\u0010&\u001a\u0004\u0018\u00010\u0003J\u000e\u00102\u001a\u00020/2\u0006\u00103\u001a\u00020\u0003R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0014\u0010\u0015R\u0011\u0010\u0007\u001a\u00020\u0006¢\u0006\b\n\u0000\u001a\u0004\b\u0016\u0010\u0017R\u0017\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00060\n¢\u0006\b\n\u0000\u001a\u0004\b\u0018\u0010\u0019R\u0011\u0010\u000b\u001a\u00020\u0006¢\u0006\b\n\u0000\u001a\u0004\b\u001a\u0010\u0017R\u0011\u0010\u001b\u001a\u00020\u0006¢\u0006\b\n\u0000\u001a\u0004\b\u001c\u0010\u0017R\u0017\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u00060\n¢\u0006\b\n\u0000\u001a\u0004\b\u001d\u0010\u0019R\u0011\u0010\u0004\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u001e\u0010\u0015R\u0011\u0010\b\u001a\u00020\u0006¢\u0006\b\n\u0000\u001a\u0004\b\u001f\u0010\u0017R\u0011\u0010\u0005\u001a\u00020\u0006¢\u0006\b\n\u0000\u001a\u0004\b \u0010\u0017R\u0011\u0010!\u001a\u00020\u00068F¢\u0006\u0006\u001a\u0004\b\"\u0010\u0017R\u001a\u0010\u000e\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\r0\u000fX\u0004¢\u0006\u0002\n\u0000R\u0013\u0010\f\u001a\u0004\u0018\u00010\r¢\u0006\b\n\u0000\u001a\u0004\b#\u0010$R\u001a\u0010\u0011\u001a\u000e\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\u00060\u000fX\u0004¢\u0006\u0002\n\u0000R \u0010\u0012\u001a\u0014\u0012\u0004\u0012\u00020\r\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00060\n0\u000fX\u0004¢\u0006\u0002\n\u0000j\u0002\b4j\u0002\b5j\u0002\b6j\u0002\b7j\u0002\b8j\u0002\b9j\u0002\b:j\u0002\b;¨\u0006="}, d2 = {"Lcom/stripe/android/model/CardBrand;", "", "code", "", "displayName", "icon", "", "cvcIcon", "errorIcon", "cvcLength", "", "defaultMaxLength", "pattern", "Ljava/util/regex/Pattern;", "partialPatterns", "", "defaultSpacePositions", "variantMaxLength", "variantSpacePositions", "(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IIILjava/util/Set;ILjava/util/regex/Pattern;Ljava/util/Map;Ljava/util/Set;Ljava/util/Map;Ljava/util/Map;)V", "getCode", "()Ljava/lang/String;", "getCvcIcon", "()I", "getCvcLength", "()Ljava/util/Set;", "getDefaultMaxLength", "defaultMaxLengthWithSpaces", "getDefaultMaxLengthWithSpaces", "getDefaultSpacePositions", "getDisplayName", "getErrorIcon", "getIcon", "maxCvcLength", "getMaxCvcLength", "getPattern", "()Ljava/util/regex/Pattern;", "formatNumber", "cardNumber", "getMaxLengthForCardNumber", "getMaxLengthWithSpacesForCardNumber", "getPatternForLength", "getSpacePositionsForCardNumber", "groupNumber", "", "(Ljava/lang/String;)[Ljava/lang/String;", "isMaxCvc", "", "cvcText", "isValidCardNumberLength", "isValidCvc", "cvc", "AmericanExpress", "Discover", "JCB", "DinersClub", "Visa", "MasterCard", "UnionPay", "Unknown", "Companion", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: CardBrand.kt */
public enum CardBrand {
    AmericanExpress("amex", "American Express", R.drawable.stripe_ic_amex, R.drawable.stripe_ic_cvc_amex, R.drawable.stripe_ic_error_amex, SetsKt.setOf(3, 4), 15, Pattern.compile("^(34|37)[0-9]*$"), (int) null, SetsKt.setOf(4, 11), (Map) null, (Set) null, 3328, (Map) null),
    Discover("discover", "Discover", R.drawable.stripe_ic_discover, 0, 0, (int) null, 0, Pattern.compile("^(60|64|65)[0-9]*$"), (int) null, (Pattern) null, (Map) null, (Set) null, 3960, (Map) null),
    JCB("jcb", "JCB", R.drawable.stripe_ic_jcb, 0, 0, (int) null, 0, Pattern.compile("^(352[89]|35[3-8][0-9])[0-9]*$"), MapsKt.mapOf(TuplesKt.to(2, Pattern.compile("^(35)$")), TuplesKt.to(3, Pattern.compile("^(35[2-8])$"))), (Pattern) null, (Map) null, (Set) null, 3704, (Map) null),
    DinersClub("diners", "Diners Club", R.drawable.stripe_ic_diners, 0, 0, (int) null, 16, Pattern.compile("^(36|30|38|39)[0-9]*$"), (int) null, (Pattern) null, MapsKt.mapOf(TuplesKt.to(Pattern.compile("^(36)[0-9]*$"), 14)), MapsKt.mapOf(TuplesKt.to(Pattern.compile("^(36)[0-9]*$"), SetsKt.setOf(4, 11))), 824, (Map) null),
    Visa("visa", "Visa", R.drawable.stripe_ic_visa, 0, 0, (int) null, 0, Pattern.compile("^(4)[0-9]*$"), (int) null, (Pattern) null, (Map) null, (Set) null, 3960, (Map) null),
    MasterCard("mastercard", "Mastercard", R.drawable.stripe_ic_mastercard, 0, 0, (int) null, 0, Pattern.compile("^(2221|2222|2223|2224|2225|2226|2227|2228|2229|222|223|224|225|226|227|228|229|23|24|25|26|270|271|2720|50|51|52|53|54|55|56|57|58|59|67)[0-9]*$"), MapsKt.mapOf(TuplesKt.to(2, Pattern.compile("^(22|23|24|25|26|27|50|51|52|53|54|55|56|57|58|59|67)$"))), (Pattern) null, (Map) null, (Set) null, 3704, (Map) null),
    UnionPay("unionpay", "UnionPay", R.drawable.stripe_ic_unionpay, 0, 0, (int) null, 0, Pattern.compile("^(62|81)[0-9]*$"), (int) null, (Pattern) null, (Map) null, (Set) null, 3960, (Map) null),
    Unknown("unknown", AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN, R.drawable.stripe_ic_unknown, 0, 0, SetsKt.setOf(3, 4), 0, (Set) null, (int) null, (Pattern) null, (Map) null, (Set) null, 4056, (Map) null);
    
    private static final int CVC_COMMON_LENGTH = 3;
    public static final Companion Companion = null;
    private final String code;
    private final int cvcIcon;
    private final Set<Integer> cvcLength;
    private final int defaultMaxLength;
    private final int defaultMaxLengthWithSpaces;
    private final Set<Integer> defaultSpacePositions;
    private final String displayName;
    private final int errorIcon;
    private final int icon;
    private final Map<Integer, Pattern> partialPatterns;
    private final Pattern pattern;
    private final Map<Pattern, Integer> variantMaxLength;
    private final Map<Pattern, Set<Integer>> variantSpacePositions;

    private CardBrand(String str, String str2, int i, int i2, int i3, Set<Integer> set, int i4, Pattern pattern2, Map<Integer, Pattern> map, Set<Integer> set2, Map<Pattern, Integer> map2, Map<Pattern, ? extends Set<Integer>> map3) {
        this.code = str;
        this.displayName = str2;
        this.icon = i;
        this.cvcIcon = i2;
        this.errorIcon = i3;
        this.cvcLength = set;
        this.defaultMaxLength = i4;
        this.pattern = pattern2;
        this.partialPatterns = map;
        this.defaultSpacePositions = set2;
        this.variantMaxLength = map2;
        this.variantSpacePositions = map3;
        this.defaultMaxLengthWithSpaces = i4 + set2.size();
    }

    public final String getCode() {
        return this.code;
    }

    public final String getDisplayName() {
        return this.displayName;
    }

    public final int getIcon() {
        return this.icon;
    }

    public final int getCvcIcon() {
        return this.cvcIcon;
    }

    public final int getErrorIcon() {
        return this.errorIcon;
    }

    public final Set<Integer> getCvcLength() {
        return this.cvcLength;
    }

    public final int getDefaultMaxLength() {
        return this.defaultMaxLength;
    }

    public final Pattern getPattern() {
        return this.pattern;
    }

    public final Set<Integer> getDefaultSpacePositions() {
        return this.defaultSpacePositions;
    }

    static {
        Companion = new Companion((DefaultConstructorMarker) null);
    }

    public final int getDefaultMaxLengthWithSpaces() {
        return this.defaultMaxLengthWithSpaces;
    }

    public final int getMaxCvcLength() {
        Integer num = (Integer) CollectionsKt.max(this.cvcLength);
        if (num != null) {
            return num.intValue();
        }
        return 3;
    }

    public final boolean isValidCardNumberLength(String str) {
        return (str == null || Unknown == this || str.length() != getMaxLengthForCardNumber(str)) ? false : true;
    }

    public final boolean isValidCvc(String str) {
        Intrinsics.checkParameterIsNotNull(str, "cvc");
        return this.cvcLength.contains(Integer.valueOf(str.length()));
    }

    /* JADX WARNING: Removed duplicated region for block: B:11:0x0025 A[ORIG_RETURN, RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:12:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean isMaxCvc(java.lang.String r3) {
        /*
            r2 = this;
            r0 = 0
            if (r3 == 0) goto L_0x001e
            if (r3 == 0) goto L_0x0016
            java.lang.CharSequence r3 = (java.lang.CharSequence) r3
            java.lang.CharSequence r3 = kotlin.text.StringsKt.trim((java.lang.CharSequence) r3)
            java.lang.String r3 = r3.toString()
            if (r3 == 0) goto L_0x001e
            int r3 = r3.length()
            goto L_0x001f
        L_0x0016:
            kotlin.TypeCastException r3 = new kotlin.TypeCastException
            java.lang.String r0 = "null cannot be cast to non-null type kotlin.CharSequence"
            r3.<init>(r0)
            throw r3
        L_0x001e:
            r3 = 0
        L_0x001f:
            int r1 = r2.getMaxCvcLength()
            if (r1 != r3) goto L_0x0026
            r0 = 1
        L_0x0026:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.stripe.android.model.CardBrand.isMaxCvc(java.lang.String):boolean");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0040, code lost:
        r5 = (java.lang.Integer) r1.getValue();
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final int getMaxLengthForCardNumber(java.lang.String r5) {
        /*
            r4 = this;
            java.lang.String r0 = "cardNumber"
            kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(r5, r0)
            java.lang.String r5 = com.stripe.android.StripeTextUtils.removeSpacesAndHyphens(r5)
            if (r5 == 0) goto L_0x000c
            goto L_0x000e
        L_0x000c:
            java.lang.String r5 = ""
        L_0x000e:
            java.util.Map<java.util.regex.Pattern, java.lang.Integer> r0 = r4.variantMaxLength
            java.util.Set r0 = r0.entrySet()
            java.lang.Iterable r0 = (java.lang.Iterable) r0
            java.util.Iterator r0 = r0.iterator()
        L_0x001a:
            boolean r1 = r0.hasNext()
            if (r1 == 0) goto L_0x003b
            java.lang.Object r1 = r0.next()
            r2 = r1
            java.util.Map$Entry r2 = (java.util.Map.Entry) r2
            java.lang.Object r2 = r2.getKey()
            java.util.regex.Pattern r2 = (java.util.regex.Pattern) r2
            r3 = r5
            java.lang.CharSequence r3 = (java.lang.CharSequence) r3
            java.util.regex.Matcher r2 = r2.matcher(r3)
            boolean r2 = r2.matches()
            if (r2 == 0) goto L_0x001a
            goto L_0x003c
        L_0x003b:
            r1 = 0
        L_0x003c:
            java.util.Map$Entry r1 = (java.util.Map.Entry) r1
            if (r1 == 0) goto L_0x004d
            java.lang.Object r5 = r1.getValue()
            java.lang.Integer r5 = (java.lang.Integer) r5
            if (r5 == 0) goto L_0x004d
            int r5 = r5.intValue()
            goto L_0x004f
        L_0x004d:
            int r5 = r4.defaultMaxLength
        L_0x004f:
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.stripe.android.model.CardBrand.getMaxLengthForCardNumber(java.lang.String):int");
    }

    public final int getMaxLengthWithSpacesForCardNumber(String str) {
        Intrinsics.checkParameterIsNotNull(str, "cardNumber");
        return getMaxLengthForCardNumber(str) + getSpacePositionsForCardNumber(str).size();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0040, code lost:
        r5 = (java.util.Set) r1.getValue();
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.util.Set<java.lang.Integer> getSpacePositionsForCardNumber(java.lang.String r5) {
        /*
            r4 = this;
            java.lang.String r0 = "cardNumber"
            kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(r5, r0)
            java.lang.String r5 = com.stripe.android.StripeTextUtils.removeSpacesAndHyphens(r5)
            if (r5 == 0) goto L_0x000c
            goto L_0x000e
        L_0x000c:
            java.lang.String r5 = ""
        L_0x000e:
            java.util.Map<java.util.regex.Pattern, java.util.Set<java.lang.Integer>> r0 = r4.variantSpacePositions
            java.util.Set r0 = r0.entrySet()
            java.lang.Iterable r0 = (java.lang.Iterable) r0
            java.util.Iterator r0 = r0.iterator()
        L_0x001a:
            boolean r1 = r0.hasNext()
            if (r1 == 0) goto L_0x003b
            java.lang.Object r1 = r0.next()
            r2 = r1
            java.util.Map$Entry r2 = (java.util.Map.Entry) r2
            java.lang.Object r2 = r2.getKey()
            java.util.regex.Pattern r2 = (java.util.regex.Pattern) r2
            r3 = r5
            java.lang.CharSequence r3 = (java.lang.CharSequence) r3
            java.util.regex.Matcher r2 = r2.matcher(r3)
            boolean r2 = r2.matches()
            if (r2 == 0) goto L_0x001a
            goto L_0x003c
        L_0x003b:
            r1 = 0
        L_0x003c:
            java.util.Map$Entry r1 = (java.util.Map.Entry) r1
            if (r1 == 0) goto L_0x0049
            java.lang.Object r5 = r1.getValue()
            java.util.Set r5 = (java.util.Set) r5
            if (r5 == 0) goto L_0x0049
            goto L_0x004b
        L_0x0049:
            java.util.Set<java.lang.Integer> r5 = r4.defaultSpacePositions
        L_0x004b:
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.stripe.android.model.CardBrand.getSpacePositionsForCardNumber(java.lang.String):java.util.Set");
    }

    public final String formatNumber(String str) {
        Intrinsics.checkParameterIsNotNull(str, "cardNumber");
        String[] groupNumber = groupNumber(str);
        ArrayList arrayList = new ArrayList();
        int length = groupNumber.length;
        for (int i = 0; i < length; i++) {
            String str2 = groupNumber[i];
            if (!(str2 != null)) {
                break;
            }
            arrayList.add(str2);
        }
        return CollectionsKt.joinToString$default(arrayList, " ", (CharSequence) null, (CharSequence) null, 0, (CharSequence) null, (Function1) null, 62, (Object) null);
    }

    public final String[] groupNumber(String str) {
        Intrinsics.checkParameterIsNotNull(str, "cardNumber");
        String take = StringsKt.take(str, getMaxLengthForCardNumber(str));
        Set<Integer> spacePositionsForCardNumber = getSpacePositionsForCardNumber(str);
        boolean z = true;
        int size = spacePositionsForCardNumber.size() + 1;
        String[] strArr = new String[size];
        int length = take.length();
        int i = 0;
        int i2 = 0;
        for (Object next : CollectionsKt.sorted(CollectionsKt.toList(spacePositionsForCardNumber))) {
            int i3 = i + 1;
            if (i < 0) {
                CollectionsKt.throwIndexOverflow();
            }
            int intValue = ((Number) next).intValue() - i;
            if (length > intValue) {
                if (take != null) {
                    String substring = take.substring(i2, intValue);
                    Intrinsics.checkExpressionValueIsNotNull(substring, "(this as java.lang.Strin…ing(startIndex, endIndex)");
                    strArr[i] = substring;
                    i2 = intValue;
                } else {
                    throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
                }
            }
            i = i3;
        }
        int i4 = 0;
        while (true) {
            if (i4 >= size) {
                i4 = -1;
                break;
            }
            if (strArr[i4] == null) {
                break;
            }
            i4++;
        }
        Integer valueOf = Integer.valueOf(i4);
        if (valueOf.intValue() == -1) {
            z = false;
        }
        if (!z) {
            valueOf = null;
        }
        if (valueOf != null) {
            int intValue2 = valueOf.intValue();
            if (take != null) {
                String substring2 = take.substring(i2);
                Intrinsics.checkExpressionValueIsNotNull(substring2, "(this as java.lang.String).substring(startIndex)");
                strArr[intValue2] = substring2;
            } else {
                throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
            }
        }
        return strArr;
    }

    /* access modifiers changed from: private */
    public final Pattern getPatternForLength(String str) {
        Pattern pattern2 = this.partialPatterns.get(Integer.valueOf(str.length()));
        return pattern2 != null ? pattern2 : this.pattern;
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0005\u001a\u00020\u00062\b\u0010\u0007\u001a\u0004\u0018\u00010\bJ\u0010\u0010\t\u001a\u00020\u00062\b\u0010\n\u001a\u0004\u0018\u00010\bR\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\u000b"}, d2 = {"Lcom/stripe/android/model/CardBrand$Companion;", "", "()V", "CVC_COMMON_LENGTH", "", "fromCardNumber", "Lcom/stripe/android/model/CardBrand;", "cardNumber", "", "fromCode", "code", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: CardBrand.kt */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        public final CardBrand fromCardNumber(String str) {
            CardBrand cardBrand;
            Matcher matcher;
            CharSequence charSequence = str;
            if (charSequence == null || StringsKt.isBlank(charSequence)) {
                return CardBrand.Unknown;
            }
            CardBrand[] values = CardBrand.values();
            int length = values.length;
            int i = 0;
            while (true) {
                if (i >= length) {
                    cardBrand = null;
                    break;
                }
                cardBrand = values[i];
                Pattern access$getPatternForLength = cardBrand.getPatternForLength(str);
                if ((access$getPatternForLength == null || (matcher = access$getPatternForLength.matcher(charSequence)) == null || !matcher.matches()) ? false : true) {
                    break;
                }
                i++;
            }
            return cardBrand != null ? cardBrand : CardBrand.Unknown;
        }

        public final CardBrand fromCode(String str) {
            CardBrand cardBrand;
            CardBrand[] values = CardBrand.values();
            int length = values.length;
            int i = 0;
            while (true) {
                if (i >= length) {
                    cardBrand = null;
                    break;
                }
                cardBrand = values[i];
                if (StringsKt.equals(cardBrand.getCode(), str, true)) {
                    break;
                }
                i++;
            }
            return cardBrand != null ? cardBrand : CardBrand.Unknown;
        }
    }
}
