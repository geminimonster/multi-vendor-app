package com.stripe.android.model;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import com.stripe.android.model.StripeIntent;
import com.stripe.android.model.parsers.SetupIntentJsonParser;
import java.util.List;
import java.util.regex.Pattern;
import kotlin.Deprecated;
import kotlin.Metadata;
import kotlin.ReplaceWith;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.internal.Intrinsics;
import org.json.JSONObject;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u000f\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0014\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\b\b\u0018\u0000 b2\u00020\u0001:\u0004`abcB\u0001\b\u0000\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\b\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\b\u0010\b\u001a\u0004\u0018\u00010\u0003\u0012\b\u0010\t\u001a\u0004\u0018\u00010\u0003\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\n\b\u0002\u0010\f\u001a\u0004\u0018\u00010\r\u0012\b\u0010\u000e\u001a\u0004\u0018\u00010\u0003\u0012\f\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u00030\u0010\u0012\b\u0010\u0011\u001a\u0004\u0018\u00010\u0012\u0012\b\u0010\u0013\u001a\u0004\u0018\u00010\u0014\u0012\n\b\u0002\u0010\u0015\u001a\u0004\u0018\u00010\u0016\u0012\b\u0010\u0017\u001a\u0004\u0018\u00010\u0018¢\u0006\u0002\u0010\u0019J\u000b\u0010D\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000b\u0010E\u001a\u0004\u0018\u00010\u0012HÆ\u0003J\u000b\u0010F\u001a\u0004\u0018\u00010\u0014HÆ\u0003J\u000b\u0010G\u001a\u0004\u0018\u00010\u0016HÆ\u0003J\u000b\u0010H\u001a\u0004\u0018\u00010\u0018HÆ\u0003J\u000b\u0010I\u001a\u0004\u0018\u00010\u0005HÆ\u0003J\t\u0010J\u001a\u00020\u0007HÆ\u0003J\u000b\u0010K\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000b\u0010L\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\t\u0010M\u001a\u00020\u000bHÆ\u0003J\u000b\u0010N\u001a\u0004\u0018\u00010\rHÆ\u0003J\u000b\u0010O\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000f\u0010P\u001a\b\u0012\u0004\u0012\u00020\u00030\u0010HÆ\u0003J¥\u0001\u0010Q\u001a\u00020\u00002\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u00072\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\t\u001a\u0004\u0018\u00010\u00032\b\b\u0002\u0010\n\u001a\u00020\u000b2\n\b\u0002\u0010\f\u001a\u0004\u0018\u00010\r2\n\b\u0002\u0010\u000e\u001a\u0004\u0018\u00010\u00032\u000e\b\u0002\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u00030\u00102\n\b\u0002\u0010\u0011\u001a\u0004\u0018\u00010\u00122\n\b\u0002\u0010\u0013\u001a\u0004\u0018\u00010\u00142\n\b\u0002\u0010\u0015\u001a\u0004\u0018\u00010\u00162\n\b\u0002\u0010\u0017\u001a\u0004\u0018\u00010\u0018HÆ\u0001J\t\u0010R\u001a\u00020SHÖ\u0001J\u0013\u0010T\u001a\u00020\u000b2\b\u0010U\u001a\u0004\u0018\u00010VHÖ\u0003J\t\u0010W\u001a\u00020SHÖ\u0001J\b\u0010X\u001a\u00020\u000bH\u0016J\b\u0010Y\u001a\u00020\u000bH\u0016J\t\u0010Z\u001a\u00020\u0003HÖ\u0001J\u0019\u0010[\u001a\u00020\\2\u0006\u0010]\u001a\u00020^2\u0006\u0010_\u001a\u00020SHÖ\u0001R\u0013\u0010\u0004\u001a\u0004\u0018\u00010\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u001a\u0010\u001bR\u0016\u0010\b\u001a\u0004\u0018\u00010\u0003X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u001c\u0010\u001dR\u0014\u0010\u0006\u001a\u00020\u0007X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u001e\u0010\u001fR\u0016\u0010\t\u001a\u0004\u0018\u00010\u0003X\u0004¢\u0006\b\n\u0000\u001a\u0004\b \u0010\u001dR\u0016\u0010\u0002\u001a\u0004\u0018\u00010\u0003X\u0004¢\u0006\b\n\u0000\u001a\u0004\b!\u0010\u001dR\u0014\u0010\n\u001a\u00020\u000bX\u0004¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\"R\u0013\u0010\u0015\u001a\u0004\u0018\u00010\u0016¢\u0006\b\n\u0000\u001a\u0004\b#\u0010$R\u0016\u0010\u0017\u001a\u0004\u0018\u00010\u0018X\u0004¢\u0006\b\n\u0000\u001a\u0004\b%\u0010&R\u0016\u0010'\u001a\u0004\u0018\u00010(8VX\u0004¢\u0006\u0006\u001a\u0004\b)\u0010*R\u0016\u0010\f\u001a\u0004\u0018\u00010\rX\u0004¢\u0006\b\n\u0000\u001a\u0004\b+\u0010,R\u0016\u0010\u000e\u001a\u0004\u0018\u00010\u0003X\u0004¢\u0006\b\n\u0000\u001a\u0004\b-\u0010\u001dR\u001a\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u00030\u0010X\u0004¢\u0006\b\n\u0000\u001a\u0004\b.\u0010/R\u001c\u00100\u001a\u0004\u0018\u0001018VX\u0004¢\u0006\f\u0012\u0004\b2\u00103\u001a\u0004\b4\u00105R\u001c\u00106\u001a\u0004\u0018\u0001078FX\u0004¢\u0006\f\u0012\u0004\b8\u00103\u001a\u0004\b9\u0010:R\u0016\u0010\u0011\u001a\u0004\u0018\u00010\u0012X\u0004¢\u0006\b\n\u0000\u001a\u0004\b;\u0010<R\u001c\u0010=\u001a\u0004\u0018\u00010>8VX\u0004¢\u0006\f\u0012\u0004\b?\u00103\u001a\u0004\b@\u0010AR\u0013\u0010\u0013\u001a\u0004\u0018\u00010\u0014¢\u0006\b\n\u0000\u001a\u0004\bB\u0010C¨\u0006d"}, d2 = {"Lcom/stripe/android/model/SetupIntent;", "Lcom/stripe/android/model/StripeIntent;", "id", "", "cancellationReason", "Lcom/stripe/android/model/SetupIntent$CancellationReason;", "created", "", "clientSecret", "description", "isLiveMode", "", "paymentMethod", "Lcom/stripe/android/model/PaymentMethod;", "paymentMethodId", "paymentMethodTypes", "", "status", "Lcom/stripe/android/model/StripeIntent$Status;", "usage", "Lcom/stripe/android/model/StripeIntent$Usage;", "lastSetupError", "Lcom/stripe/android/model/SetupIntent$Error;", "nextActionData", "Lcom/stripe/android/model/StripeIntent$NextActionData;", "(Ljava/lang/String;Lcom/stripe/android/model/SetupIntent$CancellationReason;JLjava/lang/String;Ljava/lang/String;ZLcom/stripe/android/model/PaymentMethod;Ljava/lang/String;Ljava/util/List;Lcom/stripe/android/model/StripeIntent$Status;Lcom/stripe/android/model/StripeIntent$Usage;Lcom/stripe/android/model/SetupIntent$Error;Lcom/stripe/android/model/StripeIntent$NextActionData;)V", "getCancellationReason", "()Lcom/stripe/android/model/SetupIntent$CancellationReason;", "getClientSecret", "()Ljava/lang/String;", "getCreated", "()J", "getDescription", "getId", "()Z", "getLastSetupError", "()Lcom/stripe/android/model/SetupIntent$Error;", "getNextActionData", "()Lcom/stripe/android/model/StripeIntent$NextActionData;", "nextActionType", "Lcom/stripe/android/model/StripeIntent$NextActionType;", "getNextActionType", "()Lcom/stripe/android/model/StripeIntent$NextActionType;", "getPaymentMethod", "()Lcom/stripe/android/model/PaymentMethod;", "getPaymentMethodId", "getPaymentMethodTypes", "()Ljava/util/List;", "redirectData", "Lcom/stripe/android/model/StripeIntent$RedirectData;", "redirectData$annotations", "()V", "getRedirectData", "()Lcom/stripe/android/model/StripeIntent$RedirectData;", "redirectUrl", "Landroid/net/Uri;", "redirectUrl$annotations", "getRedirectUrl", "()Landroid/net/Uri;", "getStatus", "()Lcom/stripe/android/model/StripeIntent$Status;", "stripeSdkData", "Lcom/stripe/android/model/StripeIntent$SdkData;", "stripeSdkData$annotations", "getStripeSdkData", "()Lcom/stripe/android/model/StripeIntent$SdkData;", "getUsage", "()Lcom/stripe/android/model/StripeIntent$Usage;", "component1", "component10", "component11", "component12", "component13", "component2", "component3", "component4", "component5", "component6", "component7", "component8", "component9", "copy", "describeContents", "", "equals", "other", "", "hashCode", "requiresAction", "requiresConfirmation", "toString", "writeToParcel", "", "parcel", "Landroid/os/Parcel;", "flags", "CancellationReason", "ClientSecret", "Companion", "Error", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: SetupIntent.kt */
public final class SetupIntent implements StripeIntent {
    public static final Parcelable.Creator CREATOR = new Creator();
    public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
    private final CancellationReason cancellationReason;
    private final String clientSecret;
    private final long created;
    private final String description;
    private final String id;
    private final boolean isLiveMode;
    private final Error lastSetupError;
    private final StripeIntent.NextActionData nextActionData;
    private final PaymentMethod paymentMethod;
    private final String paymentMethodId;
    private final List<String> paymentMethodTypes;
    private final StripeIntent.Status status;
    private final StripeIntent.Usage usage;

    @Metadata(bv = {1, 0, 3}, k = 3, mv = {1, 1, 16})
    public static class Creator implements Parcelable.Creator {
        public final Object createFromParcel(Parcel parcel) {
            Parcel parcel2 = parcel;
            Intrinsics.checkParameterIsNotNull(parcel2, "in");
            return new SetupIntent(parcel.readString(), parcel.readInt() != 0 ? (CancellationReason) Enum.valueOf(CancellationReason.class, parcel.readString()) : null, parcel.readLong(), parcel.readString(), parcel.readString(), parcel.readInt() != 0, parcel.readInt() != 0 ? (PaymentMethod) PaymentMethod.CREATOR.createFromParcel(parcel2) : null, parcel.readString(), parcel.createStringArrayList(), parcel.readInt() != 0 ? (StripeIntent.Status) Enum.valueOf(StripeIntent.Status.class, parcel.readString()) : null, parcel.readInt() != 0 ? (StripeIntent.Usage) Enum.valueOf(StripeIntent.Usage.class, parcel.readString()) : null, parcel.readInt() != 0 ? (Error) Error.CREATOR.createFromParcel(parcel2) : null, (StripeIntent.NextActionData) parcel2.readParcelable(SetupIntent.class.getClassLoader()));
        }

        public final Object[] newArray(int i) {
            return new SetupIntent[i];
        }
    }

    public static /* synthetic */ SetupIntent copy$default(SetupIntent setupIntent, String str, CancellationReason cancellationReason2, long j, String str2, String str3, boolean z, PaymentMethod paymentMethod2, String str4, List list, StripeIntent.Status status2, StripeIntent.Usage usage2, Error error, StripeIntent.NextActionData nextActionData2, int i, Object obj) {
        SetupIntent setupIntent2 = setupIntent;
        int i2 = i;
        return setupIntent.copy((i2 & 1) != 0 ? setupIntent.getId() : str, (i2 & 2) != 0 ? setupIntent2.cancellationReason : cancellationReason2, (i2 & 4) != 0 ? setupIntent.getCreated() : j, (i2 & 8) != 0 ? setupIntent.getClientSecret() : str2, (i2 & 16) != 0 ? setupIntent.getDescription() : str3, (i2 & 32) != 0 ? setupIntent.isLiveMode() : z, (i2 & 64) != 0 ? setupIntent.getPaymentMethod() : paymentMethod2, (i2 & 128) != 0 ? setupIntent.getPaymentMethodId() : str4, (i2 & 256) != 0 ? setupIntent.getPaymentMethodTypes() : list, (i2 & 512) != 0 ? setupIntent.getStatus() : status2, (i2 & 1024) != 0 ? setupIntent2.usage : usage2, (i2 & 2048) != 0 ? setupIntent2.lastSetupError : error, (i2 & 4096) != 0 ? setupIntent.getNextActionData() : nextActionData2);
    }

    @JvmStatic
    public static final SetupIntent fromJson(JSONObject jSONObject) {
        return Companion.fromJson(jSONObject);
    }

    @Deprecated(message = "use {@link #nextActionData}", replaceWith = @ReplaceWith(expression = "nextActionData as? StripeIntent.NextActionData.RedirectToUrl", imports = {}))
    public static /* synthetic */ void redirectData$annotations() {
    }

    @Deprecated(message = "use {@link #nextActionData}", replaceWith = @ReplaceWith(expression = "(nextActionData as? StripeIntent.NextActionData.RedirectToUrl)?.url", imports = {}))
    public static /* synthetic */ void redirectUrl$annotations() {
    }

    @Deprecated(message = "use {@link #nextActionData}", replaceWith = @ReplaceWith(expression = "nextActionData as? StripeIntent.NextActionData.SdkData", imports = {}))
    public static /* synthetic */ void stripeSdkData$annotations() {
    }

    public final String component1() {
        return getId();
    }

    public final StripeIntent.Status component10() {
        return getStatus();
    }

    public final StripeIntent.Usage component11() {
        return this.usage;
    }

    public final Error component12() {
        return this.lastSetupError;
    }

    public final StripeIntent.NextActionData component13() {
        return getNextActionData();
    }

    public final CancellationReason component2() {
        return this.cancellationReason;
    }

    public final long component3() {
        return getCreated();
    }

    public final String component4() {
        return getClientSecret();
    }

    public final String component5() {
        return getDescription();
    }

    public final boolean component6() {
        return isLiveMode();
    }

    public final PaymentMethod component7() {
        return getPaymentMethod();
    }

    public final String component8() {
        return getPaymentMethodId();
    }

    public final List<String> component9() {
        return getPaymentMethodTypes();
    }

    public final SetupIntent copy(String str, CancellationReason cancellationReason2, long j, String str2, String str3, boolean z, PaymentMethod paymentMethod2, String str4, List<String> list, StripeIntent.Status status2, StripeIntent.Usage usage2, Error error, StripeIntent.NextActionData nextActionData2) {
        List<String> list2 = list;
        Intrinsics.checkParameterIsNotNull(list2, "paymentMethodTypes");
        return new SetupIntent(str, cancellationReason2, j, str2, str3, z, paymentMethod2, str4, list2, status2, usage2, error, nextActionData2);
    }

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof SetupIntent)) {
            return false;
        }
        SetupIntent setupIntent = (SetupIntent) obj;
        return Intrinsics.areEqual((Object) getId(), (Object) setupIntent.getId()) && Intrinsics.areEqual((Object) this.cancellationReason, (Object) setupIntent.cancellationReason) && getCreated() == setupIntent.getCreated() && Intrinsics.areEqual((Object) getClientSecret(), (Object) setupIntent.getClientSecret()) && Intrinsics.areEqual((Object) getDescription(), (Object) setupIntent.getDescription()) && isLiveMode() == setupIntent.isLiveMode() && Intrinsics.areEqual((Object) getPaymentMethod(), (Object) setupIntent.getPaymentMethod()) && Intrinsics.areEqual((Object) getPaymentMethodId(), (Object) setupIntent.getPaymentMethodId()) && Intrinsics.areEqual((Object) getPaymentMethodTypes(), (Object) setupIntent.getPaymentMethodTypes()) && Intrinsics.areEqual((Object) getStatus(), (Object) setupIntent.getStatus()) && Intrinsics.areEqual((Object) this.usage, (Object) setupIntent.usage) && Intrinsics.areEqual((Object) this.lastSetupError, (Object) setupIntent.lastSetupError) && Intrinsics.areEqual((Object) getNextActionData(), (Object) setupIntent.getNextActionData());
    }

    public int hashCode() {
        String id2 = getId();
        int i = 0;
        int hashCode = (id2 != null ? id2.hashCode() : 0) * 31;
        CancellationReason cancellationReason2 = this.cancellationReason;
        int hashCode2 = cancellationReason2 != null ? cancellationReason2.hashCode() : 0;
        long created2 = getCreated();
        int i2 = (((hashCode + hashCode2) * 31) + ((int) (created2 ^ (created2 >>> 32)))) * 31;
        String clientSecret2 = getClientSecret();
        int hashCode3 = (i2 + (clientSecret2 != null ? clientSecret2.hashCode() : 0)) * 31;
        String description2 = getDescription();
        int hashCode4 = (hashCode3 + (description2 != null ? description2.hashCode() : 0)) * 31;
        boolean isLiveMode2 = isLiveMode();
        if (isLiveMode2) {
            isLiveMode2 = true;
        }
        int i3 = (hashCode4 + (isLiveMode2 ? 1 : 0)) * 31;
        PaymentMethod paymentMethod2 = getPaymentMethod();
        int hashCode5 = (i3 + (paymentMethod2 != null ? paymentMethod2.hashCode() : 0)) * 31;
        String paymentMethodId2 = getPaymentMethodId();
        int hashCode6 = (hashCode5 + (paymentMethodId2 != null ? paymentMethodId2.hashCode() : 0)) * 31;
        List<String> paymentMethodTypes2 = getPaymentMethodTypes();
        int hashCode7 = (hashCode6 + (paymentMethodTypes2 != null ? paymentMethodTypes2.hashCode() : 0)) * 31;
        StripeIntent.Status status2 = getStatus();
        int hashCode8 = (hashCode7 + (status2 != null ? status2.hashCode() : 0)) * 31;
        StripeIntent.Usage usage2 = this.usage;
        int hashCode9 = (hashCode8 + (usage2 != null ? usage2.hashCode() : 0)) * 31;
        Error error = this.lastSetupError;
        int hashCode10 = (hashCode9 + (error != null ? error.hashCode() : 0)) * 31;
        StripeIntent.NextActionData nextActionData2 = getNextActionData();
        if (nextActionData2 != null) {
            i = nextActionData2.hashCode();
        }
        return hashCode10 + i;
    }

    public String toString() {
        return "SetupIntent(id=" + getId() + ", cancellationReason=" + this.cancellationReason + ", created=" + getCreated() + ", clientSecret=" + getClientSecret() + ", description=" + getDescription() + ", isLiveMode=" + isLiveMode() + ", paymentMethod=" + getPaymentMethod() + ", paymentMethodId=" + getPaymentMethodId() + ", paymentMethodTypes=" + getPaymentMethodTypes() + ", status=" + getStatus() + ", usage=" + this.usage + ", lastSetupError=" + this.lastSetupError + ", nextActionData=" + getNextActionData() + ")";
    }

    public void writeToParcel(Parcel parcel, int i) {
        Intrinsics.checkParameterIsNotNull(parcel, "parcel");
        parcel.writeString(this.id);
        CancellationReason cancellationReason2 = this.cancellationReason;
        if (cancellationReason2 != null) {
            parcel.writeInt(1);
            parcel.writeString(cancellationReason2.name());
        } else {
            parcel.writeInt(0);
        }
        parcel.writeLong(this.created);
        parcel.writeString(this.clientSecret);
        parcel.writeString(this.description);
        parcel.writeInt(this.isLiveMode ? 1 : 0);
        PaymentMethod paymentMethod2 = this.paymentMethod;
        if (paymentMethod2 != null) {
            parcel.writeInt(1);
            paymentMethod2.writeToParcel(parcel, 0);
        } else {
            parcel.writeInt(0);
        }
        parcel.writeString(this.paymentMethodId);
        parcel.writeStringList(this.paymentMethodTypes);
        StripeIntent.Status status2 = this.status;
        if (status2 != null) {
            parcel.writeInt(1);
            parcel.writeString(status2.name());
        } else {
            parcel.writeInt(0);
        }
        StripeIntent.Usage usage2 = this.usage;
        if (usage2 != null) {
            parcel.writeInt(1);
            parcel.writeString(usage2.name());
        } else {
            parcel.writeInt(0);
        }
        Error error = this.lastSetupError;
        if (error != null) {
            parcel.writeInt(1);
            error.writeToParcel(parcel, 0);
        } else {
            parcel.writeInt(0);
        }
        parcel.writeParcelable(this.nextActionData, i);
    }

    public SetupIntent(String str, CancellationReason cancellationReason2, long j, String str2, String str3, boolean z, PaymentMethod paymentMethod2, String str4, List<String> list, StripeIntent.Status status2, StripeIntent.Usage usage2, Error error, StripeIntent.NextActionData nextActionData2) {
        Intrinsics.checkParameterIsNotNull(list, "paymentMethodTypes");
        this.id = str;
        this.cancellationReason = cancellationReason2;
        this.created = j;
        this.clientSecret = str2;
        this.description = str3;
        this.isLiveMode = z;
        this.paymentMethod = paymentMethod2;
        this.paymentMethodId = str4;
        this.paymentMethodTypes = list;
        this.status = status2;
        this.usage = usage2;
        this.lastSetupError = error;
        this.nextActionData = nextActionData2;
    }

    public String getId() {
        return this.id;
    }

    public final CancellationReason getCancellationReason() {
        return this.cancellationReason;
    }

    public long getCreated() {
        return this.created;
    }

    public String getClientSecret() {
        return this.clientSecret;
    }

    public String getDescription() {
        return this.description;
    }

    public boolean isLiveMode() {
        return this.isLiveMode;
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* synthetic */ SetupIntent(java.lang.String r19, com.stripe.android.model.SetupIntent.CancellationReason r20, long r21, java.lang.String r23, java.lang.String r24, boolean r25, com.stripe.android.model.PaymentMethod r26, java.lang.String r27, java.util.List r28, com.stripe.android.model.StripeIntent.Status r29, com.stripe.android.model.StripeIntent.Usage r30, com.stripe.android.model.SetupIntent.Error r31, com.stripe.android.model.StripeIntent.NextActionData r32, int r33, kotlin.jvm.internal.DefaultConstructorMarker r34) {
        /*
            r18 = this;
            r0 = r33
            r1 = r0 & 64
            r2 = 0
            if (r1 == 0) goto L_0x000c
            r1 = r2
            com.stripe.android.model.PaymentMethod r1 = (com.stripe.android.model.PaymentMethod) r1
            r11 = r1
            goto L_0x000e
        L_0x000c:
            r11 = r26
        L_0x000e:
            r0 = r0 & 2048(0x800, float:2.87E-42)
            if (r0 == 0) goto L_0x0018
            r0 = r2
            com.stripe.android.model.SetupIntent$Error r0 = (com.stripe.android.model.SetupIntent.Error) r0
            r16 = r0
            goto L_0x001a
        L_0x0018:
            r16 = r31
        L_0x001a:
            r3 = r18
            r4 = r19
            r5 = r20
            r6 = r21
            r8 = r23
            r9 = r24
            r10 = r25
            r12 = r27
            r13 = r28
            r14 = r29
            r15 = r30
            r17 = r32
            r3.<init>(r4, r5, r6, r8, r9, r10, r11, r12, r13, r14, r15, r16, r17)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.stripe.android.model.SetupIntent.<init>(java.lang.String, com.stripe.android.model.SetupIntent$CancellationReason, long, java.lang.String, java.lang.String, boolean, com.stripe.android.model.PaymentMethod, java.lang.String, java.util.List, com.stripe.android.model.StripeIntent$Status, com.stripe.android.model.StripeIntent$Usage, com.stripe.android.model.SetupIntent$Error, com.stripe.android.model.StripeIntent$NextActionData, int, kotlin.jvm.internal.DefaultConstructorMarker):void");
    }

    public PaymentMethod getPaymentMethod() {
        return this.paymentMethod;
    }

    public String getPaymentMethodId() {
        return this.paymentMethodId;
    }

    public List<String> getPaymentMethodTypes() {
        return this.paymentMethodTypes;
    }

    public StripeIntent.Status getStatus() {
        return this.status;
    }

    public final StripeIntent.Usage getUsage() {
        return this.usage;
    }

    public final Error getLastSetupError() {
        return this.lastSetupError;
    }

    public StripeIntent.NextActionData getNextActionData() {
        return this.nextActionData;
    }

    public StripeIntent.NextActionType getNextActionType() {
        StripeIntent.NextActionData nextActionData2 = getNextActionData();
        if (nextActionData2 instanceof StripeIntent.NextActionData.SdkData) {
            return StripeIntent.NextActionType.UseStripeSdk;
        }
        if (nextActionData2 instanceof StripeIntent.NextActionData.RedirectToUrl) {
            return StripeIntent.NextActionType.RedirectToUrl;
        }
        if (nextActionData2 instanceof StripeIntent.NextActionData.DisplayOxxoDetails) {
            return StripeIntent.NextActionType.DisplayOxxoDetails;
        }
        return null;
    }

    public StripeIntent.RedirectData getRedirectData() {
        if (getNextActionData() instanceof StripeIntent.NextActionData.RedirectToUrl) {
            return new StripeIntent.RedirectData(((StripeIntent.NextActionData.RedirectToUrl) getNextActionData()).getUrl(), ((StripeIntent.NextActionData.RedirectToUrl) getNextActionData()).getReturnUrl());
        }
        return null;
    }

    public final Uri getRedirectUrl() {
        StripeIntent.RedirectData redirectData = getRedirectData();
        if (redirectData != null) {
            return redirectData.getUrl();
        }
        return null;
    }

    public StripeIntent.SdkData getStripeSdkData() {
        StripeIntent.NextActionData nextActionData2 = getNextActionData();
        if (nextActionData2 instanceof StripeIntent.NextActionData.SdkData.Use3DS1) {
            return new StripeIntent.SdkData(true, false);
        }
        if (nextActionData2 instanceof StripeIntent.NextActionData.SdkData.Use3DS2) {
            return new StripeIntent.SdkData(false, true);
        }
        return null;
    }

    public boolean requiresAction() {
        return getStatus() == StripeIntent.Status.RequiresAction;
    }

    public boolean requiresConfirmation() {
        return getStatus() == StripeIntent.Status.RequiresConfirmation;
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0014\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\b\b\u0018\u00002\u00020\u0001:\u0001,BM\b\u0000\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\b\u0010\u0004\u001a\u0004\u0018\u00010\u0003\u0012\b\u0010\u0005\u001a\u0004\u0018\u00010\u0003\u0012\b\u0010\u0006\u001a\u0004\u0018\u00010\u0003\u0012\b\u0010\u0007\u001a\u0004\u0018\u00010\u0003\u0012\b\u0010\b\u001a\u0004\u0018\u00010\t\u0012\b\u0010\n\u001a\u0004\u0018\u00010\u000b¢\u0006\u0002\u0010\fJ\u000b\u0010\u0017\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000b\u0010\u0018\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000b\u0010\u0019\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000b\u0010\u001a\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000b\u0010\u001b\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000b\u0010\u001c\u001a\u0004\u0018\u00010\tHÆ\u0003J\u000b\u0010\u001d\u001a\u0004\u0018\u00010\u000bHÆ\u0003J]\u0010\u001e\u001a\u00020\u00002\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u0007\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\t2\n\b\u0002\u0010\n\u001a\u0004\u0018\u00010\u000bHÆ\u0001J\t\u0010\u001f\u001a\u00020 HÖ\u0001J\u0013\u0010!\u001a\u00020\"2\b\u0010#\u001a\u0004\u0018\u00010$HÖ\u0003J\t\u0010%\u001a\u00020 HÖ\u0001J\t\u0010&\u001a\u00020\u0003HÖ\u0001J\u0019\u0010'\u001a\u00020(2\u0006\u0010)\u001a\u00020*2\u0006\u0010+\u001a\u00020 HÖ\u0001R\u0013\u0010\u0002\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000eR\u0013\u0010\u0004\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u000eR\u0013\u0010\u0005\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u000eR\u0013\u0010\u0006\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u000eR\u0013\u0010\u0007\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u000eR\u0013\u0010\b\u001a\u0004\u0018\u00010\t¢\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u0014R\u0013\u0010\n\u001a\u0004\u0018\u00010\u000b¢\u0006\b\n\u0000\u001a\u0004\b\u0015\u0010\u0016¨\u0006-"}, d2 = {"Lcom/stripe/android/model/SetupIntent$Error;", "Lcom/stripe/android/model/StripeModel;", "code", "", "declineCode", "docUrl", "message", "param", "paymentMethod", "Lcom/stripe/android/model/PaymentMethod;", "type", "Lcom/stripe/android/model/SetupIntent$Error$Type;", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/stripe/android/model/PaymentMethod;Lcom/stripe/android/model/SetupIntent$Error$Type;)V", "getCode", "()Ljava/lang/String;", "getDeclineCode", "getDocUrl", "getMessage", "getParam", "getPaymentMethod", "()Lcom/stripe/android/model/PaymentMethod;", "getType", "()Lcom/stripe/android/model/SetupIntent$Error$Type;", "component1", "component2", "component3", "component4", "component5", "component6", "component7", "copy", "describeContents", "", "equals", "", "other", "", "hashCode", "toString", "writeToParcel", "", "parcel", "Landroid/os/Parcel;", "flags", "Type", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: SetupIntent.kt */
    public static final class Error implements StripeModel {
        public static final Parcelable.Creator CREATOR = new Creator();
        private final String code;
        private final String declineCode;
        private final String docUrl;
        private final String message;
        private final String param;
        private final PaymentMethod paymentMethod;
        private final Type type;

        @Metadata(bv = {1, 0, 3}, k = 3, mv = {1, 1, 16})
        public static class Creator implements Parcelable.Creator {
            public final Object createFromParcel(Parcel parcel) {
                Intrinsics.checkParameterIsNotNull(parcel, "in");
                return new Error(parcel.readString(), parcel.readString(), parcel.readString(), parcel.readString(), parcel.readString(), parcel.readInt() != 0 ? (PaymentMethod) PaymentMethod.CREATOR.createFromParcel(parcel) : null, parcel.readInt() != 0 ? (Type) Enum.valueOf(Type.class, parcel.readString()) : null);
            }

            public final Object[] newArray(int i) {
                return new Error[i];
            }
        }

        public static /* synthetic */ Error copy$default(Error error, String str, String str2, String str3, String str4, String str5, PaymentMethod paymentMethod2, Type type2, int i, Object obj) {
            if ((i & 1) != 0) {
                str = error.code;
            }
            if ((i & 2) != 0) {
                str2 = error.declineCode;
            }
            String str6 = str2;
            if ((i & 4) != 0) {
                str3 = error.docUrl;
            }
            String str7 = str3;
            if ((i & 8) != 0) {
                str4 = error.message;
            }
            String str8 = str4;
            if ((i & 16) != 0) {
                str5 = error.param;
            }
            String str9 = str5;
            if ((i & 32) != 0) {
                paymentMethod2 = error.paymentMethod;
            }
            PaymentMethod paymentMethod3 = paymentMethod2;
            if ((i & 64) != 0) {
                type2 = error.type;
            }
            return error.copy(str, str6, str7, str8, str9, paymentMethod3, type2);
        }

        public final String component1() {
            return this.code;
        }

        public final String component2() {
            return this.declineCode;
        }

        public final String component3() {
            return this.docUrl;
        }

        public final String component4() {
            return this.message;
        }

        public final String component5() {
            return this.param;
        }

        public final PaymentMethod component6() {
            return this.paymentMethod;
        }

        public final Type component7() {
            return this.type;
        }

        public final Error copy(String str, String str2, String str3, String str4, String str5, PaymentMethod paymentMethod2, Type type2) {
            return new Error(str, str2, str3, str4, str5, paymentMethod2, type2);
        }

        public int describeContents() {
            return 0;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Error)) {
                return false;
            }
            Error error = (Error) obj;
            return Intrinsics.areEqual((Object) this.code, (Object) error.code) && Intrinsics.areEqual((Object) this.declineCode, (Object) error.declineCode) && Intrinsics.areEqual((Object) this.docUrl, (Object) error.docUrl) && Intrinsics.areEqual((Object) this.message, (Object) error.message) && Intrinsics.areEqual((Object) this.param, (Object) error.param) && Intrinsics.areEqual((Object) this.paymentMethod, (Object) error.paymentMethod) && Intrinsics.areEqual((Object) this.type, (Object) error.type);
        }

        public int hashCode() {
            String str = this.code;
            int i = 0;
            int hashCode = (str != null ? str.hashCode() : 0) * 31;
            String str2 = this.declineCode;
            int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
            String str3 = this.docUrl;
            int hashCode3 = (hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31;
            String str4 = this.message;
            int hashCode4 = (hashCode3 + (str4 != null ? str4.hashCode() : 0)) * 31;
            String str5 = this.param;
            int hashCode5 = (hashCode4 + (str5 != null ? str5.hashCode() : 0)) * 31;
            PaymentMethod paymentMethod2 = this.paymentMethod;
            int hashCode6 = (hashCode5 + (paymentMethod2 != null ? paymentMethod2.hashCode() : 0)) * 31;
            Type type2 = this.type;
            if (type2 != null) {
                i = type2.hashCode();
            }
            return hashCode6 + i;
        }

        public String toString() {
            return "Error(code=" + this.code + ", declineCode=" + this.declineCode + ", docUrl=" + this.docUrl + ", message=" + this.message + ", param=" + this.param + ", paymentMethod=" + this.paymentMethod + ", type=" + this.type + ")";
        }

        public void writeToParcel(Parcel parcel, int i) {
            Intrinsics.checkParameterIsNotNull(parcel, "parcel");
            parcel.writeString(this.code);
            parcel.writeString(this.declineCode);
            parcel.writeString(this.docUrl);
            parcel.writeString(this.message);
            parcel.writeString(this.param);
            PaymentMethod paymentMethod2 = this.paymentMethod;
            if (paymentMethod2 != null) {
                parcel.writeInt(1);
                paymentMethod2.writeToParcel(parcel, 0);
            } else {
                parcel.writeInt(0);
            }
            Type type2 = this.type;
            if (type2 != null) {
                parcel.writeInt(1);
                parcel.writeString(type2.name());
                return;
            }
            parcel.writeInt(0);
        }

        public Error(String str, String str2, String str3, String str4, String str5, PaymentMethod paymentMethod2, Type type2) {
            this.code = str;
            this.declineCode = str2;
            this.docUrl = str3;
            this.message = str4;
            this.param = str5;
            this.paymentMethod = paymentMethod2;
            this.type = type2;
        }

        public final String getCode() {
            return this.code;
        }

        public final String getDeclineCode() {
            return this.declineCode;
        }

        public final String getDocUrl() {
            return this.docUrl;
        }

        public final String getMessage() {
            return this.message;
        }

        public final String getParam() {
            return this.param;
        }

        public final PaymentMethod getPaymentMethod() {
            return this.paymentMethod;
        }

        public final Type getType() {
            return this.type;
        }

        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\u000e\n\u0002\b\f\b\u0001\u0018\u0000 \u000e2\b\u0012\u0004\u0012\u00020\u00000\u0001:\u0001\u000eB\u000f\b\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006j\u0002\b\u0007j\u0002\b\bj\u0002\b\tj\u0002\b\nj\u0002\b\u000bj\u0002\b\fj\u0002\b\r¨\u0006\u000f"}, d2 = {"Lcom/stripe/android/model/SetupIntent$Error$Type;", "", "code", "", "(Ljava/lang/String;ILjava/lang/String;)V", "getCode", "()Ljava/lang/String;", "ApiConnectionError", "ApiError", "AuthenticationError", "CardError", "IdempotencyError", "InvalidRequestError", "RateLimitError", "Companion", "stripe_release"}, k = 1, mv = {1, 1, 16})
        /* compiled from: SetupIntent.kt */
        public enum Type {
            ApiConnectionError("api_connection_error"),
            ApiError("api_error"),
            AuthenticationError("authentication_error"),
            CardError("card_error"),
            IdempotencyError("idempotency_error"),
            InvalidRequestError("invalid_request_error"),
            RateLimitError("rate_limit_error");
            
            public static final Companion Companion = null;
            private final String code;

            private Type(String str) {
                this.code = str;
            }

            public final String getCode() {
                return this.code;
            }

            static {
                Companion = new Companion((DefaultConstructorMarker) null);
            }

            @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0019\u0010\u0003\u001a\u0004\u0018\u00010\u00042\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006H\u0000¢\u0006\u0002\b\u0007¨\u0006\b"}, d2 = {"Lcom/stripe/android/model/SetupIntent$Error$Type$Companion;", "", "()V", "fromCode", "Lcom/stripe/android/model/SetupIntent$Error$Type;", "typeCode", "", "fromCode$stripe_release", "stripe_release"}, k = 1, mv = {1, 1, 16})
            /* compiled from: SetupIntent.kt */
            public static final class Companion {
                private Companion() {
                }

                public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                    this();
                }

                public final Type fromCode$stripe_release(String str) {
                    for (Type type : Type.values()) {
                        if (Intrinsics.areEqual((Object) type.getCode(), (Object) str)) {
                            return type;
                        }
                    }
                    return null;
                }
            }
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b\t\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\b\b\u0018\u0000 \u00122\u00020\u0001:\u0001\u0012B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u000e\u0010\t\u001a\u00020\u0003HÀ\u0003¢\u0006\u0002\b\nJ\u0013\u0010\u000b\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u0003HÆ\u0001J\u0013\u0010\f\u001a\u00020\r2\b\u0010\u000e\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u000f\u001a\u00020\u0010HÖ\u0001J\t\u0010\u0011\u001a\u00020\u0003HÖ\u0001R\u0014\u0010\u0005\u001a\u00020\u0003X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\u0007R\u0014\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\u0007¨\u0006\u0013"}, d2 = {"Lcom/stripe/android/model/SetupIntent$ClientSecret;", "", "value", "", "(Ljava/lang/String;)V", "setupIntentId", "getSetupIntentId$stripe_release", "()Ljava/lang/String;", "getValue$stripe_release", "component1", "component1$stripe_release", "copy", "equals", "", "other", "hashCode", "", "toString", "Companion", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: SetupIntent.kt */
    public static final class ClientSecret {
        @Deprecated
        public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
        private static final Pattern PATTERN = Pattern.compile("^seti_[^_]+_secret_[^_]+$");
        private final String setupIntentId;
        private final String value;

        public static /* synthetic */ ClientSecret copy$default(ClientSecret clientSecret, String str, int i, Object obj) {
            if ((i & 1) != 0) {
                str = clientSecret.value;
            }
            return clientSecret.copy(str);
        }

        public final String component1$stripe_release() {
            return this.value;
        }

        public final ClientSecret copy(String str) {
            Intrinsics.checkParameterIsNotNull(str, "value");
            return new ClientSecret(str);
        }

        public boolean equals(Object obj) {
            if (this != obj) {
                return (obj instanceof ClientSecret) && Intrinsics.areEqual((Object) this.value, (Object) ((ClientSecret) obj).value);
            }
            return true;
        }

        public int hashCode() {
            String str = this.value;
            if (str != null) {
                return str.hashCode();
            }
            return 0;
        }

        public String toString() {
            return "ClientSecret(value=" + this.value + ")";
        }

        /* JADX WARNING: Removed duplicated region for block: B:14:0x005b  */
        /* JADX WARNING: Removed duplicated region for block: B:19:0x0091  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public ClientSecret(java.lang.String r5) {
            /*
                r4 = this;
                java.lang.String r0 = "value"
                kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(r5, r0)
                r4.<init>()
                r4.value = r5
                java.lang.CharSequence r5 = (java.lang.CharSequence) r5
                kotlin.text.Regex r0 = new kotlin.text.Regex
                java.lang.String r1 = "_secret"
                r0.<init>((java.lang.String) r1)
                r1 = 0
                java.util.List r5 = r0.split(r5, r1)
                boolean r0 = r5.isEmpty()
                if (r0 != 0) goto L_0x004d
                int r0 = r5.size()
                java.util.ListIterator r0 = r5.listIterator(r0)
            L_0x0027:
                boolean r2 = r0.hasPrevious()
                if (r2 == 0) goto L_0x004d
                java.lang.Object r2 = r0.previous()
                java.lang.String r2 = (java.lang.String) r2
                java.lang.CharSequence r2 = (java.lang.CharSequence) r2
                int r2 = r2.length()
                r3 = 1
                if (r2 != 0) goto L_0x003e
                r2 = 1
                goto L_0x003f
            L_0x003e:
                r2 = 0
            L_0x003f:
                if (r2 != 0) goto L_0x0027
                java.lang.Iterable r5 = (java.lang.Iterable) r5
                int r0 = r0.nextIndex()
                int r0 = r0 + r3
                java.util.List r5 = kotlin.collections.CollectionsKt.take(r5, r0)
                goto L_0x0051
            L_0x004d:
                java.util.List r5 = kotlin.collections.CollectionsKt.emptyList()
            L_0x0051:
                java.util.Collection r5 = (java.util.Collection) r5
                java.lang.String[] r0 = new java.lang.String[r1]
                java.lang.Object[] r5 = r5.toArray(r0)
                if (r5 == 0) goto L_0x0091
                java.lang.String[] r5 = (java.lang.String[]) r5
                r5 = r5[r1]
                r4.setupIntentId = r5
                java.util.regex.Pattern r5 = PATTERN
                java.lang.String r0 = r4.value
                java.lang.CharSequence r0 = (java.lang.CharSequence) r0
                java.util.regex.Matcher r5 = r5.matcher(r0)
                boolean r5 = r5.matches()
                if (r5 == 0) goto L_0x0072
                return
            L_0x0072:
                java.lang.StringBuilder r5 = new java.lang.StringBuilder
                r5.<init>()
                java.lang.String r0 = "Invalid client secret: "
                r5.append(r0)
                java.lang.String r0 = r4.value
                r5.append(r0)
                java.lang.String r5 = r5.toString()
                java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
                java.lang.String r5 = r5.toString()
                r0.<init>(r5)
                java.lang.Throwable r0 = (java.lang.Throwable) r0
                throw r0
            L_0x0091:
                kotlin.TypeCastException r5 = new kotlin.TypeCastException
                java.lang.String r0 = "null cannot be cast to non-null type kotlin.Array<T>"
                r5.<init>(r0)
                throw r5
            */
            throw new UnsupportedOperationException("Method not decompiled: com.stripe.android.model.SetupIntent.ClientSecret.<init>(java.lang.String):void");
        }

        public final String getValue$stripe_release() {
            return this.value;
        }

        public final String getSetupIntentId$stripe_release() {
            return this.setupIntentId;
        }

        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0016\u0010\u0003\u001a\n \u0005*\u0004\u0018\u00010\u00040\u0004X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0006"}, d2 = {"Lcom/stripe/android/model/SetupIntent$ClientSecret$Companion;", "", "()V", "PATTERN", "Ljava/util/regex/Pattern;", "kotlin.jvm.PlatformType", "stripe_release"}, k = 1, mv = {1, 1, 16})
        /* compiled from: SetupIntent.kt */
        private static final class Companion {
            private Companion() {
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0006\b\u0001\u0018\u0000 \b2\b\u0012\u0004\u0012\u00020\u00000\u0001:\u0001\bB\u000f\b\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000j\u0002\b\u0005j\u0002\b\u0006j\u0002\b\u0007¨\u0006\t"}, d2 = {"Lcom/stripe/android/model/SetupIntent$CancellationReason;", "", "code", "", "(Ljava/lang/String;ILjava/lang/String;)V", "Duplicate", "RequestedByCustomer", "Abandoned", "Companion", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: SetupIntent.kt */
    public enum CancellationReason {
        Duplicate("duplicate"),
        RequestedByCustomer("requested_by_customer"),
        Abandoned("abandoned");
        
        public static final Companion Companion = null;
        /* access modifiers changed from: private */
        public final String code;

        private CancellationReason(String str) {
            this.code = str;
        }

        static {
            Companion = new Companion((DefaultConstructorMarker) null);
        }

        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0019\u0010\u0003\u001a\u0004\u0018\u00010\u00042\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006H\u0000¢\u0006\u0002\b\u0007¨\u0006\b"}, d2 = {"Lcom/stripe/android/model/SetupIntent$CancellationReason$Companion;", "", "()V", "fromCode", "Lcom/stripe/android/model/SetupIntent$CancellationReason;", "code", "", "fromCode$stripe_release", "stripe_release"}, k = 1, mv = {1, 1, 16})
        /* compiled from: SetupIntent.kt */
        public static final class Companion {
            private Companion() {
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }

            public final CancellationReason fromCode$stripe_release(String str) {
                for (CancellationReason cancellationReason : CancellationReason.values()) {
                    if (Intrinsics.areEqual((Object) cancellationReason.code, (Object) str)) {
                        return cancellationReason;
                    }
                }
                return null;
            }
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0014\u0010\u0003\u001a\u0004\u0018\u00010\u00042\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006H\u0007¨\u0006\u0007"}, d2 = {"Lcom/stripe/android/model/SetupIntent$Companion;", "", "()V", "fromJson", "Lcom/stripe/android/model/SetupIntent;", "jsonObject", "Lorg/json/JSONObject;", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: SetupIntent.kt */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        @JvmStatic
        public final SetupIntent fromJson(JSONObject jSONObject) {
            if (jSONObject != null) {
                return new SetupIntentJsonParser().parse(jSONObject);
            }
            return null;
        }
    }
}
