package com.stripe.android.model;

import android.os.Parcel;
import android.os.Parcelable;
import com.stripe.android.CardUtils;
import com.stripe.android.ObjectBuilder;
import com.stripe.android.model.PaymentMethod;
import com.stripe.android.model.PaymentMethodCreateParams;
import com.stripe.android.model.parsers.CardJsonParser;
import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import kotlin.Metadata;
import kotlin.Result;
import kotlin.ResultKt;
import kotlin.TypeCastException;
import kotlin.collections.SetsKt;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.internal.Intrinsics;
import kotlin.text.Regex;
import kotlin.text.StringsKt;
import org.json.JSONObject;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000~\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\f\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\"\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\b@\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\b\b\u0018\u0000 \u00012\u00020\u00012\u00020\u00022\u00020\u0003:\u0004\u0001\u0001B\u0002\b\u0000\u0012\b\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\b\u0010\u0006\u001a\u0004\u0018\u00010\u0005\u0012\b\u0010\u0007\u001a\u0004\u0018\u00010\b\u0012\b\u0010\t\u001a\u0004\u0018\u00010\b\u0012\b\u0010\n\u001a\u0004\u0018\u00010\u0005\u0012\b\u0010\u000b\u001a\u0004\u0018\u00010\u0005\u0012\b\u0010\f\u001a\u0004\u0018\u00010\u0005\u0012\b\u0010\r\u001a\u0004\u0018\u00010\u0005\u0012\b\u0010\u000e\u001a\u0004\u0018\u00010\u0005\u0012\b\u0010\u000f\u001a\u0004\u0018\u00010\u0005\u0012\b\u0010\u0010\u001a\u0004\u0018\u00010\u0005\u0012\b\u0010\u0011\u001a\u0004\u0018\u00010\u0005\u0012\b\u0010\u0012\u001a\u0004\u0018\u00010\u0005\u0012\n\b\u0001\u0010\u0013\u001a\u0004\u0018\u00010\u0005\u0012\u0006\u0010\u0014\u001a\u00020\u0015\u0012\b\u0010\u0016\u001a\u0004\u0018\u00010\u0017\u0012\b\u0010\u0018\u001a\u0004\u0018\u00010\u0005\u0012\b\u0010\u0019\u001a\u0004\u0018\u00010\u0005\u0012\b\u0010\u001a\u001a\u0004\u0018\u00010\u0005\u0012\b\u0010\u001b\u001a\u0004\u0018\u00010\u0005\u0012\b\u0010\u001c\u001a\u0004\u0018\u00010\u0005\u0012\b\u0010\u001d\u001a\u0004\u0018\u00010\u0005\u0012\u000e\b\u0002\u0010\u001e\u001a\b\u0012\u0004\u0012\u00020\u00050\u001f\u0012\n\b\u0002\u0010 \u001a\u0004\u0018\u00010!\u0012\u0014\u0010\"\u001a\u0010\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u0005\u0018\u00010#¢\u0006\u0002\u0010$J\u000b\u0010F\u001a\u0004\u0018\u00010\u0005HÆ\u0003J\u000b\u0010G\u001a\u0004\u0018\u00010\u0005HÆ\u0003J\u000b\u0010H\u001a\u0004\u0018\u00010\u0005HÆ\u0003J\u000b\u0010I\u001a\u0004\u0018\u00010\u0005HÆ\u0003J\u000b\u0010J\u001a\u0004\u0018\u00010\u0005HÆ\u0003J\u000b\u0010K\u001a\u0004\u0018\u00010\u0005HÆ\u0003J\t\u0010L\u001a\u00020\u0015HÆ\u0003J\u000b\u0010M\u001a\u0004\u0018\u00010\u0017HÆ\u0003J\u000b\u0010N\u001a\u0004\u0018\u00010\u0005HÆ\u0003J\u000b\u0010O\u001a\u0004\u0018\u00010\u0005HÆ\u0003J\u000b\u0010P\u001a\u0004\u0018\u00010\u0005HÆ\u0003J\u000b\u0010Q\u001a\u0004\u0018\u00010\u0005HÆ\u0003J\u000b\u0010R\u001a\u0004\u0018\u00010\u0005HÆ\u0003J\u000b\u0010S\u001a\u0004\u0018\u00010\u0005HÆ\u0003J\u000b\u0010T\u001a\u0004\u0018\u00010\u0005HÆ\u0003J\u0014\u0010U\u001a\b\u0012\u0004\u0012\u00020\u00050\u001fHÀ\u0003¢\u0006\u0002\bVJ\u000b\u0010W\u001a\u0004\u0018\u00010!HÆ\u0003J\u0017\u0010X\u001a\u0010\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u0005\u0018\u00010#HÆ\u0003J\u0010\u0010Y\u001a\u0004\u0018\u00010\bHÆ\u0003¢\u0006\u0002\u00106J\u0010\u0010Z\u001a\u0004\u0018\u00010\bHÆ\u0003¢\u0006\u0002\u00106J\u000b\u0010[\u001a\u0004\u0018\u00010\u0005HÆ\u0003J\u000b\u0010\\\u001a\u0004\u0018\u00010\u0005HÆ\u0003J\u000b\u0010]\u001a\u0004\u0018\u00010\u0005HÆ\u0003J\u000b\u0010^\u001a\u0004\u0018\u00010\u0005HÆ\u0003J\u000b\u0010_\u001a\u0004\u0018\u00010\u0005HÆ\u0003JÈ\u0002\u0010`\u001a\u00020\u00002\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\u0007\u001a\u0004\u0018\u00010\b2\n\b\u0002\u0010\t\u001a\u0004\u0018\u00010\b2\n\b\u0002\u0010\n\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\u000b\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\f\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\r\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\u000e\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\u000f\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\u0010\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\u0011\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\u0012\u001a\u0004\u0018\u00010\u00052\n\b\u0003\u0010\u0013\u001a\u0004\u0018\u00010\u00052\b\b\u0002\u0010\u0014\u001a\u00020\u00152\n\b\u0002\u0010\u0016\u001a\u0004\u0018\u00010\u00172\n\b\u0002\u0010\u0018\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\u0019\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\u001a\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\u001b\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\u001c\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\u001d\u001a\u0004\u0018\u00010\u00052\u000e\b\u0002\u0010\u001e\u001a\b\u0012\u0004\u0012\u00020\u00050\u001f2\n\b\u0002\u0010 \u001a\u0004\u0018\u00010!2\u0016\b\u0002\u0010\"\u001a\u0010\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u0005\u0018\u00010#HÆ\u0001¢\u0006\u0002\u0010aJ\t\u0010b\u001a\u00020\bHÖ\u0001J\u0013\u0010c\u001a\u00020d2\b\u0010e\u001a\u0004\u0018\u00010fHÖ\u0003J\t\u0010g\u001a\u00020\bHÖ\u0001J\u0006\u0010h\u001a\u00020iJ\u0014\u0010j\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020f0#H\u0016J\u0006\u0010k\u001a\u00020lJ\u0006\u0010m\u001a\u00020nJ\t\u0010o\u001a\u00020\u0005HÖ\u0001J\u0006\u0010p\u001a\u00020dJ\u0006\u0010q\u001a\u00020dJ\u0015\u0010q\u001a\u00020d2\u0006\u0010r\u001a\u00020sH\u0000¢\u0006\u0002\btJ\u0006\u0010u\u001a\u00020dJ\u0015\u0010v\u001a\u00020d2\u0006\u0010r\u001a\u00020sH\u0000¢\u0006\u0002\bwJ\u0006\u0010x\u001a\u00020dJ\u0015\u0010x\u001a\u00020d2\u0006\u0010r\u001a\u00020sH\u0000¢\u0006\u0002\byJ\u0006\u0010z\u001a\u00020dJ\u0019\u0010{\u001a\u00020|2\u0006\u0010}\u001a\u00020~2\u0006\u0010\u001a\u00020\bHÖ\u0001R\u0013\u0010\u000e\u001a\u0004\u0018\u00010\u0005¢\u0006\b\n\u0000\u001a\u0004\b%\u0010&R\u0013\u0010\u0012\u001a\u0004\u0018\u00010\u0005¢\u0006\b\n\u0000\u001a\u0004\b'\u0010&R\u0013\u0010\u000b\u001a\u0004\u0018\u00010\u0005¢\u0006\b\n\u0000\u001a\u0004\b(\u0010&R\u0013\u0010\f\u001a\u0004\u0018\u00010\u0005¢\u0006\b\n\u0000\u001a\u0004\b)\u0010&R\u0013\u0010\r\u001a\u0004\u0018\u00010\u0005¢\u0006\b\n\u0000\u001a\u0004\b*\u0010&R\u0013\u0010\u000f\u001a\u0004\u0018\u00010\u0005¢\u0006\b\n\u0000\u001a\u0004\b+\u0010&R\u0013\u0010\u0010\u001a\u0004\u0018\u00010\u0005¢\u0006\b\n\u0000\u001a\u0004\b,\u0010&R\u0013\u0010\u0011\u001a\u0004\u0018\u00010\u0005¢\u0006\b\n\u0000\u001a\u0004\b-\u0010&R\u0011\u0010\u0014\u001a\u00020\u0015¢\u0006\b\n\u0000\u001a\u0004\b.\u0010/R\u0013\u0010\u0019\u001a\u0004\u0018\u00010\u0005¢\u0006\b\n\u0000\u001a\u0004\b0\u0010&R\u0013\u0010\u001a\u001a\u0004\u0018\u00010\u0005¢\u0006\b\n\u0000\u001a\u0004\b1\u0010&R\u0013\u0010\u001b\u001a\u0004\u0018\u00010\u0005¢\u0006\b\n\u0000\u001a\u0004\b2\u0010&R\u0013\u0010\u0006\u001a\u0004\u0018\u00010\u0005¢\u0006\b\n\u0000\u001a\u0004\b3\u0010&R\u0013\u0010\u001c\u001a\u0004\u0018\u00010\u0005¢\u0006\b\n\u0000\u001a\u0004\b4\u0010&R\u0017\u0010\u0007\u001a\u0004\u0018\u00010\b8\u0007¢\u0006\n\n\u0002\u00107\u001a\u0004\b5\u00106R\u0015\u0010\t\u001a\u0004\u0018\u00010\b¢\u0006\n\n\u0002\u00107\u001a\u0004\b8\u00106R\u0013\u0010\u0018\u001a\u0004\u0018\u00010\u0005¢\u0006\b\n\u0000\u001a\u0004\b9\u0010&R\u0013\u0010\u0016\u001a\u0004\u0018\u00010\u0017¢\u0006\b\n\u0000\u001a\u0004\b:\u0010;R\u0016\u0010\u001d\u001a\u0004\u0018\u00010\u0005X\u0004¢\u0006\b\n\u0000\u001a\u0004\b<\u0010&R\u0013\u0010\u0013\u001a\u0004\u0018\u00010\u0005¢\u0006\b\n\u0000\u001a\u0004\b=\u0010&R\u001a\u0010\u001e\u001a\b\u0012\u0004\u0012\u00020\u00050\u001fX\u0004¢\u0006\b\n\u0000\u001a\u0004\b>\u0010?R\u001f\u0010\"\u001a\u0010\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u0005\u0018\u00010#¢\u0006\b\n\u0000\u001a\u0004\b@\u0010AR\u0013\u0010\n\u001a\u0004\u0018\u00010\u0005¢\u0006\b\n\u0000\u001a\u0004\bB\u0010&R\u0013\u0010\u0004\u001a\u0004\u0018\u00010\u0005¢\u0006\b\n\u0000\u001a\u0004\bC\u0010&R\u0013\u0010 \u001a\u0004\u0018\u00010!¢\u0006\b\n\u0000\u001a\u0004\bD\u0010E¨\u0006\u0001"}, d2 = {"Lcom/stripe/android/model/Card;", "Lcom/stripe/android/model/StripeModel;", "Lcom/stripe/android/model/StripePaymentSource;", "Lcom/stripe/android/model/TokenParams;", "number", "", "cvc", "expMonth", "", "expYear", "name", "addressLine1", "addressLine1Check", "addressLine2", "addressCity", "addressState", "addressZip", "addressZipCheck", "addressCountry", "last4", "brand", "Lcom/stripe/android/model/CardBrand;", "funding", "Lcom/stripe/android/model/CardFunding;", "fingerprint", "country", "currency", "customerId", "cvcCheck", "id", "loggingTokens", "", "tokenizationMethod", "Lcom/stripe/android/model/TokenizationMethod;", "metadata", "", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/stripe/android/model/CardBrand;Lcom/stripe/android/model/CardFunding;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;Lcom/stripe/android/model/TokenizationMethod;Ljava/util/Map;)V", "getAddressCity", "()Ljava/lang/String;", "getAddressCountry", "getAddressLine1", "getAddressLine1Check", "getAddressLine2", "getAddressState", "getAddressZip", "getAddressZipCheck", "getBrand", "()Lcom/stripe/android/model/CardBrand;", "getCountry", "getCurrency", "getCustomerId", "getCvc", "getCvcCheck", "getExpMonth", "()Ljava/lang/Integer;", "Ljava/lang/Integer;", "getExpYear", "getFingerprint", "getFunding", "()Lcom/stripe/android/model/CardFunding;", "getId", "getLast4", "getLoggingTokens$stripe_release", "()Ljava/util/Set;", "getMetadata", "()Ljava/util/Map;", "getName", "getNumber", "getTokenizationMethod", "()Lcom/stripe/android/model/TokenizationMethod;", "component1", "component10", "component11", "component12", "component13", "component14", "component15", "component16", "component17", "component18", "component19", "component2", "component20", "component21", "component22", "component23", "component23$stripe_release", "component24", "component25", "component3", "component4", "component5", "component6", "component7", "component8", "component9", "copy", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/stripe/android/model/CardBrand;Lcom/stripe/android/model/CardFunding;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;Lcom/stripe/android/model/TokenizationMethod;Ljava/util/Map;)Lcom/stripe/android/model/Card;", "describeContents", "equals", "", "other", "", "hashCode", "toBuilder", "Lcom/stripe/android/model/Card$Builder;", "toParamMap", "toPaymentMethodParamsCard", "Lcom/stripe/android/model/PaymentMethodCreateParams$Card;", "toPaymentMethodsParams", "Lcom/stripe/android/model/PaymentMethodCreateParams;", "toString", "validateCVC", "validateCard", "now", "Ljava/util/Calendar;", "validateCard$stripe_release", "validateExpMonth", "validateExpYear", "validateExpYear$stripe_release", "validateExpiryDate", "validateExpiryDate$stripe_release", "validateNumber", "writeToParcel", "", "parcel", "Landroid/os/Parcel;", "flags", "Builder", "Companion", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: Card.kt */
public final class Card extends TokenParams implements StripeModel, StripePaymentSource {
    public static final Parcelable.Creator CREATOR = new Creator();
    public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
    public static final String OBJECT_TYPE = "card";
    private final String addressCity;
    private final String addressCountry;
    private final String addressLine1;
    private final String addressLine1Check;
    private final String addressLine2;
    private final String addressState;
    private final String addressZip;
    private final String addressZipCheck;
    private final CardBrand brand;
    private final String country;
    private final String currency;
    private final String customerId;
    private final String cvc;
    private final String cvcCheck;
    private final Integer expMonth;
    private final Integer expYear;
    private final String fingerprint;
    private final CardFunding funding;
    private final String id;
    private final String last4;
    private final Set<String> loggingTokens;
    private final Map<String, String> metadata;
    private final String name;
    private final String number;
    private final TokenizationMethod tokenizationMethod;

    @Metadata(bv = {1, 0, 3}, k = 3, mv = {1, 1, 16})
    public static class Creator implements Parcelable.Creator {
        public final Object createFromParcel(Parcel parcel) {
            String str;
            LinkedHashMap linkedHashMap;
            String str2;
            LinkedHashSet linkedHashSet;
            Intrinsics.checkParameterIsNotNull(parcel, "in");
            String readString = parcel.readString();
            String readString2 = parcel.readString();
            Integer valueOf = parcel.readInt() != 0 ? Integer.valueOf(parcel.readInt()) : null;
            Integer valueOf2 = parcel.readInt() != 0 ? Integer.valueOf(parcel.readInt()) : null;
            String readString3 = parcel.readString();
            String readString4 = parcel.readString();
            String readString5 = parcel.readString();
            String readString6 = parcel.readString();
            String readString7 = parcel.readString();
            String readString8 = parcel.readString();
            String readString9 = parcel.readString();
            String readString10 = parcel.readString();
            String readString11 = parcel.readString();
            String readString12 = parcel.readString();
            CardBrand cardBrand = (CardBrand) Enum.valueOf(CardBrand.class, parcel.readString());
            CardFunding cardFunding = parcel.readInt() != 0 ? (CardFunding) Enum.valueOf(CardFunding.class, parcel.readString()) : null;
            String readString13 = parcel.readString();
            String readString14 = parcel.readString();
            String readString15 = parcel.readString();
            String readString16 = parcel.readString();
            String readString17 = parcel.readString();
            String readString18 = parcel.readString();
            int readInt = parcel.readInt();
            LinkedHashSet linkedHashSet2 = new LinkedHashSet(readInt);
            while (true) {
                str = readString11;
                if (readInt == 0) {
                    break;
                }
                linkedHashSet2.add(parcel.readString());
                readInt--;
                readString11 = str;
            }
            TokenizationMethod tokenizationMethod = parcel.readInt() != 0 ? (TokenizationMethod) Enum.valueOf(TokenizationMethod.class, parcel.readString()) : null;
            if (parcel.readInt() != 0) {
                int readInt2 = parcel.readInt();
                LinkedHashMap linkedHashMap2 = new LinkedHashMap(readInt2);
                while (true) {
                    linkedHashSet = linkedHashSet2;
                    if (readInt2 == 0) {
                        break;
                    }
                    linkedHashMap2.put(parcel.readString(), parcel.readString());
                    readInt2--;
                    linkedHashSet2 = linkedHashSet;
                    readString10 = readString10;
                }
                str2 = readString10;
                linkedHashMap = linkedHashMap2;
            } else {
                linkedHashSet = linkedHashSet2;
                str2 = readString10;
                linkedHashMap = null;
            }
            return new Card(readString, readString2, valueOf, valueOf2, readString3, readString4, readString5, readString6, readString7, readString8, readString9, str2, str, readString12, cardBrand, cardFunding, readString13, readString14, readString15, readString16, readString17, readString18, linkedHashSet, tokenizationMethod, linkedHashMap);
        }

        public final Object[] newArray(int i) {
            return new Card[i];
        }
    }

    public static /* synthetic */ Card copy$default(Card card, String str, String str2, Integer num, Integer num2, String str3, String str4, String str5, String str6, String str7, String str8, String str9, String str10, String str11, String str12, CardBrand cardBrand, CardFunding cardFunding, String str13, String str14, String str15, String str16, String str17, String str18, Set set, TokenizationMethod tokenizationMethod2, Map map, int i, Object obj) {
        Card card2 = card;
        int i2 = i;
        return card.copy((i2 & 1) != 0 ? card2.number : str, (i2 & 2) != 0 ? card2.cvc : str2, (i2 & 4) != 0 ? card2.expMonth : num, (i2 & 8) != 0 ? card2.expYear : num2, (i2 & 16) != 0 ? card2.name : str3, (i2 & 32) != 0 ? card2.addressLine1 : str4, (i2 & 64) != 0 ? card2.addressLine1Check : str5, (i2 & 128) != 0 ? card2.addressLine2 : str6, (i2 & 256) != 0 ? card2.addressCity : str7, (i2 & 512) != 0 ? card2.addressState : str8, (i2 & 1024) != 0 ? card2.addressZip : str9, (i2 & 2048) != 0 ? card2.addressZipCheck : str10, (i2 & 4096) != 0 ? card2.addressCountry : str11, (i2 & 8192) != 0 ? card2.last4 : str12, (i2 & 16384) != 0 ? card2.brand : cardBrand, (i2 & 32768) != 0 ? card2.funding : cardFunding, (i2 & 65536) != 0 ? card2.fingerprint : str13, (i2 & 131072) != 0 ? card2.country : str14, (i2 & 262144) != 0 ? card2.currency : str15, (i2 & 524288) != 0 ? card2.customerId : str16, (i2 & 1048576) != 0 ? card2.cvcCheck : str17, (i2 & 2097152) != 0 ? card.getId() : str18, (i2 & 4194304) != 0 ? card2.loggingTokens : set, (i2 & 8388608) != 0 ? card2.tokenizationMethod : tokenizationMethod2, (i2 & 16777216) != 0 ? card2.metadata : map);
    }

    @JvmStatic
    public static final Card create(String str, Integer num, Integer num2, String str2) {
        return Companion.create(str, num, num2, str2);
    }

    @JvmStatic
    public static final Card fromJson(JSONObject jSONObject) {
        return Companion.fromJson(jSONObject);
    }

    @JvmStatic
    public static final Card fromString(String str) {
        return Companion.fromString(str);
    }

    public final String component1() {
        return this.number;
    }

    public final String component10() {
        return this.addressState;
    }

    public final String component11() {
        return this.addressZip;
    }

    public final String component12() {
        return this.addressZipCheck;
    }

    public final String component13() {
        return this.addressCountry;
    }

    public final String component14() {
        return this.last4;
    }

    public final CardBrand component15() {
        return this.brand;
    }

    public final CardFunding component16() {
        return this.funding;
    }

    public final String component17() {
        return this.fingerprint;
    }

    public final String component18() {
        return this.country;
    }

    public final String component19() {
        return this.currency;
    }

    public final String component2() {
        return this.cvc;
    }

    public final String component20() {
        return this.customerId;
    }

    public final String component21() {
        return this.cvcCheck;
    }

    public final String component22() {
        return getId();
    }

    public final Set<String> component23$stripe_release() {
        return this.loggingTokens;
    }

    public final TokenizationMethod component24() {
        return this.tokenizationMethod;
    }

    public final Map<String, String> component25() {
        return this.metadata;
    }

    public final Integer component3() {
        return this.expMonth;
    }

    public final Integer component4() {
        return this.expYear;
    }

    public final String component5() {
        return this.name;
    }

    public final String component6() {
        return this.addressLine1;
    }

    public final String component7() {
        return this.addressLine1Check;
    }

    public final String component8() {
        return this.addressLine2;
    }

    public final String component9() {
        return this.addressCity;
    }

    public final Card copy(String str, String str2, Integer num, Integer num2, String str3, String str4, String str5, String str6, String str7, String str8, String str9, String str10, String str11, String str12, CardBrand cardBrand, CardFunding cardFunding, String str13, String str14, String str15, String str16, String str17, String str18, Set<String> set, TokenizationMethod tokenizationMethod2, Map<String, String> map) {
        String str19 = str;
        Intrinsics.checkParameterIsNotNull(cardBrand, "brand");
        Intrinsics.checkParameterIsNotNull(set, "loggingTokens");
        return new Card(str, str2, num, num2, str3, str4, str5, str6, str7, str8, str9, str10, str11, str12, cardBrand, cardFunding, str13, str14, str15, str16, str17, str18, set, tokenizationMethod2, map);
    }

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Card)) {
            return false;
        }
        Card card = (Card) obj;
        return Intrinsics.areEqual((Object) this.number, (Object) card.number) && Intrinsics.areEqual((Object) this.cvc, (Object) card.cvc) && Intrinsics.areEqual((Object) this.expMonth, (Object) card.expMonth) && Intrinsics.areEqual((Object) this.expYear, (Object) card.expYear) && Intrinsics.areEqual((Object) this.name, (Object) card.name) && Intrinsics.areEqual((Object) this.addressLine1, (Object) card.addressLine1) && Intrinsics.areEqual((Object) this.addressLine1Check, (Object) card.addressLine1Check) && Intrinsics.areEqual((Object) this.addressLine2, (Object) card.addressLine2) && Intrinsics.areEqual((Object) this.addressCity, (Object) card.addressCity) && Intrinsics.areEqual((Object) this.addressState, (Object) card.addressState) && Intrinsics.areEqual((Object) this.addressZip, (Object) card.addressZip) && Intrinsics.areEqual((Object) this.addressZipCheck, (Object) card.addressZipCheck) && Intrinsics.areEqual((Object) this.addressCountry, (Object) card.addressCountry) && Intrinsics.areEqual((Object) this.last4, (Object) card.last4) && Intrinsics.areEqual((Object) this.brand, (Object) card.brand) && Intrinsics.areEqual((Object) this.funding, (Object) card.funding) && Intrinsics.areEqual((Object) this.fingerprint, (Object) card.fingerprint) && Intrinsics.areEqual((Object) this.country, (Object) card.country) && Intrinsics.areEqual((Object) this.currency, (Object) card.currency) && Intrinsics.areEqual((Object) this.customerId, (Object) card.customerId) && Intrinsics.areEqual((Object) this.cvcCheck, (Object) card.cvcCheck) && Intrinsics.areEqual((Object) getId(), (Object) card.getId()) && Intrinsics.areEqual((Object) this.loggingTokens, (Object) card.loggingTokens) && Intrinsics.areEqual((Object) this.tokenizationMethod, (Object) card.tokenizationMethod) && Intrinsics.areEqual((Object) this.metadata, (Object) card.metadata);
    }

    public int hashCode() {
        String str = this.number;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.cvc;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        Integer num = this.expMonth;
        int hashCode3 = (hashCode2 + (num != null ? num.hashCode() : 0)) * 31;
        Integer num2 = this.expYear;
        int hashCode4 = (hashCode3 + (num2 != null ? num2.hashCode() : 0)) * 31;
        String str3 = this.name;
        int hashCode5 = (hashCode4 + (str3 != null ? str3.hashCode() : 0)) * 31;
        String str4 = this.addressLine1;
        int hashCode6 = (hashCode5 + (str4 != null ? str4.hashCode() : 0)) * 31;
        String str5 = this.addressLine1Check;
        int hashCode7 = (hashCode6 + (str5 != null ? str5.hashCode() : 0)) * 31;
        String str6 = this.addressLine2;
        int hashCode8 = (hashCode7 + (str6 != null ? str6.hashCode() : 0)) * 31;
        String str7 = this.addressCity;
        int hashCode9 = (hashCode8 + (str7 != null ? str7.hashCode() : 0)) * 31;
        String str8 = this.addressState;
        int hashCode10 = (hashCode9 + (str8 != null ? str8.hashCode() : 0)) * 31;
        String str9 = this.addressZip;
        int hashCode11 = (hashCode10 + (str9 != null ? str9.hashCode() : 0)) * 31;
        String str10 = this.addressZipCheck;
        int hashCode12 = (hashCode11 + (str10 != null ? str10.hashCode() : 0)) * 31;
        String str11 = this.addressCountry;
        int hashCode13 = (hashCode12 + (str11 != null ? str11.hashCode() : 0)) * 31;
        String str12 = this.last4;
        int hashCode14 = (hashCode13 + (str12 != null ? str12.hashCode() : 0)) * 31;
        CardBrand cardBrand = this.brand;
        int hashCode15 = (hashCode14 + (cardBrand != null ? cardBrand.hashCode() : 0)) * 31;
        CardFunding cardFunding = this.funding;
        int hashCode16 = (hashCode15 + (cardFunding != null ? cardFunding.hashCode() : 0)) * 31;
        String str13 = this.fingerprint;
        int hashCode17 = (hashCode16 + (str13 != null ? str13.hashCode() : 0)) * 31;
        String str14 = this.country;
        int hashCode18 = (hashCode17 + (str14 != null ? str14.hashCode() : 0)) * 31;
        String str15 = this.currency;
        int hashCode19 = (hashCode18 + (str15 != null ? str15.hashCode() : 0)) * 31;
        String str16 = this.customerId;
        int hashCode20 = (hashCode19 + (str16 != null ? str16.hashCode() : 0)) * 31;
        String str17 = this.cvcCheck;
        int hashCode21 = (hashCode20 + (str17 != null ? str17.hashCode() : 0)) * 31;
        String id2 = getId();
        int hashCode22 = (hashCode21 + (id2 != null ? id2.hashCode() : 0)) * 31;
        Set<String> set = this.loggingTokens;
        int hashCode23 = (hashCode22 + (set != null ? set.hashCode() : 0)) * 31;
        TokenizationMethod tokenizationMethod2 = this.tokenizationMethod;
        int hashCode24 = (hashCode23 + (tokenizationMethod2 != null ? tokenizationMethod2.hashCode() : 0)) * 31;
        Map<String, String> map = this.metadata;
        if (map != null) {
            i = map.hashCode();
        }
        return hashCode24 + i;
    }

    public String toString() {
        return "Card(number=" + this.number + ", cvc=" + this.cvc + ", expMonth=" + this.expMonth + ", expYear=" + this.expYear + ", name=" + this.name + ", addressLine1=" + this.addressLine1 + ", addressLine1Check=" + this.addressLine1Check + ", addressLine2=" + this.addressLine2 + ", addressCity=" + this.addressCity + ", addressState=" + this.addressState + ", addressZip=" + this.addressZip + ", addressZipCheck=" + this.addressZipCheck + ", addressCountry=" + this.addressCountry + ", last4=" + this.last4 + ", brand=" + this.brand + ", funding=" + this.funding + ", fingerprint=" + this.fingerprint + ", country=" + this.country + ", currency=" + this.currency + ", customerId=" + this.customerId + ", cvcCheck=" + this.cvcCheck + ", id=" + getId() + ", loggingTokens=" + this.loggingTokens + ", tokenizationMethod=" + this.tokenizationMethod + ", metadata=" + this.metadata + ")";
    }

    public void writeToParcel(Parcel parcel, int i) {
        Intrinsics.checkParameterIsNotNull(parcel, "parcel");
        parcel.writeString(this.number);
        parcel.writeString(this.cvc);
        Integer num = this.expMonth;
        if (num != null) {
            parcel.writeInt(1);
            parcel.writeInt(num.intValue());
        } else {
            parcel.writeInt(0);
        }
        Integer num2 = this.expYear;
        if (num2 != null) {
            parcel.writeInt(1);
            parcel.writeInt(num2.intValue());
        } else {
            parcel.writeInt(0);
        }
        parcel.writeString(this.name);
        parcel.writeString(this.addressLine1);
        parcel.writeString(this.addressLine1Check);
        parcel.writeString(this.addressLine2);
        parcel.writeString(this.addressCity);
        parcel.writeString(this.addressState);
        parcel.writeString(this.addressZip);
        parcel.writeString(this.addressZipCheck);
        parcel.writeString(this.addressCountry);
        parcel.writeString(this.last4);
        parcel.writeString(this.brand.name());
        CardFunding cardFunding = this.funding;
        if (cardFunding != null) {
            parcel.writeInt(1);
            parcel.writeString(cardFunding.name());
        } else {
            parcel.writeInt(0);
        }
        parcel.writeString(this.fingerprint);
        parcel.writeString(this.country);
        parcel.writeString(this.currency);
        parcel.writeString(this.customerId);
        parcel.writeString(this.cvcCheck);
        parcel.writeString(this.id);
        Set<String> set = this.loggingTokens;
        parcel.writeInt(set.size());
        for (String writeString : set) {
            parcel.writeString(writeString);
        }
        TokenizationMethod tokenizationMethod2 = this.tokenizationMethod;
        if (tokenizationMethod2 != null) {
            parcel.writeInt(1);
            parcel.writeString(tokenizationMethod2.name());
        } else {
            parcel.writeInt(0);
        }
        Map<String, String> map = this.metadata;
        if (map != null) {
            parcel.writeInt(1);
            parcel.writeInt(map.size());
            for (Map.Entry<String, String> next : map.entrySet()) {
                parcel.writeString(next.getKey());
                parcel.writeString(next.getValue());
            }
            return;
        }
        parcel.writeInt(0);
    }

    public final String getNumber() {
        return this.number;
    }

    public final String getCvc() {
        return this.cvc;
    }

    public final Integer getExpMonth() {
        return this.expMonth;
    }

    public final Integer getExpYear() {
        return this.expYear;
    }

    public final String getName() {
        return this.name;
    }

    public final String getAddressLine1() {
        return this.addressLine1;
    }

    public final String getAddressLine1Check() {
        return this.addressLine1Check;
    }

    public final String getAddressLine2() {
        return this.addressLine2;
    }

    public final String getAddressCity() {
        return this.addressCity;
    }

    public final String getAddressState() {
        return this.addressState;
    }

    public final String getAddressZip() {
        return this.addressZip;
    }

    public final String getAddressZipCheck() {
        return this.addressZipCheck;
    }

    public final String getAddressCountry() {
        return this.addressCountry;
    }

    public final String getLast4() {
        return this.last4;
    }

    public final CardBrand getBrand() {
        return this.brand;
    }

    public final CardFunding getFunding() {
        return this.funding;
    }

    public final String getFingerprint() {
        return this.fingerprint;
    }

    public final String getCountry() {
        return this.country;
    }

    public final String getCurrency() {
        return this.currency;
    }

    public final String getCustomerId() {
        return this.customerId;
    }

    public final String getCvcCheck() {
        return this.cvcCheck;
    }

    public String getId() {
        return this.id;
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ Card(String str, String str2, Integer num, Integer num2, String str3, String str4, String str5, String str6, String str7, String str8, String str9, String str10, String str11, String str12, CardBrand cardBrand, CardFunding cardFunding, String str13, String str14, String str15, String str16, String str17, String str18, Set set, TokenizationMethod tokenizationMethod2, Map map, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(str, str2, num, num2, str3, str4, str5, str6, str7, str8, str9, str10, str11, str12, cardBrand, cardFunding, str13, str14, str15, str16, str17, str18, (i & 4194304) != 0 ? SetsKt.emptySet() : set, (i & 8388608) != 0 ? null : tokenizationMethod2, map);
    }

    public final Set<String> getLoggingTokens$stripe_release() {
        return this.loggingTokens;
    }

    public final TokenizationMethod getTokenizationMethod() {
        return this.tokenizationMethod;
    }

    public final Map<String, String> getMetadata() {
        return this.metadata;
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public Card(java.lang.String r5, java.lang.String r6, java.lang.Integer r7, java.lang.Integer r8, java.lang.String r9, java.lang.String r10, java.lang.String r11, java.lang.String r12, java.lang.String r13, java.lang.String r14, java.lang.String r15, java.lang.String r16, java.lang.String r17, java.lang.String r18, com.stripe.android.model.CardBrand r19, com.stripe.android.model.CardFunding r20, java.lang.String r21, java.lang.String r22, java.lang.String r23, java.lang.String r24, java.lang.String r25, java.lang.String r26, java.util.Set<java.lang.String> r27, com.stripe.android.model.TokenizationMethod r28, java.util.Map<java.lang.String, java.lang.String> r29) {
        /*
            r4 = this;
            r0 = r4
            r1 = r19
            r2 = r27
            java.lang.String r3 = "brand"
            kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(r1, r3)
            java.lang.String r3 = "loggingTokens"
            kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(r2, r3)
            java.lang.String r3 = "card"
            r4.<init>(r3, r2)
            r3 = r5
            r0.number = r3
            r3 = r6
            r0.cvc = r3
            r3 = r7
            r0.expMonth = r3
            r3 = r8
            r0.expYear = r3
            r3 = r9
            r0.name = r3
            r3 = r10
            r0.addressLine1 = r3
            r3 = r11
            r0.addressLine1Check = r3
            r3 = r12
            r0.addressLine2 = r3
            r3 = r13
            r0.addressCity = r3
            r3 = r14
            r0.addressState = r3
            r3 = r15
            r0.addressZip = r3
            r3 = r16
            r0.addressZipCheck = r3
            r3 = r17
            r0.addressCountry = r3
            r3 = r18
            r0.last4 = r3
            r0.brand = r1
            r1 = r20
            r0.funding = r1
            r1 = r21
            r0.fingerprint = r1
            r1 = r22
            r0.country = r1
            r1 = r23
            r0.currency = r1
            r1 = r24
            r0.customerId = r1
            r1 = r25
            r0.cvcCheck = r1
            r1 = r26
            r0.id = r1
            r0.loggingTokens = r2
            r1 = r28
            r0.tokenizationMethod = r1
            r1 = r29
            r0.metadata = r1
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.stripe.android.model.Card.<init>(java.lang.String, java.lang.String, java.lang.Integer, java.lang.Integer, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, com.stripe.android.model.CardBrand, com.stripe.android.model.CardFunding, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.util.Set, com.stripe.android.model.TokenizationMethod, java.util.Map):void");
    }

    public final PaymentMethodCreateParams toPaymentMethodsParams() {
        PaymentMethodCreateParams.Companion companion = PaymentMethodCreateParams.Companion;
        PaymentMethodCreateParams.Card paymentMethodParamsCard = toPaymentMethodParamsCard();
        String str = this.name;
        String str2 = this.addressLine1;
        String str3 = this.addressLine2;
        return PaymentMethodCreateParams.Companion.create$default(companion, paymentMethodParamsCard, new PaymentMethod.BillingDetails(new Address(this.addressCity, this.addressCountry, str2, str3, this.addressZip, this.addressState), (String) null, str, (String) null, 10, (DefaultConstructorMarker) null), (Map) null, 4, (Object) null);
    }

    public final PaymentMethodCreateParams.Card toPaymentMethodParamsCard() {
        return new PaymentMethodCreateParams.Card(this.number, this.expMonth, this.expYear, this.cvc, (String) null, (Set) null, 48, (DefaultConstructorMarker) null);
    }

    public final Builder toBuilder() {
        return new Builder(this.number, this.expMonth, this.expYear, this.cvc).name(this.name).addressLine1(this.addressLine1).addressLine1Check(this.addressLine1Check).addressLine2(this.addressLine2).addressCity(this.addressCity).addressState(this.addressState).addressZip(this.addressZip).addressZipCheck(this.addressZipCheck).addressCountry(this.addressCountry).brand(this.brand).fingerprint(this.fingerprint).funding(this.funding).country(this.country).currency(this.currency).customer(this.customerId).cvcCheck(this.cvcCheck).last4(this.last4).id(getId()).tokenizationMethod(this.tokenizationMethod).metadata(this.metadata).loggingTokens(this.loggingTokens);
    }

    public final boolean validateCard() {
        Calendar instance = Calendar.getInstance();
        Intrinsics.checkExpressionValueIsNotNull(instance, "Calendar.getInstance()");
        return validateCard$stripe_release(instance);
    }

    public final boolean validateNumber() {
        return CardUtils.isValidCardNumber(this.number);
    }

    public final boolean validateExpiryDate() {
        Calendar instance = Calendar.getInstance();
        Intrinsics.checkExpressionValueIsNotNull(instance, "Calendar.getInstance()");
        return validateExpiryDate$stripe_release(instance);
    }

    public final boolean validateCVC() {
        CharSequence charSequence = this.cvc;
        if (charSequence == null || StringsKt.isBlank(charSequence)) {
            return false;
        }
        String str = this.cvc;
        if (str != null) {
            String obj = StringsKt.trim((CharSequence) str).toString();
            boolean isValidCvc = this.brand.isValidCvc(this.cvc);
            if (!ModelUtils.INSTANCE.isWholePositiveNumber$stripe_release(obj) || !isValidCvc) {
                return false;
            }
            return true;
        }
        throw new TypeCastException("null cannot be cast to non-null type kotlin.CharSequence");
    }

    public final boolean validateExpMonth() {
        Integer num = this.expMonth;
        if (num == null) {
            return false;
        }
        int intValue = num.intValue();
        return 1 <= intValue && 12 >= intValue;
    }

    public final boolean validateExpYear$stripe_release(Calendar calendar) {
        Intrinsics.checkParameterIsNotNull(calendar, "now");
        Integer num = this.expYear;
        if (num != null) {
            if (!ModelUtils.INSTANCE.hasYearPassed$stripe_release(num.intValue(), calendar)) {
                return true;
            }
        }
        return false;
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x002b A[ORIG_RETURN, RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean validateCard$stripe_release(java.util.Calendar r4) {
        /*
            r3 = this;
            java.lang.String r0 = "now"
            kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(r4, r0)
            java.lang.String r0 = r3.cvc
            r1 = 1
            r2 = 0
            if (r0 != 0) goto L_0x0018
            boolean r0 = r3.validateNumber()
            if (r0 == 0) goto L_0x002b
            boolean r4 = r3.validateExpiryDate$stripe_release(r4)
            if (r4 == 0) goto L_0x002b
            goto L_0x002c
        L_0x0018:
            boolean r0 = r3.validateNumber()
            if (r0 == 0) goto L_0x002b
            boolean r4 = r3.validateExpiryDate$stripe_release(r4)
            if (r4 == 0) goto L_0x002b
            boolean r4 = r3.validateCVC()
            if (r4 == 0) goto L_0x002b
            goto L_0x002c
        L_0x002b:
            r1 = 0
        L_0x002c:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.stripe.android.model.Card.validateCard$stripe_release(java.util.Calendar):boolean");
    }

    public final boolean validateExpiryDate$stripe_release(Calendar calendar) {
        Intrinsics.checkParameterIsNotNull(calendar, "now");
        Integer num = this.expMonth;
        if (num == null || !validateExpMonth() || this.expYear == null || !validateExpYear$stripe_release(calendar) || ModelUtils.INSTANCE.hasMonthPassed$stripe_release(this.expYear.intValue(), num.intValue(), calendar)) {
            return false;
        }
        return true;
    }

    public Map<String, Object> toParamMap() {
        String str = this.number;
        if (str == null) {
            str = "";
        }
        String str2 = str;
        Integer num = this.expMonth;
        int intValue = num != null ? num.intValue() : 0;
        Integer num2 = this.expYear;
        return new CardParams(str2, intValue, num2 != null ? num2.intValue() : 0, this.cvc, this.name, new Address(this.addressCity, this.addressCountry, this.addressLine1, this.addressLine2, this.addressZip, this.addressState), this.currency).toParamMap();
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0002\b\f\n\u0002\u0018\u0002\n\u0002\b\f\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\"\n\u0000\n\u0002\u0010$\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\n\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B5\u0012\n\b\u0002\u0010\u0003\u001a\u0004\u0018\u00010\u0004\u0012\n\b\u0003\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u0012\n\b\u0003\u0010\u0007\u001a\u0004\u0018\u00010\u0006\u0012\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\u0004¢\u0006\u0002\u0010\tJ\u0010\u0010\n\u001a\u00020\u00002\b\u0010+\u001a\u0004\u0018\u00010\u0004J\u0010\u0010\u000b\u001a\u00020\u00002\b\u0010\u0014\u001a\u0004\u0018\u00010\u0004J\u0010\u0010\f\u001a\u00020\u00002\b\u0010,\u001a\u0004\u0018\u00010\u0004J\u0010\u0010\r\u001a\u00020\u00002\b\u0010\r\u001a\u0004\u0018\u00010\u0004J\u0010\u0010\u000e\u001a\u00020\u00002\b\u0010,\u001a\u0004\u0018\u00010\u0004J\u0010\u0010\u000f\u001a\u00020\u00002\b\u0010-\u001a\u0004\u0018\u00010\u0004J\u0010\u0010\u0010\u001a\u00020\u00002\b\u0010.\u001a\u0004\u0018\u00010\u0004J\u0010\u0010\u0011\u001a\u00020\u00002\b\u0010/\u001a\u0004\u0018\u00010\u0004J\u0010\u0010\u0012\u001a\u00020\u00002\b\u0010\u0012\u001a\u0004\u0018\u00010\u0013J\b\u00100\u001a\u00020\u0002H\u0016J\u0014\u00101\u001a\u0004\u0018\u00010\u00042\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004H\u0002J\u0010\u0010\u0014\u001a\u00020\u00002\b\u0010\u0014\u001a\u0004\u0018\u00010\u0004J\u0010\u0010\u0015\u001a\u00020\u00002\b\u0010\u0015\u001a\u0004\u0018\u00010\u0004J\u0010\u00102\u001a\u00020\u00002\b\u0010\u0016\u001a\u0004\u0018\u00010\u0004J\u0010\u0010\u0019\u001a\u00020\u00002\b\u0010\u0019\u001a\u0004\u0018\u00010\u0004J\u0010\u0010\u001e\u001a\u00020\u00002\b\u0010\u001e\u001a\u0004\u0018\u00010\u0004J\u0010\u0010\u001f\u001a\u00020\u00002\b\u0010\u001f\u001a\u0004\u0018\u00010 J\u0010\u0010!\u001a\u00020\u00002\b\u0010!\u001a\u0004\u0018\u00010\u0004J\u0010\u0010\"\u001a\u00020\u00002\b\u0010\"\u001a\u0004\u0018\u00010\u0004J\u0014\u0010#\u001a\u00020\u00002\f\u0010#\u001a\b\u0012\u0004\u0012\u00020\u00040$J\u001c\u0010%\u001a\u00020\u00002\u0014\u0010%\u001a\u0010\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0004\u0018\u00010&J\u0010\u0010'\u001a\u00020\u00002\b\u0010'\u001a\u0004\u0018\u00010\u0004J\u0014\u00103\u001a\u0004\u0018\u00010\u00042\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004H\u0002J\u0010\u0010)\u001a\u00020\u00002\b\u0010)\u001a\u0004\u0018\u00010*R\u0010\u0010\n\u001a\u0004\u0018\u00010\u0004X\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\u000b\u001a\u0004\u0018\u00010\u0004X\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\f\u001a\u0004\u0018\u00010\u0004X\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\r\u001a\u0004\u0018\u00010\u0004X\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\u000e\u001a\u0004\u0018\u00010\u0004X\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\u000f\u001a\u0004\u0018\u00010\u0004X\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\u0010\u001a\u0004\u0018\u00010\u0004X\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\u0011\u001a\u0004\u0018\u00010\u0004X\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\u0012\u001a\u0004\u0018\u00010\u0013X\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\u0014\u001a\u0004\u0018\u00010\u0004X\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\u0015\u001a\u0004\u0018\u00010\u0004X\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\u0016\u001a\u0004\u0018\u00010\u0004X\u000e¢\u0006\u0002\n\u0000R\u0016\u0010\b\u001a\u0004\u0018\u00010\u0004X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0017\u0010\u0018R\u0010\u0010\u0019\u001a\u0004\u0018\u00010\u0004X\u000e¢\u0006\u0002\n\u0000R\u0018\u0010\u0005\u001a\u0004\u0018\u00010\u0006X\u0004¢\u0006\n\n\u0002\u0010\u001c\u001a\u0004\b\u001a\u0010\u001bR\u0018\u0010\u0007\u001a\u0004\u0018\u00010\u0006X\u0004¢\u0006\n\n\u0002\u0010\u001c\u001a\u0004\b\u001d\u0010\u001bR\u0010\u0010\u001e\u001a\u0004\u0018\u00010\u0004X\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\u001f\u001a\u0004\u0018\u00010 X\u000e¢\u0006\u0002\n\u0000R\u0010\u0010!\u001a\u0004\u0018\u00010\u0004X\u000e¢\u0006\u0002\n\u0000R\u0014\u0010\"\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u000e¢\u0006\u0002\n\u0000R\u0016\u0010#\u001a\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010$X\u000e¢\u0006\u0002\n\u0000R\u001c\u0010%\u001a\u0010\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0004\u0018\u00010&X\u000e¢\u0006\u0002\n\u0000R\u0010\u0010'\u001a\u0004\u0018\u00010\u0004X\u000e¢\u0006\u0002\n\u0000R\u0016\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0004¢\u0006\b\n\u0000\u001a\u0004\b(\u0010\u0018R\u0010\u0010)\u001a\u0004\u0018\u00010*X\u000e¢\u0006\u0002\n\u0000¨\u00064"}, d2 = {"Lcom/stripe/android/model/Card$Builder;", "Lcom/stripe/android/ObjectBuilder;", "Lcom/stripe/android/model/Card;", "number", "", "expMonth", "", "expYear", "cvc", "(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;)V", "addressCity", "addressCountry", "addressLine1", "addressLine1Check", "addressLine2", "addressState", "addressZip", "addressZipCheck", "brand", "Lcom/stripe/android/model/CardBrand;", "country", "currency", "customerId", "getCvc$stripe_release", "()Ljava/lang/String;", "cvcCheck", "getExpMonth$stripe_release", "()Ljava/lang/Integer;", "Ljava/lang/Integer;", "getExpYear$stripe_release", "fingerprint", "funding", "Lcom/stripe/android/model/CardFunding;", "id", "last4", "loggingTokens", "", "metadata", "", "name", "getNumber$stripe_release", "tokenizationMethod", "Lcom/stripe/android/model/TokenizationMethod;", "city", "address", "state", "zip", "zipCheck", "build", "calculateLast4", "customer", "normalizeCardNumber", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: Card.kt */
    public static final class Builder implements ObjectBuilder<Card> {
        private String addressCity;
        private String addressCountry;
        private String addressLine1;
        private String addressLine1Check;
        private String addressLine2;
        private String addressState;
        private String addressZip;
        private String addressZipCheck;
        private CardBrand brand;
        private String country;
        private String currency;
        private String customerId;
        private final String cvc;
        private String cvcCheck;
        private final Integer expMonth;
        private final Integer expYear;
        private String fingerprint;
        private CardFunding funding;
        private String id;
        private String last4;
        private Set<String> loggingTokens;
        private Map<String, String> metadata;
        private String name;
        private final String number;
        private TokenizationMethod tokenizationMethod;

        public Builder() {
            this((String) null, (Integer) null, (Integer) null, (String) null, 15, (DefaultConstructorMarker) null);
        }

        public Builder(String str, Integer num, Integer num2, String str2) {
            this.number = str;
            this.expMonth = num;
            this.expYear = num2;
            this.cvc = str2;
        }

        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ Builder(String str, Integer num, Integer num2, String str2, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this((i & 1) != 0 ? null : str, (i & 2) != 0 ? null : num, (i & 4) != 0 ? null : num2, (i & 8) != 0 ? null : str2);
        }

        public final String getNumber$stripe_release() {
            return this.number;
        }

        public final Integer getExpMonth$stripe_release() {
            return this.expMonth;
        }

        public final Integer getExpYear$stripe_release() {
            return this.expYear;
        }

        public final String getCvc$stripe_release() {
            return this.cvc;
        }

        public final Builder name(String str) {
            Builder builder = this;
            builder.name = str;
            return builder;
        }

        public final Builder addressLine1(String str) {
            Builder builder = this;
            builder.addressLine1 = str;
            return builder;
        }

        public final Builder addressLine1Check(String str) {
            Builder builder = this;
            builder.addressLine1Check = str;
            return builder;
        }

        public final Builder addressLine2(String str) {
            Builder builder = this;
            builder.addressLine2 = str;
            return builder;
        }

        public final Builder addressCity(String str) {
            Builder builder = this;
            builder.addressCity = str;
            return builder;
        }

        public final Builder addressState(String str) {
            Builder builder = this;
            builder.addressState = str;
            return builder;
        }

        public final Builder addressZip(String str) {
            Builder builder = this;
            builder.addressZip = str;
            return builder;
        }

        public final Builder addressZipCheck(String str) {
            Builder builder = this;
            builder.addressZipCheck = str;
            return builder;
        }

        public final Builder addressCountry(String str) {
            Builder builder = this;
            builder.addressCountry = str;
            return builder;
        }

        public final Builder brand(CardBrand cardBrand) {
            Builder builder = this;
            builder.brand = cardBrand;
            return builder;
        }

        public final Builder fingerprint(String str) {
            Builder builder = this;
            builder.fingerprint = str;
            return builder;
        }

        public final Builder funding(CardFunding cardFunding) {
            Builder builder = this;
            builder.funding = cardFunding;
            return builder;
        }

        public final Builder country(String str) {
            Builder builder = this;
            builder.country = str;
            return builder;
        }

        public final Builder currency(String str) {
            Builder builder = this;
            builder.currency = str;
            return builder;
        }

        public final Builder customer(String str) {
            Builder builder = this;
            builder.customerId = str;
            return builder;
        }

        public final Builder cvcCheck(String str) {
            Builder builder = this;
            builder.cvcCheck = str;
            return builder;
        }

        public final Builder last4(String str) {
            Builder builder = this;
            builder.last4 = str;
            return builder;
        }

        public final Builder id(String str) {
            Builder builder = this;
            builder.id = str;
            return builder;
        }

        public final Builder tokenizationMethod(TokenizationMethod tokenizationMethod2) {
            Builder builder = this;
            builder.tokenizationMethod = tokenizationMethod2;
            return builder;
        }

        public final Builder metadata(Map<String, String> map) {
            Builder builder = this;
            builder.metadata = map;
            return builder;
        }

        public final Builder loggingTokens(Set<String> set) {
            Intrinsics.checkParameterIsNotNull(set, "loggingTokens");
            Builder builder = this;
            builder.loggingTokens = set;
            return builder;
        }

        public Card build() {
            String normalizeCardNumber = normalizeCardNumber(this.number);
            CharSequence charSequence = normalizeCardNumber;
            boolean z = false;
            String str = !(charSequence == null || StringsKt.isBlank(charSequence)) ? normalizeCardNumber : null;
            String str2 = this.last4;
            CharSequence charSequence2 = str2;
            if (charSequence2 == null || StringsKt.isBlank(charSequence2)) {
                str2 = null;
            }
            if (str2 == null) {
                str2 = calculateLast4(str);
            }
            String str3 = str2;
            Integer num = this.expMonth;
            Integer num2 = this.expYear;
            String str4 = this.cvc;
            CharSequence charSequence3 = str4;
            String str5 = !(charSequence3 == null || StringsKt.isBlank(charSequence3)) ? str4 : null;
            String str6 = this.name;
            CharSequence charSequence4 = str6;
            String str7 = !(charSequence4 == null || StringsKt.isBlank(charSequence4)) ? str6 : null;
            String str8 = this.addressLine1;
            CharSequence charSequence5 = str8;
            String str9 = !(charSequence5 == null || StringsKt.isBlank(charSequence5)) ? str8 : null;
            String str10 = this.addressLine1Check;
            CharSequence charSequence6 = str10;
            String str11 = !(charSequence6 == null || StringsKt.isBlank(charSequence6)) ? str10 : null;
            String str12 = this.addressLine2;
            CharSequence charSequence7 = str12;
            String str13 = !(charSequence7 == null || StringsKt.isBlank(charSequence7)) ? str12 : null;
            String str14 = this.addressCity;
            CharSequence charSequence8 = str14;
            String str15 = !(charSequence8 == null || StringsKt.isBlank(charSequence8)) ? str14 : null;
            String str16 = this.addressState;
            CharSequence charSequence9 = str16;
            String str17 = !(charSequence9 == null || StringsKt.isBlank(charSequence9)) ? str16 : null;
            String str18 = this.addressZip;
            CharSequence charSequence10 = str18;
            String str19 = !(charSequence10 == null || StringsKt.isBlank(charSequence10)) ? str18 : null;
            String str20 = this.addressZipCheck;
            CharSequence charSequence11 = str20;
            String str21 = !(charSequence11 == null || StringsKt.isBlank(charSequence11)) ? str20 : null;
            String str22 = this.addressCountry;
            CharSequence charSequence12 = str22;
            String str23 = !(charSequence12 == null || StringsKt.isBlank(charSequence12)) ? str22 : null;
            CardBrand cardBrand = this.brand;
            if (cardBrand == null) {
                cardBrand = CardUtils.getPossibleCardBrand(str);
            }
            CardBrand cardBrand2 = cardBrand;
            String str24 = this.fingerprint;
            CharSequence charSequence13 = str24;
            String str25 = !(charSequence13 == null || StringsKt.isBlank(charSequence13)) ? str24 : null;
            CardFunding cardFunding = this.funding;
            String str26 = this.country;
            CharSequence charSequence14 = str26;
            String str27 = !(charSequence14 == null || StringsKt.isBlank(charSequence14)) ? str26 : null;
            String str28 = this.currency;
            CharSequence charSequence15 = str28;
            String str29 = !(charSequence15 == null || StringsKt.isBlank(charSequence15)) ? str28 : null;
            String str30 = this.customerId;
            CharSequence charSequence16 = str30;
            String str31 = !(charSequence16 == null || StringsKt.isBlank(charSequence16)) ? str30 : null;
            String str32 = this.cvcCheck;
            CharSequence charSequence17 = str32;
            String str33 = !(charSequence17 == null || StringsKt.isBlank(charSequence17)) ? str32 : null;
            String str34 = this.id;
            CharSequence charSequence18 = str34;
            if (charSequence18 == null || StringsKt.isBlank(charSequence18)) {
                z = true;
            }
            String str35 = !z ? str34 : null;
            TokenizationMethod tokenizationMethod2 = this.tokenizationMethod;
            Map<String, String> map = this.metadata;
            Set<String> set = this.loggingTokens;
            if (set == null) {
                set = SetsKt.emptySet();
            }
            return new Card(str, str5, num, num2, str7, str9, str11, str13, str15, str17, str19, str21, str23, str3, cardBrand2, cardFunding, str25, str27, str29, str31, str33, str35, set, tokenizationMethod2, map);
        }

        private final String normalizeCardNumber(String str) {
            if (str != null) {
                if (str != null) {
                    String obj = StringsKt.trim((CharSequence) str).toString();
                    if (obj != null) {
                        return new Regex("\\s+|-").replace((CharSequence) obj, "");
                    }
                } else {
                    throw new TypeCastException("null cannot be cast to non-null type kotlin.CharSequence");
                }
            }
            return null;
        }

        private final String calculateLast4(String str) {
            if (str == null || str.length() <= 4) {
                return null;
            }
            String substring = str.substring(str.length() - 4);
            Intrinsics.checkExpressionValueIsNotNull(substring, "(this as java.lang.String).substring(startIndex)");
            return substring;
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J=\u0010\u0005\u001a\u00020\u00062\n\b\u0002\u0010\u0007\u001a\u0004\u0018\u00010\u00042\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\t2\n\b\u0002\u0010\n\u001a\u0004\u0018\u00010\t2\n\b\u0002\u0010\u000b\u001a\u0004\u0018\u00010\u0004H\u0007¢\u0006\u0002\u0010\fJ\u0014\u0010\r\u001a\u0004\u0018\u00010\u00062\b\u0010\u000e\u001a\u0004\u0018\u00010\u000fH\u0007J\u0012\u0010\u0010\u001a\u0004\u0018\u00010\u00062\u0006\u0010\u0011\u001a\u00020\u0004H\u0007R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\u0012"}, d2 = {"Lcom/stripe/android/model/Card$Companion;", "", "()V", "OBJECT_TYPE", "", "create", "Lcom/stripe/android/model/Card;", "number", "expMonth", "", "expYear", "cvc", "(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;)Lcom/stripe/android/model/Card;", "fromJson", "jsonObject", "Lorg/json/JSONObject;", "fromString", "jsonString", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: Card.kt */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        @JvmStatic
        public final Card fromString(String str) {
            Object obj;
            Intrinsics.checkParameterIsNotNull(str, "jsonString");
            try {
                Result.Companion companion = Result.Companion;
                Companion companion2 = this;
                obj = Result.m4constructorimpl(new JSONObject(str));
            } catch (Throwable th) {
                Result.Companion companion3 = Result.Companion;
                obj = Result.m4constructorimpl(ResultKt.createFailure(th));
            }
            if (Result.m10isFailureimpl(obj)) {
                obj = null;
            }
            JSONObject jSONObject = (JSONObject) obj;
            if (jSONObject != null) {
                return Card.Companion.fromJson(jSONObject);
            }
            return null;
        }

        @JvmStatic
        public final Card fromJson(JSONObject jSONObject) {
            if (jSONObject != null) {
                return new CardJsonParser().parse(jSONObject);
            }
            return null;
        }

        public static /* synthetic */ Card create$default(Companion companion, String str, Integer num, Integer num2, String str2, int i, Object obj) {
            if ((i & 1) != 0) {
                str = null;
            }
            if ((i & 2) != 0) {
                num = null;
            }
            if ((i & 4) != 0) {
                num2 = null;
            }
            if ((i & 8) != 0) {
                str2 = null;
            }
            return companion.create(str, num, num2, str2);
        }

        @JvmStatic
        public final Card create(String str, Integer num, Integer num2, String str2) {
            return new Builder(str, num, num2, str2).build();
        }
    }
}
