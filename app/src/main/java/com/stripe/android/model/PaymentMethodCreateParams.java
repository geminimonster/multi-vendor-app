package com.stripe.android.model;

import android.os.Parcel;
import android.os.Parcelable;
import com.stripe.android.ObjectBuilder;
import com.stripe.android.model.PaymentMethod;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import kotlin.Metadata;
import kotlin.Pair;
import kotlin.TuplesKt;
import kotlin.TypeCastException;
import kotlin.collections.CollectionsKt;
import kotlin.collections.MapsKt;
import kotlin.collections.SetsKt;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.internal.Intrinsics;
import org.json.JSONException;
import org.json.JSONObject;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\"\n\u0002\b\n\n\u0002\u0010\u0000\n\u0002\b\u0010\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u000b\b\b\u0018\u0000 K2\u00020\u00012\u00020\u0002:\tHIJKLMNOPB/\b\u0012\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u0012\u0014\u0010\u0007\u001a\u0010\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\t\u0018\u00010\b¢\u0006\u0002\u0010\nB/\b\u0012\u0012\u0006\u0010\u000b\u001a\u00020\f\u0012\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u0012\u0014\u0010\u0007\u001a\u0010\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\t\u0018\u00010\b¢\u0006\u0002\u0010\rB/\b\u0012\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u0012\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u0012\u0014\u0010\u0007\u001a\u0010\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\t\u0018\u00010\b¢\u0006\u0002\u0010\u0010B/\b\u0012\u0012\u0006\u0010\u0011\u001a\u00020\u0012\u0012\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u0012\u0014\u0010\u0007\u001a\u0010\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\t\u0018\u00010\b¢\u0006\u0002\u0010\u0013B-\b\u0012\u0012\u0006\u0010\u0014\u001a\u00020\u0015\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0014\u0010\u0007\u001a\u0010\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\t\u0018\u00010\b¢\u0006\u0002\u0010\u0016B-\b\u0012\u0012\u0006\u0010\u0017\u001a\u00020\u0018\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0014\u0010\u0007\u001a\u0010\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\t\u0018\u00010\b¢\u0006\u0002\u0010\u0019B/\b\u0012\u0012\u0006\u0010\u001a\u001a\u00020\u001b\u0012\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u0012\u0014\u0010\u0007\u001a\u0010\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\t\u0018\u00010\b¢\u0006\u0002\u0010\u001cB\u0001\b\u0000\u0012\u0006\u0010\u001d\u001a\u00020\u001e\u0012\n\b\u0002\u0010\u0003\u001a\u0004\u0018\u00010\u0004\u0012\n\b\u0002\u0010\u000b\u001a\u0004\u0018\u00010\f\u0012\n\b\u0002\u0010\u000e\u001a\u0004\u0018\u00010\u000f\u0012\n\b\u0002\u0010\u0011\u001a\u0004\u0018\u00010\u0012\u0012\n\b\u0002\u0010\u0014\u001a\u0004\u0018\u00010\u0015\u0012\n\b\u0002\u0010\u0017\u001a\u0004\u0018\u00010\u0018\u0012\n\b\u0002\u0010\u001a\u001a\u0004\u0018\u00010\u001b\u0012\n\b\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u0012\u0016\b\u0002\u0010\u0007\u001a\u0010\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\t\u0018\u00010\b\u0012\u000e\b\u0002\u0010\u001f\u001a\b\u0012\u0004\u0012\u00020\t0 ¢\u0006\u0002\u0010!J\u000e\u0010.\u001a\u00020\u001eHÀ\u0003¢\u0006\u0002\b/J\u0017\u00100\u001a\u0010\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\t\u0018\u00010\bHÂ\u0003J\u000f\u00101\u001a\b\u0012\u0004\u0012\u00020\t0 HÂ\u0003J\u000b\u00102\u001a\u0004\u0018\u00010\u0004HÂ\u0003J\u000b\u00103\u001a\u0004\u0018\u00010\fHÂ\u0003J\u000b\u00104\u001a\u0004\u0018\u00010\u000fHÂ\u0003J\u000b\u00105\u001a\u0004\u0018\u00010\u0012HÂ\u0003J\u000b\u00106\u001a\u0004\u0018\u00010\u0015HÂ\u0003J\u000b\u00107\u001a\u0004\u0018\u00010\u0018HÂ\u0003J\u000b\u00108\u001a\u0004\u0018\u00010\u001bHÂ\u0003J\u000b\u00109\u001a\u0004\u0018\u00010\u0006HÂ\u0003J\u0001\u0010:\u001a\u00020\u00002\b\b\u0002\u0010\u001d\u001a\u00020\u001e2\n\b\u0002\u0010\u0003\u001a\u0004\u0018\u00010\u00042\n\b\u0002\u0010\u000b\u001a\u0004\u0018\u00010\f2\n\b\u0002\u0010\u000e\u001a\u0004\u0018\u00010\u000f2\n\b\u0002\u0010\u0011\u001a\u0004\u0018\u00010\u00122\n\b\u0002\u0010\u0014\u001a\u0004\u0018\u00010\u00152\n\b\u0002\u0010\u0017\u001a\u0004\u0018\u00010\u00182\n\b\u0002\u0010\u001a\u001a\u0004\u0018\u00010\u001b2\n\b\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u00062\u0016\b\u0002\u0010\u0007\u001a\u0010\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\t\u0018\u00010\b2\u000e\b\u0002\u0010\u001f\u001a\b\u0012\u0004\u0012\u00020\t0 HÆ\u0001J\t\u0010;\u001a\u00020<HÖ\u0001J\u0013\u0010=\u001a\u00020>2\b\u0010?\u001a\u0004\u0018\u00010+HÖ\u0003J\t\u0010@\u001a\u00020<HÖ\u0001J\u0014\u0010A\u001a\u000e\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020+0\bH\u0016J\t\u0010B\u001a\u00020\tHÖ\u0001J\u0019\u0010C\u001a\u00020D2\u0006\u0010E\u001a\u00020F2\u0006\u0010G\u001a\u00020<HÖ\u0001R\u001c\u0010\"\u001a\n\u0012\u0004\u0012\u00020\t\u0018\u00010 8@X\u0004¢\u0006\u0006\u001a\u0004\b#\u0010$R\u0010\u0010\u0014\u001a\u0004\u0018\u00010\u0015X\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u0017\u001a\u0004\u0018\u00010\u0018X\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u0005\u001a\u0004\u0018\u00010\u0006X\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u000e\u001a\u0004\u0018\u00010\u000fX\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u000b\u001a\u0004\u0018\u00010\fX\u0004¢\u0006\u0002\n\u0000R\u001c\u0010\u0007\u001a\u0010\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\t\u0018\u00010\bX\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\u001f\u001a\b\u0012\u0004\u0012\u00020\t0 X\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u0011\u001a\u0004\u0018\u00010\u0012X\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u001a\u001a\u0004\u0018\u00010\u001bX\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\u001d\u001a\u00020\u001eX\u0004¢\u0006\b\n\u0000\u001a\u0004\b%\u0010&R\u0011\u0010'\u001a\u00020\t8F¢\u0006\u0006\u001a\u0004\b(\u0010)R \u0010*\u001a\u000e\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020+0\b8BX\u0004¢\u0006\u0006\u001a\u0004\b,\u0010-¨\u0006Q"}, d2 = {"Lcom/stripe/android/model/PaymentMethodCreateParams;", "Lcom/stripe/android/model/StripeParamsModel;", "Landroid/os/Parcelable;", "card", "Lcom/stripe/android/model/PaymentMethodCreateParams$Card;", "billingDetails", "Lcom/stripe/android/model/PaymentMethod$BillingDetails;", "metadata", "", "", "(Lcom/stripe/android/model/PaymentMethodCreateParams$Card;Lcom/stripe/android/model/PaymentMethod$BillingDetails;Ljava/util/Map;)V", "ideal", "Lcom/stripe/android/model/PaymentMethodCreateParams$Ideal;", "(Lcom/stripe/android/model/PaymentMethodCreateParams$Ideal;Lcom/stripe/android/model/PaymentMethod$BillingDetails;Ljava/util/Map;)V", "fpx", "Lcom/stripe/android/model/PaymentMethodCreateParams$Fpx;", "(Lcom/stripe/android/model/PaymentMethodCreateParams$Fpx;Lcom/stripe/android/model/PaymentMethod$BillingDetails;Ljava/util/Map;)V", "sepaDebit", "Lcom/stripe/android/model/PaymentMethodCreateParams$SepaDebit;", "(Lcom/stripe/android/model/PaymentMethodCreateParams$SepaDebit;Lcom/stripe/android/model/PaymentMethod$BillingDetails;Ljava/util/Map;)V", "auBecsDebit", "Lcom/stripe/android/model/PaymentMethodCreateParams$AuBecsDebit;", "(Lcom/stripe/android/model/PaymentMethodCreateParams$AuBecsDebit;Lcom/stripe/android/model/PaymentMethod$BillingDetails;Ljava/util/Map;)V", "bacsDebit", "Lcom/stripe/android/model/PaymentMethodCreateParams$BacsDebit;", "(Lcom/stripe/android/model/PaymentMethodCreateParams$BacsDebit;Lcom/stripe/android/model/PaymentMethod$BillingDetails;Ljava/util/Map;)V", "sofort", "Lcom/stripe/android/model/PaymentMethodCreateParams$Sofort;", "(Lcom/stripe/android/model/PaymentMethodCreateParams$Sofort;Lcom/stripe/android/model/PaymentMethod$BillingDetails;Ljava/util/Map;)V", "type", "Lcom/stripe/android/model/PaymentMethodCreateParams$Type;", "productUsage", "", "(Lcom/stripe/android/model/PaymentMethodCreateParams$Type;Lcom/stripe/android/model/PaymentMethodCreateParams$Card;Lcom/stripe/android/model/PaymentMethodCreateParams$Ideal;Lcom/stripe/android/model/PaymentMethodCreateParams$Fpx;Lcom/stripe/android/model/PaymentMethodCreateParams$SepaDebit;Lcom/stripe/android/model/PaymentMethodCreateParams$AuBecsDebit;Lcom/stripe/android/model/PaymentMethodCreateParams$BacsDebit;Lcom/stripe/android/model/PaymentMethodCreateParams$Sofort;Lcom/stripe/android/model/PaymentMethod$BillingDetails;Ljava/util/Map;Ljava/util/Set;)V", "attribution", "getAttribution$stripe_release", "()Ljava/util/Set;", "getType$stripe_release", "()Lcom/stripe/android/model/PaymentMethodCreateParams$Type;", "typeCode", "getTypeCode", "()Ljava/lang/String;", "typeParams", "", "getTypeParams", "()Ljava/util/Map;", "component1", "component1$stripe_release", "component10", "component11", "component2", "component3", "component4", "component5", "component6", "component7", "component8", "component9", "copy", "describeContents", "", "equals", "", "other", "hashCode", "toParamMap", "toString", "writeToParcel", "", "parcel", "Landroid/os/Parcel;", "flags", "AuBecsDebit", "BacsDebit", "Card", "Companion", "Fpx", "Ideal", "SepaDebit", "Sofort", "Type", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: PaymentMethodCreateParams.kt */
public final class PaymentMethodCreateParams implements StripeParamsModel, Parcelable {
    public static final Parcelable.Creator CREATOR = new Creator();
    public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
    private static final String PARAM_BILLING_DETAILS = "billing_details";
    private static final String PARAM_METADATA = "metadata";
    private static final String PARAM_TYPE = "type";
    private final AuBecsDebit auBecsDebit;
    private final BacsDebit bacsDebit;
    private final PaymentMethod.BillingDetails billingDetails;
    private final Card card;
    private final Fpx fpx;
    private final Ideal ideal;
    private final Map<String, String> metadata;
    private final Set<String> productUsage;
    private final SepaDebit sepaDebit;
    private final Sofort sofort;
    private final Type type;

    @Metadata(bv = {1, 0, 3}, k = 3, mv = {1, 1, 16})
    public static class Creator implements Parcelable.Creator {
        public final Object createFromParcel(Parcel parcel) {
            Intrinsics.checkParameterIsNotNull(parcel, "in");
            Type type = (Type) Enum.valueOf(Type.class, parcel.readString());
            LinkedHashMap linkedHashMap = null;
            Card card = parcel.readInt() != 0 ? (Card) Card.CREATOR.createFromParcel(parcel) : null;
            Ideal ideal = parcel.readInt() != 0 ? (Ideal) Ideal.CREATOR.createFromParcel(parcel) : null;
            Fpx fpx = parcel.readInt() != 0 ? (Fpx) Fpx.CREATOR.createFromParcel(parcel) : null;
            SepaDebit sepaDebit = parcel.readInt() != 0 ? (SepaDebit) SepaDebit.CREATOR.createFromParcel(parcel) : null;
            AuBecsDebit auBecsDebit = parcel.readInt() != 0 ? (AuBecsDebit) AuBecsDebit.CREATOR.createFromParcel(parcel) : null;
            BacsDebit bacsDebit = parcel.readInt() != 0 ? (BacsDebit) BacsDebit.CREATOR.createFromParcel(parcel) : null;
            Sofort sofort = parcel.readInt() != 0 ? (Sofort) Sofort.CREATOR.createFromParcel(parcel) : null;
            PaymentMethod.BillingDetails billingDetails = parcel.readInt() != 0 ? (PaymentMethod.BillingDetails) PaymentMethod.BillingDetails.CREATOR.createFromParcel(parcel) : null;
            if (parcel.readInt() != 0) {
                int readInt = parcel.readInt();
                linkedHashMap = new LinkedHashMap(readInt);
                while (readInt != 0) {
                    linkedHashMap.put(parcel.readString(), parcel.readString());
                    readInt--;
                }
            }
            LinkedHashMap linkedHashMap2 = linkedHashMap;
            int readInt2 = parcel.readInt();
            LinkedHashSet linkedHashSet = new LinkedHashSet(readInt2);
            while (readInt2 != 0) {
                linkedHashSet.add(parcel.readString());
                readInt2--;
            }
            return new PaymentMethodCreateParams(type, card, ideal, fpx, sepaDebit, auBecsDebit, bacsDebit, sofort, billingDetails, linkedHashMap2, linkedHashSet);
        }

        public final Object[] newArray(int i) {
            return new PaymentMethodCreateParams[i];
        }
    }

    @Metadata(bv = {1, 0, 3}, k = 3, mv = {1, 1, 16})
    public final /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;
        public static final /* synthetic */ int[] $EnumSwitchMapping$1;

        static {
            int[] iArr = new int[Type.values().length];
            $EnumSwitchMapping$0 = iArr;
            iArr[Type.Card.ordinal()] = 1;
            int[] iArr2 = new int[Type.values().length];
            $EnumSwitchMapping$1 = iArr2;
            iArr2[Type.Card.ordinal()] = 1;
            $EnumSwitchMapping$1[Type.Ideal.ordinal()] = 2;
            $EnumSwitchMapping$1[Type.Fpx.ordinal()] = 3;
            $EnumSwitchMapping$1[Type.SepaDebit.ordinal()] = 4;
            $EnumSwitchMapping$1[Type.AuBecsDebit.ordinal()] = 5;
            $EnumSwitchMapping$1[Type.BacsDebit.ordinal()] = 6;
            $EnumSwitchMapping$1[Type.Sofort.ordinal()] = 7;
        }
    }

    private final Map<String, String> component10() {
        return this.metadata;
    }

    private final Set<String> component11() {
        return this.productUsage;
    }

    private final Card component2() {
        return this.card;
    }

    private final Ideal component3() {
        return this.ideal;
    }

    private final Fpx component4() {
        return this.fpx;
    }

    private final SepaDebit component5() {
        return this.sepaDebit;
    }

    private final AuBecsDebit component6() {
        return this.auBecsDebit;
    }

    private final BacsDebit component7() {
        return this.bacsDebit;
    }

    private final Sofort component8() {
        return this.sofort;
    }

    private final PaymentMethod.BillingDetails component9() {
        return this.billingDetails;
    }

    public static /* synthetic */ PaymentMethodCreateParams copy$default(PaymentMethodCreateParams paymentMethodCreateParams, Type type2, Card card2, Ideal ideal2, Fpx fpx2, SepaDebit sepaDebit2, AuBecsDebit auBecsDebit2, BacsDebit bacsDebit2, Sofort sofort2, PaymentMethod.BillingDetails billingDetails2, Map map, Set set, int i, Object obj) {
        PaymentMethodCreateParams paymentMethodCreateParams2 = paymentMethodCreateParams;
        int i2 = i;
        return paymentMethodCreateParams.copy((i2 & 1) != 0 ? paymentMethodCreateParams2.type : type2, (i2 & 2) != 0 ? paymentMethodCreateParams2.card : card2, (i2 & 4) != 0 ? paymentMethodCreateParams2.ideal : ideal2, (i2 & 8) != 0 ? paymentMethodCreateParams2.fpx : fpx2, (i2 & 16) != 0 ? paymentMethodCreateParams2.sepaDebit : sepaDebit2, (i2 & 32) != 0 ? paymentMethodCreateParams2.auBecsDebit : auBecsDebit2, (i2 & 64) != 0 ? paymentMethodCreateParams2.bacsDebit : bacsDebit2, (i2 & 128) != 0 ? paymentMethodCreateParams2.sofort : sofort2, (i2 & 256) != 0 ? paymentMethodCreateParams2.billingDetails : billingDetails2, (i2 & 512) != 0 ? paymentMethodCreateParams2.metadata : map, (i2 & 1024) != 0 ? paymentMethodCreateParams2.productUsage : set);
    }

    @JvmStatic
    public static final PaymentMethodCreateParams create(AuBecsDebit auBecsDebit2, PaymentMethod.BillingDetails billingDetails2) {
        return Companion.create$default(Companion, auBecsDebit2, billingDetails2, (Map) null, 4, (Object) null);
    }

    @JvmStatic
    public static final PaymentMethodCreateParams create(AuBecsDebit auBecsDebit2, PaymentMethod.BillingDetails billingDetails2, Map<String, String> map) {
        return Companion.create(auBecsDebit2, billingDetails2, map);
    }

    @JvmStatic
    public static final PaymentMethodCreateParams create(BacsDebit bacsDebit2, PaymentMethod.BillingDetails billingDetails2) {
        return Companion.create$default(Companion, bacsDebit2, billingDetails2, (Map) null, 4, (Object) null);
    }

    @JvmStatic
    public static final PaymentMethodCreateParams create(BacsDebit bacsDebit2, PaymentMethod.BillingDetails billingDetails2, Map<String, String> map) {
        return Companion.create(bacsDebit2, billingDetails2, map);
    }

    @JvmStatic
    public static final PaymentMethodCreateParams create(Card card2) {
        return Companion.create$default(Companion, card2, (PaymentMethod.BillingDetails) null, (Map) null, 6, (Object) null);
    }

    @JvmStatic
    public static final PaymentMethodCreateParams create(Card card2, PaymentMethod.BillingDetails billingDetails2) {
        return Companion.create$default(Companion, card2, billingDetails2, (Map) null, 4, (Object) null);
    }

    @JvmStatic
    public static final PaymentMethodCreateParams create(Card card2, PaymentMethod.BillingDetails billingDetails2, Map<String, String> map) {
        return Companion.create(card2, billingDetails2, map);
    }

    @JvmStatic
    public static final PaymentMethodCreateParams create(Fpx fpx2) {
        return Companion.create$default(Companion, fpx2, (PaymentMethod.BillingDetails) null, (Map) null, 6, (Object) null);
    }

    @JvmStatic
    public static final PaymentMethodCreateParams create(Fpx fpx2, PaymentMethod.BillingDetails billingDetails2) {
        return Companion.create$default(Companion, fpx2, billingDetails2, (Map) null, 4, (Object) null);
    }

    @JvmStatic
    public static final PaymentMethodCreateParams create(Fpx fpx2, PaymentMethod.BillingDetails billingDetails2, Map<String, String> map) {
        return Companion.create(fpx2, billingDetails2, map);
    }

    @JvmStatic
    public static final PaymentMethodCreateParams create(Ideal ideal2) {
        return Companion.create$default(Companion, ideal2, (PaymentMethod.BillingDetails) null, (Map) null, 6, (Object) null);
    }

    @JvmStatic
    public static final PaymentMethodCreateParams create(Ideal ideal2, PaymentMethod.BillingDetails billingDetails2) {
        return Companion.create$default(Companion, ideal2, billingDetails2, (Map) null, 4, (Object) null);
    }

    @JvmStatic
    public static final PaymentMethodCreateParams create(Ideal ideal2, PaymentMethod.BillingDetails billingDetails2, Map<String, String> map) {
        return Companion.create(ideal2, billingDetails2, map);
    }

    @JvmStatic
    public static final PaymentMethodCreateParams create(SepaDebit sepaDebit2) {
        return Companion.create$default(Companion, sepaDebit2, (PaymentMethod.BillingDetails) null, (Map) null, 6, (Object) null);
    }

    @JvmStatic
    public static final PaymentMethodCreateParams create(SepaDebit sepaDebit2, PaymentMethod.BillingDetails billingDetails2) {
        return Companion.create$default(Companion, sepaDebit2, billingDetails2, (Map) null, 4, (Object) null);
    }

    @JvmStatic
    public static final PaymentMethodCreateParams create(SepaDebit sepaDebit2, PaymentMethod.BillingDetails billingDetails2, Map<String, String> map) {
        return Companion.create(sepaDebit2, billingDetails2, map);
    }

    @JvmStatic
    public static final PaymentMethodCreateParams create(Sofort sofort2) {
        return Companion.create$default(Companion, sofort2, (PaymentMethod.BillingDetails) null, (Map) null, 6, (Object) null);
    }

    @JvmStatic
    public static final PaymentMethodCreateParams create(Sofort sofort2, PaymentMethod.BillingDetails billingDetails2) {
        return Companion.create$default(Companion, sofort2, billingDetails2, (Map) null, 4, (Object) null);
    }

    @JvmStatic
    public static final PaymentMethodCreateParams create(Sofort sofort2, PaymentMethod.BillingDetails billingDetails2, Map<String, String> map) {
        return Companion.create(sofort2, billingDetails2, map);
    }

    @JvmStatic
    public static final PaymentMethodCreateParams createBancontact(PaymentMethod.BillingDetails billingDetails2) {
        return Companion.createBancontact$default(Companion, billingDetails2, (Map) null, 2, (Object) null);
    }

    @JvmStatic
    public static final PaymentMethodCreateParams createBancontact(PaymentMethod.BillingDetails billingDetails2, Map<String, String> map) {
        return Companion.createBancontact(billingDetails2, map);
    }

    @JvmStatic
    public static final PaymentMethodCreateParams createEps(PaymentMethod.BillingDetails billingDetails2) {
        return Companion.createEps$default(Companion, billingDetails2, (Map) null, 2, (Object) null);
    }

    @JvmStatic
    public static final PaymentMethodCreateParams createEps(PaymentMethod.BillingDetails billingDetails2, Map<String, String> map) {
        return Companion.createEps(billingDetails2, map);
    }

    @JvmStatic
    public static final PaymentMethodCreateParams createFromGooglePay(JSONObject jSONObject) throws JSONException {
        return Companion.createFromGooglePay(jSONObject);
    }

    @JvmStatic
    public static final PaymentMethodCreateParams createGiropay(PaymentMethod.BillingDetails billingDetails2) {
        return Companion.createGiropay$default(Companion, billingDetails2, (Map) null, 2, (Object) null);
    }

    @JvmStatic
    public static final PaymentMethodCreateParams createGiropay(PaymentMethod.BillingDetails billingDetails2, Map<String, String> map) {
        return Companion.createGiropay(billingDetails2, map);
    }

    @JvmStatic
    public static final PaymentMethodCreateParams createP24(PaymentMethod.BillingDetails billingDetails2) {
        return Companion.createP24$default(Companion, billingDetails2, (Map) null, 2, (Object) null);
    }

    @JvmStatic
    public static final PaymentMethodCreateParams createP24(PaymentMethod.BillingDetails billingDetails2, Map<String, String> map) {
        return Companion.createP24(billingDetails2, map);
    }

    public final Type component1$stripe_release() {
        return this.type;
    }

    public final PaymentMethodCreateParams copy(Type type2, Card card2, Ideal ideal2, Fpx fpx2, SepaDebit sepaDebit2, AuBecsDebit auBecsDebit2, BacsDebit bacsDebit2, Sofort sofort2, PaymentMethod.BillingDetails billingDetails2, Map<String, String> map, Set<String> set) {
        Intrinsics.checkParameterIsNotNull(type2, "type");
        Set<String> set2 = set;
        Intrinsics.checkParameterIsNotNull(set2, "productUsage");
        return new PaymentMethodCreateParams(type2, card2, ideal2, fpx2, sepaDebit2, auBecsDebit2, bacsDebit2, sofort2, billingDetails2, map, set2);
    }

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof PaymentMethodCreateParams)) {
            return false;
        }
        PaymentMethodCreateParams paymentMethodCreateParams = (PaymentMethodCreateParams) obj;
        return Intrinsics.areEqual((Object) this.type, (Object) paymentMethodCreateParams.type) && Intrinsics.areEqual((Object) this.card, (Object) paymentMethodCreateParams.card) && Intrinsics.areEqual((Object) this.ideal, (Object) paymentMethodCreateParams.ideal) && Intrinsics.areEqual((Object) this.fpx, (Object) paymentMethodCreateParams.fpx) && Intrinsics.areEqual((Object) this.sepaDebit, (Object) paymentMethodCreateParams.sepaDebit) && Intrinsics.areEqual((Object) this.auBecsDebit, (Object) paymentMethodCreateParams.auBecsDebit) && Intrinsics.areEqual((Object) this.bacsDebit, (Object) paymentMethodCreateParams.bacsDebit) && Intrinsics.areEqual((Object) this.sofort, (Object) paymentMethodCreateParams.sofort) && Intrinsics.areEqual((Object) this.billingDetails, (Object) paymentMethodCreateParams.billingDetails) && Intrinsics.areEqual((Object) this.metadata, (Object) paymentMethodCreateParams.metadata) && Intrinsics.areEqual((Object) this.productUsage, (Object) paymentMethodCreateParams.productUsage);
    }

    public int hashCode() {
        Type type2 = this.type;
        int i = 0;
        int hashCode = (type2 != null ? type2.hashCode() : 0) * 31;
        Card card2 = this.card;
        int hashCode2 = (hashCode + (card2 != null ? card2.hashCode() : 0)) * 31;
        Ideal ideal2 = this.ideal;
        int hashCode3 = (hashCode2 + (ideal2 != null ? ideal2.hashCode() : 0)) * 31;
        Fpx fpx2 = this.fpx;
        int hashCode4 = (hashCode3 + (fpx2 != null ? fpx2.hashCode() : 0)) * 31;
        SepaDebit sepaDebit2 = this.sepaDebit;
        int hashCode5 = (hashCode4 + (sepaDebit2 != null ? sepaDebit2.hashCode() : 0)) * 31;
        AuBecsDebit auBecsDebit2 = this.auBecsDebit;
        int hashCode6 = (hashCode5 + (auBecsDebit2 != null ? auBecsDebit2.hashCode() : 0)) * 31;
        BacsDebit bacsDebit2 = this.bacsDebit;
        int hashCode7 = (hashCode6 + (bacsDebit2 != null ? bacsDebit2.hashCode() : 0)) * 31;
        Sofort sofort2 = this.sofort;
        int hashCode8 = (hashCode7 + (sofort2 != null ? sofort2.hashCode() : 0)) * 31;
        PaymentMethod.BillingDetails billingDetails2 = this.billingDetails;
        int hashCode9 = (hashCode8 + (billingDetails2 != null ? billingDetails2.hashCode() : 0)) * 31;
        Map<String, String> map = this.metadata;
        int hashCode10 = (hashCode9 + (map != null ? map.hashCode() : 0)) * 31;
        Set<String> set = this.productUsage;
        if (set != null) {
            i = set.hashCode();
        }
        return hashCode10 + i;
    }

    public String toString() {
        return "PaymentMethodCreateParams(type=" + this.type + ", card=" + this.card + ", ideal=" + this.ideal + ", fpx=" + this.fpx + ", sepaDebit=" + this.sepaDebit + ", auBecsDebit=" + this.auBecsDebit + ", bacsDebit=" + this.bacsDebit + ", sofort=" + this.sofort + ", billingDetails=" + this.billingDetails + ", metadata=" + this.metadata + ", productUsage=" + this.productUsage + ")";
    }

    public void writeToParcel(Parcel parcel, int i) {
        Intrinsics.checkParameterIsNotNull(parcel, "parcel");
        parcel.writeString(this.type.name());
        Card card2 = this.card;
        if (card2 != null) {
            parcel.writeInt(1);
            card2.writeToParcel(parcel, 0);
        } else {
            parcel.writeInt(0);
        }
        Ideal ideal2 = this.ideal;
        if (ideal2 != null) {
            parcel.writeInt(1);
            ideal2.writeToParcel(parcel, 0);
        } else {
            parcel.writeInt(0);
        }
        Fpx fpx2 = this.fpx;
        if (fpx2 != null) {
            parcel.writeInt(1);
            fpx2.writeToParcel(parcel, 0);
        } else {
            parcel.writeInt(0);
        }
        SepaDebit sepaDebit2 = this.sepaDebit;
        if (sepaDebit2 != null) {
            parcel.writeInt(1);
            sepaDebit2.writeToParcel(parcel, 0);
        } else {
            parcel.writeInt(0);
        }
        AuBecsDebit auBecsDebit2 = this.auBecsDebit;
        if (auBecsDebit2 != null) {
            parcel.writeInt(1);
            auBecsDebit2.writeToParcel(parcel, 0);
        } else {
            parcel.writeInt(0);
        }
        BacsDebit bacsDebit2 = this.bacsDebit;
        if (bacsDebit2 != null) {
            parcel.writeInt(1);
            bacsDebit2.writeToParcel(parcel, 0);
        } else {
            parcel.writeInt(0);
        }
        Sofort sofort2 = this.sofort;
        if (sofort2 != null) {
            parcel.writeInt(1);
            sofort2.writeToParcel(parcel, 0);
        } else {
            parcel.writeInt(0);
        }
        PaymentMethod.BillingDetails billingDetails2 = this.billingDetails;
        if (billingDetails2 != null) {
            parcel.writeInt(1);
            billingDetails2.writeToParcel(parcel, 0);
        } else {
            parcel.writeInt(0);
        }
        Map<String, String> map = this.metadata;
        if (map != null) {
            parcel.writeInt(1);
            parcel.writeInt(map.size());
            for (Map.Entry<String, String> next : map.entrySet()) {
                parcel.writeString(next.getKey());
                parcel.writeString(next.getValue());
            }
        } else {
            parcel.writeInt(0);
        }
        Set<String> set = this.productUsage;
        parcel.writeInt(set.size());
        for (String writeString : set) {
            parcel.writeString(writeString);
        }
    }

    public /* synthetic */ PaymentMethodCreateParams(AuBecsDebit auBecsDebit2, PaymentMethod.BillingDetails billingDetails2, Map map, DefaultConstructorMarker defaultConstructorMarker) {
        this(auBecsDebit2, billingDetails2, (Map<String, String>) map);
    }

    public /* synthetic */ PaymentMethodCreateParams(BacsDebit bacsDebit2, PaymentMethod.BillingDetails billingDetails2, Map map, DefaultConstructorMarker defaultConstructorMarker) {
        this(bacsDebit2, billingDetails2, (Map<String, String>) map);
    }

    public /* synthetic */ PaymentMethodCreateParams(Card card2, PaymentMethod.BillingDetails billingDetails2, Map map, DefaultConstructorMarker defaultConstructorMarker) {
        this(card2, billingDetails2, (Map<String, String>) map);
    }

    public /* synthetic */ PaymentMethodCreateParams(Fpx fpx2, PaymentMethod.BillingDetails billingDetails2, Map map, DefaultConstructorMarker defaultConstructorMarker) {
        this(fpx2, billingDetails2, (Map<String, String>) map);
    }

    public /* synthetic */ PaymentMethodCreateParams(Ideal ideal2, PaymentMethod.BillingDetails billingDetails2, Map map, DefaultConstructorMarker defaultConstructorMarker) {
        this(ideal2, billingDetails2, (Map<String, String>) map);
    }

    public /* synthetic */ PaymentMethodCreateParams(SepaDebit sepaDebit2, PaymentMethod.BillingDetails billingDetails2, Map map, DefaultConstructorMarker defaultConstructorMarker) {
        this(sepaDebit2, billingDetails2, (Map<String, String>) map);
    }

    public /* synthetic */ PaymentMethodCreateParams(Sofort sofort2, PaymentMethod.BillingDetails billingDetails2, Map map, DefaultConstructorMarker defaultConstructorMarker) {
        this(sofort2, billingDetails2, (Map<String, String>) map);
    }

    public PaymentMethodCreateParams(Type type2, Card card2, Ideal ideal2, Fpx fpx2, SepaDebit sepaDebit2, AuBecsDebit auBecsDebit2, BacsDebit bacsDebit2, Sofort sofort2, PaymentMethod.BillingDetails billingDetails2, Map<String, String> map, Set<String> set) {
        Intrinsics.checkParameterIsNotNull(type2, "type");
        Intrinsics.checkParameterIsNotNull(set, "productUsage");
        this.type = type2;
        this.card = card2;
        this.ideal = ideal2;
        this.fpx = fpx2;
        this.sepaDebit = sepaDebit2;
        this.auBecsDebit = auBecsDebit2;
        this.bacsDebit = bacsDebit2;
        this.sofort = sofort2;
        this.billingDetails = billingDetails2;
        this.metadata = map;
        this.productUsage = set;
    }

    public final Type getType$stripe_release() {
        return this.type;
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* synthetic */ PaymentMethodCreateParams(com.stripe.android.model.PaymentMethodCreateParams.Type r12, com.stripe.android.model.PaymentMethodCreateParams.Card r13, com.stripe.android.model.PaymentMethodCreateParams.Ideal r14, com.stripe.android.model.PaymentMethodCreateParams.Fpx r15, com.stripe.android.model.PaymentMethodCreateParams.SepaDebit r16, com.stripe.android.model.PaymentMethodCreateParams.AuBecsDebit r17, com.stripe.android.model.PaymentMethodCreateParams.BacsDebit r18, com.stripe.android.model.PaymentMethodCreateParams.Sofort r19, com.stripe.android.model.PaymentMethod.BillingDetails r20, java.util.Map r21, java.util.Set r22, int r23, kotlin.jvm.internal.DefaultConstructorMarker r24) {
        /*
            r11 = this;
            r0 = r23
            r1 = r0 & 2
            r2 = 0
            if (r1 == 0) goto L_0x000b
            r1 = r2
            com.stripe.android.model.PaymentMethodCreateParams$Card r1 = (com.stripe.android.model.PaymentMethodCreateParams.Card) r1
            goto L_0x000c
        L_0x000b:
            r1 = r13
        L_0x000c:
            r3 = r0 & 4
            if (r3 == 0) goto L_0x0014
            r3 = r2
            com.stripe.android.model.PaymentMethodCreateParams$Ideal r3 = (com.stripe.android.model.PaymentMethodCreateParams.Ideal) r3
            goto L_0x0015
        L_0x0014:
            r3 = r14
        L_0x0015:
            r4 = r0 & 8
            if (r4 == 0) goto L_0x001d
            r4 = r2
            com.stripe.android.model.PaymentMethodCreateParams$Fpx r4 = (com.stripe.android.model.PaymentMethodCreateParams.Fpx) r4
            goto L_0x001e
        L_0x001d:
            r4 = r15
        L_0x001e:
            r5 = r0 & 16
            if (r5 == 0) goto L_0x0026
            r5 = r2
            com.stripe.android.model.PaymentMethodCreateParams$SepaDebit r5 = (com.stripe.android.model.PaymentMethodCreateParams.SepaDebit) r5
            goto L_0x0028
        L_0x0026:
            r5 = r16
        L_0x0028:
            r6 = r0 & 32
            if (r6 == 0) goto L_0x0030
            r6 = r2
            com.stripe.android.model.PaymentMethodCreateParams$AuBecsDebit r6 = (com.stripe.android.model.PaymentMethodCreateParams.AuBecsDebit) r6
            goto L_0x0032
        L_0x0030:
            r6 = r17
        L_0x0032:
            r7 = r0 & 64
            if (r7 == 0) goto L_0x003a
            r7 = r2
            com.stripe.android.model.PaymentMethodCreateParams$BacsDebit r7 = (com.stripe.android.model.PaymentMethodCreateParams.BacsDebit) r7
            goto L_0x003c
        L_0x003a:
            r7 = r18
        L_0x003c:
            r8 = r0 & 128(0x80, float:1.794E-43)
            if (r8 == 0) goto L_0x0044
            r8 = r2
            com.stripe.android.model.PaymentMethodCreateParams$Sofort r8 = (com.stripe.android.model.PaymentMethodCreateParams.Sofort) r8
            goto L_0x0046
        L_0x0044:
            r8 = r19
        L_0x0046:
            r9 = r0 & 256(0x100, float:3.59E-43)
            if (r9 == 0) goto L_0x004e
            r9 = r2
            com.stripe.android.model.PaymentMethod$BillingDetails r9 = (com.stripe.android.model.PaymentMethod.BillingDetails) r9
            goto L_0x0050
        L_0x004e:
            r9 = r20
        L_0x0050:
            r10 = r0 & 512(0x200, float:7.175E-43)
            if (r10 == 0) goto L_0x0057
            java.util.Map r2 = (java.util.Map) r2
            goto L_0x0059
        L_0x0057:
            r2 = r21
        L_0x0059:
            r0 = r0 & 1024(0x400, float:1.435E-42)
            if (r0 == 0) goto L_0x0062
            java.util.Set r0 = kotlin.collections.SetsKt.emptySet()
            goto L_0x0064
        L_0x0062:
            r0 = r22
        L_0x0064:
            r13 = r11
            r14 = r12
            r15 = r1
            r16 = r3
            r17 = r4
            r18 = r5
            r19 = r6
            r20 = r7
            r21 = r8
            r22 = r9
            r23 = r2
            r24 = r0
            r13.<init>(r14, r15, r16, r17, r18, r19, r20, r21, r22, r23, r24)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.stripe.android.model.PaymentMethodCreateParams.<init>(com.stripe.android.model.PaymentMethodCreateParams$Type, com.stripe.android.model.PaymentMethodCreateParams$Card, com.stripe.android.model.PaymentMethodCreateParams$Ideal, com.stripe.android.model.PaymentMethodCreateParams$Fpx, com.stripe.android.model.PaymentMethodCreateParams$SepaDebit, com.stripe.android.model.PaymentMethodCreateParams$AuBecsDebit, com.stripe.android.model.PaymentMethodCreateParams$BacsDebit, com.stripe.android.model.PaymentMethodCreateParams$Sofort, com.stripe.android.model.PaymentMethod$BillingDetails, java.util.Map, java.util.Set, int, kotlin.jvm.internal.DefaultConstructorMarker):void");
    }

    public final String getTypeCode() {
        return this.type.getCode$stripe_release();
    }

    public final /* synthetic */ Set<String> getAttribution$stripe_release() {
        Set<String> attribution$stripe_release;
        if (WhenMappings.$EnumSwitchMapping$0[this.type.ordinal()] != 1) {
            Set<String> set = this.productUsage;
            if (true ^ set.isEmpty()) {
                return set;
            }
            return null;
        }
        Card card2 = this.card;
        if (card2 == null || (attribution$stripe_release = card2.getAttribution$stripe_release()) == null) {
            return null;
        }
        return SetsKt.plus(attribution$stripe_release, this.productUsage);
    }

    private PaymentMethodCreateParams(Card card2, PaymentMethod.BillingDetails billingDetails2, Map<String, String> map) {
        this(Type.Card, card2, (Ideal) null, (Fpx) null, (SepaDebit) null, (AuBecsDebit) null, (BacsDebit) null, (Sofort) null, billingDetails2, map, (Set) null, 1276, (DefaultConstructorMarker) null);
    }

    private PaymentMethodCreateParams(Ideal ideal2, PaymentMethod.BillingDetails billingDetails2, Map<String, String> map) {
        this(Type.Ideal, (Card) null, ideal2, (Fpx) null, (SepaDebit) null, (AuBecsDebit) null, (BacsDebit) null, (Sofort) null, billingDetails2, map, (Set) null, 1274, (DefaultConstructorMarker) null);
    }

    private PaymentMethodCreateParams(Fpx fpx2, PaymentMethod.BillingDetails billingDetails2, Map<String, String> map) {
        this(Type.Fpx, (Card) null, (Ideal) null, fpx2, (SepaDebit) null, (AuBecsDebit) null, (BacsDebit) null, (Sofort) null, billingDetails2, map, (Set) null, 1270, (DefaultConstructorMarker) null);
    }

    private PaymentMethodCreateParams(SepaDebit sepaDebit2, PaymentMethod.BillingDetails billingDetails2, Map<String, String> map) {
        this(Type.SepaDebit, (Card) null, (Ideal) null, (Fpx) null, sepaDebit2, (AuBecsDebit) null, (BacsDebit) null, (Sofort) null, billingDetails2, map, (Set) null, 1262, (DefaultConstructorMarker) null);
    }

    private PaymentMethodCreateParams(AuBecsDebit auBecsDebit2, PaymentMethod.BillingDetails billingDetails2, Map<String, String> map) {
        this(Type.AuBecsDebit, (Card) null, (Ideal) null, (Fpx) null, (SepaDebit) null, auBecsDebit2, (BacsDebit) null, (Sofort) null, billingDetails2, map, (Set) null, 1246, (DefaultConstructorMarker) null);
    }

    private PaymentMethodCreateParams(BacsDebit bacsDebit2, PaymentMethod.BillingDetails billingDetails2, Map<String, String> map) {
        this(Type.BacsDebit, (Card) null, (Ideal) null, (Fpx) null, (SepaDebit) null, (AuBecsDebit) null, bacsDebit2, (Sofort) null, billingDetails2, map, (Set) null, 1214, (DefaultConstructorMarker) null);
    }

    private PaymentMethodCreateParams(Sofort sofort2, PaymentMethod.BillingDetails billingDetails2, Map<String, String> map) {
        this(Type.Sofort, (Card) null, (Ideal) null, (Fpx) null, (SepaDebit) null, (AuBecsDebit) null, (BacsDebit) null, sofort2, billingDetails2, map, (Set) null, 1150, (DefaultConstructorMarker) null);
    }

    public Map<String, Object> toParamMap() {
        Map mapOf = MapsKt.mapOf(TuplesKt.to("type", this.type.getCode$stripe_release()));
        PaymentMethod.BillingDetails billingDetails2 = this.billingDetails;
        Map<K, V> map = null;
        Map<K, V> mapOf2 = billingDetails2 != null ? MapsKt.mapOf(TuplesKt.to(PARAM_BILLING_DETAILS, billingDetails2.toParamMap())) : null;
        if (mapOf2 == null) {
            mapOf2 = MapsKt.emptyMap();
        }
        Map<K, V> plus = MapsKt.plus(MapsKt.plus(mapOf, (Map) mapOf2), (Map<K, V>) getTypeParams());
        Map<String, String> map2 = this.metadata;
        if (map2 != null) {
            map = MapsKt.mapOf(TuplesKt.to(PARAM_METADATA, map2));
        }
        if (map == null) {
            map = MapsKt.emptyMap();
        }
        return MapsKt.plus(plus, map);
    }

    /* JADX WARNING: Can't fix incorrect switch cases order */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final java.util.Map<java.lang.String, java.lang.Object> getTypeParams() {
        /*
            r3 = this;
            com.stripe.android.model.PaymentMethodCreateParams$Type r0 = r3.type
            int[] r1 = com.stripe.android.model.PaymentMethodCreateParams.WhenMappings.$EnumSwitchMapping$1
            int r0 = r0.ordinal()
            r0 = r1[r0]
            r1 = 0
            switch(r0) {
                case 1: goto L_0x0046;
                case 2: goto L_0x003d;
                case 3: goto L_0x0034;
                case 4: goto L_0x002b;
                case 5: goto L_0x0022;
                case 6: goto L_0x0019;
                case 7: goto L_0x0010;
                default: goto L_0x000e;
            }
        L_0x000e:
            r0 = r1
            goto L_0x004e
        L_0x0010:
            com.stripe.android.model.PaymentMethodCreateParams$Sofort r0 = r3.sofort
            if (r0 == 0) goto L_0x000e
            java.util.Map r0 = r0.toParamMap()
            goto L_0x004e
        L_0x0019:
            com.stripe.android.model.PaymentMethodCreateParams$BacsDebit r0 = r3.bacsDebit
            if (r0 == 0) goto L_0x000e
            java.util.Map r0 = r0.toParamMap()
            goto L_0x004e
        L_0x0022:
            com.stripe.android.model.PaymentMethodCreateParams$AuBecsDebit r0 = r3.auBecsDebit
            if (r0 == 0) goto L_0x000e
            java.util.Map r0 = r0.toParamMap()
            goto L_0x004e
        L_0x002b:
            com.stripe.android.model.PaymentMethodCreateParams$SepaDebit r0 = r3.sepaDebit
            if (r0 == 0) goto L_0x000e
            java.util.Map r0 = r0.toParamMap()
            goto L_0x004e
        L_0x0034:
            com.stripe.android.model.PaymentMethodCreateParams$Fpx r0 = r3.fpx
            if (r0 == 0) goto L_0x000e
            java.util.Map r0 = r0.toParamMap()
            goto L_0x004e
        L_0x003d:
            com.stripe.android.model.PaymentMethodCreateParams$Ideal r0 = r3.ideal
            if (r0 == 0) goto L_0x000e
            java.util.Map r0 = r0.toParamMap()
            goto L_0x004e
        L_0x0046:
            com.stripe.android.model.PaymentMethodCreateParams$Card r0 = r3.card
            if (r0 == 0) goto L_0x000e
            java.util.Map r0 = r0.toParamMap()
        L_0x004e:
            if (r0 == 0) goto L_0x0059
            boolean r2 = r0.isEmpty()
            if (r2 == 0) goto L_0x0057
            goto L_0x0059
        L_0x0057:
            r2 = 0
            goto L_0x005a
        L_0x0059:
            r2 = 1
        L_0x005a:
            if (r2 != 0) goto L_0x005d
            goto L_0x005e
        L_0x005d:
            r0 = r1
        L_0x005e:
            if (r0 == 0) goto L_0x006e
            com.stripe.android.model.PaymentMethodCreateParams$Type r1 = r3.type
            java.lang.String r1 = r1.getCode$stripe_release()
            kotlin.Pair r0 = kotlin.TuplesKt.to(r1, r0)
            java.util.Map r1 = kotlin.collections.MapsKt.mapOf(r0)
        L_0x006e:
            if (r1 == 0) goto L_0x0071
            goto L_0x0075
        L_0x0071:
            java.util.Map r1 = kotlin.collections.MapsKt.emptyMap()
        L_0x0075:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.stripe.android.model.PaymentMethodCreateParams.getTypeParams():java.util.Map");
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0013\b\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0019\b\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0002\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006R\u0014\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nj\u0002\b\u000bj\u0002\b\fj\u0002\b\rj\u0002\b\u000ej\u0002\b\u000fj\u0002\b\u0010j\u0002\b\u0011j\u0002\b\u0012j\u0002\b\u0013j\u0002\b\u0014j\u0002\b\u0015j\u0002\b\u0016j\u0002\b\u0017¨\u0006\u0018"}, d2 = {"Lcom/stripe/android/model/PaymentMethodCreateParams$Type;", "", "code", "", "hasMandate", "", "(Ljava/lang/String;ILjava/lang/String;Z)V", "getCode$stripe_release", "()Ljava/lang/String;", "getHasMandate", "()Z", "Card", "Ideal", "Fpx", "SepaDebit", "AuBecsDebit", "BacsDebit", "Sofort", "P24", "Bancontact", "Giropay", "Eps", "Oxxo", "Alipay", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: PaymentMethodCreateParams.kt */
    public enum Type {
        Card("card", false, 2, (boolean) null),
        Ideal("ideal", false, 2, (boolean) null),
        Fpx("fpx", false, 2, (boolean) null),
        SepaDebit("sepa_debit", true),
        AuBecsDebit("au_becs_debit", true),
        BacsDebit("bacs_debit", true),
        Sofort("sofort", false, 2, (boolean) null),
        P24("p24", false, 2, (boolean) null),
        Bancontact("bancontact", false, 2, (boolean) null),
        Giropay("giropay", false, 2, (boolean) null),
        Eps("eps", false, 2, (boolean) null),
        Oxxo("oxxo", false, 2, (boolean) null),
        Alipay("alipay", false, 2, (boolean) null);
        
        private final String code;
        private final boolean hasMandate;

        private Type(String str, boolean z) {
            this.code = str;
            this.hasMandate = z;
        }

        public final String getCode$stripe_release() {
            return this.code;
        }

        public final boolean getHasMandate() {
            return this.hasMandate;
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000H\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0010\"\n\u0002\b\u0010\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\b\b\u0018\u0000 )2\u00020\u00012\u00020\u0002:\u0002()BU\b\u0000\u0012\n\b\u0002\u0010\u0003\u001a\u0004\u0018\u00010\u0004\u0012\n\b\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u0012\n\b\u0002\u0010\u0007\u001a\u0004\u0018\u00010\u0006\u0012\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\u0004\u0012\n\b\u0002\u0010\t\u001a\u0004\u0018\u00010\u0004\u0012\u0010\b\u0002\u0010\n\u001a\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u000b¢\u0006\u0002\u0010\fJ\u000b\u0010\u0010\u001a\u0004\u0018\u00010\u0004HÂ\u0003J\u0010\u0010\u0011\u001a\u0004\u0018\u00010\u0006HÂ\u0003¢\u0006\u0002\u0010\u0012J\u0010\u0010\u0013\u001a\u0004\u0018\u00010\u0006HÂ\u0003¢\u0006\u0002\u0010\u0012J\u000b\u0010\u0014\u001a\u0004\u0018\u00010\u0004HÂ\u0003J\u000b\u0010\u0015\u001a\u0004\u0018\u00010\u0004HÂ\u0003J\u0016\u0010\u0016\u001a\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u000bHÀ\u0003¢\u0006\u0002\b\u0017J\\\u0010\u0018\u001a\u00020\u00002\n\b\u0002\u0010\u0003\u001a\u0004\u0018\u00010\u00042\n\b\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u00062\n\b\u0002\u0010\u0007\u001a\u0004\u0018\u00010\u00062\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\u00042\n\b\u0002\u0010\t\u001a\u0004\u0018\u00010\u00042\u0010\b\u0002\u0010\n\u001a\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u000bHÆ\u0001¢\u0006\u0002\u0010\u0019J\t\u0010\u001a\u001a\u00020\u0006HÖ\u0001J\u0013\u0010\u001b\u001a\u00020\u001c2\b\u0010\u001d\u001a\u0004\u0018\u00010\u001eHÖ\u0003J\t\u0010\u001f\u001a\u00020\u0006HÖ\u0001J\u0014\u0010 \u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u001e0!H\u0016J\t\u0010\"\u001a\u00020\u0004HÖ\u0001J\u0019\u0010#\u001a\u00020$2\u0006\u0010%\u001a\u00020&2\u0006\u0010'\u001a\u00020\u0006HÖ\u0001R\u001c\u0010\n\u001a\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u000bX\u0004¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000eR\u0010\u0010\b\u001a\u0004\u0018\u00010\u0004X\u0004¢\u0006\u0002\n\u0000R\u0012\u0010\u0005\u001a\u0004\u0018\u00010\u0006X\u0004¢\u0006\u0004\n\u0002\u0010\u000fR\u0012\u0010\u0007\u001a\u0004\u0018\u00010\u0006X\u0004¢\u0006\u0004\n\u0002\u0010\u000fR\u0010\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\t\u001a\u0004\u0018\u00010\u0004X\u0004¢\u0006\u0002\n\u0000¨\u0006*"}, d2 = {"Lcom/stripe/android/model/PaymentMethodCreateParams$Card;", "Lcom/stripe/android/model/StripeParamsModel;", "Landroid/os/Parcelable;", "number", "", "expiryMonth", "", "expiryYear", "cvc", "token", "attribution", "", "(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V", "getAttribution$stripe_release", "()Ljava/util/Set;", "Ljava/lang/Integer;", "component1", "component2", "()Ljava/lang/Integer;", "component3", "component4", "component5", "component6", "component6$stripe_release", "copy", "(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)Lcom/stripe/android/model/PaymentMethodCreateParams$Card;", "describeContents", "equals", "", "other", "", "hashCode", "toParamMap", "", "toString", "writeToParcel", "", "parcel", "Landroid/os/Parcel;", "flags", "Builder", "Companion", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: PaymentMethodCreateParams.kt */
    public static final class Card implements StripeParamsModel, Parcelable {
        public static final Parcelable.Creator CREATOR = new Creator();
        public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
        private static final String PARAM_CVC = "cvc";
        private static final String PARAM_EXP_MONTH = "exp_month";
        private static final String PARAM_EXP_YEAR = "exp_year";
        private static final String PARAM_NUMBER = "number";
        private static final String PARAM_TOKEN = "token";
        private final Set<String> attribution;
        private final String cvc;
        private final Integer expiryMonth;
        private final Integer expiryYear;
        private final String number;
        private final String token;

        @Metadata(bv = {1, 0, 3}, k = 3, mv = {1, 1, 16})
        public static class Creator implements Parcelable.Creator {
            public final Object createFromParcel(Parcel parcel) {
                Intrinsics.checkParameterIsNotNull(parcel, "in");
                String readString = parcel.readString();
                LinkedHashSet linkedHashSet = null;
                Integer valueOf = parcel.readInt() != 0 ? Integer.valueOf(parcel.readInt()) : null;
                Integer valueOf2 = parcel.readInt() != 0 ? Integer.valueOf(parcel.readInt()) : null;
                String readString2 = parcel.readString();
                String readString3 = parcel.readString();
                if (parcel.readInt() != 0) {
                    int readInt = parcel.readInt();
                    linkedHashSet = new LinkedHashSet(readInt);
                    while (readInt != 0) {
                        linkedHashSet.add(parcel.readString());
                        readInt--;
                    }
                }
                return new Card(readString, valueOf, valueOf2, readString2, readString3, linkedHashSet);
            }

            public final Object[] newArray(int i) {
                return new Card[i];
            }
        }

        public Card() {
            this((String) null, (Integer) null, (Integer) null, (String) null, (String) null, (Set) null, 63, (DefaultConstructorMarker) null);
        }

        private final String component1() {
            return this.number;
        }

        private final Integer component2() {
            return this.expiryMonth;
        }

        private final Integer component3() {
            return this.expiryYear;
        }

        private final String component4() {
            return this.cvc;
        }

        private final String component5() {
            return this.token;
        }

        public static /* synthetic */ Card copy$default(Card card, String str, Integer num, Integer num2, String str2, String str3, Set<String> set, int i, Object obj) {
            if ((i & 1) != 0) {
                str = card.number;
            }
            if ((i & 2) != 0) {
                num = card.expiryMonth;
            }
            Integer num3 = num;
            if ((i & 4) != 0) {
                num2 = card.expiryYear;
            }
            Integer num4 = num2;
            if ((i & 8) != 0) {
                str2 = card.cvc;
            }
            String str4 = str2;
            if ((i & 16) != 0) {
                str3 = card.token;
            }
            String str5 = str3;
            if ((i & 32) != 0) {
                set = card.attribution;
            }
            return card.copy(str, num3, num4, str4, str5, set);
        }

        @JvmStatic
        public static final Card create(String str) {
            return Companion.create(str);
        }

        public final Set<String> component6$stripe_release() {
            return this.attribution;
        }

        public final Card copy(String str, Integer num, Integer num2, String str2, String str3, Set<String> set) {
            return new Card(str, num, num2, str2, str3, set);
        }

        public int describeContents() {
            return 0;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Card)) {
                return false;
            }
            Card card = (Card) obj;
            return Intrinsics.areEqual((Object) this.number, (Object) card.number) && Intrinsics.areEqual((Object) this.expiryMonth, (Object) card.expiryMonth) && Intrinsics.areEqual((Object) this.expiryYear, (Object) card.expiryYear) && Intrinsics.areEqual((Object) this.cvc, (Object) card.cvc) && Intrinsics.areEqual((Object) this.token, (Object) card.token) && Intrinsics.areEqual((Object) this.attribution, (Object) card.attribution);
        }

        public int hashCode() {
            String str = this.number;
            int i = 0;
            int hashCode = (str != null ? str.hashCode() : 0) * 31;
            Integer num = this.expiryMonth;
            int hashCode2 = (hashCode + (num != null ? num.hashCode() : 0)) * 31;
            Integer num2 = this.expiryYear;
            int hashCode3 = (hashCode2 + (num2 != null ? num2.hashCode() : 0)) * 31;
            String str2 = this.cvc;
            int hashCode4 = (hashCode3 + (str2 != null ? str2.hashCode() : 0)) * 31;
            String str3 = this.token;
            int hashCode5 = (hashCode4 + (str3 != null ? str3.hashCode() : 0)) * 31;
            Set<String> set = this.attribution;
            if (set != null) {
                i = set.hashCode();
            }
            return hashCode5 + i;
        }

        public String toString() {
            return "Card(number=" + this.number + ", expiryMonth=" + this.expiryMonth + ", expiryYear=" + this.expiryYear + ", cvc=" + this.cvc + ", token=" + this.token + ", attribution=" + this.attribution + ")";
        }

        public void writeToParcel(Parcel parcel, int i) {
            Intrinsics.checkParameterIsNotNull(parcel, "parcel");
            parcel.writeString(this.number);
            Integer num = this.expiryMonth;
            if (num != null) {
                parcel.writeInt(1);
                parcel.writeInt(num.intValue());
            } else {
                parcel.writeInt(0);
            }
            Integer num2 = this.expiryYear;
            if (num2 != null) {
                parcel.writeInt(1);
                parcel.writeInt(num2.intValue());
            } else {
                parcel.writeInt(0);
            }
            parcel.writeString(this.cvc);
            parcel.writeString(this.token);
            Set<String> set = this.attribution;
            if (set != null) {
                parcel.writeInt(1);
                parcel.writeInt(set.size());
                for (String writeString : set) {
                    parcel.writeString(writeString);
                }
                return;
            }
            parcel.writeInt(0);
        }

        public Card(String str, Integer num, Integer num2, String str2, String str3, Set<String> set) {
            this.number = str;
            this.expiryMonth = num;
            this.expiryYear = num2;
            this.cvc = str2;
            this.token = str3;
            this.attribution = set;
        }

        /* JADX WARNING: Illegal instructions before constructor call */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public /* synthetic */ Card(java.lang.String r5, java.lang.Integer r6, java.lang.Integer r7, java.lang.String r8, java.lang.String r9, java.util.Set r10, int r11, kotlin.jvm.internal.DefaultConstructorMarker r12) {
            /*
                r4 = this;
                r12 = r11 & 1
                r0 = 0
                if (r12 == 0) goto L_0x0008
                r5 = r0
                java.lang.String r5 = (java.lang.String) r5
            L_0x0008:
                r12 = r11 & 2
                if (r12 == 0) goto L_0x000f
                r6 = r0
                java.lang.Integer r6 = (java.lang.Integer) r6
            L_0x000f:
                r12 = r6
                r6 = r11 & 4
                if (r6 == 0) goto L_0x0017
                r7 = r0
                java.lang.Integer r7 = (java.lang.Integer) r7
            L_0x0017:
                r1 = r7
                r6 = r11 & 8
                if (r6 == 0) goto L_0x001f
                r8 = r0
                java.lang.String r8 = (java.lang.String) r8
            L_0x001f:
                r2 = r8
                r6 = r11 & 16
                if (r6 == 0) goto L_0x0027
                r9 = r0
                java.lang.String r9 = (java.lang.String) r9
            L_0x0027:
                r3 = r9
                r6 = r11 & 32
                if (r6 == 0) goto L_0x002f
                r10 = r0
                java.util.Set r10 = (java.util.Set) r10
            L_0x002f:
                r0 = r10
                r6 = r4
                r7 = r5
                r8 = r12
                r9 = r1
                r10 = r2
                r11 = r3
                r12 = r0
                r6.<init>(r7, r8, r9, r10, r11, r12)
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.stripe.android.model.PaymentMethodCreateParams.Card.<init>(java.lang.String, java.lang.Integer, java.lang.Integer, java.lang.String, java.lang.String, java.util.Set, int, kotlin.jvm.internal.DefaultConstructorMarker):void");
        }

        public final Set<String> getAttribution$stripe_release() {
            return this.attribution;
        }

        public Map<String, Object> toParamMap() {
            Pair[] pairArr = {TuplesKt.to(PARAM_NUMBER, this.number), TuplesKt.to(PARAM_EXP_MONTH, this.expiryMonth), TuplesKt.to(PARAM_EXP_YEAR, this.expiryYear), TuplesKt.to(PARAM_CVC, this.cvc), TuplesKt.to(PARAM_TOKEN, this.token)};
            Collection arrayList = new ArrayList();
            for (Pair pair : CollectionsKt.listOf(pairArr)) {
                Object second = pair.getSecond();
                Pair pair2 = second != null ? TuplesKt.to(pair.getFirst(), second) : null;
                if (pair2 != null) {
                    arrayList.add(pair2);
                }
            }
            return MapsKt.toMap((List) arrayList);
        }

        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0002\b\n\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0003J\b\u0010\u000b\u001a\u00020\u0002H\u0016J\u0010\u0010\f\u001a\u00020\u00002\b\u0010\u0004\u001a\u0004\u0018\u00010\u0005J\u0015\u0010\r\u001a\u00020\u00002\b\u0010\u0006\u001a\u0004\u0018\u00010\u0007¢\u0006\u0002\u0010\u000eJ\u0015\u0010\u000f\u001a\u00020\u00002\b\u0010\t\u001a\u0004\u0018\u00010\u0007¢\u0006\u0002\u0010\u000eJ\u0010\u0010\u0010\u001a\u00020\u00002\b\u0010\n\u001a\u0004\u0018\u00010\u0005R\u0010\u0010\u0004\u001a\u0004\u0018\u00010\u0005X\u000e¢\u0006\u0002\n\u0000R\u0012\u0010\u0006\u001a\u0004\u0018\u00010\u0007X\u000e¢\u0006\u0004\n\u0002\u0010\bR\u0012\u0010\t\u001a\u0004\u0018\u00010\u0007X\u000e¢\u0006\u0004\n\u0002\u0010\bR\u0010\u0010\n\u001a\u0004\u0018\u00010\u0005X\u000e¢\u0006\u0002\n\u0000¨\u0006\u0011"}, d2 = {"Lcom/stripe/android/model/PaymentMethodCreateParams$Card$Builder;", "Lcom/stripe/android/ObjectBuilder;", "Lcom/stripe/android/model/PaymentMethodCreateParams$Card;", "()V", "cvc", "", "expiryMonth", "", "Ljava/lang/Integer;", "expiryYear", "number", "build", "setCvc", "setExpiryMonth", "(Ljava/lang/Integer;)Lcom/stripe/android/model/PaymentMethodCreateParams$Card$Builder;", "setExpiryYear", "setNumber", "stripe_release"}, k = 1, mv = {1, 1, 16})
        /* compiled from: PaymentMethodCreateParams.kt */
        public static final class Builder implements ObjectBuilder<Card> {
            private String cvc;
            private Integer expiryMonth;
            private Integer expiryYear;
            private String number;

            public final Builder setNumber(String str) {
                Builder builder = this;
                builder.number = str;
                return builder;
            }

            public final Builder setExpiryMonth(Integer num) {
                Builder builder = this;
                builder.expiryMonth = num;
                return builder;
            }

            public final Builder setExpiryYear(Integer num) {
                Builder builder = this;
                builder.expiryYear = num;
                return builder;
            }

            public final Builder setCvc(String str) {
                Builder builder = this;
                builder.cvc = str;
                return builder;
            }

            public Card build() {
                return new Card(this.number, this.expiryMonth, this.expiryYear, this.cvc, (String) null, (Set) null, 48, (DefaultConstructorMarker) null);
            }
        }

        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0010\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u0004H\u0007R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\f"}, d2 = {"Lcom/stripe/android/model/PaymentMethodCreateParams$Card$Companion;", "", "()V", "PARAM_CVC", "", "PARAM_EXP_MONTH", "PARAM_EXP_YEAR", "PARAM_NUMBER", "PARAM_TOKEN", "create", "Lcom/stripe/android/model/PaymentMethodCreateParams$Card;", "token", "stripe_release"}, k = 1, mv = {1, 1, 16})
        /* compiled from: PaymentMethodCreateParams.kt */
        public static final class Companion {
            private Companion() {
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }

            @JvmStatic
            public final Card create(String str) {
                Intrinsics.checkParameterIsNotNull(str, Card.PARAM_TOKEN);
                return new Card((String) null, (Integer) null, (Integer) null, (String) null, str, (Set) null, 46, (DefaultConstructorMarker) null);
            }
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\b\b\u0018\u0000 \u00182\u00020\u00012\u00020\u0002:\u0002\u0017\u0018B\u000f\u0012\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004¢\u0006\u0002\u0010\u0005J\u000b\u0010\u0006\u001a\u0004\u0018\u00010\u0004HÂ\u0003J\u0015\u0010\u0007\u001a\u00020\u00002\n\b\u0002\u0010\u0003\u001a\u0004\u0018\u00010\u0004HÆ\u0001J\t\u0010\b\u001a\u00020\tHÖ\u0001J\u0013\u0010\n\u001a\u00020\u000b2\b\u0010\f\u001a\u0004\u0018\u00010\rHÖ\u0003J\t\u0010\u000e\u001a\u00020\tHÖ\u0001J\u0014\u0010\u000f\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\r0\u0010H\u0016J\t\u0010\u0011\u001a\u00020\u0004HÖ\u0001J\u0019\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\tHÖ\u0001R\u0010\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0019"}, d2 = {"Lcom/stripe/android/model/PaymentMethodCreateParams$Ideal;", "Lcom/stripe/android/model/StripeParamsModel;", "Landroid/os/Parcelable;", "bank", "", "(Ljava/lang/String;)V", "component1", "copy", "describeContents", "", "equals", "", "other", "", "hashCode", "toParamMap", "", "toString", "writeToParcel", "", "parcel", "Landroid/os/Parcel;", "flags", "Builder", "Companion", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: PaymentMethodCreateParams.kt */
    public static final class Ideal implements StripeParamsModel, Parcelable {
        public static final Parcelable.Creator CREATOR = new Creator();
        @Deprecated
        public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
        private static final String PARAM_BANK = "bank";
        private final String bank;

        @Metadata(bv = {1, 0, 3}, k = 3, mv = {1, 1, 16})
        public static class Creator implements Parcelable.Creator {
            public final Object createFromParcel(Parcel parcel) {
                Intrinsics.checkParameterIsNotNull(parcel, "in");
                return new Ideal(parcel.readString());
            }

            public final Object[] newArray(int i) {
                return new Ideal[i];
            }
        }

        private final String component1() {
            return this.bank;
        }

        public static /* synthetic */ Ideal copy$default(Ideal ideal, String str, int i, Object obj) {
            if ((i & 1) != 0) {
                str = ideal.bank;
            }
            return ideal.copy(str);
        }

        public final Ideal copy(String str) {
            return new Ideal(str);
        }

        public int describeContents() {
            return 0;
        }

        public boolean equals(Object obj) {
            if (this != obj) {
                return (obj instanceof Ideal) && Intrinsics.areEqual((Object) this.bank, (Object) ((Ideal) obj).bank);
            }
            return true;
        }

        public int hashCode() {
            String str = this.bank;
            if (str != null) {
                return str.hashCode();
            }
            return 0;
        }

        public String toString() {
            return "Ideal(bank=" + this.bank + ")";
        }

        public void writeToParcel(Parcel parcel, int i) {
            Intrinsics.checkParameterIsNotNull(parcel, "parcel");
            parcel.writeString(this.bank);
        }

        public Ideal(String str) {
            this.bank = str;
        }

        public Map<String, Object> toParamMap() {
            String str = this.bank;
            Map<String, Object> mapOf = str != null ? MapsKt.mapOf(TuplesKt.to(PARAM_BANK, str)) : null;
            return mapOf != null ? mapOf : MapsKt.emptyMap();
        }

        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0007\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0003J\b\u0010\n\u001a\u00020\u0002H\u0016J\u0010\u0010\u000b\u001a\u00020\u00002\b\u0010\u0004\u001a\u0004\u0018\u00010\u0005R\u001c\u0010\u0004\u001a\u0004\u0018\u00010\u0005X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0006\u0010\u0007\"\u0004\b\b\u0010\t¨\u0006\f"}, d2 = {"Lcom/stripe/android/model/PaymentMethodCreateParams$Ideal$Builder;", "Lcom/stripe/android/ObjectBuilder;", "Lcom/stripe/android/model/PaymentMethodCreateParams$Ideal;", "()V", "bank", "", "getBank$stripe_release", "()Ljava/lang/String;", "setBank$stripe_release", "(Ljava/lang/String;)V", "build", "setBank", "stripe_release"}, k = 1, mv = {1, 1, 16})
        /* compiled from: PaymentMethodCreateParams.kt */
        public static final class Builder implements ObjectBuilder<Ideal> {
            private String bank;

            public final String getBank$stripe_release() {
                return this.bank;
            }

            public final void setBank$stripe_release(String str) {
                this.bank = str;
            }

            public final Builder setBank(String str) {
                Builder builder = this;
                builder.bank = str;
                return builder;
            }

            public Ideal build() {
                return new Ideal(this.bank);
            }
        }

        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\u0005"}, d2 = {"Lcom/stripe/android/model/PaymentMethodCreateParams$Ideal$Companion;", "", "()V", "PARAM_BANK", "", "stripe_release"}, k = 1, mv = {1, 1, 16})
        /* compiled from: PaymentMethodCreateParams.kt */
        private static final class Companion {
            private Companion() {
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\b\b\u0018\u0000 \u00182\u00020\u00012\u00020\u0002:\u0002\u0017\u0018B\u000f\u0012\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004¢\u0006\u0002\u0010\u0005J\u000b\u0010\u0006\u001a\u0004\u0018\u00010\u0004HÂ\u0003J\u0015\u0010\u0007\u001a\u00020\u00002\n\b\u0002\u0010\u0003\u001a\u0004\u0018\u00010\u0004HÆ\u0001J\t\u0010\b\u001a\u00020\tHÖ\u0001J\u0013\u0010\n\u001a\u00020\u000b2\b\u0010\f\u001a\u0004\u0018\u00010\rHÖ\u0003J\t\u0010\u000e\u001a\u00020\tHÖ\u0001J\u0014\u0010\u000f\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\r0\u0010H\u0016J\t\u0010\u0011\u001a\u00020\u0004HÖ\u0001J\u0019\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\tHÖ\u0001R\u0010\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0019"}, d2 = {"Lcom/stripe/android/model/PaymentMethodCreateParams$Fpx;", "Lcom/stripe/android/model/StripeParamsModel;", "Landroid/os/Parcelable;", "bank", "", "(Ljava/lang/String;)V", "component1", "copy", "describeContents", "", "equals", "", "other", "", "hashCode", "toParamMap", "", "toString", "writeToParcel", "", "parcel", "Landroid/os/Parcel;", "flags", "Builder", "Companion", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: PaymentMethodCreateParams.kt */
    public static final class Fpx implements StripeParamsModel, Parcelable {
        public static final Parcelable.Creator CREATOR = new Creator();
        @Deprecated
        public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
        private static final String PARAM_BANK = "bank";
        private final String bank;

        @Metadata(bv = {1, 0, 3}, k = 3, mv = {1, 1, 16})
        public static class Creator implements Parcelable.Creator {
            public final Object createFromParcel(Parcel parcel) {
                Intrinsics.checkParameterIsNotNull(parcel, "in");
                return new Fpx(parcel.readString());
            }

            public final Object[] newArray(int i) {
                return new Fpx[i];
            }
        }

        private final String component1() {
            return this.bank;
        }

        public static /* synthetic */ Fpx copy$default(Fpx fpx, String str, int i, Object obj) {
            if ((i & 1) != 0) {
                str = fpx.bank;
            }
            return fpx.copy(str);
        }

        public final Fpx copy(String str) {
            return new Fpx(str);
        }

        public int describeContents() {
            return 0;
        }

        public boolean equals(Object obj) {
            if (this != obj) {
                return (obj instanceof Fpx) && Intrinsics.areEqual((Object) this.bank, (Object) ((Fpx) obj).bank);
            }
            return true;
        }

        public int hashCode() {
            String str = this.bank;
            if (str != null) {
                return str.hashCode();
            }
            return 0;
        }

        public String toString() {
            return "Fpx(bank=" + this.bank + ")";
        }

        public void writeToParcel(Parcel parcel, int i) {
            Intrinsics.checkParameterIsNotNull(parcel, "parcel");
            parcel.writeString(this.bank);
        }

        public Fpx(String str) {
            this.bank = str;
        }

        public Map<String, Object> toParamMap() {
            String str = this.bank;
            Map<String, Object> mapOf = str != null ? MapsKt.mapOf(TuplesKt.to(PARAM_BANK, str)) : null;
            return mapOf != null ? mapOf : MapsKt.emptyMap();
        }

        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0007\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0003J\b\u0010\n\u001a\u00020\u0002H\u0016J\u0010\u0010\u000b\u001a\u00020\u00002\b\u0010\u0004\u001a\u0004\u0018\u00010\u0005R\u001c\u0010\u0004\u001a\u0004\u0018\u00010\u0005X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0006\u0010\u0007\"\u0004\b\b\u0010\t¨\u0006\f"}, d2 = {"Lcom/stripe/android/model/PaymentMethodCreateParams$Fpx$Builder;", "Lcom/stripe/android/ObjectBuilder;", "Lcom/stripe/android/model/PaymentMethodCreateParams$Fpx;", "()V", "bank", "", "getBank$stripe_release", "()Ljava/lang/String;", "setBank$stripe_release", "(Ljava/lang/String;)V", "build", "setBank", "stripe_release"}, k = 1, mv = {1, 1, 16})
        /* compiled from: PaymentMethodCreateParams.kt */
        public static final class Builder implements ObjectBuilder<Fpx> {
            private String bank;

            public final String getBank$stripe_release() {
                return this.bank;
            }

            public final void setBank$stripe_release(String str) {
                this.bank = str;
            }

            public final Builder setBank(String str) {
                Builder builder = this;
                builder.bank = str;
                return builder;
            }

            public Fpx build() {
                return new Fpx(this.bank);
            }
        }

        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\u0005"}, d2 = {"Lcom/stripe/android/model/PaymentMethodCreateParams$Fpx$Companion;", "", "()V", "PARAM_BANK", "", "stripe_release"}, k = 1, mv = {1, 1, 16})
        /* compiled from: PaymentMethodCreateParams.kt */
        private static final class Companion {
            private Companion() {
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\b\b\u0018\u0000 \u00182\u00020\u00012\u00020\u0002:\u0002\u0017\u0018B\u000f\u0012\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004¢\u0006\u0002\u0010\u0005J\u000b\u0010\u0006\u001a\u0004\u0018\u00010\u0004HÂ\u0003J\u0015\u0010\u0007\u001a\u00020\u00002\n\b\u0002\u0010\u0003\u001a\u0004\u0018\u00010\u0004HÆ\u0001J\t\u0010\b\u001a\u00020\tHÖ\u0001J\u0013\u0010\n\u001a\u00020\u000b2\b\u0010\f\u001a\u0004\u0018\u00010\rHÖ\u0003J\t\u0010\u000e\u001a\u00020\tHÖ\u0001J\u0014\u0010\u000f\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\r0\u0010H\u0016J\t\u0010\u0011\u001a\u00020\u0004HÖ\u0001J\u0019\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\tHÖ\u0001R\u0010\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0019"}, d2 = {"Lcom/stripe/android/model/PaymentMethodCreateParams$SepaDebit;", "Lcom/stripe/android/model/StripeParamsModel;", "Landroid/os/Parcelable;", "iban", "", "(Ljava/lang/String;)V", "component1", "copy", "describeContents", "", "equals", "", "other", "", "hashCode", "toParamMap", "", "toString", "writeToParcel", "", "parcel", "Landroid/os/Parcel;", "flags", "Builder", "Companion", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: PaymentMethodCreateParams.kt */
    public static final class SepaDebit implements StripeParamsModel, Parcelable {
        public static final Parcelable.Creator CREATOR = new Creator();
        @Deprecated
        public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
        private static final String PARAM_IBAN = "iban";
        private final String iban;

        @Metadata(bv = {1, 0, 3}, k = 3, mv = {1, 1, 16})
        public static class Creator implements Parcelable.Creator {
            public final Object createFromParcel(Parcel parcel) {
                Intrinsics.checkParameterIsNotNull(parcel, "in");
                return new SepaDebit(parcel.readString());
            }

            public final Object[] newArray(int i) {
                return new SepaDebit[i];
            }
        }

        private final String component1() {
            return this.iban;
        }

        public static /* synthetic */ SepaDebit copy$default(SepaDebit sepaDebit, String str, int i, Object obj) {
            if ((i & 1) != 0) {
                str = sepaDebit.iban;
            }
            return sepaDebit.copy(str);
        }

        public final SepaDebit copy(String str) {
            return new SepaDebit(str);
        }

        public int describeContents() {
            return 0;
        }

        public boolean equals(Object obj) {
            if (this != obj) {
                return (obj instanceof SepaDebit) && Intrinsics.areEqual((Object) this.iban, (Object) ((SepaDebit) obj).iban);
            }
            return true;
        }

        public int hashCode() {
            String str = this.iban;
            if (str != null) {
                return str.hashCode();
            }
            return 0;
        }

        public String toString() {
            return "SepaDebit(iban=" + this.iban + ")";
        }

        public void writeToParcel(Parcel parcel, int i) {
            Intrinsics.checkParameterIsNotNull(parcel, "parcel");
            parcel.writeString(this.iban);
        }

        public SepaDebit(String str) {
            this.iban = str;
        }

        public Map<String, Object> toParamMap() {
            String str = this.iban;
            Map<String, Object> mapOf = str != null ? MapsKt.mapOf(TuplesKt.to(PARAM_IBAN, str)) : null;
            return mapOf != null ? mapOf : MapsKt.emptyMap();
        }

        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0003J\b\u0010\u0006\u001a\u00020\u0002H\u0016J\u0010\u0010\u0007\u001a\u00020\u00002\b\u0010\u0004\u001a\u0004\u0018\u00010\u0005R\u0010\u0010\u0004\u001a\u0004\u0018\u00010\u0005X\u000e¢\u0006\u0002\n\u0000¨\u0006\b"}, d2 = {"Lcom/stripe/android/model/PaymentMethodCreateParams$SepaDebit$Builder;", "Lcom/stripe/android/ObjectBuilder;", "Lcom/stripe/android/model/PaymentMethodCreateParams$SepaDebit;", "()V", "iban", "", "build", "setIban", "stripe_release"}, k = 1, mv = {1, 1, 16})
        /* compiled from: PaymentMethodCreateParams.kt */
        public static final class Builder implements ObjectBuilder<SepaDebit> {
            private String iban;

            public final Builder setIban(String str) {
                Builder builder = this;
                builder.iban = str;
                return builder;
            }

            public SepaDebit build() {
                return new SepaDebit(this.iban);
            }
        }

        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\u0005"}, d2 = {"Lcom/stripe/android/model/PaymentMethodCreateParams$SepaDebit$Companion;", "", "()V", "PARAM_IBAN", "", "stripe_release"}, k = 1, mv = {1, 1, 16})
        /* compiled from: PaymentMethodCreateParams.kt */
        private static final class Companion {
            private Companion() {
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\f\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\b\b\u0018\u0000 \u001f2\u00020\u00012\u00020\u0002:\u0001\u001fB\u0015\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0004¢\u0006\u0002\u0010\u0006J\t\u0010\r\u001a\u00020\u0004HÆ\u0003J\t\u0010\u000e\u001a\u00020\u0004HÆ\u0003J\u001d\u0010\u000f\u001a\u00020\u00002\b\b\u0002\u0010\u0003\u001a\u00020\u00042\b\b\u0002\u0010\u0005\u001a\u00020\u0004HÆ\u0001J\t\u0010\u0010\u001a\u00020\u0011HÖ\u0001J\u0013\u0010\u0012\u001a\u00020\u00132\b\u0010\u0014\u001a\u0004\u0018\u00010\u0015HÖ\u0003J\t\u0010\u0016\u001a\u00020\u0011HÖ\u0001J\u0014\u0010\u0017\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00150\u0018H\u0016J\t\u0010\u0019\u001a\u00020\u0004HÖ\u0001J\u0019\u0010\u001a\u001a\u00020\u001b2\u0006\u0010\u001c\u001a\u00020\u001d2\u0006\u0010\u001e\u001a\u00020\u0011HÖ\u0001R\u001a\u0010\u0005\u001a\u00020\u0004X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0007\u0010\b\"\u0004\b\t\u0010\nR\u001a\u0010\u0003\u001a\u00020\u0004X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u000b\u0010\b\"\u0004\b\f\u0010\n¨\u0006 "}, d2 = {"Lcom/stripe/android/model/PaymentMethodCreateParams$AuBecsDebit;", "Lcom/stripe/android/model/StripeParamsModel;", "Landroid/os/Parcelable;", "bsbNumber", "", "accountNumber", "(Ljava/lang/String;Ljava/lang/String;)V", "getAccountNumber", "()Ljava/lang/String;", "setAccountNumber", "(Ljava/lang/String;)V", "getBsbNumber", "setBsbNumber", "component1", "component2", "copy", "describeContents", "", "equals", "", "other", "", "hashCode", "toParamMap", "", "toString", "writeToParcel", "", "parcel", "Landroid/os/Parcel;", "flags", "Companion", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: PaymentMethodCreateParams.kt */
    public static final class AuBecsDebit implements StripeParamsModel, Parcelable {
        public static final Parcelable.Creator CREATOR = new Creator();
        @Deprecated
        public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
        private static final String PARAM_ACCOUNT_NUMBER = "account_number";
        private static final String PARAM_BSB_NUMBER = "bsb_number";
        private String accountNumber;
        private String bsbNumber;

        @Metadata(bv = {1, 0, 3}, k = 3, mv = {1, 1, 16})
        public static class Creator implements Parcelable.Creator {
            public final Object createFromParcel(Parcel parcel) {
                Intrinsics.checkParameterIsNotNull(parcel, "in");
                return new AuBecsDebit(parcel.readString(), parcel.readString());
            }

            public final Object[] newArray(int i) {
                return new AuBecsDebit[i];
            }
        }

        public static /* synthetic */ AuBecsDebit copy$default(AuBecsDebit auBecsDebit, String str, String str2, int i, Object obj) {
            if ((i & 1) != 0) {
                str = auBecsDebit.bsbNumber;
            }
            if ((i & 2) != 0) {
                str2 = auBecsDebit.accountNumber;
            }
            return auBecsDebit.copy(str, str2);
        }

        public final String component1() {
            return this.bsbNumber;
        }

        public final String component2() {
            return this.accountNumber;
        }

        public final AuBecsDebit copy(String str, String str2) {
            Intrinsics.checkParameterIsNotNull(str, "bsbNumber");
            Intrinsics.checkParameterIsNotNull(str2, "accountNumber");
            return new AuBecsDebit(str, str2);
        }

        public int describeContents() {
            return 0;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof AuBecsDebit)) {
                return false;
            }
            AuBecsDebit auBecsDebit = (AuBecsDebit) obj;
            return Intrinsics.areEqual((Object) this.bsbNumber, (Object) auBecsDebit.bsbNumber) && Intrinsics.areEqual((Object) this.accountNumber, (Object) auBecsDebit.accountNumber);
        }

        public int hashCode() {
            String str = this.bsbNumber;
            int i = 0;
            int hashCode = (str != null ? str.hashCode() : 0) * 31;
            String str2 = this.accountNumber;
            if (str2 != null) {
                i = str2.hashCode();
            }
            return hashCode + i;
        }

        public String toString() {
            return "AuBecsDebit(bsbNumber=" + this.bsbNumber + ", accountNumber=" + this.accountNumber + ")";
        }

        public void writeToParcel(Parcel parcel, int i) {
            Intrinsics.checkParameterIsNotNull(parcel, "parcel");
            parcel.writeString(this.bsbNumber);
            parcel.writeString(this.accountNumber);
        }

        public AuBecsDebit(String str, String str2) {
            Intrinsics.checkParameterIsNotNull(str, "bsbNumber");
            Intrinsics.checkParameterIsNotNull(str2, "accountNumber");
            this.bsbNumber = str;
            this.accountNumber = str2;
        }

        public final String getBsbNumber() {
            return this.bsbNumber;
        }

        public final void setBsbNumber(String str) {
            Intrinsics.checkParameterIsNotNull(str, "<set-?>");
            this.bsbNumber = str;
        }

        public final String getAccountNumber() {
            return this.accountNumber;
        }

        public final void setAccountNumber(String str) {
            Intrinsics.checkParameterIsNotNull(str, "<set-?>");
            this.accountNumber = str;
        }

        public Map<String, Object> toParamMap() {
            return MapsKt.mapOf(TuplesKt.to(PARAM_BSB_NUMBER, this.bsbNumber), TuplesKt.to(PARAM_ACCOUNT_NUMBER, this.accountNumber));
        }

        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\u0006"}, d2 = {"Lcom/stripe/android/model/PaymentMethodCreateParams$AuBecsDebit$Companion;", "", "()V", "PARAM_ACCOUNT_NUMBER", "", "PARAM_BSB_NUMBER", "stripe_release"}, k = 1, mv = {1, 1, 16})
        /* compiled from: PaymentMethodCreateParams.kt */
        private static final class Companion {
            private Companion() {
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\f\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\b\b\u0018\u0000 \u001f2\u00020\u00012\u00020\u0002:\u0001\u001fB\u0015\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0004¢\u0006\u0002\u0010\u0006J\t\u0010\r\u001a\u00020\u0004HÆ\u0003J\t\u0010\u000e\u001a\u00020\u0004HÆ\u0003J\u001d\u0010\u000f\u001a\u00020\u00002\b\b\u0002\u0010\u0003\u001a\u00020\u00042\b\b\u0002\u0010\u0005\u001a\u00020\u0004HÆ\u0001J\t\u0010\u0010\u001a\u00020\u0011HÖ\u0001J\u0013\u0010\u0012\u001a\u00020\u00132\b\u0010\u0014\u001a\u0004\u0018\u00010\u0015HÖ\u0003J\t\u0010\u0016\u001a\u00020\u0011HÖ\u0001J\u0014\u0010\u0017\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00150\u0018H\u0016J\t\u0010\u0019\u001a\u00020\u0004HÖ\u0001J\u0019\u0010\u001a\u001a\u00020\u001b2\u0006\u0010\u001c\u001a\u00020\u001d2\u0006\u0010\u001e\u001a\u00020\u0011HÖ\u0001R\u001a\u0010\u0003\u001a\u00020\u0004X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0007\u0010\b\"\u0004\b\t\u0010\nR\u001a\u0010\u0005\u001a\u00020\u0004X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u000b\u0010\b\"\u0004\b\f\u0010\n¨\u0006 "}, d2 = {"Lcom/stripe/android/model/PaymentMethodCreateParams$BacsDebit;", "Lcom/stripe/android/model/StripeParamsModel;", "Landroid/os/Parcelable;", "accountNumber", "", "sortCode", "(Ljava/lang/String;Ljava/lang/String;)V", "getAccountNumber", "()Ljava/lang/String;", "setAccountNumber", "(Ljava/lang/String;)V", "getSortCode", "setSortCode", "component1", "component2", "copy", "describeContents", "", "equals", "", "other", "", "hashCode", "toParamMap", "", "toString", "writeToParcel", "", "parcel", "Landroid/os/Parcel;", "flags", "Companion", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: PaymentMethodCreateParams.kt */
    public static final class BacsDebit implements StripeParamsModel, Parcelable {
        public static final Parcelable.Creator CREATOR = new Creator();
        @Deprecated
        public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
        private static final String PARAM_ACCOUNT_NUMBER = "account_number";
        private static final String PARAM_SORT_CODE = "sort_code";
        private String accountNumber;
        private String sortCode;

        @Metadata(bv = {1, 0, 3}, k = 3, mv = {1, 1, 16})
        public static class Creator implements Parcelable.Creator {
            public final Object createFromParcel(Parcel parcel) {
                Intrinsics.checkParameterIsNotNull(parcel, "in");
                return new BacsDebit(parcel.readString(), parcel.readString());
            }

            public final Object[] newArray(int i) {
                return new BacsDebit[i];
            }
        }

        public static /* synthetic */ BacsDebit copy$default(BacsDebit bacsDebit, String str, String str2, int i, Object obj) {
            if ((i & 1) != 0) {
                str = bacsDebit.accountNumber;
            }
            if ((i & 2) != 0) {
                str2 = bacsDebit.sortCode;
            }
            return bacsDebit.copy(str, str2);
        }

        public final String component1() {
            return this.accountNumber;
        }

        public final String component2() {
            return this.sortCode;
        }

        public final BacsDebit copy(String str, String str2) {
            Intrinsics.checkParameterIsNotNull(str, "accountNumber");
            Intrinsics.checkParameterIsNotNull(str2, "sortCode");
            return new BacsDebit(str, str2);
        }

        public int describeContents() {
            return 0;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof BacsDebit)) {
                return false;
            }
            BacsDebit bacsDebit = (BacsDebit) obj;
            return Intrinsics.areEqual((Object) this.accountNumber, (Object) bacsDebit.accountNumber) && Intrinsics.areEqual((Object) this.sortCode, (Object) bacsDebit.sortCode);
        }

        public int hashCode() {
            String str = this.accountNumber;
            int i = 0;
            int hashCode = (str != null ? str.hashCode() : 0) * 31;
            String str2 = this.sortCode;
            if (str2 != null) {
                i = str2.hashCode();
            }
            return hashCode + i;
        }

        public String toString() {
            return "BacsDebit(accountNumber=" + this.accountNumber + ", sortCode=" + this.sortCode + ")";
        }

        public void writeToParcel(Parcel parcel, int i) {
            Intrinsics.checkParameterIsNotNull(parcel, "parcel");
            parcel.writeString(this.accountNumber);
            parcel.writeString(this.sortCode);
        }

        public BacsDebit(String str, String str2) {
            Intrinsics.checkParameterIsNotNull(str, "accountNumber");
            Intrinsics.checkParameterIsNotNull(str2, "sortCode");
            this.accountNumber = str;
            this.sortCode = str2;
        }

        public final String getAccountNumber() {
            return this.accountNumber;
        }

        public final void setAccountNumber(String str) {
            Intrinsics.checkParameterIsNotNull(str, "<set-?>");
            this.accountNumber = str;
        }

        public final String getSortCode() {
            return this.sortCode;
        }

        public final void setSortCode(String str) {
            Intrinsics.checkParameterIsNotNull(str, "<set-?>");
            this.sortCode = str;
        }

        public Map<String, Object> toParamMap() {
            return MapsKt.mapOf(TuplesKt.to(PARAM_ACCOUNT_NUMBER, this.accountNumber), TuplesKt.to(PARAM_SORT_CODE, this.sortCode));
        }

        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\u0006"}, d2 = {"Lcom/stripe/android/model/PaymentMethodCreateParams$BacsDebit$Companion;", "", "()V", "PARAM_ACCOUNT_NUMBER", "", "PARAM_SORT_CODE", "stripe_release"}, k = 1, mv = {1, 1, 16})
        /* compiled from: PaymentMethodCreateParams.kt */
        private static final class Companion {
            private Companion() {
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\b\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\b\b\u0018\u0000 \u001b2\u00020\u00012\u00020\u0002:\u0001\u001bB\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004¢\u0006\u0002\u0010\u0005J\u000e\u0010\t\u001a\u00020\u0004HÀ\u0003¢\u0006\u0002\b\nJ\u0013\u0010\u000b\u001a\u00020\u00002\b\b\u0002\u0010\u0003\u001a\u00020\u0004HÆ\u0001J\t\u0010\f\u001a\u00020\rHÖ\u0001J\u0013\u0010\u000e\u001a\u00020\u000f2\b\u0010\u0010\u001a\u0004\u0018\u00010\u0011HÖ\u0003J\t\u0010\u0012\u001a\u00020\rHÖ\u0001J\u0014\u0010\u0013\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00110\u0014H\u0016J\t\u0010\u0015\u001a\u00020\u0004HÖ\u0001J\u0019\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00020\rHÖ\u0001R\u001a\u0010\u0003\u001a\u00020\u0004X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0006\u0010\u0007\"\u0004\b\b\u0010\u0005¨\u0006\u001c"}, d2 = {"Lcom/stripe/android/model/PaymentMethodCreateParams$Sofort;", "Lcom/stripe/android/model/StripeParamsModel;", "Landroid/os/Parcelable;", "country", "", "(Ljava/lang/String;)V", "getCountry$stripe_release", "()Ljava/lang/String;", "setCountry$stripe_release", "component1", "component1$stripe_release", "copy", "describeContents", "", "equals", "", "other", "", "hashCode", "toParamMap", "", "toString", "writeToParcel", "", "parcel", "Landroid/os/Parcel;", "flags", "Companion", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: PaymentMethodCreateParams.kt */
    public static final class Sofort implements StripeParamsModel, Parcelable {
        public static final Parcelable.Creator CREATOR = new Creator();
        @Deprecated
        public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
        private static final String PARAM_COUNTRY = "country";
        private String country;

        @Metadata(bv = {1, 0, 3}, k = 3, mv = {1, 1, 16})
        public static class Creator implements Parcelable.Creator {
            public final Object createFromParcel(Parcel parcel) {
                Intrinsics.checkParameterIsNotNull(parcel, "in");
                return new Sofort(parcel.readString());
            }

            public final Object[] newArray(int i) {
                return new Sofort[i];
            }
        }

        public static /* synthetic */ Sofort copy$default(Sofort sofort, String str, int i, Object obj) {
            if ((i & 1) != 0) {
                str = sofort.country;
            }
            return sofort.copy(str);
        }

        public final String component1$stripe_release() {
            return this.country;
        }

        public final Sofort copy(String str) {
            Intrinsics.checkParameterIsNotNull(str, "country");
            return new Sofort(str);
        }

        public int describeContents() {
            return 0;
        }

        public boolean equals(Object obj) {
            if (this != obj) {
                return (obj instanceof Sofort) && Intrinsics.areEqual((Object) this.country, (Object) ((Sofort) obj).country);
            }
            return true;
        }

        public int hashCode() {
            String str = this.country;
            if (str != null) {
                return str.hashCode();
            }
            return 0;
        }

        public String toString() {
            return "Sofort(country=" + this.country + ")";
        }

        public void writeToParcel(Parcel parcel, int i) {
            Intrinsics.checkParameterIsNotNull(parcel, "parcel");
            parcel.writeString(this.country);
        }

        public Sofort(String str) {
            Intrinsics.checkParameterIsNotNull(str, "country");
            this.country = str;
        }

        public final String getCountry$stripe_release() {
            return this.country;
        }

        public final void setCountry$stripe_release(String str) {
            Intrinsics.checkParameterIsNotNull(str, "<set-?>");
            this.country = str;
        }

        public Map<String, Object> toParamMap() {
            String str = this.country;
            Locale locale = Locale.ROOT;
            Intrinsics.checkExpressionValueIsNotNull(locale, "Locale.ROOT");
            if (str != null) {
                String upperCase = str.toUpperCase(locale);
                Intrinsics.checkExpressionValueIsNotNull(upperCase, "(this as java.lang.String).toUpperCase(locale)");
                return MapsKt.mapOf(TuplesKt.to("country", upperCase));
            }
            throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
        }

        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\u0005"}, d2 = {"Lcom/stripe/android/model/PaymentMethodCreateParams$Sofort$Companion;", "", "()V", "PARAM_COUNTRY", "", "stripe_release"}, k = 1, mv = {1, 1, 16})
        /* compiled from: PaymentMethodCreateParams.kt */
        private static final class Companion {
            private Companion() {
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000Z\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J0\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\f2\u0016\b\u0002\u0010\r\u001a\u0010\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u000eH\u0007J0\u0010\u0007\u001a\u00020\b2\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u000b\u001a\u00020\f2\u0016\b\u0002\u0010\r\u001a\u0010\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u000eH\u0007J4\u0010\u0007\u001a\u00020\b2\u0006\u0010\u0011\u001a\u00020\u00122\n\b\u0002\u0010\u000b\u001a\u0004\u0018\u00010\f2\u0016\b\u0002\u0010\r\u001a\u0010\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u000eH\u0007J4\u0010\u0007\u001a\u00020\b2\u0006\u0010\u0013\u001a\u00020\u00142\n\b\u0002\u0010\u000b\u001a\u0004\u0018\u00010\f2\u0016\b\u0002\u0010\r\u001a\u0010\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u000eH\u0007J4\u0010\u0007\u001a\u00020\b2\u0006\u0010\u0015\u001a\u00020\u00162\n\b\u0002\u0010\u000b\u001a\u0004\u0018\u00010\f2\u0016\b\u0002\u0010\r\u001a\u0010\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u000eH\u0007J4\u0010\u0007\u001a\u00020\b2\u0006\u0010\u0017\u001a\u00020\u00182\n\b\u0002\u0010\u000b\u001a\u0004\u0018\u00010\f2\u0016\b\u0002\u0010\r\u001a\u0010\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u000eH\u0007J4\u0010\u0007\u001a\u00020\b2\u0006\u0010\u0019\u001a\u00020\u001a2\n\b\u0002\u0010\u000b\u001a\u0004\u0018\u00010\f2\u0016\b\u0002\u0010\r\u001a\u0010\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u000eH\u0007J%\u0010\u001b\u001a\u00020\b2\u0016\b\u0002\u0010\r\u001a\u0010\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u000eH\u0001¢\u0006\u0002\b\u001cJ(\u0010\u001d\u001a\u00020\b2\u0006\u0010\u000b\u001a\u00020\f2\u0016\b\u0002\u0010\r\u001a\u0010\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u000eH\u0007J(\u0010\u001e\u001a\u00020\b2\u0006\u0010\u000b\u001a\u00020\f2\u0016\b\u0002\u0010\r\u001a\u0010\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u000eH\u0007J\u0010\u0010\u001f\u001a\u00020\b2\u0006\u0010 \u001a\u00020!H\u0007J(\u0010\"\u001a\u00020\b2\u0006\u0010\u000b\u001a\u00020\f2\u0016\b\u0002\u0010\r\u001a\u0010\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u000eH\u0007J-\u0010#\u001a\u00020\b2\u0006\u0010\u000b\u001a\u00020\f2\u0016\b\u0002\u0010\r\u001a\u0010\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u000eH\u0001¢\u0006\u0002\b$J(\u0010%\u001a\u00020\b2\u0006\u0010\u000b\u001a\u00020\f2\u0016\b\u0002\u0010\r\u001a\u0010\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u000eH\u0007R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006&"}, d2 = {"Lcom/stripe/android/model/PaymentMethodCreateParams$Companion;", "", "()V", "PARAM_BILLING_DETAILS", "", "PARAM_METADATA", "PARAM_TYPE", "create", "Lcom/stripe/android/model/PaymentMethodCreateParams;", "auBecsDebit", "Lcom/stripe/android/model/PaymentMethodCreateParams$AuBecsDebit;", "billingDetails", "Lcom/stripe/android/model/PaymentMethod$BillingDetails;", "metadata", "", "bacsDebit", "Lcom/stripe/android/model/PaymentMethodCreateParams$BacsDebit;", "card", "Lcom/stripe/android/model/PaymentMethodCreateParams$Card;", "fpx", "Lcom/stripe/android/model/PaymentMethodCreateParams$Fpx;", "ideal", "Lcom/stripe/android/model/PaymentMethodCreateParams$Ideal;", "sepaDebit", "Lcom/stripe/android/model/PaymentMethodCreateParams$SepaDebit;", "sofort", "Lcom/stripe/android/model/PaymentMethodCreateParams$Sofort;", "createAlipay", "createAlipay$stripe_release", "createBancontact", "createEps", "createFromGooglePay", "googlePayPaymentData", "Lorg/json/JSONObject;", "createGiropay", "createOxxo", "createOxxo$stripe_release", "createP24", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: PaymentMethodCreateParams.kt */
    public static final class Companion {
        @JvmStatic
        public final PaymentMethodCreateParams create(AuBecsDebit auBecsDebit, PaymentMethod.BillingDetails billingDetails) {
            return create$default(this, auBecsDebit, billingDetails, (Map) null, 4, (Object) null);
        }

        @JvmStatic
        public final PaymentMethodCreateParams create(BacsDebit bacsDebit, PaymentMethod.BillingDetails billingDetails) {
            return create$default(this, bacsDebit, billingDetails, (Map) null, 4, (Object) null);
        }

        @JvmStatic
        public final PaymentMethodCreateParams create(Card card) {
            return create$default(this, card, (PaymentMethod.BillingDetails) null, (Map) null, 6, (Object) null);
        }

        @JvmStatic
        public final PaymentMethodCreateParams create(Card card, PaymentMethod.BillingDetails billingDetails) {
            return create$default(this, card, billingDetails, (Map) null, 4, (Object) null);
        }

        @JvmStatic
        public final PaymentMethodCreateParams create(Fpx fpx) {
            return create$default(this, fpx, (PaymentMethod.BillingDetails) null, (Map) null, 6, (Object) null);
        }

        @JvmStatic
        public final PaymentMethodCreateParams create(Fpx fpx, PaymentMethod.BillingDetails billingDetails) {
            return create$default(this, fpx, billingDetails, (Map) null, 4, (Object) null);
        }

        @JvmStatic
        public final PaymentMethodCreateParams create(Ideal ideal) {
            return create$default(this, ideal, (PaymentMethod.BillingDetails) null, (Map) null, 6, (Object) null);
        }

        @JvmStatic
        public final PaymentMethodCreateParams create(Ideal ideal, PaymentMethod.BillingDetails billingDetails) {
            return create$default(this, ideal, billingDetails, (Map) null, 4, (Object) null);
        }

        @JvmStatic
        public final PaymentMethodCreateParams create(SepaDebit sepaDebit) {
            return create$default(this, sepaDebit, (PaymentMethod.BillingDetails) null, (Map) null, 6, (Object) null);
        }

        @JvmStatic
        public final PaymentMethodCreateParams create(SepaDebit sepaDebit, PaymentMethod.BillingDetails billingDetails) {
            return create$default(this, sepaDebit, billingDetails, (Map) null, 4, (Object) null);
        }

        @JvmStatic
        public final PaymentMethodCreateParams create(Sofort sofort) {
            return create$default(this, sofort, (PaymentMethod.BillingDetails) null, (Map) null, 6, (Object) null);
        }

        @JvmStatic
        public final PaymentMethodCreateParams create(Sofort sofort, PaymentMethod.BillingDetails billingDetails) {
            return create$default(this, sofort, billingDetails, (Map) null, 4, (Object) null);
        }

        public final PaymentMethodCreateParams createAlipay$stripe_release() {
            return createAlipay$stripe_release$default(this, (Map) null, 1, (Object) null);
        }

        @JvmStatic
        public final PaymentMethodCreateParams createBancontact(PaymentMethod.BillingDetails billingDetails) {
            return createBancontact$default(this, billingDetails, (Map) null, 2, (Object) null);
        }

        @JvmStatic
        public final PaymentMethodCreateParams createEps(PaymentMethod.BillingDetails billingDetails) {
            return createEps$default(this, billingDetails, (Map) null, 2, (Object) null);
        }

        @JvmStatic
        public final PaymentMethodCreateParams createGiropay(PaymentMethod.BillingDetails billingDetails) {
            return createGiropay$default(this, billingDetails, (Map) null, 2, (Object) null);
        }

        public final PaymentMethodCreateParams createOxxo$stripe_release(PaymentMethod.BillingDetails billingDetails) {
            return createOxxo$stripe_release$default(this, billingDetails, (Map) null, 2, (Object) null);
        }

        @JvmStatic
        public final PaymentMethodCreateParams createP24(PaymentMethod.BillingDetails billingDetails) {
            return createP24$default(this, billingDetails, (Map) null, 2, (Object) null);
        }

        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        public static /* synthetic */ PaymentMethodCreateParams create$default(Companion companion, Card card, PaymentMethod.BillingDetails billingDetails, Map map, int i, Object obj) {
            if ((i & 2) != 0) {
                billingDetails = null;
            }
            if ((i & 4) != 0) {
                map = null;
            }
            return companion.create(card, billingDetails, (Map<String, String>) map);
        }

        @JvmStatic
        public final PaymentMethodCreateParams create(Card card, PaymentMethod.BillingDetails billingDetails, Map<String, String> map) {
            Intrinsics.checkParameterIsNotNull(card, "card");
            return new PaymentMethodCreateParams(card, billingDetails, (Map) map, (DefaultConstructorMarker) null);
        }

        public static /* synthetic */ PaymentMethodCreateParams create$default(Companion companion, Ideal ideal, PaymentMethod.BillingDetails billingDetails, Map map, int i, Object obj) {
            if ((i & 2) != 0) {
                billingDetails = null;
            }
            if ((i & 4) != 0) {
                map = null;
            }
            return companion.create(ideal, billingDetails, (Map<String, String>) map);
        }

        @JvmStatic
        public final PaymentMethodCreateParams create(Ideal ideal, PaymentMethod.BillingDetails billingDetails, Map<String, String> map) {
            Intrinsics.checkParameterIsNotNull(ideal, "ideal");
            return new PaymentMethodCreateParams(ideal, billingDetails, (Map) map, (DefaultConstructorMarker) null);
        }

        public static /* synthetic */ PaymentMethodCreateParams create$default(Companion companion, Fpx fpx, PaymentMethod.BillingDetails billingDetails, Map map, int i, Object obj) {
            if ((i & 2) != 0) {
                billingDetails = null;
            }
            if ((i & 4) != 0) {
                map = null;
            }
            return companion.create(fpx, billingDetails, (Map<String, String>) map);
        }

        @JvmStatic
        public final PaymentMethodCreateParams create(Fpx fpx, PaymentMethod.BillingDetails billingDetails, Map<String, String> map) {
            Intrinsics.checkParameterIsNotNull(fpx, "fpx");
            return new PaymentMethodCreateParams(fpx, billingDetails, (Map) map, (DefaultConstructorMarker) null);
        }

        public static /* synthetic */ PaymentMethodCreateParams create$default(Companion companion, SepaDebit sepaDebit, PaymentMethod.BillingDetails billingDetails, Map map, int i, Object obj) {
            if ((i & 2) != 0) {
                billingDetails = null;
            }
            if ((i & 4) != 0) {
                map = null;
            }
            return companion.create(sepaDebit, billingDetails, (Map<String, String>) map);
        }

        @JvmStatic
        public final PaymentMethodCreateParams create(SepaDebit sepaDebit, PaymentMethod.BillingDetails billingDetails, Map<String, String> map) {
            Intrinsics.checkParameterIsNotNull(sepaDebit, "sepaDebit");
            return new PaymentMethodCreateParams(sepaDebit, billingDetails, (Map) map, (DefaultConstructorMarker) null);
        }

        public static /* synthetic */ PaymentMethodCreateParams create$default(Companion companion, AuBecsDebit auBecsDebit, PaymentMethod.BillingDetails billingDetails, Map map, int i, Object obj) {
            if ((i & 4) != 0) {
                map = null;
            }
            return companion.create(auBecsDebit, billingDetails, (Map<String, String>) map);
        }

        @JvmStatic
        public final PaymentMethodCreateParams create(AuBecsDebit auBecsDebit, PaymentMethod.BillingDetails billingDetails, Map<String, String> map) {
            Intrinsics.checkParameterIsNotNull(auBecsDebit, "auBecsDebit");
            Intrinsics.checkParameterIsNotNull(billingDetails, "billingDetails");
            return new PaymentMethodCreateParams(auBecsDebit, billingDetails, (Map) map, (DefaultConstructorMarker) null);
        }

        public static /* synthetic */ PaymentMethodCreateParams create$default(Companion companion, BacsDebit bacsDebit, PaymentMethod.BillingDetails billingDetails, Map map, int i, Object obj) {
            if ((i & 4) != 0) {
                map = null;
            }
            return companion.create(bacsDebit, billingDetails, (Map<String, String>) map);
        }

        @JvmStatic
        public final PaymentMethodCreateParams create(BacsDebit bacsDebit, PaymentMethod.BillingDetails billingDetails, Map<String, String> map) {
            Intrinsics.checkParameterIsNotNull(bacsDebit, "bacsDebit");
            Intrinsics.checkParameterIsNotNull(billingDetails, "billingDetails");
            return new PaymentMethodCreateParams(bacsDebit, billingDetails, (Map) map, (DefaultConstructorMarker) null);
        }

        public static /* synthetic */ PaymentMethodCreateParams create$default(Companion companion, Sofort sofort, PaymentMethod.BillingDetails billingDetails, Map map, int i, Object obj) {
            if ((i & 2) != 0) {
                billingDetails = null;
            }
            if ((i & 4) != 0) {
                map = null;
            }
            return companion.create(sofort, billingDetails, (Map<String, String>) map);
        }

        @JvmStatic
        public final PaymentMethodCreateParams create(Sofort sofort, PaymentMethod.BillingDetails billingDetails, Map<String, String> map) {
            Intrinsics.checkParameterIsNotNull(sofort, "sofort");
            return new PaymentMethodCreateParams(sofort, billingDetails, (Map) map, (DefaultConstructorMarker) null);
        }

        public static /* synthetic */ PaymentMethodCreateParams createP24$default(Companion companion, PaymentMethod.BillingDetails billingDetails, Map map, int i, Object obj) {
            if ((i & 2) != 0) {
                map = null;
            }
            return companion.createP24(billingDetails, map);
        }

        @JvmStatic
        public final PaymentMethodCreateParams createP24(PaymentMethod.BillingDetails billingDetails, Map<String, String> map) {
            PaymentMethod.BillingDetails billingDetails2 = billingDetails;
            Intrinsics.checkParameterIsNotNull(billingDetails2, "billingDetails");
            return new PaymentMethodCreateParams(Type.P24, (Card) null, (Ideal) null, (Fpx) null, (SepaDebit) null, (AuBecsDebit) null, (BacsDebit) null, (Sofort) null, billingDetails2, map, (Set) null, 1278, (DefaultConstructorMarker) null);
        }

        public static /* synthetic */ PaymentMethodCreateParams createBancontact$default(Companion companion, PaymentMethod.BillingDetails billingDetails, Map map, int i, Object obj) {
            if ((i & 2) != 0) {
                map = null;
            }
            return companion.createBancontact(billingDetails, map);
        }

        @JvmStatic
        public final PaymentMethodCreateParams createBancontact(PaymentMethod.BillingDetails billingDetails, Map<String, String> map) {
            PaymentMethod.BillingDetails billingDetails2 = billingDetails;
            Intrinsics.checkParameterIsNotNull(billingDetails2, "billingDetails");
            return new PaymentMethodCreateParams(Type.Bancontact, (Card) null, (Ideal) null, (Fpx) null, (SepaDebit) null, (AuBecsDebit) null, (BacsDebit) null, (Sofort) null, billingDetails2, map, (Set) null, 1278, (DefaultConstructorMarker) null);
        }

        public static /* synthetic */ PaymentMethodCreateParams createGiropay$default(Companion companion, PaymentMethod.BillingDetails billingDetails, Map map, int i, Object obj) {
            if ((i & 2) != 0) {
                map = null;
            }
            return companion.createGiropay(billingDetails, map);
        }

        @JvmStatic
        public final PaymentMethodCreateParams createGiropay(PaymentMethod.BillingDetails billingDetails, Map<String, String> map) {
            PaymentMethod.BillingDetails billingDetails2 = billingDetails;
            Intrinsics.checkParameterIsNotNull(billingDetails2, "billingDetails");
            return new PaymentMethodCreateParams(Type.Giropay, (Card) null, (Ideal) null, (Fpx) null, (SepaDebit) null, (AuBecsDebit) null, (BacsDebit) null, (Sofort) null, billingDetails2, map, (Set) null, 1278, (DefaultConstructorMarker) null);
        }

        public static /* synthetic */ PaymentMethodCreateParams createEps$default(Companion companion, PaymentMethod.BillingDetails billingDetails, Map map, int i, Object obj) {
            if ((i & 2) != 0) {
                map = null;
            }
            return companion.createEps(billingDetails, map);
        }

        @JvmStatic
        public final PaymentMethodCreateParams createEps(PaymentMethod.BillingDetails billingDetails, Map<String, String> map) {
            PaymentMethod.BillingDetails billingDetails2 = billingDetails;
            Intrinsics.checkParameterIsNotNull(billingDetails2, "billingDetails");
            return new PaymentMethodCreateParams(Type.Eps, (Card) null, (Ideal) null, (Fpx) null, (SepaDebit) null, (AuBecsDebit) null, (BacsDebit) null, (Sofort) null, billingDetails2, map, (Set) null, 1278, (DefaultConstructorMarker) null);
        }

        public static /* synthetic */ PaymentMethodCreateParams createOxxo$stripe_release$default(Companion companion, PaymentMethod.BillingDetails billingDetails, Map map, int i, Object obj) {
            if ((i & 2) != 0) {
                map = null;
            }
            return companion.createOxxo$stripe_release(billingDetails, map);
        }

        public final /* synthetic */ PaymentMethodCreateParams createOxxo$stripe_release(PaymentMethod.BillingDetails billingDetails, Map<String, String> map) {
            PaymentMethod.BillingDetails billingDetails2 = billingDetails;
            Intrinsics.checkParameterIsNotNull(billingDetails2, "billingDetails");
            return new PaymentMethodCreateParams(Type.Oxxo, (Card) null, (Ideal) null, (Fpx) null, (SepaDebit) null, (AuBecsDebit) null, (BacsDebit) null, (Sofort) null, billingDetails2, map, (Set) null, 1278, (DefaultConstructorMarker) null);
        }

        public static /* synthetic */ PaymentMethodCreateParams createAlipay$stripe_release$default(Companion companion, Map map, int i, Object obj) {
            if ((i & 1) != 0) {
                map = null;
            }
            return companion.createAlipay$stripe_release(map);
        }

        public final /* synthetic */ PaymentMethodCreateParams createAlipay$stripe_release(Map<String, String> map) {
            return new PaymentMethodCreateParams(Type.Alipay, (Card) null, (Ideal) null, (Fpx) null, (SepaDebit) null, (AuBecsDebit) null, (BacsDebit) null, (Sofort) null, (PaymentMethod.BillingDetails) null, map, (Set) null, 1534, (DefaultConstructorMarker) null);
        }

        @JvmStatic
        public final PaymentMethodCreateParams createFromGooglePay(JSONObject jSONObject) throws JSONException {
            Intrinsics.checkParameterIsNotNull(jSONObject, "googlePayPaymentData");
            GooglePayResult fromJson = GooglePayResult.Companion.fromJson(jSONObject);
            Token token = fromJson.getToken();
            String id = token != null ? token.getId() : null;
            if (id != null) {
                return create$default(this, Card.Companion.create(id), new PaymentMethod.BillingDetails(fromJson.getAddress(), fromJson.getEmail(), fromJson.getName(), fromJson.getPhoneNumber()), (Map) null, 4, (Object) null);
            }
            throw new IllegalArgumentException("Required value was null.".toString());
        }
    }
}
