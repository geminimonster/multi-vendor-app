package com.stripe.android.model.parsers;

import com.stripe.android.model.Address;
import com.stripe.android.model.PaymentIntent;
import com.stripe.android.model.StripeJsonUtils;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.json.JSONObject;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\b\u0000\u0018\u0000 \u00072\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0003\u0007\b\tB\u0005¢\u0006\u0002\u0010\u0003J\u0012\u0010\u0004\u001a\u0004\u0018\u00010\u00022\u0006\u0010\u0005\u001a\u00020\u0006H\u0016¨\u0006\n"}, d2 = {"Lcom/stripe/android/model/parsers/PaymentIntentJsonParser;", "Lcom/stripe/android/model/parsers/ModelJsonParser;", "Lcom/stripe/android/model/PaymentIntent;", "()V", "parse", "json", "Lorg/json/JSONObject;", "Companion", "ErrorJsonParser", "ShippingJsonParser", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: PaymentIntentJsonParser.kt */
public final class PaymentIntentJsonParser implements ModelJsonParser<PaymentIntent> {
    @Deprecated
    public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
    private static final String FIELD_AMOUNT = "amount";
    private static final String FIELD_CANCELED_AT = "canceled_at";
    private static final String FIELD_CANCELLATION_REASON = "cancellation_reason";
    private static final String FIELD_CAPTURE_METHOD = "capture_method";
    private static final String FIELD_CLIENT_SECRET = "client_secret";
    private static final String FIELD_CONFIRMATION_METHOD = "confirmation_method";
    private static final String FIELD_CREATED = "created";
    private static final String FIELD_CURRENCY = "currency";
    private static final String FIELD_DESCRIPTION = "description";
    private static final String FIELD_ID = "id";
    private static final String FIELD_LAST_PAYMENT_ERROR = "last_payment_error";
    private static final String FIELD_LIVEMODE = "livemode";
    private static final String FIELD_NEXT_ACTION = "next_action";
    private static final String FIELD_OBJECT = "object";
    private static final String FIELD_PAYMENT_METHOD = "payment_method";
    private static final String FIELD_PAYMENT_METHOD_TYPES = "payment_method_types";
    private static final String FIELD_RECEIPT_EMAIL = "receipt_email";
    private static final String FIELD_SETUP_FUTURE_USAGE = "setup_future_usage";
    private static final String FIELD_SHIPPING = "shipping";
    private static final String FIELD_STATUS = "status";
    private static final String OBJECT_TYPE = "payment_intent";

    /* JADX WARNING: Removed duplicated region for block: B:20:0x00ce  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x00da  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x00e4  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x00f0  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00f8  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x0104  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.stripe.android.model.PaymentIntent parse(org.json.JSONObject r29) {
        /*
            r28 = this;
            r0 = r29
            java.lang.String r1 = "json"
            kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(r0, r1)
            java.lang.String r1 = "object"
            java.lang.String r1 = com.stripe.android.model.StripeJsonUtils.optString(r0, r1)
            java.lang.String r2 = "payment_intent"
            boolean r1 = kotlin.jvm.internal.Intrinsics.areEqual((java.lang.Object) r2, (java.lang.Object) r1)
            r2 = 1
            r1 = r1 ^ r2
            r3 = 0
            if (r1 == 0) goto L_0x0019
            return r3
        L_0x0019:
            java.lang.String r1 = "id"
            java.lang.String r5 = com.stripe.android.model.StripeJsonUtils.optString(r0, r1)
            com.stripe.android.model.parsers.ModelJsonParser$Companion r1 = com.stripe.android.model.parsers.ModelJsonParser.Companion
            java.lang.String r4 = "payment_method_types"
            org.json.JSONArray r4 = r0.optJSONArray(r4)
            java.util.List r6 = r1.jsonArrayToList$stripe_release(r4)
            com.stripe.android.model.StripeJsonUtils r1 = com.stripe.android.model.StripeJsonUtils.INSTANCE
            java.lang.String r4 = "amount"
            java.lang.Long r7 = r1.optLong$stripe_release(r0, r4)
            java.lang.String r1 = "canceled_at"
            long r8 = r0.optLong(r1)
            com.stripe.android.model.PaymentIntent$CancellationReason$Companion r1 = com.stripe.android.model.PaymentIntent.CancellationReason.Companion
            java.lang.String r4 = "cancellation_reason"
            java.lang.String r4 = com.stripe.android.model.StripeJsonUtils.optString(r0, r4)
            com.stripe.android.model.PaymentIntent$CancellationReason r10 = r1.fromCode$stripe_release(r4)
            java.lang.String r1 = "capture_method"
            java.lang.String r11 = com.stripe.android.model.StripeJsonUtils.optString(r0, r1)
            java.lang.String r1 = "client_secret"
            java.lang.String r12 = com.stripe.android.model.StripeJsonUtils.optString(r0, r1)
            java.lang.String r1 = "confirmation_method"
            java.lang.String r13 = com.stripe.android.model.StripeJsonUtils.optString(r0, r1)
            java.lang.String r1 = "created"
            long r14 = r0.optLong(r1)
            java.lang.String r1 = "currency"
            java.lang.String r16 = com.stripe.android.model.StripeJsonUtils.optCurrency$stripe_release(r0, r1)
            java.lang.String r1 = "description"
            java.lang.String r17 = com.stripe.android.model.StripeJsonUtils.optString(r0, r1)
            com.stripe.android.model.StripeJsonUtils r1 = com.stripe.android.model.StripeJsonUtils.INSTANCE
            java.lang.String r4 = "livemode"
            boolean r18 = r1.optBoolean$stripe_release(r0, r4)
            java.lang.String r1 = "payment_method"
            org.json.JSONObject r4 = r0.optJSONObject(r1)
            if (r4 == 0) goto L_0x0083
            com.stripe.android.model.parsers.PaymentMethodJsonParser r2 = new com.stripe.android.model.parsers.PaymentMethodJsonParser
            r2.<init>()
            com.stripe.android.model.PaymentMethod r2 = r2.parse((org.json.JSONObject) r4)
            goto L_0x0084
        L_0x0083:
            r2 = r3
        L_0x0084:
            java.lang.String r1 = com.stripe.android.model.StripeJsonUtils.optString(r0, r1)
            if (r2 != 0) goto L_0x008d
            r19 = 1
            goto L_0x0090
        L_0x008d:
            r4 = 0
            r19 = 0
        L_0x0090:
            if (r19 == 0) goto L_0x0093
            goto L_0x0094
        L_0x0093:
            r1 = r3
        L_0x0094:
            if (r1 == 0) goto L_0x0099
        L_0x0096:
            r21 = r1
            goto L_0x00a0
        L_0x0099:
            if (r2 == 0) goto L_0x009e
            java.lang.String r1 = r2.id
            goto L_0x0096
        L_0x009e:
            r21 = r3
        L_0x00a0:
            java.lang.String r1 = "receipt_email"
            java.lang.String r22 = com.stripe.android.model.StripeJsonUtils.optString(r0, r1)
            com.stripe.android.model.StripeIntent$Status$Companion r1 = com.stripe.android.model.StripeIntent.Status.Companion
            java.lang.String r4 = "status"
            java.lang.String r4 = com.stripe.android.model.StripeJsonUtils.optString(r0, r4)
            com.stripe.android.model.StripeIntent$Status r23 = r1.fromCode$stripe_release(r4)
            com.stripe.android.model.StripeIntent$Usage$Companion r1 = com.stripe.android.model.StripeIntent.Usage.Companion
            java.lang.String r4 = "setup_future_usage"
            java.lang.String r4 = com.stripe.android.model.StripeJsonUtils.optString(r0, r4)
            com.stripe.android.model.StripeIntent$Usage r24 = r1.fromCode$stripe_release(r4)
            com.stripe.android.model.StripeJsonUtils r1 = com.stripe.android.model.StripeJsonUtils.INSTANCE
            java.lang.String r4 = "next_action"
            java.util.Map r19 = r1.optMap$stripe_release(r0, r4)
            java.lang.String r1 = "last_payment_error"
            org.json.JSONObject r1 = r0.optJSONObject(r1)
            if (r1 == 0) goto L_0x00da
            com.stripe.android.model.parsers.PaymentIntentJsonParser$ErrorJsonParser r3 = new com.stripe.android.model.parsers.PaymentIntentJsonParser$ErrorJsonParser
            r3.<init>()
            com.stripe.android.model.PaymentIntent$Error r1 = r3.parse((org.json.JSONObject) r1)
            r25 = r1
            goto L_0x00dc
        L_0x00da:
            r25 = 0
        L_0x00dc:
            java.lang.String r1 = "shipping"
            org.json.JSONObject r1 = r0.optJSONObject(r1)
            if (r1 == 0) goto L_0x00f0
            com.stripe.android.model.parsers.PaymentIntentJsonParser$ShippingJsonParser r3 = new com.stripe.android.model.parsers.PaymentIntentJsonParser$ShippingJsonParser
            r3.<init>()
            com.stripe.android.model.PaymentIntent$Shipping r1 = r3.parse((org.json.JSONObject) r1)
            r26 = r1
            goto L_0x00f2
        L_0x00f0:
            r26 = 0
        L_0x00f2:
            org.json.JSONObject r0 = r0.optJSONObject(r4)
            if (r0 == 0) goto L_0x0104
            com.stripe.android.model.parsers.NextActionDataParser r1 = new com.stripe.android.model.parsers.NextActionDataParser
            r1.<init>()
            com.stripe.android.model.StripeIntent$NextActionData r3 = r1.parse((org.json.JSONObject) r0)
            r27 = r3
            goto L_0x0106
        L_0x0104:
            r27 = 0
        L_0x0106:
            com.stripe.android.model.PaymentIntent r0 = new com.stripe.android.model.PaymentIntent
            r4 = r0
            r20 = r2
            r4.<init>(r5, r6, r7, r8, r10, r11, r12, r13, r14, r16, r17, r18, r19, r20, r21, r22, r23, r24, r25, r26, r27)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.stripe.android.model.parsers.PaymentIntentJsonParser.parse(org.json.JSONObject):com.stripe.android.model.PaymentIntent");
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0000\u0018\u0000 \u00072\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0007B\u0005¢\u0006\u0002\u0010\u0003J\u0010\u0010\u0004\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0006H\u0016¨\u0006\b"}, d2 = {"Lcom/stripe/android/model/parsers/PaymentIntentJsonParser$ErrorJsonParser;", "Lcom/stripe/android/model/parsers/ModelJsonParser;", "Lcom/stripe/android/model/PaymentIntent$Error;", "()V", "parse", "json", "Lorg/json/JSONObject;", "Companion", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: PaymentIntentJsonParser.kt */
    public static final class ErrorJsonParser implements ModelJsonParser<PaymentIntent.Error> {
        @Deprecated
        public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
        private static final String FIELD_CHARGE = "charge";
        private static final String FIELD_CODE = "code";
        private static final String FIELD_DECLINE_CODE = "decline_code";
        private static final String FIELD_DOC_URL = "doc_url";
        private static final String FIELD_MESSAGE = "message";
        private static final String FIELD_PARAM = "param";
        private static final String FIELD_PAYMENT_METHOD = "payment_method";
        private static final String FIELD_TYPE = "type";

        public PaymentIntent.Error parse(JSONObject jSONObject) {
            Intrinsics.checkParameterIsNotNull(jSONObject, "json");
            String optString = StripeJsonUtils.optString(jSONObject, FIELD_CHARGE);
            String optString2 = StripeJsonUtils.optString(jSONObject, FIELD_CODE);
            String optString3 = StripeJsonUtils.optString(jSONObject, FIELD_DECLINE_CODE);
            String optString4 = StripeJsonUtils.optString(jSONObject, FIELD_DOC_URL);
            String optString5 = StripeJsonUtils.optString(jSONObject, "message");
            String optString6 = StripeJsonUtils.optString(jSONObject, FIELD_PARAM);
            JSONObject optJSONObject = jSONObject.optJSONObject("payment_method");
            return new PaymentIntent.Error(optString, optString2, optString3, optString4, optString5, optString6, optJSONObject != null ? new PaymentMethodJsonParser().parse(optJSONObject) : null, PaymentIntent.Error.Type.Companion.fromCode$stripe_release(StripeJsonUtils.optString(jSONObject, "type")));
        }

        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\b\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\f"}, d2 = {"Lcom/stripe/android/model/parsers/PaymentIntentJsonParser$ErrorJsonParser$Companion;", "", "()V", "FIELD_CHARGE", "", "FIELD_CODE", "FIELD_DECLINE_CODE", "FIELD_DOC_URL", "FIELD_MESSAGE", "FIELD_PARAM", "FIELD_PAYMENT_METHOD", "FIELD_TYPE", "stripe_release"}, k = 1, mv = {1, 1, 16})
        /* compiled from: PaymentIntentJsonParser.kt */
        private static final class Companion {
            private Companion() {
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0000\u0018\u0000 \u00072\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0007B\u0005¢\u0006\u0002\u0010\u0003J\u0012\u0010\u0004\u001a\u0004\u0018\u00010\u00022\u0006\u0010\u0005\u001a\u00020\u0006H\u0016¨\u0006\b"}, d2 = {"Lcom/stripe/android/model/parsers/PaymentIntentJsonParser$ShippingJsonParser;", "Lcom/stripe/android/model/parsers/ModelJsonParser;", "Lcom/stripe/android/model/PaymentIntent$Shipping;", "()V", "parse", "json", "Lorg/json/JSONObject;", "Companion", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: PaymentIntentJsonParser.kt */
    public static final class ShippingJsonParser implements ModelJsonParser<PaymentIntent.Shipping> {
        @Deprecated
        public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
        private static final String FIELD_ADDRESS = "address";
        private static final String FIELD_CARRIER = "carrier";
        private static final String FIELD_NAME = "name";
        private static final String FIELD_PHONE = "phone";
        private static final String FIELD_TRACKING_NUMBER = "tracking_number";

        public PaymentIntent.Shipping parse(JSONObject jSONObject) {
            Address address;
            JSONObject jSONObject2 = jSONObject;
            Intrinsics.checkParameterIsNotNull(jSONObject2, "json");
            JSONObject optJSONObject = jSONObject2.optJSONObject("address");
            if (optJSONObject == null || (address = new AddressJsonParser().parse(optJSONObject)) == null) {
                address = new Address((String) null, (String) null, (String) null, (String) null, (String) null, (String) null, 63, (DefaultConstructorMarker) null);
            }
            return new PaymentIntent.Shipping(address, StripeJsonUtils.optString(jSONObject2, FIELD_CARRIER), StripeJsonUtils.optString(jSONObject2, "name"), StripeJsonUtils.optString(jSONObject2, "phone"), StripeJsonUtils.optString(jSONObject2, FIELD_TRACKING_NUMBER));
        }

        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0005\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\t"}, d2 = {"Lcom/stripe/android/model/parsers/PaymentIntentJsonParser$ShippingJsonParser$Companion;", "", "()V", "FIELD_ADDRESS", "", "FIELD_CARRIER", "FIELD_NAME", "FIELD_PHONE", "FIELD_TRACKING_NUMBER", "stripe_release"}, k = 1, mv = {1, 1, 16})
        /* compiled from: PaymentIntentJsonParser.kt */
        private static final class Companion {
            private Companion() {
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0015\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0017\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0018\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\u0019"}, d2 = {"Lcom/stripe/android/model/parsers/PaymentIntentJsonParser$Companion;", "", "()V", "FIELD_AMOUNT", "", "FIELD_CANCELED_AT", "FIELD_CANCELLATION_REASON", "FIELD_CAPTURE_METHOD", "FIELD_CLIENT_SECRET", "FIELD_CONFIRMATION_METHOD", "FIELD_CREATED", "FIELD_CURRENCY", "FIELD_DESCRIPTION", "FIELD_ID", "FIELD_LAST_PAYMENT_ERROR", "FIELD_LIVEMODE", "FIELD_NEXT_ACTION", "FIELD_OBJECT", "FIELD_PAYMENT_METHOD", "FIELD_PAYMENT_METHOD_TYPES", "FIELD_RECEIPT_EMAIL", "FIELD_SETUP_FUTURE_USAGE", "FIELD_SHIPPING", "FIELD_STATUS", "OBJECT_TYPE", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: PaymentIntentJsonParser.kt */
    private static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }
}
