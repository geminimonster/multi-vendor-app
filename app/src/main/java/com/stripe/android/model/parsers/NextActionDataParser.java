package com.stripe.android.model.parsers;

import android.net.Uri;
import com.stripe.android.model.StripeIntent;
import com.stripe.android.model.StripeJsonUtils;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.json.JSONObject;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0000\u0018\u0000 \u00072\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0004\u0007\b\t\nB\u0005¢\u0006\u0002\u0010\u0003J\u0012\u0010\u0004\u001a\u0004\u0018\u00010\u00022\u0006\u0010\u0005\u001a\u00020\u0006H\u0016¨\u0006\u000b"}, d2 = {"Lcom/stripe/android/model/parsers/NextActionDataParser;", "Lcom/stripe/android/model/parsers/ModelJsonParser;", "Lcom/stripe/android/model/StripeIntent$NextActionData;", "()V", "parse", "json", "Lorg/json/JSONObject;", "Companion", "DisplayOxxoDetailsJsonParser", "RedirectToUrlParser", "SdkDataJsonParser", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: NextActionDataParser.kt */
public final class NextActionDataParser implements ModelJsonParser<StripeIntent.NextActionData> {
    @Deprecated
    public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
    private static final String FIELD_NEXT_ACTION_TYPE = "type";

    @Metadata(bv = {1, 0, 3}, k = 3, mv = {1, 1, 16})
    public final /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;

        static {
            int[] iArr = new int[StripeIntent.NextActionType.values().length];
            $EnumSwitchMapping$0 = iArr;
            iArr[StripeIntent.NextActionType.DisplayOxxoDetails.ordinal()] = 1;
            $EnumSwitchMapping$0[StripeIntent.NextActionType.RedirectToUrl.ordinal()] = 2;
            $EnumSwitchMapping$0[StripeIntent.NextActionType.UseStripeSdk.ordinal()] = 3;
        }
    }

    public StripeIntent.NextActionData parse(JSONObject jSONObject) {
        ModelJsonParser modelJsonParser;
        Intrinsics.checkParameterIsNotNull(jSONObject, "json");
        StripeIntent.NextActionType fromCode$stripe_release = StripeIntent.NextActionType.Companion.fromCode$stripe_release(jSONObject.optString("type"));
        if (fromCode$stripe_release == null) {
            return null;
        }
        int i = WhenMappings.$EnumSwitchMapping$0[fromCode$stripe_release.ordinal()];
        if (i == 1) {
            modelJsonParser = new DisplayOxxoDetailsJsonParser();
        } else if (i == 2) {
            modelJsonParser = new RedirectToUrlParser();
        } else if (i != 3) {
            return null;
        } else {
            modelJsonParser = new SdkDataJsonParser();
        }
        JSONObject optJSONObject = jSONObject.optJSONObject(fromCode$stripe_release.getCode());
        if (optJSONObject == null) {
            optJSONObject = new JSONObject();
        }
        return (StripeIntent.NextActionData) modelJsonParser.parse(optJSONObject);
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0002\u0018\u0000 \u00072\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0007B\u0005¢\u0006\u0002\u0010\u0003J\u0012\u0010\u0004\u001a\u0004\u0018\u00010\u00022\u0006\u0010\u0005\u001a\u00020\u0006H\u0016¨\u0006\b"}, d2 = {"Lcom/stripe/android/model/parsers/NextActionDataParser$DisplayOxxoDetailsJsonParser;", "Lcom/stripe/android/model/parsers/ModelJsonParser;", "Lcom/stripe/android/model/StripeIntent$NextActionData$DisplayOxxoDetails;", "()V", "parse", "json", "Lorg/json/JSONObject;", "Companion", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: NextActionDataParser.kt */
    private static final class DisplayOxxoDetailsJsonParser implements ModelJsonParser<StripeIntent.NextActionData.DisplayOxxoDetails> {
        @Deprecated
        public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
        private static final String FIELD_EXPIRES_AFTER = "expires_after";
        private static final String FIELD_NUMBER = "number";

        public StripeIntent.NextActionData.DisplayOxxoDetails parse(JSONObject jSONObject) {
            Intrinsics.checkParameterIsNotNull(jSONObject, "json");
            return new StripeIntent.NextActionData.DisplayOxxoDetails(jSONObject.optInt(FIELD_EXPIRES_AFTER), StripeJsonUtils.optString(jSONObject, FIELD_NUMBER));
        }

        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\u0006"}, d2 = {"Lcom/stripe/android/model/parsers/NextActionDataParser$DisplayOxxoDetailsJsonParser$Companion;", "", "()V", "FIELD_EXPIRES_AFTER", "", "FIELD_NUMBER", "stripe_release"}, k = 1, mv = {1, 1, 16})
        /* compiled from: NextActionDataParser.kt */
        private static final class Companion {
            private Companion() {
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u0000\u0018\u0000 \u00072\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0002\u0007\bB\u0005¢\u0006\u0002\u0010\u0003J\u0012\u0010\u0004\u001a\u0004\u0018\u00010\u00022\u0006\u0010\u0005\u001a\u00020\u0006H\u0016¨\u0006\t"}, d2 = {"Lcom/stripe/android/model/parsers/NextActionDataParser$RedirectToUrlParser;", "Lcom/stripe/android/model/parsers/ModelJsonParser;", "Lcom/stripe/android/model/StripeIntent$NextActionData$RedirectToUrl;", "()V", "parse", "json", "Lorg/json/JSONObject;", "Companion", "MobileDataParser", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: NextActionDataParser.kt */
    public static final class RedirectToUrlParser implements ModelJsonParser<StripeIntent.NextActionData.RedirectToUrl> {
        @Deprecated
        public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
        public static final String FIELD_MOBILE = "mobile";
        public static final String FIELD_RETURN_URL = "return_url";
        public static final String FIELD_URL = "url";

        public StripeIntent.NextActionData.RedirectToUrl parse(JSONObject jSONObject) {
            Intrinsics.checkParameterIsNotNull(jSONObject, "json");
            if (!jSONObject.has("url")) {
                return null;
            }
            Uri parse = Uri.parse(jSONObject.getString("url"));
            Intrinsics.checkExpressionValueIsNotNull(parse, "Uri.parse(json.getString(FIELD_URL))");
            String optString = jSONObject.optString("return_url");
            MobileDataParser mobileDataParser = new MobileDataParser();
            JSONObject optJSONObject = jSONObject.optJSONObject(FIELD_MOBILE);
            if (optJSONObject == null) {
                optJSONObject = new JSONObject();
            }
            return new StripeIntent.NextActionData.RedirectToUrl(parse, optString, mobileDataParser.parse(optJSONObject));
        }

        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0000\u0018\u0000 \u00072\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0007B\u0005¢\u0006\u0002\u0010\u0003J\u0012\u0010\u0004\u001a\u0004\u0018\u00010\u00022\u0006\u0010\u0005\u001a\u00020\u0006H\u0016¨\u0006\b"}, d2 = {"Lcom/stripe/android/model/parsers/NextActionDataParser$RedirectToUrlParser$MobileDataParser;", "Lcom/stripe/android/model/parsers/ModelJsonParser;", "Lcom/stripe/android/model/StripeIntent$NextActionData$RedirectToUrl$MobileData;", "()V", "parse", "json", "Lorg/json/JSONObject;", "Companion", "stripe_release"}, k = 1, mv = {1, 1, 16})
        /* compiled from: NextActionDataParser.kt */
        public static final class MobileDataParser implements ModelJsonParser<StripeIntent.NextActionData.RedirectToUrl.MobileData> {
            @Deprecated
            public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
            public static final String FIELD_DATA = "data";
            public static final String FIELD_TYPE = "type";

            @Metadata(bv = {1, 0, 3}, k = 3, mv = {1, 1, 16})
            public final /* synthetic */ class WhenMappings {
                public static final /* synthetic */ int[] $EnumSwitchMapping$0;

                static {
                    int[] iArr = new int[Companion.Type.values().length];
                    $EnumSwitchMapping$0 = iArr;
                    iArr[Companion.Type.Alipay.ordinal()] = 1;
                }
            }

            public StripeIntent.NextActionData.RedirectToUrl.MobileData parse(JSONObject jSONObject) {
                Intrinsics.checkParameterIsNotNull(jSONObject, "json");
                Companion.Type fromCode$stripe_release = Companion.Type.Companion.fromCode$stripe_release(StripeJsonUtils.optString(jSONObject, "type"));
                StripeIntent.NextActionData.RedirectToUrl.MobileData.Alipay alipay = null;
                JSONObject optJSONObject = fromCode$stripe_release != null ? jSONObject.optJSONObject(fromCode$stripe_release.getCode$stripe_release()) : null;
                if (fromCode$stripe_release == null || WhenMappings.$EnumSwitchMapping$0[fromCode$stripe_release.ordinal()] != 1) {
                    return null;
                }
                if (optJSONObject != null) {
                    String optString = optJSONObject.optString("data");
                    Intrinsics.checkExpressionValueIsNotNull(optString, "it.optString(FIELD_DATA)");
                    alipay = new StripeIntent.NextActionData.RedirectToUrl.MobileData.Alipay(optString);
                }
                return alipay;
            }

            @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\b\u0003\u0018\u00002\u00020\u0001:\u0001\u0006B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\u0007"}, d2 = {"Lcom/stripe/android/model/parsers/NextActionDataParser$RedirectToUrlParser$MobileDataParser$Companion;", "", "()V", "FIELD_DATA", "", "FIELD_TYPE", "Type", "stripe_release"}, k = 1, mv = {1, 1, 16})
            /* compiled from: NextActionDataParser.kt */
            private static final class Companion {
                private Companion() {
                }

                public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                    this();
                }

                @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0006\b\u0001\u0018\u0000 \b2\b\u0012\u0004\u0012\u00020\u00000\u0001:\u0001\bB\u000f\b\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004R\u0014\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006j\u0002\b\u0007¨\u0006\t"}, d2 = {"Lcom/stripe/android/model/parsers/NextActionDataParser$RedirectToUrlParser$MobileDataParser$Companion$Type;", "", "code", "", "(Ljava/lang/String;ILjava/lang/String;)V", "getCode$stripe_release", "()Ljava/lang/String;", "Alipay", "Companion", "stripe_release"}, k = 1, mv = {1, 1, 16})
                /* compiled from: NextActionDataParser.kt */
                public enum Type {
                    Alipay("alipay");
                    
                    public static final C0050Companion Companion = null;
                    private final String code;

                    private Type(String str) {
                        this.code = str;
                    }

                    public final String getCode$stripe_release() {
                        return this.code;
                    }

                    static {
                        Companion = new C0050Companion((DefaultConstructorMarker) null);
                    }

                    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0019\u0010\u0003\u001a\u0004\u0018\u00010\u00042\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006H\u0000¢\u0006\u0002\b\u0007¨\u0006\b"}, d2 = {"Lcom/stripe/android/model/parsers/NextActionDataParser$RedirectToUrlParser$MobileDataParser$Companion$Type$Companion;", "", "()V", "fromCode", "Lcom/stripe/android/model/parsers/NextActionDataParser$RedirectToUrlParser$MobileDataParser$Companion$Type;", "code", "", "fromCode$stripe_release", "stripe_release"}, k = 1, mv = {1, 1, 16})
                    /* renamed from: com.stripe.android.model.parsers.NextActionDataParser$RedirectToUrlParser$MobileDataParser$Companion$Type$Companion  reason: collision with other inner class name */
                    /* compiled from: NextActionDataParser.kt */
                    public static final class C0050Companion {
                        private C0050Companion() {
                        }

                        public /* synthetic */ C0050Companion(DefaultConstructorMarker defaultConstructorMarker) {
                            this();
                        }

                        public final Type fromCode$stripe_release(String str) {
                            for (Type type : Type.values()) {
                                if (Intrinsics.areEqual((Object) type.getCode$stripe_release(), (Object) str)) {
                                    return type;
                                }
                            }
                            return null;
                        }
                    }
                }
            }
        }

        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\u0007"}, d2 = {"Lcom/stripe/android/model/parsers/NextActionDataParser$RedirectToUrlParser$Companion;", "", "()V", "FIELD_MOBILE", "", "FIELD_RETURN_URL", "FIELD_URL", "stripe_release"}, k = 1, mv = {1, 1, 16})
        /* compiled from: NextActionDataParser.kt */
        private static final class Companion {
            private Companion() {
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0002\u0018\u0000 \t2\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\tB\u0005¢\u0006\u0002\u0010\u0003J\u0012\u0010\u0004\u001a\u0004\u0018\u00010\u00022\u0006\u0010\u0005\u001a\u00020\u0006H\u0016J\u0010\u0010\u0007\u001a\u00020\b2\u0006\u0010\u0005\u001a\u00020\u0006H\u0002¨\u0006\n"}, d2 = {"Lcom/stripe/android/model/parsers/NextActionDataParser$SdkDataJsonParser;", "Lcom/stripe/android/model/parsers/ModelJsonParser;", "Lcom/stripe/android/model/StripeIntent$NextActionData$SdkData;", "()V", "parse", "json", "Lorg/json/JSONObject;", "parseDirectoryServerEncryption", "Lcom/stripe/android/model/StripeIntent$NextActionData$SdkData$Use3DS2$DirectoryServerEncryption;", "Companion", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: NextActionDataParser.kt */
    private static final class SdkDataJsonParser implements ModelJsonParser<StripeIntent.NextActionData.SdkData> {
        @Deprecated
        public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
        private static final String FIELD_CERTIFICATE = "certificate";
        private static final String FIELD_DIRECTORY_SERVER_ENCRYPTION = "directory_server_encryption";
        private static final String FIELD_DIRECTORY_SERVER_ID = "directory_server_id";
        private static final String FIELD_DIRECTORY_SERVER_NAME = "directory_server_name";
        private static final String FIELD_KEY_ID = "key_id";
        private static final String FIELD_ROOT_CAS = "root_certificate_authorities";
        private static final String FIELD_SERVER_TRANSACTION_ID = "server_transaction_id";
        private static final String FIELD_STRIPE_JS = "stripe_js";
        private static final String FIELD_THREE_D_SECURE_2_SOURCE = "three_d_secure_2_source";
        private static final String FIELD_TYPE = "type";
        private static final String TYPE_3DS1 = "three_d_secure_redirect";
        private static final String TYPE_3DS2 = "stripe_3ds2_fingerprint";

        public StripeIntent.NextActionData.SdkData parse(JSONObject jSONObject) {
            Intrinsics.checkParameterIsNotNull(jSONObject, "json");
            String optString = StripeJsonUtils.optString(jSONObject, "type");
            if (optString != null) {
                int hashCode = optString.hashCode();
                if (hashCode != -60303033) {
                    if (hashCode == 1383888232 && optString.equals(TYPE_3DS1)) {
                        String optString2 = jSONObject.optString(FIELD_STRIPE_JS);
                        Intrinsics.checkExpressionValueIsNotNull(optString2, "json.optString(FIELD_STRIPE_JS)");
                        return new StripeIntent.NextActionData.SdkData.Use3DS1(optString2);
                    }
                } else if (optString.equals(TYPE_3DS2)) {
                    String optString3 = jSONObject.optString(FIELD_THREE_D_SECURE_2_SOURCE);
                    Intrinsics.checkExpressionValueIsNotNull(optString3, "json.optString(FIELD_THREE_D_SECURE_2_SOURCE)");
                    String optString4 = jSONObject.optString(FIELD_DIRECTORY_SERVER_NAME);
                    Intrinsics.checkExpressionValueIsNotNull(optString4, "json.optString(FIELD_DIRECTORY_SERVER_NAME)");
                    String optString5 = jSONObject.optString(FIELD_SERVER_TRANSACTION_ID);
                    Intrinsics.checkExpressionValueIsNotNull(optString5, "json.optString(FIELD_SERVER_TRANSACTION_ID)");
                    JSONObject optJSONObject = jSONObject.optJSONObject(FIELD_DIRECTORY_SERVER_ENCRYPTION);
                    if (optJSONObject == null) {
                        optJSONObject = new JSONObject();
                    }
                    return new StripeIntent.NextActionData.SdkData.Use3DS2(optString3, optString4, optString5, parseDirectoryServerEncryption(optJSONObject));
                }
            }
            return null;
        }

        /* JADX WARNING: Code restructure failed: missing block: B:8:0x002d, code lost:
            if (r1 != null) goto L_0x0034;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        private final com.stripe.android.model.StripeIntent.NextActionData.SdkData.Use3DS2.DirectoryServerEncryption parseDirectoryServerEncryption(org.json.JSONObject r6) {
            /*
                r5 = this;
                com.stripe.android.model.StripeJsonUtils r0 = com.stripe.android.model.StripeJsonUtils.INSTANCE
                java.lang.String r1 = "root_certificate_authorities"
                org.json.JSONArray r1 = r6.optJSONArray(r1)
                java.util.List r0 = r0.jsonArrayToList$stripe_release(r1)
                if (r0 == 0) goto L_0x0030
                java.lang.Iterable r0 = (java.lang.Iterable) r0
                java.util.List r1 = kotlin.collections.CollectionsKt.emptyList()
                java.util.Iterator r0 = r0.iterator()
            L_0x0018:
                boolean r2 = r0.hasNext()
                if (r2 == 0) goto L_0x002d
                java.lang.Object r2 = r0.next()
                boolean r3 = r2 instanceof java.lang.String
                if (r3 == 0) goto L_0x0018
                java.util.Collection r1 = (java.util.Collection) r1
                java.util.List r1 = kotlin.collections.CollectionsKt.plus(r1, r2)
                goto L_0x0018
            L_0x002d:
                if (r1 == 0) goto L_0x0030
                goto L_0x0034
            L_0x0030:
                java.util.List r1 = kotlin.collections.CollectionsKt.emptyList()
            L_0x0034:
                com.stripe.android.model.StripeIntent$NextActionData$SdkData$Use3DS2$DirectoryServerEncryption r0 = new com.stripe.android.model.StripeIntent$NextActionData$SdkData$Use3DS2$DirectoryServerEncryption
                java.lang.String r2 = "directory_server_id"
                java.lang.String r2 = r6.optString(r2)
                java.lang.String r3 = "json.optString(FIELD_DIRECTORY_SERVER_ID)"
                kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r2, r3)
                java.lang.String r3 = "certificate"
                java.lang.String r3 = r6.optString(r3)
                java.lang.String r4 = "json.optString(FIELD_CERTIFICATE)"
                kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r3, r4)
                java.lang.String r4 = "key_id"
                java.lang.String r6 = r6.optString(r4)
                r0.<init>(r2, r3, r1, r6)
                return r0
            */
            throw new UnsupportedOperationException("Method not decompiled: com.stripe.android.model.parsers.NextActionDataParser.SdkDataJsonParser.parseDirectoryServerEncryption(org.json.JSONObject):com.stripe.android.model.StripeIntent$NextActionData$SdkData$Use3DS2$DirectoryServerEncryption");
        }

        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\f\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\u0010"}, d2 = {"Lcom/stripe/android/model/parsers/NextActionDataParser$SdkDataJsonParser$Companion;", "", "()V", "FIELD_CERTIFICATE", "", "FIELD_DIRECTORY_SERVER_ENCRYPTION", "FIELD_DIRECTORY_SERVER_ID", "FIELD_DIRECTORY_SERVER_NAME", "FIELD_KEY_ID", "FIELD_ROOT_CAS", "FIELD_SERVER_TRANSACTION_ID", "FIELD_STRIPE_JS", "FIELD_THREE_D_SECURE_2_SOURCE", "FIELD_TYPE", "TYPE_3DS1", "TYPE_3DS2", "stripe_release"}, k = 1, mv = {1, 1, 16})
        /* compiled from: NextActionDataParser.kt */
        private static final class Companion {
            private Companion() {
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\u0005"}, d2 = {"Lcom/stripe/android/model/parsers/NextActionDataParser$Companion;", "", "()V", "FIELD_NEXT_ACTION_TYPE", "", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: NextActionDataParser.kt */
    private static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }
}
