package com.stripe.android.model;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import kotlin.Metadata;
import kotlin.Pair;
import kotlin.TuplesKt;
import kotlin.collections.CollectionsKt;
import kotlin.collections.MapsKt;
import kotlin.collections.SetsKt;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000P\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\"\n\u0002\b#\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\b\b\u0018\u0000 ?2\u00020\u00012\u00020\u0002:\u0001?BO\b\u0017\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0006\u0012\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\u0004\u0012\n\b\u0002\u0010\t\u001a\u0004\u0018\u00010\u0004\u0012\n\b\u0002\u0010\n\u001a\u0004\u0018\u00010\u000b\u0012\n\b\u0002\u0010\f\u001a\u0004\u0018\u00010\u0004¢\u0006\u0002\u0010\rB_\b\u0000\u0012\u000e\b\u0002\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\u00040\u000f\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0006\u0012\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\u0004\u0012\n\b\u0002\u0010\t\u001a\u0004\u0018\u00010\u0004\u0012\n\b\u0002\u0010\n\u001a\u0004\u0018\u00010\u000b\u0012\n\b\u0002\u0010\f\u001a\u0004\u0018\u00010\u0004¢\u0006\u0002\u0010\u0010J\u0014\u0010'\u001a\b\u0012\u0004\u0012\u00020\u00040\u000fHÀ\u0003¢\u0006\u0002\b(J\t\u0010)\u001a\u00020\u0004HÆ\u0003J\t\u0010*\u001a\u00020\u0006HÆ\u0003J\t\u0010+\u001a\u00020\u0006HÆ\u0003J\u000b\u0010,\u001a\u0004\u0018\u00010\u0004HÆ\u0003J\u000b\u0010-\u001a\u0004\u0018\u00010\u0004HÆ\u0003J\u000b\u0010.\u001a\u0004\u0018\u00010\u000bHÆ\u0003J\u000b\u0010/\u001a\u0004\u0018\u00010\u0004HÆ\u0003Jg\u00100\u001a\u00020\u00002\u000e\b\u0002\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\u00040\u000f2\b\b\u0002\u0010\u0003\u001a\u00020\u00042\b\b\u0002\u0010\u0005\u001a\u00020\u00062\b\b\u0002\u0010\u0007\u001a\u00020\u00062\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\u00042\n\b\u0002\u0010\t\u001a\u0004\u0018\u00010\u00042\n\b\u0002\u0010\n\u001a\u0004\u0018\u00010\u000b2\n\b\u0002\u0010\f\u001a\u0004\u0018\u00010\u0004HÆ\u0001J\t\u00101\u001a\u00020\u0006HÖ\u0001J\u0013\u00102\u001a\u0002032\b\u00104\u001a\u0004\u0018\u000105HÖ\u0003J\t\u00106\u001a\u00020\u0006HÖ\u0001J\u0014\u00107\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020508H\u0016J\t\u00109\u001a\u00020\u0004HÖ\u0001J\u0019\u0010:\u001a\u00020;2\u0006\u0010<\u001a\u00020=2\u0006\u0010>\u001a\u00020\u0006HÖ\u0001R\u001c\u0010\n\u001a\u0004\u0018\u00010\u000bX\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0011\u0010\u0012\"\u0004\b\u0013\u0010\u0014R\u001a\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\u00040\u000fX\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0015\u0010\u0016R\u001c\u0010\f\u001a\u0004\u0018\u00010\u0004X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0017\u0010\u0018\"\u0004\b\u0019\u0010\u001aR\u001c\u0010\b\u001a\u0004\u0018\u00010\u0004X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u001b\u0010\u0018\"\u0004\b\u001c\u0010\u001aR\u001a\u0010\u0005\u001a\u00020\u0006X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u001d\u0010\u001e\"\u0004\b\u001f\u0010 R\u001a\u0010\u0007\u001a\u00020\u0006X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b!\u0010\u001e\"\u0004\b\"\u0010 R\u001c\u0010\t\u001a\u0004\u0018\u00010\u0004X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b#\u0010\u0018\"\u0004\b$\u0010\u001aR\u001a\u0010\u0003\u001a\u00020\u0004X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b%\u0010\u0018\"\u0004\b&\u0010\u001a¨\u0006@"}, d2 = {"Lcom/stripe/android/model/CardParams;", "Lcom/stripe/android/model/StripeParamsModel;", "Landroid/os/Parcelable;", "number", "", "expMonth", "", "expYear", "cvc", "name", "address", "Lcom/stripe/android/model/Address;", "currency", "(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Lcom/stripe/android/model/Address;Ljava/lang/String;)V", "attribution", "", "(Ljava/util/Set;Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Lcom/stripe/android/model/Address;Ljava/lang/String;)V", "getAddress", "()Lcom/stripe/android/model/Address;", "setAddress", "(Lcom/stripe/android/model/Address;)V", "getAttribution$stripe_release", "()Ljava/util/Set;", "getCurrency", "()Ljava/lang/String;", "setCurrency", "(Ljava/lang/String;)V", "getCvc", "setCvc", "getExpMonth", "()I", "setExpMonth", "(I)V", "getExpYear", "setExpYear", "getName", "setName", "getNumber", "setNumber", "component1", "component1$stripe_release", "component2", "component3", "component4", "component5", "component6", "component7", "component8", "copy", "describeContents", "equals", "", "other", "", "hashCode", "toParamMap", "", "toString", "writeToParcel", "", "parcel", "Landroid/os/Parcel;", "flags", "Companion", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: CardParams.kt */
public final class CardParams implements StripeParamsModel, Parcelable {
    public static final Parcelable.Creator CREATOR = new Creator();
    @Deprecated
    public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
    private static final String PARAM_ADDRESS_CITY = "address_city";
    private static final String PARAM_ADDRESS_COUNTRY = "address_country";
    private static final String PARAM_ADDRESS_LINE1 = "address_line1";
    private static final String PARAM_ADDRESS_LINE2 = "address_line2";
    private static final String PARAM_ADDRESS_STATE = "address_state";
    private static final String PARAM_ADDRESS_ZIP = "address_zip";
    private static final String PARAM_CURRENCY = "currency";
    private static final String PARAM_CVC = "cvc";
    private static final String PARAM_EXP_MONTH = "exp_month";
    private static final String PARAM_EXP_YEAR = "exp_year";
    private static final String PARAM_NAME = "name";
    private static final String PARAM_NUMBER = "number";
    private Address address;
    private final Set<String> attribution;
    private String currency;
    private String cvc;
    private int expMonth;
    private int expYear;
    private String name;
    private String number;

    @Metadata(bv = {1, 0, 3}, k = 3, mv = {1, 1, 16})
    public static class Creator implements Parcelable.Creator {
        public final Object createFromParcel(Parcel parcel) {
            String readString;
            Intrinsics.checkParameterIsNotNull(parcel, "in");
            int readInt = parcel.readInt();
            LinkedHashSet linkedHashSet = new LinkedHashSet(readInt);
            while (true) {
                readString = parcel.readString();
                if (readInt == 0) {
                    break;
                }
                linkedHashSet.add(readString);
                readInt--;
            }
            return new CardParams(linkedHashSet, readString, parcel.readInt(), parcel.readInt(), parcel.readString(), parcel.readString(), parcel.readInt() != 0 ? (Address) Address.CREATOR.createFromParcel(parcel) : null, parcel.readString());
        }

        public final Object[] newArray(int i) {
            return new CardParams[i];
        }
    }

    public CardParams(String str, int i, int i2) {
        this(str, i, i2, (String) null, (String) null, (Address) null, (String) null, 120, (DefaultConstructorMarker) null);
    }

    public CardParams(String str, int i, int i2, String str2) {
        this(str, i, i2, str2, (String) null, (Address) null, (String) null, 112, (DefaultConstructorMarker) null);
    }

    public CardParams(String str, int i, int i2, String str2, String str3) {
        this(str, i, i2, str2, str3, (Address) null, (String) null, 96, (DefaultConstructorMarker) null);
    }

    public CardParams(String str, int i, int i2, String str2, String str3, Address address2) {
        this(str, i, i2, str2, str3, address2, (String) null, 64, (DefaultConstructorMarker) null);
    }

    public static /* synthetic */ CardParams copy$default(CardParams cardParams, Set set, String str, int i, int i2, String str2, String str3, Address address2, String str4, int i3, Object obj) {
        CardParams cardParams2 = cardParams;
        int i4 = i3;
        return cardParams.copy((i4 & 1) != 0 ? cardParams2.attribution : set, (i4 & 2) != 0 ? cardParams2.number : str, (i4 & 4) != 0 ? cardParams2.expMonth : i, (i4 & 8) != 0 ? cardParams2.expYear : i2, (i4 & 16) != 0 ? cardParams2.cvc : str2, (i4 & 32) != 0 ? cardParams2.name : str3, (i4 & 64) != 0 ? cardParams2.address : address2, (i4 & 128) != 0 ? cardParams2.currency : str4);
    }

    public final Set<String> component1$stripe_release() {
        return this.attribution;
    }

    public final String component2() {
        return this.number;
    }

    public final int component3() {
        return this.expMonth;
    }

    public final int component4() {
        return this.expYear;
    }

    public final String component5() {
        return this.cvc;
    }

    public final String component6() {
        return this.name;
    }

    public final Address component7() {
        return this.address;
    }

    public final String component8() {
        return this.currency;
    }

    public final CardParams copy(Set<String> set, String str, int i, int i2, String str2, String str3, Address address2, String str4) {
        Intrinsics.checkParameterIsNotNull(set, "attribution");
        Intrinsics.checkParameterIsNotNull(str, PARAM_NUMBER);
        return new CardParams(set, str, i, i2, str2, str3, address2, str4);
    }

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof CardParams)) {
            return false;
        }
        CardParams cardParams = (CardParams) obj;
        return Intrinsics.areEqual((Object) this.attribution, (Object) cardParams.attribution) && Intrinsics.areEqual((Object) this.number, (Object) cardParams.number) && this.expMonth == cardParams.expMonth && this.expYear == cardParams.expYear && Intrinsics.areEqual((Object) this.cvc, (Object) cardParams.cvc) && Intrinsics.areEqual((Object) this.name, (Object) cardParams.name) && Intrinsics.areEqual((Object) this.address, (Object) cardParams.address) && Intrinsics.areEqual((Object) this.currency, (Object) cardParams.currency);
    }

    public int hashCode() {
        Set<String> set = this.attribution;
        int i = 0;
        int hashCode = (set != null ? set.hashCode() : 0) * 31;
        String str = this.number;
        int hashCode2 = (((((hashCode + (str != null ? str.hashCode() : 0)) * 31) + this.expMonth) * 31) + this.expYear) * 31;
        String str2 = this.cvc;
        int hashCode3 = (hashCode2 + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.name;
        int hashCode4 = (hashCode3 + (str3 != null ? str3.hashCode() : 0)) * 31;
        Address address2 = this.address;
        int hashCode5 = (hashCode4 + (address2 != null ? address2.hashCode() : 0)) * 31;
        String str4 = this.currency;
        if (str4 != null) {
            i = str4.hashCode();
        }
        return hashCode5 + i;
    }

    public String toString() {
        return "CardParams(attribution=" + this.attribution + ", number=" + this.number + ", expMonth=" + this.expMonth + ", expYear=" + this.expYear + ", cvc=" + this.cvc + ", name=" + this.name + ", address=" + this.address + ", currency=" + this.currency + ")";
    }

    public void writeToParcel(Parcel parcel, int i) {
        Intrinsics.checkParameterIsNotNull(parcel, "parcel");
        Set<String> set = this.attribution;
        parcel.writeInt(set.size());
        for (String writeString : set) {
            parcel.writeString(writeString);
        }
        parcel.writeString(this.number);
        parcel.writeInt(this.expMonth);
        parcel.writeInt(this.expYear);
        parcel.writeString(this.cvc);
        parcel.writeString(this.name);
        Address address2 = this.address;
        if (address2 != null) {
            parcel.writeInt(1);
            address2.writeToParcel(parcel, 0);
        } else {
            parcel.writeInt(0);
        }
        parcel.writeString(this.currency);
    }

    public CardParams(Set<String> set, String str, int i, int i2, String str2, String str3, Address address2, String str4) {
        Intrinsics.checkParameterIsNotNull(set, "attribution");
        Intrinsics.checkParameterIsNotNull(str, PARAM_NUMBER);
        this.attribution = set;
        this.number = str;
        this.expMonth = i;
        this.expYear = i2;
        this.cvc = str2;
        this.name = str3;
        this.address = address2;
        this.currency = str4;
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* synthetic */ CardParams(java.util.Set r12, java.lang.String r13, int r14, int r15, java.lang.String r16, java.lang.String r17, com.stripe.android.model.Address r18, java.lang.String r19, int r20, kotlin.jvm.internal.DefaultConstructorMarker r21) {
        /*
            r11 = this;
            r0 = r20
            r1 = r0 & 1
            if (r1 == 0) goto L_0x000c
            java.util.Set r1 = kotlin.collections.SetsKt.emptySet()
            r3 = r1
            goto L_0x000d
        L_0x000c:
            r3 = r12
        L_0x000d:
            r1 = r0 & 16
            r2 = 0
            if (r1 == 0) goto L_0x0017
            r1 = r2
            java.lang.String r1 = (java.lang.String) r1
            r7 = r1
            goto L_0x0019
        L_0x0017:
            r7 = r16
        L_0x0019:
            r1 = r0 & 32
            if (r1 == 0) goto L_0x0022
            r1 = r2
            java.lang.String r1 = (java.lang.String) r1
            r8 = r1
            goto L_0x0024
        L_0x0022:
            r8 = r17
        L_0x0024:
            r1 = r0 & 64
            if (r1 == 0) goto L_0x002d
            r1 = r2
            com.stripe.android.model.Address r1 = (com.stripe.android.model.Address) r1
            r9 = r1
            goto L_0x002f
        L_0x002d:
            r9 = r18
        L_0x002f:
            r0 = r0 & 128(0x80, float:1.794E-43)
            if (r0 == 0) goto L_0x0038
            r0 = r2
            java.lang.String r0 = (java.lang.String) r0
            r10 = r0
            goto L_0x003a
        L_0x0038:
            r10 = r19
        L_0x003a:
            r2 = r11
            r4 = r13
            r5 = r14
            r6 = r15
            r2.<init>(r3, r4, r5, r6, r7, r8, r9, r10)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.stripe.android.model.CardParams.<init>(java.util.Set, java.lang.String, int, int, java.lang.String, java.lang.String, com.stripe.android.model.Address, java.lang.String, int, kotlin.jvm.internal.DefaultConstructorMarker):void");
    }

    public final Set<String> getAttribution$stripe_release() {
        return this.attribution;
    }

    public final String getNumber() {
        return this.number;
    }

    public final void setNumber(String str) {
        Intrinsics.checkParameterIsNotNull(str, "<set-?>");
        this.number = str;
    }

    public final int getExpMonth() {
        return this.expMonth;
    }

    public final void setExpMonth(int i) {
        this.expMonth = i;
    }

    public final int getExpYear() {
        return this.expYear;
    }

    public final void setExpYear(int i) {
        this.expYear = i;
    }

    public final String getCvc() {
        return this.cvc;
    }

    public final void setCvc(String str) {
        this.cvc = str;
    }

    public final String getName() {
        return this.name;
    }

    public final void setName(String str) {
        this.name = str;
    }

    public final Address getAddress() {
        return this.address;
    }

    public final void setAddress(Address address2) {
        this.address = address2;
    }

    public final String getCurrency() {
        return this.currency;
    }

    public final void setCurrency(String str) {
        this.currency = str;
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ CardParams(String str, int i, int i2, String str2, String str3, Address address2, String str4, int i3, DefaultConstructorMarker defaultConstructorMarker) {
        this(str, i, i2, (i3 & 8) != 0 ? null : str2, (i3 & 16) != 0 ? null : str3, (i3 & 32) != 0 ? null : address2, (i3 & 64) != 0 ? null : str4);
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public CardParams(String str, int i, int i2, String str2, String str3, Address address2, String str4) {
        this(SetsKt.emptySet(), str, i, i2, str2, str3, address2, str4);
        Intrinsics.checkParameterIsNotNull(str, PARAM_NUMBER);
    }

    public Map<String, Object> toParamMap() {
        Pair[] pairArr = new Pair[12];
        pairArr[0] = TuplesKt.to(PARAM_NUMBER, this.number);
        pairArr[1] = TuplesKt.to(PARAM_EXP_MONTH, Integer.valueOf(this.expMonth));
        pairArr[2] = TuplesKt.to(PARAM_EXP_YEAR, Integer.valueOf(this.expYear));
        pairArr[3] = TuplesKt.to(PARAM_CVC, this.cvc);
        pairArr[4] = TuplesKt.to("name", this.name);
        pairArr[5] = TuplesKt.to("currency", this.currency);
        Address address2 = this.address;
        pairArr[6] = TuplesKt.to(PARAM_ADDRESS_LINE1, address2 != null ? address2.getLine1() : null);
        Address address3 = this.address;
        pairArr[7] = TuplesKt.to(PARAM_ADDRESS_LINE2, address3 != null ? address3.getLine2() : null);
        Address address4 = this.address;
        pairArr[8] = TuplesKt.to(PARAM_ADDRESS_CITY, address4 != null ? address4.getCity() : null);
        Address address5 = this.address;
        pairArr[9] = TuplesKt.to(PARAM_ADDRESS_STATE, address5 != null ? address5.getState() : null);
        Address address6 = this.address;
        pairArr[10] = TuplesKt.to(PARAM_ADDRESS_ZIP, address6 != null ? address6.getPostalCode() : null);
        Address address7 = this.address;
        pairArr[11] = TuplesKt.to(PARAM_ADDRESS_COUNTRY, address7 != null ? address7.getCountry() : null);
        Map emptyMap = MapsKt.emptyMap();
        for (Pair pair : CollectionsKt.listOf(pairArr)) {
            String str = (String) pair.component1();
            Object component2 = pair.component2();
            Map mapOf = component2 != null ? MapsKt.mapOf(TuplesKt.to(str, component2)) : null;
            if (mapOf == null) {
                mapOf = MapsKt.emptyMap();
            }
            emptyMap = MapsKt.plus(emptyMap, mapOf);
        }
        return MapsKt.mapOf(TuplesKt.to("card", emptyMap));
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\f\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\u0010"}, d2 = {"Lcom/stripe/android/model/CardParams$Companion;", "", "()V", "PARAM_ADDRESS_CITY", "", "PARAM_ADDRESS_COUNTRY", "PARAM_ADDRESS_LINE1", "PARAM_ADDRESS_LINE2", "PARAM_ADDRESS_STATE", "PARAM_ADDRESS_ZIP", "PARAM_CURRENCY", "PARAM_CVC", "PARAM_EXP_MONTH", "PARAM_EXP_YEAR", "PARAM_NAME", "PARAM_NUMBER", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: CardParams.kt */
    private static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }
}
