package com.stripe.android.model;

import android.os.Parcel;
import android.os.Parcelable;
import com.stripe.android.model.Address;
import com.stripe.android.model.KlarnaSourceParams;
import com.stripe.android.model.SourceOrderParams;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.TuplesKt;
import kotlin.collections.CollectionsKt;
import kotlin.collections.MapsKt;
import kotlin.collections.SetsKt;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.internal.Intrinsics;
import org.json.JSONException;
import org.json.JSONObject;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000J\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\"\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0004\n\u0002\u0010$\n\u0002\u0010\u0000\n\u0002\b\u000b\n\u0002\u0018\u0002\n\u0002\b\u000f\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0012\u0018\u0000 >2\u00020\u0001:\u0003>?@B\u001f\b\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u000e\b\u0002\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u0005¢\u0006\u0002\u0010\u0006J\u0013\u0010+\u001a\u00020,2\b\u0010-\u001a\u0004\u0018\u00010\u000eH\u0002J\b\u0010.\u001a\u00020/H\u0016J\u0017\u00100\u001a\u00020\u00002\n\b\u0001\u0010\t\u001a\u0004\u0018\u00010\b¢\u0006\u0002\u00101J\u001e\u00102\u001a\u00020\u00002\u0016\u0010\u000f\u001a\u0012\u0012\u0004\u0012\u00020\u0003\u0012\u0006\u0012\u0004\u0018\u00010\u000e\u0018\u00010\rJ\u000e\u00103\u001a\u00020\u00002\u0006\u0010\u0014\u001a\u00020\u0003J\u001a\u00104\u001a\u00020\u00002\u0012\u0010\u0017\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u000e0\rJ\u001c\u00105\u001a\u00020\u00002\u0014\u0010\u0018\u001a\u0010\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0003\u0018\u00010\rJ\u0010\u00106\u001a\u00020\u00002\b\u0010\u001b\u001a\u0004\u0018\u00010\u001aJ\u0010\u00107\u001a\u00020\u00002\b\b\u0001\u0010\u001e\u001a\u00020\u0003J\u000e\u00108\u001a\u00020\u00002\u0006\u0010 \u001a\u00020\u0003J\u000e\u00109\u001a\u00020\u00002\u0006\u0010&\u001a\u00020\u0003J\u0010\u0010:\u001a\u00020\u00002\u0006\u0010)\u001a\u00020*H\u0002J\u0014\u0010;\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u000e0\rH\u0016J\u0010\u0010<\u001a\u00020,2\u0006\u0010=\u001a\u00020\u0000H\u0002R&\u0010\t\u001a\u0004\u0018\u00010\b2\b\u0010\u0007\u001a\u0004\u0018\u00010\b8\u0006@BX\u000e¢\u0006\n\n\u0002\u0010\f\u001a\u0004\b\n\u0010\u000bR>\u0010\u000f\u001a\u0012\u0012\u0004\u0012\u00020\u0003\u0012\u0006\u0012\u0004\u0018\u00010\u000e\u0018\u00010\r2\u0016\u0010\u0007\u001a\u0012\u0012\u0004\u0012\u00020\u0003\u0012\u0006\u0012\u0004\u0018\u00010\u000e\u0018\u00010\r@BX\u000e¢\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u0011R\u001a\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u0005X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u0013R\"\u0010\u0014\u001a\u0004\u0018\u00010\u00032\b\u0010\u0007\u001a\u0004\u0018\u00010\u0003@BX\u000e¢\u0006\b\n\u0000\u001a\u0004\b\u0015\u0010\u0016R\u001a\u0010\u0017\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u000e0\rX\u000e¢\u0006\u0002\n\u0000R:\u0010\u0018\u001a\u0010\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0003\u0018\u00010\r2\u0014\u0010\u0007\u001a\u0010\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0003\u0018\u00010\r@BX\u000e¢\u0006\b\n\u0000\u001a\u0004\b\u0019\u0010\u0011R\"\u0010\u001b\u001a\u0004\u0018\u00010\u001a2\b\u0010\u0007\u001a\u0004\u0018\u00010\u001a@BX\u000e¢\u0006\b\n\u0000\u001a\u0004\b\u001c\u0010\u001dR\"\u0010\u001e\u001a\u0004\u0018\u00010\u00032\b\u0010\u0007\u001a\u0004\u0018\u00010\u0003@BX\u000e¢\u0006\b\n\u0000\u001a\u0004\b\u001f\u0010\u0016R\u0010\u0010 \u001a\u0004\u0018\u00010\u0003X\u000e¢\u0006\u0002\n\u0000R\u0019\u0010!\u001a\u00020\u00038F¢\u0006\u000e\n\u0000\u0012\u0004\b\"\u0010#\u001a\u0004\b$\u0010\u0016R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b%\u0010\u0016R*\u0010&\u001a\u0004\u0018\u00010\u00032\b\u0010\u0007\u001a\u0004\u0018\u00010\u00038F@BX\u000e¢\u0006\u000e\n\u0000\u0012\u0004\b'\u0010#\u001a\u0004\b(\u0010\u0016R\u0010\u0010)\u001a\u0004\u0018\u00010*X\u000e¢\u0006\u0002\n\u0000¨\u0006A"}, d2 = {"Lcom/stripe/android/model/SourceParams;", "Lcom/stripe/android/model/StripeParamsModel;", "typeRaw", "", "attribution", "", "(Ljava/lang/String;Ljava/util/Set;)V", "<set-?>", "", "amount", "getAmount", "()Ljava/lang/Long;", "Ljava/lang/Long;", "", "", "apiParameterMap", "getApiParameterMap", "()Ljava/util/Map;", "getAttribution$stripe_release", "()Ljava/util/Set;", "currency", "getCurrency", "()Ljava/lang/String;", "extraParams", "metaData", "getMetaData", "Lcom/stripe/android/model/SourceParams$OwnerParams;", "owner", "getOwner", "()Lcom/stripe/android/model/SourceParams$OwnerParams;", "returnUrl", "getReturnUrl", "token", "type", "type$annotations", "()V", "getType", "getTypeRaw", "usage", "usage$annotations", "getUsage", "weChatParams", "Lcom/stripe/android/model/SourceParams$WeChatParams;", "equals", "", "other", "hashCode", "", "setAmount", "(Ljava/lang/Long;)Lcom/stripe/android/model/SourceParams;", "setApiParameterMap", "setCurrency", "setExtraParams", "setMetaData", "setOwner", "setReturnUrl", "setToken", "setUsage", "setWeChatParams", "toParamMap", "typedEquals", "params", "Companion", "OwnerParams", "WeChatParams", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: SourceParams.kt */
public final class SourceParams implements StripeParamsModel {
    public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
    private static final String PARAM_AMOUNT = "amount";
    private static final String PARAM_BANK = "bank";
    private static final String PARAM_CALL_ID = "callid";
    private static final String PARAM_CARD = "card";
    private static final String PARAM_CART_ID = "cart_id";
    private static final String PARAM_CLIENT_SECRET = "client_secret";
    private static final String PARAM_COUNTRY = "country";
    private static final String PARAM_CURRENCY = "currency";
    private static final String PARAM_CVC = "cvc";
    private static final String PARAM_EXP_MONTH = "exp_month";
    private static final String PARAM_EXP_YEAR = "exp_year";
    private static final String PARAM_FLOW = "flow";
    private static final String PARAM_IBAN = "iban";
    private static final String PARAM_KLARNA = "klarna";
    private static final String PARAM_MASTERPASS = "masterpass";
    private static final String PARAM_METADATA = "metadata";
    private static final String PARAM_NUMBER = "number";
    private static final String PARAM_OWNER = "owner";
    private static final String PARAM_PREFERRED_LANGUAGE = "preferred_language";
    private static final String PARAM_REDIRECT = "redirect";
    private static final String PARAM_RETURN_URL = "return_url";
    private static final String PARAM_SOURCE_ORDER = "source_order";
    private static final String PARAM_STATEMENT_DESCRIPTOR = "statement_descriptor";
    private static final String PARAM_TOKEN = "token";
    private static final String PARAM_TRANSACTION_ID = "transaction_id";
    private static final String PARAM_TYPE = "type";
    private static final String PARAM_USAGE = "usage";
    private static final String PARAM_VISA_CHECKOUT = "visa_checkout";
    private static final String PARAM_WECHAT = "wechat";
    private Long amount;
    private Map<String, ? extends Object> apiParameterMap;
    private final Set<String> attribution;
    private String currency;
    private Map<String, ? extends Object> extraParams;
    private Map<String, String> metaData;
    private OwnerParams owner;
    private String returnUrl;
    private String token;
    private final String type;
    private final String typeRaw;
    private String usage;
    private WeChatParams weChatParams;

    @JvmStatic
    public static final SourceParams createAlipayReusableParams(String str, String str2, String str3, String str4) {
        return Companion.createAlipayReusableParams(str, str2, str3, str4);
    }

    @JvmStatic
    public static final SourceParams createAlipaySingleUseParams(long j, String str, String str2, String str3, String str4) {
        return Companion.createAlipaySingleUseParams(j, str, str2, str3, str4);
    }

    @JvmStatic
    public static final SourceParams createBancontactParams(long j, String str, String str2, String str3, String str4) {
        return Companion.createBancontactParams(j, str, str2, str3, str4);
    }

    @JvmStatic
    public static final SourceParams createCardParams(Card card) {
        return Companion.createCardParams(card);
    }

    @JvmStatic
    public static final SourceParams createCardParamsFromGooglePay(JSONObject jSONObject) throws JSONException {
        return Companion.createCardParamsFromGooglePay(jSONObject);
    }

    @JvmStatic
    public static final SourceParams createCustomParams(String str) {
        return Companion.createCustomParams(str);
    }

    @JvmStatic
    public static final SourceParams createEPSParams(long j, String str, String str2, String str3) {
        return Companion.createEPSParams(j, str, str2, str3);
    }

    @JvmStatic
    public static final SourceParams createGiropayParams(long j, String str, String str2, String str3) {
        return Companion.createGiropayParams(j, str, str2, str3);
    }

    @JvmStatic
    public static final SourceParams createIdealParams(long j, String str, String str2, String str3, String str4) {
        return Companion.createIdealParams(j, str, str2, str3, str4);
    }

    @JvmStatic
    public static final SourceParams createKlarna(String str, String str2, KlarnaSourceParams klarnaSourceParams) {
        return Companion.createKlarna(str, str2, klarnaSourceParams);
    }

    @JvmStatic
    public static final SourceParams createMasterpassParams(String str, String str2) {
        return Companion.createMasterpassParams(str, str2);
    }

    @JvmStatic
    public static final SourceParams createMultibancoParams(long j, String str, String str2) {
        return Companion.createMultibancoParams(j, str, str2);
    }

    @JvmStatic
    public static final SourceParams createP24Params(long j, String str, String str2, String str3, String str4) {
        return Companion.createP24Params(j, str, str2, str3, str4);
    }

    @JvmStatic
    public static final Map<String, String> createRetrieveSourceParams(String str) {
        return Companion.createRetrieveSourceParams(str);
    }

    @JvmStatic
    public static final SourceParams createSepaDebitParams(String str, String str2, String str3, String str4, String str5, String str6) {
        return Companion.createSepaDebitParams(str, str2, str3, str4, str5, str6);
    }

    @JvmStatic
    public static final SourceParams createSepaDebitParams(String str, String str2, String str3, String str4, String str5, String str6, String str7) {
        return Companion.createSepaDebitParams(str, str2, str3, str4, str5, str6, str7);
    }

    @JvmStatic
    public static final SourceParams createSofortParams(long j, String str, String str2, String str3) {
        return Companion.createSofortParams(j, str, str2, str3);
    }

    @JvmStatic
    public static final SourceParams createSourceFromTokenParams(String str) {
        return Companion.createSourceFromTokenParams(str);
    }

    @JvmStatic
    public static final SourceParams createThreeDSecureParams(long j, String str, String str2, String str3) {
        return Companion.createThreeDSecureParams(j, str, str2, str3);
    }

    @JvmStatic
    public static final SourceParams createVisaCheckoutParams(String str) {
        return Companion.createVisaCheckoutParams(str);
    }

    @JvmStatic
    public static final SourceParams createWeChatPayParams(long j, String str, String str2, String str3) {
        return Companion.createWeChatPayParams(j, str, str2, str3);
    }

    public static /* synthetic */ void type$annotations() {
    }

    public static /* synthetic */ void usage$annotations() {
    }

    private SourceParams(String str, Set<String> set) {
        this.typeRaw = str;
        this.attribution = set;
        this.type = Source.Companion.asSourceType(this.typeRaw);
        this.extraParams = MapsKt.emptyMap();
    }

    public /* synthetic */ SourceParams(String str, Set set, DefaultConstructorMarker defaultConstructorMarker) {
        this(str, set);
    }

    public final String getTypeRaw() {
        return this.typeRaw;
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    /* synthetic */ SourceParams(String str, Set set, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(str, (i & 2) != 0 ? SetsKt.emptySet() : set);
    }

    public final Set<String> getAttribution$stripe_release() {
        return this.attribution;
    }

    public final String getType() {
        return this.type;
    }

    public final Long getAmount() {
        return this.amount;
    }

    public final Map<String, Object> getApiParameterMap() {
        return this.apiParameterMap;
    }

    public final String getCurrency() {
        return this.currency;
    }

    public final String getReturnUrl() {
        return this.returnUrl;
    }

    public final OwnerParams getOwner() {
        return this.owner;
    }

    public final Map<String, String> getMetaData() {
        return this.metaData;
    }

    public final String getUsage() {
        return this.usage;
    }

    public final SourceParams setAmount(Long l) {
        SourceParams sourceParams = this;
        sourceParams.amount = l;
        return sourceParams;
    }

    public final SourceParams setApiParameterMap(Map<String, ? extends Object> map) {
        SourceParams sourceParams = this;
        sourceParams.apiParameterMap = map;
        return sourceParams;
    }

    public final SourceParams setCurrency(String str) {
        Intrinsics.checkParameterIsNotNull(str, "currency");
        SourceParams sourceParams = this;
        sourceParams.currency = str;
        return sourceParams;
    }

    public final SourceParams setOwner(OwnerParams ownerParams) {
        SourceParams sourceParams = this;
        sourceParams.owner = ownerParams;
        return sourceParams;
    }

    public final SourceParams setExtraParams(Map<String, ? extends Object> map) {
        Intrinsics.checkParameterIsNotNull(map, "extraParams");
        SourceParams sourceParams = this;
        sourceParams.extraParams = map;
        return sourceParams;
    }

    public final SourceParams setReturnUrl(String str) {
        Intrinsics.checkParameterIsNotNull(str, "returnUrl");
        SourceParams sourceParams = this;
        sourceParams.returnUrl = str;
        return sourceParams;
    }

    public final SourceParams setMetaData(Map<String, String> map) {
        SourceParams sourceParams = this;
        sourceParams.metaData = map;
        return sourceParams;
    }

    public final SourceParams setToken(String str) {
        Intrinsics.checkParameterIsNotNull(str, PARAM_TOKEN);
        SourceParams sourceParams = this;
        sourceParams.token = str;
        return sourceParams;
    }

    public final SourceParams setUsage(String str) {
        Intrinsics.checkParameterIsNotNull(str, PARAM_USAGE);
        SourceParams sourceParams = this;
        sourceParams.usage = str;
        return sourceParams;
    }

    /* access modifiers changed from: private */
    public final SourceParams setWeChatParams(WeChatParams weChatParams2) {
        this.weChatParams = weChatParams2;
        return this;
    }

    public Map<String, Object> toParamMap() {
        Map mapOf = MapsKt.mapOf(TuplesKt.to("type", this.typeRaw));
        Map<String, ? extends Object> map = this.apiParameterMap;
        Map<K, V> map2 = null;
        Map<K, V> mapOf2 = map != null ? MapsKt.mapOf(TuplesKt.to(this.typeRaw, map)) : null;
        if (mapOf2 == null) {
            mapOf2 = MapsKt.emptyMap();
        }
        Map<K, V> plus = MapsKt.plus(mapOf, (Map) mapOf2);
        Long l = this.amount;
        Map mapOf3 = l != null ? MapsKt.mapOf(TuplesKt.to(PARAM_AMOUNT, Long.valueOf(l.longValue()))) : null;
        if (mapOf3 == null) {
            mapOf3 = MapsKt.emptyMap();
        }
        Map<K, V> plus2 = MapsKt.plus(plus, (Map<K, V>) mapOf3);
        String str = this.currency;
        Map mapOf4 = str != null ? MapsKt.mapOf(TuplesKt.to("currency", str)) : null;
        if (mapOf4 == null) {
            mapOf4 = MapsKt.emptyMap();
        }
        Map<K, V> plus3 = MapsKt.plus(plus2, (Map<K, V>) mapOf4);
        OwnerParams ownerParams = this.owner;
        Map<K, V> mapOf5 = ownerParams != null ? MapsKt.mapOf(TuplesKt.to(PARAM_OWNER, ownerParams.toParamMap())) : null;
        if (mapOf5 == null) {
            mapOf5 = MapsKt.emptyMap();
        }
        Map<K, V> plus4 = MapsKt.plus(plus3, mapOf5);
        String str2 = this.returnUrl;
        Map mapOf6 = str2 != null ? MapsKt.mapOf(TuplesKt.to("redirect", MapsKt.mapOf(TuplesKt.to("return_url", str2)))) : null;
        if (mapOf6 == null) {
            mapOf6 = MapsKt.emptyMap();
        }
        Map<K, V> plus5 = MapsKt.plus(plus4, (Map<K, V>) mapOf6);
        Map<String, String> map3 = this.metaData;
        Map<K, V> mapOf7 = map3 != null ? MapsKt.mapOf(TuplesKt.to(PARAM_METADATA, map3)) : null;
        if (mapOf7 == null) {
            mapOf7 = MapsKt.emptyMap();
        }
        Map<K, V> plus6 = MapsKt.plus(plus5, mapOf7);
        String str3 = this.token;
        Map mapOf8 = str3 != null ? MapsKt.mapOf(TuplesKt.to(PARAM_TOKEN, str3)) : null;
        if (mapOf8 == null) {
            mapOf8 = MapsKt.emptyMap();
        }
        Map<K, V> plus7 = MapsKt.plus(plus6, (Map<K, V>) mapOf8);
        String str4 = this.usage;
        Map mapOf9 = str4 != null ? MapsKt.mapOf(TuplesKt.to(PARAM_USAGE, str4)) : null;
        if (mapOf9 == null) {
            mapOf9 = MapsKt.emptyMap();
        }
        Map<String, ? extends Object> plus8 = MapsKt.plus(MapsKt.plus(plus7, (Map<K, V>) mapOf9), (Map<K, V>) this.extraParams);
        WeChatParams weChatParams2 = this.weChatParams;
        if (weChatParams2 != null) {
            map2 = MapsKt.mapOf(TuplesKt.to("wechat", weChatParams2.toParamMap()));
        }
        if (map2 == null) {
            map2 = MapsKt.emptyMap();
        }
        return MapsKt.plus(plus8, (Map<String, ? extends Object>) map2);
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0006\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\b\b\u0018\u0000 \u00192\u00020\u00012\u00020\u0002:\u0001\u0019B\u001d\u0012\n\b\u0002\u0010\u0003\u001a\u0004\u0018\u00010\u0004\u0012\n\b\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u0004¢\u0006\u0002\u0010\u0006J\u000b\u0010\u0007\u001a\u0004\u0018\u00010\u0004HÂ\u0003J\u000b\u0010\b\u001a\u0004\u0018\u00010\u0004HÂ\u0003J!\u0010\t\u001a\u00020\u00002\n\b\u0002\u0010\u0003\u001a\u0004\u0018\u00010\u00042\n\b\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u0004HÆ\u0001J\t\u0010\n\u001a\u00020\u000bHÖ\u0001J\u0013\u0010\f\u001a\u00020\r2\b\u0010\u000e\u001a\u0004\u0018\u00010\u000fHÖ\u0003J\t\u0010\u0010\u001a\u00020\u000bHÖ\u0001J\u0014\u0010\u0011\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u000f0\u0012H\u0016J\t\u0010\u0013\u001a\u00020\u0004HÖ\u0001J\u0019\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u000bHÖ\u0001R\u0010\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u0005\u001a\u0004\u0018\u00010\u0004X\u0004¢\u0006\u0002\n\u0000¨\u0006\u001a"}, d2 = {"Lcom/stripe/android/model/SourceParams$WeChatParams;", "Lcom/stripe/android/model/StripeParamsModel;", "Landroid/os/Parcelable;", "appId", "", "statementDescriptor", "(Ljava/lang/String;Ljava/lang/String;)V", "component1", "component2", "copy", "describeContents", "", "equals", "", "other", "", "hashCode", "toParamMap", "", "toString", "writeToParcel", "", "parcel", "Landroid/os/Parcel;", "flags", "Companion", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: SourceParams.kt */
    public static final class WeChatParams implements StripeParamsModel, Parcelable {
        public static final Parcelable.Creator CREATOR = new Creator();
        public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
        private static final String PARAM_APPID = "appid";
        private static final String PARAM_STATEMENT_DESCRIPTOR = "statement_descriptor";
        private final String appId;
        private final String statementDescriptor;

        @Metadata(bv = {1, 0, 3}, k = 3, mv = {1, 1, 16})
        public static class Creator implements Parcelable.Creator {
            public final Object createFromParcel(Parcel parcel) {
                Intrinsics.checkParameterIsNotNull(parcel, "in");
                return new WeChatParams(parcel.readString(), parcel.readString());
            }

            public final Object[] newArray(int i) {
                return new WeChatParams[i];
            }
        }

        public WeChatParams() {
            this((String) null, (String) null, 3, (DefaultConstructorMarker) null);
        }

        private final String component1() {
            return this.appId;
        }

        private final String component2() {
            return this.statementDescriptor;
        }

        public static /* synthetic */ WeChatParams copy$default(WeChatParams weChatParams, String str, String str2, int i, Object obj) {
            if ((i & 1) != 0) {
                str = weChatParams.appId;
            }
            if ((i & 2) != 0) {
                str2 = weChatParams.statementDescriptor;
            }
            return weChatParams.copy(str, str2);
        }

        public final WeChatParams copy(String str, String str2) {
            return new WeChatParams(str, str2);
        }

        public int describeContents() {
            return 0;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof WeChatParams)) {
                return false;
            }
            WeChatParams weChatParams = (WeChatParams) obj;
            return Intrinsics.areEqual((Object) this.appId, (Object) weChatParams.appId) && Intrinsics.areEqual((Object) this.statementDescriptor, (Object) weChatParams.statementDescriptor);
        }

        public int hashCode() {
            String str = this.appId;
            int i = 0;
            int hashCode = (str != null ? str.hashCode() : 0) * 31;
            String str2 = this.statementDescriptor;
            if (str2 != null) {
                i = str2.hashCode();
            }
            return hashCode + i;
        }

        public String toString() {
            return "WeChatParams(appId=" + this.appId + ", statementDescriptor=" + this.statementDescriptor + ")";
        }

        public void writeToParcel(Parcel parcel, int i) {
            Intrinsics.checkParameterIsNotNull(parcel, "parcel");
            parcel.writeString(this.appId);
            parcel.writeString(this.statementDescriptor);
        }

        public WeChatParams(String str, String str2) {
            this.appId = str;
            this.statementDescriptor = str2;
        }

        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ WeChatParams(String str, String str2, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this((i & 1) != 0 ? null : str, (i & 2) != 0 ? null : str2);
        }

        public Map<String, Object> toParamMap() {
            Map emptyMap = MapsKt.emptyMap();
            String str = this.appId;
            Map map = null;
            Map mapOf = str != null ? MapsKt.mapOf(TuplesKt.to(PARAM_APPID, str)) : null;
            if (mapOf == null) {
                mapOf = MapsKt.emptyMap();
            }
            Map plus = MapsKt.plus(emptyMap, mapOf);
            String str2 = this.statementDescriptor;
            if (str2 != null) {
                map = MapsKt.mapOf(TuplesKt.to(PARAM_STATEMENT_DESCRIPTOR, str2));
            }
            if (map == null) {
                map = MapsKt.emptyMap();
            }
            return MapsKt.plus(plus, map);
        }

        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\u0006"}, d2 = {"Lcom/stripe/android/model/SourceParams$WeChatParams$Companion;", "", "()V", "PARAM_APPID", "", "PARAM_STATEMENT_DESCRIPTOR", "stripe_release"}, k = 1, mv = {1, 1, 16})
        /* compiled from: SourceParams.kt */
        public static final class Companion {
            private Companion() {
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }
        }
    }

    public int hashCode() {
        return Objects.hash(new Object[]{this.amount, this.apiParameterMap, this.currency, this.typeRaw, this.owner, this.metaData, this.returnUrl, this.extraParams, this.token, this.usage, this.type, this.weChatParams});
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof SourceParams) {
            return typedEquals((SourceParams) obj);
        }
        return false;
    }

    private final boolean typedEquals(SourceParams sourceParams) {
        return Objects.equals(this.amount, sourceParams.amount) && Objects.equals(this.apiParameterMap, sourceParams.apiParameterMap) && Objects.equals(this.currency, sourceParams.currency) && Objects.equals(this.typeRaw, sourceParams.typeRaw) && Objects.equals(this.owner, sourceParams.owner) && Objects.equals(this.metaData, sourceParams.metaData) && Objects.equals(this.returnUrl, sourceParams.returnUrl) && Objects.equals(this.extraParams, sourceParams.extraParams) && Objects.equals(this.token, sourceParams.token) && Objects.equals(this.usage, sourceParams.usage) && Objects.equals(this.type, sourceParams.type) && Objects.equals(this.weChatParams, sourceParams.weChatParams);
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000F\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0019\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\b\b\u0018\u0000 .2\u00020\u00012\u00020\u0002:\u0001.B7\b\u0007\u0012\n\b\u0002\u0010\u0003\u001a\u0004\u0018\u00010\u0004\u0012\n\b\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u0012\n\b\u0002\u0010\u0007\u001a\u0004\u0018\u00010\u0006\u0012\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\u0006¢\u0006\u0002\u0010\tJ\u0010\u0010\u0016\u001a\u0004\u0018\u00010\u0004HÀ\u0003¢\u0006\u0002\b\u0017J\u0010\u0010\u0018\u001a\u0004\u0018\u00010\u0006HÀ\u0003¢\u0006\u0002\b\u0019J\u0010\u0010\u001a\u001a\u0004\u0018\u00010\u0006HÀ\u0003¢\u0006\u0002\b\u001bJ\u0010\u0010\u001c\u001a\u0004\u0018\u00010\u0006HÀ\u0003¢\u0006\u0002\b\u001dJ9\u0010\u001e\u001a\u00020\u00002\n\b\u0002\u0010\u0003\u001a\u0004\u0018\u00010\u00042\n\b\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u00062\n\b\u0002\u0010\u0007\u001a\u0004\u0018\u00010\u00062\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\u0006HÆ\u0001J\t\u0010\u001f\u001a\u00020 HÖ\u0001J\u0013\u0010!\u001a\u00020\"2\b\u0010#\u001a\u0004\u0018\u00010$HÖ\u0003J\t\u0010%\u001a\u00020 HÖ\u0001J\u0014\u0010&\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020$0'H\u0016J\t\u0010(\u001a\u00020\u0006HÖ\u0001J\u0019\u0010)\u001a\u00020*2\u0006\u0010+\u001a\u00020,2\u0006\u0010-\u001a\u00020 HÖ\u0001R\u001c\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\n\u0010\u000b\"\u0004\b\f\u0010\rR\u001c\u0010\u0005\u001a\u0004\u0018\u00010\u0006X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u000e\u0010\u000f\"\u0004\b\u0010\u0010\u0011R\u001c\u0010\u0007\u001a\u0004\u0018\u00010\u0006X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0012\u0010\u000f\"\u0004\b\u0013\u0010\u0011R\u001c\u0010\b\u001a\u0004\u0018\u00010\u0006X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0014\u0010\u000f\"\u0004\b\u0015\u0010\u0011¨\u0006/"}, d2 = {"Lcom/stripe/android/model/SourceParams$OwnerParams;", "Lcom/stripe/android/model/StripeParamsModel;", "Landroid/os/Parcelable;", "address", "Lcom/stripe/android/model/Address;", "email", "", "name", "phone", "(Lcom/stripe/android/model/Address;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", "getAddress$stripe_release", "()Lcom/stripe/android/model/Address;", "setAddress$stripe_release", "(Lcom/stripe/android/model/Address;)V", "getEmail$stripe_release", "()Ljava/lang/String;", "setEmail$stripe_release", "(Ljava/lang/String;)V", "getName$stripe_release", "setName$stripe_release", "getPhone$stripe_release", "setPhone$stripe_release", "component1", "component1$stripe_release", "component2", "component2$stripe_release", "component3", "component3$stripe_release", "component4", "component4$stripe_release", "copy", "describeContents", "", "equals", "", "other", "", "hashCode", "toParamMap", "", "toString", "writeToParcel", "", "parcel", "Landroid/os/Parcel;", "flags", "Companion", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: SourceParams.kt */
    public static final class OwnerParams implements StripeParamsModel, Parcelable {
        public static final Parcelable.Creator CREATOR = new Creator();
        @Deprecated
        public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
        private static final String PARAM_ADDRESS = "address";
        private static final String PARAM_EMAIL = "email";
        private static final String PARAM_NAME = "name";
        private static final String PARAM_PHONE = "phone";
        private Address address;
        private String email;
        private String name;
        private String phone;

        @Metadata(bv = {1, 0, 3}, k = 3, mv = {1, 1, 16})
        public static class Creator implements Parcelable.Creator {
            public final Object createFromParcel(Parcel parcel) {
                Intrinsics.checkParameterIsNotNull(parcel, "in");
                return new OwnerParams(parcel.readInt() != 0 ? (Address) Address.CREATOR.createFromParcel(parcel) : null, parcel.readString(), parcel.readString(), parcel.readString());
            }

            public final Object[] newArray(int i) {
                return new OwnerParams[i];
            }
        }

        public OwnerParams() {
            this((Address) null, (String) null, (String) null, (String) null, 15, (DefaultConstructorMarker) null);
        }

        public OwnerParams(Address address2) {
            this(address2, (String) null, (String) null, (String) null, 14, (DefaultConstructorMarker) null);
        }

        public OwnerParams(Address address2, String str) {
            this(address2, str, (String) null, (String) null, 12, (DefaultConstructorMarker) null);
        }

        public OwnerParams(Address address2, String str, String str2) {
            this(address2, str, str2, (String) null, 8, (DefaultConstructorMarker) null);
        }

        public static /* synthetic */ OwnerParams copy$default(OwnerParams ownerParams, Address address2, String str, String str2, String str3, int i, Object obj) {
            if ((i & 1) != 0) {
                address2 = ownerParams.address;
            }
            if ((i & 2) != 0) {
                str = ownerParams.email;
            }
            if ((i & 4) != 0) {
                str2 = ownerParams.name;
            }
            if ((i & 8) != 0) {
                str3 = ownerParams.phone;
            }
            return ownerParams.copy(address2, str, str2, str3);
        }

        public final Address component1$stripe_release() {
            return this.address;
        }

        public final String component2$stripe_release() {
            return this.email;
        }

        public final String component3$stripe_release() {
            return this.name;
        }

        public final String component4$stripe_release() {
            return this.phone;
        }

        public final OwnerParams copy(Address address2, String str, String str2, String str3) {
            return new OwnerParams(address2, str, str2, str3);
        }

        public int describeContents() {
            return 0;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof OwnerParams)) {
                return false;
            }
            OwnerParams ownerParams = (OwnerParams) obj;
            return Intrinsics.areEqual((Object) this.address, (Object) ownerParams.address) && Intrinsics.areEqual((Object) this.email, (Object) ownerParams.email) && Intrinsics.areEqual((Object) this.name, (Object) ownerParams.name) && Intrinsics.areEqual((Object) this.phone, (Object) ownerParams.phone);
        }

        public int hashCode() {
            Address address2 = this.address;
            int i = 0;
            int hashCode = (address2 != null ? address2.hashCode() : 0) * 31;
            String str = this.email;
            int hashCode2 = (hashCode + (str != null ? str.hashCode() : 0)) * 31;
            String str2 = this.name;
            int hashCode3 = (hashCode2 + (str2 != null ? str2.hashCode() : 0)) * 31;
            String str3 = this.phone;
            if (str3 != null) {
                i = str3.hashCode();
            }
            return hashCode3 + i;
        }

        public String toString() {
            return "OwnerParams(address=" + this.address + ", email=" + this.email + ", name=" + this.name + ", phone=" + this.phone + ")";
        }

        public void writeToParcel(Parcel parcel, int i) {
            Intrinsics.checkParameterIsNotNull(parcel, "parcel");
            Address address2 = this.address;
            if (address2 != null) {
                parcel.writeInt(1);
                address2.writeToParcel(parcel, 0);
            } else {
                parcel.writeInt(0);
            }
            parcel.writeString(this.email);
            parcel.writeString(this.name);
            parcel.writeString(this.phone);
        }

        public OwnerParams(Address address2, String str, String str2, String str3) {
            this.address = address2;
            this.email = str;
            this.name = str2;
            this.phone = str3;
        }

        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ OwnerParams(Address address2, String str, String str2, String str3, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this((i & 1) != 0 ? null : address2, (i & 2) != 0 ? null : str, (i & 4) != 0 ? null : str2, (i & 8) != 0 ? null : str3);
        }

        public final Address getAddress$stripe_release() {
            return this.address;
        }

        public final void setAddress$stripe_release(Address address2) {
            this.address = address2;
        }

        public final String getEmail$stripe_release() {
            return this.email;
        }

        public final void setEmail$stripe_release(String str) {
            this.email = str;
        }

        public final String getName$stripe_release() {
            return this.name;
        }

        public final void setName$stripe_release(String str) {
            this.name = str;
        }

        public final String getPhone$stripe_release() {
            return this.phone;
        }

        public final void setPhone$stripe_release(String str) {
            this.phone = str;
        }

        public Map<String, Object> toParamMap() {
            Map emptyMap = MapsKt.emptyMap();
            Address address2 = this.address;
            Map map = null;
            Map<K, V> mapOf = address2 != null ? MapsKt.mapOf(TuplesKt.to("address", address2.toParamMap())) : null;
            if (mapOf == null) {
                mapOf = MapsKt.emptyMap();
            }
            Map<K, V> plus = MapsKt.plus(emptyMap, (Map) mapOf);
            String str = this.email;
            Map mapOf2 = str != null ? MapsKt.mapOf(TuplesKt.to("email", str)) : null;
            if (mapOf2 == null) {
                mapOf2 = MapsKt.emptyMap();
            }
            Map<K, V> plus2 = MapsKt.plus(plus, (Map<K, V>) mapOf2);
            String str2 = this.name;
            Map mapOf3 = str2 != null ? MapsKt.mapOf(TuplesKt.to("name", str2)) : null;
            if (mapOf3 == null) {
                mapOf3 = MapsKt.emptyMap();
            }
            Map<K, V> plus3 = MapsKt.plus(plus2, (Map<K, V>) mapOf3);
            String str3 = this.phone;
            if (str3 != null) {
                map = MapsKt.mapOf(TuplesKt.to("phone", str3));
            }
            if (map == null) {
                map = MapsKt.emptyMap();
            }
            return MapsKt.plus(plus3, (Map<K, V>) map);
        }

        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0004\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\b"}, d2 = {"Lcom/stripe/android/model/SourceParams$OwnerParams$Companion;", "", "()V", "PARAM_ADDRESS", "", "PARAM_EMAIL", "PARAM_NAME", "PARAM_PHONE", "stripe_release"}, k = 1, mv = {1, 1, 16})
        /* compiled from: SourceParams.kt */
        private static final class Companion {
            private Companion() {
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000D\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u001d\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\t\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010$\n\u0002\b\u0011\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J0\u0010!\u001a\u00020\"2\u0006\u0010#\u001a\u00020\u00042\n\b\u0002\u0010$\u001a\u0004\u0018\u00010\u00042\n\b\u0002\u0010%\u001a\u0004\u0018\u00010\u00042\u0006\u0010&\u001a\u00020\u0004H\u0007J:\u0010'\u001a\u00020\"2\b\b\u0001\u0010(\u001a\u00020)2\u0006\u0010#\u001a\u00020\u00042\n\b\u0002\u0010$\u001a\u0004\u0018\u00010\u00042\n\b\u0002\u0010%\u001a\u0004\u0018\u00010\u00042\u0006\u0010&\u001a\u00020\u0004H\u0007J:\u0010*\u001a\u00020\"2\b\b\u0001\u0010(\u001a\u00020)2\u0006\u0010$\u001a\u00020\u00042\u0006\u0010&\u001a\u00020\u00042\n\b\u0002\u0010+\u001a\u0004\u0018\u00010\u00042\n\b\u0002\u0010,\u001a\u0004\u0018\u00010\u0004H\u0007J\u0010\u0010-\u001a\u00020\"2\u0006\u0010.\u001a\u00020/H\u0007J\u0010\u00100\u001a\u00020\"2\u0006\u00101\u001a\u000202H\u0007J\u0010\u00103\u001a\u00020\"2\u0006\u00104\u001a\u00020\u0004H\u0007J.\u00105\u001a\u00020\"2\b\b\u0001\u0010(\u001a\u00020)2\u0006\u0010$\u001a\u00020\u00042\u0006\u0010&\u001a\u00020\u00042\n\b\u0002\u0010+\u001a\u0004\u0018\u00010\u0004H\u0007J.\u00106\u001a\u00020\"2\b\b\u0001\u0010(\u001a\u00020)2\u0006\u0010$\u001a\u00020\u00042\u0006\u0010&\u001a\u00020\u00042\n\b\u0002\u0010+\u001a\u0004\u0018\u00010\u0004H\u0007J<\u00107\u001a\u00020\"2\b\b\u0001\u0010(\u001a\u00020)2\b\u0010$\u001a\u0004\u0018\u00010\u00042\u0006\u0010&\u001a\u00020\u00042\n\b\u0002\u0010+\u001a\u0004\u0018\u00010\u00042\n\b\u0002\u00108\u001a\u0004\u0018\u00010\u0004H\u0007J \u00109\u001a\u00020\"2\u0006\u0010&\u001a\u00020\u00042\u0006\u0010#\u001a\u00020\u00042\u0006\u0010:\u001a\u00020;H\u0007J\u0018\u0010<\u001a\u00020\"2\u0006\u0010=\u001a\u00020\u00042\u0006\u0010>\u001a\u00020\u0004H\u0007J\"\u0010?\u001a\u00020\"2\b\b\u0001\u0010(\u001a\u00020)2\u0006\u0010&\u001a\u00020\u00042\u0006\u0010%\u001a\u00020\u0004H\u0007J4\u0010@\u001a\u00020\"2\b\b\u0001\u0010(\u001a\u00020)2\u0006\u0010#\u001a\u00020\u00042\b\u0010$\u001a\u0004\u0018\u00010\u00042\u0006\u0010%\u001a\u00020\u00042\u0006\u0010&\u001a\u00020\u0004H\u0007J\u001e\u0010A\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00040B2\b\b\u0001\u0010C\u001a\u00020\u0004H\u0007J<\u0010D\u001a\u00020\"2\u0006\u0010$\u001a\u00020\u00042\u0006\u0010E\u001a\u00020\u00042\b\u0010F\u001a\u0004\u0018\u00010\u00042\u0006\u0010G\u001a\u00020\u00042\u0006\u0010H\u001a\u00020\u00042\b\b\u0001\u0010I\u001a\u00020\u0004H\u0007JL\u0010D\u001a\u00020\"2\u0006\u0010$\u001a\u00020\u00042\u0006\u0010E\u001a\u00020\u00042\b\u0010%\u001a\u0004\u0018\u00010\u00042\b\u0010F\u001a\u0004\u0018\u00010\u00042\b\u0010G\u001a\u0004\u0018\u00010\u00042\b\u0010H\u001a\u0004\u0018\u00010\u00042\n\b\u0001\u0010I\u001a\u0004\u0018\u00010\u0004H\u0007J0\u0010J\u001a\u00020\"2\b\b\u0001\u0010(\u001a\u00020)2\u0006\u0010&\u001a\u00020\u00042\b\b\u0001\u0010I\u001a\u00020\u00042\n\b\u0002\u0010+\u001a\u0004\u0018\u00010\u0004H\u0007J\u0010\u0010K\u001a\u00020\"2\u0006\u0010L\u001a\u00020\u0004H\u0007J*\u0010M\u001a\u00020\"2\b\b\u0001\u0010(\u001a\u00020)2\u0006\u0010#\u001a\u00020\u00042\u0006\u0010&\u001a\u00020\u00042\u0006\u0010N\u001a\u00020\u0004H\u0007J\u0010\u0010O\u001a\u00020\"2\u0006\u0010P\u001a\u00020\u0004H\u0007J.\u0010Q\u001a\u00020\"2\b\b\u0001\u0010(\u001a\u00020)2\u0006\u0010#\u001a\u00020\u00042\u0006\u0010R\u001a\u00020\u00042\n\b\u0002\u0010+\u001a\u0004\u0018\u00010\u0004H\u0007R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0017\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0018\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0019\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u001a\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u001b\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u001c\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u001d\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u001e\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u001f\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010 \u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006S"}, d2 = {"Lcom/stripe/android/model/SourceParams$Companion;", "", "()V", "PARAM_AMOUNT", "", "PARAM_BANK", "PARAM_CALL_ID", "PARAM_CARD", "PARAM_CART_ID", "PARAM_CLIENT_SECRET", "PARAM_COUNTRY", "PARAM_CURRENCY", "PARAM_CVC", "PARAM_EXP_MONTH", "PARAM_EXP_YEAR", "PARAM_FLOW", "PARAM_IBAN", "PARAM_KLARNA", "PARAM_MASTERPASS", "PARAM_METADATA", "PARAM_NUMBER", "PARAM_OWNER", "PARAM_PREFERRED_LANGUAGE", "PARAM_REDIRECT", "PARAM_RETURN_URL", "PARAM_SOURCE_ORDER", "PARAM_STATEMENT_DESCRIPTOR", "PARAM_TOKEN", "PARAM_TRANSACTION_ID", "PARAM_TYPE", "PARAM_USAGE", "PARAM_VISA_CHECKOUT", "PARAM_WECHAT", "createAlipayReusableParams", "Lcom/stripe/android/model/SourceParams;", "currency", "name", "email", "returnUrl", "createAlipaySingleUseParams", "amount", "", "createBancontactParams", "statementDescriptor", "preferredLanguage", "createCardParams", "card", "Lcom/stripe/android/model/Card;", "createCardParamsFromGooglePay", "googlePayPaymentData", "Lorg/json/JSONObject;", "createCustomParams", "type", "createEPSParams", "createGiropayParams", "createIdealParams", "bank", "createKlarna", "klarnaParams", "Lcom/stripe/android/model/KlarnaSourceParams;", "createMasterpassParams", "transactionId", "cartId", "createMultibancoParams", "createP24Params", "createRetrieveSourceParams", "", "clientSecret", "createSepaDebitParams", "iban", "addressLine1", "city", "postalCode", "country", "createSofortParams", "createSourceFromTokenParams", "tokenId", "createThreeDSecureParams", "cardId", "createVisaCheckoutParams", "callId", "createWeChatPayParams", "weChatAppId", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: SourceParams.kt */
    public static final class Companion {

        @Metadata(bv = {1, 0, 3}, k = 3, mv = {1, 1, 16})
        public final /* synthetic */ class WhenMappings {
            public static final /* synthetic */ int[] $EnumSwitchMapping$0;

            static {
                int[] iArr = new int[KlarnaSourceParams.LineItem.Type.values().length];
                $EnumSwitchMapping$0 = iArr;
                iArr[KlarnaSourceParams.LineItem.Type.Sku.ordinal()] = 1;
                $EnumSwitchMapping$0[KlarnaSourceParams.LineItem.Type.Tax.ordinal()] = 2;
                $EnumSwitchMapping$0[KlarnaSourceParams.LineItem.Type.Shipping.ordinal()] = 3;
            }
        }

        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        @JvmStatic
        public final SourceParams createP24Params(long j, String str, String str2, String str3, String str4) {
            Intrinsics.checkParameterIsNotNull(str, "currency");
            Intrinsics.checkParameterIsNotNull(str3, "email");
            Intrinsics.checkParameterIsNotNull(str4, "returnUrl");
            return new SourceParams("p24", (Set) null, 2, (DefaultConstructorMarker) null).setAmount(Long.valueOf(j)).setCurrency(str).setReturnUrl(str4).setOwner(new OwnerParams((Address) null, str3, str2, (String) null, 9, (DefaultConstructorMarker) null));
        }

        public static /* synthetic */ SourceParams createAlipayReusableParams$default(Companion companion, String str, String str2, String str3, String str4, int i, Object obj) {
            if ((i & 2) != 0) {
                str2 = null;
            }
            if ((i & 4) != 0) {
                str3 = null;
            }
            return companion.createAlipayReusableParams(str, str2, str3, str4);
        }

        @JvmStatic
        public final SourceParams createAlipayReusableParams(String str, String str2, String str3, String str4) {
            Intrinsics.checkParameterIsNotNull(str, "currency");
            Intrinsics.checkParameterIsNotNull(str4, "returnUrl");
            return new SourceParams("alipay", (Set) null, 2, (DefaultConstructorMarker) null).setCurrency(str).setReturnUrl(str4).setUsage("reusable").setOwner(new OwnerParams((Address) null, str3, str2, (String) null, 9, (DefaultConstructorMarker) null));
        }

        public static /* synthetic */ SourceParams createAlipaySingleUseParams$default(Companion companion, long j, String str, String str2, String str3, String str4, int i, Object obj) {
            return companion.createAlipaySingleUseParams(j, str, (i & 4) != 0 ? null : str2, (i & 8) != 0 ? null : str3, str4);
        }

        @JvmStatic
        public final SourceParams createAlipaySingleUseParams(long j, String str, String str2, String str3, String str4) {
            Intrinsics.checkParameterIsNotNull(str, "currency");
            Intrinsics.checkParameterIsNotNull(str4, "returnUrl");
            return new SourceParams("alipay", (Set) null, 2, (DefaultConstructorMarker) null).setCurrency(str).setAmount(Long.valueOf(j)).setReturnUrl(str4).setOwner(new OwnerParams((Address) null, str3, str2, (String) null, 9, (DefaultConstructorMarker) null));
        }

        public static /* synthetic */ SourceParams createWeChatPayParams$default(Companion companion, long j, String str, String str2, String str3, int i, Object obj) {
            if ((i & 8) != 0) {
                str3 = null;
            }
            return companion.createWeChatPayParams(j, str, str2, str3);
        }

        @JvmStatic
        public final SourceParams createWeChatPayParams(long j, String str, String str2, String str3) {
            Intrinsics.checkParameterIsNotNull(str, "currency");
            Intrinsics.checkParameterIsNotNull(str2, "weChatAppId");
            return new SourceParams("wechat", (Set) null, 2, (DefaultConstructorMarker) null).setCurrency(str).setAmount(Long.valueOf(j)).setWeChatParams(new WeChatParams(str2, str3));
        }

        @JvmStatic
        public final SourceParams createKlarna(String str, String str2, KlarnaSourceParams klarnaSourceParams) {
            SourceOrderParams.Item.Type type;
            String str3 = str;
            String str4 = str2;
            Intrinsics.checkParameterIsNotNull(str3, "returnUrl");
            Intrinsics.checkParameterIsNotNull(str4, "currency");
            Intrinsics.checkParameterIsNotNull(klarnaSourceParams, "klarnaParams");
            int i = 0;
            for (KlarnaSourceParams.LineItem totalAmount : klarnaSourceParams.getLineItems()) {
                i += totalAmount.getTotalAmount();
            }
            Iterable<KlarnaSourceParams.LineItem> lineItems = klarnaSourceParams.getLineItems();
            Collection arrayList = new ArrayList(CollectionsKt.collectionSizeOrDefault(lineItems, 10));
            for (KlarnaSourceParams.LineItem lineItem : lineItems) {
                int i2 = WhenMappings.$EnumSwitchMapping$0[lineItem.getItemType().ordinal()];
                if (i2 == 1) {
                    type = SourceOrderParams.Item.Type.Sku;
                } else if (i2 == 2) {
                    type = SourceOrderParams.Item.Type.Tax;
                } else if (i2 == 3) {
                    type = SourceOrderParams.Item.Type.Shipping;
                } else {
                    throw new NoWhenBranchMatchedException();
                }
                SourceOrderParams.Item item = r1;
                SourceOrderParams.Item item2 = new SourceOrderParams.Item(type, Integer.valueOf(lineItem.getTotalAmount()), str2, lineItem.getItemDescription(), (String) null, lineItem.getQuantity(), 16, (DefaultConstructorMarker) null);
                arrayList.add(item);
            }
            SourceOrderParams sourceOrderParams = new SourceOrderParams((List) arrayList, (SourceOrderParams.Shipping) null, 2, (DefaultConstructorMarker) null);
            return new SourceParams("klarna", (Set) null, 2, (DefaultConstructorMarker) null).setAmount(Long.valueOf((long) i)).setCurrency(str4).setReturnUrl(str3).setOwner(new OwnerParams(klarnaSourceParams.getBillingAddress(), klarnaSourceParams.getBillingEmail(), (String) null, klarnaSourceParams.getBillingPhone(), 4, (DefaultConstructorMarker) null)).setExtraParams(MapsKt.mapOf(TuplesKt.to("klarna", klarnaSourceParams.toParamMap()), TuplesKt.to(SourceParams.PARAM_FLOW, "redirect"), TuplesKt.to(SourceParams.PARAM_SOURCE_ORDER, sourceOrderParams.toParamMap())));
        }

        public static /* synthetic */ SourceParams createBancontactParams$default(Companion companion, long j, String str, String str2, String str3, String str4, int i, Object obj) {
            return companion.createBancontactParams(j, str, str2, (i & 8) != 0 ? null : str3, (i & 16) != 0 ? null : str4);
        }

        @JvmStatic
        public final SourceParams createBancontactParams(long j, String str, String str2, String str3, String str4) {
            Intrinsics.checkParameterIsNotNull(str, "name");
            Intrinsics.checkParameterIsNotNull(str2, "returnUrl");
            Map map = null;
            SourceParams returnUrl = new SourceParams("bancontact", (Set) null, 2, (DefaultConstructorMarker) null).setCurrency(Source.EURO).setAmount(Long.valueOf(j)).setOwner(new OwnerParams((Address) null, (String) null, str, (String) null, 11, (DefaultConstructorMarker) null)).setReturnUrl(str2);
            Map emptyMap = MapsKt.emptyMap();
            Map mapOf = str3 != null ? MapsKt.mapOf(TuplesKt.to(SourceParams.PARAM_STATEMENT_DESCRIPTOR, str3)) : null;
            if (mapOf == null) {
                mapOf = MapsKt.emptyMap();
            }
            Map plus = MapsKt.plus(emptyMap, mapOf);
            if (str4 != null) {
                map = MapsKt.mapOf(TuplesKt.to(SourceParams.PARAM_PREFERRED_LANGUAGE, str4));
            }
            if (map == null) {
                map = MapsKt.emptyMap();
            }
            Map plus2 = MapsKt.plus(plus, map);
            if (!plus2.isEmpty()) {
                returnUrl.setApiParameterMap(plus2);
            }
            return returnUrl;
        }

        @JvmStatic
        public final SourceParams createCustomParams(String str) {
            Intrinsics.checkParameterIsNotNull(str, "type");
            return new SourceParams(str, (Set) null, 2, (DefaultConstructorMarker) null);
        }

        @JvmStatic
        public final SourceParams createSourceFromTokenParams(String str) {
            Intrinsics.checkParameterIsNotNull(str, "tokenId");
            return new SourceParams("card", (Set) null, 2, (DefaultConstructorMarker) null).setToken(str);
        }

        @JvmStatic
        public final SourceParams createCardParams(Card card) {
            Intrinsics.checkParameterIsNotNull(card, "card");
            return new SourceParams("card", card.getLoggingTokens$stripe_release(), (DefaultConstructorMarker) null).setApiParameterMap(MapsKt.mapOf(TuplesKt.to(SourceParams.PARAM_NUMBER, card.getNumber()), TuplesKt.to(SourceParams.PARAM_EXP_MONTH, card.getExpMonth()), TuplesKt.to(SourceParams.PARAM_EXP_YEAR, card.getExpYear()), TuplesKt.to(SourceParams.PARAM_CVC, card.getCvc()))).setOwner(new OwnerParams(new Address.Builder().setLine1(card.getAddressLine1()).setLine2(card.getAddressLine2()).setCity(card.getAddressCity()).setState(card.getAddressState()).setPostalCode(card.getAddressZip()).setCountry(card.getAddressCountry()).build(), (String) null, card.getName(), (String) null, 10, (DefaultConstructorMarker) null)).setMetaData(card.getMetadata());
        }

        @JvmStatic
        public final SourceParams createCardParamsFromGooglePay(JSONObject jSONObject) throws JSONException {
            Intrinsics.checkParameterIsNotNull(jSONObject, "googlePayPaymentData");
            GooglePayResult fromJson = GooglePayResult.Companion.fromJson(jSONObject);
            String str = null;
            SourceParams sourceParams = new SourceParams("card", (Set) null, 2, (DefaultConstructorMarker) null);
            Token token = fromJson.getToken();
            if (token != null) {
                str = token.getId();
            }
            if (str != null) {
                return sourceParams.setToken(str).setOwner(new OwnerParams(fromJson.getAddress(), fromJson.getEmail(), fromJson.getName(), fromJson.getPhoneNumber()));
            }
            throw new IllegalArgumentException("Required value was null.".toString());
        }

        public static /* synthetic */ SourceParams createEPSParams$default(Companion companion, long j, String str, String str2, String str3, int i, Object obj) {
            if ((i & 8) != 0) {
                str3 = null;
            }
            return companion.createEPSParams(j, str, str2, str3);
        }

        @JvmStatic
        public final SourceParams createEPSParams(long j, String str, String str2, String str3) {
            Intrinsics.checkParameterIsNotNull(str, "name");
            Intrinsics.checkParameterIsNotNull(str2, "returnUrl");
            Map map = null;
            SourceParams returnUrl = new SourceParams("eps", (Set) null, 2, (DefaultConstructorMarker) null).setCurrency(Source.EURO).setAmount(Long.valueOf(j)).setOwner(new OwnerParams((Address) null, (String) null, str, (String) null, 11, (DefaultConstructorMarker) null)).setReturnUrl(str2);
            if (str3 != null) {
                map = MapsKt.mapOf(TuplesKt.to(SourceParams.PARAM_STATEMENT_DESCRIPTOR, str3));
            }
            return returnUrl.setApiParameterMap(map);
        }

        public static /* synthetic */ SourceParams createGiropayParams$default(Companion companion, long j, String str, String str2, String str3, int i, Object obj) {
            if ((i & 8) != 0) {
                str3 = null;
            }
            return companion.createGiropayParams(j, str, str2, str3);
        }

        @JvmStatic
        public final SourceParams createGiropayParams(long j, String str, String str2, String str3) {
            Intrinsics.checkParameterIsNotNull(str, "name");
            Intrinsics.checkParameterIsNotNull(str2, "returnUrl");
            Map map = null;
            SourceParams returnUrl = new SourceParams("giropay", (Set) null, 2, (DefaultConstructorMarker) null).setCurrency(Source.EURO).setAmount(Long.valueOf(j)).setOwner(new OwnerParams((Address) null, (String) null, str, (String) null, 11, (DefaultConstructorMarker) null)).setReturnUrl(str2);
            if (str3 != null) {
                map = MapsKt.mapOf(TuplesKt.to(SourceParams.PARAM_STATEMENT_DESCRIPTOR, str3));
            }
            return returnUrl.setApiParameterMap(map);
        }

        public static /* synthetic */ SourceParams createIdealParams$default(Companion companion, long j, String str, String str2, String str3, String str4, int i, Object obj) {
            return companion.createIdealParams(j, str, str2, (i & 8) != 0 ? null : str3, (i & 16) != 0 ? null : str4);
        }

        @JvmStatic
        public final SourceParams createIdealParams(long j, String str, String str2, String str3, String str4) {
            Intrinsics.checkParameterIsNotNull(str2, "returnUrl");
            Map map = null;
            SourceParams owner = new SourceParams("ideal", (Set) null, 2, (DefaultConstructorMarker) null).setCurrency(Source.EURO).setAmount(Long.valueOf(j)).setReturnUrl(str2).setOwner(new OwnerParams((Address) null, (String) null, str, (String) null, 11, (DefaultConstructorMarker) null));
            Map emptyMap = MapsKt.emptyMap();
            Map mapOf = str3 != null ? MapsKt.mapOf(TuplesKt.to(SourceParams.PARAM_STATEMENT_DESCRIPTOR, str3)) : null;
            if (mapOf == null) {
                mapOf = MapsKt.emptyMap();
            }
            Map plus = MapsKt.plus(emptyMap, mapOf);
            if (str4 != null) {
                map = MapsKt.mapOf(TuplesKt.to(SourceParams.PARAM_BANK, str4));
            }
            if (map == null) {
                map = MapsKt.emptyMap();
            }
            Map plus2 = MapsKt.plus(plus, map);
            if (!plus2.isEmpty()) {
                owner.setApiParameterMap(plus2);
            }
            return owner;
        }

        @JvmStatic
        public final SourceParams createMultibancoParams(long j, String str, String str2) {
            Intrinsics.checkParameterIsNotNull(str, "returnUrl");
            Intrinsics.checkParameterIsNotNull(str2, "email");
            return new SourceParams("multibanco", (Set) null, 2, (DefaultConstructorMarker) null).setCurrency(Source.EURO).setAmount(Long.valueOf(j)).setReturnUrl(str).setOwner(new OwnerParams((Address) null, str2, (String) null, (String) null, 13, (DefaultConstructorMarker) null));
        }

        @JvmStatic
        public final SourceParams createSepaDebitParams(String str, String str2, String str3, String str4, String str5, String str6) {
            Intrinsics.checkParameterIsNotNull(str, "name");
            Intrinsics.checkParameterIsNotNull(str2, SourceParams.PARAM_IBAN);
            Intrinsics.checkParameterIsNotNull(str4, "city");
            Intrinsics.checkParameterIsNotNull(str5, "postalCode");
            Intrinsics.checkParameterIsNotNull(str6, "country");
            return createSepaDebitParams(str, str2, (String) null, str3, str4, str5, str6);
        }

        @JvmStatic
        public final SourceParams createSepaDebitParams(String str, String str2, String str3, String str4, String str5, String str6, String str7) {
            String str8 = str2;
            Intrinsics.checkParameterIsNotNull(str, "name");
            Intrinsics.checkParameterIsNotNull(str2, SourceParams.PARAM_IBAN);
            String str9 = str4;
            return new SourceParams("sepa_debit", (Set) null, 2, (DefaultConstructorMarker) null).setCurrency(Source.EURO).setOwner(new OwnerParams(new Address.Builder().setLine1(str4).setCity(str5).setPostalCode(str6).setCountry(str7).build(), str3, str, (String) null, 8, (DefaultConstructorMarker) null)).setApiParameterMap(MapsKt.mapOf(TuplesKt.to(SourceParams.PARAM_IBAN, str2)));
        }

        public static /* synthetic */ SourceParams createSofortParams$default(Companion companion, long j, String str, String str2, String str3, int i, Object obj) {
            if ((i & 8) != 0) {
                str3 = null;
            }
            return companion.createSofortParams(j, str, str2, str3);
        }

        @JvmStatic
        public final SourceParams createSofortParams(long j, String str, String str2, String str3) {
            Intrinsics.checkParameterIsNotNull(str, "returnUrl");
            Intrinsics.checkParameterIsNotNull(str2, "country");
            Map mapOf = MapsKt.mapOf(TuplesKt.to("country", str2));
            Map mapOf2 = str3 != null ? MapsKt.mapOf(TuplesKt.to(SourceParams.PARAM_STATEMENT_DESCRIPTOR, str3)) : null;
            if (mapOf2 == null) {
                mapOf2 = MapsKt.emptyMap();
            }
            return new SourceParams("sofort", (Set) null, 2, (DefaultConstructorMarker) null).setCurrency(Source.EURO).setAmount(Long.valueOf(j)).setReturnUrl(str).setApiParameterMap(MapsKt.plus(mapOf, mapOf2));
        }

        @JvmStatic
        public final SourceParams createThreeDSecureParams(long j, String str, String str2, String str3) {
            Intrinsics.checkParameterIsNotNull(str, "currency");
            Intrinsics.checkParameterIsNotNull(str2, "returnUrl");
            Intrinsics.checkParameterIsNotNull(str3, "cardId");
            return new SourceParams("three_d_secure", (Set) null, 2, (DefaultConstructorMarker) null).setCurrency(str).setAmount(Long.valueOf(j)).setReturnUrl(str2).setApiParameterMap(MapsKt.mapOf(TuplesKt.to("card", str3)));
        }

        @JvmStatic
        public final SourceParams createVisaCheckoutParams(String str) {
            Intrinsics.checkParameterIsNotNull(str, "callId");
            return new SourceParams("card", (Set) null, 2, (DefaultConstructorMarker) null).setApiParameterMap(MapsKt.mapOf(TuplesKt.to(SourceParams.PARAM_VISA_CHECKOUT, MapsKt.mapOf(TuplesKt.to(SourceParams.PARAM_CALL_ID, str)))));
        }

        @JvmStatic
        public final SourceParams createMasterpassParams(String str, String str2) {
            Intrinsics.checkParameterIsNotNull(str, "transactionId");
            Intrinsics.checkParameterIsNotNull(str2, "cartId");
            return new SourceParams("card", (Set) null, 2, (DefaultConstructorMarker) null).setApiParameterMap(MapsKt.mapOf(TuplesKt.to(SourceParams.PARAM_MASTERPASS, MapsKt.mapOf(TuplesKt.to("transaction_id", str), TuplesKt.to(SourceParams.PARAM_CART_ID, str2)))));
        }

        @JvmStatic
        public final Map<String, String> createRetrieveSourceParams(String str) {
            Intrinsics.checkParameterIsNotNull(str, "clientSecret");
            return MapsKt.mapOf(TuplesKt.to("client_secret", str));
        }
    }
}
