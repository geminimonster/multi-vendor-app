package com.stripe.android.model;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import java.util.List;
import java.util.Map;
import kotlin.Deprecated;
import kotlin.Metadata;
import kotlin.ReplaceWith;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000Z\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\t\n\u0002\b\u0007\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010 \n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\f\bf\u0018\u00002\u00020\u0001:\u0006456789J\b\u00102\u001a\u00020\u000fH&J\b\u00103\u001a\u00020\u000fH&R\u0014\u0010\u0002\u001a\u0004\u0018\u00010\u0003X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0004\u0010\u0005R\u0012\u0010\u0006\u001a\u00020\u0007X¦\u0004¢\u0006\u0006\u001a\u0004\b\b\u0010\tR\u0014\u0010\n\u001a\u0004\u0018\u00010\u0003X¦\u0004¢\u0006\u0006\u001a\u0004\b\u000b\u0010\u0005R\u0014\u0010\f\u001a\u0004\u0018\u00010\u0003X¦\u0004¢\u0006\u0006\u001a\u0004\b\r\u0010\u0005R\u0012\u0010\u000e\u001a\u00020\u000fX¦\u0004¢\u0006\u0006\u001a\u0004\b\u000e\u0010\u0010R\u0014\u0010\u0011\u001a\u0004\u0018\u00010\u0012X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0013\u0010\u0014R\u0014\u0010\u0015\u001a\u0004\u0018\u00010\u0016X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0017\u0010\u0018R\u0014\u0010\u0019\u001a\u0004\u0018\u00010\u001aX¦\u0004¢\u0006\u0006\u001a\u0004\b\u001b\u0010\u001cR\u0014\u0010\u001d\u001a\u0004\u0018\u00010\u0003X¦\u0004¢\u0006\u0006\u001a\u0004\b\u001e\u0010\u0005R\u0018\u0010\u001f\u001a\b\u0012\u0004\u0012\u00020\u00030 X¦\u0004¢\u0006\u0006\u001a\u0004\b!\u0010\"R\u001c\u0010#\u001a\u0004\u0018\u00010$8&X§\u0004¢\u0006\f\u0012\u0004\b%\u0010&\u001a\u0004\b'\u0010(R\u0014\u0010)\u001a\u0004\u0018\u00010*X¦\u0004¢\u0006\u0006\u001a\u0004\b+\u0010,R\u001c\u0010-\u001a\u0004\u0018\u00010.8&X§\u0004¢\u0006\f\u0012\u0004\b/\u0010&\u001a\u0004\b0\u00101¨\u0006:"}, d2 = {"Lcom/stripe/android/model/StripeIntent;", "Lcom/stripe/android/model/StripeModel;", "clientSecret", "", "getClientSecret", "()Ljava/lang/String;", "created", "", "getCreated", "()J", "description", "getDescription", "id", "getId", "isLiveMode", "", "()Z", "nextActionData", "Lcom/stripe/android/model/StripeIntent$NextActionData;", "getNextActionData", "()Lcom/stripe/android/model/StripeIntent$NextActionData;", "nextActionType", "Lcom/stripe/android/model/StripeIntent$NextActionType;", "getNextActionType", "()Lcom/stripe/android/model/StripeIntent$NextActionType;", "paymentMethod", "Lcom/stripe/android/model/PaymentMethod;", "getPaymentMethod", "()Lcom/stripe/android/model/PaymentMethod;", "paymentMethodId", "getPaymentMethodId", "paymentMethodTypes", "", "getPaymentMethodTypes", "()Ljava/util/List;", "redirectData", "Lcom/stripe/android/model/StripeIntent$RedirectData;", "redirectData$annotations", "()V", "getRedirectData", "()Lcom/stripe/android/model/StripeIntent$RedirectData;", "status", "Lcom/stripe/android/model/StripeIntent$Status;", "getStatus", "()Lcom/stripe/android/model/StripeIntent$Status;", "stripeSdkData", "Lcom/stripe/android/model/StripeIntent$SdkData;", "stripeSdkData$annotations", "getStripeSdkData", "()Lcom/stripe/android/model/StripeIntent$SdkData;", "requiresAction", "requiresConfirmation", "NextActionData", "NextActionType", "RedirectData", "SdkData", "Status", "Usage", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: StripeIntent.kt */
public interface StripeIntent extends StripeModel {

    @Metadata(bv = {1, 0, 3}, k = 3, mv = {1, 1, 16})
    /* compiled from: StripeIntent.kt */
    public static final class DefaultImpls {
        @Deprecated(message = "use {@link #nextActionData}", replaceWith = @ReplaceWith(expression = "nextActionData as? StripeIntent.NextActionData.RedirectToUrl", imports = {}))
        public static /* synthetic */ void redirectData$annotations() {
        }

        @Deprecated(message = "use {@link #nextActionData}", replaceWith = @ReplaceWith(expression = "nextActionData as? StripeIntent.NextActionData.SdkData", imports = {}))
        public static /* synthetic */ void stripeSdkData$annotations() {
        }
    }

    String getClientSecret();

    long getCreated();

    String getDescription();

    String getId();

    NextActionData getNextActionData();

    NextActionType getNextActionType();

    PaymentMethod getPaymentMethod();

    String getPaymentMethodId();

    List<String> getPaymentMethodTypes();

    RedirectData getRedirectData();

    Status getStatus();

    SdkData getStripeSdkData();

    boolean isLiveMode();

    boolean requiresAction();

    boolean requiresConfirmation();

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\u000e\n\u0002\b\t\b\u0001\u0018\u0000 \u000b2\b\u0012\u0004\u0012\u00020\u00000\u0001:\u0001\u000bB\u000f\b\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\b\u0010\u0007\u001a\u00020\u0003H\u0016R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006j\u0002\b\bj\u0002\b\tj\u0002\b\n¨\u0006\f"}, d2 = {"Lcom/stripe/android/model/StripeIntent$NextActionType;", "", "code", "", "(Ljava/lang/String;ILjava/lang/String;)V", "getCode", "()Ljava/lang/String;", "toString", "RedirectToUrl", "UseStripeSdk", "DisplayOxxoDetails", "Companion", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: StripeIntent.kt */
    public enum NextActionType {
        RedirectToUrl("redirect_to_url"),
        UseStripeSdk("use_stripe_sdk"),
        DisplayOxxoDetails("display_oxxo_details");
        
        public static final Companion Companion = null;
        private final String code;

        private NextActionType(String str) {
            this.code = str;
        }

        public final String getCode() {
            return this.code;
        }

        static {
            Companion = new Companion((DefaultConstructorMarker) null);
        }

        public String toString() {
            return this.code;
        }

        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0019\u0010\u0003\u001a\u0004\u0018\u00010\u00042\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006H\u0000¢\u0006\u0002\b\u0007¨\u0006\b"}, d2 = {"Lcom/stripe/android/model/StripeIntent$NextActionType$Companion;", "", "()V", "fromCode", "Lcom/stripe/android/model/StripeIntent$NextActionType;", "code", "", "fromCode$stripe_release", "stripe_release"}, k = 1, mv = {1, 1, 16})
        /* compiled from: StripeIntent.kt */
        public static final class Companion {
            private Companion() {
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }

            public final NextActionType fromCode$stripe_release(String str) {
                for (NextActionType nextActionType : NextActionType.values()) {
                    if (Intrinsics.areEqual((Object) nextActionType.getCode(), (Object) str)) {
                        return nextActionType;
                    }
                }
                return null;
            }
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\u000e\n\u0002\b\r\b\u0001\u0018\u0000 \u000f2\b\u0012\u0004\u0012\u00020\u00000\u0001:\u0001\u000fB\u000f\b\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\b\u0010\u0007\u001a\u00020\u0003H\u0016R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006j\u0002\b\bj\u0002\b\tj\u0002\b\nj\u0002\b\u000bj\u0002\b\fj\u0002\b\rj\u0002\b\u000e¨\u0006\u0010"}, d2 = {"Lcom/stripe/android/model/StripeIntent$Status;", "", "code", "", "(Ljava/lang/String;ILjava/lang/String;)V", "getCode", "()Ljava/lang/String;", "toString", "Canceled", "Processing", "RequiresAction", "RequiresConfirmation", "RequiresPaymentMethod", "Succeeded", "RequiresCapture", "Companion", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: StripeIntent.kt */
    public enum Status {
        Canceled("canceled"),
        Processing("processing"),
        RequiresAction("requires_action"),
        RequiresConfirmation("requires_confirmation"),
        RequiresPaymentMethod("requires_payment_method"),
        Succeeded("succeeded"),
        RequiresCapture("requires_capture");
        
        public static final Companion Companion = null;
        private final String code;

        private Status(String str) {
            this.code = str;
        }

        public final String getCode() {
            return this.code;
        }

        static {
            Companion = new Companion((DefaultConstructorMarker) null);
        }

        public String toString() {
            return this.code;
        }

        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0019\u0010\u0003\u001a\u0004\u0018\u00010\u00042\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006H\u0000¢\u0006\u0002\b\u0007¨\u0006\b"}, d2 = {"Lcom/stripe/android/model/StripeIntent$Status$Companion;", "", "()V", "fromCode", "Lcom/stripe/android/model/StripeIntent$Status;", "code", "", "fromCode$stripe_release", "stripe_release"}, k = 1, mv = {1, 1, 16})
        /* compiled from: StripeIntent.kt */
        public static final class Companion {
            private Companion() {
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }

            public final Status fromCode$stripe_release(String str) {
                for (Status status : Status.values()) {
                    if (Intrinsics.areEqual((Object) status.getCode(), (Object) str)) {
                        return status;
                    }
                }
                return null;
            }
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\u000e\n\u0002\b\t\b\u0001\u0018\u0000 \u000b2\b\u0012\u0004\u0012\u00020\u00000\u0001:\u0001\u000bB\u000f\b\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\b\u0010\u0007\u001a\u00020\u0003H\u0016R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006j\u0002\b\bj\u0002\b\tj\u0002\b\n¨\u0006\f"}, d2 = {"Lcom/stripe/android/model/StripeIntent$Usage;", "", "code", "", "(Ljava/lang/String;ILjava/lang/String;)V", "getCode", "()Ljava/lang/String;", "toString", "OnSession", "OffSession", "OneTime", "Companion", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: StripeIntent.kt */
    public enum Usage {
        OnSession("on_session"),
        OffSession("off_session"),
        OneTime("one_time");
        
        public static final Companion Companion = null;
        private final String code;

        private Usage(String str) {
            this.code = str;
        }

        public final String getCode() {
            return this.code;
        }

        static {
            Companion = new Companion((DefaultConstructorMarker) null);
        }

        public String toString() {
            return this.code;
        }

        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0019\u0010\u0003\u001a\u0004\u0018\u00010\u00042\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006H\u0000¢\u0006\u0002\b\u0007¨\u0006\b"}, d2 = {"Lcom/stripe/android/model/StripeIntent$Usage$Companion;", "", "()V", "fromCode", "Lcom/stripe/android/model/StripeIntent$Usage;", "code", "", "fromCode$stripe_release", "stripe_release"}, k = 1, mv = {1, 1, 16})
        /* compiled from: StripeIntent.kt */
        public static final class Companion {
            private Companion() {
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }

            public final Usage fromCode$stripe_release(String str) {
                for (Usage usage : Usage.values()) {
                    if (Intrinsics.areEqual((Object) usage.getCode(), (Object) str)) {
                        return usage;
                    }
                }
                return null;
            }
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\t\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B\u0017\b\u0000\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003¢\u0006\u0002\u0010\u0005J\t\u0010\u0007\u001a\u00020\u0003HÆ\u0003J\t\u0010\b\u001a\u00020\u0003HÆ\u0003J\u001d\u0010\t\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u0003HÆ\u0001J\u0013\u0010\n\u001a\u00020\u00032\b\u0010\u000b\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\f\u001a\u00020\rHÖ\u0001J\t\u0010\u000e\u001a\u00020\u000fHÖ\u0001R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0002\u0010\u0006R\u0011\u0010\u0004\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0004\u0010\u0006¨\u0006\u0010"}, d2 = {"Lcom/stripe/android/model/StripeIntent$SdkData;", "", "is3ds1", "", "is3ds2", "(ZZ)V", "()Z", "component1", "component2", "copy", "equals", "other", "hashCode", "", "toString", "", "stripe_release"}, k = 1, mv = {1, 1, 16})
    @Deprecated(message = "use [StripeIntent.NextActionData.SdkData]")
    /* compiled from: StripeIntent.kt */
    public static final class SdkData {
        private final boolean is3ds1;
        private final boolean is3ds2;

        public static /* synthetic */ SdkData copy$default(SdkData sdkData, boolean z, boolean z2, int i, Object obj) {
            if ((i & 1) != 0) {
                z = sdkData.is3ds1;
            }
            if ((i & 2) != 0) {
                z2 = sdkData.is3ds2;
            }
            return sdkData.copy(z, z2);
        }

        public final boolean component1() {
            return this.is3ds1;
        }

        public final boolean component2() {
            return this.is3ds2;
        }

        public final SdkData copy(boolean z, boolean z2) {
            return new SdkData(z, z2);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof SdkData)) {
                return false;
            }
            SdkData sdkData = (SdkData) obj;
            return this.is3ds1 == sdkData.is3ds1 && this.is3ds2 == sdkData.is3ds2;
        }

        public int hashCode() {
            boolean z = this.is3ds1;
            boolean z2 = true;
            if (z) {
                z = true;
            }
            int i = (z ? 1 : 0) * true;
            boolean z3 = this.is3ds2;
            if (!z3) {
                z2 = z3;
            }
            return i + (z2 ? 1 : 0);
        }

        public String toString() {
            return "SdkData(is3ds1=" + this.is3ds1 + ", is3ds2=" + this.is3ds2 + ")";
        }

        public SdkData(boolean z, boolean z2) {
            this.is3ds1 = z;
            this.is3ds2 = z2;
        }

        public final boolean is3ds1() {
            return this.is3ds1;
        }

        public final boolean is3ds2() {
            return this.is3ds2;
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\t\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\b\b\u0018\u0000 \u001b2\u00020\u0001:\u0001\u001bB\u0019\b\u0000\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\b\u0010\u0004\u001a\u0004\u0018\u00010\u0005¢\u0006\u0002\u0010\u0006J\t\u0010\u000b\u001a\u00020\u0003HÆ\u0003J\u000b\u0010\f\u001a\u0004\u0018\u00010\u0005HÆ\u0003J\u001f\u0010\r\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005HÆ\u0001J\t\u0010\u000e\u001a\u00020\u000fHÖ\u0001J\u0013\u0010\u0010\u001a\u00020\u00112\b\u0010\u0012\u001a\u0004\u0018\u00010\u0013HÖ\u0003J\t\u0010\u0014\u001a\u00020\u000fHÖ\u0001J\t\u0010\u0015\u001a\u00020\u0005HÖ\u0001J\u0019\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00020\u000fHÖ\u0001R\u0013\u0010\u0004\u001a\u0004\u0018\u00010\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n¨\u0006\u001c"}, d2 = {"Lcom/stripe/android/model/StripeIntent$RedirectData;", "Landroid/os/Parcelable;", "url", "Landroid/net/Uri;", "returnUrl", "", "(Landroid/net/Uri;Ljava/lang/String;)V", "getReturnUrl", "()Ljava/lang/String;", "getUrl", "()Landroid/net/Uri;", "component1", "component2", "copy", "describeContents", "", "equals", "", "other", "", "hashCode", "toString", "writeToParcel", "", "parcel", "Landroid/os/Parcel;", "flags", "Companion", "stripe_release"}, k = 1, mv = {1, 1, 16})
    @Deprecated(message = "use {@link StripeIntent.NextActionData.RedirectToUrl}")
    /* compiled from: StripeIntent.kt */
    public static final class RedirectData implements Parcelable {
        public static final Parcelable.Creator CREATOR = new Creator();
        public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
        public static final String FIELD_RETURN_URL = "return_url";
        public static final String FIELD_URL = "url";
        private final String returnUrl;
        private final Uri url;

        @Metadata(bv = {1, 0, 3}, k = 3, mv = {1, 1, 16})
        public static class Creator implements Parcelable.Creator {
            public final Object createFromParcel(Parcel parcel) {
                Intrinsics.checkParameterIsNotNull(parcel, "in");
                return new RedirectData((Uri) parcel.readParcelable(RedirectData.class.getClassLoader()), parcel.readString());
            }

            public final Object[] newArray(int i) {
                return new RedirectData[i];
            }
        }

        public static /* synthetic */ RedirectData copy$default(RedirectData redirectData, Uri uri, String str, int i, Object obj) {
            if ((i & 1) != 0) {
                uri = redirectData.url;
            }
            if ((i & 2) != 0) {
                str = redirectData.returnUrl;
            }
            return redirectData.copy(uri, str);
        }

        public final Uri component1() {
            return this.url;
        }

        public final String component2() {
            return this.returnUrl;
        }

        public final RedirectData copy(Uri uri, String str) {
            Intrinsics.checkParameterIsNotNull(uri, "url");
            return new RedirectData(uri, str);
        }

        public int describeContents() {
            return 0;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof RedirectData)) {
                return false;
            }
            RedirectData redirectData = (RedirectData) obj;
            return Intrinsics.areEqual((Object) this.url, (Object) redirectData.url) && Intrinsics.areEqual((Object) this.returnUrl, (Object) redirectData.returnUrl);
        }

        public int hashCode() {
            Uri uri = this.url;
            int i = 0;
            int hashCode = (uri != null ? uri.hashCode() : 0) * 31;
            String str = this.returnUrl;
            if (str != null) {
                i = str.hashCode();
            }
            return hashCode + i;
        }

        public String toString() {
            return "RedirectData(url=" + this.url + ", returnUrl=" + this.returnUrl + ")";
        }

        public void writeToParcel(Parcel parcel, int i) {
            Intrinsics.checkParameterIsNotNull(parcel, "parcel");
            parcel.writeParcelable(this.url, i);
            parcel.writeString(this.returnUrl);
        }

        public RedirectData(Uri uri, String str) {
            Intrinsics.checkParameterIsNotNull(uri, "url");
            this.url = uri;
            this.returnUrl = str;
        }

        public final Uri getUrl() {
            return this.url;
        }

        public final String getReturnUrl() {
            return this.returnUrl;
        }

        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\b\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u001f\u0010\u0006\u001a\u0004\u0018\u00010\u00072\u000e\u0010\b\u001a\n\u0012\u0002\b\u0003\u0012\u0002\b\u00030\tH\u0000¢\u0006\u0002\b\nR\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\u000b"}, d2 = {"Lcom/stripe/android/model/StripeIntent$RedirectData$Companion;", "", "()V", "FIELD_RETURN_URL", "", "FIELD_URL", "create", "Lcom/stripe/android/model/StripeIntent$RedirectData;", "redirectToUrlHash", "", "create$stripe_release", "stripe_release"}, k = 1, mv = {1, 1, 16})
        /* compiled from: StripeIntent.kt */
        public static final class Companion {
            private Companion() {
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }

            public final /* synthetic */ RedirectData create$stripe_release(Map<?, ?> map) {
                Intrinsics.checkParameterIsNotNull(map, "redirectToUrlHash");
                Object obj = map.get("url");
                Object obj2 = map.get("return_url");
                String obj3 = obj instanceof String ? obj.toString() : null;
                String obj4 = obj2 instanceof String ? obj2.toString() : null;
                if (obj3 == null) {
                    return null;
                }
                Uri parse = Uri.parse(obj3);
                Intrinsics.checkExpressionValueIsNotNull(parse, "Uri.parse(it)");
                return new RedirectData(parse, obj4);
            }
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0003\u0003\u0004\u0005B\u0007\b\u0002¢\u0006\u0002\u0010\u0002\u0001\u0003\u0006\u0007\b¨\u0006\t"}, d2 = {"Lcom/stripe/android/model/StripeIntent$NextActionData;", "Lcom/stripe/android/model/StripeModel;", "()V", "DisplayOxxoDetails", "RedirectToUrl", "SdkData", "Lcom/stripe/android/model/StripeIntent$NextActionData$DisplayOxxoDetails;", "Lcom/stripe/android/model/StripeIntent$NextActionData$RedirectToUrl;", "Lcom/stripe/android/model/StripeIntent$NextActionData$SdkData;", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: StripeIntent.kt */
    public static abstract class NextActionData implements StripeModel {
        private NextActionData() {
        }

        public /* synthetic */ NextActionData(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        @Metadata(bv = {1, 0, 3}, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0002\b\n\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\b\b\u0018\u00002\u00020\u0001B\u001b\u0012\b\b\u0002\u0010\u0002\u001a\u00020\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005¢\u0006\u0002\u0010\u0006J\t\u0010\u000b\u001a\u00020\u0003HÆ\u0003J\u000b\u0010\f\u001a\u0004\u0018\u00010\u0005HÆ\u0003J\u001f\u0010\r\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005HÆ\u0001J\t\u0010\u000e\u001a\u00020\u0003HÖ\u0001J\u0013\u0010\u000f\u001a\u00020\u00102\b\u0010\u0011\u001a\u0004\u0018\u00010\u0012HÖ\u0003J\t\u0010\u0013\u001a\u00020\u0003HÖ\u0001J\t\u0010\u0014\u001a\u00020\u0005HÖ\u0001J\u0019\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00020\u0003HÖ\u0001R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0013\u0010\u0004\u001a\u0004\u0018\u00010\u0005¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n¨\u0006\u001a"}, d2 = {"Lcom/stripe/android/model/StripeIntent$NextActionData$DisplayOxxoDetails;", "Lcom/stripe/android/model/StripeIntent$NextActionData;", "expiresAfter", "", "number", "", "(ILjava/lang/String;)V", "getExpiresAfter", "()I", "getNumber", "()Ljava/lang/String;", "component1", "component2", "copy", "describeContents", "equals", "", "other", "", "hashCode", "toString", "writeToParcel", "", "parcel", "Landroid/os/Parcel;", "flags", "stripe_release"}, k = 1, mv = {1, 1, 16})
        /* compiled from: StripeIntent.kt */
        public static final class DisplayOxxoDetails extends NextActionData {
            public static final Parcelable.Creator CREATOR = new Creator();
            private final int expiresAfter;
            private final String number;

            @Metadata(bv = {1, 0, 3}, k = 3, mv = {1, 1, 16})
            public static class Creator implements Parcelable.Creator {
                public final Object createFromParcel(Parcel parcel) {
                    Intrinsics.checkParameterIsNotNull(parcel, "in");
                    return new DisplayOxxoDetails(parcel.readInt(), parcel.readString());
                }

                public final Object[] newArray(int i) {
                    return new DisplayOxxoDetails[i];
                }
            }

            public DisplayOxxoDetails() {
                this(0, (String) null, 3, (DefaultConstructorMarker) null);
            }

            public static /* synthetic */ DisplayOxxoDetails copy$default(DisplayOxxoDetails displayOxxoDetails, int i, String str, int i2, Object obj) {
                if ((i2 & 1) != 0) {
                    i = displayOxxoDetails.expiresAfter;
                }
                if ((i2 & 2) != 0) {
                    str = displayOxxoDetails.number;
                }
                return displayOxxoDetails.copy(i, str);
            }

            public final int component1() {
                return this.expiresAfter;
            }

            public final String component2() {
                return this.number;
            }

            public final DisplayOxxoDetails copy(int i, String str) {
                return new DisplayOxxoDetails(i, str);
            }

            public int describeContents() {
                return 0;
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof DisplayOxxoDetails)) {
                    return false;
                }
                DisplayOxxoDetails displayOxxoDetails = (DisplayOxxoDetails) obj;
                return this.expiresAfter == displayOxxoDetails.expiresAfter && Intrinsics.areEqual((Object) this.number, (Object) displayOxxoDetails.number);
            }

            public int hashCode() {
                int i = this.expiresAfter * 31;
                String str = this.number;
                return i + (str != null ? str.hashCode() : 0);
            }

            public String toString() {
                return "DisplayOxxoDetails(expiresAfter=" + this.expiresAfter + ", number=" + this.number + ")";
            }

            public void writeToParcel(Parcel parcel, int i) {
                Intrinsics.checkParameterIsNotNull(parcel, "parcel");
                parcel.writeInt(this.expiresAfter);
                parcel.writeString(this.number);
            }

            public final int getExpiresAfter() {
                return this.expiresAfter;
            }

            /* JADX INFO: this call moved to the top of the method (can break code semantics) */
            public /* synthetic */ DisplayOxxoDetails(int i, String str, int i2, DefaultConstructorMarker defaultConstructorMarker) {
                this((i2 & 1) != 0 ? 0 : i, (i2 & 2) != 0 ? null : str);
            }

            public final String getNumber() {
                return this.number;
            }

            public DisplayOxxoDetails(int i, String str) {
                super((DefaultConstructorMarker) null);
                this.expiresAfter = i;
                this.number = str;
            }
        }

        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\f\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\b\b\u0018\u00002\u00020\u0001:\u0001 B!\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\b\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\b\u0010\u0006\u001a\u0004\u0018\u00010\u0007¢\u0006\u0002\u0010\bJ\t\u0010\u000f\u001a\u00020\u0003HÆ\u0003J\u000b\u0010\u0010\u001a\u0004\u0018\u00010\u0005HÆ\u0003J\u000b\u0010\u0011\u001a\u0004\u0018\u00010\u0007HÆ\u0003J+\u0010\u0012\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u0007HÆ\u0001J\t\u0010\u0013\u001a\u00020\u0014HÖ\u0001J\u0013\u0010\u0015\u001a\u00020\u00162\b\u0010\u0017\u001a\u0004\u0018\u00010\u0018HÖ\u0003J\t\u0010\u0019\u001a\u00020\u0014HÖ\u0001J\t\u0010\u001a\u001a\u00020\u0005HÖ\u0001J\u0019\u0010\u001b\u001a\u00020\u001c2\u0006\u0010\u001d\u001a\u00020\u001e2\u0006\u0010\u001f\u001a\u00020\u0014HÖ\u0001R\u0013\u0010\u0006\u001a\u0004\u0018\u00010\u0007¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0013\u0010\u0004\u001a\u0004\u0018\u00010\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000e¨\u0006!"}, d2 = {"Lcom/stripe/android/model/StripeIntent$NextActionData$RedirectToUrl;", "Lcom/stripe/android/model/StripeIntent$NextActionData;", "url", "Landroid/net/Uri;", "returnUrl", "", "mobileData", "Lcom/stripe/android/model/StripeIntent$NextActionData$RedirectToUrl$MobileData;", "(Landroid/net/Uri;Ljava/lang/String;Lcom/stripe/android/model/StripeIntent$NextActionData$RedirectToUrl$MobileData;)V", "getMobileData", "()Lcom/stripe/android/model/StripeIntent$NextActionData$RedirectToUrl$MobileData;", "getReturnUrl", "()Ljava/lang/String;", "getUrl", "()Landroid/net/Uri;", "component1", "component2", "component3", "copy", "describeContents", "", "equals", "", "other", "", "hashCode", "toString", "writeToParcel", "", "parcel", "Landroid/os/Parcel;", "flags", "MobileData", "stripe_release"}, k = 1, mv = {1, 1, 16})
        /* compiled from: StripeIntent.kt */
        public static final class RedirectToUrl extends NextActionData {
            public static final Parcelable.Creator CREATOR = new Creator();
            private final MobileData mobileData;
            private final String returnUrl;
            private final Uri url;

            @Metadata(bv = {1, 0, 3}, k = 3, mv = {1, 1, 16})
            public static class Creator implements Parcelable.Creator {
                public final Object createFromParcel(Parcel parcel) {
                    Intrinsics.checkParameterIsNotNull(parcel, "in");
                    return new RedirectToUrl((Uri) parcel.readParcelable(RedirectToUrl.class.getClassLoader()), parcel.readString(), (MobileData) parcel.readParcelable(RedirectToUrl.class.getClassLoader()));
                }

                public final Object[] newArray(int i) {
                    return new RedirectToUrl[i];
                }
            }

            public static /* synthetic */ RedirectToUrl copy$default(RedirectToUrl redirectToUrl, Uri uri, String str, MobileData mobileData2, int i, Object obj) {
                if ((i & 1) != 0) {
                    uri = redirectToUrl.url;
                }
                if ((i & 2) != 0) {
                    str = redirectToUrl.returnUrl;
                }
                if ((i & 4) != 0) {
                    mobileData2 = redirectToUrl.mobileData;
                }
                return redirectToUrl.copy(uri, str, mobileData2);
            }

            public final Uri component1() {
                return this.url;
            }

            public final String component2() {
                return this.returnUrl;
            }

            public final MobileData component3() {
                return this.mobileData;
            }

            public final RedirectToUrl copy(Uri uri, String str, MobileData mobileData2) {
                Intrinsics.checkParameterIsNotNull(uri, "url");
                return new RedirectToUrl(uri, str, mobileData2);
            }

            public int describeContents() {
                return 0;
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof RedirectToUrl)) {
                    return false;
                }
                RedirectToUrl redirectToUrl = (RedirectToUrl) obj;
                return Intrinsics.areEqual((Object) this.url, (Object) redirectToUrl.url) && Intrinsics.areEqual((Object) this.returnUrl, (Object) redirectToUrl.returnUrl) && Intrinsics.areEqual((Object) this.mobileData, (Object) redirectToUrl.mobileData);
            }

            public int hashCode() {
                Uri uri = this.url;
                int i = 0;
                int hashCode = (uri != null ? uri.hashCode() : 0) * 31;
                String str = this.returnUrl;
                int hashCode2 = (hashCode + (str != null ? str.hashCode() : 0)) * 31;
                MobileData mobileData2 = this.mobileData;
                if (mobileData2 != null) {
                    i = mobileData2.hashCode();
                }
                return hashCode2 + i;
            }

            public String toString() {
                return "RedirectToUrl(url=" + this.url + ", returnUrl=" + this.returnUrl + ", mobileData=" + this.mobileData + ")";
            }

            public void writeToParcel(Parcel parcel, int i) {
                Intrinsics.checkParameterIsNotNull(parcel, "parcel");
                parcel.writeParcelable(this.url, i);
                parcel.writeString(this.returnUrl);
                parcel.writeParcelable(this.mobileData, i);
            }

            public final Uri getUrl() {
                return this.url;
            }

            public final String getReturnUrl() {
                return this.returnUrl;
            }

            public final MobileData getMobileData() {
                return this.mobileData;
            }

            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public RedirectToUrl(Uri uri, String str, MobileData mobileData2) {
                super((DefaultConstructorMarker) null);
                Intrinsics.checkParameterIsNotNull(uri, "url");
                this.url = uri;
                this.returnUrl = str;
                this.mobileData = mobileData2;
            }

            @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0001\u0003B\u0007\b\u0002¢\u0006\u0002\u0010\u0002\u0001\u0001\u0004¨\u0006\u0005"}, d2 = {"Lcom/stripe/android/model/StripeIntent$NextActionData$RedirectToUrl$MobileData;", "Lcom/stripe/android/model/StripeModel;", "()V", "Alipay", "Lcom/stripe/android/model/StripeIntent$NextActionData$RedirectToUrl$MobileData$Alipay;", "stripe_release"}, k = 1, mv = {1, 1, 16})
            /* compiled from: StripeIntent.kt */
            public static abstract class MobileData implements StripeModel {
                private MobileData() {
                }

                public /* synthetic */ MobileData(DefaultConstructorMarker defaultConstructorMarker) {
                    this();
                }

                @Metadata(bv = {1, 0, 3}, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0006\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\b\b\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\t\u0010\u0007\u001a\u00020\u0003HÆ\u0003J\u0013\u0010\b\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u0003HÆ\u0001J\t\u0010\t\u001a\u00020\nHÖ\u0001J\u0013\u0010\u000b\u001a\u00020\f2\b\u0010\r\u001a\u0004\u0018\u00010\u000eHÖ\u0003J\t\u0010\u000f\u001a\u00020\nHÖ\u0001J\t\u0010\u0010\u001a\u00020\u0003HÖ\u0001J\u0019\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\nHÖ\u0001R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006¨\u0006\u0016"}, d2 = {"Lcom/stripe/android/model/StripeIntent$NextActionData$RedirectToUrl$MobileData$Alipay;", "Lcom/stripe/android/model/StripeIntent$NextActionData$RedirectToUrl$MobileData;", "data", "", "(Ljava/lang/String;)V", "getData", "()Ljava/lang/String;", "component1", "copy", "describeContents", "", "equals", "", "other", "", "hashCode", "toString", "writeToParcel", "", "parcel", "Landroid/os/Parcel;", "flags", "stripe_release"}, k = 1, mv = {1, 1, 16})
                /* compiled from: StripeIntent.kt */
                public static final class Alipay extends MobileData {
                    public static final Parcelable.Creator CREATOR = new Creator();
                    private final String data;

                    @Metadata(bv = {1, 0, 3}, k = 3, mv = {1, 1, 16})
                    public static class Creator implements Parcelable.Creator {
                        public final Object createFromParcel(Parcel parcel) {
                            Intrinsics.checkParameterIsNotNull(parcel, "in");
                            return new Alipay(parcel.readString());
                        }

                        public final Object[] newArray(int i) {
                            return new Alipay[i];
                        }
                    }

                    public static /* synthetic */ Alipay copy$default(Alipay alipay, String str, int i, Object obj) {
                        if ((i & 1) != 0) {
                            str = alipay.data;
                        }
                        return alipay.copy(str);
                    }

                    public final String component1() {
                        return this.data;
                    }

                    public final Alipay copy(String str) {
                        Intrinsics.checkParameterIsNotNull(str, "data");
                        return new Alipay(str);
                    }

                    public int describeContents() {
                        return 0;
                    }

                    public boolean equals(Object obj) {
                        if (this != obj) {
                            return (obj instanceof Alipay) && Intrinsics.areEqual((Object) this.data, (Object) ((Alipay) obj).data);
                        }
                        return true;
                    }

                    public int hashCode() {
                        String str = this.data;
                        if (str != null) {
                            return str.hashCode();
                        }
                        return 0;
                    }

                    public String toString() {
                        return "Alipay(data=" + this.data + ")";
                    }

                    public void writeToParcel(Parcel parcel, int i) {
                        Intrinsics.checkParameterIsNotNull(parcel, "parcel");
                        parcel.writeString(this.data);
                    }

                    public final String getData() {
                        return this.data;
                    }

                    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                    public Alipay(String str) {
                        super((DefaultConstructorMarker) null);
                        Intrinsics.checkParameterIsNotNull(str, "data");
                        this.data = str;
                    }
                }
            }
        }

        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0002\u0003\u0004B\u0007\b\u0002¢\u0006\u0002\u0010\u0002\u0001\u0002\u0005\u0006¨\u0006\u0007"}, d2 = {"Lcom/stripe/android/model/StripeIntent$NextActionData$SdkData;", "Lcom/stripe/android/model/StripeIntent$NextActionData;", "()V", "Use3DS1", "Use3DS2", "Lcom/stripe/android/model/StripeIntent$NextActionData$SdkData$Use3DS1;", "Lcom/stripe/android/model/StripeIntent$NextActionData$SdkData$Use3DS2;", "stripe_release"}, k = 1, mv = {1, 1, 16})
        /* compiled from: StripeIntent.kt */
        public static abstract class SdkData extends NextActionData {
            private SdkData() {
                super((DefaultConstructorMarker) null);
            }

            public /* synthetic */ SdkData(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }

            @Metadata(bv = {1, 0, 3}, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0006\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\b\b\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\t\u0010\u0007\u001a\u00020\u0003HÆ\u0003J\u0013\u0010\b\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u0003HÆ\u0001J\t\u0010\t\u001a\u00020\nHÖ\u0001J\u0013\u0010\u000b\u001a\u00020\f2\b\u0010\r\u001a\u0004\u0018\u00010\u000eHÖ\u0003J\t\u0010\u000f\u001a\u00020\nHÖ\u0001J\t\u0010\u0010\u001a\u00020\u0003HÖ\u0001J\u0019\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\nHÖ\u0001R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006¨\u0006\u0016"}, d2 = {"Lcom/stripe/android/model/StripeIntent$NextActionData$SdkData$Use3DS1;", "Lcom/stripe/android/model/StripeIntent$NextActionData$SdkData;", "url", "", "(Ljava/lang/String;)V", "getUrl", "()Ljava/lang/String;", "component1", "copy", "describeContents", "", "equals", "", "other", "", "hashCode", "toString", "writeToParcel", "", "parcel", "Landroid/os/Parcel;", "flags", "stripe_release"}, k = 1, mv = {1, 1, 16})
            /* compiled from: StripeIntent.kt */
            public static final class Use3DS1 extends SdkData {
                public static final Parcelable.Creator CREATOR = new Creator();
                private final String url;

                @Metadata(bv = {1, 0, 3}, k = 3, mv = {1, 1, 16})
                public static class Creator implements Parcelable.Creator {
                    public final Object createFromParcel(Parcel parcel) {
                        Intrinsics.checkParameterIsNotNull(parcel, "in");
                        return new Use3DS1(parcel.readString());
                    }

                    public final Object[] newArray(int i) {
                        return new Use3DS1[i];
                    }
                }

                public static /* synthetic */ Use3DS1 copy$default(Use3DS1 use3DS1, String str, int i, Object obj) {
                    if ((i & 1) != 0) {
                        str = use3DS1.url;
                    }
                    return use3DS1.copy(str);
                }

                public final String component1() {
                    return this.url;
                }

                public final Use3DS1 copy(String str) {
                    Intrinsics.checkParameterIsNotNull(str, "url");
                    return new Use3DS1(str);
                }

                public int describeContents() {
                    return 0;
                }

                public boolean equals(Object obj) {
                    if (this != obj) {
                        return (obj instanceof Use3DS1) && Intrinsics.areEqual((Object) this.url, (Object) ((Use3DS1) obj).url);
                    }
                    return true;
                }

                public int hashCode() {
                    String str = this.url;
                    if (str != null) {
                        return str.hashCode();
                    }
                    return 0;
                }

                public String toString() {
                    return "Use3DS1(url=" + this.url + ")";
                }

                public void writeToParcel(Parcel parcel, int i) {
                    Intrinsics.checkParameterIsNotNull(parcel, "parcel");
                    parcel.writeString(this.url);
                }

                public final String getUrl() {
                    return this.url;
                }

                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public Use3DS1(String str) {
                    super((DefaultConstructorMarker) null);
                    Intrinsics.checkParameterIsNotNull(str, "url");
                    this.url = str;
                }
            }

            @Metadata(bv = {1, 0, 3}, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\r\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\b\b\u0018\u00002\u00020\u0001:\u0001!B%\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0003\u0012\u0006\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ\t\u0010\u000f\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0010\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0011\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0012\u001a\u00020\u0007HÆ\u0003J1\u0010\u0013\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00032\b\b\u0002\u0010\u0005\u001a\u00020\u00032\b\b\u0002\u0010\u0006\u001a\u00020\u0007HÆ\u0001J\t\u0010\u0014\u001a\u00020\u0015HÖ\u0001J\u0013\u0010\u0016\u001a\u00020\u00172\b\u0010\u0018\u001a\u0004\u0018\u00010\u0019HÖ\u0003J\t\u0010\u001a\u001a\u00020\u0015HÖ\u0001J\t\u0010\u001b\u001a\u00020\u0003HÖ\u0001J\u0019\u0010\u001c\u001a\u00020\u001d2\u0006\u0010\u001e\u001a\u00020\u001f2\u0006\u0010 \u001a\u00020\u0015HÖ\u0001R\u0011\u0010\u0006\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0011\u0010\u0004\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\fR\u0011\u0010\u0005\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\f¨\u0006\""}, d2 = {"Lcom/stripe/android/model/StripeIntent$NextActionData$SdkData$Use3DS2;", "Lcom/stripe/android/model/StripeIntent$NextActionData$SdkData;", "source", "", "serverName", "transactionId", "serverEncryption", "Lcom/stripe/android/model/StripeIntent$NextActionData$SdkData$Use3DS2$DirectoryServerEncryption;", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/stripe/android/model/StripeIntent$NextActionData$SdkData$Use3DS2$DirectoryServerEncryption;)V", "getServerEncryption", "()Lcom/stripe/android/model/StripeIntent$NextActionData$SdkData$Use3DS2$DirectoryServerEncryption;", "getServerName", "()Ljava/lang/String;", "getSource", "getTransactionId", "component1", "component2", "component3", "component4", "copy", "describeContents", "", "equals", "", "other", "", "hashCode", "toString", "writeToParcel", "", "parcel", "Landroid/os/Parcel;", "flags", "DirectoryServerEncryption", "stripe_release"}, k = 1, mv = {1, 1, 16})
            /* compiled from: StripeIntent.kt */
            public static final class Use3DS2 extends SdkData {
                public static final Parcelable.Creator CREATOR = new Creator();
                private final DirectoryServerEncryption serverEncryption;
                private final String serverName;
                private final String source;
                private final String transactionId;

                @Metadata(bv = {1, 0, 3}, k = 3, mv = {1, 1, 16})
                public static class Creator implements Parcelable.Creator {
                    public final Object createFromParcel(Parcel parcel) {
                        Intrinsics.checkParameterIsNotNull(parcel, "in");
                        return new Use3DS2(parcel.readString(), parcel.readString(), parcel.readString(), (DirectoryServerEncryption) DirectoryServerEncryption.CREATOR.createFromParcel(parcel));
                    }

                    public final Object[] newArray(int i) {
                        return new Use3DS2[i];
                    }
                }

                public static /* synthetic */ Use3DS2 copy$default(Use3DS2 use3DS2, String str, String str2, String str3, DirectoryServerEncryption directoryServerEncryption, int i, Object obj) {
                    if ((i & 1) != 0) {
                        str = use3DS2.source;
                    }
                    if ((i & 2) != 0) {
                        str2 = use3DS2.serverName;
                    }
                    if ((i & 4) != 0) {
                        str3 = use3DS2.transactionId;
                    }
                    if ((i & 8) != 0) {
                        directoryServerEncryption = use3DS2.serverEncryption;
                    }
                    return use3DS2.copy(str, str2, str3, directoryServerEncryption);
                }

                public final String component1() {
                    return this.source;
                }

                public final String component2() {
                    return this.serverName;
                }

                public final String component3() {
                    return this.transactionId;
                }

                public final DirectoryServerEncryption component4() {
                    return this.serverEncryption;
                }

                public final Use3DS2 copy(String str, String str2, String str3, DirectoryServerEncryption directoryServerEncryption) {
                    Intrinsics.checkParameterIsNotNull(str, "source");
                    Intrinsics.checkParameterIsNotNull(str2, "serverName");
                    Intrinsics.checkParameterIsNotNull(str3, "transactionId");
                    Intrinsics.checkParameterIsNotNull(directoryServerEncryption, "serverEncryption");
                    return new Use3DS2(str, str2, str3, directoryServerEncryption);
                }

                public int describeContents() {
                    return 0;
                }

                public boolean equals(Object obj) {
                    if (this == obj) {
                        return true;
                    }
                    if (!(obj instanceof Use3DS2)) {
                        return false;
                    }
                    Use3DS2 use3DS2 = (Use3DS2) obj;
                    return Intrinsics.areEqual((Object) this.source, (Object) use3DS2.source) && Intrinsics.areEqual((Object) this.serverName, (Object) use3DS2.serverName) && Intrinsics.areEqual((Object) this.transactionId, (Object) use3DS2.transactionId) && Intrinsics.areEqual((Object) this.serverEncryption, (Object) use3DS2.serverEncryption);
                }

                public int hashCode() {
                    String str = this.source;
                    int i = 0;
                    int hashCode = (str != null ? str.hashCode() : 0) * 31;
                    String str2 = this.serverName;
                    int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
                    String str3 = this.transactionId;
                    int hashCode3 = (hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31;
                    DirectoryServerEncryption directoryServerEncryption = this.serverEncryption;
                    if (directoryServerEncryption != null) {
                        i = directoryServerEncryption.hashCode();
                    }
                    return hashCode3 + i;
                }

                public String toString() {
                    return "Use3DS2(source=" + this.source + ", serverName=" + this.serverName + ", transactionId=" + this.transactionId + ", serverEncryption=" + this.serverEncryption + ")";
                }

                public void writeToParcel(Parcel parcel, int i) {
                    Intrinsics.checkParameterIsNotNull(parcel, "parcel");
                    parcel.writeString(this.source);
                    parcel.writeString(this.serverName);
                    parcel.writeString(this.transactionId);
                    this.serverEncryption.writeToParcel(parcel, 0);
                }

                public final String getSource() {
                    return this.source;
                }

                public final String getServerName() {
                    return this.serverName;
                }

                public final String getTransactionId() {
                    return this.transactionId;
                }

                public final DirectoryServerEncryption getServerEncryption() {
                    return this.serverEncryption;
                }

                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public Use3DS2(String str, String str2, String str3, DirectoryServerEncryption directoryServerEncryption) {
                    super((DefaultConstructorMarker) null);
                    Intrinsics.checkParameterIsNotNull(str, "source");
                    Intrinsics.checkParameterIsNotNull(str2, "serverName");
                    Intrinsics.checkParameterIsNotNull(str3, "transactionId");
                    Intrinsics.checkParameterIsNotNull(directoryServerEncryption, "serverEncryption");
                    this.source = str;
                    this.serverName = str2;
                    this.transactionId = str3;
                    this.serverEncryption = directoryServerEncryption;
                }

                @Metadata(bv = {1, 0, 3}, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\b\u000e\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\b\b\u0018\u00002\u00020\u0001B-\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\f\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00030\u0006\u0012\b\u0010\u0007\u001a\u0004\u0018\u00010\u0003¢\u0006\u0002\u0010\bJ\t\u0010\u000f\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0010\u001a\u00020\u0003HÆ\u0003J\u000f\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u00030\u0006HÆ\u0003J\u000b\u0010\u0012\u001a\u0004\u0018\u00010\u0003HÆ\u0003J9\u0010\u0013\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00032\u000e\b\u0002\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00030\u00062\n\b\u0002\u0010\u0007\u001a\u0004\u0018\u00010\u0003HÆ\u0001J\t\u0010\u0014\u001a\u00020\u0015HÖ\u0001J\u0013\u0010\u0016\u001a\u00020\u00172\b\u0010\u0018\u001a\u0004\u0018\u00010\u0019HÖ\u0003J\t\u0010\u001a\u001a\u00020\u0015HÖ\u0001J\t\u0010\u001b\u001a\u00020\u0003HÖ\u0001J\u0019\u0010\u001c\u001a\u00020\u001d2\u0006\u0010\u001e\u001a\u00020\u001f2\u0006\u0010 \u001a\u00020\u0015HÖ\u0001R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0011\u0010\u0004\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\nR\u0013\u0010\u0007\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\nR\u0017\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00030\u0006¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000e¨\u0006!"}, d2 = {"Lcom/stripe/android/model/StripeIntent$NextActionData$SdkData$Use3DS2$DirectoryServerEncryption;", "Landroid/os/Parcelable;", "directoryServerId", "", "dsCertificateData", "rootCertsData", "", "keyId", "(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)V", "getDirectoryServerId", "()Ljava/lang/String;", "getDsCertificateData", "getKeyId", "getRootCertsData", "()Ljava/util/List;", "component1", "component2", "component3", "component4", "copy", "describeContents", "", "equals", "", "other", "", "hashCode", "toString", "writeToParcel", "", "parcel", "Landroid/os/Parcel;", "flags", "stripe_release"}, k = 1, mv = {1, 1, 16})
                /* compiled from: StripeIntent.kt */
                public static final class DirectoryServerEncryption implements Parcelable {
                    public static final Parcelable.Creator CREATOR = new Creator();
                    private final String directoryServerId;
                    private final String dsCertificateData;
                    private final String keyId;
                    private final List<String> rootCertsData;

                    @Metadata(bv = {1, 0, 3}, k = 3, mv = {1, 1, 16})
                    public static class Creator implements Parcelable.Creator {
                        public final Object createFromParcel(Parcel parcel) {
                            Intrinsics.checkParameterIsNotNull(parcel, "in");
                            return new DirectoryServerEncryption(parcel.readString(), parcel.readString(), parcel.createStringArrayList(), parcel.readString());
                        }

                        public final Object[] newArray(int i) {
                            return new DirectoryServerEncryption[i];
                        }
                    }

                    public static /* synthetic */ DirectoryServerEncryption copy$default(DirectoryServerEncryption directoryServerEncryption, String str, String str2, List<String> list, String str3, int i, Object obj) {
                        if ((i & 1) != 0) {
                            str = directoryServerEncryption.directoryServerId;
                        }
                        if ((i & 2) != 0) {
                            str2 = directoryServerEncryption.dsCertificateData;
                        }
                        if ((i & 4) != 0) {
                            list = directoryServerEncryption.rootCertsData;
                        }
                        if ((i & 8) != 0) {
                            str3 = directoryServerEncryption.keyId;
                        }
                        return directoryServerEncryption.copy(str, str2, list, str3);
                    }

                    public final String component1() {
                        return this.directoryServerId;
                    }

                    public final String component2() {
                        return this.dsCertificateData;
                    }

                    public final List<String> component3() {
                        return this.rootCertsData;
                    }

                    public final String component4() {
                        return this.keyId;
                    }

                    public final DirectoryServerEncryption copy(String str, String str2, List<String> list, String str3) {
                        Intrinsics.checkParameterIsNotNull(str, "directoryServerId");
                        Intrinsics.checkParameterIsNotNull(str2, "dsCertificateData");
                        Intrinsics.checkParameterIsNotNull(list, "rootCertsData");
                        return new DirectoryServerEncryption(str, str2, list, str3);
                    }

                    public int describeContents() {
                        return 0;
                    }

                    public boolean equals(Object obj) {
                        if (this == obj) {
                            return true;
                        }
                        if (!(obj instanceof DirectoryServerEncryption)) {
                            return false;
                        }
                        DirectoryServerEncryption directoryServerEncryption = (DirectoryServerEncryption) obj;
                        return Intrinsics.areEqual((Object) this.directoryServerId, (Object) directoryServerEncryption.directoryServerId) && Intrinsics.areEqual((Object) this.dsCertificateData, (Object) directoryServerEncryption.dsCertificateData) && Intrinsics.areEqual((Object) this.rootCertsData, (Object) directoryServerEncryption.rootCertsData) && Intrinsics.areEqual((Object) this.keyId, (Object) directoryServerEncryption.keyId);
                    }

                    public int hashCode() {
                        String str = this.directoryServerId;
                        int i = 0;
                        int hashCode = (str != null ? str.hashCode() : 0) * 31;
                        String str2 = this.dsCertificateData;
                        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
                        List<String> list = this.rootCertsData;
                        int hashCode3 = (hashCode2 + (list != null ? list.hashCode() : 0)) * 31;
                        String str3 = this.keyId;
                        if (str3 != null) {
                            i = str3.hashCode();
                        }
                        return hashCode3 + i;
                    }

                    public String toString() {
                        return "DirectoryServerEncryption(directoryServerId=" + this.directoryServerId + ", dsCertificateData=" + this.dsCertificateData + ", rootCertsData=" + this.rootCertsData + ", keyId=" + this.keyId + ")";
                    }

                    public void writeToParcel(Parcel parcel, int i) {
                        Intrinsics.checkParameterIsNotNull(parcel, "parcel");
                        parcel.writeString(this.directoryServerId);
                        parcel.writeString(this.dsCertificateData);
                        parcel.writeStringList(this.rootCertsData);
                        parcel.writeString(this.keyId);
                    }

                    public DirectoryServerEncryption(String str, String str2, List<String> list, String str3) {
                        Intrinsics.checkParameterIsNotNull(str, "directoryServerId");
                        Intrinsics.checkParameterIsNotNull(str2, "dsCertificateData");
                        Intrinsics.checkParameterIsNotNull(list, "rootCertsData");
                        this.directoryServerId = str;
                        this.dsCertificateData = str2;
                        this.rootCertsData = list;
                        this.keyId = str3;
                    }

                    public final String getDirectoryServerId() {
                        return this.directoryServerId;
                    }

                    public final String getDsCertificateData() {
                        return this.dsCertificateData;
                    }

                    public final List<String> getRootCertsData() {
                        return this.rootCertsData;
                    }

                    public final String getKeyId() {
                        return this.keyId;
                    }
                }
            }
        }
    }
}
