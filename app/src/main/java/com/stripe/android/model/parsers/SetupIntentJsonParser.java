package com.stripe.android.model.parsers;

import com.stripe.android.model.SetupIntent;
import com.stripe.android.model.StripeJsonUtils;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.json.JSONObject;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u0000\u0018\u0000 \u00072\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0002\u0007\bB\u0005¢\u0006\u0002\u0010\u0003J\u0012\u0010\u0004\u001a\u0004\u0018\u00010\u00022\u0006\u0010\u0005\u001a\u00020\u0006H\u0016¨\u0006\t"}, d2 = {"Lcom/stripe/android/model/parsers/SetupIntentJsonParser;", "Lcom/stripe/android/model/parsers/ModelJsonParser;", "Lcom/stripe/android/model/SetupIntent;", "()V", "parse", "json", "Lorg/json/JSONObject;", "Companion", "ErrorJsonParser", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: SetupIntentJsonParser.kt */
public final class SetupIntentJsonParser implements ModelJsonParser<SetupIntent> {
    @Deprecated
    public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
    private static final String FIELD_CANCELLATION_REASON = "cancellation_reason";
    private static final String FIELD_CLIENT_SECRET = "client_secret";
    private static final String FIELD_CREATED = "created";
    private static final String FIELD_DESCRIPTION = "description";
    private static final String FIELD_ID = "id";
    private static final String FIELD_LAST_SETUP_ERROR = "last_setup_error";
    private static final String FIELD_LIVEMODE = "livemode";
    private static final String FIELD_NEXT_ACTION = "next_action";
    private static final String FIELD_NEXT_ACTION_TYPE = "type";
    private static final String FIELD_OBJECT = "object";
    private static final String FIELD_PAYMENT_METHOD = "payment_method";
    private static final String FIELD_PAYMENT_METHOD_TYPES = "payment_method_types";
    private static final String FIELD_STATUS = "status";
    private static final String FIELD_USAGE = "usage";
    private static final String VALUE_SETUP_INTENT = "setup_intent";

    /* JADX WARNING: Removed duplicated region for block: B:19:0x0050  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x005c  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x00b5  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.stripe.android.model.SetupIntent parse(org.json.JSONObject r22) {
        /*
            r21 = this;
            r0 = r22
            java.lang.String r1 = "json"
            kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(r0, r1)
            java.lang.String r1 = "object"
            java.lang.String r1 = com.stripe.android.model.StripeJsonUtils.optString(r0, r1)
            java.lang.String r2 = "setup_intent"
            boolean r1 = kotlin.jvm.internal.Intrinsics.areEqual((java.lang.Object) r2, (java.lang.Object) r1)
            r2 = 1
            r1 = r1 ^ r2
            r3 = 0
            if (r1 == 0) goto L_0x0019
            return r3
        L_0x0019:
            com.stripe.android.model.StripeJsonUtils r1 = com.stripe.android.model.StripeJsonUtils.INSTANCE
            java.lang.String r4 = "next_action"
            r1.optMap$stripe_release(r0, r4)
            java.lang.String r1 = "payment_method"
            org.json.JSONObject r5 = r0.optJSONObject(r1)
            if (r5 == 0) goto L_0x0033
            com.stripe.android.model.parsers.PaymentMethodJsonParser r6 = new com.stripe.android.model.parsers.PaymentMethodJsonParser
            r6.<init>()
            com.stripe.android.model.PaymentMethod r5 = r6.parse((org.json.JSONObject) r5)
            r14 = r5
            goto L_0x0034
        L_0x0033:
            r14 = r3
        L_0x0034:
            java.lang.String r1 = com.stripe.android.model.StripeJsonUtils.optString(r0, r1)
            if (r14 != 0) goto L_0x003b
            goto L_0x003c
        L_0x003b:
            r2 = 0
        L_0x003c:
            if (r2 == 0) goto L_0x003f
            goto L_0x0040
        L_0x003f:
            r1 = r3
        L_0x0040:
            if (r1 == 0) goto L_0x0044
        L_0x0042:
            r15 = r1
            goto L_0x004a
        L_0x0044:
            if (r14 == 0) goto L_0x0049
            java.lang.String r1 = r14.id
            goto L_0x0042
        L_0x0049:
            r15 = r3
        L_0x004a:
            org.json.JSONObject r1 = r0.optJSONObject(r4)
            if (r1 == 0) goto L_0x005c
            com.stripe.android.model.parsers.NextActionDataParser r2 = new com.stripe.android.model.parsers.NextActionDataParser
            r2.<init>()
            com.stripe.android.model.StripeIntent$NextActionData r1 = r2.parse((org.json.JSONObject) r1)
            r20 = r1
            goto L_0x005e
        L_0x005c:
            r20 = r3
        L_0x005e:
            java.lang.String r1 = "id"
            java.lang.String r7 = com.stripe.android.model.StripeJsonUtils.optString(r0, r1)
            java.lang.String r1 = "created"
            long r9 = r0.optLong(r1)
            java.lang.String r1 = "client_secret"
            java.lang.String r11 = com.stripe.android.model.StripeJsonUtils.optString(r0, r1)
            com.stripe.android.model.SetupIntent$CancellationReason$Companion r1 = com.stripe.android.model.SetupIntent.CancellationReason.Companion
            java.lang.String r2 = "cancellation_reason"
            java.lang.String r2 = com.stripe.android.model.StripeJsonUtils.optString(r0, r2)
            com.stripe.android.model.SetupIntent$CancellationReason r8 = r1.fromCode$stripe_release(r2)
            java.lang.String r1 = "description"
            java.lang.String r12 = com.stripe.android.model.StripeJsonUtils.optString(r0, r1)
            java.lang.String r1 = "livemode"
            boolean r13 = r0.optBoolean(r1)
            com.stripe.android.model.parsers.ModelJsonParser$Companion r1 = com.stripe.android.model.parsers.ModelJsonParser.Companion
            java.lang.String r2 = "payment_method_types"
            org.json.JSONArray r2 = r0.optJSONArray(r2)
            java.util.List r16 = r1.jsonArrayToList$stripe_release(r2)
            com.stripe.android.model.StripeIntent$Status$Companion r1 = com.stripe.android.model.StripeIntent.Status.Companion
            java.lang.String r2 = "status"
            java.lang.String r2 = com.stripe.android.model.StripeJsonUtils.optString(r0, r2)
            com.stripe.android.model.StripeIntent$Status r17 = r1.fromCode$stripe_release(r2)
            com.stripe.android.model.StripeIntent$Usage$Companion r1 = com.stripe.android.model.StripeIntent.Usage.Companion
            java.lang.String r2 = "usage"
            java.lang.String r2 = com.stripe.android.model.StripeJsonUtils.optString(r0, r2)
            com.stripe.android.model.StripeIntent$Usage r18 = r1.fromCode$stripe_release(r2)
            java.lang.String r1 = "last_setup_error"
            org.json.JSONObject r0 = r0.optJSONObject(r1)
            if (r0 == 0) goto L_0x00be
            com.stripe.android.model.parsers.SetupIntentJsonParser$ErrorJsonParser r1 = new com.stripe.android.model.parsers.SetupIntentJsonParser$ErrorJsonParser
            r1.<init>()
            com.stripe.android.model.SetupIntent$Error r3 = r1.parse((org.json.JSONObject) r0)
        L_0x00be:
            r19 = r3
            com.stripe.android.model.SetupIntent r0 = new com.stripe.android.model.SetupIntent
            r6 = r0
            r6.<init>(r7, r8, r9, r11, r12, r13, r14, r15, r16, r17, r18, r19, r20)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.stripe.android.model.parsers.SetupIntentJsonParser.parse(org.json.JSONObject):com.stripe.android.model.SetupIntent");
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0000\u0018\u0000 \u00072\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0007B\u0005¢\u0006\u0002\u0010\u0003J\u0010\u0010\u0004\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0006H\u0016¨\u0006\b"}, d2 = {"Lcom/stripe/android/model/parsers/SetupIntentJsonParser$ErrorJsonParser;", "Lcom/stripe/android/model/parsers/ModelJsonParser;", "Lcom/stripe/android/model/SetupIntent$Error;", "()V", "parse", "json", "Lorg/json/JSONObject;", "Companion", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: SetupIntentJsonParser.kt */
    public static final class ErrorJsonParser implements ModelJsonParser<SetupIntent.Error> {
        @Deprecated
        public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
        private static final String FIELD_CODE = "code";
        private static final String FIELD_DECLINE_CODE = "decline_code";
        private static final String FIELD_DOC_URL = "doc_url";
        private static final String FIELD_MESSAGE = "message";
        private static final String FIELD_PARAM = "param";
        private static final String FIELD_PAYMENT_METHOD = "payment_method";
        private static final String FIELD_TYPE = "type";

        public SetupIntent.Error parse(JSONObject jSONObject) {
            Intrinsics.checkParameterIsNotNull(jSONObject, "json");
            String optString = StripeJsonUtils.optString(jSONObject, FIELD_CODE);
            String optString2 = StripeJsonUtils.optString(jSONObject, FIELD_DECLINE_CODE);
            String optString3 = StripeJsonUtils.optString(jSONObject, FIELD_DOC_URL);
            String optString4 = StripeJsonUtils.optString(jSONObject, "message");
            String optString5 = StripeJsonUtils.optString(jSONObject, FIELD_PARAM);
            JSONObject optJSONObject = jSONObject.optJSONObject("payment_method");
            return new SetupIntent.Error(optString, optString2, optString3, optString4, optString5, optJSONObject != null ? new PaymentMethodJsonParser().parse(optJSONObject) : null, SetupIntent.Error.Type.Companion.fromCode$stripe_release(StripeJsonUtils.optString(jSONObject, "type")));
        }

        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0007\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\u000b"}, d2 = {"Lcom/stripe/android/model/parsers/SetupIntentJsonParser$ErrorJsonParser$Companion;", "", "()V", "FIELD_CODE", "", "FIELD_DECLINE_CODE", "FIELD_DOC_URL", "FIELD_MESSAGE", "FIELD_PARAM", "FIELD_PAYMENT_METHOD", "FIELD_TYPE", "stripe_release"}, k = 1, mv = {1, 1, 16})
        /* compiled from: SetupIntentJsonParser.kt */
        private static final class Companion {
            private Companion() {
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u000f\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\u0013"}, d2 = {"Lcom/stripe/android/model/parsers/SetupIntentJsonParser$Companion;", "", "()V", "FIELD_CANCELLATION_REASON", "", "FIELD_CLIENT_SECRET", "FIELD_CREATED", "FIELD_DESCRIPTION", "FIELD_ID", "FIELD_LAST_SETUP_ERROR", "FIELD_LIVEMODE", "FIELD_NEXT_ACTION", "FIELD_NEXT_ACTION_TYPE", "FIELD_OBJECT", "FIELD_PAYMENT_METHOD", "FIELD_PAYMENT_METHOD_TYPES", "FIELD_STATUS", "FIELD_USAGE", "VALUE_SETUP_INTENT", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: SetupIntentJsonParser.kt */
    private static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }
}
