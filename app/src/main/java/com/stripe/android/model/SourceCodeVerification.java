package com.stripe.android.model;

import android.os.Parcel;
import android.os.Parcelable;
import java.lang.annotation.RetentionPolicy;
import kotlin.Metadata;
import kotlin.annotation.AnnotationRetention;
import kotlin.annotation.Retention;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0002\b\n\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\b\b\u0018\u00002\u00020\u0001:\u0001\u001aB\u0019\b\u0000\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\b\u0010\u0004\u001a\u0004\u0018\u00010\u0005¢\u0006\u0002\u0010\u0006J\t\u0010\u000b\u001a\u00020\u0003HÆ\u0003J\u000b\u0010\f\u001a\u0004\u0018\u00010\u0005HÆ\u0003J\u001f\u0010\r\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005HÆ\u0001J\t\u0010\u000e\u001a\u00020\u0003HÖ\u0001J\u0013\u0010\u000f\u001a\u00020\u00102\b\u0010\u0011\u001a\u0004\u0018\u00010\u0012HÖ\u0003J\t\u0010\u0013\u001a\u00020\u0003HÖ\u0001J\t\u0010\u0014\u001a\u00020\u0005HÖ\u0001J\u0019\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00020\u0003HÖ\u0001R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0013\u0010\u0004\u001a\u0004\u0018\u00010\u0005¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n¨\u0006\u001b"}, d2 = {"Lcom/stripe/android/model/SourceCodeVerification;", "Lcom/stripe/android/model/StripeModel;", "attemptsRemaining", "", "status", "", "(ILjava/lang/String;)V", "getAttemptsRemaining", "()I", "getStatus", "()Ljava/lang/String;", "component1", "component2", "copy", "describeContents", "equals", "", "other", "", "hashCode", "toString", "writeToParcel", "", "parcel", "Landroid/os/Parcel;", "flags", "Status", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: SourceCodeVerification.kt */
public final class SourceCodeVerification implements StripeModel {
    public static final Parcelable.Creator CREATOR = new Creator();
    private final int attemptsRemaining;
    private final String status;

    @Metadata(bv = {1, 0, 3}, k = 3, mv = {1, 1, 16})
    public static class Creator implements Parcelable.Creator {
        public final Object createFromParcel(Parcel parcel) {
            Intrinsics.checkParameterIsNotNull(parcel, "in");
            return new SourceCodeVerification(parcel.readInt(), parcel.readString());
        }

        public final Object[] newArray(int i) {
            return new SourceCodeVerification[i];
        }
    }

    public static /* synthetic */ SourceCodeVerification copy$default(SourceCodeVerification sourceCodeVerification, int i, String str, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            i = sourceCodeVerification.attemptsRemaining;
        }
        if ((i2 & 2) != 0) {
            str = sourceCodeVerification.status;
        }
        return sourceCodeVerification.copy(i, str);
    }

    public final int component1() {
        return this.attemptsRemaining;
    }

    public final String component2() {
        return this.status;
    }

    public final SourceCodeVerification copy(int i, String str) {
        return new SourceCodeVerification(i, str);
    }

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof SourceCodeVerification)) {
            return false;
        }
        SourceCodeVerification sourceCodeVerification = (SourceCodeVerification) obj;
        return this.attemptsRemaining == sourceCodeVerification.attemptsRemaining && Intrinsics.areEqual((Object) this.status, (Object) sourceCodeVerification.status);
    }

    public int hashCode() {
        int i = this.attemptsRemaining * 31;
        String str = this.status;
        return i + (str != null ? str.hashCode() : 0);
    }

    public String toString() {
        return "SourceCodeVerification(attemptsRemaining=" + this.attemptsRemaining + ", status=" + this.status + ")";
    }

    public void writeToParcel(Parcel parcel, int i) {
        Intrinsics.checkParameterIsNotNull(parcel, "parcel");
        parcel.writeInt(this.attemptsRemaining);
        parcel.writeString(this.status);
    }

    public SourceCodeVerification(int i, String str) {
        this.attemptsRemaining = i;
        this.status = str;
    }

    public final int getAttemptsRemaining() {
        return this.attemptsRemaining;
    }

    public final String getStatus() {
        return this.status;
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u001b\n\u0002\b\u0002\b\u0002\u0018\u0000 \u00022\u00020\u0001:\u0001\u0002B\u0000¨\u0006\u0003"}, d2 = {"Lcom/stripe/android/model/SourceCodeVerification$Status;", "", "Companion", "stripe_release"}, k = 1, mv = {1, 1, 16})
    @Retention(AnnotationRetention.SOURCE)
    @java.lang.annotation.Retention(RetentionPolicy.SOURCE)
    /* compiled from: SourceCodeVerification.kt */
    public @interface Status {
        public static final Companion Companion = Companion.$$INSTANCE;
        public static final String FAILED = "failed";
        public static final String PENDING = "pending";
        public static final String SUCCEEDED = "succeeded";

        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\u0007"}, d2 = {"Lcom/stripe/android/model/SourceCodeVerification$Status$Companion;", "", "()V", "FAILED", "", "PENDING", "SUCCEEDED", "stripe_release"}, k = 1, mv = {1, 1, 16})
        /* compiled from: SourceCodeVerification.kt */
        public static final class Companion {
            static final /* synthetic */ Companion $$INSTANCE = new Companion();
            public static final String FAILED = "failed";
            public static final String PENDING = "pending";
            public static final String SUCCEEDED = "succeeded";

            private Companion() {
            }
        }
    }
}
