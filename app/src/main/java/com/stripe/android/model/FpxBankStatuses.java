package com.stripe.android.model;

import android.os.Parcel;
import android.os.Parcelable;
import com.stripe.android.view.FpxBank;
import java.util.LinkedHashMap;
import java.util.Map;
import kotlin.Metadata;
import kotlin.collections.MapsKt;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\u0010\u000e\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\b\b\u0018\u00002\u00020\u0001B\u001d\b\u0000\u0012\u0014\b\u0002\u0010\u0002\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u0003¢\u0006\u0002\u0010\u0006J\u0015\u0010\u0007\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u0003HÂ\u0003J\u001f\u0010\b\u001a\u00020\u00002\u0014\b\u0002\u0010\u0002\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u0003HÆ\u0001J\t\u0010\t\u001a\u00020\nHÖ\u0001J\u0013\u0010\u000b\u001a\u00020\u00052\b\u0010\f\u001a\u0004\u0018\u00010\rHÖ\u0003J\t\u0010\u000e\u001a\u00020\nHÖ\u0001J\u0015\u0010\u000f\u001a\u00020\u00052\u0006\u0010\u0010\u001a\u00020\u0011H\u0000¢\u0006\u0002\b\u0012J\t\u0010\u0013\u001a\u00020\u0004HÖ\u0001J\u0019\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\nHÖ\u0001R\u001a\u0010\u0002\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0019"}, d2 = {"Lcom/stripe/android/model/FpxBankStatuses;", "Lcom/stripe/android/model/StripeModel;", "statuses", "", "", "", "(Ljava/util/Map;)V", "component1", "copy", "describeContents", "", "equals", "other", "", "hashCode", "isOnline", "bank", "Lcom/stripe/android/view/FpxBank;", "isOnline$stripe_release", "toString", "writeToParcel", "", "parcel", "Landroid/os/Parcel;", "flags", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: FpxBankStatuses.kt */
public final class FpxBankStatuses implements StripeModel {
    public static final Parcelable.Creator CREATOR = new Creator();
    private final Map<String, Boolean> statuses;

    @Metadata(bv = {1, 0, 3}, k = 3, mv = {1, 1, 16})
    public static class Creator implements Parcelable.Creator {
        public final Object createFromParcel(Parcel parcel) {
            Intrinsics.checkParameterIsNotNull(parcel, "in");
            int readInt = parcel.readInt();
            LinkedHashMap linkedHashMap = new LinkedHashMap(readInt);
            while (readInt != 0) {
                linkedHashMap.put(parcel.readString(), Boolean.valueOf(parcel.readInt() != 0));
                readInt--;
            }
            return new FpxBankStatuses(linkedHashMap);
        }

        public final Object[] newArray(int i) {
            return new FpxBankStatuses[i];
        }
    }

    public FpxBankStatuses() {
        this((Map) null, 1, (DefaultConstructorMarker) null);
    }

    private final Map<String, Boolean> component1() {
        return this.statuses;
    }

    public static /* synthetic */ FpxBankStatuses copy$default(FpxBankStatuses fpxBankStatuses, Map<String, Boolean> map, int i, Object obj) {
        if ((i & 1) != 0) {
            map = fpxBankStatuses.statuses;
        }
        return fpxBankStatuses.copy(map);
    }

    public final FpxBankStatuses copy(Map<String, Boolean> map) {
        Intrinsics.checkParameterIsNotNull(map, "statuses");
        return new FpxBankStatuses(map);
    }

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            return (obj instanceof FpxBankStatuses) && Intrinsics.areEqual((Object) this.statuses, (Object) ((FpxBankStatuses) obj).statuses);
        }
        return true;
    }

    public int hashCode() {
        Map<String, Boolean> map = this.statuses;
        if (map != null) {
            return map.hashCode();
        }
        return 0;
    }

    public String toString() {
        return "FpxBankStatuses(statuses=" + this.statuses + ")";
    }

    public void writeToParcel(Parcel parcel, int i) {
        Intrinsics.checkParameterIsNotNull(parcel, "parcel");
        Map<String, Boolean> map = this.statuses;
        parcel.writeInt(map.size());
        for (Map.Entry<String, Boolean> next : map.entrySet()) {
            parcel.writeString(next.getKey());
            parcel.writeInt(next.getValue().booleanValue() ? 1 : 0);
        }
    }

    public FpxBankStatuses(Map<String, Boolean> map) {
        Intrinsics.checkParameterIsNotNull(map, "statuses");
        this.statuses = map;
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ FpxBankStatuses(Map map, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this((i & 1) != 0 ? MapsKt.emptyMap() : map);
    }

    public final /* synthetic */ boolean isOnline$stripe_release(FpxBank fpxBank) {
        Intrinsics.checkParameterIsNotNull(fpxBank, "bank");
        Boolean bool = this.statuses.get(fpxBank.getId());
        if (bool != null) {
            return bool.booleanValue();
        }
        return true;
    }
}
