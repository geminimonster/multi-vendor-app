package com.stripe.android.model.parsers;

import com.stripe.android.model.Stripe3ds2AuthResult;
import com.stripe.android.model.StripeJsonUtils;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
import kotlin.TuplesKt;
import kotlin.collections.CollectionsKt;
import kotlin.collections.IntIterator;
import kotlin.collections.MapsKt;
import kotlin.jvm.internal.Intrinsics;
import kotlin.ranges.RangesKt;
import org.json.JSONArray;
import org.json.JSONObject;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0000\u0018\u0000 \b2\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0004\u0007\b\t\nB\u0005¢\u0006\u0002\u0010\u0003J\u0010\u0010\u0004\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0006H\u0016¨\u0006\u000b"}, d2 = {"Lcom/stripe/android/model/parsers/Stripe3ds2AuthResultJsonParser;", "Lcom/stripe/android/model/parsers/ModelJsonParser;", "Lcom/stripe/android/model/Stripe3ds2AuthResult;", "()V", "parse", "json", "Lorg/json/JSONObject;", "AresJsonParser", "Companion", "MessageExtensionJsonParser", "ThreeDS2ErrorJsonParser", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: Stripe3ds2AuthResultJsonParser.kt */
public final class Stripe3ds2AuthResultJsonParser implements ModelJsonParser<Stripe3ds2AuthResult> {
    @Deprecated
    public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
    private static final String FIELD_ARES = "ares";
    private static final String FIELD_CREATED = "created";
    private static final String FIELD_ERROR = "error";
    private static final String FIELD_FALLBACK_REDIRECT_URL = "fallback_redirect_url";
    private static final String FIELD_ID = "id";
    private static final String FIELD_LIVEMODE = "livemode";
    private static final String FIELD_OBJECT = "object";
    private static final String FIELD_SOURCE = "source";
    private static final String FIELD_STATE = "state";

    public Stripe3ds2AuthResult parse(JSONObject jSONObject) {
        Stripe3ds2AuthResult.Ares ares;
        Stripe3ds2AuthResult.ThreeDS2Error threeDS2Error;
        JSONObject optJSONObject;
        JSONObject optJSONObject2;
        Intrinsics.checkParameterIsNotNull(jSONObject, "json");
        String str = null;
        if (!jSONObject.isNull(FIELD_ARES) && (optJSONObject2 = jSONObject.optJSONObject(FIELD_ARES)) != null) {
            ares = new AresJsonParser().parse(optJSONObject2);
        } else {
            ares = null;
        }
        if (!jSONObject.isNull("error") && (optJSONObject = jSONObject.optJSONObject("error")) != null) {
            threeDS2Error = new ThreeDS2ErrorJsonParser().parse(optJSONObject);
        } else {
            threeDS2Error = null;
        }
        if (!jSONObject.isNull(FIELD_FALLBACK_REDIRECT_URL)) {
            str = jSONObject.optString(FIELD_FALLBACK_REDIRECT_URL);
        }
        return new Stripe3ds2AuthResult(jSONObject.getString("id"), jSONObject.getString(FIELD_OBJECT), ares, Long.valueOf(jSONObject.getLong(FIELD_CREATED)), jSONObject.getString("source"), jSONObject.optString("state"), jSONObject.getBoolean(FIELD_LIVEMODE), threeDS2Error, str);
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0000\u0018\u0000 \u00072\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0007B\u0005¢\u0006\u0002\u0010\u0003J\u0010\u0010\u0004\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0006H\u0016¨\u0006\b"}, d2 = {"Lcom/stripe/android/model/parsers/Stripe3ds2AuthResultJsonParser$AresJsonParser;", "Lcom/stripe/android/model/parsers/ModelJsonParser;", "Lcom/stripe/android/model/Stripe3ds2AuthResult$Ares;", "()V", "parse", "json", "Lorg/json/JSONObject;", "Companion", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: Stripe3ds2AuthResultJsonParser.kt */
    public static final class AresJsonParser implements ModelJsonParser<Stripe3ds2AuthResult.Ares> {
        @Deprecated
        public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
        private static final String FIELD_ACS_CHALLENGE_MANDATED = "acsChallengeMandated";
        private static final String FIELD_ACS_SIGNED_CONTENT = "acsSignedContent";
        private static final String FIELD_ACS_TRANS_ID = "acsTransID";
        private static final String FIELD_ACS_URL = "acsURL";
        private static final String FIELD_AUTHENTICATION_TYPE = "authenticationType";
        private static final String FIELD_CARDHOLDER_INFO = "cardholderInfo";
        private static final String FIELD_MESSAGE_EXTENSION = "messageExtension";
        private static final String FIELD_MESSAGE_TYPE = "messageType";
        private static final String FIELD_MESSAGE_VERSION = "messageVersion";
        private static final String FIELD_SDK_TRANS_ID = "sdkTransID";
        private static final String FIELD_THREE_DS_SERVER_TRANS_ID = "threeDSServerTransID";
        private static final String FIELD_TRANS_STATUS = "transStatus";

        public Stripe3ds2AuthResult.Ares parse(JSONObject jSONObject) {
            Intrinsics.checkParameterIsNotNull(jSONObject, "json");
            String string = jSONObject.getString(FIELD_THREE_DS_SERVER_TRANS_ID);
            String optString = StripeJsonUtils.optString(jSONObject, FIELD_ACS_CHALLENGE_MANDATED);
            String optString2 = StripeJsonUtils.optString(jSONObject, FIELD_ACS_SIGNED_CONTENT);
            String string2 = jSONObject.getString(FIELD_ACS_TRANS_ID);
            String optString3 = StripeJsonUtils.optString(jSONObject, FIELD_ACS_URL);
            String optString4 = StripeJsonUtils.optString(jSONObject, FIELD_AUTHENTICATION_TYPE);
            String optString5 = StripeJsonUtils.optString(jSONObject, FIELD_CARDHOLDER_INFO);
            String string3 = jSONObject.getString(FIELD_MESSAGE_TYPE);
            String string4 = jSONObject.getString(FIELD_MESSAGE_VERSION);
            String optString6 = StripeJsonUtils.optString(jSONObject, FIELD_SDK_TRANS_ID);
            String optString7 = StripeJsonUtils.optString(jSONObject, FIELD_TRANS_STATUS);
            JSONArray optJSONArray = jSONObject.optJSONArray(FIELD_MESSAGE_EXTENSION);
            return new Stripe3ds2AuthResult.Ares(string, optString, optString2, string2, optString3, optString4, optString5, optJSONArray != null ? new MessageExtensionJsonParser().parse(optJSONArray) : null, string3, string4, optString6, optString7);
        }

        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\f\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\u0010"}, d2 = {"Lcom/stripe/android/model/parsers/Stripe3ds2AuthResultJsonParser$AresJsonParser$Companion;", "", "()V", "FIELD_ACS_CHALLENGE_MANDATED", "", "FIELD_ACS_SIGNED_CONTENT", "FIELD_ACS_TRANS_ID", "FIELD_ACS_URL", "FIELD_AUTHENTICATION_TYPE", "FIELD_CARDHOLDER_INFO", "FIELD_MESSAGE_EXTENSION", "FIELD_MESSAGE_TYPE", "FIELD_MESSAGE_VERSION", "FIELD_SDK_TRANS_ID", "FIELD_THREE_DS_SERVER_TRANS_ID", "FIELD_TRANS_STATUS", "stripe_release"}, k = 1, mv = {1, 1, 16})
        /* compiled from: Stripe3ds2AuthResultJsonParser.kt */
        private static final class Companion {
            private Companion() {
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0000\u0018\u0000 \n2\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\nB\u0005¢\u0006\u0002\u0010\u0003J\u0014\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0007J\u0010\u0010\u0004\u001a\u00020\u00022\u0006\u0010\b\u001a\u00020\tH\u0016¨\u0006\u000b"}, d2 = {"Lcom/stripe/android/model/parsers/Stripe3ds2AuthResultJsonParser$MessageExtensionJsonParser;", "Lcom/stripe/android/model/parsers/ModelJsonParser;", "Lcom/stripe/android/model/Stripe3ds2AuthResult$MessageExtension;", "()V", "parse", "", "jsonArray", "Lorg/json/JSONArray;", "json", "Lorg/json/JSONObject;", "Companion", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: Stripe3ds2AuthResultJsonParser.kt */
    public static final class MessageExtensionJsonParser implements ModelJsonParser<Stripe3ds2AuthResult.MessageExtension> {
        @Deprecated
        public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
        private static final String FIELD_CRITICALITY_INDICATOR = "criticalityIndicator";
        private static final String FIELD_DATA = "data";
        private static final String FIELD_ID = "id";
        private static final String FIELD_NAME = "name";

        public final List<Stripe3ds2AuthResult.MessageExtension> parse(JSONArray jSONArray) {
            Intrinsics.checkParameterIsNotNull(jSONArray, "jsonArray");
            Collection arrayList = new ArrayList();
            Iterator it = RangesKt.until(0, jSONArray.length()).iterator();
            while (it.hasNext()) {
                JSONObject optJSONObject = jSONArray.optJSONObject(((IntIterator) it).nextInt());
                if (optJSONObject != null) {
                    arrayList.add(optJSONObject);
                }
            }
            Iterable<JSONObject> iterable = (List) arrayList;
            Collection arrayList2 = new ArrayList(CollectionsKt.collectionSizeOrDefault(iterable, 10));
            for (JSONObject parse : iterable) {
                arrayList2.add(parse(parse));
            }
            return (List) arrayList2;
        }

        public Stripe3ds2AuthResult.MessageExtension parse(JSONObject jSONObject) {
            Map map;
            Intrinsics.checkParameterIsNotNull(jSONObject, "json");
            JSONObject optJSONObject = jSONObject.optJSONObject("data");
            if (optJSONObject != null) {
                JSONArray names = optJSONObject.names();
                if (names == null) {
                    names = new JSONArray();
                }
                Iterable until = RangesKt.until(0, names.length());
                Collection arrayList = new ArrayList(CollectionsKt.collectionSizeOrDefault(until, 10));
                Iterator it = until.iterator();
                while (it.hasNext()) {
                    arrayList.add(names.getString(((IntIterator) it).nextInt()));
                }
                Iterable<String> iterable = (List) arrayList;
                Collection arrayList2 = new ArrayList(CollectionsKt.collectionSizeOrDefault(iterable, 10));
                for (String str : iterable) {
                    arrayList2.add(MapsKt.mapOf(TuplesKt.to(str, optJSONObject.getString(str))));
                }
                map = MapsKt.emptyMap();
                for (Map plus : (List) arrayList2) {
                    map = MapsKt.plus(map, plus);
                }
            } else {
                map = MapsKt.emptyMap();
            }
            return new Stripe3ds2AuthResult.MessageExtension(StripeJsonUtils.optString(jSONObject, "name"), jSONObject.optBoolean("criticalityIndicator"), StripeJsonUtils.optString(jSONObject, "id"), MapsKt.toMap(map));
        }

        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0004\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\b"}, d2 = {"Lcom/stripe/android/model/parsers/Stripe3ds2AuthResultJsonParser$MessageExtensionJsonParser$Companion;", "", "()V", "FIELD_CRITICALITY_INDICATOR", "", "FIELD_DATA", "FIELD_ID", "FIELD_NAME", "stripe_release"}, k = 1, mv = {1, 1, 16})
        /* compiled from: Stripe3ds2AuthResultJsonParser.kt */
        private static final class Companion {
            private Companion() {
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0000\u0018\u0000 \u00072\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0007B\u0005¢\u0006\u0002\u0010\u0003J\u0010\u0010\u0004\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0006H\u0016¨\u0006\b"}, d2 = {"Lcom/stripe/android/model/parsers/Stripe3ds2AuthResultJsonParser$ThreeDS2ErrorJsonParser;", "Lcom/stripe/android/model/parsers/ModelJsonParser;", "Lcom/stripe/android/model/Stripe3ds2AuthResult$ThreeDS2Error;", "()V", "parse", "json", "Lorg/json/JSONObject;", "Companion", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: Stripe3ds2AuthResultJsonParser.kt */
    public static final class ThreeDS2ErrorJsonParser implements ModelJsonParser<Stripe3ds2AuthResult.ThreeDS2Error> {
        @Deprecated
        public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
        private static final String FIELD_ACS_TRANS_ID = "acsTransID";
        private static final String FIELD_DS_TRANS_ID = "dsTransID";
        private static final String FIELD_ERROR_CODE = "errorCode";
        private static final String FIELD_ERROR_COMPONENT = "errorComponent";
        private static final String FIELD_ERROR_DESCRIPTION = "errorDescription";
        private static final String FIELD_ERROR_DETAIL = "errorDetail";
        private static final String FIELD_ERROR_MESSAGE_TYPE = "errorMessageType";
        private static final String FIELD_MESSAGE_TYPE = "messageType";
        private static final String FIELD_MESSAGE_VERSION = "messageVersion";
        private static final String FIELD_SDK_TRANS_ID = "sdkTransID";
        private static final String FIELD_THREE_DS_SERVER_TRANS_ID = "threeDSServerTransID";

        public Stripe3ds2AuthResult.ThreeDS2Error parse(JSONObject jSONObject) {
            Intrinsics.checkParameterIsNotNull(jSONObject, "json");
            return new Stripe3ds2AuthResult.ThreeDS2Error(jSONObject.getString(FIELD_THREE_DS_SERVER_TRANS_ID), StripeJsonUtils.optString(jSONObject, FIELD_ACS_TRANS_ID), StripeJsonUtils.optString(jSONObject, FIELD_DS_TRANS_ID), jSONObject.getString(FIELD_ERROR_CODE), jSONObject.getString(FIELD_ERROR_COMPONENT), jSONObject.getString(FIELD_ERROR_DESCRIPTION), jSONObject.getString(FIELD_ERROR_DETAIL), StripeJsonUtils.optString(jSONObject, FIELD_ERROR_MESSAGE_TYPE), jSONObject.getString(FIELD_MESSAGE_TYPE), jSONObject.getString(FIELD_MESSAGE_VERSION), StripeJsonUtils.optString(jSONObject, FIELD_SDK_TRANS_ID));
        }

        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u000b\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\u000f"}, d2 = {"Lcom/stripe/android/model/parsers/Stripe3ds2AuthResultJsonParser$ThreeDS2ErrorJsonParser$Companion;", "", "()V", "FIELD_ACS_TRANS_ID", "", "FIELD_DS_TRANS_ID", "FIELD_ERROR_CODE", "FIELD_ERROR_COMPONENT", "FIELD_ERROR_DESCRIPTION", "FIELD_ERROR_DETAIL", "FIELD_ERROR_MESSAGE_TYPE", "FIELD_MESSAGE_TYPE", "FIELD_MESSAGE_VERSION", "FIELD_SDK_TRANS_ID", "FIELD_THREE_DS_SERVER_TRANS_ID", "stripe_release"}, k = 1, mv = {1, 1, 16})
        /* compiled from: Stripe3ds2AuthResultJsonParser.kt */
        private static final class Companion {
            private Companion() {
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\t\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\r"}, d2 = {"Lcom/stripe/android/model/parsers/Stripe3ds2AuthResultJsonParser$Companion;", "", "()V", "FIELD_ARES", "", "FIELD_CREATED", "FIELD_ERROR", "FIELD_FALLBACK_REDIRECT_URL", "FIELD_ID", "FIELD_LIVEMODE", "FIELD_OBJECT", "FIELD_SOURCE", "FIELD_STATE", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: Stripe3ds2AuthResultJsonParser.kt */
    private static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }
}
