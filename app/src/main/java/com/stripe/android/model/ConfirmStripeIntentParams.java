package com.stripe.android.model;

import kotlin.Metadata;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0003\bf\u0018\u0000 \t2\u00020\u0001:\u0001\tJ\b\u0010\u0006\u001a\u00020\u0007H&J\u0010\u0010\b\u001a\u00020\u00002\u0006\u0010\u0006\u001a\u00020\u0007H&R\u0012\u0010\u0002\u001a\u00020\u0003X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0004\u0010\u0005¨\u0006\n"}, d2 = {"Lcom/stripe/android/model/ConfirmStripeIntentParams;", "Lcom/stripe/android/model/StripeParamsModel;", "clientSecret", "", "getClientSecret", "()Ljava/lang/String;", "shouldUseStripeSdk", "", "withShouldUseStripeSdk", "Companion", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: ConfirmStripeIntentParams.kt */
public interface ConfirmStripeIntentParams extends StripeParamsModel {
    public static final Companion Companion = Companion.$$INSTANCE;
    public static final String PARAM_CLIENT_SECRET = "client_secret";
    public static final String PARAM_MANDATE_DATA = "mandate_data";
    public static final String PARAM_MANDATE_ID = "mandate";
    public static final String PARAM_PAYMENT_METHOD_DATA = "payment_method_data";
    public static final String PARAM_PAYMENT_METHOD_ID = "payment_method";
    public static final String PARAM_RETURN_URL = "return_url";
    public static final String PARAM_USE_STRIPE_SDK = "use_stripe_sdk";

    String getClientSecret();

    boolean shouldUseStripeSdk();

    ConfirmStripeIntentParams withShouldUseStripeSdk(boolean z);

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0007\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\u000b"}, d2 = {"Lcom/stripe/android/model/ConfirmStripeIntentParams$Companion;", "", "()V", "PARAM_CLIENT_SECRET", "", "PARAM_MANDATE_DATA", "PARAM_MANDATE_ID", "PARAM_PAYMENT_METHOD_DATA", "PARAM_PAYMENT_METHOD_ID", "PARAM_RETURN_URL", "PARAM_USE_STRIPE_SDK", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: ConfirmStripeIntentParams.kt */
    public static final class Companion {
        static final /* synthetic */ Companion $$INSTANCE = new Companion();
        public static final String PARAM_CLIENT_SECRET = "client_secret";
        public static final String PARAM_MANDATE_DATA = "mandate_data";
        public static final String PARAM_MANDATE_ID = "mandate";
        public static final String PARAM_PAYMENT_METHOD_DATA = "payment_method_data";
        public static final String PARAM_PAYMENT_METHOD_ID = "payment_method";
        public static final String PARAM_RETURN_URL = "return_url";
        public static final String PARAM_USE_STRIPE_SDK = "use_stripe_sdk";

        private Companion() {
        }
    }
}
