package com.stripe.android.model.parsers;

import com.stripe.android.model.SourceRedirect;
import com.stripe.android.model.StripeJsonUtils;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.json.JSONObject;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0000\u0018\u0000 \u00072\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0007B\u0005¢\u0006\u0002\u0010\u0003J\u0010\u0010\u0004\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0006H\u0016¨\u0006\b"}, d2 = {"Lcom/stripe/android/model/parsers/SourceRedirectJsonParser;", "Lcom/stripe/android/model/parsers/ModelJsonParser;", "Lcom/stripe/android/model/SourceRedirect;", "()V", "parse", "json", "Lorg/json/JSONObject;", "Companion", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: SourceRedirectJsonParser.kt */
public final class SourceRedirectJsonParser implements ModelJsonParser<SourceRedirect> {
    public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
    private static final String FIELD_RETURN_URL = "return_url";
    private static final String FIELD_STATUS = "status";
    private static final String FIELD_URL = "url";

    public SourceRedirect parse(JSONObject jSONObject) {
        Intrinsics.checkParameterIsNotNull(jSONObject, "json");
        return new SourceRedirect(StripeJsonUtils.optString(jSONObject, "return_url"), Companion.asStatus$stripe_release(StripeJsonUtils.optString(jSONObject, "status")), StripeJsonUtils.optString(jSONObject, "url"));
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0006\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0019\u0010\u0007\u001a\u0004\u0018\u00010\u00042\b\u0010\b\u001a\u0004\u0018\u00010\u0004H\u0001¢\u0006\u0002\b\tR\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\n"}, d2 = {"Lcom/stripe/android/model/parsers/SourceRedirectJsonParser$Companion;", "", "()V", "FIELD_RETURN_URL", "", "FIELD_STATUS", "FIELD_URL", "asStatus", "stringStatus", "asStatus$stripe_release", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: SourceRedirectJsonParser.kt */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        public final String asStatus$stripe_release(String str) {
            if (str != null) {
                switch (str.hashCode()) {
                    case -1281977283:
                        if (str.equals("failed")) {
                            return "failed";
                        }
                        break;
                    case -682587753:
                        if (str.equals("pending")) {
                            return "pending";
                        }
                        break;
                    case 945734241:
                        if (str.equals("succeeded")) {
                            return "succeeded";
                        }
                        break;
                    case 2092707723:
                        if (str.equals("not_required")) {
                            return "not_required";
                        }
                        break;
                }
            }
            return null;
        }
    }
}
