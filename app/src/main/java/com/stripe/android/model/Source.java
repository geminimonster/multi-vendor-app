package com.stripe.android.model;

import android.os.Parcel;
import android.os.Parcelable;
import com.stripe.android.model.parsers.SourceJsonParser;
import java.lang.annotation.RetentionPolicy;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import kotlin.Metadata;
import kotlin.annotation.AnnotationRetention;
import kotlin.annotation.Retention;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.internal.Intrinsics;
import org.json.JSONObject;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010$\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b@\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\b\b\b\u0018\u0000 p2\u00020\u00012\u00020\u0002:\u0006pqrstuB¤\u0002\b\u0000\u0012\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004\u0012\n\b\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u0012\n\b\u0002\u0010\u0007\u001a\u0004\u0018\u00010\u0004\u0012\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\t\u0012\n\b\u0002\u0010\n\u001a\u0004\u0018\u00010\u0006\u0012\n\b\u0002\u0010\u000b\u001a\u0004\u0018\u00010\u0004\u0012\n\b\u0002\u0010\f\u001a\u0004\u0018\u00010\u0004\u0012\n\b\u0002\u0010\r\u001a\u0004\u0018\u00010\u000e\u0012\u0016\b\u0002\u0010\u000f\u001a\u0010\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u0010\u0012\n\b\u0002\u0010\u0011\u001a\u0004\u0018\u00010\u0012\u0012\n\b\u0002\u0010\u0013\u001a\u0004\u0018\u00010\u0014\u0012\n\b\u0002\u0010\u0015\u001a\u0004\u0018\u00010\u0016\u0012\n\b\u0002\u0010\u0017\u001a\u0004\u0018\u00010\u0004\u0012\u001d\b\u0002\u0010\u0018\u001a\u0017\u0012\u0004\u0012\u00020\u0004\u0012\u000b\u0012\t\u0018\u00010\u0019¢\u0006\u0002\b\u001a\u0018\u00010\u0010\u0012\n\b\u0002\u0010\u001b\u001a\u0004\u0018\u00010\u001c\u0012\u0006\u0010\u001d\u001a\u00020\u0004\u0012\u0006\u0010\u001e\u001a\u00020\u0004\u0012\n\b\u0002\u0010\u001f\u001a\u0004\u0018\u00010\u0004\u0012\n\b\u0002\u0010 \u001a\u0004\u0018\u00010!\u0012\n\b\u0002\u0010\"\u001a\u0004\u0018\u00010#\u0012\n\b\u0002\u0010$\u001a\u0004\u0018\u00010%\u0012\n\b\u0002\u0010&\u001a\u0004\u0018\u00010\u0004¢\u0006\u0002\u0010'J\u000b\u0010M\u001a\u0004\u0018\u00010\u0004HÆ\u0003J\u000b\u0010N\u001a\u0004\u0018\u00010\u0012HÆ\u0003J\u000b\u0010O\u001a\u0004\u0018\u00010\u0014HÆ\u0003J\u000b\u0010P\u001a\u0004\u0018\u00010\u0016HÆ\u0003J\u000b\u0010Q\u001a\u0004\u0018\u00010\u0004HÆ\u0003J\u001e\u0010R\u001a\u0017\u0012\u0004\u0012\u00020\u0004\u0012\u000b\u0012\t\u0018\u00010\u0019¢\u0006\u0002\b\u001a\u0018\u00010\u0010HÆ\u0003J\u000b\u0010S\u001a\u0004\u0018\u00010\u001cHÆ\u0003J\t\u0010T\u001a\u00020\u0004HÆ\u0003J\t\u0010U\u001a\u00020\u0004HÆ\u0003J\u000b\u0010V\u001a\u0004\u0018\u00010\u0004HÆ\u0003J\u000b\u0010W\u001a\u0004\u0018\u00010!HÂ\u0003J\u0010\u0010X\u001a\u0004\u0018\u00010\u0006HÆ\u0003¢\u0006\u0002\u0010)J\u000b\u0010Y\u001a\u0004\u0018\u00010#HÂ\u0003J\u000b\u0010Z\u001a\u0004\u0018\u00010%HÆ\u0003J\u000b\u0010[\u001a\u0004\u0018\u00010\u0004HÆ\u0003J\u000b\u0010\\\u001a\u0004\u0018\u00010\u0004HÆ\u0003J\u000b\u0010]\u001a\u0004\u0018\u00010\tHÆ\u0003J\u0010\u0010^\u001a\u0004\u0018\u00010\u0006HÆ\u0003¢\u0006\u0002\u0010)J\u000b\u0010_\u001a\u0004\u0018\u00010\u0004HÆ\u0003J\u000b\u0010`\u001a\u0004\u0018\u00010\u0004HÆ\u0003J\u0010\u0010a\u001a\u0004\u0018\u00010\u000eHÆ\u0003¢\u0006\u0002\u00103J\u0017\u0010b\u001a\u0010\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u0010HÆ\u0003J±\u0002\u0010c\u001a\u00020\u00002\n\b\u0002\u0010\u0003\u001a\u0004\u0018\u00010\u00042\n\b\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u00062\n\b\u0002\u0010\u0007\u001a\u0004\u0018\u00010\u00042\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\t2\n\b\u0002\u0010\n\u001a\u0004\u0018\u00010\u00062\n\b\u0002\u0010\u000b\u001a\u0004\u0018\u00010\u00042\n\b\u0002\u0010\f\u001a\u0004\u0018\u00010\u00042\n\b\u0002\u0010\r\u001a\u0004\u0018\u00010\u000e2\u0016\b\u0002\u0010\u000f\u001a\u0010\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u00102\n\b\u0002\u0010\u0011\u001a\u0004\u0018\u00010\u00122\n\b\u0002\u0010\u0013\u001a\u0004\u0018\u00010\u00142\n\b\u0002\u0010\u0015\u001a\u0004\u0018\u00010\u00162\n\b\u0002\u0010\u0017\u001a\u0004\u0018\u00010\u00042\u001d\b\u0002\u0010\u0018\u001a\u0017\u0012\u0004\u0012\u00020\u0004\u0012\u000b\u0012\t\u0018\u00010\u0019¢\u0006\u0002\b\u001a\u0018\u00010\u00102\n\b\u0002\u0010\u001b\u001a\u0004\u0018\u00010\u001c2\b\b\u0002\u0010\u001d\u001a\u00020\u00042\b\b\u0002\u0010\u001e\u001a\u00020\u00042\n\b\u0002\u0010\u001f\u001a\u0004\u0018\u00010\u00042\n\b\u0002\u0010 \u001a\u0004\u0018\u00010!2\n\b\u0002\u0010\"\u001a\u0004\u0018\u00010#2\n\b\u0002\u0010$\u001a\u0004\u0018\u00010%2\n\b\u0002\u0010&\u001a\u0004\u0018\u00010\u0004HÆ\u0001¢\u0006\u0002\u0010dJ\t\u0010e\u001a\u00020fHÖ\u0001J\u0013\u0010g\u001a\u00020\u000e2\b\u0010h\u001a\u0004\u0018\u00010\u0019HÖ\u0003J\t\u0010i\u001a\u00020fHÖ\u0001J\t\u0010j\u001a\u00020\u0004HÖ\u0001J\u0019\u0010k\u001a\u00020l2\u0006\u0010m\u001a\u00020n2\u0006\u0010o\u001a\u00020fHÖ\u0001R\u0010\u0010\"\u001a\u0004\u0018\u00010#X\u0004¢\u0006\u0002\n\u0000R\u0010\u0010 \u001a\u0004\u0018\u00010!X\u0004¢\u0006\u0002\n\u0000R\u0015\u0010\u0005\u001a\u0004\u0018\u00010\u0006¢\u0006\n\n\u0002\u0010*\u001a\u0004\b(\u0010)R\u0013\u0010\u0007\u001a\u0004\u0018\u00010\u0004¢\u0006\b\n\u0000\u001a\u0004\b+\u0010,R\u0013\u0010\b\u001a\u0004\u0018\u00010\t¢\u0006\b\n\u0000\u001a\u0004\b-\u0010.R\u0015\u0010\n\u001a\u0004\u0018\u00010\u0006¢\u0006\n\n\u0002\u0010*\u001a\u0004\b/\u0010)R\u0013\u0010\u000b\u001a\u0004\u0018\u00010\u0004¢\u0006\b\n\u0000\u001a\u0004\b0\u0010,R\u0013\u0010\f\u001a\u0004\u0018\u00010\u0004¢\u0006\b\n\u0000\u001a\u0004\b1\u0010,R\u0016\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0004¢\u0006\b\n\u0000\u001a\u0004\b2\u0010,R\u0015\u0010\r\u001a\u0004\u0018\u00010\u000e¢\u0006\n\n\u0002\u00104\u001a\u0004\b\r\u00103R\u0011\u00105\u001a\u00020#8F¢\u0006\u0006\u001a\u0004\b6\u00107R\u001f\u0010\u000f\u001a\u0010\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u0010¢\u0006\b\n\u0000\u001a\u0004\b8\u00109R\u0013\u0010\u0011\u001a\u0004\u0018\u00010\u0012¢\u0006\b\n\u0000\u001a\u0004\b:\u0010;R\u0013\u0010\u0013\u001a\u0004\u0018\u00010\u0014¢\u0006\b\n\u0000\u001a\u0004\b<\u0010=R\u0013\u0010\u0015\u001a\u0004\u0018\u00010\u0016¢\u0006\b\n\u0000\u001a\u0004\b>\u0010?R\u0013\u0010$\u001a\u0004\u0018\u00010%¢\u0006\b\n\u0000\u001a\u0004\b@\u0010AR&\u0010\u0018\u001a\u0017\u0012\u0004\u0012\u00020\u0004\u0012\u000b\u0012\t\u0018\u00010\u0019¢\u0006\u0002\b\u001a\u0018\u00010\u0010¢\u0006\b\n\u0000\u001a\u0004\bB\u00109R\u0013\u0010\u001b\u001a\u0004\u0018\u00010\u001c¢\u0006\b\n\u0000\u001a\u0004\bC\u0010DR\u0013\u0010&\u001a\u0004\u0018\u00010\u0004¢\u0006\b\n\u0000\u001a\u0004\bE\u0010,R\u0013\u0010\u0017\u001a\u0004\u0018\u00010\u0004¢\u0006\b\n\u0000\u001a\u0004\bF\u0010,R\u0011\u0010\u001d\u001a\u00020\u0004¢\u0006\b\n\u0000\u001a\u0004\bG\u0010,R\u0011\u0010\u001e\u001a\u00020\u0004¢\u0006\b\n\u0000\u001a\u0004\bH\u0010,R\u0013\u0010\u001f\u001a\u0004\u0018\u00010\u0004¢\u0006\b\n\u0000\u001a\u0004\bI\u0010,R\u0011\u0010J\u001a\u00020!8F¢\u0006\u0006\u001a\u0004\bK\u0010L¨\u0006v"}, d2 = {"Lcom/stripe/android/model/Source;", "Lcom/stripe/android/model/StripeModel;", "Lcom/stripe/android/model/StripePaymentSource;", "id", "", "amount", "", "clientSecret", "codeVerification", "Lcom/stripe/android/model/SourceCodeVerification;", "created", "currency", "flow", "isLiveMode", "", "metaData", "", "owner", "Lcom/stripe/android/model/SourceOwner;", "receiver", "Lcom/stripe/android/model/SourceReceiver;", "redirect", "Lcom/stripe/android/model/SourceRedirect;", "status", "sourceTypeData", "", "Lkotlinx/android/parcel/RawValue;", "sourceTypeModel", "Lcom/stripe/android/model/SourceTypeModel;", "type", "typeRaw", "usage", "_weChat", "Lcom/stripe/android/model/WeChat;", "_klarna", "Lcom/stripe/android/model/Source$Klarna;", "sourceOrder", "Lcom/stripe/android/model/SourceOrder;", "statementDescriptor", "(Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Lcom/stripe/android/model/SourceCodeVerification;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/util/Map;Lcom/stripe/android/model/SourceOwner;Lcom/stripe/android/model/SourceReceiver;Lcom/stripe/android/model/SourceRedirect;Ljava/lang/String;Ljava/util/Map;Lcom/stripe/android/model/SourceTypeModel;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/stripe/android/model/WeChat;Lcom/stripe/android/model/Source$Klarna;Lcom/stripe/android/model/SourceOrder;Ljava/lang/String;)V", "getAmount", "()Ljava/lang/Long;", "Ljava/lang/Long;", "getClientSecret", "()Ljava/lang/String;", "getCodeVerification", "()Lcom/stripe/android/model/SourceCodeVerification;", "getCreated", "getCurrency", "getFlow", "getId", "()Ljava/lang/Boolean;", "Ljava/lang/Boolean;", "klarna", "getKlarna", "()Lcom/stripe/android/model/Source$Klarna;", "getMetaData", "()Ljava/util/Map;", "getOwner", "()Lcom/stripe/android/model/SourceOwner;", "getReceiver", "()Lcom/stripe/android/model/SourceReceiver;", "getRedirect", "()Lcom/stripe/android/model/SourceRedirect;", "getSourceOrder", "()Lcom/stripe/android/model/SourceOrder;", "getSourceTypeData", "getSourceTypeModel", "()Lcom/stripe/android/model/SourceTypeModel;", "getStatementDescriptor", "getStatus", "getType", "getTypeRaw", "getUsage", "weChat", "getWeChat", "()Lcom/stripe/android/model/WeChat;", "component1", "component10", "component11", "component12", "component13", "component14", "component15", "component16", "component17", "component18", "component19", "component2", "component20", "component21", "component22", "component3", "component4", "component5", "component6", "component7", "component8", "component9", "copy", "(Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Lcom/stripe/android/model/SourceCodeVerification;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/util/Map;Lcom/stripe/android/model/SourceOwner;Lcom/stripe/android/model/SourceReceiver;Lcom/stripe/android/model/SourceRedirect;Ljava/lang/String;Ljava/util/Map;Lcom/stripe/android/model/SourceTypeModel;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/stripe/android/model/WeChat;Lcom/stripe/android/model/Source$Klarna;Lcom/stripe/android/model/SourceOrder;Ljava/lang/String;)Lcom/stripe/android/model/Source;", "describeContents", "", "equals", "other", "hashCode", "toString", "writeToParcel", "", "parcel", "Landroid/os/Parcel;", "flags", "Companion", "Klarna", "SourceFlow", "SourceStatus", "SourceType", "Usage", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: Source.kt */
public final class Source implements StripeModel, StripePaymentSource {
    public static final Parcelable.Creator CREATOR = new Creator();
    public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
    public static final String EURO = "eur";
    public static final String OBJECT_TYPE = "source";
    public static final String USD = "usd";
    private final Klarna _klarna;
    private final WeChat _weChat;
    private final Long amount;
    private final String clientSecret;
    private final SourceCodeVerification codeVerification;
    private final Long created;
    private final String currency;
    private final String flow;
    private final String id;
    private final Boolean isLiveMode;
    private final Map<String, String> metaData;
    private final SourceOwner owner;
    private final SourceReceiver receiver;
    private final SourceRedirect redirect;
    private final SourceOrder sourceOrder;
    private final Map<String, Object> sourceTypeData;
    private final SourceTypeModel sourceTypeModel;
    private final String statementDescriptor;
    private final String status;
    private final String type;
    private final String typeRaw;
    private final String usage;

    @Metadata(bv = {1, 0, 3}, k = 3, mv = {1, 1, 16})
    public static class Creator implements Parcelable.Creator {
        public final Object createFromParcel(Parcel parcel) {
            Boolean bool;
            LinkedHashMap linkedHashMap;
            SourceReceiver sourceReceiver;
            LinkedHashMap linkedHashMap2;
            SourceRedirect sourceRedirect;
            Parcel parcel2 = parcel;
            Intrinsics.checkParameterIsNotNull(parcel2, "in");
            String readString = parcel.readString();
            Long valueOf = parcel.readInt() != 0 ? Long.valueOf(parcel.readLong()) : null;
            String readString2 = parcel.readString();
            SourceCodeVerification sourceCodeVerification = parcel.readInt() != 0 ? (SourceCodeVerification) SourceCodeVerification.CREATOR.createFromParcel(parcel2) : null;
            Long valueOf2 = parcel.readInt() != 0 ? Long.valueOf(parcel.readLong()) : null;
            String readString3 = parcel.readString();
            String readString4 = parcel.readString();
            if (parcel.readInt() != 0) {
                bool = Boolean.valueOf(parcel.readInt() != 0);
            } else {
                bool = null;
            }
            if (parcel.readInt() != 0) {
                int readInt = parcel.readInt();
                linkedHashMap = new LinkedHashMap(readInt);
                while (readInt != 0) {
                    linkedHashMap.put(parcel.readString(), parcel.readString());
                    readInt--;
                }
            } else {
                linkedHashMap = null;
            }
            SourceOwner sourceOwner = parcel.readInt() != 0 ? (SourceOwner) SourceOwner.CREATOR.createFromParcel(parcel2) : null;
            SourceReceiver sourceReceiver2 = parcel.readInt() != 0 ? (SourceReceiver) SourceReceiver.CREATOR.createFromParcel(parcel2) : null;
            SourceRedirect sourceRedirect2 = parcel.readInt() != 0 ? (SourceRedirect) SourceRedirect.CREATOR.createFromParcel(parcel2) : null;
            String readString5 = parcel.readString();
            if (parcel.readInt() != 0) {
                int readInt2 = parcel.readInt();
                LinkedHashMap linkedHashMap3 = new LinkedHashMap(readInt2);
                while (readInt2 != 0) {
                    linkedHashMap3.put(parcel.readString(), parcel2.readValue(Object.class.getClassLoader()));
                    readInt2--;
                    sourceRedirect2 = sourceRedirect2;
                    sourceReceiver2 = sourceReceiver2;
                }
                sourceReceiver = sourceReceiver2;
                sourceRedirect = sourceRedirect2;
                linkedHashMap2 = linkedHashMap3;
            } else {
                sourceReceiver = sourceReceiver2;
                sourceRedirect = sourceRedirect2;
                linkedHashMap2 = null;
            }
            return new Source(readString, valueOf, readString2, sourceCodeVerification, valueOf2, readString3, readString4, bool, linkedHashMap, sourceOwner, sourceReceiver, sourceRedirect, readString5, linkedHashMap2, (SourceTypeModel) parcel2.readParcelable(Source.class.getClassLoader()), parcel.readString(), parcel.readString(), parcel.readString(), parcel.readInt() != 0 ? (WeChat) WeChat.CREATOR.createFromParcel(parcel2) : null, parcel.readInt() != 0 ? (Klarna) Klarna.CREATOR.createFromParcel(parcel2) : null, parcel.readInt() != 0 ? (SourceOrder) SourceOrder.CREATOR.createFromParcel(parcel2) : null, parcel.readString());
        }

        public final Object[] newArray(int i) {
            return new Source[i];
        }
    }

    @JvmStatic
    public static final String asSourceType(String str) {
        return Companion.asSourceType(str);
    }

    private final WeChat component19() {
        return this._weChat;
    }

    private final Klarna component20() {
        return this._klarna;
    }

    public static /* synthetic */ Source copy$default(Source source, String str, Long l, String str2, SourceCodeVerification sourceCodeVerification, Long l2, String str3, String str4, Boolean bool, Map map, SourceOwner sourceOwner, SourceReceiver sourceReceiver, SourceRedirect sourceRedirect, String str5, Map map2, SourceTypeModel sourceTypeModel2, String str6, String str7, String str8, WeChat weChat, Klarna klarna, SourceOrder sourceOrder2, String str9, int i, Object obj) {
        Source source2 = source;
        int i2 = i;
        return source.copy((i2 & 1) != 0 ? source.getId() : str, (i2 & 2) != 0 ? source2.amount : l, (i2 & 4) != 0 ? source2.clientSecret : str2, (i2 & 8) != 0 ? source2.codeVerification : sourceCodeVerification, (i2 & 16) != 0 ? source2.created : l2, (i2 & 32) != 0 ? source2.currency : str3, (i2 & 64) != 0 ? source2.flow : str4, (i2 & 128) != 0 ? source2.isLiveMode : bool, (i2 & 256) != 0 ? source2.metaData : map, (i2 & 512) != 0 ? source2.owner : sourceOwner, (i2 & 1024) != 0 ? source2.receiver : sourceReceiver, (i2 & 2048) != 0 ? source2.redirect : sourceRedirect, (i2 & 4096) != 0 ? source2.status : str5, (i2 & 8192) != 0 ? source2.sourceTypeData : map2, (i2 & 16384) != 0 ? source2.sourceTypeModel : sourceTypeModel2, (i2 & 32768) != 0 ? source2.type : str6, (i2 & 65536) != 0 ? source2.typeRaw : str7, (i2 & 131072) != 0 ? source2.usage : str8, (i2 & 262144) != 0 ? source2._weChat : weChat, (i2 & 524288) != 0 ? source2._klarna : klarna, (i2 & 1048576) != 0 ? source2.sourceOrder : sourceOrder2, (i2 & 2097152) != 0 ? source2.statementDescriptor : str9);
    }

    @JvmStatic
    public static final Source fromJson(JSONObject jSONObject) {
        return Companion.fromJson(jSONObject);
    }

    public final String component1() {
        return getId();
    }

    public final SourceOwner component10() {
        return this.owner;
    }

    public final SourceReceiver component11() {
        return this.receiver;
    }

    public final SourceRedirect component12() {
        return this.redirect;
    }

    public final String component13() {
        return this.status;
    }

    public final Map<String, Object> component14() {
        return this.sourceTypeData;
    }

    public final SourceTypeModel component15() {
        return this.sourceTypeModel;
    }

    public final String component16() {
        return this.type;
    }

    public final String component17() {
        return this.typeRaw;
    }

    public final String component18() {
        return this.usage;
    }

    public final Long component2() {
        return this.amount;
    }

    public final SourceOrder component21() {
        return this.sourceOrder;
    }

    public final String component22() {
        return this.statementDescriptor;
    }

    public final String component3() {
        return this.clientSecret;
    }

    public final SourceCodeVerification component4() {
        return this.codeVerification;
    }

    public final Long component5() {
        return this.created;
    }

    public final String component6() {
        return this.currency;
    }

    public final String component7() {
        return this.flow;
    }

    public final Boolean component8() {
        return this.isLiveMode;
    }

    public final Map<String, String> component9() {
        return this.metaData;
    }

    public final Source copy(String str, Long l, String str2, SourceCodeVerification sourceCodeVerification, Long l2, String str3, String str4, Boolean bool, Map<String, String> map, SourceOwner sourceOwner, SourceReceiver sourceReceiver, SourceRedirect sourceRedirect, String str5, Map<String, ? extends Object> map2, SourceTypeModel sourceTypeModel2, String str6, String str7, String str8, WeChat weChat, Klarna klarna, SourceOrder sourceOrder2, String str9) {
        String str10 = str;
        Intrinsics.checkParameterIsNotNull(str6, "type");
        Intrinsics.checkParameterIsNotNull(str7, "typeRaw");
        return new Source(str, l, str2, sourceCodeVerification, l2, str3, str4, bool, map, sourceOwner, sourceReceiver, sourceRedirect, str5, map2, sourceTypeModel2, str6, str7, str8, weChat, klarna, sourceOrder2, str9);
    }

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Source)) {
            return false;
        }
        Source source = (Source) obj;
        return Intrinsics.areEqual((Object) getId(), (Object) source.getId()) && Intrinsics.areEqual((Object) this.amount, (Object) source.amount) && Intrinsics.areEqual((Object) this.clientSecret, (Object) source.clientSecret) && Intrinsics.areEqual((Object) this.codeVerification, (Object) source.codeVerification) && Intrinsics.areEqual((Object) this.created, (Object) source.created) && Intrinsics.areEqual((Object) this.currency, (Object) source.currency) && Intrinsics.areEqual((Object) this.flow, (Object) source.flow) && Intrinsics.areEqual((Object) this.isLiveMode, (Object) source.isLiveMode) && Intrinsics.areEqual((Object) this.metaData, (Object) source.metaData) && Intrinsics.areEqual((Object) this.owner, (Object) source.owner) && Intrinsics.areEqual((Object) this.receiver, (Object) source.receiver) && Intrinsics.areEqual((Object) this.redirect, (Object) source.redirect) && Intrinsics.areEqual((Object) this.status, (Object) source.status) && Intrinsics.areEqual((Object) this.sourceTypeData, (Object) source.sourceTypeData) && Intrinsics.areEqual((Object) this.sourceTypeModel, (Object) source.sourceTypeModel) && Intrinsics.areEqual((Object) this.type, (Object) source.type) && Intrinsics.areEqual((Object) this.typeRaw, (Object) source.typeRaw) && Intrinsics.areEqual((Object) this.usage, (Object) source.usage) && Intrinsics.areEqual((Object) this._weChat, (Object) source._weChat) && Intrinsics.areEqual((Object) this._klarna, (Object) source._klarna) && Intrinsics.areEqual((Object) this.sourceOrder, (Object) source.sourceOrder) && Intrinsics.areEqual((Object) this.statementDescriptor, (Object) source.statementDescriptor);
    }

    public int hashCode() {
        String id2 = getId();
        int i = 0;
        int hashCode = (id2 != null ? id2.hashCode() : 0) * 31;
        Long l = this.amount;
        int hashCode2 = (hashCode + (l != null ? l.hashCode() : 0)) * 31;
        String str = this.clientSecret;
        int hashCode3 = (hashCode2 + (str != null ? str.hashCode() : 0)) * 31;
        SourceCodeVerification sourceCodeVerification = this.codeVerification;
        int hashCode4 = (hashCode3 + (sourceCodeVerification != null ? sourceCodeVerification.hashCode() : 0)) * 31;
        Long l2 = this.created;
        int hashCode5 = (hashCode4 + (l2 != null ? l2.hashCode() : 0)) * 31;
        String str2 = this.currency;
        int hashCode6 = (hashCode5 + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.flow;
        int hashCode7 = (hashCode6 + (str3 != null ? str3.hashCode() : 0)) * 31;
        Boolean bool = this.isLiveMode;
        int hashCode8 = (hashCode7 + (bool != null ? bool.hashCode() : 0)) * 31;
        Map<String, String> map = this.metaData;
        int hashCode9 = (hashCode8 + (map != null ? map.hashCode() : 0)) * 31;
        SourceOwner sourceOwner = this.owner;
        int hashCode10 = (hashCode9 + (sourceOwner != null ? sourceOwner.hashCode() : 0)) * 31;
        SourceReceiver sourceReceiver = this.receiver;
        int hashCode11 = (hashCode10 + (sourceReceiver != null ? sourceReceiver.hashCode() : 0)) * 31;
        SourceRedirect sourceRedirect = this.redirect;
        int hashCode12 = (hashCode11 + (sourceRedirect != null ? sourceRedirect.hashCode() : 0)) * 31;
        String str4 = this.status;
        int hashCode13 = (hashCode12 + (str4 != null ? str4.hashCode() : 0)) * 31;
        Map<String, Object> map2 = this.sourceTypeData;
        int hashCode14 = (hashCode13 + (map2 != null ? map2.hashCode() : 0)) * 31;
        SourceTypeModel sourceTypeModel2 = this.sourceTypeModel;
        int hashCode15 = (hashCode14 + (sourceTypeModel2 != null ? sourceTypeModel2.hashCode() : 0)) * 31;
        String str5 = this.type;
        int hashCode16 = (hashCode15 + (str5 != null ? str5.hashCode() : 0)) * 31;
        String str6 = this.typeRaw;
        int hashCode17 = (hashCode16 + (str6 != null ? str6.hashCode() : 0)) * 31;
        String str7 = this.usage;
        int hashCode18 = (hashCode17 + (str7 != null ? str7.hashCode() : 0)) * 31;
        WeChat weChat = this._weChat;
        int hashCode19 = (hashCode18 + (weChat != null ? weChat.hashCode() : 0)) * 31;
        Klarna klarna = this._klarna;
        int hashCode20 = (hashCode19 + (klarna != null ? klarna.hashCode() : 0)) * 31;
        SourceOrder sourceOrder2 = this.sourceOrder;
        int hashCode21 = (hashCode20 + (sourceOrder2 != null ? sourceOrder2.hashCode() : 0)) * 31;
        String str8 = this.statementDescriptor;
        if (str8 != null) {
            i = str8.hashCode();
        }
        return hashCode21 + i;
    }

    public String toString() {
        return "Source(id=" + getId() + ", amount=" + this.amount + ", clientSecret=" + this.clientSecret + ", codeVerification=" + this.codeVerification + ", created=" + this.created + ", currency=" + this.currency + ", flow=" + this.flow + ", isLiveMode=" + this.isLiveMode + ", metaData=" + this.metaData + ", owner=" + this.owner + ", receiver=" + this.receiver + ", redirect=" + this.redirect + ", status=" + this.status + ", sourceTypeData=" + this.sourceTypeData + ", sourceTypeModel=" + this.sourceTypeModel + ", type=" + this.type + ", typeRaw=" + this.typeRaw + ", usage=" + this.usage + ", _weChat=" + this._weChat + ", _klarna=" + this._klarna + ", sourceOrder=" + this.sourceOrder + ", statementDescriptor=" + this.statementDescriptor + ")";
    }

    public void writeToParcel(Parcel parcel, int i) {
        Intrinsics.checkParameterIsNotNull(parcel, "parcel");
        parcel.writeString(this.id);
        Long l = this.amount;
        if (l != null) {
            parcel.writeInt(1);
            parcel.writeLong(l.longValue());
        } else {
            parcel.writeInt(0);
        }
        parcel.writeString(this.clientSecret);
        SourceCodeVerification sourceCodeVerification = this.codeVerification;
        if (sourceCodeVerification != null) {
            parcel.writeInt(1);
            sourceCodeVerification.writeToParcel(parcel, 0);
        } else {
            parcel.writeInt(0);
        }
        Long l2 = this.created;
        if (l2 != null) {
            parcel.writeInt(1);
            parcel.writeLong(l2.longValue());
        } else {
            parcel.writeInt(0);
        }
        parcel.writeString(this.currency);
        parcel.writeString(this.flow);
        Boolean bool = this.isLiveMode;
        if (bool != null) {
            parcel.writeInt(1);
            parcel.writeInt(bool.booleanValue() ? 1 : 0);
        } else {
            parcel.writeInt(0);
        }
        Map<String, String> map = this.metaData;
        if (map != null) {
            parcel.writeInt(1);
            parcel.writeInt(map.size());
            for (Map.Entry<String, String> next : map.entrySet()) {
                parcel.writeString(next.getKey());
                parcel.writeString(next.getValue());
            }
        } else {
            parcel.writeInt(0);
        }
        SourceOwner sourceOwner = this.owner;
        if (sourceOwner != null) {
            parcel.writeInt(1);
            sourceOwner.writeToParcel(parcel, 0);
        } else {
            parcel.writeInt(0);
        }
        SourceReceiver sourceReceiver = this.receiver;
        if (sourceReceiver != null) {
            parcel.writeInt(1);
            sourceReceiver.writeToParcel(parcel, 0);
        } else {
            parcel.writeInt(0);
        }
        SourceRedirect sourceRedirect = this.redirect;
        if (sourceRedirect != null) {
            parcel.writeInt(1);
            sourceRedirect.writeToParcel(parcel, 0);
        } else {
            parcel.writeInt(0);
        }
        parcel.writeString(this.status);
        Map<String, Object> map2 = this.sourceTypeData;
        if (map2 != null) {
            parcel.writeInt(1);
            parcel.writeInt(map2.size());
            for (Map.Entry<String, Object> next2 : map2.entrySet()) {
                parcel.writeString(next2.getKey());
                parcel.writeValue(next2.getValue());
            }
        } else {
            parcel.writeInt(0);
        }
        parcel.writeParcelable(this.sourceTypeModel, i);
        parcel.writeString(this.type);
        parcel.writeString(this.typeRaw);
        parcel.writeString(this.usage);
        WeChat weChat = this._weChat;
        if (weChat != null) {
            parcel.writeInt(1);
            weChat.writeToParcel(parcel, 0);
        } else {
            parcel.writeInt(0);
        }
        Klarna klarna = this._klarna;
        if (klarna != null) {
            parcel.writeInt(1);
            klarna.writeToParcel(parcel, 0);
        } else {
            parcel.writeInt(0);
        }
        SourceOrder sourceOrder2 = this.sourceOrder;
        if (sourceOrder2 != null) {
            parcel.writeInt(1);
            sourceOrder2.writeToParcel(parcel, 0);
        } else {
            parcel.writeInt(0);
        }
        parcel.writeString(this.statementDescriptor);
    }

    public Source(String str, Long l, String str2, SourceCodeVerification sourceCodeVerification, Long l2, String str3, String str4, Boolean bool, Map<String, String> map, SourceOwner sourceOwner, SourceReceiver sourceReceiver, SourceRedirect sourceRedirect, String str5, Map<String, ? extends Object> map2, SourceTypeModel sourceTypeModel2, String str6, String str7, String str8, WeChat weChat, Klarna klarna, SourceOrder sourceOrder2, String str9) {
        String str10 = str6;
        String str11 = str7;
        Intrinsics.checkParameterIsNotNull(str10, "type");
        Intrinsics.checkParameterIsNotNull(str11, "typeRaw");
        this.id = str;
        this.amount = l;
        this.clientSecret = str2;
        this.codeVerification = sourceCodeVerification;
        this.created = l2;
        this.currency = str3;
        this.flow = str4;
        this.isLiveMode = bool;
        this.metaData = map;
        this.owner = sourceOwner;
        this.receiver = sourceReceiver;
        this.redirect = sourceRedirect;
        this.status = str5;
        this.sourceTypeData = map2;
        this.sourceTypeModel = sourceTypeModel2;
        this.type = str10;
        this.typeRaw = str11;
        this.usage = str8;
        this._weChat = weChat;
        this._klarna = klarna;
        this.sourceOrder = sourceOrder2;
        this.statementDescriptor = str9;
    }

    public String getId() {
        return this.id;
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* synthetic */ Source(java.lang.String r27, java.lang.Long r28, java.lang.String r29, com.stripe.android.model.SourceCodeVerification r30, java.lang.Long r31, java.lang.String r32, java.lang.String r33, java.lang.Boolean r34, java.util.Map r35, com.stripe.android.model.SourceOwner r36, com.stripe.android.model.SourceReceiver r37, com.stripe.android.model.SourceRedirect r38, java.lang.String r39, java.util.Map r40, com.stripe.android.model.SourceTypeModel r41, java.lang.String r42, java.lang.String r43, java.lang.String r44, com.stripe.android.model.WeChat r45, com.stripe.android.model.Source.Klarna r46, com.stripe.android.model.SourceOrder r47, java.lang.String r48, int r49, kotlin.jvm.internal.DefaultConstructorMarker r50) {
        /*
            r26 = this;
            r0 = r49
            r1 = r0 & 2
            r2 = 0
            if (r1 == 0) goto L_0x000c
            r1 = r2
            java.lang.Long r1 = (java.lang.Long) r1
            r5 = r1
            goto L_0x000e
        L_0x000c:
            r5 = r28
        L_0x000e:
            r1 = r0 & 4
            if (r1 == 0) goto L_0x0017
            r1 = r2
            java.lang.String r1 = (java.lang.String) r1
            r6 = r1
            goto L_0x0019
        L_0x0017:
            r6 = r29
        L_0x0019:
            r1 = r0 & 8
            if (r1 == 0) goto L_0x0022
            r1 = r2
            com.stripe.android.model.SourceCodeVerification r1 = (com.stripe.android.model.SourceCodeVerification) r1
            r7 = r1
            goto L_0x0024
        L_0x0022:
            r7 = r30
        L_0x0024:
            r1 = r0 & 16
            if (r1 == 0) goto L_0x002d
            r1 = r2
            java.lang.Long r1 = (java.lang.Long) r1
            r8 = r1
            goto L_0x002f
        L_0x002d:
            r8 = r31
        L_0x002f:
            r1 = r0 & 32
            if (r1 == 0) goto L_0x0038
            r1 = r2
            java.lang.String r1 = (java.lang.String) r1
            r9 = r1
            goto L_0x003a
        L_0x0038:
            r9 = r32
        L_0x003a:
            r1 = r0 & 64
            if (r1 == 0) goto L_0x0043
            r1 = r2
            java.lang.String r1 = (java.lang.String) r1
            r10 = r1
            goto L_0x0045
        L_0x0043:
            r10 = r33
        L_0x0045:
            r1 = r0 & 128(0x80, float:1.794E-43)
            if (r1 == 0) goto L_0x004e
            r1 = r2
            java.lang.Boolean r1 = (java.lang.Boolean) r1
            r11 = r1
            goto L_0x0050
        L_0x004e:
            r11 = r34
        L_0x0050:
            r1 = r0 & 256(0x100, float:3.59E-43)
            if (r1 == 0) goto L_0x0059
            r1 = r2
            java.util.Map r1 = (java.util.Map) r1
            r12 = r1
            goto L_0x005b
        L_0x0059:
            r12 = r35
        L_0x005b:
            r1 = r0 & 512(0x200, float:7.175E-43)
            if (r1 == 0) goto L_0x0064
            r1 = r2
            com.stripe.android.model.SourceOwner r1 = (com.stripe.android.model.SourceOwner) r1
            r13 = r1
            goto L_0x0066
        L_0x0064:
            r13 = r36
        L_0x0066:
            r1 = r0 & 1024(0x400, float:1.435E-42)
            if (r1 == 0) goto L_0x006f
            r1 = r2
            com.stripe.android.model.SourceReceiver r1 = (com.stripe.android.model.SourceReceiver) r1
            r14 = r1
            goto L_0x0071
        L_0x006f:
            r14 = r37
        L_0x0071:
            r1 = r0 & 2048(0x800, float:2.87E-42)
            if (r1 == 0) goto L_0x007a
            r1 = r2
            com.stripe.android.model.SourceRedirect r1 = (com.stripe.android.model.SourceRedirect) r1
            r15 = r1
            goto L_0x007c
        L_0x007a:
            r15 = r38
        L_0x007c:
            r1 = r0 & 4096(0x1000, float:5.74E-42)
            if (r1 == 0) goto L_0x0086
            r1 = r2
            java.lang.String r1 = (java.lang.String) r1
            r16 = r1
            goto L_0x0088
        L_0x0086:
            r16 = r39
        L_0x0088:
            r1 = r0 & 8192(0x2000, float:1.14794E-41)
            if (r1 == 0) goto L_0x0092
            r1 = r2
            java.util.Map r1 = (java.util.Map) r1
            r17 = r1
            goto L_0x0094
        L_0x0092:
            r17 = r40
        L_0x0094:
            r1 = r0 & 16384(0x4000, float:2.2959E-41)
            if (r1 == 0) goto L_0x009e
            r1 = r2
            com.stripe.android.model.SourceTypeModel r1 = (com.stripe.android.model.SourceTypeModel) r1
            r18 = r1
            goto L_0x00a0
        L_0x009e:
            r18 = r41
        L_0x00a0:
            r1 = 131072(0x20000, float:1.83671E-40)
            r1 = r1 & r0
            if (r1 == 0) goto L_0x00ab
            r1 = r2
            java.lang.String r1 = (java.lang.String) r1
            r21 = r1
            goto L_0x00ad
        L_0x00ab:
            r21 = r44
        L_0x00ad:
            r1 = 262144(0x40000, float:3.67342E-40)
            r1 = r1 & r0
            if (r1 == 0) goto L_0x00b8
            r1 = r2
            com.stripe.android.model.WeChat r1 = (com.stripe.android.model.WeChat) r1
            r22 = r1
            goto L_0x00ba
        L_0x00b8:
            r22 = r45
        L_0x00ba:
            r1 = 524288(0x80000, float:7.34684E-40)
            r1 = r1 & r0
            if (r1 == 0) goto L_0x00c5
            r1 = r2
            com.stripe.android.model.Source$Klarna r1 = (com.stripe.android.model.Source.Klarna) r1
            r23 = r1
            goto L_0x00c7
        L_0x00c5:
            r23 = r46
        L_0x00c7:
            r1 = 1048576(0x100000, float:1.469368E-39)
            r1 = r1 & r0
            if (r1 == 0) goto L_0x00d2
            r1 = r2
            com.stripe.android.model.SourceOrder r1 = (com.stripe.android.model.SourceOrder) r1
            r24 = r1
            goto L_0x00d4
        L_0x00d2:
            r24 = r47
        L_0x00d4:
            r1 = 2097152(0x200000, float:2.938736E-39)
            r0 = r0 & r1
            if (r0 == 0) goto L_0x00df
            r0 = r2
            java.lang.String r0 = (java.lang.String) r0
            r25 = r0
            goto L_0x00e1
        L_0x00df:
            r25 = r48
        L_0x00e1:
            r3 = r26
            r4 = r27
            r19 = r42
            r20 = r43
            r3.<init>(r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15, r16, r17, r18, r19, r20, r21, r22, r23, r24, r25)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.stripe.android.model.Source.<init>(java.lang.String, java.lang.Long, java.lang.String, com.stripe.android.model.SourceCodeVerification, java.lang.Long, java.lang.String, java.lang.String, java.lang.Boolean, java.util.Map, com.stripe.android.model.SourceOwner, com.stripe.android.model.SourceReceiver, com.stripe.android.model.SourceRedirect, java.lang.String, java.util.Map, com.stripe.android.model.SourceTypeModel, java.lang.String, java.lang.String, java.lang.String, com.stripe.android.model.WeChat, com.stripe.android.model.Source$Klarna, com.stripe.android.model.SourceOrder, java.lang.String, int, kotlin.jvm.internal.DefaultConstructorMarker):void");
    }

    public final Long getAmount() {
        return this.amount;
    }

    public final String getClientSecret() {
        return this.clientSecret;
    }

    public final SourceCodeVerification getCodeVerification() {
        return this.codeVerification;
    }

    public final Long getCreated() {
        return this.created;
    }

    public final String getCurrency() {
        return this.currency;
    }

    public final String getFlow() {
        return this.flow;
    }

    public final Boolean isLiveMode() {
        return this.isLiveMode;
    }

    public final Map<String, String> getMetaData() {
        return this.metaData;
    }

    public final SourceOwner getOwner() {
        return this.owner;
    }

    public final SourceReceiver getReceiver() {
        return this.receiver;
    }

    public final SourceRedirect getRedirect() {
        return this.redirect;
    }

    public final String getStatus() {
        return this.status;
    }

    public final Map<String, Object> getSourceTypeData() {
        return this.sourceTypeData;
    }

    public final SourceTypeModel getSourceTypeModel() {
        return this.sourceTypeModel;
    }

    public final String getType() {
        return this.type;
    }

    public final String getTypeRaw() {
        return this.typeRaw;
    }

    public final String getUsage() {
        return this.usage;
    }

    public final SourceOrder getSourceOrder() {
        return this.sourceOrder;
    }

    public final String getStatementDescriptor() {
        return this.statementDescriptor;
    }

    public final WeChat getWeChat() {
        if (Intrinsics.areEqual((Object) "wechat", (Object) this.type)) {
            WeChat weChat = this._weChat;
            if (weChat != null) {
                return weChat;
            }
            throw new IllegalArgumentException("Required value was null.".toString());
        }
        throw new IllegalStateException("Source type must be 'wechat'".toString());
    }

    public final Klarna getKlarna() {
        if (Intrinsics.areEqual((Object) "klarna", (Object) this.type)) {
            Klarna klarna = this._klarna;
            if (klarna != null) {
                return klarna;
            }
            throw new IllegalArgumentException("Required value was null.".toString());
        }
        throw new IllegalStateException("Source type must be 'klarna'".toString());
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u001b\n\u0002\b\u0002\b\u0002\u0018\u0000 \u00022\u00020\u0001:\u0001\u0002B\u0000¨\u0006\u0003"}, d2 = {"Lcom/stripe/android/model/Source$SourceType;", "", "Companion", "stripe_release"}, k = 1, mv = {1, 1, 16})
    @Retention(AnnotationRetention.SOURCE)
    @java.lang.annotation.Retention(RetentionPolicy.SOURCE)
    /* compiled from: Source.kt */
    public @interface SourceType {
        public static final String ALIPAY = "alipay";
        public static final String BANCONTACT = "bancontact";
        public static final String CARD = "card";
        public static final Companion Companion = Companion.$$INSTANCE;
        public static final String EPS = "eps";
        public static final String GIROPAY = "giropay";
        public static final String IDEAL = "ideal";
        public static final String KLARNA = "klarna";
        public static final String MULTIBANCO = "multibanco";
        public static final String P24 = "p24";
        public static final String SEPA_DEBIT = "sepa_debit";
        public static final String SOFORT = "sofort";
        public static final String THREE_D_SECURE = "three_d_secure";
        public static final String UNKNOWN = "unknown";
        public static final String WECHAT = "wechat";

        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u000e\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\u0012"}, d2 = {"Lcom/stripe/android/model/Source$SourceType$Companion;", "", "()V", "ALIPAY", "", "BANCONTACT", "CARD", "EPS", "GIROPAY", "IDEAL", "KLARNA", "MULTIBANCO", "P24", "SEPA_DEBIT", "SOFORT", "THREE_D_SECURE", "UNKNOWN", "WECHAT", "stripe_release"}, k = 1, mv = {1, 1, 16})
        /* compiled from: Source.kt */
        public static final class Companion {
            static final /* synthetic */ Companion $$INSTANCE = new Companion();
            public static final String ALIPAY = "alipay";
            public static final String BANCONTACT = "bancontact";
            public static final String CARD = "card";
            public static final String EPS = "eps";
            public static final String GIROPAY = "giropay";
            public static final String IDEAL = "ideal";
            public static final String KLARNA = "klarna";
            public static final String MULTIBANCO = "multibanco";
            public static final String P24 = "p24";
            public static final String SEPA_DEBIT = "sepa_debit";
            public static final String SOFORT = "sofort";
            public static final String THREE_D_SECURE = "three_d_secure";
            public static final String UNKNOWN = "unknown";
            public static final String WECHAT = "wechat";

            private Companion() {
            }
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u001b\n\u0002\b\u0002\b\u0002\u0018\u0000 \u00022\u00020\u0001:\u0001\u0002B\u0000¨\u0006\u0003"}, d2 = {"Lcom/stripe/android/model/Source$SourceStatus;", "", "Companion", "stripe_release"}, k = 1, mv = {1, 1, 16})
    @Retention(AnnotationRetention.SOURCE)
    @java.lang.annotation.Retention(RetentionPolicy.SOURCE)
    /* compiled from: Source.kt */
    public @interface SourceStatus {
        public static final String CANCELED = "canceled";
        public static final String CHARGEABLE = "chargeable";
        public static final String CONSUMED = "consumed";
        public static final Companion Companion = Companion.$$INSTANCE;
        public static final String FAILED = "failed";
        public static final String PENDING = "pending";

        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0005\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\t"}, d2 = {"Lcom/stripe/android/model/Source$SourceStatus$Companion;", "", "()V", "CANCELED", "", "CHARGEABLE", "CONSUMED", "FAILED", "PENDING", "stripe_release"}, k = 1, mv = {1, 1, 16})
        /* compiled from: Source.kt */
        public static final class Companion {
            static final /* synthetic */ Companion $$INSTANCE = new Companion();
            public static final String CANCELED = "canceled";
            public static final String CHARGEABLE = "chargeable";
            public static final String CONSUMED = "consumed";
            public static final String FAILED = "failed";
            public static final String PENDING = "pending";

            private Companion() {
            }
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u001b\n\u0002\b\u0002\b\u0002\u0018\u0000 \u00022\u00020\u0001:\u0001\u0002B\u0000¨\u0006\u0003"}, d2 = {"Lcom/stripe/android/model/Source$Usage;", "", "Companion", "stripe_release"}, k = 1, mv = {1, 1, 16})
    @Retention(AnnotationRetention.SOURCE)
    @java.lang.annotation.Retention(RetentionPolicy.SOURCE)
    /* compiled from: Source.kt */
    public @interface Usage {
        public static final Companion Companion = Companion.$$INSTANCE;
        public static final String REUSABLE = "reusable";
        public static final String SINGLE_USE = "single_use";

        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\u0006"}, d2 = {"Lcom/stripe/android/model/Source$Usage$Companion;", "", "()V", "REUSABLE", "", "SINGLE_USE", "stripe_release"}, k = 1, mv = {1, 1, 16})
        /* compiled from: Source.kt */
        public static final class Companion {
            static final /* synthetic */ Companion $$INSTANCE = new Companion();
            public static final String REUSABLE = "reusable";
            public static final String SINGLE_USE = "single_use";

            private Companion() {
            }
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u001b\n\u0002\b\u0002\b\u0002\u0018\u0000 \u00022\u00020\u0001:\u0001\u0002B\u0000¨\u0006\u0003"}, d2 = {"Lcom/stripe/android/model/Source$SourceFlow;", "", "Companion", "stripe_release"}, k = 1, mv = {1, 1, 16})
    @Retention(AnnotationRetention.SOURCE)
    @java.lang.annotation.Retention(RetentionPolicy.SOURCE)
    /* compiled from: Source.kt */
    public @interface SourceFlow {
        public static final String CODE_VERIFICATION = "code_verification";
        public static final Companion Companion = Companion.$$INSTANCE;
        public static final String NONE = "none";
        public static final String RECEIVER = "receiver";
        public static final String REDIRECT = "redirect";

        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0004\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\b"}, d2 = {"Lcom/stripe/android/model/Source$SourceFlow$Companion;", "", "()V", "CODE_VERIFICATION", "", "NONE", "RECEIVER", "REDIRECT", "stripe_release"}, k = 1, mv = {1, 1, 16})
        /* compiled from: Source.kt */
        public static final class Companion {
            static final /* synthetic */ Companion $$INSTANCE = new Companion();
            public static final String CODE_VERIFICATION = "code_verification";
            public static final String NONE = "none";
            public static final String RECEIVER = "receiver";
            public static final String REDIRECT = "redirect";

            private Companion() {
            }
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0010\n\u0002\u0010\"\n\u0002\b*\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\b\b\u0018\u00002\u00020\u0001BÁ\u0001\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\b\u0010\u0004\u001a\u0004\u0018\u00010\u0003\u0012\b\u0010\u0005\u001a\u0004\u0018\u00010\u0003\u0012\b\u0010\u0006\u001a\u0004\u0018\u00010\u0003\u0012\b\u0010\u0007\u001a\u0004\u0018\u00010\u0003\u0012\b\u0010\b\u001a\u0004\u0018\u00010\u0003\u0012\b\u0010\t\u001a\u0004\u0018\u00010\u0003\u0012\b\u0010\n\u001a\u0004\u0018\u00010\u0003\u0012\b\u0010\u000b\u001a\u0004\u0018\u00010\u0003\u0012\b\u0010\f\u001a\u0004\u0018\u00010\u0003\u0012\b\u0010\r\u001a\u0004\u0018\u00010\u0003\u0012\b\u0010\u000e\u001a\u0004\u0018\u00010\u0003\u0012\b\u0010\u000f\u001a\u0004\u0018\u00010\u0003\u0012\b\u0010\u0010\u001a\u0004\u0018\u00010\u0003\u0012\b\u0010\u0011\u001a\u0004\u0018\u00010\u0003\u0012\b\u0010\u0012\u001a\u0004\u0018\u00010\u0003\u0012\f\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u00030\u0014\u0012\f\u0010\u0015\u001a\b\u0012\u0004\u0012\u00020\u00030\u0014¢\u0006\u0002\u0010\u0016J\u000b\u0010+\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000b\u0010,\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000b\u0010-\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000b\u0010.\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000b\u0010/\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000b\u00100\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000b\u00101\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000b\u00102\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000f\u00103\u001a\b\u0012\u0004\u0012\u00020\u00030\u0014HÆ\u0003J\u000f\u00104\u001a\b\u0012\u0004\u0012\u00020\u00030\u0014HÆ\u0003J\u000b\u00105\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000b\u00106\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000b\u00107\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000b\u00108\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000b\u00109\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000b\u0010:\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000b\u0010;\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000b\u0010<\u001a\u0004\u0018\u00010\u0003HÆ\u0003Jé\u0001\u0010=\u001a\u00020\u00002\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u0007\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\t\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\n\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u000b\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\f\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\r\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u000e\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u000f\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u0010\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u0011\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u0012\u001a\u0004\u0018\u00010\u00032\u000e\b\u0002\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u00030\u00142\u000e\b\u0002\u0010\u0015\u001a\b\u0012\u0004\u0012\u00020\u00030\u0014HÆ\u0001J\t\u0010>\u001a\u00020?HÖ\u0001J\u0013\u0010@\u001a\u00020A2\b\u0010B\u001a\u0004\u0018\u00010CHÖ\u0003J\t\u0010D\u001a\u00020?HÖ\u0001J\t\u0010E\u001a\u00020\u0003HÖ\u0001J\u0019\u0010F\u001a\u00020G2\u0006\u0010H\u001a\u00020I2\u0006\u0010J\u001a\u00020?HÖ\u0001R\u0013\u0010\u0006\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0017\u0010\u0018R\u0017\u0010\u0015\u001a\b\u0012\u0004\u0012\u00020\u00030\u0014¢\u0006\b\n\u0000\u001a\u0004\b\u0019\u0010\u001aR\u0013\u0010\u0002\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u001b\u0010\u0018R\u0013\u0010\u0004\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u001c\u0010\u0018R\u0013\u0010\u000b\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u001d\u0010\u0018R\u0013\u0010\f\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u001e\u0010\u0018R\u0013\u0010\r\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u001f\u0010\u0018R\u0013\u0010\u000e\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b \u0010\u0018R\u0013\u0010\u0007\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b!\u0010\u0018R\u0013\u0010\b\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\"\u0010\u0018R\u0013\u0010\t\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b#\u0010\u0018R\u0013\u0010\n\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b$\u0010\u0018R\u0013\u0010\u000f\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b%\u0010\u0018R\u0013\u0010\u0010\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b&\u0010\u0018R\u0013\u0010\u0011\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b'\u0010\u0018R\u0013\u0010\u0012\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b(\u0010\u0018R\u0017\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u00030\u0014¢\u0006\b\n\u0000\u001a\u0004\b)\u0010\u001aR\u0013\u0010\u0005\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b*\u0010\u0018¨\u0006K"}, d2 = {"Lcom/stripe/android/model/Source$Klarna;", "Lcom/stripe/android/model/StripeModel;", "firstName", "", "lastName", "purchaseCountry", "clientToken", "payNowAssetUrlsDescriptive", "payNowAssetUrlsStandard", "payNowName", "payNowRedirectUrl", "payLaterAssetUrlsDescriptive", "payLaterAssetUrlsStandard", "payLaterName", "payLaterRedirectUrl", "payOverTimeAssetUrlsDescriptive", "payOverTimeAssetUrlsStandard", "payOverTimeName", "payOverTimeRedirectUrl", "paymentMethodCategories", "", "customPaymentMethods", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;Ljava/util/Set;)V", "getClientToken", "()Ljava/lang/String;", "getCustomPaymentMethods", "()Ljava/util/Set;", "getFirstName", "getLastName", "getPayLaterAssetUrlsDescriptive", "getPayLaterAssetUrlsStandard", "getPayLaterName", "getPayLaterRedirectUrl", "getPayNowAssetUrlsDescriptive", "getPayNowAssetUrlsStandard", "getPayNowName", "getPayNowRedirectUrl", "getPayOverTimeAssetUrlsDescriptive", "getPayOverTimeAssetUrlsStandard", "getPayOverTimeName", "getPayOverTimeRedirectUrl", "getPaymentMethodCategories", "getPurchaseCountry", "component1", "component10", "component11", "component12", "component13", "component14", "component15", "component16", "component17", "component18", "component2", "component3", "component4", "component5", "component6", "component7", "component8", "component9", "copy", "describeContents", "", "equals", "", "other", "", "hashCode", "toString", "writeToParcel", "", "parcel", "Landroid/os/Parcel;", "flags", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: Source.kt */
    public static final class Klarna implements StripeModel {
        public static final Parcelable.Creator CREATOR = new Creator();
        private final String clientToken;
        private final Set<String> customPaymentMethods;
        private final String firstName;
        private final String lastName;
        private final String payLaterAssetUrlsDescriptive;
        private final String payLaterAssetUrlsStandard;
        private final String payLaterName;
        private final String payLaterRedirectUrl;
        private final String payNowAssetUrlsDescriptive;
        private final String payNowAssetUrlsStandard;
        private final String payNowName;
        private final String payNowRedirectUrl;
        private final String payOverTimeAssetUrlsDescriptive;
        private final String payOverTimeAssetUrlsStandard;
        private final String payOverTimeName;
        private final String payOverTimeRedirectUrl;
        private final Set<String> paymentMethodCategories;
        private final String purchaseCountry;

        @Metadata(bv = {1, 0, 3}, k = 3, mv = {1, 1, 16})
        public static class Creator implements Parcelable.Creator {
            public final Object createFromParcel(Parcel parcel) {
                String str;
                Intrinsics.checkParameterIsNotNull(parcel, "in");
                String readString = parcel.readString();
                String readString2 = parcel.readString();
                String readString3 = parcel.readString();
                String readString4 = parcel.readString();
                String readString5 = parcel.readString();
                String readString6 = parcel.readString();
                String readString7 = parcel.readString();
                String readString8 = parcel.readString();
                String readString9 = parcel.readString();
                String readString10 = parcel.readString();
                String readString11 = parcel.readString();
                String readString12 = parcel.readString();
                String readString13 = parcel.readString();
                String readString14 = parcel.readString();
                String readString15 = parcel.readString();
                String readString16 = parcel.readString();
                int readInt = parcel.readInt();
                String str2 = readString14;
                LinkedHashSet linkedHashSet = new LinkedHashSet(readInt);
                while (true) {
                    str = readString13;
                    if (readInt == 0) {
                        break;
                    }
                    linkedHashSet.add(parcel.readString());
                    readInt--;
                    readString13 = str;
                }
                int readInt2 = parcel.readInt();
                LinkedHashSet linkedHashSet2 = new LinkedHashSet(readInt2);
                while (true) {
                    LinkedHashSet linkedHashSet3 = linkedHashSet;
                    if (readInt2 != 0) {
                        linkedHashSet2.add(parcel.readString());
                        readInt2--;
                        linkedHashSet = linkedHashSet3;
                    } else {
                        return new Klarna(readString, readString2, readString3, readString4, readString5, readString6, readString7, readString8, readString9, readString10, readString11, readString12, str, str2, readString15, readString16, linkedHashSet3, linkedHashSet2);
                    }
                }
            }

            public final Object[] newArray(int i) {
                return new Klarna[i];
            }
        }

        public static /* synthetic */ Klarna copy$default(Klarna klarna, String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, String str9, String str10, String str11, String str12, String str13, String str14, String str15, String str16, Set set, Set set2, int i, Object obj) {
            Klarna klarna2 = klarna;
            int i2 = i;
            return klarna.copy((i2 & 1) != 0 ? klarna2.firstName : str, (i2 & 2) != 0 ? klarna2.lastName : str2, (i2 & 4) != 0 ? klarna2.purchaseCountry : str3, (i2 & 8) != 0 ? klarna2.clientToken : str4, (i2 & 16) != 0 ? klarna2.payNowAssetUrlsDescriptive : str5, (i2 & 32) != 0 ? klarna2.payNowAssetUrlsStandard : str6, (i2 & 64) != 0 ? klarna2.payNowName : str7, (i2 & 128) != 0 ? klarna2.payNowRedirectUrl : str8, (i2 & 256) != 0 ? klarna2.payLaterAssetUrlsDescriptive : str9, (i2 & 512) != 0 ? klarna2.payLaterAssetUrlsStandard : str10, (i2 & 1024) != 0 ? klarna2.payLaterName : str11, (i2 & 2048) != 0 ? klarna2.payLaterRedirectUrl : str12, (i2 & 4096) != 0 ? klarna2.payOverTimeAssetUrlsDescriptive : str13, (i2 & 8192) != 0 ? klarna2.payOverTimeAssetUrlsStandard : str14, (i2 & 16384) != 0 ? klarna2.payOverTimeName : str15, (i2 & 32768) != 0 ? klarna2.payOverTimeRedirectUrl : str16, (i2 & 65536) != 0 ? klarna2.paymentMethodCategories : set, (i2 & 131072) != 0 ? klarna2.customPaymentMethods : set2);
        }

        public final String component1() {
            return this.firstName;
        }

        public final String component10() {
            return this.payLaterAssetUrlsStandard;
        }

        public final String component11() {
            return this.payLaterName;
        }

        public final String component12() {
            return this.payLaterRedirectUrl;
        }

        public final String component13() {
            return this.payOverTimeAssetUrlsDescriptive;
        }

        public final String component14() {
            return this.payOverTimeAssetUrlsStandard;
        }

        public final String component15() {
            return this.payOverTimeName;
        }

        public final String component16() {
            return this.payOverTimeRedirectUrl;
        }

        public final Set<String> component17() {
            return this.paymentMethodCategories;
        }

        public final Set<String> component18() {
            return this.customPaymentMethods;
        }

        public final String component2() {
            return this.lastName;
        }

        public final String component3() {
            return this.purchaseCountry;
        }

        public final String component4() {
            return this.clientToken;
        }

        public final String component5() {
            return this.payNowAssetUrlsDescriptive;
        }

        public final String component6() {
            return this.payNowAssetUrlsStandard;
        }

        public final String component7() {
            return this.payNowName;
        }

        public final String component8() {
            return this.payNowRedirectUrl;
        }

        public final String component9() {
            return this.payLaterAssetUrlsDescriptive;
        }

        public final Klarna copy(String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, String str9, String str10, String str11, String str12, String str13, String str14, String str15, String str16, Set<String> set, Set<String> set2) {
            String str17 = str;
            Intrinsics.checkParameterIsNotNull(set, "paymentMethodCategories");
            Intrinsics.checkParameterIsNotNull(set2, "customPaymentMethods");
            return new Klarna(str, str2, str3, str4, str5, str6, str7, str8, str9, str10, str11, str12, str13, str14, str15, str16, set, set2);
        }

        public int describeContents() {
            return 0;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Klarna)) {
                return false;
            }
            Klarna klarna = (Klarna) obj;
            return Intrinsics.areEqual((Object) this.firstName, (Object) klarna.firstName) && Intrinsics.areEqual((Object) this.lastName, (Object) klarna.lastName) && Intrinsics.areEqual((Object) this.purchaseCountry, (Object) klarna.purchaseCountry) && Intrinsics.areEqual((Object) this.clientToken, (Object) klarna.clientToken) && Intrinsics.areEqual((Object) this.payNowAssetUrlsDescriptive, (Object) klarna.payNowAssetUrlsDescriptive) && Intrinsics.areEqual((Object) this.payNowAssetUrlsStandard, (Object) klarna.payNowAssetUrlsStandard) && Intrinsics.areEqual((Object) this.payNowName, (Object) klarna.payNowName) && Intrinsics.areEqual((Object) this.payNowRedirectUrl, (Object) klarna.payNowRedirectUrl) && Intrinsics.areEqual((Object) this.payLaterAssetUrlsDescriptive, (Object) klarna.payLaterAssetUrlsDescriptive) && Intrinsics.areEqual((Object) this.payLaterAssetUrlsStandard, (Object) klarna.payLaterAssetUrlsStandard) && Intrinsics.areEqual((Object) this.payLaterName, (Object) klarna.payLaterName) && Intrinsics.areEqual((Object) this.payLaterRedirectUrl, (Object) klarna.payLaterRedirectUrl) && Intrinsics.areEqual((Object) this.payOverTimeAssetUrlsDescriptive, (Object) klarna.payOverTimeAssetUrlsDescriptive) && Intrinsics.areEqual((Object) this.payOverTimeAssetUrlsStandard, (Object) klarna.payOverTimeAssetUrlsStandard) && Intrinsics.areEqual((Object) this.payOverTimeName, (Object) klarna.payOverTimeName) && Intrinsics.areEqual((Object) this.payOverTimeRedirectUrl, (Object) klarna.payOverTimeRedirectUrl) && Intrinsics.areEqual((Object) this.paymentMethodCategories, (Object) klarna.paymentMethodCategories) && Intrinsics.areEqual((Object) this.customPaymentMethods, (Object) klarna.customPaymentMethods);
        }

        public int hashCode() {
            String str = this.firstName;
            int i = 0;
            int hashCode = (str != null ? str.hashCode() : 0) * 31;
            String str2 = this.lastName;
            int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
            String str3 = this.purchaseCountry;
            int hashCode3 = (hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31;
            String str4 = this.clientToken;
            int hashCode4 = (hashCode3 + (str4 != null ? str4.hashCode() : 0)) * 31;
            String str5 = this.payNowAssetUrlsDescriptive;
            int hashCode5 = (hashCode4 + (str5 != null ? str5.hashCode() : 0)) * 31;
            String str6 = this.payNowAssetUrlsStandard;
            int hashCode6 = (hashCode5 + (str6 != null ? str6.hashCode() : 0)) * 31;
            String str7 = this.payNowName;
            int hashCode7 = (hashCode6 + (str7 != null ? str7.hashCode() : 0)) * 31;
            String str8 = this.payNowRedirectUrl;
            int hashCode8 = (hashCode7 + (str8 != null ? str8.hashCode() : 0)) * 31;
            String str9 = this.payLaterAssetUrlsDescriptive;
            int hashCode9 = (hashCode8 + (str9 != null ? str9.hashCode() : 0)) * 31;
            String str10 = this.payLaterAssetUrlsStandard;
            int hashCode10 = (hashCode9 + (str10 != null ? str10.hashCode() : 0)) * 31;
            String str11 = this.payLaterName;
            int hashCode11 = (hashCode10 + (str11 != null ? str11.hashCode() : 0)) * 31;
            String str12 = this.payLaterRedirectUrl;
            int hashCode12 = (hashCode11 + (str12 != null ? str12.hashCode() : 0)) * 31;
            String str13 = this.payOverTimeAssetUrlsDescriptive;
            int hashCode13 = (hashCode12 + (str13 != null ? str13.hashCode() : 0)) * 31;
            String str14 = this.payOverTimeAssetUrlsStandard;
            int hashCode14 = (hashCode13 + (str14 != null ? str14.hashCode() : 0)) * 31;
            String str15 = this.payOverTimeName;
            int hashCode15 = (hashCode14 + (str15 != null ? str15.hashCode() : 0)) * 31;
            String str16 = this.payOverTimeRedirectUrl;
            int hashCode16 = (hashCode15 + (str16 != null ? str16.hashCode() : 0)) * 31;
            Set<String> set = this.paymentMethodCategories;
            int hashCode17 = (hashCode16 + (set != null ? set.hashCode() : 0)) * 31;
            Set<String> set2 = this.customPaymentMethods;
            if (set2 != null) {
                i = set2.hashCode();
            }
            return hashCode17 + i;
        }

        public String toString() {
            return "Klarna(firstName=" + this.firstName + ", lastName=" + this.lastName + ", purchaseCountry=" + this.purchaseCountry + ", clientToken=" + this.clientToken + ", payNowAssetUrlsDescriptive=" + this.payNowAssetUrlsDescriptive + ", payNowAssetUrlsStandard=" + this.payNowAssetUrlsStandard + ", payNowName=" + this.payNowName + ", payNowRedirectUrl=" + this.payNowRedirectUrl + ", payLaterAssetUrlsDescriptive=" + this.payLaterAssetUrlsDescriptive + ", payLaterAssetUrlsStandard=" + this.payLaterAssetUrlsStandard + ", payLaterName=" + this.payLaterName + ", payLaterRedirectUrl=" + this.payLaterRedirectUrl + ", payOverTimeAssetUrlsDescriptive=" + this.payOverTimeAssetUrlsDescriptive + ", payOverTimeAssetUrlsStandard=" + this.payOverTimeAssetUrlsStandard + ", payOverTimeName=" + this.payOverTimeName + ", payOverTimeRedirectUrl=" + this.payOverTimeRedirectUrl + ", paymentMethodCategories=" + this.paymentMethodCategories + ", customPaymentMethods=" + this.customPaymentMethods + ")";
        }

        public void writeToParcel(Parcel parcel, int i) {
            Intrinsics.checkParameterIsNotNull(parcel, "parcel");
            parcel.writeString(this.firstName);
            parcel.writeString(this.lastName);
            parcel.writeString(this.purchaseCountry);
            parcel.writeString(this.clientToken);
            parcel.writeString(this.payNowAssetUrlsDescriptive);
            parcel.writeString(this.payNowAssetUrlsStandard);
            parcel.writeString(this.payNowName);
            parcel.writeString(this.payNowRedirectUrl);
            parcel.writeString(this.payLaterAssetUrlsDescriptive);
            parcel.writeString(this.payLaterAssetUrlsStandard);
            parcel.writeString(this.payLaterName);
            parcel.writeString(this.payLaterRedirectUrl);
            parcel.writeString(this.payOverTimeAssetUrlsDescriptive);
            parcel.writeString(this.payOverTimeAssetUrlsStandard);
            parcel.writeString(this.payOverTimeName);
            parcel.writeString(this.payOverTimeRedirectUrl);
            Set<String> set = this.paymentMethodCategories;
            parcel.writeInt(set.size());
            for (String writeString : set) {
                parcel.writeString(writeString);
            }
            Set<String> set2 = this.customPaymentMethods;
            parcel.writeInt(set2.size());
            for (String writeString2 : set2) {
                parcel.writeString(writeString2);
            }
        }

        public Klarna(String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, String str9, String str10, String str11, String str12, String str13, String str14, String str15, String str16, Set<String> set, Set<String> set2) {
            Set<String> set3 = set;
            Set<String> set4 = set2;
            Intrinsics.checkParameterIsNotNull(set3, "paymentMethodCategories");
            Intrinsics.checkParameterIsNotNull(set4, "customPaymentMethods");
            this.firstName = str;
            this.lastName = str2;
            this.purchaseCountry = str3;
            this.clientToken = str4;
            this.payNowAssetUrlsDescriptive = str5;
            this.payNowAssetUrlsStandard = str6;
            this.payNowName = str7;
            this.payNowRedirectUrl = str8;
            this.payLaterAssetUrlsDescriptive = str9;
            this.payLaterAssetUrlsStandard = str10;
            this.payLaterName = str11;
            this.payLaterRedirectUrl = str12;
            this.payOverTimeAssetUrlsDescriptive = str13;
            this.payOverTimeAssetUrlsStandard = str14;
            this.payOverTimeName = str15;
            this.payOverTimeRedirectUrl = str16;
            this.paymentMethodCategories = set3;
            this.customPaymentMethods = set4;
        }

        public final String getFirstName() {
            return this.firstName;
        }

        public final String getLastName() {
            return this.lastName;
        }

        public final String getPurchaseCountry() {
            return this.purchaseCountry;
        }

        public final String getClientToken() {
            return this.clientToken;
        }

        public final String getPayNowAssetUrlsDescriptive() {
            return this.payNowAssetUrlsDescriptive;
        }

        public final String getPayNowAssetUrlsStandard() {
            return this.payNowAssetUrlsStandard;
        }

        public final String getPayNowName() {
            return this.payNowName;
        }

        public final String getPayNowRedirectUrl() {
            return this.payNowRedirectUrl;
        }

        public final String getPayLaterAssetUrlsDescriptive() {
            return this.payLaterAssetUrlsDescriptive;
        }

        public final String getPayLaterAssetUrlsStandard() {
            return this.payLaterAssetUrlsStandard;
        }

        public final String getPayLaterName() {
            return this.payLaterName;
        }

        public final String getPayLaterRedirectUrl() {
            return this.payLaterRedirectUrl;
        }

        public final String getPayOverTimeAssetUrlsDescriptive() {
            return this.payOverTimeAssetUrlsDescriptive;
        }

        public final String getPayOverTimeAssetUrlsStandard() {
            return this.payOverTimeAssetUrlsStandard;
        }

        public final String getPayOverTimeName() {
            return this.payOverTimeName;
        }

        public final String getPayOverTimeRedirectUrl() {
            return this.payOverTimeRedirectUrl;
        }

        public final Set<String> getPaymentMethodCategories() {
            return this.paymentMethodCategories;
        }

        public final Set<String> getCustomPaymentMethods() {
            return this.customPaymentMethods;
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0012\u0010\u0007\u001a\u00020\u00042\b\u0010\b\u001a\u0004\u0018\u00010\u0004H\u0007J\u0014\u0010\t\u001a\u0004\u0018\u00010\n2\b\u0010\u000b\u001a\u0004\u0018\u00010\fH\u0007R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\r"}, d2 = {"Lcom/stripe/android/model/Source$Companion;", "", "()V", "EURO", "", "OBJECT_TYPE", "USD", "asSourceType", "sourceType", "fromJson", "Lcom/stripe/android/model/Source;", "jsonObject", "Lorg/json/JSONObject;", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: Source.kt */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        @JvmStatic
        public final Source fromJson(JSONObject jSONObject) {
            if (jSONObject != null) {
                return new SourceJsonParser().parse(jSONObject);
            }
            return null;
        }

        @JvmStatic
        public final String asSourceType(String str) {
            if (str != null) {
                switch (str.hashCode()) {
                    case -1920743119:
                        if (str.equals("bancontact")) {
                            return "bancontact";
                        }
                        break;
                    case -1414960566:
                        if (str.equals("alipay")) {
                            return "alipay";
                        }
                        break;
                    case -896955097:
                        if (str.equals("sofort")) {
                            return "sofort";
                        }
                        break;
                    case -825238221:
                        if (str.equals("three_d_secure")) {
                            return "three_d_secure";
                        }
                        break;
                    case -791770330:
                        if (str.equals("wechat")) {
                            return "wechat";
                        }
                        break;
                    case -284840886:
                        boolean equals = str.equals("unknown");
                        break;
                    case 100648:
                        if (str.equals("eps")) {
                            return "eps";
                        }
                        break;
                    case 109234:
                        if (str.equals("p24")) {
                            return "p24";
                        }
                        break;
                    case 3046160:
                        if (str.equals("card")) {
                            return "card";
                        }
                        break;
                    case 38358441:
                        if (str.equals("giropay")) {
                            return "giropay";
                        }
                        break;
                    case 100048981:
                        if (str.equals("ideal")) {
                            return "ideal";
                        }
                        break;
                    case 1251821346:
                        if (str.equals("multibanco")) {
                            return "multibanco";
                        }
                        break;
                    case 1636477296:
                        if (str.equals("sepa_debit")) {
                            return "sepa_debit";
                        }
                        break;
                }
            }
            return "unknown";
        }
    }
}
