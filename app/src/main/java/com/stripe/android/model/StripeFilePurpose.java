package com.stripe.android.model;

import java.util.NoSuchElementException;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\u000e\n\u0002\b\f\b\u0001\u0018\u0000 \u000e2\b\u0012\u0004\u0012\u00020\u00000\u0001:\u0001\u000eB\u000f\b\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004R\u0014\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006j\u0002\b\u0007j\u0002\b\bj\u0002\b\tj\u0002\b\nj\u0002\b\u000bj\u0002\b\fj\u0002\b\r¨\u0006\u000f"}, d2 = {"Lcom/stripe/android/model/StripeFilePurpose;", "", "code", "", "(Ljava/lang/String;ILjava/lang/String;)V", "getCode$stripe_release", "()Ljava/lang/String;", "BusinessIcon", "BusinessLogo", "CustomerSignature", "DisputeEvidence", "IdentityDocument", "PciDocument", "TaxDocumentUserUpload", "Companion", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: StripeFilePurpose.kt */
public enum StripeFilePurpose {
    BusinessIcon("business_icon"),
    BusinessLogo("business_logo"),
    CustomerSignature("customer_signature"),
    DisputeEvidence("dispute_evidence"),
    IdentityDocument("identity_document"),
    PciDocument("pci_document"),
    TaxDocumentUserUpload("tax_document_user_upload");
    
    public static final Companion Companion = null;
    private final String code;

    private StripeFilePurpose(String str) {
        this.code = str;
    }

    public final String getCode$stripe_release() {
        return this.code;
    }

    static {
        Companion = new Companion((DefaultConstructorMarker) null);
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0012\u0010\u0003\u001a\u0004\u0018\u00010\u00042\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006¨\u0006\u0007"}, d2 = {"Lcom/stripe/android/model/StripeFilePurpose$Companion;", "", "()V", "fromCode", "Lcom/stripe/android/model/StripeFilePurpose;", "code", "", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: StripeFilePurpose.kt */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        public final StripeFilePurpose fromCode(String str) {
            for (StripeFilePurpose stripeFilePurpose : StripeFilePurpose.values()) {
                if (Intrinsics.areEqual((Object) stripeFilePurpose.getCode$stripe_release(), (Object) str)) {
                    return stripeFilePurpose;
                }
            }
            throw new NoSuchElementException("Array contains no element matching the predicate.");
        }
    }
}
