package com.stripe.android.model;

import android.os.Parcel;
import android.os.Parcelable;
import com.stripe.android.model.parsers.TokenJsonParser;
import java.lang.annotation.RetentionPolicy;
import java.util.Date;
import kotlin.Metadata;
import kotlin.Result;
import kotlin.ResultKt;
import kotlin.annotation.AnnotationRetention;
import kotlin.annotation.Retention;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.internal.Intrinsics;
import org.json.JSONObject;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000P\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0016\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\b\b\u0018\u0000 02\u00020\u00012\u00020\u0002:\u000201BG\b\u0000\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\t\u0012\n\b\u0002\u0010\u000b\u001a\u0004\u0018\u00010\f\u0012\n\b\u0002\u0010\r\u001a\u0004\u0018\u00010\u000e¢\u0006\u0002\u0010\u000fJ\t\u0010\u001c\u001a\u00020\u0004HÆ\u0003J\t\u0010\u001d\u001a\u00020\u0004HÆ\u0003J\t\u0010\u001e\u001a\u00020\u0007HÆ\u0003J\t\u0010\u001f\u001a\u00020\tHÆ\u0003J\t\u0010 \u001a\u00020\tHÆ\u0003J\u000b\u0010!\u001a\u0004\u0018\u00010\fHÆ\u0003J\u000b\u0010\"\u001a\u0004\u0018\u00010\u000eHÆ\u0003JS\u0010#\u001a\u00020\u00002\b\b\u0002\u0010\u0003\u001a\u00020\u00042\b\b\u0002\u0010\u0005\u001a\u00020\u00042\b\b\u0002\u0010\u0006\u001a\u00020\u00072\b\b\u0002\u0010\b\u001a\u00020\t2\b\b\u0002\u0010\n\u001a\u00020\t2\n\b\u0002\u0010\u000b\u001a\u0004\u0018\u00010\f2\n\b\u0002\u0010\r\u001a\u0004\u0018\u00010\u000eHÆ\u0001J\t\u0010$\u001a\u00020%HÖ\u0001J\u0013\u0010&\u001a\u00020\t2\b\u0010'\u001a\u0004\u0018\u00010(HÖ\u0003J\t\u0010)\u001a\u00020%HÖ\u0001J\t\u0010*\u001a\u00020\u0004HÖ\u0001J\u0019\u0010+\u001a\u00020,2\u0006\u0010-\u001a\u00020.2\u0006\u0010/\u001a\u00020%HÖ\u0001R\u0013\u0010\u000b\u001a\u0004\u0018\u00010\f¢\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u0011R\u0013\u0010\r\u001a\u0004\u0018\u00010\u000e¢\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u0013R\u0011\u0010\u0006\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\b\u0014\u0010\u0015R\u0014\u0010\u0003\u001a\u00020\u0004X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0016\u0010\u0017R\u0011\u0010\b\u001a\u00020\t¢\u0006\b\n\u0000\u001a\u0004\b\u0018\u0010\u0019R\u0011\u0010\u0005\u001a\u00020\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u001a\u0010\u0017R\u0011\u0010\n\u001a\u00020\t¢\u0006\b\n\u0000\u001a\u0004\b\u001b\u0010\u0019¨\u00062"}, d2 = {"Lcom/stripe/android/model/Token;", "Lcom/stripe/android/model/StripeModel;", "Lcom/stripe/android/model/StripePaymentSource;", "id", "", "type", "created", "Ljava/util/Date;", "livemode", "", "used", "bankAccount", "Lcom/stripe/android/model/BankAccount;", "card", "Lcom/stripe/android/model/Card;", "(Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;ZZLcom/stripe/android/model/BankAccount;Lcom/stripe/android/model/Card;)V", "getBankAccount", "()Lcom/stripe/android/model/BankAccount;", "getCard", "()Lcom/stripe/android/model/Card;", "getCreated", "()Ljava/util/Date;", "getId", "()Ljava/lang/String;", "getLivemode", "()Z", "getType", "getUsed", "component1", "component2", "component3", "component4", "component5", "component6", "component7", "copy", "describeContents", "", "equals", "other", "", "hashCode", "toString", "writeToParcel", "", "parcel", "Landroid/os/Parcel;", "flags", "Companion", "TokenType", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: Token.kt */
public final class Token implements StripeModel, StripePaymentSource {
    public static final Parcelable.Creator CREATOR = new Creator();
    public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
    private final BankAccount bankAccount;
    private final Card card;
    private final Date created;
    private final String id;
    private final boolean livemode;
    private final String type;
    private final boolean used;

    @Metadata(bv = {1, 0, 3}, k = 3, mv = {1, 1, 16})
    public static class Creator implements Parcelable.Creator {
        public final Object createFromParcel(Parcel parcel) {
            Intrinsics.checkParameterIsNotNull(parcel, "in");
            String readString = parcel.readString();
            String readString2 = parcel.readString();
            Date date = (Date) parcel.readSerializable();
            boolean z = false;
            boolean z2 = parcel.readInt() != 0;
            if (parcel.readInt() != 0) {
                z = true;
            }
            return new Token(readString, readString2, date, z2, z, parcel.readInt() != 0 ? (BankAccount) BankAccount.CREATOR.createFromParcel(parcel) : null, parcel.readInt() != 0 ? (Card) Card.CREATOR.createFromParcel(parcel) : null);
        }

        public final Object[] newArray(int i) {
            return new Token[i];
        }
    }

    public static /* synthetic */ Token copy$default(Token token, String str, String str2, Date date, boolean z, boolean z2, BankAccount bankAccount2, Card card2, int i, Object obj) {
        if ((i & 1) != 0) {
            str = token.getId();
        }
        if ((i & 2) != 0) {
            str2 = token.type;
        }
        String str3 = str2;
        if ((i & 4) != 0) {
            date = token.created;
        }
        Date date2 = date;
        if ((i & 8) != 0) {
            z = token.livemode;
        }
        boolean z3 = z;
        if ((i & 16) != 0) {
            z2 = token.used;
        }
        boolean z4 = z2;
        if ((i & 32) != 0) {
            bankAccount2 = token.bankAccount;
        }
        BankAccount bankAccount3 = bankAccount2;
        if ((i & 64) != 0) {
            card2 = token.card;
        }
        return token.copy(str, str3, date2, z3, z4, bankAccount3, card2);
    }

    @JvmStatic
    public static final Token fromJson(JSONObject jSONObject) {
        return Companion.fromJson(jSONObject);
    }

    @JvmStatic
    public static final Token fromString(String str) {
        return Companion.fromString(str);
    }

    public final String component1() {
        return getId();
    }

    public final String component2() {
        return this.type;
    }

    public final Date component3() {
        return this.created;
    }

    public final boolean component4() {
        return this.livemode;
    }

    public final boolean component5() {
        return this.used;
    }

    public final BankAccount component6() {
        return this.bankAccount;
    }

    public final Card component7() {
        return this.card;
    }

    public final Token copy(String str, String str2, Date date, boolean z, boolean z2, BankAccount bankAccount2, Card card2) {
        Intrinsics.checkParameterIsNotNull(str, "id");
        Intrinsics.checkParameterIsNotNull(str2, "type");
        Intrinsics.checkParameterIsNotNull(date, "created");
        return new Token(str, str2, date, z, z2, bankAccount2, card2);
    }

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Token)) {
            return false;
        }
        Token token = (Token) obj;
        return Intrinsics.areEqual((Object) getId(), (Object) token.getId()) && Intrinsics.areEqual((Object) this.type, (Object) token.type) && Intrinsics.areEqual((Object) this.created, (Object) token.created) && this.livemode == token.livemode && this.used == token.used && Intrinsics.areEqual((Object) this.bankAccount, (Object) token.bankAccount) && Intrinsics.areEqual((Object) this.card, (Object) token.card);
    }

    public int hashCode() {
        String id2 = getId();
        int i = 0;
        int hashCode = (id2 != null ? id2.hashCode() : 0) * 31;
        String str = this.type;
        int hashCode2 = (hashCode + (str != null ? str.hashCode() : 0)) * 31;
        Date date = this.created;
        int hashCode3 = (hashCode2 + (date != null ? date.hashCode() : 0)) * 31;
        boolean z = this.livemode;
        boolean z2 = true;
        if (z) {
            z = true;
        }
        int i2 = (hashCode3 + (z ? 1 : 0)) * 31;
        boolean z3 = this.used;
        if (!z3) {
            z2 = z3;
        }
        int i3 = (i2 + (z2 ? 1 : 0)) * 31;
        BankAccount bankAccount2 = this.bankAccount;
        int hashCode4 = (i3 + (bankAccount2 != null ? bankAccount2.hashCode() : 0)) * 31;
        Card card2 = this.card;
        if (card2 != null) {
            i = card2.hashCode();
        }
        return hashCode4 + i;
    }

    public String toString() {
        return "Token(id=" + getId() + ", type=" + this.type + ", created=" + this.created + ", livemode=" + this.livemode + ", used=" + this.used + ", bankAccount=" + this.bankAccount + ", card=" + this.card + ")";
    }

    public void writeToParcel(Parcel parcel, int i) {
        Intrinsics.checkParameterIsNotNull(parcel, "parcel");
        parcel.writeString(this.id);
        parcel.writeString(this.type);
        parcel.writeSerializable(this.created);
        parcel.writeInt(this.livemode ? 1 : 0);
        parcel.writeInt(this.used ? 1 : 0);
        BankAccount bankAccount2 = this.bankAccount;
        if (bankAccount2 != null) {
            parcel.writeInt(1);
            bankAccount2.writeToParcel(parcel, 0);
        } else {
            parcel.writeInt(0);
        }
        Card card2 = this.card;
        if (card2 != null) {
            parcel.writeInt(1);
            card2.writeToParcel(parcel, 0);
            return;
        }
        parcel.writeInt(0);
    }

    public Token(String str, String str2, Date date, boolean z, boolean z2, BankAccount bankAccount2, Card card2) {
        Intrinsics.checkParameterIsNotNull(str, "id");
        Intrinsics.checkParameterIsNotNull(str2, "type");
        Intrinsics.checkParameterIsNotNull(date, "created");
        this.id = str;
        this.type = str2;
        this.created = date;
        this.livemode = z;
        this.used = z2;
        this.bankAccount = bankAccount2;
        this.card = card2;
    }

    public String getId() {
        return this.id;
    }

    public final String getType() {
        return this.type;
    }

    public final Date getCreated() {
        return this.created;
    }

    public final boolean getLivemode() {
        return this.livemode;
    }

    public final boolean getUsed() {
        return this.used;
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ Token(String str, String str2, Date date, boolean z, boolean z2, BankAccount bankAccount2, Card card2, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(str, str2, date, z, z2, (i & 32) != 0 ? null : bankAccount2, (i & 64) != 0 ? null : card2);
    }

    public final BankAccount getBankAccount() {
        return this.bankAccount;
    }

    public final Card getCard() {
        return this.card;
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u001b\n\u0002\b\u0002\b\u0002\u0018\u0000 \u00022\u00020\u0001:\u0001\u0002B\u0000¨\u0006\u0003"}, d2 = {"Lcom/stripe/android/model/Token$TokenType;", "", "Companion", "stripe_release"}, k = 1, mv = {1, 1, 16})
    @Retention(AnnotationRetention.SOURCE)
    @java.lang.annotation.Retention(RetentionPolicy.SOURCE)
    /* compiled from: Token.kt */
    public @interface TokenType {
        public static final String ACCOUNT = "account";
        public static final String BANK_ACCOUNT = "bank_account";
        public static final String CARD = "card";
        public static final String CVC_UPDATE = "cvc_update";
        public static final Companion Companion = Companion.$$INSTANCE;
        public static final String PERSON = "person";
        public static final String PII = "pii";

        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0006\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\n"}, d2 = {"Lcom/stripe/android/model/Token$TokenType$Companion;", "", "()V", "ACCOUNT", "", "BANK_ACCOUNT", "CARD", "CVC_UPDATE", "PERSON", "PII", "stripe_release"}, k = 1, mv = {1, 1, 16})
        /* compiled from: Token.kt */
        public static final class Companion {
            static final /* synthetic */ Companion $$INSTANCE = new Companion();
            public static final String ACCOUNT = "account";
            public static final String BANK_ACCOUNT = "bank_account";
            public static final String CARD = "card";
            public static final String CVC_UPDATE = "cvc_update";
            public static final String PERSON = "person";
            public static final String PII = "pii";

            private Companion() {
            }
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0014\u0010\u0003\u001a\u0004\u0018\u00010\u00042\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006H\u0007J\u0014\u0010\u0007\u001a\u0004\u0018\u00010\u00042\b\u0010\b\u001a\u0004\u0018\u00010\tH\u0007¨\u0006\n"}, d2 = {"Lcom/stripe/android/model/Token$Companion;", "", "()V", "fromJson", "Lcom/stripe/android/model/Token;", "jsonObject", "Lorg/json/JSONObject;", "fromString", "jsonString", "", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: Token.kt */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        @JvmStatic
        public final Token fromString(String str) {
            Object obj;
            if (str == null) {
                return null;
            }
            try {
                Result.Companion companion = Result.Companion;
                Companion companion2 = this;
                obj = Result.m4constructorimpl(new JSONObject(str));
            } catch (Throwable th) {
                Result.Companion companion3 = Result.Companion;
                obj = Result.m4constructorimpl(ResultKt.createFailure(th));
            }
            if (Result.m10isFailureimpl(obj)) {
                obj = null;
            }
            JSONObject jSONObject = (JSONObject) obj;
            if (jSONObject != null) {
                return Token.Companion.fromJson(jSONObject);
            }
            return null;
        }

        @JvmStatic
        public final Token fromJson(JSONObject jSONObject) {
            if (jSONObject != null) {
                return new TokenJsonParser().parse(jSONObject);
            }
            return null;
        }
    }
}
