package com.stripe.android.model.parsers;

import com.stripe.android.model.CustomerSource;
import kotlin.Metadata;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\b\u0000\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0003J\u0012\u0010\u0004\u001a\u0004\u0018\u00010\u00022\u0006\u0010\u0005\u001a\u00020\u0006H\u0016¨\u0006\u0007"}, d2 = {"Lcom/stripe/android/model/parsers/CustomerSourceJsonParser;", "Lcom/stripe/android/model/parsers/ModelJsonParser;", "Lcom/stripe/android/model/CustomerSource;", "()V", "parse", "json", "Lorg/json/JSONObject;", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: CustomerSourceJsonParser.kt */
public final class CustomerSourceJsonParser implements ModelJsonParser<CustomerSource> {
    /* JADX WARNING: Removed duplicated region for block: B:14:0x004a  */
    /* JADX WARNING: Removed duplicated region for block: B:16:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.stripe.android.model.CustomerSource parse(org.json.JSONObject r5) {
        /*
            r4 = this;
            java.lang.String r0 = "json"
            kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(r5, r0)
            java.lang.String r0 = "object"
            java.lang.String r0 = com.stripe.android.model.StripeJsonUtils.optString(r5, r0)
            r1 = 0
            if (r0 != 0) goto L_0x000f
            goto L_0x0046
        L_0x000f:
            int r2 = r0.hashCode()
            r3 = -896505829(0xffffffffca90681b, float:-4731917.5)
            if (r2 == r3) goto L_0x0032
            r3 = 3046160(0x2e7b10, float:4.26858E-39)
            if (r2 == r3) goto L_0x001e
            goto L_0x0046
        L_0x001e:
            java.lang.String r2 = "card"
            boolean r0 = r0.equals(r2)
            if (r0 == 0) goto L_0x0046
            com.stripe.android.model.parsers.CardJsonParser r0 = new com.stripe.android.model.parsers.CardJsonParser
            r0.<init>()
            com.stripe.android.model.Card r5 = r0.parse((org.json.JSONObject) r5)
            com.stripe.android.model.StripePaymentSource r5 = (com.stripe.android.model.StripePaymentSource) r5
            goto L_0x0047
        L_0x0032:
            java.lang.String r2 = "source"
            boolean r0 = r0.equals(r2)
            if (r0 == 0) goto L_0x0046
            com.stripe.android.model.parsers.SourceJsonParser r0 = new com.stripe.android.model.parsers.SourceJsonParser
            r0.<init>()
            com.stripe.android.model.Source r5 = r0.parse((org.json.JSONObject) r5)
            com.stripe.android.model.StripePaymentSource r5 = (com.stripe.android.model.StripePaymentSource) r5
            goto L_0x0047
        L_0x0046:
            r5 = r1
        L_0x0047:
            if (r5 != 0) goto L_0x004a
            goto L_0x004f
        L_0x004a:
            com.stripe.android.model.CustomerSource r1 = new com.stripe.android.model.CustomerSource
            r1.<init>(r5)
        L_0x004f:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.stripe.android.model.parsers.CustomerSourceJsonParser.parse(org.json.JSONObject):com.stripe.android.model.CustomerSource");
    }
}
