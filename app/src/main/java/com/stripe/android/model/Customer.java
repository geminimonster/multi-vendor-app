package com.stripe.android.model;

import android.os.Parcel;
import android.os.Parcelable;
import com.stripe.android.model.parsers.CustomerJsonParser;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import kotlin.Metadata;
import kotlin.Result;
import kotlin.ResultKt;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.internal.Intrinsics;
import org.json.JSONObject;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000F\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\b\n\u0002\b\u001b\n\u0002\u0010\u0000\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\b\b\u0018\u0000 32\u00020\u0001:\u00013BO\b\u0000\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\b\u0010\u0004\u001a\u0004\u0018\u00010\u0003\u0012\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u0012\f\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\t0\b\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\b\u0010\f\u001a\u0004\u0018\u00010\r\u0012\b\u0010\u000e\u001a\u0004\u0018\u00010\u0003¢\u0006\u0002\u0010\u000fJ\u000b\u0010\u001d\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000b\u0010\u001e\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000b\u0010\u001f\u001a\u0004\u0018\u00010\u0006HÆ\u0003J\u000f\u0010 \u001a\b\u0012\u0004\u0012\u00020\t0\bHÆ\u0003J\t\u0010!\u001a\u00020\u000bHÆ\u0003J\u0010\u0010\"\u001a\u0004\u0018\u00010\rHÆ\u0003¢\u0006\u0002\u0010\u001aJ\u000b\u0010#\u001a\u0004\u0018\u00010\u0003HÆ\u0003Jd\u0010$\u001a\u00020\u00002\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u00062\u000e\b\u0002\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\t0\b2\b\b\u0002\u0010\n\u001a\u00020\u000b2\n\b\u0002\u0010\f\u001a\u0004\u0018\u00010\r2\n\b\u0002\u0010\u000e\u001a\u0004\u0018\u00010\u0003HÆ\u0001¢\u0006\u0002\u0010%J\t\u0010&\u001a\u00020\rHÖ\u0001J\u0013\u0010'\u001a\u00020\u000b2\b\u0010(\u001a\u0004\u0018\u00010)HÖ\u0003J\u0010\u0010*\u001a\u0004\u0018\u00010\t2\u0006\u0010+\u001a\u00020\u0003J\t\u0010,\u001a\u00020\rHÖ\u0001J\t\u0010-\u001a\u00020\u0003HÖ\u0001J\u0019\u0010.\u001a\u00020/2\u0006\u00100\u001a\u0002012\u0006\u00102\u001a\u00020\rHÖ\u0001R\u0013\u0010\u0004\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u0011R\u0011\u0010\n\u001a\u00020\u000b¢\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u0013R\u0013\u0010\u0002\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0014\u0010\u0011R\u0013\u0010\u0005\u001a\u0004\u0018\u00010\u0006¢\u0006\b\n\u0000\u001a\u0004\b\u0015\u0010\u0016R\u0017\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\t0\b¢\u0006\b\n\u0000\u001a\u0004\b\u0017\u0010\u0018R\u0015\u0010\f\u001a\u0004\u0018\u00010\r¢\u0006\n\n\u0002\u0010\u001b\u001a\u0004\b\u0019\u0010\u001aR\u0013\u0010\u000e\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u001c\u0010\u0011¨\u00064"}, d2 = {"Lcom/stripe/android/model/Customer;", "Lcom/stripe/android/model/StripeModel;", "id", "", "defaultSource", "shippingInformation", "Lcom/stripe/android/model/ShippingInformation;", "sources", "", "Lcom/stripe/android/model/CustomerSource;", "hasMore", "", "totalCount", "", "url", "(Ljava/lang/String;Ljava/lang/String;Lcom/stripe/android/model/ShippingInformation;Ljava/util/List;ZLjava/lang/Integer;Ljava/lang/String;)V", "getDefaultSource", "()Ljava/lang/String;", "getHasMore", "()Z", "getId", "getShippingInformation", "()Lcom/stripe/android/model/ShippingInformation;", "getSources", "()Ljava/util/List;", "getTotalCount", "()Ljava/lang/Integer;", "Ljava/lang/Integer;", "getUrl", "component1", "component2", "component3", "component4", "component5", "component6", "component7", "copy", "(Ljava/lang/String;Ljava/lang/String;Lcom/stripe/android/model/ShippingInformation;Ljava/util/List;ZLjava/lang/Integer;Ljava/lang/String;)Lcom/stripe/android/model/Customer;", "describeContents", "equals", "other", "", "getSourceById", "sourceId", "hashCode", "toString", "writeToParcel", "", "parcel", "Landroid/os/Parcel;", "flags", "Companion", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: Customer.kt */
public final class Customer implements StripeModel {
    public static final Parcelable.Creator CREATOR = new Creator();
    public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
    private final String defaultSource;
    private final boolean hasMore;
    private final String id;
    private final ShippingInformation shippingInformation;
    private final List<CustomerSource> sources;
    private final Integer totalCount;
    private final String url;

    @Metadata(bv = {1, 0, 3}, k = 3, mv = {1, 1, 16})
    public static class Creator implements Parcelable.Creator {
        public final Object createFromParcel(Parcel parcel) {
            Intrinsics.checkParameterIsNotNull(parcel, "in");
            String readString = parcel.readString();
            String readString2 = parcel.readString();
            ShippingInformation shippingInformation = parcel.readInt() != 0 ? (ShippingInformation) ShippingInformation.CREATOR.createFromParcel(parcel) : null;
            int readInt = parcel.readInt();
            ArrayList arrayList = new ArrayList(readInt);
            while (readInt != 0) {
                arrayList.add((CustomerSource) CustomerSource.CREATOR.createFromParcel(parcel));
                readInt--;
            }
            return new Customer(readString, readString2, shippingInformation, arrayList, parcel.readInt() != 0, parcel.readInt() != 0 ? Integer.valueOf(parcel.readInt()) : null, parcel.readString());
        }

        public final Object[] newArray(int i) {
            return new Customer[i];
        }
    }

    public static /* synthetic */ Customer copy$default(Customer customer, String str, String str2, ShippingInformation shippingInformation2, List<CustomerSource> list, boolean z, Integer num, String str3, int i, Object obj) {
        if ((i & 1) != 0) {
            str = customer.id;
        }
        if ((i & 2) != 0) {
            str2 = customer.defaultSource;
        }
        String str4 = str2;
        if ((i & 4) != 0) {
            shippingInformation2 = customer.shippingInformation;
        }
        ShippingInformation shippingInformation3 = shippingInformation2;
        if ((i & 8) != 0) {
            list = customer.sources;
        }
        List<CustomerSource> list2 = list;
        if ((i & 16) != 0) {
            z = customer.hasMore;
        }
        boolean z2 = z;
        if ((i & 32) != 0) {
            num = customer.totalCount;
        }
        Integer num2 = num;
        if ((i & 64) != 0) {
            str3 = customer.url;
        }
        return customer.copy(str, str4, shippingInformation3, list2, z2, num2, str3);
    }

    @JvmStatic
    public static final Customer fromJson(JSONObject jSONObject) {
        return Companion.fromJson(jSONObject);
    }

    @JvmStatic
    public static final Customer fromString(String str) {
        return Companion.fromString(str);
    }

    public final String component1() {
        return this.id;
    }

    public final String component2() {
        return this.defaultSource;
    }

    public final ShippingInformation component3() {
        return this.shippingInformation;
    }

    public final List<CustomerSource> component4() {
        return this.sources;
    }

    public final boolean component5() {
        return this.hasMore;
    }

    public final Integer component6() {
        return this.totalCount;
    }

    public final String component7() {
        return this.url;
    }

    public final Customer copy(String str, String str2, ShippingInformation shippingInformation2, List<CustomerSource> list, boolean z, Integer num, String str3) {
        Intrinsics.checkParameterIsNotNull(list, "sources");
        return new Customer(str, str2, shippingInformation2, list, z, num, str3);
    }

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Customer)) {
            return false;
        }
        Customer customer = (Customer) obj;
        return Intrinsics.areEqual((Object) this.id, (Object) customer.id) && Intrinsics.areEqual((Object) this.defaultSource, (Object) customer.defaultSource) && Intrinsics.areEqual((Object) this.shippingInformation, (Object) customer.shippingInformation) && Intrinsics.areEqual((Object) this.sources, (Object) customer.sources) && this.hasMore == customer.hasMore && Intrinsics.areEqual((Object) this.totalCount, (Object) customer.totalCount) && Intrinsics.areEqual((Object) this.url, (Object) customer.url);
    }

    public int hashCode() {
        String str = this.id;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.defaultSource;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        ShippingInformation shippingInformation2 = this.shippingInformation;
        int hashCode3 = (hashCode2 + (shippingInformation2 != null ? shippingInformation2.hashCode() : 0)) * 31;
        List<CustomerSource> list = this.sources;
        int hashCode4 = (hashCode3 + (list != null ? list.hashCode() : 0)) * 31;
        boolean z = this.hasMore;
        if (z) {
            z = true;
        }
        int i2 = (hashCode4 + (z ? 1 : 0)) * 31;
        Integer num = this.totalCount;
        int hashCode5 = (i2 + (num != null ? num.hashCode() : 0)) * 31;
        String str3 = this.url;
        if (str3 != null) {
            i = str3.hashCode();
        }
        return hashCode5 + i;
    }

    public String toString() {
        return "Customer(id=" + this.id + ", defaultSource=" + this.defaultSource + ", shippingInformation=" + this.shippingInformation + ", sources=" + this.sources + ", hasMore=" + this.hasMore + ", totalCount=" + this.totalCount + ", url=" + this.url + ")";
    }

    public void writeToParcel(Parcel parcel, int i) {
        Intrinsics.checkParameterIsNotNull(parcel, "parcel");
        parcel.writeString(this.id);
        parcel.writeString(this.defaultSource);
        ShippingInformation shippingInformation2 = this.shippingInformation;
        if (shippingInformation2 != null) {
            parcel.writeInt(1);
            shippingInformation2.writeToParcel(parcel, 0);
        } else {
            parcel.writeInt(0);
        }
        List<CustomerSource> list = this.sources;
        parcel.writeInt(list.size());
        for (CustomerSource writeToParcel : list) {
            writeToParcel.writeToParcel(parcel, 0);
        }
        parcel.writeInt(this.hasMore ? 1 : 0);
        Integer num = this.totalCount;
        if (num != null) {
            parcel.writeInt(1);
            parcel.writeInt(num.intValue());
        } else {
            parcel.writeInt(0);
        }
        parcel.writeString(this.url);
    }

    public Customer(String str, String str2, ShippingInformation shippingInformation2, List<CustomerSource> list, boolean z, Integer num, String str3) {
        Intrinsics.checkParameterIsNotNull(list, "sources");
        this.id = str;
        this.defaultSource = str2;
        this.shippingInformation = shippingInformation2;
        this.sources = list;
        this.hasMore = z;
        this.totalCount = num;
        this.url = str3;
    }

    public final String getId() {
        return this.id;
    }

    public final String getDefaultSource() {
        return this.defaultSource;
    }

    public final ShippingInformation getShippingInformation() {
        return this.shippingInformation;
    }

    public final List<CustomerSource> getSources() {
        return this.sources;
    }

    public final boolean getHasMore() {
        return this.hasMore;
    }

    public final Integer getTotalCount() {
        return this.totalCount;
    }

    public final String getUrl() {
        return this.url;
    }

    public final CustomerSource getSourceById(String str) {
        Object obj;
        Intrinsics.checkParameterIsNotNull(str, "sourceId");
        Iterator it = this.sources.iterator();
        while (true) {
            if (!it.hasNext()) {
                obj = null;
                break;
            }
            obj = it.next();
            if (Intrinsics.areEqual((Object) ((CustomerSource) obj).getId(), (Object) str)) {
                break;
            }
        }
        return (CustomerSource) obj;
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0012\u0010\u0003\u001a\u0004\u0018\u00010\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0007J\u0014\u0010\u0007\u001a\u0004\u0018\u00010\u00042\b\u0010\b\u001a\u0004\u0018\u00010\tH\u0007¨\u0006\n"}, d2 = {"Lcom/stripe/android/model/Customer$Companion;", "", "()V", "fromJson", "Lcom/stripe/android/model/Customer;", "jsonObject", "Lorg/json/JSONObject;", "fromString", "jsonString", "", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: Customer.kt */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        @JvmStatic
        public final Customer fromString(String str) {
            Object obj;
            if (str == null) {
                return null;
            }
            try {
                Result.Companion companion = Result.Companion;
                Companion companion2 = this;
                obj = Result.m4constructorimpl(new JSONObject(str));
            } catch (Throwable th) {
                Result.Companion companion3 = Result.Companion;
                obj = Result.m4constructorimpl(ResultKt.createFailure(th));
            }
            if (Result.m10isFailureimpl(obj)) {
                obj = null;
            }
            JSONObject jSONObject = (JSONObject) obj;
            if (jSONObject != null) {
                return Customer.Companion.fromJson(jSONObject);
            }
            return null;
        }

        @JvmStatic
        public final Customer fromJson(JSONObject jSONObject) {
            Intrinsics.checkParameterIsNotNull(jSONObject, "jsonObject");
            return new CustomerJsonParser().parse(jSONObject);
        }
    }
}
