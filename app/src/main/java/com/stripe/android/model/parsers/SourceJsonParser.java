package com.stripe.android.model.parsers;

import com.stripe.android.model.Source;
import com.stripe.android.model.SourceCodeVerification;
import com.stripe.android.model.SourceOrder;
import com.stripe.android.model.SourceOwner;
import com.stripe.android.model.SourceReceiver;
import com.stripe.android.model.SourceRedirect;
import com.stripe.android.model.StripeJsonUtils;
import com.stripe.android.model.StripeModel;
import com.stripe.android.model.WeChat;
import java.util.Map;
import java.util.Set;
import kotlin.Metadata;
import kotlin.collections.SetsKt;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.internal.Intrinsics;
import org.json.JSONObject;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u0000\u0018\u0000 \u00072\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0002\u0007\bB\u0005¢\u0006\u0002\u0010\u0003J\u0012\u0010\u0004\u001a\u0004\u0018\u00010\u00022\u0006\u0010\u0005\u001a\u00020\u0006H\u0016¨\u0006\t"}, d2 = {"Lcom/stripe/android/model/parsers/SourceJsonParser;", "Lcom/stripe/android/model/parsers/ModelJsonParser;", "Lcom/stripe/android/model/Source;", "()V", "parse", "json", "Lorg/json/JSONObject;", "Companion", "KlarnaJsonParser", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: SourceJsonParser.kt */
public final class SourceJsonParser implements ModelJsonParser<Source> {
    @Deprecated
    public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
    private static final String FIELD_AMOUNT = "amount";
    private static final String FIELD_CLIENT_SECRET = "client_secret";
    private static final String FIELD_CODE_VERIFICATION = "code_verification";
    private static final String FIELD_CREATED = "created";
    private static final String FIELD_CURRENCY = "currency";
    private static final String FIELD_FLOW = "flow";
    private static final String FIELD_ID = "id";
    private static final String FIELD_KLARNA = "klarna";
    private static final String FIELD_LIVEMODE = "livemode";
    private static final String FIELD_METADATA = "metadata";
    private static final String FIELD_OBJECT = "object";
    private static final String FIELD_OWNER = "owner";
    private static final String FIELD_RECEIVER = "receiver";
    private static final String FIELD_REDIRECT = "redirect";
    private static final String FIELD_SOURCE_ORDER = "source_order";
    private static final String FIELD_STATEMENT_DESCRIPTOR = "statement_descriptor";
    private static final String FIELD_STATUS = "status";
    private static final String FIELD_TYPE = "type";
    private static final String FIELD_USAGE = "usage";
    private static final String FIELD_WECHAT = "wechat";
    /* access modifiers changed from: private */
    public static final Set<String> MODELED_TYPES = SetsKt.setOf("card", "sepa_debit");
    private static final String VALUE_CARD = "card";
    private static final String VALUE_SOURCE = "source";

    @JvmStatic
    public static final String asSourceType(String str) {
        return Companion.asSourceType(str);
    }

    public Source parse(JSONObject jSONObject) {
        Intrinsics.checkParameterIsNotNull(jSONObject, "json");
        String optString = jSONObject.optString(FIELD_OBJECT);
        if (optString != null) {
            int hashCode = optString.hashCode();
            if (hashCode != -896505829) {
                if (hashCode == 3046160 && optString.equals("card")) {
                    return Companion.fromCardJson(jSONObject);
                }
            } else if (optString.equals("source")) {
                return Companion.fromSourceJson(jSONObject);
            }
        }
        return null;
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\"\n\u0002\u0010\u000e\n\u0002\b\u0003\b\u0000\u0018\u0000 \u000b2\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u000bB\u0005¢\u0006\u0002\u0010\u0003J\u0010\u0010\u0004\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0006H\u0016J\u001e\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\t0\b2\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\n\u001a\u00020\tH\u0002¨\u0006\f"}, d2 = {"Lcom/stripe/android/model/parsers/SourceJsonParser$KlarnaJsonParser;", "Lcom/stripe/android/model/parsers/ModelJsonParser;", "Lcom/stripe/android/model/Source$Klarna;", "()V", "parse", "json", "Lorg/json/JSONObject;", "parseSet", "", "", "key", "Companion", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: SourceJsonParser.kt */
    public static final class KlarnaJsonParser implements ModelJsonParser<Source.Klarna> {
        @Deprecated
        public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
        private static final String FIELD_CLIENT_TOKEN = "client_token";
        private static final String FIELD_CUSTOM_PAYMENT_METHODS = "custom_payment_methods";
        private static final String FIELD_FIRST_NAME = "first_name";
        private static final String FIELD_LAST_NAME = "last_name";
        private static final String FIELD_PAYMENT_METHOD_CATEGORIES = "payment_method_categories";
        private static final String FIELD_PAY_LATER_ASSET_URLS_DESCRIPTIVE = "pay_later_asset_urls_descriptive";
        private static final String FIELD_PAY_LATER_ASSET_URLS_STANDARD = "pay_later_asset_urls_standard";
        private static final String FIELD_PAY_LATER_NAME = "pay_later_name";
        private static final String FIELD_PAY_LATER_REDIRECT_URL = "pay_later_redirect_url";
        private static final String FIELD_PAY_NOW_ASSET_URLS_DESCRIPTIVE = "pay_now_asset_urls_descriptive";
        private static final String FIELD_PAY_NOW_ASSET_URLS_STANDARD = "pay_now_asset_urls_standard";
        private static final String FIELD_PAY_NOW_NAME = "pay_now_name";
        private static final String FIELD_PAY_NOW_REDIRECT_URL = "pay_now_redirect_url";
        private static final String FIELD_PAY_OVER_TIME_ASSET_URLS_DESCRIPTIVE = "pay_over_time_asset_urls_descriptive";
        private static final String FIELD_PAY_OVER_TIME_ASSET_URLS_STANDARD = "pay_over_time_asset_urls_standard";
        private static final String FIELD_PAY_OVER_TIME_NAME = "pay_over_time_name";
        private static final String FIELD_PAY_OVER_TIME_REDIRECT_URL = "pay_over_time_redirect_url";
        private static final String FIELD_PURCHASE_COUNTRY = "purchase_country";

        public Source.Klarna parse(JSONObject jSONObject) {
            JSONObject jSONObject2 = jSONObject;
            Intrinsics.checkParameterIsNotNull(jSONObject2, "json");
            Source.Klarna klarna = r3;
            Source.Klarna klarna2 = new Source.Klarna(StripeJsonUtils.optString(jSONObject2, FIELD_FIRST_NAME), StripeJsonUtils.optString(jSONObject2, FIELD_LAST_NAME), StripeJsonUtils.optString(jSONObject2, FIELD_PURCHASE_COUNTRY), StripeJsonUtils.optString(jSONObject2, FIELD_CLIENT_TOKEN), StripeJsonUtils.optString(jSONObject2, FIELD_PAY_NOW_ASSET_URLS_DESCRIPTIVE), StripeJsonUtils.optString(jSONObject2, FIELD_PAY_NOW_ASSET_URLS_STANDARD), StripeJsonUtils.optString(jSONObject2, FIELD_PAY_NOW_NAME), StripeJsonUtils.optString(jSONObject2, FIELD_PAY_NOW_REDIRECT_URL), StripeJsonUtils.optString(jSONObject2, FIELD_PAY_LATER_ASSET_URLS_DESCRIPTIVE), StripeJsonUtils.optString(jSONObject2, FIELD_PAY_LATER_ASSET_URLS_STANDARD), StripeJsonUtils.optString(jSONObject2, FIELD_PAY_LATER_NAME), StripeJsonUtils.optString(jSONObject2, FIELD_PAY_LATER_REDIRECT_URL), StripeJsonUtils.optString(jSONObject2, FIELD_PAY_OVER_TIME_ASSET_URLS_DESCRIPTIVE), StripeJsonUtils.optString(jSONObject2, FIELD_PAY_OVER_TIME_ASSET_URLS_STANDARD), StripeJsonUtils.optString(jSONObject2, FIELD_PAY_OVER_TIME_NAME), StripeJsonUtils.optString(jSONObject2, FIELD_PAY_OVER_TIME_REDIRECT_URL), parseSet(jSONObject2, FIELD_PAYMENT_METHOD_CATEGORIES), parseSet(jSONObject2, FIELD_CUSTOM_PAYMENT_METHODS));
            return klarna;
        }

        /* JADX WARNING: Code restructure failed: missing block: B:2:0x0006, code lost:
            r7 = kotlin.text.StringsKt.split$default((java.lang.CharSequence) r7, new java.lang.String[]{","}, false, 0, 6, (java.lang.Object) null);
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        private final java.util.Set<java.lang.String> parseSet(org.json.JSONObject r7, java.lang.String r8) {
            /*
                r6 = this;
                java.lang.String r7 = com.stripe.android.model.StripeJsonUtils.optString(r7, r8)
                if (r7 == 0) goto L_0x0020
                r0 = r7
                java.lang.CharSequence r0 = (java.lang.CharSequence) r0
                java.lang.String r7 = ","
                java.lang.String[] r1 = new java.lang.String[]{r7}
                r2 = 0
                r3 = 0
                r4 = 6
                r5 = 0
                java.util.List r7 = kotlin.text.StringsKt.split$default((java.lang.CharSequence) r0, (java.lang.String[]) r1, (boolean) r2, (int) r3, (int) r4, (java.lang.Object) r5)
                if (r7 == 0) goto L_0x0020
                java.lang.Iterable r7 = (java.lang.Iterable) r7
                java.util.Set r7 = kotlin.collections.CollectionsKt.toSet(r7)
                goto L_0x0021
            L_0x0020:
                r7 = 0
            L_0x0021:
                if (r7 == 0) goto L_0x0024
                goto L_0x0028
            L_0x0024:
                java.util.Set r7 = kotlin.collections.SetsKt.emptySet()
            L_0x0028:
                return r7
            */
            throw new UnsupportedOperationException("Method not decompiled: com.stripe.android.model.parsers.SourceJsonParser.KlarnaJsonParser.parseSet(org.json.JSONObject, java.lang.String):java.util.Set");
        }

        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0012\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\u0016"}, d2 = {"Lcom/stripe/android/model/parsers/SourceJsonParser$KlarnaJsonParser$Companion;", "", "()V", "FIELD_CLIENT_TOKEN", "", "FIELD_CUSTOM_PAYMENT_METHODS", "FIELD_FIRST_NAME", "FIELD_LAST_NAME", "FIELD_PAYMENT_METHOD_CATEGORIES", "FIELD_PAY_LATER_ASSET_URLS_DESCRIPTIVE", "FIELD_PAY_LATER_ASSET_URLS_STANDARD", "FIELD_PAY_LATER_NAME", "FIELD_PAY_LATER_REDIRECT_URL", "FIELD_PAY_NOW_ASSET_URLS_DESCRIPTIVE", "FIELD_PAY_NOW_ASSET_URLS_STANDARD", "FIELD_PAY_NOW_NAME", "FIELD_PAY_NOW_REDIRECT_URL", "FIELD_PAY_OVER_TIME_ASSET_URLS_DESCRIPTIVE", "FIELD_PAY_OVER_TIME_ASSET_URLS_STANDARD", "FIELD_PAY_OVER_TIME_NAME", "FIELD_PAY_OVER_TIME_REDIRECT_URL", "FIELD_PURCHASE_COUNTRY", "stripe_release"}, k = 1, mv = {1, 1, 16})
        /* compiled from: SourceJsonParser.kt */
        private static final class Companion {
            private Companion() {
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0014\n\u0002\u0010\"\n\u0002\b\u000b\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0014\u0010\u001c\u001a\u0004\u0018\u00010\u00042\b\u0010\u001d\u001a\u0004\u0018\u00010\u0004H\u0002J\u0014\u0010\u001e\u001a\u0004\u0018\u00010\u00042\b\u0010\u001f\u001a\u0004\u0018\u00010\u0004H\u0002J\u0012\u0010 \u001a\u00020\u00042\b\u0010!\u001a\u0004\u0018\u00010\u0004H\u0007J\u0014\u0010\"\u001a\u0004\u0018\u00010\u00042\b\u0010#\u001a\u0004\u0018\u00010\u0004H\u0002J\u0010\u0010$\u001a\u00020%2\u0006\u0010&\u001a\u00020'H\u0002J\u0010\u0010(\u001a\u00020%2\u0006\u0010&\u001a\u00020'H\u0002J.\u0010)\u001a\u0004\u0018\u0001H*\"\n\b\u0000\u0010*\u0018\u0001*\u00020+2\u0006\u0010&\u001a\u00020'2\b\b\u0001\u0010,\u001a\u00020\u0004H\b¢\u0006\u0002\u0010-R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0017\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u0014\u0010\u0018\u001a\b\u0012\u0004\u0012\u00020\u00040\u0019X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u001a\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u001b\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006."}, d2 = {"Lcom/stripe/android/model/parsers/SourceJsonParser$Companion;", "", "()V", "FIELD_AMOUNT", "", "FIELD_CLIENT_SECRET", "FIELD_CODE_VERIFICATION", "FIELD_CREATED", "FIELD_CURRENCY", "FIELD_FLOW", "FIELD_ID", "FIELD_KLARNA", "FIELD_LIVEMODE", "FIELD_METADATA", "FIELD_OBJECT", "FIELD_OWNER", "FIELD_RECEIVER", "FIELD_REDIRECT", "FIELD_SOURCE_ORDER", "FIELD_STATEMENT_DESCRIPTOR", "FIELD_STATUS", "FIELD_TYPE", "FIELD_USAGE", "FIELD_WECHAT", "MODELED_TYPES", "", "VALUE_CARD", "VALUE_SOURCE", "asSourceFlow", "sourceFlow", "asSourceStatus", "sourceStatus", "asSourceType", "sourceType", "asUsage", "usage", "fromCardJson", "Lcom/stripe/android/model/Source;", "jsonObject", "Lorg/json/JSONObject;", "fromSourceJson", "optStripeJsonModel", "T", "Lcom/stripe/android/model/StripeModel;", "key", "(Lorg/json/JSONObject;Ljava/lang/String;)Lcom/stripe/android/model/StripeModel;", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: SourceJsonParser.kt */
    private static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        /* access modifiers changed from: private */
        public final Source fromCardJson(JSONObject jSONObject) {
            JSONObject jSONObject2 = jSONObject;
            return new Source(StripeJsonUtils.optString(jSONObject2, "id"), (Long) null, (String) null, (SourceCodeVerification) null, (Long) null, (String) null, (String) null, (Boolean) null, (Map) null, (SourceOwner) null, (SourceReceiver) null, (SourceRedirect) null, (String) null, (Map) null, new SourceCardDataJsonParser().parse(jSONObject2), "card", "card", (String) null, (WeChat) null, (Source.Klarna) null, (SourceOrder) null, (String) null, 4079614, (DefaultConstructorMarker) null);
        }

        /* JADX WARNING: type inference failed for: r4v25, types: [com.stripe.android.model.StripeModel] */
        /* JADX WARNING: type inference failed for: r4v35, types: [com.stripe.android.model.StripeModel] */
        /* JADX WARNING: type inference failed for: r4v45, types: [com.stripe.android.model.StripeModel] */
        /* access modifiers changed from: private */
        /* JADX WARNING: Can't fix incorrect switch cases order */
        /* JADX WARNING: Multi-variable type inference failed */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final com.stripe.android.model.Source fromSourceJson(org.json.JSONObject r30) {
            /*
                r29 = this;
                r0 = r30
                java.lang.String r1 = "type"
                java.lang.String r1 = com.stripe.android.model.StripeJsonUtils.optString(r0, r1)
                if (r1 == 0) goto L_0x000b
                goto L_0x000d
            L_0x000b:
                java.lang.String r1 = "unknown"
            L_0x000d:
                r2 = r29
                com.stripe.android.model.parsers.SourceJsonParser$Companion r2 = (com.stripe.android.model.parsers.SourceJsonParser.Companion) r2
                java.lang.String r15 = r2.asSourceType(r1)
                com.stripe.android.model.StripeJsonUtils r3 = com.stripe.android.model.StripeJsonUtils.INSTANCE
                org.json.JSONObject r4 = r0.optJSONObject(r1)
                java.util.Map r16 = r3.jsonObjectToMap$stripe_release(r4)
                java.util.Set r3 = com.stripe.android.model.parsers.SourceJsonParser.MODELED_TYPES
                boolean r3 = r3.contains(r1)
                java.lang.String r4 = "owner"
                java.lang.String r5 = "redirect"
                java.lang.String r6 = "receiver"
                java.lang.String r7 = "code_verification"
                if (r3 == 0) goto L_0x00f4
                boolean r3 = r0.has(r1)
                if (r3 != 0) goto L_0x003a
                r3 = 0
                goto L_0x00ef
            L_0x003a:
                int r3 = r1.hashCode()
                switch(r3) {
                    case -808719889: goto L_0x00cb;
                    case -776144932: goto L_0x00b1;
                    case 3046160: goto L_0x0095;
                    case 106164915: goto L_0x007b;
                    case 1615551277: goto L_0x0060;
                    case 1636477296: goto L_0x0043;
                    default: goto L_0x0041;
                }
            L_0x0041:
                goto L_0x00e5
            L_0x0043:
                java.lang.String r3 = "sepa_debit"
                boolean r9 = r1.equals(r3)
                if (r9 == 0) goto L_0x00e5
                org.json.JSONObject r3 = r0.optJSONObject(r3)
                if (r3 == 0) goto L_0x005b
                com.stripe.android.model.parsers.SourceSepaDebitDataJsonParser r9 = new com.stripe.android.model.parsers.SourceSepaDebitDataJsonParser
                r9.<init>()
                com.stripe.android.model.SourceTypeModel$SepaDebit r3 = r9.parse((org.json.JSONObject) r3)
                goto L_0x005c
            L_0x005b:
                r3 = 0
            L_0x005c:
                com.stripe.android.model.StripeModel r3 = (com.stripe.android.model.StripeModel) r3
                goto L_0x00e6
            L_0x0060:
                boolean r3 = r1.equals(r7)
                if (r3 == 0) goto L_0x00e5
                org.json.JSONObject r3 = r0.optJSONObject(r7)
                if (r3 == 0) goto L_0x0076
                com.stripe.android.model.parsers.SourceCodeVerificationJsonParser r9 = new com.stripe.android.model.parsers.SourceCodeVerificationJsonParser
                r9.<init>()
                com.stripe.android.model.SourceCodeVerification r3 = r9.parse((org.json.JSONObject) r3)
                goto L_0x0077
            L_0x0076:
                r3 = 0
            L_0x0077:
                com.stripe.android.model.StripeModel r3 = (com.stripe.android.model.StripeModel) r3
                goto L_0x00e6
            L_0x007b:
                boolean r3 = r1.equals(r4)
                if (r3 == 0) goto L_0x00e5
                org.json.JSONObject r3 = r0.optJSONObject(r4)
                if (r3 == 0) goto L_0x0091
                com.stripe.android.model.parsers.SourceOwnerJsonParser r9 = new com.stripe.android.model.parsers.SourceOwnerJsonParser
                r9.<init>()
                com.stripe.android.model.SourceOwner r3 = r9.parse((org.json.JSONObject) r3)
                goto L_0x0092
            L_0x0091:
                r3 = 0
            L_0x0092:
                com.stripe.android.model.StripeModel r3 = (com.stripe.android.model.StripeModel) r3
                goto L_0x00e6
            L_0x0095:
                java.lang.String r3 = "card"
                boolean r9 = r1.equals(r3)
                if (r9 == 0) goto L_0x00e5
                org.json.JSONObject r3 = r0.optJSONObject(r3)
                if (r3 == 0) goto L_0x00ad
                com.stripe.android.model.parsers.SourceCardDataJsonParser r9 = new com.stripe.android.model.parsers.SourceCardDataJsonParser
                r9.<init>()
                com.stripe.android.model.SourceTypeModel$Card r3 = r9.parse((org.json.JSONObject) r3)
                goto L_0x00ae
            L_0x00ad:
                r3 = 0
            L_0x00ae:
                com.stripe.android.model.StripeModel r3 = (com.stripe.android.model.StripeModel) r3
                goto L_0x00e6
            L_0x00b1:
                boolean r3 = r1.equals(r5)
                if (r3 == 0) goto L_0x00e5
                org.json.JSONObject r3 = r0.optJSONObject(r5)
                if (r3 == 0) goto L_0x00c7
                com.stripe.android.model.parsers.SourceRedirectJsonParser r9 = new com.stripe.android.model.parsers.SourceRedirectJsonParser
                r9.<init>()
                com.stripe.android.model.SourceRedirect r3 = r9.parse((org.json.JSONObject) r3)
                goto L_0x00c8
            L_0x00c7:
                r3 = 0
            L_0x00c8:
                com.stripe.android.model.StripeModel r3 = (com.stripe.android.model.StripeModel) r3
                goto L_0x00e6
            L_0x00cb:
                boolean r3 = r1.equals(r6)
                if (r3 == 0) goto L_0x00e5
                org.json.JSONObject r3 = r0.optJSONObject(r6)
                if (r3 == 0) goto L_0x00e1
                com.stripe.android.model.parsers.SourceReceiverJsonParser r9 = new com.stripe.android.model.parsers.SourceReceiverJsonParser
                r9.<init>()
                com.stripe.android.model.SourceReceiver r3 = r9.parse((org.json.JSONObject) r3)
                goto L_0x00e2
            L_0x00e1:
                r3 = 0
            L_0x00e2:
                com.stripe.android.model.StripeModel r3 = (com.stripe.android.model.StripeModel) r3
                goto L_0x00e6
            L_0x00e5:
                r3 = 0
            L_0x00e6:
                boolean r9 = r3 instanceof com.stripe.android.model.SourceTypeModel
                if (r9 != 0) goto L_0x00eb
                r3 = 0
            L_0x00eb:
                com.stripe.android.model.SourceTypeModel r3 = (com.stripe.android.model.SourceTypeModel) r3
                com.stripe.android.model.StripeModel r3 = (com.stripe.android.model.StripeModel) r3
            L_0x00ef:
                com.stripe.android.model.SourceTypeModel r3 = (com.stripe.android.model.SourceTypeModel) r3
                r17 = r3
                goto L_0x00f6
            L_0x00f4:
                r17 = 0
            L_0x00f6:
                java.lang.String r3 = "id"
                java.lang.String r3 = com.stripe.android.model.StripeJsonUtils.optString(r0, r3)
                com.stripe.android.model.StripeJsonUtils r9 = com.stripe.android.model.StripeJsonUtils.INSTANCE
                java.lang.String r10 = "amount"
                java.lang.Long r9 = r9.optLong$stripe_release(r0, r10)
                java.lang.String r10 = "client_secret"
                java.lang.String r10 = com.stripe.android.model.StripeJsonUtils.optString(r0, r10)
                boolean r11 = r0.has(r7)
                if (r11 != 0) goto L_0x0112
                r7 = 0
                goto L_0x012e
            L_0x0112:
                org.json.JSONObject r7 = r0.optJSONObject(r7)
                if (r7 == 0) goto L_0x0122
                com.stripe.android.model.parsers.SourceCodeVerificationJsonParser r11 = new com.stripe.android.model.parsers.SourceCodeVerificationJsonParser
                r11.<init>()
                com.stripe.android.model.SourceCodeVerification r7 = r11.parse((org.json.JSONObject) r7)
                goto L_0x0123
            L_0x0122:
                r7 = 0
            L_0x0123:
                com.stripe.android.model.StripeModel r7 = (com.stripe.android.model.StripeModel) r7
                boolean r11 = r7 instanceof com.stripe.android.model.SourceCodeVerification
                if (r11 != 0) goto L_0x012a
                r7 = 0
            L_0x012a:
                com.stripe.android.model.SourceCodeVerification r7 = (com.stripe.android.model.SourceCodeVerification) r7
                com.stripe.android.model.StripeModel r7 = (com.stripe.android.model.StripeModel) r7
            L_0x012e:
                com.stripe.android.model.SourceCodeVerification r7 = (com.stripe.android.model.SourceCodeVerification) r7
                com.stripe.android.model.StripeJsonUtils r11 = com.stripe.android.model.StripeJsonUtils.INSTANCE
                java.lang.String r12 = "created"
                java.lang.Long r11 = r11.optLong$stripe_release(r0, r12)
                java.lang.String r12 = "currency"
                java.lang.String r12 = com.stripe.android.model.StripeJsonUtils.optString(r0, r12)
                java.lang.String r13 = "flow"
                java.lang.String r13 = com.stripe.android.model.StripeJsonUtils.optString(r0, r13)
                java.lang.String r13 = r2.asSourceFlow(r13)
                java.lang.String r14 = "livemode"
                boolean r14 = r0.optBoolean(r14)
                java.lang.Boolean r14 = java.lang.Boolean.valueOf(r14)
                com.stripe.android.model.StripeJsonUtils r8 = com.stripe.android.model.StripeJsonUtils.INSTANCE
                r19 = r1
                java.lang.String r1 = "metadata"
                org.json.JSONObject r1 = r0.optJSONObject(r1)
                java.util.Map r1 = r8.jsonObjectToStringMap$stripe_release(r1)
                boolean r8 = r0.has(r4)
                if (r8 != 0) goto L_0x0168
                r4 = 0
                goto L_0x0184
            L_0x0168:
                org.json.JSONObject r4 = r0.optJSONObject(r4)
                if (r4 == 0) goto L_0x0178
                com.stripe.android.model.parsers.SourceOwnerJsonParser r8 = new com.stripe.android.model.parsers.SourceOwnerJsonParser
                r8.<init>()
                com.stripe.android.model.SourceOwner r4 = r8.parse((org.json.JSONObject) r4)
                goto L_0x0179
            L_0x0178:
                r4 = 0
            L_0x0179:
                com.stripe.android.model.StripeModel r4 = (com.stripe.android.model.StripeModel) r4
                boolean r8 = r4 instanceof com.stripe.android.model.SourceOwner
                if (r8 != 0) goto L_0x0180
                r4 = 0
            L_0x0180:
                com.stripe.android.model.SourceOwner r4 = (com.stripe.android.model.SourceOwner) r4
                com.stripe.android.model.StripeModel r4 = (com.stripe.android.model.StripeModel) r4
            L_0x0184:
                r20 = r4
                com.stripe.android.model.SourceOwner r20 = (com.stripe.android.model.SourceOwner) r20
                boolean r4 = r0.has(r6)
                if (r4 != 0) goto L_0x0190
                r4 = 0
                goto L_0x01ac
            L_0x0190:
                org.json.JSONObject r4 = r0.optJSONObject(r6)
                if (r4 == 0) goto L_0x01a0
                com.stripe.android.model.parsers.SourceReceiverJsonParser r6 = new com.stripe.android.model.parsers.SourceReceiverJsonParser
                r6.<init>()
                com.stripe.android.model.SourceReceiver r4 = r6.parse((org.json.JSONObject) r4)
                goto L_0x01a1
            L_0x01a0:
                r4 = 0
            L_0x01a1:
                com.stripe.android.model.StripeModel r4 = (com.stripe.android.model.StripeModel) r4
                boolean r6 = r4 instanceof com.stripe.android.model.SourceReceiver
                if (r6 != 0) goto L_0x01a8
                r4 = 0
            L_0x01a8:
                com.stripe.android.model.SourceReceiver r4 = (com.stripe.android.model.SourceReceiver) r4
                com.stripe.android.model.StripeModel r4 = (com.stripe.android.model.StripeModel) r4
            L_0x01ac:
                r21 = r4
                com.stripe.android.model.SourceReceiver r21 = (com.stripe.android.model.SourceReceiver) r21
                boolean r4 = r0.has(r5)
                if (r4 != 0) goto L_0x01b8
                r4 = 0
                goto L_0x01d4
            L_0x01b8:
                org.json.JSONObject r4 = r0.optJSONObject(r5)
                if (r4 == 0) goto L_0x01c8
                com.stripe.android.model.parsers.SourceRedirectJsonParser r5 = new com.stripe.android.model.parsers.SourceRedirectJsonParser
                r5.<init>()
                com.stripe.android.model.SourceRedirect r4 = r5.parse((org.json.JSONObject) r4)
                goto L_0x01c9
            L_0x01c8:
                r4 = 0
            L_0x01c9:
                com.stripe.android.model.StripeModel r4 = (com.stripe.android.model.StripeModel) r4
                boolean r5 = r4 instanceof com.stripe.android.model.SourceRedirect
                if (r5 != 0) goto L_0x01d0
                r4 = 0
            L_0x01d0:
                com.stripe.android.model.SourceRedirect r4 = (com.stripe.android.model.SourceRedirect) r4
                com.stripe.android.model.StripeModel r4 = (com.stripe.android.model.StripeModel) r4
            L_0x01d4:
                r22 = r4
                com.stripe.android.model.SourceRedirect r22 = (com.stripe.android.model.SourceRedirect) r22
                java.lang.String r4 = "source_order"
                org.json.JSONObject r4 = r0.optJSONObject(r4)
                if (r4 == 0) goto L_0x01ec
                com.stripe.android.model.parsers.SourceOrderJsonParser r5 = new com.stripe.android.model.parsers.SourceOrderJsonParser
                r5.<init>()
                com.stripe.android.model.SourceOrder r4 = r5.parse((org.json.JSONObject) r4)
                r23 = r4
                goto L_0x01ee
            L_0x01ec:
                r23 = 0
            L_0x01ee:
                java.lang.String r4 = "statement_descriptor"
                java.lang.String r24 = com.stripe.android.model.StripeJsonUtils.optString(r0, r4)
                java.lang.String r4 = "status"
                java.lang.String r4 = com.stripe.android.model.StripeJsonUtils.optString(r0, r4)
                java.lang.String r25 = r2.asSourceStatus(r4)
                java.lang.String r4 = "usage"
                java.lang.String r4 = com.stripe.android.model.StripeJsonUtils.optString(r0, r4)
                java.lang.String r26 = r2.asUsage(r4)
                java.lang.String r2 = "wechat"
                boolean r4 = kotlin.jvm.internal.Intrinsics.areEqual((java.lang.Object) r2, (java.lang.Object) r15)
                if (r4 == 0) goto L_0x022a
                com.stripe.android.model.parsers.WeChatJsonParser r4 = new com.stripe.android.model.parsers.WeChatJsonParser
                r4.<init>()
                org.json.JSONObject r2 = r0.optJSONObject(r2)
                if (r2 == 0) goto L_0x021e
                goto L_0x0223
            L_0x021e:
                org.json.JSONObject r2 = new org.json.JSONObject
                r2.<init>()
            L_0x0223:
                com.stripe.android.model.WeChat r2 = r4.parse((org.json.JSONObject) r2)
                r27 = r2
                goto L_0x022c
            L_0x022a:
                r27 = 0
            L_0x022c:
                java.lang.String r2 = "klarna"
                boolean r4 = kotlin.jvm.internal.Intrinsics.areEqual((java.lang.Object) r2, (java.lang.Object) r15)
                if (r4 == 0) goto L_0x024a
                com.stripe.android.model.parsers.SourceJsonParser$KlarnaJsonParser r4 = new com.stripe.android.model.parsers.SourceJsonParser$KlarnaJsonParser
                r4.<init>()
                org.json.JSONObject r0 = r0.optJSONObject(r2)
                if (r0 == 0) goto L_0x0240
                goto L_0x0245
            L_0x0240:
                org.json.JSONObject r0 = new org.json.JSONObject
                r0.<init>()
            L_0x0245:
                com.stripe.android.model.Source$Klarna r0 = r4.parse((org.json.JSONObject) r0)
                goto L_0x024b
            L_0x024a:
                r0 = 0
            L_0x024b:
                com.stripe.android.model.Source r28 = new com.stripe.android.model.Source
                r2 = r28
                r4 = r9
                r5 = r10
                r6 = r7
                r7 = r11
                r8 = r12
                r9 = r13
                r10 = r14
                r11 = r1
                r12 = r20
                r13 = r21
                r14 = r22
                r1 = r15
                r15 = r25
                r18 = r1
                r20 = r26
                r21 = r27
                r22 = r0
                r2.<init>(r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15, r16, r17, r18, r19, r20, r21, r22, r23, r24)
                return r28
            */
            throw new UnsupportedOperationException("Method not decompiled: com.stripe.android.model.parsers.SourceJsonParser.Companion.fromSourceJson(org.json.JSONObject):com.stripe.android.model.Source");
        }

        private final /* synthetic */ <T extends StripeModel> T optStripeJsonModel(JSONObject jSONObject, String str) {
            T t = null;
            if (!jSONObject.has(str)) {
                return null;
            }
            switch (str.hashCode()) {
                case -808719889:
                    if (str.equals("receiver")) {
                        JSONObject optJSONObject = jSONObject.optJSONObject("receiver");
                        if (optJSONObject != null) {
                            t = new SourceReceiverJsonParser().parse(optJSONObject);
                        }
                        t = (StripeModel) t;
                        break;
                    }
                    break;
                case -776144932:
                    if (str.equals("redirect")) {
                        JSONObject optJSONObject2 = jSONObject.optJSONObject("redirect");
                        if (optJSONObject2 != null) {
                            t = new SourceRedirectJsonParser().parse(optJSONObject2);
                        }
                        t = (StripeModel) t;
                        break;
                    }
                    break;
                case 3046160:
                    if (str.equals("card")) {
                        JSONObject optJSONObject3 = jSONObject.optJSONObject("card");
                        if (optJSONObject3 != null) {
                            t = new SourceCardDataJsonParser().parse(optJSONObject3);
                        }
                        t = (StripeModel) t;
                        break;
                    }
                    break;
                case 106164915:
                    if (str.equals(SourceJsonParser.FIELD_OWNER)) {
                        JSONObject optJSONObject4 = jSONObject.optJSONObject(SourceJsonParser.FIELD_OWNER);
                        if (optJSONObject4 != null) {
                            t = new SourceOwnerJsonParser().parse(optJSONObject4);
                        }
                        t = (StripeModel) t;
                        break;
                    }
                    break;
                case 1615551277:
                    if (str.equals("code_verification")) {
                        JSONObject optJSONObject5 = jSONObject.optJSONObject("code_verification");
                        if (optJSONObject5 != null) {
                            t = new SourceCodeVerificationJsonParser().parse(optJSONObject5);
                        }
                        t = (StripeModel) t;
                        break;
                    }
                    break;
                case 1636477296:
                    if (str.equals("sepa_debit")) {
                        JSONObject optJSONObject6 = jSONObject.optJSONObject("sepa_debit");
                        if (optJSONObject6 != null) {
                            t = new SourceSepaDebitDataJsonParser().parse(optJSONObject6);
                        }
                        t = (StripeModel) t;
                        break;
                    }
                    break;
            }
            Intrinsics.reifiedOperationMarker(2, "T");
            return (StripeModel) t;
        }

        private final String asSourceStatus(String str) {
            if (str != null) {
                switch (str.hashCode()) {
                    case -1281977283:
                        if (str.equals("failed")) {
                            return "failed";
                        }
                        break;
                    case -682587753:
                        if (str.equals("pending")) {
                            return "pending";
                        }
                        break;
                    case -567770136:
                        if (str.equals("consumed")) {
                            return "consumed";
                        }
                        break;
                    case -123173735:
                        if (str.equals("canceled")) {
                            return "canceled";
                        }
                        break;
                    case 1418477070:
                        if (str.equals("chargeable")) {
                            return "chargeable";
                        }
                        break;
                }
            }
            return null;
        }

        @JvmStatic
        public final String asSourceType(String str) {
            String str2 = str;
            if (str2 != null) {
                switch (str.hashCode()) {
                    case -1920743119:
                        if (str2.equals("bancontact")) {
                            return "bancontact";
                        }
                        break;
                    case -1414960566:
                        if (str2.equals("alipay")) {
                            return "alipay";
                        }
                        break;
                    case -1128905083:
                        if (str2.equals("klarna")) {
                            return "klarna";
                        }
                        break;
                    case -896955097:
                        if (str2.equals("sofort")) {
                            return "sofort";
                        }
                        break;
                    case -825238221:
                        if (str2.equals("three_d_secure")) {
                            return "three_d_secure";
                        }
                        break;
                    case -791770330:
                        if (str2.equals("wechat")) {
                            return "wechat";
                        }
                        break;
                    case -284840886:
                        boolean equals = str2.equals("unknown");
                        break;
                    case 100648:
                        if (str2.equals("eps")) {
                            return "eps";
                        }
                        break;
                    case 109234:
                        if (str2.equals("p24")) {
                            return "p24";
                        }
                        break;
                    case 3046160:
                        if (str2.equals("card")) {
                            return "card";
                        }
                        break;
                    case 38358441:
                        if (str2.equals("giropay")) {
                            return "giropay";
                        }
                        break;
                    case 100048981:
                        if (str2.equals("ideal")) {
                            return "ideal";
                        }
                        break;
                    case 1251821346:
                        if (str2.equals("multibanco")) {
                            return "multibanco";
                        }
                        break;
                    case 1636477296:
                        if (str2.equals("sepa_debit")) {
                            return "sepa_debit";
                        }
                        break;
                }
            }
            return "unknown";
        }

        private final String asUsage(String str) {
            if (str != null) {
                int hashCode = str.hashCode();
                if (hashCode != -280723221) {
                    if (hashCode == 913970448 && str.equals("single_use")) {
                        return "single_use";
                    }
                } else if (str.equals("reusable")) {
                    return "reusable";
                }
            }
            return null;
        }

        private final String asSourceFlow(String str) {
            if (str != null) {
                switch (str.hashCode()) {
                    case -808719889:
                        if (str.equals("receiver")) {
                            return "receiver";
                        }
                        break;
                    case -776144932:
                        if (str.equals("redirect")) {
                            return "redirect";
                        }
                        break;
                    case 3387192:
                        if (str.equals("none")) {
                            return "none";
                        }
                        break;
                    case 1615551277:
                        if (str.equals("code_verification")) {
                            return "code_verification";
                        }
                        break;
                }
            }
            return null;
        }
    }
}
