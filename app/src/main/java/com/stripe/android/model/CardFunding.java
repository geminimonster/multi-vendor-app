package com.stripe.android.model;

import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\u000e\n\u0002\b\t\b\u0001\u0018\u0000 \u000b2\b\u0012\u0004\u0012\u00020\u00000\u0001:\u0001\u000bB\u000f\b\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004R\u0014\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006j\u0002\b\u0007j\u0002\b\bj\u0002\b\tj\u0002\b\n¨\u0006\f"}, d2 = {"Lcom/stripe/android/model/CardFunding;", "", "code", "", "(Ljava/lang/String;ILjava/lang/String;)V", "getCode$stripe_release", "()Ljava/lang/String;", "Credit", "Debit", "Prepaid", "Unknown", "Companion", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: CardFunding.kt */
public enum CardFunding {
    Credit("credit"),
    Debit("debit"),
    Prepaid("prepaid"),
    Unknown("unknown");
    
    public static final Companion Companion = null;
    private final String code;

    private CardFunding(String str) {
        this.code = str;
    }

    public final String getCode$stripe_release() {
        return this.code;
    }

    static {
        Companion = new Companion((DefaultConstructorMarker) null);
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0019\u0010\u0003\u001a\u0004\u0018\u00010\u00042\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006H\u0000¢\u0006\u0002\b\u0007¨\u0006\b"}, d2 = {"Lcom/stripe/android/model/CardFunding$Companion;", "", "()V", "fromCode", "Lcom/stripe/android/model/CardFunding;", "code", "", "fromCode$stripe_release", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: CardFunding.kt */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        public final CardFunding fromCode$stripe_release(String str) {
            for (CardFunding cardFunding : CardFunding.values()) {
                if (Intrinsics.areEqual((Object) cardFunding.getCode$stripe_release(), (Object) str)) {
                    return cardFunding;
                }
            }
            return null;
        }
    }
}
