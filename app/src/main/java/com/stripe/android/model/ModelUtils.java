package com.stripe.android.model;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Locale;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.StringCompanionObject;

@Metadata(bv = {1, 0, 3}, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\r\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0004\bÀ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J%\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00062\u0006\u0010\b\u001a\u00020\tH\u0000¢\u0006\u0002\b\nJ\u001d\u0010\u000b\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\b\u001a\u00020\tH\u0000¢\u0006\u0002\b\fJ\u0010\u0010\r\u001a\u00020\u00042\u0006\u0010\u000e\u001a\u00020\u000fH\u0002J\u0017\u0010\u0010\u001a\u00020\u00042\b\u0010\u0011\u001a\u0004\u0018\u00010\u0012H\u0000¢\u0006\u0002\b\u0013J\u001d\u0010\u0014\u001a\u00020\u00062\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\b\u001a\u00020\tH\u0000¢\u0006\u0002\b\u0015¨\u0006\u0016"}, d2 = {"Lcom/stripe/android/model/ModelUtils;", "", "()V", "hasMonthPassed", "", "year", "", "month", "now", "Ljava/util/Calendar;", "hasMonthPassed$stripe_release", "hasYearPassed", "hasYearPassed$stripe_release", "isDigitsOnly", "str", "", "isWholePositiveNumber", "value", "", "isWholePositiveNumber$stripe_release", "normalizeYear", "normalizeYear$stripe_release", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: ModelUtils.kt */
public final class ModelUtils {
    public static final ModelUtils INSTANCE = new ModelUtils();

    private ModelUtils() {
    }

    public final /* synthetic */ boolean isWholePositiveNumber$stripe_release(String str) {
        return str != null && isDigitsOnly(str);
    }

    private final boolean isDigitsOnly(CharSequence charSequence) {
        int i = 0;
        while (i < charSequence.length()) {
            int codePointAt = Character.codePointAt(charSequence, i);
            if (!Character.isDigit(codePointAt)) {
                return false;
            }
            i += Character.charCount(codePointAt);
        }
        return true;
    }

    public final /* synthetic */ boolean hasMonthPassed$stripe_release(int i, int i2, Calendar calendar) {
        Intrinsics.checkParameterIsNotNull(calendar, "now");
        if (hasYearPassed$stripe_release(i, calendar)) {
            return true;
        }
        if (normalizeYear$stripe_release(i, calendar) != calendar.get(1) || i2 >= calendar.get(2) + 1) {
            return false;
        }
        return true;
    }

    public final /* synthetic */ boolean hasYearPassed$stripe_release(int i, Calendar calendar) {
        Intrinsics.checkParameterIsNotNull(calendar, "now");
        return normalizeYear$stripe_release(i, calendar) < calendar.get(1);
    }

    public final /* synthetic */ int normalizeYear$stripe_release(int i, Calendar calendar) {
        Intrinsics.checkParameterIsNotNull(calendar, "now");
        if (i < 0 || 99 < i) {
            return i;
        }
        String valueOf = String.valueOf(calendar.get(1));
        int length = valueOf.length() - 2;
        if (valueOf != null) {
            String substring = valueOf.substring(0, length);
            Intrinsics.checkExpressionValueIsNotNull(substring, "(this as java.lang.Strin…ing(startIndex, endIndex)");
            StringCompanionObject stringCompanionObject = StringCompanionObject.INSTANCE;
            Locale locale = Locale.US;
            Intrinsics.checkExpressionValueIsNotNull(locale, "Locale.US");
            String format = String.format(locale, "%s%02d", Arrays.copyOf(new Object[]{substring, Integer.valueOf(i)}, 2));
            Intrinsics.checkExpressionValueIsNotNull(format, "java.lang.String.format(locale, format, *args)");
            return Integer.parseInt(format);
        }
        throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
    }
}
