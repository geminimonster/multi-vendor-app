package com.stripe.android.model;

import android.os.Parcel;
import android.os.Parcelable;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\t\n\u0002\b\u000f\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\b\b\u0018\u00002\u00020\u0001B)\b\u0000\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0005\u0012\u0006\u0010\u0007\u001a\u00020\u0005¢\u0006\u0002\u0010\bJ\u000b\u0010\u000f\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\t\u0010\u0010\u001a\u00020\u0005HÆ\u0003J\t\u0010\u0011\u001a\u00020\u0005HÆ\u0003J\t\u0010\u0012\u001a\u00020\u0005HÆ\u0003J3\u0010\u0013\u001a\u00020\u00002\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u00052\b\b\u0002\u0010\u0007\u001a\u00020\u0005HÆ\u0001J\t\u0010\u0014\u001a\u00020\u0015HÖ\u0001J\u0013\u0010\u0016\u001a\u00020\u00172\b\u0010\u0018\u001a\u0004\u0018\u00010\u0019HÖ\u0003J\t\u0010\u001a\u001a\u00020\u0015HÖ\u0001J\t\u0010\u001b\u001a\u00020\u0003HÖ\u0001J\u0019\u0010\u001c\u001a\u00020\u001d2\u0006\u0010\u001e\u001a\u00020\u001f2\u0006\u0010 \u001a\u00020\u0015HÖ\u0001R\u0013\u0010\u0002\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0011\u0010\u0006\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\fR\u0011\u0010\u0007\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\f¨\u0006!"}, d2 = {"Lcom/stripe/android/model/SourceReceiver;", "Lcom/stripe/android/model/StripeModel;", "address", "", "amountCharged", "", "amountReceived", "amountReturned", "(Ljava/lang/String;JJJ)V", "getAddress", "()Ljava/lang/String;", "getAmountCharged", "()J", "getAmountReceived", "getAmountReturned", "component1", "component2", "component3", "component4", "copy", "describeContents", "", "equals", "", "other", "", "hashCode", "toString", "writeToParcel", "", "parcel", "Landroid/os/Parcel;", "flags", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: SourceReceiver.kt */
public final class SourceReceiver implements StripeModel {
    public static final Parcelable.Creator CREATOR = new Creator();
    private final String address;
    private final long amountCharged;
    private final long amountReceived;
    private final long amountReturned;

    @Metadata(bv = {1, 0, 3}, k = 3, mv = {1, 1, 16})
    public static class Creator implements Parcelable.Creator {
        public final Object createFromParcel(Parcel parcel) {
            Intrinsics.checkParameterIsNotNull(parcel, "in");
            return new SourceReceiver(parcel.readString(), parcel.readLong(), parcel.readLong(), parcel.readLong());
        }

        public final Object[] newArray(int i) {
            return new SourceReceiver[i];
        }
    }

    public static /* synthetic */ SourceReceiver copy$default(SourceReceiver sourceReceiver, String str, long j, long j2, long j3, int i, Object obj) {
        if ((i & 1) != 0) {
            str = sourceReceiver.address;
        }
        if ((i & 2) != 0) {
            j = sourceReceiver.amountCharged;
        }
        long j4 = j;
        if ((i & 4) != 0) {
            j2 = sourceReceiver.amountReceived;
        }
        long j5 = j2;
        if ((i & 8) != 0) {
            j3 = sourceReceiver.amountReturned;
        }
        return sourceReceiver.copy(str, j4, j5, j3);
    }

    public final String component1() {
        return this.address;
    }

    public final long component2() {
        return this.amountCharged;
    }

    public final long component3() {
        return this.amountReceived;
    }

    public final long component4() {
        return this.amountReturned;
    }

    public final SourceReceiver copy(String str, long j, long j2, long j3) {
        return new SourceReceiver(str, j, j2, j3);
    }

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof SourceReceiver)) {
            return false;
        }
        SourceReceiver sourceReceiver = (SourceReceiver) obj;
        return Intrinsics.areEqual((Object) this.address, (Object) sourceReceiver.address) && this.amountCharged == sourceReceiver.amountCharged && this.amountReceived == sourceReceiver.amountReceived && this.amountReturned == sourceReceiver.amountReturned;
    }

    public int hashCode() {
        String str = this.address;
        int hashCode = str != null ? str.hashCode() : 0;
        long j = this.amountCharged;
        long j2 = this.amountReceived;
        long j3 = this.amountReturned;
        return (((((hashCode * 31) + ((int) (j ^ (j >>> 32)))) * 31) + ((int) (j2 ^ (j2 >>> 32)))) * 31) + ((int) (j3 ^ (j3 >>> 32)));
    }

    public String toString() {
        return "SourceReceiver(address=" + this.address + ", amountCharged=" + this.amountCharged + ", amountReceived=" + this.amountReceived + ", amountReturned=" + this.amountReturned + ")";
    }

    public void writeToParcel(Parcel parcel, int i) {
        Intrinsics.checkParameterIsNotNull(parcel, "parcel");
        parcel.writeString(this.address);
        parcel.writeLong(this.amountCharged);
        parcel.writeLong(this.amountReceived);
        parcel.writeLong(this.amountReturned);
    }

    public SourceReceiver(String str, long j, long j2, long j3) {
        this.address = str;
        this.amountCharged = j;
        this.amountReceived = j2;
        this.amountReturned = j3;
    }

    public final String getAddress() {
        return this.address;
    }

    public final long getAmountCharged() {
        return this.amountCharged;
    }

    public final long getAmountReceived() {
        return this.amountReceived;
    }

    public final long getAmountReturned() {
        return this.amountReturned;
    }
}
