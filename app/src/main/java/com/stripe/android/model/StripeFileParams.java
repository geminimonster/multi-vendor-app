package com.stripe.android.model;

import android.os.Parcel;
import android.os.Parcelable;
import java.io.File;
import java.util.LinkedHashMap;
import java.util.Map;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\b\b\u0018\u00002\u00020\u0001:\u0001\u0019B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\u000e\u0010\r\u001a\u00020\u0003HÀ\u0003¢\u0006\u0002\b\u000eJ\u000e\u0010\u000f\u001a\u00020\u0005HÀ\u0003¢\u0006\u0002\b\u0010J\u001d\u0010\u0011\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u0005HÆ\u0001J\u0013\u0010\u0012\u001a\u00020\u00132\b\u0010\u0014\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0015\u001a\u00020\u0016HÖ\u0001J\t\u0010\u0017\u001a\u00020\u0018HÖ\u0001R\u0014\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0010\u0010\t\u001a\u0004\u0018\u00010\nX\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\f¨\u0006\u001a"}, d2 = {"Lcom/stripe/android/model/StripeFileParams;", "", "file", "Ljava/io/File;", "purpose", "Lcom/stripe/android/model/StripeFilePurpose;", "(Ljava/io/File;Lcom/stripe/android/model/StripeFilePurpose;)V", "getFile$stripe_release", "()Ljava/io/File;", "fileLink", "Lcom/stripe/android/model/StripeFileParams$FileLink;", "getPurpose$stripe_release", "()Lcom/stripe/android/model/StripeFilePurpose;", "component1", "component1$stripe_release", "component2", "component2$stripe_release", "copy", "equals", "", "other", "hashCode", "", "toString", "", "FileLink", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: StripeFileParams.kt */
public final class StripeFileParams {
    private final File file;
    private final FileLink fileLink;
    private final StripeFilePurpose purpose;

    public static /* synthetic */ StripeFileParams copy$default(StripeFileParams stripeFileParams, File file2, StripeFilePurpose stripeFilePurpose, int i, Object obj) {
        if ((i & 1) != 0) {
            file2 = stripeFileParams.file;
        }
        if ((i & 2) != 0) {
            stripeFilePurpose = stripeFileParams.purpose;
        }
        return stripeFileParams.copy(file2, stripeFilePurpose);
    }

    public final File component1$stripe_release() {
        return this.file;
    }

    public final StripeFilePurpose component2$stripe_release() {
        return this.purpose;
    }

    public final StripeFileParams copy(File file2, StripeFilePurpose stripeFilePurpose) {
        Intrinsics.checkParameterIsNotNull(file2, "file");
        Intrinsics.checkParameterIsNotNull(stripeFilePurpose, "purpose");
        return new StripeFileParams(file2, stripeFilePurpose);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof StripeFileParams)) {
            return false;
        }
        StripeFileParams stripeFileParams = (StripeFileParams) obj;
        return Intrinsics.areEqual((Object) this.file, (Object) stripeFileParams.file) && Intrinsics.areEqual((Object) this.purpose, (Object) stripeFileParams.purpose);
    }

    public int hashCode() {
        File file2 = this.file;
        int i = 0;
        int hashCode = (file2 != null ? file2.hashCode() : 0) * 31;
        StripeFilePurpose stripeFilePurpose = this.purpose;
        if (stripeFilePurpose != null) {
            i = stripeFilePurpose.hashCode();
        }
        return hashCode + i;
    }

    public String toString() {
        return "StripeFileParams(file=" + this.file + ", purpose=" + this.purpose + ")";
    }

    public StripeFileParams(File file2, StripeFilePurpose stripeFilePurpose) {
        Intrinsics.checkParameterIsNotNull(file2, "file");
        Intrinsics.checkParameterIsNotNull(stripeFilePurpose, "purpose");
        this.file = file2;
        this.purpose = stripeFilePurpose;
    }

    public final File getFile$stripe_release() {
        return this.file;
    }

    public final StripeFilePurpose getPurpose$stripe_release() {
        return this.purpose;
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0010$\n\u0002\u0010\u000e\n\u0002\b\t\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\b\b\u0018\u00002\u00020\u0001B5\b\u0007\u0012\b\b\u0002\u0010\u0002\u001a\u00020\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\u0016\b\u0002\u0010\u0006\u001a\u0010\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\b\u0018\u00010\u0007¢\u0006\u0002\u0010\tJ\t\u0010\u000b\u001a\u00020\u0003HÂ\u0003J\u0010\u0010\f\u001a\u0004\u0018\u00010\u0005HÂ\u0003¢\u0006\u0002\u0010\rJ\u0017\u0010\u000e\u001a\u0010\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\b\u0018\u00010\u0007HÂ\u0003J<\u0010\u000f\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u00052\u0016\b\u0002\u0010\u0006\u001a\u0010\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\b\u0018\u00010\u0007HÆ\u0001¢\u0006\u0002\u0010\u0010J\t\u0010\u0011\u001a\u00020\u0012HÖ\u0001J\u0013\u0010\u0013\u001a\u00020\u00032\b\u0010\u0014\u001a\u0004\u0018\u00010\u0015HÖ\u0003J\t\u0010\u0016\u001a\u00020\u0012HÖ\u0001J\t\u0010\u0017\u001a\u00020\bHÖ\u0001J\u0019\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00020\u001b2\u0006\u0010\u001c\u001a\u00020\u0012HÖ\u0001R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u0012\u0010\u0004\u001a\u0004\u0018\u00010\u0005X\u0004¢\u0006\u0004\n\u0002\u0010\nR\u001c\u0010\u0006\u001a\u0010\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\b\u0018\u00010\u0007X\u0004¢\u0006\u0002\n\u0000¨\u0006\u001d"}, d2 = {"Lcom/stripe/android/model/StripeFileParams$FileLink;", "Landroid/os/Parcelable;", "create", "", "expiresAt", "", "metadata", "", "", "(ZLjava/lang/Long;Ljava/util/Map;)V", "Ljava/lang/Long;", "component1", "component2", "()Ljava/lang/Long;", "component3", "copy", "(ZLjava/lang/Long;Ljava/util/Map;)Lcom/stripe/android/model/StripeFileParams$FileLink;", "describeContents", "", "equals", "other", "", "hashCode", "toString", "writeToParcel", "", "parcel", "Landroid/os/Parcel;", "flags", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: StripeFileParams.kt */
    public static final class FileLink implements Parcelable {
        public static final Parcelable.Creator CREATOR = new Creator();
        private final boolean create;
        private final Long expiresAt;
        private final Map<String, String> metadata;

        @Metadata(bv = {1, 0, 3}, k = 3, mv = {1, 1, 16})
        public static class Creator implements Parcelable.Creator {
            public final Object createFromParcel(Parcel parcel) {
                Intrinsics.checkParameterIsNotNull(parcel, "in");
                boolean z = parcel.readInt() != 0;
                LinkedHashMap linkedHashMap = null;
                Long valueOf = parcel.readInt() != 0 ? Long.valueOf(parcel.readLong()) : null;
                if (parcel.readInt() != 0) {
                    int readInt = parcel.readInt();
                    LinkedHashMap linkedHashMap2 = new LinkedHashMap(readInt);
                    while (readInt != 0) {
                        linkedHashMap2.put(parcel.readString(), parcel.readString());
                        readInt--;
                    }
                    linkedHashMap = linkedHashMap2;
                }
                return new FileLink(z, valueOf, linkedHashMap);
            }

            public final Object[] newArray(int i) {
                return new FileLink[i];
            }
        }

        public FileLink() {
            this(false, (Long) null, (Map) null, 7, (DefaultConstructorMarker) null);
        }

        public FileLink(boolean z) {
            this(z, (Long) null, (Map) null, 6, (DefaultConstructorMarker) null);
        }

        public FileLink(boolean z, Long l) {
            this(z, l, (Map) null, 4, (DefaultConstructorMarker) null);
        }

        private final boolean component1() {
            return this.create;
        }

        private final Long component2() {
            return this.expiresAt;
        }

        private final Map<String, String> component3() {
            return this.metadata;
        }

        public static /* synthetic */ FileLink copy$default(FileLink fileLink, boolean z, Long l, Map<String, String> map, int i, Object obj) {
            if ((i & 1) != 0) {
                z = fileLink.create;
            }
            if ((i & 2) != 0) {
                l = fileLink.expiresAt;
            }
            if ((i & 4) != 0) {
                map = fileLink.metadata;
            }
            return fileLink.copy(z, l, map);
        }

        public final FileLink copy(boolean z, Long l, Map<String, String> map) {
            return new FileLink(z, l, map);
        }

        public int describeContents() {
            return 0;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof FileLink)) {
                return false;
            }
            FileLink fileLink = (FileLink) obj;
            return this.create == fileLink.create && Intrinsics.areEqual((Object) this.expiresAt, (Object) fileLink.expiresAt) && Intrinsics.areEqual((Object) this.metadata, (Object) fileLink.metadata);
        }

        public int hashCode() {
            boolean z = this.create;
            if (z) {
                z = true;
            }
            int i = (z ? 1 : 0) * true;
            Long l = this.expiresAt;
            int i2 = 0;
            int hashCode = (i + (l != null ? l.hashCode() : 0)) * 31;
            Map<String, String> map = this.metadata;
            if (map != null) {
                i2 = map.hashCode();
            }
            return hashCode + i2;
        }

        public String toString() {
            return "FileLink(create=" + this.create + ", expiresAt=" + this.expiresAt + ", metadata=" + this.metadata + ")";
        }

        public void writeToParcel(Parcel parcel, int i) {
            Intrinsics.checkParameterIsNotNull(parcel, "parcel");
            parcel.writeInt(this.create ? 1 : 0);
            Long l = this.expiresAt;
            if (l != null) {
                parcel.writeInt(1);
                parcel.writeLong(l.longValue());
            } else {
                parcel.writeInt(0);
            }
            Map<String, String> map = this.metadata;
            if (map != null) {
                parcel.writeInt(1);
                parcel.writeInt(map.size());
                for (Map.Entry<String, String> next : map.entrySet()) {
                    parcel.writeString(next.getKey());
                    parcel.writeString(next.getValue());
                }
                return;
            }
            parcel.writeInt(0);
        }

        public FileLink(boolean z, Long l, Map<String, String> map) {
            this.create = z;
            this.expiresAt = l;
            this.metadata = map;
        }

        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ FileLink(boolean z, Long l, Map map, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this((i & 1) != 0 ? false : z, (i & 2) != 0 ? null : l, (i & 4) != 0 ? null : map);
        }
    }
}
