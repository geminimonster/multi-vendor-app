package com.stripe.android.model;

import android.os.Parcel;
import android.os.Parcelable;
import androidx.core.view.PointerIconCompat;
import com.stripe.android.model.MandateDataParams;
import com.stripe.android.model.PaymentMethodCreateParams;
import java.util.Map;
import kotlin.Metadata;
import kotlin.Pair;
import kotlin.TuplesKt;
import kotlin.collections.CollectionsKt;
import kotlin.collections.MapsKt;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000V\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\u0010\u0000\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b@\n\u0002\u0010\b\n\u0002\b\t\b\b\u0018\u0000 `2\u00020\u0001:\u0003`abBÁ\u0001\b\u0000\u0012\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u0012\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\u0005\u0012\u0016\b\u0002\u0010\t\u001a\u0010\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u000b\u0018\u00010\n\u0012\u0006\u0010\f\u001a\u00020\u0005\u0012\n\b\u0002\u0010\r\u001a\u0004\u0018\u00010\u0005\u0012\n\b\u0002\u0010\u000e\u001a\u0004\u0018\u00010\u000f\u0012\b\b\u0002\u0010\u0010\u001a\u00020\u000f\u0012\n\b\u0002\u0010\u0011\u001a\u0004\u0018\u00010\u0012\u0012\n\b\u0002\u0010\u0013\u001a\u0004\u0018\u00010\u0005\u0012\n\b\u0002\u0010\u0014\u001a\u0004\u0018\u00010\u0015\u0012\n\b\u0002\u0010\u0016\u001a\u0004\u0018\u00010\u0017\u0012\n\b\u0002\u0010\u0018\u001a\u0004\u0018\u00010\u0019\u0012\n\b\u0002\u0010\u001a\u001a\u0004\u0018\u00010\u0005¢\u0006\u0002\u0010\u001bJ\u000b\u0010F\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000b\u0010G\u001a\u0004\u0018\u00010\u0012HÆ\u0003J\u000b\u0010H\u001a\u0004\u0018\u00010\u0005HÆ\u0003J\u000b\u0010I\u001a\u0004\u0018\u00010\u0015HÆ\u0003J\u000b\u0010J\u001a\u0004\u0018\u00010\u0017HÆ\u0003J\u000b\u0010K\u001a\u0004\u0018\u00010\u0019HÆ\u0003J\u000b\u0010L\u001a\u0004\u0018\u00010\u0005HÆ\u0003J\u000b\u0010M\u001a\u0004\u0018\u00010\u0005HÆ\u0003J\u000b\u0010N\u001a\u0004\u0018\u00010\u0007HÆ\u0003J\u000b\u0010O\u001a\u0004\u0018\u00010\u0005HÆ\u0003J\u0017\u0010P\u001a\u0010\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u000b\u0018\u00010\nHÆ\u0003J\t\u0010Q\u001a\u00020\u0005HÆ\u0003J\u000b\u0010R\u001a\u0004\u0018\u00010\u0005HÆ\u0003J\u0010\u0010S\u001a\u0004\u0018\u00010\u000fHÆ\u0003¢\u0006\u0002\u00107J\t\u0010T\u001a\u00020\u000fHÂ\u0003JÊ\u0001\u0010U\u001a\u00020\u00002\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u00072\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\u00052\u0016\b\u0002\u0010\t\u001a\u0010\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u000b\u0018\u00010\n2\b\b\u0002\u0010\f\u001a\u00020\u00052\n\b\u0002\u0010\r\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\u000e\u001a\u0004\u0018\u00010\u000f2\b\b\u0002\u0010\u0010\u001a\u00020\u000f2\n\b\u0002\u0010\u0011\u001a\u0004\u0018\u00010\u00122\n\b\u0002\u0010\u0013\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\u0014\u001a\u0004\u0018\u00010\u00152\n\b\u0002\u0010\u0016\u001a\u0004\u0018\u00010\u00172\n\b\u0002\u0010\u0018\u001a\u0004\u0018\u00010\u00192\n\b\u0002\u0010\u001a\u001a\u0004\u0018\u00010\u0005HÆ\u0001¢\u0006\u0002\u0010VJ\u0013\u0010W\u001a\u00020\u000f2\b\u0010X\u001a\u0004\u0018\u00010\u000bHÖ\u0003J\t\u0010Y\u001a\u00020ZHÖ\u0001J\u0006\u0010[\u001a\u00020\u000fJ\b\u0010\\\u001a\u00020\u000fH\u0016J\u0014\u0010]\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u000b0\nH\u0016J\t\u0010^\u001a\u00020\u0005HÖ\u0001J\u0010\u0010_\u001a\u00020\u00002\u0006\u0010\\\u001a\u00020\u000fH\u0016R\u0014\u0010\f\u001a\u00020\u0005X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u001c\u0010\u001dR\u001f\u0010\t\u001a\u0010\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u000b\u0018\u00010\n¢\u0006\b\n\u0000\u001a\u0004\b\u001e\u0010\u001fR\u001c\u0010\u0014\u001a\u0004\u0018\u00010\u0015X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b \u0010!\"\u0004\b\"\u0010#R\"\u0010$\u001a\u0010\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u000b\u0018\u00010\n8BX\u0004¢\u0006\u0006\u001a\u0004\b%\u0010\u001fR\u001c\u0010\u0013\u001a\u0004\u0018\u00010\u0005X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b&\u0010\u001d\"\u0004\b'\u0010(R\u0013\u0010\u0002\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b)\u0010*R\u0013\u0010\u0004\u001a\u0004\u0018\u00010\u0005¢\u0006\b\n\u0000\u001a\u0004\b+\u0010\u001dR\u001c\u0010\u0011\u001a\u0004\u0018\u00010\u0012X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b,\u0010-\"\u0004\b.\u0010/R \u00100\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u000b0\n8BX\u0004¢\u0006\u0006\u001a\u0004\b1\u0010\u001fR\u001c\u0010\u001a\u001a\u0004\u0018\u00010\u0005X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b2\u0010\u001d\"\u0004\b3\u0010(R\u001c\u0010\r\u001a\u0004\u0018\u00010\u0005X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b4\u0010\u001d\"\u0004\b5\u0010(R\u001e\u0010\u000e\u001a\u0004\u0018\u00010\u000fX\u000e¢\u0006\u0010\n\u0002\u0010:\u001a\u0004\b6\u00107\"\u0004\b8\u00109R\u001c\u0010\u0016\u001a\u0004\u0018\u00010\u0017X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b;\u0010<\"\u0004\b=\u0010>R\u001c\u0010\u0018\u001a\u0004\u0018\u00010\u0019X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b?\u0010@\"\u0004\bA\u0010BR\u0013\u0010\b\u001a\u0004\u0018\u00010\u0005¢\u0006\b\n\u0000\u001a\u0004\bC\u0010\u001dR\u0013\u0010\u0006\u001a\u0004\u0018\u00010\u0007¢\u0006\b\n\u0000\u001a\u0004\bD\u0010ER\u000e\u0010\u0010\u001a\u00020\u000fX\u0004¢\u0006\u0002\n\u0000¨\u0006c"}, d2 = {"Lcom/stripe/android/model/ConfirmPaymentIntentParams;", "Lcom/stripe/android/model/ConfirmStripeIntentParams;", "paymentMethodCreateParams", "Lcom/stripe/android/model/PaymentMethodCreateParams;", "paymentMethodId", "", "sourceParams", "Lcom/stripe/android/model/SourceParams;", "sourceId", "extraParams", "", "", "clientSecret", "returnUrl", "savePaymentMethod", "", "useStripeSdk", "paymentMethodOptions", "Lcom/stripe/android/model/PaymentMethodOptionsParams;", "mandateId", "mandateData", "Lcom/stripe/android/model/MandateDataParams;", "setupFutureUsage", "Lcom/stripe/android/model/ConfirmPaymentIntentParams$SetupFutureUsage;", "shipping", "Lcom/stripe/android/model/ConfirmPaymentIntentParams$Shipping;", "receiptEmail", "(Lcom/stripe/android/model/PaymentMethodCreateParams;Ljava/lang/String;Lcom/stripe/android/model/SourceParams;Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;ZLcom/stripe/android/model/PaymentMethodOptionsParams;Ljava/lang/String;Lcom/stripe/android/model/MandateDataParams;Lcom/stripe/android/model/ConfirmPaymentIntentParams$SetupFutureUsage;Lcom/stripe/android/model/ConfirmPaymentIntentParams$Shipping;Ljava/lang/String;)V", "getClientSecret", "()Ljava/lang/String;", "getExtraParams", "()Ljava/util/Map;", "getMandateData", "()Lcom/stripe/android/model/MandateDataParams;", "setMandateData", "(Lcom/stripe/android/model/MandateDataParams;)V", "mandateDataParams", "getMandateDataParams", "getMandateId", "setMandateId", "(Ljava/lang/String;)V", "getPaymentMethodCreateParams", "()Lcom/stripe/android/model/PaymentMethodCreateParams;", "getPaymentMethodId", "getPaymentMethodOptions", "()Lcom/stripe/android/model/PaymentMethodOptionsParams;", "setPaymentMethodOptions", "(Lcom/stripe/android/model/PaymentMethodOptionsParams;)V", "paymentMethodParamMap", "getPaymentMethodParamMap", "getReceiptEmail", "setReceiptEmail", "getReturnUrl", "setReturnUrl", "getSavePaymentMethod", "()Ljava/lang/Boolean;", "setSavePaymentMethod", "(Ljava/lang/Boolean;)V", "Ljava/lang/Boolean;", "getSetupFutureUsage", "()Lcom/stripe/android/model/ConfirmPaymentIntentParams$SetupFutureUsage;", "setSetupFutureUsage", "(Lcom/stripe/android/model/ConfirmPaymentIntentParams$SetupFutureUsage;)V", "getShipping", "()Lcom/stripe/android/model/ConfirmPaymentIntentParams$Shipping;", "setShipping", "(Lcom/stripe/android/model/ConfirmPaymentIntentParams$Shipping;)V", "getSourceId", "getSourceParams", "()Lcom/stripe/android/model/SourceParams;", "component1", "component10", "component11", "component12", "component13", "component14", "component15", "component2", "component3", "component4", "component5", "component6", "component7", "component8", "component9", "copy", "(Lcom/stripe/android/model/PaymentMethodCreateParams;Ljava/lang/String;Lcom/stripe/android/model/SourceParams;Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;ZLcom/stripe/android/model/PaymentMethodOptionsParams;Ljava/lang/String;Lcom/stripe/android/model/MandateDataParams;Lcom/stripe/android/model/ConfirmPaymentIntentParams$SetupFutureUsage;Lcom/stripe/android/model/ConfirmPaymentIntentParams$Shipping;Ljava/lang/String;)Lcom/stripe/android/model/ConfirmPaymentIntentParams;", "equals", "other", "hashCode", "", "shouldSavePaymentMethod", "shouldUseStripeSdk", "toParamMap", "toString", "withShouldUseStripeSdk", "Companion", "SetupFutureUsage", "Shipping", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: ConfirmPaymentIntentParams.kt */
public final class ConfirmPaymentIntentParams implements ConfirmStripeIntentParams {
    public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
    private static final String PARAM_PAYMENT_METHOD_OPTIONS = "payment_method_options";
    private static final String PARAM_RECEIPT_EMAIL = "receipt_email";
    private static final String PARAM_SAVE_PAYMENT_METHOD = "save_payment_method";
    private static final String PARAM_SETUP_FUTURE_USAGE = "setup_future_usage";
    private static final String PARAM_SHIPPING = "shipping";
    public static final String PARAM_SOURCE_DATA = "source_data";
    private static final String PARAM_SOURCE_ID = "source";
    private final String clientSecret;
    private final Map<String, Object> extraParams;
    private MandateDataParams mandateData;
    private String mandateId;
    private final PaymentMethodCreateParams paymentMethodCreateParams;
    private final String paymentMethodId;
    private PaymentMethodOptionsParams paymentMethodOptions;
    private String receiptEmail;
    private String returnUrl;
    private Boolean savePaymentMethod;
    private SetupFutureUsage setupFutureUsage;
    private Shipping shipping;
    private final String sourceId;
    private final SourceParams sourceParams;
    private final boolean useStripeSdk;

    private final boolean component9() {
        return this.useStripeSdk;
    }

    public static /* synthetic */ ConfirmPaymentIntentParams copy$default(ConfirmPaymentIntentParams confirmPaymentIntentParams, PaymentMethodCreateParams paymentMethodCreateParams2, String str, SourceParams sourceParams2, String str2, Map map, String str3, String str4, Boolean bool, boolean z, PaymentMethodOptionsParams paymentMethodOptionsParams, String str5, MandateDataParams mandateDataParams, SetupFutureUsage setupFutureUsage2, Shipping shipping2, String str6, int i, Object obj) {
        ConfirmPaymentIntentParams confirmPaymentIntentParams2 = confirmPaymentIntentParams;
        int i2 = i;
        return confirmPaymentIntentParams.copy((i2 & 1) != 0 ? confirmPaymentIntentParams2.paymentMethodCreateParams : paymentMethodCreateParams2, (i2 & 2) != 0 ? confirmPaymentIntentParams2.paymentMethodId : str, (i2 & 4) != 0 ? confirmPaymentIntentParams2.sourceParams : sourceParams2, (i2 & 8) != 0 ? confirmPaymentIntentParams2.sourceId : str2, (i2 & 16) != 0 ? confirmPaymentIntentParams2.extraParams : map, (i2 & 32) != 0 ? confirmPaymentIntentParams.getClientSecret() : str3, (i2 & 64) != 0 ? confirmPaymentIntentParams2.returnUrl : str4, (i2 & 128) != 0 ? confirmPaymentIntentParams2.savePaymentMethod : bool, (i2 & 256) != 0 ? confirmPaymentIntentParams2.useStripeSdk : z, (i2 & 512) != 0 ? confirmPaymentIntentParams2.paymentMethodOptions : paymentMethodOptionsParams, (i2 & 1024) != 0 ? confirmPaymentIntentParams2.mandateId : str5, (i2 & 2048) != 0 ? confirmPaymentIntentParams2.mandateData : mandateDataParams, (i2 & 4096) != 0 ? confirmPaymentIntentParams2.setupFutureUsage : setupFutureUsage2, (i2 & 8192) != 0 ? confirmPaymentIntentParams2.shipping : shipping2, (i2 & 16384) != 0 ? confirmPaymentIntentParams2.receiptEmail : str6);
    }

    @JvmStatic
    public static final ConfirmPaymentIntentParams create(String str) {
        return Companion.create$default(Companion, str, (String) null, (Map) null, (Shipping) null, 14, (Object) null);
    }

    @JvmStatic
    public static final ConfirmPaymentIntentParams create(String str, String str2) {
        return Companion.create$default(Companion, str, str2, (Map) null, (Shipping) null, 12, (Object) null);
    }

    @JvmStatic
    public static final ConfirmPaymentIntentParams create(String str, String str2, Map<String, ? extends Object> map) {
        return Companion.create$default(Companion, str, str2, map, (Shipping) null, 8, (Object) null);
    }

    @JvmStatic
    public static final ConfirmPaymentIntentParams create(String str, String str2, Map<String, ? extends Object> map, Shipping shipping2) {
        return Companion.create(str, str2, map, shipping2);
    }

    @JvmStatic
    public static final ConfirmPaymentIntentParams createAlipay$stripe_release(String str, String str2) {
        return Companion.createAlipay$stripe_release(str, str2);
    }

    @JvmStatic
    public static final ConfirmPaymentIntentParams createWithPaymentMethodCreateParams(PaymentMethodCreateParams paymentMethodCreateParams2, String str) {
        return Companion.createWithPaymentMethodCreateParams$default(Companion, paymentMethodCreateParams2, str, (String) null, (Boolean) null, (Map) null, (String) null, (MandateDataParams) null, (SetupFutureUsage) null, (Shipping) null, 508, (Object) null);
    }

    @JvmStatic
    public static final ConfirmPaymentIntentParams createWithPaymentMethodCreateParams(PaymentMethodCreateParams paymentMethodCreateParams2, String str, String str2) {
        return Companion.createWithPaymentMethodCreateParams$default(Companion, paymentMethodCreateParams2, str, str2, (Boolean) null, (Map) null, (String) null, (MandateDataParams) null, (SetupFutureUsage) null, (Shipping) null, 504, (Object) null);
    }

    @JvmStatic
    public static final ConfirmPaymentIntentParams createWithPaymentMethodCreateParams(PaymentMethodCreateParams paymentMethodCreateParams2, String str, String str2, Boolean bool) {
        return Companion.createWithPaymentMethodCreateParams$default(Companion, paymentMethodCreateParams2, str, str2, bool, (Map) null, (String) null, (MandateDataParams) null, (SetupFutureUsage) null, (Shipping) null, 496, (Object) null);
    }

    @JvmStatic
    public static final ConfirmPaymentIntentParams createWithPaymentMethodCreateParams(PaymentMethodCreateParams paymentMethodCreateParams2, String str, String str2, Boolean bool, Map<String, ? extends Object> map) {
        return Companion.createWithPaymentMethodCreateParams$default(Companion, paymentMethodCreateParams2, str, str2, bool, map, (String) null, (MandateDataParams) null, (SetupFutureUsage) null, (Shipping) null, 480, (Object) null);
    }

    @JvmStatic
    public static final ConfirmPaymentIntentParams createWithPaymentMethodCreateParams(PaymentMethodCreateParams paymentMethodCreateParams2, String str, String str2, Boolean bool, Map<String, ? extends Object> map, String str3) {
        return Companion.createWithPaymentMethodCreateParams$default(Companion, paymentMethodCreateParams2, str, str2, bool, map, str3, (MandateDataParams) null, (SetupFutureUsage) null, (Shipping) null, 448, (Object) null);
    }

    @JvmStatic
    public static final ConfirmPaymentIntentParams createWithPaymentMethodCreateParams(PaymentMethodCreateParams paymentMethodCreateParams2, String str, String str2, Boolean bool, Map<String, ? extends Object> map, String str3, MandateDataParams mandateDataParams) {
        return Companion.createWithPaymentMethodCreateParams$default(Companion, paymentMethodCreateParams2, str, str2, bool, map, str3, mandateDataParams, (SetupFutureUsage) null, (Shipping) null, 384, (Object) null);
    }

    @JvmStatic
    public static final ConfirmPaymentIntentParams createWithPaymentMethodCreateParams(PaymentMethodCreateParams paymentMethodCreateParams2, String str, String str2, Boolean bool, Map<String, ? extends Object> map, String str3, MandateDataParams mandateDataParams, SetupFutureUsage setupFutureUsage2) {
        return Companion.createWithPaymentMethodCreateParams$default(Companion, paymentMethodCreateParams2, str, str2, bool, map, str3, mandateDataParams, setupFutureUsage2, (Shipping) null, 256, (Object) null);
    }

    @JvmStatic
    public static final ConfirmPaymentIntentParams createWithPaymentMethodCreateParams(PaymentMethodCreateParams paymentMethodCreateParams2, String str, String str2, Boolean bool, Map<String, ? extends Object> map, String str3, MandateDataParams mandateDataParams, SetupFutureUsage setupFutureUsage2, Shipping shipping2) {
        return Companion.createWithPaymentMethodCreateParams(paymentMethodCreateParams2, str, str2, bool, map, str3, mandateDataParams, setupFutureUsage2, shipping2);
    }

    @JvmStatic
    public static final ConfirmPaymentIntentParams createWithPaymentMethodId(String str, String str2) {
        return Companion.createWithPaymentMethodId$default(Companion, str, str2, (String) null, (Boolean) null, (Map) null, (PaymentMethodOptionsParams) null, (String) null, (MandateDataParams) null, (SetupFutureUsage) null, (Shipping) null, PointerIconCompat.TYPE_GRAB, (Object) null);
    }

    @JvmStatic
    public static final ConfirmPaymentIntentParams createWithPaymentMethodId(String str, String str2, String str3) {
        return Companion.createWithPaymentMethodId$default(Companion, str, str2, str3, (Boolean) null, (Map) null, (PaymentMethodOptionsParams) null, (String) null, (MandateDataParams) null, (SetupFutureUsage) null, (Shipping) null, PointerIconCompat.TYPE_TOP_RIGHT_DIAGONAL_DOUBLE_ARROW, (Object) null);
    }

    @JvmStatic
    public static final ConfirmPaymentIntentParams createWithPaymentMethodId(String str, String str2, String str3, Boolean bool) {
        return Companion.createWithPaymentMethodId$default(Companion, str, str2, str3, bool, (Map) null, (PaymentMethodOptionsParams) null, (String) null, (MandateDataParams) null, (SetupFutureUsage) null, (Shipping) null, PointerIconCompat.TYPE_TEXT, (Object) null);
    }

    @JvmStatic
    public static final ConfirmPaymentIntentParams createWithPaymentMethodId(String str, String str2, String str3, Boolean bool, Map<String, ? extends Object> map) {
        return Companion.createWithPaymentMethodId$default(Companion, str, str2, str3, bool, map, (PaymentMethodOptionsParams) null, (String) null, (MandateDataParams) null, (SetupFutureUsage) null, (Shipping) null, 992, (Object) null);
    }

    @JvmStatic
    public static final ConfirmPaymentIntentParams createWithPaymentMethodId(String str, String str2, String str3, Boolean bool, Map<String, ? extends Object> map, PaymentMethodOptionsParams paymentMethodOptionsParams) {
        return Companion.createWithPaymentMethodId$default(Companion, str, str2, str3, bool, map, paymentMethodOptionsParams, (String) null, (MandateDataParams) null, (SetupFutureUsage) null, (Shipping) null, 960, (Object) null);
    }

    @JvmStatic
    public static final ConfirmPaymentIntentParams createWithPaymentMethodId(String str, String str2, String str3, Boolean bool, Map<String, ? extends Object> map, PaymentMethodOptionsParams paymentMethodOptionsParams, String str4) {
        return Companion.createWithPaymentMethodId$default(Companion, str, str2, str3, bool, map, paymentMethodOptionsParams, str4, (MandateDataParams) null, (SetupFutureUsage) null, (Shipping) null, 896, (Object) null);
    }

    @JvmStatic
    public static final ConfirmPaymentIntentParams createWithPaymentMethodId(String str, String str2, String str3, Boolean bool, Map<String, ? extends Object> map, PaymentMethodOptionsParams paymentMethodOptionsParams, String str4, MandateDataParams mandateDataParams) {
        return Companion.createWithPaymentMethodId$default(Companion, str, str2, str3, bool, map, paymentMethodOptionsParams, str4, mandateDataParams, (SetupFutureUsage) null, (Shipping) null, 768, (Object) null);
    }

    @JvmStatic
    public static final ConfirmPaymentIntentParams createWithPaymentMethodId(String str, String str2, String str3, Boolean bool, Map<String, ? extends Object> map, PaymentMethodOptionsParams paymentMethodOptionsParams, String str4, MandateDataParams mandateDataParams, SetupFutureUsage setupFutureUsage2) {
        return Companion.createWithPaymentMethodId$default(Companion, str, str2, str3, bool, map, paymentMethodOptionsParams, str4, mandateDataParams, setupFutureUsage2, (Shipping) null, 512, (Object) null);
    }

    @JvmStatic
    public static final ConfirmPaymentIntentParams createWithPaymentMethodId(String str, String str2, String str3, Boolean bool, Map<String, ? extends Object> map, PaymentMethodOptionsParams paymentMethodOptionsParams, String str4, MandateDataParams mandateDataParams, SetupFutureUsage setupFutureUsage2, Shipping shipping2) {
        return Companion.createWithPaymentMethodId(str, str2, str3, bool, map, paymentMethodOptionsParams, str4, mandateDataParams, setupFutureUsage2, shipping2);
    }

    @JvmStatic
    public static final ConfirmPaymentIntentParams createWithSourceId(String str, String str2, String str3) {
        return Companion.createWithSourceId$default(Companion, str, str2, str3, (Boolean) null, (Map) null, (Shipping) null, 56, (Object) null);
    }

    @JvmStatic
    public static final ConfirmPaymentIntentParams createWithSourceId(String str, String str2, String str3, Boolean bool) {
        return Companion.createWithSourceId$default(Companion, str, str2, str3, bool, (Map) null, (Shipping) null, 48, (Object) null);
    }

    @JvmStatic
    public static final ConfirmPaymentIntentParams createWithSourceId(String str, String str2, String str3, Boolean bool, Map<String, ? extends Object> map) {
        return Companion.createWithSourceId$default(Companion, str, str2, str3, bool, map, (Shipping) null, 32, (Object) null);
    }

    @JvmStatic
    public static final ConfirmPaymentIntentParams createWithSourceId(String str, String str2, String str3, Boolean bool, Map<String, ? extends Object> map, Shipping shipping2) {
        return Companion.createWithSourceId(str, str2, str3, bool, map, shipping2);
    }

    @JvmStatic
    public static final ConfirmPaymentIntentParams createWithSourceParams(SourceParams sourceParams2, String str, String str2) {
        return Companion.createWithSourceParams$default(Companion, sourceParams2, str, str2, (Boolean) null, (Map) null, (Shipping) null, 56, (Object) null);
    }

    @JvmStatic
    public static final ConfirmPaymentIntentParams createWithSourceParams(SourceParams sourceParams2, String str, String str2, Boolean bool) {
        return Companion.createWithSourceParams$default(Companion, sourceParams2, str, str2, bool, (Map) null, (Shipping) null, 48, (Object) null);
    }

    @JvmStatic
    public static final ConfirmPaymentIntentParams createWithSourceParams(SourceParams sourceParams2, String str, String str2, Boolean bool, Map<String, ? extends Object> map) {
        return Companion.createWithSourceParams$default(Companion, sourceParams2, str, str2, bool, map, (Shipping) null, 32, (Object) null);
    }

    @JvmStatic
    public static final ConfirmPaymentIntentParams createWithSourceParams(SourceParams sourceParams2, String str, String str2, Boolean bool, Map<String, ? extends Object> map, Shipping shipping2) {
        return Companion.createWithSourceParams(sourceParams2, str, str2, bool, map, shipping2);
    }

    public final PaymentMethodCreateParams component1() {
        return this.paymentMethodCreateParams;
    }

    public final PaymentMethodOptionsParams component10() {
        return this.paymentMethodOptions;
    }

    public final String component11() {
        return this.mandateId;
    }

    public final MandateDataParams component12() {
        return this.mandateData;
    }

    public final SetupFutureUsage component13() {
        return this.setupFutureUsage;
    }

    public final Shipping component14() {
        return this.shipping;
    }

    public final String component15() {
        return this.receiptEmail;
    }

    public final String component2() {
        return this.paymentMethodId;
    }

    public final SourceParams component3() {
        return this.sourceParams;
    }

    public final String component4() {
        return this.sourceId;
    }

    public final Map<String, Object> component5() {
        return this.extraParams;
    }

    public final String component6() {
        return getClientSecret();
    }

    public final String component7() {
        return this.returnUrl;
    }

    public final Boolean component8() {
        return this.savePaymentMethod;
    }

    public final ConfirmPaymentIntentParams copy(PaymentMethodCreateParams paymentMethodCreateParams2, String str, SourceParams sourceParams2, String str2, Map<String, ? extends Object> map, String str3, String str4, Boolean bool, boolean z, PaymentMethodOptionsParams paymentMethodOptionsParams, String str5, MandateDataParams mandateDataParams, SetupFutureUsage setupFutureUsage2, Shipping shipping2, String str6) {
        String str7 = str3;
        Intrinsics.checkParameterIsNotNull(str7, "clientSecret");
        return new ConfirmPaymentIntentParams(paymentMethodCreateParams2, str, sourceParams2, str2, map, str7, str4, bool, z, paymentMethodOptionsParams, str5, mandateDataParams, setupFutureUsage2, shipping2, str6);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ConfirmPaymentIntentParams)) {
            return false;
        }
        ConfirmPaymentIntentParams confirmPaymentIntentParams = (ConfirmPaymentIntentParams) obj;
        return Intrinsics.areEqual((Object) this.paymentMethodCreateParams, (Object) confirmPaymentIntentParams.paymentMethodCreateParams) && Intrinsics.areEqual((Object) this.paymentMethodId, (Object) confirmPaymentIntentParams.paymentMethodId) && Intrinsics.areEqual((Object) this.sourceParams, (Object) confirmPaymentIntentParams.sourceParams) && Intrinsics.areEqual((Object) this.sourceId, (Object) confirmPaymentIntentParams.sourceId) && Intrinsics.areEqual((Object) this.extraParams, (Object) confirmPaymentIntentParams.extraParams) && Intrinsics.areEqual((Object) getClientSecret(), (Object) confirmPaymentIntentParams.getClientSecret()) && Intrinsics.areEqual((Object) this.returnUrl, (Object) confirmPaymentIntentParams.returnUrl) && Intrinsics.areEqual((Object) this.savePaymentMethod, (Object) confirmPaymentIntentParams.savePaymentMethod) && this.useStripeSdk == confirmPaymentIntentParams.useStripeSdk && Intrinsics.areEqual((Object) this.paymentMethodOptions, (Object) confirmPaymentIntentParams.paymentMethodOptions) && Intrinsics.areEqual((Object) this.mandateId, (Object) confirmPaymentIntentParams.mandateId) && Intrinsics.areEqual((Object) this.mandateData, (Object) confirmPaymentIntentParams.mandateData) && Intrinsics.areEqual((Object) this.setupFutureUsage, (Object) confirmPaymentIntentParams.setupFutureUsage) && Intrinsics.areEqual((Object) this.shipping, (Object) confirmPaymentIntentParams.shipping) && Intrinsics.areEqual((Object) this.receiptEmail, (Object) confirmPaymentIntentParams.receiptEmail);
    }

    public int hashCode() {
        PaymentMethodCreateParams paymentMethodCreateParams2 = this.paymentMethodCreateParams;
        int i = 0;
        int hashCode = (paymentMethodCreateParams2 != null ? paymentMethodCreateParams2.hashCode() : 0) * 31;
        String str = this.paymentMethodId;
        int hashCode2 = (hashCode + (str != null ? str.hashCode() : 0)) * 31;
        SourceParams sourceParams2 = this.sourceParams;
        int hashCode3 = (hashCode2 + (sourceParams2 != null ? sourceParams2.hashCode() : 0)) * 31;
        String str2 = this.sourceId;
        int hashCode4 = (hashCode3 + (str2 != null ? str2.hashCode() : 0)) * 31;
        Map<String, Object> map = this.extraParams;
        int hashCode5 = (hashCode4 + (map != null ? map.hashCode() : 0)) * 31;
        String clientSecret2 = getClientSecret();
        int hashCode6 = (hashCode5 + (clientSecret2 != null ? clientSecret2.hashCode() : 0)) * 31;
        String str3 = this.returnUrl;
        int hashCode7 = (hashCode6 + (str3 != null ? str3.hashCode() : 0)) * 31;
        Boolean bool = this.savePaymentMethod;
        int hashCode8 = (hashCode7 + (bool != null ? bool.hashCode() : 0)) * 31;
        boolean z = this.useStripeSdk;
        if (z) {
            z = true;
        }
        int i2 = (hashCode8 + (z ? 1 : 0)) * 31;
        PaymentMethodOptionsParams paymentMethodOptionsParams = this.paymentMethodOptions;
        int hashCode9 = (i2 + (paymentMethodOptionsParams != null ? paymentMethodOptionsParams.hashCode() : 0)) * 31;
        String str4 = this.mandateId;
        int hashCode10 = (hashCode9 + (str4 != null ? str4.hashCode() : 0)) * 31;
        MandateDataParams mandateDataParams = this.mandateData;
        int hashCode11 = (hashCode10 + (mandateDataParams != null ? mandateDataParams.hashCode() : 0)) * 31;
        SetupFutureUsage setupFutureUsage2 = this.setupFutureUsage;
        int hashCode12 = (hashCode11 + (setupFutureUsage2 != null ? setupFutureUsage2.hashCode() : 0)) * 31;
        Shipping shipping2 = this.shipping;
        int hashCode13 = (hashCode12 + (shipping2 != null ? shipping2.hashCode() : 0)) * 31;
        String str5 = this.receiptEmail;
        if (str5 != null) {
            i = str5.hashCode();
        }
        return hashCode13 + i;
    }

    public String toString() {
        return "ConfirmPaymentIntentParams(paymentMethodCreateParams=" + this.paymentMethodCreateParams + ", paymentMethodId=" + this.paymentMethodId + ", sourceParams=" + this.sourceParams + ", sourceId=" + this.sourceId + ", extraParams=" + this.extraParams + ", clientSecret=" + getClientSecret() + ", returnUrl=" + this.returnUrl + ", savePaymentMethod=" + this.savePaymentMethod + ", useStripeSdk=" + this.useStripeSdk + ", paymentMethodOptions=" + this.paymentMethodOptions + ", mandateId=" + this.mandateId + ", mandateData=" + this.mandateData + ", setupFutureUsage=" + this.setupFutureUsage + ", shipping=" + this.shipping + ", receiptEmail=" + this.receiptEmail + ")";
    }

    public ConfirmPaymentIntentParams(PaymentMethodCreateParams paymentMethodCreateParams2, String str, SourceParams sourceParams2, String str2, Map<String, ? extends Object> map, String str3, String str4, Boolean bool, boolean z, PaymentMethodOptionsParams paymentMethodOptionsParams, String str5, MandateDataParams mandateDataParams, SetupFutureUsage setupFutureUsage2, Shipping shipping2, String str6) {
        Intrinsics.checkParameterIsNotNull(str3, "clientSecret");
        this.paymentMethodCreateParams = paymentMethodCreateParams2;
        this.paymentMethodId = str;
        this.sourceParams = sourceParams2;
        this.sourceId = str2;
        this.extraParams = map;
        this.clientSecret = str3;
        this.returnUrl = str4;
        this.savePaymentMethod = bool;
        this.useStripeSdk = z;
        this.paymentMethodOptions = paymentMethodOptionsParams;
        this.mandateId = str5;
        this.mandateData = mandateDataParams;
        this.setupFutureUsage = setupFutureUsage2;
        this.shipping = shipping2;
        this.receiptEmail = str6;
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* synthetic */ ConfirmPaymentIntentParams(com.stripe.android.model.PaymentMethodCreateParams r20, java.lang.String r21, com.stripe.android.model.SourceParams r22, java.lang.String r23, java.util.Map r24, java.lang.String r25, java.lang.String r26, java.lang.Boolean r27, boolean r28, com.stripe.android.model.PaymentMethodOptionsParams r29, java.lang.String r30, com.stripe.android.model.MandateDataParams r31, com.stripe.android.model.ConfirmPaymentIntentParams.SetupFutureUsage r32, com.stripe.android.model.ConfirmPaymentIntentParams.Shipping r33, java.lang.String r34, int r35, kotlin.jvm.internal.DefaultConstructorMarker r36) {
        /*
            r19 = this;
            r0 = r35
            r1 = r0 & 1
            r2 = 0
            if (r1 == 0) goto L_0x000c
            r1 = r2
            com.stripe.android.model.PaymentMethodCreateParams r1 = (com.stripe.android.model.PaymentMethodCreateParams) r1
            r4 = r1
            goto L_0x000e
        L_0x000c:
            r4 = r20
        L_0x000e:
            r1 = r0 & 2
            if (r1 == 0) goto L_0x0017
            r1 = r2
            java.lang.String r1 = (java.lang.String) r1
            r5 = r1
            goto L_0x0019
        L_0x0017:
            r5 = r21
        L_0x0019:
            r1 = r0 & 4
            if (r1 == 0) goto L_0x0022
            r1 = r2
            com.stripe.android.model.SourceParams r1 = (com.stripe.android.model.SourceParams) r1
            r6 = r1
            goto L_0x0024
        L_0x0022:
            r6 = r22
        L_0x0024:
            r1 = r0 & 8
            if (r1 == 0) goto L_0x002d
            r1 = r2
            java.lang.String r1 = (java.lang.String) r1
            r7 = r1
            goto L_0x002f
        L_0x002d:
            r7 = r23
        L_0x002f:
            r1 = r0 & 16
            if (r1 == 0) goto L_0x0038
            r1 = r2
            java.util.Map r1 = (java.util.Map) r1
            r8 = r1
            goto L_0x003a
        L_0x0038:
            r8 = r24
        L_0x003a:
            r1 = r0 & 64
            if (r1 == 0) goto L_0x0043
            r1 = r2
            java.lang.String r1 = (java.lang.String) r1
            r10 = r1
            goto L_0x0045
        L_0x0043:
            r10 = r26
        L_0x0045:
            r1 = r0 & 128(0x80, float:1.794E-43)
            if (r1 == 0) goto L_0x004e
            r1 = r2
            java.lang.Boolean r1 = (java.lang.Boolean) r1
            r11 = r1
            goto L_0x0050
        L_0x004e:
            r11 = r27
        L_0x0050:
            r1 = r0 & 256(0x100, float:3.59E-43)
            if (r1 == 0) goto L_0x0057
            r1 = 0
            r12 = 0
            goto L_0x0059
        L_0x0057:
            r12 = r28
        L_0x0059:
            r1 = r0 & 512(0x200, float:7.175E-43)
            if (r1 == 0) goto L_0x0062
            r1 = r2
            com.stripe.android.model.PaymentMethodOptionsParams r1 = (com.stripe.android.model.PaymentMethodOptionsParams) r1
            r13 = r1
            goto L_0x0064
        L_0x0062:
            r13 = r29
        L_0x0064:
            r1 = r0 & 1024(0x400, float:1.435E-42)
            if (r1 == 0) goto L_0x006d
            r1 = r2
            java.lang.String r1 = (java.lang.String) r1
            r14 = r1
            goto L_0x006f
        L_0x006d:
            r14 = r30
        L_0x006f:
            r1 = r0 & 2048(0x800, float:2.87E-42)
            if (r1 == 0) goto L_0x0078
            r1 = r2
            com.stripe.android.model.MandateDataParams r1 = (com.stripe.android.model.MandateDataParams) r1
            r15 = r1
            goto L_0x007a
        L_0x0078:
            r15 = r31
        L_0x007a:
            r1 = r0 & 4096(0x1000, float:5.74E-42)
            if (r1 == 0) goto L_0x0084
            r1 = r2
            com.stripe.android.model.ConfirmPaymentIntentParams$SetupFutureUsage r1 = (com.stripe.android.model.ConfirmPaymentIntentParams.SetupFutureUsage) r1
            r16 = r1
            goto L_0x0086
        L_0x0084:
            r16 = r32
        L_0x0086:
            r1 = r0 & 8192(0x2000, float:1.14794E-41)
            if (r1 == 0) goto L_0x0090
            r1 = r2
            com.stripe.android.model.ConfirmPaymentIntentParams$Shipping r1 = (com.stripe.android.model.ConfirmPaymentIntentParams.Shipping) r1
            r17 = r1
            goto L_0x0092
        L_0x0090:
            r17 = r33
        L_0x0092:
            r0 = r0 & 16384(0x4000, float:2.2959E-41)
            if (r0 == 0) goto L_0x009c
            r0 = r2
            java.lang.String r0 = (java.lang.String) r0
            r18 = r0
            goto L_0x009e
        L_0x009c:
            r18 = r34
        L_0x009e:
            r3 = r19
            r9 = r25
            r3.<init>(r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15, r16, r17, r18)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.stripe.android.model.ConfirmPaymentIntentParams.<init>(com.stripe.android.model.PaymentMethodCreateParams, java.lang.String, com.stripe.android.model.SourceParams, java.lang.String, java.util.Map, java.lang.String, java.lang.String, java.lang.Boolean, boolean, com.stripe.android.model.PaymentMethodOptionsParams, java.lang.String, com.stripe.android.model.MandateDataParams, com.stripe.android.model.ConfirmPaymentIntentParams$SetupFutureUsage, com.stripe.android.model.ConfirmPaymentIntentParams$Shipping, java.lang.String, int, kotlin.jvm.internal.DefaultConstructorMarker):void");
    }

    public final PaymentMethodCreateParams getPaymentMethodCreateParams() {
        return this.paymentMethodCreateParams;
    }

    public final String getPaymentMethodId() {
        return this.paymentMethodId;
    }

    public final SourceParams getSourceParams() {
        return this.sourceParams;
    }

    public final String getSourceId() {
        return this.sourceId;
    }

    public final Map<String, Object> getExtraParams() {
        return this.extraParams;
    }

    public String getClientSecret() {
        return this.clientSecret;
    }

    public final String getReturnUrl() {
        return this.returnUrl;
    }

    public final void setReturnUrl(String str) {
        this.returnUrl = str;
    }

    public final Boolean getSavePaymentMethod() {
        return this.savePaymentMethod;
    }

    public final void setSavePaymentMethod(Boolean bool) {
        this.savePaymentMethod = bool;
    }

    public final PaymentMethodOptionsParams getPaymentMethodOptions() {
        return this.paymentMethodOptions;
    }

    public final void setPaymentMethodOptions(PaymentMethodOptionsParams paymentMethodOptionsParams) {
        this.paymentMethodOptions = paymentMethodOptionsParams;
    }

    public final String getMandateId() {
        return this.mandateId;
    }

    public final void setMandateId(String str) {
        this.mandateId = str;
    }

    public final MandateDataParams getMandateData() {
        return this.mandateData;
    }

    public final void setMandateData(MandateDataParams mandateDataParams) {
        this.mandateData = mandateDataParams;
    }

    public final SetupFutureUsage getSetupFutureUsage() {
        return this.setupFutureUsage;
    }

    public final void setSetupFutureUsage(SetupFutureUsage setupFutureUsage2) {
        this.setupFutureUsage = setupFutureUsage2;
    }

    public final Shipping getShipping() {
        return this.shipping;
    }

    public final void setShipping(Shipping shipping2) {
        this.shipping = shipping2;
    }

    public final String getReceiptEmail() {
        return this.receiptEmail;
    }

    public final void setReceiptEmail(String str) {
        this.receiptEmail = str;
    }

    public final boolean shouldSavePaymentMethod() {
        return Intrinsics.areEqual((Object) this.savePaymentMethod, (Object) true);
    }

    public boolean shouldUseStripeSdk() {
        return this.useStripeSdk;
    }

    public ConfirmPaymentIntentParams withShouldUseStripeSdk(boolean z) {
        return copy$default(this, (PaymentMethodCreateParams) null, (String) null, (SourceParams) null, (String) null, (Map) null, (String) null, (String) null, (Boolean) null, z, (PaymentMethodOptionsParams) null, (String) null, (MandateDataParams) null, (SetupFutureUsage) null, (Shipping) null, (String) null, 32511, (Object) null);
    }

    public Map<String, Object> toParamMap() {
        Map mapOf = MapsKt.mapOf(TuplesKt.to("client_secret", getClientSecret()), TuplesKt.to("use_stripe_sdk", Boolean.valueOf(this.useStripeSdk)));
        Boolean bool = this.savePaymentMethod;
        Map map = null;
        Map mapOf2 = bool != null ? MapsKt.mapOf(TuplesKt.to(PARAM_SAVE_PAYMENT_METHOD, Boolean.valueOf(bool.booleanValue()))) : null;
        if (mapOf2 == null) {
            mapOf2 = MapsKt.emptyMap();
        }
        Map plus = MapsKt.plus(mapOf, mapOf2);
        String str = this.mandateId;
        Map mapOf3 = str != null ? MapsKt.mapOf(TuplesKt.to("mandate", str)) : null;
        if (mapOf3 == null) {
            mapOf3 = MapsKt.emptyMap();
        }
        Map plus2 = MapsKt.plus(plus, mapOf3);
        Map<String, Object> mandateDataParams = getMandateDataParams();
        Map<K, V> mapOf4 = mandateDataParams != null ? MapsKt.mapOf(TuplesKt.to("mandate_data", mandateDataParams)) : null;
        if (mapOf4 == null) {
            mapOf4 = MapsKt.emptyMap();
        }
        Map<K, V> plus3 = MapsKt.plus(plus2, (Map) mapOf4);
        String str2 = this.returnUrl;
        Map mapOf5 = str2 != null ? MapsKt.mapOf(TuplesKt.to("return_url", str2)) : null;
        if (mapOf5 == null) {
            mapOf5 = MapsKt.emptyMap();
        }
        Map<K, V> plus4 = MapsKt.plus(plus3, (Map<K, V>) mapOf5);
        PaymentMethodOptionsParams paymentMethodOptionsParams = this.paymentMethodOptions;
        Map<K, V> mapOf6 = paymentMethodOptionsParams != null ? MapsKt.mapOf(TuplesKt.to(PARAM_PAYMENT_METHOD_OPTIONS, paymentMethodOptionsParams.toParamMap())) : null;
        if (mapOf6 == null) {
            mapOf6 = MapsKt.emptyMap();
        }
        Map<K, V> plus5 = MapsKt.plus(plus4, mapOf6);
        SetupFutureUsage setupFutureUsage2 = this.setupFutureUsage;
        Map mapOf7 = setupFutureUsage2 != null ? MapsKt.mapOf(TuplesKt.to(PARAM_SETUP_FUTURE_USAGE, setupFutureUsage2.getCode$stripe_release())) : null;
        if (mapOf7 == null) {
            mapOf7 = MapsKt.emptyMap();
        }
        Map<K, V> plus6 = MapsKt.plus(plus5, (Map<K, V>) mapOf7);
        Shipping shipping2 = this.shipping;
        Map<K, V> mapOf8 = shipping2 != null ? MapsKt.mapOf(TuplesKt.to("shipping", shipping2.toParamMap())) : null;
        if (mapOf8 == null) {
            mapOf8 = MapsKt.emptyMap();
        }
        Map<String, Object> plus7 = MapsKt.plus(MapsKt.plus(plus6, mapOf8), (Map<K, V>) getPaymentMethodParamMap());
        String str3 = this.receiptEmail;
        if (str3 != null) {
            map = MapsKt.mapOf(TuplesKt.to(PARAM_RECEIPT_EMAIL, str3));
        }
        if (map == null) {
            map = MapsKt.emptyMap();
        }
        Map<String, Object> plus8 = MapsKt.plus(plus7, (Map<String, Object>) map);
        Map<String, Object> map2 = this.extraParams;
        if (map2 == null) {
            map2 = MapsKt.emptyMap();
        }
        return MapsKt.plus(plus8, map2);
    }

    private final Map<String, Object> getPaymentMethodParamMap() {
        PaymentMethodCreateParams paymentMethodCreateParams2 = this.paymentMethodCreateParams;
        if (paymentMethodCreateParams2 != null) {
            return MapsKt.mapOf(TuplesKt.to("payment_method_data", paymentMethodCreateParams2.toParamMap()));
        }
        String str = this.paymentMethodId;
        if (str != null) {
            return MapsKt.mapOf(TuplesKt.to("payment_method", str));
        }
        SourceParams sourceParams2 = this.sourceParams;
        if (sourceParams2 != null) {
            return MapsKt.mapOf(TuplesKt.to(PARAM_SOURCE_DATA, sourceParams2.toParamMap()));
        }
        String str2 = this.sourceId;
        if (str2 != null) {
            return MapsKt.mapOf(TuplesKt.to("source", str2));
        }
        return MapsKt.emptyMap();
    }

    private final Map<String, Object> getMandateDataParams() {
        PaymentMethodCreateParams.Type type$stripe_release;
        Map<String, Object> paramMap;
        MandateDataParams mandateDataParams = this.mandateData;
        if (mandateDataParams != null && (paramMap = mandateDataParams.toParamMap()) != null) {
            return paramMap;
        }
        PaymentMethodCreateParams paymentMethodCreateParams2 = this.paymentMethodCreateParams;
        if (paymentMethodCreateParams2 == null || (type$stripe_release = paymentMethodCreateParams2.getType$stripe_release()) == null || !type$stripe_release.getHasMandate() || this.mandateId != null) {
            return null;
        }
        return new MandateDataParams(MandateDataParams.Type.Online.Companion.getDEFAULT$stripe_release()).toParamMap();
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0006\b\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u000f\b\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004R\u0014\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006j\u0002\b\u0007j\u0002\b\b¨\u0006\t"}, d2 = {"Lcom/stripe/android/model/ConfirmPaymentIntentParams$SetupFutureUsage;", "", "code", "", "(Ljava/lang/String;ILjava/lang/String;)V", "getCode$stripe_release", "()Ljava/lang/String;", "OnSession", "OffSession", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: ConfirmPaymentIntentParams.kt */
    public enum SetupFutureUsage {
        OnSession("on_session"),
        OffSession("off_session");
        
        private final String code;

        private SetupFutureUsage(String str) {
            this.code = str;
        }

        public final String getCode$stripe_release() {
            return this.code;
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000F\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0017\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\b\b\u0018\u0000 ,2\u00020\u00012\u00020\u0002:\u0001,B;\b\u0007\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\n\b\u0002\u0010\u0007\u001a\u0004\u0018\u00010\u0006\u0012\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\u0006\u0012\n\b\u0002\u0010\t\u001a\u0004\u0018\u00010\u0006¢\u0006\u0002\u0010\nJ\u000e\u0010\u0012\u001a\u00020\u0004HÀ\u0003¢\u0006\u0002\b\u0013J\u000e\u0010\u0014\u001a\u00020\u0006HÀ\u0003¢\u0006\u0002\b\u0015J\u0010\u0010\u0016\u001a\u0004\u0018\u00010\u0006HÀ\u0003¢\u0006\u0002\b\u0017J\u0010\u0010\u0018\u001a\u0004\u0018\u00010\u0006HÀ\u0003¢\u0006\u0002\b\u0019J\u0010\u0010\u001a\u001a\u0004\u0018\u00010\u0006HÀ\u0003¢\u0006\u0002\b\u001bJA\u0010\u001c\u001a\u00020\u00002\b\b\u0002\u0010\u0003\u001a\u00020\u00042\b\b\u0002\u0010\u0005\u001a\u00020\u00062\n\b\u0002\u0010\u0007\u001a\u0004\u0018\u00010\u00062\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\u00062\n\b\u0002\u0010\t\u001a\u0004\u0018\u00010\u0006HÆ\u0001J\t\u0010\u001d\u001a\u00020\u001eHÖ\u0001J\u0013\u0010\u001f\u001a\u00020 2\b\u0010!\u001a\u0004\u0018\u00010\"HÖ\u0003J\t\u0010#\u001a\u00020\u001eHÖ\u0001J\u0014\u0010$\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\"0%H\u0016J\t\u0010&\u001a\u00020\u0006HÖ\u0001J\u0019\u0010'\u001a\u00020(2\u0006\u0010)\u001a\u00020*2\u0006\u0010+\u001a\u00020\u001eHÖ\u0001R\u0014\u0010\u0003\u001a\u00020\u0004X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0016\u0010\u0007\u001a\u0004\u0018\u00010\u0006X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000eR\u0014\u0010\u0005\u001a\u00020\u0006X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u000eR\u0016\u0010\b\u001a\u0004\u0018\u00010\u0006X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u000eR\u0016\u0010\t\u001a\u0004\u0018\u00010\u0006X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u000e¨\u0006-"}, d2 = {"Lcom/stripe/android/model/ConfirmPaymentIntentParams$Shipping;", "Lcom/stripe/android/model/StripeParamsModel;", "Landroid/os/Parcelable;", "address", "Lcom/stripe/android/model/Address;", "name", "", "carrier", "phone", "trackingNumber", "(Lcom/stripe/android/model/Address;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", "getAddress$stripe_release", "()Lcom/stripe/android/model/Address;", "getCarrier$stripe_release", "()Ljava/lang/String;", "getName$stripe_release", "getPhone$stripe_release", "getTrackingNumber$stripe_release", "component1", "component1$stripe_release", "component2", "component2$stripe_release", "component3", "component3$stripe_release", "component4", "component4$stripe_release", "component5", "component5$stripe_release", "copy", "describeContents", "", "equals", "", "other", "", "hashCode", "toParamMap", "", "toString", "writeToParcel", "", "parcel", "Landroid/os/Parcel;", "flags", "Companion", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: ConfirmPaymentIntentParams.kt */
    public static final class Shipping implements StripeParamsModel, Parcelable {
        public static final Parcelable.Creator CREATOR = new Creator();
        @Deprecated
        public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
        private static final String PARAM_ADDRESS = "address";
        private static final String PARAM_CARRIER = "carrier";
        private static final String PARAM_NAME = "name";
        private static final String PARAM_PHONE = "phone";
        private static final String PARAM_TRACKING_NUMBER = "tracking_number";
        private final Address address;
        private final String carrier;
        private final String name;
        private final String phone;
        private final String trackingNumber;

        @Metadata(bv = {1, 0, 3}, k = 3, mv = {1, 1, 16})
        public static class Creator implements Parcelable.Creator {
            public final Object createFromParcel(Parcel parcel) {
                Intrinsics.checkParameterIsNotNull(parcel, "in");
                return new Shipping((Address) Address.CREATOR.createFromParcel(parcel), parcel.readString(), parcel.readString(), parcel.readString(), parcel.readString());
            }

            public final Object[] newArray(int i) {
                return new Shipping[i];
            }
        }

        public Shipping(Address address2, String str) {
            this(address2, str, (String) null, (String) null, (String) null, 28, (DefaultConstructorMarker) null);
        }

        public Shipping(Address address2, String str, String str2) {
            this(address2, str, str2, (String) null, (String) null, 24, (DefaultConstructorMarker) null);
        }

        public Shipping(Address address2, String str, String str2, String str3) {
            this(address2, str, str2, str3, (String) null, 16, (DefaultConstructorMarker) null);
        }

        public static /* synthetic */ Shipping copy$default(Shipping shipping, Address address2, String str, String str2, String str3, String str4, int i, Object obj) {
            if ((i & 1) != 0) {
                address2 = shipping.address;
            }
            if ((i & 2) != 0) {
                str = shipping.name;
            }
            String str5 = str;
            if ((i & 4) != 0) {
                str2 = shipping.carrier;
            }
            String str6 = str2;
            if ((i & 8) != 0) {
                str3 = shipping.phone;
            }
            String str7 = str3;
            if ((i & 16) != 0) {
                str4 = shipping.trackingNumber;
            }
            return shipping.copy(address2, str5, str6, str7, str4);
        }

        public final Address component1$stripe_release() {
            return this.address;
        }

        public final String component2$stripe_release() {
            return this.name;
        }

        public final String component3$stripe_release() {
            return this.carrier;
        }

        public final String component4$stripe_release() {
            return this.phone;
        }

        public final String component5$stripe_release() {
            return this.trackingNumber;
        }

        public final Shipping copy(Address address2, String str, String str2, String str3, String str4) {
            Intrinsics.checkParameterIsNotNull(address2, "address");
            Intrinsics.checkParameterIsNotNull(str, "name");
            return new Shipping(address2, str, str2, str3, str4);
        }

        public int describeContents() {
            return 0;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Shipping)) {
                return false;
            }
            Shipping shipping = (Shipping) obj;
            return Intrinsics.areEqual((Object) this.address, (Object) shipping.address) && Intrinsics.areEqual((Object) this.name, (Object) shipping.name) && Intrinsics.areEqual((Object) this.carrier, (Object) shipping.carrier) && Intrinsics.areEqual((Object) this.phone, (Object) shipping.phone) && Intrinsics.areEqual((Object) this.trackingNumber, (Object) shipping.trackingNumber);
        }

        public int hashCode() {
            Address address2 = this.address;
            int i = 0;
            int hashCode = (address2 != null ? address2.hashCode() : 0) * 31;
            String str = this.name;
            int hashCode2 = (hashCode + (str != null ? str.hashCode() : 0)) * 31;
            String str2 = this.carrier;
            int hashCode3 = (hashCode2 + (str2 != null ? str2.hashCode() : 0)) * 31;
            String str3 = this.phone;
            int hashCode4 = (hashCode3 + (str3 != null ? str3.hashCode() : 0)) * 31;
            String str4 = this.trackingNumber;
            if (str4 != null) {
                i = str4.hashCode();
            }
            return hashCode4 + i;
        }

        public String toString() {
            return "Shipping(address=" + this.address + ", name=" + this.name + ", carrier=" + this.carrier + ", phone=" + this.phone + ", trackingNumber=" + this.trackingNumber + ")";
        }

        public void writeToParcel(Parcel parcel, int i) {
            Intrinsics.checkParameterIsNotNull(parcel, "parcel");
            this.address.writeToParcel(parcel, 0);
            parcel.writeString(this.name);
            parcel.writeString(this.carrier);
            parcel.writeString(this.phone);
            parcel.writeString(this.trackingNumber);
        }

        public Shipping(Address address2, String str, String str2, String str3, String str4) {
            Intrinsics.checkParameterIsNotNull(address2, "address");
            Intrinsics.checkParameterIsNotNull(str, "name");
            this.address = address2;
            this.name = str;
            this.carrier = str2;
            this.phone = str3;
            this.trackingNumber = str4;
        }

        public final Address getAddress$stripe_release() {
            return this.address;
        }

        public final String getName$stripe_release() {
            return this.name;
        }

        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ Shipping(Address address2, String str, String str2, String str3, String str4, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this(address2, str, (i & 4) != 0 ? null : str2, (i & 8) != 0 ? null : str3, (i & 16) != 0 ? null : str4);
        }

        public final String getCarrier$stripe_release() {
            return this.carrier;
        }

        public final String getPhone$stripe_release() {
            return this.phone;
        }

        public final String getTrackingNumber$stripe_release() {
            return this.trackingNumber;
        }

        public Map<String, Object> toParamMap() {
            Pair[] pairArr = {TuplesKt.to("address", this.address.toParamMap()), TuplesKt.to("name", this.name), TuplesKt.to(PARAM_CARRIER, this.carrier), TuplesKt.to("phone", this.phone), TuplesKt.to(PARAM_TRACKING_NUMBER, this.trackingNumber)};
            Map<String, Object> emptyMap = MapsKt.emptyMap();
            for (Pair pair : CollectionsKt.listOf(pairArr)) {
                String str = (String) pair.component1();
                Object component2 = pair.component2();
                Map mapOf = component2 != null ? MapsKt.mapOf(TuplesKt.to(str, component2)) : null;
                if (mapOf == null) {
                    mapOf = MapsKt.emptyMap();
                }
                emptyMap = MapsKt.plus(emptyMap, (Map<String, Object>) mapOf);
            }
            return emptyMap;
        }

        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0005\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\t"}, d2 = {"Lcom/stripe/android/model/ConfirmPaymentIntentParams$Shipping$Companion;", "", "()V", "PARAM_ADDRESS", "", "PARAM_CARRIER", "PARAM_NAME", "PARAM_PHONE", "PARAM_TRACKING_NUMBER", "stripe_release"}, k = 1, mv = {1, 1, 16})
        /* compiled from: ConfirmPaymentIntentParams.kt */
        private static final class Companion {
            private Companion() {
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000V\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010$\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J@\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u00042\n\b\u0002\u0010\u000e\u001a\u0004\u0018\u00010\u00042\u0016\b\u0002\u0010\u000f\u001a\u0010\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0001\u0018\u00010\u00102\n\b\u0002\u0010\u0011\u001a\u0004\u0018\u00010\u0012H\u0007J\u001d\u0010\u0013\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u00042\u0006\u0010\u000e\u001a\u00020\u0004H\u0001¢\u0006\u0002\b\u0014J}\u0010\u0015\u001a\u00020\f2\u0006\u0010\u0016\u001a\u00020\u00172\u0006\u0010\r\u001a\u00020\u00042\n\b\u0002\u0010\u000e\u001a\u0004\u0018\u00010\u00042\n\b\u0002\u0010\u0018\u001a\u0004\u0018\u00010\u00192\u0016\b\u0002\u0010\u000f\u001a\u0010\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0001\u0018\u00010\u00102\n\b\u0002\u0010\u001a\u001a\u0004\u0018\u00010\u00042\n\b\u0002\u0010\u001b\u001a\u0004\u0018\u00010\u001c2\n\b\u0002\u0010\u001d\u001a\u0004\u0018\u00010\u001e2\n\b\u0002\u0010\u0011\u001a\u0004\u0018\u00010\u0012H\u0007¢\u0006\u0002\u0010\u001fJ\u0001\u0010 \u001a\u00020\f2\u0006\u0010!\u001a\u00020\u00042\u0006\u0010\r\u001a\u00020\u00042\n\b\u0002\u0010\u000e\u001a\u0004\u0018\u00010\u00042\n\b\u0002\u0010\u0018\u001a\u0004\u0018\u00010\u00192\u0016\b\u0002\u0010\u000f\u001a\u0010\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0001\u0018\u00010\u00102\n\b\u0002\u0010\"\u001a\u0004\u0018\u00010#2\n\b\u0002\u0010\u001a\u001a\u0004\u0018\u00010\u00042\n\b\u0002\u0010\u001b\u001a\u0004\u0018\u00010\u001c2\n\b\u0002\u0010\u001d\u001a\u0004\u0018\u00010\u001e2\n\b\u0002\u0010\u0011\u001a\u0004\u0018\u00010\u0012H\u0007¢\u0006\u0002\u0010$JU\u0010%\u001a\u00020\f2\u0006\u0010&\u001a\u00020\u00042\u0006\u0010\r\u001a\u00020\u00042\u0006\u0010\u000e\u001a\u00020\u00042\n\b\u0002\u0010\u0018\u001a\u0004\u0018\u00010\u00192\u0016\b\u0002\u0010\u000f\u001a\u0010\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0001\u0018\u00010\u00102\n\b\u0002\u0010\u0011\u001a\u0004\u0018\u00010\u0012H\u0007¢\u0006\u0002\u0010'JU\u0010(\u001a\u00020\f2\u0006\u0010)\u001a\u00020*2\u0006\u0010\r\u001a\u00020\u00042\u0006\u0010\u000e\u001a\u00020\u00042\n\b\u0002\u0010\u0018\u001a\u0004\u0018\u00010\u00192\u0016\b\u0002\u0010\u000f\u001a\u0010\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0001\u0018\u00010\u00102\n\b\u0002\u0010\u0011\u001a\u0004\u0018\u00010\u0012H\u0007¢\u0006\u0002\u0010+R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006,"}, d2 = {"Lcom/stripe/android/model/ConfirmPaymentIntentParams$Companion;", "", "()V", "PARAM_PAYMENT_METHOD_OPTIONS", "", "PARAM_RECEIPT_EMAIL", "PARAM_SAVE_PAYMENT_METHOD", "PARAM_SETUP_FUTURE_USAGE", "PARAM_SHIPPING", "PARAM_SOURCE_DATA", "PARAM_SOURCE_ID", "create", "Lcom/stripe/android/model/ConfirmPaymentIntentParams;", "clientSecret", "returnUrl", "extraParams", "", "shipping", "Lcom/stripe/android/model/ConfirmPaymentIntentParams$Shipping;", "createAlipay", "createAlipay$stripe_release", "createWithPaymentMethodCreateParams", "paymentMethodCreateParams", "Lcom/stripe/android/model/PaymentMethodCreateParams;", "savePaymentMethod", "", "mandateId", "mandateData", "Lcom/stripe/android/model/MandateDataParams;", "setupFutureUsage", "Lcom/stripe/android/model/ConfirmPaymentIntentParams$SetupFutureUsage;", "(Lcom/stripe/android/model/PaymentMethodCreateParams;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/util/Map;Ljava/lang/String;Lcom/stripe/android/model/MandateDataParams;Lcom/stripe/android/model/ConfirmPaymentIntentParams$SetupFutureUsage;Lcom/stripe/android/model/ConfirmPaymentIntentParams$Shipping;)Lcom/stripe/android/model/ConfirmPaymentIntentParams;", "createWithPaymentMethodId", "paymentMethodId", "paymentMethodOptions", "Lcom/stripe/android/model/PaymentMethodOptionsParams;", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/util/Map;Lcom/stripe/android/model/PaymentMethodOptionsParams;Ljava/lang/String;Lcom/stripe/android/model/MandateDataParams;Lcom/stripe/android/model/ConfirmPaymentIntentParams$SetupFutureUsage;Lcom/stripe/android/model/ConfirmPaymentIntentParams$Shipping;)Lcom/stripe/android/model/ConfirmPaymentIntentParams;", "createWithSourceId", "sourceId", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/util/Map;Lcom/stripe/android/model/ConfirmPaymentIntentParams$Shipping;)Lcom/stripe/android/model/ConfirmPaymentIntentParams;", "createWithSourceParams", "sourceParams", "Lcom/stripe/android/model/SourceParams;", "(Lcom/stripe/android/model/SourceParams;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/util/Map;Lcom/stripe/android/model/ConfirmPaymentIntentParams$Shipping;)Lcom/stripe/android/model/ConfirmPaymentIntentParams;", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: ConfirmPaymentIntentParams.kt */
    public static final class Companion {
        @JvmStatic
        public final ConfirmPaymentIntentParams create(String str) {
            return create$default(this, str, (String) null, (Map) null, (Shipping) null, 14, (Object) null);
        }

        @JvmStatic
        public final ConfirmPaymentIntentParams create(String str, String str2) {
            return create$default(this, str, str2, (Map) null, (Shipping) null, 12, (Object) null);
        }

        @JvmStatic
        public final ConfirmPaymentIntentParams create(String str, String str2, Map<String, ? extends Object> map) {
            return create$default(this, str, str2, map, (Shipping) null, 8, (Object) null);
        }

        @JvmStatic
        public final ConfirmPaymentIntentParams createWithPaymentMethodCreateParams(PaymentMethodCreateParams paymentMethodCreateParams, String str) {
            return createWithPaymentMethodCreateParams$default(this, paymentMethodCreateParams, str, (String) null, (Boolean) null, (Map) null, (String) null, (MandateDataParams) null, (SetupFutureUsage) null, (Shipping) null, 508, (Object) null);
        }

        @JvmStatic
        public final ConfirmPaymentIntentParams createWithPaymentMethodCreateParams(PaymentMethodCreateParams paymentMethodCreateParams, String str, String str2) {
            return createWithPaymentMethodCreateParams$default(this, paymentMethodCreateParams, str, str2, (Boolean) null, (Map) null, (String) null, (MandateDataParams) null, (SetupFutureUsage) null, (Shipping) null, 504, (Object) null);
        }

        @JvmStatic
        public final ConfirmPaymentIntentParams createWithPaymentMethodCreateParams(PaymentMethodCreateParams paymentMethodCreateParams, String str, String str2, Boolean bool) {
            return createWithPaymentMethodCreateParams$default(this, paymentMethodCreateParams, str, str2, bool, (Map) null, (String) null, (MandateDataParams) null, (SetupFutureUsage) null, (Shipping) null, 496, (Object) null);
        }

        @JvmStatic
        public final ConfirmPaymentIntentParams createWithPaymentMethodCreateParams(PaymentMethodCreateParams paymentMethodCreateParams, String str, String str2, Boolean bool, Map<String, ? extends Object> map) {
            return createWithPaymentMethodCreateParams$default(this, paymentMethodCreateParams, str, str2, bool, map, (String) null, (MandateDataParams) null, (SetupFutureUsage) null, (Shipping) null, 480, (Object) null);
        }

        @JvmStatic
        public final ConfirmPaymentIntentParams createWithPaymentMethodCreateParams(PaymentMethodCreateParams paymentMethodCreateParams, String str, String str2, Boolean bool, Map<String, ? extends Object> map, String str3) {
            return createWithPaymentMethodCreateParams$default(this, paymentMethodCreateParams, str, str2, bool, map, str3, (MandateDataParams) null, (SetupFutureUsage) null, (Shipping) null, 448, (Object) null);
        }

        @JvmStatic
        public final ConfirmPaymentIntentParams createWithPaymentMethodCreateParams(PaymentMethodCreateParams paymentMethodCreateParams, String str, String str2, Boolean bool, Map<String, ? extends Object> map, String str3, MandateDataParams mandateDataParams) {
            return createWithPaymentMethodCreateParams$default(this, paymentMethodCreateParams, str, str2, bool, map, str3, mandateDataParams, (SetupFutureUsage) null, (Shipping) null, 384, (Object) null);
        }

        @JvmStatic
        public final ConfirmPaymentIntentParams createWithPaymentMethodCreateParams(PaymentMethodCreateParams paymentMethodCreateParams, String str, String str2, Boolean bool, Map<String, ? extends Object> map, String str3, MandateDataParams mandateDataParams, SetupFutureUsage setupFutureUsage) {
            return createWithPaymentMethodCreateParams$default(this, paymentMethodCreateParams, str, str2, bool, map, str3, mandateDataParams, setupFutureUsage, (Shipping) null, 256, (Object) null);
        }

        @JvmStatic
        public final ConfirmPaymentIntentParams createWithPaymentMethodId(String str, String str2) {
            return createWithPaymentMethodId$default(this, str, str2, (String) null, (Boolean) null, (Map) null, (PaymentMethodOptionsParams) null, (String) null, (MandateDataParams) null, (SetupFutureUsage) null, (Shipping) null, PointerIconCompat.TYPE_GRAB, (Object) null);
        }

        @JvmStatic
        public final ConfirmPaymentIntentParams createWithPaymentMethodId(String str, String str2, String str3) {
            return createWithPaymentMethodId$default(this, str, str2, str3, (Boolean) null, (Map) null, (PaymentMethodOptionsParams) null, (String) null, (MandateDataParams) null, (SetupFutureUsage) null, (Shipping) null, PointerIconCompat.TYPE_TOP_RIGHT_DIAGONAL_DOUBLE_ARROW, (Object) null);
        }

        @JvmStatic
        public final ConfirmPaymentIntentParams createWithPaymentMethodId(String str, String str2, String str3, Boolean bool) {
            return createWithPaymentMethodId$default(this, str, str2, str3, bool, (Map) null, (PaymentMethodOptionsParams) null, (String) null, (MandateDataParams) null, (SetupFutureUsage) null, (Shipping) null, PointerIconCompat.TYPE_TEXT, (Object) null);
        }

        @JvmStatic
        public final ConfirmPaymentIntentParams createWithPaymentMethodId(String str, String str2, String str3, Boolean bool, Map<String, ? extends Object> map) {
            return createWithPaymentMethodId$default(this, str, str2, str3, bool, map, (PaymentMethodOptionsParams) null, (String) null, (MandateDataParams) null, (SetupFutureUsage) null, (Shipping) null, 992, (Object) null);
        }

        @JvmStatic
        public final ConfirmPaymentIntentParams createWithPaymentMethodId(String str, String str2, String str3, Boolean bool, Map<String, ? extends Object> map, PaymentMethodOptionsParams paymentMethodOptionsParams) {
            return createWithPaymentMethodId$default(this, str, str2, str3, bool, map, paymentMethodOptionsParams, (String) null, (MandateDataParams) null, (SetupFutureUsage) null, (Shipping) null, 960, (Object) null);
        }

        @JvmStatic
        public final ConfirmPaymentIntentParams createWithPaymentMethodId(String str, String str2, String str3, Boolean bool, Map<String, ? extends Object> map, PaymentMethodOptionsParams paymentMethodOptionsParams, String str4) {
            return createWithPaymentMethodId$default(this, str, str2, str3, bool, map, paymentMethodOptionsParams, str4, (MandateDataParams) null, (SetupFutureUsage) null, (Shipping) null, 896, (Object) null);
        }

        @JvmStatic
        public final ConfirmPaymentIntentParams createWithPaymentMethodId(String str, String str2, String str3, Boolean bool, Map<String, ? extends Object> map, PaymentMethodOptionsParams paymentMethodOptionsParams, String str4, MandateDataParams mandateDataParams) {
            return createWithPaymentMethodId$default(this, str, str2, str3, bool, map, paymentMethodOptionsParams, str4, mandateDataParams, (SetupFutureUsage) null, (Shipping) null, 768, (Object) null);
        }

        @JvmStatic
        public final ConfirmPaymentIntentParams createWithPaymentMethodId(String str, String str2, String str3, Boolean bool, Map<String, ? extends Object> map, PaymentMethodOptionsParams paymentMethodOptionsParams, String str4, MandateDataParams mandateDataParams, SetupFutureUsage setupFutureUsage) {
            return createWithPaymentMethodId$default(this, str, str2, str3, bool, map, paymentMethodOptionsParams, str4, mandateDataParams, setupFutureUsage, (Shipping) null, 512, (Object) null);
        }

        @JvmStatic
        public final ConfirmPaymentIntentParams createWithSourceId(String str, String str2, String str3) {
            return createWithSourceId$default(this, str, str2, str3, (Boolean) null, (Map) null, (Shipping) null, 56, (Object) null);
        }

        @JvmStatic
        public final ConfirmPaymentIntentParams createWithSourceId(String str, String str2, String str3, Boolean bool) {
            return createWithSourceId$default(this, str, str2, str3, bool, (Map) null, (Shipping) null, 48, (Object) null);
        }

        @JvmStatic
        public final ConfirmPaymentIntentParams createWithSourceId(String str, String str2, String str3, Boolean bool, Map<String, ? extends Object> map) {
            return createWithSourceId$default(this, str, str2, str3, bool, map, (Shipping) null, 32, (Object) null);
        }

        @JvmStatic
        public final ConfirmPaymentIntentParams createWithSourceParams(SourceParams sourceParams, String str, String str2) {
            return createWithSourceParams$default(this, sourceParams, str, str2, (Boolean) null, (Map) null, (Shipping) null, 56, (Object) null);
        }

        @JvmStatic
        public final ConfirmPaymentIntentParams createWithSourceParams(SourceParams sourceParams, String str, String str2, Boolean bool) {
            return createWithSourceParams$default(this, sourceParams, str, str2, bool, (Map) null, (Shipping) null, 48, (Object) null);
        }

        @JvmStatic
        public final ConfirmPaymentIntentParams createWithSourceParams(SourceParams sourceParams, String str, String str2, Boolean bool, Map<String, ? extends Object> map) {
            return createWithSourceParams$default(this, sourceParams, str, str2, bool, map, (Shipping) null, 32, (Object) null);
        }

        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        public static /* synthetic */ ConfirmPaymentIntentParams create$default(Companion companion, String str, String str2, Map map, Shipping shipping, int i, Object obj) {
            if ((i & 2) != 0) {
                str2 = null;
            }
            if ((i & 4) != 0) {
                map = null;
            }
            if ((i & 8) != 0) {
                shipping = null;
            }
            return companion.create(str, str2, map, shipping);
        }

        @JvmStatic
        public final ConfirmPaymentIntentParams create(String str, String str2, Map<String, ? extends Object> map, Shipping shipping) {
            Map<String, ? extends Object> map2 = map;
            Intrinsics.checkParameterIsNotNull(str, "clientSecret");
            return new ConfirmPaymentIntentParams((PaymentMethodCreateParams) null, (String) null, (SourceParams) null, (String) null, map2, str, str2, (Boolean) null, false, (PaymentMethodOptionsParams) null, (String) null, (MandateDataParams) null, (SetupFutureUsage) null, shipping, (String) null, 24463, (DefaultConstructorMarker) null);
        }

        public static /* synthetic */ ConfirmPaymentIntentParams createWithPaymentMethodId$default(Companion companion, String str, String str2, String str3, Boolean bool, Map map, PaymentMethodOptionsParams paymentMethodOptionsParams, String str4, MandateDataParams mandateDataParams, SetupFutureUsage setupFutureUsage, Shipping shipping, int i, Object obj) {
            int i2 = i;
            return companion.createWithPaymentMethodId(str, str2, (i2 & 4) != 0 ? null : str3, (i2 & 8) != 0 ? null : bool, (i2 & 16) != 0 ? null : map, (i2 & 32) != 0 ? null : paymentMethodOptionsParams, (i2 & 64) != 0 ? null : str4, (i2 & 128) != 0 ? null : mandateDataParams, (i2 & 256) != 0 ? null : setupFutureUsage, (i2 & 512) != 0 ? null : shipping);
        }

        @JvmStatic
        public final ConfirmPaymentIntentParams createWithPaymentMethodId(String str, String str2, String str3, Boolean bool, Map<String, ? extends Object> map, PaymentMethodOptionsParams paymentMethodOptionsParams, String str4, MandateDataParams mandateDataParams, SetupFutureUsage setupFutureUsage, Shipping shipping) {
            Map<String, ? extends Object> map2 = map;
            Intrinsics.checkParameterIsNotNull(str, "paymentMethodId");
            Intrinsics.checkParameterIsNotNull(str2, "clientSecret");
            return new ConfirmPaymentIntentParams((PaymentMethodCreateParams) null, str, (SourceParams) null, (String) null, map2, str2, str3, bool, false, paymentMethodOptionsParams, str4, mandateDataParams, setupFutureUsage, shipping, (String) null, 16653, (DefaultConstructorMarker) null);
        }

        public static /* synthetic */ ConfirmPaymentIntentParams createWithPaymentMethodCreateParams$default(Companion companion, PaymentMethodCreateParams paymentMethodCreateParams, String str, String str2, Boolean bool, Map map, String str3, MandateDataParams mandateDataParams, SetupFutureUsage setupFutureUsage, Shipping shipping, int i, Object obj) {
            int i2 = i;
            return companion.createWithPaymentMethodCreateParams(paymentMethodCreateParams, str, (i2 & 4) != 0 ? null : str2, (i2 & 8) != 0 ? null : bool, (i2 & 16) != 0 ? null : map, (i2 & 32) != 0 ? null : str3, (i2 & 64) != 0 ? null : mandateDataParams, (i2 & 128) != 0 ? null : setupFutureUsage, (i2 & 256) != 0 ? null : shipping);
        }

        @JvmStatic
        public final ConfirmPaymentIntentParams createWithPaymentMethodCreateParams(PaymentMethodCreateParams paymentMethodCreateParams, String str, String str2, Boolean bool, Map<String, ? extends Object> map, String str3, MandateDataParams mandateDataParams, SetupFutureUsage setupFutureUsage, Shipping shipping) {
            Map<String, ? extends Object> map2 = map;
            Intrinsics.checkParameterIsNotNull(paymentMethodCreateParams, "paymentMethodCreateParams");
            Intrinsics.checkParameterIsNotNull(str, "clientSecret");
            return new ConfirmPaymentIntentParams(paymentMethodCreateParams, (String) null, (SourceParams) null, (String) null, map2, str, str2, bool, false, (PaymentMethodOptionsParams) null, str3, mandateDataParams, setupFutureUsage, shipping, (String) null, 17166, (DefaultConstructorMarker) null);
        }

        public static /* synthetic */ ConfirmPaymentIntentParams createWithSourceId$default(Companion companion, String str, String str2, String str3, Boolean bool, Map map, Shipping shipping, int i, Object obj) {
            return companion.createWithSourceId(str, str2, str3, (i & 8) != 0 ? null : bool, (i & 16) != 0 ? null : map, (i & 32) != 0 ? null : shipping);
        }

        @JvmStatic
        public final ConfirmPaymentIntentParams createWithSourceId(String str, String str2, String str3, Boolean bool, Map<String, ? extends Object> map, Shipping shipping) {
            Map<String, ? extends Object> map2 = map;
            Intrinsics.checkParameterIsNotNull(str, "sourceId");
            Intrinsics.checkParameterIsNotNull(str2, "clientSecret");
            Intrinsics.checkParameterIsNotNull(str3, "returnUrl");
            return new ConfirmPaymentIntentParams((PaymentMethodCreateParams) null, (String) null, (SourceParams) null, str, map2, str2, str3, bool, false, (PaymentMethodOptionsParams) null, (String) null, (MandateDataParams) null, (SetupFutureUsage) null, shipping, (String) null, 24327, (DefaultConstructorMarker) null);
        }

        public static /* synthetic */ ConfirmPaymentIntentParams createWithSourceParams$default(Companion companion, SourceParams sourceParams, String str, String str2, Boolean bool, Map map, Shipping shipping, int i, Object obj) {
            return companion.createWithSourceParams(sourceParams, str, str2, (i & 8) != 0 ? null : bool, (i & 16) != 0 ? null : map, (i & 32) != 0 ? null : shipping);
        }

        @JvmStatic
        public final ConfirmPaymentIntentParams createWithSourceParams(SourceParams sourceParams, String str, String str2, Boolean bool, Map<String, ? extends Object> map, Shipping shipping) {
            Map<String, ? extends Object> map2 = map;
            Intrinsics.checkParameterIsNotNull(sourceParams, "sourceParams");
            Intrinsics.checkParameterIsNotNull(str, "clientSecret");
            Intrinsics.checkParameterIsNotNull(str2, "returnUrl");
            return new ConfirmPaymentIntentParams((PaymentMethodCreateParams) null, (String) null, sourceParams, (String) null, map2, str, str2, bool, false, (PaymentMethodOptionsParams) null, (String) null, (MandateDataParams) null, (SetupFutureUsage) null, shipping, (String) null, 24331, (DefaultConstructorMarker) null);
        }

        @JvmStatic
        public final ConfirmPaymentIntentParams createAlipay$stripe_release(String str, String str2) {
            Intrinsics.checkParameterIsNotNull(str, "clientSecret");
            Intrinsics.checkParameterIsNotNull(str2, "returnUrl");
            return new ConfirmPaymentIntentParams(PaymentMethodCreateParams.Companion.createAlipay$stripe_release$default(PaymentMethodCreateParams.Companion, (Map) null, 1, (Object) null), (String) null, (SourceParams) null, (String) null, (Map) null, str, str2, (Boolean) null, false, (PaymentMethodOptionsParams) null, (String) null, (MandateDataParams) null, (SetupFutureUsage) null, (Shipping) null, (String) null, 32670, (DefaultConstructorMarker) null);
        }
    }
}
