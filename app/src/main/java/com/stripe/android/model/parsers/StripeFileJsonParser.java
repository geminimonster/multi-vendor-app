package com.stripe.android.model.parsers;

import com.stripe.android.model.StripeFile;
import com.stripe.android.model.StripeFilePurpose;
import com.stripe.android.model.StripeJsonUtils;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.json.JSONObject;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0000\u0018\u0000 \u00072\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0007B\u0005¢\u0006\u0002\u0010\u0003J\u0010\u0010\u0004\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0006H\u0016¨\u0006\b"}, d2 = {"Lcom/stripe/android/model/parsers/StripeFileJsonParser;", "Lcom/stripe/android/model/parsers/ModelJsonParser;", "Lcom/stripe/android/model/StripeFile;", "()V", "parse", "json", "Lorg/json/JSONObject;", "Companion", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: StripeFileJsonParser.kt */
public final class StripeFileJsonParser implements ModelJsonParser<StripeFile> {
    @Deprecated
    public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
    private static final String FIELD_CREATED = "created";
    private static final String FIELD_FILENAME = "filename";
    private static final String FIELD_ID = "id";
    private static final String FIELD_PURPOSE = "purpose";
    private static final String FIELD_SIZE = "size";
    private static final String FIELD_TITLE = "title";
    private static final String FIELD_TYPE = "type";
    private static final String FIELD_URL = "url";

    public StripeFile parse(JSONObject jSONObject) {
        Intrinsics.checkParameterIsNotNull(jSONObject, "json");
        return new StripeFile(StripeJsonUtils.optString(jSONObject, "id"), StripeJsonUtils.INSTANCE.optLong$stripe_release(jSONObject, FIELD_CREATED), StripeJsonUtils.optString(jSONObject, FIELD_FILENAME), StripeFilePurpose.Companion.fromCode(StripeJsonUtils.optString(jSONObject, FIELD_PURPOSE)), StripeJsonUtils.INSTANCE.optInteger$stripe_release(jSONObject, FIELD_SIZE), StripeJsonUtils.optString(jSONObject, "title"), StripeJsonUtils.optString(jSONObject, "type"), StripeJsonUtils.optString(jSONObject, "url"));
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\b\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\f"}, d2 = {"Lcom/stripe/android/model/parsers/StripeFileJsonParser$Companion;", "", "()V", "FIELD_CREATED", "", "FIELD_FILENAME", "FIELD_ID", "FIELD_PURPOSE", "FIELD_SIZE", "FIELD_TITLE", "FIELD_TYPE", "FIELD_URL", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: StripeFileJsonParser.kt */
    private static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }
}
