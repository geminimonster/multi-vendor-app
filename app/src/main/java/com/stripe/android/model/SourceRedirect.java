package com.stripe.android.model;

import android.os.Parcel;
import android.os.Parcelable;
import java.lang.annotation.RetentionPolicy;
import kotlin.Metadata;
import kotlin.annotation.AnnotationRetention;
import kotlin.annotation.Retention;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\f\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\b\b\u0018\u00002\u00020\u0001:\u0001\u001cB%\b\u0000\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\b\u0010\u0004\u001a\u0004\u0018\u00010\u0003\u0012\b\u0010\u0005\u001a\u0004\u0018\u00010\u0003¢\u0006\u0002\u0010\u0006J\u000b\u0010\u000b\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000b\u0010\f\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000b\u0010\r\u001a\u0004\u0018\u00010\u0003HÆ\u0003J-\u0010\u000e\u001a\u00020\u00002\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u0003HÆ\u0001J\t\u0010\u000f\u001a\u00020\u0010HÖ\u0001J\u0013\u0010\u0011\u001a\u00020\u00122\b\u0010\u0013\u001a\u0004\u0018\u00010\u0014HÖ\u0003J\t\u0010\u0015\u001a\u00020\u0010HÖ\u0001J\t\u0010\u0016\u001a\u00020\u0003HÖ\u0001J\u0019\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00020\u001a2\u0006\u0010\u001b\u001a\u00020\u0010HÖ\u0001R\u0013\u0010\u0002\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0013\u0010\u0004\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\bR\u0013\u0010\u0005\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\b¨\u0006\u001d"}, d2 = {"Lcom/stripe/android/model/SourceRedirect;", "Lcom/stripe/android/model/StripeModel;", "returnUrl", "", "status", "url", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", "getReturnUrl", "()Ljava/lang/String;", "getStatus", "getUrl", "component1", "component2", "component3", "copy", "describeContents", "", "equals", "", "other", "", "hashCode", "toString", "writeToParcel", "", "parcel", "Landroid/os/Parcel;", "flags", "Status", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: SourceRedirect.kt */
public final class SourceRedirect implements StripeModel {
    public static final Parcelable.Creator CREATOR = new Creator();
    private final String returnUrl;
    private final String status;
    private final String url;

    @Metadata(bv = {1, 0, 3}, k = 3, mv = {1, 1, 16})
    public static class Creator implements Parcelable.Creator {
        public final Object createFromParcel(Parcel parcel) {
            Intrinsics.checkParameterIsNotNull(parcel, "in");
            return new SourceRedirect(parcel.readString(), parcel.readString(), parcel.readString());
        }

        public final Object[] newArray(int i) {
            return new SourceRedirect[i];
        }
    }

    public static /* synthetic */ SourceRedirect copy$default(SourceRedirect sourceRedirect, String str, String str2, String str3, int i, Object obj) {
        if ((i & 1) != 0) {
            str = sourceRedirect.returnUrl;
        }
        if ((i & 2) != 0) {
            str2 = sourceRedirect.status;
        }
        if ((i & 4) != 0) {
            str3 = sourceRedirect.url;
        }
        return sourceRedirect.copy(str, str2, str3);
    }

    public final String component1() {
        return this.returnUrl;
    }

    public final String component2() {
        return this.status;
    }

    public final String component3() {
        return this.url;
    }

    public final SourceRedirect copy(String str, String str2, String str3) {
        return new SourceRedirect(str, str2, str3);
    }

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof SourceRedirect)) {
            return false;
        }
        SourceRedirect sourceRedirect = (SourceRedirect) obj;
        return Intrinsics.areEqual((Object) this.returnUrl, (Object) sourceRedirect.returnUrl) && Intrinsics.areEqual((Object) this.status, (Object) sourceRedirect.status) && Intrinsics.areEqual((Object) this.url, (Object) sourceRedirect.url);
    }

    public int hashCode() {
        String str = this.returnUrl;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.status;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.url;
        if (str3 != null) {
            i = str3.hashCode();
        }
        return hashCode2 + i;
    }

    public String toString() {
        return "SourceRedirect(returnUrl=" + this.returnUrl + ", status=" + this.status + ", url=" + this.url + ")";
    }

    public void writeToParcel(Parcel parcel, int i) {
        Intrinsics.checkParameterIsNotNull(parcel, "parcel");
        parcel.writeString(this.returnUrl);
        parcel.writeString(this.status);
        parcel.writeString(this.url);
    }

    public SourceRedirect(String str, String str2, String str3) {
        this.returnUrl = str;
        this.status = str2;
        this.url = str3;
    }

    public final String getReturnUrl() {
        return this.returnUrl;
    }

    public final String getStatus() {
        return this.status;
    }

    public final String getUrl() {
        return this.url;
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u001b\n\u0002\b\u0002\b\u0002\u0018\u0000 \u00022\u00020\u0001:\u0001\u0002B\u0000¨\u0006\u0003"}, d2 = {"Lcom/stripe/android/model/SourceRedirect$Status;", "", "Companion", "stripe_release"}, k = 1, mv = {1, 1, 16})
    @Retention(AnnotationRetention.SOURCE)
    @java.lang.annotation.Retention(RetentionPolicy.SOURCE)
    /* compiled from: SourceRedirect.kt */
    public @interface Status {
        public static final Companion Companion = Companion.$$INSTANCE;
        public static final String FAILED = "failed";
        public static final String NOT_REQUIRED = "not_required";
        public static final String PENDING = "pending";
        public static final String SUCCEEDED = "succeeded";

        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0004\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\b"}, d2 = {"Lcom/stripe/android/model/SourceRedirect$Status$Companion;", "", "()V", "FAILED", "", "NOT_REQUIRED", "PENDING", "SUCCEEDED", "stripe_release"}, k = 1, mv = {1, 1, 16})
        /* compiled from: SourceRedirect.kt */
        public static final class Companion {
            static final /* synthetic */ Companion $$INSTANCE = new Companion();
            public static final String FAILED = "failed";
            public static final String NOT_REQUIRED = "not_required";
            public static final String PENDING = "pending";
            public static final String SUCCEEDED = "succeeded";

            private Companion() {
            }
        }
    }
}
