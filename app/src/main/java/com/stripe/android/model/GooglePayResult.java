package com.stripe.android.model;

import android.os.Parcel;
import android.os.Parcelable;
import com.stripe.android.model.parsers.TokenJsonParser;
import kotlin.Metadata;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.internal.Intrinsics;
import org.json.JSONException;
import org.json.JSONObject;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000H\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0013\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\b\b\u0018\u0000 +2\u00020\u0001:\u0001+BO\b\u0000\u0012\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u0012\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\u0007\u0012\n\b\u0002\u0010\t\u001a\u0004\u0018\u00010\u0007\u0012\n\b\u0002\u0010\n\u001a\u0004\u0018\u00010\u000b¢\u0006\u0002\u0010\fJ\u000b\u0010\u0017\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000b\u0010\u0018\u001a\u0004\u0018\u00010\u0005HÆ\u0003J\u000b\u0010\u0019\u001a\u0004\u0018\u00010\u0007HÆ\u0003J\u000b\u0010\u001a\u001a\u0004\u0018\u00010\u0007HÆ\u0003J\u000b\u0010\u001b\u001a\u0004\u0018\u00010\u0007HÆ\u0003J\u000b\u0010\u001c\u001a\u0004\u0018\u00010\u000bHÆ\u0003JQ\u0010\u001d\u001a\u00020\u00002\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u00072\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\u00072\n\b\u0002\u0010\t\u001a\u0004\u0018\u00010\u00072\n\b\u0002\u0010\n\u001a\u0004\u0018\u00010\u000bHÆ\u0001J\t\u0010\u001e\u001a\u00020\u001fHÖ\u0001J\u0013\u0010 \u001a\u00020!2\b\u0010\"\u001a\u0004\u0018\u00010#HÖ\u0003J\t\u0010$\u001a\u00020\u001fHÖ\u0001J\t\u0010%\u001a\u00020\u0007HÖ\u0001J\u0019\u0010&\u001a\u00020'2\u0006\u0010(\u001a\u00020)2\u0006\u0010*\u001a\u00020\u001fHÖ\u0001R\u0013\u0010\u0004\u001a\u0004\u0018\u00010\u0005¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000eR\u0013\u0010\b\u001a\u0004\u0018\u00010\u0007¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010R\u0013\u0010\u0006\u001a\u0004\u0018\u00010\u0007¢\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u0010R\u0013\u0010\t\u001a\u0004\u0018\u00010\u0007¢\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u0010R\u0013\u0010\n\u001a\u0004\u0018\u00010\u000b¢\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u0014R\u0013\u0010\u0002\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0015\u0010\u0016¨\u0006,"}, d2 = {"Lcom/stripe/android/model/GooglePayResult;", "Landroid/os/Parcelable;", "token", "Lcom/stripe/android/model/Token;", "address", "Lcom/stripe/android/model/Address;", "name", "", "email", "phoneNumber", "shippingInformation", "Lcom/stripe/android/model/ShippingInformation;", "(Lcom/stripe/android/model/Token;Lcom/stripe/android/model/Address;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/stripe/android/model/ShippingInformation;)V", "getAddress", "()Lcom/stripe/android/model/Address;", "getEmail", "()Ljava/lang/String;", "getName", "getPhoneNumber", "getShippingInformation", "()Lcom/stripe/android/model/ShippingInformation;", "getToken", "()Lcom/stripe/android/model/Token;", "component1", "component2", "component3", "component4", "component5", "component6", "copy", "describeContents", "", "equals", "", "other", "", "hashCode", "toString", "writeToParcel", "", "parcel", "Landroid/os/Parcel;", "flags", "Companion", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: GooglePayResult.kt */
public final class GooglePayResult implements Parcelable {
    public static final Parcelable.Creator CREATOR = new Creator();
    public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
    private final Address address;
    private final String email;
    private final String name;
    private final String phoneNumber;
    private final ShippingInformation shippingInformation;
    private final Token token;

    @Metadata(bv = {1, 0, 3}, k = 3, mv = {1, 1, 16})
    public static class Creator implements Parcelable.Creator {
        public final Object createFromParcel(Parcel parcel) {
            Intrinsics.checkParameterIsNotNull(parcel, "in");
            return new GooglePayResult(parcel.readInt() != 0 ? (Token) Token.CREATOR.createFromParcel(parcel) : null, parcel.readInt() != 0 ? (Address) Address.CREATOR.createFromParcel(parcel) : null, parcel.readString(), parcel.readString(), parcel.readString(), parcel.readInt() != 0 ? (ShippingInformation) ShippingInformation.CREATOR.createFromParcel(parcel) : null);
        }

        public final Object[] newArray(int i) {
            return new GooglePayResult[i];
        }
    }

    public GooglePayResult() {
        this((Token) null, (Address) null, (String) null, (String) null, (String) null, (ShippingInformation) null, 63, (DefaultConstructorMarker) null);
    }

    public static /* synthetic */ GooglePayResult copy$default(GooglePayResult googlePayResult, Token token2, Address address2, String str, String str2, String str3, ShippingInformation shippingInformation2, int i, Object obj) {
        if ((i & 1) != 0) {
            token2 = googlePayResult.token;
        }
        if ((i & 2) != 0) {
            address2 = googlePayResult.address;
        }
        Address address3 = address2;
        if ((i & 4) != 0) {
            str = googlePayResult.name;
        }
        String str4 = str;
        if ((i & 8) != 0) {
            str2 = googlePayResult.email;
        }
        String str5 = str2;
        if ((i & 16) != 0) {
            str3 = googlePayResult.phoneNumber;
        }
        String str6 = str3;
        if ((i & 32) != 0) {
            shippingInformation2 = googlePayResult.shippingInformation;
        }
        return googlePayResult.copy(token2, address3, str4, str5, str6, shippingInformation2);
    }

    @JvmStatic
    public static final GooglePayResult fromJson(JSONObject jSONObject) throws JSONException {
        return Companion.fromJson(jSONObject);
    }

    public final Token component1() {
        return this.token;
    }

    public final Address component2() {
        return this.address;
    }

    public final String component3() {
        return this.name;
    }

    public final String component4() {
        return this.email;
    }

    public final String component5() {
        return this.phoneNumber;
    }

    public final ShippingInformation component6() {
        return this.shippingInformation;
    }

    public final GooglePayResult copy(Token token2, Address address2, String str, String str2, String str3, ShippingInformation shippingInformation2) {
        return new GooglePayResult(token2, address2, str, str2, str3, shippingInformation2);
    }

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof GooglePayResult)) {
            return false;
        }
        GooglePayResult googlePayResult = (GooglePayResult) obj;
        return Intrinsics.areEqual((Object) this.token, (Object) googlePayResult.token) && Intrinsics.areEqual((Object) this.address, (Object) googlePayResult.address) && Intrinsics.areEqual((Object) this.name, (Object) googlePayResult.name) && Intrinsics.areEqual((Object) this.email, (Object) googlePayResult.email) && Intrinsics.areEqual((Object) this.phoneNumber, (Object) googlePayResult.phoneNumber) && Intrinsics.areEqual((Object) this.shippingInformation, (Object) googlePayResult.shippingInformation);
    }

    public int hashCode() {
        Token token2 = this.token;
        int i = 0;
        int hashCode = (token2 != null ? token2.hashCode() : 0) * 31;
        Address address2 = this.address;
        int hashCode2 = (hashCode + (address2 != null ? address2.hashCode() : 0)) * 31;
        String str = this.name;
        int hashCode3 = (hashCode2 + (str != null ? str.hashCode() : 0)) * 31;
        String str2 = this.email;
        int hashCode4 = (hashCode3 + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.phoneNumber;
        int hashCode5 = (hashCode4 + (str3 != null ? str3.hashCode() : 0)) * 31;
        ShippingInformation shippingInformation2 = this.shippingInformation;
        if (shippingInformation2 != null) {
            i = shippingInformation2.hashCode();
        }
        return hashCode5 + i;
    }

    public String toString() {
        return "GooglePayResult(token=" + this.token + ", address=" + this.address + ", name=" + this.name + ", email=" + this.email + ", phoneNumber=" + this.phoneNumber + ", shippingInformation=" + this.shippingInformation + ")";
    }

    public void writeToParcel(Parcel parcel, int i) {
        Intrinsics.checkParameterIsNotNull(parcel, "parcel");
        Token token2 = this.token;
        if (token2 != null) {
            parcel.writeInt(1);
            token2.writeToParcel(parcel, 0);
        } else {
            parcel.writeInt(0);
        }
        Address address2 = this.address;
        if (address2 != null) {
            parcel.writeInt(1);
            address2.writeToParcel(parcel, 0);
        } else {
            parcel.writeInt(0);
        }
        parcel.writeString(this.name);
        parcel.writeString(this.email);
        parcel.writeString(this.phoneNumber);
        ShippingInformation shippingInformation2 = this.shippingInformation;
        if (shippingInformation2 != null) {
            parcel.writeInt(1);
            shippingInformation2.writeToParcel(parcel, 0);
            return;
        }
        parcel.writeInt(0);
    }

    public GooglePayResult(Token token2, Address address2, String str, String str2, String str3, ShippingInformation shippingInformation2) {
        this.token = token2;
        this.address = address2;
        this.name = str;
        this.email = str2;
        this.phoneNumber = str3;
        this.shippingInformation = shippingInformation2;
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* synthetic */ GooglePayResult(com.stripe.android.model.Token r5, com.stripe.android.model.Address r6, java.lang.String r7, java.lang.String r8, java.lang.String r9, com.stripe.android.model.ShippingInformation r10, int r11, kotlin.jvm.internal.DefaultConstructorMarker r12) {
        /*
            r4 = this;
            r12 = r11 & 1
            r0 = 0
            if (r12 == 0) goto L_0x0008
            r5 = r0
            com.stripe.android.model.Token r5 = (com.stripe.android.model.Token) r5
        L_0x0008:
            r12 = r11 & 2
            if (r12 == 0) goto L_0x000f
            r6 = r0
            com.stripe.android.model.Address r6 = (com.stripe.android.model.Address) r6
        L_0x000f:
            r12 = r6
            r6 = r11 & 4
            if (r6 == 0) goto L_0x0017
            r7 = r0
            java.lang.String r7 = (java.lang.String) r7
        L_0x0017:
            r1 = r7
            r6 = r11 & 8
            if (r6 == 0) goto L_0x001f
            r8 = r0
            java.lang.String r8 = (java.lang.String) r8
        L_0x001f:
            r2 = r8
            r6 = r11 & 16
            if (r6 == 0) goto L_0x0027
            r9 = r0
            java.lang.String r9 = (java.lang.String) r9
        L_0x0027:
            r3 = r9
            r6 = r11 & 32
            if (r6 == 0) goto L_0x002f
            r10 = r0
            com.stripe.android.model.ShippingInformation r10 = (com.stripe.android.model.ShippingInformation) r10
        L_0x002f:
            r0 = r10
            r6 = r4
            r7 = r5
            r8 = r12
            r9 = r1
            r10 = r2
            r11 = r3
            r12 = r0
            r6.<init>(r7, r8, r9, r10, r11, r12)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.stripe.android.model.GooglePayResult.<init>(com.stripe.android.model.Token, com.stripe.android.model.Address, java.lang.String, java.lang.String, java.lang.String, com.stripe.android.model.ShippingInformation, int, kotlin.jvm.internal.DefaultConstructorMarker):void");
    }

    public final Token getToken() {
        return this.token;
    }

    public final Address getAddress() {
        return this.address;
    }

    public final String getName() {
        return this.name;
    }

    public final String getEmail() {
        return this.email;
    }

    public final String getPhoneNumber() {
        return this.phoneNumber;
    }

    public final ShippingInformation getShippingInformation() {
        return this.shippingInformation;
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0012\u0010\u0003\u001a\u0004\u0018\u00010\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0002J\u0010\u0010\u0007\u001a\u00020\b2\u0006\u0010\u0005\u001a\u00020\u0006H\u0007¨\u0006\t"}, d2 = {"Lcom/stripe/android/model/GooglePayResult$Companion;", "", "()V", "createShippingInformation", "Lcom/stripe/android/model/ShippingInformation;", "paymentDataJson", "Lorg/json/JSONObject;", "fromJson", "Lcom/stripe/android/model/GooglePayResult;", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: GooglePayResult.kt */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        @JvmStatic
        public final GooglePayResult fromJson(JSONObject jSONObject) throws JSONException {
            Address address;
            Intrinsics.checkParameterIsNotNull(jSONObject, "paymentDataJson");
            JSONObject jSONObject2 = jSONObject.getJSONObject("paymentMethodData");
            Token parse = new TokenJsonParser().parse(new JSONObject(jSONObject2.getJSONObject("tokenizationData").getString("token")));
            JSONObject optJSONObject = jSONObject2.getJSONObject("info").optJSONObject("billingAddress");
            if (optJSONObject != null) {
                address = new Address(StripeJsonUtils.optString(optJSONObject, "locality"), StripeJsonUtils.optString(optJSONObject, "countryCode"), StripeJsonUtils.optString(optJSONObject, "address1"), StripeJsonUtils.optString(optJSONObject, "address2"), StripeJsonUtils.optString(optJSONObject, "postalCode"), StripeJsonUtils.optString(optJSONObject, "administrativeArea"));
            } else {
                address = null;
            }
            return new GooglePayResult(parse, address, StripeJsonUtils.optString(optJSONObject, "name"), StripeJsonUtils.optString(jSONObject, "email"), StripeJsonUtils.optString(optJSONObject, "phoneNumber"), createShippingInformation(jSONObject));
        }

        private final ShippingInformation createShippingInformation(JSONObject jSONObject) {
            JSONObject optJSONObject = jSONObject.optJSONObject("shippingAddress");
            if (optJSONObject == null) {
                return null;
            }
            String optString = StripeJsonUtils.optString(optJSONObject, "address1");
            String optString2 = StripeJsonUtils.optString(optJSONObject, "address2");
            String optString3 = StripeJsonUtils.optString(optJSONObject, "postalCode");
            return new ShippingInformation(new Address(StripeJsonUtils.optString(optJSONObject, "locality"), StripeJsonUtils.optString(optJSONObject, "countryCode"), optString, optString2, optString3, StripeJsonUtils.optString(optJSONObject, "administrativeArea")), StripeJsonUtils.optString(optJSONObject, "name"), StripeJsonUtils.optString(optJSONObject, "phoneNumber"));
        }
    }
}
