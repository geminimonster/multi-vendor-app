package com.stripe.android.model.parsers;

import com.stripe.android.model.FpxBankStatuses;
import com.stripe.android.model.StripeJsonUtils;
import java.util.Map;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.jvm.internal.Intrinsics;
import org.json.JSONObject;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0000\u0018\u0000 \u00072\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0007B\u0005¢\u0006\u0002\u0010\u0003J\u0010\u0010\u0004\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0006H\u0016¨\u0006\b"}, d2 = {"Lcom/stripe/android/model/parsers/FpxBankStatusesJsonParser;", "Lcom/stripe/android/model/parsers/ModelJsonParser;", "Lcom/stripe/android/model/FpxBankStatuses;", "()V", "parse", "json", "Lorg/json/JSONObject;", "Companion", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: FpxBankStatusesJsonParser.kt */
public final class FpxBankStatusesJsonParser implements ModelJsonParser<FpxBankStatuses> {
    @Deprecated
    public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
    private static final String FIELD_PARSED_BANK_STATUS = "parsed_bank_status";

    public FpxBankStatuses parse(JSONObject jSONObject) {
        Intrinsics.checkParameterIsNotNull(jSONObject, "json");
        Map<String, Object> optMap$stripe_release = StripeJsonUtils.INSTANCE.optMap$stripe_release(jSONObject, FIELD_PARSED_BANK_STATUS);
        if (optMap$stripe_release == null || optMap$stripe_release.isEmpty()) {
            optMap$stripe_release = null;
        }
        if (optMap$stripe_release == null) {
            return new FpxBankStatuses((Map) null, 1, (DefaultConstructorMarker) null);
        }
        if (optMap$stripe_release != null) {
            return new FpxBankStatuses(optMap$stripe_release);
        }
        throw new TypeCastException("null cannot be cast to non-null type kotlin.collections.Map<kotlin.String, kotlin.Boolean>");
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\u0005"}, d2 = {"Lcom/stripe/android/model/parsers/FpxBankStatusesJsonParser$Companion;", "", "()V", "FIELD_PARSED_BANK_STATUS", "", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: FpxBankStatusesJsonParser.kt */
    private static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }
}
