package com.stripe.android.model.parsers;

import com.stripe.android.model.Customer;
import com.stripe.android.model.CustomerSource;
import com.stripe.android.model.ShippingInformation;
import com.stripe.android.model.StripeJsonUtils;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt;
import kotlin.collections.IntIterator;
import kotlin.jvm.internal.Intrinsics;
import kotlin.ranges.RangesKt;
import org.json.JSONArray;
import org.json.JSONObject;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0000\u0018\u0000 \t2\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\tB\u0005¢\u0006\u0002\u0010\u0003J\u0012\u0010\u0006\u001a\u0004\u0018\u00010\u00022\u0006\u0010\u0007\u001a\u00020\bH\u0016R\u000e\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000¨\u0006\n"}, d2 = {"Lcom/stripe/android/model/parsers/CustomerJsonParser;", "Lcom/stripe/android/model/parsers/ModelJsonParser;", "Lcom/stripe/android/model/Customer;", "()V", "customerSourceJsonParser", "Lcom/stripe/android/model/parsers/CustomerSourceJsonParser;", "parse", "json", "Lorg/json/JSONObject;", "Companion", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: CustomerJsonParser.kt */
public final class CustomerJsonParser implements ModelJsonParser<Customer> {
    @Deprecated
    public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
    private static final String FIELD_DATA = "data";
    private static final String FIELD_DEFAULT_SOURCE = "default_source";
    private static final String FIELD_HAS_MORE = "has_more";
    private static final String FIELD_ID = "id";
    private static final String FIELD_OBJECT = "object";
    private static final String FIELD_SHIPPING = "shipping";
    private static final String FIELD_SOURCES = "sources";
    private static final String FIELD_TOTAL_COUNT = "total_count";
    private static final String FIELD_URL = "url";
    private static final String VALUE_APPLE_PAY = "apple_pay";
    private static final String VALUE_CUSTOMER = "customer";
    private static final String VALUE_LIST = "list";
    private final CustomerSourceJsonParser customerSourceJsonParser = new CustomerSourceJsonParser();

    public Customer parse(JSONObject jSONObject) {
        String str;
        Integer num;
        boolean z;
        List list;
        Intrinsics.checkParameterIsNotNull(jSONObject, "json");
        if (!Intrinsics.areEqual((Object) VALUE_CUSTOMER, (Object) StripeJsonUtils.optString(jSONObject, FIELD_OBJECT))) {
            return null;
        }
        String optString = StripeJsonUtils.optString(jSONObject, "id");
        String optString2 = StripeJsonUtils.optString(jSONObject, FIELD_DEFAULT_SOURCE);
        JSONObject optJSONObject = jSONObject.optJSONObject("shipping");
        ShippingInformation parse = optJSONObject != null ? new ShippingInformationJsonParser().parse(optJSONObject) : null;
        JSONObject optJSONObject2 = jSONObject.optJSONObject(FIELD_SOURCES);
        if (optJSONObject2 == null || !Intrinsics.areEqual((Object) VALUE_LIST, (Object) StripeJsonUtils.optString(optJSONObject2, FIELD_OBJECT))) {
            num = null;
            list = CollectionsKt.emptyList();
            str = null;
            z = false;
        } else {
            boolean optBoolean$stripe_release = StripeJsonUtils.INSTANCE.optBoolean$stripe_release(optJSONObject2, FIELD_HAS_MORE);
            Integer optInteger$stripe_release = StripeJsonUtils.INSTANCE.optInteger$stripe_release(optJSONObject2, FIELD_TOTAL_COUNT);
            String optString3 = StripeJsonUtils.optString(optJSONObject2, "url");
            JSONArray optJSONArray = optJSONObject2.optJSONArray("data");
            if (optJSONArray == null) {
                optJSONArray = new JSONArray();
            }
            Iterable until = RangesKt.until(0, optJSONArray.length());
            Collection arrayList = new ArrayList(CollectionsKt.collectionSizeOrDefault(until, 10));
            Iterator it = until.iterator();
            while (it.hasNext()) {
                arrayList.add(optJSONArray.getJSONObject(((IntIterator) it).nextInt()));
            }
            Collection arrayList2 = new ArrayList();
            for (JSONObject jSONObject2 : (List) arrayList) {
                CustomerSourceJsonParser customerSourceJsonParser2 = this.customerSourceJsonParser;
                Intrinsics.checkExpressionValueIsNotNull(jSONObject2, "it");
                CustomerSource parse2 = customerSourceJsonParser2.parse(jSONObject2);
                if (parse2 != null) {
                    arrayList2.add(parse2);
                }
            }
            Collection arrayList3 = new ArrayList();
            for (Object next : (List) arrayList2) {
                if (!Intrinsics.areEqual((Object) VALUE_APPLE_PAY, (Object) ((CustomerSource) next).getTokenizationMethod())) {
                    arrayList3.add(next);
                }
            }
            z = optBoolean$stripe_release;
            list = (List) arrayList3;
            num = optInteger$stripe_release;
            str = optString3;
        }
        return new Customer(optString, optString2, parse, list, z, num, str);
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\f\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\u0010"}, d2 = {"Lcom/stripe/android/model/parsers/CustomerJsonParser$Companion;", "", "()V", "FIELD_DATA", "", "FIELD_DEFAULT_SOURCE", "FIELD_HAS_MORE", "FIELD_ID", "FIELD_OBJECT", "FIELD_SHIPPING", "FIELD_SOURCES", "FIELD_TOTAL_COUNT", "FIELD_URL", "VALUE_APPLE_PAY", "VALUE_CUSTOMER", "VALUE_LIST", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: CustomerJsonParser.kt */
    private static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }
}
