package com.stripe.android.model;

import android.os.Parcel;
import android.os.Parcelable;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000H\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\b\b\u0018\u00002\u00020\u00012\u00020\u0002B\u000f\b\u0000\u0012\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0002\u0010\u0004J\b\u0010\r\u001a\u0004\u0018\u00010\u000eJ\b\u0010\u000f\u001a\u0004\u0018\u00010\u0010J\t\u0010\u0011\u001a\u00020\u0002HÂ\u0003J\u0013\u0010\u0012\u001a\u00020\u00002\b\b\u0002\u0010\u0003\u001a\u00020\u0002HÆ\u0001J\t\u0010\u0013\u001a\u00020\u0014HÖ\u0001J\u0013\u0010\u0015\u001a\u00020\u00162\b\u0010\u0017\u001a\u0004\u0018\u00010\u0018HÖ\u0003J\t\u0010\u0019\u001a\u00020\u0014HÖ\u0001J\t\u0010\u001a\u001a\u00020\u0006HÖ\u0001J\u0019\u0010\u001b\u001a\u00020\u001c2\u0006\u0010\u001d\u001a\u00020\u001e2\u0006\u0010\u001f\u001a\u00020\u0014HÖ\u0001R\u0016\u0010\u0005\u001a\u0004\u0018\u00010\u00068VX\u0004¢\u0006\u0006\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\t\u001a\u00020\u00068F¢\u0006\u0006\u001a\u0004\b\n\u0010\bR\u000e\u0010\u0003\u001a\u00020\u0002X\u0004¢\u0006\u0002\n\u0000R\u0013\u0010\u000b\u001a\u0004\u0018\u00010\u00068F¢\u0006\u0006\u001a\u0004\b\f\u0010\b¨\u0006 "}, d2 = {"Lcom/stripe/android/model/CustomerSource;", "Lcom/stripe/android/model/StripeModel;", "Lcom/stripe/android/model/StripePaymentSource;", "stripePaymentSource", "(Lcom/stripe/android/model/StripePaymentSource;)V", "id", "", "getId", "()Ljava/lang/String;", "sourceType", "getSourceType", "tokenizationMethod", "getTokenizationMethod", "asCard", "Lcom/stripe/android/model/Card;", "asSource", "Lcom/stripe/android/model/Source;", "component1", "copy", "describeContents", "", "equals", "", "other", "", "hashCode", "toString", "writeToParcel", "", "parcel", "Landroid/os/Parcel;", "flags", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: CustomerSource.kt */
public final class CustomerSource implements StripeModel, StripePaymentSource {
    public static final Parcelable.Creator CREATOR = new Creator();
    private final StripePaymentSource stripePaymentSource;

    @Metadata(bv = {1, 0, 3}, k = 3, mv = {1, 1, 16})
    public static class Creator implements Parcelable.Creator {
        public final Object createFromParcel(Parcel parcel) {
            Intrinsics.checkParameterIsNotNull(parcel, "in");
            return new CustomerSource((StripePaymentSource) parcel.readParcelable(CustomerSource.class.getClassLoader()));
        }

        public final Object[] newArray(int i) {
            return new CustomerSource[i];
        }
    }

    private final StripePaymentSource component1() {
        return this.stripePaymentSource;
    }

    public static /* synthetic */ CustomerSource copy$default(CustomerSource customerSource, StripePaymentSource stripePaymentSource2, int i, Object obj) {
        if ((i & 1) != 0) {
            stripePaymentSource2 = customerSource.stripePaymentSource;
        }
        return customerSource.copy(stripePaymentSource2);
    }

    public final CustomerSource copy(StripePaymentSource stripePaymentSource2) {
        Intrinsics.checkParameterIsNotNull(stripePaymentSource2, "stripePaymentSource");
        return new CustomerSource(stripePaymentSource2);
    }

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            return (obj instanceof CustomerSource) && Intrinsics.areEqual((Object) this.stripePaymentSource, (Object) ((CustomerSource) obj).stripePaymentSource);
        }
        return true;
    }

    public int hashCode() {
        StripePaymentSource stripePaymentSource2 = this.stripePaymentSource;
        if (stripePaymentSource2 != null) {
            return stripePaymentSource2.hashCode();
        }
        return 0;
    }

    public String toString() {
        return "CustomerSource(stripePaymentSource=" + this.stripePaymentSource + ")";
    }

    public void writeToParcel(Parcel parcel, int i) {
        Intrinsics.checkParameterIsNotNull(parcel, "parcel");
        parcel.writeParcelable(this.stripePaymentSource, i);
    }

    public CustomerSource(StripePaymentSource stripePaymentSource2) {
        Intrinsics.checkParameterIsNotNull(stripePaymentSource2, "stripePaymentSource");
        this.stripePaymentSource = stripePaymentSource2;
    }

    public String getId() {
        return this.stripePaymentSource.getId();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:4:0x000f, code lost:
        r0 = ((com.stripe.android.model.SourceTypeModel.Card) r0.getSourceTypeModel()).getTokenizationMethod();
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.String getTokenizationMethod() {
        /*
            r3 = this;
            com.stripe.android.model.Source r0 = r3.asSource()
            r1 = 0
            if (r0 == 0) goto L_0x0025
            com.stripe.android.model.SourceTypeModel r2 = r0.getSourceTypeModel()
            boolean r2 = r2 instanceof com.stripe.android.model.SourceTypeModel.Card
            if (r2 == 0) goto L_0x0020
            com.stripe.android.model.SourceTypeModel r0 = r0.getSourceTypeModel()
            com.stripe.android.model.SourceTypeModel$Card r0 = (com.stripe.android.model.SourceTypeModel.Card) r0
            com.stripe.android.model.TokenizationMethod r0 = r0.getTokenizationMethod()
            if (r0 == 0) goto L_0x0020
            java.lang.String r0 = r0.getCode()
            goto L_0x0021
        L_0x0020:
            r0 = r1
        L_0x0021:
            if (r0 == 0) goto L_0x0025
            r1 = r0
            goto L_0x0035
        L_0x0025:
            com.stripe.android.model.Card r0 = r3.asCard()
            if (r0 == 0) goto L_0x0035
            com.stripe.android.model.TokenizationMethod r0 = r0.getTokenizationMethod()
            if (r0 == 0) goto L_0x0035
            java.lang.String r1 = r0.getCode()
        L_0x0035:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.stripe.android.model.CustomerSource.getTokenizationMethod():java.lang.String");
    }

    public final String getSourceType() {
        StripePaymentSource stripePaymentSource2 = this.stripePaymentSource;
        if (stripePaymentSource2 instanceof Card) {
            return "card";
        }
        return stripePaymentSource2 instanceof Source ? ((Source) stripePaymentSource2).getType() : "unknown";
    }

    public final Source asSource() {
        StripePaymentSource stripePaymentSource2 = this.stripePaymentSource;
        if (!(stripePaymentSource2 instanceof Source)) {
            stripePaymentSource2 = null;
        }
        return (Source) stripePaymentSource2;
    }

    public final Card asCard() {
        StripePaymentSource stripePaymentSource2 = this.stripePaymentSource;
        if (!(stripePaymentSource2 instanceof Card)) {
            stripePaymentSource2 = null;
        }
        return (Card) stripePaymentSource2;
    }
}
