package com.stripe.android.model;

import android.os.Parcel;
import android.os.Parcelable;
import com.stripe.android.model.MandateDataParams;
import com.stripe.android.model.PaymentMethodCreateParams;
import java.util.Map;
import kotlin.Metadata;
import kotlin.TuplesKt;
import kotlin.collections.MapsKt;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000P\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0010$\n\u0002\u0010\u0000\n\u0002\b\u0017\n\u0002\u0010\b\n\u0002\b\b\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\b\b\u0018\u0000 <2\u00020\u00012\u00020\u0002:\u0001<BU\b\u0000\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\n\b\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u0004\u0012\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u0012\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\u0004\u0012\b\b\u0002\u0010\t\u001a\u00020\n\u0012\n\b\u0002\u0010\u000b\u001a\u0004\u0018\u00010\u0004\u0012\n\b\u0002\u0010\f\u001a\u0004\u0018\u00010\r¢\u0006\u0002\u0010\u000eJ\t\u0010$\u001a\u00020\u0004HÆ\u0003J\u0010\u0010%\u001a\u0004\u0018\u00010\u0004HÀ\u0003¢\u0006\u0002\b&J\u0010\u0010'\u001a\u0004\u0018\u00010\u0007HÀ\u0003¢\u0006\u0002\b(J\u000b\u0010)\u001a\u0004\u0018\u00010\u0004HÆ\u0003J\t\u0010*\u001a\u00020\nHÂ\u0003J\u000b\u0010+\u001a\u0004\u0018\u00010\u0004HÆ\u0003J\u000b\u0010,\u001a\u0004\u0018\u00010\rHÆ\u0003JY\u0010-\u001a\u00020\u00002\b\b\u0002\u0010\u0003\u001a\u00020\u00042\n\b\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u00042\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u00072\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\u00042\b\b\u0002\u0010\t\u001a\u00020\n2\n\b\u0002\u0010\u000b\u001a\u0004\u0018\u00010\u00042\n\b\u0002\u0010\f\u001a\u0004\u0018\u00010\rHÆ\u0001J\t\u0010.\u001a\u00020/HÖ\u0001J\u0013\u00100\u001a\u00020\n2\b\u00101\u001a\u0004\u0018\u00010\u0017HÖ\u0003J\t\u00102\u001a\u00020/HÖ\u0001J\b\u00103\u001a\u00020\nH\u0016J\u0014\u00104\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00170\u0016H\u0016J\t\u00105\u001a\u00020\u0004HÖ\u0001J\u0010\u00106\u001a\u00020\u00002\u0006\u00103\u001a\u00020\nH\u0016J\u0019\u00107\u001a\u0002082\u0006\u00109\u001a\u00020:2\u0006\u0010;\u001a\u00020/HÖ\u0001R\u0014\u0010\u0003\u001a\u00020\u0004X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010R\u001c\u0010\f\u001a\u0004\u0018\u00010\rX\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0011\u0010\u0012\"\u0004\b\u0013\u0010\u0014R\"\u0010\u0015\u001a\u0010\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0017\u0018\u00010\u00168BX\u0004¢\u0006\u0006\u001a\u0004\b\u0018\u0010\u0019R\u001c\u0010\u000b\u001a\u0004\u0018\u00010\u0004X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u001a\u0010\u0010\"\u0004\b\u001b\u0010\u001cR\u0016\u0010\u0006\u001a\u0004\u0018\u00010\u0007X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u001d\u0010\u001eR\u0016\u0010\u0005\u001a\u0004\u0018\u00010\u0004X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u001f\u0010\u0010R \u0010 \u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00170\u00168BX\u0004¢\u0006\u0006\u001a\u0004\b!\u0010\u0019R\u001c\u0010\b\u001a\u0004\u0018\u00010\u0004X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\"\u0010\u0010\"\u0004\b#\u0010\u001cR\u000e\u0010\t\u001a\u00020\nX\u0004¢\u0006\u0002\n\u0000¨\u0006="}, d2 = {"Lcom/stripe/android/model/ConfirmSetupIntentParams;", "Lcom/stripe/android/model/ConfirmStripeIntentParams;", "Landroid/os/Parcelable;", "clientSecret", "", "paymentMethodId", "paymentMethodCreateParams", "Lcom/stripe/android/model/PaymentMethodCreateParams;", "returnUrl", "useStripeSdk", "", "mandateId", "mandateData", "Lcom/stripe/android/model/MandateDataParams;", "(Ljava/lang/String;Ljava/lang/String;Lcom/stripe/android/model/PaymentMethodCreateParams;Ljava/lang/String;ZLjava/lang/String;Lcom/stripe/android/model/MandateDataParams;)V", "getClientSecret", "()Ljava/lang/String;", "getMandateData", "()Lcom/stripe/android/model/MandateDataParams;", "setMandateData", "(Lcom/stripe/android/model/MandateDataParams;)V", "mandateDataParams", "", "", "getMandateDataParams", "()Ljava/util/Map;", "getMandateId", "setMandateId", "(Ljava/lang/String;)V", "getPaymentMethodCreateParams$stripe_release", "()Lcom/stripe/android/model/PaymentMethodCreateParams;", "getPaymentMethodId$stripe_release", "paymentMethodParamMap", "getPaymentMethodParamMap", "getReturnUrl", "setReturnUrl", "component1", "component2", "component2$stripe_release", "component3", "component3$stripe_release", "component4", "component5", "component6", "component7", "copy", "describeContents", "", "equals", "other", "hashCode", "shouldUseStripeSdk", "toParamMap", "toString", "withShouldUseStripeSdk", "writeToParcel", "", "parcel", "Landroid/os/Parcel;", "flags", "Companion", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: ConfirmSetupIntentParams.kt */
public final class ConfirmSetupIntentParams implements ConfirmStripeIntentParams, Parcelable {
    public static final Parcelable.Creator CREATOR = new Creator();
    public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
    private final String clientSecret;
    private MandateDataParams mandateData;
    private String mandateId;
    private final PaymentMethodCreateParams paymentMethodCreateParams;
    private final String paymentMethodId;
    private String returnUrl;
    private final boolean useStripeSdk;

    @Metadata(bv = {1, 0, 3}, k = 3, mv = {1, 1, 16})
    public static class Creator implements Parcelable.Creator {
        public final Object createFromParcel(Parcel parcel) {
            Intrinsics.checkParameterIsNotNull(parcel, "in");
            return new ConfirmSetupIntentParams(parcel.readString(), parcel.readString(), parcel.readInt() != 0 ? (PaymentMethodCreateParams) PaymentMethodCreateParams.CREATOR.createFromParcel(parcel) : null, parcel.readString(), parcel.readInt() != 0, parcel.readString(), parcel.readInt() != 0 ? (MandateDataParams) MandateDataParams.CREATOR.createFromParcel(parcel) : null);
        }

        public final Object[] newArray(int i) {
            return new ConfirmSetupIntentParams[i];
        }
    }

    private final boolean component5() {
        return this.useStripeSdk;
    }

    public static /* synthetic */ ConfirmSetupIntentParams copy$default(ConfirmSetupIntentParams confirmSetupIntentParams, String str, String str2, PaymentMethodCreateParams paymentMethodCreateParams2, String str3, boolean z, String str4, MandateDataParams mandateDataParams, int i, Object obj) {
        if ((i & 1) != 0) {
            str = confirmSetupIntentParams.getClientSecret();
        }
        if ((i & 2) != 0) {
            str2 = confirmSetupIntentParams.paymentMethodId;
        }
        String str5 = str2;
        if ((i & 4) != 0) {
            paymentMethodCreateParams2 = confirmSetupIntentParams.paymentMethodCreateParams;
        }
        PaymentMethodCreateParams paymentMethodCreateParams3 = paymentMethodCreateParams2;
        if ((i & 8) != 0) {
            str3 = confirmSetupIntentParams.returnUrl;
        }
        String str6 = str3;
        if ((i & 16) != 0) {
            z = confirmSetupIntentParams.useStripeSdk;
        }
        boolean z2 = z;
        if ((i & 32) != 0) {
            str4 = confirmSetupIntentParams.mandateId;
        }
        String str7 = str4;
        if ((i & 64) != 0) {
            mandateDataParams = confirmSetupIntentParams.mandateData;
        }
        return confirmSetupIntentParams.copy(str, str5, paymentMethodCreateParams3, str6, z2, str7, mandateDataParams);
    }

    @JvmStatic
    public static final ConfirmSetupIntentParams create(PaymentMethodCreateParams paymentMethodCreateParams2, String str) {
        return Companion.create$default(Companion, paymentMethodCreateParams2, str, (String) null, (String) null, (MandateDataParams) null, 28, (Object) null);
    }

    @JvmStatic
    public static final ConfirmSetupIntentParams create(PaymentMethodCreateParams paymentMethodCreateParams2, String str, String str2) {
        return Companion.create$default(Companion, paymentMethodCreateParams2, str, str2, (String) null, (MandateDataParams) null, 24, (Object) null);
    }

    @JvmStatic
    public static final ConfirmSetupIntentParams create(PaymentMethodCreateParams paymentMethodCreateParams2, String str, String str2, String str3) {
        return Companion.create$default(Companion, paymentMethodCreateParams2, str, str2, str3, (MandateDataParams) null, 16, (Object) null);
    }

    @JvmStatic
    public static final ConfirmSetupIntentParams create(PaymentMethodCreateParams paymentMethodCreateParams2, String str, String str2, String str3, MandateDataParams mandateDataParams) {
        return Companion.create(paymentMethodCreateParams2, str, str2, str3, mandateDataParams);
    }

    @JvmStatic
    public static final ConfirmSetupIntentParams create(String str, String str2) {
        return Companion.create$default(Companion, str, str2, (String) null, (String) null, (MandateDataParams) null, 28, (Object) null);
    }

    @JvmStatic
    public static final ConfirmSetupIntentParams create(String str, String str2, String str3) {
        return Companion.create$default(Companion, str, str2, str3, (String) null, (MandateDataParams) null, 24, (Object) null);
    }

    @JvmStatic
    public static final ConfirmSetupIntentParams create(String str, String str2, String str3, String str4) {
        return Companion.create$default(Companion, str, str2, str3, str4, (MandateDataParams) null, 16, (Object) null);
    }

    @JvmStatic
    public static final ConfirmSetupIntentParams create(String str, String str2, String str3, String str4, MandateDataParams mandateDataParams) {
        return Companion.create(str, str2, str3, str4, mandateDataParams);
    }

    @JvmStatic
    public static final ConfirmSetupIntentParams createWithoutPaymentMethod(String str) {
        return Companion.createWithoutPaymentMethod$default(Companion, str, (String) null, 2, (Object) null);
    }

    @JvmStatic
    public static final ConfirmSetupIntentParams createWithoutPaymentMethod(String str, String str2) {
        return Companion.createWithoutPaymentMethod(str, str2);
    }

    public final String component1() {
        return getClientSecret();
    }

    public final String component2$stripe_release() {
        return this.paymentMethodId;
    }

    public final PaymentMethodCreateParams component3$stripe_release() {
        return this.paymentMethodCreateParams;
    }

    public final String component4() {
        return this.returnUrl;
    }

    public final String component6() {
        return this.mandateId;
    }

    public final MandateDataParams component7() {
        return this.mandateData;
    }

    public final ConfirmSetupIntentParams copy(String str, String str2, PaymentMethodCreateParams paymentMethodCreateParams2, String str3, boolean z, String str4, MandateDataParams mandateDataParams) {
        Intrinsics.checkParameterIsNotNull(str, "clientSecret");
        return new ConfirmSetupIntentParams(str, str2, paymentMethodCreateParams2, str3, z, str4, mandateDataParams);
    }

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ConfirmSetupIntentParams)) {
            return false;
        }
        ConfirmSetupIntentParams confirmSetupIntentParams = (ConfirmSetupIntentParams) obj;
        return Intrinsics.areEqual((Object) getClientSecret(), (Object) confirmSetupIntentParams.getClientSecret()) && Intrinsics.areEqual((Object) this.paymentMethodId, (Object) confirmSetupIntentParams.paymentMethodId) && Intrinsics.areEqual((Object) this.paymentMethodCreateParams, (Object) confirmSetupIntentParams.paymentMethodCreateParams) && Intrinsics.areEqual((Object) this.returnUrl, (Object) confirmSetupIntentParams.returnUrl) && this.useStripeSdk == confirmSetupIntentParams.useStripeSdk && Intrinsics.areEqual((Object) this.mandateId, (Object) confirmSetupIntentParams.mandateId) && Intrinsics.areEqual((Object) this.mandateData, (Object) confirmSetupIntentParams.mandateData);
    }

    public int hashCode() {
        String clientSecret2 = getClientSecret();
        int i = 0;
        int hashCode = (clientSecret2 != null ? clientSecret2.hashCode() : 0) * 31;
        String str = this.paymentMethodId;
        int hashCode2 = (hashCode + (str != null ? str.hashCode() : 0)) * 31;
        PaymentMethodCreateParams paymentMethodCreateParams2 = this.paymentMethodCreateParams;
        int hashCode3 = (hashCode2 + (paymentMethodCreateParams2 != null ? paymentMethodCreateParams2.hashCode() : 0)) * 31;
        String str2 = this.returnUrl;
        int hashCode4 = (hashCode3 + (str2 != null ? str2.hashCode() : 0)) * 31;
        boolean z = this.useStripeSdk;
        if (z) {
            z = true;
        }
        int i2 = (hashCode4 + (z ? 1 : 0)) * 31;
        String str3 = this.mandateId;
        int hashCode5 = (i2 + (str3 != null ? str3.hashCode() : 0)) * 31;
        MandateDataParams mandateDataParams = this.mandateData;
        if (mandateDataParams != null) {
            i = mandateDataParams.hashCode();
        }
        return hashCode5 + i;
    }

    public String toString() {
        return "ConfirmSetupIntentParams(clientSecret=" + getClientSecret() + ", paymentMethodId=" + this.paymentMethodId + ", paymentMethodCreateParams=" + this.paymentMethodCreateParams + ", returnUrl=" + this.returnUrl + ", useStripeSdk=" + this.useStripeSdk + ", mandateId=" + this.mandateId + ", mandateData=" + this.mandateData + ")";
    }

    public void writeToParcel(Parcel parcel, int i) {
        Intrinsics.checkParameterIsNotNull(parcel, "parcel");
        parcel.writeString(this.clientSecret);
        parcel.writeString(this.paymentMethodId);
        PaymentMethodCreateParams paymentMethodCreateParams2 = this.paymentMethodCreateParams;
        if (paymentMethodCreateParams2 != null) {
            parcel.writeInt(1);
            paymentMethodCreateParams2.writeToParcel(parcel, 0);
        } else {
            parcel.writeInt(0);
        }
        parcel.writeString(this.returnUrl);
        parcel.writeInt(this.useStripeSdk ? 1 : 0);
        parcel.writeString(this.mandateId);
        MandateDataParams mandateDataParams = this.mandateData;
        if (mandateDataParams != null) {
            parcel.writeInt(1);
            mandateDataParams.writeToParcel(parcel, 0);
            return;
        }
        parcel.writeInt(0);
    }

    public ConfirmSetupIntentParams(String str, String str2, PaymentMethodCreateParams paymentMethodCreateParams2, String str3, boolean z, String str4, MandateDataParams mandateDataParams) {
        Intrinsics.checkParameterIsNotNull(str, "clientSecret");
        this.clientSecret = str;
        this.paymentMethodId = str2;
        this.paymentMethodCreateParams = paymentMethodCreateParams2;
        this.returnUrl = str3;
        this.useStripeSdk = z;
        this.mandateId = str4;
        this.mandateData = mandateDataParams;
    }

    public /* synthetic */ String getClientSecret() {
        return this.clientSecret;
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* synthetic */ ConfirmSetupIntentParams(java.lang.String r8, java.lang.String r9, com.stripe.android.model.PaymentMethodCreateParams r10, java.lang.String r11, boolean r12, java.lang.String r13, com.stripe.android.model.MandateDataParams r14, int r15, kotlin.jvm.internal.DefaultConstructorMarker r16) {
        /*
            r7 = this;
            r0 = r15 & 2
            r1 = 0
            if (r0 == 0) goto L_0x0009
            r0 = r1
            java.lang.String r0 = (java.lang.String) r0
            goto L_0x000a
        L_0x0009:
            r0 = r9
        L_0x000a:
            r2 = r15 & 4
            if (r2 == 0) goto L_0x0012
            r2 = r1
            com.stripe.android.model.PaymentMethodCreateParams r2 = (com.stripe.android.model.PaymentMethodCreateParams) r2
            goto L_0x0013
        L_0x0012:
            r2 = r10
        L_0x0013:
            r3 = r15 & 8
            if (r3 == 0) goto L_0x001b
            r3 = r1
            java.lang.String r3 = (java.lang.String) r3
            goto L_0x001c
        L_0x001b:
            r3 = r11
        L_0x001c:
            r4 = r15 & 16
            if (r4 == 0) goto L_0x0022
            r4 = 0
            goto L_0x0023
        L_0x0022:
            r4 = r12
        L_0x0023:
            r5 = r15 & 32
            if (r5 == 0) goto L_0x002b
            r5 = r1
            java.lang.String r5 = (java.lang.String) r5
            goto L_0x002c
        L_0x002b:
            r5 = r13
        L_0x002c:
            r6 = r15 & 64
            if (r6 == 0) goto L_0x0033
            com.stripe.android.model.MandateDataParams r1 = (com.stripe.android.model.MandateDataParams) r1
            goto L_0x0034
        L_0x0033:
            r1 = r14
        L_0x0034:
            r9 = r7
            r10 = r8
            r11 = r0
            r12 = r2
            r13 = r3
            r14 = r4
            r15 = r5
            r16 = r1
            r9.<init>(r10, r11, r12, r13, r14, r15, r16)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.stripe.android.model.ConfirmSetupIntentParams.<init>(java.lang.String, java.lang.String, com.stripe.android.model.PaymentMethodCreateParams, java.lang.String, boolean, java.lang.String, com.stripe.android.model.MandateDataParams, int, kotlin.jvm.internal.DefaultConstructorMarker):void");
    }

    public final /* synthetic */ String getPaymentMethodId$stripe_release() {
        return this.paymentMethodId;
    }

    public final /* synthetic */ PaymentMethodCreateParams getPaymentMethodCreateParams$stripe_release() {
        return this.paymentMethodCreateParams;
    }

    public final String getReturnUrl() {
        return this.returnUrl;
    }

    public final void setReturnUrl(String str) {
        this.returnUrl = str;
    }

    public final String getMandateId() {
        return this.mandateId;
    }

    public final void setMandateId(String str) {
        this.mandateId = str;
    }

    public final MandateDataParams getMandateData() {
        return this.mandateData;
    }

    public final void setMandateData(MandateDataParams mandateDataParams) {
        this.mandateData = mandateDataParams;
    }

    public boolean shouldUseStripeSdk() {
        return this.useStripeSdk;
    }

    public ConfirmSetupIntentParams withShouldUseStripeSdk(boolean z) {
        return copy$default(this, (String) null, (String) null, (PaymentMethodCreateParams) null, (String) null, z, (String) null, (MandateDataParams) null, 111, (Object) null);
    }

    public Map<String, Object> toParamMap() {
        Map mapOf = MapsKt.mapOf(TuplesKt.to("client_secret", getClientSecret()), TuplesKt.to("use_stripe_sdk", Boolean.valueOf(this.useStripeSdk)));
        String str = this.returnUrl;
        Map<K, V> map = null;
        Map mapOf2 = str != null ? MapsKt.mapOf(TuplesKt.to("return_url", str)) : null;
        if (mapOf2 == null) {
            mapOf2 = MapsKt.emptyMap();
        }
        Map plus = MapsKt.plus(mapOf, mapOf2);
        String str2 = this.mandateId;
        Map mapOf3 = str2 != null ? MapsKt.mapOf(TuplesKt.to("mandate", str2)) : null;
        if (mapOf3 == null) {
            mapOf3 = MapsKt.emptyMap();
        }
        Map plus2 = MapsKt.plus(plus, mapOf3);
        Map<String, Object> mandateDataParams = getMandateDataParams();
        if (mandateDataParams != null) {
            map = MapsKt.mapOf(TuplesKt.to("mandate_data", mandateDataParams));
        }
        if (map == null) {
            map = MapsKt.emptyMap();
        }
        return MapsKt.plus(MapsKt.plus(plus2, (Map) map), (Map<K, V>) getPaymentMethodParamMap());
    }

    private final Map<String, Object> getPaymentMethodParamMap() {
        PaymentMethodCreateParams paymentMethodCreateParams2 = this.paymentMethodCreateParams;
        if (paymentMethodCreateParams2 != null) {
            return MapsKt.mapOf(TuplesKt.to("payment_method_data", paymentMethodCreateParams2.toParamMap()));
        }
        String str = this.paymentMethodId;
        if (str != null) {
            return MapsKt.mapOf(TuplesKt.to("payment_method", str));
        }
        return MapsKt.emptyMap();
    }

    private final Map<String, Object> getMandateDataParams() {
        PaymentMethodCreateParams.Type type$stripe_release;
        Map<String, Object> paramMap;
        MandateDataParams mandateDataParams = this.mandateData;
        if (mandateDataParams != null && (paramMap = mandateDataParams.toParamMap()) != null) {
            return paramMap;
        }
        PaymentMethodCreateParams paymentMethodCreateParams2 = this.paymentMethodCreateParams;
        if (paymentMethodCreateParams2 == null || (type$stripe_release = paymentMethodCreateParams2.getType$stripe_release()) == null || !type$stripe_release.getHasMandate() || this.mandateId != null) {
            return null;
        }
        return new MandateDataParams(MandateDataParams.Type.Online.Companion.getDEFAULT$stripe_release()).toParamMap();
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J<\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\n\b\u0002\u0010\t\u001a\u0004\u0018\u00010\b2\n\b\u0002\u0010\n\u001a\u0004\u0018\u00010\b2\n\b\u0002\u0010\u000b\u001a\u0004\u0018\u00010\fH\u0007J<\u0010\u0003\u001a\u00020\u00042\u0006\u0010\r\u001a\u00020\b2\u0006\u0010\u0007\u001a\u00020\b2\n\b\u0002\u0010\t\u001a\u0004\u0018\u00010\b2\n\b\u0002\u0010\n\u001a\u0004\u0018\u00010\b2\n\b\u0002\u0010\u000b\u001a\u0004\u0018\u00010\fH\u0007J\u001c\u0010\u000e\u001a\u00020\u00042\u0006\u0010\u0007\u001a\u00020\b2\n\b\u0002\u0010\t\u001a\u0004\u0018\u00010\bH\u0007¨\u0006\u000f"}, d2 = {"Lcom/stripe/android/model/ConfirmSetupIntentParams$Companion;", "", "()V", "create", "Lcom/stripe/android/model/ConfirmSetupIntentParams;", "paymentMethodCreateParams", "Lcom/stripe/android/model/PaymentMethodCreateParams;", "clientSecret", "", "returnUrl", "mandateId", "mandateData", "Lcom/stripe/android/model/MandateDataParams;", "paymentMethodId", "createWithoutPaymentMethod", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: ConfirmSetupIntentParams.kt */
    public static final class Companion {
        @JvmStatic
        public final ConfirmSetupIntentParams create(PaymentMethodCreateParams paymentMethodCreateParams, String str) {
            return create$default(this, paymentMethodCreateParams, str, (String) null, (String) null, (MandateDataParams) null, 28, (Object) null);
        }

        @JvmStatic
        public final ConfirmSetupIntentParams create(PaymentMethodCreateParams paymentMethodCreateParams, String str, String str2) {
            return create$default(this, paymentMethodCreateParams, str, str2, (String) null, (MandateDataParams) null, 24, (Object) null);
        }

        @JvmStatic
        public final ConfirmSetupIntentParams create(PaymentMethodCreateParams paymentMethodCreateParams, String str, String str2, String str3) {
            return create$default(this, paymentMethodCreateParams, str, str2, str3, (MandateDataParams) null, 16, (Object) null);
        }

        @JvmStatic
        public final ConfirmSetupIntentParams create(String str, String str2) {
            return create$default(this, str, str2, (String) null, (String) null, (MandateDataParams) null, 28, (Object) null);
        }

        @JvmStatic
        public final ConfirmSetupIntentParams create(String str, String str2, String str3) {
            return create$default(this, str, str2, str3, (String) null, (MandateDataParams) null, 24, (Object) null);
        }

        @JvmStatic
        public final ConfirmSetupIntentParams create(String str, String str2, String str3, String str4) {
            return create$default(this, str, str2, str3, str4, (MandateDataParams) null, 16, (Object) null);
        }

        @JvmStatic
        public final ConfirmSetupIntentParams createWithoutPaymentMethod(String str) {
            return createWithoutPaymentMethod$default(this, str, (String) null, 2, (Object) null);
        }

        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        public static /* synthetic */ ConfirmSetupIntentParams createWithoutPaymentMethod$default(Companion companion, String str, String str2, int i, Object obj) {
            if ((i & 2) != 0) {
                str2 = null;
            }
            return companion.createWithoutPaymentMethod(str, str2);
        }

        @JvmStatic
        public final ConfirmSetupIntentParams createWithoutPaymentMethod(String str, String str2) {
            Intrinsics.checkParameterIsNotNull(str, "clientSecret");
            return new ConfirmSetupIntentParams(str, (String) null, (PaymentMethodCreateParams) null, str2, false, (String) null, (MandateDataParams) null, 118, (DefaultConstructorMarker) null);
        }

        public static /* synthetic */ ConfirmSetupIntentParams create$default(Companion companion, String str, String str2, String str3, String str4, MandateDataParams mandateDataParams, int i, Object obj) {
            if ((i & 4) != 0) {
                str3 = null;
            }
            String str5 = str3;
            if ((i & 8) != 0) {
                str4 = null;
            }
            String str6 = str4;
            if ((i & 16) != 0) {
                mandateDataParams = null;
            }
            return companion.create(str, str2, str5, str6, mandateDataParams);
        }

        @JvmStatic
        public final ConfirmSetupIntentParams create(String str, String str2, String str3, String str4, MandateDataParams mandateDataParams) {
            Intrinsics.checkParameterIsNotNull(str, "paymentMethodId");
            String str5 = str2;
            Intrinsics.checkParameterIsNotNull(str2, "clientSecret");
            return new ConfirmSetupIntentParams(str5, str, (PaymentMethodCreateParams) null, str3, false, str4, mandateDataParams, 20, (DefaultConstructorMarker) null);
        }

        public static /* synthetic */ ConfirmSetupIntentParams create$default(Companion companion, PaymentMethodCreateParams paymentMethodCreateParams, String str, String str2, String str3, MandateDataParams mandateDataParams, int i, Object obj) {
            if ((i & 4) != 0) {
                str2 = null;
            }
            String str4 = str2;
            if ((i & 8) != 0) {
                str3 = null;
            }
            String str5 = str3;
            if ((i & 16) != 0) {
                mandateDataParams = null;
            }
            return companion.create(paymentMethodCreateParams, str, str4, str5, mandateDataParams);
        }

        @JvmStatic
        public final ConfirmSetupIntentParams create(PaymentMethodCreateParams paymentMethodCreateParams, String str, String str2, String str3, MandateDataParams mandateDataParams) {
            Intrinsics.checkParameterIsNotNull(paymentMethodCreateParams, "paymentMethodCreateParams");
            String str4 = str;
            Intrinsics.checkParameterIsNotNull(str, "clientSecret");
            return new ConfirmSetupIntentParams(str4, (String) null, paymentMethodCreateParams, str2, false, str3, mandateDataParams, 18, (DefaultConstructorMarker) null);
        }
    }
}
