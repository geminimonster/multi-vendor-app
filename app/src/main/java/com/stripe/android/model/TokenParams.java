package com.stripe.android.model;

import android.os.Parcelable;
import java.util.Set;
import kotlin.Metadata;
import kotlin.collections.SetsKt;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\"\n\u0002\b\u0006\b&\u0018\u00002\u00020\u00012\u00020\u0002B\u001d\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u000e\b\u0002\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00040\u0006¢\u0006\u0002\u0010\u0007R\u001a\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00040\u0006X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u0014\u0010\u0003\u001a\u00020\u0004X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000b¨\u0006\f"}, d2 = {"Lcom/stripe/android/model/TokenParams;", "Lcom/stripe/android/model/StripeParamsModel;", "Landroid/os/Parcelable;", "tokenType", "", "attribution", "", "(Ljava/lang/String;Ljava/util/Set;)V", "getAttribution$stripe_release", "()Ljava/util/Set;", "getTokenType$stripe_release", "()Ljava/lang/String;", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: TokenParams.kt */
public abstract class TokenParams implements StripeParamsModel, Parcelable {
    private final Set<String> attribution;
    private final String tokenType;

    public TokenParams(String str, Set<String> set) {
        Intrinsics.checkParameterIsNotNull(str, "tokenType");
        Intrinsics.checkParameterIsNotNull(set, "attribution");
        this.tokenType = str;
        this.attribution = set;
    }

    public final String getTokenType$stripe_release() {
        return this.tokenType;
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ TokenParams(String str, Set set, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(str, (i & 2) != 0 ? SetsKt.emptySet() : set);
    }

    public final Set<String> getAttribution$stripe_release() {
        return this.attribution;
    }
}
