package com.stripe.android.model;

import android.os.Parcel;
import android.os.Parcelable;
import com.stripe.android.model.BankAccountTokenParams;
import java.lang.annotation.RetentionPolicy;
import java.util.Map;
import java.util.Set;
import kotlin.Deprecated;
import kotlin.Metadata;
import kotlin.annotation.AnnotationRetention;
import kotlin.annotation.Retention;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000H\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\f\n\u0002\u0018\u0002\n\u0002\b\u001d\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\b\b\u0018\u00002\u00020\u00012\u00020\u0002:\u0002=>BG\b\u0017\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\b\b\u0001\u0010\u0005\u001a\u00020\u0004\u0012\b\b\u0001\u0010\u0006\u001a\u00020\u0004\u0012\n\b\u0002\u0010\u0007\u001a\u0004\u0018\u00010\u0004\u0012\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\u0004\u0012\n\b\u0002\u0010\t\u001a\u0004\u0018\u00010\u0004¢\u0006\u0002\u0010\nB[\b\u0017\u0012\b\u0010\b\u001a\u0004\u0018\u00010\u0004\u0012\b\u0010\t\u001a\u0004\u0018\u00010\u0004\u0012\b\u0010\u000b\u001a\u0004\u0018\u00010\u0004\u0012\n\b\u0001\u0010\u0005\u001a\u0004\u0018\u00010\u0004\u0012\n\b\u0001\u0010\u0006\u001a\u0004\u0018\u00010\u0004\u0012\b\u0010\f\u001a\u0004\u0018\u00010\u0004\u0012\b\u0010\r\u001a\u0004\u0018\u00010\u0004\u0012\b\u0010\u0007\u001a\u0004\u0018\u00010\u0004¢\u0006\u0002\u0010\u000eB\u0001\b\u0000\u0012\n\b\u0002\u0010\u000f\u001a\u0004\u0018\u00010\u0004\u0012\n\b\u0002\u0010\u0003\u001a\u0004\u0018\u00010\u0004\u0012\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\u0004\u0012\n\b\u0002\u0010\t\u001a\u0004\u0018\u00010\u0004\u0012\n\b\u0002\u0010\u000b\u001a\u0004\u0018\u00010\u0004\u0012\n\b\u0003\u0010\u0005\u001a\u0004\u0018\u00010\u0004\u0012\n\b\u0003\u0010\u0006\u001a\u0004\u0018\u00010\u0004\u0012\n\b\u0002\u0010\f\u001a\u0004\u0018\u00010\u0004\u0012\n\b\u0002\u0010\r\u001a\u0004\u0018\u00010\u0004\u0012\n\b\u0002\u0010\u0007\u001a\u0004\u0018\u00010\u0004\u0012\n\b\u0002\u0010\u0010\u001a\u0004\u0018\u00010\u0011¢\u0006\u0002\u0010\u0012J\u000b\u0010\"\u001a\u0004\u0018\u00010\u0004HÆ\u0003J\u000b\u0010#\u001a\u0004\u0018\u00010\u0004HÆ\u0003J\u000b\u0010$\u001a\u0004\u0018\u00010\u0011HÆ\u0003J\u000b\u0010%\u001a\u0004\u0018\u00010\u0004HÆ\u0003J\u000b\u0010&\u001a\u0004\u0018\u00010\u0004HÆ\u0003J\u000b\u0010'\u001a\u0004\u0018\u00010\u0004HÆ\u0003J\u000b\u0010(\u001a\u0004\u0018\u00010\u0004HÆ\u0003J\u000b\u0010)\u001a\u0004\u0018\u00010\u0004HÆ\u0003J\u000b\u0010*\u001a\u0004\u0018\u00010\u0004HÆ\u0003J\u000b\u0010+\u001a\u0004\u0018\u00010\u0004HÆ\u0003J\u000b\u0010,\u001a\u0004\u0018\u00010\u0004HÆ\u0003J\u0001\u0010-\u001a\u00020\u00002\n\b\u0002\u0010\u000f\u001a\u0004\u0018\u00010\u00042\n\b\u0002\u0010\u0003\u001a\u0004\u0018\u00010\u00042\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\u00042\n\b\u0002\u0010\t\u001a\u0004\u0018\u00010\u00042\n\b\u0002\u0010\u000b\u001a\u0004\u0018\u00010\u00042\n\b\u0003\u0010\u0005\u001a\u0004\u0018\u00010\u00042\n\b\u0003\u0010\u0006\u001a\u0004\u0018\u00010\u00042\n\b\u0002\u0010\f\u001a\u0004\u0018\u00010\u00042\n\b\u0002\u0010\r\u001a\u0004\u0018\u00010\u00042\n\b\u0002\u0010\u0007\u001a\u0004\u0018\u00010\u00042\n\b\u0002\u0010\u0010\u001a\u0004\u0018\u00010\u0011HÆ\u0001J\t\u0010.\u001a\u00020/HÖ\u0001J\u0013\u00100\u001a\u0002012\b\u00102\u001a\u0004\u0018\u000103HÖ\u0003J\t\u00104\u001a\u00020/HÖ\u0001J\u0014\u00105\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020306H\u0016J\t\u00107\u001a\u00020\u0004HÖ\u0001J\u0019\u00108\u001a\u0002092\u0006\u0010:\u001a\u00020;2\u0006\u0010<\u001a\u00020/HÖ\u0001R\u0013\u0010\b\u001a\u0004\u0018\u00010\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u0014R\u0013\u0010\t\u001a\u0004\u0018\u00010\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0015\u0010\u0014R\u001e\u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0006X\u0004¢\u0006\u000e\n\u0000\u0012\u0004\b\u0016\u0010\u0017\u001a\u0004\b\u0018\u0010\u0014R\u0013\u0010\u000b\u001a\u0004\u0018\u00010\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0019\u0010\u0014R\u0016\u0010\u0005\u001a\u0004\u0018\u00010\u0004X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u001a\u0010\u0014R\u0016\u0010\u0006\u001a\u0004\u0018\u00010\u0004X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u001b\u0010\u0014R\u0013\u0010\f\u001a\u0004\u0018\u00010\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u001c\u0010\u0014R\u0013\u0010\u000f\u001a\u0004\u0018\u00010\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u001d\u0010\u0014R\u0013\u0010\r\u001a\u0004\u0018\u00010\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u001e\u0010\u0014R\u0013\u0010\u0007\u001a\u0004\u0018\u00010\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u001f\u0010\u0014R\u0013\u0010\u0010\u001a\u0004\u0018\u00010\u0011¢\u0006\b\n\u0000\u001a\u0004\b \u0010!¨\u0006?"}, d2 = {"Lcom/stripe/android/model/BankAccount;", "Lcom/stripe/android/model/StripeModel;", "Lcom/stripe/android/model/TokenParams;", "accountNumber", "", "countryCode", "currency", "routingNumber", "accountHolderName", "accountHolderType", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", "bankName", "fingerprint", "last4", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", "id", "status", "Lcom/stripe/android/model/BankAccount$Status;", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/stripe/android/model/BankAccount$Status;)V", "getAccountHolderName", "()Ljava/lang/String;", "getAccountHolderType", "accountNumber$annotations", "()V", "getAccountNumber", "getBankName", "getCountryCode", "getCurrency", "getFingerprint", "getId", "getLast4", "getRoutingNumber", "getStatus", "()Lcom/stripe/android/model/BankAccount$Status;", "component1", "component10", "component11", "component2", "component3", "component4", "component5", "component6", "component7", "component8", "component9", "copy", "describeContents", "", "equals", "", "other", "", "hashCode", "toParamMap", "", "toString", "writeToParcel", "", "parcel", "Landroid/os/Parcel;", "flags", "BankAccountType", "Status", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: BankAccount.kt */
public final class BankAccount extends TokenParams implements StripeModel {
    public static final Parcelable.Creator CREATOR = new Creator();
    private final String accountHolderName;
    private final String accountHolderType;
    private final String accountNumber;
    private final String bankName;
    private final String countryCode;
    private final String currency;
    private final String fingerprint;
    private final String id;
    private final String last4;
    private final String routingNumber;
    private final Status status;

    @Metadata(bv = {1, 0, 3}, k = 3, mv = {1, 1, 16})
    public static class Creator implements Parcelable.Creator {
        public final Object createFromParcel(Parcel parcel) {
            Status status;
            Intrinsics.checkParameterIsNotNull(parcel, "in");
            String readString = parcel.readString();
            String readString2 = parcel.readString();
            String readString3 = parcel.readString();
            String readString4 = parcel.readString();
            String readString5 = parcel.readString();
            String readString6 = parcel.readString();
            String readString7 = parcel.readString();
            String readString8 = parcel.readString();
            String readString9 = parcel.readString();
            String readString10 = parcel.readString();
            if (parcel.readInt() != 0) {
                status = (Status) Enum.valueOf(Status.class, parcel.readString());
            } else {
                status = null;
            }
            return new BankAccount(readString, readString2, readString3, readString4, readString5, readString6, readString7, readString8, readString9, readString10, status);
        }

        public final Object[] newArray(int i) {
            return new BankAccount[i];
        }
    }

    public BankAccount() {
        this((String) null, (String) null, (String) null, (String) null, (String) null, (String) null, (String) null, (String) null, (String) null, (String) null, (Status) null, 2047, (DefaultConstructorMarker) null);
    }

    @Deprecated(message = "Use BankAccountTokenParams")
    public BankAccount(String str, String str2, String str3) {
        this(str, str2, str3, (String) null, (String) null, (String) null, 56, (DefaultConstructorMarker) null);
    }

    @Deprecated(message = "Use BankAccountTokenParams")
    public BankAccount(String str, String str2, String str3, String str4) {
        this(str, str2, str3, str4, (String) null, (String) null, 48, (DefaultConstructorMarker) null);
    }

    @Deprecated(message = "Use BankAccountTokenParams")
    public BankAccount(String str, String str2, String str3, String str4, String str5) {
        this(str, str2, str3, str4, str5, (String) null, 32, (DefaultConstructorMarker) null);
    }

    @Deprecated(message = "Use BankAccountTokenParams")
    public static /* synthetic */ void accountNumber$annotations() {
    }

    public static /* synthetic */ BankAccount copy$default(BankAccount bankAccount, String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, String str9, String str10, Status status2, int i, Object obj) {
        BankAccount bankAccount2 = bankAccount;
        int i2 = i;
        return bankAccount.copy((i2 & 1) != 0 ? bankAccount2.id : str, (i2 & 2) != 0 ? bankAccount2.accountNumber : str2, (i2 & 4) != 0 ? bankAccount2.accountHolderName : str3, (i2 & 8) != 0 ? bankAccount2.accountHolderType : str4, (i2 & 16) != 0 ? bankAccount2.bankName : str5, (i2 & 32) != 0 ? bankAccount2.countryCode : str6, (i2 & 64) != 0 ? bankAccount2.currency : str7, (i2 & 128) != 0 ? bankAccount2.fingerprint : str8, (i2 & 256) != 0 ? bankAccount2.last4 : str9, (i2 & 512) != 0 ? bankAccount2.routingNumber : str10, (i2 & 1024) != 0 ? bankAccount2.status : status2);
    }

    public final String component1() {
        return this.id;
    }

    public final String component10() {
        return this.routingNumber;
    }

    public final Status component11() {
        return this.status;
    }

    public final String component2() {
        return this.accountNumber;
    }

    public final String component3() {
        return this.accountHolderName;
    }

    public final String component4() {
        return this.accountHolderType;
    }

    public final String component5() {
        return this.bankName;
    }

    public final String component6() {
        return this.countryCode;
    }

    public final String component7() {
        return this.currency;
    }

    public final String component8() {
        return this.fingerprint;
    }

    public final String component9() {
        return this.last4;
    }

    public final BankAccount copy(String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, String str9, String str10, Status status2) {
        return new BankAccount(str, str2, str3, str4, str5, str6, str7, str8, str9, str10, status2);
    }

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof BankAccount)) {
            return false;
        }
        BankAccount bankAccount = (BankAccount) obj;
        return Intrinsics.areEqual((Object) this.id, (Object) bankAccount.id) && Intrinsics.areEqual((Object) this.accountNumber, (Object) bankAccount.accountNumber) && Intrinsics.areEqual((Object) this.accountHolderName, (Object) bankAccount.accountHolderName) && Intrinsics.areEqual((Object) this.accountHolderType, (Object) bankAccount.accountHolderType) && Intrinsics.areEqual((Object) this.bankName, (Object) bankAccount.bankName) && Intrinsics.areEqual((Object) this.countryCode, (Object) bankAccount.countryCode) && Intrinsics.areEqual((Object) this.currency, (Object) bankAccount.currency) && Intrinsics.areEqual((Object) this.fingerprint, (Object) bankAccount.fingerprint) && Intrinsics.areEqual((Object) this.last4, (Object) bankAccount.last4) && Intrinsics.areEqual((Object) this.routingNumber, (Object) bankAccount.routingNumber) && Intrinsics.areEqual((Object) this.status, (Object) bankAccount.status);
    }

    public int hashCode() {
        String str = this.id;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.accountNumber;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.accountHolderName;
        int hashCode3 = (hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31;
        String str4 = this.accountHolderType;
        int hashCode4 = (hashCode3 + (str4 != null ? str4.hashCode() : 0)) * 31;
        String str5 = this.bankName;
        int hashCode5 = (hashCode4 + (str5 != null ? str5.hashCode() : 0)) * 31;
        String str6 = this.countryCode;
        int hashCode6 = (hashCode5 + (str6 != null ? str6.hashCode() : 0)) * 31;
        String str7 = this.currency;
        int hashCode7 = (hashCode6 + (str7 != null ? str7.hashCode() : 0)) * 31;
        String str8 = this.fingerprint;
        int hashCode8 = (hashCode7 + (str8 != null ? str8.hashCode() : 0)) * 31;
        String str9 = this.last4;
        int hashCode9 = (hashCode8 + (str9 != null ? str9.hashCode() : 0)) * 31;
        String str10 = this.routingNumber;
        int hashCode10 = (hashCode9 + (str10 != null ? str10.hashCode() : 0)) * 31;
        Status status2 = this.status;
        if (status2 != null) {
            i = status2.hashCode();
        }
        return hashCode10 + i;
    }

    public String toString() {
        return "BankAccount(id=" + this.id + ", accountNumber=" + this.accountNumber + ", accountHolderName=" + this.accountHolderName + ", accountHolderType=" + this.accountHolderType + ", bankName=" + this.bankName + ", countryCode=" + this.countryCode + ", currency=" + this.currency + ", fingerprint=" + this.fingerprint + ", last4=" + this.last4 + ", routingNumber=" + this.routingNumber + ", status=" + this.status + ")";
    }

    public void writeToParcel(Parcel parcel, int i) {
        Intrinsics.checkParameterIsNotNull(parcel, "parcel");
        parcel.writeString(this.id);
        parcel.writeString(this.accountNumber);
        parcel.writeString(this.accountHolderName);
        parcel.writeString(this.accountHolderType);
        parcel.writeString(this.bankName);
        parcel.writeString(this.countryCode);
        parcel.writeString(this.currency);
        parcel.writeString(this.fingerprint);
        parcel.writeString(this.last4);
        parcel.writeString(this.routingNumber);
        Status status2 = this.status;
        if (status2 != null) {
            parcel.writeInt(1);
            parcel.writeString(status2.name());
            return;
        }
        parcel.writeInt(0);
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* synthetic */ BankAccount(java.lang.String r13, java.lang.String r14, java.lang.String r15, java.lang.String r16, java.lang.String r17, java.lang.String r18, java.lang.String r19, java.lang.String r20, java.lang.String r21, java.lang.String r22, com.stripe.android.model.BankAccount.Status r23, int r24, kotlin.jvm.internal.DefaultConstructorMarker r25) {
        /*
            r12 = this;
            r0 = r24
            r1 = r0 & 1
            r2 = 0
            if (r1 == 0) goto L_0x000b
            r1 = r2
            java.lang.String r1 = (java.lang.String) r1
            goto L_0x000c
        L_0x000b:
            r1 = r13
        L_0x000c:
            r3 = r0 & 2
            if (r3 == 0) goto L_0x0014
            r3 = r2
            java.lang.String r3 = (java.lang.String) r3
            goto L_0x0015
        L_0x0014:
            r3 = r14
        L_0x0015:
            r4 = r0 & 4
            if (r4 == 0) goto L_0x001d
            r4 = r2
            java.lang.String r4 = (java.lang.String) r4
            goto L_0x001e
        L_0x001d:
            r4 = r15
        L_0x001e:
            r5 = r0 & 8
            if (r5 == 0) goto L_0x0026
            r5 = r2
            java.lang.String r5 = (java.lang.String) r5
            goto L_0x0028
        L_0x0026:
            r5 = r16
        L_0x0028:
            r6 = r0 & 16
            if (r6 == 0) goto L_0x0030
            r6 = r2
            java.lang.String r6 = (java.lang.String) r6
            goto L_0x0032
        L_0x0030:
            r6 = r17
        L_0x0032:
            r7 = r0 & 32
            if (r7 == 0) goto L_0x003a
            r7 = r2
            java.lang.String r7 = (java.lang.String) r7
            goto L_0x003c
        L_0x003a:
            r7 = r18
        L_0x003c:
            r8 = r0 & 64
            if (r8 == 0) goto L_0x0044
            r8 = r2
            java.lang.String r8 = (java.lang.String) r8
            goto L_0x0046
        L_0x0044:
            r8 = r19
        L_0x0046:
            r9 = r0 & 128(0x80, float:1.794E-43)
            if (r9 == 0) goto L_0x004e
            r9 = r2
            java.lang.String r9 = (java.lang.String) r9
            goto L_0x0050
        L_0x004e:
            r9 = r20
        L_0x0050:
            r10 = r0 & 256(0x100, float:3.59E-43)
            if (r10 == 0) goto L_0x0058
            r10 = r2
            java.lang.String r10 = (java.lang.String) r10
            goto L_0x005a
        L_0x0058:
            r10 = r21
        L_0x005a:
            r11 = r0 & 512(0x200, float:7.175E-43)
            if (r11 == 0) goto L_0x0062
            r11 = r2
            java.lang.String r11 = (java.lang.String) r11
            goto L_0x0064
        L_0x0062:
            r11 = r22
        L_0x0064:
            r0 = r0 & 1024(0x400, float:1.435E-42)
            if (r0 == 0) goto L_0x006c
            r0 = r2
            com.stripe.android.model.BankAccount$Status r0 = (com.stripe.android.model.BankAccount.Status) r0
            goto L_0x006e
        L_0x006c:
            r0 = r23
        L_0x006e:
            r13 = r12
            r14 = r1
            r15 = r3
            r16 = r4
            r17 = r5
            r18 = r6
            r19 = r7
            r20 = r8
            r21 = r9
            r22 = r10
            r23 = r11
            r24 = r0
            r13.<init>(r14, r15, r16, r17, r18, r19, r20, r21, r22, r23, r24)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.stripe.android.model.BankAccount.<init>(java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, com.stripe.android.model.BankAccount$Status, int, kotlin.jvm.internal.DefaultConstructorMarker):void");
    }

    public final String getId() {
        return this.id;
    }

    public final String getAccountNumber() {
        return this.accountNumber;
    }

    public final String getAccountHolderName() {
        return this.accountHolderName;
    }

    public final String getAccountHolderType() {
        return this.accountHolderType;
    }

    public final String getBankName() {
        return this.bankName;
    }

    public final String getCountryCode() {
        return this.countryCode;
    }

    public final String getCurrency() {
        return this.currency;
    }

    public final String getFingerprint() {
        return this.fingerprint;
    }

    public final String getLast4() {
        return this.last4;
    }

    public final String getRoutingNumber() {
        return this.routingNumber;
    }

    public final Status getStatus() {
        return this.status;
    }

    public BankAccount(String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, String str9, String str10, Status status2) {
        super("bank_account", (Set) null, 2, (DefaultConstructorMarker) null);
        this.id = str;
        this.accountNumber = str2;
        this.accountHolderName = str3;
        this.accountHolderType = str4;
        this.bankName = str5;
        this.countryCode = str6;
        this.currency = str7;
        this.fingerprint = str8;
        this.last4 = str9;
        this.routingNumber = str10;
        this.status = status2;
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u001b\n\u0002\b\u0002\b\u0002\u0018\u0000 \u00022\u00020\u0001:\u0001\u0002B\u0000¨\u0006\u0003"}, d2 = {"Lcom/stripe/android/model/BankAccount$BankAccountType;", "", "Companion", "stripe_release"}, k = 1, mv = {1, 1, 16})
    @Retention(AnnotationRetention.SOURCE)
    @java.lang.annotation.Retention(RetentionPolicy.SOURCE)
    /* compiled from: BankAccount.kt */
    public @interface BankAccountType {
        public static final String COMPANY = "company";
        public static final Companion Companion = Companion.$$INSTANCE;
        public static final String INDIVIDUAL = "individual";

        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\u0006"}, d2 = {"Lcom/stripe/android/model/BankAccount$BankAccountType$Companion;", "", "()V", "COMPANY", "", "INDIVIDUAL", "stripe_release"}, k = 1, mv = {1, 1, 16})
        /* compiled from: BankAccount.kt */
        public static final class Companion {
            static final /* synthetic */ Companion $$INSTANCE = new Companion();
            public static final String COMPANY = "company";
            public static final String INDIVIDUAL = "individual";

            private Companion() {
            }
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\u000e\n\u0002\b\n\b\u0001\u0018\u0000 \f2\b\u0012\u0004\u0012\u00020\u00000\u0001:\u0001\fB\u000f\b\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004R\u0014\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006j\u0002\b\u0007j\u0002\b\bj\u0002\b\tj\u0002\b\nj\u0002\b\u000b¨\u0006\r"}, d2 = {"Lcom/stripe/android/model/BankAccount$Status;", "", "code", "", "(Ljava/lang/String;ILjava/lang/String;)V", "getCode$stripe_release", "()Ljava/lang/String;", "New", "Validated", "Verified", "VerificationFailed", "Errored", "Companion", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: BankAccount.kt */
    public enum Status {
        New("new"),
        Validated("validated"),
        Verified("verified"),
        VerificationFailed("verification_failed"),
        Errored("errored");
        
        public static final Companion Companion = null;
        private final String code;

        private Status(String str) {
            this.code = str;
        }

        public final String getCode$stripe_release() {
            return this.code;
        }

        static {
            Companion = new Companion((DefaultConstructorMarker) null);
        }

        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0019\u0010\u0003\u001a\u0004\u0018\u00010\u00042\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006H\u0000¢\u0006\u0002\b\u0007¨\u0006\b"}, d2 = {"Lcom/stripe/android/model/BankAccount$Status$Companion;", "", "()V", "fromCode", "Lcom/stripe/android/model/BankAccount$Status;", "code", "", "fromCode$stripe_release", "stripe_release"}, k = 1, mv = {1, 1, 16})
        /* compiled from: BankAccount.kt */
        public static final class Companion {
            private Companion() {
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }

            public final Status fromCode$stripe_release(String str) {
                for (Status status : Status.values()) {
                    if (Intrinsics.areEqual((Object) status.getCode$stripe_release(), (Object) str)) {
                        return status;
                    }
                }
                return null;
            }
        }
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ BankAccount(String str, String str2, String str3, String str4, String str5, String str6, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(str, str2, str3, (i & 8) != 0 ? null : str4, (i & 16) != 0 ? null : str5, (i & 32) != 0 ? null : str6);
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    @kotlin.Deprecated(message = "Use BankAccountTokenParams")
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public BankAccount(java.lang.String r16, java.lang.String r17, java.lang.String r18, java.lang.String r19, java.lang.String r20, java.lang.String r21) {
        /*
            r15 = this;
            java.lang.String r0 = "accountNumber"
            r3 = r16
            kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(r3, r0)
            java.lang.String r0 = "countryCode"
            r7 = r17
            kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(r7, r0)
            java.lang.String r0 = "currency"
            r8 = r18
            kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(r8, r0)
            r2 = 0
            r6 = 0
            r10 = 0
            r9 = 0
            r12 = 0
            r13 = 1297(0x511, float:1.817E-42)
            r14 = 0
            r1 = r15
            r4 = r20
            r5 = r21
            r11 = r19
            r1.<init>(r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.stripe.android.model.BankAccount.<init>(java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String):void");
    }

    @Deprecated(message = "For internal use only")
    public BankAccount(String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8) {
        this((String) null, (String) null, str, str2, str3, str4, str5, str6, str7, str8, (Status) null, 1026, (DefaultConstructorMarker) null);
    }

    public Map<String, Object> toParamMap() {
        String str;
        String str2;
        String str3 = this.countryCode;
        String str4 = str3 != null ? str3 : "";
        String str5 = this.currency;
        if (str5 != null) {
            str = str5;
        } else {
            str = "";
        }
        String str6 = this.accountNumber;
        if (str6 != null) {
            str2 = str6;
        } else {
            str2 = "";
        }
        return new BankAccountTokenParams(str4, str, str2, BankAccountTokenParams.Type.Companion.fromCode$stripe_release(this.accountHolderType), this.accountHolderName, this.routingNumber).toParamMap();
    }
}
