package com.stripe.android.model;

import com.stripe.android.model.StripeIntent;
import java.io.ByteArrayInputStream;
import java.nio.charset.Charset;
import java.security.PublicKey;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.collections.CollectionsKt;
import kotlin.jvm.internal.Intrinsics;
import kotlin.text.Charsets;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u000b\b\u0000\u0018\u00002\u00020\u0001:\u0002\u0014\u0015B\u000f\b\u0010\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004B'\b\u0002\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\b\u0012\u0006\u0010\t\u001a\u00020\u0006\u0012\u0006\u0010\n\u001a\u00020\u000b¢\u0006\u0002\u0010\fR\u0011\u0010\u0007\u001a\u00020\b¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000eR\u0011\u0010\n\u001a\u00020\u000b¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010R\u0011\u0010\t\u001a\u00020\u0006¢\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u0012R\u0011\u0010\u0005\u001a\u00020\u0006¢\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u0012¨\u0006\u0016"}, d2 = {"Lcom/stripe/android/model/Stripe3ds2Fingerprint;", "", "sdkData", "Lcom/stripe/android/model/StripeIntent$NextActionData$SdkData$Use3DS2;", "(Lcom/stripe/android/model/StripeIntent$NextActionData$SdkData$Use3DS2;)V", "source", "", "directoryServer", "Lcom/stripe/android/model/Stripe3ds2Fingerprint$DirectoryServer;", "serverTransactionId", "directoryServerEncryption", "Lcom/stripe/android/model/Stripe3ds2Fingerprint$DirectoryServerEncryption;", "(Ljava/lang/String;Lcom/stripe/android/model/Stripe3ds2Fingerprint$DirectoryServer;Ljava/lang/String;Lcom/stripe/android/model/Stripe3ds2Fingerprint$DirectoryServerEncryption;)V", "getDirectoryServer", "()Lcom/stripe/android/model/Stripe3ds2Fingerprint$DirectoryServer;", "getDirectoryServerEncryption", "()Lcom/stripe/android/model/Stripe3ds2Fingerprint$DirectoryServerEncryption;", "getServerTransactionId", "()Ljava/lang/String;", "getSource", "DirectoryServer", "DirectoryServerEncryption", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: Stripe3ds2Fingerprint.kt */
public final class Stripe3ds2Fingerprint {
    private final DirectoryServer directoryServer;
    private final DirectoryServerEncryption directoryServerEncryption;
    private final String serverTransactionId;
    private final String source;

    private Stripe3ds2Fingerprint(String str, DirectoryServer directoryServer2, String str2, DirectoryServerEncryption directoryServerEncryption2) {
        this.source = str;
        this.directoryServer = directoryServer2;
        this.serverTransactionId = str2;
        this.directoryServerEncryption = directoryServerEncryption2;
    }

    public final String getSource() {
        return this.source;
    }

    public final DirectoryServer getDirectoryServer() {
        return this.directoryServer;
    }

    public final String getServerTransactionId() {
        return this.serverTransactionId;
    }

    public final DirectoryServerEncryption getDirectoryServerEncryption() {
        return this.directoryServerEncryption;
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public Stripe3ds2Fingerprint(StripeIntent.NextActionData.SdkData.Use3DS2 use3DS2) throws CertificateException {
        this(use3DS2.getSource(), DirectoryServer.Companion.lookup$stripe_release(use3DS2.getServerName()), use3DS2.getTransactionId(), new DirectoryServerEncryption(use3DS2.getServerEncryption().getDirectoryServerId(), use3DS2.getServerEncryption().getDsCertificateData(), use3DS2.getServerEncryption().getRootCertsData(), use3DS2.getServerEncryption().getKeyId()));
        Intrinsics.checkParameterIsNotNull(use3DS2, "sdkData");
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\b\u0018\u0000 \u00182\u00020\u0001:\u0001\u0018B/\b\u0001\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\f\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00030\u0006\u0012\b\u0010\u0007\u001a\u0004\u0018\u00010\u0003¢\u0006\u0002\u0010\bJ\u0010\u0010\u0014\u001a\u00020\u00112\u0006\u0010\u0015\u001a\u00020\u0003H\u0002J\u001c\u0010\u0016\u001a\b\u0012\u0004\u0012\u00020\u00110\u00062\f\u0010\u0017\u001a\b\u0012\u0004\u0012\u00020\u00030\u0006H\u0002R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0011\u0010\u000b\u001a\u00020\f¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000eR\u0013\u0010\u0007\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\nR\u0017\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u00110\u0006¢\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u0013¨\u0006\u0019"}, d2 = {"Lcom/stripe/android/model/Stripe3ds2Fingerprint$DirectoryServerEncryption;", "", "directoryServerId", "", "dsCertificateData", "rootCertsData", "", "keyId", "(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)V", "getDirectoryServerId", "()Ljava/lang/String;", "directoryServerPublicKey", "Ljava/security/PublicKey;", "getDirectoryServerPublicKey", "()Ljava/security/PublicKey;", "getKeyId", "rootCerts", "Ljava/security/cert/X509Certificate;", "getRootCerts", "()Ljava/util/List;", "generateCertificate", "certificateData", "generateCertificates", "certificatesData", "Companion", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: Stripe3ds2Fingerprint.kt */
    public static final class DirectoryServerEncryption {
        public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
        private static final String FIELD_CERTIFICATE = "certificate";
        private static final String FIELD_DIRECTORY_SERVER_ID = "directory_server_id";
        private static final String FIELD_KEY_ID = "key_id";
        private static final String FIELD_ROOT_CAS = "root_certificate_authorities";
        private final String directoryServerId;
        private final PublicKey directoryServerPublicKey;
        private final String keyId;
        private final List<X509Certificate> rootCerts;

        public DirectoryServerEncryption(String str, String str2, List<String> list, String str3) throws CertificateException {
            Intrinsics.checkParameterIsNotNull(str, "directoryServerId");
            Intrinsics.checkParameterIsNotNull(str2, "dsCertificateData");
            Intrinsics.checkParameterIsNotNull(list, "rootCertsData");
            this.directoryServerId = str;
            this.keyId = str3;
            PublicKey publicKey = generateCertificate(str2).getPublicKey();
            Intrinsics.checkExpressionValueIsNotNull(publicKey, "generateCertificate(dsCertificateData).publicKey");
            this.directoryServerPublicKey = publicKey;
            this.rootCerts = generateCertificates(list);
        }

        public final String getDirectoryServerId() {
            return this.directoryServerId;
        }

        public final String getKeyId() {
            return this.keyId;
        }

        public final PublicKey getDirectoryServerPublicKey() {
            return this.directoryServerPublicKey;
        }

        public final List<X509Certificate> getRootCerts() {
            return this.rootCerts;
        }

        private final List<X509Certificate> generateCertificates(List<String> list) throws CertificateException {
            Iterable<String> iterable = list;
            Collection arrayList = new ArrayList(CollectionsKt.collectionSizeOrDefault(iterable, 10));
            for (String generateCertificate : iterable) {
                arrayList.add(generateCertificate(generateCertificate));
            }
            return (List) arrayList;
        }

        private final X509Certificate generateCertificate(String str) throws CertificateException {
            CertificateFactory instance = CertificateFactory.getInstance("X.509");
            Charset charset = Charsets.UTF_8;
            if (str != null) {
                byte[] bytes = str.getBytes(charset);
                Intrinsics.checkExpressionValueIsNotNull(bytes, "(this as java.lang.String).getBytes(charset)");
                Certificate generateCertificate = instance.generateCertificate(new ByteArrayInputStream(bytes));
                if (generateCertificate != null) {
                    return (X509Certificate) generateCertificate;
                }
                throw new TypeCastException("null cannot be cast to non-null type java.security.cert.X509Certificate");
            }
            throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
        }

        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\b\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u001f\u0010\b\u001a\u00020\t2\u0010\u0010\n\u001a\f\u0012\u0004\u0012\u00020\u0004\u0012\u0002\b\u00030\u000bH\u0000¢\u0006\u0002\b\fR\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\r"}, d2 = {"Lcom/stripe/android/model/Stripe3ds2Fingerprint$DirectoryServerEncryption$Companion;", "", "()V", "FIELD_CERTIFICATE", "", "FIELD_DIRECTORY_SERVER_ID", "FIELD_KEY_ID", "FIELD_ROOT_CAS", "create", "Lcom/stripe/android/model/Stripe3ds2Fingerprint$DirectoryServerEncryption;", "data", "", "create$stripe_release", "stripe_release"}, k = 1, mv = {1, 1, 16})
        /* compiled from: Stripe3ds2Fingerprint.kt */
        public static final class Companion {
            private Companion() {
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }

            public final /* synthetic */ DirectoryServerEncryption create$stripe_release(Map<String, ?> map) throws CertificateException {
                List list;
                Intrinsics.checkParameterIsNotNull(map, "data");
                if (map.containsKey(DirectoryServerEncryption.FIELD_ROOT_CAS)) {
                    Object obj = map.get(DirectoryServerEncryption.FIELD_ROOT_CAS);
                    if (obj != null) {
                        list = (List) obj;
                    } else {
                        throw new TypeCastException("null cannot be cast to non-null type kotlin.collections.List<kotlin.String>");
                    }
                } else {
                    list = CollectionsKt.emptyList();
                }
                Object obj2 = map.get(DirectoryServerEncryption.FIELD_DIRECTORY_SERVER_ID);
                if (obj2 != null) {
                    String str = (String) obj2;
                    Object obj3 = map.get(DirectoryServerEncryption.FIELD_CERTIFICATE);
                    if (obj3 != null) {
                        return new DirectoryServerEncryption(str, (String) obj3, list, (String) map.get("key_id"));
                    }
                    throw new TypeCastException("null cannot be cast to non-null type kotlin.String");
                }
                throw new TypeCastException("null cannot be cast to non-null type kotlin.String");
            }
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\u000e\n\u0002\b\n\b\u0001\u0018\u0000 \f2\b\u0012\u0004\u0012\u00020\u00000\u0001:\u0001\fB\u0017\b\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003¢\u0006\u0002\u0010\u0005R\u0011\u0010\u0004\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\u0007R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\u0007j\u0002\b\tj\u0002\b\nj\u0002\b\u000b¨\u0006\r"}, d2 = {"Lcom/stripe/android/model/Stripe3ds2Fingerprint$DirectoryServer;", "", "networkName", "", "id", "(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V", "getId", "()Ljava/lang/String;", "getNetworkName", "Visa", "Mastercard", "Amex", "Companion", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: Stripe3ds2Fingerprint.kt */
    public enum DirectoryServer {
        Visa("visa", "A000000003"),
        Mastercard("mastercard", "A000000004"),
        Amex("american_express", "A000000025");
        
        public static final Companion Companion = null;
        private final String id;
        private final String networkName;

        private DirectoryServer(String str, String str2) {
            this.networkName = str;
            this.id = str2;
        }

        public final String getId() {
            return this.id;
        }

        public final String getNetworkName() {
            return this.networkName;
        }

        static {
            Companion = new Companion((DefaultConstructorMarker) null);
        }

        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0015\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0000¢\u0006\u0002\b\u0007¨\u0006\b"}, d2 = {"Lcom/stripe/android/model/Stripe3ds2Fingerprint$DirectoryServer$Companion;", "", "()V", "lookup", "Lcom/stripe/android/model/Stripe3ds2Fingerprint$DirectoryServer;", "networkName", "", "lookup$stripe_release", "stripe_release"}, k = 1, mv = {1, 1, 16})
        /* compiled from: Stripe3ds2Fingerprint.kt */
        public static final class Companion {
            private Companion() {
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }

            public final /* synthetic */ DirectoryServer lookup$stripe_release(String str) {
                DirectoryServer directoryServer;
                Intrinsics.checkParameterIsNotNull(str, "networkName");
                DirectoryServer[] values = DirectoryServer.values();
                int length = values.length;
                int i = 0;
                while (true) {
                    if (i >= length) {
                        directoryServer = null;
                        break;
                    }
                    directoryServer = values[i];
                    if (Intrinsics.areEqual((Object) directoryServer.getNetworkName(), (Object) str)) {
                        break;
                    }
                    i++;
                }
                if (directoryServer != null) {
                    return directoryServer;
                }
                throw new IllegalStateException(("Invalid directory server networkName: '" + str + '\'').toString());
            }
        }
    }
}
