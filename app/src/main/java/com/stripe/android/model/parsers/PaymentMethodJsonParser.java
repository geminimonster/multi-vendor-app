package com.stripe.android.model.parsers;

import com.stripe.android.model.PaymentMethod;
import com.stripe.android.model.StripeJsonUtils;
import com.stripe.android.model.wallets.Wallet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt;
import kotlin.jvm.internal.Intrinsics;
import org.json.JSONObject;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\n\b\u0000\u0018\u0000 \u000b2\b\u0012\u0004\u0012\u00020\u00020\u0001:\t\u0007\b\t\n\u000b\f\r\u000e\u000fB\u0005¢\u0006\u0002\u0010\u0003J\u0010\u0010\u0004\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0006H\u0016¨\u0006\u0010"}, d2 = {"Lcom/stripe/android/model/parsers/PaymentMethodJsonParser;", "Lcom/stripe/android/model/parsers/ModelJsonParser;", "Lcom/stripe/android/model/PaymentMethod;", "()V", "parse", "json", "Lorg/json/JSONObject;", "AuBecsDebitJsonParser", "BacsDebitJsonParser", "BillingDetails", "CardJsonParser", "Companion", "FpxJsonParser", "IdealJsonParser", "SepaDebitJsonParser", "SofortJsonParser", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: PaymentMethodJsonParser.kt */
public final class PaymentMethodJsonParser implements ModelJsonParser<PaymentMethod> {
    @Deprecated
    public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
    private static final String FIELD_BILLING_DETAILS = "billing_details";
    private static final String FIELD_CREATED = "created";
    private static final String FIELD_CUSTOMER = "customer";
    private static final String FIELD_ID = "id";
    private static final String FIELD_LIVEMODE = "livemode";
    private static final String FIELD_METADATA = "metadata";
    private static final String FIELD_TYPE = "type";

    @Metadata(bv = {1, 0, 3}, k = 3, mv = {1, 1, 16})
    public final /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;

        static {
            int[] iArr = new int[PaymentMethod.Type.values().length];
            $EnumSwitchMapping$0 = iArr;
            iArr[PaymentMethod.Type.Card.ordinal()] = 1;
            $EnumSwitchMapping$0[PaymentMethod.Type.CardPresent.ordinal()] = 2;
            $EnumSwitchMapping$0[PaymentMethod.Type.Ideal.ordinal()] = 3;
            $EnumSwitchMapping$0[PaymentMethod.Type.Fpx.ordinal()] = 4;
            $EnumSwitchMapping$0[PaymentMethod.Type.SepaDebit.ordinal()] = 5;
            $EnumSwitchMapping$0[PaymentMethod.Type.AuBecsDebit.ordinal()] = 6;
            $EnumSwitchMapping$0[PaymentMethod.Type.BacsDebit.ordinal()] = 7;
            $EnumSwitchMapping$0[PaymentMethod.Type.Sofort.ordinal()] = 8;
        }
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v2, resolved type: com.stripe.android.model.PaymentMethod$Card} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v4, resolved type: com.stripe.android.model.PaymentMethod$Ideal} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v6, resolved type: com.stripe.android.model.PaymentMethod$Fpx} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v8, resolved type: com.stripe.android.model.PaymentMethod$SepaDebit} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v10, resolved type: com.stripe.android.model.PaymentMethod$AuBecsDebit} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v12, resolved type: com.stripe.android.model.PaymentMethod$BacsDebit} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v14, resolved type: com.stripe.android.model.PaymentMethod$Sofort} */
    /* JADX WARNING: type inference failed for: r3v1 */
    /* JADX WARNING: type inference failed for: r3v16 */
    /* JADX WARNING: type inference failed for: r3v17 */
    /* JADX WARNING: type inference failed for: r3v18 */
    /* JADX WARNING: type inference failed for: r3v19 */
    /* JADX WARNING: type inference failed for: r3v20 */
    /* JADX WARNING: type inference failed for: r3v21 */
    /* JADX WARNING: type inference failed for: r3v22 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.stripe.android.model.PaymentMethod parse(org.json.JSONObject r6) {
        /*
            r5 = this;
            java.lang.String r0 = "json"
            kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(r6, r0)
            com.stripe.android.model.PaymentMethod$Type$Companion r0 = com.stripe.android.model.PaymentMethod.Type.Companion
            java.lang.String r1 = "type"
            java.lang.String r1 = com.stripe.android.model.StripeJsonUtils.optString(r6, r1)
            com.stripe.android.model.PaymentMethod$Type r0 = r0.fromCode$stripe_release(r1)
            com.stripe.android.model.PaymentMethod$Builder r1 = new com.stripe.android.model.PaymentMethod$Builder
            r1.<init>()
            java.lang.String r2 = "id"
            java.lang.String r2 = com.stripe.android.model.StripeJsonUtils.optString(r6, r2)
            com.stripe.android.model.PaymentMethod$Builder r1 = r1.setId(r2)
            com.stripe.android.model.PaymentMethod$Builder r1 = r1.setType(r0)
            com.stripe.android.model.StripeJsonUtils r2 = com.stripe.android.model.StripeJsonUtils.INSTANCE
            java.lang.String r3 = "created"
            java.lang.Long r2 = r2.optLong$stripe_release(r6, r3)
            com.stripe.android.model.PaymentMethod$Builder r1 = r1.setCreated(r2)
            java.lang.String r2 = "billing_details"
            org.json.JSONObject r2 = r6.optJSONObject(r2)
            r3 = 0
            if (r2 == 0) goto L_0x0043
            com.stripe.android.model.parsers.PaymentMethodJsonParser$BillingDetails r4 = new com.stripe.android.model.parsers.PaymentMethodJsonParser$BillingDetails
            r4.<init>()
            com.stripe.android.model.PaymentMethod$BillingDetails r2 = r4.parse((org.json.JSONObject) r2)
            goto L_0x0044
        L_0x0043:
            r2 = r3
        L_0x0044:
            com.stripe.android.model.PaymentMethod$Builder r1 = r1.setBillingDetails(r2)
            java.lang.String r2 = "customer"
            java.lang.String r2 = com.stripe.android.model.StripeJsonUtils.optString(r6, r2)
            com.stripe.android.model.PaymentMethod$Builder r1 = r1.setCustomerId(r2)
            java.lang.String r2 = "livemode"
            boolean r2 = r6.optBoolean(r2)
            com.stripe.android.model.PaymentMethod$Builder r1 = r1.setLiveMode(r2)
            com.stripe.android.model.StripeJsonUtils r2 = com.stripe.android.model.StripeJsonUtils.INSTANCE
            java.lang.String r4 = "metadata"
            java.util.Map r2 = r2.optHash$stripe_release(r6, r4)
            com.stripe.android.model.PaymentMethod$Builder r1 = r1.setMetadata(r2)
            if (r0 != 0) goto L_0x006c
            goto L_0x0117
        L_0x006c:
            int[] r2 = com.stripe.android.model.parsers.PaymentMethodJsonParser.WhenMappings.$EnumSwitchMapping$0
            int r4 = r0.ordinal()
            r2 = r2[r4]
            switch(r2) {
                case 1: goto L_0x0103;
                case 2: goto L_0x00f9;
                case 3: goto L_0x00e4;
                case 4: goto L_0x00cf;
                case 5: goto L_0x00ba;
                case 6: goto L_0x00a5;
                case 7: goto L_0x008f;
                case 8: goto L_0x0079;
                default: goto L_0x0077;
            }
        L_0x0077:
            goto L_0x0117
        L_0x0079:
            java.lang.String r0 = r0.code
            org.json.JSONObject r6 = r6.optJSONObject(r0)
            if (r6 == 0) goto L_0x008a
            com.stripe.android.model.parsers.PaymentMethodJsonParser$SofortJsonParser r0 = new com.stripe.android.model.parsers.PaymentMethodJsonParser$SofortJsonParser
            r0.<init>()
            com.stripe.android.model.PaymentMethod$Sofort r3 = r0.parse((org.json.JSONObject) r6)
        L_0x008a:
            r1.setSofort(r3)
            goto L_0x0117
        L_0x008f:
            java.lang.String r0 = r0.code
            org.json.JSONObject r6 = r6.optJSONObject(r0)
            if (r6 == 0) goto L_0x00a0
            com.stripe.android.model.parsers.PaymentMethodJsonParser$BacsDebitJsonParser r0 = new com.stripe.android.model.parsers.PaymentMethodJsonParser$BacsDebitJsonParser
            r0.<init>()
            com.stripe.android.model.PaymentMethod$BacsDebit r3 = r0.parse((org.json.JSONObject) r6)
        L_0x00a0:
            r1.setBacsDebit(r3)
            goto L_0x0117
        L_0x00a5:
            java.lang.String r0 = r0.code
            org.json.JSONObject r6 = r6.optJSONObject(r0)
            if (r6 == 0) goto L_0x00b6
            com.stripe.android.model.parsers.PaymentMethodJsonParser$AuBecsDebitJsonParser r0 = new com.stripe.android.model.parsers.PaymentMethodJsonParser$AuBecsDebitJsonParser
            r0.<init>()
            com.stripe.android.model.PaymentMethod$AuBecsDebit r3 = r0.parse((org.json.JSONObject) r6)
        L_0x00b6:
            r1.setAuBecsDebit(r3)
            goto L_0x0117
        L_0x00ba:
            java.lang.String r0 = r0.code
            org.json.JSONObject r6 = r6.optJSONObject(r0)
            if (r6 == 0) goto L_0x00cb
            com.stripe.android.model.parsers.PaymentMethodJsonParser$SepaDebitJsonParser r0 = new com.stripe.android.model.parsers.PaymentMethodJsonParser$SepaDebitJsonParser
            r0.<init>()
            com.stripe.android.model.PaymentMethod$SepaDebit r3 = r0.parse((org.json.JSONObject) r6)
        L_0x00cb:
            r1.setSepaDebit(r3)
            goto L_0x0117
        L_0x00cf:
            java.lang.String r0 = r0.code
            org.json.JSONObject r6 = r6.optJSONObject(r0)
            if (r6 == 0) goto L_0x00e0
            com.stripe.android.model.parsers.PaymentMethodJsonParser$FpxJsonParser r0 = new com.stripe.android.model.parsers.PaymentMethodJsonParser$FpxJsonParser
            r0.<init>()
            com.stripe.android.model.PaymentMethod$Fpx r3 = r0.parse((org.json.JSONObject) r6)
        L_0x00e0:
            r1.setFpx(r3)
            goto L_0x0117
        L_0x00e4:
            java.lang.String r0 = r0.code
            org.json.JSONObject r6 = r6.optJSONObject(r0)
            if (r6 == 0) goto L_0x00f5
            com.stripe.android.model.parsers.PaymentMethodJsonParser$IdealJsonParser r0 = new com.stripe.android.model.parsers.PaymentMethodJsonParser$IdealJsonParser
            r0.<init>()
            com.stripe.android.model.PaymentMethod$Ideal r3 = r0.parse((org.json.JSONObject) r6)
        L_0x00f5:
            r1.setIdeal(r3)
            goto L_0x0117
        L_0x00f9:
            com.stripe.android.model.PaymentMethod$CardPresent$Companion r6 = com.stripe.android.model.PaymentMethod.CardPresent.Companion
            com.stripe.android.model.PaymentMethod$CardPresent r6 = r6.getEMPTY$stripe_release()
            r1.setCardPresent(r6)
            goto L_0x0117
        L_0x0103:
            java.lang.String r0 = r0.code
            org.json.JSONObject r6 = r6.optJSONObject(r0)
            if (r6 == 0) goto L_0x0114
            com.stripe.android.model.parsers.PaymentMethodJsonParser$CardJsonParser r0 = new com.stripe.android.model.parsers.PaymentMethodJsonParser$CardJsonParser
            r0.<init>()
            com.stripe.android.model.PaymentMethod$Card r3 = r0.parse((org.json.JSONObject) r6)
        L_0x0114:
            r1.setCard(r3)
        L_0x0117:
            com.stripe.android.model.PaymentMethod r6 = r1.build()
            return r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.stripe.android.model.parsers.PaymentMethodJsonParser.parse(org.json.JSONObject):com.stripe.android.model.PaymentMethod");
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0000\u0018\u0000 \u00072\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0007B\u0005¢\u0006\u0002\u0010\u0003J\u0010\u0010\u0004\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0006H\u0016¨\u0006\b"}, d2 = {"Lcom/stripe/android/model/parsers/PaymentMethodJsonParser$BillingDetails;", "Lcom/stripe/android/model/parsers/ModelJsonParser;", "Lcom/stripe/android/model/PaymentMethod$BillingDetails;", "()V", "parse", "json", "Lorg/json/JSONObject;", "Companion", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: PaymentMethodJsonParser.kt */
    public static final class BillingDetails implements ModelJsonParser<PaymentMethod.BillingDetails> {
        @Deprecated
        public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
        private static final String FIELD_ADDRESS = "address";
        private static final String FIELD_EMAIL = "email";
        private static final String FIELD_NAME = "name";
        private static final String FIELD_PHONE = "phone";

        public PaymentMethod.BillingDetails parse(JSONObject jSONObject) {
            Intrinsics.checkParameterIsNotNull(jSONObject, "json");
            JSONObject optJSONObject = jSONObject.optJSONObject("address");
            return new PaymentMethod.BillingDetails(optJSONObject != null ? new AddressJsonParser().parse(optJSONObject) : null, StripeJsonUtils.optString(jSONObject, "email"), StripeJsonUtils.optString(jSONObject, "name"), StripeJsonUtils.optString(jSONObject, "phone"));
        }

        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0004\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\b"}, d2 = {"Lcom/stripe/android/model/parsers/PaymentMethodJsonParser$BillingDetails$Companion;", "", "()V", "FIELD_ADDRESS", "", "FIELD_EMAIL", "FIELD_NAME", "FIELD_PHONE", "stripe_release"}, k = 1, mv = {1, 1, 16})
        /* compiled from: PaymentMethodJsonParser.kt */
        private static final class Companion {
            private Companion() {
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0000\u0018\u0000 \b2\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0004\u0007\b\t\nB\u0005¢\u0006\u0002\u0010\u0003J\u0010\u0010\u0004\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0006H\u0016¨\u0006\u000b"}, d2 = {"Lcom/stripe/android/model/parsers/PaymentMethodJsonParser$CardJsonParser;", "Lcom/stripe/android/model/parsers/ModelJsonParser;", "Lcom/stripe/android/model/PaymentMethod$Card;", "()V", "parse", "json", "Lorg/json/JSONObject;", "ChecksJsonParser", "Companion", "NetworksJsonParser", "ThreeDSecureUsageJsonParser", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: PaymentMethodJsonParser.kt */
    public static final class CardJsonParser implements ModelJsonParser<PaymentMethod.Card> {
        @Deprecated
        public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
        private static final String FIELD_BRAND = "brand";
        private static final String FIELD_CHECKS = "checks";
        private static final String FIELD_COUNTRY = "country";
        private static final String FIELD_EXP_MONTH = "exp_month";
        private static final String FIELD_EXP_YEAR = "exp_year";
        private static final String FIELD_FUNDING = "funding";
        private static final String FIELD_LAST4 = "last4";
        private static final String FIELD_NETWORKS = "networks";
        private static final String FIELD_THREE_D_SECURE_USAGE = "three_d_secure_usage";
        private static final String FIELD_WALLET = "wallet";

        public PaymentMethod.Card parse(JSONObject jSONObject) {
            Intrinsics.checkParameterIsNotNull(jSONObject, "json");
            String optString = StripeJsonUtils.optString(jSONObject, FIELD_BRAND);
            JSONObject optJSONObject = jSONObject.optJSONObject(FIELD_CHECKS);
            PaymentMethod.Card.Checks parse = optJSONObject != null ? new ChecksJsonParser().parse(optJSONObject) : null;
            String optString2 = StripeJsonUtils.optString(jSONObject, "country");
            Integer optInteger$stripe_release = StripeJsonUtils.INSTANCE.optInteger$stripe_release(jSONObject, FIELD_EXP_MONTH);
            Integer optInteger$stripe_release2 = StripeJsonUtils.INSTANCE.optInteger$stripe_release(jSONObject, FIELD_EXP_YEAR);
            String optString3 = StripeJsonUtils.optString(jSONObject, FIELD_FUNDING);
            String optString4 = StripeJsonUtils.optString(jSONObject, FIELD_LAST4);
            JSONObject optJSONObject2 = jSONObject.optJSONObject(FIELD_THREE_D_SECURE_USAGE);
            PaymentMethod.Card.ThreeDSecureUsage parse2 = optJSONObject2 != null ? new ThreeDSecureUsageJsonParser().parse(optJSONObject2) : null;
            JSONObject optJSONObject3 = jSONObject.optJSONObject(FIELD_WALLET);
            Wallet parse3 = optJSONObject3 != null ? new WalletJsonParser().parse(optJSONObject3) : null;
            JSONObject optJSONObject4 = jSONObject.optJSONObject(FIELD_NETWORKS);
            return new PaymentMethod.Card(optString, parse, optString2, optInteger$stripe_release, optInteger$stripe_release2, optString3, optString4, parse2, parse3, optJSONObject4 != null ? new NetworksJsonParser().parse(optJSONObject4) : null);
        }

        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0000\u0018\u0000 \u00072\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0007B\u0005¢\u0006\u0002\u0010\u0003J\u0010\u0010\u0004\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0006H\u0016¨\u0006\b"}, d2 = {"Lcom/stripe/android/model/parsers/PaymentMethodJsonParser$CardJsonParser$ChecksJsonParser;", "Lcom/stripe/android/model/parsers/ModelJsonParser;", "Lcom/stripe/android/model/PaymentMethod$Card$Checks;", "()V", "parse", "json", "Lorg/json/JSONObject;", "Companion", "stripe_release"}, k = 1, mv = {1, 1, 16})
        /* compiled from: PaymentMethodJsonParser.kt */
        public static final class ChecksJsonParser implements ModelJsonParser<PaymentMethod.Card.Checks> {
            @Deprecated
            public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
            private static final String FIELD_ADDRESS_LINE1_CHECK = "address_line1_check";
            private static final String FIELD_ADDRESS_POSTAL_CODE_CHECK = "address_postal_code_check";
            private static final String FIELD_CVC_CHECK = "cvc_check";

            public PaymentMethod.Card.Checks parse(JSONObject jSONObject) {
                Intrinsics.checkParameterIsNotNull(jSONObject, "json");
                return new PaymentMethod.Card.Checks(StripeJsonUtils.optString(jSONObject, FIELD_ADDRESS_LINE1_CHECK), StripeJsonUtils.optString(jSONObject, FIELD_ADDRESS_POSTAL_CODE_CHECK), StripeJsonUtils.optString(jSONObject, FIELD_CVC_CHECK));
            }

            @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\u0007"}, d2 = {"Lcom/stripe/android/model/parsers/PaymentMethodJsonParser$CardJsonParser$ChecksJsonParser$Companion;", "", "()V", "FIELD_ADDRESS_LINE1_CHECK", "", "FIELD_ADDRESS_POSTAL_CODE_CHECK", "FIELD_CVC_CHECK", "stripe_release"}, k = 1, mv = {1, 1, 16})
            /* compiled from: PaymentMethodJsonParser.kt */
            private static final class Companion {
                private Companion() {
                }

                public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                    this();
                }
            }
        }

        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0000\u0018\u0000 \u00072\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0007B\u0005¢\u0006\u0002\u0010\u0003J\u0010\u0010\u0004\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0006H\u0016¨\u0006\b"}, d2 = {"Lcom/stripe/android/model/parsers/PaymentMethodJsonParser$CardJsonParser$ThreeDSecureUsageJsonParser;", "Lcom/stripe/android/model/parsers/ModelJsonParser;", "Lcom/stripe/android/model/PaymentMethod$Card$ThreeDSecureUsage;", "()V", "parse", "json", "Lorg/json/JSONObject;", "Companion", "stripe_release"}, k = 1, mv = {1, 1, 16})
        /* compiled from: PaymentMethodJsonParser.kt */
        public static final class ThreeDSecureUsageJsonParser implements ModelJsonParser<PaymentMethod.Card.ThreeDSecureUsage> {
            @Deprecated
            public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
            private static final String FIELD_IS_SUPPORTED = "supported";

            public PaymentMethod.Card.ThreeDSecureUsage parse(JSONObject jSONObject) {
                Intrinsics.checkParameterIsNotNull(jSONObject, "json");
                return new PaymentMethod.Card.ThreeDSecureUsage(StripeJsonUtils.INSTANCE.optBoolean$stripe_release(jSONObject, FIELD_IS_SUPPORTED));
            }

            @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\u0005"}, d2 = {"Lcom/stripe/android/model/parsers/PaymentMethodJsonParser$CardJsonParser$ThreeDSecureUsageJsonParser$Companion;", "", "()V", "FIELD_IS_SUPPORTED", "", "stripe_release"}, k = 1, mv = {1, 1, 16})
            /* compiled from: PaymentMethodJsonParser.kt */
            private static final class Companion {
                private Companion() {
                }

                public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                    this();
                }
            }
        }

        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0000\u0018\u0000 \u00072\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0007B\u0005¢\u0006\u0002\u0010\u0003J\u0012\u0010\u0004\u001a\u0004\u0018\u00010\u00022\u0006\u0010\u0005\u001a\u00020\u0006H\u0016¨\u0006\b"}, d2 = {"Lcom/stripe/android/model/parsers/PaymentMethodJsonParser$CardJsonParser$NetworksJsonParser;", "Lcom/stripe/android/model/parsers/ModelJsonParser;", "Lcom/stripe/android/model/PaymentMethod$Card$Networks;", "()V", "parse", "json", "Lorg/json/JSONObject;", "Companion", "stripe_release"}, k = 1, mv = {1, 1, 16})
        /* compiled from: PaymentMethodJsonParser.kt */
        public static final class NetworksJsonParser implements ModelJsonParser<PaymentMethod.Card.Networks> {
            @Deprecated
            public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
            private static final String FIELD_AVAIABLE = "available";
            private static final String FIELD_PREFERRED = "preferred";
            private static final String FIELD_SELECTION_MANDATORY = "selection_mandatory";

            public PaymentMethod.Card.Networks parse(JSONObject jSONObject) {
                Intrinsics.checkParameterIsNotNull(jSONObject, "json");
                List<Object> jsonArrayToList$stripe_release = StripeJsonUtils.INSTANCE.jsonArrayToList$stripe_release(jSONObject.optJSONArray(FIELD_AVAIABLE));
                if (jsonArrayToList$stripe_release == null) {
                    jsonArrayToList$stripe_release = CollectionsKt.emptyList();
                }
                Iterable<Object> iterable = jsonArrayToList$stripe_release;
                Collection arrayList = new ArrayList(CollectionsKt.collectionSizeOrDefault(iterable, 10));
                for (Object obj : iterable) {
                    arrayList.add(obj.toString());
                }
                return new PaymentMethod.Card.Networks(CollectionsKt.toSet((List) arrayList), StripeJsonUtils.INSTANCE.optBoolean$stripe_release(jSONObject, FIELD_SELECTION_MANDATORY), StripeJsonUtils.optString(jSONObject, FIELD_PREFERRED));
            }

            @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\u0007"}, d2 = {"Lcom/stripe/android/model/parsers/PaymentMethodJsonParser$CardJsonParser$NetworksJsonParser$Companion;", "", "()V", "FIELD_AVAIABLE", "", "FIELD_PREFERRED", "FIELD_SELECTION_MANDATORY", "stripe_release"}, k = 1, mv = {1, 1, 16})
            /* compiled from: PaymentMethodJsonParser.kt */
            private static final class Companion {
                private Companion() {
                }

                public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                    this();
                }
            }
        }

        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\n\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\u000e"}, d2 = {"Lcom/stripe/android/model/parsers/PaymentMethodJsonParser$CardJsonParser$Companion;", "", "()V", "FIELD_BRAND", "", "FIELD_CHECKS", "FIELD_COUNTRY", "FIELD_EXP_MONTH", "FIELD_EXP_YEAR", "FIELD_FUNDING", "FIELD_LAST4", "FIELD_NETWORKS", "FIELD_THREE_D_SECURE_USAGE", "FIELD_WALLET", "stripe_release"}, k = 1, mv = {1, 1, 16})
        /* compiled from: PaymentMethodJsonParser.kt */
        private static final class Companion {
            private Companion() {
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0000\u0018\u0000 \u00072\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0007B\u0005¢\u0006\u0002\u0010\u0003J\u0010\u0010\u0004\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0006H\u0016¨\u0006\b"}, d2 = {"Lcom/stripe/android/model/parsers/PaymentMethodJsonParser$IdealJsonParser;", "Lcom/stripe/android/model/parsers/ModelJsonParser;", "Lcom/stripe/android/model/PaymentMethod$Ideal;", "()V", "parse", "json", "Lorg/json/JSONObject;", "Companion", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: PaymentMethodJsonParser.kt */
    public static final class IdealJsonParser implements ModelJsonParser<PaymentMethod.Ideal> {
        @Deprecated
        public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
        private static final String FIELD_BANK = "bank";
        private static final String FIELD_BIC = "bic";

        public PaymentMethod.Ideal parse(JSONObject jSONObject) {
            Intrinsics.checkParameterIsNotNull(jSONObject, "json");
            return new PaymentMethod.Ideal(StripeJsonUtils.optString(jSONObject, FIELD_BANK), StripeJsonUtils.optString(jSONObject, FIELD_BIC));
        }

        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\u0006"}, d2 = {"Lcom/stripe/android/model/parsers/PaymentMethodJsonParser$IdealJsonParser$Companion;", "", "()V", "FIELD_BANK", "", "FIELD_BIC", "stripe_release"}, k = 1, mv = {1, 1, 16})
        /* compiled from: PaymentMethodJsonParser.kt */
        private static final class Companion {
            private Companion() {
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0000\u0018\u0000 \u00072\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0007B\u0005¢\u0006\u0002\u0010\u0003J\u0010\u0010\u0004\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0006H\u0016¨\u0006\b"}, d2 = {"Lcom/stripe/android/model/parsers/PaymentMethodJsonParser$FpxJsonParser;", "Lcom/stripe/android/model/parsers/ModelJsonParser;", "Lcom/stripe/android/model/PaymentMethod$Fpx;", "()V", "parse", "json", "Lorg/json/JSONObject;", "Companion", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: PaymentMethodJsonParser.kt */
    public static final class FpxJsonParser implements ModelJsonParser<PaymentMethod.Fpx> {
        @Deprecated
        public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
        private static final String FIELD_ACCOUNT_HOLDER_TYPE = "account_holder_type";
        private static final String FIELD_BANK = "bank";

        public PaymentMethod.Fpx parse(JSONObject jSONObject) {
            Intrinsics.checkParameterIsNotNull(jSONObject, "json");
            return new PaymentMethod.Fpx(StripeJsonUtils.optString(jSONObject, FIELD_BANK), StripeJsonUtils.optString(jSONObject, FIELD_ACCOUNT_HOLDER_TYPE));
        }

        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\u0006"}, d2 = {"Lcom/stripe/android/model/parsers/PaymentMethodJsonParser$FpxJsonParser$Companion;", "", "()V", "FIELD_ACCOUNT_HOLDER_TYPE", "", "FIELD_BANK", "stripe_release"}, k = 1, mv = {1, 1, 16})
        /* compiled from: PaymentMethodJsonParser.kt */
        private static final class Companion {
            private Companion() {
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0000\u0018\u0000 \u00072\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0007B\u0005¢\u0006\u0002\u0010\u0003J\u0010\u0010\u0004\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0006H\u0016¨\u0006\b"}, d2 = {"Lcom/stripe/android/model/parsers/PaymentMethodJsonParser$SepaDebitJsonParser;", "Lcom/stripe/android/model/parsers/ModelJsonParser;", "Lcom/stripe/android/model/PaymentMethod$SepaDebit;", "()V", "parse", "json", "Lorg/json/JSONObject;", "Companion", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: PaymentMethodJsonParser.kt */
    public static final class SepaDebitJsonParser implements ModelJsonParser<PaymentMethod.SepaDebit> {
        @Deprecated
        public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
        private static final String FIELD_BANK_CODE = "bank_code";
        private static final String FIELD_BRANCH_CODE = "branch_code";
        private static final String FIELD_COUNTRY = "country";
        private static final String FIELD_FINGERPRINT = "fingerprint";
        private static final String FIELD_LAST4 = "last4";

        public PaymentMethod.SepaDebit parse(JSONObject jSONObject) {
            Intrinsics.checkParameterIsNotNull(jSONObject, "json");
            return new PaymentMethod.SepaDebit(StripeJsonUtils.optString(jSONObject, FIELD_BANK_CODE), StripeJsonUtils.optString(jSONObject, FIELD_BRANCH_CODE), StripeJsonUtils.optString(jSONObject, "country"), StripeJsonUtils.optString(jSONObject, FIELD_FINGERPRINT), StripeJsonUtils.optString(jSONObject, FIELD_LAST4));
        }

        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0005\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\t"}, d2 = {"Lcom/stripe/android/model/parsers/PaymentMethodJsonParser$SepaDebitJsonParser$Companion;", "", "()V", "FIELD_BANK_CODE", "", "FIELD_BRANCH_CODE", "FIELD_COUNTRY", "FIELD_FINGERPRINT", "FIELD_LAST4", "stripe_release"}, k = 1, mv = {1, 1, 16})
        /* compiled from: PaymentMethodJsonParser.kt */
        private static final class Companion {
            private Companion() {
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0000\u0018\u0000 \u00072\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0007B\u0005¢\u0006\u0002\u0010\u0003J\u0012\u0010\u0004\u001a\u0004\u0018\u00010\u00022\u0006\u0010\u0005\u001a\u00020\u0006H\u0016¨\u0006\b"}, d2 = {"Lcom/stripe/android/model/parsers/PaymentMethodJsonParser$AuBecsDebitJsonParser;", "Lcom/stripe/android/model/parsers/ModelJsonParser;", "Lcom/stripe/android/model/PaymentMethod$AuBecsDebit;", "()V", "parse", "json", "Lorg/json/JSONObject;", "Companion", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: PaymentMethodJsonParser.kt */
    public static final class AuBecsDebitJsonParser implements ModelJsonParser<PaymentMethod.AuBecsDebit> {
        @Deprecated
        public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
        private static final String FIELD_BSB_NUMBER = "bsb_number";
        private static final String FIELD_FINGERPRINT = "fingerprint";
        private static final String FIELD_LAST4 = "last4";

        public PaymentMethod.AuBecsDebit parse(JSONObject jSONObject) {
            Intrinsics.checkParameterIsNotNull(jSONObject, "json");
            return new PaymentMethod.AuBecsDebit(StripeJsonUtils.optString(jSONObject, FIELD_BSB_NUMBER), StripeJsonUtils.optString(jSONObject, FIELD_FINGERPRINT), StripeJsonUtils.optString(jSONObject, FIELD_LAST4));
        }

        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\u0007"}, d2 = {"Lcom/stripe/android/model/parsers/PaymentMethodJsonParser$AuBecsDebitJsonParser$Companion;", "", "()V", "FIELD_BSB_NUMBER", "", "FIELD_FINGERPRINT", "FIELD_LAST4", "stripe_release"}, k = 1, mv = {1, 1, 16})
        /* compiled from: PaymentMethodJsonParser.kt */
        private static final class Companion {
            private Companion() {
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0000\u0018\u0000 \u00072\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0007B\u0005¢\u0006\u0002\u0010\u0003J\u0012\u0010\u0004\u001a\u0004\u0018\u00010\u00022\u0006\u0010\u0005\u001a\u00020\u0006H\u0016¨\u0006\b"}, d2 = {"Lcom/stripe/android/model/parsers/PaymentMethodJsonParser$BacsDebitJsonParser;", "Lcom/stripe/android/model/parsers/ModelJsonParser;", "Lcom/stripe/android/model/PaymentMethod$BacsDebit;", "()V", "parse", "json", "Lorg/json/JSONObject;", "Companion", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: PaymentMethodJsonParser.kt */
    public static final class BacsDebitJsonParser implements ModelJsonParser<PaymentMethod.BacsDebit> {
        @Deprecated
        public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
        private static final String FIELD_FINGERPRINT = "fingerprint";
        private static final String FIELD_LAST4 = "last4";
        private static final String FIELD_SORT_CODE = "sort_code";

        public PaymentMethod.BacsDebit parse(JSONObject jSONObject) {
            Intrinsics.checkParameterIsNotNull(jSONObject, "json");
            return new PaymentMethod.BacsDebit(StripeJsonUtils.optString(jSONObject, FIELD_FINGERPRINT), StripeJsonUtils.optString(jSONObject, FIELD_LAST4), StripeJsonUtils.optString(jSONObject, FIELD_SORT_CODE));
        }

        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\u0007"}, d2 = {"Lcom/stripe/android/model/parsers/PaymentMethodJsonParser$BacsDebitJsonParser$Companion;", "", "()V", "FIELD_FINGERPRINT", "", "FIELD_LAST4", "FIELD_SORT_CODE", "stripe_release"}, k = 1, mv = {1, 1, 16})
        /* compiled from: PaymentMethodJsonParser.kt */
        private static final class Companion {
            private Companion() {
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0000\u0018\u0000 \u00072\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0007B\u0005¢\u0006\u0002\u0010\u0003J\u0010\u0010\u0004\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0006H\u0016¨\u0006\b"}, d2 = {"Lcom/stripe/android/model/parsers/PaymentMethodJsonParser$SofortJsonParser;", "Lcom/stripe/android/model/parsers/ModelJsonParser;", "Lcom/stripe/android/model/PaymentMethod$Sofort;", "()V", "parse", "json", "Lorg/json/JSONObject;", "Companion", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: PaymentMethodJsonParser.kt */
    public static final class SofortJsonParser implements ModelJsonParser<PaymentMethod.Sofort> {
        @Deprecated
        public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
        private static final String FIELD_COUNTRY = "country";

        public PaymentMethod.Sofort parse(JSONObject jSONObject) {
            Intrinsics.checkParameterIsNotNull(jSONObject, "json");
            return new PaymentMethod.Sofort(StripeJsonUtils.optString(jSONObject, "country"));
        }

        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\u0005"}, d2 = {"Lcom/stripe/android/model/parsers/PaymentMethodJsonParser$SofortJsonParser$Companion;", "", "()V", "FIELD_COUNTRY", "", "stripe_release"}, k = 1, mv = {1, 1, 16})
        /* compiled from: PaymentMethodJsonParser.kt */
        private static final class Companion {
            private Companion() {
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0007\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\u000b"}, d2 = {"Lcom/stripe/android/model/parsers/PaymentMethodJsonParser$Companion;", "", "()V", "FIELD_BILLING_DETAILS", "", "FIELD_CREATED", "FIELD_CUSTOMER", "FIELD_ID", "FIELD_LIVEMODE", "FIELD_METADATA", "FIELD_TYPE", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: PaymentMethodJsonParser.kt */
    private static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }
}
