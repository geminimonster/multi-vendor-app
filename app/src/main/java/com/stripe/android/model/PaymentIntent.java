package com.stripe.android.model;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.internal.AnalyticsEvents;
import com.stripe.android.model.PaymentMethod;
import com.stripe.android.model.StripeIntent;
import com.stripe.android.model.parsers.PaymentIntentJsonParser;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import kotlin.Deprecated;
import kotlin.Metadata;
import kotlin.ReplaceWith;
import kotlin.jvm.internal.Intrinsics;
import org.json.JSONObject;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010 \n\u0000\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010$\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0018\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u001b\n\u0002\u0010\b\n\u0002\b\u0007\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\b\b\u0018\u0000 \u00012\u00020\u0001:\b~\u0001\u0001\u0001B\u0002\b\u0000\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u0005\u0012\b\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u0012\b\b\u0002\u0010\b\u001a\u00020\u0007\u0012\n\b\u0002\u0010\t\u001a\u0004\u0018\u00010\n\u0012\b\u0010\u000b\u001a\u0004\u0018\u00010\u0003\u0012\b\u0010\f\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\r\u001a\u0004\u0018\u00010\u0003\u0012\u0006\u0010\u000e\u001a\u00020\u0007\u0012\b\u0010\u000f\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\u0010\u001a\u0004\u0018\u00010\u0003\u0012\u0006\u0010\u0011\u001a\u00020\u0012\u0012\u001d\b\u0002\u0010\u0013\u001a\u0017\u0012\u0004\u0012\u00020\u0003\u0012\u000b\u0012\t\u0018\u00010\u0015¢\u0006\u0002\b\u0016\u0018\u00010\u0014\u0012\n\b\u0002\u0010\u0017\u001a\u0004\u0018\u00010\u0018\u0012\n\b\u0002\u0010\u0019\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\u001a\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\u001b\u001a\u0004\u0018\u00010\u001c\u0012\n\b\u0002\u0010\u001d\u001a\u0004\u0018\u00010\u001e\u0012\n\b\u0002\u0010\u001f\u001a\u0004\u0018\u00010 \u0012\n\b\u0002\u0010!\u001a\u0004\u0018\u00010\"\u0012\n\b\u0002\u0010#\u001a\u0004\u0018\u00010$¢\u0006\u0002\u0010%J\u000b\u0010Z\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000b\u0010[\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000b\u0010\\\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\t\u0010]\u001a\u00020\u0012HÆ\u0003J\u001e\u0010^\u001a\u0017\u0012\u0004\u0012\u00020\u0003\u0012\u000b\u0012\t\u0018\u00010\u0015¢\u0006\u0002\b\u0016\u0018\u00010\u0014HÆ\u0003J\u000b\u0010_\u001a\u0004\u0018\u00010\u0018HÆ\u0003J\u000b\u0010`\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000b\u0010a\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000b\u0010b\u001a\u0004\u0018\u00010\u001cHÆ\u0003J\u000b\u0010c\u001a\u0004\u0018\u00010\u001eHÂ\u0003J\u000b\u0010d\u001a\u0004\u0018\u00010 HÆ\u0003J\u000f\u0010e\u001a\b\u0012\u0004\u0012\u00020\u00030\u0005HÆ\u0003J\u000b\u0010f\u001a\u0004\u0018\u00010\"HÆ\u0003J\u000b\u0010g\u001a\u0004\u0018\u00010$HÆ\u0003J\u0010\u0010h\u001a\u0004\u0018\u00010\u0007HÆ\u0003¢\u0006\u0002\u0010'J\t\u0010i\u001a\u00020\u0007HÆ\u0003J\u000b\u0010j\u001a\u0004\u0018\u00010\nHÆ\u0003J\u000b\u0010k\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000b\u0010l\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000b\u0010m\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\t\u0010n\u001a\u00020\u0007HÆ\u0003J\u0002\u0010o\u001a\u00020\u00002\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u00032\u000e\b\u0002\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u00052\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u00072\b\b\u0002\u0010\b\u001a\u00020\u00072\n\b\u0002\u0010\t\u001a\u0004\u0018\u00010\n2\n\b\u0002\u0010\u000b\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\f\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\r\u001a\u0004\u0018\u00010\u00032\b\b\u0002\u0010\u000e\u001a\u00020\u00072\n\b\u0002\u0010\u000f\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u0010\u001a\u0004\u0018\u00010\u00032\b\b\u0002\u0010\u0011\u001a\u00020\u00122\u001d\b\u0002\u0010\u0013\u001a\u0017\u0012\u0004\u0012\u00020\u0003\u0012\u000b\u0012\t\u0018\u00010\u0015¢\u0006\u0002\b\u0016\u0018\u00010\u00142\n\b\u0002\u0010\u0017\u001a\u0004\u0018\u00010\u00182\n\b\u0002\u0010\u0019\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u001a\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u001b\u001a\u0004\u0018\u00010\u001c2\n\b\u0002\u0010\u001d\u001a\u0004\u0018\u00010\u001e2\n\b\u0002\u0010\u001f\u001a\u0004\u0018\u00010 2\n\b\u0002\u0010!\u001a\u0004\u0018\u00010\"2\n\b\u0002\u0010#\u001a\u0004\u0018\u00010$HÆ\u0001¢\u0006\u0002\u0010pJ\t\u0010q\u001a\u00020rHÖ\u0001J\u0013\u0010s\u001a\u00020\u00122\b\u0010t\u001a\u0004\u0018\u00010\u0015HÖ\u0003J\t\u0010u\u001a\u00020rHÖ\u0001J\b\u0010v\u001a\u00020\u0012H\u0016J\b\u0010w\u001a\u00020\u0012H\u0016J\t\u0010x\u001a\u00020\u0003HÖ\u0001J\u0019\u0010y\u001a\u00020z2\u0006\u0010{\u001a\u00020|2\u0006\u0010}\u001a\u00020rHÖ\u0001R\u0015\u0010\u0006\u001a\u0004\u0018\u00010\u0007¢\u0006\n\n\u0002\u0010(\u001a\u0004\b&\u0010'R\u0011\u0010\b\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\b)\u0010*R\u0013\u0010\t\u001a\u0004\u0018\u00010\n¢\u0006\b\n\u0000\u001a\u0004\b+\u0010,R\u0013\u0010\u000b\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b-\u0010.R\u0016\u0010\f\u001a\u0004\u0018\u00010\u0003X\u0004¢\u0006\b\n\u0000\u001a\u0004\b/\u0010.R\u0013\u0010\r\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b0\u0010.R\u0014\u0010\u000e\u001a\u00020\u0007X\u0004¢\u0006\b\n\u0000\u001a\u0004\b1\u0010*R\u0013\u0010\u000f\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b2\u0010.R\u0016\u0010\u0010\u001a\u0004\u0018\u00010\u0003X\u0004¢\u0006\b\n\u0000\u001a\u0004\b3\u0010.R\u0016\u0010\u0002\u001a\u0004\u0018\u00010\u0003X\u0004¢\u0006\b\n\u0000\u001a\u0004\b4\u0010.R\u0014\u0010\u0011\u001a\u00020\u0012X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0011\u00105R\u0013\u0010\u001f\u001a\u0004\u0018\u00010 ¢\u0006\b\n\u0000\u001a\u0004\b6\u00107R&\u0010\u0013\u001a\u0017\u0012\u0004\u0012\u00020\u0003\u0012\u000b\u0012\t\u0018\u00010\u0015¢\u0006\u0002\b\u0016\u0018\u00010\u0014¢\u0006\b\n\u0000\u001a\u0004\b8\u00109R\u0016\u0010#\u001a\u0004\u0018\u00010$X\u0004¢\u0006\b\n\u0000\u001a\u0004\b:\u0010;R\u0016\u0010<\u001a\u0004\u0018\u00010=8VX\u0004¢\u0006\u0006\u001a\u0004\b>\u0010?R\u0016\u0010\u0017\u001a\u0004\u0018\u00010\u0018X\u0004¢\u0006\b\n\u0000\u001a\u0004\b@\u0010AR\u0016\u0010\u0019\u001a\u0004\u0018\u00010\u0003X\u0004¢\u0006\b\n\u0000\u001a\u0004\bB\u0010.R\u001a\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u0005X\u0004¢\u0006\b\n\u0000\u001a\u0004\bC\u0010DR\u0013\u0010\u001a\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\bE\u0010.R\u001c\u0010F\u001a\u0004\u0018\u00010G8VX\u0004¢\u0006\f\u0012\u0004\bH\u0010I\u001a\u0004\bJ\u0010KR\u001c\u0010L\u001a\u0004\u0018\u00010M8FX\u0004¢\u0006\f\u0012\u0004\bN\u0010I\u001a\u0004\bO\u0010PR\u0010\u0010\u001d\u001a\u0004\u0018\u00010\u001eX\u0004¢\u0006\u0002\n\u0000R\u0013\u0010!\u001a\u0004\u0018\u00010\"¢\u0006\b\n\u0000\u001a\u0004\bQ\u0010RR\u0016\u0010\u001b\u001a\u0004\u0018\u00010\u001cX\u0004¢\u0006\b\n\u0000\u001a\u0004\bS\u0010TR\u001c\u0010U\u001a\u0004\u0018\u00010V8VX\u0004¢\u0006\f\u0012\u0004\bW\u0010I\u001a\u0004\bX\u0010Y¨\u0006\u0001"}, d2 = {"Lcom/stripe/android/model/PaymentIntent;", "Lcom/stripe/android/model/StripeIntent;", "id", "", "paymentMethodTypes", "", "amount", "", "canceledAt", "cancellationReason", "Lcom/stripe/android/model/PaymentIntent$CancellationReason;", "captureMethod", "clientSecret", "confirmationMethod", "created", "currency", "description", "isLiveMode", "", "nextAction", "", "", "Lkotlinx/android/parcel/RawValue;", "paymentMethod", "Lcom/stripe/android/model/PaymentMethod;", "paymentMethodId", "receiptEmail", "status", "Lcom/stripe/android/model/StripeIntent$Status;", "setupFutureUsage", "Lcom/stripe/android/model/StripeIntent$Usage;", "lastPaymentError", "Lcom/stripe/android/model/PaymentIntent$Error;", "shipping", "Lcom/stripe/android/model/PaymentIntent$Shipping;", "nextActionData", "Lcom/stripe/android/model/StripeIntent$NextActionData;", "(Ljava/lang/String;Ljava/util/List;Ljava/lang/Long;JLcom/stripe/android/model/PaymentIntent$CancellationReason;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;ZLjava/util/Map;Lcom/stripe/android/model/PaymentMethod;Ljava/lang/String;Ljava/lang/String;Lcom/stripe/android/model/StripeIntent$Status;Lcom/stripe/android/model/StripeIntent$Usage;Lcom/stripe/android/model/PaymentIntent$Error;Lcom/stripe/android/model/PaymentIntent$Shipping;Lcom/stripe/android/model/StripeIntent$NextActionData;)V", "getAmount", "()Ljava/lang/Long;", "Ljava/lang/Long;", "getCanceledAt", "()J", "getCancellationReason", "()Lcom/stripe/android/model/PaymentIntent$CancellationReason;", "getCaptureMethod", "()Ljava/lang/String;", "getClientSecret", "getConfirmationMethod", "getCreated", "getCurrency", "getDescription", "getId", "()Z", "getLastPaymentError", "()Lcom/stripe/android/model/PaymentIntent$Error;", "getNextAction", "()Ljava/util/Map;", "getNextActionData", "()Lcom/stripe/android/model/StripeIntent$NextActionData;", "nextActionType", "Lcom/stripe/android/model/StripeIntent$NextActionType;", "getNextActionType", "()Lcom/stripe/android/model/StripeIntent$NextActionType;", "getPaymentMethod", "()Lcom/stripe/android/model/PaymentMethod;", "getPaymentMethodId", "getPaymentMethodTypes", "()Ljava/util/List;", "getReceiptEmail", "redirectData", "Lcom/stripe/android/model/StripeIntent$RedirectData;", "redirectData$annotations", "()V", "getRedirectData", "()Lcom/stripe/android/model/StripeIntent$RedirectData;", "redirectUrl", "Landroid/net/Uri;", "redirectUrl$annotations", "getRedirectUrl", "()Landroid/net/Uri;", "getShipping", "()Lcom/stripe/android/model/PaymentIntent$Shipping;", "getStatus", "()Lcom/stripe/android/model/StripeIntent$Status;", "stripeSdkData", "Lcom/stripe/android/model/StripeIntent$SdkData;", "stripeSdkData$annotations", "getStripeSdkData", "()Lcom/stripe/android/model/StripeIntent$SdkData;", "component1", "component10", "component11", "component12", "component13", "component14", "component15", "component16", "component17", "component18", "component19", "component2", "component20", "component21", "component3", "component4", "component5", "component6", "component7", "component8", "component9", "copy", "(Ljava/lang/String;Ljava/util/List;Ljava/lang/Long;JLcom/stripe/android/model/PaymentIntent$CancellationReason;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;ZLjava/util/Map;Lcom/stripe/android/model/PaymentMethod;Ljava/lang/String;Ljava/lang/String;Lcom/stripe/android/model/StripeIntent$Status;Lcom/stripe/android/model/StripeIntent$Usage;Lcom/stripe/android/model/PaymentIntent$Error;Lcom/stripe/android/model/PaymentIntent$Shipping;Lcom/stripe/android/model/StripeIntent$NextActionData;)Lcom/stripe/android/model/PaymentIntent;", "describeContents", "", "equals", "other", "hashCode", "requiresAction", "requiresConfirmation", "toString", "writeToParcel", "", "parcel", "Landroid/os/Parcel;", "flags", "CancellationReason", "ClientSecret", "Companion", "Error", "Shipping", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: PaymentIntent.kt */
public final class PaymentIntent implements StripeIntent {
    public static final Parcelable.Creator CREATOR = new Creator();
    public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
    private final Long amount;
    private final long canceledAt;
    private final CancellationReason cancellationReason;
    private final String captureMethod;
    private final String clientSecret;
    private final String confirmationMethod;
    private final long created;
    private final String currency;
    private final String description;
    private final String id;
    private final boolean isLiveMode;
    private final Error lastPaymentError;
    private final Map<String, Object> nextAction;
    private final StripeIntent.NextActionData nextActionData;
    private final PaymentMethod paymentMethod;
    private final String paymentMethodId;
    private final List<String> paymentMethodTypes;
    private final String receiptEmail;
    private final StripeIntent.Usage setupFutureUsage;
    private final Shipping shipping;
    private final StripeIntent.Status status;

    @Metadata(bv = {1, 0, 3}, k = 3, mv = {1, 1, 16})
    public static class Creator implements Parcelable.Creator {
        public final Object createFromParcel(Parcel parcel) {
            long j;
            LinkedHashMap linkedHashMap;
            String str;
            Parcel parcel2 = parcel;
            Intrinsics.checkParameterIsNotNull(parcel2, "in");
            String readString = parcel.readString();
            List createStringArrayList = parcel.createStringArrayList();
            Long valueOf = parcel.readInt() != 0 ? Long.valueOf(parcel.readLong()) : null;
            long readLong = parcel.readLong();
            CancellationReason cancellationReason = parcel.readInt() != 0 ? (CancellationReason) Enum.valueOf(CancellationReason.class, parcel.readString()) : null;
            String readString2 = parcel.readString();
            String readString3 = parcel.readString();
            String readString4 = parcel.readString();
            long readLong2 = parcel.readLong();
            String readString5 = parcel.readString();
            String readString6 = parcel.readString();
            boolean z = parcel.readInt() != 0;
            if (parcel.readInt() != 0) {
                int readInt = parcel.readInt();
                LinkedHashMap linkedHashMap2 = new LinkedHashMap(readInt);
                while (readInt != 0) {
                    linkedHashMap2.put(parcel.readString(), parcel2.readValue(Object.class.getClassLoader()));
                    readInt--;
                    readString5 = readString5;
                    readLong2 = readLong2;
                }
                j = readLong2;
                str = readString5;
                linkedHashMap = linkedHashMap2;
            } else {
                j = readLong2;
                str = readString5;
                linkedHashMap = null;
            }
            return new PaymentIntent(readString, createStringArrayList, valueOf, readLong, cancellationReason, readString2, readString3, readString4, j, str, readString6, z, linkedHashMap, parcel.readInt() != 0 ? (PaymentMethod) PaymentMethod.CREATOR.createFromParcel(parcel2) : null, parcel.readString(), parcel.readString(), parcel.readInt() != 0 ? (StripeIntent.Status) Enum.valueOf(StripeIntent.Status.class, parcel.readString()) : null, parcel.readInt() != 0 ? (StripeIntent.Usage) Enum.valueOf(StripeIntent.Usage.class, parcel.readString()) : null, parcel.readInt() != 0 ? (Error) Error.CREATOR.createFromParcel(parcel2) : null, parcel.readInt() != 0 ? (Shipping) Shipping.CREATOR.createFromParcel(parcel2) : null, (StripeIntent.NextActionData) parcel2.readParcelable(PaymentIntent.class.getClassLoader()));
        }

        public final Object[] newArray(int i) {
            return new PaymentIntent[i];
        }
    }

    private final StripeIntent.Usage component18() {
        return this.setupFutureUsage;
    }

    public static /* synthetic */ PaymentIntent copy$default(PaymentIntent paymentIntent, String str, List list, Long l, long j, CancellationReason cancellationReason2, String str2, String str3, String str4, long j2, String str5, String str6, boolean z, Map map, PaymentMethod paymentMethod2, String str7, String str8, StripeIntent.Status status2, StripeIntent.Usage usage, Error error, Shipping shipping2, StripeIntent.NextActionData nextActionData2, int i, Object obj) {
        PaymentIntent paymentIntent2 = paymentIntent;
        int i2 = i;
        return paymentIntent.copy((i2 & 1) != 0 ? paymentIntent.getId() : str, (i2 & 2) != 0 ? paymentIntent.getPaymentMethodTypes() : list, (i2 & 4) != 0 ? paymentIntent2.amount : l, (i2 & 8) != 0 ? paymentIntent2.canceledAt : j, (i2 & 16) != 0 ? paymentIntent2.cancellationReason : cancellationReason2, (i2 & 32) != 0 ? paymentIntent2.captureMethod : str2, (i2 & 64) != 0 ? paymentIntent.getClientSecret() : str3, (i2 & 128) != 0 ? paymentIntent2.confirmationMethod : str4, (i2 & 256) != 0 ? paymentIntent.getCreated() : j2, (i2 & 512) != 0 ? paymentIntent2.currency : str5, (i2 & 1024) != 0 ? paymentIntent.getDescription() : str6, (i2 & 2048) != 0 ? paymentIntent.isLiveMode() : z, (i2 & 4096) != 0 ? paymentIntent2.nextAction : map, (i2 & 8192) != 0 ? paymentIntent.getPaymentMethod() : paymentMethod2, (i2 & 16384) != 0 ? paymentIntent.getPaymentMethodId() : str7, (i2 & 32768) != 0 ? paymentIntent2.receiptEmail : str8, (i2 & 65536) != 0 ? paymentIntent.getStatus() : status2, (i2 & 131072) != 0 ? paymentIntent2.setupFutureUsage : usage, (i2 & 262144) != 0 ? paymentIntent2.lastPaymentError : error, (i2 & 524288) != 0 ? paymentIntent2.shipping : shipping2, (i2 & 1048576) != 0 ? paymentIntent.getNextActionData() : nextActionData2);
    }

    @Deprecated(message = "use {@link #nextActionData}", replaceWith = @ReplaceWith(expression = "nextActionData as? StripeIntent.NextActionData.RedirectToUrl", imports = {}))
    public static /* synthetic */ void redirectData$annotations() {
    }

    @Deprecated(message = "use {@link #nextActionData}", replaceWith = @ReplaceWith(expression = "(nextActionData as? StripeIntent.NextActionData.RedirectToUrl)?.url", imports = {}))
    public static /* synthetic */ void redirectUrl$annotations() {
    }

    @Deprecated(message = "use {@link #nextActionData}", replaceWith = @ReplaceWith(expression = "nextActionData as? StripeIntent.NextActionData.SdkData", imports = {}))
    public static /* synthetic */ void stripeSdkData$annotations() {
    }

    public final String component1() {
        return getId();
    }

    public final String component10() {
        return this.currency;
    }

    public final String component11() {
        return getDescription();
    }

    public final boolean component12() {
        return isLiveMode();
    }

    public final Map<String, Object> component13() {
        return this.nextAction;
    }

    public final PaymentMethod component14() {
        return getPaymentMethod();
    }

    public final String component15() {
        return getPaymentMethodId();
    }

    public final String component16() {
        return this.receiptEmail;
    }

    public final StripeIntent.Status component17() {
        return getStatus();
    }

    public final Error component19() {
        return this.lastPaymentError;
    }

    public final List<String> component2() {
        return getPaymentMethodTypes();
    }

    public final Shipping component20() {
        return this.shipping;
    }

    public final StripeIntent.NextActionData component21() {
        return getNextActionData();
    }

    public final Long component3() {
        return this.amount;
    }

    public final long component4() {
        return this.canceledAt;
    }

    public final CancellationReason component5() {
        return this.cancellationReason;
    }

    public final String component6() {
        return this.captureMethod;
    }

    public final String component7() {
        return getClientSecret();
    }

    public final String component8() {
        return this.confirmationMethod;
    }

    public final long component9() {
        return getCreated();
    }

    public final PaymentIntent copy(String str, List<String> list, Long l, long j, CancellationReason cancellationReason2, String str2, String str3, String str4, long j2, String str5, String str6, boolean z, Map<String, ? extends Object> map, PaymentMethod paymentMethod2, String str7, String str8, StripeIntent.Status status2, StripeIntent.Usage usage, Error error, Shipping shipping2, StripeIntent.NextActionData nextActionData2) {
        String str9 = str;
        Intrinsics.checkParameterIsNotNull(list, "paymentMethodTypes");
        return new PaymentIntent(str, list, l, j, cancellationReason2, str2, str3, str4, j2, str5, str6, z, map, paymentMethod2, str7, str8, status2, usage, error, shipping2, nextActionData2);
    }

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof PaymentIntent)) {
            return false;
        }
        PaymentIntent paymentIntent = (PaymentIntent) obj;
        return Intrinsics.areEqual((Object) getId(), (Object) paymentIntent.getId()) && Intrinsics.areEqual((Object) getPaymentMethodTypes(), (Object) paymentIntent.getPaymentMethodTypes()) && Intrinsics.areEqual((Object) this.amount, (Object) paymentIntent.amount) && this.canceledAt == paymentIntent.canceledAt && Intrinsics.areEqual((Object) this.cancellationReason, (Object) paymentIntent.cancellationReason) && Intrinsics.areEqual((Object) this.captureMethod, (Object) paymentIntent.captureMethod) && Intrinsics.areEqual((Object) getClientSecret(), (Object) paymentIntent.getClientSecret()) && Intrinsics.areEqual((Object) this.confirmationMethod, (Object) paymentIntent.confirmationMethod) && getCreated() == paymentIntent.getCreated() && Intrinsics.areEqual((Object) this.currency, (Object) paymentIntent.currency) && Intrinsics.areEqual((Object) getDescription(), (Object) paymentIntent.getDescription()) && isLiveMode() == paymentIntent.isLiveMode() && Intrinsics.areEqual((Object) this.nextAction, (Object) paymentIntent.nextAction) && Intrinsics.areEqual((Object) getPaymentMethod(), (Object) paymentIntent.getPaymentMethod()) && Intrinsics.areEqual((Object) getPaymentMethodId(), (Object) paymentIntent.getPaymentMethodId()) && Intrinsics.areEqual((Object) this.receiptEmail, (Object) paymentIntent.receiptEmail) && Intrinsics.areEqual((Object) getStatus(), (Object) paymentIntent.getStatus()) && Intrinsics.areEqual((Object) this.setupFutureUsage, (Object) paymentIntent.setupFutureUsage) && Intrinsics.areEqual((Object) this.lastPaymentError, (Object) paymentIntent.lastPaymentError) && Intrinsics.areEqual((Object) this.shipping, (Object) paymentIntent.shipping) && Intrinsics.areEqual((Object) getNextActionData(), (Object) paymentIntent.getNextActionData());
    }

    public int hashCode() {
        String id2 = getId();
        int i = 0;
        int hashCode = (id2 != null ? id2.hashCode() : 0) * 31;
        List<String> paymentMethodTypes2 = getPaymentMethodTypes();
        int hashCode2 = (hashCode + (paymentMethodTypes2 != null ? paymentMethodTypes2.hashCode() : 0)) * 31;
        Long l = this.amount;
        int hashCode3 = l != null ? l.hashCode() : 0;
        long j = this.canceledAt;
        int i2 = (((hashCode2 + hashCode3) * 31) + ((int) (j ^ (j >>> 32)))) * 31;
        CancellationReason cancellationReason2 = this.cancellationReason;
        int hashCode4 = (i2 + (cancellationReason2 != null ? cancellationReason2.hashCode() : 0)) * 31;
        String str = this.captureMethod;
        int hashCode5 = (hashCode4 + (str != null ? str.hashCode() : 0)) * 31;
        String clientSecret2 = getClientSecret();
        int hashCode6 = (hashCode5 + (clientSecret2 != null ? clientSecret2.hashCode() : 0)) * 31;
        String str2 = this.confirmationMethod;
        int hashCode7 = str2 != null ? str2.hashCode() : 0;
        long created2 = getCreated();
        int i3 = (((hashCode6 + hashCode7) * 31) + ((int) (created2 ^ (created2 >>> 32)))) * 31;
        String str3 = this.currency;
        int hashCode8 = (i3 + (str3 != null ? str3.hashCode() : 0)) * 31;
        String description2 = getDescription();
        int hashCode9 = (hashCode8 + (description2 != null ? description2.hashCode() : 0)) * 31;
        boolean isLiveMode2 = isLiveMode();
        if (isLiveMode2) {
            isLiveMode2 = true;
        }
        int i4 = (hashCode9 + (isLiveMode2 ? 1 : 0)) * 31;
        Map<String, Object> map = this.nextAction;
        int hashCode10 = (i4 + (map != null ? map.hashCode() : 0)) * 31;
        PaymentMethod paymentMethod2 = getPaymentMethod();
        int hashCode11 = (hashCode10 + (paymentMethod2 != null ? paymentMethod2.hashCode() : 0)) * 31;
        String paymentMethodId2 = getPaymentMethodId();
        int hashCode12 = (hashCode11 + (paymentMethodId2 != null ? paymentMethodId2.hashCode() : 0)) * 31;
        String str4 = this.receiptEmail;
        int hashCode13 = (hashCode12 + (str4 != null ? str4.hashCode() : 0)) * 31;
        StripeIntent.Status status2 = getStatus();
        int hashCode14 = (hashCode13 + (status2 != null ? status2.hashCode() : 0)) * 31;
        StripeIntent.Usage usage = this.setupFutureUsage;
        int hashCode15 = (hashCode14 + (usage != null ? usage.hashCode() : 0)) * 31;
        Error error = this.lastPaymentError;
        int hashCode16 = (hashCode15 + (error != null ? error.hashCode() : 0)) * 31;
        Shipping shipping2 = this.shipping;
        int hashCode17 = (hashCode16 + (shipping2 != null ? shipping2.hashCode() : 0)) * 31;
        StripeIntent.NextActionData nextActionData2 = getNextActionData();
        if (nextActionData2 != null) {
            i = nextActionData2.hashCode();
        }
        return hashCode17 + i;
    }

    public String toString() {
        return "PaymentIntent(id=" + getId() + ", paymentMethodTypes=" + getPaymentMethodTypes() + ", amount=" + this.amount + ", canceledAt=" + this.canceledAt + ", cancellationReason=" + this.cancellationReason + ", captureMethod=" + this.captureMethod + ", clientSecret=" + getClientSecret() + ", confirmationMethod=" + this.confirmationMethod + ", created=" + getCreated() + ", currency=" + this.currency + ", description=" + getDescription() + ", isLiveMode=" + isLiveMode() + ", nextAction=" + this.nextAction + ", paymentMethod=" + getPaymentMethod() + ", paymentMethodId=" + getPaymentMethodId() + ", receiptEmail=" + this.receiptEmail + ", status=" + getStatus() + ", setupFutureUsage=" + this.setupFutureUsage + ", lastPaymentError=" + this.lastPaymentError + ", shipping=" + this.shipping + ", nextActionData=" + getNextActionData() + ")";
    }

    public void writeToParcel(Parcel parcel, int i) {
        Intrinsics.checkParameterIsNotNull(parcel, "parcel");
        parcel.writeString(this.id);
        parcel.writeStringList(this.paymentMethodTypes);
        Long l = this.amount;
        if (l != null) {
            parcel.writeInt(1);
            parcel.writeLong(l.longValue());
        } else {
            parcel.writeInt(0);
        }
        parcel.writeLong(this.canceledAt);
        CancellationReason cancellationReason2 = this.cancellationReason;
        if (cancellationReason2 != null) {
            parcel.writeInt(1);
            parcel.writeString(cancellationReason2.name());
        } else {
            parcel.writeInt(0);
        }
        parcel.writeString(this.captureMethod);
        parcel.writeString(this.clientSecret);
        parcel.writeString(this.confirmationMethod);
        parcel.writeLong(this.created);
        parcel.writeString(this.currency);
        parcel.writeString(this.description);
        parcel.writeInt(this.isLiveMode ? 1 : 0);
        Map<String, Object> map = this.nextAction;
        if (map != null) {
            parcel.writeInt(1);
            parcel.writeInt(map.size());
            for (Map.Entry<String, Object> next : map.entrySet()) {
                parcel.writeString(next.getKey());
                parcel.writeValue(next.getValue());
            }
        } else {
            parcel.writeInt(0);
        }
        PaymentMethod paymentMethod2 = this.paymentMethod;
        if (paymentMethod2 != null) {
            parcel.writeInt(1);
            paymentMethod2.writeToParcel(parcel, 0);
        } else {
            parcel.writeInt(0);
        }
        parcel.writeString(this.paymentMethodId);
        parcel.writeString(this.receiptEmail);
        StripeIntent.Status status2 = this.status;
        if (status2 != null) {
            parcel.writeInt(1);
            parcel.writeString(status2.name());
        } else {
            parcel.writeInt(0);
        }
        StripeIntent.Usage usage = this.setupFutureUsage;
        if (usage != null) {
            parcel.writeInt(1);
            parcel.writeString(usage.name());
        } else {
            parcel.writeInt(0);
        }
        Error error = this.lastPaymentError;
        if (error != null) {
            parcel.writeInt(1);
            error.writeToParcel(parcel, 0);
        } else {
            parcel.writeInt(0);
        }
        Shipping shipping2 = this.shipping;
        if (shipping2 != null) {
            parcel.writeInt(1);
            shipping2.writeToParcel(parcel, 0);
        } else {
            parcel.writeInt(0);
        }
        parcel.writeParcelable(this.nextActionData, i);
    }

    public PaymentIntent(String str, List<String> list, Long l, long j, CancellationReason cancellationReason2, String str2, String str3, String str4, long j2, String str5, String str6, boolean z, Map<String, ? extends Object> map, PaymentMethod paymentMethod2, String str7, String str8, StripeIntent.Status status2, StripeIntent.Usage usage, Error error, Shipping shipping2, StripeIntent.NextActionData nextActionData2) {
        Intrinsics.checkParameterIsNotNull(list, "paymentMethodTypes");
        this.id = str;
        this.paymentMethodTypes = list;
        this.amount = l;
        this.canceledAt = j;
        this.cancellationReason = cancellationReason2;
        this.captureMethod = str2;
        this.clientSecret = str3;
        this.confirmationMethod = str4;
        this.created = j2;
        this.currency = str5;
        this.description = str6;
        this.isLiveMode = z;
        this.nextAction = map;
        this.paymentMethod = paymentMethod2;
        this.paymentMethodId = str7;
        this.receiptEmail = str8;
        this.status = status2;
        this.setupFutureUsage = usage;
        this.lastPaymentError = error;
        this.shipping = shipping2;
        this.nextActionData = nextActionData2;
    }

    public String getId() {
        return this.id;
    }

    public List<String> getPaymentMethodTypes() {
        return this.paymentMethodTypes;
    }

    public final Long getAmount() {
        return this.amount;
    }

    public final long getCanceledAt() {
        return this.canceledAt;
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* synthetic */ PaymentIntent(java.lang.String r28, java.util.List r29, java.lang.Long r30, long r31, com.stripe.android.model.PaymentIntent.CancellationReason r33, java.lang.String r34, java.lang.String r35, java.lang.String r36, long r37, java.lang.String r39, java.lang.String r40, boolean r41, java.util.Map r42, com.stripe.android.model.PaymentMethod r43, java.lang.String r44, java.lang.String r45, com.stripe.android.model.StripeIntent.Status r46, com.stripe.android.model.StripeIntent.Usage r47, com.stripe.android.model.PaymentIntent.Error r48, com.stripe.android.model.PaymentIntent.Shipping r49, com.stripe.android.model.StripeIntent.NextActionData r50, int r51, kotlin.jvm.internal.DefaultConstructorMarker r52) {
        /*
            r27 = this;
            r0 = r51
            r1 = r0 & 8
            if (r1 == 0) goto L_0x000a
            r1 = 0
            r7 = r1
            goto L_0x000c
        L_0x000a:
            r7 = r31
        L_0x000c:
            r1 = r0 & 16
            r2 = 0
            if (r1 == 0) goto L_0x0016
            r1 = r2
            com.stripe.android.model.PaymentIntent$CancellationReason r1 = (com.stripe.android.model.PaymentIntent.CancellationReason) r1
            r9 = r1
            goto L_0x0018
        L_0x0016:
            r9 = r33
        L_0x0018:
            r1 = r0 & 128(0x80, float:1.794E-43)
            if (r1 == 0) goto L_0x0021
            r1 = r2
            java.lang.String r1 = (java.lang.String) r1
            r12 = r1
            goto L_0x0023
        L_0x0021:
            r12 = r36
        L_0x0023:
            r1 = r0 & 1024(0x400, float:1.435E-42)
            if (r1 == 0) goto L_0x002d
            r1 = r2
            java.lang.String r1 = (java.lang.String) r1
            r16 = r1
            goto L_0x002f
        L_0x002d:
            r16 = r40
        L_0x002f:
            r1 = r0 & 4096(0x1000, float:5.74E-42)
            if (r1 == 0) goto L_0x0039
            r1 = r2
            java.util.Map r1 = (java.util.Map) r1
            r18 = r1
            goto L_0x003b
        L_0x0039:
            r18 = r42
        L_0x003b:
            r1 = r0 & 8192(0x2000, float:1.14794E-41)
            if (r1 == 0) goto L_0x0045
            r1 = r2
            com.stripe.android.model.PaymentMethod r1 = (com.stripe.android.model.PaymentMethod) r1
            r19 = r1
            goto L_0x0047
        L_0x0045:
            r19 = r43
        L_0x0047:
            r1 = r0 & 16384(0x4000, float:2.2959E-41)
            if (r1 == 0) goto L_0x0051
            r1 = r2
            java.lang.String r1 = (java.lang.String) r1
            r20 = r1
            goto L_0x0053
        L_0x0051:
            r20 = r44
        L_0x0053:
            r1 = 32768(0x8000, float:4.5918E-41)
            r1 = r1 & r0
            if (r1 == 0) goto L_0x005f
            r1 = r2
            java.lang.String r1 = (java.lang.String) r1
            r21 = r1
            goto L_0x0061
        L_0x005f:
            r21 = r45
        L_0x0061:
            r1 = 65536(0x10000, float:9.18355E-41)
            r1 = r1 & r0
            if (r1 == 0) goto L_0x006c
            r1 = r2
            com.stripe.android.model.StripeIntent$Status r1 = (com.stripe.android.model.StripeIntent.Status) r1
            r22 = r1
            goto L_0x006e
        L_0x006c:
            r22 = r46
        L_0x006e:
            r1 = 131072(0x20000, float:1.83671E-40)
            r1 = r1 & r0
            if (r1 == 0) goto L_0x0079
            r1 = r2
            com.stripe.android.model.StripeIntent$Usage r1 = (com.stripe.android.model.StripeIntent.Usage) r1
            r23 = r1
            goto L_0x007b
        L_0x0079:
            r23 = r47
        L_0x007b:
            r1 = 262144(0x40000, float:3.67342E-40)
            r1 = r1 & r0
            if (r1 == 0) goto L_0x0086
            r1 = r2
            com.stripe.android.model.PaymentIntent$Error r1 = (com.stripe.android.model.PaymentIntent.Error) r1
            r24 = r1
            goto L_0x0088
        L_0x0086:
            r24 = r48
        L_0x0088:
            r1 = 524288(0x80000, float:7.34684E-40)
            r1 = r1 & r0
            if (r1 == 0) goto L_0x0093
            r1 = r2
            com.stripe.android.model.PaymentIntent$Shipping r1 = (com.stripe.android.model.PaymentIntent.Shipping) r1
            r25 = r1
            goto L_0x0095
        L_0x0093:
            r25 = r49
        L_0x0095:
            r1 = 1048576(0x100000, float:1.469368E-39)
            r0 = r0 & r1
            if (r0 == 0) goto L_0x00a0
            r0 = r2
            com.stripe.android.model.StripeIntent$NextActionData r0 = (com.stripe.android.model.StripeIntent.NextActionData) r0
            r26 = r0
            goto L_0x00a2
        L_0x00a0:
            r26 = r50
        L_0x00a2:
            r3 = r27
            r4 = r28
            r5 = r29
            r6 = r30
            r10 = r34
            r11 = r35
            r13 = r37
            r15 = r39
            r17 = r41
            r3.<init>(r4, r5, r6, r7, r9, r10, r11, r12, r13, r15, r16, r17, r18, r19, r20, r21, r22, r23, r24, r25, r26)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.stripe.android.model.PaymentIntent.<init>(java.lang.String, java.util.List, java.lang.Long, long, com.stripe.android.model.PaymentIntent$CancellationReason, java.lang.String, java.lang.String, java.lang.String, long, java.lang.String, java.lang.String, boolean, java.util.Map, com.stripe.android.model.PaymentMethod, java.lang.String, java.lang.String, com.stripe.android.model.StripeIntent$Status, com.stripe.android.model.StripeIntent$Usage, com.stripe.android.model.PaymentIntent$Error, com.stripe.android.model.PaymentIntent$Shipping, com.stripe.android.model.StripeIntent$NextActionData, int, kotlin.jvm.internal.DefaultConstructorMarker):void");
    }

    public final CancellationReason getCancellationReason() {
        return this.cancellationReason;
    }

    public final String getCaptureMethod() {
        return this.captureMethod;
    }

    public String getClientSecret() {
        return this.clientSecret;
    }

    public final String getConfirmationMethod() {
        return this.confirmationMethod;
    }

    public long getCreated() {
        return this.created;
    }

    public final String getCurrency() {
        return this.currency;
    }

    public String getDescription() {
        return this.description;
    }

    public boolean isLiveMode() {
        return this.isLiveMode;
    }

    public final Map<String, Object> getNextAction() {
        return this.nextAction;
    }

    public PaymentMethod getPaymentMethod() {
        return this.paymentMethod;
    }

    public String getPaymentMethodId() {
        return this.paymentMethodId;
    }

    public final String getReceiptEmail() {
        return this.receiptEmail;
    }

    public StripeIntent.Status getStatus() {
        return this.status;
    }

    public final Error getLastPaymentError() {
        return this.lastPaymentError;
    }

    public final Shipping getShipping() {
        return this.shipping;
    }

    public StripeIntent.NextActionData getNextActionData() {
        return this.nextActionData;
    }

    public StripeIntent.NextActionType getNextActionType() {
        StripeIntent.NextActionData nextActionData2 = getNextActionData();
        if (nextActionData2 instanceof StripeIntent.NextActionData.SdkData) {
            return StripeIntent.NextActionType.UseStripeSdk;
        }
        if (nextActionData2 instanceof StripeIntent.NextActionData.RedirectToUrl) {
            return StripeIntent.NextActionType.RedirectToUrl;
        }
        if (nextActionData2 instanceof StripeIntent.NextActionData.DisplayOxxoDetails) {
            return StripeIntent.NextActionType.DisplayOxxoDetails;
        }
        return null;
    }

    public final Uri getRedirectUrl() {
        StripeIntent.RedirectData redirectData = getRedirectData();
        if (redirectData != null) {
            return redirectData.getUrl();
        }
        return null;
    }

    public StripeIntent.SdkData getStripeSdkData() {
        StripeIntent.NextActionData nextActionData2 = getNextActionData();
        if (nextActionData2 instanceof StripeIntent.NextActionData.SdkData.Use3DS1) {
            return new StripeIntent.SdkData(true, false);
        }
        if (nextActionData2 instanceof StripeIntent.NextActionData.SdkData.Use3DS2) {
            return new StripeIntent.SdkData(false, true);
        }
        return null;
    }

    public StripeIntent.RedirectData getRedirectData() {
        if (getNextActionData() instanceof StripeIntent.NextActionData.RedirectToUrl) {
            return new StripeIntent.RedirectData(((StripeIntent.NextActionData.RedirectToUrl) getNextActionData()).getUrl(), ((StripeIntent.NextActionData.RedirectToUrl) getNextActionData()).getReturnUrl());
        }
        return null;
    }

    public boolean requiresAction() {
        return getStatus() == StripeIntent.Status.RequiresAction;
    }

    public boolean requiresConfirmation() {
        return getStatus() == StripeIntent.Status.RequiresConfirmation;
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0016\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\b\b\u0018\u00002\u00020\u0001:\u0001/BW\b\u0000\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\b\u0010\u0004\u001a\u0004\u0018\u00010\u0003\u0012\b\u0010\u0005\u001a\u0004\u0018\u00010\u0003\u0012\b\u0010\u0006\u001a\u0004\u0018\u00010\u0003\u0012\b\u0010\u0007\u001a\u0004\u0018\u00010\u0003\u0012\b\u0010\b\u001a\u0004\u0018\u00010\u0003\u0012\b\u0010\t\u001a\u0004\u0018\u00010\n\u0012\b\u0010\u000b\u001a\u0004\u0018\u00010\f¢\u0006\u0002\u0010\rJ\u000b\u0010\u0019\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000b\u0010\u001a\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000b\u0010\u001b\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000b\u0010\u001c\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000b\u0010\u001d\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000b\u0010\u001e\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000b\u0010\u001f\u001a\u0004\u0018\u00010\nHÆ\u0003J\u000b\u0010 \u001a\u0004\u0018\u00010\fHÆ\u0003Ji\u0010!\u001a\u00020\u00002\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u0007\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\t\u001a\u0004\u0018\u00010\n2\n\b\u0002\u0010\u000b\u001a\u0004\u0018\u00010\fHÆ\u0001J\t\u0010\"\u001a\u00020#HÖ\u0001J\u0013\u0010$\u001a\u00020%2\b\u0010&\u001a\u0004\u0018\u00010'HÖ\u0003J\t\u0010(\u001a\u00020#HÖ\u0001J\t\u0010)\u001a\u00020\u0003HÖ\u0001J\u0019\u0010*\u001a\u00020+2\u0006\u0010,\u001a\u00020-2\u0006\u0010.\u001a\u00020#HÖ\u0001R\u0013\u0010\u0002\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u000fR\u0013\u0010\u0004\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u000fR\u0013\u0010\u0005\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u000fR\u0013\u0010\u0006\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u000fR\u0013\u0010\u0007\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u000fR\u0013\u0010\b\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0014\u0010\u000fR\u0013\u0010\t\u001a\u0004\u0018\u00010\n¢\u0006\b\n\u0000\u001a\u0004\b\u0015\u0010\u0016R\u0013\u0010\u000b\u001a\u0004\u0018\u00010\f¢\u0006\b\n\u0000\u001a\u0004\b\u0017\u0010\u0018¨\u00060"}, d2 = {"Lcom/stripe/android/model/PaymentIntent$Error;", "Lcom/stripe/android/model/StripeModel;", "charge", "", "code", "declineCode", "docUrl", "message", "param", "paymentMethod", "Lcom/stripe/android/model/PaymentMethod;", "type", "Lcom/stripe/android/model/PaymentIntent$Error$Type;", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/stripe/android/model/PaymentMethod;Lcom/stripe/android/model/PaymentIntent$Error$Type;)V", "getCharge", "()Ljava/lang/String;", "getCode", "getDeclineCode", "getDocUrl", "getMessage", "getParam", "getPaymentMethod", "()Lcom/stripe/android/model/PaymentMethod;", "getType", "()Lcom/stripe/android/model/PaymentIntent$Error$Type;", "component1", "component2", "component3", "component4", "component5", "component6", "component7", "component8", "copy", "describeContents", "", "equals", "", "other", "", "hashCode", "toString", "writeToParcel", "", "parcel", "Landroid/os/Parcel;", "flags", "Type", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: PaymentIntent.kt */
    public static final class Error implements StripeModel {
        public static final Parcelable.Creator CREATOR = new Creator();
        private final String charge;
        private final String code;
        private final String declineCode;
        private final String docUrl;
        private final String message;
        private final String param;
        private final PaymentMethod paymentMethod;
        private final Type type;

        @Metadata(bv = {1, 0, 3}, k = 3, mv = {1, 1, 16})
        public static class Creator implements Parcelable.Creator {
            public final Object createFromParcel(Parcel parcel) {
                Intrinsics.checkParameterIsNotNull(parcel, "in");
                return new Error(parcel.readString(), parcel.readString(), parcel.readString(), parcel.readString(), parcel.readString(), parcel.readString(), parcel.readInt() != 0 ? (PaymentMethod) PaymentMethod.CREATOR.createFromParcel(parcel) : null, parcel.readInt() != 0 ? (Type) Enum.valueOf(Type.class, parcel.readString()) : null);
            }

            public final Object[] newArray(int i) {
                return new Error[i];
            }
        }

        public static /* synthetic */ Error copy$default(Error error, String str, String str2, String str3, String str4, String str5, String str6, PaymentMethod paymentMethod2, Type type2, int i, Object obj) {
            Error error2 = error;
            int i2 = i;
            return error.copy((i2 & 1) != 0 ? error2.charge : str, (i2 & 2) != 0 ? error2.code : str2, (i2 & 4) != 0 ? error2.declineCode : str3, (i2 & 8) != 0 ? error2.docUrl : str4, (i2 & 16) != 0 ? error2.message : str5, (i2 & 32) != 0 ? error2.param : str6, (i2 & 64) != 0 ? error2.paymentMethod : paymentMethod2, (i2 & 128) != 0 ? error2.type : type2);
        }

        public final String component1() {
            return this.charge;
        }

        public final String component2() {
            return this.code;
        }

        public final String component3() {
            return this.declineCode;
        }

        public final String component4() {
            return this.docUrl;
        }

        public final String component5() {
            return this.message;
        }

        public final String component6() {
            return this.param;
        }

        public final PaymentMethod component7() {
            return this.paymentMethod;
        }

        public final Type component8() {
            return this.type;
        }

        public final Error copy(String str, String str2, String str3, String str4, String str5, String str6, PaymentMethod paymentMethod2, Type type2) {
            return new Error(str, str2, str3, str4, str5, str6, paymentMethod2, type2);
        }

        public int describeContents() {
            return 0;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Error)) {
                return false;
            }
            Error error = (Error) obj;
            return Intrinsics.areEqual((Object) this.charge, (Object) error.charge) && Intrinsics.areEqual((Object) this.code, (Object) error.code) && Intrinsics.areEqual((Object) this.declineCode, (Object) error.declineCode) && Intrinsics.areEqual((Object) this.docUrl, (Object) error.docUrl) && Intrinsics.areEqual((Object) this.message, (Object) error.message) && Intrinsics.areEqual((Object) this.param, (Object) error.param) && Intrinsics.areEqual((Object) this.paymentMethod, (Object) error.paymentMethod) && Intrinsics.areEqual((Object) this.type, (Object) error.type);
        }

        public int hashCode() {
            String str = this.charge;
            int i = 0;
            int hashCode = (str != null ? str.hashCode() : 0) * 31;
            String str2 = this.code;
            int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
            String str3 = this.declineCode;
            int hashCode3 = (hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31;
            String str4 = this.docUrl;
            int hashCode4 = (hashCode3 + (str4 != null ? str4.hashCode() : 0)) * 31;
            String str5 = this.message;
            int hashCode5 = (hashCode4 + (str5 != null ? str5.hashCode() : 0)) * 31;
            String str6 = this.param;
            int hashCode6 = (hashCode5 + (str6 != null ? str6.hashCode() : 0)) * 31;
            PaymentMethod paymentMethod2 = this.paymentMethod;
            int hashCode7 = (hashCode6 + (paymentMethod2 != null ? paymentMethod2.hashCode() : 0)) * 31;
            Type type2 = this.type;
            if (type2 != null) {
                i = type2.hashCode();
            }
            return hashCode7 + i;
        }

        public String toString() {
            return "Error(charge=" + this.charge + ", code=" + this.code + ", declineCode=" + this.declineCode + ", docUrl=" + this.docUrl + ", message=" + this.message + ", param=" + this.param + ", paymentMethod=" + this.paymentMethod + ", type=" + this.type + ")";
        }

        public void writeToParcel(Parcel parcel, int i) {
            Intrinsics.checkParameterIsNotNull(parcel, "parcel");
            parcel.writeString(this.charge);
            parcel.writeString(this.code);
            parcel.writeString(this.declineCode);
            parcel.writeString(this.docUrl);
            parcel.writeString(this.message);
            parcel.writeString(this.param);
            PaymentMethod paymentMethod2 = this.paymentMethod;
            if (paymentMethod2 != null) {
                parcel.writeInt(1);
                paymentMethod2.writeToParcel(parcel, 0);
            } else {
                parcel.writeInt(0);
            }
            Type type2 = this.type;
            if (type2 != null) {
                parcel.writeInt(1);
                parcel.writeString(type2.name());
                return;
            }
            parcel.writeInt(0);
        }

        public Error(String str, String str2, String str3, String str4, String str5, String str6, PaymentMethod paymentMethod2, Type type2) {
            this.charge = str;
            this.code = str2;
            this.declineCode = str3;
            this.docUrl = str4;
            this.message = str5;
            this.param = str6;
            this.paymentMethod = paymentMethod2;
            this.type = type2;
        }

        public final String getCharge() {
            return this.charge;
        }

        public final String getCode() {
            return this.code;
        }

        public final String getDeclineCode() {
            return this.declineCode;
        }

        public final String getDocUrl() {
            return this.docUrl;
        }

        public final String getMessage() {
            return this.message;
        }

        public final String getParam() {
            return this.param;
        }

        public final PaymentMethod getPaymentMethod() {
            return this.paymentMethod;
        }

        public final Type getType() {
            return this.type;
        }

        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\u000e\n\u0002\b\f\b\u0001\u0018\u0000 \u000e2\b\u0012\u0004\u0012\u00020\u00000\u0001:\u0001\u000eB\u000f\b\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006j\u0002\b\u0007j\u0002\b\bj\u0002\b\tj\u0002\b\nj\u0002\b\u000bj\u0002\b\fj\u0002\b\r¨\u0006\u000f"}, d2 = {"Lcom/stripe/android/model/PaymentIntent$Error$Type;", "", "code", "", "(Ljava/lang/String;ILjava/lang/String;)V", "getCode", "()Ljava/lang/String;", "ApiConnectionError", "ApiError", "AuthenticationError", "CardError", "IdempotencyError", "InvalidRequestError", "RateLimitError", "Companion", "stripe_release"}, k = 1, mv = {1, 1, 16})
        /* compiled from: PaymentIntent.kt */
        public enum Type {
            ApiConnectionError("api_connection_error"),
            ApiError("api_error"),
            AuthenticationError("authentication_error"),
            CardError("card_error"),
            IdempotencyError("idempotency_error"),
            InvalidRequestError("invalid_request_error"),
            RateLimitError("rate_limit_error");
            
            public static final Companion Companion = null;
            private final String code;

            private Type(String str) {
                this.code = str;
            }

            public final String getCode() {
                return this.code;
            }

            static {
                Companion = new Companion((DefaultConstructorMarker) null);
            }

            @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0019\u0010\u0003\u001a\u0004\u0018\u00010\u00042\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006H\u0000¢\u0006\u0002\b\u0007¨\u0006\b"}, d2 = {"Lcom/stripe/android/model/PaymentIntent$Error$Type$Companion;", "", "()V", "fromCode", "Lcom/stripe/android/model/PaymentIntent$Error$Type;", "typeCode", "", "fromCode$stripe_release", "stripe_release"}, k = 1, mv = {1, 1, 16})
            /* compiled from: PaymentIntent.kt */
            public static final class Companion {
                private Companion() {
                }

                public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                    this();
                }

                public final Type fromCode$stripe_release(String str) {
                    for (Type type : Type.values()) {
                        if (Intrinsics.areEqual((Object) type.getCode(), (Object) str)) {
                            return type;
                        }
                    }
                    return null;
                }
            }
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0012\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\b\b\u0018\u00002\u00020\u0001B=\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u0005\u0012\n\b\u0002\u0010\u0007\u001a\u0004\u0018\u00010\u0005\u0012\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\u0005¢\u0006\u0002\u0010\tJ\t\u0010\u0011\u001a\u00020\u0003HÆ\u0003J\u000b\u0010\u0012\u001a\u0004\u0018\u00010\u0005HÆ\u0003J\u000b\u0010\u0013\u001a\u0004\u0018\u00010\u0005HÆ\u0003J\u000b\u0010\u0014\u001a\u0004\u0018\u00010\u0005HÆ\u0003J\u000b\u0010\u0015\u001a\u0004\u0018\u00010\u0005HÆ\u0003JC\u0010\u0016\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\u0007\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\u0005HÆ\u0001J\t\u0010\u0017\u001a\u00020\u0018HÖ\u0001J\u0013\u0010\u0019\u001a\u00020\u001a2\b\u0010\u001b\u001a\u0004\u0018\u00010\u001cHÖ\u0003J\t\u0010\u001d\u001a\u00020\u0018HÖ\u0001J\t\u0010\u001e\u001a\u00020\u0005HÖ\u0001J\u0019\u0010\u001f\u001a\u00020 2\u0006\u0010!\u001a\u00020\"2\u0006\u0010#\u001a\u00020\u0018HÖ\u0001R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u0013\u0010\u0004\u001a\u0004\u0018\u00010\u0005¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\rR\u0013\u0010\u0006\u001a\u0004\u0018\u00010\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\rR\u0013\u0010\u0007\u001a\u0004\u0018\u00010\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\rR\u0013\u0010\b\u001a\u0004\u0018\u00010\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\r¨\u0006$"}, d2 = {"Lcom/stripe/android/model/PaymentIntent$Shipping;", "Lcom/stripe/android/model/StripeModel;", "address", "Lcom/stripe/android/model/Address;", "carrier", "", "name", "phone", "trackingNumber", "(Lcom/stripe/android/model/Address;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", "getAddress", "()Lcom/stripe/android/model/Address;", "getCarrier", "()Ljava/lang/String;", "getName", "getPhone", "getTrackingNumber", "component1", "component2", "component3", "component4", "component5", "copy", "describeContents", "", "equals", "", "other", "", "hashCode", "toString", "writeToParcel", "", "parcel", "Landroid/os/Parcel;", "flags", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: PaymentIntent.kt */
    public static final class Shipping implements StripeModel {
        public static final Parcelable.Creator CREATOR = new Creator();
        private final Address address;
        private final String carrier;
        private final String name;
        private final String phone;
        private final String trackingNumber;

        @Metadata(bv = {1, 0, 3}, k = 3, mv = {1, 1, 16})
        public static class Creator implements Parcelable.Creator {
            public final Object createFromParcel(Parcel parcel) {
                Intrinsics.checkParameterIsNotNull(parcel, "in");
                return new Shipping((Address) Address.CREATOR.createFromParcel(parcel), parcel.readString(), parcel.readString(), parcel.readString(), parcel.readString());
            }

            public final Object[] newArray(int i) {
                return new Shipping[i];
            }
        }

        public static /* synthetic */ Shipping copy$default(Shipping shipping, Address address2, String str, String str2, String str3, String str4, int i, Object obj) {
            if ((i & 1) != 0) {
                address2 = shipping.address;
            }
            if ((i & 2) != 0) {
                str = shipping.carrier;
            }
            String str5 = str;
            if ((i & 4) != 0) {
                str2 = shipping.name;
            }
            String str6 = str2;
            if ((i & 8) != 0) {
                str3 = shipping.phone;
            }
            String str7 = str3;
            if ((i & 16) != 0) {
                str4 = shipping.trackingNumber;
            }
            return shipping.copy(address2, str5, str6, str7, str4);
        }

        public final Address component1() {
            return this.address;
        }

        public final String component2() {
            return this.carrier;
        }

        public final String component3() {
            return this.name;
        }

        public final String component4() {
            return this.phone;
        }

        public final String component5() {
            return this.trackingNumber;
        }

        public final Shipping copy(Address address2, String str, String str2, String str3, String str4) {
            Intrinsics.checkParameterIsNotNull(address2, PaymentMethod.BillingDetails.PARAM_ADDRESS);
            return new Shipping(address2, str, str2, str3, str4);
        }

        public int describeContents() {
            return 0;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Shipping)) {
                return false;
            }
            Shipping shipping = (Shipping) obj;
            return Intrinsics.areEqual((Object) this.address, (Object) shipping.address) && Intrinsics.areEqual((Object) this.carrier, (Object) shipping.carrier) && Intrinsics.areEqual((Object) this.name, (Object) shipping.name) && Intrinsics.areEqual((Object) this.phone, (Object) shipping.phone) && Intrinsics.areEqual((Object) this.trackingNumber, (Object) shipping.trackingNumber);
        }

        public int hashCode() {
            Address address2 = this.address;
            int i = 0;
            int hashCode = (address2 != null ? address2.hashCode() : 0) * 31;
            String str = this.carrier;
            int hashCode2 = (hashCode + (str != null ? str.hashCode() : 0)) * 31;
            String str2 = this.name;
            int hashCode3 = (hashCode2 + (str2 != null ? str2.hashCode() : 0)) * 31;
            String str3 = this.phone;
            int hashCode4 = (hashCode3 + (str3 != null ? str3.hashCode() : 0)) * 31;
            String str4 = this.trackingNumber;
            if (str4 != null) {
                i = str4.hashCode();
            }
            return hashCode4 + i;
        }

        public String toString() {
            return "Shipping(address=" + this.address + ", carrier=" + this.carrier + ", name=" + this.name + ", phone=" + this.phone + ", trackingNumber=" + this.trackingNumber + ")";
        }

        public void writeToParcel(Parcel parcel, int i) {
            Intrinsics.checkParameterIsNotNull(parcel, "parcel");
            this.address.writeToParcel(parcel, 0);
            parcel.writeString(this.carrier);
            parcel.writeString(this.name);
            parcel.writeString(this.phone);
            parcel.writeString(this.trackingNumber);
        }

        public Shipping(Address address2, String str, String str2, String str3, String str4) {
            Intrinsics.checkParameterIsNotNull(address2, PaymentMethod.BillingDetails.PARAM_ADDRESS);
            this.address = address2;
            this.carrier = str;
            this.name = str2;
            this.phone = str3;
            this.trackingNumber = str4;
        }

        public final Address getAddress() {
            return this.address;
        }

        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ Shipping(Address address2, String str, String str2, String str3, String str4, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this(address2, (i & 2) != 0 ? null : str, (i & 4) != 0 ? null : str2, (i & 8) != 0 ? null : str3, (i & 16) != 0 ? null : str4);
        }

        public final String getCarrier() {
            return this.carrier;
        }

        public final String getName() {
            return this.name;
        }

        public final String getPhone() {
            return this.phone;
        }

        public final String getTrackingNumber() {
            return this.trackingNumber;
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b\t\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\b\b\u0018\u0000 \u00122\u00020\u0001:\u0001\u0012B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u000e\u0010\t\u001a\u00020\u0003HÀ\u0003¢\u0006\u0002\b\nJ\u0013\u0010\u000b\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u0003HÆ\u0001J\u0013\u0010\f\u001a\u00020\r2\b\u0010\u000e\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u000f\u001a\u00020\u0010HÖ\u0001J\t\u0010\u0011\u001a\u00020\u0003HÖ\u0001R\u0014\u0010\u0005\u001a\u00020\u0003X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\u0007R\u0014\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\u0007¨\u0006\u0013"}, d2 = {"Lcom/stripe/android/model/PaymentIntent$ClientSecret;", "", "value", "", "(Ljava/lang/String;)V", "paymentIntentId", "getPaymentIntentId$stripe_release", "()Ljava/lang/String;", "getValue$stripe_release", "component1", "component1$stripe_release", "copy", "equals", "", "other", "hashCode", "", "toString", "Companion", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: PaymentIntent.kt */
    public static final class ClientSecret {
        @Deprecated
        public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
        private static final Pattern PATTERN = Pattern.compile("^pi_[^_]+_secret_[^_]+$");
        private final String paymentIntentId;
        private final String value;

        public static /* synthetic */ ClientSecret copy$default(ClientSecret clientSecret, String str, int i, Object obj) {
            if ((i & 1) != 0) {
                str = clientSecret.value;
            }
            return clientSecret.copy(str);
        }

        public final String component1$stripe_release() {
            return this.value;
        }

        public final ClientSecret copy(String str) {
            Intrinsics.checkParameterIsNotNull(str, "value");
            return new ClientSecret(str);
        }

        public boolean equals(Object obj) {
            if (this != obj) {
                return (obj instanceof ClientSecret) && Intrinsics.areEqual((Object) this.value, (Object) ((ClientSecret) obj).value);
            }
            return true;
        }

        public int hashCode() {
            String str = this.value;
            if (str != null) {
                return str.hashCode();
            }
            return 0;
        }

        public String toString() {
            return "ClientSecret(value=" + this.value + ")";
        }

        /* JADX WARNING: Removed duplicated region for block: B:14:0x005b  */
        /* JADX WARNING: Removed duplicated region for block: B:19:0x0091  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public ClientSecret(java.lang.String r5) {
            /*
                r4 = this;
                java.lang.String r0 = "value"
                kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(r5, r0)
                r4.<init>()
                r4.value = r5
                java.lang.CharSequence r5 = (java.lang.CharSequence) r5
                kotlin.text.Regex r0 = new kotlin.text.Regex
                java.lang.String r1 = "_secret"
                r0.<init>((java.lang.String) r1)
                r1 = 0
                java.util.List r5 = r0.split(r5, r1)
                boolean r0 = r5.isEmpty()
                if (r0 != 0) goto L_0x004d
                int r0 = r5.size()
                java.util.ListIterator r0 = r5.listIterator(r0)
            L_0x0027:
                boolean r2 = r0.hasPrevious()
                if (r2 == 0) goto L_0x004d
                java.lang.Object r2 = r0.previous()
                java.lang.String r2 = (java.lang.String) r2
                java.lang.CharSequence r2 = (java.lang.CharSequence) r2
                int r2 = r2.length()
                r3 = 1
                if (r2 != 0) goto L_0x003e
                r2 = 1
                goto L_0x003f
            L_0x003e:
                r2 = 0
            L_0x003f:
                if (r2 != 0) goto L_0x0027
                java.lang.Iterable r5 = (java.lang.Iterable) r5
                int r0 = r0.nextIndex()
                int r0 = r0 + r3
                java.util.List r5 = kotlin.collections.CollectionsKt.take(r5, r0)
                goto L_0x0051
            L_0x004d:
                java.util.List r5 = kotlin.collections.CollectionsKt.emptyList()
            L_0x0051:
                java.util.Collection r5 = (java.util.Collection) r5
                java.lang.String[] r0 = new java.lang.String[r1]
                java.lang.Object[] r5 = r5.toArray(r0)
                if (r5 == 0) goto L_0x0091
                java.lang.String[] r5 = (java.lang.String[]) r5
                r5 = r5[r1]
                r4.paymentIntentId = r5
                java.util.regex.Pattern r5 = PATTERN
                java.lang.String r0 = r4.value
                java.lang.CharSequence r0 = (java.lang.CharSequence) r0
                java.util.regex.Matcher r5 = r5.matcher(r0)
                boolean r5 = r5.matches()
                if (r5 == 0) goto L_0x0072
                return
            L_0x0072:
                java.lang.StringBuilder r5 = new java.lang.StringBuilder
                r5.<init>()
                java.lang.String r0 = "Invalid client secret: "
                r5.append(r0)
                java.lang.String r0 = r4.value
                r5.append(r0)
                java.lang.String r5 = r5.toString()
                java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
                java.lang.String r5 = r5.toString()
                r0.<init>(r5)
                java.lang.Throwable r0 = (java.lang.Throwable) r0
                throw r0
            L_0x0091:
                kotlin.TypeCastException r5 = new kotlin.TypeCastException
                java.lang.String r0 = "null cannot be cast to non-null type kotlin.Array<T>"
                r5.<init>(r0)
                throw r5
            */
            throw new UnsupportedOperationException("Method not decompiled: com.stripe.android.model.PaymentIntent.ClientSecret.<init>(java.lang.String):void");
        }

        public final String getValue$stripe_release() {
            return this.value;
        }

        public final String getPaymentIntentId$stripe_release() {
            return this.paymentIntentId;
        }

        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0016\u0010\u0003\u001a\n \u0005*\u0004\u0018\u00010\u00040\u0004X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0006"}, d2 = {"Lcom/stripe/android/model/PaymentIntent$ClientSecret$Companion;", "", "()V", "PATTERN", "Ljava/util/regex/Pattern;", "kotlin.jvm.PlatformType", "stripe_release"}, k = 1, mv = {1, 1, 16})
        /* compiled from: PaymentIntent.kt */
        private static final class Companion {
            private Companion() {
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\u000e\n\u0002\b\n\b\u0001\u0018\u0000 \f2\b\u0012\u0004\u0012\u00020\u00000\u0001:\u0001\fB\u000f\b\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000j\u0002\b\u0005j\u0002\b\u0006j\u0002\b\u0007j\u0002\b\bj\u0002\b\tj\u0002\b\nj\u0002\b\u000b¨\u0006\r"}, d2 = {"Lcom/stripe/android/model/PaymentIntent$CancellationReason;", "", "code", "", "(Ljava/lang/String;ILjava/lang/String;)V", "Duplicate", "Fraudulent", "RequestedByCustomer", "Abandoned", "FailedInvoice", "VoidInvoice", "Automatic", "Companion", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: PaymentIntent.kt */
    public enum CancellationReason {
        Duplicate("duplicate"),
        Fraudulent("fraudulent"),
        RequestedByCustomer("requested_by_customer"),
        Abandoned("abandoned"),
        FailedInvoice("failed_invoice"),
        VoidInvoice("void_invoice"),
        Automatic(AnalyticsEvents.PARAMETER_SHARE_DIALOG_SHOW_AUTOMATIC);
        
        public static final Companion Companion = null;
        /* access modifiers changed from: private */
        public final String code;

        private CancellationReason(String str) {
            this.code = str;
        }

        static {
            Companion = new Companion((DefaultConstructorMarker) null);
        }

        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0019\u0010\u0003\u001a\u0004\u0018\u00010\u00042\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006H\u0000¢\u0006\u0002\b\u0007¨\u0006\b"}, d2 = {"Lcom/stripe/android/model/PaymentIntent$CancellationReason$Companion;", "", "()V", "fromCode", "Lcom/stripe/android/model/PaymentIntent$CancellationReason;", "code", "", "fromCode$stripe_release", "stripe_release"}, k = 1, mv = {1, 1, 16})
        /* compiled from: PaymentIntent.kt */
        public static final class Companion {
            private Companion() {
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }

            public final CancellationReason fromCode$stripe_release(String str) {
                for (CancellationReason cancellationReason : CancellationReason.values()) {
                    if (Intrinsics.areEqual((Object) cancellationReason.code, (Object) str)) {
                        return cancellationReason;
                    }
                }
                return null;
            }
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0012\u0010\u0003\u001a\u0004\u0018\u00010\u00042\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006¨\u0006\u0007"}, d2 = {"Lcom/stripe/android/model/PaymentIntent$Companion;", "", "()V", "fromJson", "Lcom/stripe/android/model/PaymentIntent;", "jsonObject", "Lorg/json/JSONObject;", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: PaymentIntent.kt */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        public final PaymentIntent fromJson(JSONObject jSONObject) {
            if (jSONObject != null) {
                return new PaymentIntentJsonParser().parse(jSONObject);
            }
            return null;
        }
    }
}
