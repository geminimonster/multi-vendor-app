package com.stripe.android.model;

import android.os.Parcel;
import android.os.Parcelable;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u001b\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\b\b\u0018\u00002\u00020\u0001BW\b\u0000\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\b\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\b\u0010\u0006\u001a\u0004\u0018\u00010\u0005\u0012\b\u0010\u0007\u001a\u0004\u0018\u00010\u0005\u0012\b\u0010\b\u001a\u0004\u0018\u00010\u0003\u0012\b\u0010\t\u001a\u0004\u0018\u00010\u0005\u0012\b\u0010\n\u001a\u0004\u0018\u00010\u0005\u0012\b\u0010\u000b\u001a\u0004\u0018\u00010\u0005¢\u0006\u0002\u0010\fJ\u000b\u0010\u0017\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000b\u0010\u0018\u001a\u0004\u0018\u00010\u0005HÆ\u0003J\u000b\u0010\u0019\u001a\u0004\u0018\u00010\u0005HÆ\u0003J\u000b\u0010\u001a\u001a\u0004\u0018\u00010\u0005HÆ\u0003J\u000b\u0010\u001b\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000b\u0010\u001c\u001a\u0004\u0018\u00010\u0005HÆ\u0003J\u000b\u0010\u001d\u001a\u0004\u0018\u00010\u0005HÆ\u0003J\u000b\u0010\u001e\u001a\u0004\u0018\u00010\u0005HÆ\u0003Ji\u0010\u001f\u001a\u00020\u00002\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\u0007\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\t\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\n\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\u000b\u001a\u0004\u0018\u00010\u0005HÆ\u0001J\t\u0010 \u001a\u00020!HÖ\u0001J\u0013\u0010\"\u001a\u00020#2\b\u0010$\u001a\u0004\u0018\u00010%HÖ\u0003J\t\u0010&\u001a\u00020!HÖ\u0001J\t\u0010'\u001a\u00020\u0005HÖ\u0001J\u0019\u0010(\u001a\u00020)2\u0006\u0010*\u001a\u00020+2\u0006\u0010,\u001a\u00020!HÖ\u0001R\u0013\u0010\u0002\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000eR\u0013\u0010\u0004\u001a\u0004\u0018\u00010\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010R\u0013\u0010\u0006\u001a\u0004\u0018\u00010\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u0010R\u0013\u0010\u0007\u001a\u0004\u0018\u00010\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u0010R\u0013\u0010\b\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u000eR\u0013\u0010\t\u001a\u0004\u0018\u00010\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0014\u0010\u0010R\u0013\u0010\n\u001a\u0004\u0018\u00010\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0015\u0010\u0010R\u0013\u0010\u000b\u001a\u0004\u0018\u00010\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0016\u0010\u0010¨\u0006-"}, d2 = {"Lcom/stripe/android/model/SourceOwner;", "Lcom/stripe/android/model/StripeModel;", "address", "Lcom/stripe/android/model/Address;", "email", "", "name", "phone", "verifiedAddress", "verifiedEmail", "verifiedName", "verifiedPhone", "(Lcom/stripe/android/model/Address;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/stripe/android/model/Address;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", "getAddress", "()Lcom/stripe/android/model/Address;", "getEmail", "()Ljava/lang/String;", "getName", "getPhone", "getVerifiedAddress", "getVerifiedEmail", "getVerifiedName", "getVerifiedPhone", "component1", "component2", "component3", "component4", "component5", "component6", "component7", "component8", "copy", "describeContents", "", "equals", "", "other", "", "hashCode", "toString", "writeToParcel", "", "parcel", "Landroid/os/Parcel;", "flags", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: SourceOwner.kt */
public final class SourceOwner implements StripeModel {
    public static final Parcelable.Creator CREATOR = new Creator();
    private final Address address;
    private final String email;
    private final String name;
    private final String phone;
    private final Address verifiedAddress;
    private final String verifiedEmail;
    private final String verifiedName;
    private final String verifiedPhone;

    @Metadata(bv = {1, 0, 3}, k = 3, mv = {1, 1, 16})
    public static class Creator implements Parcelable.Creator {
        public final Object createFromParcel(Parcel parcel) {
            Intrinsics.checkParameterIsNotNull(parcel, "in");
            return new SourceOwner(parcel.readInt() != 0 ? (Address) Address.CREATOR.createFromParcel(parcel) : null, parcel.readString(), parcel.readString(), parcel.readString(), parcel.readInt() != 0 ? (Address) Address.CREATOR.createFromParcel(parcel) : null, parcel.readString(), parcel.readString(), parcel.readString());
        }

        public final Object[] newArray(int i) {
            return new SourceOwner[i];
        }
    }

    public static /* synthetic */ SourceOwner copy$default(SourceOwner sourceOwner, Address address2, String str, String str2, String str3, Address address3, String str4, String str5, String str6, int i, Object obj) {
        SourceOwner sourceOwner2 = sourceOwner;
        int i2 = i;
        return sourceOwner.copy((i2 & 1) != 0 ? sourceOwner2.address : address2, (i2 & 2) != 0 ? sourceOwner2.email : str, (i2 & 4) != 0 ? sourceOwner2.name : str2, (i2 & 8) != 0 ? sourceOwner2.phone : str3, (i2 & 16) != 0 ? sourceOwner2.verifiedAddress : address3, (i2 & 32) != 0 ? sourceOwner2.verifiedEmail : str4, (i2 & 64) != 0 ? sourceOwner2.verifiedName : str5, (i2 & 128) != 0 ? sourceOwner2.verifiedPhone : str6);
    }

    public final Address component1() {
        return this.address;
    }

    public final String component2() {
        return this.email;
    }

    public final String component3() {
        return this.name;
    }

    public final String component4() {
        return this.phone;
    }

    public final Address component5() {
        return this.verifiedAddress;
    }

    public final String component6() {
        return this.verifiedEmail;
    }

    public final String component7() {
        return this.verifiedName;
    }

    public final String component8() {
        return this.verifiedPhone;
    }

    public final SourceOwner copy(Address address2, String str, String str2, String str3, Address address3, String str4, String str5, String str6) {
        return new SourceOwner(address2, str, str2, str3, address3, str4, str5, str6);
    }

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof SourceOwner)) {
            return false;
        }
        SourceOwner sourceOwner = (SourceOwner) obj;
        return Intrinsics.areEqual((Object) this.address, (Object) sourceOwner.address) && Intrinsics.areEqual((Object) this.email, (Object) sourceOwner.email) && Intrinsics.areEqual((Object) this.name, (Object) sourceOwner.name) && Intrinsics.areEqual((Object) this.phone, (Object) sourceOwner.phone) && Intrinsics.areEqual((Object) this.verifiedAddress, (Object) sourceOwner.verifiedAddress) && Intrinsics.areEqual((Object) this.verifiedEmail, (Object) sourceOwner.verifiedEmail) && Intrinsics.areEqual((Object) this.verifiedName, (Object) sourceOwner.verifiedName) && Intrinsics.areEqual((Object) this.verifiedPhone, (Object) sourceOwner.verifiedPhone);
    }

    public int hashCode() {
        Address address2 = this.address;
        int i = 0;
        int hashCode = (address2 != null ? address2.hashCode() : 0) * 31;
        String str = this.email;
        int hashCode2 = (hashCode + (str != null ? str.hashCode() : 0)) * 31;
        String str2 = this.name;
        int hashCode3 = (hashCode2 + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.phone;
        int hashCode4 = (hashCode3 + (str3 != null ? str3.hashCode() : 0)) * 31;
        Address address3 = this.verifiedAddress;
        int hashCode5 = (hashCode4 + (address3 != null ? address3.hashCode() : 0)) * 31;
        String str4 = this.verifiedEmail;
        int hashCode6 = (hashCode5 + (str4 != null ? str4.hashCode() : 0)) * 31;
        String str5 = this.verifiedName;
        int hashCode7 = (hashCode6 + (str5 != null ? str5.hashCode() : 0)) * 31;
        String str6 = this.verifiedPhone;
        if (str6 != null) {
            i = str6.hashCode();
        }
        return hashCode7 + i;
    }

    public String toString() {
        return "SourceOwner(address=" + this.address + ", email=" + this.email + ", name=" + this.name + ", phone=" + this.phone + ", verifiedAddress=" + this.verifiedAddress + ", verifiedEmail=" + this.verifiedEmail + ", verifiedName=" + this.verifiedName + ", verifiedPhone=" + this.verifiedPhone + ")";
    }

    public void writeToParcel(Parcel parcel, int i) {
        Intrinsics.checkParameterIsNotNull(parcel, "parcel");
        Address address2 = this.address;
        if (address2 != null) {
            parcel.writeInt(1);
            address2.writeToParcel(parcel, 0);
        } else {
            parcel.writeInt(0);
        }
        parcel.writeString(this.email);
        parcel.writeString(this.name);
        parcel.writeString(this.phone);
        Address address3 = this.verifiedAddress;
        if (address3 != null) {
            parcel.writeInt(1);
            address3.writeToParcel(parcel, 0);
        } else {
            parcel.writeInt(0);
        }
        parcel.writeString(this.verifiedEmail);
        parcel.writeString(this.verifiedName);
        parcel.writeString(this.verifiedPhone);
    }

    public SourceOwner(Address address2, String str, String str2, String str3, Address address3, String str4, String str5, String str6) {
        this.address = address2;
        this.email = str;
        this.name = str2;
        this.phone = str3;
        this.verifiedAddress = address3;
        this.verifiedEmail = str4;
        this.verifiedName = str5;
        this.verifiedPhone = str6;
    }

    public final Address getAddress() {
        return this.address;
    }

    public final String getEmail() {
        return this.email;
    }

    public final String getName() {
        return this.name;
    }

    public final String getPhone() {
        return this.phone;
    }

    public final Address getVerifiedAddress() {
        return this.verifiedAddress;
    }

    public final String getVerifiedEmail() {
        return this.verifiedEmail;
    }

    public final String getVerifiedName() {
        return this.verifiedName;
    }

    public final String getVerifiedPhone() {
        return this.verifiedPhone;
    }
}
