package com.stripe.android.model.parsers;

import com.stripe.android.model.SourceCodeVerification;
import com.stripe.android.model.StripeJsonUtils;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.json.JSONObject;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0000\u0018\u0000 \u00072\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0007B\u0005¢\u0006\u0002\u0010\u0003J\u0010\u0010\u0004\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0006H\u0016¨\u0006\b"}, d2 = {"Lcom/stripe/android/model/parsers/SourceCodeVerificationJsonParser;", "Lcom/stripe/android/model/parsers/ModelJsonParser;", "Lcom/stripe/android/model/SourceCodeVerification;", "()V", "parse", "json", "Lorg/json/JSONObject;", "Companion", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: SourceCodeVerificationJsonParser.kt */
public final class SourceCodeVerificationJsonParser implements ModelJsonParser<SourceCodeVerification> {
    @Deprecated
    public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
    private static final String FIELD_ATTEMPTS_REMAINING = "attempts_remaining";
    private static final String FIELD_STATUS = "status";
    private static final int INVALID_ATTEMPTS_REMAINING = -1;

    public SourceCodeVerification parse(JSONObject jSONObject) {
        Intrinsics.checkParameterIsNotNull(jSONObject, "json");
        return new SourceCodeVerification(jSONObject.optInt(FIELD_ATTEMPTS_REMAINING, -1), Companion.asStatus(StripeJsonUtils.optString(jSONObject, "status")));
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0014\u0010\b\u001a\u0004\u0018\u00010\u00042\b\u0010\t\u001a\u0004\u0018\u00010\u0004H\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007XT¢\u0006\u0002\n\u0000¨\u0006\n"}, d2 = {"Lcom/stripe/android/model/parsers/SourceCodeVerificationJsonParser$Companion;", "", "()V", "FIELD_ATTEMPTS_REMAINING", "", "FIELD_STATUS", "INVALID_ATTEMPTS_REMAINING", "", "asStatus", "stringStatus", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: SourceCodeVerificationJsonParser.kt */
    private static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        /* access modifiers changed from: private */
        public final String asStatus(String str) {
            if (str != null) {
                int hashCode = str.hashCode();
                if (hashCode != -1281977283) {
                    if (hashCode != -682587753) {
                        if (hashCode == 945734241 && str.equals("succeeded")) {
                            return "succeeded";
                        }
                    } else if (str.equals("pending")) {
                        return "pending";
                    }
                } else if (str.equals("failed")) {
                    return "failed";
                }
            }
            return null;
        }
    }
}
