package com.stripe.android.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
import kotlin.TuplesKt;
import kotlin.collections.CollectionsKt;
import kotlin.collections.IntIterator;
import kotlin.collections.MapsKt;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.internal.Intrinsics;
import kotlin.ranges.RangesKt;
import org.json.JSONArray;
import org.json.JSONObject;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000F\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010 \n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010$\n\u0000\n\u0002\u0018\u0002\n\u0002\b\f\n\u0002\u0010\u000b\n\u0002\b\t\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\t\n\u0002\b\u0006\bÀ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u001f\u0010\u0005\u001a\n\u0012\u0004\u0012\u00020\u0001\u0018\u00010\u00062\b\u0010\u0007\u001a\u0004\u0018\u00010\bH\u0000¢\u0006\u0002\b\tJ'\u0010\n\u001a\u0012\u0012\u0004\u0012\u00020\u0004\u0012\u0006\u0012\u0004\u0018\u00010\u0001\u0018\u00010\u000b2\b\u0010\f\u001a\u0004\u0018\u00010\rH\u0000¢\u0006\u0002\b\u000eJ%\u0010\u000f\u001a\u0010\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u000b2\b\u0010\f\u001a\u0004\u0018\u00010\rH\u0000¢\u0006\u0002\b\u0010J\u0018\u0010\u0011\u001a\u0004\u0018\u00010\b2\f\u0010\u0012\u001a\b\u0012\u0002\b\u0003\u0018\u00010\u0006H\u0002J#\u0010\u0013\u001a\u0004\u0018\u00010\r2\u0012\u0010\u0014\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0002\b\u0003\u0018\u00010\u000bH\u0000¢\u0006\u0002\b\u0015J\u0019\u0010\u0016\u001a\u0004\u0018\u00010\u00042\b\u0010\u0017\u001a\u0004\u0018\u00010\u0004H\u0000¢\u0006\u0002\b\u0018J\u001f\u0010\u0019\u001a\u00020\u001a2\u0006\u0010\f\u001a\u00020\r2\b\b\u0001\u0010\u001b\u001a\u00020\u0004H\u0000¢\u0006\u0002\b\u001cJ!\u0010\u001d\u001a\u0004\u0018\u00010\u00042\u0006\u0010\f\u001a\u00020\r2\b\b\u0001\u0010\u001b\u001a\u00020\u0004H\u0001¢\u0006\u0002\b\u001eJ!\u0010\u001f\u001a\u0004\u0018\u00010\u00042\u0006\u0010\f\u001a\u00020\r2\b\b\u0001\u0010\u001b\u001a\u00020\u0004H\u0001¢\u0006\u0002\b J-\u0010!\u001a\u0010\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u000b2\u0006\u0010\f\u001a\u00020\r2\b\b\u0001\u0010\u001b\u001a\u00020\u0004H\u0000¢\u0006\u0002\b\"J#\u0010#\u001a\u0004\u0018\u00010$2\u0006\u0010\f\u001a\u00020\r2\b\b\u0001\u0010\u001b\u001a\u00020\u0004H\u0000¢\u0006\u0004\b%\u0010&J#\u0010'\u001a\u0004\u0018\u00010(2\u0006\u0010\f\u001a\u00020\r2\b\b\u0001\u0010\u001b\u001a\u00020\u0004H\u0000¢\u0006\u0004\b)\u0010*J/\u0010+\u001a\u0012\u0012\u0004\u0012\u00020\u0004\u0012\u0006\u0012\u0004\u0018\u00010\u0001\u0018\u00010\u000b2\u0006\u0010\f\u001a\u00020\r2\b\b\u0001\u0010\u001b\u001a\u00020\u0004H\u0000¢\u0006\u0002\b,J\u001e\u0010-\u001a\u0004\u0018\u00010\u00042\b\u0010\f\u001a\u0004\u0018\u00010\r2\b\b\u0001\u0010\u001b\u001a\u00020\u0004H\u0007R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006."}, d2 = {"Lcom/stripe/android/model/StripeJsonUtils;", "", "()V", "NULL", "", "jsonArrayToList", "", "jsonArray", "Lorg/json/JSONArray;", "jsonArrayToList$stripe_release", "jsonObjectToMap", "", "jsonObject", "Lorg/json/JSONObject;", "jsonObjectToMap$stripe_release", "jsonObjectToStringMap", "jsonObjectToStringMap$stripe_release", "listToJsonArray", "values", "mapToJsonObject", "mapObject", "mapToJsonObject$stripe_release", "nullIfNullOrEmpty", "possibleNull", "nullIfNullOrEmpty$stripe_release", "optBoolean", "", "fieldName", "optBoolean$stripe_release", "optCountryCode", "optCountryCode$stripe_release", "optCurrency", "optCurrency$stripe_release", "optHash", "optHash$stripe_release", "optInteger", "", "optInteger$stripe_release", "(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/Integer;", "optLong", "", "optLong$stripe_release", "(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/Long;", "optMap", "optMap$stripe_release", "optString", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: StripeJsonUtils.kt */
public final class StripeJsonUtils {
    public static final StripeJsonUtils INSTANCE = new StripeJsonUtils();
    private static final String NULL = "null";

    private StripeJsonUtils() {
    }

    public final /* synthetic */ boolean optBoolean$stripe_release(JSONObject jSONObject, String str) {
        Intrinsics.checkParameterIsNotNull(jSONObject, "jsonObject");
        Intrinsics.checkParameterIsNotNull(str, "fieldName");
        return jSONObject.has(str) && jSONObject.optBoolean(str, false);
    }

    public final /* synthetic */ Integer optInteger$stripe_release(JSONObject jSONObject, String str) {
        Intrinsics.checkParameterIsNotNull(jSONObject, "jsonObject");
        Intrinsics.checkParameterIsNotNull(str, "fieldName");
        if (!jSONObject.has(str)) {
            return null;
        }
        return Integer.valueOf(jSONObject.optInt(str));
    }

    public final /* synthetic */ Long optLong$stripe_release(JSONObject jSONObject, String str) {
        Intrinsics.checkParameterIsNotNull(jSONObject, "jsonObject");
        Intrinsics.checkParameterIsNotNull(str, "fieldName");
        if (!jSONObject.has(str)) {
            return null;
        }
        return Long.valueOf(jSONObject.optLong(str));
    }

    @JvmStatic
    public static final String optString(JSONObject jSONObject, String str) {
        Intrinsics.checkParameterIsNotNull(str, "fieldName");
        return INSTANCE.nullIfNullOrEmpty$stripe_release(jSONObject != null ? jSONObject.optString(str) : null);
    }

    public final /* synthetic */ String optCountryCode$stripe_release(JSONObject jSONObject, String str) {
        Intrinsics.checkParameterIsNotNull(jSONObject, "jsonObject");
        Intrinsics.checkParameterIsNotNull(str, "fieldName");
        String nullIfNullOrEmpty$stripe_release = nullIfNullOrEmpty$stripe_release(jSONObject.optString(str));
        if (nullIfNullOrEmpty$stripe_release != null) {
            if (nullIfNullOrEmpty$stripe_release.length() == 2) {
                return nullIfNullOrEmpty$stripe_release;
            }
        }
        return null;
    }

    @JvmStatic
    public static final String optCurrency$stripe_release(JSONObject jSONObject, String str) {
        Intrinsics.checkParameterIsNotNull(jSONObject, "jsonObject");
        Intrinsics.checkParameterIsNotNull(str, "fieldName");
        String nullIfNullOrEmpty$stripe_release = INSTANCE.nullIfNullOrEmpty$stripe_release(jSONObject.optString(str));
        if (nullIfNullOrEmpty$stripe_release != null) {
            if (nullIfNullOrEmpty$stripe_release.length() == 3) {
                return nullIfNullOrEmpty$stripe_release;
            }
        }
        return null;
    }

    public final /* synthetic */ Map<String, Object> optMap$stripe_release(JSONObject jSONObject, String str) {
        Intrinsics.checkParameterIsNotNull(jSONObject, "jsonObject");
        Intrinsics.checkParameterIsNotNull(str, "fieldName");
        JSONObject optJSONObject = jSONObject.optJSONObject(str);
        if (optJSONObject != null) {
            return INSTANCE.jsonObjectToMap$stripe_release(optJSONObject);
        }
        return null;
    }

    public final /* synthetic */ Map<String, String> optHash$stripe_release(JSONObject jSONObject, String str) {
        Intrinsics.checkParameterIsNotNull(jSONObject, "jsonObject");
        Intrinsics.checkParameterIsNotNull(str, "fieldName");
        JSONObject optJSONObject = jSONObject.optJSONObject(str);
        if (optJSONObject != null) {
            return INSTANCE.jsonObjectToStringMap$stripe_release(optJSONObject);
        }
        return null;
    }

    public final /* synthetic */ Map<String, Object> jsonObjectToMap$stripe_release(JSONObject jSONObject) {
        Map map;
        if (jSONObject == null) {
            return null;
        }
        JSONArray names = jSONObject.names();
        if (names == null) {
            names = new JSONArray();
        }
        Iterable until = RangesKt.until(0, names.length());
        Collection arrayList = new ArrayList(CollectionsKt.collectionSizeOrDefault(until, 10));
        Iterator it = until.iterator();
        while (it.hasNext()) {
            arrayList.add(names.getString(((IntIterator) it).nextInt()));
        }
        Collection arrayList2 = new ArrayList();
        for (String str : (List) arrayList) {
            Object opt = jSONObject.opt(str);
            if (opt == null || !(!Intrinsics.areEqual(opt, (Object) NULL))) {
                map = null;
            } else {
                if (opt instanceof JSONObject) {
                    opt = INSTANCE.jsonObjectToMap$stripe_release((JSONObject) opt);
                } else if (opt instanceof JSONArray) {
                    opt = INSTANCE.jsonArrayToList$stripe_release((JSONArray) opt);
                }
                map = MapsKt.mapOf(TuplesKt.to(str, opt));
            }
            if (map != null) {
                arrayList2.add(map);
            }
        }
        Map<String, Object> emptyMap = MapsKt.emptyMap();
        for (Map plus : (List) arrayList2) {
            emptyMap = MapsKt.plus(emptyMap, (Map<String, Object>) plus);
        }
        return emptyMap;
    }

    public final /* synthetic */ Map<String, String> jsonObjectToStringMap$stripe_release(JSONObject jSONObject) {
        if (jSONObject == null) {
            return null;
        }
        JSONArray names = jSONObject.names();
        if (names == null) {
            names = new JSONArray();
        }
        Iterable until = RangesKt.until(0, names.length());
        Collection arrayList = new ArrayList(CollectionsKt.collectionSizeOrDefault(until, 10));
        Iterator it = until.iterator();
        while (it.hasNext()) {
            arrayList.add(names.getString(((IntIterator) it).nextInt()));
        }
        Collection arrayList2 = new ArrayList();
        for (String str : (List) arrayList) {
            Object opt = jSONObject.opt(str);
            Map mapOf = (opt == null || !(Intrinsics.areEqual(opt, (Object) NULL) ^ true)) ? null : MapsKt.mapOf(TuplesKt.to(str, opt.toString()));
            if (mapOf != null) {
                arrayList2.add(mapOf);
            }
        }
        Map<String, String> emptyMap = MapsKt.emptyMap();
        for (Map plus : (List) arrayList2) {
            emptyMap = MapsKt.plus(emptyMap, (Map<String, String>) plus);
        }
        return emptyMap;
    }

    public final /* synthetic */ List<Object> jsonArrayToList$stripe_release(JSONArray jSONArray) {
        if (jSONArray == null) {
            return null;
        }
        Iterable until = RangesKt.until(0, jSONArray.length());
        Collection arrayList = new ArrayList(CollectionsKt.collectionSizeOrDefault(until, 10));
        Iterator it = until.iterator();
        while (it.hasNext()) {
            arrayList.add(jSONArray.get(((IntIterator) it).nextInt()));
        }
        Collection arrayList2 = new ArrayList();
        for (Object next : (List) arrayList) {
            if (next instanceof JSONArray) {
                next = INSTANCE.jsonArrayToList$stripe_release((JSONArray) next);
            } else if (next instanceof JSONObject) {
                next = INSTANCE.jsonObjectToMap$stripe_release((JSONObject) next);
            } else if (Intrinsics.areEqual(next, (Object) NULL)) {
                next = null;
            }
            if (next != null) {
                arrayList2.add(next);
            }
        }
        return (List) arrayList2;
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final org.json.JSONObject mapToJsonObject$stripe_release(java.util.Map<java.lang.String, ?> r6) {
        /*
            r5 = this;
            if (r6 != 0) goto L_0x0004
            r6 = 0
            return r6
        L_0x0004:
            org.json.JSONObject r0 = new org.json.JSONObject
            r0.<init>()
            java.util.Set r1 = r6.keySet()
            java.util.Iterator r1 = r1.iterator()
        L_0x0011:
            boolean r2 = r1.hasNext()
            if (r2 == 0) goto L_0x0056
            java.lang.Object r2 = r1.next()
            java.lang.String r2 = (java.lang.String) r2
            java.lang.Object r3 = r6.get(r2)
            if (r3 == 0) goto L_0x0011
            boolean r4 = r3 instanceof java.util.Map     // Catch:{  }
            if (r4 == 0) goto L_0x0033
            java.util.Map r3 = (java.util.Map) r3     // Catch:{ JSONException -> 0x0031 }
            org.json.JSONObject r3 = r5.mapToJsonObject$stripe_release(r3)     // Catch:{ JSONException -> 0x0031 }
            r0.put(r2, r3)     // Catch:{ JSONException -> 0x0031 }
            goto L_0x0011
        L_0x0031:
            goto L_0x0011
        L_0x0033:
            boolean r4 = r3 instanceof java.util.List     // Catch:{  }
            if (r4 == 0) goto L_0x0041
            java.util.List r3 = (java.util.List) r3     // Catch:{  }
            org.json.JSONArray r3 = r5.listToJsonArray(r3)     // Catch:{  }
            r0.put(r2, r3)     // Catch:{  }
            goto L_0x0011
        L_0x0041:
            boolean r4 = r3 instanceof java.lang.Number     // Catch:{  }
            if (r4 != 0) goto L_0x0052
            boolean r4 = r3 instanceof java.lang.Boolean     // Catch:{  }
            if (r4 == 0) goto L_0x004a
            goto L_0x0052
        L_0x004a:
            java.lang.String r3 = r3.toString()     // Catch:{  }
            r0.put(r2, r3)     // Catch:{  }
            goto L_0x0011
        L_0x0052:
            r0.put(r2, r3)     // Catch:{  }
            goto L_0x0011
        L_0x0056:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.stripe.android.model.StripeJsonUtils.mapToJsonObject$stripe_release(java.util.Map):org.json.JSONObject");
    }

    private final JSONArray listToJsonArray(List<?> list) {
        if (list == null) {
            return null;
        }
        JSONArray jSONArray = new JSONArray();
        for (Object next : list) {
            if (next instanceof Map) {
                next = INSTANCE.mapToJsonObject$stripe_release((Map) next);
            } else if (next instanceof List) {
                next = INSTANCE.listToJsonArray((List) next);
            } else if (!(next instanceof Number) && !(next instanceof Boolean)) {
                next = String.valueOf(next);
            }
            jSONArray.put(next);
        }
        return jSONArray;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0019, code lost:
        if ((r5.length() == 0) != false) goto L_0x001b;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.String nullIfNullOrEmpty$stripe_release(java.lang.String r5) {
        /*
            r4 = this;
            r0 = 0
            if (r5 == 0) goto L_0x0021
            java.lang.String r1 = "null"
            boolean r1 = kotlin.jvm.internal.Intrinsics.areEqual((java.lang.Object) r1, (java.lang.Object) r5)
            r2 = 0
            r3 = 1
            if (r1 != 0) goto L_0x001b
            r1 = r5
            java.lang.CharSequence r1 = (java.lang.CharSequence) r1
            int r1 = r1.length()
            if (r1 != 0) goto L_0x0018
            r1 = 1
            goto L_0x0019
        L_0x0018:
            r1 = 0
        L_0x0019:
            if (r1 == 0) goto L_0x001c
        L_0x001b:
            r2 = 1
        L_0x001c:
            if (r2 != 0) goto L_0x001f
            goto L_0x0020
        L_0x001f:
            r5 = r0
        L_0x0020:
            r0 = r5
        L_0x0021:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.stripe.android.model.StripeJsonUtils.nullIfNullOrEmpty$stripe_release(java.lang.String):java.lang.String");
    }
}
