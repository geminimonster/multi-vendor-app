package com.stripe.android.model.parsers;

import com.stripe.android.model.BankAccount;
import com.stripe.android.model.Card;
import com.stripe.android.model.StripeJsonUtils;
import com.stripe.android.model.Token;
import java.util.Date;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.json.JSONObject;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0000\u0018\u0000 \u00072\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0007B\u0005¢\u0006\u0002\u0010\u0003J\u0012\u0010\u0004\u001a\u0004\u0018\u00010\u00022\u0006\u0010\u0005\u001a\u00020\u0006H\u0016¨\u0006\b"}, d2 = {"Lcom/stripe/android/model/parsers/TokenJsonParser;", "Lcom/stripe/android/model/parsers/ModelJsonParser;", "Lcom/stripe/android/model/Token;", "()V", "parse", "json", "Lorg/json/JSONObject;", "Companion", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: TokenJsonParser.kt */
public final class TokenJsonParser implements ModelJsonParser<Token> {
    @Deprecated
    public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
    private static final String FIELD_BANK_ACCOUNT = "bank_account";
    private static final String FIELD_CARD = "card";
    private static final String FIELD_CREATED = "created";
    private static final String FIELD_ID = "id";
    private static final String FIELD_LIVEMODE = "livemode";
    private static final String FIELD_TYPE = "type";
    private static final String FIELD_USED = "used";

    public Token parse(JSONObject jSONObject) {
        Intrinsics.checkParameterIsNotNull(jSONObject, "json");
        String optString = StripeJsonUtils.optString(jSONObject, "id");
        Long optLong$stripe_release = StripeJsonUtils.INSTANCE.optLong$stripe_release(jSONObject, FIELD_CREATED);
        String access$asTokenType = Companion.asTokenType(StripeJsonUtils.optString(jSONObject, "type"));
        if (optString == null || optLong$stripe_release == null) {
            return null;
        }
        boolean optBoolean$stripe_release = StripeJsonUtils.INSTANCE.optBoolean$stripe_release(jSONObject, FIELD_USED);
        boolean optBoolean$stripe_release2 = StripeJsonUtils.INSTANCE.optBoolean$stripe_release(jSONObject, FIELD_LIVEMODE);
        Date date = new Date(optLong$stripe_release.longValue() * ((long) 1000));
        if (Intrinsics.areEqual((Object) "bank_account", (Object) access$asTokenType)) {
            JSONObject optJSONObject = jSONObject.optJSONObject("bank_account");
            if (optJSONObject != null) {
                return new Token(optString, "bank_account", date, optBoolean$stripe_release2, optBoolean$stripe_release, new BankAccountJsonParser().parse(optJSONObject), (Card) null, 64, (DefaultConstructorMarker) null);
            }
            return null;
        } else if (Intrinsics.areEqual((Object) "card", (Object) access$asTokenType)) {
            JSONObject optJSONObject2 = jSONObject.optJSONObject("card");
            if (optJSONObject2 != null) {
                return new Token(optString, "card", date, optBoolean$stripe_release2, optBoolean$stripe_release, (BankAccount) null, new CardJsonParser().parse(optJSONObject2), 32, (DefaultConstructorMarker) null);
            }
            return null;
        } else if (Intrinsics.areEqual((Object) "pii", (Object) access$asTokenType) || Intrinsics.areEqual((Object) "account", (Object) access$asTokenType) || Intrinsics.areEqual((Object) "cvc_update", (Object) access$asTokenType) || Intrinsics.areEqual((Object) "person", (Object) access$asTokenType)) {
            return new Token(optString, access$asTokenType, date, optBoolean$stripe_release2, optBoolean$stripe_release, (BankAccount) null, (Card) null, 96, (DefaultConstructorMarker) null);
        } else {
            return null;
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\t\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0014\u0010\u000b\u001a\u0004\u0018\u00010\u00042\b\u0010\f\u001a\u0004\u0018\u00010\u0004H\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\r"}, d2 = {"Lcom/stripe/android/model/parsers/TokenJsonParser$Companion;", "", "()V", "FIELD_BANK_ACCOUNT", "", "FIELD_CARD", "FIELD_CREATED", "FIELD_ID", "FIELD_LIVEMODE", "FIELD_TYPE", "FIELD_USED", "asTokenType", "possibleTokenType", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: TokenJsonParser.kt */
    private static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        /* access modifiers changed from: private */
        public final String asTokenType(String str) {
            if (str != null) {
                switch (str.hashCode()) {
                    case -1825227990:
                        if (str.equals("bank_account")) {
                            return "bank_account";
                        }
                        break;
                    case -1177318867:
                        if (str.equals("account")) {
                            return "account";
                        }
                        break;
                    case -991716523:
                        if (str.equals("person")) {
                            return "person";
                        }
                        break;
                    case 110992:
                        if (str.equals("pii")) {
                            return "pii";
                        }
                        break;
                    case 3046160:
                        if (str.equals("card")) {
                            return "card";
                        }
                        break;
                    case 1802926488:
                        if (str.equals("cvc_update")) {
                            return "cvc_update";
                        }
                        break;
                }
            }
            return null;
        }
    }
}
