package com.stripe.android;

import androidx.browser.trusted.sharing.ShareTarget;
import com.stripe.android.exception.InvalidRequestException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.collections.CollectionsKt;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.text.Charsets;
import kotlin.text.StringsKt;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000R\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0010\u0012\n\u0002\b\u0003\n\u0002\u0010$\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\b \u0018\u0000 02\u00020\u0001:\u0003012B\u0005¢\u0006\u0002\u0010\u0002J\u0015\u0010+\u001a\u00020,2\u0006\u0010-\u001a\u00020.H\u0010¢\u0006\u0002\b/R\u0012\u0010\u0003\u001a\u00020\u0004X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0005\u0010\u0006R\u0016\u0010\u0007\u001a\u0004\u0018\u00010\u0004X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\u0006R\u0016\u0010\t\u001a\u0004\u0018\u00010\n8BX\u0004¢\u0006\u0006\u001a\u0004\b\u000b\u0010\fR \u0010\r\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0002\b\u0003\u0018\u00010\u000e8@X\u0004¢\u0006\u0006\u001a\u0004\b\u000f\u0010\u0010R\u0014\u0010\u0011\u001a\u00020\u00048PX\u0004¢\u0006\u0006\u001a\u0004\b\u0012\u0010\u0006R \u0010\u0013\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00040\u000e8@X\u0004¢\u0006\u0006\u001a\u0004\b\u0014\u0010\u0010R\u0012\u0010\u0015\u001a\u00020\u0016X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0017\u0010\u0018R\u0012\u0010\u0019\u001a\u00020\u001aX¦\u0004¢\u0006\u0006\u001a\u0004\b\u001b\u0010\u001cR\u0012\u0010\u001d\u001a\u00020\u001eX¦\u0004¢\u0006\u0006\u001a\u0004\b\u001f\u0010 R\u001e\u0010!\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0002\b\u0003\u0018\u00010\u000eX¦\u0004¢\u0006\u0006\u001a\u0004\b\"\u0010\u0010R\u0014\u0010#\u001a\u00020\u00048DX\u0004¢\u0006\u0006\u001a\u0004\b$\u0010\u0006R\u000e\u0010%\u001a\u00020&X\u0004¢\u0006\u0002\n\u0000R\u0014\u0010'\u001a\u00020\u00048@X\u0004¢\u0006\u0006\u001a\u0004\b(\u0010\u0006R\u0014\u0010)\u001a\u00020\u00048BX\u0004¢\u0006\u0006\u001a\u0004\b*\u0010\u0006¨\u00063"}, d2 = {"Lcom/stripe/android/StripeRequest;", "", "()V", "baseUrl", "", "getBaseUrl", "()Ljava/lang/String;", "body", "getBody", "bodyBytes", "", "getBodyBytes", "()[B", "compactParams", "", "getCompactParams$stripe_release", "()Ljava/util/Map;", "contentType", "getContentType$stripe_release", "headers", "getHeaders$stripe_release", "headersFactory", "Lcom/stripe/android/RequestHeadersFactory;", "getHeadersFactory", "()Lcom/stripe/android/RequestHeadersFactory;", "method", "Lcom/stripe/android/StripeRequest$Method;", "getMethod", "()Lcom/stripe/android/StripeRequest$Method;", "mimeType", "Lcom/stripe/android/StripeRequest$MimeType;", "getMimeType", "()Lcom/stripe/android/StripeRequest$MimeType;", "params", "getParams", "query", "getQuery", "queryStringFactory", "Lcom/stripe/android/QueryStringFactory;", "url", "getUrl$stripe_release", "urlWithQuery", "getUrlWithQuery", "writeBody", "", "outputStream", "Ljava/io/OutputStream;", "writeBody$stripe_release", "Companion", "Method", "MimeType", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: StripeRequest.kt */
public abstract class StripeRequest {
    private static final String CHARSET = Charsets.UTF_8.name();
    public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
    /* access modifiers changed from: private */
    public static final Function1<String, String> DEFAULT_SYSTEM_PROPERTY_SUPPLIER = StripeRequest$Companion$DEFAULT_SYSTEM_PROPERTY_SUPPLIER$1.INSTANCE;
    private final String body;
    private final QueryStringFactory queryStringFactory = new QueryStringFactory();

    public abstract String getBaseUrl();

    public abstract RequestHeadersFactory getHeadersFactory();

    public abstract Method getMethod();

    public abstract MimeType getMimeType();

    public abstract Map<String, ?> getParams();

    public final Map<String, ?> getCompactParams$stripe_release() {
        Map<String, ?> params = getParams();
        if (params != null) {
            return Companion.compactParams(params);
        }
        return null;
    }

    public final String getUrl$stripe_release() throws UnsupportedEncodingException, InvalidRequestException {
        if (Method.GET == getMethod()) {
            return getUrlWithQuery();
        }
        return getBaseUrl();
    }

    public String getContentType$stripe_release() {
        return getMimeType() + "; charset=" + CHARSET;
    }

    public final Map<String, String> getHeaders$stripe_release() {
        return getHeadersFactory().create();
    }

    public void writeBody$stripe_release(OutputStream outputStream) {
        Intrinsics.checkParameterIsNotNull(outputStream, "outputStream");
        byte[] bodyBytes = getBodyBytes();
        if (bodyBytes != null) {
            outputStream.write(bodyBytes);
            outputStream.flush();
        }
    }

    /* access modifiers changed from: protected */
    public String getBody() {
        return this.body;
    }

    private final byte[] getBodyBytes() {
        try {
            String body2 = getBody();
            if (body2 == null) {
                return null;
            }
            Charset charset = Charsets.UTF_8;
            if (body2 != null) {
                byte[] bytes = body2.getBytes(charset);
                Intrinsics.checkExpressionValueIsNotNull(bytes, "(this as java.lang.String).getBytes(charset)");
                return bytes;
            }
            throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
        } catch (UnsupportedEncodingException e) {
            throw new InvalidRequestException((StripeError) null, (String) null, 0, "Unable to encode parameters to " + Charsets.UTF_8.name() + ". " + "Please contact support@stripe.com for assistance.", e, 7, (DefaultConstructorMarker) null);
        }
    }

    /* access modifiers changed from: protected */
    public final String getQuery() {
        return this.queryStringFactory.create(getCompactParams$stripe_release());
    }

    private final String getUrlWithQuery() {
        String[] strArr = new String[2];
        strArr[0] = getBaseUrl();
        String query = getQuery();
        if (!(query.length() > 0)) {
            query = null;
        }
        strArr[1] = query;
        Iterable listOfNotNull = CollectionsKt.listOfNotNull((T[]) strArr);
        String str = "?";
        if (StringsKt.contains$default((CharSequence) getBaseUrl(), (CharSequence) str, false, 2, (Object) null)) {
            str = "&";
        }
        return CollectionsKt.joinToString$default(listOfNotNull, str, (CharSequence) null, (CharSequence) null, 0, (CharSequence) null, (Function1) null, 62, (Object) null);
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0007\b\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u000f\b\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006j\u0002\b\u0007j\u0002\b\bj\u0002\b\t¨\u0006\n"}, d2 = {"Lcom/stripe/android/StripeRequest$Method;", "", "code", "", "(Ljava/lang/String;ILjava/lang/String;)V", "getCode", "()Ljava/lang/String;", "GET", "POST", "DELETE", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: StripeRequest.kt */
    public enum Method {
        GET(ShareTarget.METHOD_GET),
        POST(ShareTarget.METHOD_POST),
        DELETE("DELETE");
        
        private final String code;

        private Method(String str) {
            this.code = str;
        }

        public final String getCode() {
            return this.code;
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\u000e\n\u0002\b\b\b\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u000f\b\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\b\u0010\u0007\u001a\u00020\u0003H\u0016R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006j\u0002\b\bj\u0002\b\tj\u0002\b\n¨\u0006\u000b"}, d2 = {"Lcom/stripe/android/StripeRequest$MimeType;", "", "code", "", "(Ljava/lang/String;ILjava/lang/String;)V", "getCode", "()Ljava/lang/String;", "toString", "Form", "MultipartForm", "Json", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: StripeRequest.kt */
    public enum MimeType {
        Form(ShareTarget.ENCODING_TYPE_URL_ENCODED),
        MultipartForm(ShareTarget.ENCODING_TYPE_MULTIPART),
        Json("application/json");
        
        private final String code;

        private MimeType(String str) {
            this.code = str;
        }

        public final String getCode() {
            return this.code;
        }

        public String toString() {
            return this.code;
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010$\n\u0002\b\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J&\u0010\n\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00010\u000b2\u0010\u0010\f\u001a\f\u0012\u0004\u0012\u00020\u0004\u0012\u0002\b\u00030\u000bH\u0002R\u0016\u0010\u0003\u001a\n \u0005*\u0004\u0018\u00010\u00040\u0004X\u0004¢\u0006\u0002\n\u0000R \u0010\u0006\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00040\u0007X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\t¨\u0006\r"}, d2 = {"Lcom/stripe/android/StripeRequest$Companion;", "", "()V", "CHARSET", "", "kotlin.jvm.PlatformType", "DEFAULT_SYSTEM_PROPERTY_SUPPLIER", "Lkotlin/Function1;", "getDEFAULT_SYSTEM_PROPERTY_SUPPLIER$stripe_release", "()Lkotlin/jvm/functions/Function1;", "compactParams", "", "params", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: StripeRequest.kt */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        public final Function1<String, String> getDEFAULT_SYSTEM_PROPERTY_SUPPLIER$stripe_release() {
            return StripeRequest.DEFAULT_SYSTEM_PROPERTY_SUPPLIER;
        }

        /* access modifiers changed from: private */
        public final Map<String, Object> compactParams(Map<String, ?> map) {
            HashMap hashMap = new HashMap(map);
            Iterator it = new HashSet(hashMap.keySet()).iterator();
            while (it.hasNext()) {
                String str = (String) it.next();
                Object obj = hashMap.get(str);
                if (obj instanceof CharSequence) {
                    if (((CharSequence) obj).length() == 0) {
                        hashMap.remove(str);
                    }
                } else if (obj instanceof Map) {
                    Intrinsics.checkExpressionValueIsNotNull(str, "key");
                    hashMap.put(str, compactParams((Map) obj));
                } else if (obj == null) {
                    hashMap.remove(str);
                }
            }
            return hashMap;
        }
    }
}
