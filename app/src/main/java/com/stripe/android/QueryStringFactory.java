package com.stripe.android;

import com.stripe.android.exception.InvalidRequestException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt;
import kotlin.jvm.internal.Intrinsics;
import kotlin.text.Charsets;
import okhttp3.HttpUrl;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010$\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0007\b\u0000\u0018\u00002\u00020\u0001:\u0001\u000fB\u0005¢\u0006\u0002\u0010\u0002J\u001a\u0010\u0003\u001a\u00020\u00042\u0012\u0010\u0005\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0002\b\u0003\u0018\u00010\u0006J\"\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\t0\b2\u0012\u0010\u0005\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0002\b\u0003\u0018\u00010\u0006H\u0002J\"\u0010\n\u001a\b\u0012\u0004\u0012\u00020\t0\b2\n\u0010\u0005\u001a\u0006\u0012\u0002\b\u00030\b2\u0006\u0010\u000b\u001a\u00020\u0004H\u0002J.\u0010\f\u001a\b\u0012\u0004\u0012\u00020\t0\b2\u0012\u0010\u0005\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0002\b\u0003\u0018\u00010\u00062\n\b\u0002\u0010\u000b\u001a\u0004\u0018\u00010\u0004H\u0002J \u0010\r\u001a\b\u0012\u0004\u0012\u00020\t0\b2\b\u0010\u000e\u001a\u0004\u0018\u00010\u00012\u0006\u0010\u000b\u001a\u00020\u0004H\u0002¨\u0006\u0010"}, d2 = {"Lcom/stripe/android/QueryStringFactory;", "", "()V", "create", "", "params", "", "flattenParams", "", "Lcom/stripe/android/QueryStringFactory$Parameter;", "flattenParamsList", "keyPrefix", "flattenParamsMap", "flattenParamsValue", "value", "Parameter", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: QueryStringFactory.kt */
public final class QueryStringFactory {
    public final String create(Map<String, ?> map) {
        return CollectionsKt.joinToString$default(flattenParams(map), "&", (CharSequence) null, (CharSequence) null, 0, (CharSequence) null, QueryStringFactory$create$1.INSTANCE, 30, (Object) null);
    }

    private final List<Parameter> flattenParams(Map<String, ?> map) throws InvalidRequestException {
        return flattenParamsMap$default(this, map, (String) null, 2, (Object) null);
    }

    private final List<Parameter> flattenParamsList(List<?> list, String str) throws InvalidRequestException {
        if (list.isEmpty()) {
            return CollectionsKt.listOf(new Parameter(str, ""));
        }
        String str2 = str + HttpUrl.PATH_SEGMENT_ENCODE_SET_URI;
        Collection arrayList = new ArrayList();
        for (Object flattenParamsValue : list) {
            CollectionsKt.addAll(arrayList, flattenParamsValue(flattenParamsValue, str2));
        }
        return (List) arrayList;
    }

    static /* synthetic */ List flattenParamsMap$default(QueryStringFactory queryStringFactory, Map map, String str, int i, Object obj) throws InvalidRequestException {
        if ((i & 2) != 0) {
            str = null;
        }
        return queryStringFactory.flattenParamsMap(map, str);
    }

    private final List<Parameter> flattenParamsValue(Object obj, String str) throws InvalidRequestException {
        Object obj2 = obj;
        String str2 = str;
        if (obj2 instanceof Map) {
            return flattenParamsMap((Map) obj2, str2);
        }
        if (obj2 instanceof List) {
            return flattenParamsList((List) obj2, str2);
        }
        if (Intrinsics.areEqual(obj2, (Object) "")) {
            throw new InvalidRequestException(new StripeError((String) null, (String) null, (String) null, str, (String) null, (String) null, (String) null, 119, (DefaultConstructorMarker) null), (String) null, 0, "You cannot set '" + str2 + "' to an empty string. We interpret empty strings as " + "null in requests. You may set '" + str2 + "' to null to delete the property.", (Throwable) null, 22, (DefaultConstructorMarker) null);
        } else if (obj2 == null) {
            return CollectionsKt.listOf(new Parameter(str2, ""));
        } else {
            return CollectionsKt.listOf(new Parameter(str2, obj.toString()));
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0004\b\b\u0018\u00002\u00020\u0001B\u0017\b\u0000\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003¢\u0006\u0002\u0010\u0005J\t\u0010\u0006\u001a\u00020\u0003HÂ\u0003J\t\u0010\u0007\u001a\u00020\u0003HÂ\u0003J\u001d\u0010\b\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u0003HÆ\u0001J\u0013\u0010\t\u001a\u00020\n2\b\u0010\u000b\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\f\u001a\u00020\rHÖ\u0001J\b\u0010\u000e\u001a\u00020\u0003H\u0016J\u0010\u0010\u000f\u001a\u00020\u00032\u0006\u0010\u0010\u001a\u00020\u0003H\u0002R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0011"}, d2 = {"Lcom/stripe/android/QueryStringFactory$Parameter;", "", "key", "", "value", "(Ljava/lang/String;Ljava/lang/String;)V", "component1", "component2", "copy", "equals", "", "other", "hashCode", "", "toString", "urlEncode", "str", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: QueryStringFactory.kt */
    private static final class Parameter {
        private final String key;
        private final String value;

        private final String component1() {
            return this.key;
        }

        private final String component2() {
            return this.value;
        }

        public static /* synthetic */ Parameter copy$default(Parameter parameter, String str, String str2, int i, Object obj) {
            if ((i & 1) != 0) {
                str = parameter.key;
            }
            if ((i & 2) != 0) {
                str2 = parameter.value;
            }
            return parameter.copy(str, str2);
        }

        public final Parameter copy(String str, String str2) {
            Intrinsics.checkParameterIsNotNull(str, "key");
            Intrinsics.checkParameterIsNotNull(str2, "value");
            return new Parameter(str, str2);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Parameter)) {
                return false;
            }
            Parameter parameter = (Parameter) obj;
            return Intrinsics.areEqual((Object) this.key, (Object) parameter.key) && Intrinsics.areEqual((Object) this.value, (Object) parameter.value);
        }

        public int hashCode() {
            String str = this.key;
            int i = 0;
            int hashCode = (str != null ? str.hashCode() : 0) * 31;
            String str2 = this.value;
            if (str2 != null) {
                i = str2.hashCode();
            }
            return hashCode + i;
        }

        public Parameter(String str, String str2) {
            Intrinsics.checkParameterIsNotNull(str, "key");
            Intrinsics.checkParameterIsNotNull(str2, "value");
            this.key = str;
            this.value = str2;
        }

        public String toString() {
            String urlEncode = urlEncode(this.key);
            String urlEncode2 = urlEncode(this.value);
            return urlEncode + '=' + urlEncode2;
        }

        private final String urlEncode(String str) throws UnsupportedEncodingException {
            String encode = URLEncoder.encode(str, Charsets.UTF_8.name());
            Intrinsics.checkExpressionValueIsNotNull(encode, "URLEncoder.encode(str, Charsets.UTF_8.name())");
            return encode;
        }
    }

    private final List<Parameter> flattenParamsMap(Map<String, ?> map, String str) throws InvalidRequestException {
        if (map == null) {
            return CollectionsKt.emptyList();
        }
        Collection arrayList = new ArrayList();
        for (Map.Entry next : map.entrySet()) {
            String str2 = (String) next.getKey();
            Object value = next.getValue();
            if (str != null) {
                String str3 = str + '[' + str2 + ']';
                if (str3 != null) {
                    str2 = str3;
                }
            }
            CollectionsKt.addAll(arrayList, flattenParamsValue(value, str2));
        }
        return (List) arrayList;
    }
}
