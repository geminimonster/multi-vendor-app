package com.stripe.android;

import com.stripe.android.model.CardBrand;
import kotlin.Metadata;
import kotlin.jvm.JvmStatic;
import kotlin.text.StringsKt;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0006\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0012\u0010\u0003\u001a\u00020\u00042\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006H\u0007J\u001a\u0010\u0003\u001a\u00020\u00042\b\u0010\u0005\u001a\u0004\u0018\u00010\u00062\u0006\u0010\u0007\u001a\u00020\bH\u0002J\u0017\u0010\t\u001a\u00020\b2\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006H\u0000¢\u0006\u0002\b\nJ\u0012\u0010\u000b\u001a\u00020\b2\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006H\u0007J\u0017\u0010\f\u001a\u00020\b2\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006H\u0000¢\u0006\u0002\b\r¨\u0006\u000e"}, d2 = {"Lcom/stripe/android/CardUtils;", "", "()V", "getPossibleCardBrand", "Lcom/stripe/android/model/CardBrand;", "cardNumber", "", "shouldNormalize", "", "isValidCardLength", "isValidCardLength$stripe_release", "isValidCardNumber", "isValidLuhnNumber", "isValidLuhnNumber$stripe_release", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: CardUtils.kt */
public final class CardUtils {
    public static final CardUtils INSTANCE = new CardUtils();

    private CardUtils() {
    }

    @JvmStatic
    public static final CardBrand getPossibleCardBrand(String str) {
        return INSTANCE.getPossibleCardBrand(str, true);
    }

    @JvmStatic
    public static final boolean isValidCardNumber(String str) {
        String removeSpacesAndHyphens = StripeTextUtils.removeSpacesAndHyphens(str);
        return INSTANCE.isValidLuhnNumber$stripe_release(removeSpacesAndHyphens) && INSTANCE.isValidCardLength$stripe_release(removeSpacesAndHyphens);
    }

    public final boolean isValidLuhnNumber$stripe_release(String str) {
        if (str == null) {
            return false;
        }
        int i = 0;
        boolean z = true;
        for (int length = str.length() - 1; length >= 0; length--) {
            char charAt = str.charAt(length);
            if (!Character.isDigit(charAt)) {
                return false;
            }
            int numericValue = Character.getNumericValue(charAt);
            z = !z;
            if (z) {
                numericValue *= 2;
            }
            if (numericValue > 9) {
                numericValue -= 9;
            }
            i += numericValue;
        }
        if (i % 10 == 0) {
            return true;
        }
        return false;
    }

    public final boolean isValidCardLength$stripe_release(String str) {
        return str != null && getPossibleCardBrand(str, false).isValidCardNumberLength(str);
    }

    private final CardBrand getPossibleCardBrand(String str, boolean z) {
        CharSequence charSequence = str;
        if (charSequence == null || StringsKt.isBlank(charSequence)) {
            return CardBrand.Unknown;
        }
        if (z) {
            str = StripeTextUtils.removeSpacesAndHyphens(str);
        }
        return CardBrand.Companion.fromCardNumber(str);
    }
}
