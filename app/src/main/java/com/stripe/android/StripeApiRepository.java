package com.stripe.android;

import android.content.Context;
import android.util.Pair;
import androidx.lifecycle.CoroutineLiveDataKt;
import androidx.lifecycle.LiveData;
import com.facebook.appevents.AppEventsConstants;
import com.facebook.internal.NativeProtocol;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.stripe.android.AnalyticsRequest;
import com.stripe.android.ApiRequest;
import com.stripe.android.exception.APIConnectionException;
import com.stripe.android.exception.APIException;
import com.stripe.android.exception.AuthenticationException;
import com.stripe.android.exception.CardException;
import com.stripe.android.exception.InvalidRequestException;
import com.stripe.android.exception.PermissionException;
import com.stripe.android.exception.RateLimitException;
import com.stripe.android.exception.StripeException;
import com.stripe.android.model.ConfirmPaymentIntentParams;
import com.stripe.android.model.ConfirmSetupIntentParams;
import com.stripe.android.model.Customer;
import com.stripe.android.model.FpxBankStatuses;
import com.stripe.android.model.ListPaymentMethodsParams;
import com.stripe.android.model.PaymentIntent;
import com.stripe.android.model.PaymentMethod;
import com.stripe.android.model.PaymentMethodCreateParams;
import com.stripe.android.model.SetupIntent;
import com.stripe.android.model.ShippingInformation;
import com.stripe.android.model.Source;
import com.stripe.android.model.SourceParams;
import com.stripe.android.model.Stripe3ds2AuthResult;
import com.stripe.android.model.StripeFile;
import com.stripe.android.model.StripeFileParams;
import com.stripe.android.model.StripeIntent;
import com.stripe.android.model.StripeModel;
import com.stripe.android.model.Token;
import com.stripe.android.model.TokenParams;
import com.stripe.android.model.parsers.CustomerJsonParser;
import com.stripe.android.model.parsers.ModelJsonParser;
import com.stripe.android.model.parsers.PaymentIntentJsonParser;
import com.stripe.android.model.parsers.PaymentMethodJsonParser;
import com.stripe.android.model.parsers.SetupIntentJsonParser;
import com.stripe.android.model.parsers.SourceJsonParser;
import com.stripe.android.model.parsers.Stripe3ds2AuthResultJsonParser;
import com.stripe.android.model.parsers.StripeFileJsonParser;
import com.stripe.android.model.parsers.TokenJsonParser;
import java.io.IOException;
import java.security.Security;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import kotlin.Metadata;
import kotlin.TuplesKt;
import kotlin.collections.CollectionsKt;
import kotlin.collections.IntIterator;
import kotlin.collections.MapsKt;
import kotlin.coroutines.Continuation;
import kotlin.coroutines.CoroutineContext;
import kotlin.coroutines.jvm.internal.Boxing;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.StringCompanionObject;
import kotlin.ranges.RangesKt;
import kotlin.text.StringsKt;
import kotlinx.coroutines.CoroutineScope;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000Þ\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\"\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u000b\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u000b\b\u0000\u0018\u0000 \u00012\u00020\u0001:\f\u0001\u0001\u0001\u0001\u0001\u0001B\u0001\b\u0001\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u0012\b\b\u0002\u0010\b\u001a\u00020\t\u0012\b\b\u0002\u0010\n\u001a\u00020\u000b\u0012\b\b\u0002\u0010\f\u001a\u00020\r\u0012\b\b\u0002\u0010\u000e\u001a\u00020\u000f\u0012\b\b\u0002\u0010\u0010\u001a\u00020\u0011\u0012\b\b\u0002\u0010\u0012\u001a\u00020\u0013\u0012\b\b\u0002\u0010\u0014\u001a\u00020\u0015\u0012\b\b\u0002\u0010\u0016\u001a\u00020\u0017\u0012\b\b\u0002\u0010\u0018\u001a\u00020\u0005\u0012\b\b\u0002\u0010\u0019\u001a\u00020\u0005¢\u0006\u0002\u0010\u001aJ@\u0010\"\u001a\u0004\u0018\u00010#2\u0006\u0010$\u001a\u00020\u00052\u0006\u0010\u0004\u001a\u00020\u00052\f\u0010%\u001a\b\u0012\u0004\u0012\u00020\u00050&2\u0006\u0010'\u001a\u00020\u00052\u0006\u0010(\u001a\u00020\u00052\u0006\u0010)\u001a\u00020*H\u0016J8\u0010+\u001a\u0004\u0018\u00010,2\u0006\u0010$\u001a\u00020\u00052\u0006\u0010\u0004\u001a\u00020\u00052\f\u0010%\u001a\b\u0012\u0004\u0012\u00020\u00050&2\u0006\u0010-\u001a\u00020\u00052\u0006\u0010)\u001a\u00020*H\u0016J.\u0010.\u001a\u00020/2\u0006\u00100\u001a\u0002012\u0006\u0010'\u001a\u00020\u00052\u0006\u00102\u001a\u00020*2\f\u00103\u001a\b\u0012\u0004\u0012\u00020104H\u0016J\"\u00105\u001a\u0004\u0018\u0001062\u0006\u00107\u001a\u00020\u00052\u0006\u0010'\u001a\u00020\u00052\u0006\u00102\u001a\u00020*H\u0016J\"\u00108\u001a\u0004\u0018\u0001092\u0006\u0010:\u001a\u00020\u00052\u0006\u0010'\u001a\u00020\u00052\u0006\u00102\u001a\u00020*H\u0016J\u001d\u0010;\u001a\u00020<2\u0006\u0010'\u001a\u00020\u00052\u0006\u0010)\u001a\u00020*H\u0001¢\u0006\u0002\b=J&\u0010;\u001a\u00020/2\u0006\u0010'\u001a\u00020\u00052\u0006\u0010)\u001a\u00020*2\f\u00103\u001a\b\u0012\u0004\u0012\u00020<04H\u0016J(\u0010>\u001a\u0004\u0018\u0001062\u0006\u0010?\u001a\u00020@2\u0006\u00102\u001a\u00020*2\f\u0010A\u001a\b\u0012\u0004\u0012\u00020\u00050BH\u0016J(\u0010C\u001a\u0004\u0018\u0001092\u0006\u0010D\u001a\u00020E2\u0006\u00102\u001a\u00020*2\f\u0010A\u001a\b\u0012\u0004\u0012\u00020\u00050BH\u0016J*\u0010F\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020H0G2\u0006\u0010I\u001a\u00020\u00052\f\u0010A\u001a\b\u0012\u0004\u0012\u00020\u00050BH\u0002J\u0018\u0010J\u001a\u00020K2\u0006\u0010L\u001a\u00020M2\u0006\u0010)\u001a\u00020*H\u0016J\u001a\u0010N\u001a\u0004\u0018\u00010,2\u0006\u0010O\u001a\u00020P2\u0006\u00102\u001a\u00020*H\u0016J\u001a\u0010Q\u001a\u0004\u0018\u00010#2\u0006\u0010R\u001a\u00020S2\u0006\u00102\u001a\u00020*H\u0016J\u001a\u0010T\u001a\u0004\u0018\u00010U2\u0006\u0010V\u001a\u00020W2\u0006\u00102\u001a\u00020*H\u0016J8\u0010X\u001a\u0004\u0018\u00010#2\u0006\u0010$\u001a\u00020\u00052\u0006\u0010\u0004\u001a\u00020\u00052\f\u0010%\u001a\b\u0012\u0004\u0012\u00020\u00050&2\u0006\u0010'\u001a\u00020\u00052\u0006\u0010)\u001a\u00020*H\u0016J0\u0010Y\u001a\u0004\u0018\u00010,2\u0006\u0010\u0004\u001a\u00020\u00052\f\u0010%\u001a\b\u0012\u0004\u0012\u00020\u00050&2\u0006\u0010-\u001a\u00020\u00052\u0006\u0010)\u001a\u00020*H\u0016J\u0014\u0010Z\u001a\u000e\u0012\u0004\u0012\u00020<\u0012\u0004\u0012\u00020\u00050[H\u0002J/\u0010\\\u001a\u0004\u0018\u0001H]\"\b\b\u0000\u0010]*\u00020^2\u0006\u0010_\u001a\u00020`2\f\u0010a\u001a\b\u0012\u0004\u0012\u0002H]0bH\u0002¢\u0006\u0002\u0010cJ\u0018\u0010d\u001a\u00020/2\u0006\u0010e\u001a\u00020f2\u0006\u0010\u0004\u001a\u00020\u0005H\u0002J)\u0010d\u001a\u00020/2\u0012\u0010g\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020H0G2\u0006\u0010\u0004\u001a\u00020\u0005H\u0001¢\u0006\u0002\bhJ\b\u0010i\u001a\u00020/H\u0002J\u0015\u0010j\u001a\u00020\u00052\u0006\u0010-\u001a\u00020\u0005H\u0001¢\u0006\u0002\bkJ\u001f\u0010l\u001a\b\u0012\u0004\u0012\u00020n0m2\u0006\u00102\u001a\u00020*H@ø\u0001\u0000¢\u0006\u0002\u0010oJ4\u0010p\u001a\b\u0012\u0004\u0012\u00020,0B2\u0006\u0010q\u001a\u00020r2\u0006\u0010\u0004\u001a\u00020\u00052\f\u0010%\u001a\b\u0012\u0004\u0012\u00020\u00050&2\u0006\u0010)\u001a\u00020*H\u0016J\u0010\u0010s\u001a\u00020/2\u0006\u0010t\u001a\u00020uH\u0002J\u0010\u0010v\u001a\u00020/2\u0006\u0010w\u001a\u00020xH\u0002J\u0015\u0010y\u001a\u00020u2\u0006\u0010_\u001a\u00020`H\u0001¢\u0006\u0002\bzJ\u0015\u0010{\u001a\u00020u2\u0006\u0010|\u001a\u00020}H\u0001¢\u0006\u0002\b~J\u001d\u0010\u001a\u00020/2\u0013\u0010\u0001\u001a\u000e\u0012\u0004\u0012\u00020<\u0012\u0004\u0012\u00020\u00050[H\u0002J*\u0010\u0001\u001a\u0005\u0018\u00010\u00012\u0006\u0010$\u001a\u00020\u00052\f\u0010%\u001a\b\u0012\u0004\u0012\u00020\u00050&2\u0006\u0010)\u001a\u00020*H\u0016J5\u0010\u0001\u001a\u00020/2\u0006\u0010I\u001a\u00020\u00052\u0006\u00102\u001a\u00020*2\f\u0010A\u001a\b\u0012\u0004\u0012\u00020\u00050B2\f\u00103\u001a\b\u0012\u0004\u0012\u00020104H\u0016J-\u0010\u0001\u001a\u00020\u00052\u0007\u0010\u0001\u001a\u00020\u00052\u0007\u0010\u0001\u001a\u00020\u00052\u0007\u0010\u0001\u001a\u00020\u00052\u0007\u0010\u0001\u001a\u00020\u0005H\u0016J)\u0010\u0001\u001a\u0004\u0018\u0001062\u0006\u0010I\u001a\u00020\u00052\u0006\u00102\u001a\u00020*2\f\u0010A\u001a\b\u0012\u0004\u0012\u00020\u00050BH\u0016J)\u0010\u0001\u001a\u0004\u0018\u0001092\u0006\u0010I\u001a\u00020\u00052\u0006\u00102\u001a\u00020*2\f\u0010A\u001a\b\u0012\u0004\u0012\u00020\u00050BH\u0016J#\u0010\u0001\u001a\u0004\u0018\u00010#2\u0006\u0010'\u001a\u00020\u00052\u0006\u0010I\u001a\u00020\u00052\u0006\u00102\u001a\u00020*H\u0016J/\u0010\u0001\u001a\u00020/2\u0006\u0010'\u001a\u00020\u00052\u0006\u0010I\u001a\u00020\u00052\u0006\u00102\u001a\u00020*2\f\u00103\u001a\b\u0012\u0004\u0012\u00020#04H\u0016J<\u0010\u0001\u001a\u0005\u0018\u00010\u00012\u0006\u0010$\u001a\u00020\u00052\u0006\u0010\u0004\u001a\u00020\u00052\f\u0010%\u001a\b\u0012\u0004\u0012\u00020\u00050&2\b\u0010\u0001\u001a\u00030\u00012\u0006\u0010)\u001a\u00020*H\u0016JB\u0010\u0001\u001a\u0005\u0018\u00010\u00012\u0006\u0010$\u001a\u00020\u00052\u0006\u0010\u0004\u001a\u00020\u00052\f\u0010%\u001a\b\u0012\u0004\u0012\u00020\u00050&2\u0006\u0010'\u001a\u00020\u00052\u0006\u0010(\u001a\u00020\u00052\u0006\u0010)\u001a\u00020*H\u0016J+\u0010\u0001\u001a\u00030\u00012\b\u0010\u0001\u001a\u00030\u00012\u0007\u0010\u0001\u001a\u00020\u00052\u0006\u0010)\u001a\u00020*H\u0001¢\u0006\u0003\b\u0001J3\u0010\u0001\u001a\u00020/2\b\u0010\u0001\u001a\u00030\u00012\u0007\u0010\u0001\u001a\u00020\u00052\u0006\u0010)\u001a\u00020*2\r\u00103\u001a\t\u0012\u0005\u0012\u00030\u000104H\u0016J6\u0010\u0001\u001a\u00020/2\u0007\u0010\u0001\u001a\u00020\u00052\u0007\u0010\u0001\u001a\u00020\u00052\u0007\u0010\u0001\u001a\u00020\u00052\u0007\u0010\u0001\u001a\u00020\u00052\u0007\u0010\u0001\u001a\u00020\u0005H\u0016R\u000e\u0010\u0012\u001a\u00020\u0013X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\rX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u001b\u001a\u00020\u001cX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u001d\u001a\u00020\u001eX\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u0006\u001a\u0004\u0018\u00010\u0007X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0004¢\u0006\u0002\n\u0000R\u0016\u0010\u001f\u001a\u0004\u0018\u00010\u00058BX\u0004¢\u0006\u0006\u001a\u0004\b \u0010!R\u000e\u0010\u0014\u001a\u00020\u0015X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\u0017X\u0004¢\u0006\u0002\n\u0000\u0002\u0004\n\u0002\b\u0019¨\u0006\u0001"}, d2 = {"Lcom/stripe/android/StripeApiRepository;", "Lcom/stripe/android/StripeRepository;", "context", "Landroid/content/Context;", "publishableKey", "", "appInfo", "Lcom/stripe/android/AppInfo;", "logger", "Lcom/stripe/android/Logger;", "stripeApiRequestExecutor", "Lcom/stripe/android/ApiRequestExecutor;", "analyticsRequestExecutor", "Lcom/stripe/android/AnalyticsRequestExecutor;", "fingerprintDataRepository", "Lcom/stripe/android/FingerprintDataRepository;", "apiFingerprintParamsFactory", "Lcom/stripe/android/ApiFingerprintParamsFactory;", "analyticsDataFactory", "Lcom/stripe/android/AnalyticsDataFactory;", "fingerprintParamsUtils", "Lcom/stripe/android/FingerprintParamsUtils;", "workContext", "Lkotlin/coroutines/CoroutineContext;", "apiVersion", "sdkVersion", "(Landroid/content/Context;Ljava/lang/String;Lcom/stripe/android/AppInfo;Lcom/stripe/android/Logger;Lcom/stripe/android/ApiRequestExecutor;Lcom/stripe/android/AnalyticsRequestExecutor;Lcom/stripe/android/FingerprintDataRepository;Lcom/stripe/android/ApiFingerprintParamsFactory;Lcom/stripe/android/AnalyticsDataFactory;Lcom/stripe/android/FingerprintParamsUtils;Lkotlin/coroutines/CoroutineContext;Ljava/lang/String;Ljava/lang/String;)V", "analyticsRequestFactory", "Lcom/stripe/android/AnalyticsRequest$Factory;", "apiRequestFactory", "Lcom/stripe/android/ApiRequest$Factory;", "fingerprintGuid", "getFingerprintGuid", "()Ljava/lang/String;", "addCustomerSource", "Lcom/stripe/android/model/Source;", "customerId", "productUsageTokens", "", "sourceId", "sourceType", "requestOptions", "Lcom/stripe/android/ApiRequest$Options;", "attachPaymentMethod", "Lcom/stripe/android/model/PaymentMethod;", "paymentMethodId", "cancelIntent", "", "stripeIntent", "Lcom/stripe/android/model/StripeIntent;", "options", "callback", "Lcom/stripe/android/ApiResultCallback;", "cancelPaymentIntentSource", "Lcom/stripe/android/model/PaymentIntent;", "paymentIntentId", "cancelSetupIntentSource", "Lcom/stripe/android/model/SetupIntent;", "setupIntentId", "complete3ds2Auth", "", "complete3ds2Auth$stripe_release", "confirmPaymentIntent", "confirmPaymentIntentParams", "Lcom/stripe/android/model/ConfirmPaymentIntentParams;", "expandFields", "", "confirmSetupIntent", "confirmSetupIntentParams", "Lcom/stripe/android/model/ConfirmSetupIntentParams;", "createClientSecretParam", "", "", "clientSecret", "createFile", "Lcom/stripe/android/model/StripeFile;", "fileParams", "Lcom/stripe/android/model/StripeFileParams;", "createPaymentMethod", "paymentMethodCreateParams", "Lcom/stripe/android/model/PaymentMethodCreateParams;", "createSource", "sourceParams", "Lcom/stripe/android/model/SourceParams;", "createToken", "Lcom/stripe/android/model/Token;", "tokenParams", "Lcom/stripe/android/model/TokenParams;", "deleteCustomerSource", "detachPaymentMethod", "disableDnsCache", "Landroid/util/Pair;", "fetchStripeModel", "ModelType", "Lcom/stripe/android/model/StripeModel;", "apiRequest", "Lcom/stripe/android/ApiRequest;", "jsonParser", "Lcom/stripe/android/model/parsers/ModelJsonParser;", "(Lcom/stripe/android/ApiRequest;Lcom/stripe/android/model/parsers/ModelJsonParser;)Lcom/stripe/android/model/StripeModel;", "fireAnalyticsRequest", "event", "Lcom/stripe/android/AnalyticsEvent;", "params", "fireAnalyticsRequest$stripe_release", "fireFingerprintRequest", "getDetachPaymentMethodUrl", "getDetachPaymentMethodUrl$stripe_release", "getFpxBankStatus", "Landroidx/lifecycle/LiveData;", "Lcom/stripe/android/model/FpxBankStatuses;", "(Lcom/stripe/android/ApiRequest$Options;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "getPaymentMethods", "listPaymentMethodsParams", "Lcom/stripe/android/model/ListPaymentMethodsParams;", "handleApiError", "response", "Lcom/stripe/android/StripeResponse;", "makeAnalyticsRequest", "request", "Lcom/stripe/android/AnalyticsRequest;", "makeApiRequest", "makeApiRequest$stripe_release", "makeFileUploadRequest", "fileUploadRequest", "Lcom/stripe/android/FileUploadRequest;", "makeFileUploadRequest$stripe_release", "resetDnsCacheTtl", "dnsCacheData", "retrieveCustomer", "Lcom/stripe/android/model/Customer;", "retrieveIntent", "retrieveIssuingCardPin", "cardId", "verificationId", "userOneTimeCode", "ephemeralKeySecret", "retrievePaymentIntent", "retrieveSetupIntent", "retrieveSource", "setCustomerShippingInfo", "shippingInformation", "Lcom/stripe/android/model/ShippingInformation;", "setDefaultCustomerSource", "start3ds2Auth", "Lcom/stripe/android/model/Stripe3ds2AuthResult;", "authParams", "Lcom/stripe/android/Stripe3ds2AuthParams;", "stripeIntentId", "start3ds2Auth$stripe_release", "updateIssuingCardPin", "newPin", "CancelIntentTask", "Companion", "Complete3ds2AuthTask", "RetrieveIntentTask", "RetrieveSourceTask", "Start3ds2AuthTask", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: StripeApiRepository.kt */
public final class StripeApiRepository implements StripeRepository {
    public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
    private static final String DNS_CACHE_TTL_PROPERTY_NAME = "networkaddress.cache.ttl";
    private final AnalyticsDataFactory analyticsDataFactory;
    private final AnalyticsRequestExecutor analyticsRequestExecutor;
    private final AnalyticsRequest.Factory analyticsRequestFactory;
    private final ApiFingerprintParamsFactory apiFingerprintParamsFactory;
    /* access modifiers changed from: private */
    public final ApiRequest.Factory apiRequestFactory;
    private final AppInfo appInfo;
    private final FingerprintDataRepository fingerprintDataRepository;
    private final FingerprintParamsUtils fingerprintParamsUtils;
    private final Logger logger;
    private final ApiRequestExecutor stripeApiRequestExecutor;
    private final CoroutineContext workContext;

    public StripeApiRepository(Context context, String str) {
        this(context, str, (AppInfo) null, (Logger) null, (ApiRequestExecutor) null, (AnalyticsRequestExecutor) null, (FingerprintDataRepository) null, (ApiFingerprintParamsFactory) null, (AnalyticsDataFactory) null, (FingerprintParamsUtils) null, (CoroutineContext) null, (String) null, (String) null, 8188, (DefaultConstructorMarker) null);
    }

    public StripeApiRepository(Context context, String str, AppInfo appInfo2) {
        this(context, str, appInfo2, (Logger) null, (ApiRequestExecutor) null, (AnalyticsRequestExecutor) null, (FingerprintDataRepository) null, (ApiFingerprintParamsFactory) null, (AnalyticsDataFactory) null, (FingerprintParamsUtils) null, (CoroutineContext) null, (String) null, (String) null, 8184, (DefaultConstructorMarker) null);
    }

    public StripeApiRepository(Context context, String str, AppInfo appInfo2, Logger logger2) {
        this(context, str, appInfo2, logger2, (ApiRequestExecutor) null, (AnalyticsRequestExecutor) null, (FingerprintDataRepository) null, (ApiFingerprintParamsFactory) null, (AnalyticsDataFactory) null, (FingerprintParamsUtils) null, (CoroutineContext) null, (String) null, (String) null, 8176, (DefaultConstructorMarker) null);
    }

    public StripeApiRepository(Context context, String str, AppInfo appInfo2, Logger logger2, ApiRequestExecutor apiRequestExecutor) {
        this(context, str, appInfo2, logger2, apiRequestExecutor, (AnalyticsRequestExecutor) null, (FingerprintDataRepository) null, (ApiFingerprintParamsFactory) null, (AnalyticsDataFactory) null, (FingerprintParamsUtils) null, (CoroutineContext) null, (String) null, (String) null, 8160, (DefaultConstructorMarker) null);
    }

    public StripeApiRepository(Context context, String str, AppInfo appInfo2, Logger logger2, ApiRequestExecutor apiRequestExecutor, AnalyticsRequestExecutor analyticsRequestExecutor2) {
        this(context, str, appInfo2, logger2, apiRequestExecutor, analyticsRequestExecutor2, (FingerprintDataRepository) null, (ApiFingerprintParamsFactory) null, (AnalyticsDataFactory) null, (FingerprintParamsUtils) null, (CoroutineContext) null, (String) null, (String) null, 8128, (DefaultConstructorMarker) null);
    }

    public StripeApiRepository(Context context, String str, AppInfo appInfo2, Logger logger2, ApiRequestExecutor apiRequestExecutor, AnalyticsRequestExecutor analyticsRequestExecutor2, FingerprintDataRepository fingerprintDataRepository2) {
        this(context, str, appInfo2, logger2, apiRequestExecutor, analyticsRequestExecutor2, fingerprintDataRepository2, (ApiFingerprintParamsFactory) null, (AnalyticsDataFactory) null, (FingerprintParamsUtils) null, (CoroutineContext) null, (String) null, (String) null, 8064, (DefaultConstructorMarker) null);
    }

    public StripeApiRepository(Context context, String str, AppInfo appInfo2, Logger logger2, ApiRequestExecutor apiRequestExecutor, AnalyticsRequestExecutor analyticsRequestExecutor2, FingerprintDataRepository fingerprintDataRepository2, ApiFingerprintParamsFactory apiFingerprintParamsFactory2) {
        this(context, str, appInfo2, logger2, apiRequestExecutor, analyticsRequestExecutor2, fingerprintDataRepository2, apiFingerprintParamsFactory2, (AnalyticsDataFactory) null, (FingerprintParamsUtils) null, (CoroutineContext) null, (String) null, (String) null, 7936, (DefaultConstructorMarker) null);
    }

    public StripeApiRepository(Context context, String str, AppInfo appInfo2, Logger logger2, ApiRequestExecutor apiRequestExecutor, AnalyticsRequestExecutor analyticsRequestExecutor2, FingerprintDataRepository fingerprintDataRepository2, ApiFingerprintParamsFactory apiFingerprintParamsFactory2, AnalyticsDataFactory analyticsDataFactory2) {
        this(context, str, appInfo2, logger2, apiRequestExecutor, analyticsRequestExecutor2, fingerprintDataRepository2, apiFingerprintParamsFactory2, analyticsDataFactory2, (FingerprintParamsUtils) null, (CoroutineContext) null, (String) null, (String) null, 7680, (DefaultConstructorMarker) null);
    }

    public StripeApiRepository(Context context, String str, AppInfo appInfo2, Logger logger2, ApiRequestExecutor apiRequestExecutor, AnalyticsRequestExecutor analyticsRequestExecutor2, FingerprintDataRepository fingerprintDataRepository2, ApiFingerprintParamsFactory apiFingerprintParamsFactory2, AnalyticsDataFactory analyticsDataFactory2, FingerprintParamsUtils fingerprintParamsUtils2) {
        this(context, str, appInfo2, logger2, apiRequestExecutor, analyticsRequestExecutor2, fingerprintDataRepository2, apiFingerprintParamsFactory2, analyticsDataFactory2, fingerprintParamsUtils2, (CoroutineContext) null, (String) null, (String) null, 7168, (DefaultConstructorMarker) null);
    }

    public StripeApiRepository(Context context, String str, AppInfo appInfo2, Logger logger2, ApiRequestExecutor apiRequestExecutor, AnalyticsRequestExecutor analyticsRequestExecutor2, FingerprintDataRepository fingerprintDataRepository2, ApiFingerprintParamsFactory apiFingerprintParamsFactory2, AnalyticsDataFactory analyticsDataFactory2, FingerprintParamsUtils fingerprintParamsUtils2, CoroutineContext coroutineContext) {
        this(context, str, appInfo2, logger2, apiRequestExecutor, analyticsRequestExecutor2, fingerprintDataRepository2, apiFingerprintParamsFactory2, analyticsDataFactory2, fingerprintParamsUtils2, coroutineContext, (String) null, (String) null, 6144, (DefaultConstructorMarker) null);
    }

    public StripeApiRepository(Context context, String str, AppInfo appInfo2, Logger logger2, ApiRequestExecutor apiRequestExecutor, AnalyticsRequestExecutor analyticsRequestExecutor2, FingerprintDataRepository fingerprintDataRepository2, ApiFingerprintParamsFactory apiFingerprintParamsFactory2, AnalyticsDataFactory analyticsDataFactory2, FingerprintParamsUtils fingerprintParamsUtils2, CoroutineContext coroutineContext, String str2) {
        this(context, str, appInfo2, logger2, apiRequestExecutor, analyticsRequestExecutor2, fingerprintDataRepository2, apiFingerprintParamsFactory2, analyticsDataFactory2, fingerprintParamsUtils2, coroutineContext, str2, (String) null, 4096, (DefaultConstructorMarker) null);
    }

    public StripeApiRepository(Context context, String str, AppInfo appInfo2, Logger logger2, ApiRequestExecutor apiRequestExecutor, AnalyticsRequestExecutor analyticsRequestExecutor2, FingerprintDataRepository fingerprintDataRepository2, ApiFingerprintParamsFactory apiFingerprintParamsFactory2, AnalyticsDataFactory analyticsDataFactory2, FingerprintParamsUtils fingerprintParamsUtils2, CoroutineContext coroutineContext, String str2, String str3) {
        Intrinsics.checkParameterIsNotNull(context, "context");
        Intrinsics.checkParameterIsNotNull(str, "publishableKey");
        Intrinsics.checkParameterIsNotNull(logger2, "logger");
        Intrinsics.checkParameterIsNotNull(apiRequestExecutor, "stripeApiRequestExecutor");
        Intrinsics.checkParameterIsNotNull(analyticsRequestExecutor2, "analyticsRequestExecutor");
        Intrinsics.checkParameterIsNotNull(fingerprintDataRepository2, "fingerprintDataRepository");
        Intrinsics.checkParameterIsNotNull(apiFingerprintParamsFactory2, "apiFingerprintParamsFactory");
        Intrinsics.checkParameterIsNotNull(analyticsDataFactory2, "analyticsDataFactory");
        Intrinsics.checkParameterIsNotNull(fingerprintParamsUtils2, "fingerprintParamsUtils");
        Intrinsics.checkParameterIsNotNull(coroutineContext, "workContext");
        Intrinsics.checkParameterIsNotNull(str2, "apiVersion");
        Intrinsics.checkParameterIsNotNull(str3, "sdkVersion");
        this.appInfo = appInfo2;
        this.logger = logger2;
        this.stripeApiRequestExecutor = apiRequestExecutor;
        this.analyticsRequestExecutor = analyticsRequestExecutor2;
        this.fingerprintDataRepository = fingerprintDataRepository2;
        this.apiFingerprintParamsFactory = apiFingerprintParamsFactory2;
        this.analyticsDataFactory = analyticsDataFactory2;
        this.fingerprintParamsUtils = fingerprintParamsUtils2;
        this.workContext = coroutineContext;
        this.analyticsRequestFactory = new AnalyticsRequest.Factory(logger2);
        this.apiRequestFactory = new ApiRequest.Factory(this.appInfo, str2, str3);
        fireFingerprintRequest();
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* synthetic */ StripeApiRepository(android.content.Context r16, java.lang.String r17, com.stripe.android.AppInfo r18, com.stripe.android.Logger r19, com.stripe.android.ApiRequestExecutor r20, com.stripe.android.AnalyticsRequestExecutor r21, com.stripe.android.FingerprintDataRepository r22, com.stripe.android.ApiFingerprintParamsFactory r23, com.stripe.android.AnalyticsDataFactory r24, com.stripe.android.FingerprintParamsUtils r25, kotlin.coroutines.CoroutineContext r26, java.lang.String r27, java.lang.String r28, int r29, kotlin.jvm.internal.DefaultConstructorMarker r30) {
        /*
            r15 = this;
            r1 = r16
            r0 = r29
            r2 = r0 & 4
            if (r2 == 0) goto L_0x000d
            r2 = 0
            com.stripe.android.AppInfo r2 = (com.stripe.android.AppInfo) r2
            r3 = r2
            goto L_0x000f
        L_0x000d:
            r3 = r18
        L_0x000f:
            r2 = r0 & 8
            if (r2 == 0) goto L_0x001b
            com.stripe.android.Logger$Companion r2 = com.stripe.android.Logger.Companion
            com.stripe.android.Logger r2 = r2.noop$stripe_release()
            r4 = r2
            goto L_0x001d
        L_0x001b:
            r4 = r19
        L_0x001d:
            r2 = r0 & 16
            if (r2 == 0) goto L_0x002a
            com.stripe.android.ApiRequestExecutor$Default r2 = new com.stripe.android.ApiRequestExecutor$Default
            r2.<init>(r4)
            com.stripe.android.ApiRequestExecutor r2 = (com.stripe.android.ApiRequestExecutor) r2
            r5 = r2
            goto L_0x002c
        L_0x002a:
            r5 = r20
        L_0x002c:
            r2 = r0 & 32
            if (r2 == 0) goto L_0x0039
            com.stripe.android.AnalyticsRequestExecutor$Default r2 = new com.stripe.android.AnalyticsRequestExecutor$Default
            r2.<init>(r4)
            com.stripe.android.AnalyticsRequestExecutor r2 = (com.stripe.android.AnalyticsRequestExecutor) r2
            r6 = r2
            goto L_0x003b
        L_0x0039:
            r6 = r21
        L_0x003b:
            r2 = r0 & 64
            if (r2 == 0) goto L_0x0048
            com.stripe.android.FingerprintDataRepository$Default r2 = new com.stripe.android.FingerprintDataRepository$Default
            r2.<init>(r1)
            com.stripe.android.FingerprintDataRepository r2 = (com.stripe.android.FingerprintDataRepository) r2
            r7 = r2
            goto L_0x004a
        L_0x0048:
            r7 = r22
        L_0x004a:
            r2 = r0 & 128(0x80, float:1.794E-43)
            if (r2 == 0) goto L_0x0055
            com.stripe.android.ApiFingerprintParamsFactory r2 = new com.stripe.android.ApiFingerprintParamsFactory
            r2.<init>((android.content.Context) r1)
            r8 = r2
            goto L_0x0057
        L_0x0055:
            r8 = r23
        L_0x0057:
            r2 = r0 & 256(0x100, float:3.59E-43)
            if (r2 == 0) goto L_0x0064
            com.stripe.android.AnalyticsDataFactory r2 = new com.stripe.android.AnalyticsDataFactory
            r9 = r17
            r2.<init>(r1, r9)
            r10 = r2
            goto L_0x0068
        L_0x0064:
            r9 = r17
            r10 = r24
        L_0x0068:
            r2 = r0 & 512(0x200, float:7.175E-43)
            if (r2 == 0) goto L_0x0073
            com.stripe.android.FingerprintParamsUtils r2 = new com.stripe.android.FingerprintParamsUtils
            r2.<init>((android.content.Context) r1)
            r11 = r2
            goto L_0x0075
        L_0x0073:
            r11 = r25
        L_0x0075:
            r2 = r0 & 1024(0x400, float:1.435E-42)
            if (r2 == 0) goto L_0x0081
            kotlinx.coroutines.CoroutineDispatcher r2 = kotlinx.coroutines.Dispatchers.getIO()
            kotlin.coroutines.CoroutineContext r2 = (kotlin.coroutines.CoroutineContext) r2
            r12 = r2
            goto L_0x0083
        L_0x0081:
            r12 = r26
        L_0x0083:
            r2 = r0 & 2048(0x800, float:2.87E-42)
            if (r2 == 0) goto L_0x0093
            com.stripe.android.ApiVersion$Companion r2 = com.stripe.android.ApiVersion.Companion
            com.stripe.android.ApiVersion r2 = r2.get$stripe_release()
            java.lang.String r2 = r2.getCode$stripe_release()
            r13 = r2
            goto L_0x0095
        L_0x0093:
            r13 = r27
        L_0x0095:
            r0 = r0 & 4096(0x1000, float:5.74E-42)
            if (r0 == 0) goto L_0x009d
            java.lang.String r0 = "AndroidBindings/14.5.0"
            r14 = r0
            goto L_0x009f
        L_0x009d:
            r14 = r28
        L_0x009f:
            r0 = r15
            r1 = r16
            r2 = r17
            r9 = r10
            r10 = r11
            r11 = r12
            r12 = r13
            r13 = r14
            r0.<init>(r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.stripe.android.StripeApiRepository.<init>(android.content.Context, java.lang.String, com.stripe.android.AppInfo, com.stripe.android.Logger, com.stripe.android.ApiRequestExecutor, com.stripe.android.AnalyticsRequestExecutor, com.stripe.android.FingerprintDataRepository, com.stripe.android.ApiFingerprintParamsFactory, com.stripe.android.AnalyticsDataFactory, com.stripe.android.FingerprintParamsUtils, kotlin.coroutines.CoroutineContext, java.lang.String, java.lang.String, int, kotlin.jvm.internal.DefaultConstructorMarker):void");
    }

    private final String getFingerprintGuid() {
        FingerprintData fingerprintData = this.fingerprintDataRepository.get();
        if (fingerprintData != null) {
            return fingerprintData.getGuid$stripe_release();
        }
        return null;
    }

    public PaymentIntent confirmPaymentIntent(ConfirmPaymentIntentParams confirmPaymentIntentParams, ApiRequest.Options options, List<String> list) throws AuthenticationException, InvalidRequestException, APIConnectionException, APIException {
        String str;
        Intrinsics.checkParameterIsNotNull(confirmPaymentIntentParams, "confirmPaymentIntentParams");
        Intrinsics.checkParameterIsNotNull(options, "options");
        Intrinsics.checkParameterIsNotNull(list, "expandFields");
        Map<String, ?> addFingerprintData$stripe_release = this.fingerprintParamsUtils.addFingerprintData$stripe_release(MapsKt.plus(confirmPaymentIntentParams.toParamMap(), (Map<String, Object>) Companion.createExpandParam(list)), getFingerprintGuid());
        String confirmPaymentIntentUrl$stripe_release = Companion.getConfirmPaymentIntentUrl$stripe_release(new PaymentIntent.ClientSecret(confirmPaymentIntentParams.getClientSecret()).getPaymentIntentId$stripe_release());
        try {
            fireFingerprintRequest();
            PaymentMethodCreateParams paymentMethodCreateParams = confirmPaymentIntentParams.getPaymentMethodCreateParams();
            if (paymentMethodCreateParams == null || (str = paymentMethodCreateParams.getTypeCode()) == null) {
                SourceParams sourceParams = confirmPaymentIntentParams.getSourceParams();
                str = sourceParams != null ? sourceParams.getType() : null;
            }
            fireAnalyticsRequest$stripe_release(this.analyticsDataFactory.createPaymentIntentConfirmationParams$stripe_release(str), options.getApiKey());
            return (PaymentIntent) fetchStripeModel(this.apiRequestFactory.createPost(confirmPaymentIntentUrl$stripe_release, options, addFingerprintData$stripe_release), new PaymentIntentJsonParser());
        } catch (CardException e) {
            throw APIException.Companion.create$stripe_release(e);
        }
    }

    public PaymentIntent retrievePaymentIntent(String str, ApiRequest.Options options, List<String> list) throws AuthenticationException, InvalidRequestException, APIConnectionException, APIException {
        Intrinsics.checkParameterIsNotNull(str, "clientSecret");
        Intrinsics.checkParameterIsNotNull(options, "options");
        Intrinsics.checkParameterIsNotNull(list, "expandFields");
        String paymentIntentId$stripe_release = new PaymentIntent.ClientSecret(str).getPaymentIntentId$stripe_release();
        fireFingerprintRequest();
        fireAnalyticsRequest$stripe_release(this.analyticsDataFactory.createPaymentIntentRetrieveParams$stripe_release(paymentIntentId$stripe_release), options.getApiKey());
        try {
            return (PaymentIntent) fetchStripeModel(this.apiRequestFactory.createGet(Companion.getRetrievePaymentIntentUrl$stripe_release(paymentIntentId$stripe_release), options, createClientSecretParam(str, list)), new PaymentIntentJsonParser());
        } catch (CardException e) {
            throw APIException.Companion.create$stripe_release(e);
        }
    }

    public PaymentIntent cancelPaymentIntentSource(String str, String str2, ApiRequest.Options options) throws AuthenticationException, InvalidRequestException, APIConnectionException, APIException {
        Intrinsics.checkParameterIsNotNull(str, "paymentIntentId");
        Intrinsics.checkParameterIsNotNull(str2, "sourceId");
        Intrinsics.checkParameterIsNotNull(options, "options");
        fireFingerprintRequest();
        fireAnalyticsRequest(AnalyticsEvent.PaymentIntentCancelSource, options.getApiKey());
        try {
            return (PaymentIntent) fetchStripeModel(this.apiRequestFactory.createPost(Companion.getCancelPaymentIntentSourceUrl$stripe_release(str), options, MapsKt.mapOf(TuplesKt.to("source", str2))), new PaymentIntentJsonParser());
        } catch (CardException e) {
            throw APIException.Companion.create$stripe_release(e);
        }
    }

    public SetupIntent confirmSetupIntent(ConfirmSetupIntentParams confirmSetupIntentParams, ApiRequest.Options options, List<String> list) throws AuthenticationException, InvalidRequestException, APIConnectionException, APIException {
        Intrinsics.checkParameterIsNotNull(confirmSetupIntentParams, "confirmSetupIntentParams");
        Intrinsics.checkParameterIsNotNull(options, "options");
        Intrinsics.checkParameterIsNotNull(list, "expandFields");
        String setupIntentId$stripe_release = new SetupIntent.ClientSecret(confirmSetupIntentParams.getClientSecret()).getSetupIntentId$stripe_release();
        fireFingerprintRequest();
        AnalyticsDataFactory analyticsDataFactory2 = this.analyticsDataFactory;
        PaymentMethodCreateParams paymentMethodCreateParams$stripe_release = confirmSetupIntentParams.getPaymentMethodCreateParams$stripe_release();
        fireAnalyticsRequest$stripe_release(analyticsDataFactory2.createSetupIntentConfirmationParams$stripe_release(paymentMethodCreateParams$stripe_release != null ? paymentMethodCreateParams$stripe_release.getTypeCode() : null, setupIntentId$stripe_release), options.getApiKey());
        try {
            return (SetupIntent) fetchStripeModel(this.apiRequestFactory.createPost(Companion.getConfirmSetupIntentUrl$stripe_release(setupIntentId$stripe_release), options, this.fingerprintParamsUtils.addFingerprintData$stripe_release(MapsKt.plus(confirmSetupIntentParams.toParamMap(), (Map<String, Object>) Companion.createExpandParam(list)), getFingerprintGuid())), new SetupIntentJsonParser());
        } catch (CardException e) {
            throw APIException.Companion.create$stripe_release(e);
        }
    }

    public SetupIntent retrieveSetupIntent(String str, ApiRequest.Options options, List<String> list) throws AuthenticationException, InvalidRequestException, APIConnectionException, APIException {
        Intrinsics.checkParameterIsNotNull(str, "clientSecret");
        Intrinsics.checkParameterIsNotNull(options, "options");
        Intrinsics.checkParameterIsNotNull(list, "expandFields");
        String setupIntentId$stripe_release = new SetupIntent.ClientSecret(str).getSetupIntentId$stripe_release();
        try {
            fireFingerprintRequest();
            fireAnalyticsRequest$stripe_release(this.analyticsDataFactory.createSetupIntentRetrieveParams$stripe_release(setupIntentId$stripe_release), options.getApiKey());
            return (SetupIntent) fetchStripeModel(this.apiRequestFactory.createGet(Companion.getRetrieveSetupIntentUrl$stripe_release(setupIntentId$stripe_release), options, createClientSecretParam(str, list)), new SetupIntentJsonParser());
        } catch (CardException e) {
            throw APIException.Companion.create$stripe_release(e);
        }
    }

    public SetupIntent cancelSetupIntentSource(String str, String str2, ApiRequest.Options options) throws AuthenticationException, InvalidRequestException, APIConnectionException, APIException {
        Intrinsics.checkParameterIsNotNull(str, "setupIntentId");
        Intrinsics.checkParameterIsNotNull(str2, "sourceId");
        Intrinsics.checkParameterIsNotNull(options, "options");
        fireAnalyticsRequest(AnalyticsEvent.SetupIntentCancelSource, options.getApiKey());
        try {
            return (SetupIntent) fetchStripeModel(this.apiRequestFactory.createPost(Companion.getCancelSetupIntentSourceUrl$stripe_release(str), options, MapsKt.mapOf(TuplesKt.to("source", str2))), new SetupIntentJsonParser());
        } catch (CardException e) {
            throw APIException.Companion.create$stripe_release(e);
        }
    }

    public void retrieveIntent(String str, ApiRequest.Options options, List<String> list, ApiResultCallback<StripeIntent> apiResultCallback) {
        Intrinsics.checkParameterIsNotNull(str, "clientSecret");
        Intrinsics.checkParameterIsNotNull(options, "options");
        Intrinsics.checkParameterIsNotNull(list, "expandFields");
        Intrinsics.checkParameterIsNotNull(apiResultCallback, "callback");
        new RetrieveIntentTask(this, str, options, list, apiResultCallback).execute$stripe_release();
    }

    public void cancelIntent(StripeIntent stripeIntent, String str, ApiRequest.Options options, ApiResultCallback<StripeIntent> apiResultCallback) {
        Intrinsics.checkParameterIsNotNull(stripeIntent, "stripeIntent");
        Intrinsics.checkParameterIsNotNull(str, "sourceId");
        Intrinsics.checkParameterIsNotNull(options, "options");
        Intrinsics.checkParameterIsNotNull(apiResultCallback, "callback");
        new CancelIntentTask(this, stripeIntent, str, options, apiResultCallback).execute$stripe_release();
    }

    public Source createSource(SourceParams sourceParams, ApiRequest.Options options) throws AuthenticationException, InvalidRequestException, APIConnectionException, APIException {
        Intrinsics.checkParameterIsNotNull(sourceParams, "sourceParams");
        Intrinsics.checkParameterIsNotNull(options, "options");
        fireFingerprintRequest();
        fireAnalyticsRequest$stripe_release(this.analyticsDataFactory.createSourceCreationParams$stripe_release(sourceParams.getType(), sourceParams.getAttribution$stripe_release()), options.getApiKey());
        try {
            return (Source) fetchStripeModel(this.apiRequestFactory.createPost(Companion.getSourcesUrl$stripe_release(), options, MapsKt.plus(sourceParams.toParamMap(), (Map<String, Object>) this.apiFingerprintParamsFactory.createParams(getFingerprintGuid()))), new SourceJsonParser());
        } catch (CardException e) {
            throw APIException.Companion.create$stripe_release(e);
        }
    }

    public Source retrieveSource(String str, String str2, ApiRequest.Options options) throws AuthenticationException, InvalidRequestException, APIConnectionException, APIException {
        Intrinsics.checkParameterIsNotNull(str, "sourceId");
        Intrinsics.checkParameterIsNotNull(str2, "clientSecret");
        Intrinsics.checkParameterIsNotNull(options, "options");
        String retrieveSourceApiUrl$stripe_release = Companion.getRetrieveSourceApiUrl$stripe_release(str);
        fireAnalyticsRequest$stripe_release(this.analyticsDataFactory.createSourceRetrieveParams$stripe_release(str), options.getApiKey());
        try {
            return (Source) fetchStripeModel(this.apiRequestFactory.createGet(retrieveSourceApiUrl$stripe_release, options, SourceParams.Companion.createRetrieveSourceParams(str2)), new SourceJsonParser());
        } catch (CardException e) {
            throw APIException.Companion.create$stripe_release(e);
        }
    }

    public void retrieveSource(String str, String str2, ApiRequest.Options options, ApiResultCallback<Source> apiResultCallback) {
        Intrinsics.checkParameterIsNotNull(str, "sourceId");
        Intrinsics.checkParameterIsNotNull(str2, "clientSecret");
        Intrinsics.checkParameterIsNotNull(options, "options");
        Intrinsics.checkParameterIsNotNull(apiResultCallback, "callback");
        new RetrieveSourceTask(this, str, str2, options, apiResultCallback).execute$stripe_release();
    }

    public PaymentMethod createPaymentMethod(PaymentMethodCreateParams paymentMethodCreateParams, ApiRequest.Options options) throws AuthenticationException, InvalidRequestException, APIConnectionException, APIException {
        Intrinsics.checkParameterIsNotNull(paymentMethodCreateParams, "paymentMethodCreateParams");
        Intrinsics.checkParameterIsNotNull(options, "options");
        fireFingerprintRequest();
        try {
            PaymentMethod paymentMethod = (PaymentMethod) fetchStripeModel(this.apiRequestFactory.createPost(Companion.getPaymentMethodsUrl$stripe_release(), options, MapsKt.plus(paymentMethodCreateParams.toParamMap(), (Map<String, Object>) this.apiFingerprintParamsFactory.createParams(getFingerprintGuid()))), new PaymentMethodJsonParser());
            AnalyticsDataFactory analyticsDataFactory2 = this.analyticsDataFactory;
            PaymentMethod.Type type = null;
            String str = paymentMethod != null ? paymentMethod.id : null;
            if (paymentMethod != null) {
                type = paymentMethod.type;
            }
            fireAnalyticsRequest$stripe_release(analyticsDataFactory2.createPaymentMethodCreationParams$stripe_release(str, type, paymentMethodCreateParams.getAttribution$stripe_release()), options.getApiKey());
            return paymentMethod;
        } catch (CardException e) {
            throw APIException.Companion.create$stripe_release(e);
        }
    }

    public Token createToken(TokenParams tokenParams, ApiRequest.Options options) throws AuthenticationException, InvalidRequestException, APIConnectionException, CardException, APIException {
        Intrinsics.checkParameterIsNotNull(tokenParams, "tokenParams");
        Intrinsics.checkParameterIsNotNull(options, "options");
        fireFingerprintRequest();
        fireAnalyticsRequest$stripe_release(this.analyticsDataFactory.createTokenCreationParams$stripe_release(tokenParams.getAttribution$stripe_release(), tokenParams.getTokenType$stripe_release()), options.getApiKey());
        return (Token) fetchStripeModel(this.apiRequestFactory.createPost(Companion.getTokensUrl$stripe_release(), options, MapsKt.plus(tokenParams.toParamMap(), (Map<String, Object>) this.apiFingerprintParamsFactory.createParams(getFingerprintGuid()))), new TokenJsonParser());
    }

    public Source addCustomerSource(String str, String str2, Set<String> set, String str3, String str4, ApiRequest.Options options) throws InvalidRequestException, APIConnectionException, APIException, AuthenticationException, CardException {
        Intrinsics.checkParameterIsNotNull(str, "customerId");
        Intrinsics.checkParameterIsNotNull(str2, "publishableKey");
        Intrinsics.checkParameterIsNotNull(set, "productUsageTokens");
        Intrinsics.checkParameterIsNotNull(str3, "sourceId");
        Intrinsics.checkParameterIsNotNull(str4, "sourceType");
        Intrinsics.checkParameterIsNotNull(options, "requestOptions");
        fireAnalyticsRequest$stripe_release(this.analyticsDataFactory.createAddSourceParams$stripe_release(set, str4), str2);
        return (Source) fetchStripeModel(this.apiRequestFactory.createPost(Companion.getAddCustomerSourceUrl$stripe_release(str), options, MapsKt.mapOf(TuplesKt.to("source", str3))), new SourceJsonParser());
    }

    public Source deleteCustomerSource(String str, String str2, Set<String> set, String str3, ApiRequest.Options options) throws InvalidRequestException, APIConnectionException, APIException, AuthenticationException, CardException {
        Intrinsics.checkParameterIsNotNull(str, "customerId");
        Intrinsics.checkParameterIsNotNull(str2, "publishableKey");
        Intrinsics.checkParameterIsNotNull(set, "productUsageTokens");
        Intrinsics.checkParameterIsNotNull(str3, "sourceId");
        Intrinsics.checkParameterIsNotNull(options, "requestOptions");
        fireAnalyticsRequest$stripe_release(this.analyticsDataFactory.createDeleteSourceParams$stripe_release(set), str2);
        return (Source) fetchStripeModel(this.apiRequestFactory.createDelete(Companion.getDeleteCustomerSourceUrl$stripe_release(str, str3), options), new SourceJsonParser());
    }

    public PaymentMethod attachPaymentMethod(String str, String str2, Set<String> set, String str3, ApiRequest.Options options) throws InvalidRequestException, APIConnectionException, APIException, AuthenticationException, CardException {
        Intrinsics.checkParameterIsNotNull(str, "customerId");
        Intrinsics.checkParameterIsNotNull(str2, "publishableKey");
        Intrinsics.checkParameterIsNotNull(set, "productUsageTokens");
        Intrinsics.checkParameterIsNotNull(str3, "paymentMethodId");
        Intrinsics.checkParameterIsNotNull(options, "requestOptions");
        fireFingerprintRequest();
        fireAnalyticsRequest$stripe_release(this.analyticsDataFactory.createAttachPaymentMethodParams$stripe_release(set), str2);
        return (PaymentMethod) fetchStripeModel(this.apiRequestFactory.createPost(Companion.getAttachPaymentMethodUrl$stripe_release(str3), options, MapsKt.mapOf(TuplesKt.to("customer", str))), new PaymentMethodJsonParser());
    }

    public PaymentMethod detachPaymentMethod(String str, Set<String> set, String str2, ApiRequest.Options options) throws InvalidRequestException, APIConnectionException, APIException, AuthenticationException, CardException {
        Intrinsics.checkParameterIsNotNull(str, "publishableKey");
        Intrinsics.checkParameterIsNotNull(set, "productUsageTokens");
        Intrinsics.checkParameterIsNotNull(str2, "paymentMethodId");
        Intrinsics.checkParameterIsNotNull(options, "requestOptions");
        fireAnalyticsRequest$stripe_release(this.analyticsDataFactory.createDetachPaymentMethodParams$stripe_release(set), str);
        return (PaymentMethod) fetchStripeModel(ApiRequest.Factory.createPost$default(this.apiRequestFactory, getDetachPaymentMethodUrl$stripe_release(str2), options, (Map) null, 4, (Object) null), new PaymentMethodJsonParser());
    }

    public List<PaymentMethod> getPaymentMethods(ListPaymentMethodsParams listPaymentMethodsParams, String str, Set<String> set, ApiRequest.Options options) throws InvalidRequestException, APIConnectionException, APIException, AuthenticationException, CardException {
        Intrinsics.checkParameterIsNotNull(listPaymentMethodsParams, "listPaymentMethodsParams");
        Intrinsics.checkParameterIsNotNull(str, "publishableKey");
        Intrinsics.checkParameterIsNotNull(set, "productUsageTokens");
        Intrinsics.checkParameterIsNotNull(options, "requestOptions");
        StripeResponse makeApiRequest$stripe_release = makeApiRequest$stripe_release(this.apiRequestFactory.createGet(Companion.getPaymentMethodsUrl$stripe_release(), options, listPaymentMethodsParams.toParamMap()));
        fireAnalyticsRequest$stripe_release(AnalyticsDataFactory.createParams$stripe_release$default(this.analyticsDataFactory, AnalyticsEvent.CustomerRetrievePaymentMethods, set, (String) null, (String) null, (Map) null, 28, (Object) null), str);
        try {
            JSONArray optJSONArray = makeApiRequest$stripe_release.getResponseJson$stripe_release().optJSONArray("data");
            if (optJSONArray == null) {
                optJSONArray = new JSONArray();
            }
            Collection arrayList = new ArrayList();
            Iterator it = RangesKt.until(0, optJSONArray.length()).iterator();
            while (it.hasNext()) {
                int nextInt = ((IntIterator) it).nextInt();
                PaymentMethodJsonParser paymentMethodJsonParser = new PaymentMethodJsonParser();
                JSONObject optJSONObject = optJSONArray.optJSONObject(nextInt);
                Intrinsics.checkExpressionValueIsNotNull(optJSONObject, "data.optJSONObject(it)");
                PaymentMethod parse = paymentMethodJsonParser.parse(optJSONObject);
                if (parse != null) {
                    arrayList.add(parse);
                }
            }
            return (List) arrayList;
        } catch (JSONException unused) {
            return CollectionsKt.emptyList();
        }
    }

    public Customer setDefaultCustomerSource(String str, String str2, Set<String> set, String str3, String str4, ApiRequest.Options options) throws InvalidRequestException, APIConnectionException, APIException, AuthenticationException, CardException {
        Intrinsics.checkParameterIsNotNull(str, "customerId");
        Intrinsics.checkParameterIsNotNull(str2, "publishableKey");
        Intrinsics.checkParameterIsNotNull(set, "productUsageTokens");
        Intrinsics.checkParameterIsNotNull(str3, "sourceId");
        Intrinsics.checkParameterIsNotNull(str4, "sourceType");
        Intrinsics.checkParameterIsNotNull(options, "requestOptions");
        fireAnalyticsRequest$stripe_release(AnalyticsDataFactory.createParams$stripe_release$default(this.analyticsDataFactory, AnalyticsEvent.CustomerSetDefaultSource, set, str4, (String) null, (Map) null, 24, (Object) null), str2);
        return (Customer) fetchStripeModel(this.apiRequestFactory.createPost(Companion.getRetrieveCustomerUrl$stripe_release(str), options, MapsKt.mapOf(TuplesKt.to("default_source", str3))), new CustomerJsonParser());
    }

    public Customer setCustomerShippingInfo(String str, String str2, Set<String> set, ShippingInformation shippingInformation, ApiRequest.Options options) throws InvalidRequestException, APIConnectionException, APIException, AuthenticationException, CardException {
        Intrinsics.checkParameterIsNotNull(str, "customerId");
        Intrinsics.checkParameterIsNotNull(str2, "publishableKey");
        Intrinsics.checkParameterIsNotNull(set, "productUsageTokens");
        Intrinsics.checkParameterIsNotNull(shippingInformation, "shippingInformation");
        Intrinsics.checkParameterIsNotNull(options, "requestOptions");
        fireAnalyticsRequest$stripe_release(AnalyticsDataFactory.createParams$stripe_release$default(this.analyticsDataFactory, AnalyticsEvent.CustomerSetShippingInfo, set, (String) null, (String) null, (Map) null, 28, (Object) null), str2);
        return (Customer) fetchStripeModel(this.apiRequestFactory.createPost(Companion.getRetrieveCustomerUrl$stripe_release(str), options, MapsKt.mapOf(TuplesKt.to(FirebaseAnalytics.Param.SHIPPING, shippingInformation.toParamMap()))), new CustomerJsonParser());
    }

    public Customer retrieveCustomer(String str, Set<String> set, ApiRequest.Options options) throws InvalidRequestException, APIConnectionException, APIException, AuthenticationException, CardException {
        Intrinsics.checkParameterIsNotNull(str, "customerId");
        Intrinsics.checkParameterIsNotNull(set, "productUsageTokens");
        Intrinsics.checkParameterIsNotNull(options, "requestOptions");
        fireAnalyticsRequest$stripe_release(AnalyticsDataFactory.createParams$stripe_release$default(this.analyticsDataFactory, AnalyticsEvent.CustomerRetrieve, set, (String) null, (String) null, (Map) null, 28, (Object) null), options.getApiKey());
        return (Customer) fetchStripeModel(ApiRequest.Factory.createGet$default(this.apiRequestFactory, Companion.getRetrieveCustomerUrl$stripe_release(str), options, (Map) null, 4, (Object) null), new CustomerJsonParser());
    }

    public String retrieveIssuingCardPin(String str, String str2, String str3, String str4) throws InvalidRequestException, APIConnectionException, APIException, AuthenticationException, CardException, JSONException {
        Intrinsics.checkParameterIsNotNull(str, "cardId");
        Intrinsics.checkParameterIsNotNull(str2, "verificationId");
        Intrinsics.checkParameterIsNotNull(str3, "userOneTimeCode");
        Intrinsics.checkParameterIsNotNull(str4, "ephemeralKeySecret");
        fireAnalyticsRequest(AnalyticsEvent.IssuingRetrievePin, str4);
        String string = makeApiRequest$stripe_release(this.apiRequestFactory.createGet(Companion.getIssuingCardPinUrl$stripe_release(str), new ApiRequest.Options(str4, (String) null, (String) null, 6, (DefaultConstructorMarker) null), MapsKt.mapOf(TuplesKt.to("verification", Companion.createVerificationParam(str2, str3))))).getResponseJson$stripe_release().getString("pin");
        Intrinsics.checkExpressionValueIsNotNull(string, "response.responseJson.getString(\"pin\")");
        return string;
    }

    public void updateIssuingCardPin(String str, String str2, String str3, String str4, String str5) throws InvalidRequestException, APIConnectionException, APIException, AuthenticationException, CardException {
        Intrinsics.checkParameterIsNotNull(str, "cardId");
        Intrinsics.checkParameterIsNotNull(str2, "newPin");
        Intrinsics.checkParameterIsNotNull(str3, "verificationId");
        Intrinsics.checkParameterIsNotNull(str4, "userOneTimeCode");
        Intrinsics.checkParameterIsNotNull(str5, "ephemeralKeySecret");
        fireAnalyticsRequest(AnalyticsEvent.IssuingUpdatePin, str5);
        makeApiRequest$stripe_release(this.apiRequestFactory.createPost(Companion.getIssuingCardPinUrl$stripe_release(str), new ApiRequest.Options(str5, (String) null, (String) null, 6, (DefaultConstructorMarker) null), MapsKt.mapOf(TuplesKt.to("verification", Companion.createVerificationParam(str3, str4)), TuplesKt.to("pin", str2))));
    }

    public Object getFpxBankStatus(ApiRequest.Options options, Continuation<? super LiveData<FpxBankStatuses>> continuation) {
        return CoroutineLiveDataKt.liveData$default(this.workContext, 0, (Function2) new StripeApiRepository$getFpxBankStatus$2(this, options, (Continuation) null), 2, (Object) null);
    }

    public final Stripe3ds2AuthResult start3ds2Auth$stripe_release(Stripe3ds2AuthParams stripe3ds2AuthParams, String str, ApiRequest.Options options) throws InvalidRequestException, APIConnectionException, APIException, CardException, AuthenticationException, JSONException {
        Intrinsics.checkParameterIsNotNull(stripe3ds2AuthParams, "authParams");
        Intrinsics.checkParameterIsNotNull(str, "stripeIntentId");
        Intrinsics.checkParameterIsNotNull(options, "requestOptions");
        fireAnalyticsRequest$stripe_release(this.analyticsDataFactory.createAuthParams$stripe_release(AnalyticsEvent.Auth3ds2Start, str), options.getApiKey());
        return new Stripe3ds2AuthResultJsonParser().parse(makeApiRequest$stripe_release(this.apiRequestFactory.createPost(Companion.getApiUrl("3ds2/authenticate"), options, stripe3ds2AuthParams.toParamMap())).getResponseJson$stripe_release());
    }

    public void start3ds2Auth(Stripe3ds2AuthParams stripe3ds2AuthParams, String str, ApiRequest.Options options, ApiResultCallback<Stripe3ds2AuthResult> apiResultCallback) {
        Intrinsics.checkParameterIsNotNull(stripe3ds2AuthParams, "authParams");
        Intrinsics.checkParameterIsNotNull(str, "stripeIntentId");
        Intrinsics.checkParameterIsNotNull(options, "requestOptions");
        Intrinsics.checkParameterIsNotNull(apiResultCallback, "callback");
        new Start3ds2AuthTask(this, stripe3ds2AuthParams, str, options, apiResultCallback).execute$stripe_release();
    }

    public final boolean complete3ds2Auth$stripe_release(String str, ApiRequest.Options options) throws InvalidRequestException, APIConnectionException, APIException, CardException, AuthenticationException {
        Intrinsics.checkParameterIsNotNull(str, "sourceId");
        Intrinsics.checkParameterIsNotNull(options, "requestOptions");
        return makeApiRequest$stripe_release(this.apiRequestFactory.createPost(Companion.getApiUrl("3ds2/challenge_complete"), options, MapsKt.mapOf(TuplesKt.to("source", str)))).isOk$stripe_release();
    }

    public void complete3ds2Auth(String str, ApiRequest.Options options, ApiResultCallback<Boolean> apiResultCallback) {
        Intrinsics.checkParameterIsNotNull(str, "sourceId");
        Intrinsics.checkParameterIsNotNull(options, "requestOptions");
        Intrinsics.checkParameterIsNotNull(apiResultCallback, "callback");
        new Complete3ds2AuthTask(this, str, options, apiResultCallback).execute$stripe_release();
    }

    public StripeFile createFile(StripeFileParams stripeFileParams, ApiRequest.Options options) {
        Intrinsics.checkParameterIsNotNull(stripeFileParams, "fileParams");
        Intrinsics.checkParameterIsNotNull(options, "requestOptions");
        StripeResponse makeFileUploadRequest$stripe_release = makeFileUploadRequest$stripe_release(new FileUploadRequest(stripeFileParams, options, this.appInfo, (Function1) null, (String) null, 24, (DefaultConstructorMarker) null));
        fireAnalyticsRequest(AnalyticsEvent.FileCreate, options.getApiKey());
        return new StripeFileJsonParser().parse(makeFileUploadRequest$stripe_release.getResponseJson$stripe_release());
    }

    public final String getDetachPaymentMethodUrl$stripe_release(String str) {
        Intrinsics.checkParameterIsNotNull(str, "paymentMethodId");
        return Companion.getApiUrl("payment_methods/%s/detach", str);
    }

    private final void handleApiError(StripeResponse stripeResponse) throws InvalidRequestException, AuthenticationException, CardException, APIException {
        String requestId$stripe_release = stripeResponse.getRequestId$stripe_release();
        int code$stripe_release = stripeResponse.getCode$stripe_release();
        StripeError parse = new StripeErrorJsonParser().parse(stripeResponse.getResponseJson$stripe_release());
        if (code$stripe_release != 429) {
            switch (code$stripe_release) {
                case 400:
                case 404:
                    throw new InvalidRequestException(parse, requestId$stripe_release, code$stripe_release, (String) null, (Throwable) null, 24, (DefaultConstructorMarker) null);
                case 401:
                    throw new AuthenticationException(parse, requestId$stripe_release);
                case 402:
                    throw new CardException(parse, requestId$stripe_release);
                case 403:
                    throw new PermissionException(parse, requestId$stripe_release);
                default:
                    throw new APIException(parse, requestId$stripe_release, code$stripe_release, (String) null, (Throwable) null, 24, (DefaultConstructorMarker) null);
            }
        } else {
            throw new RateLimitException(parse, requestId$stripe_release, (String) null, (Throwable) null, 12, (DefaultConstructorMarker) null);
        }
    }

    private final <ModelType extends StripeModel> ModelType fetchStripeModel(ApiRequest apiRequest, ModelJsonParser<? extends ModelType> modelJsonParser) {
        return modelJsonParser.parse(makeApiRequest$stripe_release(apiRequest).getResponseJson$stripe_release());
    }

    public final StripeResponse makeApiRequest$stripe_release(ApiRequest apiRequest) throws AuthenticationException, InvalidRequestException, APIConnectionException, CardException, APIException {
        Intrinsics.checkParameterIsNotNull(apiRequest, "apiRequest");
        Pair<Boolean, String> disableDnsCache = disableDnsCache();
        try {
            StripeResponse execute = this.stripeApiRequestExecutor.execute(apiRequest);
            if (execute.isError$stripe_release()) {
                handleApiError(execute);
            }
            resetDnsCacheTtl(disableDnsCache);
            return execute;
        } catch (IOException e) {
            throw APIConnectionException.Companion.create$stripe_release(e, apiRequest.getBaseUrl());
        }
    }

    public final StripeResponse makeFileUploadRequest$stripe_release(FileUploadRequest fileUploadRequest) throws AuthenticationException, InvalidRequestException, APIConnectionException, CardException, APIException {
        Intrinsics.checkParameterIsNotNull(fileUploadRequest, "fileUploadRequest");
        Pair<Boolean, String> disableDnsCache = disableDnsCache();
        try {
            StripeResponse execute = this.stripeApiRequestExecutor.execute(fileUploadRequest);
            if (execute.isError$stripe_release()) {
                handleApiError(execute);
            }
            resetDnsCacheTtl(disableDnsCache);
            return execute;
        } catch (IOException e) {
            throw APIConnectionException.Companion.create$stripe_release(e, fileUploadRequest.getBaseUrl());
        }
    }

    private final void makeAnalyticsRequest(AnalyticsRequest analyticsRequest) {
        this.analyticsRequestExecutor.executeAsync(analyticsRequest);
    }

    private final Pair<Boolean, String> disableDnsCache() {
        try {
            String property = Security.getProperty(DNS_CACHE_TTL_PROPERTY_NAME);
            Security.setProperty(DNS_CACHE_TTL_PROPERTY_NAME, AppEventsConstants.EVENT_PARAM_VALUE_NO);
            Pair<Boolean, String> create = Pair.create(true, property);
            Intrinsics.checkExpressionValueIsNotNull(create, "Pair.create(true, originalDNSCacheTtl)");
            return create;
        } catch (SecurityException unused) {
            Pair<Boolean, String> create2 = Pair.create(false, (Object) null);
            Intrinsics.checkExpressionValueIsNotNull(create2, "Pair.create(false, null)");
            return create2;
        }
    }

    private final void resetDnsCacheTtl(Pair<Boolean, String> pair) {
        Object obj = pair.first;
        Intrinsics.checkExpressionValueIsNotNull(obj, "dnsCacheData.first");
        if (((Boolean) obj).booleanValue()) {
            String str = (String) pair.second;
            if (str == null) {
                str = "-1";
            }
            Security.setProperty(DNS_CACHE_TTL_PROPERTY_NAME, str);
        }
    }

    private final void fireFingerprintRequest() {
        this.fingerprintDataRepository.refresh();
    }

    private final void fireAnalyticsRequest(AnalyticsEvent analyticsEvent, String str) {
        fireAnalyticsRequest$stripe_release(AnalyticsDataFactory.createParams$stripe_release$default(this.analyticsDataFactory, analyticsEvent, (Set) null, (String) null, (String) null, (Map) null, 30, (Object) null), str);
    }

    public final void fireAnalyticsRequest$stripe_release(Map<String, ? extends Object> map, String str) {
        Intrinsics.checkParameterIsNotNull(map, NativeProtocol.WEB_DIALOG_PARAMS);
        Intrinsics.checkParameterIsNotNull(str, "publishableKey");
        makeAnalyticsRequest(this.analyticsRequestFactory.create$stripe_release(map, new ApiRequest.Options(str, (String) null, (String) null, 6, (DefaultConstructorMarker) null), this.appInfo));
    }

    private final Map<String, Object> createClientSecretParam(String str, List<String> list) {
        return MapsKt.plus(MapsKt.mapOf(TuplesKt.to("client_secret", str)), Companion.createExpandParam(list));
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B3\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\b\u0012\u0006\u0010\t\u001a\u00020\n\u0012\f\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\u00020\f¢\u0006\u0002\u0010\rJ\u0013\u0010\u000e\u001a\u00020\u0002H@ø\u0001\u0000¢\u0006\u0004\b\u000f\u0010\u0010R\u000e\u0010\u0005\u001a\u00020\u0006X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\bX\u0004¢\u0006\u0002\n\u0000\u0002\u0004\n\u0002\b\u0019¨\u0006\u0011"}, d2 = {"Lcom/stripe/android/StripeApiRepository$Start3ds2AuthTask;", "Lcom/stripe/android/ApiOperation;", "Lcom/stripe/android/model/Stripe3ds2AuthResult;", "stripeApiRepository", "Lcom/stripe/android/StripeApiRepository;", "params", "Lcom/stripe/android/Stripe3ds2AuthParams;", "stripeIntentId", "", "requestOptions", "Lcom/stripe/android/ApiRequest$Options;", "callback", "Lcom/stripe/android/ApiResultCallback;", "(Lcom/stripe/android/StripeApiRepository;Lcom/stripe/android/Stripe3ds2AuthParams;Ljava/lang/String;Lcom/stripe/android/ApiRequest$Options;Lcom/stripe/android/ApiResultCallback;)V", "getResult", "getResult$stripe_release", "(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: StripeApiRepository.kt */
    private static final class Start3ds2AuthTask extends ApiOperation<Stripe3ds2AuthResult> {
        private final Stripe3ds2AuthParams params;
        private final ApiRequest.Options requestOptions;
        private final StripeApiRepository stripeApiRepository;
        private final String stripeIntentId;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Start3ds2AuthTask(StripeApiRepository stripeApiRepository2, Stripe3ds2AuthParams stripe3ds2AuthParams, String str, ApiRequest.Options options, ApiResultCallback<Stripe3ds2AuthResult> apiResultCallback) {
            super((CoroutineScope) null, apiResultCallback, 1, (DefaultConstructorMarker) null);
            Intrinsics.checkParameterIsNotNull(stripeApiRepository2, "stripeApiRepository");
            Intrinsics.checkParameterIsNotNull(stripe3ds2AuthParams, NativeProtocol.WEB_DIALOG_PARAMS);
            Intrinsics.checkParameterIsNotNull(str, "stripeIntentId");
            Intrinsics.checkParameterIsNotNull(options, "requestOptions");
            Intrinsics.checkParameterIsNotNull(apiResultCallback, "callback");
            this.stripeApiRepository = stripeApiRepository2;
            this.params = stripe3ds2AuthParams;
            this.stripeIntentId = str;
            this.requestOptions = options;
        }

        public Object getResult$stripe_release(Continuation<? super Stripe3ds2AuthResult> continuation) throws StripeException, JSONException {
            return this.stripeApiRepository.start3ds2Auth$stripe_release(this.params, this.stripeIntentId, this.requestOptions);
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B+\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\b\u0012\f\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00020\n¢\u0006\u0002\u0010\u000bJ\u0013\u0010\f\u001a\u00020\u0002H@ø\u0001\u0000¢\u0006\u0004\b\r\u0010\u000eR\u000e\u0010\u0007\u001a\u00020\bX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0004¢\u0006\u0002\n\u0000\u0002\u0004\n\u0002\b\u0019¨\u0006\u000f"}, d2 = {"Lcom/stripe/android/StripeApiRepository$Complete3ds2AuthTask;", "Lcom/stripe/android/ApiOperation;", "", "stripeApiRepository", "Lcom/stripe/android/StripeApiRepository;", "sourceId", "", "requestOptions", "Lcom/stripe/android/ApiRequest$Options;", "callback", "Lcom/stripe/android/ApiResultCallback;", "(Lcom/stripe/android/StripeApiRepository;Ljava/lang/String;Lcom/stripe/android/ApiRequest$Options;Lcom/stripe/android/ApiResultCallback;)V", "getResult", "getResult$stripe_release", "(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: StripeApiRepository.kt */
    private static final class Complete3ds2AuthTask extends ApiOperation<Boolean> {
        private final ApiRequest.Options requestOptions;
        private final String sourceId;
        private final StripeApiRepository stripeApiRepository;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Complete3ds2AuthTask(StripeApiRepository stripeApiRepository2, String str, ApiRequest.Options options, ApiResultCallback<Boolean> apiResultCallback) {
            super((CoroutineScope) null, apiResultCallback, 1, (DefaultConstructorMarker) null);
            Intrinsics.checkParameterIsNotNull(stripeApiRepository2, "stripeApiRepository");
            Intrinsics.checkParameterIsNotNull(str, "sourceId");
            Intrinsics.checkParameterIsNotNull(options, "requestOptions");
            Intrinsics.checkParameterIsNotNull(apiResultCallback, "callback");
            this.stripeApiRepository = stripeApiRepository2;
            this.sourceId = str;
            this.requestOptions = options;
        }

        public Object getResult$stripe_release(Continuation<? super Boolean> continuation) throws StripeException {
            return Boxing.boxBoolean(this.stripeApiRepository.complete3ds2Auth$stripe_release(this.sourceId, this.requestOptions));
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B9\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\b\u0012\f\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00060\n\u0012\f\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\u00020\f¢\u0006\u0002\u0010\rJ\u0015\u0010\u000e\u001a\u0004\u0018\u00010\u0002H@ø\u0001\u0000¢\u0006\u0004\b\u000f\u0010\u0010R\u000e\u0010\u0005\u001a\u00020\u0006X\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00060\nX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\bX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0004¢\u0006\u0002\n\u0000\u0002\u0004\n\u0002\b\u0019¨\u0006\u0011"}, d2 = {"Lcom/stripe/android/StripeApiRepository$RetrieveIntentTask;", "Lcom/stripe/android/ApiOperation;", "Lcom/stripe/android/model/StripeIntent;", "stripeRepository", "Lcom/stripe/android/StripeRepository;", "clientSecret", "", "requestOptions", "Lcom/stripe/android/ApiRequest$Options;", "expandFields", "", "callback", "Lcom/stripe/android/ApiResultCallback;", "(Lcom/stripe/android/StripeRepository;Ljava/lang/String;Lcom/stripe/android/ApiRequest$Options;Ljava/util/List;Lcom/stripe/android/ApiResultCallback;)V", "getResult", "getResult$stripe_release", "(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: StripeApiRepository.kt */
    private static final class RetrieveIntentTask extends ApiOperation<StripeIntent> {
        private final String clientSecret;
        private final List<String> expandFields;
        private final ApiRequest.Options requestOptions;
        private final StripeRepository stripeRepository;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public RetrieveIntentTask(StripeRepository stripeRepository2, String str, ApiRequest.Options options, List<String> list, ApiResultCallback<StripeIntent> apiResultCallback) {
            super((CoroutineScope) null, apiResultCallback, 1, (DefaultConstructorMarker) null);
            Intrinsics.checkParameterIsNotNull(stripeRepository2, "stripeRepository");
            Intrinsics.checkParameterIsNotNull(str, "clientSecret");
            Intrinsics.checkParameterIsNotNull(options, "requestOptions");
            Intrinsics.checkParameterIsNotNull(list, "expandFields");
            Intrinsics.checkParameterIsNotNull(apiResultCallback, "callback");
            this.stripeRepository = stripeRepository2;
            this.clientSecret = str;
            this.requestOptions = options;
            this.expandFields = list;
        }

        public Object getResult$stripe_release(Continuation<? super StripeIntent> continuation) throws StripeException {
            if (StringsKt.startsWith$default(this.clientSecret, "pi_", false, 2, (Object) null)) {
                return this.stripeRepository.retrievePaymentIntent(this.clientSecret, this.requestOptions, this.expandFields);
            }
            if (StringsKt.startsWith$default(this.clientSecret, "seti_", false, 2, (Object) null)) {
                return this.stripeRepository.retrieveSetupIntent(this.clientSecret, this.requestOptions, this.expandFields);
            }
            return null;
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B3\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0002\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t\u0012\f\u0010\n\u001a\b\u0012\u0004\u0012\u00020\u00020\u000b¢\u0006\u0002\u0010\fJ\u0015\u0010\r\u001a\u0004\u0018\u00010\u0002H@ø\u0001\u0000¢\u0006\u0004\b\u000e\u0010\u000fR\u000e\u0010\b\u001a\u00020\tX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0002X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0004¢\u0006\u0002\n\u0000\u0002\u0004\n\u0002\b\u0019¨\u0006\u0010"}, d2 = {"Lcom/stripe/android/StripeApiRepository$CancelIntentTask;", "Lcom/stripe/android/ApiOperation;", "Lcom/stripe/android/model/StripeIntent;", "stripeRepository", "Lcom/stripe/android/StripeRepository;", "stripeIntent", "sourceId", "", "requestOptions", "Lcom/stripe/android/ApiRequest$Options;", "callback", "Lcom/stripe/android/ApiResultCallback;", "(Lcom/stripe/android/StripeRepository;Lcom/stripe/android/model/StripeIntent;Ljava/lang/String;Lcom/stripe/android/ApiRequest$Options;Lcom/stripe/android/ApiResultCallback;)V", "getResult", "getResult$stripe_release", "(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: StripeApiRepository.kt */
    private static final class CancelIntentTask extends ApiOperation<StripeIntent> {
        private final ApiRequest.Options requestOptions;
        private final String sourceId;
        private final StripeIntent stripeIntent;
        private final StripeRepository stripeRepository;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public CancelIntentTask(StripeRepository stripeRepository2, StripeIntent stripeIntent2, String str, ApiRequest.Options options, ApiResultCallback<StripeIntent> apiResultCallback) {
            super((CoroutineScope) null, apiResultCallback, 1, (DefaultConstructorMarker) null);
            Intrinsics.checkParameterIsNotNull(stripeRepository2, "stripeRepository");
            Intrinsics.checkParameterIsNotNull(stripeIntent2, "stripeIntent");
            Intrinsics.checkParameterIsNotNull(str, "sourceId");
            Intrinsics.checkParameterIsNotNull(options, "requestOptions");
            Intrinsics.checkParameterIsNotNull(apiResultCallback, "callback");
            this.stripeRepository = stripeRepository2;
            this.stripeIntent = stripeIntent2;
            this.sourceId = str;
            this.requestOptions = options;
        }

        public Object getResult$stripe_release(Continuation<? super StripeIntent> continuation) throws StripeException {
            StripeIntent stripeIntent2 = this.stripeIntent;
            String str = "";
            if (stripeIntent2 instanceof PaymentIntent) {
                StripeRepository stripeRepository2 = this.stripeRepository;
                String id = stripeIntent2.getId();
                if (id != null) {
                    str = id;
                }
                return stripeRepository2.cancelPaymentIntentSource(str, this.sourceId, this.requestOptions);
            } else if (!(stripeIntent2 instanceof SetupIntent)) {
                return null;
            } else {
                StripeRepository stripeRepository3 = this.stripeRepository;
                String id2 = stripeIntent2.getId();
                if (id2 != null) {
                    str = id2;
                }
                return stripeRepository3.cancelSetupIntentSource(str, this.sourceId, this.requestOptions);
            }
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B3\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0006\u0012\u0006\u0010\b\u001a\u00020\t\u0012\f\u0010\n\u001a\b\u0012\u0004\u0012\u00020\u00020\u000b¢\u0006\u0002\u0010\fJ\u0015\u0010\r\u001a\u0004\u0018\u00010\u0002H@ø\u0001\u0000¢\u0006\u0004\b\u000e\u0010\u000fR\u000e\u0010\u0007\u001a\u00020\u0006X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0004¢\u0006\u0002\n\u0000\u0002\u0004\n\u0002\b\u0019¨\u0006\u0010"}, d2 = {"Lcom/stripe/android/StripeApiRepository$RetrieveSourceTask;", "Lcom/stripe/android/ApiOperation;", "Lcom/stripe/android/model/Source;", "stripeRepository", "Lcom/stripe/android/StripeRepository;", "sourceId", "", "clientSecret", "requestOptions", "Lcom/stripe/android/ApiRequest$Options;", "callback", "Lcom/stripe/android/ApiResultCallback;", "(Lcom/stripe/android/StripeRepository;Ljava/lang/String;Ljava/lang/String;Lcom/stripe/android/ApiRequest$Options;Lcom/stripe/android/ApiResultCallback;)V", "getResult", "getResult$stripe_release", "(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: StripeApiRepository.kt */
    private static final class RetrieveSourceTask extends ApiOperation<Source> {
        private final String clientSecret;
        private final ApiRequest.Options requestOptions;
        private final String sourceId;
        private final StripeRepository stripeRepository;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public RetrieveSourceTask(StripeRepository stripeRepository2, String str, String str2, ApiRequest.Options options, ApiResultCallback<Source> apiResultCallback) {
            super((CoroutineScope) null, apiResultCallback, 1, (DefaultConstructorMarker) null);
            Intrinsics.checkParameterIsNotNull(stripeRepository2, "stripeRepository");
            Intrinsics.checkParameterIsNotNull(str, "sourceId");
            Intrinsics.checkParameterIsNotNull(str2, "clientSecret");
            Intrinsics.checkParameterIsNotNull(options, "requestOptions");
            Intrinsics.checkParameterIsNotNull(apiResultCallback, "callback");
            this.stripeRepository = stripeRepository2;
            this.sourceId = str;
            this.clientSecret = str2;
            this.requestOptions = options;
        }

        public Object getResult$stripe_release(Continuation<? super Source> continuation) throws StripeException {
            return this.stripeRepository.retrieveSource(this.sourceId, this.clientSecret, this.requestOptions);
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\b\n\u0002\u0010$\n\u0002\u0010 \n\u0002\b\n\n\u0002\u0010\u0011\n\u0002\b \b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J(\u0010\f\u001a\u0014\u0012\u0004\u0012\u00020\u0004\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00040\u000e0\r2\f\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u00040\u000eH\u0002J$\u0010\u0010\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00040\r2\u0006\u0010\u0011\u001a\u00020\u00042\u0006\u0010\u0012\u001a\u00020\u0004H\u0002J\u0015\u0010\u0013\u001a\u00020\u00042\u0006\u0010\u0014\u001a\u00020\u0004H\u0001¢\u0006\u0002\b\u0015J\u0010\u0010\u0016\u001a\u00020\u00042\u0006\u0010\u0017\u001a\u00020\u0004H\u0002J)\u0010\u0016\u001a\u00020\u00042\u0006\u0010\u0017\u001a\u00020\u00042\u0012\u0010\u0018\u001a\n\u0012\u0006\b\u0001\u0012\u00020\u00010\u0019\"\u00020\u0001H\u0002¢\u0006\u0002\u0010\u001aJ\u0015\u0010\u001b\u001a\u00020\u00042\u0006\u0010\u001c\u001a\u00020\u0004H\u0001¢\u0006\u0002\b\u001dJ\u0015\u0010\u001e\u001a\u00020\u00042\u0006\u0010\u001f\u001a\u00020\u0004H\u0001¢\u0006\u0002\b J\u0015\u0010!\u001a\u00020\u00042\u0006\u0010\"\u001a\u00020\u0004H\u0001¢\u0006\u0002\b#J\u0015\u0010$\u001a\u00020\u00042\u0006\u0010\u001f\u001a\u00020\u0004H\u0001¢\u0006\u0002\b%J\u0015\u0010&\u001a\u00020\u00042\u0006\u0010\"\u001a\u00020\u0004H\u0001¢\u0006\u0002\b'J\u001d\u0010(\u001a\u00020\u00042\u0006\u0010\u0014\u001a\u00020\u00042\u0006\u0010)\u001a\u00020\u0004H\u0001¢\u0006\u0002\b*J\u0015\u0010+\u001a\u00020\u00042\u0006\u0010,\u001a\u00020\u0004H\u0001¢\u0006\u0002\b-J\u0015\u0010.\u001a\u00020\u00042\u0006\u0010\u0014\u001a\u00020\u0004H\u0001¢\u0006\u0002\b/J\u0015\u00100\u001a\u00020\u00042\u0006\u0010\u001f\u001a\u00020\u0004H\u0001¢\u0006\u0002\b1J\u0015\u00102\u001a\u00020\u00042\u0006\u0010\"\u001a\u00020\u0004H\u0001¢\u0006\u0002\b3J\u0015\u00104\u001a\u00020\u00042\u0006\u0010)\u001a\u00020\u0004H\u0001¢\u0006\u0002\b5J\u0015\u00106\u001a\u00020\u00042\u0006\u00107\u001a\u00020\u0004H\u0001¢\u0006\u0002\b8R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u0014\u0010\u0005\u001a\u00020\u00048@X\u0004¢\u0006\u0006\u001a\u0004\b\u0006\u0010\u0007R\u0014\u0010\b\u001a\u00020\u00048@X\u0004¢\u0006\u0006\u001a\u0004\b\t\u0010\u0007R\u0014\u0010\n\u001a\u00020\u00048@X\u0004¢\u0006\u0006\u001a\u0004\b\u000b\u0010\u0007\u0002\u0004\n\u0002\b\u0019¨\u00069"}, d2 = {"Lcom/stripe/android/StripeApiRepository$Companion;", "", "()V", "DNS_CACHE_TTL_PROPERTY_NAME", "", "paymentMethodsUrl", "getPaymentMethodsUrl$stripe_release", "()Ljava/lang/String;", "sourcesUrl", "getSourcesUrl$stripe_release", "tokensUrl", "getTokensUrl$stripe_release", "createExpandParam", "", "", "expandFields", "createVerificationParam", "verificationId", "userOneTimeCode", "getAddCustomerSourceUrl", "customerId", "getAddCustomerSourceUrl$stripe_release", "getApiUrl", "path", "args", "", "(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;", "getAttachPaymentMethodUrl", "paymentMethodId", "getAttachPaymentMethodUrl$stripe_release", "getCancelPaymentIntentSourceUrl", "paymentIntentId", "getCancelPaymentIntentSourceUrl$stripe_release", "getCancelSetupIntentSourceUrl", "setupIntentId", "getCancelSetupIntentSourceUrl$stripe_release", "getConfirmPaymentIntentUrl", "getConfirmPaymentIntentUrl$stripe_release", "getConfirmSetupIntentUrl", "getConfirmSetupIntentUrl$stripe_release", "getDeleteCustomerSourceUrl", "sourceId", "getDeleteCustomerSourceUrl$stripe_release", "getIssuingCardPinUrl", "cardId", "getIssuingCardPinUrl$stripe_release", "getRetrieveCustomerUrl", "getRetrieveCustomerUrl$stripe_release", "getRetrievePaymentIntentUrl", "getRetrievePaymentIntentUrl$stripe_release", "getRetrieveSetupIntentUrl", "getRetrieveSetupIntentUrl$stripe_release", "getRetrieveSourceApiUrl", "getRetrieveSourceApiUrl$stripe_release", "getRetrieveTokenApiUrl", "tokenId", "getRetrieveTokenApiUrl$stripe_release", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: StripeApiRepository.kt */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        /* access modifiers changed from: private */
        public final Map<String, String> createVerificationParam(String str, String str2) {
            return MapsKt.mapOf(TuplesKt.to("id", str), TuplesKt.to("one_time_code", str2));
        }

        public final /* synthetic */ String getTokensUrl$stripe_release() {
            return StripeApiRepository.Companion.getApiUrl("tokens");
        }

        public final /* synthetic */ String getSourcesUrl$stripe_release() {
            return StripeApiRepository.Companion.getApiUrl("sources");
        }

        public final /* synthetic */ String getPaymentMethodsUrl$stripe_release() {
            return StripeApiRepository.Companion.getApiUrl("payment_methods");
        }

        public final /* synthetic */ String getRetrievePaymentIntentUrl$stripe_release(String str) {
            Intrinsics.checkParameterIsNotNull(str, "paymentIntentId");
            return getApiUrl("payment_intents/%s", str);
        }

        public final /* synthetic */ String getConfirmPaymentIntentUrl$stripe_release(String str) {
            Intrinsics.checkParameterIsNotNull(str, "paymentIntentId");
            return getApiUrl("payment_intents/%s/confirm", str);
        }

        public final /* synthetic */ String getCancelPaymentIntentSourceUrl$stripe_release(String str) {
            Intrinsics.checkParameterIsNotNull(str, "paymentIntentId");
            return getApiUrl("payment_intents/%s/source_cancel", str);
        }

        public final /* synthetic */ String getRetrieveSetupIntentUrl$stripe_release(String str) {
            Intrinsics.checkParameterIsNotNull(str, "setupIntentId");
            return getApiUrl("setup_intents/%s", str);
        }

        public final /* synthetic */ String getConfirmSetupIntentUrl$stripe_release(String str) {
            Intrinsics.checkParameterIsNotNull(str, "setupIntentId");
            return getApiUrl("setup_intents/%s/confirm", str);
        }

        public final /* synthetic */ String getCancelSetupIntentSourceUrl$stripe_release(String str) {
            Intrinsics.checkParameterIsNotNull(str, "setupIntentId");
            return getApiUrl("setup_intents/%s/source_cancel", str);
        }

        public final /* synthetic */ String getAddCustomerSourceUrl$stripe_release(String str) {
            Intrinsics.checkParameterIsNotNull(str, "customerId");
            return getApiUrl("customers/%s/sources", str);
        }

        public final /* synthetic */ String getDeleteCustomerSourceUrl$stripe_release(String str, String str2) {
            Intrinsics.checkParameterIsNotNull(str, "customerId");
            Intrinsics.checkParameterIsNotNull(str2, "sourceId");
            return getApiUrl("customers/%s/sources/%s", str, str2);
        }

        public final /* synthetic */ String getAttachPaymentMethodUrl$stripe_release(String str) {
            Intrinsics.checkParameterIsNotNull(str, "paymentMethodId");
            return getApiUrl("payment_methods/%s/attach", str);
        }

        public final /* synthetic */ String getRetrieveCustomerUrl$stripe_release(String str) {
            Intrinsics.checkParameterIsNotNull(str, "customerId");
            return getApiUrl("customers/%s", str);
        }

        public final /* synthetic */ String getRetrieveSourceApiUrl$stripe_release(String str) {
            Intrinsics.checkParameterIsNotNull(str, "sourceId");
            return getApiUrl("sources/%s", str);
        }

        public final /* synthetic */ String getRetrieveTokenApiUrl$stripe_release(String str) {
            Intrinsics.checkParameterIsNotNull(str, "tokenId");
            return getApiUrl("tokens/%s", str);
        }

        public final /* synthetic */ String getIssuingCardPinUrl$stripe_release(String str) {
            Intrinsics.checkParameterIsNotNull(str, "cardId");
            return getApiUrl("issuing/cards/%s/pin", str);
        }

        /* access modifiers changed from: private */
        public final String getApiUrl(String str, Object... objArr) {
            StringCompanionObject stringCompanionObject = StringCompanionObject.INSTANCE;
            Locale locale = Locale.ENGLISH;
            Intrinsics.checkExpressionValueIsNotNull(locale, "Locale.ENGLISH");
            Object[] copyOf = Arrays.copyOf(objArr, objArr.length);
            String format = String.format(locale, str, Arrays.copyOf(copyOf, copyOf.length));
            Intrinsics.checkExpressionValueIsNotNull(format, "java.lang.String.format(locale, format, *args)");
            return getApiUrl(format);
        }

        /* access modifiers changed from: private */
        public final String getApiUrl(String str) {
            return "https://api.stripe.com/v1/" + str;
        }

        /* access modifiers changed from: private */
        public final Map<String, List<String>> createExpandParam(List<String> list) {
            Map<String, List<String>> map = null;
            if (!(!list.isEmpty())) {
                list = null;
            }
            if (list != null) {
                map = MapsKt.mapOf(TuplesKt.to("expand", list));
            }
            return map != null ? map : MapsKt.emptyMap();
        }
    }
}
