package com.stripe.android.view;

import androidx.lifecycle.MutableLiveData;
import com.stripe.android.PaymentSessionConfig;
import com.stripe.android.model.ShippingInformation;
import com.stripe.android.model.ShippingMethod;
import java.util.List;
import kotlin.Metadata;
import kotlin.Result;
import kotlin.ResultKt;
import kotlin.Unit;
import kotlin.collections.CollectionsKt;
import kotlin.coroutines.Continuation;
import kotlin.coroutines.intrinsics.IntrinsicsKt;
import kotlin.coroutines.jvm.internal.DebugMetadata;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.Intrinsics;
import kotlinx.coroutines.CoroutineScope;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\u0010\u0000\u001a\u00020\u0001*\u00020\u0002H@¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"<anonymous>", "", "Lkotlinx/coroutines/CoroutineScope;", "invoke", "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;"}, k = 3, mv = {1, 1, 16})
@DebugMetadata(c = "com.stripe.android.view.PaymentFlowViewModel$validateShippingInformation$1", f = "PaymentFlowViewModel.kt", i = {}, l = {}, m = "invokeSuspend", n = {}, s = {})
/* compiled from: PaymentFlowViewModel.kt */
final class PaymentFlowViewModel$validateShippingInformation$1 extends SuspendLambda implements Function2<CoroutineScope, Continuation<? super Unit>, Object> {
    final /* synthetic */ MutableLiveData $resultData;
    final /* synthetic */ PaymentSessionConfig.ShippingInformationValidator $shippingInfoValidator;
    final /* synthetic */ ShippingInformation $shippingInformation;
    final /* synthetic */ PaymentSessionConfig.ShippingMethodsFactory $shippingMethodsFactory;
    int label;
    private CoroutineScope p$;
    final /* synthetic */ PaymentFlowViewModel this$0;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    PaymentFlowViewModel$validateShippingInformation$1(PaymentFlowViewModel paymentFlowViewModel, PaymentSessionConfig.ShippingInformationValidator shippingInformationValidator, ShippingInformation shippingInformation, PaymentSessionConfig.ShippingMethodsFactory shippingMethodsFactory, MutableLiveData mutableLiveData, Continuation continuation) {
        super(2, continuation);
        this.this$0 = paymentFlowViewModel;
        this.$shippingInfoValidator = shippingInformationValidator;
        this.$shippingInformation = shippingInformation;
        this.$shippingMethodsFactory = shippingMethodsFactory;
        this.$resultData = mutableLiveData;
    }

    public final Continuation<Unit> create(Object obj, Continuation<?> continuation) {
        Intrinsics.checkParameterIsNotNull(continuation, "completion");
        PaymentFlowViewModel$validateShippingInformation$1 paymentFlowViewModel$validateShippingInformation$1 = new PaymentFlowViewModel$validateShippingInformation$1(this.this$0, this.$shippingInfoValidator, this.$shippingInformation, this.$shippingMethodsFactory, this.$resultData, continuation);
        paymentFlowViewModel$validateShippingInformation$1.p$ = (CoroutineScope) obj;
        return paymentFlowViewModel$validateShippingInformation$1;
    }

    public final Object invoke(Object obj, Object obj2) {
        return ((PaymentFlowViewModel$validateShippingInformation$1) create(obj, (Continuation) obj2)).invokeSuspend(Unit.INSTANCE);
    }

    public final Object invokeSuspend(Object obj) {
        IntrinsicsKt.getCOROUTINE_SUSPENDED();
        if (this.label == 0) {
            ResultKt.throwOnFailure(obj);
            if (this.$shippingInfoValidator.isValid(this.$shippingInformation)) {
                PaymentSessionConfig.ShippingMethodsFactory shippingMethodsFactory = this.$shippingMethodsFactory;
                List<ShippingMethod> create = shippingMethodsFactory != null ? shippingMethodsFactory.create(this.$shippingInformation) : null;
                if (create == null) {
                    create = CollectionsKt.emptyList();
                }
                this.this$0.setShippingMethods$stripe_release(create);
                MutableLiveData mutableLiveData = this.$resultData;
                Result.Companion companion = Result.Companion;
                mutableLiveData.postValue(Result.m3boximpl(Result.m4constructorimpl(create)));
            } else {
                String errorMessage = this.$shippingInfoValidator.getErrorMessage(this.$shippingInformation);
                this.this$0.setShippingMethods$stripe_release(CollectionsKt.emptyList());
                MutableLiveData mutableLiveData2 = this.$resultData;
                Result.Companion companion2 = Result.Companion;
                mutableLiveData2.postValue(Result.m3boximpl(Result.m4constructorimpl(ResultKt.createFailure(new RuntimeException(errorMessage)))));
            }
            return Unit.INSTANCE;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
