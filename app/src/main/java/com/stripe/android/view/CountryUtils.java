package com.stripe.android.view;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt;
import kotlin.collections.SetsKt;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\"\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\u0002\bÀ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0015\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\bH\u0000¢\u0006\u0002\b\u000eJ\u0017\u0010\u000f\u001a\u0004\u0018\u00010\u00052\u0006\u0010\r\u001a\u00020\bH\u0000¢\u0006\u0002\b\u0010J\u0017\u0010\u0011\u001a\u0004\u0018\u00010\u00052\u0006\u0010\u0012\u001a\u00020\bH\u0000¢\u0006\u0002\b\u0013J\u001b\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u00050\u00042\u0006\u0010\u0015\u001a\u00020\u0016H\u0000¢\u0006\u0002\b\u0017R\u0014\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X\u0004¢\u0006\u0002\n\u0000R\u001a\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\b0\u0007X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n¨\u0006\u0018"}, d2 = {"Lcom/stripe/android/view/CountryUtils;", "", "()V", "COUNTRIES", "", "Lcom/stripe/android/view/Country;", "NO_POSTAL_CODE_COUNTRIES", "", "", "getNO_POSTAL_CODE_COUNTRIES$stripe_release", "()Ljava/util/Set;", "doesCountryUsePostalCode", "", "countryCode", "doesCountryUsePostalCode$stripe_release", "getCountryByCode", "getCountryByCode$stripe_release", "getCountryByName", "countryName", "getCountryByName$stripe_release", "getOrderedCountries", "currentLocale", "Ljava/util/Locale;", "getOrderedCountries$stripe_release", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: CountryUtils.kt */
public final class CountryUtils {
    private static final List<Country> COUNTRIES;
    public static final CountryUtils INSTANCE = new CountryUtils();
    private static final Set<String> NO_POSTAL_CODE_COUNTRIES = SetsKt.setOf("AE", "AG", "AN", "AO", "AW", "BF", "BI", "BJ", "BO", "BS", "BW", "BZ", "CD", "CF", "CG", "CI", "CK", "CM", "DJ", "DM", "ER", "FJ", "GD", "GH", "GM", "GN", "GQ", "GY", "HK", "IE", "JM", "KE", "KI", "KM", "KN", "KP", "LC", "ML", "MO", "MR", "MS", "MU", "MW", "NR", "NU", "PA", "QA", "RW", "SB", "SC", "SL", "SO", "SR", "ST", "SY", "TF", "TK", "TL", "TO", "TT", "TV", "TZ", "UG", "VU", "YE", "ZA", "ZW");

    static {
        String[] iSOCountries = Locale.getISOCountries();
        Intrinsics.checkExpressionValueIsNotNull(iSOCountries, "Locale.getISOCountries()");
        Collection arrayList = new ArrayList(iSOCountries.length);
        for (String str : iSOCountries) {
            Intrinsics.checkExpressionValueIsNotNull(str, "code");
            String displayCountry = new Locale("", str).getDisplayCountry();
            Intrinsics.checkExpressionValueIsNotNull(displayCountry, "Locale(\"\", code).displayCountry");
            arrayList.add(new Country(str, displayCountry));
        }
        COUNTRIES = (List) arrayList;
    }

    private CountryUtils() {
    }

    public final Set<String> getNO_POSTAL_CODE_COUNTRIES$stripe_release() {
        return NO_POSTAL_CODE_COUNTRIES;
    }

    public final /* synthetic */ Country getCountryByName$stripe_release(String str) {
        Object obj;
        Intrinsics.checkParameterIsNotNull(str, "countryName");
        Iterator it = COUNTRIES.iterator();
        while (true) {
            if (!it.hasNext()) {
                obj = null;
                break;
            }
            obj = it.next();
            if (Intrinsics.areEqual((Object) ((Country) obj).getName(), (Object) str)) {
                break;
            }
        }
        return (Country) obj;
    }

    public final /* synthetic */ Country getCountryByCode$stripe_release(String str) {
        Object obj;
        Intrinsics.checkParameterIsNotNull(str, "countryCode");
        Iterator it = COUNTRIES.iterator();
        while (true) {
            if (!it.hasNext()) {
                obj = null;
                break;
            }
            obj = it.next();
            if (Intrinsics.areEqual((Object) ((Country) obj).getCode(), (Object) str)) {
                break;
            }
        }
        return (Country) obj;
    }

    public final /* synthetic */ List<Country> getOrderedCountries$stripe_release(Locale locale) {
        Intrinsics.checkParameterIsNotNull(locale, "currentLocale");
        String country = locale.getCountry();
        Intrinsics.checkExpressionValueIsNotNull(country, "currentLocale.country");
        Collection listOfNotNull = CollectionsKt.listOfNotNull(getCountryByCode$stripe_release(country));
        Collection arrayList = new ArrayList();
        for (Object next : CollectionsKt.sortedWith(COUNTRIES, new CountryUtils$getOrderedCountries$$inlined$sortedBy$1())) {
            if (!Intrinsics.areEqual((Object) ((Country) next).getCode(), (Object) locale.getCountry())) {
                arrayList.add(next);
            }
        }
        return CollectionsKt.plus(listOfNotNull, (List) arrayList);
    }

    public final /* synthetic */ boolean doesCountryUsePostalCode$stripe_release(String str) {
        Intrinsics.checkParameterIsNotNull(str, "countryCode");
        return !NO_POSTAL_CODE_COUNTRIES.contains(str);
    }
}
