package com.stripe.android.view;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.PagerAdapter;
import com.stripe.android.PaymentSessionConfig;
import com.stripe.android.model.ShippingInformation;
import com.stripe.android.model.ShippingMethod;
import java.util.List;
import java.util.Set;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.Unit;
import kotlin.collections.CollectionsKt;
import kotlin.collections.SetsKt;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000p\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\"\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0006\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u000b\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0004\n\u0002\u0010\r\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u0000\u0018\u00002\u00020\u0001:\u00019B;\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u000e\b\u0002\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\b0\u0007\u0012\u0014\b\u0002\u0010\t\u001a\u000e\u0012\u0004\u0012\u00020\u000b\u0012\u0004\u0012\u00020\f0\n¢\u0006\u0002\u0010\rJ \u0010)\u001a\u00020\f2\u0006\u0010*\u001a\u00020+2\u0006\u0010,\u001a\u00020-2\u0006\u0010.\u001a\u00020/H\u0016J\b\u00100\u001a\u00020-H\u0016J\u0017\u00101\u001a\u0004\u0018\u00010\u00172\u0006\u0010,\u001a\u00020-H\u0000¢\u0006\u0002\b2J\u0012\u00103\u001a\u0004\u0018\u0001042\u0006\u0010,\u001a\u00020-H\u0016J\u0018\u00105\u001a\u00020/2\u0006\u0010*\u001a\u00020+2\u0006\u0010,\u001a\u00020-H\u0016J\u0018\u00106\u001a\u00020\u000f2\u0006\u0010.\u001a\u0002072\u0006\u00108\u001a\u00020/H\u0016R\u0014\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\b0\u0007X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R$\u0010\u0010\u001a\u00020\u000f2\u0006\u0010\u000e\u001a\u00020\u000f@@X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0011\u0010\u0012\"\u0004\b\u0013\u0010\u0014R\u001a\u0010\t\u001a\u000e\u0012\u0004\u0012\u00020\u000b\u0012\u0004\u0012\u00020\f0\nX\u0004¢\u0006\u0002\n\u0000R\u001a\u0010\u0015\u001a\b\u0012\u0004\u0012\u00020\u00170\u00168BX\u0004¢\u0006\u0006\u001a\u0004\b\u0018\u0010\u0019R\u000e\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000R(\u0010\u001a\u001a\u0004\u0018\u00010\u000b2\b\u0010\u000e\u001a\u0004\u0018\u00010\u000b@@X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u001b\u0010\u001c\"\u0004\b\u001d\u0010\u001eR(\u0010 \u001a\u0004\u0018\u00010\u001f2\b\u0010\u000e\u001a\u0004\u0018\u00010\u001f@@X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b!\u0010\"\"\u0004\b#\u0010$R0\u0010%\u001a\b\u0012\u0004\u0012\u00020\u000b0\u00162\f\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\u000b0\u0016@@X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b&\u0010\u0019\"\u0004\b'\u0010(¨\u0006:"}, d2 = {"Lcom/stripe/android/view/PaymentFlowPagerAdapter;", "Landroidx/viewpager/widget/PagerAdapter;", "context", "Landroid/content/Context;", "paymentSessionConfig", "Lcom/stripe/android/PaymentSessionConfig;", "allowedShippingCountryCodes", "", "", "onShippingMethodSelectedCallback", "Lkotlin/Function1;", "Lcom/stripe/android/model/ShippingMethod;", "", "(Landroid/content/Context;Lcom/stripe/android/PaymentSessionConfig;Ljava/util/Set;Lkotlin/jvm/functions/Function1;)V", "value", "", "isShippingInfoSubmitted", "isShippingInfoSubmitted$stripe_release", "()Z", "setShippingInfoSubmitted$stripe_release", "(Z)V", "pages", "", "Lcom/stripe/android/view/PaymentFlowPage;", "getPages", "()Ljava/util/List;", "selectedShippingMethod", "getSelectedShippingMethod$stripe_release", "()Lcom/stripe/android/model/ShippingMethod;", "setSelectedShippingMethod$stripe_release", "(Lcom/stripe/android/model/ShippingMethod;)V", "Lcom/stripe/android/model/ShippingInformation;", "shippingInformation", "getShippingInformation$stripe_release", "()Lcom/stripe/android/model/ShippingInformation;", "setShippingInformation$stripe_release", "(Lcom/stripe/android/model/ShippingInformation;)V", "shippingMethods", "getShippingMethods$stripe_release", "setShippingMethods$stripe_release", "(Ljava/util/List;)V", "destroyItem", "collection", "Landroid/view/ViewGroup;", "position", "", "view", "", "getCount", "getPageAt", "getPageAt$stripe_release", "getPageTitle", "", "instantiateItem", "isViewFromObject", "Landroid/view/View;", "o", "PaymentFlowViewHolder", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: PaymentFlowPagerAdapter.kt */
public final class PaymentFlowPagerAdapter extends PagerAdapter {
    private final Set<String> allowedShippingCountryCodes;
    private final Context context;
    private boolean isShippingInfoSubmitted;
    private final Function1<ShippingMethod, Unit> onShippingMethodSelectedCallback;
    private final PaymentSessionConfig paymentSessionConfig;
    private ShippingMethod selectedShippingMethod;
    private ShippingInformation shippingInformation;
    private List<ShippingMethod> shippingMethods;

    @Metadata(bv = {1, 0, 3}, k = 3, mv = {1, 1, 16})
    public final /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;

        static {
            int[] iArr = new int[PaymentFlowPage.values().length];
            $EnumSwitchMapping$0 = iArr;
            iArr[PaymentFlowPage.ShippingInfo.ordinal()] = 1;
            $EnumSwitchMapping$0[PaymentFlowPage.ShippingMethod.ordinal()] = 2;
        }
    }

    public boolean isViewFromObject(View view, Object obj) {
        Intrinsics.checkParameterIsNotNull(view, "view");
        Intrinsics.checkParameterIsNotNull(obj, "o");
        return view == obj;
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ PaymentFlowPagerAdapter(Context context2, PaymentSessionConfig paymentSessionConfig2, Set set, Function1 function1, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(context2, paymentSessionConfig2, (i & 4) != 0 ? SetsKt.emptySet() : set, (i & 8) != 0 ? AnonymousClass1.INSTANCE : function1);
    }

    public PaymentFlowPagerAdapter(Context context2, PaymentSessionConfig paymentSessionConfig2, Set<String> set, Function1<? super ShippingMethod, Unit> function1) {
        Intrinsics.checkParameterIsNotNull(context2, "context");
        Intrinsics.checkParameterIsNotNull(paymentSessionConfig2, "paymentSessionConfig");
        Intrinsics.checkParameterIsNotNull(set, "allowedShippingCountryCodes");
        Intrinsics.checkParameterIsNotNull(function1, "onShippingMethodSelectedCallback");
        this.context = context2;
        this.paymentSessionConfig = paymentSessionConfig2;
        this.allowedShippingCountryCodes = set;
        this.onShippingMethodSelectedCallback = function1;
        this.shippingMethods = CollectionsKt.emptyList();
    }

    private final List<PaymentFlowPage> getPages() {
        PaymentFlowPage[] paymentFlowPageArr = new PaymentFlowPage[2];
        PaymentFlowPage paymentFlowPage = PaymentFlowPage.ShippingInfo;
        PaymentFlowPage paymentFlowPage2 = null;
        if (!this.paymentSessionConfig.isShippingInfoRequired()) {
            paymentFlowPage = null;
        }
        boolean z = false;
        paymentFlowPageArr[0] = paymentFlowPage;
        PaymentFlowPage paymentFlowPage3 = PaymentFlowPage.ShippingMethod;
        if (this.paymentSessionConfig.isShippingMethodRequired() && (!this.paymentSessionConfig.isShippingInfoRequired() || this.isShippingInfoSubmitted)) {
            z = true;
        }
        if (z) {
            paymentFlowPage2 = paymentFlowPage3;
        }
        paymentFlowPageArr[1] = paymentFlowPage2;
        return CollectionsKt.listOfNotNull((T[]) paymentFlowPageArr);
    }

    public final ShippingInformation getShippingInformation$stripe_release() {
        return this.shippingInformation;
    }

    public final void setShippingInformation$stripe_release(ShippingInformation shippingInformation2) {
        this.shippingInformation = shippingInformation2;
        notifyDataSetChanged();
    }

    public final boolean isShippingInfoSubmitted$stripe_release() {
        return this.isShippingInfoSubmitted;
    }

    public final void setShippingInfoSubmitted$stripe_release(boolean z) {
        this.isShippingInfoSubmitted = z;
        notifyDataSetChanged();
    }

    public final List<ShippingMethod> getShippingMethods$stripe_release() {
        return this.shippingMethods;
    }

    public final void setShippingMethods$stripe_release(List<ShippingMethod> list) {
        Intrinsics.checkParameterIsNotNull(list, "value");
        this.shippingMethods = list;
        notifyDataSetChanged();
    }

    public final ShippingMethod getSelectedShippingMethod$stripe_release() {
        return this.selectedShippingMethod;
    }

    public final void setSelectedShippingMethod$stripe_release(ShippingMethod shippingMethod) {
        this.selectedShippingMethod = shippingMethod;
        notifyDataSetChanged();
    }

    public Object instantiateItem(ViewGroup viewGroup, int i) {
        PaymentFlowViewHolder paymentFlowViewHolder;
        Intrinsics.checkParameterIsNotNull(viewGroup, "collection");
        int i2 = WhenMappings.$EnumSwitchMapping$0[getPages().get(i).ordinal()];
        if (i2 == 1) {
            paymentFlowViewHolder = new PaymentFlowViewHolder.ShippingInformationViewHolder(viewGroup);
        } else if (i2 == 2) {
            paymentFlowViewHolder = new PaymentFlowViewHolder.ShippingMethodViewHolder(viewGroup);
        } else {
            throw new NoWhenBranchMatchedException();
        }
        if (paymentFlowViewHolder instanceof PaymentFlowViewHolder.ShippingInformationViewHolder) {
            ((PaymentFlowViewHolder.ShippingInformationViewHolder) paymentFlowViewHolder).bind(this.paymentSessionConfig, this.shippingInformation, this.allowedShippingCountryCodes);
        } else if (paymentFlowViewHolder instanceof PaymentFlowViewHolder.ShippingMethodViewHolder) {
            ((PaymentFlowViewHolder.ShippingMethodViewHolder) paymentFlowViewHolder).bind(this.shippingMethods, this.selectedShippingMethod, this.onShippingMethodSelectedCallback);
        }
        viewGroup.addView(paymentFlowViewHolder.itemView);
        View view = paymentFlowViewHolder.itemView;
        Intrinsics.checkExpressionValueIsNotNull(view, "viewHolder.itemView");
        return view;
    }

    public void destroyItem(ViewGroup viewGroup, int i, Object obj) {
        Intrinsics.checkParameterIsNotNull(viewGroup, "collection");
        Intrinsics.checkParameterIsNotNull(obj, "view");
        viewGroup.removeView((View) obj);
    }

    public int getCount() {
        return getPages().size();
    }

    public final PaymentFlowPage getPageAt$stripe_release(int i) {
        return (PaymentFlowPage) CollectionsKt.getOrNull(getPages(), i);
    }

    public CharSequence getPageTitle(int i) {
        return this.context.getString(getPages().get(i).getTitleResId());
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b0\u0018\u00002\u00020\u0001:\u0002\u0005\u0006B\u000f\b\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004\u0001\u0002\u0007\b¨\u0006\t"}, d2 = {"Lcom/stripe/android/view/PaymentFlowPagerAdapter$PaymentFlowViewHolder;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "itemView", "Landroid/view/View;", "(Landroid/view/View;)V", "ShippingInformationViewHolder", "ShippingMethodViewHolder", "Lcom/stripe/android/view/PaymentFlowPagerAdapter$PaymentFlowViewHolder$ShippingInformationViewHolder;", "Lcom/stripe/android/view/PaymentFlowPagerAdapter$PaymentFlowViewHolder$ShippingMethodViewHolder;", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: PaymentFlowPagerAdapter.kt */
    public static abstract class PaymentFlowViewHolder extends RecyclerView.ViewHolder {
        private PaymentFlowViewHolder(View view) {
            super(view);
        }

        public /* synthetic */ PaymentFlowViewHolder(View view, DefaultConstructorMarker defaultConstructorMarker) {
            this(view);
        }

        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\"\n\u0002\u0010\u000e\n\u0000\u0018\u00002\u00020\u0001B\u000f\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004B\r\u0012\u0006\u0010\u0005\u001a\u00020\u0006¢\u0006\u0002\u0010\u0007J&\u0010\n\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\r2\b\u0010\u000e\u001a\u0004\u0018\u00010\u000f2\f\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u00120\u0011R\u000e\u0010\b\u001a\u00020\tX\u0004¢\u0006\u0002\n\u0000¨\u0006\u0013"}, d2 = {"Lcom/stripe/android/view/PaymentFlowPagerAdapter$PaymentFlowViewHolder$ShippingInformationViewHolder;", "Lcom/stripe/android/view/PaymentFlowPagerAdapter$PaymentFlowViewHolder;", "root", "Landroid/view/ViewGroup;", "(Landroid/view/ViewGroup;)V", "viewBinding", "Lcom/stripe/android/databinding/ShippingInfoPageBinding;", "(Lcom/stripe/android/databinding/ShippingInfoPageBinding;)V", "shippingInfoWidget", "Lcom/stripe/android/view/ShippingInfoWidget;", "bind", "", "paymentSessionConfig", "Lcom/stripe/android/PaymentSessionConfig;", "shippingInformation", "Lcom/stripe/android/model/ShippingInformation;", "allowedShippingCountryCodes", "", "", "stripe_release"}, k = 1, mv = {1, 1, 16})
        /* compiled from: PaymentFlowPagerAdapter.kt */
        public static final class ShippingInformationViewHolder extends PaymentFlowViewHolder {
            private final ShippingInfoWidget shippingInfoWidget;

            /* JADX WARNING: Illegal instructions before constructor call */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public ShippingInformationViewHolder(com.stripe.android.databinding.ShippingInfoPageBinding r3) {
                /*
                    r2 = this;
                    java.lang.String r0 = "viewBinding"
                    kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(r3, r0)
                    android.widget.ScrollView r0 = r3.getRoot()
                    java.lang.String r1 = "viewBinding.root"
                    kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r0, r1)
                    android.view.View r0 = (android.view.View) r0
                    r1 = 0
                    r2.<init>(r0, r1)
                    com.stripe.android.view.ShippingInfoWidget r3 = r3.shippingInfoWidget
                    java.lang.String r0 = "viewBinding.shippingInfoWidget"
                    kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r3, r0)
                    r2.shippingInfoWidget = r3
                    return
                */
                throw new UnsupportedOperationException("Method not decompiled: com.stripe.android.view.PaymentFlowPagerAdapter.PaymentFlowViewHolder.ShippingInformationViewHolder.<init>(com.stripe.android.databinding.ShippingInfoPageBinding):void");
            }

            /* JADX WARNING: Illegal instructions before constructor call */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public ShippingInformationViewHolder(android.view.ViewGroup r3) {
                /*
                    r2 = this;
                    java.lang.String r0 = "root"
                    kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(r3, r0)
                    android.content.Context r0 = r3.getContext()
                    android.view.LayoutInflater r0 = android.view.LayoutInflater.from(r0)
                    r1 = 0
                    com.stripe.android.databinding.ShippingInfoPageBinding r3 = com.stripe.android.databinding.ShippingInfoPageBinding.inflate(r0, r3, r1)
                    java.lang.String r0 = "ShippingInfoPageBinding.…  false\n                )"
                    kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r3, r0)
                    r2.<init>((com.stripe.android.databinding.ShippingInfoPageBinding) r3)
                    return
                */
                throw new UnsupportedOperationException("Method not decompiled: com.stripe.android.view.PaymentFlowPagerAdapter.PaymentFlowViewHolder.ShippingInformationViewHolder.<init>(android.view.ViewGroup):void");
            }

            public final void bind(PaymentSessionConfig paymentSessionConfig, ShippingInformation shippingInformation, Set<String> set) {
                Intrinsics.checkParameterIsNotNull(paymentSessionConfig, "paymentSessionConfig");
                Intrinsics.checkParameterIsNotNull(set, "allowedShippingCountryCodes");
                this.shippingInfoWidget.setHiddenFields(paymentSessionConfig.getHiddenShippingInfoFields());
                this.shippingInfoWidget.setOptionalFields(paymentSessionConfig.getOptionalShippingInfoFields());
                this.shippingInfoWidget.setAllowedCountryCodes(set);
                this.shippingInfoWidget.populateShippingInfo(shippingInformation);
            }
        }

        @Metadata(bv = {1, 0, 3}, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u000f\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004B\r\u0012\u0006\u0010\u0005\u001a\u00020\u0006¢\u0006\u0002\u0010\u0007J2\u0010\n\u001a\u00020\u000b2\f\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u000e0\r2\b\u0010\u000f\u001a\u0004\u0018\u00010\u000e2\u0012\u0010\u0010\u001a\u000e\u0012\u0004\u0012\u00020\u000e\u0012\u0004\u0012\u00020\u000b0\u0011R\u000e\u0010\b\u001a\u00020\tX\u0004¢\u0006\u0002\n\u0000¨\u0006\u0012"}, d2 = {"Lcom/stripe/android/view/PaymentFlowPagerAdapter$PaymentFlowViewHolder$ShippingMethodViewHolder;", "Lcom/stripe/android/view/PaymentFlowPagerAdapter$PaymentFlowViewHolder;", "root", "Landroid/view/ViewGroup;", "(Landroid/view/ViewGroup;)V", "viewBinding", "Lcom/stripe/android/databinding/ShippingMethodPageBinding;", "(Lcom/stripe/android/databinding/ShippingMethodPageBinding;)V", "shippingMethodWidget", "Lcom/stripe/android/view/SelectShippingMethodWidget;", "bind", "", "shippingMethods", "", "Lcom/stripe/android/model/ShippingMethod;", "selectedShippingMethod", "onShippingMethodSelectedCallback", "Lkotlin/Function1;", "stripe_release"}, k = 1, mv = {1, 1, 16})
        /* compiled from: PaymentFlowPagerAdapter.kt */
        public static final class ShippingMethodViewHolder extends PaymentFlowViewHolder {
            private final SelectShippingMethodWidget shippingMethodWidget;

            /* JADX WARNING: Illegal instructions before constructor call */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public ShippingMethodViewHolder(com.stripe.android.databinding.ShippingMethodPageBinding r3) {
                /*
                    r2 = this;
                    java.lang.String r0 = "viewBinding"
                    kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(r3, r0)
                    android.widget.FrameLayout r0 = r3.getRoot()
                    java.lang.String r1 = "viewBinding.root"
                    kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r0, r1)
                    android.view.View r0 = (android.view.View) r0
                    r1 = 0
                    r2.<init>(r0, r1)
                    com.stripe.android.view.SelectShippingMethodWidget r3 = r3.selectShippingMethodWidget
                    java.lang.String r0 = "viewBinding.selectShippingMethodWidget"
                    kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r3, r0)
                    r2.shippingMethodWidget = r3
                    return
                */
                throw new UnsupportedOperationException("Method not decompiled: com.stripe.android.view.PaymentFlowPagerAdapter.PaymentFlowViewHolder.ShippingMethodViewHolder.<init>(com.stripe.android.databinding.ShippingMethodPageBinding):void");
            }

            /* JADX WARNING: Illegal instructions before constructor call */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public ShippingMethodViewHolder(android.view.ViewGroup r3) {
                /*
                    r2 = this;
                    java.lang.String r0 = "root"
                    kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(r3, r0)
                    android.content.Context r0 = r3.getContext()
                    android.view.LayoutInflater r0 = android.view.LayoutInflater.from(r0)
                    r1 = 0
                    com.stripe.android.databinding.ShippingMethodPageBinding r3 = com.stripe.android.databinding.ShippingMethodPageBinding.inflate(r0, r3, r1)
                    java.lang.String r0 = "ShippingMethodPageBindin…  false\n                )"
                    kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r3, r0)
                    r2.<init>((com.stripe.android.databinding.ShippingMethodPageBinding) r3)
                    return
                */
                throw new UnsupportedOperationException("Method not decompiled: com.stripe.android.view.PaymentFlowPagerAdapter.PaymentFlowViewHolder.ShippingMethodViewHolder.<init>(android.view.ViewGroup):void");
            }

            public final void bind(List<ShippingMethod> list, ShippingMethod shippingMethod, Function1<? super ShippingMethod, Unit> function1) {
                Intrinsics.checkParameterIsNotNull(list, "shippingMethods");
                Intrinsics.checkParameterIsNotNull(function1, "onShippingMethodSelectedCallback");
                this.shippingMethodWidget.setShippingMethods(list);
                this.shippingMethodWidget.setShippingMethodSelectedCallback(function1);
                if (shippingMethod != null) {
                    this.shippingMethodWidget.setSelectedShippingMethod(shippingMethod);
                }
            }
        }
    }
}
