package com.stripe.android.view;

import android.content.Context;
import android.content.res.Resources;
import android.os.Parcel;
import android.os.Parcelable;
import com.stripe.android.model.StripeJsonUtils;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.collections.CollectionsKt;
import kotlin.collections.MapsKt;
import kotlin.jvm.internal.Intrinsics;
import org.json.JSONObject;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0003\b\u0000\u0018\u0000 \u00112\u00020\u0001:\u0002\u0010\u0011B\u0019\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0002\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006B\u001d\u0012\f\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\t0\b\u0012\b\b\u0002\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\nJ\u0010\u0010\r\u001a\u0004\u0018\u00010\t2\u0006\u0010\u000e\u001a\u00020\u000fR\u001a\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\t0\bX\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u000e\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0012"}, d2 = {"Lcom/stripe/android/view/BecsDebitBanks;", "", "context", "Landroid/content/Context;", "shouldIncludeTestBank", "", "(Landroid/content/Context;Z)V", "banks", "", "Lcom/stripe/android/view/BecsDebitBanks$Bank;", "(Ljava/util/List;Z)V", "getBanks$stripe_release", "()Ljava/util/List;", "byPrefix", "bsb", "", "Bank", "Companion", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: BecsDebitBanks.kt */
public final class BecsDebitBanks {
    @Deprecated
    public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
    private static final Bank STRIPE_TEST_BANK = new Bank("00", "STRIPE", "Stripe Test Bank");
    private final List<Bank> banks;
    private final boolean shouldIncludeTestBank;

    public BecsDebitBanks(List<Bank> list, boolean z) {
        Intrinsics.checkParameterIsNotNull(list, "banks");
        this.banks = list;
        this.shouldIncludeTestBank = z;
    }

    public final List<Bank> getBanks$stripe_release() {
        return this.banks;
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ BecsDebitBanks(List list, boolean z, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this((List<Bank>) list, (i & 2) != 0 ? true : z);
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ BecsDebitBanks(Context context, boolean z, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(context, (i & 2) != 0 ? true : z);
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public BecsDebitBanks(Context context, boolean z) {
        this((List<Bank>) Companion.createBanksData(context), z);
        Intrinsics.checkParameterIsNotNull(context, "context");
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v0, resolved type: com.stripe.android.view.BecsDebitBanks$Bank} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v1, resolved type: com.stripe.android.view.BecsDebitBanks$Bank} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v5, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v1, resolved type: com.stripe.android.view.BecsDebitBanks$Bank} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v3, resolved type: com.stripe.android.view.BecsDebitBanks$Bank} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.stripe.android.view.BecsDebitBanks.Bank byPrefix(java.lang.String r7) {
        /*
            r6 = this;
            java.lang.String r0 = "bsb"
            kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(r7, r0)
            java.util.List<com.stripe.android.view.BecsDebitBanks$Bank> r0 = r6.banks
            java.util.Collection r0 = (java.util.Collection) r0
            com.stripe.android.view.BecsDebitBanks$Bank r1 = STRIPE_TEST_BANK
            boolean r2 = r6.shouldIncludeTestBank
            r3 = 0
            if (r2 == 0) goto L_0x0011
            goto L_0x0012
        L_0x0011:
            r1 = r3
        L_0x0012:
            java.util.List r1 = kotlin.collections.CollectionsKt.listOfNotNull(r1)
            java.lang.Iterable r1 = (java.lang.Iterable) r1
            java.util.List r0 = kotlin.collections.CollectionsKt.plus(r0, r1)
            java.lang.Iterable r0 = (java.lang.Iterable) r0
            java.util.Iterator r0 = r0.iterator()
        L_0x0022:
            boolean r1 = r0.hasNext()
            if (r1 == 0) goto L_0x003c
            java.lang.Object r1 = r0.next()
            r2 = r1
            com.stripe.android.view.BecsDebitBanks$Bank r2 = (com.stripe.android.view.BecsDebitBanks.Bank) r2
            java.lang.String r2 = r2.getPrefix$stripe_release()
            r4 = 0
            r5 = 2
            boolean r2 = kotlin.text.StringsKt.startsWith$default(r7, r2, r4, r5, r3)
            if (r2 == 0) goto L_0x0022
            r3 = r1
        L_0x003c:
            com.stripe.android.view.BecsDebitBanks$Bank r3 = (com.stripe.android.view.BecsDebitBanks.Bank) r3
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.stripe.android.view.BecsDebitBanks.byPrefix(java.lang.String):com.stripe.android.view.BecsDebitBanks$Bank");
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u000f\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\b\b\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0003¢\u0006\u0002\u0010\u0006J\u000e\u0010\u000b\u001a\u00020\u0003HÀ\u0003¢\u0006\u0002\b\fJ\u000e\u0010\r\u001a\u00020\u0003HÀ\u0003¢\u0006\u0002\b\u000eJ\u000e\u0010\u000f\u001a\u00020\u0003HÀ\u0003¢\u0006\u0002\b\u0010J'\u0010\u0011\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00032\b\b\u0002\u0010\u0005\u001a\u00020\u0003HÆ\u0001J\t\u0010\u0012\u001a\u00020\u0013HÖ\u0001J\u0013\u0010\u0014\u001a\u00020\u00152\b\u0010\u0016\u001a\u0004\u0018\u00010\u0017HÖ\u0003J\t\u0010\u0018\u001a\u00020\u0013HÖ\u0001J\t\u0010\u0019\u001a\u00020\u0003HÖ\u0001J\u0019\u0010\u001a\u001a\u00020\u001b2\u0006\u0010\u001c\u001a\u00020\u001d2\u0006\u0010\u001e\u001a\u00020\u0013HÖ\u0001R\u0014\u0010\u0004\u001a\u00020\u0003X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0014\u0010\u0005\u001a\u00020\u0003X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\bR\u0014\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\b¨\u0006\u001f"}, d2 = {"Lcom/stripe/android/view/BecsDebitBanks$Bank;", "Landroid/os/Parcelable;", "prefix", "", "code", "name", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", "getCode$stripe_release", "()Ljava/lang/String;", "getName$stripe_release", "getPrefix$stripe_release", "component1", "component1$stripe_release", "component2", "component2$stripe_release", "component3", "component3$stripe_release", "copy", "describeContents", "", "equals", "", "other", "", "hashCode", "toString", "writeToParcel", "", "parcel", "Landroid/os/Parcel;", "flags", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: BecsDebitBanks.kt */
    public static final class Bank implements Parcelable {
        public static final Parcelable.Creator CREATOR = new Creator();
        private final String code;
        private final String name;
        private final String prefix;

        @Metadata(bv = {1, 0, 3}, k = 3, mv = {1, 1, 16})
        public static class Creator implements Parcelable.Creator {
            public final Object createFromParcel(Parcel parcel) {
                Intrinsics.checkParameterIsNotNull(parcel, "in");
                return new Bank(parcel.readString(), parcel.readString(), parcel.readString());
            }

            public final Object[] newArray(int i) {
                return new Bank[i];
            }
        }

        public static /* synthetic */ Bank copy$default(Bank bank, String str, String str2, String str3, int i, Object obj) {
            if ((i & 1) != 0) {
                str = bank.prefix;
            }
            if ((i & 2) != 0) {
                str2 = bank.code;
            }
            if ((i & 4) != 0) {
                str3 = bank.name;
            }
            return bank.copy(str, str2, str3);
        }

        public final String component1$stripe_release() {
            return this.prefix;
        }

        public final String component2$stripe_release() {
            return this.code;
        }

        public final String component3$stripe_release() {
            return this.name;
        }

        public final Bank copy(String str, String str2, String str3) {
            Intrinsics.checkParameterIsNotNull(str, "prefix");
            Intrinsics.checkParameterIsNotNull(str2, "code");
            Intrinsics.checkParameterIsNotNull(str3, "name");
            return new Bank(str, str2, str3);
        }

        public int describeContents() {
            return 0;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Bank)) {
                return false;
            }
            Bank bank = (Bank) obj;
            return Intrinsics.areEqual((Object) this.prefix, (Object) bank.prefix) && Intrinsics.areEqual((Object) this.code, (Object) bank.code) && Intrinsics.areEqual((Object) this.name, (Object) bank.name);
        }

        public int hashCode() {
            String str = this.prefix;
            int i = 0;
            int hashCode = (str != null ? str.hashCode() : 0) * 31;
            String str2 = this.code;
            int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
            String str3 = this.name;
            if (str3 != null) {
                i = str3.hashCode();
            }
            return hashCode2 + i;
        }

        public String toString() {
            return "Bank(prefix=" + this.prefix + ", code=" + this.code + ", name=" + this.name + ")";
        }

        public void writeToParcel(Parcel parcel, int i) {
            Intrinsics.checkParameterIsNotNull(parcel, "parcel");
            parcel.writeString(this.prefix);
            parcel.writeString(this.code);
            parcel.writeString(this.name);
        }

        public Bank(String str, String str2, String str3) {
            Intrinsics.checkParameterIsNotNull(str, "prefix");
            Intrinsics.checkParameterIsNotNull(str2, "code");
            Intrinsics.checkParameterIsNotNull(str3, "name");
            this.prefix = str;
            this.code = str2;
            this.name = str3;
        }

        public final String getPrefix$stripe_release() {
            return this.prefix;
        }

        public final String getCode$stripe_release() {
            return this.code;
        }

        public final String getName$stripe_release() {
            return this.name;
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0016\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00040\u00062\u0006\u0010\u0007\u001a\u00020\bH\u0002J\u0010\u0010\t\u001a\u00020\n2\u0006\u0010\u0007\u001a\u00020\bH\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0004¢\u0006\u0002\n\u0000¨\u0006\u000b"}, d2 = {"Lcom/stripe/android/view/BecsDebitBanks$Companion;", "", "()V", "STRIPE_TEST_BANK", "Lcom/stripe/android/view/BecsDebitBanks$Bank;", "createBanksData", "", "context", "Landroid/content/Context;", "readFile", "", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: BecsDebitBanks.kt */
    private static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        /* access modifiers changed from: private */
        public final List<Bank> createBanksData(Context context) {
            Map<String, Object> jsonObjectToMap$stripe_release = StripeJsonUtils.INSTANCE.jsonObjectToMap$stripe_release(new JSONObject(readFile(context)));
            if (jsonObjectToMap$stripe_release == null) {
                jsonObjectToMap$stripe_release = MapsKt.emptyMap();
            }
            Collection arrayList = new ArrayList(jsonObjectToMap$stripe_release.size());
            for (Map.Entry next : jsonObjectToMap$stripe_release.entrySet()) {
                Object value = next.getValue();
                if (value != null) {
                    List list = (List) value;
                    arrayList.add(new Bank((String) next.getKey(), String.valueOf(CollectionsKt.first(list)), String.valueOf(CollectionsKt.last(list))));
                } else {
                    throw new TypeCastException("null cannot be cast to non-null type kotlin.collections.List<*>");
                }
            }
            return (List) arrayList;
        }

        private final String readFile(Context context) {
            Resources resources = context.getResources();
            Intrinsics.checkExpressionValueIsNotNull(resources, "context.resources");
            String next = new Scanner(resources.getAssets().open("au_becs_bsb.json")).useDelimiter("\\A").next();
            Intrinsics.checkExpressionValueIsNotNull(next, "Scanner(\n               …seDelimiter(\"\\\\A\").next()");
            return next;
        }
    }
}
