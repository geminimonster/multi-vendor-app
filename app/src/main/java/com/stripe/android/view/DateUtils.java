package com.stripe.android.view;

import java.util.Calendar;
import kotlin.Metadata;
import kotlin.Result;
import kotlin.ResultKt;
import kotlin.TypeCastException;
import kotlin.collections.CollectionsKt;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.internal.Intrinsics;
import kotlin.ranges.IntRange;

@Metadata(bv = {1, 0, 3}, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0010\u0011\n\u0002\b\u0003\bÀ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0012\u0010\u0005\u001a\u00020\u00042\b\b\u0001\u0010\u0006\u001a\u00020\u0004H\u0007J\u001a\u0010\u0005\u001a\u00020\u00042\b\b\u0001\u0010\u0006\u001a\u00020\u00042\u0006\u0010\u0007\u001a\u00020\bH\u0007J\u001c\u0010\t\u001a\u00020\n2\b\b\u0001\u0010\u000b\u001a\u00020\u00042\b\b\u0001\u0010\f\u001a\u00020\u0004H\u0007J\u0018\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u00042\u0006\u0010\u0010\u001a\u00020\u0004H\u0007J \u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u00042\u0006\u0010\u0010\u001a\u00020\u00042\u0006\u0010\u0007\u001a\u00020\bH\u0007J\u0012\u0010\u0011\u001a\u00020\u000e2\b\u0010\u0012\u001a\u0004\u0018\u00010\nH\u0007J\u001d\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\n0\u00142\b\b\u0001\u0010\u0015\u001a\u00020\nH\u0007¢\u0006\u0002\u0010\u0016R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\u0017"}, d2 = {"Lcom/stripe/android/view/DateUtils;", "", "()V", "MAX_VALID_YEAR", "", "convertTwoDigitYearToFour", "inputYear", "calendar", "Ljava/util/Calendar;", "createDateStringFromIntegerInput", "", "month", "year", "isExpiryDataValid", "", "expiryMonth", "expiryYear", "isValidMonth", "monthString", "separateDateStringParts", "", "expiryInput", "(Ljava/lang/String;)[Ljava/lang/String;", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: DateUtils.kt */
public final class DateUtils {
    public static final DateUtils INSTANCE = new DateUtils();
    private static final int MAX_VALID_YEAR = 9980;

    private DateUtils() {
    }

    @JvmStatic
    public static final boolean isValidMonth(String str) {
        Boolean bool;
        try {
            Result.Companion companion = Result.Companion;
            boolean z = true;
            IntRange intRange = new IntRange(1, 12);
            Integer valueOf = str != null ? Integer.valueOf(Integer.parseInt(str)) : null;
            if (valueOf == null || !intRange.contains(valueOf.intValue())) {
                z = false;
            }
            bool = Result.m4constructorimpl(Boolean.valueOf(z));
        } catch (Throwable th) {
            Result.Companion companion2 = Result.Companion;
            bool = Result.m4constructorimpl(ResultKt.createFailure(th));
        }
        if (Result.m10isFailureimpl(bool)) {
            bool = false;
        }
        return ((Boolean) bool).booleanValue();
    }

    @JvmStatic
    public static final String[] separateDateStringParts(String str) {
        Intrinsics.checkParameterIsNotNull(str, "expiryInput");
        if (str.length() >= 2) {
            String substring = str.substring(0, 2);
            Intrinsics.checkExpressionValueIsNotNull(substring, "(this as java.lang.Strin…ing(startIndex, endIndex)");
            String substring2 = str.substring(2);
            Intrinsics.checkExpressionValueIsNotNull(substring2, "(this as java.lang.String).substring(startIndex)");
            Object[] array = CollectionsKt.listOf(substring, substring2).toArray(new String[0]);
            if (array != null) {
                return (String[]) array;
            }
            throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
        }
        Object[] array2 = CollectionsKt.listOf(str, "").toArray(new String[0]);
        if (array2 != null) {
            return (String[]) array2;
        }
        throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @JvmStatic
    public static final boolean isExpiryDataValid(int i, int i2) {
        Calendar instance = Calendar.getInstance();
        Intrinsics.checkExpressionValueIsNotNull(instance, "Calendar.getInstance()");
        return isExpiryDataValid(i, i2, instance);
    }

    @JvmStatic
    public static final boolean isExpiryDataValid(int i, int i2, Calendar calendar) {
        int i3;
        Intrinsics.checkParameterIsNotNull(calendar, "calendar");
        if (i < 1 || i > 12 || i2 < 0 || i2 > MAX_VALID_YEAR || i2 < (i3 = calendar.get(1))) {
            return false;
        }
        return i2 > i3 || i >= calendar.get(2) + 1;
    }

    @JvmStatic
    public static final String createDateStringFromIntegerInput(int i, int i2) {
        String valueOf = String.valueOf(i);
        if (valueOf.length() == 1) {
            valueOf = '0' + valueOf;
        }
        String valueOf2 = String.valueOf(i2);
        if (valueOf2.length() == 3) {
            return "";
        }
        if (valueOf2.length() > 2) {
            int length = valueOf2.length() - 2;
            if (valueOf2 != null) {
                valueOf2 = valueOf2.substring(length);
                Intrinsics.checkExpressionValueIsNotNull(valueOf2, "(this as java.lang.String).substring(startIndex)");
            } else {
                throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
            }
        } else if (valueOf2.length() == 1) {
            valueOf2 = '0' + valueOf2;
        }
        return valueOf + valueOf2;
    }

    @JvmStatic
    public static final int convertTwoDigitYearToFour(int i) {
        Calendar instance = Calendar.getInstance();
        Intrinsics.checkExpressionValueIsNotNull(instance, "Calendar.getInstance()");
        return convertTwoDigitYearToFour(i, instance);
    }

    @JvmStatic
    public static final int convertTwoDigitYearToFour(int i, Calendar calendar) {
        Intrinsics.checkParameterIsNotNull(calendar, "calendar");
        int i2 = calendar.get(1);
        int i3 = i2 / 100;
        int i4 = i2 % 100;
        if (i4 > 80 && i < 20) {
            i3++;
        } else if (i4 < 20 && i > 80) {
            i3--;
        }
        return (i3 * 100) + i;
    }
}
