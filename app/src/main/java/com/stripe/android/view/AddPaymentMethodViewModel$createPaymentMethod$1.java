package com.stripe.android.view;

import androidx.lifecycle.MutableLiveData;
import com.stripe.android.ApiResultCallback;
import com.stripe.android.model.PaymentMethod;
import kotlin.Metadata;
import kotlin.Result;
import kotlin.ResultKt;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000!\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003*\u0001\u0000\b\n\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001J\u0014\u0010\u0003\u001a\u00020\u00042\n\u0010\u0005\u001a\u00060\u0006j\u0002`\u0007H\u0016J\u0010\u0010\b\u001a\u00020\u00042\u0006\u0010\t\u001a\u00020\u0002H\u0016¨\u0006\n"}, d2 = {"com/stripe/android/view/AddPaymentMethodViewModel$createPaymentMethod$1", "Lcom/stripe/android/ApiResultCallback;", "Lcom/stripe/android/model/PaymentMethod;", "onError", "", "e", "Ljava/lang/Exception;", "Lkotlin/Exception;", "onSuccess", "result", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: AddPaymentMethodViewModel.kt */
public final class AddPaymentMethodViewModel$createPaymentMethod$1 implements ApiResultCallback<PaymentMethod> {
    final /* synthetic */ MutableLiveData $resultData;

    AddPaymentMethodViewModel$createPaymentMethod$1(MutableLiveData mutableLiveData) {
        this.$resultData = mutableLiveData;
    }

    public void onSuccess(PaymentMethod paymentMethod) {
        Intrinsics.checkParameterIsNotNull(paymentMethod, "result");
        MutableLiveData mutableLiveData = this.$resultData;
        Result.Companion companion = Result.Companion;
        mutableLiveData.setValue(Result.m3boximpl(Result.m4constructorimpl(paymentMethod)));
    }

    public void onError(Exception exc) {
        Intrinsics.checkParameterIsNotNull(exc, "e");
        MutableLiveData mutableLiveData = this.$resultData;
        Result.Companion companion = Result.Companion;
        mutableLiveData.setValue(Result.m3boximpl(Result.m4constructorimpl(ResultKt.createFailure(exc))));
    }
}
