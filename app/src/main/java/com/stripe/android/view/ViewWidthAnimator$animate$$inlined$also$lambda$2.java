package com.stripe.android.view;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.view.View;
import android.view.ViewGroup;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0017\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000*\u0001\u0000\b\n\u0018\u00002\u00020\u0001J\u0012\u0010\u0002\u001a\u00020\u00032\b\u0010\u0004\u001a\u0004\u0018\u00010\u0005H\u0016¨\u0006\u0006¸\u0006\u0000"}, d2 = {"com/stripe/android/view/ViewWidthAnimator$animate$1$2", "Landroid/animation/AnimatorListenerAdapter;", "onAnimationEnd", "", "animation", "Landroid/animation/Animator;", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: ViewWidthAnimator.kt */
public final class ViewWidthAnimator$animate$$inlined$also$lambda$2 extends AnimatorListenerAdapter {
    final /* synthetic */ int $endWidth$inlined;
    final /* synthetic */ Function0 $onAnimationEnd$inlined;
    final /* synthetic */ ViewWidthAnimator this$0;

    ViewWidthAnimator$animate$$inlined$also$lambda$2(ViewWidthAnimator viewWidthAnimator, int i, Function0 function0) {
        this.this$0 = viewWidthAnimator;
        this.$endWidth$inlined = i;
        this.$onAnimationEnd$inlined = function0;
    }

    public void onAnimationEnd(Animator animator) {
        super.onAnimationEnd(animator);
        View access$getView$p = this.this$0.view;
        ViewGroup.LayoutParams layoutParams = this.this$0.view.getLayoutParams();
        layoutParams.width = this.$endWidth$inlined;
        access$getView$p.setLayoutParams(layoutParams);
        this.$onAnimationEnd$inlined.invoke();
    }
}
