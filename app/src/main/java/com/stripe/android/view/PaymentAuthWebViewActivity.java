package com.stripe.android.view;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelStoreOwner;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import com.stripe.android.Logger;
import com.stripe.android.PaymentAuthWebViewStarter;
import com.stripe.android.R;
import com.stripe.android.databinding.PaymentAuthWebViewActivityBinding;
import com.stripe.android.stripe3ds2.utils.CustomizeUtils;
import com.stripe.android.view.PaymentAuthWebViewActivityViewModel;
import com.ults.listeners.SdkChallengeInterface;
import kotlin.Lazy;
import kotlin.LazyKt;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import kotlin.text.StringsKt;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\b\u0010\r\u001a\u00020\u000eH\u0002J\b\u0010\u000f\u001a\u00020\u000eH\u0002J\b\u0010\u0010\u001a\u00020\u000eH\u0016J\u0012\u0010\u0011\u001a\u00020\u000e2\b\u0010\u0012\u001a\u0004\u0018\u00010\u0013H\u0014J\u0010\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u0017H\u0016J\b\u0010\u0018\u001a\u00020\u000eH\u0014J\u0010\u0010\u0019\u001a\u00020\u00152\u0006\u0010\u001a\u001a\u00020\u001bH\u0016R\u000e\u0010\u0003\u001a\u00020\u0004X.¢\u0006\u0002\n\u0000R\u001b\u0010\u0005\u001a\u00020\u00068BX\u0002¢\u0006\f\n\u0004\b\t\u0010\n\u001a\u0004\b\u0007\u0010\bR\u000e\u0010\u000b\u001a\u00020\fX.¢\u0006\u0002\n\u0000¨\u0006\u001c"}, d2 = {"Lcom/stripe/android/view/PaymentAuthWebViewActivity;", "Landroidx/appcompat/app/AppCompatActivity;", "()V", "logger", "Lcom/stripe/android/Logger;", "viewBinding", "Lcom/stripe/android/databinding/PaymentAuthWebViewActivityBinding;", "getViewBinding", "()Lcom/stripe/android/databinding/PaymentAuthWebViewActivityBinding;", "viewBinding$delegate", "Lkotlin/Lazy;", "viewModel", "Lcom/stripe/android/view/PaymentAuthWebViewActivityViewModel;", "cancelIntentSource", "", "customizeToolbar", "onBackPressed", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onCreateOptionsMenu", "", "menu", "Landroid/view/Menu;", "onDestroy", "onOptionsItemSelected", "item", "Landroid/view/MenuItem;", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: PaymentAuthWebViewActivity.kt */
public final class PaymentAuthWebViewActivity extends AppCompatActivity {
    private Logger logger;
    private final Lazy viewBinding$delegate = LazyKt.lazy(new PaymentAuthWebViewActivity$viewBinding$2(this));
    private PaymentAuthWebViewActivityViewModel viewModel;

    private final PaymentAuthWebViewActivityBinding getViewBinding() {
        return (PaymentAuthWebViewActivityBinding) this.viewBinding$delegate.getValue();
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        PaymentAuthWebViewStarter.Args args = (PaymentAuthWebViewStarter.Args) getIntent().getParcelableExtra(PaymentAuthWebViewStarter.EXTRA_ARGS);
        if (args == null) {
            setResult(0);
            finish();
            return;
        }
        ViewModel viewModel2 = new ViewModelProvider((ViewModelStoreOwner) this, (ViewModelProvider.Factory) new PaymentAuthWebViewActivityViewModel.Factory(args)).get(PaymentAuthWebViewActivityViewModel.class);
        Intrinsics.checkExpressionValueIsNotNull(viewModel2, "ViewModelProvider(\n     …ityViewModel::class.java]");
        this.viewModel = (PaymentAuthWebViewActivityViewModel) viewModel2;
        Logger instance$stripe_release = Logger.Companion.getInstance$stripe_release(args.getEnableLogging());
        this.logger = instance$stripe_release;
        if (instance$stripe_release == null) {
            Intrinsics.throwUninitializedPropertyAccessException("logger");
        }
        instance$stripe_release.debug("PaymentAuthWebViewActivity#onCreate()");
        LocalBroadcastManager.getInstance(this).sendBroadcast(new Intent().setAction(SdkChallengeInterface.UL_HANDLE_CHALLENGE_ACTION));
        setContentView((View) getViewBinding().getRoot());
        setSupportActionBar(getViewBinding().toolbar);
        customizeToolbar();
        String clientSecret = args.getClientSecret();
        Intent intent = new Intent();
        PaymentAuthWebViewActivityViewModel paymentAuthWebViewActivityViewModel = this.viewModel;
        if (paymentAuthWebViewActivityViewModel == null) {
            Intrinsics.throwUninitializedPropertyAccessException("viewModel");
        }
        setResult(-1, intent.putExtras(paymentAuthWebViewActivityViewModel.getPaymentResult$stripe_release().toBundle()));
        if (StringsKt.isBlank(clientSecret)) {
            Logger logger2 = this.logger;
            if (logger2 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("logger");
            }
            logger2.debug("PaymentAuthWebViewActivity#onCreate() - clientSecret is blank");
            finish();
            return;
        }
        Logger logger3 = this.logger;
        if (logger3 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("logger");
        }
        logger3.debug("PaymentAuthWebViewActivity#onCreate() - PaymentAuthWebView init and loadUrl");
        PaymentAuthWebView paymentAuthWebView = getViewBinding().authWebView;
        Activity activity = this;
        Logger logger4 = this.logger;
        if (logger4 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("logger");
        }
        ProgressBar progressBar = getViewBinding().authWebViewProgressBar;
        Intrinsics.checkExpressionValueIsNotNull(progressBar, "viewBinding.authWebViewProgressBar");
        paymentAuthWebView.init(activity, logger4, progressBar, clientSecret, args.getReturnUrl());
        getViewBinding().authWebView.loadUrl(args.getUrl());
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        getViewBinding().authWebViewContainer.removeAllViews();
        getViewBinding().authWebView.destroy();
        super.onDestroy();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        Intrinsics.checkParameterIsNotNull(menu, "menu");
        Logger logger2 = this.logger;
        if (logger2 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("logger");
        }
        logger2.debug("PaymentAuthWebViewActivity#onCreateOptionsMenu()");
        getMenuInflater().inflate(R.menu.payment_auth_web_view_menu, menu);
        PaymentAuthWebViewActivityViewModel paymentAuthWebViewActivityViewModel = this.viewModel;
        if (paymentAuthWebViewActivityViewModel == null) {
            Intrinsics.throwUninitializedPropertyAccessException("viewModel");
        }
        String buttonText$stripe_release = paymentAuthWebViewActivityViewModel.getButtonText$stripe_release();
        if (buttonText$stripe_release != null) {
            Logger logger3 = this.logger;
            if (logger3 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("logger");
            }
            logger3.debug("PaymentAuthWebViewActivity#customizeToolbar() - updating close button text");
            MenuItem findItem = menu.findItem(R.id.action_close);
            Intrinsics.checkExpressionValueIsNotNull(findItem, "menu.findItem(R.id.action_close)");
            findItem.setTitle(buttonText$stripe_release);
        }
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        Intrinsics.checkParameterIsNotNull(menuItem, "item");
        Logger logger2 = this.logger;
        if (logger2 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("logger");
        }
        logger2.debug("PaymentAuthWebViewActivity#onOptionsItemSelected()");
        if (menuItem.getItemId() != R.id.action_close) {
            return super.onOptionsItemSelected(menuItem);
        }
        cancelIntentSource();
        return true;
    }

    public void onBackPressed() {
        cancelIntentSource();
    }

    private final void cancelIntentSource() {
        PaymentAuthWebViewActivityViewModel paymentAuthWebViewActivityViewModel = this.viewModel;
        if (paymentAuthWebViewActivityViewModel == null) {
            Intrinsics.throwUninitializedPropertyAccessException("viewModel");
        }
        paymentAuthWebViewActivityViewModel.cancelIntentSource$stripe_release().observe(this, new PaymentAuthWebViewActivity$cancelIntentSource$1(this));
    }

    private final void customizeToolbar() {
        Logger logger2 = this.logger;
        if (logger2 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("logger");
        }
        logger2.debug("PaymentAuthWebViewActivity#customizeToolbar()");
        PaymentAuthWebViewActivityViewModel paymentAuthWebViewActivityViewModel = this.viewModel;
        if (paymentAuthWebViewActivityViewModel == null) {
            Intrinsics.throwUninitializedPropertyAccessException("viewModel");
        }
        PaymentAuthWebViewActivityViewModel.ToolbarTitleData toolbarTitle$stripe_release = paymentAuthWebViewActivityViewModel.getToolbarTitle$stripe_release();
        if (toolbarTitle$stripe_release != null) {
            Logger logger3 = this.logger;
            if (logger3 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("logger");
            }
            logger3.debug("PaymentAuthWebViewActivity#customizeToolbar() - updating toolbar title");
            Toolbar toolbar = getViewBinding().toolbar;
            Intrinsics.checkExpressionValueIsNotNull(toolbar, "viewBinding.toolbar");
            toolbar.setTitle((CharSequence) CustomizeUtils.INSTANCE.buildStyledText(this, toolbarTitle$stripe_release.getText$stripe_release(), toolbarTitle$stripe_release.getToolbarCustomization$stripe_release()));
        }
        PaymentAuthWebViewActivityViewModel paymentAuthWebViewActivityViewModel2 = this.viewModel;
        if (paymentAuthWebViewActivityViewModel2 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("viewModel");
        }
        String toolbarBackgroundColor$stripe_release = paymentAuthWebViewActivityViewModel2.getToolbarBackgroundColor$stripe_release();
        if (toolbarBackgroundColor$stripe_release != null) {
            Logger logger4 = this.logger;
            if (logger4 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("logger");
            }
            logger4.debug("PaymentAuthWebViewActivity#customizeToolbar() - updating toolbar background color");
            int parseColor = Color.parseColor(toolbarBackgroundColor$stripe_release);
            getViewBinding().toolbar.setBackgroundColor(parseColor);
            CustomizeUtils.INSTANCE.setStatusBarColor(this, parseColor);
        }
    }
}
