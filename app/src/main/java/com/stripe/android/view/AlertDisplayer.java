package com.stripe.android.view;

import android.app.Activity;
import android.content.DialogInterface;
import androidx.appcompat.app.AlertDialog;
import com.stripe.android.R;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\b`\u0018\u00002\u00020\u0001:\u0001\u0006J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&¨\u0006\u0007"}, d2 = {"Lcom/stripe/android/view/AlertDisplayer;", "", "show", "", "message", "", "DefaultAlertDisplayer", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: AlertDisplayer.kt */
public interface AlertDisplayer {
    void show(String str);

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bH\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\t"}, d2 = {"Lcom/stripe/android/view/AlertDisplayer$DefaultAlertDisplayer;", "Lcom/stripe/android/view/AlertDisplayer;", "activity", "Landroid/app/Activity;", "(Landroid/app/Activity;)V", "show", "", "message", "", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: AlertDisplayer.kt */
    public static final class DefaultAlertDisplayer implements AlertDisplayer {
        private final Activity activity;

        public DefaultAlertDisplayer(Activity activity2) {
            Intrinsics.checkParameterIsNotNull(activity2, "activity");
            this.activity = activity2;
        }

        public void show(String str) {
            Intrinsics.checkParameterIsNotNull(str, "message");
            if (!this.activity.isFinishing()) {
                new AlertDialog.Builder(this.activity, R.style.AlertDialogStyle).setMessage((CharSequence) str).setCancelable(true).setPositiveButton(17039370, (DialogInterface.OnClickListener) AlertDisplayer$DefaultAlertDisplayer$show$1.INSTANCE).create().show();
            }
        }
    }
}
