package com.stripe.android.view;

import kotlin.Metadata;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n¢\u0006\u0002\b\u0002"}, d2 = {"<anonymous>", "", "run"}, k = 3, mv = {1, 1, 16})
/* compiled from: StripeEditText.kt */
final class StripeEditText$setHintDelayed$1 implements Runnable {
    final /* synthetic */ CharSequence $hint;
    final /* synthetic */ StripeEditText this$0;

    StripeEditText$setHintDelayed$1(StripeEditText stripeEditText, CharSequence charSequence) {
        this.this$0 = stripeEditText;
        this.$hint = charSequence;
    }

    public final void run() {
        this.this$0.setHintSafely(this.$hint);
    }
}
