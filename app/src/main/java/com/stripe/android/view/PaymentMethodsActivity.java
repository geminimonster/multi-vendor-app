package com.stripe.android.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.LifecycleOwner;
import com.stripe.android.CustomerSession;
import com.stripe.android.databinding.PaymentMethodsActivityBinding;
import com.stripe.android.model.PaymentMethod;
import com.stripe.android.view.AddPaymentMethodActivityStarter;
import com.stripe.android.view.PaymentMethodsActivityStarter;
import kotlin.Lazy;
import kotlin.LazyKt;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000r\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u0000 A2\u00020\u0001:\u0001AB\u0005¢\u0006\u0002\u0010\u0002J\b\u0010,\u001a\u00020-H\u0002J\b\u0010.\u001a\u00020-H\u0002J\u001c\u0010/\u001a\u00020-2\b\u00100\u001a\u0004\u0018\u0001012\b\b\u0002\u00102\u001a\u000203H\u0002J\"\u00104\u001a\u00020-2\u0006\u00105\u001a\u0002032\u0006\u00102\u001a\u0002032\b\u00106\u001a\u0004\u0018\u000107H\u0016J\u0010\u00108\u001a\u00020-2\u0006\u00100\u001a\u000201H\u0002J\b\u00109\u001a\u00020-H\u0016J\u0012\u0010:\u001a\u00020-2\b\u0010;\u001a\u0004\u0018\u00010<H\u0014J\b\u0010=\u001a\u00020-H\u0014J\u0012\u0010>\u001a\u00020-2\b\u00106\u001a\u0004\u0018\u000107H\u0002J\b\u0010?\u001a\u00020\u001eH\u0016J\b\u0010@\u001a\u00020-H\u0002R\u001b\u0010\u0003\u001a\u00020\u00048BX\u0002¢\u0006\f\n\u0004\b\u0007\u0010\b\u001a\u0004\b\u0005\u0010\u0006R\u001b\u0010\t\u001a\u00020\n8BX\u0002¢\u0006\f\n\u0004\b\r\u0010\b\u001a\u0004\b\u000b\u0010\fR\u001b\u0010\u000e\u001a\u00020\u000f8BX\u0002¢\u0006\f\n\u0004\b\u0012\u0010\b\u001a\u0004\b\u0010\u0010\u0011R\u001b\u0010\u0013\u001a\u00020\u00148BX\u0002¢\u0006\f\n\u0004\b\u0017\u0010\b\u001a\u0004\b\u0015\u0010\u0016R\u001b\u0010\u0018\u001a\u00020\u00198BX\u0002¢\u0006\f\n\u0004\b\u001c\u0010\b\u001a\u0004\b\u001a\u0010\u001bR\u001b\u0010\u001d\u001a\u00020\u001e8BX\u0002¢\u0006\f\n\u0004\b!\u0010\b\u001a\u0004\b\u001f\u0010 R\u001b\u0010\"\u001a\u00020#8@X\u0002¢\u0006\f\n\u0004\b&\u0010\b\u001a\u0004\b$\u0010%R\u001b\u0010'\u001a\u00020(8BX\u0002¢\u0006\f\n\u0004\b+\u0010\b\u001a\u0004\b)\u0010*¨\u0006B"}, d2 = {"Lcom/stripe/android/view/PaymentMethodsActivity;", "Landroidx/appcompat/app/AppCompatActivity;", "()V", "adapter", "Lcom/stripe/android/view/PaymentMethodsAdapter;", "getAdapter", "()Lcom/stripe/android/view/PaymentMethodsAdapter;", "adapter$delegate", "Lkotlin/Lazy;", "alertDisplayer", "Lcom/stripe/android/view/AlertDisplayer;", "getAlertDisplayer", "()Lcom/stripe/android/view/AlertDisplayer;", "alertDisplayer$delegate", "args", "Lcom/stripe/android/view/PaymentMethodsActivityStarter$Args;", "getArgs", "()Lcom/stripe/android/view/PaymentMethodsActivityStarter$Args;", "args$delegate", "cardDisplayTextFactory", "Lcom/stripe/android/view/CardDisplayTextFactory;", "getCardDisplayTextFactory", "()Lcom/stripe/android/view/CardDisplayTextFactory;", "cardDisplayTextFactory$delegate", "customerSession", "Lcom/stripe/android/CustomerSession;", "getCustomerSession", "()Lcom/stripe/android/CustomerSession;", "customerSession$delegate", "startedFromPaymentSession", "", "getStartedFromPaymentSession", "()Z", "startedFromPaymentSession$delegate", "viewBinding", "Lcom/stripe/android/databinding/PaymentMethodsActivityBinding;", "getViewBinding$stripe_release", "()Lcom/stripe/android/databinding/PaymentMethodsActivityBinding;", "viewBinding$delegate", "viewModel", "Lcom/stripe/android/view/PaymentMethodsViewModel;", "getViewModel", "()Lcom/stripe/android/view/PaymentMethodsViewModel;", "viewModel$delegate", "fetchCustomerPaymentMethods", "", "finishWithGooglePay", "finishWithResult", "paymentMethod", "Lcom/stripe/android/model/PaymentMethod;", "resultCode", "", "onActivityResult", "requestCode", "data", "Landroid/content/Intent;", "onAddedPaymentMethod", "onBackPressed", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onDestroy", "onPaymentMethodCreated", "onSupportNavigateUp", "setupRecyclerView", "Companion", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: PaymentMethodsActivity.kt */
public final class PaymentMethodsActivity extends AppCompatActivity {
    public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
    public static final String PRODUCT_TOKEN = "PaymentMethodsActivity";
    private final Lazy adapter$delegate = LazyKt.lazy(new PaymentMethodsActivity$adapter$2(this));
    private final Lazy alertDisplayer$delegate = LazyKt.lazy(new PaymentMethodsActivity$alertDisplayer$2(this));
    private final Lazy args$delegate = LazyKt.lazy(new PaymentMethodsActivity$args$2(this));
    private final Lazy cardDisplayTextFactory$delegate = LazyKt.lazy(new PaymentMethodsActivity$cardDisplayTextFactory$2(this));
    private final Lazy customerSession$delegate = LazyKt.lazy(PaymentMethodsActivity$customerSession$2.INSTANCE);
    private final Lazy startedFromPaymentSession$delegate = LazyKt.lazy(new PaymentMethodsActivity$startedFromPaymentSession$2(this));
    private final Lazy viewBinding$delegate = LazyKt.lazy(new PaymentMethodsActivity$viewBinding$2(this));
    private final Lazy viewModel$delegate = LazyKt.lazy(new PaymentMethodsActivity$viewModel$2(this));

    /* access modifiers changed from: private */
    public final PaymentMethodsAdapter getAdapter() {
        return (PaymentMethodsAdapter) this.adapter$delegate.getValue();
    }

    /* access modifiers changed from: private */
    public final AlertDisplayer getAlertDisplayer() {
        return (AlertDisplayer) this.alertDisplayer$delegate.getValue();
    }

    /* access modifiers changed from: private */
    public final PaymentMethodsActivityStarter.Args getArgs() {
        return (PaymentMethodsActivityStarter.Args) this.args$delegate.getValue();
    }

    private final CardDisplayTextFactory getCardDisplayTextFactory() {
        return (CardDisplayTextFactory) this.cardDisplayTextFactory$delegate.getValue();
    }

    /* access modifiers changed from: private */
    public final CustomerSession getCustomerSession() {
        return (CustomerSession) this.customerSession$delegate.getValue();
    }

    /* access modifiers changed from: private */
    public final boolean getStartedFromPaymentSession() {
        return ((Boolean) this.startedFromPaymentSession$delegate.getValue()).booleanValue();
    }

    /* access modifiers changed from: private */
    public final PaymentMethodsViewModel getViewModel() {
        return (PaymentMethodsViewModel) this.viewModel$delegate.getValue();
    }

    public final PaymentMethodsActivityBinding getViewBinding$stripe_release() {
        return (PaymentMethodsActivityBinding) this.viewBinding$delegate.getValue();
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((View) getViewBinding$stripe_release().getRoot());
        Integer windowFlags$stripe_release = getArgs().getWindowFlags$stripe_release();
        if (windowFlags$stripe_release != null) {
            getWindow().addFlags(windowFlags$stripe_release.intValue());
        }
        LifecycleOwner lifecycleOwner = this;
        getViewModel().getSnackbarData$stripe_release().observe(lifecycleOwner, new PaymentMethodsActivity$onCreate$2(this));
        getViewModel().getProgressData$stripe_release().observe(lifecycleOwner, new PaymentMethodsActivity$onCreate$3(this));
        setupRecyclerView();
        setSupportActionBar(getViewBinding$stripe_release().toolbar);
        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.setDisplayHomeAsUpEnabled(true);
            supportActionBar.setDisplayShowHomeEnabled(true);
        }
        fetchCustomerPaymentMethods();
        getViewBinding$stripe_release().recycler.requestFocusFromTouch();
    }

    private final void setupRecyclerView() {
        Context context = this;
        DeletePaymentMethodDialogFactory deletePaymentMethodDialogFactory = new DeletePaymentMethodDialogFactory(context, getAdapter(), getCardDisplayTextFactory(), getCustomerSession(), getViewModel().getProductUsage$stripe_release(), new PaymentMethodsActivity$setupRecyclerView$deletePaymentMethodDialogFactory$1(this));
        getAdapter().setListener$stripe_release(new PaymentMethodsActivity$setupRecyclerView$1(this, deletePaymentMethodDialogFactory));
        PaymentMethodsRecyclerView paymentMethodsRecyclerView = getViewBinding$stripe_release().recycler;
        Intrinsics.checkExpressionValueIsNotNull(paymentMethodsRecyclerView, "viewBinding.recycler");
        paymentMethodsRecyclerView.setAdapter(getAdapter());
        getViewBinding$stripe_release().recycler.setPaymentMethodSelectedCallback$stripe_release(new PaymentMethodsActivity$setupRecyclerView$2(this));
        if (getArgs().getCanDeletePaymentMethods$stripe_release()) {
            getViewBinding$stripe_release().recycler.attachItemTouchHelper$stripe_release(new PaymentMethodSwipeCallback(context, getAdapter(), new SwipeToDeleteCallbackListener(deletePaymentMethodDialogFactory)));
        }
    }

    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (i == 6001 && i2 == -1) {
            onPaymentMethodCreated(intent);
        }
    }

    public boolean onSupportNavigateUp() {
        finishWithResult(getAdapter().getSelectedPaymentMethod$stripe_release(), 0);
        return true;
    }

    private final void onPaymentMethodCreated(Intent intent) {
        Unit unit;
        PaymentMethod paymentMethod;
        if (intent != null) {
            AddPaymentMethodActivityStarter.Result fromIntent = AddPaymentMethodActivityStarter.Result.Companion.fromIntent(intent);
            if (fromIntent == null || (paymentMethod = fromIntent.getPaymentMethod()) == null) {
                unit = null;
            } else {
                onAddedPaymentMethod(paymentMethod);
                unit = Unit.INSTANCE;
            }
            if (unit != null) {
                return;
            }
        }
        fetchCustomerPaymentMethods();
        Unit unit2 = Unit.INSTANCE;
    }

    private final void onAddedPaymentMethod(PaymentMethod paymentMethod) {
        PaymentMethod.Type type = paymentMethod.type;
        if (type == null || !type.isReusable) {
            finishWithResult$default(this, paymentMethod, 0, 2, (Object) null);
            return;
        }
        fetchCustomerPaymentMethods();
        getViewModel().onPaymentMethodAdded$stripe_release(paymentMethod);
    }

    public void onBackPressed() {
        finishWithResult(getAdapter().getSelectedPaymentMethod$stripe_release(), 0);
    }

    private final void fetchCustomerPaymentMethods() {
        getViewModel().getPaymentMethods$stripe_release().observe(this, new PaymentMethodsActivity$fetchCustomerPaymentMethods$1(this));
    }

    /* access modifiers changed from: private */
    public final void finishWithGooglePay() {
        setResult(-1, new Intent().putExtras(new PaymentMethodsActivityStarter.Result((PaymentMethod) null, true, 1, (DefaultConstructorMarker) null).toBundle()));
        finish();
    }

    static /* synthetic */ void finishWithResult$default(PaymentMethodsActivity paymentMethodsActivity, PaymentMethod paymentMethod, int i, int i2, Object obj) {
        if ((i2 & 2) != 0) {
            i = -1;
        }
        paymentMethodsActivity.finishWithResult(paymentMethod, i);
    }

    private final void finishWithResult(PaymentMethod paymentMethod, int i) {
        Intent intent = new Intent();
        intent.putExtras(new PaymentMethodsActivityStarter.Result(paymentMethod, getArgs().getUseGooglePay$stripe_release() && paymentMethod == null).toBundle());
        setResult(i, intent);
        finish();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        PaymentMethodsViewModel viewModel = getViewModel();
        PaymentMethod selectedPaymentMethod$stripe_release = getAdapter().getSelectedPaymentMethod$stripe_release();
        viewModel.setSelectedPaymentMethodId$stripe_release(selectedPaymentMethod$stripe_release != null ? selectedPaymentMethod$stripe_release.id : null);
        super.onDestroy();
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\u0005"}, d2 = {"Lcom/stripe/android/view/PaymentMethodsActivity$Companion;", "", "()V", "PRODUCT_TOKEN", "", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: PaymentMethodsActivity.kt */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }
}
