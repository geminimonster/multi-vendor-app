package com.stripe.android.view;

import com.stripe.android.view.StripeEditText;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0017\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000*\u0001\u0000\b\n\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\u0016¨\u0006\u0006"}, d2 = {"com/stripe/android/view/CardInputWidget$initView$8", "Lcom/stripe/android/view/StripeEditText$AfterTextChangedListener;", "onTextChanged", "", "text", "", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: CardInputWidget.kt */
public final class CardInputWidget$initView$8 implements StripeEditText.AfterTextChangedListener {
    final /* synthetic */ CardInputWidget this$0;

    CardInputWidget$initView$8(CardInputWidget cardInputWidget) {
        this.this$0 = cardInputWidget;
    }

    public void onTextChanged(String str) {
        CardInputListener access$getCardInputListener$p;
        Intrinsics.checkParameterIsNotNull(str, "text");
        if (this.this$0.getBrand().isMaxCvc(str) && (access$getCardInputListener$p = this.this$0.cardInputListener) != null) {
            access$getCardInputListener$p.onCvcComplete();
        }
        CardInputWidget cardInputWidget = this.this$0;
        cardInputWidget.updateIconCvc(cardInputWidget.getCvcNumberEditText$stripe_release().hasFocus(), str);
    }
}
