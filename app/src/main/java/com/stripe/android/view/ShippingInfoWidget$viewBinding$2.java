package com.stripe.android.view;

import android.content.Context;
import android.view.LayoutInflater;
import com.stripe.android.databinding.AddressWidgetBinding;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.Lambda;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n¢\u0006\u0002\b\u0002"}, d2 = {"<anonymous>", "Lcom/stripe/android/databinding/AddressWidgetBinding;", "invoke"}, k = 3, mv = {1, 1, 16})
/* compiled from: ShippingInfoWidget.kt */
final class ShippingInfoWidget$viewBinding$2 extends Lambda implements Function0<AddressWidgetBinding> {
    final /* synthetic */ Context $context;
    final /* synthetic */ ShippingInfoWidget this$0;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    ShippingInfoWidget$viewBinding$2(ShippingInfoWidget shippingInfoWidget, Context context) {
        super(0);
        this.this$0 = shippingInfoWidget;
        this.$context = context;
    }

    public final AddressWidgetBinding invoke() {
        return AddressWidgetBinding.inflate(LayoutInflater.from(this.$context), this.this$0);
    }
}
