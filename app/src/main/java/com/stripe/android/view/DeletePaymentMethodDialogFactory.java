package com.stripe.android.view;

import android.content.Context;
import android.content.DialogInterface;
import androidx.appcompat.app.AlertDialog;
import com.stripe.android.CustomerSession;
import com.stripe.android.R;
import com.stripe.android.StripeError;
import com.stripe.android.model.PaymentMethod;
import java.util.Set;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000D\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\"\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0000\u0018\u00002\u00020\u0001:\u0001\u0017BI\b\u0000\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t\u0012\f\u0010\n\u001a\b\u0012\u0004\u0012\u00020\f0\u000b\u0012\u0012\u0010\r\u001a\u000e\u0012\u0004\u0012\u00020\u000f\u0012\u0004\u0012\u00020\u00100\u000e¢\u0006\u0002\u0010\u0011J\u000e\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u000fJ\u0015\u0010\u0015\u001a\u00020\u00102\u0006\u0010\u0014\u001a\u00020\u000fH\u0000¢\u0006\u0002\b\u0016R\u000e\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0004¢\u0006\u0002\n\u0000R\u001a\u0010\r\u001a\u000e\u0012\u0004\u0012\u00020\u000f\u0012\u0004\u0012\u00020\u00100\u000eX\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\n\u001a\b\u0012\u0004\u0012\u00020\f0\u000bX\u0004¢\u0006\u0002\n\u0000¨\u0006\u0018"}, d2 = {"Lcom/stripe/android/view/DeletePaymentMethodDialogFactory;", "", "context", "Landroid/content/Context;", "adapter", "Lcom/stripe/android/view/PaymentMethodsAdapter;", "cardDisplayTextFactory", "Lcom/stripe/android/view/CardDisplayTextFactory;", "customerSession", "Lcom/stripe/android/CustomerSession;", "productUsage", "", "", "onDeletedPaymentMethodCallback", "Lkotlin/Function1;", "Lcom/stripe/android/model/PaymentMethod;", "", "(Landroid/content/Context;Lcom/stripe/android/view/PaymentMethodsAdapter;Lcom/stripe/android/view/CardDisplayTextFactory;Lcom/stripe/android/CustomerSession;Ljava/util/Set;Lkotlin/jvm/functions/Function1;)V", "create", "Landroidx/appcompat/app/AlertDialog;", "paymentMethod", "onDeletedPaymentMethod", "onDeletedPaymentMethod$stripe_release", "PaymentMethodDeleteListener", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: DeletePaymentMethodDialogFactory.kt */
public final class DeletePaymentMethodDialogFactory {
    /* access modifiers changed from: private */
    public final PaymentMethodsAdapter adapter;
    private final CardDisplayTextFactory cardDisplayTextFactory;
    private final Context context;
    private final CustomerSession customerSession;
    private final Function1<PaymentMethod, Unit> onDeletedPaymentMethodCallback;
    private final Set<String> productUsage;

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\b\u0002\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\"\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\b\u0010\t\u001a\u0004\u0018\u00010\nH\u0016J\u0010\u0010\u000b\u001a\u00020\u00042\u0006\u0010\f\u001a\u00020\rH\u0016¨\u0006\u000e"}, d2 = {"Lcom/stripe/android/view/DeletePaymentMethodDialogFactory$PaymentMethodDeleteListener;", "Lcom/stripe/android/CustomerSession$PaymentMethodRetrievalListener;", "()V", "onError", "", "errorCode", "", "errorMessage", "", "stripeError", "Lcom/stripe/android/StripeError;", "onPaymentMethodRetrieved", "paymentMethod", "Lcom/stripe/android/model/PaymentMethod;", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: DeletePaymentMethodDialogFactory.kt */
    private static final class PaymentMethodDeleteListener implements CustomerSession.PaymentMethodRetrievalListener {
        public void onError(int i, String str, StripeError stripeError) {
            Intrinsics.checkParameterIsNotNull(str, "errorMessage");
        }

        public void onPaymentMethodRetrieved(PaymentMethod paymentMethod) {
            Intrinsics.checkParameterIsNotNull(paymentMethod, "paymentMethod");
        }
    }

    public DeletePaymentMethodDialogFactory(Context context2, PaymentMethodsAdapter paymentMethodsAdapter, CardDisplayTextFactory cardDisplayTextFactory2, CustomerSession customerSession2, Set<String> set, Function1<? super PaymentMethod, Unit> function1) {
        Intrinsics.checkParameterIsNotNull(context2, "context");
        Intrinsics.checkParameterIsNotNull(paymentMethodsAdapter, "adapter");
        Intrinsics.checkParameterIsNotNull(cardDisplayTextFactory2, "cardDisplayTextFactory");
        Intrinsics.checkParameterIsNotNull(customerSession2, "customerSession");
        Intrinsics.checkParameterIsNotNull(set, "productUsage");
        Intrinsics.checkParameterIsNotNull(function1, "onDeletedPaymentMethodCallback");
        this.context = context2;
        this.adapter = paymentMethodsAdapter;
        this.cardDisplayTextFactory = cardDisplayTextFactory2;
        this.customerSession = customerSession2;
        this.productUsage = set;
        this.onDeletedPaymentMethodCallback = function1;
    }

    public final /* synthetic */ AlertDialog create(PaymentMethod paymentMethod) {
        Intrinsics.checkParameterIsNotNull(paymentMethod, "paymentMethod");
        PaymentMethod.Card card = paymentMethod.card;
        AlertDialog create = new AlertDialog.Builder(this.context, R.style.AlertDialogStyle).setTitle(R.string.delete_payment_method_prompt_title).setMessage((CharSequence) card != null ? this.cardDisplayTextFactory.createUnstyled$stripe_release(card) : null).setPositiveButton(17039379, (DialogInterface.OnClickListener) new DeletePaymentMethodDialogFactory$create$1(this, paymentMethod)).setNegativeButton(17039369, (DialogInterface.OnClickListener) new DeletePaymentMethodDialogFactory$create$2(this, paymentMethod)).setOnCancelListener(new DeletePaymentMethodDialogFactory$create$3(this, paymentMethod)).create();
        Intrinsics.checkExpressionValueIsNotNull(create, "AlertDialog.Builder(cont…  }\n            .create()");
        return create;
    }

    public final /* synthetic */ void onDeletedPaymentMethod$stripe_release(PaymentMethod paymentMethod) {
        Intrinsics.checkParameterIsNotNull(paymentMethod, "paymentMethod");
        this.adapter.deletePaymentMethod$stripe_release(paymentMethod);
        String str = paymentMethod.id;
        if (str != null) {
            this.customerSession.detachPaymentMethod$stripe_release(str, this.productUsage, new PaymentMethodDeleteListener());
        }
        this.onDeletedPaymentMethodCallback.invoke(paymentMethod);
    }
}
