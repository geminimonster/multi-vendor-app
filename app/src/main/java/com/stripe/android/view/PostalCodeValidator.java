package com.stripe.android.view;

import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import kotlin.Metadata;
import kotlin.TuplesKt;
import kotlin.collections.MapsKt;
import kotlin.jvm.internal.Intrinsics;
import kotlin.text.StringsKt;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\b\u0003\b\u0000\u0018\u0000 \u000b2\u00020\u0001:\u0001\u000bB\u0005¢\u0006\u0002\u0010\u0002J4\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\b\u0010\u0007\u001a\u0004\u0018\u00010\u00062\f\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00060\t2\f\u0010\n\u001a\b\u0012\u0004\u0012\u00020\u00060\t¨\u0006\f"}, d2 = {"Lcom/stripe/android/view/PostalCodeValidator;", "", "()V", "isValid", "", "postalCode", "", "countryCode", "optionalShippingInfoFields", "", "hiddenShippingInfoFields", "Companion", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: PostalCodeValidator.kt */
public final class PostalCodeValidator {
    @Deprecated
    public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
    private static final Map<String, Pattern> POSTAL_CODE_PATTERNS;

    public final boolean isValid(String str, String str2, List<String> list, List<String> list2) {
        Matcher matcher;
        Intrinsics.checkParameterIsNotNull(str, "postalCode");
        Intrinsics.checkParameterIsNotNull(list, "optionalShippingInfoFields");
        Intrinsics.checkParameterIsNotNull(list2, "hiddenShippingInfoFields");
        if (str2 == null) {
            return false;
        }
        CharSequence charSequence = str;
        if (!(charSequence.length() == 0) || !Companion.isPostalCodeNotRequired(list, list2)) {
            Pattern pattern = POSTAL_CODE_PATTERNS.get(str2);
            if (pattern != null && (matcher = pattern.matcher(charSequence)) != null) {
                return matcher.matches();
            }
            if (!CountryUtils.INSTANCE.doesCountryUsePostalCode$stripe_release(str2) || (!StringsKt.isBlank(charSequence))) {
                return true;
            }
            return false;
        }
        return true;
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010 \n\u0002\b\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J$\u0010\b\u001a\u00020\t2\f\u0010\n\u001a\b\u0012\u0004\u0012\u00020\u00050\u000b2\f\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u00050\u000bH\u0002R*\u0010\u0003\u001a\u001e\u0012\f\u0012\n \u0006*\u0004\u0018\u00010\u00050\u0005\u0012\f\u0012\n \u0006*\u0004\u0018\u00010\u00070\u00070\u0004X\u0004¢\u0006\u0002\n\u0000¨\u0006\r"}, d2 = {"Lcom/stripe/android/view/PostalCodeValidator$Companion;", "", "()V", "POSTAL_CODE_PATTERNS", "", "", "kotlin.jvm.PlatformType", "Ljava/util/regex/Pattern;", "isPostalCodeNotRequired", "", "optionalShippingInfoFields", "", "hiddenShippingInfoFields", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: PostalCodeValidator.kt */
    private static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        /* access modifiers changed from: private */
        public final boolean isPostalCodeNotRequired(List<String> list, List<String> list2) {
            return list.contains("postal_code") || list2.contains("postal_code");
        }
    }

    static {
        Locale locale = Locale.US;
        Intrinsics.checkExpressionValueIsNotNull(locale, "Locale.US");
        Locale locale2 = Locale.CANADA;
        Intrinsics.checkExpressionValueIsNotNull(locale2, "Locale.CANADA");
        POSTAL_CODE_PATTERNS = MapsKt.mapOf(TuplesKt.to(locale.getCountry(), Pattern.compile("^[0-9]{5}(?:-[0-9]{4})?$")), TuplesKt.to(locale2.getCountry(), Pattern.compile("^(?!.*[DFIOQU])[A-VXY][0-9][A-Z] ?[0-9][A-Z][0-9]$")));
    }
}
