package com.stripe.android.view;

import android.content.Context;
import android.os.Build;
import android.text.Editable;
import android.text.InputFilter;
import android.text.method.DigitsKeyListener;
import android.util.AttributeSet;
import androidx.appcompat.R;
import com.google.android.material.textfield.TextInputLayout;
import com.stripe.android.model.CardBrand;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.Intrinsics;
import kotlin.text.StringsKt;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000T\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0007\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0010\u0011\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B%\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\b\b\u0002\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ\u001b\u0010\u001d\u001a\b\u0012\u0004\u0012\u00020\u001f0\u001e2\u0006\u0010\r\u001a\u00020\u000eH\u0002¢\u0006\u0002\u0010 J-\u0010!\u001a\u00020\u00112\u0006\u0010\r\u001a\u00020\u000e2\n\b\u0002\u0010\"\u001a\u0004\u0018\u00010\n2\n\b\u0002\u0010#\u001a\u0004\u0018\u00010$H\u0000¢\u0006\u0002\b%R\u0016\u0010\t\u001a\u0004\u0018\u00010\n8TX\u0004¢\u0006\u0006\u001a\u0004\b\u000b\u0010\fR\u000e\u0010\r\u001a\u00020\u000eX\u000e¢\u0006\u0002\n\u0000R \u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u00110\u0010X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0012\u0010\u0013\"\u0004\b\u0014\u0010\u0015R\u0013\u0010\u0016\u001a\u0004\u0018\u00010\n8F¢\u0006\u0006\u001a\u0004\b\u0017\u0010\fR\u0014\u0010\u0018\u001a\u00020\u00198BX\u0004¢\u0006\u0006\u001a\u0004\b\u0018\u0010\u001aR\u0014\u0010\u001b\u001a\u00020\n8@X\u0004¢\u0006\u0006\u001a\u0004\b\u001c\u0010\f¨\u0006&"}, d2 = {"Lcom/stripe/android/view/CvcEditText;", "Lcom/stripe/android/view/StripeEditText;", "context", "Landroid/content/Context;", "attrs", "Landroid/util/AttributeSet;", "defStyleAttr", "", "(Landroid/content/Context;Landroid/util/AttributeSet;I)V", "accessibilityText", "", "getAccessibilityText", "()Ljava/lang/String;", "cardBrand", "Lcom/stripe/android/model/CardBrand;", "completionCallback", "Lkotlin/Function0;", "", "getCompletionCallback$stripe_release", "()Lkotlin/jvm/functions/Function0;", "setCompletionCallback$stripe_release", "(Lkotlin/jvm/functions/Function0;)V", "cvcValue", "getCvcValue", "isValid", "", "()Z", "rawCvcValue", "getRawCvcValue$stripe_release", "createFilters", "", "Landroid/text/InputFilter;", "(Lcom/stripe/android/model/CardBrand;)[Landroid/text/InputFilter;", "updateBrand", "customHintText", "textInputLayout", "Lcom/google/android/material/textfield/TextInputLayout;", "updateBrand$stripe_release", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: CvcEditText.kt */
public final class CvcEditText extends StripeEditText {
    /* access modifiers changed from: private */
    public CardBrand cardBrand;
    private /* synthetic */ Function0<Unit> completionCallback;

    public CvcEditText(Context context) {
        this(context, (AttributeSet) null, 0, 6, (DefaultConstructorMarker) null);
    }

    public CvcEditText(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0, 4, (DefaultConstructorMarker) null);
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ CvcEditText(Context context, AttributeSet attributeSet, int i, int i2, DefaultConstructorMarker defaultConstructorMarker) {
        this(context, (i2 & 2) != 0 ? null : attributeSet, (i2 & 4) != 0 ? R.attr.editTextStyle : i);
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CvcEditText(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        Intrinsics.checkParameterIsNotNull(context, "context");
        this.cardBrand = CardBrand.Unknown;
        this.completionCallback = CvcEditText$completionCallback$1.INSTANCE;
        setErrorMessage(getResources().getString(com.stripe.android.R.string.invalid_cvc));
        setHint(com.stripe.android.R.string.cvc_number_hint);
        setMaxLines(1);
        setFilters(createFilters(CardBrand.Unknown));
        setInputType(16);
        setKeyListener(DigitsKeyListener.getInstance(false, true));
        if (Build.VERSION.SDK_INT >= 26) {
            setAutofillHints(new String[]{"creditCardSecurityCode"});
        }
        addTextChangedListener(new StripeTextWatcher(this) {
            final /* synthetic */ CvcEditText this$0;

            {
                this.this$0 = r1;
            }

            public void afterTextChanged(Editable editable) {
                this.this$0.setShouldShowError(false);
                if (this.this$0.cardBrand.isMaxCvc(this.this$0.getRawCvcValue$stripe_release())) {
                    this.this$0.getCompletionCallback$stripe_release().invoke();
                }
            }
        });
    }

    public final String getCvcValue() {
        String rawCvcValue$stripe_release = getRawCvcValue$stripe_release();
        if (isValid()) {
            return rawCvcValue$stripe_release;
        }
        return null;
    }

    public final /* synthetic */ String getRawCvcValue$stripe_release() {
        String fieldText$stripe_release = getFieldText$stripe_release();
        if (fieldText$stripe_release != null) {
            return StringsKt.trim((CharSequence) fieldText$stripe_release).toString();
        }
        throw new TypeCastException("null cannot be cast to non-null type kotlin.CharSequence");
    }

    private final boolean isValid() {
        return this.cardBrand.isValidCvc(getRawCvcValue$stripe_release());
    }

    public final Function0<Unit> getCompletionCallback$stripe_release() {
        return this.completionCallback;
    }

    public final void setCompletionCallback$stripe_release(Function0<Unit> function0) {
        Intrinsics.checkParameterIsNotNull(function0, "<set-?>");
        this.completionCallback = function0;
    }

    /* access modifiers changed from: protected */
    public String getAccessibilityText() {
        return getResources().getString(com.stripe.android.R.string.acc_label_cvc_node, new Object[]{getText()});
    }

    public static /* synthetic */ void updateBrand$stripe_release$default(CvcEditText cvcEditText, CardBrand cardBrand2, String str, TextInputLayout textInputLayout, int i, Object obj) {
        if ((i & 2) != 0) {
            str = null;
        }
        if ((i & 4) != 0) {
            textInputLayout = null;
        }
        cvcEditText.updateBrand$stripe_release(cardBrand2, str, textInputLayout);
    }

    public final /* synthetic */ void updateBrand$stripe_release(CardBrand cardBrand2, String str, TextInputLayout textInputLayout) {
        String str2;
        Intrinsics.checkParameterIsNotNull(cardBrand2, "cardBrand");
        this.cardBrand = cardBrand2;
        setFilters(createFilters(cardBrand2));
        if (str == null) {
            if (cardBrand2 == CardBrand.AmericanExpress) {
                str2 = getResources().getString(com.stripe.android.R.string.cvc_amex_hint);
            } else {
                str2 = getResources().getString(com.stripe.android.R.string.cvc_number_hint);
            }
            str = str2;
            Intrinsics.checkExpressionValueIsNotNull(str, "if (cardBrand == CardBra…umber_hint)\n            }");
        }
        if (textInputLayout != null) {
            textInputLayout.setHint(str);
        } else {
            setHint(str);
        }
    }

    private final InputFilter[] createFilters(CardBrand cardBrand2) {
        return new InputFilter[]{new InputFilter.LengthFilter(cardBrand2.getMaxCvcLength())};
    }
}
