package com.stripe.android.view;

import android.view.ViewStub;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.Lambda;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n¢\u0006\u0002\b\u0002"}, d2 = {"<anonymous>", "Landroid/view/ViewStub;", "invoke"}, k = 3, mv = {1, 1, 16})
/* compiled from: StripeActivity.kt */
final class StripeActivity$viewStub$2 extends Lambda implements Function0<ViewStub> {
    final /* synthetic */ StripeActivity this$0;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    StripeActivity$viewStub$2(StripeActivity stripeActivity) {
        super(0);
        this.this$0 = stripeActivity;
    }

    public final ViewStub invoke() {
        return this.this$0.getViewBinding().viewStub;
    }
}
