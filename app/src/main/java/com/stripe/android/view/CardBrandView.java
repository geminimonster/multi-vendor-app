package com.stripe.android.view;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.FrameLayout;
import android.widget.ImageView;
import androidx.core.graphics.drawable.DrawableCompat;
import com.stripe.android.databinding.CardBrandViewBinding;
import com.stripe.android.model.CardBrand;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0004\b\u0000\u0018\u00002\u00020\u0001B%\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\b\b\u0002\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ\r\u0010\u0012\u001a\u00020\u0013H\u0000¢\u0006\u0002\b\u0014J\u001d\u0010\u0015\u001a\u00020\u00132\u0006\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u0019H\u0000¢\u0006\u0002\b\u001aJ\u0015\u0010\u001b\u001a\u00020\u00132\u0006\u0010\u0016\u001a\u00020\u0017H\u0000¢\u0006\u0002\b\u001cR\u000e\u0010\t\u001a\u00020\nX\u0004¢\u0006\u0002\n\u0000R\u001e\u0010\u000b\u001a\u00020\u00078\u0000@\u0000X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\f\u0010\r\"\u0004\b\u000e\u0010\u000fR\u000e\u0010\u0010\u001a\u00020\u0011X\u0004¢\u0006\u0002\n\u0000¨\u0006\u001d"}, d2 = {"Lcom/stripe/android/view/CardBrandView;", "Landroid/widget/FrameLayout;", "context", "Landroid/content/Context;", "attrs", "Landroid/util/AttributeSet;", "defStyleAttr", "", "(Landroid/content/Context;Landroid/util/AttributeSet;I)V", "iconView", "Landroid/widget/ImageView;", "tintColorInt", "getTintColorInt$stripe_release", "()I", "setTintColorInt$stripe_release", "(I)V", "viewBinding", "Lcom/stripe/android/databinding/CardBrandViewBinding;", "applyTint", "", "applyTint$stripe_release", "showBrandIcon", "brand", "Lcom/stripe/android/model/CardBrand;", "shouldShowErrorIcon", "", "showBrandIcon$stripe_release", "showCvcIcon", "showCvcIcon$stripe_release", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: CardBrandView.kt */
public final class CardBrandView extends FrameLayout {
    private final ImageView iconView;
    private int tintColorInt;
    private final CardBrandViewBinding viewBinding;

    public CardBrandView(Context context) {
        this(context, (AttributeSet) null, 0, 6, (DefaultConstructorMarker) null);
    }

    public CardBrandView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0, 4, (DefaultConstructorMarker) null);
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ CardBrandView(Context context, AttributeSet attributeSet, int i, int i2, DefaultConstructorMarker defaultConstructorMarker) {
        this(context, (i2 & 2) != 0 ? null : attributeSet, (i2 & 4) != 0 ? 0 : i);
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CardBrandView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        Intrinsics.checkParameterIsNotNull(context, "context");
        CardBrandViewBinding inflate = CardBrandViewBinding.inflate(LayoutInflater.from(context), this);
        Intrinsics.checkExpressionValueIsNotNull(inflate, "CardBrandViewBinding.inf…text),\n        this\n    )");
        this.viewBinding = inflate;
        ImageView imageView = inflate.icon;
        Intrinsics.checkExpressionValueIsNotNull(imageView, "viewBinding.icon");
        this.iconView = imageView;
        setClickable(false);
        setFocusable(false);
    }

    public final int getTintColorInt$stripe_release() {
        return this.tintColorInt;
    }

    public final void setTintColorInt$stripe_release(int i) {
        this.tintColorInt = i;
    }

    public final void showBrandIcon$stripe_release(CardBrand cardBrand, boolean z) {
        Intrinsics.checkParameterIsNotNull(cardBrand, "brand");
        if (z) {
            this.iconView.setImageResource(cardBrand.getErrorIcon());
            return;
        }
        this.iconView.setImageResource(cardBrand.getIcon());
        if (cardBrand == CardBrand.Unknown) {
            applyTint$stripe_release();
        }
    }

    public final void showCvcIcon$stripe_release(CardBrand cardBrand) {
        Intrinsics.checkParameterIsNotNull(cardBrand, "brand");
        this.iconView.setImageResource(cardBrand.getCvcIcon());
        applyTint$stripe_release();
    }

    public final void applyTint$stripe_release() {
        Drawable wrap = DrawableCompat.wrap(this.iconView.getDrawable());
        DrawableCompat.setTint(wrap.mutate(), this.tintColorInt);
        this.iconView.setImageDrawable(DrawableCompat.unwrap(wrap));
    }
}
