package com.stripe.android.view;

import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelStoreOwner;
import com.stripe.android.view.PaymentFlowViewModel;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.Lambda;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n¢\u0006\u0002\b\u0002"}, d2 = {"<anonymous>", "Lcom/stripe/android/view/PaymentFlowViewModel;", "invoke"}, k = 3, mv = {1, 1, 16})
/* compiled from: PaymentFlowActivity.kt */
final class PaymentFlowActivity$viewModel$2 extends Lambda implements Function0<PaymentFlowViewModel> {
    final /* synthetic */ PaymentFlowActivity this$0;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    PaymentFlowActivity$viewModel$2(PaymentFlowActivity paymentFlowActivity) {
        super(0);
        this.this$0 = paymentFlowActivity;
    }

    public final PaymentFlowViewModel invoke() {
        PaymentFlowActivity paymentFlowActivity = this.this$0;
        return (PaymentFlowViewModel) new ViewModelProvider((ViewModelStoreOwner) paymentFlowActivity, (ViewModelProvider.Factory) new PaymentFlowViewModel.Factory(paymentFlowActivity.getCustomerSession(), this.this$0.getArgs().getPaymentSessionData$stripe_release())).get(PaymentFlowViewModel.class);
    }
}
