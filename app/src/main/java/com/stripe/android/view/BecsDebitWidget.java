package com.stripe.android.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.FrameLayout;
import com.google.android.material.textfield.TextInputLayout;
import com.stripe.android.R;
import com.stripe.android.databinding.BecsDebitWidgetBinding;
import com.stripe.android.model.Address;
import com.stripe.android.model.PaymentMethod;
import com.stripe.android.model.PaymentMethodCreateParams;
import com.stripe.android.view.BecsDebitBanks;
import java.util.Map;
import kotlin.Lazy;
import kotlin.LazyKt;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.collections.SetsKt;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.text.StringsKt;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000S\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\u0003*\u0001\u0019\u0018\u00002\u00020\u0001:\u0001$B/\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\b\b\u0002\u0010\u0006\u001a\u00020\u0007\u0012\b\b\u0002\u0010\b\u001a\u00020\t¢\u0006\u0002\u0010\nJ\u0010\u0010!\u001a\u00020\"2\u0006\u0010\u0004\u001a\u00020\u0005H\u0002J\b\u0010#\u001a\u00020\"H\u0002R\u0014\u0010\u000b\u001a\u00020\f8BX\u0004¢\u0006\u0006\u001a\u0004\b\u000b\u0010\rR\u0013\u0010\u000e\u001a\u0004\u0018\u00010\u000f8F¢\u0006\u0006\u001a\u0004\b\u0010\u0010\u0011R\u001a\u0010\u0012\u001a\u00020\u0013X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0014\u0010\u0015\"\u0004\b\u0016\u0010\u0017R\u0010\u0010\u0018\u001a\u00020\u0019X\u0004¢\u0006\u0004\n\u0002\u0010\u001aR\u001b\u0010\u001b\u001a\u00020\u001c8@X\u0002¢\u0006\f\n\u0004\b\u001f\u0010 \u001a\u0004\b\u001d\u0010\u001e¨\u0006%"}, d2 = {"Lcom/stripe/android/view/BecsDebitWidget;", "Landroid/widget/FrameLayout;", "context", "Landroid/content/Context;", "attrs", "Landroid/util/AttributeSet;", "defStyleAttr", "", "companyName", "", "(Landroid/content/Context;Landroid/util/AttributeSet;ILjava/lang/String;)V", "isInputValid", "", "()Z", "params", "Lcom/stripe/android/model/PaymentMethodCreateParams;", "getParams", "()Lcom/stripe/android/model/PaymentMethodCreateParams;", "validParamsCallback", "Lcom/stripe/android/view/BecsDebitWidget$ValidParamsCallback;", "getValidParamsCallback", "()Lcom/stripe/android/view/BecsDebitWidget$ValidParamsCallback;", "setValidParamsCallback", "(Lcom/stripe/android/view/BecsDebitWidget$ValidParamsCallback;)V", "validParamsTextWatcher", "com/stripe/android/view/BecsDebitWidget$validParamsTextWatcher$1", "Lcom/stripe/android/view/BecsDebitWidget$validParamsTextWatcher$1;", "viewBinding", "Lcom/stripe/android/databinding/BecsDebitWidgetBinding;", "getViewBinding$stripe_release", "()Lcom/stripe/android/databinding/BecsDebitWidgetBinding;", "viewBinding$delegate", "Lkotlin/Lazy;", "applyAttributes", "", "verifyCompanyName", "ValidParamsCallback", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: BecsDebitWidget.kt */
public final class BecsDebitWidget extends FrameLayout {
    private ValidParamsCallback validParamsCallback;
    private final BecsDebitWidget$validParamsTextWatcher$1 validParamsTextWatcher;
    private final Lazy viewBinding$delegate;

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\bf\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&¨\u0006\u0006"}, d2 = {"Lcom/stripe/android/view/BecsDebitWidget$ValidParamsCallback;", "", "onInputChanged", "", "isValid", "", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: BecsDebitWidget.kt */
    public interface ValidParamsCallback {
        void onInputChanged(boolean z);
    }

    public BecsDebitWidget(Context context) {
        this(context, (AttributeSet) null, 0, (String) null, 14, (DefaultConstructorMarker) null);
    }

    public BecsDebitWidget(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0, (String) null, 12, (DefaultConstructorMarker) null);
    }

    public BecsDebitWidget(Context context, AttributeSet attributeSet, int i) {
        this(context, attributeSet, i, (String) null, 8, (DefaultConstructorMarker) null);
    }

    public final BecsDebitWidgetBinding getViewBinding$stripe_release() {
        return (BecsDebitWidgetBinding) this.viewBinding$delegate.getValue();
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ BecsDebitWidget(Context context, AttributeSet attributeSet, int i, String str, int i2, DefaultConstructorMarker defaultConstructorMarker) {
        this(context, (i2 & 2) != 0 ? null : attributeSet, (i2 & 4) != 0 ? 0 : i, (i2 & 8) != 0 ? "" : str);
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BecsDebitWidget(Context context, AttributeSet attributeSet, int i, String str) {
        super(context, attributeSet, i);
        Intrinsics.checkParameterIsNotNull(context, "context");
        Intrinsics.checkParameterIsNotNull(str, "companyName");
        this.viewBinding$delegate = LazyKt.lazy(new BecsDebitWidget$viewBinding$2(this, context));
        this.validParamsCallback = new BecsDebitWidget$validParamsCallback$1();
        this.validParamsTextWatcher = new BecsDebitWidget$validParamsTextWatcher$1(this);
        if (Build.VERSION.SDK_INT >= 26) {
            getViewBinding$stripe_release().nameEditText.setAutofillHints(new String[]{"name"});
            getViewBinding$stripe_release().emailEditText.setAutofillHints(new String[]{"emailAddress"});
        }
        for (StripeEditText addTextChangedListener : SetsKt.setOf(getViewBinding$stripe_release().nameEditText, getViewBinding$stripe_release().emailEditText, getViewBinding$stripe_release().bsbEditText, getViewBinding$stripe_release().accountNumberEditText)) {
            addTextChangedListener.addTextChangedListener(this.validParamsTextWatcher);
        }
        getViewBinding$stripe_release().bsbEditText.setOnBankChangedCallback(new Function1<BecsDebitBanks.Bank, Unit>(this) {
            final /* synthetic */ BecsDebitWidget this$0;

            {
                this.this$0 = r1;
            }

            public /* bridge */ /* synthetic */ Object invoke(Object obj) {
                invoke((BecsDebitBanks.Bank) obj);
                return Unit.INSTANCE;
            }

            /* JADX WARNING: Code restructure failed: missing block: B:29:0x0096, code lost:
                if (r0.equals("73") != false) goto L_0x00b5;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:31:0x009f, code lost:
                if (r0.equals("08") != false) goto L_0x00ca;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:36:0x00b3, code lost:
                if (r0.equals("03") != false) goto L_0x00b5;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:39:0x00bd, code lost:
                if (r0.equals("01") != false) goto L_0x00ca;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:41:0x00c6, code lost:
                if (r0.equals("00") != false) goto L_0x00ca;
             */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final void invoke(com.stripe.android.view.BecsDebitBanks.Bank r5) {
                /*
                    r4 = this;
                    r0 = 0
                    java.lang.String r1 = "viewBinding.bsbTextInputLayout"
                    if (r5 == 0) goto L_0x002a
                    com.stripe.android.view.BecsDebitWidget r2 = r4.this$0
                    com.stripe.android.databinding.BecsDebitWidgetBinding r2 = r2.getViewBinding$stripe_release()
                    com.stripe.android.view.IconTextInputLayout r2 = r2.bsbTextInputLayout
                    kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r2, r1)
                    java.lang.String r3 = r5.getName$stripe_release()
                    java.lang.CharSequence r3 = (java.lang.CharSequence) r3
                    r2.setHelperText(r3)
                    com.stripe.android.view.BecsDebitWidget r2 = r4.this$0
                    com.stripe.android.databinding.BecsDebitWidgetBinding r2 = r2.getViewBinding$stripe_release()
                    com.stripe.android.view.IconTextInputLayout r2 = r2.bsbTextInputLayout
                    kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r2, r1)
                    r1 = 1
                    r2.setHelperTextEnabled(r1)
                    goto L_0x004a
                L_0x002a:
                    com.stripe.android.view.BecsDebitWidget r2 = r4.this$0
                    com.stripe.android.databinding.BecsDebitWidgetBinding r2 = r2.getViewBinding$stripe_release()
                    com.stripe.android.view.IconTextInputLayout r2 = r2.bsbTextInputLayout
                    kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r2, r1)
                    r3 = r0
                    java.lang.CharSequence r3 = (java.lang.CharSequence) r3
                    r2.setHelperText(r3)
                    com.stripe.android.view.BecsDebitWidget r2 = r4.this$0
                    com.stripe.android.databinding.BecsDebitWidgetBinding r2 = r2.getViewBinding$stripe_release()
                    com.stripe.android.view.IconTextInputLayout r2 = r2.bsbTextInputLayout
                    kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r2, r1)
                    r1 = 0
                    r2.setHelperTextEnabled(r1)
                L_0x004a:
                    com.stripe.android.view.BecsDebitWidget r1 = r4.this$0
                    com.stripe.android.databinding.BecsDebitWidgetBinding r1 = r1.getViewBinding$stripe_release()
                    com.stripe.android.view.BecsDebitAccountNumberEditText r1 = r1.accountNumberEditText
                    if (r5 == 0) goto L_0x005f
                    java.lang.String r5 = r5.getPrefix$stripe_release()
                    if (r5 == 0) goto L_0x005f
                    r0 = 2
                    java.lang.String r0 = kotlin.text.StringsKt.take((java.lang.String) r5, (int) r0)
                L_0x005f:
                    r5 = 9
                    if (r0 != 0) goto L_0x0065
                    goto L_0x00c9
                L_0x0065:
                    int r2 = r0.hashCode()
                    r3 = 1536(0x600, float:2.152E-42)
                    if (r2 == r3) goto L_0x00c0
                    r3 = 1537(0x601, float:2.154E-42)
                    if (r2 == r3) goto L_0x00b7
                    r3 = 1539(0x603, float:2.157E-42)
                    if (r2 == r3) goto L_0x00ad
                    r3 = 1542(0x606, float:2.161E-42)
                    if (r2 == r3) goto L_0x00a2
                    r3 = 1544(0x608, float:2.164E-42)
                    if (r2 == r3) goto L_0x0099
                    r5 = 1756(0x6dc, float:2.46E-42)
                    if (r2 == r5) goto L_0x0090
                    r5 = 1784(0x6f8, float:2.5E-42)
                    if (r2 == r5) goto L_0x0086
                    goto L_0x00c9
                L_0x0086:
                    java.lang.String r5 = "80"
                    boolean r5 = r0.equals(r5)
                    if (r5 == 0) goto L_0x00c9
                    r5 = 4
                    goto L_0x00ca
                L_0x0090:
                    java.lang.String r5 = "73"
                    boolean r5 = r0.equals(r5)
                    if (r5 == 0) goto L_0x00c9
                    goto L_0x00b5
                L_0x0099:
                    java.lang.String r2 = "08"
                    boolean r0 = r0.equals(r2)
                    if (r0 == 0) goto L_0x00c9
                    goto L_0x00ca
                L_0x00a2:
                    java.lang.String r5 = "06"
                    boolean r5 = r0.equals(r5)
                    if (r5 == 0) goto L_0x00c9
                    r5 = 8
                    goto L_0x00ca
                L_0x00ad:
                    java.lang.String r5 = "03"
                    boolean r5 = r0.equals(r5)
                    if (r5 == 0) goto L_0x00c9
                L_0x00b5:
                    r5 = 6
                    goto L_0x00ca
                L_0x00b7:
                    java.lang.String r2 = "01"
                    boolean r0 = r0.equals(r2)
                    if (r0 == 0) goto L_0x00c9
                    goto L_0x00ca
                L_0x00c0:
                    java.lang.String r2 = "00"
                    boolean r0 = r0.equals(r2)
                    if (r0 == 0) goto L_0x00c9
                    goto L_0x00ca
                L_0x00c9:
                    r5 = 5
                L_0x00ca:
                    r1.setMinLength(r5)
                    return
                */
                throw new UnsupportedOperationException("Method not decompiled: com.stripe.android.view.BecsDebitWidget.AnonymousClass2.invoke(com.stripe.android.view.BecsDebitBanks$Bank):void");
            }
        });
        getViewBinding$stripe_release().bsbEditText.setOnCompletedCallback(new Function0<Unit>(this) {
            final /* synthetic */ BecsDebitWidget this$0;

            {
                this.this$0 = r1;
            }

            public final void invoke() {
                this.this$0.getViewBinding$stripe_release().accountNumberTextInputLayout.requestFocus();
            }
        });
        EmailEditText emailEditText = getViewBinding$stripe_release().emailEditText;
        StripeEditText stripeEditText = getViewBinding$stripe_release().nameEditText;
        Intrinsics.checkExpressionValueIsNotNull(stripeEditText, "viewBinding.nameEditText");
        emailEditText.setDeleteEmptyListener(new BackUpFieldDeleteListener(stripeEditText));
        BecsDebitBsbEditText becsDebitBsbEditText = getViewBinding$stripe_release().bsbEditText;
        EmailEditText emailEditText2 = getViewBinding$stripe_release().emailEditText;
        Intrinsics.checkExpressionValueIsNotNull(emailEditText2, "viewBinding.emailEditText");
        becsDebitBsbEditText.setDeleteEmptyListener(new BackUpFieldDeleteListener(emailEditText2));
        BecsDebitAccountNumberEditText becsDebitAccountNumberEditText = getViewBinding$stripe_release().accountNumberEditText;
        BecsDebitBsbEditText becsDebitBsbEditText2 = getViewBinding$stripe_release().bsbEditText;
        Intrinsics.checkExpressionValueIsNotNull(becsDebitBsbEditText2, "viewBinding.bsbEditText");
        becsDebitAccountNumberEditText.setDeleteEmptyListener(new BackUpFieldDeleteListener(becsDebitBsbEditText2));
        getViewBinding$stripe_release().nameEditText.setErrorMessage$stripe_release(getResources().getString(R.string.becs_widget_name_required));
        StripeEditText stripeEditText2 = getViewBinding$stripe_release().nameEditText;
        TextInputLayout textInputLayout = getViewBinding$stripe_release().nameTextInputLayout;
        Intrinsics.checkExpressionValueIsNotNull(textInputLayout, "viewBinding.nameTextInputLayout");
        stripeEditText2.setErrorMessageListener(new ErrorListener(textInputLayout));
        EmailEditText emailEditText3 = getViewBinding$stripe_release().emailEditText;
        TextInputLayout textInputLayout2 = getViewBinding$stripe_release().emailTextInputLayout;
        Intrinsics.checkExpressionValueIsNotNull(textInputLayout2, "viewBinding.emailTextInputLayout");
        emailEditText3.setErrorMessageListener(new ErrorListener(textInputLayout2));
        BecsDebitBsbEditText becsDebitBsbEditText3 = getViewBinding$stripe_release().bsbEditText;
        IconTextInputLayout iconTextInputLayout = getViewBinding$stripe_release().bsbTextInputLayout;
        Intrinsics.checkExpressionValueIsNotNull(iconTextInputLayout, "viewBinding.bsbTextInputLayout");
        becsDebitBsbEditText3.setErrorMessageListener(new ErrorListener(iconTextInputLayout));
        BecsDebitAccountNumberEditText becsDebitAccountNumberEditText2 = getViewBinding$stripe_release().accountNumberEditText;
        TextInputLayout textInputLayout3 = getViewBinding$stripe_release().accountNumberTextInputLayout;
        Intrinsics.checkExpressionValueIsNotNull(textInputLayout3, "viewBinding.accountNumberTextInputLayout");
        becsDebitAccountNumberEditText2.setErrorMessageListener(new ErrorListener(textInputLayout3));
        for (StripeEditText stripeEditText3 : SetsKt.setOf(getViewBinding$stripe_release().nameEditText, getViewBinding$stripe_release().emailEditText)) {
            stripeEditText3.addTextChangedListener(new BecsDebitWidget$4$1(stripeEditText3));
        }
        str = !(StringsKt.isBlank(str) ^ true) ? null : str;
        if (str != null) {
            getViewBinding$stripe_release().mandateAcceptanceTextView.setCompanyName(str);
        }
        if (attributeSet != null) {
            applyAttributes(attributeSet);
        }
        verifyCompanyName();
    }

    public final ValidParamsCallback getValidParamsCallback() {
        return this.validParamsCallback;
    }

    public final void setValidParamsCallback(ValidParamsCallback validParamsCallback2) {
        Intrinsics.checkParameterIsNotNull(validParamsCallback2, "<set-?>");
        this.validParamsCallback = validParamsCallback2;
    }

    /* access modifiers changed from: private */
    public final boolean isInputValid() {
        String fieldText$stripe_release = getViewBinding$stripe_release().nameEditText.getFieldText$stripe_release();
        String email = getViewBinding$stripe_release().emailEditText.getEmail();
        String bsb$stripe_release = getViewBinding$stripe_release().bsbEditText.getBsb$stripe_release();
        String accountNumber = getViewBinding$stripe_release().accountNumberEditText.getAccountNumber();
        if (!StringsKt.isBlank(fieldText$stripe_release)) {
            CharSequence charSequence = email;
            if (!(charSequence == null || StringsKt.isBlank(charSequence))) {
                CharSequence charSequence2 = bsb$stripe_release;
                if (!(charSequence2 == null || StringsKt.isBlank(charSequence2))) {
                    CharSequence charSequence3 = accountNumber;
                    if (!(charSequence3 == null || StringsKt.isBlank(charSequence3))) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    private final void verifyCompanyName() {
        if (!getViewBinding$stripe_release().mandateAcceptanceTextView.isValid$stripe_release()) {
            throw new IllegalArgumentException("A company name is required to render a BecsDebitWidget.".toString());
        }
    }

    private final void applyAttributes(AttributeSet attributeSet) {
        Context context = getContext();
        Intrinsics.checkExpressionValueIsNotNull(context, "context");
        TypedArray obtainStyledAttributes = context.getTheme().obtainStyledAttributes(attributeSet, R.styleable.BecsDebitWidget, 0, 0);
        try {
            String string = obtainStyledAttributes.getString(R.styleable.BecsDebitWidget_companyName);
            if (string != null) {
                getViewBinding$stripe_release().mandateAcceptanceTextView.setCompanyName(string);
            }
        } finally {
            obtainStyledAttributes.recycle();
        }
    }

    public final PaymentMethodCreateParams getParams() {
        String fieldText$stripe_release = getViewBinding$stripe_release().nameEditText.getFieldText$stripe_release();
        String email = getViewBinding$stripe_release().emailEditText.getEmail();
        String bsb$stripe_release = getViewBinding$stripe_release().bsbEditText.getBsb$stripe_release();
        String accountNumber = getViewBinding$stripe_release().accountNumberEditText.getAccountNumber();
        CharSequence charSequence = fieldText$stripe_release;
        getViewBinding$stripe_release().nameEditText.setShouldShowError(StringsKt.isBlank(charSequence));
        CharSequence charSequence2 = email;
        boolean z = false;
        getViewBinding$stripe_release().emailEditText.setShouldShowError(charSequence2 == null || StringsKt.isBlank(charSequence2));
        CharSequence charSequence3 = bsb$stripe_release;
        getViewBinding$stripe_release().bsbEditText.setShouldShowError(charSequence3 == null || StringsKt.isBlank(charSequence3));
        CharSequence charSequence4 = accountNumber;
        getViewBinding$stripe_release().accountNumberEditText.setShouldShowError(charSequence4 == null || StringsKt.isBlank(charSequence4));
        if (StringsKt.isBlank(charSequence)) {
            return null;
        }
        if (charSequence2 == null || StringsKt.isBlank(charSequence2)) {
            return null;
        }
        if (charSequence3 == null || StringsKt.isBlank(charSequence3)) {
            return null;
        }
        if (charSequence4 == null || StringsKt.isBlank(charSequence4)) {
            z = true;
        }
        if (z) {
            return null;
        }
        return PaymentMethodCreateParams.Companion.create$default(PaymentMethodCreateParams.Companion, new PaymentMethodCreateParams.AuBecsDebit(bsb$stripe_release, accountNumber), new PaymentMethod.BillingDetails((Address) null, email, fieldText$stripe_release, (String) null, 9, (DefaultConstructorMarker) null), (Map) null, 4, (Object) null);
    }
}
