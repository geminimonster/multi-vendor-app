package com.stripe.android.view;

import android.app.Activity;
import android.content.DialogInterface;
import android.webkit.ConsoleMessage;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import androidx.appcompat.app.AlertDialog;
import com.stripe.android.Logger;
import com.stripe.android.R;
import kotlin.Metadata;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000-\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000*\u0001\u0000\b\n\u0018\u00002\u00020\u0001J\u0012\u0010\u0002\u001a\u00020\u00032\b\u0010\u0004\u001a\u0004\u0018\u00010\u0005H\u0016J0\u0010\u0006\u001a\u00020\u00032\b\u0010\u0007\u001a\u0004\u0018\u00010\b2\b\u0010\t\u001a\u0004\u0018\u00010\n2\b\u0010\u000b\u001a\u0004\u0018\u00010\n2\b\u0010\f\u001a\u0004\u0018\u00010\rH\u0016¨\u0006\u000e"}, d2 = {"com/stripe/android/view/PaymentAuthWebView$init$1", "Landroid/webkit/WebChromeClient;", "onConsoleMessage", "", "consoleMessage", "Landroid/webkit/ConsoleMessage;", "onJsConfirm", "view", "Landroid/webkit/WebView;", "url", "", "message", "result", "Landroid/webkit/JsResult;", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: PaymentAuthWebView.kt */
public final class PaymentAuthWebView$init$1 extends WebChromeClient {
    final /* synthetic */ Activity $activity;
    final /* synthetic */ Logger $logger;

    PaymentAuthWebView$init$1(Logger logger, Activity activity) {
        this.$logger = logger;
        this.$activity = activity;
    }

    public boolean onConsoleMessage(ConsoleMessage consoleMessage) {
        String message;
        if (!(consoleMessage == null || (message = consoleMessage.message()) == null)) {
            this.$logger.debug(message);
        }
        return super.onConsoleMessage(consoleMessage);
    }

    public boolean onJsConfirm(WebView webView, String str, String str2, JsResult jsResult) {
        new AlertDialog.Builder(this.$activity, R.style.AlertDialogStyle).setMessage((CharSequence) str2).setPositiveButton(17039370, (DialogInterface.OnClickListener) new PaymentAuthWebView$init$1$onJsConfirm$1(jsResult)).setNegativeButton(17039360, (DialogInterface.OnClickListener) new PaymentAuthWebView$init$1$onJsConfirm$2(jsResult)).create().show();
        return true;
    }
}
