package com.stripe.android.view;

import com.stripe.android.model.ShippingMethod;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n¢\u0006\u0002\b\u0002"}, d2 = {"<anonymous>", "Lcom/stripe/android/view/PaymentFlowPagerAdapter;", "invoke"}, k = 3, mv = {1, 1, 16})
/* compiled from: PaymentFlowActivity.kt */
final class PaymentFlowActivity$paymentFlowPagerAdapter$2 extends Lambda implements Function0<PaymentFlowPagerAdapter> {
    final /* synthetic */ PaymentFlowActivity this$0;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    PaymentFlowActivity$paymentFlowPagerAdapter$2(PaymentFlowActivity paymentFlowActivity) {
        super(0);
        this.this$0 = paymentFlowActivity;
    }

    public final PaymentFlowPagerAdapter invoke() {
        PaymentFlowActivity paymentFlowActivity = this.this$0;
        return new PaymentFlowPagerAdapter(paymentFlowActivity, paymentFlowActivity.getPaymentSessionConfig(), this.this$0.getPaymentSessionConfig().getAllowedShippingCountryCodes(), new Function1<ShippingMethod, Unit>(this) {
            final /* synthetic */ PaymentFlowActivity$paymentFlowPagerAdapter$2 this$0;

            {
                this.this$0 = r1;
            }

            public /* bridge */ /* synthetic */ Object invoke(Object obj) {
                invoke((ShippingMethod) obj);
                return Unit.INSTANCE;
            }

            public final void invoke(ShippingMethod shippingMethod) {
                Intrinsics.checkParameterIsNotNull(shippingMethod, "it");
                this.this$0.this$0.getViewModel().setSelectedShippingMethod$stripe_release(shippingMethod);
            }
        });
    }
}
