package com.stripe.android.view;

import android.content.Context;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.widget.EditText;
import com.google.android.material.R;
import com.google.android.material.textfield.TextInputLayout;
import com.stripe.android.utils.ClassUtils;
import java.lang.reflect.Method;
import java.util.Set;
import kotlin.Metadata;
import kotlin.collections.SetsKt;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\t\u0018\u0000 \u001a2\u00020\u0001:\u0001\u001aB%\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\b\b\u0002\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ\b\u0010\u000f\u001a\u00020\u0010H\u0002J\r\u0010\u0011\u001a\u00020\u0012H\u0001¢\u0006\u0002\b\u0013J0\u0010\u0014\u001a\u00020\u00102\u0006\u0010\u0015\u001a\u00020\u00122\u0006\u0010\u0016\u001a\u00020\u00072\u0006\u0010\u0017\u001a\u00020\u00072\u0006\u0010\u0018\u001a\u00020\u00072\u0006\u0010\u0019\u001a\u00020\u0007H\u0014R\u0010\u0010\t\u001a\u0004\u0018\u00010\nX\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u000b\u001a\u0004\u0018\u00010\fX\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\r\u001a\u0004\u0018\u00010\u000eX\u0004¢\u0006\u0002\n\u0000¨\u0006\u001b"}, d2 = {"Lcom/stripe/android/view/IconTextInputLayout;", "Lcom/google/android/material/textfield/TextInputLayout;", "context", "Landroid/content/Context;", "attrs", "Landroid/util/AttributeSet;", "defStyleAttr", "", "(Landroid/content/Context;Landroid/util/AttributeSet;I)V", "bounds", "Landroid/graphics/Rect;", "collapsingTextHelper", "", "recalculateMethod", "Ljava/lang/reflect/Method;", "adjustBounds", "", "hasObtainedCollapsingTextHelper", "", "hasObtainedCollapsingTextHelper$stripe_release", "onLayout", "changed", "left", "top", "right", "bottom", "Companion", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: IconTextInputLayout.kt */
public final class IconTextInputLayout extends TextInputLayout {
    private static final Set<String> BOUNDS_FIELD_NAMES = SetsKt.setOf("mCollapsedBounds", "collapsedBounds");
    @Deprecated
    public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
    private static final Set<String> RECALCULATE_METHOD_NAMES = SetsKt.setOf("recalculate");
    private static final Set<String> TEXT_FIELD_NAMES = SetsKt.setOf("mCollapsingTextHelper", "collapsingTextHelper");
    private final Rect bounds;
    private final Object collapsingTextHelper;
    private final Method recalculateMethod;

    public IconTextInputLayout(Context context) {
        this(context, (AttributeSet) null, 0, 6, (DefaultConstructorMarker) null);
    }

    public IconTextInputLayout(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0, 4, (DefaultConstructorMarker) null);
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ IconTextInputLayout(Context context, AttributeSet attributeSet, int i, int i2, DefaultConstructorMarker defaultConstructorMarker) {
        this(context, (i2 & 2) != 0 ? null : attributeSet, (i2 & 4) != 0 ? R.attr.textInputStyle : i);
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public IconTextInputLayout(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        Intrinsics.checkParameterIsNotNull(context, "context");
        Object internalObject = ClassUtils.getInternalObject(TextInputLayout.class, TEXT_FIELD_NAMES, this);
        this.collapsingTextHelper = internalObject;
        if (internalObject == null) {
            this.bounds = null;
            this.recalculateMethod = null;
            return;
        }
        this.bounds = (Rect) ClassUtils.getInternalObject(internalObject.getClass(), BOUNDS_FIELD_NAMES, this.collapsingTextHelper);
        this.recalculateMethod = ClassUtils.findMethod(this.collapsingTextHelper.getClass(), RECALCULATE_METHOD_NAMES);
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        super.onLayout(z, i, i2, i3, i4);
        adjustBounds();
    }

    private final void adjustBounds() {
        Rect rect;
        EditText editText = getEditText();
        if (this.collapsingTextHelper != null && (rect = this.bounds) != null && this.recalculateMethod != null && editText != null) {
            try {
                rect.left = editText.getLeft() + editText.getPaddingStart();
                this.recalculateMethod.invoke(this.collapsingTextHelper, new Object[0]);
            } catch (Exception unused) {
            }
        }
    }

    public final boolean hasObtainedCollapsingTextHelper$stripe_release() {
        return (this.collapsingTextHelper == null || this.bounds == null || this.recalculateMethod == null) ? false : true;
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\"\n\u0002\u0010\u000e\n\u0002\b\u0003\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0014\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X\u0004¢\u0006\u0002\n\u0000¨\u0006\b"}, d2 = {"Lcom/stripe/android/view/IconTextInputLayout$Companion;", "", "()V", "BOUNDS_FIELD_NAMES", "", "", "RECALCULATE_METHOD_NAMES", "TEXT_FIELD_NAMES", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: IconTextInputLayout.kt */
    private static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }
}
