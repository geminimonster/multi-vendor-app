package com.stripe.android.view;

import android.content.res.Resources;
import android.text.ParcelableSpan;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.TypefaceSpan;
import com.stripe.android.R;
import com.stripe.android.model.CardBrand;
import com.stripe.android.model.PaymentMethod;
import java.util.Map;
import kotlin.Metadata;
import kotlin.TuplesKt;
import kotlin.collections.MapsKt;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000X\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\b\u0000\u0018\u0000 \u001f2\u00020\u0001:\u0001\u001fB\u000f\b\u0010\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004B\u0017\b\u0000\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\b¢\u0006\u0002\u0010\tJ'\u0010\n\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\r2\b\u0010\u000e\u001a\u0004\u0018\u00010\u000f2\u0006\u0010\u0010\u001a\u00020\u0011H\u0000¢\u0006\u0002\b\u0012J\u0015\u0010\u0013\u001a\u00020\u000f2\u0006\u0010\u0014\u001a\u00020\u0015H\u0000¢\u0006\u0002\b\u0016J(\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00020\u000b2\u0006\u0010\u001a\u001a\u00020\u001b2\u0006\u0010\u001c\u001a\u00020\u001d2\u0006\u0010\u001e\u001a\u00020\u001dH\u0002R\u000e\u0010\u0005\u001a\u00020\u0006X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\bX\u0004¢\u0006\u0002\n\u0000¨\u0006 "}, d2 = {"Lcom/stripe/android/view/CardDisplayTextFactory;", "", "context", "Landroid/content/Context;", "(Landroid/content/Context;)V", "resources", "Landroid/content/res/Resources;", "themeConfig", "Lcom/stripe/android/view/ThemeConfig;", "(Landroid/content/res/Resources;Lcom/stripe/android/view/ThemeConfig;)V", "createStyled", "Landroid/text/SpannableString;", "brand", "Lcom/stripe/android/model/CardBrand;", "last4", "", "isSelected", "", "createStyled$stripe_release", "createUnstyled", "card", "Lcom/stripe/android/model/PaymentMethod$Card;", "createUnstyled$stripe_release", "setSpan", "", "displayString", "span", "Landroid/text/ParcelableSpan;", "start", "", "end", "Companion", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: CardDisplayTextFactory.kt */
public final class CardDisplayTextFactory {
    private static final Map<String, Integer> BRAND_RESOURCE_MAP = MapsKt.mapOf(TuplesKt.to("amex", Integer.valueOf(R.string.amex_short)), TuplesKt.to("diners", Integer.valueOf(R.string.diners_club)), TuplesKt.to("discover", Integer.valueOf(R.string.discover)), TuplesKt.to("jcb", Integer.valueOf(R.string.jcb)), TuplesKt.to("mastercard", Integer.valueOf(R.string.mastercard)), TuplesKt.to("visa", Integer.valueOf(R.string.visa)), TuplesKt.to("unionpay", Integer.valueOf(R.string.unionpay)), TuplesKt.to("unknown", Integer.valueOf(R.string.unknown)));
    @Deprecated
    public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
    private final Resources resources;
    private final ThemeConfig themeConfig;

    public CardDisplayTextFactory(Resources resources2, ThemeConfig themeConfig2) {
        Intrinsics.checkParameterIsNotNull(resources2, "resources");
        Intrinsics.checkParameterIsNotNull(themeConfig2, "themeConfig");
        this.resources = resources2;
        this.themeConfig = themeConfig2;
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public CardDisplayTextFactory(android.content.Context r3) {
        /*
            r2 = this;
            java.lang.String r0 = "context"
            kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(r3, r0)
            android.content.res.Resources r0 = r3.getResources()
            java.lang.String r1 = "context.resources"
            kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r0, r1)
            com.stripe.android.view.ThemeConfig r1 = new com.stripe.android.view.ThemeConfig
            r1.<init>(r3)
            r2.<init>(r0, r1)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.stripe.android.view.CardDisplayTextFactory.<init>(android.content.Context):void");
    }

    public final /* synthetic */ SpannableString createStyled$stripe_release(CardBrand cardBrand, String str, boolean z) {
        Intrinsics.checkParameterIsNotNull(cardBrand, "brand");
        String displayName = cardBrand.getDisplayName();
        int length = displayName.length();
        if (str == null) {
            SpannableString spannableString = new SpannableString(displayName);
            setSpan(spannableString, new TypefaceSpan("sans-serif-medium"), 0, length);
            return spannableString;
        }
        String string = this.resources.getString(R.string.ending_in, new Object[]{displayName, str});
        Intrinsics.checkExpressionValueIsNotNull(string, "resources.getString(R.st…ing_in, brandText, last4)");
        int length2 = string.length();
        int length3 = length2 - str.length();
        int textColor$stripe_release = this.themeConfig.getTextColor$stripe_release(z);
        int textAlphaColor$stripe_release = this.themeConfig.getTextAlphaColor$stripe_release(z);
        SpannableString spannableString2 = new SpannableString(string);
        setSpan(spannableString2, new TypefaceSpan("sans-serif-medium"), 0, length);
        setSpan(spannableString2, new ForegroundColorSpan(textColor$stripe_release), 0, length);
        setSpan(spannableString2, new ForegroundColorSpan(textAlphaColor$stripe_release), length, length3);
        setSpan(spannableString2, new TypefaceSpan("sans-serif-medium"), length3, length2);
        setSpan(spannableString2, new ForegroundColorSpan(textColor$stripe_release), length3, length2);
        return spannableString2;
    }

    public final /* synthetic */ String createUnstyled$stripe_release(PaymentMethod.Card card) {
        Intrinsics.checkParameterIsNotNull(card, "card");
        Resources resources2 = this.resources;
        int i = R.string.ending_in;
        Object[] objArr = new Object[2];
        Resources resources3 = this.resources;
        Integer num = BRAND_RESOURCE_MAP.get(card.brand);
        objArr[0] = resources3.getString(num != null ? num.intValue() : R.string.unknown);
        objArr[1] = card.last4;
        String string = resources2.getString(i, objArr);
        Intrinsics.checkExpressionValueIsNotNull(string, "resources.getString(\n   …     card.last4\n        )");
        return string;
    }

    private final void setSpan(SpannableString spannableString, ParcelableSpan parcelableSpan, int i, int i2) {
        spannableString.setSpan(parcelableSpan, i, i2, 33);
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\u0010\u000e\n\u0002\u0010\b\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u001a\u0010\u0003\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0007"}, d2 = {"Lcom/stripe/android/view/CardDisplayTextFactory$Companion;", "", "()V", "BRAND_RESOURCE_MAP", "", "", "", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: CardDisplayTextFactory.kt */
    private static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }
}
