package com.stripe.android.view;

import android.content.Context;
import android.os.Build;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.text.Editable;
import android.text.InputFilter;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import com.google.android.material.textfield.TextInputLayout;
import com.stripe.android.R;
import com.stripe.android.databinding.AddressWidgetBinding;
import com.stripe.android.model.Address;
import com.stripe.android.model.ShippingInformation;
import java.lang.annotation.RetentionPolicy;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import kotlin.Lazy;
import kotlin.LazyKt;
import kotlin.Metadata;
import kotlin.annotation.AnnotationRetention;
import kotlin.annotation.Retention;
import kotlin.collections.CollectionsKt;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000~\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0010\u000e\n\u0002\b\b\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0010\"\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001:\u0001KB%\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\b\b\u0002\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ\b\u0010-\u001a\u00020.H\u0002J\b\u0010/\u001a\u00020.H\u0002J\u0010\u00100\u001a\u0002012\u0006\u00102\u001a\u00020\u0015H\u0002J\u0010\u00103\u001a\u0002012\u0006\u00102\u001a\u00020\u0015H\u0002J\u0010\u00104\u001a\u0002012\u0006\u00102\u001a\u00020\u0015H\u0002J\u0010\u00105\u001a\u00020.2\u0006\u00106\u001a\u000207H\u0002J\u0010\u00108\u001a\u00020.2\b\u0010#\u001a\u0004\u0018\u00010 J\b\u00109\u001a\u00020.H\u0002J\b\u0010:\u001a\u00020.H\u0002J\b\u0010;\u001a\u00020.H\u0002J\b\u0010<\u001a\u00020.H\u0002J\b\u0010=\u001a\u00020.H\u0002J\u0014\u0010>\u001a\u00020.2\f\u0010?\u001a\b\u0012\u0004\u0012\u00020\u00150@J\u0016\u0010A\u001a\u00020.2\u000e\u0010B\u001a\n\u0012\u0004\u0012\u00020\u0015\u0018\u00010\u0014J\u0016\u0010C\u001a\u00020.2\u000e\u0010D\u001a\n\u0012\u0004\u0012\u00020\u0015\u0018\u00010\u0014J\b\u0010E\u001a\u00020.H\u0002J\u0010\u0010F\u001a\u00020.2\u0006\u0010G\u001a\u00020HH\u0002J\u0010\u0010I\u001a\u00020.2\u0006\u0010G\u001a\u00020HH\u0002J\u0006\u0010J\u001a\u000201R\u000e\u0010\t\u001a\u00020\nX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\nX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\rX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\rX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\nX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\rX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0012X\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u00150\u0014X\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\nX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0017\u001a\u00020\rX\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\u0018\u001a\b\u0012\u0004\u0012\u00020\u00150\u0014X\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0019\u001a\u00020\nX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u001a\u001a\u00020\rX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u001b\u001a\u00020\nX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u001c\u001a\u00020\rX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u001d\u001a\u00020\u001eX\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\u001f\u001a\u00020 8BX\u0004¢\u0006\u0006\u001a\u0004\b!\u0010\"R\u0013\u0010#\u001a\u0004\u0018\u00010 8F¢\u0006\u0006\u001a\u0004\b$\u0010\"R\u000e\u0010%\u001a\u00020\nX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010&\u001a\u00020\rX\u0004¢\u0006\u0002\n\u0000R\u001b\u0010'\u001a\u00020(8BX\u0002¢\u0006\f\n\u0004\b+\u0010,\u001a\u0004\b)\u0010*¨\u0006L"}, d2 = {"Lcom/stripe/android/view/ShippingInfoWidget;", "Landroid/widget/LinearLayout;", "context", "Landroid/content/Context;", "attrs", "Landroid/util/AttributeSet;", "defStyleAttr", "", "(Landroid/content/Context;Landroid/util/AttributeSet;I)V", "addressEditText", "Lcom/stripe/android/view/StripeEditText;", "addressEditText2", "addressLine1TextInputLayout", "Lcom/google/android/material/textfield/TextInputLayout;", "addressLine2TextInputLayout", "cityEditText", "cityTextInputLayout", "countryAutoCompleteTextView", "Lcom/stripe/android/view/CountryAutoCompleteTextView;", "hiddenShippingInfoFields", "", "", "nameEditText", "nameTextInputLayout", "optionalShippingInfoFields", "phoneNumberEditText", "phoneNumberTextInputLayout", "postalCodeEditText", "postalCodeTextInputLayout", "postalCodeValidator", "Lcom/stripe/android/view/PostalCodeValidator;", "rawShippingInformation", "Lcom/stripe/android/model/ShippingInformation;", "getRawShippingInformation", "()Lcom/stripe/android/model/ShippingInformation;", "shippingInformation", "getShippingInformation", "stateEditText", "stateTextInputLayout", "viewBinding", "Lcom/stripe/android/databinding/AddressWidgetBinding;", "getViewBinding", "()Lcom/stripe/android/databinding/AddressWidgetBinding;", "viewBinding$delegate", "Lkotlin/Lazy;", "hideHiddenFields", "", "initView", "isFieldHidden", "", "field", "isFieldOptional", "isFieldRequired", "populateAddressFields", "address", "Lcom/stripe/android/model/Address;", "populateShippingInfo", "renderCanadianForm", "renderGreatBritainForm", "renderInternationalForm", "renderLabels", "renderUSForm", "setAllowedCountryCodes", "allowedCountryCodes", "", "setHiddenFields", "hiddenAddressFields", "setOptionalFields", "optionalAddressFields", "setupErrorHandling", "updateConfigForCountry", "country", "Lcom/stripe/android/view/Country;", "updatePostalCodeInputFilter", "validateAllFields", "CustomizableShippingField", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: ShippingInfoWidget.kt */
public final class ShippingInfoWidget extends LinearLayout {
    private final StripeEditText addressEditText;
    private final StripeEditText addressEditText2;
    private final TextInputLayout addressLine1TextInputLayout;
    private final TextInputLayout addressLine2TextInputLayout;
    private final StripeEditText cityEditText;
    private final TextInputLayout cityTextInputLayout;
    private final CountryAutoCompleteTextView countryAutoCompleteTextView;
    private List<String> hiddenShippingInfoFields;
    private final StripeEditText nameEditText;
    private final TextInputLayout nameTextInputLayout;
    private List<String> optionalShippingInfoFields;
    private final StripeEditText phoneNumberEditText;
    private final TextInputLayout phoneNumberTextInputLayout;
    private final StripeEditText postalCodeEditText;
    private final TextInputLayout postalCodeTextInputLayout;
    private final PostalCodeValidator postalCodeValidator;
    private final StripeEditText stateEditText;
    private final TextInputLayout stateTextInputLayout;
    private final Lazy viewBinding$delegate;

    public ShippingInfoWidget(Context context) {
        this(context, (AttributeSet) null, 0, 6, (DefaultConstructorMarker) null);
    }

    public ShippingInfoWidget(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0, 4, (DefaultConstructorMarker) null);
    }

    private final AddressWidgetBinding getViewBinding() {
        return (AddressWidgetBinding) this.viewBinding$delegate.getValue();
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ ShippingInfoWidget(Context context, AttributeSet attributeSet, int i, int i2, DefaultConstructorMarker defaultConstructorMarker) {
        this(context, (i2 & 2) != 0 ? null : attributeSet, (i2 & 4) != 0 ? 0 : i);
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ShippingInfoWidget(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        Intrinsics.checkParameterIsNotNull(context, "context");
        this.viewBinding$delegate = LazyKt.lazy(new ShippingInfoWidget$viewBinding$2(this, context));
        this.postalCodeValidator = new PostalCodeValidator();
        this.optionalShippingInfoFields = CollectionsKt.emptyList();
        this.hiddenShippingInfoFields = CollectionsKt.emptyList();
        CountryAutoCompleteTextView countryAutoCompleteTextView2 = getViewBinding().countryAutocompleteAaw;
        Intrinsics.checkExpressionValueIsNotNull(countryAutoCompleteTextView2, "viewBinding.countryAutocompleteAaw");
        this.countryAutoCompleteTextView = countryAutoCompleteTextView2;
        TextInputLayout textInputLayout = getViewBinding().tlAddressLine1Aaw;
        Intrinsics.checkExpressionValueIsNotNull(textInputLayout, "viewBinding.tlAddressLine1Aaw");
        this.addressLine1TextInputLayout = textInputLayout;
        TextInputLayout textInputLayout2 = getViewBinding().tlAddressLine2Aaw;
        Intrinsics.checkExpressionValueIsNotNull(textInputLayout2, "viewBinding.tlAddressLine2Aaw");
        this.addressLine2TextInputLayout = textInputLayout2;
        TextInputLayout textInputLayout3 = getViewBinding().tlCityAaw;
        Intrinsics.checkExpressionValueIsNotNull(textInputLayout3, "viewBinding.tlCityAaw");
        this.cityTextInputLayout = textInputLayout3;
        TextInputLayout textInputLayout4 = getViewBinding().tlNameAaw;
        Intrinsics.checkExpressionValueIsNotNull(textInputLayout4, "viewBinding.tlNameAaw");
        this.nameTextInputLayout = textInputLayout4;
        TextInputLayout textInputLayout5 = getViewBinding().tlPostalCodeAaw;
        Intrinsics.checkExpressionValueIsNotNull(textInputLayout5, "viewBinding.tlPostalCodeAaw");
        this.postalCodeTextInputLayout = textInputLayout5;
        TextInputLayout textInputLayout6 = getViewBinding().tlStateAaw;
        Intrinsics.checkExpressionValueIsNotNull(textInputLayout6, "viewBinding.tlStateAaw");
        this.stateTextInputLayout = textInputLayout6;
        TextInputLayout textInputLayout7 = getViewBinding().tlPhoneNumberAaw;
        Intrinsics.checkExpressionValueIsNotNull(textInputLayout7, "viewBinding.tlPhoneNumberAaw");
        this.phoneNumberTextInputLayout = textInputLayout7;
        StripeEditText stripeEditText = getViewBinding().etAddressLineOneAaw;
        Intrinsics.checkExpressionValueIsNotNull(stripeEditText, "viewBinding.etAddressLineOneAaw");
        this.addressEditText = stripeEditText;
        StripeEditText stripeEditText2 = getViewBinding().etAddressLineTwoAaw;
        Intrinsics.checkExpressionValueIsNotNull(stripeEditText2, "viewBinding.etAddressLineTwoAaw");
        this.addressEditText2 = stripeEditText2;
        StripeEditText stripeEditText3 = getViewBinding().etCityAaw;
        Intrinsics.checkExpressionValueIsNotNull(stripeEditText3, "viewBinding.etCityAaw");
        this.cityEditText = stripeEditText3;
        StripeEditText stripeEditText4 = getViewBinding().etNameAaw;
        Intrinsics.checkExpressionValueIsNotNull(stripeEditText4, "viewBinding.etNameAaw");
        this.nameEditText = stripeEditText4;
        StripeEditText stripeEditText5 = getViewBinding().etPostalCodeAaw;
        Intrinsics.checkExpressionValueIsNotNull(stripeEditText5, "viewBinding.etPostalCodeAaw");
        this.postalCodeEditText = stripeEditText5;
        StripeEditText stripeEditText6 = getViewBinding().etStateAaw;
        Intrinsics.checkExpressionValueIsNotNull(stripeEditText6, "viewBinding.etStateAaw");
        this.stateEditText = stripeEditText6;
        StripeEditText stripeEditText7 = getViewBinding().etPhoneNumberAaw;
        Intrinsics.checkExpressionValueIsNotNull(stripeEditText7, "viewBinding.etPhoneNumberAaw");
        this.phoneNumberEditText = stripeEditText7;
        setOrientation(1);
        if (Build.VERSION.SDK_INT >= 26) {
            this.nameEditText.setAutofillHints(new String[]{"name"});
            this.addressLine1TextInputLayout.setAutofillHints(new String[]{"postalAddress"});
            this.postalCodeEditText.setAutofillHints(new String[]{"postalCode"});
            this.phoneNumberEditText.setAutofillHints(new String[]{"phone"});
        }
        initView();
    }

    public final ShippingInformation getShippingInformation() {
        if (!validateAllFields()) {
            return null;
        }
        return getRawShippingInformation();
    }

    private final ShippingInformation getRawShippingInformation() {
        Address.Builder city = new Address.Builder().setCity(this.cityEditText.getFieldText$stripe_release());
        Country selectedCountry = this.countryAutoCompleteTextView.getSelectedCountry();
        return new ShippingInformation(city.setCountry(selectedCountry != null ? selectedCountry.getCode() : null).setLine1(this.addressEditText.getFieldText$stripe_release()).setLine2(this.addressEditText2.getFieldText$stripe_release()).setPostalCode(this.postalCodeEditText.getFieldText$stripe_release()).setState(this.stateEditText.getFieldText$stripe_release()).build(), this.nameEditText.getFieldText$stripe_release(), this.phoneNumberEditText.getFieldText$stripe_release());
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u001b\n\u0002\b\u0002\b\u0002\u0018\u0000 \u00022\u00020\u0001:\u0001\u0002B\u0000¨\u0006\u0003"}, d2 = {"Lcom/stripe/android/view/ShippingInfoWidget$CustomizableShippingField;", "", "Companion", "stripe_release"}, k = 1, mv = {1, 1, 16})
    @Retention(AnnotationRetention.SOURCE)
    @java.lang.annotation.Retention(RetentionPolicy.SOURCE)
    /* compiled from: ShippingInfoWidget.kt */
    public @interface CustomizableShippingField {
        public static final String ADDRESS_LINE_ONE_FIELD = "address_line_one";
        public static final String ADDRESS_LINE_TWO_FIELD = "address_line_two";
        public static final String CITY_FIELD = "city";
        public static final Companion Companion = Companion.$$INSTANCE;
        public static final String PHONE_FIELD = "phone";
        public static final String POSTAL_CODE_FIELD = "postal_code";
        public static final String STATE_FIELD = "state";

        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0006\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\n"}, d2 = {"Lcom/stripe/android/view/ShippingInfoWidget$CustomizableShippingField$Companion;", "", "()V", "ADDRESS_LINE_ONE_FIELD", "", "ADDRESS_LINE_TWO_FIELD", "CITY_FIELD", "PHONE_FIELD", "POSTAL_CODE_FIELD", "STATE_FIELD", "stripe_release"}, k = 1, mv = {1, 1, 16})
        /* compiled from: ShippingInfoWidget.kt */
        public static final class Companion {
            static final /* synthetic */ Companion $$INSTANCE = new Companion();
            public static final String ADDRESS_LINE_ONE_FIELD = "address_line_one";
            public static final String ADDRESS_LINE_TWO_FIELD = "address_line_two";
            public static final String CITY_FIELD = "city";
            public static final String PHONE_FIELD = "phone";
            public static final String POSTAL_CODE_FIELD = "postal_code";
            public static final String STATE_FIELD = "state";

            private Companion() {
            }
        }
    }

    public final void setOptionalFields(List<String> list) {
        if (list == null) {
            list = CollectionsKt.emptyList();
        }
        this.optionalShippingInfoFields = list;
        renderLabels();
        Country selectedCountry = this.countryAutoCompleteTextView.getSelectedCountry();
        if (selectedCountry != null) {
            updateConfigForCountry(selectedCountry);
        }
    }

    public final void setHiddenFields(List<String> list) {
        if (list == null) {
            list = CollectionsKt.emptyList();
        }
        this.hiddenShippingInfoFields = list;
        renderLabels();
        Country selectedCountry = this.countryAutoCompleteTextView.getSelectedCountry();
        if (selectedCountry != null) {
            updateConfigForCountry(selectedCountry);
        }
    }

    public final void populateShippingInfo(ShippingInformation shippingInformation) {
        if (shippingInformation != null) {
            Address address = shippingInformation.getAddress();
            if (address != null) {
                populateAddressFields(address);
            }
            this.nameEditText.setText(shippingInformation.getName());
            this.phoneNumberEditText.setText(shippingInformation.getPhone());
        }
    }

    private final void populateAddressFields(Address address) {
        this.cityEditText.setText(address.getCity());
        String country = address.getCountry();
        if (country != null) {
            if (country.length() > 0) {
                this.countryAutoCompleteTextView.setCountrySelected$stripe_release(country);
            }
        }
        this.addressEditText.setText(address.getLine1());
        this.addressEditText2.setText(address.getLine2());
        this.postalCodeEditText.setText(address.getPostalCode());
        this.stateEditText.setText(address.getState());
    }

    public final void setAllowedCountryCodes(Set<String> set) {
        Intrinsics.checkParameterIsNotNull(set, "allowedCountryCodes");
        this.countryAutoCompleteTextView.setAllowedCountryCodes$stripe_release(set);
    }

    public final boolean validateAllFields() {
        String obj;
        Editable text;
        String obj2;
        Editable text2;
        String obj3;
        Editable text3;
        String obj4;
        Editable text4;
        String obj5;
        Editable text5;
        String obj6;
        Editable text6 = this.addressEditText.getText();
        if (text6 == null || (obj = text6.toString()) == null || (text = this.nameEditText.getText()) == null || (obj2 = text.toString()) == null || (text2 = this.cityEditText.getText()) == null || (obj3 = text2.toString()) == null || (text3 = this.stateEditText.getText()) == null || (obj4 = text3.toString()) == null || (text4 = this.postalCodeEditText.getText()) == null || (obj5 = text4.toString()) == null || (text5 = this.phoneNumberEditText.getText()) == null || (obj6 = text5.toString()) == null) {
            return false;
        }
        this.countryAutoCompleteTextView.validateCountry$stripe_release();
        Country selectedCountry = this.countryAutoCompleteTextView.getSelectedCountry();
        boolean isValid = this.postalCodeValidator.isValid(obj5, selectedCountry != null ? selectedCountry.getCode() : null, this.optionalShippingInfoFields, this.hiddenShippingInfoFields);
        this.postalCodeEditText.setShouldShowError(!isValid);
        boolean z = (obj.length() == 0) && isFieldRequired("address_line_one");
        this.addressEditText.setShouldShowError(z);
        boolean z2 = (obj3.length() == 0) && isFieldRequired("city");
        this.cityEditText.setShouldShowError(z2);
        boolean z3 = obj2.length() == 0;
        this.nameEditText.setShouldShowError(z3);
        boolean z4 = (obj4.length() == 0) && isFieldRequired("state");
        this.stateEditText.setShouldShowError(z4);
        boolean z5 = (obj6.length() == 0) && isFieldRequired("phone");
        this.phoneNumberEditText.setShouldShowError(z5);
        if (!isValid || z || z2 || z4 || z3 || z5 || selectedCountry == null) {
            return false;
        }
        return true;
    }

    private final boolean isFieldRequired(String str) {
        return !isFieldOptional(str) && !isFieldHidden(str);
    }

    private final boolean isFieldOptional(String str) {
        return this.optionalShippingInfoFields.contains(str);
    }

    private final boolean isFieldHidden(String str) {
        return this.hiddenShippingInfoFields.contains(str);
    }

    private final void initView() {
        ShippingInfoWidget shippingInfoWidget = this;
        this.countryAutoCompleteTextView.setCountryChangeCallback$stripe_release(new ShippingInfoWidget$initView$1(shippingInfoWidget));
        this.phoneNumberEditText.addTextChangedListener(new PhoneNumberFormattingTextWatcher());
        setupErrorHandling();
        renderLabels();
        Country selectedCountry = this.countryAutoCompleteTextView.getSelectedCountry();
        if (selectedCountry != null) {
            shippingInfoWidget.updateConfigForCountry(selectedCountry);
        }
    }

    private final void setupErrorHandling() {
        this.addressEditText.setErrorMessageListener(new ErrorListener(this.addressLine1TextInputLayout));
        this.cityEditText.setErrorMessageListener(new ErrorListener(this.cityTextInputLayout));
        this.nameEditText.setErrorMessageListener(new ErrorListener(this.nameTextInputLayout));
        this.postalCodeEditText.setErrorMessageListener(new ErrorListener(this.postalCodeTextInputLayout));
        this.stateEditText.setErrorMessageListener(new ErrorListener(this.stateTextInputLayout));
        this.phoneNumberEditText.setErrorMessageListener(new ErrorListener(this.phoneNumberTextInputLayout));
        this.addressEditText.setErrorMessage(getResources().getString(R.string.address_required));
        this.cityEditText.setErrorMessage(getResources().getString(R.string.address_city_required));
        this.nameEditText.setErrorMessage(getResources().getString(R.string.address_name_required));
        this.phoneNumberEditText.setErrorMessage(getResources().getString(R.string.address_phone_number_required));
    }

    private final void renderLabels() {
        CharSequence charSequence;
        CharSequence charSequence2;
        this.nameTextInputLayout.setHint(getResources().getString(R.string.address_label_name));
        TextInputLayout textInputLayout = this.cityTextInputLayout;
        if (isFieldOptional("city")) {
            charSequence = getResources().getString(R.string.address_label_city_optional);
        } else {
            charSequence = getResources().getString(R.string.address_label_city);
        }
        textInputLayout.setHint(charSequence);
        TextInputLayout textInputLayout2 = this.phoneNumberTextInputLayout;
        if (isFieldOptional("phone")) {
            charSequence2 = getResources().getString(R.string.address_label_phone_number_optional);
        } else {
            charSequence2 = getResources().getString(R.string.address_label_phone_number);
        }
        textInputLayout2.setHint(charSequence2);
        hideHiddenFields();
    }

    private final void hideHiddenFields() {
        if (isFieldHidden("address_line_one")) {
            this.addressLine1TextInputLayout.setVisibility(8);
        }
        if (isFieldHidden("address_line_two")) {
            this.addressLine2TextInputLayout.setVisibility(8);
        }
        if (isFieldHidden("state")) {
            this.stateTextInputLayout.setVisibility(8);
        }
        if (isFieldHidden("city")) {
            this.cityTextInputLayout.setVisibility(8);
        }
        if (isFieldHidden("postal_code")) {
            this.postalCodeTextInputLayout.setVisibility(8);
        }
        if (isFieldHidden("phone")) {
            this.phoneNumberTextInputLayout.setVisibility(8);
        }
    }

    /* access modifiers changed from: private */
    public final void updateConfigForCountry(Country country) {
        String code = country.getCode();
        Locale locale = Locale.US;
        Intrinsics.checkExpressionValueIsNotNull(locale, "Locale.US");
        if (Intrinsics.areEqual((Object) code, (Object) locale.getCountry())) {
            renderUSForm();
        } else {
            Locale locale2 = Locale.UK;
            Intrinsics.checkExpressionValueIsNotNull(locale2, "Locale.UK");
            if (Intrinsics.areEqual((Object) code, (Object) locale2.getCountry())) {
                renderGreatBritainForm();
            } else {
                Locale locale3 = Locale.CANADA;
                Intrinsics.checkExpressionValueIsNotNull(locale3, "Locale.CANADA");
                if (Intrinsics.areEqual((Object) code, (Object) locale3.getCountry())) {
                    renderCanadianForm();
                } else {
                    renderInternationalForm();
                }
            }
        }
        updatePostalCodeInputFilter(country);
        this.postalCodeTextInputLayout.setVisibility((!CountryUtils.INSTANCE.doesCountryUsePostalCode$stripe_release(country.getCode()) || isFieldHidden("postal_code")) ? 8 : 0);
    }

    private final void updatePostalCodeInputFilter(Country country) {
        StripeEditText stripeEditText = this.postalCodeEditText;
        String code = country.getCode();
        Locale locale = Locale.CANADA;
        Intrinsics.checkExpressionValueIsNotNull(locale, "Locale.CANADA");
        stripeEditText.setFilters(Intrinsics.areEqual((Object) code, (Object) locale.getCountry()) ? new InputFilter[]{new InputFilter.AllCaps()} : new InputFilter[0]);
    }

    private final void renderUSForm() {
        CharSequence charSequence;
        CharSequence charSequence2;
        CharSequence charSequence3;
        TextInputLayout textInputLayout = this.addressLine1TextInputLayout;
        if (isFieldOptional("address_line_one")) {
            charSequence = getResources().getString(R.string.address_label_address_optional);
        } else {
            charSequence = getResources().getString(R.string.address_label_address);
        }
        textInputLayout.setHint(charSequence);
        this.addressLine2TextInputLayout.setHint(getResources().getString(R.string.address_label_apt_optional));
        TextInputLayout textInputLayout2 = this.postalCodeTextInputLayout;
        if (isFieldOptional("postal_code")) {
            charSequence2 = getResources().getString(R.string.address_label_zip_code_optional);
        } else {
            charSequence2 = getResources().getString(R.string.address_label_zip_code);
        }
        textInputLayout2.setHint(charSequence2);
        TextInputLayout textInputLayout3 = this.stateTextInputLayout;
        if (isFieldOptional("state")) {
            charSequence3 = getResources().getString(R.string.address_label_state_optional);
        } else {
            charSequence3 = getResources().getString(R.string.address_label_state);
        }
        textInputLayout3.setHint(charSequence3);
        this.postalCodeEditText.setErrorMessage(getResources().getString(R.string.address_zip_invalid));
        this.stateEditText.setErrorMessage(getResources().getString(R.string.address_state_required));
    }

    private final void renderGreatBritainForm() {
        CharSequence charSequence;
        CharSequence charSequence2;
        CharSequence charSequence3;
        TextInputLayout textInputLayout = this.addressLine1TextInputLayout;
        if (isFieldOptional("address_line_one")) {
            charSequence = getResources().getString(R.string.address_label_address_line1_optional);
        } else {
            charSequence = getResources().getString(R.string.address_label_address_line1);
        }
        textInputLayout.setHint(charSequence);
        this.addressLine2TextInputLayout.setHint(getResources().getString(R.string.address_label_address_line2_optional));
        TextInputLayout textInputLayout2 = this.postalCodeTextInputLayout;
        if (isFieldOptional("postal_code")) {
            charSequence2 = getResources().getString(R.string.address_label_postcode_optional);
        } else {
            charSequence2 = getResources().getString(R.string.address_label_postcode);
        }
        textInputLayout2.setHint(charSequence2);
        TextInputLayout textInputLayout3 = this.stateTextInputLayout;
        if (isFieldOptional("state")) {
            charSequence3 = getResources().getString(R.string.address_label_county_optional);
        } else {
            charSequence3 = getResources().getString(R.string.address_label_county);
        }
        textInputLayout3.setHint(charSequence3);
        this.postalCodeEditText.setErrorMessage(getResources().getString(R.string.address_postcode_invalid));
        this.stateEditText.setErrorMessage(getResources().getString(R.string.address_county_required));
    }

    private final void renderCanadianForm() {
        CharSequence charSequence;
        CharSequence charSequence2;
        CharSequence charSequence3;
        TextInputLayout textInputLayout = this.addressLine1TextInputLayout;
        if (isFieldOptional("address_line_one")) {
            charSequence = getResources().getString(R.string.address_label_address_optional);
        } else {
            charSequence = getResources().getString(R.string.address_label_address);
        }
        textInputLayout.setHint(charSequence);
        this.addressLine2TextInputLayout.setHint(getResources().getString(R.string.address_label_apt_optional));
        TextInputLayout textInputLayout2 = this.postalCodeTextInputLayout;
        if (isFieldOptional("postal_code")) {
            charSequence2 = getResources().getString(R.string.address_label_postal_code_optional);
        } else {
            charSequence2 = getResources().getString(R.string.address_label_postal_code);
        }
        textInputLayout2.setHint(charSequence2);
        TextInputLayout textInputLayout3 = this.stateTextInputLayout;
        if (isFieldOptional("state")) {
            charSequence3 = getResources().getString(R.string.address_label_province_optional);
        } else {
            charSequence3 = getResources().getString(R.string.address_label_province);
        }
        textInputLayout3.setHint(charSequence3);
        this.postalCodeEditText.setErrorMessage(getResources().getString(R.string.address_postal_code_invalid));
        this.stateEditText.setErrorMessage(getResources().getString(R.string.address_province_required));
    }

    private final void renderInternationalForm() {
        CharSequence charSequence;
        CharSequence charSequence2;
        CharSequence charSequence3;
        TextInputLayout textInputLayout = this.addressLine1TextInputLayout;
        if (isFieldOptional("address_line_one")) {
            charSequence = getResources().getString(R.string.address_label_address_line1_optional);
        } else {
            charSequence = getResources().getString(R.string.address_label_address_line1);
        }
        textInputLayout.setHint(charSequence);
        this.addressLine2TextInputLayout.setHint(getResources().getString(R.string.address_label_address_line2_optional));
        TextInputLayout textInputLayout2 = this.postalCodeTextInputLayout;
        if (isFieldOptional("postal_code")) {
            charSequence2 = getResources().getString(R.string.address_label_zip_postal_code_optional);
        } else {
            charSequence2 = getResources().getString(R.string.address_label_zip_postal_code);
        }
        textInputLayout2.setHint(charSequence2);
        TextInputLayout textInputLayout3 = this.stateTextInputLayout;
        if (isFieldOptional("state")) {
            charSequence3 = getResources().getString(R.string.address_label_region_generic_optional);
        } else {
            charSequence3 = getResources().getString(R.string.address_label_region_generic);
        }
        textInputLayout3.setHint(charSequence3);
        this.postalCodeEditText.setErrorMessage(getResources().getString(R.string.address_zip_postal_invalid));
        this.stateEditText.setErrorMessage(getResources().getString(R.string.address_region_generic_required));
    }
}
