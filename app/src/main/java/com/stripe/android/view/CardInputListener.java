package com.stripe.android.view;

import java.lang.annotation.RetentionPolicy;
import kotlin.Metadata;
import kotlin.annotation.AnnotationRetention;
import kotlin.annotation.Retention;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0010\u000e\n\u0002\b\u0002\bf\u0018\u00002\u00020\u0001:\u0001\tJ\b\u0010\u0002\u001a\u00020\u0003H&J\b\u0010\u0004\u001a\u00020\u0003H&J\b\u0010\u0005\u001a\u00020\u0003H&J\u0010\u0010\u0006\u001a\u00020\u00032\u0006\u0010\u0007\u001a\u00020\bH&¨\u0006\n"}, d2 = {"Lcom/stripe/android/view/CardInputListener;", "", "onCardComplete", "", "onCvcComplete", "onExpirationComplete", "onFocusChange", "focusField", "", "FocusField", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: CardInputListener.kt */
public interface CardInputListener {
    void onCardComplete();

    void onCvcComplete();

    void onExpirationComplete();

    void onFocusChange(String str);

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u001b\n\u0002\b\u0002\b\u0002\u0018\u0000 \u00022\u00020\u0001:\u0001\u0002B\u0000¨\u0006\u0003"}, d2 = {"Lcom/stripe/android/view/CardInputListener$FocusField;", "", "Companion", "stripe_release"}, k = 1, mv = {1, 1, 16})
    @Retention(AnnotationRetention.SOURCE)
    @java.lang.annotation.Retention(RetentionPolicy.SOURCE)
    /* compiled from: CardInputListener.kt */
    public @interface FocusField {
        public static final Companion Companion = Companion.$$INSTANCE;
        public static final String FOCUS_CARD = "focus_card";
        public static final String FOCUS_CVC = "focus_cvc";
        public static final String FOCUS_EXPIRY = "focus_expiry";
        public static final String FOCUS_POSTAL = "focus_postal";

        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0004\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\b"}, d2 = {"Lcom/stripe/android/view/CardInputListener$FocusField$Companion;", "", "()V", "FOCUS_CARD", "", "FOCUS_CVC", "FOCUS_EXPIRY", "FOCUS_POSTAL", "stripe_release"}, k = 1, mv = {1, 1, 16})
        /* compiled from: CardInputListener.kt */
        public static final class Companion {
            static final /* synthetic */ Companion $$INSTANCE = new Companion();
            public static final String FOCUS_CARD = "focus_card";
            public static final String FOCUS_CVC = "focus_cvc";
            public static final String FOCUS_EXPIRY = "focus_expiry";
            public static final String FOCUS_POSTAL = "focus_postal";

            private Companion() {
            }
        }
    }
}
