package com.stripe.android.view;

import android.content.DialogInterface;
import com.stripe.android.model.PaymentMethod;
import kotlin.Metadata;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n¢\u0006\u0002\b\u0005"}, d2 = {"<anonymous>", "", "it", "Landroid/content/DialogInterface;", "kotlin.jvm.PlatformType", "onCancel"}, k = 3, mv = {1, 1, 16})
/* compiled from: DeletePaymentMethodDialogFactory.kt */
final class DeletePaymentMethodDialogFactory$create$3 implements DialogInterface.OnCancelListener {
    final /* synthetic */ PaymentMethod $paymentMethod;
    final /* synthetic */ DeletePaymentMethodDialogFactory this$0;

    DeletePaymentMethodDialogFactory$create$3(DeletePaymentMethodDialogFactory deletePaymentMethodDialogFactory, PaymentMethod paymentMethod) {
        this.this$0 = deletePaymentMethodDialogFactory;
        this.$paymentMethod = paymentMethod;
    }

    public final void onCancel(DialogInterface dialogInterface) {
        this.this$0.adapter.resetPaymentMethod$stripe_release(this.$paymentMethod);
    }
}
