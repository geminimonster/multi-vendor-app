package com.stripe.android.view;

import androidx.lifecycle.LiveDataScope;
import com.stripe.android.model.FpxBankStatuses;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.coroutines.Continuation;
import kotlin.coroutines.jvm.internal.DebugMetadata;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\u0010\u0000\u001a\u00020\u0001*\b\u0012\u0004\u0012\u00020\u00030\u0002H@¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"<anonymous>", "", "Landroidx/lifecycle/LiveDataScope;", "Lcom/stripe/android/model/FpxBankStatuses;", "invoke", "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;"}, k = 3, mv = {1, 1, 16})
@DebugMetadata(c = "com.stripe.android.view.FpxViewModel$getFpxBankStatues$1", f = "FpxViewModel.kt", i = {0, 0, 1}, l = {32, 33}, m = "invokeSuspend", n = {"$this$liveData", "$this$runCatching", "$this$liveData"}, s = {"L$0", "L$1", "L$0"})
/* compiled from: FpxViewModel.kt */
final class FpxViewModel$getFpxBankStatues$1 extends SuspendLambda implements Function2<LiveDataScope<FpxBankStatuses>, Continuation<? super Unit>, Object> {
    Object L$0;
    Object L$1;
    Object L$2;
    int label;
    private LiveDataScope p$;
    final /* synthetic */ FpxViewModel this$0;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    FpxViewModel$getFpxBankStatues$1(FpxViewModel fpxViewModel, Continuation continuation) {
        super(2, continuation);
        this.this$0 = fpxViewModel;
    }

    public final Continuation<Unit> create(Object obj, Continuation<?> continuation) {
        Intrinsics.checkParameterIsNotNull(continuation, "completion");
        FpxViewModel$getFpxBankStatues$1 fpxViewModel$getFpxBankStatues$1 = new FpxViewModel$getFpxBankStatues$1(this.this$0, continuation);
        fpxViewModel$getFpxBankStatues$1.p$ = (LiveDataScope) obj;
        return fpxViewModel$getFpxBankStatues$1;
    }

    public final Object invoke(Object obj, Object obj2) {
        return ((FpxViewModel$getFpxBankStatues$1) create(obj, (Continuation) obj2)).invokeSuspend(Unit.INSTANCE);
    }

    /* JADX WARNING: Removed duplicated region for block: B:24:0x0082  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x008f A[RETURN] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object invokeSuspend(java.lang.Object r12) {
        /*
            r11 = this;
            java.lang.Object r0 = kotlin.coroutines.intrinsics.IntrinsicsKt.getCOROUTINE_SUSPENDED()
            int r1 = r11.label
            r2 = 2
            r3 = 1
            if (r1 == 0) goto L_0x0031
            if (r1 == r3) goto L_0x001f
            if (r1 != r2) goto L_0x0017
            java.lang.Object r0 = r11.L$0
            androidx.lifecycle.LiveDataScope r0 = (androidx.lifecycle.LiveDataScope) r0
            kotlin.ResultKt.throwOnFailure(r12)
            goto L_0x0090
        L_0x0017:
            java.lang.IllegalStateException r12 = new java.lang.IllegalStateException
            java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
            r12.<init>(r0)
            throw r12
        L_0x001f:
            java.lang.Object r1 = r11.L$2
            androidx.lifecycle.LiveDataScope r1 = (androidx.lifecycle.LiveDataScope) r1
            java.lang.Object r4 = r11.L$1
            androidx.lifecycle.LiveDataScope r4 = (androidx.lifecycle.LiveDataScope) r4
            java.lang.Object r4 = r11.L$0
            androidx.lifecycle.LiveDataScope r4 = (androidx.lifecycle.LiveDataScope) r4
            kotlin.ResultKt.throwOnFailure(r12)     // Catch:{ all -> 0x002f }
            goto L_0x005e
        L_0x002f:
            r12 = move-exception
            goto L_0x0067
        L_0x0031:
            kotlin.ResultKt.throwOnFailure(r12)
            androidx.lifecycle.LiveDataScope r1 = r11.p$
            kotlin.Result$Companion r12 = kotlin.Result.Companion     // Catch:{ all -> 0x0065 }
            com.stripe.android.view.FpxViewModel r12 = r11.this$0     // Catch:{ all -> 0x0065 }
            com.stripe.android.StripeApiRepository r12 = r12.stripeRepository     // Catch:{ all -> 0x0065 }
            com.stripe.android.ApiRequest$Options r10 = new com.stripe.android.ApiRequest$Options     // Catch:{ all -> 0x0065 }
            com.stripe.android.view.FpxViewModel r4 = r11.this$0     // Catch:{ all -> 0x0065 }
            java.lang.String r5 = r4.publishableKey     // Catch:{ all -> 0x0065 }
            r6 = 0
            r7 = 0
            r8 = 6
            r9 = 0
            r4 = r10
            r4.<init>(r5, r6, r7, r8, r9)     // Catch:{ all -> 0x0065 }
            r11.L$0 = r1     // Catch:{ all -> 0x0065 }
            r11.L$1 = r1     // Catch:{ all -> 0x0065 }
            r11.L$2 = r1     // Catch:{ all -> 0x0065 }
            r11.label = r3     // Catch:{ all -> 0x0065 }
            java.lang.Object r12 = r12.getFpxBankStatus(r10, r11)     // Catch:{ all -> 0x0065 }
            if (r12 != r0) goto L_0x005d
            return r0
        L_0x005d:
            r4 = r1
        L_0x005e:
            androidx.lifecycle.LiveData r12 = (androidx.lifecycle.LiveData) r12     // Catch:{ all -> 0x002f }
            java.lang.Object r12 = kotlin.Result.m4constructorimpl(r12)     // Catch:{ all -> 0x002f }
            goto L_0x0071
        L_0x0065:
            r12 = move-exception
            r4 = r1
        L_0x0067:
            kotlin.Result$Companion r5 = kotlin.Result.Companion
            java.lang.Object r12 = kotlin.ResultKt.createFailure(r12)
            java.lang.Object r12 = kotlin.Result.m4constructorimpl(r12)
        L_0x0071:
            androidx.lifecycle.MutableLiveData r5 = new androidx.lifecycle.MutableLiveData
            com.stripe.android.model.FpxBankStatuses r6 = new com.stripe.android.model.FpxBankStatuses
            r7 = 0
            r6.<init>(r7, r3, r7)
            r5.<init>(r6)
            boolean r3 = kotlin.Result.m10isFailureimpl(r12)
            if (r3 == 0) goto L_0x0083
            r12 = r5
        L_0x0083:
            androidx.lifecycle.LiveData r12 = (androidx.lifecycle.LiveData) r12
            r11.L$0 = r4
            r11.label = r2
            java.lang.Object r12 = r1.emitSource(r12, r11)
            if (r12 != r0) goto L_0x0090
            return r0
        L_0x0090:
            kotlin.Unit r12 = kotlin.Unit.INSTANCE
            return r12
        */
        throw new UnsupportedOperationException("Method not decompiled: com.stripe.android.view.FpxViewModel$getFpxBankStatues$1.invokeSuspend(java.lang.Object):java.lang.Object");
    }
}
