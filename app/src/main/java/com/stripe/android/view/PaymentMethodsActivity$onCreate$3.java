package com.stripe.android.view;

import android.widget.ProgressBar;
import androidx.lifecycle.Observer;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n¢\u0006\u0004\b\u0005\u0010\u0006"}, d2 = {"<anonymous>", "", "it", "", "kotlin.jvm.PlatformType", "onChanged", "(Ljava/lang/Boolean;)V"}, k = 3, mv = {1, 1, 16})
/* compiled from: PaymentMethodsActivity.kt */
final class PaymentMethodsActivity$onCreate$3<T> implements Observer<Boolean> {
    final /* synthetic */ PaymentMethodsActivity this$0;

    PaymentMethodsActivity$onCreate$3(PaymentMethodsActivity paymentMethodsActivity) {
        this.this$0 = paymentMethodsActivity;
    }

    public final void onChanged(Boolean bool) {
        ProgressBar progressBar = this.this$0.getViewBinding$stripe_release().progressBar;
        Intrinsics.checkExpressionValueIsNotNull(progressBar, "viewBinding.progressBar");
        Intrinsics.checkExpressionValueIsNotNull(bool, "it");
        progressBar.setVisibility(bool.booleanValue() ? 0 : 8);
    }
}
