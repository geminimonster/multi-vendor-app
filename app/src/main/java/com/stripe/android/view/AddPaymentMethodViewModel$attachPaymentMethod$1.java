package com.stripe.android.view;

import androidx.lifecycle.MutableLiveData;
import com.stripe.android.CustomerSession;
import com.stripe.android.StripeError;
import com.stripe.android.model.PaymentMethod;
import kotlin.Metadata;
import kotlin.Result;
import kotlin.ResultKt;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000+\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000*\u0001\u0000\b\n\u0018\u00002\u00020\u0001J\"\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00072\b\u0010\b\u001a\u0004\u0018\u00010\tH\u0016J\u0010\u0010\n\u001a\u00020\u00032\u0006\u0010\u000b\u001a\u00020\fH\u0016¨\u0006\r"}, d2 = {"com/stripe/android/view/AddPaymentMethodViewModel$attachPaymentMethod$1", "Lcom/stripe/android/CustomerSession$PaymentMethodRetrievalListener;", "onError", "", "errorCode", "", "errorMessage", "", "stripeError", "Lcom/stripe/android/StripeError;", "onPaymentMethodRetrieved", "paymentMethod", "Lcom/stripe/android/model/PaymentMethod;", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: AddPaymentMethodViewModel.kt */
public final class AddPaymentMethodViewModel$attachPaymentMethod$1 implements CustomerSession.PaymentMethodRetrievalListener {
    final /* synthetic */ MutableLiveData $resultData;
    final /* synthetic */ AddPaymentMethodViewModel this$0;

    AddPaymentMethodViewModel$attachPaymentMethod$1(AddPaymentMethodViewModel addPaymentMethodViewModel, MutableLiveData mutableLiveData) {
        this.this$0 = addPaymentMethodViewModel;
        this.$resultData = mutableLiveData;
    }

    public void onPaymentMethodRetrieved(PaymentMethod paymentMethod) {
        Intrinsics.checkParameterIsNotNull(paymentMethod, "paymentMethod");
        MutableLiveData mutableLiveData = this.$resultData;
        Result.Companion companion = Result.Companion;
        mutableLiveData.setValue(Result.m3boximpl(Result.m4constructorimpl(paymentMethod)));
    }

    public void onError(int i, String str, StripeError stripeError) {
        Intrinsics.checkParameterIsNotNull(str, "errorMessage");
        MutableLiveData mutableLiveData = this.$resultData;
        Result.Companion companion = Result.Companion;
        mutableLiveData.setValue(Result.m3boximpl(Result.m4constructorimpl(ResultKt.createFailure(new RuntimeException(this.this$0.errorMessageTranslator.translate(i, str, stripeError))))));
    }
}
