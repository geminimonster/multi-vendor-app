package com.stripe.android.view;

import android.content.Context;
import android.os.Build;
import android.text.InputFilter;
import android.util.AttributeSet;
import androidx.appcompat.R;
import com.stripe.android.CardUtils;
import com.stripe.android.StripeTextUtils;
import com.stripe.android.model.CardBrand;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000F\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0010\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0002\b\u0012\u0018\u00002\u00020\u0001B%\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\b\b\u0002\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ\b\u0010.\u001a\u00020\u0010H\u0002J\u0015\u0010/\u001a\u00020\u00102\u0006\u00100\u001a\u00020\nH\u0000¢\u0006\u0002\b1J\r\u00102\u001a\u00020\u0010H\u0000¢\u0006\u0002\b3J%\u00104\u001a\u00020\u00072\u0006\u00105\u001a\u00020\u00072\u0006\u00106\u001a\u00020\u00072\u0006\u00107\u001a\u00020\u0007H\u0000¢\u0006\u0002\b8R\u0016\u0010\t\u001a\u0004\u0018\u00010\n8TX\u0004¢\u0006\u0006\u001a\u0004\b\u000b\u0010\fR<\u0010\u0011\u001a\u000e\u0012\u0004\u0012\u00020\u000f\u0012\u0004\u0012\u00020\u00100\u000e2\u0012\u0010\r\u001a\u000e\u0012\u0004\u0012\u00020\u000f\u0012\u0004\u0012\u00020\u00100\u000e@@X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0012\u0010\u0013\"\u0004\b\u0014\u0010\u0015R,\u0010\u0017\u001a\u00020\u000f2\u0006\u0010\u0016\u001a\u00020\u000f8\u0006@@X\u000e¢\u0006\u0014\n\u0000\u0012\u0004\b\u0018\u0010\u0019\u001a\u0004\b\u001a\u0010\u001b\"\u0004\b\u001c\u0010\u001dR\u0013\u0010\u001e\u001a\u0004\u0018\u00010\n8F¢\u0006\u0006\u001a\u0004\b\u001f\u0010\fR \u0010 \u001a\b\u0012\u0004\u0012\u00020\u00100!X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\"\u0010#\"\u0004\b$\u0010%R\u000e\u0010&\u001a\u00020'X\u000e¢\u0006\u0002\n\u0000R\u001e\u0010)\u001a\u00020'2\u0006\u0010(\u001a\u00020'@BX\u000e¢\u0006\b\n\u0000\u001a\u0004\b)\u0010*R\u0011\u0010+\u001a\u00020\u00078F¢\u0006\u0006\u001a\u0004\b,\u0010-¨\u00069"}, d2 = {"Lcom/stripe/android/view/CardNumberEditText;", "Lcom/stripe/android/view/StripeEditText;", "context", "Landroid/content/Context;", "attrs", "Landroid/util/AttributeSet;", "defStyleAttr", "", "(Landroid/content/Context;Landroid/util/AttributeSet;I)V", "accessibilityText", "", "getAccessibilityText", "()Ljava/lang/String;", "callback", "Lkotlin/Function1;", "Lcom/stripe/android/model/CardBrand;", "", "brandChangeCallback", "getBrandChangeCallback$stripe_release", "()Lkotlin/jvm/functions/Function1;", "setBrandChangeCallback$stripe_release", "(Lkotlin/jvm/functions/Function1;)V", "value", "cardBrand", "cardBrand$annotations", "()V", "getCardBrand", "()Lcom/stripe/android/model/CardBrand;", "setCardBrand$stripe_release", "(Lcom/stripe/android/model/CardBrand;)V", "cardNumber", "getCardNumber", "completionCallback", "Lkotlin/Function0;", "getCompletionCallback$stripe_release", "()Lkotlin/jvm/functions/Function0;", "setCompletionCallback$stripe_release", "(Lkotlin/jvm/functions/Function0;)V", "ignoreChanges", "", "<set-?>", "isCardNumberValid", "()Z", "lengthMax", "getLengthMax", "()I", "listenForTextChanges", "updateCardBrandFromNumber", "partialNumber", "updateCardBrandFromNumber$stripe_release", "updateLengthFilter", "updateLengthFilter$stripe_release", "updateSelectionIndex", "newLength", "editActionStart", "editActionAddition", "updateSelectionIndex$stripe_release", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: CardNumberEditText.kt */
public final class CardNumberEditText extends StripeEditText {
    private /* synthetic */ Function1<? super CardBrand, Unit> brandChangeCallback;
    private CardBrand cardBrand;
    private /* synthetic */ Function0<Unit> completionCallback;
    /* access modifiers changed from: private */
    public boolean ignoreChanges;
    /* access modifiers changed from: private */
    public boolean isCardNumberValid;

    public CardNumberEditText(Context context) {
        this(context, (AttributeSet) null, 0, 6, (DefaultConstructorMarker) null);
    }

    public CardNumberEditText(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0, 4, (DefaultConstructorMarker) null);
    }

    public static /* synthetic */ void cardBrand$annotations() {
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ CardNumberEditText(Context context, AttributeSet attributeSet, int i, int i2, DefaultConstructorMarker defaultConstructorMarker) {
        this(context, (i2 & 2) != 0 ? null : attributeSet, (i2 & 4) != 0 ? R.attr.editTextStyle : i);
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CardNumberEditText(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        Intrinsics.checkParameterIsNotNull(context, "context");
        this.cardBrand = CardBrand.Unknown;
        this.brandChangeCallback = CardNumberEditText$brandChangeCallback$1.INSTANCE;
        this.completionCallback = CardNumberEditText$completionCallback$1.INSTANCE;
        setErrorMessage(getResources().getString(com.stripe.android.R.string.invalid_card_number));
        listenForTextChanges();
        if (Build.VERSION.SDK_INT >= 26) {
            setAutofillHints(new String[]{"creditCardNumber"});
        }
    }

    public final CardBrand getCardBrand() {
        return this.cardBrand;
    }

    public final void setCardBrand$stripe_release(CardBrand cardBrand2) {
        Intrinsics.checkParameterIsNotNull(cardBrand2, "value");
        CardBrand cardBrand3 = this.cardBrand;
        this.cardBrand = cardBrand2;
        if (cardBrand2 != cardBrand3) {
            this.brandChangeCallback.invoke(cardBrand2);
            updateLengthFilter$stripe_release();
        }
    }

    public final Function1<CardBrand, Unit> getBrandChangeCallback$stripe_release() {
        return this.brandChangeCallback;
    }

    public final void setBrandChangeCallback$stripe_release(Function1<? super CardBrand, Unit> function1) {
        Intrinsics.checkParameterIsNotNull(function1, "callback");
        this.brandChangeCallback = function1;
        function1.invoke(this.cardBrand);
    }

    public final Function0<Unit> getCompletionCallback$stripe_release() {
        return this.completionCallback;
    }

    public final void setCompletionCallback$stripe_release(Function0<Unit> function0) {
        Intrinsics.checkParameterIsNotNull(function0, "<set-?>");
        this.completionCallback = function0;
    }

    public final int getLengthMax() {
        return this.cardBrand.getMaxLengthWithSpacesForCardNumber(getFieldText$stripe_release());
    }

    public final boolean isCardNumberValid() {
        return this.isCardNumberValid;
    }

    public final String getCardNumber() {
        if (this.isCardNumberValid) {
            return StripeTextUtils.removeSpacesAndHyphens(getFieldText$stripe_release());
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public String getAccessibilityText() {
        return getResources().getString(com.stripe.android.R.string.acc_label_card_number_node, new Object[]{getText()});
    }

    public final /* synthetic */ void updateLengthFilter$stripe_release() {
        setFilters(new InputFilter[]{new InputFilter.LengthFilter(getLengthMax())});
    }

    public final /* synthetic */ int updateSelectionIndex$stripe_release(int i, int i2, int i3) {
        int i4 = 0;
        boolean z = false;
        for (Number intValue : this.cardBrand.getSpacePositionsForCardNumber(getFieldText$stripe_release())) {
            int intValue2 = intValue.intValue();
            if (i2 <= intValue2 && i2 + i3 > intValue2) {
                i4++;
            }
            if (i3 == 0 && i2 == intValue2 + 1) {
                z = true;
            }
        }
        int i5 = i2 + i3 + i4;
        if (z && i5 > 0) {
            i5--;
        }
        return i5 <= i ? i5 : i;
    }

    private final void listenForTextChanges() {
        addTextChangedListener(new CardNumberEditText$listenForTextChanges$1(this));
    }

    public final /* synthetic */ void updateCardBrandFromNumber$stripe_release(String str) {
        Intrinsics.checkParameterIsNotNull(str, "partialNumber");
        setCardBrand$stripe_release(CardUtils.getPossibleCardBrand(str));
    }
}
