package com.stripe.android.view;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Build;
import android.util.AttributeSet;
import android.webkit.URLUtil;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import com.facebook.share.internal.ShareConstants;
import com.stripe.android.Logger;
import java.util.Set;
import kotlin.Metadata;
import kotlin.Result;
import kotlin.ResultKt;
import kotlin.Unit;
import kotlin.collections.SetsKt;
import kotlin.jvm.internal.Intrinsics;
import kotlin.text.StringsKt;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000F\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0006\b\u0000\u0018\u0000 \u001b2\u00020\u0001:\u0002\u001b\u001cB%\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\b\b\u0002\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ\b\u0010\u000b\u001a\u00020\fH\u0002J\b\u0010\r\u001a\u00020\fH\u0003J\b\u0010\u000e\u001a\u00020\fH\u0016J2\u0010\u000f\u001a\u00020\f2\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u00172\n\b\u0002\u0010\u0018\u001a\u0004\u0018\u00010\u0017J\r\u0010\u0019\u001a\u00020\fH\u0000¢\u0006\u0002\b\u001aR\u0010\u0010\t\u001a\u0004\u0018\u00010\nX\u000e¢\u0006\u0002\n\u0000¨\u0006\u001d"}, d2 = {"Lcom/stripe/android/view/PaymentAuthWebView;", "Landroid/webkit/WebView;", "context", "Landroid/content/Context;", "attrs", "Landroid/util/AttributeSet;", "defStyleAttr", "", "(Landroid/content/Context;Landroid/util/AttributeSet;I)V", "webViewClient", "Lcom/stripe/android/view/PaymentAuthWebView$PaymentAuthWebViewClient;", "cleanup", "", "configureSettings", "destroy", "init", "activity", "Landroid/app/Activity;", "logger", "Lcom/stripe/android/Logger;", "progressBar", "Landroid/widget/ProgressBar;", "clientSecret", "", "returnUrl", "loadBlank", "loadBlank$stripe_release", "Companion", "PaymentAuthWebViewClient", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: PaymentAuthWebView.kt */
public final class PaymentAuthWebView extends WebView {
    @Deprecated
    public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
    private PaymentAuthWebViewClient webViewClient;

    public PaymentAuthWebView(Context context) {
        this(context, (AttributeSet) null, 0, 6, (DefaultConstructorMarker) null);
    }

    public PaymentAuthWebView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0, 4, (DefaultConstructorMarker) null);
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ PaymentAuthWebView(Context context, AttributeSet attributeSet, int i, int i2, DefaultConstructorMarker defaultConstructorMarker) {
        this(context, (i2 & 2) != 0 ? null : attributeSet, (i2 & 4) != 0 ? 0 : i);
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public PaymentAuthWebView(Context context, AttributeSet attributeSet, int i) {
        super(Companion.createContext(context), attributeSet, i);
        Intrinsics.checkParameterIsNotNull(context, "context");
        configureSettings();
    }

    public static /* synthetic */ void init$default(PaymentAuthWebView paymentAuthWebView, Activity activity, Logger logger, ProgressBar progressBar, String str, String str2, int i, Object obj) {
        if ((i & 16) != 0) {
            str2 = null;
        }
        paymentAuthWebView.init(activity, logger, progressBar, str, str2);
    }

    public final void init(Activity activity, Logger logger, ProgressBar progressBar, String str, String str2) {
        Intrinsics.checkParameterIsNotNull(activity, "activity");
        Intrinsics.checkParameterIsNotNull(logger, "logger");
        Intrinsics.checkParameterIsNotNull(progressBar, "progressBar");
        Intrinsics.checkParameterIsNotNull(str, "clientSecret");
        PackageManager packageManager = activity.getPackageManager();
        Intrinsics.checkExpressionValueIsNotNull(packageManager, "activity.packageManager");
        PaymentAuthWebViewClient paymentAuthWebViewClient = new PaymentAuthWebViewClient(activity, packageManager, logger, progressBar, str, str2);
        setWebViewClient(paymentAuthWebViewClient);
        this.webViewClient = paymentAuthWebViewClient;
        setWebChromeClient(new PaymentAuthWebView$init$1(logger, activity));
    }

    public void destroy() {
        cleanup();
        super.destroy();
    }

    private final void cleanup() {
        clearHistory();
        loadBlank$stripe_release();
        onPause();
        removeAllViews();
        destroyDrawingCache();
    }

    private final void configureSettings() {
        WebSettings settings = getSettings();
        Intrinsics.checkExpressionValueIsNotNull(settings, "settings");
        settings.setJavaScriptEnabled(true);
        WebSettings settings2 = getSettings();
        Intrinsics.checkExpressionValueIsNotNull(settings2, "settings");
        settings2.setAllowContentAccess(false);
        WebSettings settings3 = getSettings();
        Intrinsics.checkExpressionValueIsNotNull(settings3, "settings");
        settings3.setDomStorageEnabled(true);
    }

    public final void loadBlank$stripe_release() {
        PaymentAuthWebViewClient paymentAuthWebViewClient = this.webViewClient;
        if (paymentAuthWebViewClient != null) {
            paymentAuthWebViewClient.setHasLoadedBlank$stripe_release(true);
        }
        loadUrl(PaymentAuthWebViewClient.BLANK_PAGE);
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0004H\u0002¨\u0006\u0006"}, d2 = {"Lcom/stripe/android/view/PaymentAuthWebView$Companion;", "", "()V", "createContext", "Landroid/content/Context;", "context", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: PaymentAuthWebView.kt */
    private static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        /* access modifiers changed from: private */
        public final Context createContext(Context context) {
            if (Build.VERSION.SDK_INT < 21) {
                return context;
            }
            Context createConfigurationContext = context.createConfigurationContext(new Configuration());
            Intrinsics.checkExpressionValueIsNotNull(createConfigurationContext, "context.createConfigurat…nContext(Configuration())");
            return createConfigurationContext;
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000`\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0007\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\b\n\u0002\u0010\"\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\b\u0000\u0018\u0000 22\u00020\u0001:\u00012B7\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\b\u0010\f\u001a\u0004\u0018\u00010\u000b¢\u0006\u0002\u0010\rJ\b\u0010\u001a\u001a\u00020\u001bH\u0002J\u0010\u0010\u001c\u001a\u00020\u00132\u0006\u0010\u001d\u001a\u00020\u000bH\u0002J\u0010\u0010\u001e\u001a\u00020\u00132\u0006\u0010\u001d\u001a\u00020\u000bH\u0002J\u0010\u0010\u001f\u001a\u00020\u00132\u0006\u0010 \u001a\u00020\u0019H\u0002J\u0010\u0010!\u001a\u00020\u00132\u0006\u0010 \u001a\u00020\u0019H\u0002J\u001e\u0010\"\u001a\u00020\u00132\u0006\u0010\u001d\u001a\u00020\u000b2\f\u0010#\u001a\b\u0012\u0004\u0012\u00020\u000b0$H\u0002J\b\u0010%\u001a\u00020\u001bH\u0002J\u001a\u0010&\u001a\u00020\u001b2\u0006\u0010'\u001a\u00020(2\b\u0010\u001d\u001a\u0004\u0018\u00010\u000bH\u0016J\u0010\u0010)\u001a\u00020\u001b2\u0006\u0010*\u001a\u00020+H\u0002J\u0010\u0010,\u001a\u00020\u001b2\u0006\u0010 \u001a\u00020\u0019H\u0002J\u0018\u0010-\u001a\u00020\u00132\u0006\u0010'\u001a\u00020(2\u0006\u0010.\u001a\u00020/H\u0017J\u0018\u0010-\u001a\u00020\u00132\u0006\u0010'\u001a\u00020(2\u0006\u00100\u001a\u00020\u000bH\u0016J\u0010\u00101\u001a\u00020\u001b2\u0006\u0010 \u001a\u00020\u0019H\u0002R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0004¢\u0006\u0002\n\u0000R\"\u0010\u000f\u001a\u0004\u0018\u00010\u000b2\b\u0010\u000e\u001a\u0004\u0018\u00010\u000b@BX\u000e¢\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u0011R\u001a\u0010\u0012\u001a\u00020\u0013X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0014\u0010\u0015\"\u0004\b\u0016\u0010\u0017R\u000e\u0010\u0006\u001a\u00020\u0007X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u0018\u001a\u0004\u0018\u00010\u0019X\u0004¢\u0006\u0002\n\u0000¨\u00063"}, d2 = {"Lcom/stripe/android/view/PaymentAuthWebView$PaymentAuthWebViewClient;", "Landroid/webkit/WebViewClient;", "activity", "Landroid/app/Activity;", "packageManager", "Landroid/content/pm/PackageManager;", "logger", "Lcom/stripe/android/Logger;", "progressBar", "Landroid/widget/ProgressBar;", "clientSecret", "", "returnUrl", "(Landroid/app/Activity;Landroid/content/pm/PackageManager;Lcom/stripe/android/Logger;Landroid/widget/ProgressBar;Ljava/lang/String;Ljava/lang/String;)V", "<set-?>", "completionUrlParam", "getCompletionUrlParam", "()Ljava/lang/String;", "hasLoadedBlank", "", "getHasLoadedBlank$stripe_release", "()Z", "setHasLoadedBlank$stripe_release", "(Z)V", "userReturnUri", "Landroid/net/Uri;", "hideProgressBar", "", "isAuthenticateUrl", "url", "isCompletionUrl", "isPredefinedReturnUrl", "uri", "isReturnUrl", "isWhiteListedUrl", "whitelistedUrls", "", "onAuthCompleted", "onPageFinished", "view", "Landroid/webkit/WebView;", "openIntent", "intent", "Landroid/content/Intent;", "openIntentScheme", "shouldOverrideUrlLoading", "request", "Landroid/webkit/WebResourceRequest;", "urlString", "updateCompletionUrl", "Companion", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: PaymentAuthWebView.kt */
    public static final class PaymentAuthWebViewClient extends WebViewClient {
        private static final Set<String> AUTHENTICATE_URLS = SetsKt.setOf("https://hooks.stripe.com/three_d_secure/authenticate");
        public static final String BLANK_PAGE = "about:blank";
        private static final Set<String> COMPLETION_URLS = SetsKt.setOf("https://hooks.stripe.com/redirect/complete/src_", "https://hooks.stripe.com/3d_secure/complete/tdsrc_");
        public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
        public static final String PARAM_PAYMENT_CLIENT_SECRET = "payment_intent_client_secret";
        private static final String PARAM_RETURN_URL = "return_url";
        public static final String PARAM_SETUP_CLIENT_SECRET = "setup_intent_client_secret";
        private final Activity activity;
        private final String clientSecret;
        private String completionUrlParam;
        private boolean hasLoadedBlank;
        private final Logger logger;
        private final PackageManager packageManager;
        private final ProgressBar progressBar;
        private final Uri userReturnUri;

        public PaymentAuthWebViewClient(Activity activity2, PackageManager packageManager2, Logger logger2, ProgressBar progressBar2, String str, String str2) {
            Intrinsics.checkParameterIsNotNull(activity2, "activity");
            Intrinsics.checkParameterIsNotNull(packageManager2, "packageManager");
            Intrinsics.checkParameterIsNotNull(logger2, "logger");
            Intrinsics.checkParameterIsNotNull(progressBar2, "progressBar");
            Intrinsics.checkParameterIsNotNull(str, "clientSecret");
            this.activity = activity2;
            this.packageManager = packageManager2;
            this.logger = logger2;
            this.progressBar = progressBar2;
            this.clientSecret = str;
            this.userReturnUri = str2 != null ? Uri.parse(str2) : null;
        }

        public final String getCompletionUrlParam() {
            return this.completionUrlParam;
        }

        public final boolean getHasLoadedBlank$stripe_release() {
            return this.hasLoadedBlank;
        }

        public final void setHasLoadedBlank$stripe_release(boolean z) {
            this.hasLoadedBlank = z;
        }

        public void onPageFinished(WebView webView, String str) {
            Intrinsics.checkParameterIsNotNull(webView, "view");
            Logger logger2 = this.logger;
            logger2.debug("PaymentAuthWebViewClient#onPageFinished() - " + str);
            super.onPageFinished(webView, str);
            if (!this.hasLoadedBlank) {
                hideProgressBar();
            }
            if (str != null && isCompletionUrl(str)) {
                onAuthCompleted();
            }
        }

        private final void hideProgressBar() {
            this.logger.debug("PaymentAuthWebViewClient#hideProgressBar()");
            this.progressBar.setVisibility(8);
        }

        private final boolean isAuthenticateUrl(String str) {
            return isWhiteListedUrl(str, AUTHENTICATE_URLS);
        }

        private final boolean isCompletionUrl(String str) {
            return isWhiteListedUrl(str, COMPLETION_URLS);
        }

        private final boolean isWhiteListedUrl(String str, Set<String> set) {
            for (String startsWith$default : set) {
                if (StringsKt.startsWith$default(str, startsWith$default, false, 2, (Object) null)) {
                    return true;
                }
            }
            return false;
        }

        public boolean shouldOverrideUrlLoading(WebView webView, String str) {
            Intrinsics.checkParameterIsNotNull(webView, "view");
            Intrinsics.checkParameterIsNotNull(str, "urlString");
            Logger logger2 = this.logger;
            logger2.debug("PaymentAuthWebViewClient#shouldOverrideUrlLoading() - " + str);
            Uri parse = Uri.parse(str);
            Intrinsics.checkExpressionValueIsNotNull(parse, ShareConstants.MEDIA_URI);
            updateCompletionUrl(parse);
            if (isReturnUrl(parse)) {
                this.logger.debug("PaymentAuthWebViewClient#shouldOverrideUrlLoading() - handle return URL");
                onAuthCompleted();
                return true;
            } else if (StringsKt.equals("intent", parse.getScheme(), true)) {
                openIntentScheme(parse);
                return true;
            } else if (URLUtil.isNetworkUrl(parse.toString())) {
                return super.shouldOverrideUrlLoading(webView, str);
            } else {
                openIntent(new Intent("android.intent.action.VIEW", parse));
                return true;
            }
        }

        private final void openIntentScheme(Uri uri) {
            Object obj;
            this.logger.debug("PaymentAuthWebViewClient#openIntentScheme()");
            try {
                Result.Companion companion = Result.Companion;
                Intent parseUri = Intent.parseUri(uri.toString(), 1);
                Intrinsics.checkExpressionValueIsNotNull(parseUri, "Intent.parseUri(uri.toSt…Intent.URI_INTENT_SCHEME)");
                openIntent(parseUri);
                obj = Result.m4constructorimpl(Unit.INSTANCE);
            } catch (Throwable th) {
                Result.Companion companion2 = Result.Companion;
                obj = Result.m4constructorimpl(ResultKt.createFailure(th));
            }
            if (Result.m7exceptionOrNullimpl(obj) != null) {
                Result.Companion companion3 = Result.Companion;
                onAuthCompleted();
                Result.m4constructorimpl(Unit.INSTANCE);
            }
        }

        private final void openIntent(Intent intent) {
            this.logger.debug("PaymentAuthWebViewClient#openIntent()");
            if (intent.resolveActivity(this.packageManager) != null) {
                this.activity.startActivity(intent);
            } else if (!Intrinsics.areEqual((Object) intent.getScheme(), (Object) "alipays")) {
                onAuthCompleted();
            }
        }

        private final void updateCompletionUrl(Uri uri) {
            this.logger.debug("PaymentAuthWebViewClient#updateCompletionUrl()");
            String uri2 = uri.toString();
            Intrinsics.checkExpressionValueIsNotNull(uri2, "uri.toString()");
            String queryParameter = isAuthenticateUrl(uri2) ? uri.getQueryParameter("return_url") : null;
            CharSequence charSequence = queryParameter;
            if (!(charSequence == null || StringsKt.isBlank(charSequence))) {
                this.completionUrlParam = queryParameter;
            }
        }

        public boolean shouldOverrideUrlLoading(WebView webView, WebResourceRequest webResourceRequest) {
            Intrinsics.checkParameterIsNotNull(webView, "view");
            Intrinsics.checkParameterIsNotNull(webResourceRequest, ShareConstants.WEB_DIALOG_RESULT_PARAM_REQUEST_ID);
            this.logger.debug("PaymentAuthWebViewClient#shouldOverrideUrlLoading(WebResourceRequest)");
            String uri = webResourceRequest.getUrl().toString();
            Intrinsics.checkExpressionValueIsNotNull(uri, "request.url.toString()");
            return shouldOverrideUrlLoading(webView, uri);
        }

        private final boolean isReturnUrl(Uri uri) {
            String str;
            this.logger.debug("PaymentAuthWebViewClient#isReturnUrl()");
            if (isPredefinedReturnUrl(uri)) {
                return true;
            }
            Uri uri2 = this.userReturnUri;
            if (uri2 != null) {
                if (uri2.getScheme() == null || !Intrinsics.areEqual((Object) this.userReturnUri.getScheme(), (Object) uri.getScheme()) || this.userReturnUri.getHost() == null || !Intrinsics.areEqual((Object) this.userReturnUri.getHost(), (Object) uri.getHost())) {
                    return false;
                }
                return true;
            } else if (uri.isOpaque()) {
                return false;
            } else {
                Set<String> queryParameterNames = uri.getQueryParameterNames();
                if (queryParameterNames.contains(PARAM_PAYMENT_CLIENT_SECRET)) {
                    str = uri.getQueryParameter(PARAM_PAYMENT_CLIENT_SECRET);
                } else {
                    str = queryParameterNames.contains(PARAM_SETUP_CLIENT_SECRET) ? uri.getQueryParameter(PARAM_SETUP_CLIENT_SECRET) : null;
                }
                return Intrinsics.areEqual((Object) this.clientSecret, (Object) str);
            }
        }

        private final boolean isPredefinedReturnUrl(Uri uri) {
            return Intrinsics.areEqual((Object) "stripejs://use_stripe_sdk/return_url", (Object) uri.toString());
        }

        private final void onAuthCompleted() {
            this.logger.debug("PaymentAuthWebViewClient#onAuthCompleted()");
            this.activity.finish();
        }

        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\"\n\u0002\u0010\u000e\n\u0002\b\u0006\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0014\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0005XT¢\u0006\u0002\n\u0000R\u0014\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0005XT¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0005XT¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u0005XT¢\u0006\u0002\n\u0000¨\u0006\u000b"}, d2 = {"Lcom/stripe/android/view/PaymentAuthWebView$PaymentAuthWebViewClient$Companion;", "", "()V", "AUTHENTICATE_URLS", "", "", "BLANK_PAGE", "COMPLETION_URLS", "PARAM_PAYMENT_CLIENT_SECRET", "PARAM_RETURN_URL", "PARAM_SETUP_CLIENT_SECRET", "stripe_release"}, k = 1, mv = {1, 1, 16})
        /* compiled from: PaymentAuthWebView.kt */
        public static final class Companion {
            private Companion() {
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }
        }
    }
}
