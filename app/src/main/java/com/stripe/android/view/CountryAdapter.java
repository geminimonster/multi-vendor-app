package com.stripe.android.view;

import android.app.Activity;
import android.content.Context;
import android.os.IBinder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;
import com.stripe.android.R;
import com.stripe.android.databinding.CountryTextViewBinding;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.jvm.internal.Intrinsics;
import kotlin.text.StringsKt;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000`\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\"\n\u0002\u0010\u000e\n\u0002\b\u0003\b\u0000\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001(B\u001b\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\f\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00020\u0006¢\u0006\u0002\u0010\u0007J\b\u0010\u0015\u001a\u00020\u0016H\u0016J\b\u0010\u0017\u001a\u00020\u0018H\u0016J\u0010\u0010\u0019\u001a\u00020\u00022\u0006\u0010\u001a\u001a\u00020\u0016H\u0016J\u0010\u0010\u001b\u001a\u00020\u001c2\u0006\u0010\u001a\u001a\u00020\u0016H\u0016J\"\u0010\u001d\u001a\u00020\u001e2\u0006\u0010\u001a\u001a\u00020\u00162\b\u0010\u001f\u001a\u0004\u0018\u00010\u001e2\u0006\u0010 \u001a\u00020!H\u0016J\u001b\u0010\"\u001a\u00020#2\f\u0010$\u001a\b\u0012\u0004\u0012\u00020&0%H\u0000¢\u0006\u0002\b'R\u000e\u0010\b\u001a\u00020\tX\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\n\u001a\u00020\u00028@X\u0004¢\u0006\u0006\u001a\u0004\b\u000b\u0010\fR\u0016\u0010\r\u001a\n \u000f*\u0004\u0018\u00010\u000e0\u000eX\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u00020\u0006X\u000e¢\u0006\u0002\n\u0000R \u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00020\u0006X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0011\u0010\u0012\"\u0004\b\u0013\u0010\u0014¨\u0006)"}, d2 = {"Lcom/stripe/android/view/CountryAdapter;", "Landroid/widget/ArrayAdapter;", "Lcom/stripe/android/view/Country;", "context", "Landroid/content/Context;", "unfilteredCountries", "", "(Landroid/content/Context;Ljava/util/List;)V", "countryFilter", "Lcom/stripe/android/view/CountryAdapter$CountryFilter;", "firstItem", "getFirstItem$stripe_release", "()Lcom/stripe/android/view/Country;", "layoutInflater", "Landroid/view/LayoutInflater;", "kotlin.jvm.PlatformType", "suggestions", "getUnfilteredCountries$stripe_release", "()Ljava/util/List;", "setUnfilteredCountries$stripe_release", "(Ljava/util/List;)V", "getCount", "", "getFilter", "Landroid/widget/Filter;", "getItem", "i", "getItemId", "", "getView", "Landroid/view/View;", "view", "viewGroup", "Landroid/view/ViewGroup;", "updateUnfilteredCountries", "", "allowedCountryCodes", "", "", "updateUnfilteredCountries$stripe_release", "CountryFilter", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: CountryAdapter.kt */
public final class CountryAdapter extends ArrayAdapter<Country> {
    private final CountryFilter countryFilter;
    private final LayoutInflater layoutInflater;
    /* access modifiers changed from: private */
    public List<Country> suggestions;
    private List<Country> unfilteredCountries;

    public final List<Country> getUnfilteredCountries$stripe_release() {
        return this.unfilteredCountries;
    }

    public final void setUnfilteredCountries$stripe_release(List<Country> list) {
        Intrinsics.checkParameterIsNotNull(list, "<set-?>");
        this.unfilteredCountries = list;
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CountryAdapter(Context context, List<Country> list) {
        super(context, R.layout.country_text_view);
        Intrinsics.checkParameterIsNotNull(context, "context");
        Intrinsics.checkParameterIsNotNull(list, "unfilteredCountries");
        this.unfilteredCountries = list;
        this.layoutInflater = LayoutInflater.from(context);
        this.countryFilter = new CountryFilter(this.unfilteredCountries, this, (Activity) (!(context instanceof Activity) ? null : context));
        this.suggestions = this.unfilteredCountries;
    }

    public final /* synthetic */ Country getFirstItem$stripe_release() {
        return getItem(0);
    }

    public int getCount() {
        return this.suggestions.size();
    }

    public Country getItem(int i) {
        return this.suggestions.get(i);
    }

    public long getItemId(int i) {
        return (long) getItem(i).hashCode();
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        TextView textView;
        Intrinsics.checkParameterIsNotNull(viewGroup, "viewGroup");
        if (view instanceof TextView) {
            textView = (TextView) view;
        } else {
            CountryTextViewBinding inflate = CountryTextViewBinding.inflate(this.layoutInflater, viewGroup, false);
            Intrinsics.checkExpressionValueIsNotNull(inflate, "CountryTextViewBinding.i…  false\n                )");
            textView = inflate.getRoot();
            if (textView == null) {
                throw new TypeCastException("null cannot be cast to non-null type android.widget.TextView");
            }
        }
        textView.setText(getItem(i).getName());
        return textView;
    }

    public Filter getFilter() {
        return this.countryFilter;
    }

    public final boolean updateUnfilteredCountries$stripe_release(Set<String> set) {
        Intrinsics.checkParameterIsNotNull(set, "allowedCountryCodes");
        if (set.isEmpty()) {
            return false;
        }
        Collection arrayList = new ArrayList();
        Iterator it = this.unfilteredCountries.iterator();
        while (true) {
            boolean z = true;
            if (it.hasNext()) {
                Object next = it.next();
                String component1 = ((Country) next).component1();
                Iterable iterable = set;
                if (!(iterable instanceof Collection) || !((Collection) iterable).isEmpty()) {
                    Iterator it2 = iterable.iterator();
                    while (true) {
                        if (it2.hasNext()) {
                            if (StringsKt.equals((String) it2.next(), component1, true)) {
                                break;
                            }
                        } else {
                            break;
                        }
                    }
                }
                z = false;
                if (z) {
                    arrayList.add(next);
                }
            } else {
                List<Country> list = (List) arrayList;
                this.unfilteredCountries = list;
                this.countryFilter.setUnfilteredCountries$stripe_release(list);
                this.suggestions = this.unfilteredCountries;
                notifyDataSetChanged();
                return true;
            }
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000H\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\r\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u0002\u0018\u00002\u00020\u0001B%\u0012\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\b\u0010\u0007\u001a\u0004\u0018\u00010\b¢\u0006\u0002\u0010\tJ\u0018\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u00040\u00032\b\u0010\u0011\u001a\u0004\u0018\u00010\u0012H\u0002J\u0018\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u00040\u00032\b\u0010\u0011\u001a\u0004\u0018\u00010\u0012H\u0002J\u0010\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0007\u001a\u00020\bH\u0002J \u0010\u0016\u001a\u00020\u00172\f\u0010\u0018\u001a\b\u0012\u0004\u0012\u00020\u00040\u00032\b\u0010\u0011\u001a\u0004\u0018\u00010\u0012H\u0002J\u0012\u0010\u0019\u001a\u00020\u001a2\b\u0010\u0011\u001a\u0004\u0018\u00010\u0012H\u0014J\u001c\u0010\u001b\u001a\u00020\u00152\b\u0010\u0011\u001a\u0004\u0018\u00010\u00122\b\u0010\u001c\u001a\u0004\u0018\u00010\u001aH\u0014R\u0016\u0010\n\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\b0\u000bX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0004¢\u0006\u0002\n\u0000R \u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\f\u0010\r\"\u0004\b\u000e\u0010\u000f¨\u0006\u001d"}, d2 = {"Lcom/stripe/android/view/CountryAdapter$CountryFilter;", "Landroid/widget/Filter;", "unfilteredCountries", "", "Lcom/stripe/android/view/Country;", "adapter", "Lcom/stripe/android/view/CountryAdapter;", "activity", "Landroid/app/Activity;", "(Ljava/util/List;Lcom/stripe/android/view/CountryAdapter;Landroid/app/Activity;)V", "activityRef", "Ljava/lang/ref/WeakReference;", "getUnfilteredCountries$stripe_release", "()Ljava/util/List;", "setUnfilteredCountries$stripe_release", "(Ljava/util/List;)V", "filteredSuggestedCountries", "constraint", "", "getSuggestedCountries", "hideKeyboard", "", "isMatch", "", "countries", "performFiltering", "Landroid/widget/Filter$FilterResults;", "publishResults", "filterResults", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: CountryAdapter.kt */
    private static final class CountryFilter extends Filter {
        private final WeakReference<Activity> activityRef;
        private final CountryAdapter adapter;
        private List<Country> unfilteredCountries;

        public final List<Country> getUnfilteredCountries$stripe_release() {
            return this.unfilteredCountries;
        }

        public final void setUnfilteredCountries$stripe_release(List<Country> list) {
            Intrinsics.checkParameterIsNotNull(list, "<set-?>");
            this.unfilteredCountries = list;
        }

        public CountryFilter(List<Country> list, CountryAdapter countryAdapter, Activity activity) {
            Intrinsics.checkParameterIsNotNull(list, "unfilteredCountries");
            Intrinsics.checkParameterIsNotNull(countryAdapter, "adapter");
            this.unfilteredCountries = list;
            this.adapter = countryAdapter;
            this.activityRef = new WeakReference<>(activity);
        }

        /* access modifiers changed from: protected */
        public Filter.FilterResults performFiltering(CharSequence charSequence) {
            List<Country> list;
            Filter.FilterResults filterResults = new Filter.FilterResults();
            if (charSequence == null || (list = filteredSuggestedCountries(charSequence)) == null) {
                list = this.unfilteredCountries;
            }
            filterResults.values = list;
            return filterResults;
        }

        /* access modifiers changed from: protected */
        public void publishResults(CharSequence charSequence, Filter.FilterResults filterResults) {
            Object obj = filterResults != null ? filterResults.values : null;
            if (obj != null) {
                List list = (List) obj;
                Activity activity = (Activity) this.activityRef.get();
                if (activity != null) {
                    Iterable iterable = list;
                    boolean z = false;
                    if (!(iterable instanceof Collection) || !((Collection) iterable).isEmpty()) {
                        Iterator it = iterable.iterator();
                        while (true) {
                            if (it.hasNext()) {
                                if (Intrinsics.areEqual((Object) ((Country) it.next()).getName(), (Object) charSequence)) {
                                    z = true;
                                    break;
                                }
                            } else {
                                break;
                            }
                        }
                    }
                    if (z) {
                        Intrinsics.checkExpressionValueIsNotNull(activity, "activity");
                        hideKeyboard(activity);
                    }
                }
                this.adapter.suggestions = list;
                this.adapter.notifyDataSetChanged();
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type kotlin.collections.List<com.stripe.android.view.Country>");
        }

        private final List<Country> filteredSuggestedCountries(CharSequence charSequence) {
            List<Country> suggestedCountries = getSuggestedCountries(charSequence);
            return (suggestedCountries.isEmpty() || isMatch(suggestedCountries, charSequence)) ? this.unfilteredCountries : suggestedCountries;
        }

        private final List<Country> getSuggestedCountries(CharSequence charSequence) {
            Collection arrayList = new ArrayList();
            for (Object next : this.unfilteredCountries) {
                String name = ((Country) next).getName();
                Locale locale = Locale.ROOT;
                Intrinsics.checkExpressionValueIsNotNull(locale, "Locale.ROOT");
                if (name != null) {
                    String lowerCase = name.toLowerCase(locale);
                    Intrinsics.checkExpressionValueIsNotNull(lowerCase, "(this as java.lang.String).toLowerCase(locale)");
                    String valueOf = String.valueOf(charSequence);
                    Locale locale2 = Locale.ROOT;
                    Intrinsics.checkExpressionValueIsNotNull(locale2, "Locale.ROOT");
                    if (valueOf != null) {
                        String lowerCase2 = valueOf.toLowerCase(locale2);
                        Intrinsics.checkExpressionValueIsNotNull(lowerCase2, "(this as java.lang.String).toLowerCase(locale)");
                        if (StringsKt.startsWith$default(lowerCase, lowerCase2, false, 2, (Object) null)) {
                            arrayList.add(next);
                        }
                    } else {
                        throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
                    }
                } else {
                    throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
                }
            }
            return (List) arrayList;
        }

        private final boolean isMatch(List<Country> list, CharSequence charSequence) {
            return list.size() == 1 && Intrinsics.areEqual((Object) list.get(0).getName(), (Object) String.valueOf(charSequence));
        }

        private final void hideKeyboard(Activity activity) {
            Object systemService = activity.getSystemService("input_method");
            IBinder iBinder = null;
            if (!(systemService instanceof InputMethodManager)) {
                systemService = null;
            }
            InputMethodManager inputMethodManager = (InputMethodManager) systemService;
            if (inputMethodManager != null && inputMethodManager.isAcceptingText()) {
                View currentFocus = activity.getCurrentFocus();
                if (currentFocus != null) {
                    iBinder = currentFocus.getWindowToken();
                }
                inputMethodManager.hideSoftInputFromWindow(iBinder, 0);
            }
        }
    }
}
