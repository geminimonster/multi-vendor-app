package com.stripe.android.view;

import android.app.Application;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelStoreOwner;
import com.stripe.android.view.PaymentMethodsViewModel;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n¢\u0006\u0002\b\u0002"}, d2 = {"<anonymous>", "Lcom/stripe/android/view/PaymentMethodsViewModel;", "invoke"}, k = 3, mv = {1, 1, 16})
/* compiled from: PaymentMethodsActivity.kt */
final class PaymentMethodsActivity$viewModel$2 extends Lambda implements Function0<PaymentMethodsViewModel> {
    final /* synthetic */ PaymentMethodsActivity this$0;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    PaymentMethodsActivity$viewModel$2(PaymentMethodsActivity paymentMethodsActivity) {
        super(0);
        this.this$0 = paymentMethodsActivity;
    }

    public final PaymentMethodsViewModel invoke() {
        PaymentMethodsActivity paymentMethodsActivity = this.this$0;
        Application application = paymentMethodsActivity.getApplication();
        Intrinsics.checkExpressionValueIsNotNull(application, "application");
        return (PaymentMethodsViewModel) new ViewModelProvider((ViewModelStoreOwner) paymentMethodsActivity, (ViewModelProvider.Factory) new PaymentMethodsViewModel.Factory(application, this.this$0.getCustomerSession(), this.this$0.getArgs().getInitialPaymentMethodId$stripe_release(), this.this$0.getStartedFromPaymentSession())).get(PaymentMethodsViewModel.class);
    }
}
