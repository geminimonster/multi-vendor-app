package com.stripe.android.view;

import android.view.KeyEvent;
import android.view.View;
import com.stripe.android.view.StripeEditText;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001c\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u00032\u0006\u0010\u0005\u001a\u00020\u00062\u000e\u0010\u0007\u001a\n \u0004*\u0004\u0018\u00010\b0\bH\n¢\u0006\u0002\b\t"}, d2 = {"<anonymous>", "", "<anonymous parameter 0>", "Landroid/view/View;", "kotlin.jvm.PlatformType", "keyCode", "", "event", "Landroid/view/KeyEvent;", "onKey"}, k = 3, mv = {1, 1, 16})
/* compiled from: StripeEditText.kt */
final class StripeEditText$listenForDeleteEmpty$1 implements View.OnKeyListener {
    final /* synthetic */ StripeEditText this$0;

    StripeEditText$listenForDeleteEmpty$1(StripeEditText stripeEditText) {
        this.this$0 = stripeEditText;
    }

    public final boolean onKey(View view, int i, KeyEvent keyEvent) {
        StripeEditText.DeleteEmptyListener access$getDeleteEmptyListener$p;
        StripeEditText stripeEditText = this.this$0;
        Intrinsics.checkExpressionValueIsNotNull(keyEvent, "event");
        stripeEditText.setLastKeyDelete(stripeEditText.isDeleteKey(i, keyEvent));
        if (!this.this$0.isLastKeyDelete() || this.this$0.length() != 0 || (access$getDeleteEmptyListener$p = this.this$0.deleteEmptyListener) == null) {
            return false;
        }
        access$getDeleteEmptyListener$p.onDeleteEmpty();
        return false;
    }
}
