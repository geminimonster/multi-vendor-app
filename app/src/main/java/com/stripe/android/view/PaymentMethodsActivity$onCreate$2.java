package com.stripe.android.view;

import android.view.View;
import androidx.lifecycle.Observer;
import com.google.android.material.snackbar.Snackbar;
import kotlin.Metadata;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\u0010\u0000\u001a\u00020\u00012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003H\n¢\u0006\u0002\b\u0004"}, d2 = {"<anonymous>", "", "snackbarText", "", "onChanged"}, k = 3, mv = {1, 1, 16})
/* compiled from: PaymentMethodsActivity.kt */
final class PaymentMethodsActivity$onCreate$2<T> implements Observer<String> {
    final /* synthetic */ PaymentMethodsActivity this$0;

    PaymentMethodsActivity$onCreate$2(PaymentMethodsActivity paymentMethodsActivity) {
        this.this$0 = paymentMethodsActivity;
    }

    public final void onChanged(String str) {
        if (str != null) {
            Snackbar.make((View) this.this$0.getViewBinding$stripe_release().coordinator, (CharSequence) str, -1).show();
        }
    }
}
