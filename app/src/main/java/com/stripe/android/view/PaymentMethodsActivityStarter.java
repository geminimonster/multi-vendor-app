package com.stripe.android.view;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import androidx.fragment.app.Fragment;
import com.stripe.android.ObjectBuilder;
import com.stripe.android.PaymentConfiguration;
import com.stripe.android.model.PaymentMethod;
import com.stripe.android.view.ActivityStarter;
import java.util.ArrayList;
import java.util.List;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u0000 \u000b2\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001:\u0003\n\u000b\fB\u000f\b\u0016\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006B\u000f\b\u0016\u0012\u0006\u0010\u0007\u001a\u00020\b¢\u0006\u0002\u0010\t¨\u0006\r"}, d2 = {"Lcom/stripe/android/view/PaymentMethodsActivityStarter;", "Lcom/stripe/android/view/ActivityStarter;", "Lcom/stripe/android/view/PaymentMethodsActivity;", "Lcom/stripe/android/view/PaymentMethodsActivityStarter$Args;", "activity", "Landroid/app/Activity;", "(Landroid/app/Activity;)V", "fragment", "Landroidx/fragment/app/Fragment;", "(Landroidx/fragment/app/Fragment;)V", "Args", "Companion", "Result", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: PaymentMethodsActivityStarter.kt */
public final class PaymentMethodsActivityStarter extends ActivityStarter<PaymentMethodsActivity, Args> {
    public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
    public static final int REQUEST_CODE = 6000;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public PaymentMethodsActivityStarter(Activity activity) {
        super(activity, PaymentMethodsActivity.class, Args.Companion.getDEFAULT$stripe_release(), (int) REQUEST_CODE);
        Intrinsics.checkParameterIsNotNull(activity, "activity");
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public PaymentMethodsActivityStarter(Fragment fragment) {
        super(fragment, PaymentMethodsActivity.class, Args.Companion.getDEFAULT$stripe_release(), (int) REQUEST_CODE);
        Intrinsics.checkParameterIsNotNull(fragment, "fragment");
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000L\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b.\n\u0002\u0010\u0000\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\b\b\u0018\u0000 G2\u00020\u0001:\u0002FGBm\b\u0000\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\b\b\u0001\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\f\u0010\b\u001a\b\u0012\u0004\u0012\u00020\n0\t\u0012\b\u0010\u000b\u001a\u0004\u0018\u00010\f\u0012\n\b\u0002\u0010\r\u001a\u0004\u0018\u00010\u0005\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u0012\b\b\u0002\u0010\u0010\u001a\u00020\u0007\u0012\b\b\u0002\u0010\u0011\u001a\u00020\u0007\u0012\b\b\u0002\u0010\u0012\u001a\u00020\u0007¢\u0006\u0002\u0010\u0013J\u0010\u0010&\u001a\u0004\u0018\u00010\u0003HÀ\u0003¢\u0006\u0002\b'J\u000e\u0010(\u001a\u00020\u0007HÀ\u0003¢\u0006\u0002\b)J\t\u0010*\u001a\u00020\u0005HÆ\u0003J\u000e\u0010+\u001a\u00020\u0007HÀ\u0003¢\u0006\u0002\b,J\u0014\u0010-\u001a\b\u0012\u0004\u0012\u00020\n0\tHÀ\u0003¢\u0006\u0002\b.J\u0010\u0010/\u001a\u0004\u0018\u00010\fHÀ\u0003¢\u0006\u0002\b0J\u0012\u00101\u001a\u0004\u0018\u00010\u0005HÀ\u0003¢\u0006\u0004\b2\u0010$J\u000e\u00103\u001a\u00020\u000fHÀ\u0003¢\u0006\u0002\b4J\u000e\u00105\u001a\u00020\u0007HÀ\u0003¢\u0006\u0002\b6J\u000e\u00107\u001a\u00020\u0007HÀ\u0003¢\u0006\u0002\b8J~\u00109\u001a\u00020\u00002\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u00032\b\b\u0003\u0010\u0004\u001a\u00020\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u00072\u000e\b\u0002\u0010\b\u001a\b\u0012\u0004\u0012\u00020\n0\t2\n\b\u0002\u0010\u000b\u001a\u0004\u0018\u00010\f2\n\b\u0002\u0010\r\u001a\u0004\u0018\u00010\u00052\b\b\u0002\u0010\u000e\u001a\u00020\u000f2\b\b\u0002\u0010\u0010\u001a\u00020\u00072\b\b\u0002\u0010\u0011\u001a\u00020\u00072\b\b\u0002\u0010\u0012\u001a\u00020\u0007HÆ\u0001¢\u0006\u0002\u0010:J\t\u0010;\u001a\u00020\u0005HÖ\u0001J\u0013\u0010<\u001a\u00020\u00072\b\u0010=\u001a\u0004\u0018\u00010>HÖ\u0003J\t\u0010?\u001a\u00020\u0005HÖ\u0001J\t\u0010@\u001a\u00020\u0003HÖ\u0001J\u0019\u0010A\u001a\u00020B2\u0006\u0010C\u001a\u00020D2\u0006\u0010E\u001a\u00020\u0005HÖ\u0001R\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0014\u0010\u0015R\u0014\u0010\u000e\u001a\u00020\u000fX\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0016\u0010\u0017R\u0014\u0010\u0012\u001a\u00020\u0007X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0018\u0010\u0019R\u0016\u0010\u0002\u001a\u0004\u0018\u00010\u0003X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u001a\u0010\u001bR\u0014\u0010\u0006\u001a\u00020\u0007X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u001c\u0010\u0019R\u0016\u0010\u000b\u001a\u0004\u0018\u00010\fX\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u001d\u0010\u001eR\u001a\u0010\b\u001a\b\u0012\u0004\u0012\u00020\n0\tX\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u001f\u0010 R\u0014\u0010\u0010\u001a\u00020\u0007X\u0004¢\u0006\b\n\u0000\u001a\u0004\b!\u0010\u0019R\u0014\u0010\u0011\u001a\u00020\u0007X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\"\u0010\u0019R\u0018\u0010\r\u001a\u0004\u0018\u00010\u0005X\u0004¢\u0006\n\n\u0002\u0010%\u001a\u0004\b#\u0010$¨\u0006H"}, d2 = {"Lcom/stripe/android/view/PaymentMethodsActivityStarter$Args;", "Lcom/stripe/android/view/ActivityStarter$Args;", "initialPaymentMethodId", "", "addPaymentMethodFooterLayoutId", "", "isPaymentSessionActive", "", "paymentMethodTypes", "", "Lcom/stripe/android/model/PaymentMethod$Type;", "paymentConfiguration", "Lcom/stripe/android/PaymentConfiguration;", "windowFlags", "billingAddressFields", "Lcom/stripe/android/view/BillingAddressFields;", "shouldShowGooglePay", "useGooglePay", "canDeletePaymentMethods", "(Ljava/lang/String;IZLjava/util/List;Lcom/stripe/android/PaymentConfiguration;Ljava/lang/Integer;Lcom/stripe/android/view/BillingAddressFields;ZZZ)V", "getAddPaymentMethodFooterLayoutId", "()I", "getBillingAddressFields$stripe_release", "()Lcom/stripe/android/view/BillingAddressFields;", "getCanDeletePaymentMethods$stripe_release", "()Z", "getInitialPaymentMethodId$stripe_release", "()Ljava/lang/String;", "isPaymentSessionActive$stripe_release", "getPaymentConfiguration$stripe_release", "()Lcom/stripe/android/PaymentConfiguration;", "getPaymentMethodTypes$stripe_release", "()Ljava/util/List;", "getShouldShowGooglePay$stripe_release", "getUseGooglePay$stripe_release", "getWindowFlags$stripe_release", "()Ljava/lang/Integer;", "Ljava/lang/Integer;", "component1", "component1$stripe_release", "component10", "component10$stripe_release", "component2", "component3", "component3$stripe_release", "component4", "component4$stripe_release", "component5", "component5$stripe_release", "component6", "component6$stripe_release", "component7", "component7$stripe_release", "component8", "component8$stripe_release", "component9", "component9$stripe_release", "copy", "(Ljava/lang/String;IZLjava/util/List;Lcom/stripe/android/PaymentConfiguration;Ljava/lang/Integer;Lcom/stripe/android/view/BillingAddressFields;ZZZ)Lcom/stripe/android/view/PaymentMethodsActivityStarter$Args;", "describeContents", "equals", "other", "", "hashCode", "toString", "writeToParcel", "", "parcel", "Landroid/os/Parcel;", "flags", "Builder", "Companion", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: PaymentMethodsActivityStarter.kt */
    public static final class Args implements ActivityStarter.Args {
        public static final Parcelable.Creator CREATOR = new Creator();
        public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
        /* access modifiers changed from: private */
        public static final Args DEFAULT = new Builder().build();
        private final int addPaymentMethodFooterLayoutId;
        private final BillingAddressFields billingAddressFields;
        private final boolean canDeletePaymentMethods;
        private final String initialPaymentMethodId;
        private final boolean isPaymentSessionActive;
        private final PaymentConfiguration paymentConfiguration;
        private final List<PaymentMethod.Type> paymentMethodTypes;
        private final boolean shouldShowGooglePay;
        private final boolean useGooglePay;
        private final Integer windowFlags;

        @Metadata(bv = {1, 0, 3}, k = 3, mv = {1, 1, 16})
        public static class Creator implements Parcelable.Creator {
            public final Object createFromParcel(Parcel parcel) {
                Intrinsics.checkParameterIsNotNull(parcel, "in");
                String readString = parcel.readString();
                int readInt = parcel.readInt();
                boolean z = parcel.readInt() != 0;
                int readInt2 = parcel.readInt();
                ArrayList arrayList = new ArrayList(readInt2);
                while (readInt2 != 0) {
                    arrayList.add((PaymentMethod.Type) Enum.valueOf(PaymentMethod.Type.class, parcel.readString()));
                    readInt2--;
                }
                Integer num = null;
                PaymentConfiguration paymentConfiguration = parcel.readInt() != 0 ? (PaymentConfiguration) PaymentConfiguration.CREATOR.createFromParcel(parcel) : null;
                if (parcel.readInt() != 0) {
                    num = Integer.valueOf(parcel.readInt());
                }
                return new Args(readString, readInt, z, arrayList, paymentConfiguration, num, (BillingAddressFields) Enum.valueOf(BillingAddressFields.class, parcel.readString()), parcel.readInt() != 0, parcel.readInt() != 0, parcel.readInt() != 0);
            }

            public final Object[] newArray(int i) {
                return new Args[i];
            }
        }

        public static /* synthetic */ Args copy$default(Args args, String str, int i, boolean z, List list, PaymentConfiguration paymentConfiguration2, Integer num, BillingAddressFields billingAddressFields2, boolean z2, boolean z3, boolean z4, int i2, Object obj) {
            Args args2 = args;
            int i3 = i2;
            return args.copy((i3 & 1) != 0 ? args2.initialPaymentMethodId : str, (i3 & 2) != 0 ? args2.addPaymentMethodFooterLayoutId : i, (i3 & 4) != 0 ? args2.isPaymentSessionActive : z, (i3 & 8) != 0 ? args2.paymentMethodTypes : list, (i3 & 16) != 0 ? args2.paymentConfiguration : paymentConfiguration2, (i3 & 32) != 0 ? args2.windowFlags : num, (i3 & 64) != 0 ? args2.billingAddressFields : billingAddressFields2, (i3 & 128) != 0 ? args2.shouldShowGooglePay : z2, (i3 & 256) != 0 ? args2.useGooglePay : z3, (i3 & 512) != 0 ? args2.canDeletePaymentMethods : z4);
        }

        public final String component1$stripe_release() {
            return this.initialPaymentMethodId;
        }

        public final boolean component10$stripe_release() {
            return this.canDeletePaymentMethods;
        }

        public final int component2() {
            return this.addPaymentMethodFooterLayoutId;
        }

        public final boolean component3$stripe_release() {
            return this.isPaymentSessionActive;
        }

        public final List<PaymentMethod.Type> component4$stripe_release() {
            return this.paymentMethodTypes;
        }

        public final PaymentConfiguration component5$stripe_release() {
            return this.paymentConfiguration;
        }

        public final Integer component6$stripe_release() {
            return this.windowFlags;
        }

        public final BillingAddressFields component7$stripe_release() {
            return this.billingAddressFields;
        }

        public final boolean component8$stripe_release() {
            return this.shouldShowGooglePay;
        }

        public final boolean component9$stripe_release() {
            return this.useGooglePay;
        }

        public final Args copy(String str, int i, boolean z, List<? extends PaymentMethod.Type> list, PaymentConfiguration paymentConfiguration2, Integer num, BillingAddressFields billingAddressFields2, boolean z2, boolean z3, boolean z4) {
            List<? extends PaymentMethod.Type> list2 = list;
            Intrinsics.checkParameterIsNotNull(list2, "paymentMethodTypes");
            BillingAddressFields billingAddressFields3 = billingAddressFields2;
            Intrinsics.checkParameterIsNotNull(billingAddressFields3, "billingAddressFields");
            return new Args(str, i, z, list2, paymentConfiguration2, num, billingAddressFields3, z2, z3, z4);
        }

        public int describeContents() {
            return 0;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Args)) {
                return false;
            }
            Args args = (Args) obj;
            return Intrinsics.areEqual((Object) this.initialPaymentMethodId, (Object) args.initialPaymentMethodId) && this.addPaymentMethodFooterLayoutId == args.addPaymentMethodFooterLayoutId && this.isPaymentSessionActive == args.isPaymentSessionActive && Intrinsics.areEqual((Object) this.paymentMethodTypes, (Object) args.paymentMethodTypes) && Intrinsics.areEqual((Object) this.paymentConfiguration, (Object) args.paymentConfiguration) && Intrinsics.areEqual((Object) this.windowFlags, (Object) args.windowFlags) && Intrinsics.areEqual((Object) this.billingAddressFields, (Object) args.billingAddressFields) && this.shouldShowGooglePay == args.shouldShowGooglePay && this.useGooglePay == args.useGooglePay && this.canDeletePaymentMethods == args.canDeletePaymentMethods;
        }

        public int hashCode() {
            String str = this.initialPaymentMethodId;
            int i = 0;
            int hashCode = (((str != null ? str.hashCode() : 0) * 31) + this.addPaymentMethodFooterLayoutId) * 31;
            boolean z = this.isPaymentSessionActive;
            boolean z2 = true;
            if (z) {
                z = true;
            }
            int i2 = (hashCode + (z ? 1 : 0)) * 31;
            List<PaymentMethod.Type> list = this.paymentMethodTypes;
            int hashCode2 = (i2 + (list != null ? list.hashCode() : 0)) * 31;
            PaymentConfiguration paymentConfiguration2 = this.paymentConfiguration;
            int hashCode3 = (hashCode2 + (paymentConfiguration2 != null ? paymentConfiguration2.hashCode() : 0)) * 31;
            Integer num = this.windowFlags;
            int hashCode4 = (hashCode3 + (num != null ? num.hashCode() : 0)) * 31;
            BillingAddressFields billingAddressFields2 = this.billingAddressFields;
            if (billingAddressFields2 != null) {
                i = billingAddressFields2.hashCode();
            }
            int i3 = (hashCode4 + i) * 31;
            boolean z3 = this.shouldShowGooglePay;
            if (z3) {
                z3 = true;
            }
            int i4 = (i3 + (z3 ? 1 : 0)) * 31;
            boolean z4 = this.useGooglePay;
            if (z4) {
                z4 = true;
            }
            int i5 = (i4 + (z4 ? 1 : 0)) * 31;
            boolean z5 = this.canDeletePaymentMethods;
            if (!z5) {
                z2 = z5;
            }
            return i5 + (z2 ? 1 : 0);
        }

        public String toString() {
            return "Args(initialPaymentMethodId=" + this.initialPaymentMethodId + ", addPaymentMethodFooterLayoutId=" + this.addPaymentMethodFooterLayoutId + ", isPaymentSessionActive=" + this.isPaymentSessionActive + ", paymentMethodTypes=" + this.paymentMethodTypes + ", paymentConfiguration=" + this.paymentConfiguration + ", windowFlags=" + this.windowFlags + ", billingAddressFields=" + this.billingAddressFields + ", shouldShowGooglePay=" + this.shouldShowGooglePay + ", useGooglePay=" + this.useGooglePay + ", canDeletePaymentMethods=" + this.canDeletePaymentMethods + ")";
        }

        public void writeToParcel(Parcel parcel, int i) {
            Intrinsics.checkParameterIsNotNull(parcel, "parcel");
            parcel.writeString(this.initialPaymentMethodId);
            parcel.writeInt(this.addPaymentMethodFooterLayoutId);
            parcel.writeInt(this.isPaymentSessionActive ? 1 : 0);
            List<PaymentMethod.Type> list = this.paymentMethodTypes;
            parcel.writeInt(list.size());
            for (PaymentMethod.Type name : list) {
                parcel.writeString(name.name());
            }
            PaymentConfiguration paymentConfiguration2 = this.paymentConfiguration;
            if (paymentConfiguration2 != null) {
                parcel.writeInt(1);
                paymentConfiguration2.writeToParcel(parcel, 0);
            } else {
                parcel.writeInt(0);
            }
            Integer num = this.windowFlags;
            if (num != null) {
                parcel.writeInt(1);
                parcel.writeInt(num.intValue());
            } else {
                parcel.writeInt(0);
            }
            parcel.writeString(this.billingAddressFields.name());
            parcel.writeInt(this.shouldShowGooglePay ? 1 : 0);
            parcel.writeInt(this.useGooglePay ? 1 : 0);
            parcel.writeInt(this.canDeletePaymentMethods ? 1 : 0);
        }

        public Args(String str, int i, boolean z, List<? extends PaymentMethod.Type> list, PaymentConfiguration paymentConfiguration2, Integer num, BillingAddressFields billingAddressFields2, boolean z2, boolean z3, boolean z4) {
            Intrinsics.checkParameterIsNotNull(list, "paymentMethodTypes");
            Intrinsics.checkParameterIsNotNull(billingAddressFields2, "billingAddressFields");
            this.initialPaymentMethodId = str;
            this.addPaymentMethodFooterLayoutId = i;
            this.isPaymentSessionActive = z;
            this.paymentMethodTypes = list;
            this.paymentConfiguration = paymentConfiguration2;
            this.windowFlags = num;
            this.billingAddressFields = billingAddressFields2;
            this.shouldShowGooglePay = z2;
            this.useGooglePay = z3;
            this.canDeletePaymentMethods = z4;
        }

        public final String getInitialPaymentMethodId$stripe_release() {
            return this.initialPaymentMethodId;
        }

        public final int getAddPaymentMethodFooterLayoutId() {
            return this.addPaymentMethodFooterLayoutId;
        }

        public final boolean isPaymentSessionActive$stripe_release() {
            return this.isPaymentSessionActive;
        }

        public final List<PaymentMethod.Type> getPaymentMethodTypes$stripe_release() {
            return this.paymentMethodTypes;
        }

        public final PaymentConfiguration getPaymentConfiguration$stripe_release() {
            return this.paymentConfiguration;
        }

        /* JADX WARNING: Illegal instructions before constructor call */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public /* synthetic */ Args(java.lang.String r14, int r15, boolean r16, java.util.List r17, com.stripe.android.PaymentConfiguration r18, java.lang.Integer r19, com.stripe.android.view.BillingAddressFields r20, boolean r21, boolean r22, boolean r23, int r24, kotlin.jvm.internal.DefaultConstructorMarker r25) {
            /*
                r13 = this;
                r0 = r24
                r1 = r0 & 32
                if (r1 == 0) goto L_0x000b
                r1 = 0
                java.lang.Integer r1 = (java.lang.Integer) r1
                r8 = r1
                goto L_0x000d
            L_0x000b:
                r8 = r19
            L_0x000d:
                r1 = r0 & 128(0x80, float:1.794E-43)
                r2 = 0
                if (r1 == 0) goto L_0x0014
                r10 = 0
                goto L_0x0016
            L_0x0014:
                r10 = r21
            L_0x0016:
                r1 = r0 & 256(0x100, float:3.59E-43)
                if (r1 == 0) goto L_0x001c
                r11 = 0
                goto L_0x001e
            L_0x001c:
                r11 = r22
            L_0x001e:
                r0 = r0 & 512(0x200, float:7.175E-43)
                if (r0 == 0) goto L_0x0025
                r0 = 1
                r12 = 1
                goto L_0x0027
            L_0x0025:
                r12 = r23
            L_0x0027:
                r2 = r13
                r3 = r14
                r4 = r15
                r5 = r16
                r6 = r17
                r7 = r18
                r9 = r20
                r2.<init>(r3, r4, r5, r6, r7, r8, r9, r10, r11, r12)
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.stripe.android.view.PaymentMethodsActivityStarter.Args.<init>(java.lang.String, int, boolean, java.util.List, com.stripe.android.PaymentConfiguration, java.lang.Integer, com.stripe.android.view.BillingAddressFields, boolean, boolean, boolean, int, kotlin.jvm.internal.DefaultConstructorMarker):void");
        }

        public final Integer getWindowFlags$stripe_release() {
            return this.windowFlags;
        }

        public final BillingAddressFields getBillingAddressFields$stripe_release() {
            return this.billingAddressFields;
        }

        public final boolean getShouldShowGooglePay$stripe_release() {
            return this.shouldShowGooglePay;
        }

        public final boolean getUseGooglePay$stripe_release() {
            return this.useGooglePay;
        }

        public final boolean getCanDeletePaymentMethods$stripe_release() {
            return this.canDeletePaymentMethods;
        }

        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0012\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0003J\b\u0010\u0016\u001a\u00020\u0002H\u0016J\u0010\u0010\u0017\u001a\u00020\u00002\b\b\u0001\u0010\u0004\u001a\u00020\u0005J\u000e\u0010\u0018\u001a\u00020\u00002\u0006\u0010\u0006\u001a\u00020\u0007J\u000e\u0010\u0019\u001a\u00020\u00002\u0006\u0010\b\u001a\u00020\tJ\u0010\u0010\u001a\u001a\u00020\u00002\b\u0010\n\u001a\u0004\u0018\u00010\u000bJ\u000e\u0010\u001b\u001a\u00020\u00002\u0006\u0010\f\u001a\u00020\tJ\u0010\u0010\u001c\u001a\u00020\u00002\b\u0010\r\u001a\u0004\u0018\u00010\u000eJ\u0014\u0010\u001d\u001a\u00020\u00002\f\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u00110\u0010J\u000e\u0010\u001e\u001a\u00020\u00002\u0006\u0010\u0012\u001a\u00020\tJ\u0015\u0010\u001f\u001a\u00020\u00002\u0006\u0010\u0013\u001a\u00020\tH\u0000¢\u0006\u0002\b J\u0015\u0010!\u001a\u00020\u00002\b\u0010\u0014\u001a\u0004\u0018\u00010\u0005¢\u0006\u0002\u0010\"R\u0012\u0010\u0004\u001a\u00020\u00058\u0002@\u0002X\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\n\u001a\u0004\u0018\u00010\u000bX\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\tX\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\r\u001a\u0004\u0018\u00010\u000eX\u000e¢\u0006\u0002\n\u0000R\u0016\u0010\u000f\u001a\n\u0012\u0004\u0012\u00020\u0011\u0018\u00010\u0010X\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\tX\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\tX\u000e¢\u0006\u0002\n\u0000R\u0012\u0010\u0014\u001a\u0004\u0018\u00010\u0005X\u000e¢\u0006\u0004\n\u0002\u0010\u0015¨\u0006#"}, d2 = {"Lcom/stripe/android/view/PaymentMethodsActivityStarter$Args$Builder;", "Lcom/stripe/android/ObjectBuilder;", "Lcom/stripe/android/view/PaymentMethodsActivityStarter$Args;", "()V", "addPaymentMethodFooterLayoutId", "", "billingAddressFields", "Lcom/stripe/android/view/BillingAddressFields;", "canDeletePaymentMethods", "", "initialPaymentMethodId", "", "isPaymentSessionActive", "paymentConfiguration", "Lcom/stripe/android/PaymentConfiguration;", "paymentMethodTypes", "", "Lcom/stripe/android/model/PaymentMethod$Type;", "shouldShowGooglePay", "useGooglePay", "windowFlags", "Ljava/lang/Integer;", "build", "setAddPaymentMethodFooter", "setBillingAddressFields", "setCanDeletePaymentMethods", "setInitialPaymentMethodId", "setIsPaymentSessionActive", "setPaymentConfiguration", "setPaymentMethodTypes", "setShouldShowGooglePay", "setUseGooglePay", "setUseGooglePay$stripe_release", "setWindowFlags", "(Ljava/lang/Integer;)Lcom/stripe/android/view/PaymentMethodsActivityStarter$Args$Builder;", "stripe_release"}, k = 1, mv = {1, 1, 16})
        /* compiled from: PaymentMethodsActivityStarter.kt */
        public static final class Builder implements ObjectBuilder<Args> {
            private int addPaymentMethodFooterLayoutId;
            private BillingAddressFields billingAddressFields = BillingAddressFields.PostalCode;
            private boolean canDeletePaymentMethods = true;
            private String initialPaymentMethodId;
            private boolean isPaymentSessionActive;
            private PaymentConfiguration paymentConfiguration;
            private List<? extends PaymentMethod.Type> paymentMethodTypes;
            private boolean shouldShowGooglePay;
            private boolean useGooglePay;
            private Integer windowFlags;

            public final Builder setBillingAddressFields(BillingAddressFields billingAddressFields2) {
                Intrinsics.checkParameterIsNotNull(billingAddressFields2, "billingAddressFields");
                Builder builder = this;
                builder.billingAddressFields = billingAddressFields2;
                return builder;
            }

            public final Builder setInitialPaymentMethodId(String str) {
                Builder builder = this;
                builder.initialPaymentMethodId = str;
                return builder;
            }

            public final Builder setIsPaymentSessionActive(boolean z) {
                Builder builder = this;
                builder.isPaymentSessionActive = z;
                return builder;
            }

            public final Builder setPaymentConfiguration(PaymentConfiguration paymentConfiguration2) {
                Builder builder = this;
                builder.paymentConfiguration = paymentConfiguration2;
                return builder;
            }

            public final Builder setPaymentMethodTypes(List<? extends PaymentMethod.Type> list) {
                Intrinsics.checkParameterIsNotNull(list, "paymentMethodTypes");
                Builder builder = this;
                builder.paymentMethodTypes = list;
                return builder;
            }

            public final Builder setShouldShowGooglePay(boolean z) {
                Builder builder = this;
                builder.shouldShowGooglePay = z;
                return builder;
            }

            public final Builder setAddPaymentMethodFooter(int i) {
                Builder builder = this;
                builder.addPaymentMethodFooterLayoutId = i;
                return builder;
            }

            public final Builder setWindowFlags(Integer num) {
                Builder builder = this;
                builder.windowFlags = num;
                return builder;
            }

            public final Builder setUseGooglePay$stripe_release(boolean z) {
                Builder builder = this;
                builder.useGooglePay = z;
                return builder;
            }

            public final Builder setCanDeletePaymentMethods(boolean z) {
                Builder builder = this;
                builder.canDeletePaymentMethods = z;
                return builder;
            }

            public Args build() {
                String str = this.initialPaymentMethodId;
                boolean z = this.isPaymentSessionActive;
                List<? extends PaymentMethod.Type> list = this.paymentMethodTypes;
                if (list == null) {
                    list = CollectionsKt.listOf(PaymentMethod.Type.Card);
                }
                boolean z2 = this.shouldShowGooglePay;
                boolean z3 = this.useGooglePay;
                PaymentConfiguration paymentConfiguration2 = this.paymentConfiguration;
                return new Args(str, this.addPaymentMethodFooterLayoutId, z, list, paymentConfiguration2, this.windowFlags, this.billingAddressFields, z2, z3, this.canDeletePaymentMethods);
            }
        }

        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0015\u0010\u0007\u001a\u00020\u00042\u0006\u0010\b\u001a\u00020\tH\u0000¢\u0006\u0002\b\nR\u0014\u0010\u0003\u001a\u00020\u0004X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006¨\u0006\u000b"}, d2 = {"Lcom/stripe/android/view/PaymentMethodsActivityStarter$Args$Companion;", "", "()V", "DEFAULT", "Lcom/stripe/android/view/PaymentMethodsActivityStarter$Args;", "getDEFAULT$stripe_release", "()Lcom/stripe/android/view/PaymentMethodsActivityStarter$Args;", "create", "intent", "Landroid/content/Intent;", "create$stripe_release", "stripe_release"}, k = 1, mv = {1, 1, 16})
        /* compiled from: PaymentMethodsActivityStarter.kt */
        public static final class Companion {
            private Companion() {
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }

            public final Args getDEFAULT$stripe_release() {
                return Args.DEFAULT;
            }

            public final /* synthetic */ Args create$stripe_release(Intent intent) {
                Intrinsics.checkParameterIsNotNull(intent, "intent");
                Parcelable parcelableExtra = intent.getParcelableExtra("extra_activity_args");
                if (parcelableExtra != null) {
                    return (Args) parcelableExtra;
                }
                throw new IllegalArgumentException("Required value was null.".toString());
            }
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\b\b\u0018\u0000 \u001b2\u00020\u0001:\u0001\u001bB\u001d\b\u0000\u0012\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\b\b\u0002\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\u000b\u0010\t\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\t\u0010\n\u001a\u00020\u0005HÆ\u0003J\u001f\u0010\u000b\u001a\u00020\u00002\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u0005HÆ\u0001J\t\u0010\f\u001a\u00020\rHÖ\u0001J\u0013\u0010\u000e\u001a\u00020\u00052\b\u0010\u000f\u001a\u0004\u0018\u00010\u0010HÖ\u0003J\t\u0010\u0011\u001a\u00020\rHÖ\u0001J\b\u0010\u0012\u001a\u00020\u0013H\u0016J\t\u0010\u0014\u001a\u00020\u0015HÖ\u0001J\u0019\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00020\rHÖ\u0001R\u0012\u0010\u0002\u001a\u0004\u0018\u00010\u00038\u0006X\u0004¢\u0006\u0002\n\u0000R\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\b¨\u0006\u001c"}, d2 = {"Lcom/stripe/android/view/PaymentMethodsActivityStarter$Result;", "Lcom/stripe/android/view/ActivityStarter$Result;", "paymentMethod", "Lcom/stripe/android/model/PaymentMethod;", "useGooglePay", "", "(Lcom/stripe/android/model/PaymentMethod;Z)V", "getUseGooglePay", "()Z", "component1", "component2", "copy", "describeContents", "", "equals", "other", "", "hashCode", "toBundle", "Landroid/os/Bundle;", "toString", "", "writeToParcel", "", "parcel", "Landroid/os/Parcel;", "flags", "Companion", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: PaymentMethodsActivityStarter.kt */
    public static final class Result implements ActivityStarter.Result {
        public static final Parcelable.Creator CREATOR = new Creator();
        public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
        public final PaymentMethod paymentMethod;
        private final boolean useGooglePay;

        @Metadata(bv = {1, 0, 3}, k = 3, mv = {1, 1, 16})
        public static class Creator implements Parcelable.Creator {
            public final Object createFromParcel(Parcel parcel) {
                Intrinsics.checkParameterIsNotNull(parcel, "in");
                return new Result(parcel.readInt() != 0 ? (PaymentMethod) PaymentMethod.CREATOR.createFromParcel(parcel) : null, parcel.readInt() != 0);
            }

            public final Object[] newArray(int i) {
                return new Result[i];
            }
        }

        public Result() {
            this((PaymentMethod) null, false, 3, (DefaultConstructorMarker) null);
        }

        public static /* synthetic */ Result copy$default(Result result, PaymentMethod paymentMethod2, boolean z, int i, Object obj) {
            if ((i & 1) != 0) {
                paymentMethod2 = result.paymentMethod;
            }
            if ((i & 2) != 0) {
                z = result.useGooglePay;
            }
            return result.copy(paymentMethod2, z);
        }

        @JvmStatic
        public static final Result fromIntent(Intent intent) {
            return Companion.fromIntent(intent);
        }

        public final PaymentMethod component1() {
            return this.paymentMethod;
        }

        public final boolean component2() {
            return this.useGooglePay;
        }

        public final Result copy(PaymentMethod paymentMethod2, boolean z) {
            return new Result(paymentMethod2, z);
        }

        public int describeContents() {
            return 0;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Result)) {
                return false;
            }
            Result result = (Result) obj;
            return Intrinsics.areEqual((Object) this.paymentMethod, (Object) result.paymentMethod) && this.useGooglePay == result.useGooglePay;
        }

        public int hashCode() {
            PaymentMethod paymentMethod2 = this.paymentMethod;
            int hashCode = (paymentMethod2 != null ? paymentMethod2.hashCode() : 0) * 31;
            boolean z = this.useGooglePay;
            if (z) {
                z = true;
            }
            return hashCode + (z ? 1 : 0);
        }

        public String toString() {
            return "Result(paymentMethod=" + this.paymentMethod + ", useGooglePay=" + this.useGooglePay + ")";
        }

        public void writeToParcel(Parcel parcel, int i) {
            Intrinsics.checkParameterIsNotNull(parcel, "parcel");
            PaymentMethod paymentMethod2 = this.paymentMethod;
            if (paymentMethod2 != null) {
                parcel.writeInt(1);
                paymentMethod2.writeToParcel(parcel, 0);
            } else {
                parcel.writeInt(0);
            }
            parcel.writeInt(this.useGooglePay ? 1 : 0);
        }

        public Result(PaymentMethod paymentMethod2, boolean z) {
            this.paymentMethod = paymentMethod2;
            this.useGooglePay = z;
        }

        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ Result(PaymentMethod paymentMethod2, boolean z, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this((i & 1) != 0 ? null : paymentMethod2, (i & 2) != 0 ? false : z);
        }

        public final boolean getUseGooglePay() {
            return this.useGooglePay;
        }

        public Bundle toBundle() {
            Bundle bundle = new Bundle();
            bundle.putParcelable("extra_activity_result", this);
            return bundle;
        }

        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0014\u0010\u0003\u001a\u0004\u0018\u00010\u00042\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006H\u0007¨\u0006\u0007"}, d2 = {"Lcom/stripe/android/view/PaymentMethodsActivityStarter$Result$Companion;", "", "()V", "fromIntent", "Lcom/stripe/android/view/PaymentMethodsActivityStarter$Result;", "intent", "Landroid/content/Intent;", "stripe_release"}, k = 1, mv = {1, 1, 16})
        /* compiled from: PaymentMethodsActivityStarter.kt */
        public static final class Companion {
            private Companion() {
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }

            @JvmStatic
            public final Result fromIntent(Intent intent) {
                if (intent != null) {
                    return (Result) intent.getParcelableExtra("extra_activity_result");
                }
                return null;
            }
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\u0005"}, d2 = {"Lcom/stripe/android/view/PaymentMethodsActivityStarter$Companion;", "", "()V", "REQUEST_CODE", "", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: PaymentMethodsActivityStarter.kt */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }
}
