package com.stripe.android.view;

import android.animation.ValueAnimator;
import android.view.View;
import android.view.ViewGroup;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\b\u0003\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n¢\u0006\u0002\b\u0005¨\u0006\u0006"}, d2 = {"<anonymous>", "", "valueAnimator", "Landroid/animation/ValueAnimator;", "kotlin.jvm.PlatformType", "onAnimationUpdate", "com/stripe/android/view/ViewWidthAnimator$animate$1$1"}, k = 3, mv = {1, 1, 16})
/* compiled from: ViewWidthAnimator.kt */
final class ViewWidthAnimator$animate$$inlined$also$lambda$1 implements ValueAnimator.AnimatorUpdateListener {
    final /* synthetic */ int $endWidth$inlined;
    final /* synthetic */ Function0 $onAnimationEnd$inlined;
    final /* synthetic */ ViewWidthAnimator this$0;

    ViewWidthAnimator$animate$$inlined$also$lambda$1(ViewWidthAnimator viewWidthAnimator, int i, Function0 function0) {
        this.this$0 = viewWidthAnimator;
        this.$endWidth$inlined = i;
        this.$onAnimationEnd$inlined = function0;
    }

    public final void onAnimationUpdate(ValueAnimator valueAnimator) {
        Intrinsics.checkExpressionValueIsNotNull(valueAnimator, "valueAnimator");
        Object animatedValue = valueAnimator.getAnimatedValue();
        if (animatedValue != null) {
            int intValue = ((Integer) animatedValue).intValue();
            View access$getView$p = this.this$0.view;
            ViewGroup.LayoutParams layoutParams = this.this$0.view.getLayoutParams();
            layoutParams.width = intValue;
            access$getView$p.setLayoutParams(layoutParams);
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type kotlin.Int");
    }
}
