package com.stripe.android.view;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.os.Parcelable;
import android.text.Layout;
import android.text.TextPaint;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.Transformation;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import androidx.core.view.ViewCompat;
import com.facebook.appevents.AppEventsConstants;
import com.google.android.material.textfield.TextInputLayout;
import com.stripe.android.R;
import com.stripe.android.databinding.CardInputWidgetBinding;
import com.stripe.android.model.Address;
import com.stripe.android.model.Card;
import com.stripe.android.model.CardBrand;
import com.stripe.android.model.PaymentMethod;
import com.stripe.android.model.PaymentMethodCreateParams;
import com.stripe.android.view.CardValidCallback;
import com.stripe.android.view.PostalCodeEditText;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.collections.CollectionsKt;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.MutablePropertyReference1Impl;
import kotlin.jvm.internal.Reflection;
import kotlin.properties.Delegates;
import kotlin.properties.ReadWriteProperty;
import kotlin.reflect.KProperty;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\b\u0003\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u001d\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\f\n\u0002\u0018\u0002\n\u0002\b\u0013\n\u0002\u0018\u0002\n\u0002\b\u001a*\u0002,P\u0018\u0000 Þ\u00012\u00020\u00012\u00020\u0002:\u001cÚ\u0001Û\u0001Ü\u0001Ý\u0001Þ\u0001ß\u0001à\u0001á\u0001â\u0001ã\u0001ä\u0001å\u0001æ\u0001ç\u0001B%\b\u0007\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\n\b\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u0012\b\b\u0002\u0010\u0007\u001a\u00020\b¢\u0006\u0002\u0010\tJ\u0012\u0010\u0001\u001a\u00030\u00012\u0006\u0010\u0005\u001a\u00020\u0006H\u0002J\n\u0010\u0001\u001a\u00030\u0001H\u0016J\"\u0010\u0001\u001a\u00020;2\u0006\u0010\u0011\u001a\u00020\u00122\t\b\u0002\u0010\u0001\u001a\u00020;H\u0001¢\u0006\u0003\b\u0001J\u001b\u0010\u0001\u001a\u00020\b2\u0007\u0010\u0001\u001a\u00020;2\u0007\u0010\u0001\u001a\u00020\fH\u0002J\u001b\u0010\u0001\u001a\u0005\u0018\u00010\u00012\u0007\u0010\u0001\u001a\u00020\bH\u0001¢\u0006\u0003\b\u0001J\u0014\u0010\u0001\u001a\u00030\u00012\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006H\u0002J\t\u0010 \u0001\u001a\u00020XH\u0016J\n\u0010¡\u0001\u001a\u00030\u0001H\u0014J\u0013\u0010¢\u0001\u001a\u00020X2\b\u0010£\u0001\u001a\u00030¤\u0001H\u0016J7\u0010¥\u0001\u001a\u00030\u00012\u0007\u0010¦\u0001\u001a\u00020X2\u0007\u0010§\u0001\u001a\u00020\b2\u0007\u0010¨\u0001\u001a\u00020\b2\u0007\u0010©\u0001\u001a\u00020\b2\u0007\u0010ª\u0001\u001a\u00020\bH\u0014J\u0014\u0010«\u0001\u001a\u00030\u00012\b\u0010¬\u0001\u001a\u00030­\u0001H\u0014J\n\u0010®\u0001\u001a\u00030­\u0001H\u0014J\u0013\u0010¯\u0001\u001a\u00030\u00012\u0007\u0010°\u0001\u001a\u00020XH\u0016J\n\u0010±\u0001\u001a\u00030\u0001H\u0002J\n\u0010²\u0001\u001a\u00030\u0001H\u0002J\u0013\u0010³\u0001\u001a\u00030\u00012\u0007\u0010´\u0001\u001a\u00020;H\u0016J\u0015\u0010µ\u0001\u001a\u00030\u00012\t\u0010¶\u0001\u001a\u0004\u0018\u00010\"H\u0016J\u0015\u0010·\u0001\u001a\u00030\u00012\t\u0010\u0001\u001a\u0004\u0018\u00010;H\u0016J\u0016\u0010¸\u0001\u001a\u00030\u00012\n\u0010¹\u0001\u001a\u0005\u0018\u00010º\u0001H\u0016J\u0015\u0010»\u0001\u001a\u00030\u00012\t\u0010¼\u0001\u001a\u0004\u0018\u00010*H\u0016J\u0015\u0010½\u0001\u001a\u00030\u00012\t\u0010¾\u0001\u001a\u0004\u0018\u00010;H\u0016J\u0016\u0010¿\u0001\u001a\u00030\u00012\n\u0010À\u0001\u001a\u0005\u0018\u00010º\u0001H\u0016J\u0013\u0010Á\u0001\u001a\u00030\u00012\u0007\u0010 \u0001\u001a\u00020XH\u0016J \u0010Â\u0001\u001a\u00030\u00012\t\b\u0001\u0010Ã\u0001\u001a\u00020\b2\t\b\u0001\u0010Ä\u0001\u001a\u00020\bH\u0016J\u0016\u0010Å\u0001\u001a\u00030\u00012\n\u0010Æ\u0001\u001a\u0005\u0018\u00010º\u0001H\u0016J\u001b\u0010Ç\u0001\u001a\u00030\u00012\t\u0010È\u0001\u001a\u0004\u0018\u00010;H\u0000¢\u0006\u0003\bÉ\u0001J\u0016\u0010Ê\u0001\u001a\u00030\u00012\n\u0010Ë\u0001\u001a\u0005\u0018\u00010º\u0001H\u0016J\u001a\u0010Ì\u0001\u001a\u00030\u00012\u000e\u0010Í\u0001\u001a\t\u0012\u0005\u0012\u00030Î\u00010\u000bH\u0002J&\u0010Ï\u0001\u001a\u00030\u00012\b\u0010Ð\u0001\u001a\u00030\u00012\u0007\u0010Ñ\u0001\u001a\u00020\b2\u0007\u0010Ò\u0001\u001a\u00020\bH\u0002J\n\u0010Ó\u0001\u001a\u00030\u0001H\u0002J\u001e\u0010Ô\u0001\u001a\u00030\u00012\u0007\u0010Õ\u0001\u001a\u00020X2\t\u0010Ö\u0001\u001a\u0004\u0018\u00010;H\u0002J\n\u0010×\u0001\u001a\u00030\u0001H\u0002J\u0018\u0010Ø\u0001\u001a\u00030\u00012\u0006\u0010W\u001a\u00020XH\u0001¢\u0006\u0003\bÙ\u0001R\u0014\u0010\n\u001a\b\u0012\u0004\u0012\u00020\f0\u000bX\u0004¢\u0006\u0002\n\u0000R\u0016\u0010\r\u001a\u0004\u0018\u00010\u000e8BX\u0004¢\u0006\u0006\u001a\u0004\b\u000f\u0010\u0010R\u0014\u0010\u0011\u001a\u00020\u00128BX\u0004¢\u0006\u0006\u001a\u0004\b\u0013\u0010\u0014R\u0016\u0010\u0015\u001a\u0004\u0018\u00010\u00168VX\u0004¢\u0006\u0006\u001a\u0004\b\u0017\u0010\u0018R\u0014\u0010\u0019\u001a\u00020\u001aX\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u001b\u0010\u001cR\u0016\u0010\u001d\u001a\u0004\u0018\u00010\u001e8VX\u0004¢\u0006\u0006\u001a\u0004\b\u001f\u0010 R\u0010\u0010!\u001a\u0004\u0018\u00010\"X\u000e¢\u0006\u0002\n\u0000R\u0014\u0010#\u001a\u00020$X\u0004¢\u0006\b\n\u0000\u001a\u0004\b%\u0010&R\u000e\u0010'\u001a\u00020(X\u0004¢\u0006\u0002\n\u0000R\u0010\u0010)\u001a\u0004\u0018\u00010*X\u000e¢\u0006\u0002\n\u0000R\u0010\u0010+\u001a\u00020,X\u0004¢\u0006\u0004\n\u0002\u0010-R\u000e\u0010.\u001a\u00020/X\u0004¢\u0006\u0002\n\u0000R \u00100\u001a\b\u0012\u0004\u0012\u00020\f0\u000b8@X\u0004¢\u0006\f\u0012\u0004\b1\u00102\u001a\u0004\b3\u00104R\u0014\u00105\u001a\u000206X\u0004¢\u0006\b\n\u0000\u001a\u0004\b7\u00108R\u000e\u00109\u001a\u00020(X\u0004¢\u0006\u0002\n\u0000R\u0014\u0010:\u001a\u00020;8BX\u0004¢\u0006\u0006\u001a\u0004\b<\u0010=R\u0016\u0010>\u001a\u0004\u0018\u00010;8BX\u0004¢\u0006\u0006\u001a\u0004\b?\u0010=R\u0014\u0010@\u001a\u00020AX\u0004¢\u0006\b\n\u0000\u001a\u0004\bB\u0010CR\u000e\u0010D\u001a\u00020(X\u0004¢\u0006\u0002\n\u0000R\u0014\u0010E\u001a\u00020\b8BX\u0004¢\u0006\u0006\u001a\u0004\bF\u0010GR \u0010H\u001a\b\u0012\u0004\u0012\u00020\b0IX\u000e¢\u0006\u000e\n\u0000\u001a\u0004\bJ\u0010K\"\u0004\bL\u0010MR\u000e\u0010N\u001a\u00020;X\u000e¢\u0006\u0002\n\u0000R\u0010\u0010O\u001a\u00020PX\u0004¢\u0006\u0004\n\u0002\u0010QR\u001a\u0010R\u001a\b\u0012\u0004\u0012\u00020T0S8BX\u0004¢\u0006\u0006\u001a\u0004\bU\u0010VR\u001a\u0010W\u001a\u00020XX\u000e¢\u0006\u000e\n\u0000\u001a\u0004\bY\u0010Z\"\u0004\b[\u0010\\R\u000e\u0010]\u001a\u00020XX\u000e¢\u0006\u0002\n\u0000R\u001a\u0010^\u001a\u00020_X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b`\u0010a\"\u0004\bb\u0010cR\u0016\u0010d\u001a\u0004\u0018\u00010e8VX\u0004¢\u0006\u0006\u001a\u0004\bf\u0010gR\u0016\u0010h\u001a\u0004\u0018\u00010i8VX\u0004¢\u0006\u0006\u001a\u0004\bj\u0010kR\u0014\u0010l\u001a\u00020;8BX\u0004¢\u0006\u0006\u001a\u0004\bm\u0010=R\u0014\u0010n\u001a\u00020oX\u0004¢\u0006\b\n\u0000\u001a\u0004\bp\u0010qR\u0014\u0010r\u001a\u00020sX\u0004¢\u0006\b\n\u0000\u001a\u0004\bt\u0010uR+\u0010w\u001a\u00020X2\u0006\u0010v\u001a\u00020X8F@FX\u0002¢\u0006\u0012\n\u0004\bz\u0010{\u001a\u0004\bx\u0010Z\"\u0004\by\u0010\\R\u001a\u0010|\u001a\u00020XX\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b}\u0010Z\"\u0004\b~\u0010\\R\u0016\u0010\u001a\u00020(X\u0004¢\u0006\n\n\u0000\u001a\u0006\b\u0001\u0010\u0001R\u0018\u0010\u0001\u001a\u0004\u0018\u00010;8BX\u0004¢\u0006\u0007\u001a\u0005\b\u0001\u0010=R%\u0010\u0001\u001a\b\u0012\u0004\u0012\u00020\f0\u000b8\u0000X\u0004¢\u0006\u0010\n\u0000\u0012\u0005\b\u0001\u00102\u001a\u0005\b\u0001\u00104R1\u0010\u0001\u001a\u00020X2\u0007\u0010\u0001\u001a\u00020X8\u0000@BX\u000e¢\u0006\u0017\n\u0000\u0012\u0005\b\u0001\u00102\u001a\u0005\b\u0001\u0010Z\"\u0005\b\u0001\u0010\\R/\u0010\u0001\u001a\u00020X2\u0006\u0010v\u001a\u00020X8F@FX\u0002¢\u0006\u0015\n\u0005\b\u0001\u0010{\u001a\u0005\b\u0001\u0010Z\"\u0005\b\u0001\u0010\\R\u0010\u0010\u0001\u001a\u00030\u0001X\u0004¢\u0006\u0002\n\u0000¨\u0006è\u0001"}, d2 = {"Lcom/stripe/android/view/CardInputWidget;", "Landroid/widget/LinearLayout;", "Lcom/stripe/android/view/CardWidget;", "context", "Landroid/content/Context;", "attrs", "Landroid/util/AttributeSet;", "defStyleAttr", "", "(Landroid/content/Context;Landroid/util/AttributeSet;I)V", "allFields", "", "Lcom/stripe/android/view/StripeEditText;", "billingDetails", "Lcom/stripe/android/model/PaymentMethod$BillingDetails;", "getBillingDetails", "()Lcom/stripe/android/model/PaymentMethod$BillingDetails;", "brand", "Lcom/stripe/android/model/CardBrand;", "getBrand", "()Lcom/stripe/android/model/CardBrand;", "card", "Lcom/stripe/android/model/Card;", "getCard", "()Lcom/stripe/android/model/Card;", "cardBrandView", "Lcom/stripe/android/view/CardBrandView;", "getCardBrandView$stripe_release", "()Lcom/stripe/android/view/CardBrandView;", "cardBuilder", "Lcom/stripe/android/model/Card$Builder;", "getCardBuilder", "()Lcom/stripe/android/model/Card$Builder;", "cardInputListener", "Lcom/stripe/android/view/CardInputListener;", "cardNumberEditText", "Lcom/stripe/android/view/CardNumberEditText;", "getCardNumberEditText$stripe_release", "()Lcom/stripe/android/view/CardNumberEditText;", "cardNumberTextInputLayout", "Lcom/google/android/material/textfield/TextInputLayout;", "cardValidCallback", "Lcom/stripe/android/view/CardValidCallback;", "cardValidTextWatcher", "com/stripe/android/view/CardInputWidget$cardValidTextWatcher$1", "Lcom/stripe/android/view/CardInputWidget$cardValidTextWatcher$1;", "containerLayout", "Landroid/widget/FrameLayout;", "currentFields", "currentFields$annotations", "()V", "getCurrentFields$stripe_release", "()Ljava/util/List;", "cvcNumberEditText", "Lcom/stripe/android/view/CvcEditText;", "getCvcNumberEditText$stripe_release", "()Lcom/stripe/android/view/CvcEditText;", "cvcNumberTextInputLayout", "cvcPlaceHolder", "", "getCvcPlaceHolder", "()Ljava/lang/String;", "cvcValue", "getCvcValue", "expiryDateEditText", "Lcom/stripe/android/view/ExpiryDateEditText;", "getExpiryDateEditText$stripe_release", "()Lcom/stripe/android/view/ExpiryDateEditText;", "expiryDateTextInputLayout", "frameWidth", "getFrameWidth", "()I", "frameWidthSupplier", "Lkotlin/Function0;", "getFrameWidthSupplier$stripe_release", "()Lkotlin/jvm/functions/Function0;", "setFrameWidthSupplier$stripe_release", "(Lkotlin/jvm/functions/Function0;)V", "hiddenCardText", "inputChangeTextWatcher", "com/stripe/android/view/CardInputWidget$inputChangeTextWatcher$1", "Lcom/stripe/android/view/CardInputWidget$inputChangeTextWatcher$1;", "invalidFields", "", "Lcom/stripe/android/view/CardValidCallback$Fields;", "getInvalidFields", "()Ljava/util/Set;", "isShowingFullCard", "", "isShowingFullCard$stripe_release", "()Z", "setShowingFullCard$stripe_release", "(Z)V", "isViewInitialized", "layoutWidthCalculator", "Lcom/stripe/android/view/CardInputWidget$LayoutWidthCalculator;", "getLayoutWidthCalculator$stripe_release", "()Lcom/stripe/android/view/CardInputWidget$LayoutWidthCalculator;", "setLayoutWidthCalculator$stripe_release", "(Lcom/stripe/android/view/CardInputWidget$LayoutWidthCalculator;)V", "paymentMethodCard", "Lcom/stripe/android/model/PaymentMethodCreateParams$Card;", "getPaymentMethodCard", "()Lcom/stripe/android/model/PaymentMethodCreateParams$Card;", "paymentMethodCreateParams", "Lcom/stripe/android/model/PaymentMethodCreateParams;", "getPaymentMethodCreateParams", "()Lcom/stripe/android/model/PaymentMethodCreateParams;", "peekCardText", "getPeekCardText", "placementParameters", "Lcom/stripe/android/view/CardInputWidget$PlacementParameters;", "getPlacementParameters$stripe_release", "()Lcom/stripe/android/view/CardInputWidget$PlacementParameters;", "postalCodeEditText", "Lcom/stripe/android/view/PostalCodeEditText;", "getPostalCodeEditText$stripe_release", "()Lcom/stripe/android/view/PostalCodeEditText;", "<set-?>", "postalCodeEnabled", "getPostalCodeEnabled", "setPostalCodeEnabled", "postalCodeEnabled$delegate", "Lkotlin/properties/ReadWriteProperty;", "postalCodeRequired", "getPostalCodeRequired", "setPostalCodeRequired", "postalCodeTextInputLayout", "getPostalCodeTextInputLayout$stripe_release", "()Lcom/google/android/material/textfield/TextInputLayout;", "postalCodeValue", "getPostalCodeValue", "requiredFields", "requiredFields$annotations", "getRequiredFields$stripe_release", "value", "shouldShowErrorIcon", "shouldShowErrorIcon$annotations", "getShouldShowErrorIcon$stripe_release", "setShouldShowErrorIcon", "usZipCodeRequired", "getUsZipCodeRequired", "setUsZipCodeRequired", "usZipCodeRequired$delegate", "viewBinding", "Lcom/stripe/android/databinding/CardInputWidgetBinding;", "applyAttributes", "", "clear", "createHiddenCardText", "cardNumber", "createHiddenCardText$stripe_release", "getDesiredWidthInPixels", "text", "editText", "getFocusRequestOnTouch", "Landroid/view/View;", "touchX", "getFocusRequestOnTouch$stripe_release", "initView", "isEnabled", "onFinishInflate", "onInterceptTouchEvent", "ev", "Landroid/view/MotionEvent;", "onLayout", "changed", "l", "t", "r", "b", "onRestoreInstanceState", "state", "Landroid/os/Parcelable;", "onSaveInstanceState", "onWindowFocusChanged", "hasWindowFocus", "scrollLeft", "scrollRight", "setCardHint", "cardHint", "setCardInputListener", "listener", "setCardNumber", "setCardNumberTextWatcher", "cardNumberTextWatcher", "Landroid/text/TextWatcher;", "setCardValidCallback", "callback", "setCvcCode", "cvcCode", "setCvcNumberTextWatcher", "cvcNumberTextWatcher", "setEnabled", "setExpiryDate", "month", "year", "setExpiryDateTextWatcher", "expiryDateTextWatcher", "setPostalCode", "postalCode", "setPostalCode$stripe_release", "setPostalCodeTextWatcher", "postalCodeTextWatcher", "startSlideAnimation", "animations", "Landroid/view/animation/Animation;", "updateFieldLayout", "view", "width", "leftMargin", "updateIcon", "updateIconCvc", "hasFocus", "cvcText", "updateIconForCvcEntry", "updateSpaceSizes", "updateSpaceSizes$stripe_release", "AnimationEndListener", "CardFieldAnimation", "CardNumberSlideLeftAnimation", "CardNumberSlideRightAnimation", "Companion", "CvcSlideLeftAnimation", "CvcSlideRightAnimation", "DefaultLayoutWidthCalculator", "ExpiryDateSlideLeftAnimation", "ExpiryDateSlideRightAnimation", "LayoutWidthCalculator", "PlacementParameters", "PostalCodeSlideLeftAnimation", "PostalCodeSlideRightAnimation", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: CardInputWidget.kt */
public final class CardInputWidget extends LinearLayout implements CardWidget {
    static final /* synthetic */ KProperty[] $$delegatedProperties;
    private static final String CVC_PLACEHOLDER_AMEX = "2345";
    private static final String CVC_PLACEHOLDER_COMMON = "CVC";
    public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
    private static final int DEFAULT_READER_ID = R.id.stripe_default_reader_id;
    private static final String FULL_SIZING_CARD_TEXT = "4242 4242 4242 4242";
    private static final String FULL_SIZING_DATE_TEXT = "MM/MM";
    private static final String FULL_SIZING_POSTAL_CODE_TEXT = "1234567890";
    public static final String LOGGING_TOKEN = "CardInputView";
    private static final String PEEK_TEXT_AMEX = "34343";
    private static final String PEEK_TEXT_COMMON = "4242";
    private static final String PEEK_TEXT_DINERS_14 = "88";
    private static final String STATE_CARD_VIEWED = "state_card_viewed";
    private static final String STATE_POSTAL_CODE_ENABLED = "state_postal_code_enabled";
    private static final String STATE_SUPER_STATE = "state_super_state";
    private final List<StripeEditText> allFields;
    private final /* synthetic */ CardBrandView cardBrandView;
    /* access modifiers changed from: private */
    public CardInputListener cardInputListener;
    private final /* synthetic */ CardNumberEditText cardNumberEditText;
    private final TextInputLayout cardNumberTextInputLayout;
    /* access modifiers changed from: private */
    public CardValidCallback cardValidCallback;
    private final CardInputWidget$cardValidTextWatcher$1 cardValidTextWatcher;
    /* access modifiers changed from: private */
    public final FrameLayout containerLayout;
    private final /* synthetic */ CvcEditText cvcNumberEditText;
    private final TextInputLayout cvcNumberTextInputLayout;
    private final /* synthetic */ ExpiryDateEditText expiryDateEditText;
    private final TextInputLayout expiryDateTextInputLayout;
    private /* synthetic */ Function0<Integer> frameWidthSupplier;
    /* access modifiers changed from: private */
    public String hiddenCardText;
    private final CardInputWidget$inputChangeTextWatcher$1 inputChangeTextWatcher;
    private /* synthetic */ boolean isShowingFullCard;
    private boolean isViewInitialized;
    private /* synthetic */ LayoutWidthCalculator layoutWidthCalculator;
    private final PlacementParameters placementParameters;
    private final /* synthetic */ PostalCodeEditText postalCodeEditText;
    private final ReadWriteProperty postalCodeEnabled$delegate;
    private boolean postalCodeRequired;
    private final TextInputLayout postalCodeTextInputLayout;
    private final /* synthetic */ List<StripeEditText> requiredFields;
    /* access modifiers changed from: private */
    public boolean shouldShowErrorIcon;
    private final ReadWriteProperty usZipCodeRequired$delegate;
    private final CardInputWidgetBinding viewBinding;

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\b\"\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0016J\u0010\u0010\u0007\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0016¨\u0006\b"}, d2 = {"Lcom/stripe/android/view/CardInputWidget$AnimationEndListener;", "Landroid/view/animation/Animation$AnimationListener;", "()V", "onAnimationRepeat", "", "animation", "Landroid/view/animation/Animation;", "onAnimationStart", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: CardInputWidget.kt */
    private static abstract class AnimationEndListener implements Animation.AnimationListener {
        public void onAnimationRepeat(Animation animation) {
            Intrinsics.checkParameterIsNotNull(animation, "animation");
        }

        public void onAnimationStart(Animation animation) {
            Intrinsics.checkParameterIsNotNull(animation, "animation");
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\b`\u0018\u00002\u00020\u0001J\u0018\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0007H&¨\u0006\b"}, d2 = {"Lcom/stripe/android/view/CardInputWidget$LayoutWidthCalculator;", "", "calculate", "", "text", "", "paint", "Landroid/text/TextPaint;", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: CardInputWidget.kt */
    public interface LayoutWidthCalculator {
        int calculate(String str, TextPaint textPaint);
    }

    @Metadata(bv = {1, 0, 3}, k = 3, mv = {1, 1, 16})
    public final /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;

        static {
            int[] iArr = new int[CardBrand.values().length];
            $EnumSwitchMapping$0 = iArr;
            iArr[CardBrand.AmericanExpress.ordinal()] = 1;
            $EnumSwitchMapping$0[CardBrand.DinersClub.ordinal()] = 2;
        }
    }

    public CardInputWidget(Context context) {
        this(context, (AttributeSet) null, 0, 6, (DefaultConstructorMarker) null);
    }

    public CardInputWidget(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0, 4, (DefaultConstructorMarker) null);
    }

    public static /* synthetic */ void currentFields$annotations() {
    }

    public static /* synthetic */ void requiredFields$annotations() {
    }

    public static /* synthetic */ void shouldShowErrorIcon$annotations() {
    }

    public final boolean getPostalCodeEnabled() {
        return ((Boolean) this.postalCodeEnabled$delegate.getValue(this, $$delegatedProperties[0])).booleanValue();
    }

    public final boolean getUsZipCodeRequired() {
        return ((Boolean) this.usZipCodeRequired$delegate.getValue(this, $$delegatedProperties[1])).booleanValue();
    }

    public final void setPostalCodeEnabled(boolean z) {
        this.postalCodeEnabled$delegate.setValue(this, $$delegatedProperties[0], Boolean.valueOf(z));
    }

    public final void setUsZipCodeRequired(boolean z) {
        this.usZipCodeRequired$delegate.setValue(this, $$delegatedProperties[1], Boolean.valueOf(z));
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ CardInputWidget(Context context, AttributeSet attributeSet, int i, int i2, DefaultConstructorMarker defaultConstructorMarker) {
        this(context, (i2 & 2) != 0 ? null : attributeSet, (i2 & 4) != 0 ? 0 : i);
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CardInputWidget(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        Intrinsics.checkParameterIsNotNull(context, "context");
        CardInputWidgetBinding inflate = CardInputWidgetBinding.inflate(LayoutInflater.from(context), this);
        Intrinsics.checkExpressionValueIsNotNull(inflate, "CardInputWidgetBinding.i…text),\n        this\n    )");
        this.viewBinding = inflate;
        FrameLayout frameLayout = inflate.container;
        Intrinsics.checkExpressionValueIsNotNull(frameLayout, "viewBinding.container");
        this.containerLayout = frameLayout;
        CardBrandView cardBrandView2 = this.viewBinding.cardBrandView;
        Intrinsics.checkExpressionValueIsNotNull(cardBrandView2, "viewBinding.cardBrandView");
        this.cardBrandView = cardBrandView2;
        TextInputLayout textInputLayout = this.viewBinding.cardNumberTextInputLayout;
        Intrinsics.checkExpressionValueIsNotNull(textInputLayout, "viewBinding.cardNumberTextInputLayout");
        this.cardNumberTextInputLayout = textInputLayout;
        TextInputLayout textInputLayout2 = this.viewBinding.expiryDateTextInputLayout;
        Intrinsics.checkExpressionValueIsNotNull(textInputLayout2, "viewBinding.expiryDateTextInputLayout");
        this.expiryDateTextInputLayout = textInputLayout2;
        TextInputLayout textInputLayout3 = this.viewBinding.cvcTextInputLayout;
        Intrinsics.checkExpressionValueIsNotNull(textInputLayout3, "viewBinding.cvcTextInputLayout");
        this.cvcNumberTextInputLayout = textInputLayout3;
        TextInputLayout textInputLayout4 = this.viewBinding.postalCodeTextInputLayout;
        Intrinsics.checkExpressionValueIsNotNull(textInputLayout4, "viewBinding.postalCodeTextInputLayout");
        this.postalCodeTextInputLayout = textInputLayout4;
        CardNumberEditText cardNumberEditText2 = this.viewBinding.cardNumberEditText;
        Intrinsics.checkExpressionValueIsNotNull(cardNumberEditText2, "viewBinding.cardNumberEditText");
        this.cardNumberEditText = cardNumberEditText2;
        ExpiryDateEditText expiryDateEditText2 = this.viewBinding.expiryDateEditText;
        Intrinsics.checkExpressionValueIsNotNull(expiryDateEditText2, "viewBinding.expiryDateEditText");
        this.expiryDateEditText = expiryDateEditText2;
        CvcEditText cvcEditText = this.viewBinding.cvcEditText;
        Intrinsics.checkExpressionValueIsNotNull(cvcEditText, "viewBinding.cvcEditText");
        this.cvcNumberEditText = cvcEditText;
        PostalCodeEditText postalCodeEditText2 = this.viewBinding.postalCodeEditText;
        Intrinsics.checkExpressionValueIsNotNull(postalCodeEditText2, "viewBinding.postalCodeEditText");
        this.postalCodeEditText = postalCodeEditText2;
        this.cardValidTextWatcher = new CardInputWidget$cardValidTextWatcher$1(this);
        this.inputChangeTextWatcher = new CardInputWidget$inputChangeTextWatcher$1(this);
        this.isShowingFullCard = true;
        this.layoutWidthCalculator = new DefaultLayoutWidthCalculator();
        this.placementParameters = new PlacementParameters();
        Delegates delegates = Delegates.INSTANCE;
        this.postalCodeEnabled$delegate = new CardInputWidget$$special$$inlined$observable$1(true, true, this);
        Delegates delegates2 = Delegates.INSTANCE;
        this.usZipCodeRequired$delegate = new CardInputWidget$$special$$inlined$observable$2(false, false, this);
        if (getId() == -1) {
            setId(DEFAULT_READER_ID);
        }
        setOrientation(0);
        setMinimumWidth(getResources().getDimensionPixelSize(R.dimen.stripe_card_widget_min_width));
        this.frameWidthSupplier = new Function0<Integer>(this) {
            final /* synthetic */ CardInputWidget this$0;

            {
                this.this$0 = r1;
            }

            public final int invoke() {
                return this.this$0.containerLayout.getWidth();
            }
        };
        List<StripeEditText> listOf = CollectionsKt.listOf(this.cardNumberEditText, this.cvcNumberEditText, this.expiryDateEditText);
        this.requiredFields = listOf;
        this.allFields = CollectionsKt.plus(listOf, this.postalCodeEditText);
        initView(attributeSet);
        this.hiddenCardText = createHiddenCardText$stripe_release$default(this, getBrand(), (String) null, 2, (Object) null);
    }

    public final CardBrandView getCardBrandView$stripe_release() {
        return this.cardBrandView;
    }

    public final TextInputLayout getPostalCodeTextInputLayout$stripe_release() {
        return this.postalCodeTextInputLayout;
    }

    public final CardNumberEditText getCardNumberEditText$stripe_release() {
        return this.cardNumberEditText;
    }

    public final ExpiryDateEditText getExpiryDateEditText$stripe_release() {
        return this.expiryDateEditText;
    }

    public final CvcEditText getCvcNumberEditText$stripe_release() {
        return this.cvcNumberEditText;
    }

    public final PostalCodeEditText getPostalCodeEditText$stripe_release() {
        return this.postalCodeEditText;
    }

    /* access modifiers changed from: private */
    public final Set<CardValidCallback.Fields> getInvalidFields() {
        CardValidCallback.Fields[] fieldsArr = new CardValidCallback.Fields[3];
        CardValidCallback.Fields fields = CardValidCallback.Fields.Number;
        boolean z = true;
        CardValidCallback.Fields fields2 = null;
        if (!(this.cardNumberEditText.getCardNumber() == null)) {
            fields = null;
        }
        fieldsArr[0] = fields;
        CardValidCallback.Fields fields3 = CardValidCallback.Fields.Expiry;
        if (!(this.expiryDateEditText.getValidDateFields() == null)) {
            fields3 = null;
        }
        fieldsArr[1] = fields3;
        CardValidCallback.Fields fields4 = CardValidCallback.Fields.Cvc;
        if (getCvcValue() != null) {
            z = false;
        }
        if (z) {
            fields2 = fields4;
        }
        fieldsArr[2] = fields2;
        return CollectionsKt.toSet(CollectionsKt.listOfNotNull((T[]) fieldsArr));
    }

    public final boolean getShouldShowErrorIcon$stripe_release() {
        return this.shouldShowErrorIcon;
    }

    /* access modifiers changed from: private */
    public final void setShouldShowErrorIcon(boolean z) {
        boolean z2 = this.shouldShowErrorIcon != z;
        this.shouldShowErrorIcon = z;
        if (z2) {
            updateIcon();
        }
    }

    public final boolean isShowingFullCard$stripe_release() {
        return this.isShowingFullCard;
    }

    public final void setShowingFullCard$stripe_release(boolean z) {
        this.isShowingFullCard = z;
    }

    public final LayoutWidthCalculator getLayoutWidthCalculator$stripe_release() {
        return this.layoutWidthCalculator;
    }

    public final void setLayoutWidthCalculator$stripe_release(LayoutWidthCalculator layoutWidthCalculator2) {
        Intrinsics.checkParameterIsNotNull(layoutWidthCalculator2, "<set-?>");
        this.layoutWidthCalculator = layoutWidthCalculator2;
    }

    public final PlacementParameters getPlacementParameters$stripe_release() {
        return this.placementParameters;
    }

    private final String getPostalCodeValue() {
        if (getPostalCodeEnabled()) {
            return this.postalCodeEditText.getPostalCode$stripe_release();
        }
        return null;
    }

    /* access modifiers changed from: private */
    public final String getCvcValue() {
        return this.cvcNumberEditText.getCvcValue();
    }

    /* access modifiers changed from: private */
    public final CardBrand getBrand() {
        return this.cardNumberEditText.getCardBrand();
    }

    public final List<StripeEditText> getRequiredFields$stripe_release() {
        return this.requiredFields;
    }

    public final /* synthetic */ List<StripeEditText> getCurrentFields$stripe_release() {
        Collection collection = this.requiredFields;
        PostalCodeEditText postalCodeEditText2 = this.postalCodeEditText;
        if (!getPostalCodeEnabled()) {
            postalCodeEditText2 = null;
        }
        return CollectionsKt.filterNotNull(CollectionsKt.plus(collection, postalCodeEditText2));
    }

    public PaymentMethodCreateParams.Card getPaymentMethodCard() {
        Card card = getCard();
        if (card == null) {
            return null;
        }
        return new PaymentMethodCreateParams.Card(card.getNumber(), card.getExpMonth(), card.getExpYear(), card.getCvc(), (String) null, card.getAttribution$stripe_release(), 16, (DefaultConstructorMarker) null);
    }

    private final PaymentMethod.BillingDetails getBillingDetails() {
        String postalCodeValue = getPostalCodeValue();
        if (postalCodeValue != null) {
            return new PaymentMethod.BillingDetails(new Address((String) null, (String) null, (String) null, (String) null, postalCodeValue, (String) null, 47, (DefaultConstructorMarker) null), (String) null, (String) null, (String) null, 14, (DefaultConstructorMarker) null);
        }
        return null;
    }

    public PaymentMethodCreateParams getPaymentMethodCreateParams() {
        PaymentMethodCreateParams.Card paymentMethodCard = getPaymentMethodCard();
        if (paymentMethodCard != null) {
            return PaymentMethodCreateParams.Companion.create$default(PaymentMethodCreateParams.Companion, paymentMethodCard, getBillingDetails(), (Map) null, 4, (Object) null);
        }
        return null;
    }

    public Card getCard() {
        Card.Builder cardBuilder = getCardBuilder();
        if (cardBuilder != null) {
            return cardBuilder.build();
        }
        return null;
    }

    /* JADX WARNING: Removed duplicated region for block: B:28:0x006f  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x008e  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00a2  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x00a8  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.stripe.android.model.Card.Builder getCardBuilder() {
        /*
            r9 = this;
            com.stripe.android.view.CardNumberEditText r0 = r9.cardNumberEditText
            java.lang.String r0 = r0.getCardNumber()
            com.stripe.android.view.ExpiryDateEditText r1 = r9.expiryDateEditText
            kotlin.Pair r1 = r1.getValidDateFields()
            java.lang.String r2 = r9.getCvcValue()
            com.stripe.android.view.CardNumberEditText r3 = r9.cardNumberEditText
            r4 = 1
            r5 = 0
            if (r0 != 0) goto L_0x0018
            r6 = 1
            goto L_0x0019
        L_0x0018:
            r6 = 0
        L_0x0019:
            r3.setShouldShowError(r6)
            com.stripe.android.view.ExpiryDateEditText r3 = r9.expiryDateEditText
            if (r1 != 0) goto L_0x0022
            r6 = 1
            goto L_0x0023
        L_0x0022:
            r6 = 0
        L_0x0023:
            r3.setShouldShowError(r6)
            com.stripe.android.view.CvcEditText r3 = r9.cvcNumberEditText
            if (r2 != 0) goto L_0x002c
            r6 = 1
            goto L_0x002d
        L_0x002c:
            r6 = 0
        L_0x002d:
            r3.setShouldShowError(r6)
            com.stripe.android.view.PostalCodeEditText r3 = r9.postalCodeEditText
            boolean r6 = r9.postalCodeRequired
            if (r6 != 0) goto L_0x003c
            boolean r6 = r9.getUsZipCodeRequired()
            if (r6 == 0) goto L_0x0054
        L_0x003c:
            com.stripe.android.view.PostalCodeEditText r6 = r9.postalCodeEditText
            java.lang.String r6 = r6.getPostalCode$stripe_release()
            java.lang.CharSequence r6 = (java.lang.CharSequence) r6
            if (r6 == 0) goto L_0x004f
            boolean r6 = kotlin.text.StringsKt.isBlank(r6)
            if (r6 == 0) goto L_0x004d
            goto L_0x004f
        L_0x004d:
            r6 = 0
            goto L_0x0050
        L_0x004f:
            r6 = 1
        L_0x0050:
            if (r6 == 0) goto L_0x0054
            r6 = 1
            goto L_0x0055
        L_0x0054:
            r6 = 0
        L_0x0055:
            r3.setShouldShowError(r6)
            java.util.List r3 = r9.getCurrentFields$stripe_release()
            java.lang.Iterable r3 = (java.lang.Iterable) r3
            java.util.ArrayList r6 = new java.util.ArrayList
            r6.<init>()
            java.util.Collection r6 = (java.util.Collection) r6
            java.util.Iterator r3 = r3.iterator()
        L_0x0069:
            boolean r7 = r3.hasNext()
            if (r7 == 0) goto L_0x0080
            java.lang.Object r7 = r3.next()
            r8 = r7
            com.stripe.android.view.StripeEditText r8 = (com.stripe.android.view.StripeEditText) r8
            boolean r8 = r8.getShouldShowError()
            if (r8 == 0) goto L_0x0069
            r6.add(r7)
            goto L_0x0069
        L_0x0080:
            java.util.List r6 = (java.util.List) r6
            java.lang.Iterable r6 = (java.lang.Iterable) r6
            java.util.Iterator r3 = r6.iterator()
        L_0x0088:
            boolean r6 = r3.hasNext()
            if (r6 == 0) goto L_0x00a0
            java.lang.Object r6 = r3.next()
            com.stripe.android.view.StripeEditText r6 = (com.stripe.android.view.StripeEditText) r6
            java.lang.String r7 = r6.getErrorMessage$stripe_release()
            if (r7 == 0) goto L_0x0088
            java.lang.CharSequence r7 = (java.lang.CharSequence) r7
            r6.announceForAccessibility(r7)
            goto L_0x0088
        L_0x00a0:
            if (r0 != 0) goto L_0x00a8
            com.stripe.android.view.CardNumberEditText r0 = r9.cardNumberEditText
            r0.requestFocus()
            goto L_0x00c5
        L_0x00a8:
            if (r1 != 0) goto L_0x00b0
            com.stripe.android.view.ExpiryDateEditText r0 = r9.expiryDateEditText
            r0.requestFocus()
            goto L_0x00c5
        L_0x00b0:
            if (r2 != 0) goto L_0x00b8
            com.stripe.android.view.CvcEditText r0 = r9.cvcNumberEditText
            r0.requestFocus()
            goto L_0x00c5
        L_0x00b8:
            com.stripe.android.view.PostalCodeEditText r3 = r9.postalCodeEditText
            boolean r3 = r3.getShouldShowError()
            if (r3 == 0) goto L_0x00ca
            com.stripe.android.view.PostalCodeEditText r0 = r9.postalCodeEditText
            r0.requestFocus()
        L_0x00c5:
            r9.setShouldShowErrorIcon(r4)
            r0 = 0
            return r0
        L_0x00ca:
            r9.setShouldShowErrorIcon(r5)
            com.stripe.android.model.Card$Builder r3 = new com.stripe.android.model.Card$Builder
            java.lang.Object r4 = r1.getFirst()
            java.lang.Integer r4 = (java.lang.Integer) r4
            java.lang.Object r1 = r1.getSecond()
            java.lang.Integer r1 = (java.lang.Integer) r1
            r3.<init>(r0, r4, r1, r2)
            java.lang.String r0 = r9.getPostalCodeValue()
            com.stripe.android.model.Card$Builder r0 = r3.addressZip(r0)
            java.lang.String r1 = "CardInputView"
            java.util.Set r1 = kotlin.collections.SetsKt.setOf(r1)
            com.stripe.android.model.Card$Builder r0 = r0.loggingTokens(r1)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.stripe.android.view.CardInputWidget.getCardBuilder():com.stripe.android.model.Card$Builder");
    }

    private final int getFrameWidth() {
        return this.frameWidthSupplier.invoke().intValue();
    }

    public final Function0<Integer> getFrameWidthSupplier$stripe_release() {
        return this.frameWidthSupplier;
    }

    public final void setFrameWidthSupplier$stripe_release(Function0<Integer> function0) {
        Intrinsics.checkParameterIsNotNull(function0, "<set-?>");
        this.frameWidthSupplier = function0;
    }

    public final boolean getPostalCodeRequired() {
        return this.postalCodeRequired;
    }

    public final void setPostalCodeRequired(boolean z) {
        this.postalCodeRequired = z;
    }

    /* access modifiers changed from: protected */
    public void onFinishInflate() {
        super.onFinishInflate();
        this.postalCodeEditText.setConfig$stripe_release(PostalCodeEditText.Config.Global);
    }

    public void setCardValidCallback(CardValidCallback cardValidCallback2) {
        this.cardValidCallback = cardValidCallback2;
        for (StripeEditText removeTextChangedListener : this.requiredFields) {
            removeTextChangedListener.removeTextChangedListener(this.cardValidTextWatcher);
        }
        if (cardValidCallback2 != null) {
            for (StripeEditText addTextChangedListener : this.requiredFields) {
                addTextChangedListener.addTextChangedListener(this.cardValidTextWatcher);
            }
        }
        CardValidCallback cardValidCallback3 = this.cardValidCallback;
        if (cardValidCallback3 != null) {
            cardValidCallback3.onInputChanged(getInvalidFields().isEmpty(), getInvalidFields());
        }
    }

    public void setCardInputListener(CardInputListener cardInputListener2) {
        this.cardInputListener = cardInputListener2;
    }

    public void setCardNumber(String str) {
        this.cardNumberEditText.setText(str);
        this.isShowingFullCard = !this.cardNumberEditText.isCardNumberValid();
    }

    public void setCardHint(String str) {
        Intrinsics.checkParameterIsNotNull(str, "cardHint");
        this.cardNumberEditText.setHint(str);
    }

    public void setExpiryDate(int i, int i2) {
        this.expiryDateEditText.setText(DateUtils.createDateStringFromIntegerInput(i, i2));
    }

    public void setCvcCode(String str) {
        this.cvcNumberEditText.setText(str);
    }

    public final /* synthetic */ void setPostalCode$stripe_release(String str) {
        this.postalCodeEditText.setText(str);
    }

    public void clear() {
        Iterable currentFields$stripe_release = getCurrentFields$stripe_release();
        boolean z = false;
        if (!(currentFields$stripe_release instanceof Collection) || !((Collection) currentFields$stripe_release).isEmpty()) {
            Iterator it = currentFields$stripe_release.iterator();
            while (true) {
                if (it.hasNext()) {
                    if (((StripeEditText) it.next()).hasFocus()) {
                        z = true;
                        break;
                    }
                } else {
                    break;
                }
            }
        }
        if (z || hasFocus()) {
            this.cardNumberEditText.requestFocus();
        }
        for (StripeEditText text : getCurrentFields$stripe_release()) {
            text.setText("");
        }
    }

    public void setEnabled(boolean z) {
        for (StripeEditText enabled : getCurrentFields$stripe_release()) {
            enabled.setEnabled(z);
        }
    }

    public void setCardNumberTextWatcher(TextWatcher textWatcher) {
        this.cardNumberEditText.addTextChangedListener(textWatcher);
    }

    public void setExpiryDateTextWatcher(TextWatcher textWatcher) {
        this.expiryDateEditText.addTextChangedListener(textWatcher);
    }

    public void setCvcNumberTextWatcher(TextWatcher textWatcher) {
        this.cvcNumberEditText.addTextChangedListener(textWatcher);
    }

    public void setPostalCodeTextWatcher(TextWatcher textWatcher) {
        this.postalCodeEditText.addTextChangedListener(textWatcher);
    }

    public boolean isEnabled() {
        Iterable<StripeEditText> iterable = this.requiredFields;
        if ((iterable instanceof Collection) && ((Collection) iterable).isEmpty()) {
            return true;
        }
        for (StripeEditText isEnabled : iterable) {
            if (!isEnabled.isEnabled()) {
                return false;
            }
        }
        return true;
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        Intrinsics.checkParameterIsNotNull(motionEvent, "ev");
        if (motionEvent.getAction() != 0) {
            return super.onInterceptTouchEvent(motionEvent);
        }
        View focusRequestOnTouch$stripe_release = getFocusRequestOnTouch$stripe_release((int) motionEvent.getX());
        if (focusRequestOnTouch$stripe_release == null) {
            return super.onInterceptTouchEvent(motionEvent);
        }
        focusRequestOnTouch$stripe_release.requestFocus();
        return true;
    }

    /* access modifiers changed from: protected */
    public Parcelable onSaveInstanceState() {
        Bundle bundle = new Bundle();
        bundle.putParcelable(STATE_SUPER_STATE, super.onSaveInstanceState());
        bundle.putBoolean(STATE_CARD_VIEWED, this.isShowingFullCard);
        bundle.putBoolean(STATE_POSTAL_CODE_ENABLED, getPostalCodeEnabled());
        return bundle;
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Parcelable parcelable) {
        int i;
        int i2;
        int i3;
        int i4;
        Intrinsics.checkParameterIsNotNull(parcelable, "state");
        if (parcelable instanceof Bundle) {
            Bundle bundle = (Bundle) parcelable;
            setPostalCodeEnabled(bundle.getBoolean(STATE_POSTAL_CODE_ENABLED, true));
            boolean z = bundle.getBoolean(STATE_CARD_VIEWED, true);
            this.isShowingFullCard = z;
            updateSpaceSizes$stripe_release(z);
            this.placementParameters.setTotalLengthInPixels$stripe_release(getFrameWidth());
            int i5 = 0;
            if (this.isShowingFullCard) {
                i2 = this.placementParameters.getDateLeftMargin$stripe_release(true);
                i = this.placementParameters.getCvcLeftMargin$stripe_release(true);
                i3 = this.placementParameters.getPostalCodeLeftMargin$stripe_release(true);
            } else {
                int hiddenCardWidth$stripe_release = this.placementParameters.getHiddenCardWidth$stripe_release() * -1;
                i2 = this.placementParameters.getDateLeftMargin$stripe_release(false);
                i = this.placementParameters.getCvcLeftMargin$stripe_release(false);
                if (getPostalCodeEnabled()) {
                    i4 = this.placementParameters.getPostalCodeLeftMargin$stripe_release(false);
                } else {
                    i4 = this.placementParameters.getTotalLengthInPixels$stripe_release();
                }
                int i6 = i4;
                i5 = hiddenCardWidth$stripe_release;
                i3 = i6;
            }
            updateFieldLayout(this.cardNumberTextInputLayout, this.placementParameters.getCardWidth$stripe_release(), i5);
            updateFieldLayout(this.expiryDateTextInputLayout, this.placementParameters.getDateWidth$stripe_release(), i2);
            updateFieldLayout(this.cvcNumberTextInputLayout, this.placementParameters.getCvcWidth$stripe_release(), i);
            updateFieldLayout(this.postalCodeTextInputLayout, this.placementParameters.getPostalCodeWidth$stripe_release(), i3);
            super.onRestoreInstanceState(bundle.getParcelable(STATE_SUPER_STATE));
            return;
        }
        super.onRestoreInstanceState(parcelable);
    }

    public final View getFocusRequestOnTouch$stripe_release(int i) {
        int left = this.containerLayout.getLeft();
        StripeEditText stripeEditText = null;
        if (this.isShowingFullCard) {
            if (i >= left + this.placementParameters.getCardWidth$stripe_release()) {
                if (i < this.placementParameters.getCardTouchBufferLimit$stripe_release()) {
                    stripeEditText = this.cardNumberEditText;
                } else if (i < this.placementParameters.getDateStartPosition$stripe_release()) {
                    stripeEditText = this.expiryDateEditText;
                }
            }
            return stripeEditText;
        } else if (getPostalCodeEnabled()) {
            if (i >= left + this.placementParameters.getPeekCardWidth$stripe_release()) {
                if (i < this.placementParameters.getCardTouchBufferLimit$stripe_release()) {
                    stripeEditText = this.cardNumberEditText;
                } else if (i < this.placementParameters.getDateStartPosition$stripe_release()) {
                    stripeEditText = this.expiryDateEditText;
                } else if (i >= this.placementParameters.getDateStartPosition$stripe_release() + this.placementParameters.getDateWidth$stripe_release()) {
                    if (i < this.placementParameters.getDateRightTouchBufferLimit$stripe_release()) {
                        stripeEditText = this.expiryDateEditText;
                    } else if (i < this.placementParameters.getCvcStartPosition$stripe_release()) {
                        stripeEditText = this.cvcNumberEditText;
                    } else if (i >= this.placementParameters.getCvcStartPosition$stripe_release() + this.placementParameters.getCvcWidth$stripe_release()) {
                        if (i < this.placementParameters.getCvcRightTouchBufferLimit$stripe_release()) {
                            stripeEditText = this.cvcNumberEditText;
                        } else if (i < this.placementParameters.getPostalCodeStartPosition$stripe_release()) {
                            stripeEditText = this.postalCodeEditText;
                        }
                    }
                }
            }
            return stripeEditText;
        } else {
            if (i >= left + this.placementParameters.getPeekCardWidth$stripe_release()) {
                if (i < this.placementParameters.getCardTouchBufferLimit$stripe_release()) {
                    stripeEditText = this.cardNumberEditText;
                } else if (i < this.placementParameters.getDateStartPosition$stripe_release()) {
                    stripeEditText = this.expiryDateEditText;
                } else if (i >= this.placementParameters.getDateStartPosition$stripe_release() + this.placementParameters.getDateWidth$stripe_release()) {
                    if (i < this.placementParameters.getDateRightTouchBufferLimit$stripe_release()) {
                        stripeEditText = this.expiryDateEditText;
                    } else if (i < this.placementParameters.getCvcStartPosition$stripe_release()) {
                        stripeEditText = this.cvcNumberEditText;
                    }
                }
            }
            return stripeEditText;
        }
    }

    public final void updateSpaceSizes$stripe_release(boolean z) {
        int frameWidth = getFrameWidth();
        int left = this.containerLayout.getLeft();
        if (frameWidth != 0) {
            this.placementParameters.setCardWidth$stripe_release(getDesiredWidthInPixels(FULL_SIZING_CARD_TEXT, this.cardNumberEditText));
            this.placementParameters.setDateWidth$stripe_release(getDesiredWidthInPixels(FULL_SIZING_DATE_TEXT, this.expiryDateEditText));
            this.placementParameters.setHiddenCardWidth$stripe_release(getDesiredWidthInPixels(this.hiddenCardText, this.cardNumberEditText));
            this.placementParameters.setCvcWidth$stripe_release(getDesiredWidthInPixels(getCvcPlaceHolder(), this.cvcNumberEditText));
            this.placementParameters.setPostalCodeWidth$stripe_release(getDesiredWidthInPixels(FULL_SIZING_POSTAL_CODE_TEXT, this.postalCodeEditText));
            this.placementParameters.setPeekCardWidth$stripe_release(getDesiredWidthInPixels(getPeekCardText(), this.cardNumberEditText));
            this.placementParameters.updateSpacing$stripe_release(z, getPostalCodeEnabled(), left, frameWidth);
        }
    }

    private final void updateFieldLayout(View view, int i, int i2) {
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        if (layoutParams != null) {
            FrameLayout.LayoutParams layoutParams2 = (FrameLayout.LayoutParams) layoutParams;
            layoutParams2.width = i;
            layoutParams2.leftMargin = i2;
            view.setLayoutParams(layoutParams2);
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type android.widget.FrameLayout.LayoutParams");
    }

    private final int getDesiredWidthInPixels(String str, StripeEditText stripeEditText) {
        LayoutWidthCalculator layoutWidthCalculator2 = this.layoutWidthCalculator;
        TextPaint paint = stripeEditText.getPaint();
        Intrinsics.checkExpressionValueIsNotNull(paint, "editText.paint");
        return layoutWidthCalculator2.calculate(str, paint);
    }

    private final void initView(AttributeSet attributeSet) {
        if (attributeSet != null) {
            applyAttributes(attributeSet);
        }
        ViewCompat.setAccessibilityDelegate(this.cardNumberEditText, new CardInputWidget$initView$2());
        boolean z = true;
        this.isShowingFullCard = true;
        int defaultErrorColorInt = this.cardNumberEditText.getDefaultErrorColorInt();
        CardBrandView cardBrandView2 = this.cardBrandView;
        ColorStateList hintTextColors = this.cardNumberEditText.getHintTextColors();
        Intrinsics.checkExpressionValueIsNotNull(hintTextColors, "cardNumberEditText.hintTextColors");
        cardBrandView2.setTintColorInt$stripe_release(hintTextColors.getDefaultColor());
        String str = null;
        if (attributeSet != null) {
            Context context = getContext();
            Intrinsics.checkExpressionValueIsNotNull(context, "context");
            TypedArray obtainStyledAttributes = context.getTheme().obtainStyledAttributes(attributeSet, R.styleable.CardInputView, 0, 0);
            try {
                this.cardBrandView.setTintColorInt$stripe_release(obtainStyledAttributes.getColor(R.styleable.CardInputView_cardTint, this.cardBrandView.getTintColorInt$stripe_release()));
                defaultErrorColorInt = obtainStyledAttributes.getColor(R.styleable.CardInputView_cardTextErrorColor, defaultErrorColorInt);
                str = obtainStyledAttributes.getString(R.styleable.CardInputView_cardHintText);
                z = obtainStyledAttributes.getBoolean(R.styleable.CardInputView_android_focusedByDefault, true);
            } finally {
                obtainStyledAttributes.recycle();
            }
        }
        if (str != null) {
            this.cardNumberEditText.setHint(str);
        }
        for (StripeEditText errorColor : getCurrentFields$stripe_release()) {
            errorColor.setErrorColor(defaultErrorColorInt);
        }
        this.cardNumberEditText.setOnFocusChangeListener(new CardInputWidget$initView$5(this));
        this.expiryDateEditText.setOnFocusChangeListener(new CardInputWidget$initView$6(this));
        this.expiryDateEditText.setDeleteEmptyListener(new BackUpFieldDeleteListener(this.cardNumberEditText));
        this.cvcNumberEditText.setDeleteEmptyListener(new BackUpFieldDeleteListener(this.expiryDateEditText));
        this.postalCodeEditText.setDeleteEmptyListener(new BackUpFieldDeleteListener(this.cvcNumberEditText));
        this.cvcNumberEditText.setOnFocusChangeListener(new CardInputWidget$initView$7(this));
        this.cvcNumberEditText.setAfterTextChangedListener(new CardInputWidget$initView$8(this));
        this.cardNumberEditText.setCompletionCallback$stripe_release(new CardInputWidget$initView$9(this));
        this.cardNumberEditText.setBrandChangeCallback$stripe_release(new CardInputWidget$initView$10(this));
        this.expiryDateEditText.setCompletionCallback$stripe_release(new CardInputWidget$initView$11(this));
        this.cvcNumberEditText.setCompletionCallback$stripe_release(new CardInputWidget$initView$12(this));
        for (StripeEditText addTextChangedListener : this.allFields) {
            addTextChangedListener.addTextChangedListener(this.inputChangeTextWatcher);
        }
        if (z) {
            this.cardNumberEditText.requestFocus();
        }
    }

    public static /* synthetic */ String createHiddenCardText$stripe_release$default(CardInputWidget cardInputWidget, CardBrand cardBrand, String str, int i, Object obj) {
        if ((i & 2) != 0) {
            str = cardInputWidget.cardNumberEditText.getFieldText$stripe_release();
        }
        return cardInputWidget.createHiddenCardText$stripe_release(cardBrand, str);
    }

    public final String createHiddenCardText$stripe_release(CardBrand cardBrand, String str) {
        Intrinsics.checkParameterIsNotNull(cardBrand, "brand");
        Intrinsics.checkParameterIsNotNull(str, "cardNumber");
        List arrayList = new ArrayList();
        int i = 0;
        for (Number intValue : CollectionsKt.sorted(CollectionsKt.toList(cardBrand.getSpacePositionsForCardNumber(str)))) {
            int intValue2 = intValue.intValue();
            int i2 = intValue2 - i;
            for (int i3 = 0; i3 < i2; i3++) {
                arrayList.add(AppEventsConstants.EVENT_PARAM_VALUE_NO);
            }
            arrayList.add(" ");
            i = intValue2 + 1;
        }
        return CollectionsKt.joinToString$default(arrayList, "", (CharSequence) null, (CharSequence) null, 0, (CharSequence) null, (Function1) null, 62, (Object) null);
    }

    private final void applyAttributes(AttributeSet attributeSet) {
        Context context = getContext();
        Intrinsics.checkExpressionValueIsNotNull(context, "context");
        TypedArray obtainStyledAttributes = context.getTheme().obtainStyledAttributes(attributeSet, R.styleable.CardElement, 0, 0);
        try {
            setPostalCodeEnabled(obtainStyledAttributes.getBoolean(R.styleable.CardElement_shouldShowPostalCode, true));
            this.postalCodeRequired = obtainStyledAttributes.getBoolean(R.styleable.CardElement_shouldRequirePostalCode, false);
            setUsZipCodeRequired(obtainStyledAttributes.getBoolean(R.styleable.CardElement_shouldRequireUsZipCode, false));
        } finally {
            obtainStyledAttributes.recycle();
        }
    }

    /* access modifiers changed from: private */
    public final void scrollLeft() {
        PostalCodeSlideLeftAnimation postalCodeSlideLeftAnimation;
        if (!this.isShowingFullCard && this.isViewInitialized) {
            int dateLeftMargin$stripe_release = this.placementParameters.getDateLeftMargin$stripe_release(false);
            int cvcLeftMargin$stripe_release = this.placementParameters.getCvcLeftMargin$stripe_release(false);
            int postalCodeLeftMargin$stripe_release = this.placementParameters.getPostalCodeLeftMargin$stripe_release(false);
            updateSpaceSizes$stripe_release(true);
            CardNumberSlideLeftAnimation cardNumberSlideLeftAnimation = new CardNumberSlideLeftAnimation(this.cardNumberTextInputLayout);
            int dateLeftMargin$stripe_release2 = this.placementParameters.getDateLeftMargin$stripe_release(true);
            ExpiryDateSlideLeftAnimation expiryDateSlideLeftAnimation = new ExpiryDateSlideLeftAnimation(this.expiryDateTextInputLayout, dateLeftMargin$stripe_release, dateLeftMargin$stripe_release2);
            int i = (dateLeftMargin$stripe_release2 - dateLeftMargin$stripe_release) + cvcLeftMargin$stripe_release;
            CvcSlideLeftAnimation cvcSlideLeftAnimation = new CvcSlideLeftAnimation(this.cvcNumberTextInputLayout, cvcLeftMargin$stripe_release, i, this.placementParameters.getCvcWidth$stripe_release());
            int i2 = (i - cvcLeftMargin$stripe_release) + postalCodeLeftMargin$stripe_release;
            if (getPostalCodeEnabled()) {
                postalCodeSlideLeftAnimation = new PostalCodeSlideLeftAnimation(this.postalCodeTextInputLayout, postalCodeLeftMargin$stripe_release, i2, this.placementParameters.getPostalCodeWidth$stripe_release());
            } else {
                postalCodeSlideLeftAnimation = null;
            }
            startSlideAnimation(CollectionsKt.listOfNotNull((T[]) new CardFieldAnimation[]{cardNumberSlideLeftAnimation, expiryDateSlideLeftAnimation, cvcSlideLeftAnimation, postalCodeSlideLeftAnimation}));
            this.isShowingFullCard = true;
        }
    }

    /* access modifiers changed from: private */
    public final void scrollRight() {
        PostalCodeSlideRightAnimation postalCodeSlideRightAnimation;
        if (this.isShowingFullCard && this.isViewInitialized) {
            int dateLeftMargin$stripe_release = this.placementParameters.getDateLeftMargin$stripe_release(true);
            updateSpaceSizes$stripe_release(false);
            CardNumberSlideRightAnimation cardNumberSlideRightAnimation = new CardNumberSlideRightAnimation(this.cardNumberTextInputLayout, this.placementParameters.getHiddenCardWidth$stripe_release(), this.expiryDateEditText);
            int dateLeftMargin$stripe_release2 = this.placementParameters.getDateLeftMargin$stripe_release(false);
            ExpiryDateSlideRightAnimation expiryDateSlideRightAnimation = new ExpiryDateSlideRightAnimation(this.expiryDateTextInputLayout, dateLeftMargin$stripe_release, dateLeftMargin$stripe_release2);
            int cvcLeftMargin$stripe_release = this.placementParameters.getCvcLeftMargin$stripe_release(false);
            int i = (dateLeftMargin$stripe_release - dateLeftMargin$stripe_release2) + cvcLeftMargin$stripe_release;
            CvcSlideRightAnimation cvcSlideRightAnimation = new CvcSlideRightAnimation(this.cvcNumberTextInputLayout, i, cvcLeftMargin$stripe_release, this.placementParameters.getCvcWidth$stripe_release());
            int postalCodeLeftMargin$stripe_release = this.placementParameters.getPostalCodeLeftMargin$stripe_release(false);
            int i2 = (i - cvcLeftMargin$stripe_release) + postalCodeLeftMargin$stripe_release;
            if (getPostalCodeEnabled()) {
                postalCodeSlideRightAnimation = new PostalCodeSlideRightAnimation(this.postalCodeTextInputLayout, i2, postalCodeLeftMargin$stripe_release, this.placementParameters.getPostalCodeWidth$stripe_release());
            } else {
                postalCodeSlideRightAnimation = null;
            }
            startSlideAnimation(CollectionsKt.listOfNotNull((T[]) new CardFieldAnimation[]{cardNumberSlideRightAnimation, expiryDateSlideRightAnimation, cvcSlideRightAnimation, postalCodeSlideRightAnimation}));
            this.isShowingFullCard = false;
        }
    }

    private final void startSlideAnimation(List<? extends Animation> list) {
        AnimationSet animationSet = new AnimationSet(true);
        for (Animation addAnimation : list) {
            animationSet.addAnimation(addAnimation);
        }
        this.containerLayout.startAnimation(animationSet);
    }

    public void onWindowFocusChanged(boolean z) {
        super.onWindowFocusChanged(z);
        if (z && CardBrand.Unknown == getBrand()) {
            this.cardBrandView.applyTint$stripe_release();
        }
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        int i5;
        super.onLayout(z, i, i2, i3, i4);
        if (!this.isViewInitialized && getWidth() != 0) {
            this.isViewInitialized = true;
            this.placementParameters.setTotalLengthInPixels$stripe_release(getFrameWidth());
            updateSpaceSizes$stripe_release(this.isShowingFullCard);
            View view = this.cardNumberTextInputLayout;
            int cardWidth$stripe_release = this.placementParameters.getCardWidth$stripe_release();
            if (this.isShowingFullCard) {
                i5 = 0;
            } else {
                i5 = this.placementParameters.getHiddenCardWidth$stripe_release() * -1;
            }
            updateFieldLayout(view, cardWidth$stripe_release, i5);
            updateFieldLayout(this.expiryDateTextInputLayout, this.placementParameters.getDateWidth$stripe_release(), this.placementParameters.getDateLeftMargin$stripe_release(this.isShowingFullCard));
            updateFieldLayout(this.cvcNumberTextInputLayout, this.placementParameters.getCvcWidth$stripe_release(), this.placementParameters.getCvcLeftMargin$stripe_release(this.isShowingFullCard));
            updateFieldLayout(this.postalCodeTextInputLayout, this.placementParameters.getPostalCodeWidth$stripe_release(), this.placementParameters.getPostalCodeLeftMargin$stripe_release(this.isShowingFullCard));
        }
    }

    private final String getCvcPlaceHolder() {
        return CardBrand.AmericanExpress == getBrand() ? CVC_PLACEHOLDER_AMEX : CVC_PLACEHOLDER_COMMON;
    }

    private final String getPeekCardText() {
        int i = WhenMappings.$EnumSwitchMapping$0[getBrand().ordinal()];
        if (i != 1) {
            return i != 2 ? PEEK_TEXT_COMMON : PEEK_TEXT_DINERS_14;
        }
        return PEEK_TEXT_AMEX;
    }

    /* access modifiers changed from: private */
    public final void updateIcon() {
        this.cardBrandView.showBrandIcon$stripe_release(getBrand(), this.shouldShowErrorIcon);
    }

    /* access modifiers changed from: private */
    public final void updateIconCvc(boolean z, String str) {
        if (this.shouldShowErrorIcon) {
            updateIcon();
        } else if (Companion.shouldIconShowBrand$stripe_release(getBrand(), z, str)) {
            updateIcon();
        } else {
            updateIconForCvcEntry();
        }
    }

    private final void updateIconForCvcEntry() {
        this.cardBrandView.showCvcIcon$stripe_release(getBrand());
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b9\n\u0002\u0010\u000b\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0006\b\u0000\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u0015\u0010<\u001a\u00020\u00042\u0006\u0010=\u001a\u00020>H\u0000¢\u0006\u0002\b?J\u0015\u0010@\u001a\u00020\u00042\u0006\u0010=\u001a\u00020>H\u0000¢\u0006\u0002\bAJ\u0015\u0010B\u001a\u00020\u00042\u0006\u0010=\u001a\u00020>H\u0000¢\u0006\u0002\bCJ\b\u0010D\u001a\u00020EH\u0016J-\u0010F\u001a\u00020G2\u0006\u0010H\u001a\u00020>2\u0006\u0010I\u001a\u00020>2\u0006\u0010J\u001a\u00020\u00042\u0006\u0010K\u001a\u00020\u0004H\u0000¢\u0006\u0002\bLR\u001a\u0010\u0003\u001a\u00020\u0004X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\bR\u0014\u0010\t\u001a\u00020\u00048BX\u0004¢\u0006\u0006\u001a\u0004\b\n\u0010\u0006R\u0014\u0010\u000b\u001a\u00020\u00048BX\u0004¢\u0006\u0006\u001a\u0004\b\f\u0010\u0006R\u0014\u0010\r\u001a\u00020\u00048@X\u0004¢\u0006\u0006\u001a\u0004\b\u000e\u0010\u0006R\u001a\u0010\u000f\u001a\u00020\u0004X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0010\u0010\u0006\"\u0004\b\u0011\u0010\bR\u001a\u0010\u0012\u001a\u00020\u0004X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0013\u0010\u0006\"\u0004\b\u0014\u0010\bR\u001a\u0010\u0015\u001a\u00020\u0004X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0016\u0010\u0006\"\u0004\b\u0017\u0010\bR\u001a\u0010\u0018\u001a\u00020\u0004X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0019\u0010\u0006\"\u0004\b\u001a\u0010\bR\u001a\u0010\u001b\u001a\u00020\u0004X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u001c\u0010\u0006\"\u0004\b\u001d\u0010\bR\u001a\u0010\u001e\u001a\u00020\u0004X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u001f\u0010\u0006\"\u0004\b \u0010\bR\u001a\u0010!\u001a\u00020\u0004X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\"\u0010\u0006\"\u0004\b#\u0010\bR\u001a\u0010$\u001a\u00020\u0004X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b%\u0010\u0006\"\u0004\b&\u0010\bR\u001a\u0010'\u001a\u00020\u0004X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b(\u0010\u0006\"\u0004\b)\u0010\bR\u001a\u0010*\u001a\u00020\u0004X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b+\u0010\u0006\"\u0004\b,\u0010\bR\u001a\u0010-\u001a\u00020\u0004X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b.\u0010\u0006\"\u0004\b/\u0010\bR\u001a\u00100\u001a\u00020\u0004X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b1\u0010\u0006\"\u0004\b2\u0010\bR\u001a\u00103\u001a\u00020\u0004X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b4\u0010\u0006\"\u0004\b5\u0010\bR\u001a\u00106\u001a\u00020\u0004X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b7\u0010\u0006\"\u0004\b8\u0010\bR\u001a\u00109\u001a\u00020\u0004X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b:\u0010\u0006\"\u0004\b;\u0010\b¨\u0006M"}, d2 = {"Lcom/stripe/android/view/CardInputWidget$PlacementParameters;", "", "()V", "cardDateSeparation", "", "getCardDateSeparation$stripe_release", "()I", "setCardDateSeparation$stripe_release", "(I)V", "cardPeekCvcLeftMargin", "getCardPeekCvcLeftMargin", "cardPeekDateLeftMargin", "getCardPeekDateLeftMargin", "cardPeekPostalCodeLeftMargin", "getCardPeekPostalCodeLeftMargin$stripe_release", "cardTouchBufferLimit", "getCardTouchBufferLimit$stripe_release", "setCardTouchBufferLimit$stripe_release", "cardWidth", "getCardWidth$stripe_release", "setCardWidth$stripe_release", "cvcPostalCodeSeparation", "getCvcPostalCodeSeparation$stripe_release", "setCvcPostalCodeSeparation$stripe_release", "cvcRightTouchBufferLimit", "getCvcRightTouchBufferLimit$stripe_release", "setCvcRightTouchBufferLimit$stripe_release", "cvcStartPosition", "getCvcStartPosition$stripe_release", "setCvcStartPosition$stripe_release", "cvcWidth", "getCvcWidth$stripe_release", "setCvcWidth$stripe_release", "dateCvcSeparation", "getDateCvcSeparation$stripe_release", "setDateCvcSeparation$stripe_release", "dateRightTouchBufferLimit", "getDateRightTouchBufferLimit$stripe_release", "setDateRightTouchBufferLimit$stripe_release", "dateStartPosition", "getDateStartPosition$stripe_release", "setDateStartPosition$stripe_release", "dateWidth", "getDateWidth$stripe_release", "setDateWidth$stripe_release", "hiddenCardWidth", "getHiddenCardWidth$stripe_release", "setHiddenCardWidth$stripe_release", "peekCardWidth", "getPeekCardWidth$stripe_release", "setPeekCardWidth$stripe_release", "postalCodeStartPosition", "getPostalCodeStartPosition$stripe_release", "setPostalCodeStartPosition$stripe_release", "postalCodeWidth", "getPostalCodeWidth$stripe_release", "setPostalCodeWidth$stripe_release", "totalLengthInPixels", "getTotalLengthInPixels$stripe_release", "setTotalLengthInPixels$stripe_release", "getCvcLeftMargin", "isFullCard", "", "getCvcLeftMargin$stripe_release", "getDateLeftMargin", "getDateLeftMargin$stripe_release", "getPostalCodeLeftMargin", "getPostalCodeLeftMargin$stripe_release", "toString", "", "updateSpacing", "", "isShowingFullCard", "postalCodeEnabled", "frameStart", "frameWidth", "updateSpacing$stripe_release", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: CardInputWidget.kt */
    public static final class PlacementParameters {
        private int cardDateSeparation;
        private int cardTouchBufferLimit;
        private int cardWidth;
        private int cvcPostalCodeSeparation;
        private int cvcRightTouchBufferLimit;
        private int cvcStartPosition;
        private int cvcWidth;
        private int dateCvcSeparation;
        private int dateRightTouchBufferLimit;
        private int dateStartPosition;
        private int dateWidth;
        private int hiddenCardWidth;
        private int peekCardWidth;
        private int postalCodeStartPosition;
        private int postalCodeWidth;
        private int totalLengthInPixels;

        public final int getTotalLengthInPixels$stripe_release() {
            return this.totalLengthInPixels;
        }

        public final void setTotalLengthInPixels$stripe_release(int i) {
            this.totalLengthInPixels = i;
        }

        public final int getCardWidth$stripe_release() {
            return this.cardWidth;
        }

        public final void setCardWidth$stripe_release(int i) {
            this.cardWidth = i;
        }

        public final int getHiddenCardWidth$stripe_release() {
            return this.hiddenCardWidth;
        }

        public final void setHiddenCardWidth$stripe_release(int i) {
            this.hiddenCardWidth = i;
        }

        public final int getPeekCardWidth$stripe_release() {
            return this.peekCardWidth;
        }

        public final void setPeekCardWidth$stripe_release(int i) {
            this.peekCardWidth = i;
        }

        public final int getCardDateSeparation$stripe_release() {
            return this.cardDateSeparation;
        }

        public final void setCardDateSeparation$stripe_release(int i) {
            this.cardDateSeparation = i;
        }

        public final int getDateWidth$stripe_release() {
            return this.dateWidth;
        }

        public final void setDateWidth$stripe_release(int i) {
            this.dateWidth = i;
        }

        public final int getDateCvcSeparation$stripe_release() {
            return this.dateCvcSeparation;
        }

        public final void setDateCvcSeparation$stripe_release(int i) {
            this.dateCvcSeparation = i;
        }

        public final int getCvcWidth$stripe_release() {
            return this.cvcWidth;
        }

        public final void setCvcWidth$stripe_release(int i) {
            this.cvcWidth = i;
        }

        public final int getCvcPostalCodeSeparation$stripe_release() {
            return this.cvcPostalCodeSeparation;
        }

        public final void setCvcPostalCodeSeparation$stripe_release(int i) {
            this.cvcPostalCodeSeparation = i;
        }

        public final int getPostalCodeWidth$stripe_release() {
            return this.postalCodeWidth;
        }

        public final void setPostalCodeWidth$stripe_release(int i) {
            this.postalCodeWidth = i;
        }

        public final int getCardTouchBufferLimit$stripe_release() {
            return this.cardTouchBufferLimit;
        }

        public final void setCardTouchBufferLimit$stripe_release(int i) {
            this.cardTouchBufferLimit = i;
        }

        public final int getDateStartPosition$stripe_release() {
            return this.dateStartPosition;
        }

        public final void setDateStartPosition$stripe_release(int i) {
            this.dateStartPosition = i;
        }

        public final int getDateRightTouchBufferLimit$stripe_release() {
            return this.dateRightTouchBufferLimit;
        }

        public final void setDateRightTouchBufferLimit$stripe_release(int i) {
            this.dateRightTouchBufferLimit = i;
        }

        public final int getCvcStartPosition$stripe_release() {
            return this.cvcStartPosition;
        }

        public final void setCvcStartPosition$stripe_release(int i) {
            this.cvcStartPosition = i;
        }

        public final int getCvcRightTouchBufferLimit$stripe_release() {
            return this.cvcRightTouchBufferLimit;
        }

        public final void setCvcRightTouchBufferLimit$stripe_release(int i) {
            this.cvcRightTouchBufferLimit = i;
        }

        public final int getPostalCodeStartPosition$stripe_release() {
            return this.postalCodeStartPosition;
        }

        public final void setPostalCodeStartPosition$stripe_release(int i) {
            this.postalCodeStartPosition = i;
        }

        private final /* synthetic */ int getCardPeekDateLeftMargin() {
            return this.peekCardWidth + this.cardDateSeparation;
        }

        private final /* synthetic */ int getCardPeekCvcLeftMargin() {
            return getCardPeekDateLeftMargin() + this.dateWidth + this.dateCvcSeparation;
        }

        public final /* synthetic */ int getCardPeekPostalCodeLeftMargin$stripe_release() {
            return getCardPeekCvcLeftMargin() + this.postalCodeWidth + this.cvcPostalCodeSeparation;
        }

        public final /* synthetic */ int getDateLeftMargin$stripe_release(boolean z) {
            if (z) {
                return this.cardWidth + this.cardDateSeparation;
            }
            return getCardPeekDateLeftMargin();
        }

        public final /* synthetic */ int getCvcLeftMargin$stripe_release(boolean z) {
            if (z) {
                return this.totalLengthInPixels;
            }
            return getCardPeekCvcLeftMargin();
        }

        public final /* synthetic */ int getPostalCodeLeftMargin$stripe_release(boolean z) {
            if (z) {
                return this.totalLengthInPixels;
            }
            return getCardPeekPostalCodeLeftMargin$stripe_release();
        }

        public final /* synthetic */ void updateSpacing$stripe_release(boolean z, boolean z2, int i, int i2) {
            if (z) {
                int i3 = this.cardWidth;
                int i4 = (i2 - i3) - this.dateWidth;
                this.cardDateSeparation = i4;
                this.cardTouchBufferLimit = i + i3 + (i4 / 2);
                this.dateStartPosition = i + i3 + i4;
            } else if (z2) {
                int i5 = i2 * 3;
                int i6 = this.peekCardWidth;
                int i7 = this.dateWidth;
                int i8 = ((i5 / 10) - i6) - (i7 / 4);
                this.cardDateSeparation = i8;
                int i9 = this.cvcWidth;
                int i10 = ((((i5 / 5) - i6) - i8) - i7) - i9;
                this.dateCvcSeparation = i10;
                int i11 = (((((((i2 * 4) / 5) - i6) - i8) - i7) - i9) - i10) - this.postalCodeWidth;
                this.cvcPostalCodeSeparation = i11;
                int i12 = i + i6 + i8;
                this.cardTouchBufferLimit = i12 / 3;
                this.dateStartPosition = i12;
                int i13 = i12 + i7 + i10;
                this.dateRightTouchBufferLimit = i13 / 3;
                this.cvcStartPosition = i13;
                int i14 = i13 + i9 + i11;
                this.cvcRightTouchBufferLimit = i14 / 3;
                this.postalCodeStartPosition = i14;
            } else {
                int i15 = this.peekCardWidth;
                int i16 = this.dateWidth;
                int i17 = ((i2 / 2) - i15) - (i16 / 2);
                this.cardDateSeparation = i17;
                int i18 = (((i2 - i15) - i17) - i16) - this.cvcWidth;
                this.dateCvcSeparation = i18;
                this.cardTouchBufferLimit = i + i15 + (i17 / 2);
                int i19 = i + i15 + i17;
                this.dateStartPosition = i19;
                this.dateRightTouchBufferLimit = i19 + i16 + (i18 / 2);
                this.cvcStartPosition = i19 + i16 + i18;
            }
        }

        public String toString() {
            return ("\n                TotalLengthInPixels = " + this.totalLengthInPixels + "\n                CardWidth = " + this.cardWidth + "\n                HiddenCardWidth = " + this.hiddenCardWidth + "\n                PeekCardWidth = " + this.peekCardWidth + "\n                CardDateSeparation = " + this.cardDateSeparation + "\n                DateWidth = " + this.dateWidth + "\n                DateCvcSeparation = " + this.dateCvcSeparation + "\n                CvcWidth = " + this.cvcWidth + "\n                CvcPostalCodeSeparation = " + this.cvcPostalCodeSeparation + "\n                PostalCodeWidth: " + this.postalCodeWidth + "\n                ") + ("\n                Touch Buffer Data:\n                CardTouchBufferLimit = " + this.cardTouchBufferLimit + "\n                DateStartPosition = " + this.dateStartPosition + "\n                DateRightTouchBufferLimit = " + this.dateRightTouchBufferLimit + "\n                CvcStartPosition = " + this.cvcStartPosition + "\n                CvcRightTouchBufferLimit = " + this.cvcRightTouchBufferLimit + "\n                PostalCodeStartPosition = " + this.postalCodeStartPosition + "\n                ");
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\b\"\u0018\u0000 \u00032\u00020\u0001:\u0001\u0003B\u0005¢\u0006\u0002\u0010\u0002¨\u0006\u0004"}, d2 = {"Lcom/stripe/android/view/CardInputWidget$CardFieldAnimation;", "Landroid/view/animation/Animation;", "()V", "Companion", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: CardInputWidget.kt */
    private static abstract class CardFieldAnimation extends Animation {
        private static final long ANIMATION_LENGTH = 150;
        @Deprecated
        public static final Companion Companion = new Companion((DefaultConstructorMarker) null);

        public CardFieldAnimation() {
            setDuration(150);
        }

        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\t\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\u0005"}, d2 = {"Lcom/stripe/android/view/CardInputWidget$CardFieldAnimation$Companion;", "", "()V", "ANIMATION_LENGTH", "", "stripe_release"}, k = 1, mv = {1, 1, 16})
        /* compiled from: CardInputWidget.kt */
        private static final class Companion {
            private Companion() {
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0007\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u0018\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\nH\u0014R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\u000b"}, d2 = {"Lcom/stripe/android/view/CardInputWidget$CardNumberSlideLeftAnimation;", "Lcom/stripe/android/view/CardInputWidget$CardFieldAnimation;", "view", "Landroid/view/View;", "(Landroid/view/View;)V", "applyTransformation", "", "interpolatedTime", "", "t", "Landroid/view/animation/Transformation;", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: CardInputWidget.kt */
    private static final class CardNumberSlideLeftAnimation extends CardFieldAnimation {
        /* access modifiers changed from: private */
        public final View view;

        public CardNumberSlideLeftAnimation(View view2) {
            Intrinsics.checkParameterIsNotNull(view2, "view");
            this.view = view2;
            setAnimationListener(new AnimationEndListener(this) {
                final /* synthetic */ CardNumberSlideLeftAnimation this$0;

                {
                    this.this$0 = r1;
                }

                public void onAnimationEnd(Animation animation) {
                    Intrinsics.checkParameterIsNotNull(animation, "animation");
                    this.this$0.view.requestFocus();
                }
            });
        }

        /* access modifiers changed from: protected */
        public void applyTransformation(float f, Transformation transformation) {
            Intrinsics.checkParameterIsNotNull(transformation, "t");
            super.applyTransformation(f, transformation);
            View view2 = this.view;
            ViewGroup.LayoutParams layoutParams = view2.getLayoutParams();
            if (layoutParams != null) {
                FrameLayout.LayoutParams layoutParams2 = (FrameLayout.LayoutParams) layoutParams;
                layoutParams2.leftMargin = (int) (((float) layoutParams2.leftMargin) * (((float) 1) - f));
                view2.setLayoutParams(layoutParams2);
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type android.widget.FrameLayout.LayoutParams");
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0007\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0002\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0005¢\u0006\u0002\u0010\u0007J\u0018\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\rH\u0014R\u000e\u0010\u0006\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\u000e"}, d2 = {"Lcom/stripe/android/view/CardInputWidget$ExpiryDateSlideLeftAnimation;", "Lcom/stripe/android/view/CardInputWidget$CardFieldAnimation;", "view", "Landroid/view/View;", "startPosition", "", "destination", "(Landroid/view/View;II)V", "applyTransformation", "", "interpolatedTime", "", "t", "Landroid/view/animation/Transformation;", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: CardInputWidget.kt */
    private static final class ExpiryDateSlideLeftAnimation extends CardFieldAnimation {
        private final int destination;
        private final int startPosition;
        private final View view;

        public ExpiryDateSlideLeftAnimation(View view2, int i, int i2) {
            Intrinsics.checkParameterIsNotNull(view2, "view");
            this.view = view2;
            this.startPosition = i;
            this.destination = i2;
        }

        /* access modifiers changed from: protected */
        public void applyTransformation(float f, Transformation transformation) {
            Intrinsics.checkParameterIsNotNull(transformation, "t");
            super.applyTransformation(f, transformation);
            View view2 = this.view;
            ViewGroup.LayoutParams layoutParams = view2.getLayoutParams();
            if (layoutParams != null) {
                FrameLayout.LayoutParams layoutParams2 = (FrameLayout.LayoutParams) layoutParams;
                layoutParams2.leftMargin = (int) ((((float) this.destination) * f) + ((((float) 1) - f) * ((float) this.startPosition)));
                view2.setLayoutParams(layoutParams2);
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type android.widget.FrameLayout.LayoutParams");
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0007\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0002\u0018\u00002\u00020\u0001B%\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0005\u0012\u0006\u0010\u0007\u001a\u00020\u0005¢\u0006\u0002\u0010\bJ\u0018\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000eH\u0014R\u000e\u0010\u0006\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\u000f"}, d2 = {"Lcom/stripe/android/view/CardInputWidget$CvcSlideLeftAnimation;", "Lcom/stripe/android/view/CardInputWidget$CardFieldAnimation;", "view", "Landroid/view/View;", "startPosition", "", "destination", "newWidth", "(Landroid/view/View;III)V", "applyTransformation", "", "interpolatedTime", "", "t", "Landroid/view/animation/Transformation;", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: CardInputWidget.kt */
    private static final class CvcSlideLeftAnimation extends CardFieldAnimation {
        private final int destination;
        private final int newWidth;
        private final int startPosition;
        private final View view;

        public CvcSlideLeftAnimation(View view2, int i, int i2, int i3) {
            Intrinsics.checkParameterIsNotNull(view2, "view");
            this.view = view2;
            this.startPosition = i;
            this.destination = i2;
            this.newWidth = i3;
        }

        /* access modifiers changed from: protected */
        public void applyTransformation(float f, Transformation transformation) {
            Intrinsics.checkParameterIsNotNull(transformation, "t");
            super.applyTransformation(f, transformation);
            View view2 = this.view;
            ViewGroup.LayoutParams layoutParams = view2.getLayoutParams();
            if (layoutParams != null) {
                FrameLayout.LayoutParams layoutParams2 = (FrameLayout.LayoutParams) layoutParams;
                layoutParams2.leftMargin = (int) ((((float) this.destination) * f) + ((((float) 1) - f) * ((float) this.startPosition)));
                layoutParams2.rightMargin = 0;
                layoutParams2.width = this.newWidth;
                view2.setLayoutParams(layoutParams2);
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type android.widget.FrameLayout.LayoutParams");
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0007\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0002\u0018\u00002\u00020\u0001B%\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0005\u0012\u0006\u0010\u0007\u001a\u00020\u0005¢\u0006\u0002\u0010\bJ\u001a\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\f2\b\u0010\r\u001a\u0004\u0018\u00010\u000eH\u0014R\u000e\u0010\u0006\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\u000f"}, d2 = {"Lcom/stripe/android/view/CardInputWidget$PostalCodeSlideLeftAnimation;", "Lcom/stripe/android/view/CardInputWidget$CardFieldAnimation;", "view", "Landroid/view/View;", "startPosition", "", "destination", "newWidth", "(Landroid/view/View;III)V", "applyTransformation", "", "interpolatedTime", "", "t", "Landroid/view/animation/Transformation;", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: CardInputWidget.kt */
    private static final class PostalCodeSlideLeftAnimation extends CardFieldAnimation {
        private final int destination;
        private final int newWidth;
        private final int startPosition;
        private final View view;

        public PostalCodeSlideLeftAnimation(View view2, int i, int i2, int i3) {
            Intrinsics.checkParameterIsNotNull(view2, "view");
            this.view = view2;
            this.startPosition = i;
            this.destination = i2;
            this.newWidth = i3;
        }

        /* access modifiers changed from: protected */
        public void applyTransformation(float f, Transformation transformation) {
            super.applyTransformation(f, transformation);
            View view2 = this.view;
            ViewGroup.LayoutParams layoutParams = view2.getLayoutParams();
            if (layoutParams != null) {
                FrameLayout.LayoutParams layoutParams2 = (FrameLayout.LayoutParams) layoutParams;
                layoutParams2.leftMargin = (int) ((((float) this.destination) * f) + ((((float) 1) - f) * ((float) this.startPosition)));
                layoutParams2.rightMargin = 0;
                layoutParams2.width = this.newWidth;
                view2.setLayoutParams(layoutParams2);
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type android.widget.FrameLayout.LayoutParams");
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0007\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0002\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0003¢\u0006\u0002\u0010\u0007J\u0018\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\rH\u0014R\u000e\u0010\u0006\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\u000e"}, d2 = {"Lcom/stripe/android/view/CardInputWidget$CardNumberSlideRightAnimation;", "Lcom/stripe/android/view/CardInputWidget$CardFieldAnimation;", "view", "Landroid/view/View;", "hiddenCardWidth", "", "focusOnEndView", "(Landroid/view/View;ILandroid/view/View;)V", "applyTransformation", "", "interpolatedTime", "", "t", "Landroid/view/animation/Transformation;", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: CardInputWidget.kt */
    private static final class CardNumberSlideRightAnimation extends CardFieldAnimation {
        /* access modifiers changed from: private */
        public final View focusOnEndView;
        private final int hiddenCardWidth;
        private final View view;

        public CardNumberSlideRightAnimation(View view2, int i, View view3) {
            Intrinsics.checkParameterIsNotNull(view2, "view");
            Intrinsics.checkParameterIsNotNull(view3, "focusOnEndView");
            this.view = view2;
            this.hiddenCardWidth = i;
            this.focusOnEndView = view3;
            setAnimationListener(new AnimationEndListener(this) {
                final /* synthetic */ CardNumberSlideRightAnimation this$0;

                {
                    this.this$0 = r1;
                }

                public void onAnimationEnd(Animation animation) {
                    Intrinsics.checkParameterIsNotNull(animation, "animation");
                    this.this$0.focusOnEndView.requestFocus();
                }
            });
        }

        /* access modifiers changed from: protected */
        public void applyTransformation(float f, Transformation transformation) {
            Intrinsics.checkParameterIsNotNull(transformation, "t");
            super.applyTransformation(f, transformation);
            View view2 = this.view;
            ViewGroup.LayoutParams layoutParams = view2.getLayoutParams();
            if (layoutParams != null) {
                FrameLayout.LayoutParams layoutParams2 = (FrameLayout.LayoutParams) layoutParams;
                layoutParams2.leftMargin = (int) (((float) this.hiddenCardWidth) * -1.0f * f);
                view2.setLayoutParams(layoutParams2);
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type android.widget.FrameLayout.LayoutParams");
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0007\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0002\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0005¢\u0006\u0002\u0010\u0007J\u0018\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\rH\u0014R\u000e\u0010\u0006\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\u000e"}, d2 = {"Lcom/stripe/android/view/CardInputWidget$ExpiryDateSlideRightAnimation;", "Lcom/stripe/android/view/CardInputWidget$CardFieldAnimation;", "view", "Landroid/view/View;", "startMargin", "", "destination", "(Landroid/view/View;II)V", "applyTransformation", "", "interpolatedTime", "", "t", "Landroid/view/animation/Transformation;", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: CardInputWidget.kt */
    private static final class ExpiryDateSlideRightAnimation extends CardFieldAnimation {
        private final int destination;
        private final int startMargin;
        private final View view;

        public ExpiryDateSlideRightAnimation(View view2, int i, int i2) {
            Intrinsics.checkParameterIsNotNull(view2, "view");
            this.view = view2;
            this.startMargin = i;
            this.destination = i2;
        }

        /* access modifiers changed from: protected */
        public void applyTransformation(float f, Transformation transformation) {
            Intrinsics.checkParameterIsNotNull(transformation, "t");
            super.applyTransformation(f, transformation);
            View view2 = this.view;
            ViewGroup.LayoutParams layoutParams = view2.getLayoutParams();
            if (layoutParams != null) {
                FrameLayout.LayoutParams layoutParams2 = (FrameLayout.LayoutParams) layoutParams;
                layoutParams2.leftMargin = (int) ((((float) this.destination) * f) + ((((float) 1) - f) * ((float) this.startMargin)));
                view2.setLayoutParams(layoutParams2);
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type android.widget.FrameLayout.LayoutParams");
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0007\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0002\u0018\u00002\u00020\u0001B%\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0005\u0012\u0006\u0010\u0007\u001a\u00020\u0005¢\u0006\u0002\u0010\bJ\u0018\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000eH\u0014R\u000e\u0010\u0006\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\u000f"}, d2 = {"Lcom/stripe/android/view/CardInputWidget$CvcSlideRightAnimation;", "Lcom/stripe/android/view/CardInputWidget$CardFieldAnimation;", "view", "Landroid/view/View;", "startMargin", "", "destination", "newWidth", "(Landroid/view/View;III)V", "applyTransformation", "", "interpolatedTime", "", "t", "Landroid/view/animation/Transformation;", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: CardInputWidget.kt */
    private static final class CvcSlideRightAnimation extends CardFieldAnimation {
        private final int destination;
        private final int newWidth;
        private final int startMargin;
        private final View view;

        public CvcSlideRightAnimation(View view2, int i, int i2, int i3) {
            Intrinsics.checkParameterIsNotNull(view2, "view");
            this.view = view2;
            this.startMargin = i;
            this.destination = i2;
            this.newWidth = i3;
        }

        /* access modifiers changed from: protected */
        public void applyTransformation(float f, Transformation transformation) {
            Intrinsics.checkParameterIsNotNull(transformation, "t");
            super.applyTransformation(f, transformation);
            View view2 = this.view;
            ViewGroup.LayoutParams layoutParams = view2.getLayoutParams();
            if (layoutParams != null) {
                FrameLayout.LayoutParams layoutParams2 = (FrameLayout.LayoutParams) layoutParams;
                layoutParams2.leftMargin = (int) ((((float) this.destination) * f) + ((((float) 1) - f) * ((float) this.startMargin)));
                layoutParams2.rightMargin = 0;
                layoutParams2.width = this.newWidth;
                view2.setLayoutParams(layoutParams2);
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type android.widget.FrameLayout.LayoutParams");
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0007\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0002\u0018\u00002\u00020\u0001B%\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0005\u0012\u0006\u0010\u0007\u001a\u00020\u0005¢\u0006\u0002\u0010\bJ\u0018\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000eH\u0014R\u000e\u0010\u0006\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\u000f"}, d2 = {"Lcom/stripe/android/view/CardInputWidget$PostalCodeSlideRightAnimation;", "Lcom/stripe/android/view/CardInputWidget$CardFieldAnimation;", "view", "Landroid/view/View;", "startMargin", "", "destination", "newWidth", "(Landroid/view/View;III)V", "applyTransformation", "", "interpolatedTime", "", "t", "Landroid/view/animation/Transformation;", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: CardInputWidget.kt */
    private static final class PostalCodeSlideRightAnimation extends CardFieldAnimation {
        private final int destination;
        private final int newWidth;
        private final int startMargin;
        private final View view;

        public PostalCodeSlideRightAnimation(View view2, int i, int i2, int i3) {
            Intrinsics.checkParameterIsNotNull(view2, "view");
            this.view = view2;
            this.startMargin = i;
            this.destination = i2;
            this.newWidth = i3;
        }

        /* access modifiers changed from: protected */
        public void applyTransformation(float f, Transformation transformation) {
            Intrinsics.checkParameterIsNotNull(transformation, "t");
            super.applyTransformation(f, transformation);
            View view2 = this.view;
            ViewGroup.LayoutParams layoutParams = view2.getLayoutParams();
            if (layoutParams != null) {
                FrameLayout.LayoutParams layoutParams2 = (FrameLayout.LayoutParams) layoutParams;
                layoutParams2.leftMargin = (int) ((((float) this.destination) * f) + ((((float) 1) - f) * ((float) this.startMargin)));
                layoutParams2.rightMargin = 0;
                layoutParams2.width = this.newWidth;
                view2.setLayoutParams(layoutParams2);
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type android.widget.FrameLayout.LayoutParams");
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0000\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u0018\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bH\u0016¨\u0006\t"}, d2 = {"Lcom/stripe/android/view/CardInputWidget$DefaultLayoutWidthCalculator;", "Lcom/stripe/android/view/CardInputWidget$LayoutWidthCalculator;", "()V", "calculate", "", "text", "", "paint", "Landroid/text/TextPaint;", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: CardInputWidget.kt */
    public static final class DefaultLayoutWidthCalculator implements LayoutWidthCalculator {
        public int calculate(String str, TextPaint textPaint) {
            Intrinsics.checkParameterIsNotNull(str, "text");
            Intrinsics.checkParameterIsNotNull(textPaint, "paint");
            return (int) Layout.getDesiredWidth(str, textPaint);
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u000b\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J'\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u00132\b\u0010\u0017\u001a\u0004\u0018\u00010\u0004H\u0001¢\u0006\u0002\b\u0018R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u0010\u0010\u0006\u001a\u00020\u00078\u0002X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\u0019"}, d2 = {"Lcom/stripe/android/view/CardInputWidget$Companion;", "", "()V", "CVC_PLACEHOLDER_AMEX", "", "CVC_PLACEHOLDER_COMMON", "DEFAULT_READER_ID", "", "FULL_SIZING_CARD_TEXT", "FULL_SIZING_DATE_TEXT", "FULL_SIZING_POSTAL_CODE_TEXT", "LOGGING_TOKEN", "PEEK_TEXT_AMEX", "PEEK_TEXT_COMMON", "PEEK_TEXT_DINERS_14", "STATE_CARD_VIEWED", "STATE_POSTAL_CODE_ENABLED", "STATE_SUPER_STATE", "shouldIconShowBrand", "", "brand", "Lcom/stripe/android/model/CardBrand;", "cvcHasFocus", "cvcText", "shouldIconShowBrand$stripe_release", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: CardInputWidget.kt */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        public final boolean shouldIconShowBrand$stripe_release(CardBrand cardBrand, boolean z, String str) {
            Intrinsics.checkParameterIsNotNull(cardBrand, "brand");
            return !z || cardBrand.isMaxCvc(str);
        }
    }

    static {
        Class<CardInputWidget> cls = CardInputWidget.class;
        $$delegatedProperties = new KProperty[]{Reflection.mutableProperty1(new MutablePropertyReference1Impl(Reflection.getOrCreateKotlinClass(cls), "postalCodeEnabled", "getPostalCodeEnabled()Z")), Reflection.mutableProperty1(new MutablePropertyReference1Impl(Reflection.getOrCreateKotlinClass(cls), "usZipCodeRequired", "getUsZipCodeRequired()Z"))};
    }
}
