package com.stripe.android.view;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import androidx.fragment.app.Fragment;
import com.stripe.android.ObjectBuilder;
import com.stripe.android.PaymentConfiguration;
import com.stripe.android.model.PaymentMethod;
import com.stripe.android.view.ActivityStarter;
import kotlin.Metadata;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u0000 \u000b2\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001:\u0003\n\u000b\fB\u000f\b\u0016\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006B\u000f\b\u0016\u0012\u0006\u0010\u0007\u001a\u00020\b¢\u0006\u0002\u0010\t¨\u0006\r"}, d2 = {"Lcom/stripe/android/view/AddPaymentMethodActivityStarter;", "Lcom/stripe/android/view/ActivityStarter;", "Lcom/stripe/android/view/AddPaymentMethodActivity;", "Lcom/stripe/android/view/AddPaymentMethodActivityStarter$Args;", "activity", "Landroid/app/Activity;", "(Landroid/app/Activity;)V", "fragment", "Landroidx/fragment/app/Fragment;", "(Landroidx/fragment/app/Fragment;)V", "Args", "Companion", "Result", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: AddPaymentMethodActivityStarter.kt */
public final class AddPaymentMethodActivityStarter extends ActivityStarter<AddPaymentMethodActivity, Args> {
    public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
    public static final int REQUEST_CODE = 6001;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public AddPaymentMethodActivityStarter(Activity activity) {
        super(activity, AddPaymentMethodActivity.class, Args.Companion.getDEFAULT$stripe_release(), (int) REQUEST_CODE);
        Intrinsics.checkParameterIsNotNull(activity, "activity");
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public AddPaymentMethodActivityStarter(Fragment fragment) {
        super(fragment, AddPaymentMethodActivity.class, Args.Companion.getDEFAULT$stripe_release(), (int) REQUEST_CODE);
        Intrinsics.checkParameterIsNotNull(fragment, "fragment");
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000H\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b&\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\b\b\u0018\u0000 >2\u00020\u0001:\u0002=>BO\b\u0000\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0005\u0012\u0006\u0010\u0007\u001a\u00020\u0005\u0012\u0006\u0010\b\u001a\u00020\t\u0012\b\u0010\n\u001a\u0004\u0018\u00010\u000b\u0012\b\b\u0001\u0010\f\u001a\u00020\r\u0012\n\b\u0002\u0010\u000e\u001a\u0004\u0018\u00010\r¢\u0006\u0002\u0010\u000fJ\u000e\u0010\u001f\u001a\u00020\u0003HÀ\u0003¢\u0006\u0002\b J\u000e\u0010!\u001a\u00020\u0005HÀ\u0003¢\u0006\u0002\b\"J\u000e\u0010#\u001a\u00020\u0005HÀ\u0003¢\u0006\u0002\b$J\u000e\u0010%\u001a\u00020\u0005HÀ\u0003¢\u0006\u0002\b&J\u000e\u0010'\u001a\u00020\tHÀ\u0003¢\u0006\u0002\b(J\u0010\u0010)\u001a\u0004\u0018\u00010\u000bHÀ\u0003¢\u0006\u0002\b*J\u000e\u0010+\u001a\u00020\rHÀ\u0003¢\u0006\u0002\b,J\u0012\u0010-\u001a\u0004\u0018\u00010\rHÀ\u0003¢\u0006\u0004\b.\u0010\u001dJb\u0010/\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u00052\b\b\u0002\u0010\u0007\u001a\u00020\u00052\b\b\u0002\u0010\b\u001a\u00020\t2\n\b\u0002\u0010\n\u001a\u0004\u0018\u00010\u000b2\b\b\u0003\u0010\f\u001a\u00020\r2\n\b\u0002\u0010\u000e\u001a\u0004\u0018\u00010\rHÆ\u0001¢\u0006\u0002\u00100J\t\u00101\u001a\u00020\rHÖ\u0001J\u0013\u00102\u001a\u00020\u00052\b\u00103\u001a\u0004\u0018\u000104HÖ\u0003J\t\u00105\u001a\u00020\rHÖ\u0001J\t\u00106\u001a\u000207HÖ\u0001J\u0019\u00108\u001a\u0002092\u0006\u0010:\u001a\u00020;2\u0006\u0010<\u001a\u00020\rHÖ\u0001R\u0014\u0010\f\u001a\u00020\rX\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u0011R\u0014\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u0013R\u0014\u0010\u0006\u001a\u00020\u0005X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0014\u0010\u0015R\u0016\u0010\n\u001a\u0004\u0018\u00010\u000bX\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0016\u0010\u0017R\u0014\u0010\b\u001a\u00020\tX\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0018\u0010\u0019R\u0014\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u001a\u0010\u0015R\u0014\u0010\u0007\u001a\u00020\u0005X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u001b\u0010\u0015R\u0018\u0010\u000e\u001a\u0004\u0018\u00010\rX\u0004¢\u0006\n\n\u0002\u0010\u001e\u001a\u0004\b\u001c\u0010\u001d¨\u0006?"}, d2 = {"Lcom/stripe/android/view/AddPaymentMethodActivityStarter$Args;", "Lcom/stripe/android/view/ActivityStarter$Args;", "billingAddressFields", "Lcom/stripe/android/view/BillingAddressFields;", "shouldAttachToCustomer", "", "isPaymentSessionActive", "shouldInitCustomerSessionTokens", "paymentMethodType", "Lcom/stripe/android/model/PaymentMethod$Type;", "paymentConfiguration", "Lcom/stripe/android/PaymentConfiguration;", "addPaymentMethodFooterLayoutId", "", "windowFlags", "(Lcom/stripe/android/view/BillingAddressFields;ZZZLcom/stripe/android/model/PaymentMethod$Type;Lcom/stripe/android/PaymentConfiguration;ILjava/lang/Integer;)V", "getAddPaymentMethodFooterLayoutId$stripe_release", "()I", "getBillingAddressFields$stripe_release", "()Lcom/stripe/android/view/BillingAddressFields;", "isPaymentSessionActive$stripe_release", "()Z", "getPaymentConfiguration$stripe_release", "()Lcom/stripe/android/PaymentConfiguration;", "getPaymentMethodType$stripe_release", "()Lcom/stripe/android/model/PaymentMethod$Type;", "getShouldAttachToCustomer$stripe_release", "getShouldInitCustomerSessionTokens$stripe_release", "getWindowFlags$stripe_release", "()Ljava/lang/Integer;", "Ljava/lang/Integer;", "component1", "component1$stripe_release", "component2", "component2$stripe_release", "component3", "component3$stripe_release", "component4", "component4$stripe_release", "component5", "component5$stripe_release", "component6", "component6$stripe_release", "component7", "component7$stripe_release", "component8", "component8$stripe_release", "copy", "(Lcom/stripe/android/view/BillingAddressFields;ZZZLcom/stripe/android/model/PaymentMethod$Type;Lcom/stripe/android/PaymentConfiguration;ILjava/lang/Integer;)Lcom/stripe/android/view/AddPaymentMethodActivityStarter$Args;", "describeContents", "equals", "other", "", "hashCode", "toString", "", "writeToParcel", "", "parcel", "Landroid/os/Parcel;", "flags", "Builder", "Companion", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: AddPaymentMethodActivityStarter.kt */
    public static final class Args implements ActivityStarter.Args {
        public static final Parcelable.Creator CREATOR = new Creator();
        public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
        /* access modifiers changed from: private */
        public static final Args DEFAULT = new Builder().build();
        private final int addPaymentMethodFooterLayoutId;
        private final BillingAddressFields billingAddressFields;
        private final boolean isPaymentSessionActive;
        private final PaymentConfiguration paymentConfiguration;
        private final PaymentMethod.Type paymentMethodType;
        private final boolean shouldAttachToCustomer;
        private final boolean shouldInitCustomerSessionTokens;
        private final Integer windowFlags;

        @Metadata(bv = {1, 0, 3}, k = 3, mv = {1, 1, 16})
        public static class Creator implements Parcelable.Creator {
            public final Object createFromParcel(Parcel parcel) {
                Intrinsics.checkParameterIsNotNull(parcel, "in");
                return new Args((BillingAddressFields) Enum.valueOf(BillingAddressFields.class, parcel.readString()), parcel.readInt() != 0, parcel.readInt() != 0, parcel.readInt() != 0, (PaymentMethod.Type) Enum.valueOf(PaymentMethod.Type.class, parcel.readString()), parcel.readInt() != 0 ? (PaymentConfiguration) PaymentConfiguration.CREATOR.createFromParcel(parcel) : null, parcel.readInt(), parcel.readInt() != 0 ? Integer.valueOf(parcel.readInt()) : null);
            }

            public final Object[] newArray(int i) {
                return new Args[i];
            }
        }

        public static /* synthetic */ Args copy$default(Args args, BillingAddressFields billingAddressFields2, boolean z, boolean z2, boolean z3, PaymentMethod.Type type, PaymentConfiguration paymentConfiguration2, int i, Integer num, int i2, Object obj) {
            Args args2 = args;
            int i3 = i2;
            return args.copy((i3 & 1) != 0 ? args2.billingAddressFields : billingAddressFields2, (i3 & 2) != 0 ? args2.shouldAttachToCustomer : z, (i3 & 4) != 0 ? args2.isPaymentSessionActive : z2, (i3 & 8) != 0 ? args2.shouldInitCustomerSessionTokens : z3, (i3 & 16) != 0 ? args2.paymentMethodType : type, (i3 & 32) != 0 ? args2.paymentConfiguration : paymentConfiguration2, (i3 & 64) != 0 ? args2.addPaymentMethodFooterLayoutId : i, (i3 & 128) != 0 ? args2.windowFlags : num);
        }

        public final BillingAddressFields component1$stripe_release() {
            return this.billingAddressFields;
        }

        public final boolean component2$stripe_release() {
            return this.shouldAttachToCustomer;
        }

        public final boolean component3$stripe_release() {
            return this.isPaymentSessionActive;
        }

        public final boolean component4$stripe_release() {
            return this.shouldInitCustomerSessionTokens;
        }

        public final PaymentMethod.Type component5$stripe_release() {
            return this.paymentMethodType;
        }

        public final PaymentConfiguration component6$stripe_release() {
            return this.paymentConfiguration;
        }

        public final int component7$stripe_release() {
            return this.addPaymentMethodFooterLayoutId;
        }

        public final Integer component8$stripe_release() {
            return this.windowFlags;
        }

        public final Args copy(BillingAddressFields billingAddressFields2, boolean z, boolean z2, boolean z3, PaymentMethod.Type type, PaymentConfiguration paymentConfiguration2, int i, Integer num) {
            Intrinsics.checkParameterIsNotNull(billingAddressFields2, "billingAddressFields");
            Intrinsics.checkParameterIsNotNull(type, "paymentMethodType");
            return new Args(billingAddressFields2, z, z2, z3, type, paymentConfiguration2, i, num);
        }

        public int describeContents() {
            return 0;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Args)) {
                return false;
            }
            Args args = (Args) obj;
            return Intrinsics.areEqual((Object) this.billingAddressFields, (Object) args.billingAddressFields) && this.shouldAttachToCustomer == args.shouldAttachToCustomer && this.isPaymentSessionActive == args.isPaymentSessionActive && this.shouldInitCustomerSessionTokens == args.shouldInitCustomerSessionTokens && Intrinsics.areEqual((Object) this.paymentMethodType, (Object) args.paymentMethodType) && Intrinsics.areEqual((Object) this.paymentConfiguration, (Object) args.paymentConfiguration) && this.addPaymentMethodFooterLayoutId == args.addPaymentMethodFooterLayoutId && Intrinsics.areEqual((Object) this.windowFlags, (Object) args.windowFlags);
        }

        public int hashCode() {
            BillingAddressFields billingAddressFields2 = this.billingAddressFields;
            int i = 0;
            int hashCode = (billingAddressFields2 != null ? billingAddressFields2.hashCode() : 0) * 31;
            boolean z = this.shouldAttachToCustomer;
            boolean z2 = true;
            if (z) {
                z = true;
            }
            int i2 = (hashCode + (z ? 1 : 0)) * 31;
            boolean z3 = this.isPaymentSessionActive;
            if (z3) {
                z3 = true;
            }
            int i3 = (i2 + (z3 ? 1 : 0)) * 31;
            boolean z4 = this.shouldInitCustomerSessionTokens;
            if (!z4) {
                z2 = z4;
            }
            int i4 = (i3 + (z2 ? 1 : 0)) * 31;
            PaymentMethod.Type type = this.paymentMethodType;
            int hashCode2 = (i4 + (type != null ? type.hashCode() : 0)) * 31;
            PaymentConfiguration paymentConfiguration2 = this.paymentConfiguration;
            int hashCode3 = (((hashCode2 + (paymentConfiguration2 != null ? paymentConfiguration2.hashCode() : 0)) * 31) + this.addPaymentMethodFooterLayoutId) * 31;
            Integer num = this.windowFlags;
            if (num != null) {
                i = num.hashCode();
            }
            return hashCode3 + i;
        }

        public String toString() {
            return "Args(billingAddressFields=" + this.billingAddressFields + ", shouldAttachToCustomer=" + this.shouldAttachToCustomer + ", isPaymentSessionActive=" + this.isPaymentSessionActive + ", shouldInitCustomerSessionTokens=" + this.shouldInitCustomerSessionTokens + ", paymentMethodType=" + this.paymentMethodType + ", paymentConfiguration=" + this.paymentConfiguration + ", addPaymentMethodFooterLayoutId=" + this.addPaymentMethodFooterLayoutId + ", windowFlags=" + this.windowFlags + ")";
        }

        public void writeToParcel(Parcel parcel, int i) {
            Intrinsics.checkParameterIsNotNull(parcel, "parcel");
            parcel.writeString(this.billingAddressFields.name());
            parcel.writeInt(this.shouldAttachToCustomer ? 1 : 0);
            parcel.writeInt(this.isPaymentSessionActive ? 1 : 0);
            parcel.writeInt(this.shouldInitCustomerSessionTokens ? 1 : 0);
            parcel.writeString(this.paymentMethodType.name());
            PaymentConfiguration paymentConfiguration2 = this.paymentConfiguration;
            if (paymentConfiguration2 != null) {
                parcel.writeInt(1);
                paymentConfiguration2.writeToParcel(parcel, 0);
            } else {
                parcel.writeInt(0);
            }
            parcel.writeInt(this.addPaymentMethodFooterLayoutId);
            Integer num = this.windowFlags;
            if (num != null) {
                parcel.writeInt(1);
                parcel.writeInt(num.intValue());
                return;
            }
            parcel.writeInt(0);
        }

        public Args(BillingAddressFields billingAddressFields2, boolean z, boolean z2, boolean z3, PaymentMethod.Type type, PaymentConfiguration paymentConfiguration2, int i, Integer num) {
            Intrinsics.checkParameterIsNotNull(billingAddressFields2, "billingAddressFields");
            Intrinsics.checkParameterIsNotNull(type, "paymentMethodType");
            this.billingAddressFields = billingAddressFields2;
            this.shouldAttachToCustomer = z;
            this.isPaymentSessionActive = z2;
            this.shouldInitCustomerSessionTokens = z3;
            this.paymentMethodType = type;
            this.paymentConfiguration = paymentConfiguration2;
            this.addPaymentMethodFooterLayoutId = i;
            this.windowFlags = num;
        }

        public final BillingAddressFields getBillingAddressFields$stripe_release() {
            return this.billingAddressFields;
        }

        public final boolean getShouldAttachToCustomer$stripe_release() {
            return this.shouldAttachToCustomer;
        }

        public final boolean isPaymentSessionActive$stripe_release() {
            return this.isPaymentSessionActive;
        }

        public final boolean getShouldInitCustomerSessionTokens$stripe_release() {
            return this.shouldInitCustomerSessionTokens;
        }

        public final PaymentMethod.Type getPaymentMethodType$stripe_release() {
            return this.paymentMethodType;
        }

        public final PaymentConfiguration getPaymentConfiguration$stripe_release() {
            return this.paymentConfiguration;
        }

        public final int getAddPaymentMethodFooterLayoutId$stripe_release() {
            return this.addPaymentMethodFooterLayoutId;
        }

        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ Args(BillingAddressFields billingAddressFields2, boolean z, boolean z2, boolean z3, PaymentMethod.Type type, PaymentConfiguration paymentConfiguration2, int i, Integer num, int i2, DefaultConstructorMarker defaultConstructorMarker) {
            this(billingAddressFields2, z, z2, z3, type, paymentConfiguration2, i, (i2 & 128) != 0 ? null : num);
        }

        public final Integer getWindowFlags$stripe_release() {
            return this.windowFlags;
        }

        @Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0012\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0003J\b\u0010\u0012\u001a\u00020\u0002H\u0016J\u0010\u0010\u0013\u001a\u00020\u00002\b\b\u0001\u0010\u0004\u001a\u00020\u0005J\u000e\u0010\u0014\u001a\u00020\u00002\u0006\u0010\u0006\u001a\u00020\u0007J\u0015\u0010\u0015\u001a\u00020\u00002\u0006\u0010\b\u001a\u00020\tH\u0000¢\u0006\u0002\b\u0016J\u0017\u0010\u0017\u001a\u00020\u00002\b\u0010\n\u001a\u0004\u0018\u00010\u000bH\u0000¢\u0006\u0002\b\u0018J\u000e\u0010\u0019\u001a\u00020\u00002\u0006\u0010\f\u001a\u00020\rJ\u000e\u0010\u001a\u001a\u00020\u00002\u0006\u0010\u000e\u001a\u00020\tJ\u0015\u0010\u001b\u001a\u00020\u00002\u0006\u0010\u000f\u001a\u00020\tH\u0000¢\u0006\u0002\b\u001cJ\u0015\u0010\u001d\u001a\u00020\u00002\b\u0010\u0010\u001a\u0004\u0018\u00010\u0005¢\u0006\u0002\u0010\u001eR\u0012\u0010\u0004\u001a\u00020\u00058\u0002@\u0002X\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\n\u001a\u0004\u0018\u00010\u000bX\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\f\u001a\u0004\u0018\u00010\rX\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\tX\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\tX\u000e¢\u0006\u0002\n\u0000R\u0012\u0010\u0010\u001a\u0004\u0018\u00010\u0005X\u000e¢\u0006\u0004\n\u0002\u0010\u0011¨\u0006\u001f"}, d2 = {"Lcom/stripe/android/view/AddPaymentMethodActivityStarter$Args$Builder;", "Lcom/stripe/android/ObjectBuilder;", "Lcom/stripe/android/view/AddPaymentMethodActivityStarter$Args;", "()V", "addPaymentMethodFooterLayoutId", "", "billingAddressFields", "Lcom/stripe/android/view/BillingAddressFields;", "isPaymentSessionActive", "", "paymentConfiguration", "Lcom/stripe/android/PaymentConfiguration;", "paymentMethodType", "Lcom/stripe/android/model/PaymentMethod$Type;", "shouldAttachToCustomer", "shouldInitCustomerSessionTokens", "windowFlags", "Ljava/lang/Integer;", "build", "setAddPaymentMethodFooter", "setBillingAddressFields", "setIsPaymentSessionActive", "setIsPaymentSessionActive$stripe_release", "setPaymentConfiguration", "setPaymentConfiguration$stripe_release", "setPaymentMethodType", "setShouldAttachToCustomer", "setShouldInitCustomerSessionTokens", "setShouldInitCustomerSessionTokens$stripe_release", "setWindowFlags", "(Ljava/lang/Integer;)Lcom/stripe/android/view/AddPaymentMethodActivityStarter$Args$Builder;", "stripe_release"}, k = 1, mv = {1, 1, 16})
        /* compiled from: AddPaymentMethodActivityStarter.kt */
        public static final class Builder implements ObjectBuilder<Args> {
            private int addPaymentMethodFooterLayoutId;
            private BillingAddressFields billingAddressFields = BillingAddressFields.PostalCode;
            private boolean isPaymentSessionActive;
            private PaymentConfiguration paymentConfiguration;
            private PaymentMethod.Type paymentMethodType = PaymentMethod.Type.Card;
            private boolean shouldAttachToCustomer;
            private boolean shouldInitCustomerSessionTokens = true;
            private Integer windowFlags;

            public final Builder setShouldAttachToCustomer(boolean z) {
                Builder builder = this;
                builder.shouldAttachToCustomer = z;
                return builder;
            }

            public final Builder setBillingAddressFields(BillingAddressFields billingAddressFields2) {
                Intrinsics.checkParameterIsNotNull(billingAddressFields2, "billingAddressFields");
                Builder builder = this;
                builder.billingAddressFields = billingAddressFields2;
                return builder;
            }

            public final Builder setPaymentMethodType(PaymentMethod.Type type) {
                Intrinsics.checkParameterIsNotNull(type, "paymentMethodType");
                Builder builder = this;
                builder.paymentMethodType = type;
                return builder;
            }

            public final Builder setAddPaymentMethodFooter(int i) {
                Builder builder = this;
                builder.addPaymentMethodFooterLayoutId = i;
                return builder;
            }

            public final Builder setWindowFlags(Integer num) {
                Builder builder = this;
                builder.windowFlags = num;
                return builder;
            }

            public final /* synthetic */ Builder setIsPaymentSessionActive$stripe_release(boolean z) {
                Builder builder = this;
                builder.isPaymentSessionActive = z;
                return builder;
            }

            public final /* synthetic */ Builder setShouldInitCustomerSessionTokens$stripe_release(boolean z) {
                Builder builder = this;
                builder.shouldInitCustomerSessionTokens = z;
                return builder;
            }

            public final /* synthetic */ Builder setPaymentConfiguration$stripe_release(PaymentConfiguration paymentConfiguration2) {
                Builder builder = this;
                builder.paymentConfiguration = paymentConfiguration2;
                return builder;
            }

            public Args build() {
                BillingAddressFields billingAddressFields2 = this.billingAddressFields;
                boolean z = this.shouldAttachToCustomer;
                boolean z2 = this.isPaymentSessionActive;
                boolean z3 = this.shouldInitCustomerSessionTokens;
                PaymentMethod.Type type = this.paymentMethodType;
                if (type == null) {
                    type = PaymentMethod.Type.Card;
                }
                return new Args(billingAddressFields2, z, z2, z3, type, this.paymentConfiguration, this.addPaymentMethodFooterLayoutId, this.windowFlags);
            }
        }

        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0015\u0010\u0007\u001a\u00020\u00042\u0006\u0010\b\u001a\u00020\tH\u0000¢\u0006\u0002\b\nR\u0014\u0010\u0003\u001a\u00020\u0004X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006¨\u0006\u000b"}, d2 = {"Lcom/stripe/android/view/AddPaymentMethodActivityStarter$Args$Companion;", "", "()V", "DEFAULT", "Lcom/stripe/android/view/AddPaymentMethodActivityStarter$Args;", "getDEFAULT$stripe_release", "()Lcom/stripe/android/view/AddPaymentMethodActivityStarter$Args;", "create", "intent", "Landroid/content/Intent;", "create$stripe_release", "stripe_release"}, k = 1, mv = {1, 1, 16})
        /* compiled from: AddPaymentMethodActivityStarter.kt */
        public static final class Companion {
            private Companion() {
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }

            public final Args getDEFAULT$stripe_release() {
                return Args.DEFAULT;
            }

            public final /* synthetic */ Args create$stripe_release(Intent intent) {
                Intrinsics.checkParameterIsNotNull(intent, "intent");
                Parcelable parcelableExtra = intent.getParcelableExtra("extra_activity_args");
                if (parcelableExtra != null) {
                    return (Args) parcelableExtra;
                }
                throw new IllegalArgumentException("Required value was null.".toString());
            }
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\b\b\u0018\u0000 \u00192\u00020\u0001:\u0001\u0019B\u000f\b\u0000\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\t\u0010\u0007\u001a\u00020\u0003HÆ\u0003J\u0013\u0010\b\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u0003HÆ\u0001J\t\u0010\t\u001a\u00020\nHÖ\u0001J\u0013\u0010\u000b\u001a\u00020\f2\b\u0010\r\u001a\u0004\u0018\u00010\u000eHÖ\u0003J\t\u0010\u000f\u001a\u00020\nHÖ\u0001J\b\u0010\u0010\u001a\u00020\u0011H\u0016J\t\u0010\u0012\u001a\u00020\u0013HÖ\u0001J\u0019\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\nHÖ\u0001R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006¨\u0006\u001a"}, d2 = {"Lcom/stripe/android/view/AddPaymentMethodActivityStarter$Result;", "Lcom/stripe/android/view/ActivityStarter$Result;", "paymentMethod", "Lcom/stripe/android/model/PaymentMethod;", "(Lcom/stripe/android/model/PaymentMethod;)V", "getPaymentMethod", "()Lcom/stripe/android/model/PaymentMethod;", "component1", "copy", "describeContents", "", "equals", "", "other", "", "hashCode", "toBundle", "Landroid/os/Bundle;", "toString", "", "writeToParcel", "", "parcel", "Landroid/os/Parcel;", "flags", "Companion", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: AddPaymentMethodActivityStarter.kt */
    public static final class Result implements ActivityStarter.Result {
        public static final Parcelable.Creator CREATOR = new Creator();
        public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
        private final PaymentMethod paymentMethod;

        @Metadata(bv = {1, 0, 3}, k = 3, mv = {1, 1, 16})
        public static class Creator implements Parcelable.Creator {
            public final Object createFromParcel(Parcel parcel) {
                Intrinsics.checkParameterIsNotNull(parcel, "in");
                return new Result((PaymentMethod) PaymentMethod.CREATOR.createFromParcel(parcel));
            }

            public final Object[] newArray(int i) {
                return new Result[i];
            }
        }

        public static /* synthetic */ Result copy$default(Result result, PaymentMethod paymentMethod2, int i, Object obj) {
            if ((i & 1) != 0) {
                paymentMethod2 = result.paymentMethod;
            }
            return result.copy(paymentMethod2);
        }

        @JvmStatic
        public static final Result fromIntent(Intent intent) {
            return Companion.fromIntent(intent);
        }

        public final PaymentMethod component1() {
            return this.paymentMethod;
        }

        public final Result copy(PaymentMethod paymentMethod2) {
            Intrinsics.checkParameterIsNotNull(paymentMethod2, "paymentMethod");
            return new Result(paymentMethod2);
        }

        public int describeContents() {
            return 0;
        }

        public boolean equals(Object obj) {
            if (this != obj) {
                return (obj instanceof Result) && Intrinsics.areEqual((Object) this.paymentMethod, (Object) ((Result) obj).paymentMethod);
            }
            return true;
        }

        public int hashCode() {
            PaymentMethod paymentMethod2 = this.paymentMethod;
            if (paymentMethod2 != null) {
                return paymentMethod2.hashCode();
            }
            return 0;
        }

        public String toString() {
            return "Result(paymentMethod=" + this.paymentMethod + ")";
        }

        public void writeToParcel(Parcel parcel, int i) {
            Intrinsics.checkParameterIsNotNull(parcel, "parcel");
            this.paymentMethod.writeToParcel(parcel, 0);
        }

        public Result(PaymentMethod paymentMethod2) {
            Intrinsics.checkParameterIsNotNull(paymentMethod2, "paymentMethod");
            this.paymentMethod = paymentMethod2;
        }

        public final PaymentMethod getPaymentMethod() {
            return this.paymentMethod;
        }

        public Bundle toBundle() {
            Bundle bundle = new Bundle();
            bundle.putParcelable("extra_activity_result", this);
            return bundle;
        }

        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0014\u0010\u0003\u001a\u0004\u0018\u00010\u00042\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006H\u0007¨\u0006\u0007"}, d2 = {"Lcom/stripe/android/view/AddPaymentMethodActivityStarter$Result$Companion;", "", "()V", "fromIntent", "Lcom/stripe/android/view/AddPaymentMethodActivityStarter$Result;", "intent", "Landroid/content/Intent;", "stripe_release"}, k = 1, mv = {1, 1, 16})
        /* compiled from: AddPaymentMethodActivityStarter.kt */
        public static final class Companion {
            private Companion() {
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }

            @JvmStatic
            public final Result fromIntent(Intent intent) {
                if (intent != null) {
                    return (Result) intent.getParcelableExtra("extra_activity_result");
                }
                return null;
            }
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\u0005"}, d2 = {"Lcom/stripe/android/view/AddPaymentMethodActivityStarter$Companion;", "", "()V", "REQUEST_CODE", "", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: AddPaymentMethodActivityStarter.kt */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }
}
