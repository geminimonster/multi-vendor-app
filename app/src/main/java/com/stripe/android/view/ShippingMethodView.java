package com.stripe.android.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.core.content.ContextCompat;
import com.stripe.android.R;
import com.stripe.android.databinding.ShippingMethodViewBinding;
import com.stripe.android.model.ShippingMethod;
import java.util.Currency;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\b\u0000\u0018\u00002\u00020\u0001B%\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\b\b\u0002\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ\u0010\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0013H\u0016J\u000e\u0010\u0014\u001a\u00020\u00112\u0006\u0010\u0015\u001a\u00020\u0016R\u000e\u0010\t\u001a\u00020\nX\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u000b\u001a\u00020\u00078\u0002X\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\f\u001a\u00020\u00078\u0002X\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\r\u001a\u00020\u00078\u0002X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0004¢\u0006\u0002\n\u0000¨\u0006\u0017"}, d2 = {"Lcom/stripe/android/view/ShippingMethodView;", "Landroid/widget/RelativeLayout;", "context", "Landroid/content/Context;", "attrs", "Landroid/util/AttributeSet;", "defStyleAttr", "", "(Landroid/content/Context;Landroid/util/AttributeSet;I)V", "colorUtils", "Lcom/stripe/android/view/StripeColorUtils;", "selectedColorInt", "unselectedTextColorPrimaryInt", "unselectedTextColorSecondaryInt", "viewBinding", "Lcom/stripe/android/databinding/ShippingMethodViewBinding;", "setSelected", "", "selected", "", "setShippingMethod", "shippingMethod", "Lcom/stripe/android/model/ShippingMethod;", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: ShippingMethodView.kt */
public final class ShippingMethodView extends RelativeLayout {
    private final StripeColorUtils colorUtils;
    private final int selectedColorInt;
    private final int unselectedTextColorPrimaryInt;
    private final int unselectedTextColorSecondaryInt;
    private final ShippingMethodViewBinding viewBinding;

    public ShippingMethodView(Context context) {
        this(context, (AttributeSet) null, 0, 6, (DefaultConstructorMarker) null);
    }

    public ShippingMethodView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0, 4, (DefaultConstructorMarker) null);
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ ShippingMethodView(Context context, AttributeSet attributeSet, int i, int i2, DefaultConstructorMarker defaultConstructorMarker) {
        this(context, (i2 & 2) != 0 ? null : attributeSet, (i2 & 4) != 0 ? 0 : i);
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ShippingMethodView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        Intrinsics.checkParameterIsNotNull(context, "context");
        this.colorUtils = new StripeColorUtils(context);
        ShippingMethodViewBinding inflate = ShippingMethodViewBinding.inflate(LayoutInflater.from(context), this);
        Intrinsics.checkExpressionValueIsNotNull(inflate, "ShippingMethodViewBindin…text),\n        this\n    )");
        this.viewBinding = inflate;
        int i2 = this.colorUtils.getThemeAccentColor().data;
        int i3 = this.colorUtils.getThemeTextColorPrimary().data;
        int i4 = this.colorUtils.getThemeTextColorSecondary().data;
        this.selectedColorInt = StripeColorUtils.Companion.isColorTransparent(i2) ? ContextCompat.getColor(context, R.color.stripe_accent_color_default) : i2;
        this.unselectedTextColorPrimaryInt = StripeColorUtils.Companion.isColorTransparent(i3) ? ContextCompat.getColor(context, R.color.stripe_color_text_unselected_primary_default) : i3;
        this.unselectedTextColorSecondaryInt = StripeColorUtils.Companion.isColorTransparent(i4) ? ContextCompat.getColor(context, R.color.stripe_color_text_unselected_secondary_default) : i4;
    }

    public void setSelected(boolean z) {
        if (z) {
            this.viewBinding.name.setTextColor(this.selectedColorInt);
            this.viewBinding.description.setTextColor(this.selectedColorInt);
            this.viewBinding.price.setTextColor(this.selectedColorInt);
            AppCompatImageView appCompatImageView = this.viewBinding.selectedIcon;
            Intrinsics.checkExpressionValueIsNotNull(appCompatImageView, "viewBinding.selectedIcon");
            appCompatImageView.setVisibility(0);
            return;
        }
        this.viewBinding.name.setTextColor(this.unselectedTextColorPrimaryInt);
        this.viewBinding.description.setTextColor(this.unselectedTextColorSecondaryInt);
        this.viewBinding.price.setTextColor(this.unselectedTextColorPrimaryInt);
        AppCompatImageView appCompatImageView2 = this.viewBinding.selectedIcon;
        Intrinsics.checkExpressionValueIsNotNull(appCompatImageView2, "viewBinding.selectedIcon");
        appCompatImageView2.setVisibility(4);
    }

    public final void setShippingMethod(ShippingMethod shippingMethod) {
        Intrinsics.checkParameterIsNotNull(shippingMethod, "shippingMethod");
        TextView textView = this.viewBinding.name;
        Intrinsics.checkExpressionValueIsNotNull(textView, "viewBinding.name");
        textView.setText(shippingMethod.getLabel());
        TextView textView2 = this.viewBinding.description;
        Intrinsics.checkExpressionValueIsNotNull(textView2, "viewBinding.description");
        textView2.setText(shippingMethod.getDetail());
        TextView textView3 = this.viewBinding.price;
        Intrinsics.checkExpressionValueIsNotNull(textView3, "viewBinding.price");
        long amount = shippingMethod.getAmount();
        Currency currency = shippingMethod.getCurrency();
        String string = getContext().getString(R.string.price_free);
        Intrinsics.checkExpressionValueIsNotNull(string, "context.getString(R.string.price_free)");
        textView3.setText(PaymentUtils.formatPriceStringUsingFree(amount, currency, string));
    }
}
