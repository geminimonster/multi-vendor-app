package com.stripe.android.view;

import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelStoreOwner;
import com.stripe.android.view.AddPaymentMethodViewModel;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.Lambda;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n¢\u0006\u0002\b\u0002"}, d2 = {"<anonymous>", "Lcom/stripe/android/view/AddPaymentMethodViewModel;", "invoke"}, k = 3, mv = {1, 1, 16})
/* compiled from: AddPaymentMethodActivity.kt */
final class AddPaymentMethodActivity$viewModel$2 extends Lambda implements Function0<AddPaymentMethodViewModel> {
    final /* synthetic */ AddPaymentMethodActivity this$0;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    AddPaymentMethodActivity$viewModel$2(AddPaymentMethodActivity addPaymentMethodActivity) {
        super(0);
        this.this$0 = addPaymentMethodActivity;
    }

    public final AddPaymentMethodViewModel invoke() {
        AddPaymentMethodActivity addPaymentMethodActivity = this.this$0;
        return (AddPaymentMethodViewModel) new ViewModelProvider((ViewModelStoreOwner) addPaymentMethodActivity, (ViewModelProvider.Factory) new AddPaymentMethodViewModel.Factory(addPaymentMethodActivity.getStripe(), this.this$0.getCustomerSession(), this.this$0.getArgs())).get(AddPaymentMethodViewModel.class);
    }
}
