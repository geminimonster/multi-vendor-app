package com.stripe.android.view;

import androidx.lifecycle.Observer;
import com.stripe.android.exception.StripeException;
import com.stripe.android.model.PaymentMethod;
import com.stripe.android.view.i18n.TranslatorManager;
import java.util.List;
import kotlin.Metadata;
import kotlin.Result;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\u0010\u0000\u001a\u00020\u00012&\u0010\u0002\u001a\"\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00050\u0004 \u0006*\u0010\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00050\u0004\u0018\u00010\u00030\u0003H\n¢\u0006\u0002\b\u0007"}, d2 = {"<anonymous>", "", "result", "Lkotlin/Result;", "", "Lcom/stripe/android/model/PaymentMethod;", "kotlin.jvm.PlatformType", "onChanged"}, k = 3, mv = {1, 1, 16})
/* compiled from: PaymentMethodsActivity.kt */
final class PaymentMethodsActivity$fetchCustomerPaymentMethods$1<T> implements Observer<Result<? extends List<? extends PaymentMethod>>> {
    final /* synthetic */ PaymentMethodsActivity this$0;

    PaymentMethodsActivity$fetchCustomerPaymentMethods$1(PaymentMethodsActivity paymentMethodsActivity) {
        this.this$0 = paymentMethodsActivity;
    }

    public final void onChanged(Result<? extends List<? extends PaymentMethod>> result) {
        String str;
        Object r5 = result.m13unboximpl();
        Throwable r0 = Result.m7exceptionOrNullimpl(r5);
        if (r0 == null) {
            this.this$0.getAdapter().setPaymentMethods$stripe_release((List) r5);
            return;
        }
        AlertDisplayer access$getAlertDisplayer$p = this.this$0.getAlertDisplayer();
        if (r0 instanceof StripeException) {
            StripeException stripeException = (StripeException) r0;
            str = TranslatorManager.INSTANCE.getErrorMessageTranslator().translate(stripeException.getStatusCode(), r0.getMessage(), stripeException.getStripeError());
        } else {
            str = r0.getMessage();
            if (str == null) {
                str = "";
            }
        }
        access$getAlertDisplayer$p.show(str);
    }
}
