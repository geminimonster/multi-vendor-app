package com.stripe.android.view;

import android.content.Context;
import android.text.Editable;
import android.text.InputFilter;
import android.text.method.DigitsKeyListener;
import android.util.AttributeSet;
import androidx.appcompat.R;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import kotlin.text.StringsKt;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\t\b\u0000\u0018\u0000 \u00122\u00020\u0001:\u0001\u0012B%\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\b\b\u0002\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bR\u0013\u0010\t\u001a\u0004\u0018\u00010\n8F¢\u0006\u0006\u001a\u0004\b\u000b\u0010\fR\u001a\u0010\r\u001a\u00020\u0007X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u000e\u0010\u000f\"\u0004\b\u0010\u0010\u0011¨\u0006\u0013"}, d2 = {"Lcom/stripe/android/view/BecsDebitAccountNumberEditText;", "Lcom/stripe/android/view/StripeEditText;", "context", "Landroid/content/Context;", "attrs", "Landroid/util/AttributeSet;", "defStyleAttr", "", "(Landroid/content/Context;Landroid/util/AttributeSet;I)V", "accountNumber", "", "getAccountNumber", "()Ljava/lang/String;", "minLength", "getMinLength", "()I", "setMinLength", "(I)V", "Companion", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: BecsDebitAccountNumberEditText.kt */
public final class BecsDebitAccountNumberEditText extends StripeEditText {
    public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
    public static final int DEFAULT_MIN_LENGTH = 5;
    private static final int MAX_LENGTH = 9;
    private int minLength;

    public BecsDebitAccountNumberEditText(Context context) {
        this(context, (AttributeSet) null, 0, 6, (DefaultConstructorMarker) null);
    }

    public BecsDebitAccountNumberEditText(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0, 4, (DefaultConstructorMarker) null);
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ BecsDebitAccountNumberEditText(Context context, AttributeSet attributeSet, int i, int i2, DefaultConstructorMarker defaultConstructorMarker) {
        this(context, (i2 & 2) != 0 ? null : attributeSet, (i2 & 4) != 0 ? R.attr.editTextStyle : i);
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BecsDebitAccountNumberEditText(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        Intrinsics.checkParameterIsNotNull(context, "context");
        this.minLength = 5;
        setFilters((InputFilter[]) new InputFilter.LengthFilter[]{new InputFilter.LengthFilter(9)});
        setKeyListener(DigitsKeyListener.getInstance(false, true));
        addTextChangedListener(new StripeTextWatcher(this) {
            final /* synthetic */ BecsDebitAccountNumberEditText this$0;

            {
                this.this$0 = r1;
            }

            public void afterTextChanged(Editable editable) {
                this.this$0.setShouldShowError(false);
            }
        });
    }

    public final String getAccountNumber() {
        String str;
        if (StringsKt.isBlank(getFieldText$stripe_release())) {
            str = getResources().getString(com.stripe.android.R.string.becs_widget_account_number_required);
        } else {
            str = getFieldText$stripe_release().length() < this.minLength ? getResources().getString(com.stripe.android.R.string.becs_widget_account_number_incomplete) : null;
        }
        setErrorMessage$stripe_release(str);
        String fieldText$stripe_release = getFieldText$stripe_release();
        if (getErrorMessage$stripe_release() == null) {
            return fieldText$stripe_release;
        }
        return null;
    }

    public final int getMinLength() {
        return this.minLength;
    }

    public final void setMinLength(int i) {
        this.minLength = i;
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\u0006"}, d2 = {"Lcom/stripe/android/view/BecsDebitAccountNumberEditText$Companion;", "", "()V", "DEFAULT_MIN_LENGTH", "", "MAX_LENGTH", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: BecsDebitAccountNumberEditText.kt */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }
}
