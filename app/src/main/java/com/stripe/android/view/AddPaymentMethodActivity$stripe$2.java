package com.stripe.android.view;

import android.content.Context;
import com.stripe.android.PaymentConfiguration;
import com.stripe.android.Stripe;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n¢\u0006\u0002\b\u0002"}, d2 = {"<anonymous>", "Lcom/stripe/android/Stripe;", "invoke"}, k = 3, mv = {1, 1, 16})
/* compiled from: AddPaymentMethodActivity.kt */
final class AddPaymentMethodActivity$stripe$2 extends Lambda implements Function0<Stripe> {
    final /* synthetic */ AddPaymentMethodActivity this$0;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    AddPaymentMethodActivity$stripe$2(AddPaymentMethodActivity addPaymentMethodActivity) {
        super(0);
        this.this$0 = addPaymentMethodActivity;
    }

    public final Stripe invoke() {
        PaymentConfiguration paymentConfiguration$stripe_release = this.this$0.getArgs().getPaymentConfiguration$stripe_release();
        if (paymentConfiguration$stripe_release == null) {
            paymentConfiguration$stripe_release = PaymentConfiguration.Companion.getInstance(this.this$0);
        }
        Context applicationContext = this.this$0.getApplicationContext();
        Intrinsics.checkExpressionValueIsNotNull(applicationContext, "applicationContext");
        return new Stripe(applicationContext, paymentConfiguration$stripe_release.getPublishableKey(), (String) null, false, 12, (DefaultConstructorMarker) null);
    }
}
