package com.stripe.android.view;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import com.stripe.android.PaymentController;
import com.stripe.android.exception.StripeException;
import com.stripe.android.model.Source;
import com.stripe.android.stripe3ds2.transaction.ChallengeFlowOutcome;
import com.ults.listeners.SdkChallengeInterface;
import kotlin.Lazy;
import kotlin.LazyKt;
import kotlin.Metadata;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u0000 \r2\u00020\u0001:\u0001\rB\u0005¢\u0006\u0002\u0010\u0002J\u0012\u0010\t\u001a\u00020\n2\b\u0010\u000b\u001a\u0004\u0018\u00010\fH\u0014R\u001b\u0010\u0003\u001a\u00020\u00048BX\u0002¢\u0006\f\n\u0004\b\u0007\u0010\b\u001a\u0004\b\u0005\u0010\u0006¨\u0006\u000e"}, d2 = {"Lcom/stripe/android/view/Stripe3ds2CompletionActivity;", "Landroidx/appcompat/app/AppCompatActivity;", "()V", "flowOutcome", "", "getFlowOutcome", "()I", "flowOutcome$delegate", "Lkotlin/Lazy;", "onCreate", "", "savedInstanceState", "Landroid/os/Bundle;", "Companion", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: Stripe3ds2CompletionActivity.kt */
public final class Stripe3ds2CompletionActivity extends AppCompatActivity {
    public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
    public static final String EXTRA_CLIENT_SECRET = "extra_client_secret";
    public static final String EXTRA_STRIPE_ACCOUNT = "extra_stripe_account";
    private static final int UNKNOWN_FLOW_OUTCOME = -1;
    private final Lazy flowOutcome$delegate = LazyKt.lazy(new Stripe3ds2CompletionActivity$flowOutcome$2(this));

    @Metadata(bv = {1, 0, 3}, k = 3, mv = {1, 1, 16})
    public final /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;

        static {
            int[] iArr = new int[ChallengeFlowOutcome.values().length];
            $EnumSwitchMapping$0 = iArr;
            iArr[ChallengeFlowOutcome.CompleteSuccessful.ordinal()] = 1;
            $EnumSwitchMapping$0[ChallengeFlowOutcome.Cancel.ordinal()] = 2;
            $EnumSwitchMapping$0[ChallengeFlowOutcome.Timeout.ordinal()] = 3;
            $EnumSwitchMapping$0[ChallengeFlowOutcome.CompleteUnsuccessful.ordinal()] = 4;
            $EnumSwitchMapping$0[ChallengeFlowOutcome.ProtocolError.ordinal()] = 5;
            $EnumSwitchMapping$0[ChallengeFlowOutcome.RuntimeError.ordinal()] = 6;
        }
    }

    private final int getFlowOutcome() {
        return ((Number) this.flowOutcome$delegate.getValue()).intValue();
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        PaymentController.Result result = new PaymentController.Result(getIntent().getStringExtra(EXTRA_CLIENT_SECRET), getFlowOutcome(), (StripeException) null, false, (String) null, (Source) null, getIntent().getStringExtra(EXTRA_STRIPE_ACCOUNT), 60, (DefaultConstructorMarker) null);
        LocalBroadcastManager.getInstance(this).sendBroadcast(new Intent().setAction(SdkChallengeInterface.UL_HANDLE_CHALLENGE_ACTION));
        setResult(-1, new Intent().putExtras(result.toBundle()));
        finish();
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007XT¢\u0006\u0002\n\u0000¨\u0006\b"}, d2 = {"Lcom/stripe/android/view/Stripe3ds2CompletionActivity$Companion;", "", "()V", "EXTRA_CLIENT_SECRET", "", "EXTRA_STRIPE_ACCOUNT", "UNKNOWN_FLOW_OUTCOME", "", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: Stripe3ds2CompletionActivity.kt */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }
}
