package com.stripe.android.view;

import androidx.lifecycle.Observer;
import com.stripe.android.model.ShippingMethod;
import java.util.List;
import kotlin.Metadata;
import kotlin.Result;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\u0010\u0000\u001a\u00020\u00012&\u0010\u0002\u001a\"\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00050\u0004 \u0006*\u0010\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00050\u0004\u0018\u00010\u00030\u0003H\n¢\u0006\u0002\b\u0007"}, d2 = {"<anonymous>", "", "it", "Lkotlin/Result;", "", "Lcom/stripe/android/model/ShippingMethod;", "kotlin.jvm.PlatformType", "onChanged"}, k = 3, mv = {1, 1, 16})
/* compiled from: PaymentFlowActivity.kt */
final class PaymentFlowActivity$validateShippingInformation$1<T> implements Observer<Result<? extends List<? extends ShippingMethod>>> {
    final /* synthetic */ PaymentFlowActivity this$0;

    PaymentFlowActivity$validateShippingInformation$1(PaymentFlowActivity paymentFlowActivity) {
        this.this$0 = paymentFlowActivity;
    }

    public final void onChanged(Result<? extends List<? extends ShippingMethod>> result) {
        Object r3 = result.m13unboximpl();
        PaymentFlowActivity paymentFlowActivity = this.this$0;
        Throwable r1 = Result.m7exceptionOrNullimpl(r3);
        if (r1 == null) {
            paymentFlowActivity.onShippingInfoValidated((List) r3);
        } else {
            this.this$0.onShippingInfoError(r1.getMessage());
        }
    }
}
