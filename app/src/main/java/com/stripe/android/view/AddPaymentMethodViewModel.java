package com.stripe.android.view;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import com.facebook.internal.NativeProtocol;
import com.stripe.android.CustomerSession;
import com.stripe.android.PaymentSession;
import com.stripe.android.Stripe;
import com.stripe.android.model.PaymentMethod;
import com.stripe.android.model.PaymentMethodCreateParams;
import com.stripe.android.view.AddPaymentMethodActivityStarter;
import com.stripe.android.view.i18n.ErrorMessageTranslator;
import com.stripe.android.view.i18n.TranslatorManager;
import java.util.Map;
import java.util.Set;
import kotlin.Metadata;
import kotlin.Result;
import kotlin.collections.CollectionsKt;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000F\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\"\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u0000\u0018\u00002\u00020\u0001:\u0001\u0018B'\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\b\b\u0002\u0010\b\u001a\u00020\t¢\u0006\u0002\u0010\nJ$\u0010\u000e\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00110\u00100\u000f2\u0006\u0010\u0012\u001a\u00020\u0011H\u0000ø\u0001\u0000¢\u0006\u0002\b\u0013J$\u0010\u0014\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00110\u00100\u000f2\u0006\u0010\u0015\u001a\u00020\u0016H\u0000ø\u0001\u0000¢\u0006\u0002\b\u0017R\u000e\u0010\u0006\u001a\u00020\u0007X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\r0\fX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000\u0002\u0004\n\u0002\b\u0019¨\u0006\u0019"}, d2 = {"Lcom/stripe/android/view/AddPaymentMethodViewModel;", "Landroidx/lifecycle/ViewModel;", "stripe", "Lcom/stripe/android/Stripe;", "customerSession", "Lcom/stripe/android/CustomerSession;", "args", "Lcom/stripe/android/view/AddPaymentMethodActivityStarter$Args;", "errorMessageTranslator", "Lcom/stripe/android/view/i18n/ErrorMessageTranslator;", "(Lcom/stripe/android/Stripe;Lcom/stripe/android/CustomerSession;Lcom/stripe/android/view/AddPaymentMethodActivityStarter$Args;Lcom/stripe/android/view/i18n/ErrorMessageTranslator;)V", "productUsage", "", "", "attachPaymentMethod", "Landroidx/lifecycle/LiveData;", "Lkotlin/Result;", "Lcom/stripe/android/model/PaymentMethod;", "paymentMethod", "attachPaymentMethod$stripe_release", "createPaymentMethod", "params", "Lcom/stripe/android/model/PaymentMethodCreateParams;", "createPaymentMethod$stripe_release", "Factory", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: AddPaymentMethodViewModel.kt */
public final class AddPaymentMethodViewModel extends ViewModel {
    private final AddPaymentMethodActivityStarter.Args args;
    private final CustomerSession customerSession;
    /* access modifiers changed from: private */
    public final ErrorMessageTranslator errorMessageTranslator;
    private final Set<String> productUsage;
    private final Stripe stripe;

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ AddPaymentMethodViewModel(Stripe stripe2, CustomerSession customerSession2, AddPaymentMethodActivityStarter.Args args2, ErrorMessageTranslator errorMessageTranslator2, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(stripe2, customerSession2, args2, (i & 8) != 0 ? TranslatorManager.INSTANCE.getErrorMessageTranslator() : errorMessageTranslator2);
    }

    public AddPaymentMethodViewModel(Stripe stripe2, CustomerSession customerSession2, AddPaymentMethodActivityStarter.Args args2, ErrorMessageTranslator errorMessageTranslator2) {
        Intrinsics.checkParameterIsNotNull(stripe2, "stripe");
        Intrinsics.checkParameterIsNotNull(customerSession2, "customerSession");
        Intrinsics.checkParameterIsNotNull(args2, "args");
        Intrinsics.checkParameterIsNotNull(errorMessageTranslator2, "errorMessageTranslator");
        this.stripe = stripe2;
        this.customerSession = customerSession2;
        this.args = args2;
        this.errorMessageTranslator = errorMessageTranslator2;
        String[] strArr = new String[2];
        strArr[0] = AddPaymentMethodActivity.PRODUCT_TOKEN;
        strArr[1] = args2.isPaymentSessionActive$stripe_release() ? PaymentSession.PRODUCT_TOKEN : null;
        this.productUsage = CollectionsKt.toSet(CollectionsKt.listOfNotNull((T[]) strArr));
    }

    public final LiveData<Result<PaymentMethod>> createPaymentMethod$stripe_release(PaymentMethodCreateParams paymentMethodCreateParams) {
        PaymentMethodCreateParams paymentMethodCreateParams2 = paymentMethodCreateParams;
        Intrinsics.checkParameterIsNotNull(paymentMethodCreateParams2, NativeProtocol.WEB_DIALOG_PARAMS);
        MutableLiveData mutableLiveData = new MutableLiveData();
        Stripe.createPaymentMethod$default(this.stripe, PaymentMethodCreateParams.copy$default(paymentMethodCreateParams2, (PaymentMethodCreateParams.Type) null, (PaymentMethodCreateParams.Card) null, (PaymentMethodCreateParams.Ideal) null, (PaymentMethodCreateParams.Fpx) null, (PaymentMethodCreateParams.SepaDebit) null, (PaymentMethodCreateParams.AuBecsDebit) null, (PaymentMethodCreateParams.BacsDebit) null, (PaymentMethodCreateParams.Sofort) null, (PaymentMethod.BillingDetails) null, (Map) null, this.productUsage, 1023, (Object) null), (String) null, (String) null, new AddPaymentMethodViewModel$createPaymentMethod$1(mutableLiveData), 6, (Object) null);
        return mutableLiveData;
    }

    public final /* synthetic */ LiveData<Result<PaymentMethod>> attachPaymentMethod$stripe_release(PaymentMethod paymentMethod) {
        Intrinsics.checkParameterIsNotNull(paymentMethod, "paymentMethod");
        MutableLiveData mutableLiveData = new MutableLiveData();
        CustomerSession customerSession2 = this.customerSession;
        String str = paymentMethod.id;
        if (str == null) {
            str = "";
        }
        customerSession2.attachPaymentMethod$stripe_release(str, this.productUsage, new AddPaymentMethodViewModel$attachPaymentMethod$1(this, mutableLiveData));
        return mutableLiveData;
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0000\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ'\u0010\t\u001a\u0002H\n\"\n\b\u0000\u0010\n*\u0004\u0018\u00010\u000b2\f\u0010\f\u001a\b\u0012\u0004\u0012\u0002H\n0\rH\u0016¢\u0006\u0002\u0010\u000eR\u000e\u0010\u0006\u001a\u00020\u0007X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\u000f"}, d2 = {"Lcom/stripe/android/view/AddPaymentMethodViewModel$Factory;", "Landroidx/lifecycle/ViewModelProvider$Factory;", "stripe", "Lcom/stripe/android/Stripe;", "customerSession", "Lcom/stripe/android/CustomerSession;", "args", "Lcom/stripe/android/view/AddPaymentMethodActivityStarter$Args;", "(Lcom/stripe/android/Stripe;Lcom/stripe/android/CustomerSession;Lcom/stripe/android/view/AddPaymentMethodActivityStarter$Args;)V", "create", "T", "Landroidx/lifecycle/ViewModel;", "modelClass", "Ljava/lang/Class;", "(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: AddPaymentMethodViewModel.kt */
    public static final class Factory implements ViewModelProvider.Factory {
        private final AddPaymentMethodActivityStarter.Args args;
        private final CustomerSession customerSession;
        private final Stripe stripe;

        public Factory(Stripe stripe2, CustomerSession customerSession2, AddPaymentMethodActivityStarter.Args args2) {
            Intrinsics.checkParameterIsNotNull(stripe2, "stripe");
            Intrinsics.checkParameterIsNotNull(customerSession2, "customerSession");
            Intrinsics.checkParameterIsNotNull(args2, "args");
            this.stripe = stripe2;
            this.customerSession = customerSession2;
            this.args = args2;
        }

        public <T extends ViewModel> T create(Class<T> cls) {
            Intrinsics.checkParameterIsNotNull(cls, "modelClass");
            return (ViewModel) new AddPaymentMethodViewModel(this.stripe, this.customerSession, this.args, (ErrorMessageTranslator) null, 8, (DefaultConstructorMarker) null);
        }
    }
}
