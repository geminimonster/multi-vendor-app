package com.stripe.android.view;

import android.text.Editable;
import kotlin.Metadata;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0017\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000*\u0001\u0000\b\n\u0018\u00002\u00020\u0001J\u0012\u0010\u0002\u001a\u00020\u00032\b\u0010\u0004\u001a\u0004\u0018\u00010\u0005H\u0016¨\u0006\u0006"}, d2 = {"com/stripe/android/view/CardInputWidget$inputChangeTextWatcher$1", "Lcom/stripe/android/view/StripeTextWatcher;", "afterTextChanged", "", "s", "Landroid/text/Editable;", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: CardInputWidget.kt */
public final class CardInputWidget$inputChangeTextWatcher$1 extends StripeTextWatcher {
    final /* synthetic */ CardInputWidget this$0;

    CardInputWidget$inputChangeTextWatcher$1(CardInputWidget cardInputWidget) {
        this.this$0 = cardInputWidget;
    }

    public void afterTextChanged(Editable editable) {
        super.afterTextChanged(editable);
        this.this$0.setShouldShowErrorIcon(false);
    }
}
