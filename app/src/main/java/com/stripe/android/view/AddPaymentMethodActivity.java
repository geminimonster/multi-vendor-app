package com.stripe.android.view;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.core.text.util.LinkifyCompat;
import androidx.core.view.ViewCompat;
import com.stripe.android.CustomerSession;
import com.stripe.android.R;
import com.stripe.android.Stripe;
import com.stripe.android.databinding.AddPaymentMethodActivityBinding;
import com.stripe.android.model.PaymentMethod;
import com.stripe.android.model.PaymentMethodCreateParams;
import com.stripe.android.view.AddPaymentMethodActivityStarter;
import kotlin.Lazy;
import kotlin.LazyKt;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000x\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u0000 A2\u00020\u0001:\u0001AB\u0005¢\u0006\u0002\u0010\u0002J\u0010\u0010+\u001a\u00020,2\u0006\u0010-\u001a\u00020.H\u0002J\u0010\u0010/\u001a\u00020,2\u0006\u0010\t\u001a\u00020\nH\u0002J\u0012\u00100\u001a\u0004\u0018\u0001012\u0006\u00102\u001a\u000203H\u0002J\u001f\u00104\u001a\u00020,2\u0006\u0010&\u001a\u00020'2\b\u00105\u001a\u0004\u0018\u000106H\u0000¢\u0006\u0002\b7J\u0010\u00108\u001a\u00020\u00042\u0006\u0010\t\u001a\u00020\nH\u0002J\u0010\u00109\u001a\u00020,2\u0006\u0010-\u001a\u00020.H\u0002J\b\u0010:\u001a\u00020,H\u0016J\u0012\u0010;\u001a\u00020,2\b\u0010<\u001a\u0004\u0018\u00010=H\u0014J\u0010\u0010>\u001a\u00020,2\u0006\u0010?\u001a\u00020\u0019H\u0014J\b\u0010@\u001a\u00020,H\u0014R\u001b\u0010\u0003\u001a\u00020\u00048BX\u0002¢\u0006\f\n\u0004\b\u0007\u0010\b\u001a\u0004\b\u0005\u0010\u0006R\u001b\u0010\t\u001a\u00020\n8BX\u0002¢\u0006\f\n\u0004\b\r\u0010\b\u001a\u0004\b\u000b\u0010\fR\u001b\u0010\u000e\u001a\u00020\u000f8BX\u0002¢\u0006\f\n\u0004\b\u0012\u0010\b\u001a\u0004\b\u0010\u0010\u0011R\u001b\u0010\u0013\u001a\u00020\u00148BX\u0002¢\u0006\f\n\u0004\b\u0017\u0010\b\u001a\u0004\b\u0015\u0010\u0016R\u001b\u0010\u0018\u001a\u00020\u00198BX\u0002¢\u0006\f\n\u0004\b\u001c\u0010\b\u001a\u0004\b\u001a\u0010\u001bR\u001b\u0010\u001d\u001a\u00020\u001e8BX\u0002¢\u0006\f\n\u0004\b!\u0010\b\u001a\u0004\b\u001f\u0010 R\u0014\u0010\"\u001a\u00020#8CX\u0004¢\u0006\u0006\u001a\u0004\b$\u0010%R\u001b\u0010&\u001a\u00020'8BX\u0002¢\u0006\f\n\u0004\b*\u0010\b\u001a\u0004\b(\u0010)¨\u0006B"}, d2 = {"Lcom/stripe/android/view/AddPaymentMethodActivity;", "Lcom/stripe/android/view/StripeActivity;", "()V", "addPaymentMethodView", "Lcom/stripe/android/view/AddPaymentMethodView;", "getAddPaymentMethodView", "()Lcom/stripe/android/view/AddPaymentMethodView;", "addPaymentMethodView$delegate", "Lkotlin/Lazy;", "args", "Lcom/stripe/android/view/AddPaymentMethodActivityStarter$Args;", "getArgs", "()Lcom/stripe/android/view/AddPaymentMethodActivityStarter$Args;", "args$delegate", "customerSession", "Lcom/stripe/android/CustomerSession;", "getCustomerSession", "()Lcom/stripe/android/CustomerSession;", "customerSession$delegate", "paymentMethodType", "Lcom/stripe/android/model/PaymentMethod$Type;", "getPaymentMethodType", "()Lcom/stripe/android/model/PaymentMethod$Type;", "paymentMethodType$delegate", "shouldAttachToCustomer", "", "getShouldAttachToCustomer", "()Z", "shouldAttachToCustomer$delegate", "stripe", "Lcom/stripe/android/Stripe;", "getStripe", "()Lcom/stripe/android/Stripe;", "stripe$delegate", "titleStringRes", "", "getTitleStringRes", "()I", "viewModel", "Lcom/stripe/android/view/AddPaymentMethodViewModel;", "getViewModel", "()Lcom/stripe/android/view/AddPaymentMethodViewModel;", "viewModel$delegate", "attachPaymentMethodToCustomer", "", "paymentMethod", "Lcom/stripe/android/model/PaymentMethod;", "configureView", "createFooterView", "Landroid/view/View;", "contentRoot", "Landroid/view/ViewGroup;", "createPaymentMethod", "params", "Lcom/stripe/android/model/PaymentMethodCreateParams;", "createPaymentMethod$stripe_release", "createPaymentMethodView", "finishWithPaymentMethod", "onActionSave", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onProgressBarVisibilityChanged", "visible", "onResume", "Companion", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: AddPaymentMethodActivity.kt */
public final class AddPaymentMethodActivity extends StripeActivity {
    public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
    public static final String PRODUCT_TOKEN = "AddPaymentMethodActivity";
    private final Lazy addPaymentMethodView$delegate = LazyKt.lazy(new AddPaymentMethodActivity$addPaymentMethodView$2(this));
    private final Lazy args$delegate = LazyKt.lazy(new AddPaymentMethodActivity$args$2(this));
    private final Lazy customerSession$delegate = LazyKt.lazy(AddPaymentMethodActivity$customerSession$2.INSTANCE);
    private final Lazy paymentMethodType$delegate = LazyKt.lazy(new AddPaymentMethodActivity$paymentMethodType$2(this));
    private final Lazy shouldAttachToCustomer$delegate = LazyKt.lazy(new AddPaymentMethodActivity$shouldAttachToCustomer$2(this));
    private final Lazy stripe$delegate = LazyKt.lazy(new AddPaymentMethodActivity$stripe$2(this));
    private final Lazy viewModel$delegate = LazyKt.lazy(new AddPaymentMethodActivity$viewModel$2(this));

    @Metadata(bv = {1, 0, 3}, k = 3, mv = {1, 1, 16})
    public final /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;
        public static final /* synthetic */ int[] $EnumSwitchMapping$1;

        static {
            int[] iArr = new int[PaymentMethod.Type.values().length];
            $EnumSwitchMapping$0 = iArr;
            iArr[PaymentMethod.Type.Card.ordinal()] = 1;
            $EnumSwitchMapping$0[PaymentMethod.Type.Fpx.ordinal()] = 2;
            int[] iArr2 = new int[PaymentMethod.Type.values().length];
            $EnumSwitchMapping$1 = iArr2;
            iArr2[PaymentMethod.Type.Card.ordinal()] = 1;
            $EnumSwitchMapping$1[PaymentMethod.Type.Fpx.ordinal()] = 2;
        }
    }

    private final AddPaymentMethodView getAddPaymentMethodView() {
        return (AddPaymentMethodView) this.addPaymentMethodView$delegate.getValue();
    }

    /* access modifiers changed from: private */
    public final AddPaymentMethodActivityStarter.Args getArgs() {
        return (AddPaymentMethodActivityStarter.Args) this.args$delegate.getValue();
    }

    /* access modifiers changed from: private */
    public final CustomerSession getCustomerSession() {
        return (CustomerSession) this.customerSession$delegate.getValue();
    }

    /* access modifiers changed from: private */
    public final PaymentMethod.Type getPaymentMethodType() {
        return (PaymentMethod.Type) this.paymentMethodType$delegate.getValue();
    }

    /* access modifiers changed from: private */
    public final boolean getShouldAttachToCustomer() {
        return ((Boolean) this.shouldAttachToCustomer$delegate.getValue()).booleanValue();
    }

    /* access modifiers changed from: private */
    public final Stripe getStripe() {
        return (Stripe) this.stripe$delegate.getValue();
    }

    private final AddPaymentMethodViewModel getViewModel() {
        return (AddPaymentMethodViewModel) this.viewModel$delegate.getValue();
    }

    private final int getTitleStringRes() {
        int i = WhenMappings.$EnumSwitchMapping$0[getPaymentMethodType().ordinal()];
        if (i == 1) {
            return R.string.title_add_a_card;
        }
        if (i == 2) {
            return R.string.title_bank_account;
        }
        throw new IllegalArgumentException("Unsupported Payment Method type: " + getPaymentMethodType().code);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        configureView(getArgs());
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        getAddPaymentMethodView().requestFocus();
    }

    private final void configureView(AddPaymentMethodActivityStarter.Args args) {
        Integer windowFlags$stripe_release = args.getWindowFlags$stripe_release();
        if (windowFlags$stripe_release != null) {
            getWindow().addFlags(windowFlags$stripe_release.intValue());
        }
        getViewStub$stripe_release().setLayoutResource(R.layout.add_payment_method_activity);
        View inflate = getViewStub$stripe_release().inflate();
        if (inflate != null) {
            AddPaymentMethodActivityBinding bind = AddPaymentMethodActivityBinding.bind((ViewGroup) inflate);
            Intrinsics.checkExpressionValueIsNotNull(bind, "AddPaymentMethodActivityBinding.bind(scrollView)");
            bind.root.addView(getAddPaymentMethodView());
            LinearLayout linearLayout = bind.root;
            Intrinsics.checkExpressionValueIsNotNull(linearLayout, "viewBinding.root");
            View createFooterView = createFooterView(linearLayout);
            if (createFooterView != null) {
                if (Build.VERSION.SDK_INT >= 22) {
                    getAddPaymentMethodView().setAccessibilityTraversalBefore(createFooterView.getId());
                    createFooterView.setAccessibilityTraversalAfter(getAddPaymentMethodView().getId());
                }
                bind.root.addView(createFooterView);
            }
            setTitle(getTitleStringRes());
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type android.view.ViewGroup");
    }

    /* access modifiers changed from: private */
    public final AddPaymentMethodView createPaymentMethodView(AddPaymentMethodActivityStarter.Args args) {
        int i = WhenMappings.$EnumSwitchMapping$1[getPaymentMethodType().ordinal()];
        if (i == 1) {
            return new AddPaymentMethodCardView(this, (AttributeSet) null, 0, args.getBillingAddressFields$stripe_release(), 6, (DefaultConstructorMarker) null);
        }
        if (i == 2) {
            return AddPaymentMethodFpxView.Companion.create$stripe_release(this);
        }
        throw new IllegalArgumentException("Unsupported Payment Method type: " + getPaymentMethodType().code);
    }

    private final View createFooterView(ViewGroup viewGroup) {
        if (getArgs().getAddPaymentMethodFooterLayoutId$stripe_release() <= 0) {
            return null;
        }
        View inflate = getLayoutInflater().inflate(getArgs().getAddPaymentMethodFooterLayoutId$stripe_release(), viewGroup, false);
        Intrinsics.checkExpressionValueIsNotNull(inflate, "footerView");
        inflate.setId(R.id.stripe_add_payment_method_footer);
        if (!(inflate instanceof TextView)) {
            return inflate;
        }
        TextView textView = (TextView) inflate;
        LinkifyCompat.addLinks(textView, 15);
        ViewCompat.enableAccessibleClickableSpanSupport(inflate);
        textView.setMovementMethod(LinkMovementMethod.getInstance());
        return inflate;
    }

    public void onActionSave() {
        createPaymentMethod$stripe_release(getViewModel(), getAddPaymentMethodView().getCreateParams());
    }

    public final void createPaymentMethod$stripe_release(AddPaymentMethodViewModel addPaymentMethodViewModel, PaymentMethodCreateParams paymentMethodCreateParams) {
        Intrinsics.checkParameterIsNotNull(addPaymentMethodViewModel, "viewModel");
        if (paymentMethodCreateParams != null) {
            setProgressBarVisible(true);
            addPaymentMethodViewModel.createPaymentMethod$stripe_release(paymentMethodCreateParams).observe(this, new AddPaymentMethodActivity$createPaymentMethod$1(this));
        }
    }

    /* access modifiers changed from: private */
    public final void attachPaymentMethodToCustomer(PaymentMethod paymentMethod) {
        getViewModel().attachPaymentMethod$stripe_release(paymentMethod).observe(this, new AddPaymentMethodActivity$attachPaymentMethodToCustomer$1(this));
    }

    /* access modifiers changed from: private */
    public final void finishWithPaymentMethod(PaymentMethod paymentMethod) {
        setProgressBarVisible(false);
        setResult(-1, new Intent().putExtras(new AddPaymentMethodActivityStarter.Result(paymentMethod).toBundle()));
        finish();
    }

    /* access modifiers changed from: protected */
    public void onProgressBarVisibilityChanged(boolean z) {
        getAddPaymentMethodView().setCommunicatingProgress(z);
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\u0005"}, d2 = {"Lcom/stripe/android/view/AddPaymentMethodActivity$Companion;", "", "()V", "PRODUCT_TOKEN", "", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: AddPaymentMethodActivity.kt */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }
}
