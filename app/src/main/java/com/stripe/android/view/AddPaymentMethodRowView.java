package com.stripe.android.view;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import com.stripe.android.R;
import com.stripe.android.databinding.AddPaymentMethodRowBinding;
import com.stripe.android.model.PaymentMethod;
import com.stripe.android.view.AddPaymentMethodActivityStarter;
import com.stripe.android.view.PaymentMethodsActivityStarter;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\b\u0000\u0018\u0000 \u000e2\u00020\u0001:\u0001\u000eB+\b\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0001\u0010\u0004\u001a\u00020\u0005\u0012\b\b\u0001\u0010\u0006\u001a\u00020\u0005\u0012\u0006\u0010\u0007\u001a\u00020\b¢\u0006\u0002\u0010\tJ\b\u0010\f\u001a\u00020\rH\u0014R\u000e\u0010\u0007\u001a\u00020\bX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0004¢\u0006\u0002\n\u0000¨\u0006\u000f"}, d2 = {"Lcom/stripe/android/view/AddPaymentMethodRowView;", "Landroid/widget/FrameLayout;", "context", "Landroid/content/Context;", "idRes", "", "labelRes", "args", "Lcom/stripe/android/view/AddPaymentMethodActivityStarter$Args;", "(Landroid/content/Context;IILcom/stripe/android/view/AddPaymentMethodActivityStarter$Args;)V", "viewBinding", "Lcom/stripe/android/databinding/AddPaymentMethodRowBinding;", "onAttachedToWindow", "", "Companion", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: AddPaymentMethodRowView.kt */
public final class AddPaymentMethodRowView extends FrameLayout {
    public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
    /* access modifiers changed from: private */
    public final AddPaymentMethodActivityStarter.Args args;
    private final int labelRes;
    private final AddPaymentMethodRowBinding viewBinding;

    public /* synthetic */ AddPaymentMethodRowView(Context context, int i, int i2, AddPaymentMethodActivityStarter.Args args2, DefaultConstructorMarker defaultConstructorMarker) {
        this(context, i, i2, args2);
    }

    private AddPaymentMethodRowView(Context context, int i, int i2, AddPaymentMethodActivityStarter.Args args2) {
        super(context);
        this.labelRes = i2;
        this.args = args2;
        AddPaymentMethodRowBinding inflate = AddPaymentMethodRowBinding.inflate(LayoutInflater.from(context), this, true);
        Intrinsics.checkExpressionValueIsNotNull(inflate, "AddPaymentMethodRowBindi… this,\n        true\n    )");
        this.viewBinding = inflate;
        setId(i);
        setLayoutParams(new ViewGroup.LayoutParams(-1, -2));
        setFocusable(true);
        setClickable(true);
        setContentDescription(context.getString(this.labelRes));
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.viewBinding.label.setText(this.labelRes);
        Context context = getContext();
        if (!(context instanceof Activity)) {
            context = null;
        }
        Activity activity = (Activity) context;
        if (activity != null) {
            setOnClickListener(new AddPaymentMethodRowView$onAttachedToWindow$$inlined$let$lambda$1(activity, this));
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u001d\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bH\u0000¢\u0006\u0002\b\tJ\u001d\u0010\n\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bH\u0000¢\u0006\u0002\b\u000b¨\u0006\f"}, d2 = {"Lcom/stripe/android/view/AddPaymentMethodRowView$Companion;", "", "()V", "createCard", "Lcom/stripe/android/view/AddPaymentMethodRowView;", "activity", "Landroid/app/Activity;", "args", "Lcom/stripe/android/view/PaymentMethodsActivityStarter$Args;", "createCard$stripe_release", "createFpx", "createFpx$stripe_release", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: AddPaymentMethodRowView.kt */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        public final AddPaymentMethodRowView createCard$stripe_release(Activity activity, PaymentMethodsActivityStarter.Args args) {
            Intrinsics.checkParameterIsNotNull(activity, "activity");
            Intrinsics.checkParameterIsNotNull(args, "args");
            return new AddPaymentMethodRowView(activity, R.id.stripe_payment_methods_add_card, R.string.payment_method_add_new_card, new AddPaymentMethodActivityStarter.Args.Builder().setBillingAddressFields(args.getBillingAddressFields$stripe_release()).setShouldAttachToCustomer(true).setIsPaymentSessionActive$stripe_release(args.isPaymentSessionActive$stripe_release()).setPaymentMethodType(PaymentMethod.Type.Card).setAddPaymentMethodFooter(args.getAddPaymentMethodFooterLayoutId()).setPaymentConfiguration$stripe_release(args.getPaymentConfiguration$stripe_release()).setWindowFlags(args.getWindowFlags$stripe_release()).build(), (DefaultConstructorMarker) null);
        }

        public final AddPaymentMethodRowView createFpx$stripe_release(Activity activity, PaymentMethodsActivityStarter.Args args) {
            Intrinsics.checkParameterIsNotNull(activity, "activity");
            Intrinsics.checkParameterIsNotNull(args, "args");
            return new AddPaymentMethodRowView(activity, R.id.stripe_payment_methods_add_fpx, R.string.payment_method_add_new_fpx, new AddPaymentMethodActivityStarter.Args.Builder().setIsPaymentSessionActive$stripe_release(args.isPaymentSessionActive$stripe_release()).setPaymentMethodType(PaymentMethod.Type.Fpx).setPaymentConfiguration$stripe_release(args.getPaymentConfiguration$stripe_release()).build(), (DefaultConstructorMarker) null);
        }
    }
}
