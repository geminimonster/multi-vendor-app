package com.stripe.android.view;

import android.content.Context;
import android.text.InputFilter;
import android.text.method.DigitsKeyListener;
import android.util.AttributeSet;
import androidx.appcompat.R;
import com.stripe.android.view.BecsDebitBanks;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.collections.CollectionsKt;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.text.StringsKt;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000P\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\t\b\u0000\u0018\u0000 &2\u00020\u0001:\u0001&B%\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\b\b\u0002\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ\u0010\u0010#\u001a\u00020\u00102\u0006\u0010\u000f\u001a\u00020\u0010H\u0002J\u0010\u0010$\u001a\u00020\u00182\u0006\u0010%\u001a\u00020\u0014H\u0002R\u0016\u0010\t\u001a\u0004\u0018\u00010\n8BX\u0004¢\u0006\u0006\u001a\u0004\b\u000b\u0010\fR\u000e\u0010\r\u001a\u00020\u000eX\u0004¢\u0006\u0002\n\u0000R\u0016\u0010\u000f\u001a\u0004\u0018\u00010\u00108@X\u0004¢\u0006\u0006\u001a\u0004\b\u0011\u0010\u0012R\u0014\u0010\u0013\u001a\u00020\u00148BX\u0004¢\u0006\u0006\u001a\u0004\b\u0013\u0010\u0015R(\u0010\u0016\u001a\u0010\u0012\u0006\u0012\u0004\u0018\u00010\n\u0012\u0004\u0012\u00020\u00180\u0017X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0019\u0010\u001a\"\u0004\b\u001b\u0010\u001cR \u0010\u001d\u001a\b\u0012\u0004\u0012\u00020\u00180\u001eX\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u001f\u0010 \"\u0004\b!\u0010\"¨\u0006'"}, d2 = {"Lcom/stripe/android/view/BecsDebitBsbEditText;", "Lcom/stripe/android/view/StripeEditText;", "context", "Landroid/content/Context;", "attrs", "Landroid/util/AttributeSet;", "defStyleAttr", "", "(Landroid/content/Context;Landroid/util/AttributeSet;I)V", "bank", "Lcom/stripe/android/view/BecsDebitBanks$Bank;", "getBank", "()Lcom/stripe/android/view/BecsDebitBanks$Bank;", "banks", "Lcom/stripe/android/view/BecsDebitBanks;", "bsb", "", "getBsb$stripe_release", "()Ljava/lang/String;", "isComplete", "", "()Z", "onBankChangedCallback", "Lkotlin/Function1;", "", "getOnBankChangedCallback", "()Lkotlin/jvm/functions/Function1;", "setOnBankChangedCallback", "(Lkotlin/jvm/functions/Function1;)V", "onCompletedCallback", "Lkotlin/Function0;", "getOnCompletedCallback", "()Lkotlin/jvm/functions/Function0;", "setOnCompletedCallback", "(Lkotlin/jvm/functions/Function0;)V", "formatBsb", "updateIcon", "isError", "Companion", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: BecsDebitBsbEditText.kt */
public final class BecsDebitBsbEditText extends StripeEditText {
    @Deprecated
    public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
    private static final int MAX_LENGTH = 7;
    private static final int MIN_VALIDATION_THRESHOLD = 2;
    private static final String SEPARATOR = "-";
    private final BecsDebitBanks banks;
    private Function1<? super BecsDebitBanks.Bank, Unit> onBankChangedCallback;
    private Function0<Unit> onCompletedCallback;

    public BecsDebitBsbEditText(Context context) {
        this(context, (AttributeSet) null, 0, 6, (DefaultConstructorMarker) null);
    }

    public BecsDebitBsbEditText(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0, 4, (DefaultConstructorMarker) null);
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ BecsDebitBsbEditText(Context context, AttributeSet attributeSet, int i, int i2, DefaultConstructorMarker defaultConstructorMarker) {
        this(context, (i2 & 2) != 0 ? null : attributeSet, (i2 & 4) != 0 ? R.attr.editTextStyle : i);
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BecsDebitBsbEditText(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        Intrinsics.checkParameterIsNotNull(context, "context");
        this.banks = new BecsDebitBanks(context, false, 2, (DefaultConstructorMarker) null);
        this.onBankChangedCallback = BecsDebitBsbEditText$onBankChangedCallback$1.INSTANCE;
        this.onCompletedCallback = BecsDebitBsbEditText$onCompletedCallback$1.INSTANCE;
        setFilters((InputFilter[]) new InputFilter.LengthFilter[]{new InputFilter.LengthFilter(7)});
        setKeyListener(DigitsKeyListener.getInstance(false, true));
        addTextChangedListener(new StripeTextWatcher(this) {
            private String formattedBsb;
            private boolean ignoreChanges;
            private Integer newCursorPosition;
            final /* synthetic */ BecsDebitBsbEditText this$0;

            {
                this.this$0 = r1;
            }

            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                String str;
                if (!this.ignoreChanges && i <= 4) {
                    Integer num = null;
                    if (charSequence != null) {
                        str = charSequence.toString();
                    } else {
                        str = null;
                    }
                    if (str == null) {
                        str = "";
                    }
                    CharSequence charSequence2 = str;
                    Appendable sb = new StringBuilder();
                    int length = charSequence2.length();
                    for (int i4 = 0; i4 < length; i4++) {
                        char charAt = charSequence2.charAt(i4);
                        if (Character.isDigit(charAt)) {
                            sb.append(charAt);
                        }
                    }
                    String sb2 = ((StringBuilder) sb).toString();
                    Intrinsics.checkExpressionValueIsNotNull(sb2, "filterTo(StringBuilder(), predicate).toString()");
                    String access$formatBsb = this.this$0.formatBsb(sb2);
                    this.formattedBsb = access$formatBsb;
                    if (access$formatBsb != null) {
                        num = Integer.valueOf(access$formatBsb.length());
                    }
                    this.newCursorPosition = num;
                }
            }

            /* JADX WARNING: Removed duplicated region for block: B:18:0x0063  */
            /* JADX WARNING: Removed duplicated region for block: B:21:0x0079  */
            /* JADX WARNING: Removed duplicated region for block: B:24:0x0099  */
            /* JADX WARNING: Removed duplicated region for block: B:26:? A[RETURN, SYNTHETIC] */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public void afterTextChanged(android.text.Editable r6) {
                /*
                    r5 = this;
                    boolean r6 = r5.ignoreChanges
                    if (r6 == 0) goto L_0x0005
                    return
                L_0x0005:
                    r6 = 1
                    r5.ignoreChanges = r6
                    com.stripe.android.view.BecsDebitBsbEditText r0 = r5.this$0
                    boolean r0 = r0.isLastKeyDelete()
                    r1 = 0
                    if (r0 != 0) goto L_0x0037
                    java.lang.String r0 = r5.formattedBsb
                    if (r0 == 0) goto L_0x0037
                    com.stripe.android.view.BecsDebitBsbEditText r2 = r5.this$0
                    java.lang.CharSequence r0 = (java.lang.CharSequence) r0
                    r2.setText(r0)
                    java.lang.Integer r0 = r5.newCursorPosition
                    if (r0 == 0) goto L_0x0037
                    java.lang.Number r0 = (java.lang.Number) r0
                    int r0 = r0.intValue()
                    com.stripe.android.view.BecsDebitBsbEditText r2 = r5.this$0
                    java.lang.String r3 = r2.getFieldText$stripe_release()
                    int r3 = r3.length()
                    int r0 = kotlin.ranges.RangesKt.coerceIn((int) r0, (int) r1, (int) r3)
                    r2.setSelection(r0)
                L_0x0037:
                    r0 = 0
                    r2 = r0
                    java.lang.String r2 = (java.lang.String) r2
                    r5.formattedBsb = r2
                    r2 = r0
                    java.lang.Integer r2 = (java.lang.Integer) r2
                    r5.newCursorPosition = r2
                    r5.ignoreChanges = r1
                    com.stripe.android.view.BecsDebitBsbEditText r2 = r5.this$0
                    com.stripe.android.view.BecsDebitBanks$Bank r2 = r2.getBank()
                    if (r2 != 0) goto L_0x005e
                    com.stripe.android.view.BecsDebitBsbEditText r2 = r5.this$0
                    java.lang.String r2 = r2.getFieldText$stripe_release()
                    int r2 = r2.length()
                    com.stripe.android.view.BecsDebitBsbEditText.Companion unused = com.stripe.android.view.BecsDebitBsbEditText.Companion
                    r3 = 2
                    if (r2 < r3) goto L_0x005e
                    r2 = 1
                    goto L_0x005f
                L_0x005e:
                    r2 = 0
                L_0x005f:
                    com.stripe.android.view.BecsDebitBsbEditText r3 = r5.this$0
                    if (r2 == 0) goto L_0x006d
                    android.content.res.Resources r0 = r3.getResources()
                    int r4 = com.stripe.android.R.string.becs_widget_bsb_invalid
                    java.lang.String r0 = r0.getString(r4)
                L_0x006d:
                    r3.setErrorMessage$stripe_release(r0)
                    com.stripe.android.view.BecsDebitBsbEditText r0 = r5.this$0
                    java.lang.String r3 = r0.getErrorMessage$stripe_release()
                    if (r3 == 0) goto L_0x0079
                    goto L_0x007a
                L_0x0079:
                    r6 = 0
                L_0x007a:
                    r0.setShouldShowError(r6)
                    com.stripe.android.view.BecsDebitBsbEditText r6 = r5.this$0
                    kotlin.jvm.functions.Function1 r6 = r6.getOnBankChangedCallback()
                    com.stripe.android.view.BecsDebitBsbEditText r0 = r5.this$0
                    com.stripe.android.view.BecsDebitBanks$Bank r0 = r0.getBank()
                    r6.invoke(r0)
                    com.stripe.android.view.BecsDebitBsbEditText r6 = r5.this$0
                    r6.updateIcon(r2)
                    com.stripe.android.view.BecsDebitBsbEditText r6 = r5.this$0
                    boolean r6 = r6.isComplete()
                    if (r6 == 0) goto L_0x00a2
                    com.stripe.android.view.BecsDebitBsbEditText r6 = r5.this$0
                    kotlin.jvm.functions.Function0 r6 = r6.getOnCompletedCallback()
                    r6.invoke()
                L_0x00a2:
                    return
                */
                throw new UnsupportedOperationException("Method not decompiled: com.stripe.android.view.BecsDebitBsbEditText.AnonymousClass1.afterTextChanged(android.text.Editable):void");
            }
        });
    }

    public final Function1<BecsDebitBanks.Bank, Unit> getOnBankChangedCallback() {
        return this.onBankChangedCallback;
    }

    public final void setOnBankChangedCallback(Function1<? super BecsDebitBanks.Bank, Unit> function1) {
        Intrinsics.checkParameterIsNotNull(function1, "<set-?>");
        this.onBankChangedCallback = function1;
    }

    public final Function0<Unit> getOnCompletedCallback() {
        return this.onCompletedCallback;
    }

    public final void setOnCompletedCallback(Function0<Unit> function0) {
        Intrinsics.checkParameterIsNotNull(function0, "<set-?>");
        this.onCompletedCallback = function0;
    }

    public final String getBsb$stripe_release() {
        String str;
        if (getFieldText$stripe_release().length() < 2) {
            str = getResources().getString(com.stripe.android.R.string.becs_widget_bsb_incomplete);
        } else if (getBank() == null) {
            str = getResources().getString(com.stripe.android.R.string.becs_widget_bsb_invalid);
        } else {
            str = getFieldText$stripe_release().length() < 7 ? getResources().getString(com.stripe.android.R.string.becs_widget_bsb_incomplete) : null;
        }
        setErrorMessage$stripe_release(str);
        CharSequence fieldText$stripe_release = getFieldText$stripe_release();
        Appendable sb = new StringBuilder();
        int length = fieldText$stripe_release.length();
        for (int i = 0; i < length; i++) {
            char charAt = fieldText$stripe_release.charAt(i);
            if (Character.isDigit(charAt)) {
                sb.append(charAt);
            }
        }
        String sb2 = ((StringBuilder) sb).toString();
        Intrinsics.checkExpressionValueIsNotNull(sb2, "filterTo(StringBuilder(), predicate).toString()");
        if (isComplete()) {
            return sb2;
        }
        return null;
    }

    /* access modifiers changed from: private */
    public final boolean isComplete() {
        return getBank() != null && getFieldText$stripe_release().length() == 7;
    }

    /* access modifiers changed from: private */
    public final BecsDebitBanks.Bank getBank() {
        return this.banks.byPrefix(getFieldText$stripe_release());
    }

    /* access modifiers changed from: private */
    public final void updateIcon(boolean z) {
        int i;
        if (z) {
            i = com.stripe.android.R.drawable.stripe_ic_bank_error;
        } else {
            i = com.stripe.android.R.drawable.stripe_ic_bank;
        }
        setCompoundDrawablesRelativeWithIntrinsicBounds(i, 0, 0, 0);
    }

    /* access modifiers changed from: private */
    public final String formatBsb(String str) {
        if (str.length() < 3) {
            return str;
        }
        return CollectionsKt.joinToString$default(CollectionsKt.listOf(StringsKt.take(str, 3), StringsKt.takeLast(str, str.length() - 3)), SEPARATOR, (CharSequence) null, (CharSequence) null, 0, (CharSequence) null, (Function1) null, 62, (Object) null);
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007XT¢\u0006\u0002\n\u0000¨\u0006\b"}, d2 = {"Lcom/stripe/android/view/BecsDebitBsbEditText$Companion;", "", "()V", "MAX_LENGTH", "", "MIN_VALIDATION_THRESHOLD", "SEPARATOR", "", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: BecsDebitBsbEditText.kt */
    private static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }
}
