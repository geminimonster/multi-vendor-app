package com.stripe.android.view;

import com.stripe.android.view.PaymentMethodsAdapter;
import kotlin.Metadata;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n¢\u0006\u0002\b\u0002"}, d2 = {"<anonymous>", "", "run"}, k = 3, mv = {1, 1, 16})
/* compiled from: PaymentMethodsAdapter.kt */
final class PaymentMethodsAdapter$onPositionClicked$1 implements Runnable {
    final /* synthetic */ int $position;
    final /* synthetic */ PaymentMethodsAdapter this$0;

    PaymentMethodsAdapter$onPositionClicked$1(PaymentMethodsAdapter paymentMethodsAdapter, int i) {
        this.this$0 = paymentMethodsAdapter;
        this.$position = i;
    }

    public final void run() {
        PaymentMethodsAdapter.Listener listener$stripe_release = this.this$0.getListener$stripe_release();
        if (listener$stripe_release != null) {
            listener$stripe_release.onPaymentMethodClick(this.this$0.getPaymentMethodAtPosition$stripe_release(this.$position));
        }
    }
}
