package com.stripe.android.view;

import android.app.Application;
import android.content.Context;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.CoroutineLiveDataKt;
import androidx.lifecycle.LiveData;
import com.stripe.android.AnalyticsDataFactory;
import com.stripe.android.AnalyticsRequestExecutor;
import com.stripe.android.ApiFingerprintParamsFactory;
import com.stripe.android.ApiRequestExecutor;
import com.stripe.android.AppInfo;
import com.stripe.android.FingerprintDataRepository;
import com.stripe.android.FingerprintParamsUtils;
import com.stripe.android.Logger;
import com.stripe.android.PaymentConfiguration;
import com.stripe.android.StripeApiRepository;
import com.stripe.android.model.FpxBankStatuses;
import java.util.concurrent.CancellationException;
import kotlin.Metadata;
import kotlin.coroutines.Continuation;
import kotlin.coroutines.CoroutineContext;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.Intrinsics;
import kotlinx.coroutines.Dispatchers;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\b\u0000\u0018\u00002\u00020\u0001B\u0019\b\u0001\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0002\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\u0013\u0010\u0012\u001a\b\u0012\u0004\u0012\u00020\u00140\u0013H\u0000¢\u0006\u0002\b\u0015J\b\u0010\u0016\u001a\u00020\u0017H\u0014R\u000e\u0010\u0007\u001a\u00020\bX\u0004¢\u0006\u0002\n\u0000R\u001e\u0010\t\u001a\u0004\u0018\u00010\nX\u000e¢\u0006\u0010\n\u0002\u0010\u000f\u001a\u0004\b\u000b\u0010\f\"\u0004\b\r\u0010\u000eR\u000e\u0010\u0010\u001a\u00020\u0011X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0018"}, d2 = {"Lcom/stripe/android/view/FpxViewModel;", "Landroidx/lifecycle/AndroidViewModel;", "application", "Landroid/app/Application;", "workContext", "Lkotlin/coroutines/CoroutineContext;", "(Landroid/app/Application;Lkotlin/coroutines/CoroutineContext;)V", "publishableKey", "", "selectedPosition", "", "getSelectedPosition$stripe_release", "()Ljava/lang/Integer;", "setSelectedPosition$stripe_release", "(Ljava/lang/Integer;)V", "Ljava/lang/Integer;", "stripeRepository", "Lcom/stripe/android/StripeApiRepository;", "getFpxBankStatues", "Landroidx/lifecycle/LiveData;", "Lcom/stripe/android/model/FpxBankStatuses;", "getFpxBankStatues$stripe_release", "onCleared", "", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: FpxViewModel.kt */
public final class FpxViewModel extends AndroidViewModel {
    /* access modifiers changed from: private */
    public final String publishableKey;
    private Integer selectedPosition;
    /* access modifiers changed from: private */
    public final StripeApiRepository stripeRepository;
    private final CoroutineContext workContext;

    public FpxViewModel(Application application) {
        this(application, (CoroutineContext) null, 2, (DefaultConstructorMarker) null);
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ FpxViewModel(Application application, CoroutineContext coroutineContext, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(application, (i & 2) != 0 ? Dispatchers.getIO() : coroutineContext);
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FpxViewModel(Application application, CoroutineContext coroutineContext) {
        super(application);
        Application application2 = application;
        CoroutineContext coroutineContext2 = coroutineContext;
        Intrinsics.checkParameterIsNotNull(application2, "application");
        Intrinsics.checkParameterIsNotNull(coroutineContext2, "workContext");
        this.workContext = coroutineContext2;
        Context context = application2;
        this.publishableKey = PaymentConfiguration.Companion.getInstance(context).getPublishableKey();
        this.stripeRepository = new StripeApiRepository(context, this.publishableKey, (AppInfo) null, (Logger) null, (ApiRequestExecutor) null, (AnalyticsRequestExecutor) null, (FingerprintDataRepository) null, (ApiFingerprintParamsFactory) null, (AnalyticsDataFactory) null, (FingerprintParamsUtils) null, this.workContext, (String) null, (String) null, 7164, (DefaultConstructorMarker) null);
    }

    public final Integer getSelectedPosition$stripe_release() {
        return this.selectedPosition;
    }

    public final void setSelectedPosition$stripe_release(Integer num) {
        this.selectedPosition = num;
    }

    public final /* synthetic */ LiveData<FpxBankStatuses> getFpxBankStatues$stripe_release() {
        return CoroutineLiveDataKt.liveData$default(this.workContext, 0, (Function2) new FpxViewModel$getFpxBankStatues$1(this, (Continuation) null), 2, (Object) null);
    }

    /* access modifiers changed from: protected */
    public void onCleared() {
        super.onCleared();
        JobKt__JobKt.cancelChildren$default(this.workContext, (CancellationException) null, 1, (Object) null);
    }
}
