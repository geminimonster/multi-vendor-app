package com.stripe.android.view;

import androidx.lifecycle.Observer;
import com.stripe.android.model.Customer;
import java.util.List;
import kotlin.Metadata;
import kotlin.Result;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\b\u0003\u0010\u0000\u001a\u00020\u00012\u001a\u0010\u0002\u001a\u0016\u0012\u0004\u0012\u00020\u0004 \u0005*\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u00030\u0003H\n¢\u0006\u0002\b\u0006¨\u0006\u0007"}, d2 = {"<anonymous>", "", "result", "Lkotlin/Result;", "Lcom/stripe/android/model/Customer;", "kotlin.jvm.PlatformType", "onChanged", "com/stripe/android/view/PaymentFlowActivity$onShippingInfoValidated$1$1"}, k = 3, mv = {1, 1, 16})
/* compiled from: PaymentFlowActivity.kt */
final class PaymentFlowActivity$onShippingInfoValidated$$inlined$let$lambda$1<T> implements Observer<Result<? extends Customer>> {
    final /* synthetic */ List $shippingMethods$inlined;
    final /* synthetic */ PaymentFlowActivity this$0;

    PaymentFlowActivity$onShippingInfoValidated$$inlined$let$lambda$1(PaymentFlowActivity paymentFlowActivity, List list) {
        this.this$0 = paymentFlowActivity;
        this.$shippingMethods$inlined = list;
    }

    public final void onChanged(Result<? extends Customer> result) {
        Object r3 = result.m13unboximpl();
        Throwable r0 = Result.m7exceptionOrNullimpl(r3);
        if (r0 == null) {
            this.this$0.onShippingInfoSaved$stripe_release(((Customer) r3).getShippingInformation(), this.$shippingMethods$inlined);
            return;
        }
        PaymentFlowActivity paymentFlowActivity = this.this$0;
        String message = r0.getMessage();
        if (message == null) {
            message = "";
        }
        paymentFlowActivity.showError(message);
    }
}
