package com.stripe.android.view;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import androidx.core.content.ContextCompat;
import androidx.core.graphics.drawable.DrawableCompat;
import com.google.android.material.textfield.TextInputLayout;
import com.stripe.android.CardUtils;
import com.stripe.android.R;
import com.stripe.android.databinding.CardMultilineWidgetBinding;
import com.stripe.android.model.Address;
import com.stripe.android.model.Card;
import com.stripe.android.model.CardBrand;
import com.stripe.android.model.PaymentMethod;
import com.stripe.android.model.PaymentMethodCreateParams;
import com.stripe.android.view.CardValidCallback;
import com.stripe.android.view.PostalCodeEditText;
import com.stripe.android.view.StripeEditText;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Collection;
import java.util.Map;
import java.util.Set;
import kotlin.Metadata;
import kotlin.Pair;
import kotlin.TypeCastException;
import kotlin.Unit;
import kotlin.collections.CollectionsKt;
import kotlin.collections.SetsKt;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.MutablePropertyReference1Impl;
import kotlin.jvm.internal.Reflection;
import kotlin.properties.Delegates;
import kotlin.properties.ReadWriteProperty;
import kotlin.reflect.KProperty;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000ñ\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\u001e\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0006\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0017\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0010\n\u0002\u0018\u0002\n\u0002\b\u001b*\u0001+\u0018\u0000 ¨\u00012\u00020\u00012\u00020\u0002:\u0002¨\u0001B/\b\u0007\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\n\b\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u0012\b\b\u0002\u0010\u0007\u001a\u00020\b\u0012\b\b\u0002\u0010\t\u001a\u00020\n¢\u0006\u0002\u0010\u000bJ\u0010\u0010v\u001a\u00020w2\u0006\u0010\t\u001a\u00020\nH\u0002J\u0010\u0010x\u001a\u00020w2\u0006\u0010\u0005\u001a\u00020\u0006H\u0002J\b\u0010y\u001a\u00020wH\u0016J\u0010\u0010z\u001a\u00020{2\u0006\u0010|\u001a\u00020}H\u0002J\b\u0010~\u001a\u00020wH\u0002J\b\u0010\u001a\u00020wH\u0002J\t\u0010\u0001\u001a\u00020wH\u0002J+\u0010\u0001\u001a\u00020w2\u0007\u0010\u0001\u001a\u0002062\u0007\u0010\u0001\u001a\u0002062\u0006\u00105\u001a\u0002062\u0006\u0010d\u001a\u000206H\u0002J\b\u0010I\u001a\u00020\nH\u0016J\t\u0010\u0001\u001a\u00020wH\u0014J\u0012\u0010\u0001\u001a\u00020w2\u0007\u0010\u0001\u001a\u00020\nH\u0016J\u0012\u0010\u0001\u001a\u00020w2\u0007\u0010\u0001\u001a\u00020\u001cH\u0016J\u0014\u0010\u0001\u001a\u00020w2\t\u0010\u0001\u001a\u0004\u0018\u00010\u001eH\u0016J\u0013\u0010\u0001\u001a\u00020w2\b\u0010\u001f\u001a\u0004\u0018\u00010\u001cH\u0016J\u0015\u0010\u0001\u001a\u00020w2\n\u0010\u0001\u001a\u0005\u0018\u00010\u0001H\u0016J\u0014\u0010\u0001\u001a\u00020w2\t\u0010\u0001\u001a\u0004\u0018\u00010)H\u0016J\u0014\u0010\u0001\u001a\u00020w2\t\u0010\u0001\u001a\u0004\u0018\u00010\u001cH\u0016J\u0012\u0010\u0001\u001a\u00020w2\t\u0010\u0001\u001a\u0004\u0018\u00010\u001cJ\u0015\u0010\u0001\u001a\u00020w2\n\u0010\u0001\u001a\u0005\u0018\u00010\u0001H\u0016J\u0012\u0010\u0001\u001a\u00020w2\u0007\u0010\u0001\u001a\u00020\nH\u0016J\u001f\u0010\u0001\u001a\u00020w2\t\b\u0001\u0010\u0001\u001a\u00020\b2\t\b\u0001\u0010\u0001\u001a\u00020\bH\u0016J\u0015\u0010\u0001\u001a\u00020w2\n\u0010\u0001\u001a\u0005\u0018\u00010\u0001H\u0016J\u0015\u0010\u0001\u001a\u00020w2\n\u0010\u0001\u001a\u0005\u0018\u00010\u0001H\u0016J\u000f\u0010 \u0001\u001a\u00020w2\u0006\u0010\t\u001a\u00020\nJ\t\u0010¡\u0001\u001a\u00020wH\u0002J\t\u0010¢\u0001\u001a\u00020wH\u0002J\u001d\u0010£\u0001\u001a\u00020w2\t\b\u0001\u0010¤\u0001\u001a\u00020\b2\u0007\u0010¥\u0001\u001a\u00020\nH\u0002J\u0007\u0010¦\u0001\u001a\u00020\nJ\u0007\u0010§\u0001\u001a\u00020\nR\u001a\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u000e0\r8BX\u0004¢\u0006\u0006\u001a\u0004\b\u000f\u0010\u0010R\u0016\u0010\u0011\u001a\u0004\u0018\u00010\u00128VX\u0004¢\u0006\u0006\u001a\u0004\b\u0013\u0010\u0014R\u000e\u0010\u0015\u001a\u00020\u0016X\u000e¢\u0006\u0002\n\u0000R\u0016\u0010\u0017\u001a\u0004\u0018\u00010\u00188VX\u0004¢\u0006\u0006\u001a\u0004\b\u0019\u0010\u001aR\u000e\u0010\u001b\u001a\u00020\u001cX\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\u001d\u001a\u0004\u0018\u00010\u001eX\u000e¢\u0006\u0002\n\u0000R\u0016\u0010\u001f\u001a\u0004\u0018\u00010\u001c8BX\u0004¢\u0006\u0006\u001a\u0004\b \u0010!R\u0014\u0010\"\u001a\u00020#X\u0004¢\u0006\b\n\u0000\u001a\u0004\b$\u0010%R\u000e\u0010&\u001a\u00020'X\u0004¢\u0006\u0002\n\u0000R\u0010\u0010(\u001a\u0004\u0018\u00010)X\u000e¢\u0006\u0002\n\u0000R\u0010\u0010*\u001a\u00020+X\u0004¢\u0006\u0004\n\u0002\u0010,R\u0010\u0010-\u001a\u0004\u0018\u00010\u001cX\u000e¢\u0006\u0002\n\u0000R\u0014\u0010.\u001a\u00020/X\u0004¢\u0006\b\n\u0000\u001a\u0004\b0\u00101R\u0014\u00102\u001a\u00020\b8CX\u0004¢\u0006\u0006\u001a\u0004\b3\u00104R\u000e\u00105\u001a\u000206X\u0004¢\u0006\u0002\n\u0000R\u000e\u00107\u001a\u00020\bX\u0004¢\u0006\u0002\n\u0000R\"\u00108\u001a\u0010\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\b\u0018\u0001098BX\u0004¢\u0006\u0006\u001a\u0004\b:\u0010;R\u0014\u0010<\u001a\u00020=X\u0004¢\u0006\b\n\u0000\u001a\u0004\b>\u0010?R\u000e\u0010@\u001a\u000206X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010A\u001a\u00020\nX\u000e¢\u0006\u0002\n\u0000R\u001a\u0010B\u001a\b\u0012\u0004\u0012\u00020D0C8BX\u0004¢\u0006\u0006\u001a\u0004\bE\u0010FR\u0014\u0010G\u001a\u00020\n8BX\u0004¢\u0006\u0006\u001a\u0004\bG\u0010HR\u000e\u0010I\u001a\u00020\nX\u000e¢\u0006\u0002\n\u0000R\u0013\u0010J\u001a\u0004\u0018\u00010K8F¢\u0006\u0006\u001a\u0004\bL\u0010MR\u0013\u0010N\u001a\u0004\u0018\u00010O8F¢\u0006\u0006\u001a\u0004\bP\u0010QR\u0016\u0010R\u001a\u0004\u0018\u00010S8VX\u0004¢\u0006\u0006\u001a\u0004\bT\u0010UR\u0016\u0010V\u001a\u0004\u0018\u00010W8VX\u0004¢\u0006\u0006\u001a\u0004\bX\u0010YR\u000e\u0010Z\u001a\u00020[X\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\\\u001a\u00020]X\u0004¢\u0006\b\n\u0000\u001a\u0004\b^\u0010_R\u001a\u0010`\u001a\u00020\nX\u000e¢\u0006\u000e\n\u0000\u001a\u0004\ba\u0010H\"\u0004\bb\u0010cR\u0014\u0010d\u001a\u000206X\u0004¢\u0006\b\n\u0000\u001a\u0004\be\u0010fR,\u0010h\u001a\u00020\n2\u0006\u0010g\u001a\u00020\n8\u0000@BX\u000e¢\u0006\u0014\n\u0000\u0012\u0004\bi\u0010j\u001a\u0004\bk\u0010H\"\u0004\bl\u0010cR\u000e\u0010\t\u001a\u00020\nX\u000e¢\u0006\u0002\n\u0000R\u0010\u0010m\u001a\u00020\b8\u0002X\u0004¢\u0006\u0002\n\u0000R+\u0010o\u001a\u00020\n2\u0006\u0010n\u001a\u00020\n8F@FX\u0002¢\u0006\u0012\n\u0004\br\u0010s\u001a\u0004\bp\u0010H\"\u0004\bq\u0010cR\u000e\u0010t\u001a\u00020uX\u0004¢\u0006\u0002\n\u0000¨\u0006©\u0001"}, d2 = {"Lcom/stripe/android/view/CardMultilineWidget;", "Landroid/widget/LinearLayout;", "Lcom/stripe/android/view/CardWidget;", "context", "Landroid/content/Context;", "attrs", "Landroid/util/AttributeSet;", "defStyleAttr", "", "shouldShowPostalCode", "", "(Landroid/content/Context;Landroid/util/AttributeSet;IZ)V", "allFields", "", "Lcom/stripe/android/view/StripeEditText;", "getAllFields", "()Ljava/util/Collection;", "card", "Lcom/stripe/android/model/Card;", "getCard", "()Lcom/stripe/android/model/Card;", "cardBrand", "Lcom/stripe/android/model/CardBrand;", "cardBuilder", "Lcom/stripe/android/model/Card$Builder;", "getCardBuilder", "()Lcom/stripe/android/model/Card$Builder;", "cardHintText", "", "cardInputListener", "Lcom/stripe/android/view/CardInputListener;", "cardNumber", "getCardNumber", "()Ljava/lang/String;", "cardNumberEditText", "Lcom/stripe/android/view/CardNumberEditText;", "getCardNumberEditText$stripe_release", "()Lcom/stripe/android/view/CardNumberEditText;", "cardNumberTextInputLayout", "Lcom/stripe/android/view/IconTextInputLayout;", "cardValidCallback", "Lcom/stripe/android/view/CardValidCallback;", "cardValidTextWatcher", "com/stripe/android/view/CardMultilineWidget$cardValidTextWatcher$1", "Lcom/stripe/android/view/CardMultilineWidget$cardValidTextWatcher$1;", "customCvcLabel", "cvcEditText", "Lcom/stripe/android/view/CvcEditText;", "getCvcEditText$stripe_release", "()Lcom/stripe/android/view/CvcEditText;", "cvcHelperText", "getCvcHelperText", "()I", "cvcTextInputLayout", "Lcom/google/android/material/textfield/TextInputLayout;", "dynamicBufferInPixels", "expiryDate", "Lkotlin/Pair;", "getExpiryDate", "()Lkotlin/Pair;", "expiryDateEditText", "Lcom/stripe/android/view/ExpiryDateEditText;", "getExpiryDateEditText$stripe_release", "()Lcom/stripe/android/view/ExpiryDateEditText;", "expiryTextInputLayout", "hasAdjustedDrawable", "invalidFields", "", "Lcom/stripe/android/view/CardValidCallback$Fields;", "getInvalidFields", "()Ljava/util/Set;", "isCvcLengthValid", "()Z", "isEnabled", "paymentMethodBillingDetails", "Lcom/stripe/android/model/PaymentMethod$BillingDetails;", "getPaymentMethodBillingDetails", "()Lcom/stripe/android/model/PaymentMethod$BillingDetails;", "paymentMethodBillingDetailsBuilder", "Lcom/stripe/android/model/PaymentMethod$BillingDetails$Builder;", "getPaymentMethodBillingDetailsBuilder", "()Lcom/stripe/android/model/PaymentMethod$BillingDetails$Builder;", "paymentMethodCard", "Lcom/stripe/android/model/PaymentMethodCreateParams$Card;", "getPaymentMethodCard", "()Lcom/stripe/android/model/PaymentMethodCreateParams$Card;", "paymentMethodCreateParams", "Lcom/stripe/android/model/PaymentMethodCreateParams;", "getPaymentMethodCreateParams", "()Lcom/stripe/android/model/PaymentMethodCreateParams;", "pixelsToAdjust", "", "postalCodeEditText", "Lcom/stripe/android/view/PostalCodeEditText;", "getPostalCodeEditText$stripe_release", "()Lcom/stripe/android/view/PostalCodeEditText;", "postalCodeRequired", "getPostalCodeRequired", "setPostalCodeRequired", "(Z)V", "postalInputLayout", "getPostalInputLayout$stripe_release", "()Lcom/google/android/material/textfield/TextInputLayout;", "value", "shouldShowErrorIcon", "shouldShowErrorIcon$annotations", "()V", "getShouldShowErrorIcon$stripe_release", "setShouldShowErrorIcon", "tintColorInt", "<set-?>", "usZipCodeRequired", "getUsZipCodeRequired", "setUsZipCodeRequired", "usZipCodeRequired$delegate", "Lkotlin/properties/ReadWriteProperty;", "viewBinding", "Lcom/stripe/android/databinding/CardMultilineWidgetBinding;", "adjustViewForPostalCodeAttribute", "", "checkAttributeSet", "clear", "createDrawableBounds", "Landroid/graphics/Rect;", "drawable", "Landroid/graphics/drawable/Drawable;", "flipToCvcIconIfNotFinished", "initDeleteEmptyListeners", "initFocusChangeListeners", "initTextInputLayoutErrorHandlers", "cardInputLayout", "expiryInputLayout", "onFinishInflate", "onWindowFocusChanged", "hasWindowFocus", "setCardHint", "cardHint", "setCardInputListener", "listener", "setCardNumber", "setCardNumberTextWatcher", "cardNumberTextWatcher", "Landroid/text/TextWatcher;", "setCardValidCallback", "callback", "setCvcCode", "cvcCode", "setCvcLabel", "cvcLabel", "setCvcNumberTextWatcher", "cvcNumberTextWatcher", "setEnabled", "enabled", "setExpiryDate", "month", "year", "setExpiryDateTextWatcher", "expiryDateTextWatcher", "setPostalCodeTextWatcher", "postalCodeTextWatcher", "setShouldShowPostalCode", "updateBrandUi", "updateCvc", "updateDrawable", "iconResourceId", "shouldTint", "validateAllFields", "validateCardNumber", "Companion", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: CardMultilineWidget.kt */
public final class CardMultilineWidget extends LinearLayout implements CardWidget {
    static final /* synthetic */ KProperty[] $$delegatedProperties = {Reflection.mutableProperty1(new MutablePropertyReference1Impl(Reflection.getOrCreateKotlinClass(CardMultilineWidget.class), "usZipCodeRequired", "getUsZipCodeRequired()Z"))};
    private static final String CARD_MULTILINE_TOKEN = "CardMultilineView";
    private static final long CARD_NUMBER_HINT_DELAY = 120;
    private static final long COMMON_HINT_DELAY = 90;
    @Deprecated
    public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
    /* access modifiers changed from: private */
    public CardBrand cardBrand;
    /* access modifiers changed from: private */
    public String cardHintText;
    /* access modifiers changed from: private */
    public CardInputListener cardInputListener;
    private final CardNumberEditText cardNumberEditText;
    private final IconTextInputLayout cardNumberTextInputLayout;
    /* access modifiers changed from: private */
    public CardValidCallback cardValidCallback;
    private final CardMultilineWidget$cardValidTextWatcher$1 cardValidTextWatcher;
    private String customCvcLabel;
    private final CvcEditText cvcEditText;
    private final TextInputLayout cvcTextInputLayout;
    private final int dynamicBufferInPixels;
    private final ExpiryDateEditText expiryDateEditText;
    private final TextInputLayout expiryTextInputLayout;
    private boolean hasAdjustedDrawable;
    private boolean isEnabled;
    private final double pixelsToAdjust;
    private final PostalCodeEditText postalCodeEditText;
    private boolean postalCodeRequired;
    private final TextInputLayout postalInputLayout;
    /* access modifiers changed from: private */
    public boolean shouldShowErrorIcon;
    /* access modifiers changed from: private */
    public boolean shouldShowPostalCode;
    private final int tintColorInt;
    private final ReadWriteProperty usZipCodeRequired$delegate;
    private final CardMultilineWidgetBinding viewBinding;

    public CardMultilineWidget(Context context) {
        this(context, (AttributeSet) null, 0, false, 14, (DefaultConstructorMarker) null);
    }

    public CardMultilineWidget(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0, false, 12, (DefaultConstructorMarker) null);
    }

    public CardMultilineWidget(Context context, AttributeSet attributeSet, int i) {
        this(context, attributeSet, i, false, 8, (DefaultConstructorMarker) null);
    }

    public static /* synthetic */ void shouldShowErrorIcon$annotations() {
    }

    public final boolean getUsZipCodeRequired() {
        return ((Boolean) this.usZipCodeRequired$delegate.getValue(this, $$delegatedProperties[0])).booleanValue();
    }

    public final void setUsZipCodeRequired(boolean z) {
        this.usZipCodeRequired$delegate.setValue(this, $$delegatedProperties[0], Boolean.valueOf(z));
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ CardMultilineWidget(Context context, AttributeSet attributeSet, int i, boolean z, int i2, DefaultConstructorMarker defaultConstructorMarker) {
        this(context, (i2 & 2) != 0 ? null : attributeSet, (i2 & 4) != 0 ? 0 : i, (i2 & 8) != 0 ? true : z);
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CardMultilineWidget(Context context, AttributeSet attributeSet, int i, boolean z) {
        super(context, attributeSet, i);
        Intrinsics.checkParameterIsNotNull(context, "context");
        this.shouldShowPostalCode = z;
        CardMultilineWidgetBinding inflate = CardMultilineWidgetBinding.inflate(LayoutInflater.from(context), this);
        Intrinsics.checkExpressionValueIsNotNull(inflate, "CardMultilineWidgetBindi…text),\n        this\n    )");
        this.viewBinding = inflate;
        CardNumberEditText cardNumberEditText2 = inflate.etCardNumber;
        Intrinsics.checkExpressionValueIsNotNull(cardNumberEditText2, "viewBinding.etCardNumber");
        this.cardNumberEditText = cardNumberEditText2;
        ExpiryDateEditText expiryDateEditText2 = this.viewBinding.etExpiry;
        Intrinsics.checkExpressionValueIsNotNull(expiryDateEditText2, "viewBinding.etExpiry");
        this.expiryDateEditText = expiryDateEditText2;
        CvcEditText cvcEditText2 = this.viewBinding.etCvc;
        Intrinsics.checkExpressionValueIsNotNull(cvcEditText2, "viewBinding.etCvc");
        this.cvcEditText = cvcEditText2;
        PostalCodeEditText postalCodeEditText2 = this.viewBinding.etPostalCode;
        Intrinsics.checkExpressionValueIsNotNull(postalCodeEditText2, "viewBinding.etPostalCode");
        this.postalCodeEditText = postalCodeEditText2;
        IconTextInputLayout iconTextInputLayout = this.viewBinding.tlCardNumber;
        Intrinsics.checkExpressionValueIsNotNull(iconTextInputLayout, "viewBinding.tlCardNumber");
        this.cardNumberTextInputLayout = iconTextInputLayout;
        TextInputLayout textInputLayout = this.viewBinding.tlExpiry;
        Intrinsics.checkExpressionValueIsNotNull(textInputLayout, "viewBinding.tlExpiry");
        this.expiryTextInputLayout = textInputLayout;
        TextInputLayout textInputLayout2 = this.viewBinding.tlCvc;
        Intrinsics.checkExpressionValueIsNotNull(textInputLayout2, "viewBinding.tlCvc");
        this.cvcTextInputLayout = textInputLayout2;
        TextInputLayout textInputLayout3 = this.viewBinding.tlPostalCode;
        Intrinsics.checkExpressionValueIsNotNull(textInputLayout3, "viewBinding.tlPostalCode");
        this.postalInputLayout = textInputLayout3;
        this.cardValidTextWatcher = new CardMultilineWidget$cardValidTextWatcher$1(this);
        this.cardBrand = CardBrand.Unknown;
        String string = getResources().getString(R.string.card_number_hint);
        Intrinsics.checkExpressionValueIsNotNull(string, "resources.getString(R.string.card_number_hint)");
        this.cardHintText = string;
        Delegates delegates = Delegates.INSTANCE;
        this.usZipCodeRequired$delegate = new CardMultilineWidget$$special$$inlined$observable$1(false, false, this);
        this.pixelsToAdjust = (double) getResources().getDimension(R.dimen.stripe_card_icon_multiline_padding_bottom);
        this.dynamicBufferInPixels = new BigDecimal(this.pixelsToAdjust).setScale(0, RoundingMode.HALF_DOWN).intValue();
        setOrientation(1);
        ColorStateList hintTextColors = this.cardNumberEditText.getHintTextColors();
        Intrinsics.checkExpressionValueIsNotNull(hintTextColors, "cardNumberEditText.hintTextColors");
        this.tintColorInt = hintTextColors.getDefaultColor();
        if (attributeSet != null) {
            checkAttributeSet(attributeSet);
        }
        initTextInputLayoutErrorHandlers(this.cardNumberTextInputLayout, this.expiryTextInputLayout, this.cvcTextInputLayout, this.postalInputLayout);
        initFocusChangeListeners();
        initDeleteEmptyListeners();
        this.cardNumberEditText.setCompletionCallback$stripe_release(new Function0<Unit>(this) {
            final /* synthetic */ CardMultilineWidget this$0;

            {
                this.this$0 = r1;
            }

            public final void invoke() {
                this.this$0.getExpiryDateEditText$stripe_release().requestFocus();
                CardInputListener access$getCardInputListener$p = this.this$0.cardInputListener;
                if (access$getCardInputListener$p != null) {
                    access$getCardInputListener$p.onCardComplete();
                }
            }
        });
        this.cardNumberEditText.setBrandChangeCallback$stripe_release(new Function1<CardBrand, Unit>(this) {
            final /* synthetic */ CardMultilineWidget this$0;

            {
                this.this$0 = r1;
            }

            public /* bridge */ /* synthetic */ Object invoke(Object obj) {
                invoke((CardBrand) obj);
                return Unit.INSTANCE;
            }

            public final void invoke(CardBrand cardBrand) {
                Intrinsics.checkParameterIsNotNull(cardBrand, "brand");
                this.this$0.cardBrand = cardBrand;
                this.this$0.updateBrandUi();
            }
        });
        this.expiryDateEditText.setCompletionCallback$stripe_release(new Function0<Unit>(this) {
            final /* synthetic */ CardMultilineWidget this$0;

            {
                this.this$0 = r1;
            }

            public final void invoke() {
                this.this$0.getCvcEditText$stripe_release().requestFocus();
                CardInputListener access$getCardInputListener$p = this.this$0.cardInputListener;
                if (access$getCardInputListener$p != null) {
                    access$getCardInputListener$p.onExpirationComplete();
                }
            }
        });
        this.cvcEditText.setAfterTextChangedListener(new StripeEditText.AfterTextChangedListener(this) {
            final /* synthetic */ CardMultilineWidget this$0;

            {
                this.this$0 = r1;
            }

            public void onTextChanged(String str) {
                Intrinsics.checkParameterIsNotNull(str, "text");
                if (this.this$0.cardBrand.isMaxCvc(str)) {
                    this.this$0.updateBrandUi();
                    if (this.this$0.shouldShowPostalCode) {
                        this.this$0.getPostalCodeEditText$stripe_release().requestFocus();
                    }
                    CardInputListener access$getCardInputListener$p = this.this$0.cardInputListener;
                    if (access$getCardInputListener$p != null) {
                        access$getCardInputListener$p.onCvcComplete();
                    }
                } else {
                    this.this$0.flipToCvcIconIfNotFinished();
                }
                this.this$0.getCvcEditText$stripe_release().setShouldShowError(false);
            }
        });
        adjustViewForPostalCodeAttribute(this.shouldShowPostalCode);
        this.cardNumberEditText.updateLengthFilter$stripe_release();
        this.cardBrand = CardBrand.Unknown;
        updateBrandUi();
        for (StripeEditText addTextChangedListener : getAllFields()) {
            addTextChangedListener.addTextChangedListener(new CardMultilineWidget$$special$$inlined$forEach$lambda$1(this));
        }
        this.isEnabled = true;
    }

    public final CardNumberEditText getCardNumberEditText$stripe_release() {
        return this.cardNumberEditText;
    }

    public final ExpiryDateEditText getExpiryDateEditText$stripe_release() {
        return this.expiryDateEditText;
    }

    public final CvcEditText getCvcEditText$stripe_release() {
        return this.cvcEditText;
    }

    public final PostalCodeEditText getPostalCodeEditText$stripe_release() {
        return this.postalCodeEditText;
    }

    public final TextInputLayout getPostalInputLayout$stripe_release() {
        return this.postalInputLayout;
    }

    /* access modifiers changed from: private */
    public final Set<CardValidCallback.Fields> getInvalidFields() {
        CardValidCallback.Fields[] fieldsArr = new CardValidCallback.Fields[3];
        CardValidCallback.Fields fields = CardValidCallback.Fields.Number;
        boolean z = false;
        CardValidCallback.Fields fields2 = null;
        if (!(getCardNumber() == null)) {
            fields = null;
        }
        fieldsArr[0] = fields;
        CardValidCallback.Fields fields3 = CardValidCallback.Fields.Expiry;
        if (getExpiryDate() == null) {
            z = true;
        }
        if (!z) {
            fields3 = null;
        }
        fieldsArr[1] = fields3;
        CardValidCallback.Fields fields4 = CardValidCallback.Fields.Cvc;
        if (!isCvcLengthValid()) {
            fields2 = fields4;
        }
        fieldsArr[2] = fields2;
        return CollectionsKt.toSet(CollectionsKt.listOfNotNull((T[]) fieldsArr));
    }

    public final boolean getPostalCodeRequired() {
        return this.postalCodeRequired;
    }

    public final void setPostalCodeRequired(boolean z) {
        this.postalCodeRequired = z;
    }

    public PaymentMethodCreateParams.Card getPaymentMethodCard() {
        Card card = getCard();
        if (card == null) {
            return null;
        }
        return new PaymentMethodCreateParams.Card(card.getNumber(), card.getExpMonth(), card.getExpYear(), card.getCvc(), (String) null, card.getAttribution$stripe_release(), 16, (DefaultConstructorMarker) null);
    }

    public PaymentMethodCreateParams getPaymentMethodCreateParams() {
        PaymentMethodCreateParams.Card paymentMethodCard = getPaymentMethodCard();
        if (paymentMethodCard != null) {
            return PaymentMethodCreateParams.Companion.create$default(PaymentMethodCreateParams.Companion, paymentMethodCard, getPaymentMethodBillingDetails(), (Map) null, 4, (Object) null);
        }
        return null;
    }

    public final PaymentMethod.BillingDetails getPaymentMethodBillingDetails() {
        PaymentMethod.BillingDetails.Builder paymentMethodBillingDetailsBuilder = getPaymentMethodBillingDetailsBuilder();
        if (paymentMethodBillingDetailsBuilder != null) {
            return paymentMethodBillingDetailsBuilder.build();
        }
        return null;
    }

    public final PaymentMethod.BillingDetails.Builder getPaymentMethodBillingDetailsBuilder() {
        if (!this.shouldShowPostalCode || !validateAllFields()) {
            return null;
        }
        return new PaymentMethod.BillingDetails.Builder().setAddress(new Address.Builder().setPostalCode(this.postalCodeEditText.getPostalCode$stripe_release()).build());
    }

    public Card getCard() {
        Card.Builder cardBuilder = getCardBuilder();
        if (cardBuilder != null) {
            return cardBuilder.build();
        }
        return null;
    }

    public Card.Builder getCardBuilder() {
        String str = null;
        if (!validateAllFields()) {
            setShouldShowErrorIcon(true);
            return null;
        }
        setShouldShowErrorIcon(false);
        String cardNumber = getCardNumber();
        Pair<Integer, Integer> validDateFields = this.expiryDateEditText.getValidDateFields();
        if (validDateFields != null) {
            Editable text = this.cvcEditText.getText();
            String obj = text != null ? text.toString() : null;
            Editable text2 = this.postalCodeEditText.getText();
            String obj2 = text2 != null ? text2.toString() : null;
            if (this.shouldShowPostalCode) {
                str = obj2;
            }
            return new Card.Builder(cardNumber, validDateFields.getFirst(), validDateFields.getSecond(), obj).addressZip(str).loggingTokens(SetsKt.setOf(CARD_MULTILINE_TOKEN));
        }
        throw new IllegalArgumentException("Required value was null.".toString());
    }

    private final String getCardNumber() {
        return this.cardNumberEditText.getCardNumber();
    }

    private final Pair<Integer, Integer> getExpiryDate() {
        return this.expiryDateEditText.getValidDateFields();
    }

    private final boolean isCvcLengthValid() {
        return this.cardBrand.isValidCvc(this.cvcEditText.getRawCvcValue$stripe_release());
    }

    private final Collection<StripeEditText> getAllFields() {
        return CollectionsKt.listOf(this.cardNumberEditText, this.expiryDateEditText, this.cvcEditText, this.postalCodeEditText);
    }

    /* access modifiers changed from: private */
    public final int getCvcHelperText() {
        if (CardBrand.AmericanExpress == this.cardBrand) {
            return R.string.cvc_multiline_helper_amex;
        }
        return R.string.cvc_multiline_helper;
    }

    public final boolean getShouldShowErrorIcon$stripe_release() {
        return this.shouldShowErrorIcon;
    }

    /* access modifiers changed from: private */
    public final void setShouldShowErrorIcon(boolean z) {
        boolean z2 = this.shouldShowErrorIcon != z;
        this.shouldShowErrorIcon = z;
        if (z2) {
            updateBrandUi();
        }
    }

    /* access modifiers changed from: protected */
    public void onFinishInflate() {
        super.onFinishInflate();
        this.postalCodeEditText.setConfig$stripe_release(PostalCodeEditText.Config.Global);
    }

    public void clear() {
        this.cardNumberEditText.setText("");
        this.expiryDateEditText.setText("");
        this.cvcEditText.setText("");
        this.postalCodeEditText.setText("");
        this.cardNumberEditText.setShouldShowError(false);
        this.expiryDateEditText.setShouldShowError(false);
        this.cvcEditText.setShouldShowError(false);
        this.postalCodeEditText.setShouldShowError(false);
        this.cardBrand = CardBrand.Unknown;
        updateBrandUi();
    }

    public void setCardInputListener(CardInputListener cardInputListener2) {
        this.cardInputListener = cardInputListener2;
    }

    public void setCardValidCallback(CardValidCallback cardValidCallback2) {
        this.cardValidCallback = cardValidCallback2;
        for (StripeEditText removeTextChangedListener : getAllFields()) {
            removeTextChangedListener.removeTextChangedListener(this.cardValidTextWatcher);
        }
        if (cardValidCallback2 != null) {
            for (StripeEditText addTextChangedListener : getAllFields()) {
                addTextChangedListener.addTextChangedListener(this.cardValidTextWatcher);
            }
        }
        CardValidCallback cardValidCallback3 = this.cardValidCallback;
        if (cardValidCallback3 != null) {
            cardValidCallback3.onInputChanged(getInvalidFields().isEmpty(), getInvalidFields());
        }
    }

    public void setCardHint(String str) {
        Intrinsics.checkParameterIsNotNull(str, "cardHint");
        this.cardHintText = str;
    }

    /* JADX WARNING: Removed duplicated region for block: B:20:0x0064  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0077  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0072 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean validateAllFields() {
        /*
            r8 = this;
            java.lang.String r0 = r8.getCardNumber()
            boolean r0 = com.stripe.android.CardUtils.isValidCardNumber(r0)
            kotlin.Pair r1 = r8.getExpiryDate()
            r2 = 0
            r3 = 1
            if (r1 == 0) goto L_0x0012
            r1 = 1
            goto L_0x0013
        L_0x0012:
            r1 = 0
        L_0x0013:
            boolean r4 = r8.isCvcLengthValid()
            com.stripe.android.view.CardNumberEditText r5 = r8.cardNumberEditText
            r6 = r0 ^ 1
            r5.setShouldShowError(r6)
            com.stripe.android.view.ExpiryDateEditText r5 = r8.expiryDateEditText
            r6 = r1 ^ 1
            r5.setShouldShowError(r6)
            com.stripe.android.view.CvcEditText r5 = r8.cvcEditText
            r6 = r4 ^ 1
            r5.setShouldShowError(r6)
            com.stripe.android.view.PostalCodeEditText r5 = r8.postalCodeEditText
            boolean r6 = r8.postalCodeRequired
            if (r6 != 0) goto L_0x0038
            boolean r6 = r8.getUsZipCodeRequired()
            if (r6 == 0) goto L_0x0050
        L_0x0038:
            com.stripe.android.view.PostalCodeEditText r6 = r8.postalCodeEditText
            java.lang.String r6 = r6.getPostalCode$stripe_release()
            java.lang.CharSequence r6 = (java.lang.CharSequence) r6
            if (r6 == 0) goto L_0x004b
            boolean r6 = kotlin.text.StringsKt.isBlank(r6)
            if (r6 == 0) goto L_0x0049
            goto L_0x004b
        L_0x0049:
            r6 = 0
            goto L_0x004c
        L_0x004b:
            r6 = 1
        L_0x004c:
            if (r6 == 0) goto L_0x0050
            r6 = 1
            goto L_0x0051
        L_0x0050:
            r6 = 0
        L_0x0051:
            r5.setShouldShowError(r6)
            java.util.Collection r5 = r8.getAllFields()
            java.lang.Iterable r5 = (java.lang.Iterable) r5
            java.util.Iterator r5 = r5.iterator()
        L_0x005e:
            boolean r6 = r5.hasNext()
            if (r6 == 0) goto L_0x0072
            java.lang.Object r6 = r5.next()
            r7 = r6
            com.stripe.android.view.StripeEditText r7 = (com.stripe.android.view.StripeEditText) r7
            boolean r7 = r7.getShouldShowError()
            if (r7 == 0) goto L_0x005e
            goto L_0x0073
        L_0x0072:
            r6 = 0
        L_0x0073:
            com.stripe.android.view.StripeEditText r6 = (com.stripe.android.view.StripeEditText) r6
            if (r6 == 0) goto L_0x007a
            r6.requestFocus()
        L_0x007a:
            if (r0 == 0) goto L_0x0089
            if (r1 == 0) goto L_0x0089
            if (r4 == 0) goto L_0x0089
            com.stripe.android.view.PostalCodeEditText r0 = r8.postalCodeEditText
            boolean r0 = r0.getShouldShowError()
            if (r0 != 0) goto L_0x0089
            r2 = 1
        L_0x0089:
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.stripe.android.view.CardMultilineWidget.validateAllFields():boolean");
    }

    public void onWindowFocusChanged(boolean z) {
        super.onWindowFocusChanged(z);
        if (z) {
            updateBrandUi();
        }
    }

    public final void setCvcLabel(String str) {
        this.customCvcLabel = str;
        updateCvc();
    }

    public final void setShouldShowPostalCode(boolean z) {
        this.shouldShowPostalCode = z;
        adjustViewForPostalCodeAttribute(z);
    }

    public void setCardNumber(String str) {
        this.cardNumberEditText.setText(str);
    }

    public void setExpiryDate(int i, int i2) {
        this.expiryDateEditText.setText(DateUtils.createDateStringFromIntegerInput(i, i2));
    }

    public void setCvcCode(String str) {
        this.cvcEditText.setText(str);
    }

    public final boolean validateCardNumber() {
        boolean isValidCardNumber = CardUtils.isValidCardNumber(getCardNumber());
        this.cardNumberEditText.setShouldShowError(!isValidCardNumber);
        return isValidCardNumber;
    }

    public void setCardNumberTextWatcher(TextWatcher textWatcher) {
        this.cardNumberEditText.addTextChangedListener(textWatcher);
    }

    public void setExpiryDateTextWatcher(TextWatcher textWatcher) {
        this.expiryDateEditText.addTextChangedListener(textWatcher);
    }

    public void setCvcNumberTextWatcher(TextWatcher textWatcher) {
        this.cvcEditText.addTextChangedListener(textWatcher);
    }

    public void setPostalCodeTextWatcher(TextWatcher textWatcher) {
        this.postalCodeEditText.addTextChangedListener(textWatcher);
    }

    public boolean isEnabled() {
        return this.isEnabled;
    }

    public void setEnabled(boolean z) {
        this.expiryTextInputLayout.setEnabled(z);
        this.cardNumberTextInputLayout.setEnabled(z);
        this.cvcTextInputLayout.setEnabled(z);
        this.postalInputLayout.setEnabled(z);
        this.isEnabled = z;
    }

    private final void adjustViewForPostalCodeAttribute(boolean z) {
        int i;
        if (z) {
            i = R.string.expiry_label_short;
        } else {
            i = R.string.acc_label_expiry_date;
        }
        this.expiryTextInputLayout.setHint(getResources().getString(i));
        int i2 = z ? R.id.et_postal_code : -1;
        this.cvcEditText.setNextFocusForwardId(i2);
        this.cvcEditText.setNextFocusDownId(i2);
        int i3 = z ? 0 : 8;
        this.postalInputLayout.setVisibility(i3);
        this.cvcEditText.setImeOptions(i3 == 8 ? 6 : 5);
        int dimensionPixelSize = z ? getResources().getDimensionPixelSize(R.dimen.stripe_add_card_expiry_middle_margin) : 0;
        ViewGroup.LayoutParams layoutParams = this.cvcTextInputLayout.getLayoutParams();
        if (layoutParams != null) {
            LinearLayout.LayoutParams layoutParams2 = (LinearLayout.LayoutParams) layoutParams;
            layoutParams2.setMargins(0, 0, dimensionPixelSize, 0);
            layoutParams2.setMarginEnd(dimensionPixelSize);
            this.cvcTextInputLayout.setLayoutParams(layoutParams2);
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type android.widget.LinearLayout.LayoutParams");
    }

    private final void checkAttributeSet(AttributeSet attributeSet) {
        Context context = getContext();
        Intrinsics.checkExpressionValueIsNotNull(context, "context");
        TypedArray obtainStyledAttributes = context.getTheme().obtainStyledAttributes(attributeSet, R.styleable.CardElement, 0, 0);
        try {
            this.shouldShowPostalCode = obtainStyledAttributes.getBoolean(R.styleable.CardElement_shouldShowPostalCode, true);
            this.postalCodeRequired = obtainStyledAttributes.getBoolean(R.styleable.CardElement_shouldRequirePostalCode, false);
            setUsZipCodeRequired(obtainStyledAttributes.getBoolean(R.styleable.CardElement_shouldRequireUsZipCode, false));
        } finally {
            obtainStyledAttributes.recycle();
        }
    }

    /* access modifiers changed from: private */
    public final void flipToCvcIconIfNotFinished() {
        if (!this.cardBrand.isMaxCvc(this.cvcEditText.getFieldText$stripe_release())) {
            if (this.shouldShowErrorIcon) {
                updateDrawable(this.cardBrand.getErrorIcon(), false);
            } else {
                updateDrawable(this.cardBrand.getCvcIcon(), true);
            }
        }
    }

    private final void initDeleteEmptyListeners() {
        this.expiryDateEditText.setDeleteEmptyListener(new BackUpFieldDeleteListener(this.cardNumberEditText));
        this.cvcEditText.setDeleteEmptyListener(new BackUpFieldDeleteListener(this.expiryDateEditText));
        this.postalCodeEditText.setDeleteEmptyListener(new BackUpFieldDeleteListener(this.cvcEditText));
    }

    private final void initFocusChangeListeners() {
        this.cardNumberEditText.setOnFocusChangeListener(new CardMultilineWidget$initFocusChangeListeners$1(this));
        this.expiryDateEditText.setOnFocusChangeListener(new CardMultilineWidget$initFocusChangeListeners$2(this));
        this.cvcEditText.setOnFocusChangeListener(new CardMultilineWidget$initFocusChangeListeners$3(this));
        this.postalCodeEditText.setOnFocusChangeListener(new CardMultilineWidget$initFocusChangeListeners$4(this));
    }

    private final void initTextInputLayoutErrorHandlers(TextInputLayout textInputLayout, TextInputLayout textInputLayout2, TextInputLayout textInputLayout3, TextInputLayout textInputLayout4) {
        this.cardNumberEditText.setErrorMessageListener(new ErrorListener(textInputLayout));
        this.expiryDateEditText.setErrorMessageListener(new ErrorListener(textInputLayout2));
        this.cvcEditText.setErrorMessageListener(new ErrorListener(textInputLayout3));
        this.postalCodeEditText.setErrorMessageListener(new ErrorListener(textInputLayout4));
    }

    /* access modifiers changed from: private */
    public final void updateBrandUi() {
        updateCvc();
        boolean z = false;
        if (this.shouldShowErrorIcon) {
            updateDrawable(this.cardBrand.getErrorIcon(), false);
            return;
        }
        int icon = this.cardBrand.getIcon();
        if (CardBrand.Unknown == this.cardBrand) {
            z = true;
        }
        updateDrawable(icon, z);
    }

    private final void updateCvc() {
        this.cvcEditText.updateBrand$stripe_release(this.cardBrand, this.customCvcLabel, this.cvcTextInputLayout);
    }

    private final void updateDrawable(int i, boolean z) {
        Drawable drawable = ContextCompat.getDrawable(getContext(), i);
        if (drawable != null) {
            Intrinsics.checkExpressionValueIsNotNull(drawable, "ContextCompat.getDrawabl…iconResourceId) ?: return");
            Drawable drawable2 = this.cardNumberEditText.getCompoundDrawablesRelative()[0];
            if (drawable2 != null) {
                int compoundDrawablePadding = this.cardNumberEditText.getCompoundDrawablePadding();
                drawable.setBounds(createDrawableBounds(drawable2));
                Drawable wrap = DrawableCompat.wrap(drawable);
                if (z) {
                    DrawableCompat.setTint(wrap.mutate(), this.tintColorInt);
                }
                this.cardNumberEditText.setCompoundDrawablePadding(compoundDrawablePadding);
                this.cardNumberEditText.setCompoundDrawablesRelative(wrap, (Drawable) null, (Drawable) null, (Drawable) null);
            }
        }
    }

    private final Rect createDrawableBounds(Drawable drawable) {
        Rect rect = new Rect();
        drawable.copyBounds(rect);
        if (!this.hasAdjustedDrawable) {
            rect.top -= this.dynamicBufferInPixels;
            rect.bottom -= this.dynamicBufferInPixels;
            this.hasAdjustedDrawable = true;
        }
        return rect;
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\t\n\u0002\b\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0006XT¢\u0006\u0002\n\u0000¨\u0006\b"}, d2 = {"Lcom/stripe/android/view/CardMultilineWidget$Companion;", "", "()V", "CARD_MULTILINE_TOKEN", "", "CARD_NUMBER_HINT_DELAY", "", "COMMON_HINT_DELAY", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: CardMultilineWidget.kt */
    private static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }
}
