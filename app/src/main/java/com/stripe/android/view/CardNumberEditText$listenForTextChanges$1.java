package com.stripe.android.view;

import android.text.Editable;
import com.stripe.android.CardUtils;
import com.stripe.android.StripeTextUtils;
import kotlin.Metadata;
import kotlin.ranges.RangesKt;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000-\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\r\n\u0002\b\u0006*\u0001\u0000\b\n\u0018\u00002\u00020\u0001J\u0012\u0010\t\u001a\u00020\n2\b\u0010\u000b\u001a\u0004\u0018\u00010\fH\u0016J*\u0010\r\u001a\u00020\n2\b\u0010\u000b\u001a\u0004\u0018\u00010\u000e2\u0006\u0010\u000f\u001a\u00020\u00052\u0006\u0010\u0010\u001a\u00020\u00052\u0006\u0010\u0011\u001a\u00020\u0005H\u0016J*\u0010\u0012\u001a\u00020\n2\b\u0010\u000b\u001a\u0004\u0018\u00010\u000e2\u0006\u0010\u000f\u001a\u00020\u00052\u0006\u0010\u0013\u001a\u00020\u00052\u0006\u0010\u0010\u001a\u00020\u0005H\u0016R\u0010\u0010\u0002\u001a\u0004\u0018\u00010\u0003X\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0005X\u000e¢\u0006\u0002\n\u0000R\u0012\u0010\u0007\u001a\u0004\u0018\u00010\u0005X\u000e¢\u0006\u0004\n\u0002\u0010\b¨\u0006\u0014"}, d2 = {"com/stripe/android/view/CardNumberEditText$listenForTextChanges$1", "Lcom/stripe/android/view/StripeTextWatcher;", "formattedNumber", "", "latestChangeStart", "", "latestInsertionSize", "newCursorPosition", "Ljava/lang/Integer;", "afterTextChanged", "", "s", "Landroid/text/Editable;", "beforeTextChanged", "", "start", "count", "after", "onTextChanged", "before", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: CardNumberEditText.kt */
public final class CardNumberEditText$listenForTextChanges$1 extends StripeTextWatcher {
    private String formattedNumber;
    private int latestChangeStart;
    private int latestInsertionSize;
    private Integer newCursorPosition;
    final /* synthetic */ CardNumberEditText this$0;

    CardNumberEditText$listenForTextChanges$1(CardNumberEditText cardNumberEditText) {
        this.this$0 = cardNumberEditText;
    }

    public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        if (!this.this$0.ignoreChanges) {
            this.latestChangeStart = i;
            this.latestInsertionSize = i3;
        }
    }

    public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        String removeSpacesAndHyphens;
        if (!this.this$0.ignoreChanges) {
            String obj = charSequence != null ? charSequence.toString() : null;
            if (obj == null) {
                obj = "";
            }
            if (i < 4) {
                this.this$0.updateCardBrandFromNumber$stripe_release(obj);
            }
            if (i <= 16 && (removeSpacesAndHyphens = StripeTextUtils.removeSpacesAndHyphens(obj)) != null) {
                String formatNumber = this.this$0.getCardBrand().formatNumber(removeSpacesAndHyphens);
                this.newCursorPosition = Integer.valueOf(this.this$0.updateSelectionIndex$stripe_release(formatNumber.length(), this.latestChangeStart, this.latestInsertionSize));
                this.formattedNumber = formatNumber;
            }
        }
    }

    public void afterTextChanged(Editable editable) {
        String str;
        if (!this.this$0.ignoreChanges) {
            this.this$0.ignoreChanges = true;
            if (!this.this$0.isLastKeyDelete() && (str = this.formattedNumber) != null) {
                this.this$0.setText(str);
                Integer num = this.newCursorPosition;
                if (num != null) {
                    int intValue = num.intValue();
                    CardNumberEditText cardNumberEditText = this.this$0;
                    cardNumberEditText.setSelection(RangesKt.coerceIn(intValue, 0, cardNumberEditText.getFieldText$stripe_release().length()));
                }
            }
            this.formattedNumber = null;
            this.newCursorPosition = null;
            this.this$0.ignoreChanges = false;
            if (this.this$0.getFieldText$stripe_release().length() == this.this$0.getLengthMax()) {
                boolean isCardNumberValid = this.this$0.isCardNumberValid();
                CardNumberEditText cardNumberEditText2 = this.this$0;
                cardNumberEditText2.isCardNumberValid = CardUtils.isValidCardNumber(cardNumberEditText2.getFieldText$stripe_release());
                CardNumberEditText cardNumberEditText3 = this.this$0;
                cardNumberEditText3.setShouldShowError(true ^ cardNumberEditText3.isCardNumberValid());
                if (!isCardNumberValid && this.this$0.isCardNumberValid()) {
                    this.this$0.getCompletionCallback$stripe_release().invoke();
                    return;
                }
                return;
            }
            CardNumberEditText cardNumberEditText4 = this.this$0;
            cardNumberEditText4.isCardNumberValid = CardUtils.isValidCardNumber(cardNumberEditText4.getFieldText$stripe_release());
            this.this$0.setShouldShowError(false);
        }
    }
}
