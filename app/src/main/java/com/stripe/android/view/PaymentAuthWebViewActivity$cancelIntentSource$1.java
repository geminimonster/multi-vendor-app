package com.stripe.android.view;

import android.content.Intent;
import androidx.lifecycle.Observer;
import kotlin.Metadata;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n¢\u0006\u0002\b\u0005"}, d2 = {"<anonymous>", "", "intent", "Landroid/content/Intent;", "kotlin.jvm.PlatformType", "onChanged"}, k = 3, mv = {1, 1, 16})
/* compiled from: PaymentAuthWebViewActivity.kt */
final class PaymentAuthWebViewActivity$cancelIntentSource$1<T> implements Observer<Intent> {
    final /* synthetic */ PaymentAuthWebViewActivity this$0;

    PaymentAuthWebViewActivity$cancelIntentSource$1(PaymentAuthWebViewActivity paymentAuthWebViewActivity) {
        this.this$0 = paymentAuthWebViewActivity;
    }

    public final void onChanged(Intent intent) {
        this.this$0.setResult(-1, intent);
        this.this$0.finish();
    }
}
