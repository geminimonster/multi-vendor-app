package com.stripe.android.view;

import android.view.View;
import com.stripe.android.view.CardMultilineWidget;
import kotlin.Metadata;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u00032\u0006\u0010\u0005\u001a\u00020\u0006H\n¢\u0006\u0002\b\u0007"}, d2 = {"<anonymous>", "", "<anonymous parameter 0>", "Landroid/view/View;", "kotlin.jvm.PlatformType", "hasFocus", "", "onFocusChange"}, k = 3, mv = {1, 1, 16})
/* compiled from: CardMultilineWidget.kt */
final class CardMultilineWidget$initFocusChangeListeners$3 implements View.OnFocusChangeListener {
    final /* synthetic */ CardMultilineWidget this$0;

    CardMultilineWidget$initFocusChangeListeners$3(CardMultilineWidget cardMultilineWidget) {
        this.this$0 = cardMultilineWidget;
    }

    public final void onFocusChange(View view, boolean z) {
        if (z) {
            this.this$0.flipToCvcIconIfNotFinished();
            CvcEditText cvcEditText$stripe_release = this.this$0.getCvcEditText$stripe_release();
            int access$getCvcHelperText$p = this.this$0.getCvcHelperText();
            CardMultilineWidget.Companion unused = CardMultilineWidget.Companion;
            cvcEditText$stripe_release.setHintDelayed(access$getCvcHelperText$p, 90);
            CardInputListener access$getCardInputListener$p = this.this$0.cardInputListener;
            if (access$getCardInputListener$p != null) {
                access$getCardInputListener$p.onFocusChange("focus_cvc");
                return;
            }
            return;
        }
        this.this$0.updateBrandUi();
        this.this$0.getCvcEditText$stripe_release().setHint("");
    }
}
