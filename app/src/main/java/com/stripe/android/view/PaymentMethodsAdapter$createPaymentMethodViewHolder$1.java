package com.stripe.android.view;

import android.view.View;
import androidx.core.view.accessibility.AccessibilityViewCommand;
import com.stripe.android.view.PaymentMethodsAdapter;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00032\b\u0010\u0004\u001a\u0004\u0018\u00010\u0005H\n¢\u0006\u0002\b\u0006"}, d2 = {"<anonymous>", "", "<anonymous parameter 0>", "Landroid/view/View;", "<anonymous parameter 1>", "Landroidx/core/view/accessibility/AccessibilityViewCommand$CommandArguments;", "perform"}, k = 3, mv = {1, 1, 16})
/* compiled from: PaymentMethodsAdapter.kt */
final class PaymentMethodsAdapter$createPaymentMethodViewHolder$1 implements AccessibilityViewCommand {
    final /* synthetic */ PaymentMethodsAdapter.ViewHolder.PaymentMethodViewHolder $viewHolder;
    final /* synthetic */ PaymentMethodsAdapter this$0;

    PaymentMethodsAdapter$createPaymentMethodViewHolder$1(PaymentMethodsAdapter paymentMethodsAdapter, PaymentMethodsAdapter.ViewHolder.PaymentMethodViewHolder paymentMethodViewHolder) {
        this.this$0 = paymentMethodsAdapter;
        this.$viewHolder = paymentMethodViewHolder;
    }

    public final boolean perform(View view, AccessibilityViewCommand.CommandArguments commandArguments) {
        Intrinsics.checkParameterIsNotNull(view, "<anonymous parameter 0>");
        PaymentMethodsAdapter.Listener listener$stripe_release = this.this$0.getListener$stripe_release();
        if (listener$stripe_release == null) {
            return true;
        }
        listener$stripe_release.onDeletePaymentMethodAction(this.this$0.getPaymentMethodAtPosition$stripe_release(this.$viewHolder.getAdapterPosition()));
        return true;
    }
}
