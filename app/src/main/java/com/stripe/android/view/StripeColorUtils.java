package com.stripe.android.view;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.TypedValue;
import androidx.core.content.ContextCompat;
import androidx.core.graphics.drawable.DrawableCompat;
import kotlin.Metadata;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\b\u0000\u0018\u0000 \u00152\u00020\u0001:\u0001\u0015B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bH\u0002J\u0006\u0010\t\u001a\u00020\nJ\u0006\u0010\u000b\u001a\u00020\nJ\u0006\u0010\f\u001a\u00020\nJ\u0006\u0010\r\u001a\u00020\nJ&\u0010\u000e\u001a\u00020\u000f2\n\u0010\u0010\u001a\u00060\u0011R\u00020\u00122\b\b\u0001\u0010\u0013\u001a\u00020\u00062\b\b\u0001\u0010\u0014\u001a\u00020\u0006R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0016"}, d2 = {"Lcom/stripe/android/view/StripeColorUtils;", "", "context", "Landroid/content/Context;", "(Landroid/content/Context;)V", "getIdentifier", "", "attrName", "", "getThemeAccentColor", "Landroid/util/TypedValue;", "getThemeColorControlNormal", "getThemeTextColorPrimary", "getThemeTextColorSecondary", "getTintedIconWithAttribute", "Landroid/graphics/drawable/Drawable;", "theme", "Landroid/content/res/Resources$Theme;", "Landroid/content/res/Resources;", "attributeResource", "iconResourceId", "Companion", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: StripeColorUtils.kt */
public final class StripeColorUtils {
    public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
    private final Context context;

    @JvmStatic
    public static final boolean isColorDark(int i) {
        return Companion.isColorDark(i);
    }

    @JvmStatic
    public static final boolean isColorTransparent(int i) {
        return Companion.isColorTransparent(i);
    }

    public StripeColorUtils(Context context2) {
        Intrinsics.checkParameterIsNotNull(context2, "context");
        this.context = context2;
    }

    public final TypedValue getThemeAccentColor() {
        int i;
        if (Build.VERSION.SDK_INT >= 21) {
            i = 16843829;
        } else {
            i = getIdentifier("colorAccent");
        }
        TypedValue typedValue = new TypedValue();
        this.context.getTheme().resolveAttribute(i, typedValue, true);
        return typedValue;
    }

    public final TypedValue getThemeColorControlNormal() {
        int i;
        if (Build.VERSION.SDK_INT >= 21) {
            i = 16843817;
        } else {
            i = getIdentifier("colorControlNormal");
        }
        TypedValue typedValue = new TypedValue();
        this.context.getTheme().resolveAttribute(i, typedValue, true);
        return typedValue;
    }

    private final int getIdentifier(String str) {
        return this.context.getResources().getIdentifier(str, "attr", this.context.getPackageName());
    }

    public final TypedValue getThemeTextColorSecondary() {
        int i = Build.VERSION.SDK_INT >= 21 ? 16842808 : 17170439;
        TypedValue typedValue = new TypedValue();
        this.context.getTheme().resolveAttribute(i, typedValue, true);
        return typedValue;
    }

    public final TypedValue getThemeTextColorPrimary() {
        int i = Build.VERSION.SDK_INT >= 21 ? 16842806 : 17170435;
        TypedValue typedValue = new TypedValue();
        this.context.getTheme().resolveAttribute(i, typedValue, true);
        return typedValue;
    }

    public final Drawable getTintedIconWithAttribute(Resources.Theme theme, int i, int i2) {
        Intrinsics.checkParameterIsNotNull(theme, "theme");
        TypedValue typedValue = new TypedValue();
        theme.resolveAttribute(i, typedValue, true);
        int i3 = typedValue.data;
        Drawable drawable = ContextCompat.getDrawable(this.context, i2);
        if (drawable == null) {
            Intrinsics.throwNpe();
        }
        Drawable wrap = DrawableCompat.wrap(drawable);
        DrawableCompat.setTint(wrap.mutate(), i3);
        Intrinsics.checkExpressionValueIsNotNull(wrap, "compatIcon");
        return wrap;
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0012\u0010\u0003\u001a\u00020\u00042\b\b\u0001\u0010\u0005\u001a\u00020\u0006H\u0007J\u0012\u0010\u0007\u001a\u00020\u00042\b\b\u0001\u0010\u0005\u001a\u00020\u0006H\u0007¨\u0006\b"}, d2 = {"Lcom/stripe/android/view/StripeColorUtils$Companion;", "", "()V", "isColorDark", "", "color", "", "isColorTransparent", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: StripeColorUtils.kt */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        @JvmStatic
        public final boolean isColorTransparent(int i) {
            return Color.alpha(i) < 16;
        }

        @JvmStatic
        public final boolean isColorDark(int i) {
            return (((((double) Color.red(i)) * 0.299d) + (((double) Color.green(i)) * 0.587d)) + (((double) Color.blue(i)) * 0.114d)) / ((double) 255) <= 0.5d;
        }
    }
}
