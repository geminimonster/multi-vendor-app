package com.stripe.android.view;

import androidx.lifecycle.Observer;
import com.stripe.android.model.PaymentMethod;
import kotlin.Metadata;
import kotlin.Result;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\u0010\u0000\u001a\u00020\u00012\u001a\u0010\u0002\u001a\u0016\u0012\u0004\u0012\u00020\u0004 \u0005*\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u00030\u0003H\n¢\u0006\u0002\b\u0006"}, d2 = {"<anonymous>", "", "result", "Lkotlin/Result;", "Lcom/stripe/android/model/PaymentMethod;", "kotlin.jvm.PlatformType", "onChanged"}, k = 3, mv = {1, 1, 16})
/* compiled from: AddPaymentMethodActivity.kt */
final class AddPaymentMethodActivity$attachPaymentMethodToCustomer$1<T> implements Observer<Result<? extends PaymentMethod>> {
    final /* synthetic */ AddPaymentMethodActivity this$0;

    AddPaymentMethodActivity$attachPaymentMethodToCustomer$1(AddPaymentMethodActivity addPaymentMethodActivity) {
        this.this$0 = addPaymentMethodActivity;
    }

    public final void onChanged(Result<? extends PaymentMethod> result) {
        Object r3 = result.m13unboximpl();
        AddPaymentMethodActivity addPaymentMethodActivity = this.this$0;
        Throwable r1 = Result.m7exceptionOrNullimpl(r3);
        if (r1 == null) {
            addPaymentMethodActivity.finishWithPaymentMethod((PaymentMethod) r3);
            return;
        }
        this.this$0.setProgressBarVisible(false);
        AddPaymentMethodActivity addPaymentMethodActivity2 = this.this$0;
        String message = r1.getMessage();
        if (message == null) {
            message = "";
        }
        addPaymentMethodActivity2.showError(message);
    }
}
