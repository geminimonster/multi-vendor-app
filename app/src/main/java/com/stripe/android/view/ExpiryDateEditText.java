package com.stripe.android.view;

import android.content.Context;
import android.os.Build;
import android.text.Editable;
import android.util.AttributeSet;
import androidx.appcompat.R;
import kotlin.Metadata;
import kotlin.Pair;
import kotlin.Result;
import kotlin.ResultKt;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.Intrinsics;
import kotlin.text.Regex;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u000e\u0018\u0000 &2\u00020\u0001:\u0001&B%\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\b\b\u0002\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ\b\u0010\u001c\u001a\u00020\u000fH\u0002J\u0018\u0010\u001d\u001a\u00020\u000f2\u0006\u0010\u001e\u001a\u00020\n2\u0006\u0010\u001f\u001a\u00020\nH\u0002J-\u0010 \u001a\u00020\u00072\u0006\u0010!\u001a\u00020\u00072\u0006\u0010\"\u001a\u00020\u00072\u0006\u0010#\u001a\u00020\u00072\u0006\u0010$\u001a\u00020\u0007H\u0001¢\u0006\u0002\b%R\u0016\u0010\t\u001a\u0004\u0018\u00010\n8TX\u0004¢\u0006\u0006\u001a\u0004\b\u000b\u0010\fR \u0010\r\u001a\b\u0012\u0004\u0012\u00020\u000f0\u000eX\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0010\u0010\u0011\"\u0004\b\u0012\u0010\u0013R\u001e\u0010\u0016\u001a\u00020\u00152\u0006\u0010\u0014\u001a\u00020\u0015@BX\u000e¢\u0006\b\n\u0000\u001a\u0004\b\u0016\u0010\u0017R\u001f\u0010\u0018\u001a\u0010\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u0007\u0018\u00010\u00198F¢\u0006\u0006\u001a\u0004\b\u001a\u0010\u001b¨\u0006'"}, d2 = {"Lcom/stripe/android/view/ExpiryDateEditText;", "Lcom/stripe/android/view/StripeEditText;", "context", "Landroid/content/Context;", "attrs", "Landroid/util/AttributeSet;", "defStyleAttr", "", "(Landroid/content/Context;Landroid/util/AttributeSet;I)V", "accessibilityText", "", "getAccessibilityText", "()Ljava/lang/String;", "completionCallback", "Lkotlin/Function0;", "", "getCompletionCallback$stripe_release", "()Lkotlin/jvm/functions/Function0;", "setCompletionCallback$stripe_release", "(Lkotlin/jvm/functions/Function0;)V", "<set-?>", "", "isDateValid", "()Z", "validDateFields", "Lkotlin/Pair;", "getValidDateFields", "()Lkotlin/Pair;", "listenForTextChanges", "updateInputValues", "month", "year", "updateSelectionIndex", "newLength", "editActionStart", "editActionAddition", "maxInputLength", "updateSelectionIndex$stripe_release", "Companion", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: ExpiryDateEditText.kt */
public final class ExpiryDateEditText extends StripeEditText {
    @Deprecated
    public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
    private static final int INVALID_INPUT = -1;
    private static final int MAX_INPUT_LENGTH = 5;
    private /* synthetic */ Function0<Unit> completionCallback;
    /* access modifiers changed from: private */
    public boolean isDateValid;

    public ExpiryDateEditText(Context context) {
        this(context, (AttributeSet) null, 0, 6, (DefaultConstructorMarker) null);
    }

    public ExpiryDateEditText(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0, 4, (DefaultConstructorMarker) null);
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ ExpiryDateEditText(Context context, AttributeSet attributeSet, int i, int i2, DefaultConstructorMarker defaultConstructorMarker) {
        this(context, (i2 & 2) != 0 ? null : attributeSet, (i2 & 4) != 0 ? R.attr.editTextStyle : i);
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ExpiryDateEditText(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        Intrinsics.checkParameterIsNotNull(context, "context");
        setErrorMessage(getResources().getString(com.stripe.android.R.string.invalid_expiry_year));
        listenForTextChanges();
        if (Build.VERSION.SDK_INT >= 26) {
            setAutofillHints(new String[]{"creditCardExpirationDate"});
        }
        this.completionCallback = ExpiryDateEditText$completionCallback$1.INSTANCE;
    }

    public final Function0<Unit> getCompletionCallback$stripe_release() {
        return this.completionCallback;
    }

    public final void setCompletionCallback$stripe_release(Function0<Unit> function0) {
        Intrinsics.checkParameterIsNotNull(function0, "<set-?>");
        this.completionCallback = function0;
    }

    public final boolean isDateValid() {
        return this.isDateValid;
    }

    public final Pair<Integer, Integer> getValidDateFields() {
        Editable text = getText();
        String obj = text != null ? text.toString() : null;
        if (!this.isDateValid) {
            obj = null;
        }
        if (obj == null) {
            return null;
        }
        String[] separateDateStringParts = DateUtils.separateDateStringParts(new Regex("/").replace((CharSequence) obj, ""));
        try {
            return new Pair<>(Integer.valueOf(Integer.parseInt(separateDateStringParts[0])), Integer.valueOf(DateUtils.convertTwoDigitYearToFour(Integer.parseInt(separateDateStringParts[1]))));
        } catch (NumberFormatException unused) {
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public String getAccessibilityText() {
        return getResources().getString(com.stripe.android.R.string.acc_label_expiry_date_node, new Object[]{getText()});
    }

    private final void listenForTextChanges() {
        addTextChangedListener(new ExpiryDateEditText$listenForTextChanges$1(this));
    }

    public final int updateSelectionIndex$stripe_release(int i, int i2, int i3, int i4) {
        boolean z = true;
        int i5 = (i2 > 2 || i2 + i3 < 2) ? 0 : 1;
        if (!(i3 == 0 && i2 == 3)) {
            z = false;
        }
        int i6 = i2 + i3 + i5;
        if (z && i6 > 0) {
            i6--;
        }
        if (i6 <= i) {
            i = i6;
        }
        return Math.min(i4, i);
    }

    /* access modifiers changed from: private */
    public final void updateInputValues(String str, String str2) {
        int i;
        Object obj;
        Object obj2;
        int i2 = -1;
        int i3 = -1;
        if (str.length() != 2) {
            i = -1;
        } else {
            try {
                Result.Companion companion = Result.Companion;
                ExpiryDateEditText expiryDateEditText = this;
                obj2 = Result.m4constructorimpl(Integer.valueOf(Integer.parseInt(str)));
            } catch (Throwable th) {
                Result.Companion companion2 = Result.Companion;
                obj2 = Result.m4constructorimpl(ResultKt.createFailure(th));
            }
            if (Result.m10isFailureimpl(obj2)) {
                obj2 = -1;
            }
            i = ((Number) obj2).intValue();
        }
        if (str2.length() == 2) {
            try {
                Result.Companion companion3 = Result.Companion;
                ExpiryDateEditText expiryDateEditText2 = this;
                obj = Result.m4constructorimpl(Integer.valueOf(DateUtils.convertTwoDigitYearToFour(Integer.parseInt(str2))));
            } catch (Throwable th2) {
                Result.Companion companion4 = Result.Companion;
                obj = Result.m4constructorimpl(ResultKt.createFailure(th2));
            }
            if (!Result.m10isFailureimpl(obj)) {
                i3 = obj;
            }
            i2 = ((Number) i3).intValue();
        }
        this.isDateValid = DateUtils.isExpiryDataValid(i, i2);
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\u0006"}, d2 = {"Lcom/stripe/android/view/ExpiryDateEditText$Companion;", "", "()V", "INVALID_INPUT", "", "MAX_INPUT_LENGTH", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: ExpiryDateEditText.kt */
    private static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }
}
