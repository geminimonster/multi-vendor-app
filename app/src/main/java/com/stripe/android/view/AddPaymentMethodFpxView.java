package com.stripe.android.view;

import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.core.widget.ImageViewCompat;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;
import com.stripe.android.R;
import com.stripe.android.databinding.FpxBankItemBinding;
import com.stripe.android.model.FpxBankStatuses;
import com.stripe.android.model.PaymentMethod;
import com.stripe.android.model.PaymentMethodCreateParams;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.collections.ArraysKt;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u0000\u0018\u0000 \u001a2\u00020\u0001:\u0002\u0019\u001aB%\b\u0001\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\b\b\u0002\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ\u0012\u0010\u0015\u001a\u00020\u00162\b\u0010\u0017\u001a\u0004\u0018\u00010\u0018H\u0002R\u0016\u0010\t\u001a\u0004\u0018\u00010\n8VX\u0004¢\u0006\u0006\u001a\u0004\b\u000b\u0010\fR\u000e\u0010\r\u001a\u00020\u000eX\u0004¢\u0006\u0002\n\u0000R\u001b\u0010\u000f\u001a\u00020\u00108BX\u0002¢\u0006\f\n\u0004\b\u0013\u0010\u0014\u001a\u0004\b\u0011\u0010\u0012¨\u0006\u001b"}, d2 = {"Lcom/stripe/android/view/AddPaymentMethodFpxView;", "Lcom/stripe/android/view/AddPaymentMethodView;", "activity", "Landroidx/fragment/app/FragmentActivity;", "attrs", "Landroid/util/AttributeSet;", "defStyleAttr", "", "(Landroidx/fragment/app/FragmentActivity;Landroid/util/AttributeSet;I)V", "createParams", "Lcom/stripe/android/model/PaymentMethodCreateParams;", "getCreateParams", "()Lcom/stripe/android/model/PaymentMethodCreateParams;", "fpxAdapter", "Lcom/stripe/android/view/AddPaymentMethodFpxView$Adapter;", "viewModel", "Lcom/stripe/android/view/FpxViewModel;", "getViewModel", "()Lcom/stripe/android/view/FpxViewModel;", "viewModel$delegate", "Lkotlin/Lazy;", "onFpxBankStatusesUpdated", "", "fpxBankStatuses", "Lcom/stripe/android/model/FpxBankStatuses;", "Adapter", "Companion", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: AddPaymentMethodFpxView.kt */
public final class AddPaymentMethodFpxView extends AddPaymentMethodView {
    public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
    private final Adapter fpxAdapter;
    private final Lazy viewModel$delegate;

    public AddPaymentMethodFpxView(FragmentActivity fragmentActivity) {
        this(fragmentActivity, (AttributeSet) null, 0, 6, (DefaultConstructorMarker) null);
    }

    public AddPaymentMethodFpxView(FragmentActivity fragmentActivity, AttributeSet attributeSet) {
        this(fragmentActivity, attributeSet, 0, 4, (DefaultConstructorMarker) null);
    }

    /* access modifiers changed from: private */
    public final FpxViewModel getViewModel() {
        return (FpxViewModel) this.viewModel$delegate.getValue();
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ AddPaymentMethodFpxView(FragmentActivity fragmentActivity, AttributeSet attributeSet, int i, int i2, DefaultConstructorMarker defaultConstructorMarker) {
        this(fragmentActivity, (i2 & 2) != 0 ? null : attributeSet, (i2 & 4) != 0 ? 0 : i);
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public AddPaymentMethodFpxView(androidx.fragment.app.FragmentActivity r3, android.util.AttributeSet r4, int r5) {
        /*
            r2 = this;
            java.lang.String r0 = "activity"
            kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(r3, r0)
            r0 = r3
            android.content.Context r0 = (android.content.Context) r0
            r2.<init>(r0, r4, r5)
            com.stripe.android.view.AddPaymentMethodFpxView$Adapter r4 = new com.stripe.android.view.AddPaymentMethodFpxView$Adapter
            com.stripe.android.view.ThemeConfig r5 = new com.stripe.android.view.ThemeConfig
            r5.<init>(r0)
            com.stripe.android.view.AddPaymentMethodFpxView$fpxAdapter$1 r1 = new com.stripe.android.view.AddPaymentMethodFpxView$fpxAdapter$1
            r1.<init>(r2)
            kotlin.jvm.functions.Function1 r1 = (kotlin.jvm.functions.Function1) r1
            r4.<init>(r5, r1)
            r2.fpxAdapter = r4
            com.stripe.android.view.AddPaymentMethodFpxView$viewModel$2 r4 = new com.stripe.android.view.AddPaymentMethodFpxView$viewModel$2
            r4.<init>(r3)
            kotlin.jvm.functions.Function0 r4 = (kotlin.jvm.functions.Function0) r4
            kotlin.Lazy r4 = kotlin.LazyKt.lazy(r4)
            r2.viewModel$delegate = r4
            android.view.LayoutInflater r4 = r3.getLayoutInflater()
            r5 = r2
            android.view.ViewGroup r5 = (android.view.ViewGroup) r5
            r1 = 1
            com.stripe.android.databinding.FpxPaymentMethodBinding r4 = com.stripe.android.databinding.FpxPaymentMethodBinding.inflate(r4, r5, r1)
            java.lang.String r5 = "FpxPaymentMethodBinding.…           true\n        )"
            kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r4, r5)
            int r5 = com.stripe.android.R.id.stripe_payment_methods_add_fpx
            r2.setId(r5)
            androidx.recyclerview.widget.RecyclerView r4 = r4.fpxList
            com.stripe.android.view.AddPaymentMethodFpxView$Adapter r5 = r2.fpxAdapter
            androidx.recyclerview.widget.RecyclerView$Adapter r5 = (androidx.recyclerview.widget.RecyclerView.Adapter) r5
            r4.setAdapter(r5)
            r4.setHasFixedSize(r1)
            androidx.recyclerview.widget.LinearLayoutManager r5 = new androidx.recyclerview.widget.LinearLayoutManager
            r5.<init>(r0)
            androidx.recyclerview.widget.RecyclerView$LayoutManager r5 = (androidx.recyclerview.widget.RecyclerView.LayoutManager) r5
            r4.setLayoutManager(r5)
            androidx.recyclerview.widget.DefaultItemAnimator r5 = new androidx.recyclerview.widget.DefaultItemAnimator
            r5.<init>()
            androidx.recyclerview.widget.RecyclerView$ItemAnimator r5 = (androidx.recyclerview.widget.RecyclerView.ItemAnimator) r5
            r4.setItemAnimator(r5)
            com.stripe.android.view.FpxViewModel r4 = r2.getViewModel()
            androidx.lifecycle.LiveData r4 = r4.getFpxBankStatues$stripe_release()
            androidx.lifecycle.LifecycleOwner r3 = (androidx.lifecycle.LifecycleOwner) r3
            com.stripe.android.view.AddPaymentMethodFpxView$2 r5 = new com.stripe.android.view.AddPaymentMethodFpxView$2
            r0 = r2
            com.stripe.android.view.AddPaymentMethodFpxView r0 = (com.stripe.android.view.AddPaymentMethodFpxView) r0
            r5.<init>(r0)
            kotlin.jvm.functions.Function1 r5 = (kotlin.jvm.functions.Function1) r5
            com.stripe.android.view.AddPaymentMethodFpxView$sam$androidx_lifecycle_Observer$0 r0 = new com.stripe.android.view.AddPaymentMethodFpxView$sam$androidx_lifecycle_Observer$0
            r0.<init>(r5)
            androidx.lifecycle.Observer r0 = (androidx.lifecycle.Observer) r0
            r4.observe(r3, r0)
            com.stripe.android.view.FpxViewModel r3 = r2.getViewModel()
            java.lang.Integer r3 = r3.getSelectedPosition$stripe_release()
            if (r3 == 0) goto L_0x0094
            java.lang.Number r3 = (java.lang.Number) r3
            int r3 = r3.intValue()
            com.stripe.android.view.AddPaymentMethodFpxView$Adapter r4 = r2.fpxAdapter
            r4.updateSelected$stripe_release(r3)
        L_0x0094:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.stripe.android.view.AddPaymentMethodFpxView.<init>(androidx.fragment.app.FragmentActivity, android.util.AttributeSet, int):void");
    }

    public PaymentMethodCreateParams getCreateParams() {
        FpxBank selectedBank$stripe_release = this.fpxAdapter.getSelectedBank$stripe_release();
        if (selectedBank$stripe_release != null) {
            return PaymentMethodCreateParams.Companion.create$default(PaymentMethodCreateParams.Companion, new PaymentMethodCreateParams.Fpx(selectedBank$stripe_release.getCode()), (PaymentMethod.BillingDetails) null, (Map) null, 6, (Object) null);
        }
        return null;
    }

    /* access modifiers changed from: private */
    public final void onFpxBankStatusesUpdated(FpxBankStatuses fpxBankStatuses) {
        if (fpxBankStatuses != null) {
            this.fpxAdapter.updateStatuses$stripe_release(fpxBankStatuses);
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\f\n\u0002\u0010\t\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0006\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001%B!\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0012\u0010\u0005\u001a\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\b0\u0006¢\u0006\u0002\u0010\tJ\u0010\u0010\u0016\u001a\u00020\r2\u0006\u0010\u0017\u001a\u00020\u0007H\u0002J\b\u0010\u0018\u001a\u00020\u0007H\u0016J\u0010\u0010\u0019\u001a\u00020\u001a2\u0006\u0010\u0017\u001a\u00020\u0007H\u0016J\u0018\u0010\u001b\u001a\u00020\b2\u0006\u0010\u001c\u001a\u00020\u00022\u0006\u0010\u001d\u001a\u00020\u0007H\u0016J\u0018\u0010\u001e\u001a\u00020\u00022\u0006\u0010\u001f\u001a\u00020 2\u0006\u0010\u001d\u001a\u00020\u0007H\u0016J\u0015\u0010!\u001a\u00020\b2\u0006\u0010\u0017\u001a\u00020\u0007H\u0000¢\u0006\u0002\b\"J\u0015\u0010#\u001a\u00020\b2\u0006\u0010\n\u001a\u00020\u000bH\u0000¢\u0006\u0002\b$R\u000e\u0010\n\u001a\u00020\u000bX\u000e¢\u0006\u0002\n\u0000R\u001a\u0010\u0005\u001a\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\b0\u0006X\u0004¢\u0006\u0002\n\u0000R\u0016\u0010\f\u001a\u0004\u0018\u00010\r8@X\u0004¢\u0006\u0006\u001a\u0004\b\u000e\u0010\u000fR$\u0010\u0011\u001a\u00020\u00072\u0006\u0010\u0010\u001a\u00020\u0007@FX\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0012\u0010\u0013\"\u0004\b\u0014\u0010\u0015R\u000e\u0010\u0003\u001a\u00020\u0004X\u0004¢\u0006\u0002\n\u0000¨\u0006&"}, d2 = {"Lcom/stripe/android/view/AddPaymentMethodFpxView$Adapter;", "Landroidx/recyclerview/widget/RecyclerView$Adapter;", "Lcom/stripe/android/view/AddPaymentMethodFpxView$Adapter$ViewHolder;", "themeConfig", "Lcom/stripe/android/view/ThemeConfig;", "itemSelectedCallback", "Lkotlin/Function1;", "", "", "(Lcom/stripe/android/view/ThemeConfig;Lkotlin/jvm/functions/Function1;)V", "fpxBankStatuses", "Lcom/stripe/android/model/FpxBankStatuses;", "selectedBank", "Lcom/stripe/android/view/FpxBank;", "getSelectedBank$stripe_release", "()Lcom/stripe/android/view/FpxBank;", "value", "selectedPosition", "getSelectedPosition", "()I", "setSelectedPosition", "(I)V", "getItem", "position", "getItemCount", "getItemId", "", "onBindViewHolder", "viewHolder", "i", "onCreateViewHolder", "parent", "Landroid/view/ViewGroup;", "updateSelected", "updateSelected$stripe_release", "updateStatuses", "updateStatuses$stripe_release", "ViewHolder", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: AddPaymentMethodFpxView.kt */
    private static final class Adapter extends RecyclerView.Adapter<ViewHolder> {
        private FpxBankStatuses fpxBankStatuses = new FpxBankStatuses((Map) null, 1, (DefaultConstructorMarker) null);
        private final Function1<Integer, Unit> itemSelectedCallback;
        private int selectedPosition = -1;
        private final ThemeConfig themeConfig;

        public long getItemId(int i) {
            return (long) i;
        }

        public Adapter(ThemeConfig themeConfig2, Function1<? super Integer, Unit> function1) {
            Intrinsics.checkParameterIsNotNull(themeConfig2, "themeConfig");
            Intrinsics.checkParameterIsNotNull(function1, "itemSelectedCallback");
            this.themeConfig = themeConfig2;
            this.itemSelectedCallback = function1;
            setHasStableIds(true);
        }

        public final int getSelectedPosition() {
            return this.selectedPosition;
        }

        public final void setSelectedPosition(int i) {
            int i2 = this.selectedPosition;
            if (i != i2) {
                if (i2 != -1) {
                    notifyItemChanged(i2);
                }
                notifyItemChanged(i);
                this.itemSelectedCallback.invoke(Integer.valueOf(i));
            }
            this.selectedPosition = i;
        }

        public final FpxBank getSelectedBank$stripe_release() {
            int i = this.selectedPosition;
            if (i == -1) {
                return null;
            }
            return getItem(i);
        }

        public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            Intrinsics.checkParameterIsNotNull(viewGroup, "parent");
            FpxBankItemBinding inflate = FpxBankItemBinding.inflate(LayoutInflater.from(viewGroup.getContext()), viewGroup, false);
            Intrinsics.checkExpressionValueIsNotNull(inflate, "FpxBankItemBinding.infla…  false\n                )");
            return new ViewHolder(inflate, this.themeConfig);
        }

        public void onBindViewHolder(ViewHolder viewHolder, int i) {
            Intrinsics.checkParameterIsNotNull(viewHolder, "viewHolder");
            viewHolder.setSelected$stripe_release(i == this.selectedPosition);
            FpxBank item = getItem(i);
            viewHolder.update$stripe_release(item, this.fpxBankStatuses.isOnline$stripe_release(item));
            viewHolder.itemView.setOnClickListener(new AddPaymentMethodFpxView$Adapter$onBindViewHolder$1(this, viewHolder));
        }

        public int getItemCount() {
            return FpxBank.values().length;
        }

        private final FpxBank getItem(int i) {
            return FpxBank.values()[i];
        }

        public final void updateSelected$stripe_release(int i) {
            setSelectedPosition(i);
            notifyItemChanged(i);
        }

        public final void updateStatuses$stripe_release(FpxBankStatuses fpxBankStatuses2) {
            Intrinsics.checkParameterIsNotNull(fpxBankStatuses2, "fpxBankStatuses");
            this.fpxBankStatuses = fpxBankStatuses2;
            Collection arrayList = new ArrayList();
            for (Object next : ArraysKt.getIndices((T[]) FpxBank.values())) {
                if (!fpxBankStatuses2.isOnline$stripe_release(getItem(((Number) next).intValue()))) {
                    arrayList.add(next);
                }
            }
            for (Number intValue : (List) arrayList) {
                notifyItemChanged(intValue.intValue());
            }
        }

        @Metadata(bv = {1, 0, 3}, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u0002\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\u0015\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\fH\u0000¢\u0006\u0002\b\rJ\u001d\u0010\u000e\u001a\u00020\n2\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\fH\u0000¢\u0006\u0002\b\u0012R\u000e\u0010\u0007\u001a\u00020\bX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0013"}, d2 = {"Lcom/stripe/android/view/AddPaymentMethodFpxView$Adapter$ViewHolder;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "viewBinding", "Lcom/stripe/android/databinding/FpxBankItemBinding;", "themeConfig", "Lcom/stripe/android/view/ThemeConfig;", "(Lcom/stripe/android/databinding/FpxBankItemBinding;Lcom/stripe/android/view/ThemeConfig;)V", "resources", "Landroid/content/res/Resources;", "setSelected", "", "isSelected", "", "setSelected$stripe_release", "update", "fpxBank", "Lcom/stripe/android/view/FpxBank;", "isOnline", "update$stripe_release", "stripe_release"}, k = 1, mv = {1, 1, 16})
        /* compiled from: AddPaymentMethodFpxView.kt */
        private static final class ViewHolder extends RecyclerView.ViewHolder {
            private final Resources resources;
            private final ThemeConfig themeConfig;
            private final FpxBankItemBinding viewBinding;

            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public ViewHolder(FpxBankItemBinding fpxBankItemBinding, ThemeConfig themeConfig2) {
                super(fpxBankItemBinding.getRoot());
                Intrinsics.checkParameterIsNotNull(fpxBankItemBinding, "viewBinding");
                Intrinsics.checkParameterIsNotNull(themeConfig2, "themeConfig");
                this.viewBinding = fpxBankItemBinding;
                this.themeConfig = themeConfig2;
                View view = this.itemView;
                Intrinsics.checkExpressionValueIsNotNull(view, "itemView");
                Resources resources2 = view.getResources();
                Intrinsics.checkExpressionValueIsNotNull(resources2, "itemView.resources");
                this.resources = resources2;
            }

            public final void update$stripe_release(FpxBank fpxBank, boolean z) {
                CharSequence charSequence;
                Intrinsics.checkParameterIsNotNull(fpxBank, "fpxBank");
                AppCompatTextView appCompatTextView = this.viewBinding.name;
                Intrinsics.checkExpressionValueIsNotNull(appCompatTextView, "viewBinding.name");
                if (z) {
                    charSequence = fpxBank.getDisplayName();
                } else {
                    charSequence = this.resources.getString(R.string.fpx_bank_offline, new Object[]{fpxBank.getDisplayName()});
                }
                appCompatTextView.setText(charSequence);
                this.viewBinding.icon.setImageResource(fpxBank.getBrandIconResId());
            }

            public final void setSelected$stripe_release(boolean z) {
                this.viewBinding.name.setTextColor(this.themeConfig.getTextColor$stripe_release(z));
                ImageViewCompat.setImageTintList(this.viewBinding.checkIcon, ColorStateList.valueOf(this.themeConfig.getTintColor$stripe_release(z)));
                AppCompatImageView appCompatImageView = this.viewBinding.checkIcon;
                Intrinsics.checkExpressionValueIsNotNull(appCompatImageView, "viewBinding.checkIcon");
                appCompatImageView.setVisibility(z ? 0 : 8);
            }
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0015\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0000¢\u0006\u0002\b\u0007¨\u0006\b"}, d2 = {"Lcom/stripe/android/view/AddPaymentMethodFpxView$Companion;", "", "()V", "create", "Lcom/stripe/android/view/AddPaymentMethodFpxView;", "activity", "Landroidx/fragment/app/FragmentActivity;", "create$stripe_release", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: AddPaymentMethodFpxView.kt */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        public final /* synthetic */ AddPaymentMethodFpxView create$stripe_release(FragmentActivity fragmentActivity) {
            Intrinsics.checkParameterIsNotNull(fragmentActivity, "activity");
            return new AddPaymentMethodFpxView(fragmentActivity, (AttributeSet) null, 0, 6, (DefaultConstructorMarker) null);
        }
    }
}
