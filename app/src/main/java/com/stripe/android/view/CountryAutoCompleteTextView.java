package com.stripe.android.view;

import android.content.Context;
import android.content.res.Resources;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.FrameLayout;
import androidx.core.os.ConfigurationCompat;
import com.google.android.material.textfield.TextInputLayout;
import com.stripe.android.R;
import com.stripe.android.databinding.CountryAutocompleteViewBinding;
import java.util.Iterator;
import java.util.Locale;
import java.util.Set;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000T\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u000b\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\"\n\u0002\b\f\b\u0000\u0018\u00002\u00020\u0001B%\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\b\b\u0002\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ\b\u0010!\u001a\u00020\u0014H\u0002J\u0010\u0010\"\u001a\u00020#2\u0006\u0010$\u001a\u00020#H\u0002J\u001b\u0010%\u001a\u00020\u00142\f\u0010&\u001a\b\u0012\u0004\u0012\u00020#0'H\u0000¢\u0006\u0002\b(J\u0015\u0010)\u001a\u00020\u00142\u0006\u0010$\u001a\u00020#H\u0000¢\u0006\u0002\b*J\b\u0010+\u001a\u00020\u0014H\u0002J\u0015\u0010,\u001a\u00020\u00142\u0006\u0010-\u001a\u00020#H\u0001¢\u0006\u0002\b.J\u0010\u0010/\u001a\u00020\u00142\u0006\u00100\u001a\u00020\u0013H\u0002J\r\u00101\u001a\u00020\u0014H\u0000¢\u0006\u0002\b2R\u000e\u0010\t\u001a\u00020\nX\u0004¢\u0006\u0002\n\u0000R\u001c\u0010\u000b\u001a\u00020\f8\u0000X\u0004¢\u0006\u000e\n\u0000\u0012\u0004\b\r\u0010\u000e\u001a\u0004\b\u000f\u0010\u0010R&\u0010\u0011\u001a\u000e\u0012\u0004\u0012\u00020\u0013\u0012\u0004\u0012\u00020\u00140\u0012X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0015\u0010\u0016\"\u0004\b\u0017\u0010\u0018R&\u0010\u0019\u001a\u0004\u0018\u00010\u00138\u0006@\u0006X\u000e¢\u0006\u0014\n\u0000\u0012\u0004\b\u001a\u0010\u000e\u001a\u0004\b\u001b\u0010\u001c\"\u0004\b\u001d\u0010\u001eR\u000e\u0010\u001f\u001a\u00020 X\u0004¢\u0006\u0002\n\u0000¨\u00063"}, d2 = {"Lcom/stripe/android/view/CountryAutoCompleteTextView;", "Landroid/widget/FrameLayout;", "context", "Landroid/content/Context;", "attrs", "Landroid/util/AttributeSet;", "defStyleAttr", "", "(Landroid/content/Context;Landroid/util/AttributeSet;I)V", "countryAdapter", "Lcom/stripe/android/view/CountryAdapter;", "countryAutocomplete", "Landroid/widget/AutoCompleteTextView;", "countryAutocomplete$annotations", "()V", "getCountryAutocomplete$stripe_release", "()Landroid/widget/AutoCompleteTextView;", "countryChangeCallback", "Lkotlin/Function1;", "Lcom/stripe/android/view/Country;", "", "getCountryChangeCallback$stripe_release", "()Lkotlin/jvm/functions/Function1;", "setCountryChangeCallback$stripe_release", "(Lkotlin/jvm/functions/Function1;)V", "selectedCountry", "selectedCountry$annotations", "getSelectedCountry", "()Lcom/stripe/android/view/Country;", "setSelectedCountry", "(Lcom/stripe/android/view/Country;)V", "viewBinding", "Lcom/stripe/android/databinding/CountryAutocompleteViewBinding;", "clearError", "getDisplayCountry", "", "countryCode", "setAllowedCountryCodes", "allowedCountryCodes", "", "setAllowedCountryCodes$stripe_release", "setCountrySelected", "setCountrySelected$stripe_release", "updateInitialCountry", "updateUiForCountryEntered", "displayCountryEntered", "updateUiForCountryEntered$stripe_release", "updatedSelectedCountryCode", "country", "validateCountry", "validateCountry$stripe_release", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: CountryAutoCompleteTextView.kt */
public final class CountryAutoCompleteTextView extends FrameLayout {
    /* access modifiers changed from: private */
    public final CountryAdapter countryAdapter;
    private final AutoCompleteTextView countryAutocomplete;
    private /* synthetic */ Function1<? super Country, Unit> countryChangeCallback;
    private Country selectedCountry;
    /* access modifiers changed from: private */
    public final CountryAutocompleteViewBinding viewBinding;

    public CountryAutoCompleteTextView(Context context) {
        this(context, (AttributeSet) null, 0, 6, (DefaultConstructorMarker) null);
    }

    public CountryAutoCompleteTextView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0, 4, (DefaultConstructorMarker) null);
    }

    public static /* synthetic */ void countryAutocomplete$annotations() {
    }

    public static /* synthetic */ void selectedCountry$annotations() {
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ CountryAutoCompleteTextView(Context context, AttributeSet attributeSet, int i, int i2, DefaultConstructorMarker defaultConstructorMarker) {
        this(context, (i2 & 2) != 0 ? null : attributeSet, (i2 & 4) != 0 ? 0 : i);
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CountryAutoCompleteTextView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        Intrinsics.checkParameterIsNotNull(context, "context");
        CountryAutocompleteViewBinding inflate = CountryAutocompleteViewBinding.inflate(LayoutInflater.from(context), this);
        Intrinsics.checkExpressionValueIsNotNull(inflate, "CountryAutocompleteViewB…           this\n        )");
        this.viewBinding = inflate;
        CountryUtils countryUtils = CountryUtils.INSTANCE;
        Resources resources = context.getResources();
        Intrinsics.checkExpressionValueIsNotNull(resources, "context.resources");
        Locale locale = ConfigurationCompat.getLocales(resources.getConfiguration()).get(0);
        Intrinsics.checkExpressionValueIsNotNull(locale, "ConfigurationCompat.getL…sources.configuration)[0]");
        this.countryAdapter = new CountryAdapter(context, countryUtils.getOrderedCountries$stripe_release(locale));
        AutoCompleteTextView autoCompleteTextView = this.viewBinding.countryAutocomplete;
        Intrinsics.checkExpressionValueIsNotNull(autoCompleteTextView, "viewBinding.countryAutocomplete");
        this.countryAutocomplete = autoCompleteTextView;
        this.countryChangeCallback = CountryAutoCompleteTextView$countryChangeCallback$1.INSTANCE;
        this.countryAutocomplete.setThreshold(0);
        this.countryAutocomplete.setAdapter(this.countryAdapter);
        this.countryAutocomplete.setOnItemClickListener(new AdapterView.OnItemClickListener(this) {
            final /* synthetic */ CountryAutoCompleteTextView this$0;

            {
                this.this$0 = r1;
            }

            public final void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
                CountryAutoCompleteTextView countryAutoCompleteTextView = this.this$0;
                countryAutoCompleteTextView.updatedSelectedCountryCode(countryAutoCompleteTextView.countryAdapter.getItem(i));
            }
        });
        this.countryAutocomplete.setOnFocusChangeListener(new View.OnFocusChangeListener(this) {
            final /* synthetic */ CountryAutoCompleteTextView this$0;

            {
                this.this$0 = r1;
            }

            public final void onFocusChange(View view, boolean z) {
                if (z) {
                    this.this$0.getCountryAutocomplete$stripe_release().showDropDown();
                    return;
                }
                this.this$0.updateUiForCountryEntered$stripe_release(this.this$0.getCountryAutocomplete$stripe_release().getText().toString());
            }
        });
        this.selectedCountry = this.countryAdapter.getFirstItem$stripe_release();
        updateInitialCountry();
        final String string = getResources().getString(R.string.address_country_invalid);
        Intrinsics.checkExpressionValueIsNotNull(string, "resources.getString(R.st….address_country_invalid)");
        this.countryAutocomplete.setValidator(new AutoCompleteTextView.Validator(this) {
            final /* synthetic */ CountryAutoCompleteTextView this$0;

            {
                this.this$0 = r1;
            }

            public CharSequence fixText(CharSequence charSequence) {
                return charSequence != null ? charSequence : "";
            }

            public boolean isValid(CharSequence charSequence) {
                Object obj;
                Iterator it = this.this$0.countryAdapter.getUnfilteredCountries$stripe_release().iterator();
                while (true) {
                    if (!it.hasNext()) {
                        obj = null;
                        break;
                    }
                    obj = it.next();
                    if (Intrinsics.areEqual((Object) ((Country) obj).getName(), (Object) String.valueOf(charSequence))) {
                        break;
                    }
                }
                Country country = (Country) obj;
                this.this$0.setSelectedCountry(country);
                if (country != null) {
                    this.this$0.clearError();
                } else {
                    TextInputLayout textInputLayout = this.this$0.viewBinding.countryTextInputLayout;
                    Intrinsics.checkExpressionValueIsNotNull(textInputLayout, "viewBinding.countryTextInputLayout");
                    textInputLayout.setError(string);
                    TextInputLayout textInputLayout2 = this.this$0.viewBinding.countryTextInputLayout;
                    Intrinsics.checkExpressionValueIsNotNull(textInputLayout2, "viewBinding.countryTextInputLayout");
                    textInputLayout2.setErrorEnabled(true);
                }
                if (country != null) {
                    return true;
                }
                return false;
            }
        });
    }

    public final AutoCompleteTextView getCountryAutocomplete$stripe_release() {
        return this.countryAutocomplete;
    }

    public final Country getSelectedCountry() {
        return this.selectedCountry;
    }

    public final void setSelectedCountry(Country country) {
        this.selectedCountry = country;
    }

    public final Function1<Country, Unit> getCountryChangeCallback$stripe_release() {
        return this.countryChangeCallback;
    }

    public final void setCountryChangeCallback$stripe_release(Function1<? super Country, Unit> function1) {
        Intrinsics.checkParameterIsNotNull(function1, "<set-?>");
        this.countryChangeCallback = function1;
    }

    private final void updateInitialCountry() {
        Country firstItem$stripe_release = this.countryAdapter.getFirstItem$stripe_release();
        this.countryAutocomplete.setText(firstItem$stripe_release.getName());
        this.selectedCountry = firstItem$stripe_release;
        this.countryChangeCallback.invoke(firstItem$stripe_release);
    }

    public final void setAllowedCountryCodes$stripe_release(Set<String> set) {
        Intrinsics.checkParameterIsNotNull(set, "allowedCountryCodes");
        if (this.countryAdapter.updateUnfilteredCountries$stripe_release(set)) {
            updateInitialCountry();
        }
    }

    public final void setCountrySelected$stripe_release(String str) {
        Intrinsics.checkParameterIsNotNull(str, "countryCode");
        updateUiForCountryEntered$stripe_release(getDisplayCountry(str));
    }

    public final void updateUiForCountryEntered$stripe_release(String str) {
        Intrinsics.checkParameterIsNotNull(str, "displayCountryEntered");
        Country countryByName$stripe_release = CountryUtils.INSTANCE.getCountryByName$stripe_release(str);
        if (countryByName$stripe_release != null) {
            updatedSelectedCountryCode(countryByName$stripe_release);
        } else {
            Country country = this.selectedCountry;
            str = country != null ? country.getName() : null;
        }
        this.countryAutocomplete.setText(str);
    }

    /* access modifiers changed from: private */
    public final void updatedSelectedCountryCode(Country country) {
        clearError();
        if (!Intrinsics.areEqual((Object) this.selectedCountry, (Object) country)) {
            this.selectedCountry = country;
            this.countryChangeCallback.invoke(country);
        }
    }

    private final String getDisplayCountry(String str) {
        String name;
        Country countryByCode$stripe_release = CountryUtils.INSTANCE.getCountryByCode$stripe_release(str);
        if (countryByCode$stripe_release != null && (name = countryByCode$stripe_release.getName()) != null) {
            return name;
        }
        String displayCountry = new Locale("", str).getDisplayCountry();
        Intrinsics.checkExpressionValueIsNotNull(displayCountry, "Locale(\"\", countryCode).displayCountry");
        return displayCountry;
    }

    public final void validateCountry$stripe_release() {
        this.countryAutocomplete.performValidation();
    }

    /* access modifiers changed from: private */
    public final void clearError() {
        TextInputLayout textInputLayout = this.viewBinding.countryTextInputLayout;
        Intrinsics.checkExpressionValueIsNotNull(textInputLayout, "viewBinding.countryTextInputLayout");
        textInputLayout.setError((CharSequence) null);
        TextInputLayout textInputLayout2 = this.viewBinding.countryTextInputLayout;
        Intrinsics.checkExpressionValueIsNotNull(textInputLayout2, "viewBinding.countryTextInputLayout");
        textInputLayout2.setErrorEnabled(false);
    }
}
