package com.stripe.android.view;

import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.FunctionReference;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Reflection;
import kotlin.reflect.KDeclarationContainer;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0000\u001a\u00020\u00012\u0015\u0010\u0002\u001a\u00110\u0003¢\u0006\f\b\u0004\u0012\b\b\u0005\u0012\u0004\b\b(\u0006¢\u0006\u0002\b\u0007"}, d2 = {"<anonymous>", "", "p1", "Lcom/stripe/android/view/Country;", "Lkotlin/ParameterName;", "name", "country", "invoke"}, k = 3, mv = {1, 1, 16})
/* compiled from: ShippingInfoWidget.kt */
final /* synthetic */ class ShippingInfoWidget$initView$1 extends FunctionReference implements Function1<Country, Unit> {
    ShippingInfoWidget$initView$1(ShippingInfoWidget shippingInfoWidget) {
        super(1, shippingInfoWidget);
    }

    public final String getName() {
        return "updateConfigForCountry";
    }

    public final KDeclarationContainer getOwner() {
        return Reflection.getOrCreateKotlinClass(ShippingInfoWidget.class);
    }

    public final String getSignature() {
        return "updateConfigForCountry(Lcom/stripe/android/view/Country;)V";
    }

    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        invoke((Country) obj);
        return Unit.INSTANCE;
    }

    public final void invoke(Country country) {
        Intrinsics.checkParameterIsNotNull(country, "p1");
        ((ShippingInfoWidget) this.receiver).updateConfigForCountry(country);
    }
}
