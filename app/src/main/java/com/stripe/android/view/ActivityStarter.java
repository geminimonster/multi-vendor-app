package com.stripe.android.view;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import androidx.fragment.app.Fragment;
import com.stripe.android.view.ActivityStarter.Args;
import kotlin.Deprecated;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\u0004\b&\u0018\u0000*\b\b\u0000\u0010\u0001*\u00020\u0002*\b\b\u0001\u0010\u0003*\u00020\u00042\u00020\u0005:\u0002\u0016\u0017B-\b\u0010\u0012\u0006\u0010\u0006\u001a\u00020\u0002\u0012\f\u0010\u0007\u001a\b\u0012\u0004\u0012\u00028\u00000\b\u0012\u0006\u0010\t\u001a\u00028\u0001\u0012\u0006\u0010\n\u001a\u00020\u000b¢\u0006\u0002\u0010\fB-\b\u0010\u0012\u0006\u0010\r\u001a\u00020\u000e\u0012\f\u0010\u0007\u001a\b\u0012\u0004\u0012\u00028\u00000\b\u0012\u0006\u0010\t\u001a\u00028\u0001\u0012\u0006\u0010\n\u001a\u00020\u000b¢\u0006\u0002\u0010\u000fB9\b\u0000\u0012\u0006\u0010\u0006\u001a\u00020\u0002\u0012\n\b\u0002\u0010\r\u001a\u0004\u0018\u00010\u000e\u0012\f\u0010\u0007\u001a\b\u0012\u0004\u0012\u00028\u00000\b\u0012\u0006\u0010\u0010\u001a\u00028\u0001\u0012\u0006\u0010\n\u001a\u00020\u000b¢\u0006\u0002\u0010\u0011J\b\u0010\u0013\u001a\u00020\u0014H\u0007J\u0013\u0010\u0013\u001a\u00020\u00142\u0006\u0010\t\u001a\u00028\u0001¢\u0006\u0002\u0010\u0015R\u000e\u0010\u0006\u001a\u00020\u0002X\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u0010\u001a\u00028\u0001X\u0004¢\u0006\u0004\n\u0002\u0010\u0012R\u0010\u0010\r\u001a\u0004\u0018\u00010\u000eX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\u0007\u001a\b\u0012\u0004\u0012\u00028\u00000\bX\u0004¢\u0006\u0002\n\u0000¨\u0006\u0018"}, d2 = {"Lcom/stripe/android/view/ActivityStarter;", "TargetActivityType", "Landroid/app/Activity;", "ArgsType", "Lcom/stripe/android/view/ActivityStarter$Args;", "", "activity", "targetClass", "Ljava/lang/Class;", "args", "requestCode", "", "(Landroid/app/Activity;Ljava/lang/Class;Lcom/stripe/android/view/ActivityStarter$Args;I)V", "fragment", "Landroidx/fragment/app/Fragment;", "(Landroidx/fragment/app/Fragment;Ljava/lang/Class;Lcom/stripe/android/view/ActivityStarter$Args;I)V", "defaultArgs", "(Landroid/app/Activity;Landroidx/fragment/app/Fragment;Ljava/lang/Class;Lcom/stripe/android/view/ActivityStarter$Args;I)V", "Lcom/stripe/android/view/ActivityStarter$Args;", "startForResult", "", "(Lcom/stripe/android/view/ActivityStarter$Args;)V", "Args", "Result", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: ActivityStarter.kt */
public abstract class ActivityStarter<TargetActivityType extends Activity, ArgsType extends Args> {
    private final Activity activity;
    private final ArgsType defaultArgs;
    private final Fragment fragment;
    private final int requestCode;
    private final Class<TargetActivityType> targetClass;

    public ActivityStarter(Activity activity2, Fragment fragment2, Class<TargetActivityType> cls, ArgsType argstype, int i) {
        Intrinsics.checkParameterIsNotNull(activity2, "activity");
        Intrinsics.checkParameterIsNotNull(cls, "targetClass");
        Intrinsics.checkParameterIsNotNull(argstype, "defaultArgs");
        this.activity = activity2;
        this.fragment = fragment2;
        this.targetClass = cls;
        this.defaultArgs = argstype;
        this.requestCode = i;
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ ActivityStarter(Activity activity2, Fragment fragment2, Class cls, Args args, int i, int i2, DefaultConstructorMarker defaultConstructorMarker) {
        this(activity2, (i2 & 2) != 0 ? null : fragment2, cls, args, i);
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public ActivityStarter(Activity activity2, Class<TargetActivityType> cls, ArgsType argstype, int i) {
        this(activity2, (Fragment) null, cls, argstype, i);
        Intrinsics.checkParameterIsNotNull(activity2, "activity");
        Intrinsics.checkParameterIsNotNull(cls, "targetClass");
        Intrinsics.checkParameterIsNotNull(argstype, "args");
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public ActivityStarter(androidx.fragment.app.Fragment r9, java.lang.Class<TargetActivityType> r10, ArgsType r11, int r12) {
        /*
            r8 = this;
            java.lang.String r0 = "fragment"
            kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(r9, r0)
            java.lang.String r0 = "targetClass"
            kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(r10, r0)
            java.lang.String r0 = "args"
            kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(r11, r0)
            androidx.fragment.app.FragmentActivity r0 = r9.requireActivity()
            java.lang.String r1 = "fragment.requireActivity()"
            kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r0, r1)
            r3 = r0
            android.app.Activity r3 = (android.app.Activity) r3
            r2 = r8
            r4 = r9
            r5 = r10
            r6 = r11
            r7 = r12
            r2.<init>(r3, r4, r5, r6, r7)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.stripe.android.view.ActivityStarter.<init>(androidx.fragment.app.Fragment, java.lang.Class, com.stripe.android.view.ActivityStarter$Args, int):void");
    }

    @Deprecated(message = "startForResult() requires an args parameter")
    public final void startForResult() {
        startForResult(this.defaultArgs);
    }

    public final void startForResult(ArgsType argstype) {
        Intrinsics.checkParameterIsNotNull(argstype, "args");
        Intent putExtra = new Intent(this.activity, this.targetClass).putExtra("extra_activity_args", (Parcelable) argstype);
        Intrinsics.checkExpressionValueIsNotNull(putExtra, "Intent(activity, targetC…utExtra(Args.EXTRA, args)");
        Fragment fragment2 = this.fragment;
        if (fragment2 != null) {
            fragment2.startActivityForResult(putExtra, this.requestCode);
        } else {
            this.activity.startActivityForResult(putExtra, this.requestCode);
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\bf\u0018\u0000 \u00022\u00020\u0001:\u0001\u0002¨\u0006\u0003"}, d2 = {"Lcom/stripe/android/view/ActivityStarter$Args;", "Landroid/os/Parcelable;", "Companion", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: ActivityStarter.kt */
    public interface Args extends Parcelable {
        public static final Companion Companion = Companion.$$INSTANCE;
        public static final String EXTRA = "extra_activity_args";

        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\u0005"}, d2 = {"Lcom/stripe/android/view/ActivityStarter$Args$Companion;", "", "()V", "EXTRA", "", "stripe_release"}, k = 1, mv = {1, 1, 16})
        /* compiled from: ActivityStarter.kt */
        public static final class Companion {
            static final /* synthetic */ Companion $$INSTANCE = new Companion();
            public static final String EXTRA = "extra_activity_args";

            private Companion() {
            }
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\bf\u0018\u0000 \u00042\u00020\u0001:\u0001\u0004J\b\u0010\u0002\u001a\u00020\u0003H&¨\u0006\u0005"}, d2 = {"Lcom/stripe/android/view/ActivityStarter$Result;", "Landroid/os/Parcelable;", "toBundle", "Landroid/os/Bundle;", "Companion", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: ActivityStarter.kt */
    public interface Result extends Parcelable {
        public static final Companion Companion = Companion.$$INSTANCE;
        public static final String EXTRA = "extra_activity_result";

        Bundle toBundle();

        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\u0005"}, d2 = {"Lcom/stripe/android/view/ActivityStarter$Result$Companion;", "", "()V", "EXTRA", "", "stripe_release"}, k = 1, mv = {1, 1, 16})
        /* compiled from: ActivityStarter.kt */
        public static final class Companion {
            static final /* synthetic */ Companion $$INSTANCE = new Companion();
            public static final String EXTRA = "extra_activity_result";

            private Companion() {
            }
        }
    }
}
