package com.stripe.android.view;

import android.app.Application;
import android.content.res.Resources;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import com.stripe.android.CustomerSession;
import com.stripe.android.PaymentSession;
import com.stripe.android.R;
import com.stripe.android.model.PaymentMethod;
import java.util.List;
import java.util.Set;
import kotlin.Metadata;
import kotlin.Result;
import kotlin.collections.CollectionsKt;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\"\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0005\b\u0000\u0018\u00002\u00020\u0001:\u0001-B)\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u0012\u0006\u0010\b\u001a\u00020\t¢\u0006\u0002\u0010\nJ\u001c\u0010\u001e\u001a\u0004\u0018\u00010\u00072\u0006\u0010\u001f\u001a\u00020 2\b\b\u0001\u0010!\u001a\u00020\"H\u0002J\"\u0010#\u001a\u0014\u0012\u0010\u0012\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020 0&0%0$H\u0000ø\u0001\u0000¢\u0006\u0002\b'J\u0015\u0010(\u001a\u00020)2\u0006\u0010\u001f\u001a\u00020 H\u0000¢\u0006\u0002\b*J\u0015\u0010+\u001a\u00020)2\u0006\u0010\u001f\u001a\u00020 H\u0000¢\u0006\u0002\b,R\u000e\u0010\u000b\u001a\u00020\fX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000R\u001a\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u00070\u000eX\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010R\u001a\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\t0\u0012X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u0014R\u0016\u0010\u0015\u001a\n \u0017*\u0004\u0018\u00010\u00160\u0016X\u0004¢\u0006\u0002\n\u0000R\u001c\u0010\u0006\u001a\u0004\u0018\u00010\u0007X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0018\u0010\u0019\"\u0004\b\u001a\u0010\u001bR\u001c\u0010\u001c\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00070\u0012X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u001d\u0010\u0014R\u000e\u0010\b\u001a\u00020\tX\u0004¢\u0006\u0002\n\u0000\u0002\u0004\n\u0002\b\u0019¨\u0006."}, d2 = {"Lcom/stripe/android/view/PaymentMethodsViewModel;", "Landroidx/lifecycle/AndroidViewModel;", "application", "Landroid/app/Application;", "customerSession", "Lcom/stripe/android/CustomerSession;", "selectedPaymentMethodId", "", "startedFromPaymentSession", "", "(Landroid/app/Application;Lcom/stripe/android/CustomerSession;Ljava/lang/String;Z)V", "cardDisplayTextFactory", "Lcom/stripe/android/view/CardDisplayTextFactory;", "productUsage", "", "getProductUsage$stripe_release", "()Ljava/util/Set;", "progressData", "Landroidx/lifecycle/MutableLiveData;", "getProgressData$stripe_release", "()Landroidx/lifecycle/MutableLiveData;", "resources", "Landroid/content/res/Resources;", "kotlin.jvm.PlatformType", "getSelectedPaymentMethodId$stripe_release", "()Ljava/lang/String;", "setSelectedPaymentMethodId$stripe_release", "(Ljava/lang/String;)V", "snackbarData", "getSnackbarData$stripe_release", "createSnackbarText", "paymentMethod", "Lcom/stripe/android/model/PaymentMethod;", "stringRes", "", "getPaymentMethods", "Landroidx/lifecycle/LiveData;", "Lkotlin/Result;", "", "getPaymentMethods$stripe_release", "onPaymentMethodAdded", "", "onPaymentMethodAdded$stripe_release", "onPaymentMethodRemoved", "onPaymentMethodRemoved$stripe_release", "Factory", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: PaymentMethodsViewModel.kt */
public final class PaymentMethodsViewModel extends AndroidViewModel {
    private final CardDisplayTextFactory cardDisplayTextFactory;
    private final CustomerSession customerSession;
    private final Set<String> productUsage;
    private final MutableLiveData<Boolean> progressData;
    private final Resources resources;
    private String selectedPaymentMethodId;
    private final MutableLiveData<String> snackbarData;
    private final boolean startedFromPaymentSession;

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ PaymentMethodsViewModel(Application application, CustomerSession customerSession2, String str, boolean z, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(application, customerSession2, (i & 4) != 0 ? null : str, z);
    }

    public final String getSelectedPaymentMethodId$stripe_release() {
        return this.selectedPaymentMethodId;
    }

    public final void setSelectedPaymentMethodId$stripe_release(String str) {
        this.selectedPaymentMethodId = str;
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public PaymentMethodsViewModel(Application application, CustomerSession customerSession2, String str, boolean z) {
        super(application);
        Intrinsics.checkParameterIsNotNull(application, "application");
        Intrinsics.checkParameterIsNotNull(customerSession2, "customerSession");
        this.customerSession = customerSession2;
        this.selectedPaymentMethodId = str;
        this.startedFromPaymentSession = z;
        this.resources = application.getResources();
        this.cardDisplayTextFactory = new CardDisplayTextFactory(application);
        String[] strArr = new String[2];
        strArr[0] = this.startedFromPaymentSession ? PaymentSession.PRODUCT_TOKEN : null;
        strArr[1] = PaymentMethodsActivity.PRODUCT_TOKEN;
        this.productUsage = CollectionsKt.toSet(CollectionsKt.listOfNotNull((T[]) strArr));
        this.snackbarData = new MutableLiveData<>();
        this.progressData = new MutableLiveData<>();
    }

    public final Set<String> getProductUsage$stripe_release() {
        return this.productUsage;
    }

    public final MutableLiveData<String> getSnackbarData$stripe_release() {
        return this.snackbarData;
    }

    public final MutableLiveData<Boolean> getProgressData$stripe_release() {
        return this.progressData;
    }

    public final void onPaymentMethodAdded$stripe_release(PaymentMethod paymentMethod) {
        Intrinsics.checkParameterIsNotNull(paymentMethod, "paymentMethod");
        String createSnackbarText = createSnackbarText(paymentMethod, R.string.added);
        if (createSnackbarText != null) {
            this.snackbarData.setValue(createSnackbarText);
            this.snackbarData.setValue(null);
        }
    }

    public final void onPaymentMethodRemoved$stripe_release(PaymentMethod paymentMethod) {
        Intrinsics.checkParameterIsNotNull(paymentMethod, "paymentMethod");
        String createSnackbarText = createSnackbarText(paymentMethod, R.string.removed);
        if (createSnackbarText != null) {
            this.snackbarData.setValue(createSnackbarText);
            this.snackbarData.setValue(null);
        }
    }

    private final String createSnackbarText(PaymentMethod paymentMethod, int i) {
        PaymentMethod.Card card = paymentMethod.card;
        if (card == null) {
            return null;
        }
        return this.resources.getString(i, new Object[]{this.cardDisplayTextFactory.createUnstyled$stripe_release(card)});
    }

    public final /* synthetic */ LiveData<Result<List<PaymentMethod>>> getPaymentMethods$stripe_release() {
        MutableLiveData mutableLiveData = new MutableLiveData();
        this.progressData.setValue(true);
        CustomerSession.getPaymentMethods$stripe_release$default(this.customerSession, PaymentMethod.Type.Card, (Integer) null, (String) null, (String) null, this.productUsage, new PaymentMethodsViewModel$getPaymentMethods$1(this, mutableLiveData), 14, (Object) null);
        return mutableLiveData;
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0000\u0018\u00002\u00020\u0001B'\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\b\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u0012\u0006\u0010\b\u001a\u00020\t¢\u0006\u0002\u0010\nJ'\u0010\u000b\u001a\u0002H\f\"\n\b\u0000\u0010\f*\u0004\u0018\u00010\r2\f\u0010\u000e\u001a\b\u0012\u0004\u0012\u0002H\f0\u000fH\u0016¢\u0006\u0002\u0010\u0010R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u0006\u001a\u0004\u0018\u00010\u0007X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0004¢\u0006\u0002\n\u0000¨\u0006\u0011"}, d2 = {"Lcom/stripe/android/view/PaymentMethodsViewModel$Factory;", "Landroidx/lifecycle/ViewModelProvider$Factory;", "application", "Landroid/app/Application;", "customerSession", "Lcom/stripe/android/CustomerSession;", "initialPaymentMethodId", "", "startedFromPaymentSession", "", "(Landroid/app/Application;Lcom/stripe/android/CustomerSession;Ljava/lang/String;Z)V", "create", "T", "Landroidx/lifecycle/ViewModel;", "modelClass", "Ljava/lang/Class;", "(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: PaymentMethodsViewModel.kt */
    public static final class Factory implements ViewModelProvider.Factory {
        private final Application application;
        private final CustomerSession customerSession;
        private final String initialPaymentMethodId;
        private final boolean startedFromPaymentSession;

        public Factory(Application application2, CustomerSession customerSession2, String str, boolean z) {
            Intrinsics.checkParameterIsNotNull(application2, "application");
            Intrinsics.checkParameterIsNotNull(customerSession2, "customerSession");
            this.application = application2;
            this.customerSession = customerSession2;
            this.initialPaymentMethodId = str;
            this.startedFromPaymentSession = z;
        }

        public <T extends ViewModel> T create(Class<T> cls) {
            Intrinsics.checkParameterIsNotNull(cls, "modelClass");
            return (ViewModel) new PaymentMethodsViewModel(this.application, this.customerSession, this.initialPaymentMethodId, this.startedFromPaymentSession);
        }
    }
}
