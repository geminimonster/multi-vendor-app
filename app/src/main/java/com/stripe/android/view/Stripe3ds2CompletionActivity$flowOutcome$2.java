package com.stripe.android.view;

import com.stripe.android.stripe3ds2.transaction.ChallengeCompletionIntentStarter;
import com.stripe.android.stripe3ds2.transaction.ChallengeFlowOutcome;
import com.stripe.android.view.Stripe3ds2CompletionActivity;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.Lambda;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0000\n\u0002\u0010\b\n\u0000\u0010\u0000\u001a\u00020\u0001H\n¢\u0006\u0002\b\u0002"}, d2 = {"<anonymous>", "", "invoke"}, k = 3, mv = {1, 1, 16})
/* compiled from: Stripe3ds2CompletionActivity.kt */
final class Stripe3ds2CompletionActivity$flowOutcome$2 extends Lambda implements Function0<Integer> {
    final /* synthetic */ Stripe3ds2CompletionActivity this$0;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    Stripe3ds2CompletionActivity$flowOutcome$2(Stripe3ds2CompletionActivity stripe3ds2CompletionActivity) {
        super(0);
        this.this$0 = stripe3ds2CompletionActivity;
    }

    public final int invoke() {
        int intExtra = this.this$0.getIntent().getIntExtra(ChallengeCompletionIntentStarter.EXTRA_OUTCOME, -1);
        if (intExtra == -1) {
            return 0;
        }
        switch (Stripe3ds2CompletionActivity.WhenMappings.$EnumSwitchMapping$0[ChallengeFlowOutcome.values()[intExtra].ordinal()]) {
            case 1:
                return 1;
            case 2:
                return 3;
            case 3:
                return 4;
            case 4:
            case 5:
            case 6:
                return 2;
            default:
                throw new NoWhenBranchMatchedException();
        }
    }
}
