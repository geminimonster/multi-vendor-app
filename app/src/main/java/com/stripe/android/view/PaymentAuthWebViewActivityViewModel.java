package com.stripe.android.view;

import android.content.Intent;
import android.net.Uri;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import com.stripe.android.PaymentAuthWebViewStarter;
import com.stripe.android.PaymentController;
import com.stripe.android.exception.StripeException;
import com.stripe.android.model.Source;
import com.stripe.android.stripe3ds2.init.ui.StripeToolbarCustomization;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\b\u0000\u0018\u00002\u00020\u0001:\u0002\u0017\u0018B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u0013\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u00150\u0014H\u0000¢\u0006\u0002\b\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u0016\u0010\u0005\u001a\u0004\u0018\u00010\u0006X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0014\u0010\t\u001a\u00020\n8@X\u0004¢\u0006\u0006\u001a\u0004\b\u000b\u0010\fR\u0016\u0010\r\u001a\u0004\u0018\u00010\u0006X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\bR\u0016\u0010\u000f\u001a\u0004\u0018\u00010\u0010X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u0012¨\u0006\u0019"}, d2 = {"Lcom/stripe/android/view/PaymentAuthWebViewActivityViewModel;", "Landroidx/lifecycle/ViewModel;", "args", "Lcom/stripe/android/PaymentAuthWebViewStarter$Args;", "(Lcom/stripe/android/PaymentAuthWebViewStarter$Args;)V", "buttonText", "", "getButtonText$stripe_release", "()Ljava/lang/String;", "paymentResult", "Lcom/stripe/android/PaymentController$Result;", "getPaymentResult$stripe_release", "()Lcom/stripe/android/PaymentController$Result;", "toolbarBackgroundColor", "getToolbarBackgroundColor$stripe_release", "toolbarTitle", "Lcom/stripe/android/view/PaymentAuthWebViewActivityViewModel$ToolbarTitleData;", "getToolbarTitle$stripe_release", "()Lcom/stripe/android/view/PaymentAuthWebViewActivityViewModel$ToolbarTitleData;", "cancelIntentSource", "Landroidx/lifecycle/LiveData;", "Landroid/content/Intent;", "cancelIntentSource$stripe_release", "Factory", "ToolbarTitleData", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: PaymentAuthWebViewActivityViewModel.kt */
public final class PaymentAuthWebViewActivityViewModel extends ViewModel {
    private final PaymentAuthWebViewStarter.Args args;
    private final /* synthetic */ String buttonText;
    private final /* synthetic */ String toolbarBackgroundColor;
    private final /* synthetic */ ToolbarTitleData toolbarTitle;

    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0026, code lost:
        if ((r3 == null || kotlin.text.StringsKt.isBlank(r3)) == false) goto L_0x002a;
     */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0060  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public PaymentAuthWebViewActivityViewModel(com.stripe.android.PaymentAuthWebViewStarter.Args r6) {
        /*
            r5 = this;
            java.lang.String r0 = "args"
            kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(r6, r0)
            r5.<init>()
            r5.args = r6
            com.stripe.android.stripe3ds2.init.ui.StripeToolbarCustomization r6 = r6.getToolbarCustomization()
            r0 = 0
            r1 = 1
            r2 = 0
            if (r6 == 0) goto L_0x0029
            java.lang.String r6 = r6.getButtonText()
            r3 = r6
            java.lang.CharSequence r3 = (java.lang.CharSequence) r3
            if (r3 == 0) goto L_0x0025
            boolean r3 = kotlin.text.StringsKt.isBlank(r3)
            if (r3 == 0) goto L_0x0023
            goto L_0x0025
        L_0x0023:
            r3 = 0
            goto L_0x0026
        L_0x0025:
            r3 = 1
        L_0x0026:
            if (r3 != 0) goto L_0x0029
            goto L_0x002a
        L_0x0029:
            r6 = r2
        L_0x002a:
            r5.buttonText = r6
            com.stripe.android.PaymentAuthWebViewStarter$Args r6 = r5.args
            com.stripe.android.stripe3ds2.init.ui.StripeToolbarCustomization r6 = r6.getToolbarCustomization()
            if (r6 == 0) goto L_0x0055
            java.lang.String r3 = r6.getHeaderText()
            r4 = r3
            java.lang.CharSequence r4 = (java.lang.CharSequence) r4
            if (r4 == 0) goto L_0x0043
            boolean r4 = kotlin.text.StringsKt.isBlank(r4)
            if (r4 == 0) goto L_0x0044
        L_0x0043:
            r0 = 1
        L_0x0044:
            if (r0 != 0) goto L_0x0047
            goto L_0x0048
        L_0x0047:
            r3 = r2
        L_0x0048:
            if (r3 == 0) goto L_0x0055
            com.stripe.android.view.PaymentAuthWebViewActivityViewModel$ToolbarTitleData r0 = new com.stripe.android.view.PaymentAuthWebViewActivityViewModel$ToolbarTitleData
            java.lang.String r1 = "it"
            kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r3, r1)
            r0.<init>(r3, r6)
            goto L_0x0056
        L_0x0055:
            r0 = r2
        L_0x0056:
            r5.toolbarTitle = r0
            com.stripe.android.PaymentAuthWebViewStarter$Args r6 = r5.args
            com.stripe.android.stripe3ds2.init.ui.StripeToolbarCustomization r6 = r6.getToolbarCustomization()
            if (r6 == 0) goto L_0x0064
            java.lang.String r2 = r6.getBackgroundColor()
        L_0x0064:
            r5.toolbarBackgroundColor = r2
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.stripe.android.view.PaymentAuthWebViewActivityViewModel.<init>(com.stripe.android.PaymentAuthWebViewStarter$Args):void");
    }

    public final String getButtonText$stripe_release() {
        return this.buttonText;
    }

    public final ToolbarTitleData getToolbarTitle$stripe_release() {
        return this.toolbarTitle;
    }

    public final String getToolbarBackgroundColor$stripe_release() {
        return this.toolbarBackgroundColor;
    }

    public final /* synthetic */ PaymentController.Result getPaymentResult$stripe_release() {
        String clientSecret = this.args.getClientSecret();
        Uri parse = Uri.parse(this.args.getUrl());
        Intrinsics.checkExpressionValueIsNotNull(parse, "Uri.parse(args.url)");
        String lastPathSegment = parse.getLastPathSegment();
        if (lastPathSegment == null) {
            lastPathSegment = "";
        }
        return new PaymentController.Result(clientSecret, 0, (StripeException) null, false, lastPathSegment, (Source) null, this.args.getStripeAccountId(), 46, (DefaultConstructorMarker) null);
    }

    public final /* synthetic */ LiveData<Intent> cancelIntentSource$stripe_release() {
        MutableLiveData mutableLiveData = new MutableLiveData();
        mutableLiveData.setValue(new Intent().putExtras(PaymentController.Result.copy$default(getPaymentResult$stripe_release(), (String) null, 3, (StripeException) null, true, (String) null, (Source) null, (String) null, 117, (Object) null).toBundle()));
        return mutableLiveData;
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u000b\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\b\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\u000e\u0010\u000b\u001a\u00020\u0003HÀ\u0003¢\u0006\u0002\b\fJ\u000e\u0010\r\u001a\u00020\u0005HÀ\u0003¢\u0006\u0002\b\u000eJ\u001d\u0010\u000f\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u0005HÆ\u0001J\u0013\u0010\u0010\u001a\u00020\u00112\b\u0010\u0012\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0013\u001a\u00020\u0014HÖ\u0001J\t\u0010\u0015\u001a\u00020\u0003HÖ\u0001R\u0014\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0014\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n¨\u0006\u0016"}, d2 = {"Lcom/stripe/android/view/PaymentAuthWebViewActivityViewModel$ToolbarTitleData;", "", "text", "", "toolbarCustomization", "Lcom/stripe/android/stripe3ds2/init/ui/StripeToolbarCustomization;", "(Ljava/lang/String;Lcom/stripe/android/stripe3ds2/init/ui/StripeToolbarCustomization;)V", "getText$stripe_release", "()Ljava/lang/String;", "getToolbarCustomization$stripe_release", "()Lcom/stripe/android/stripe3ds2/init/ui/StripeToolbarCustomization;", "component1", "component1$stripe_release", "component2", "component2$stripe_release", "copy", "equals", "", "other", "hashCode", "", "toString", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: PaymentAuthWebViewActivityViewModel.kt */
    public static final class ToolbarTitleData {
        private final String text;
        private final StripeToolbarCustomization toolbarCustomization;

        public static /* synthetic */ ToolbarTitleData copy$default(ToolbarTitleData toolbarTitleData, String str, StripeToolbarCustomization stripeToolbarCustomization, int i, Object obj) {
            if ((i & 1) != 0) {
                str = toolbarTitleData.text;
            }
            if ((i & 2) != 0) {
                stripeToolbarCustomization = toolbarTitleData.toolbarCustomization;
            }
            return toolbarTitleData.copy(str, stripeToolbarCustomization);
        }

        public final String component1$stripe_release() {
            return this.text;
        }

        public final StripeToolbarCustomization component2$stripe_release() {
            return this.toolbarCustomization;
        }

        public final ToolbarTitleData copy(String str, StripeToolbarCustomization stripeToolbarCustomization) {
            Intrinsics.checkParameterIsNotNull(str, "text");
            Intrinsics.checkParameterIsNotNull(stripeToolbarCustomization, "toolbarCustomization");
            return new ToolbarTitleData(str, stripeToolbarCustomization);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof ToolbarTitleData)) {
                return false;
            }
            ToolbarTitleData toolbarTitleData = (ToolbarTitleData) obj;
            return Intrinsics.areEqual((Object) this.text, (Object) toolbarTitleData.text) && Intrinsics.areEqual((Object) this.toolbarCustomization, (Object) toolbarTitleData.toolbarCustomization);
        }

        public int hashCode() {
            String str = this.text;
            int i = 0;
            int hashCode = (str != null ? str.hashCode() : 0) * 31;
            StripeToolbarCustomization stripeToolbarCustomization = this.toolbarCustomization;
            if (stripeToolbarCustomization != null) {
                i = stripeToolbarCustomization.hashCode();
            }
            return hashCode + i;
        }

        public String toString() {
            return "ToolbarTitleData(text=" + this.text + ", toolbarCustomization=" + this.toolbarCustomization + ")";
        }

        public ToolbarTitleData(String str, StripeToolbarCustomization stripeToolbarCustomization) {
            Intrinsics.checkParameterIsNotNull(str, "text");
            Intrinsics.checkParameterIsNotNull(stripeToolbarCustomization, "toolbarCustomization");
            this.text = str;
            this.toolbarCustomization = stripeToolbarCustomization;
        }

        public final String getText$stripe_release() {
            return this.text;
        }

        public final StripeToolbarCustomization getToolbarCustomization$stripe_release() {
            return this.toolbarCustomization;
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J'\u0010\u0005\u001a\u0002H\u0006\"\n\b\u0000\u0010\u0006*\u0004\u0018\u00010\u00072\f\u0010\b\u001a\b\u0012\u0004\u0012\u0002H\u00060\tH\u0016¢\u0006\u0002\u0010\nR\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\u000b"}, d2 = {"Lcom/stripe/android/view/PaymentAuthWebViewActivityViewModel$Factory;", "Landroidx/lifecycle/ViewModelProvider$Factory;", "args", "Lcom/stripe/android/PaymentAuthWebViewStarter$Args;", "(Lcom/stripe/android/PaymentAuthWebViewStarter$Args;)V", "create", "T", "Landroidx/lifecycle/ViewModel;", "modelClass", "Ljava/lang/Class;", "(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: PaymentAuthWebViewActivityViewModel.kt */
    public static final class Factory implements ViewModelProvider.Factory {
        private final PaymentAuthWebViewStarter.Args args;

        public Factory(PaymentAuthWebViewStarter.Args args2) {
            Intrinsics.checkParameterIsNotNull(args2, "args");
            this.args = args2;
        }

        public <T extends ViewModel> T create(Class<T> cls) {
            Intrinsics.checkParameterIsNotNull(cls, "modelClass");
            return (ViewModel) new PaymentAuthWebViewActivityViewModel(this.args);
        }
    }
}
