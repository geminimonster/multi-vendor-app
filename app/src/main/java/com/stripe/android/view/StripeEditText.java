package com.stripe.android.view;

import android.content.Context;
import android.content.res.ColorStateList;
import android.os.Build;
import android.os.Handler;
import android.text.Editable;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputConnection;
import android.view.inputmethod.InputConnectionWrapper;
import androidx.appcompat.R;
import androidx.core.content.ContextCompat;
import com.google.android.material.textfield.TextInputEditText;
import com.stripe.android.view.StripeColorUtils;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\r\n\u0000\n\u0002\u0010\t\n\u0002\b\u0007\b\u0016\u0018\u00002\u00020\u0001:\u0004JKLMB%\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\b\b\u0002\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ\b\u0010.\u001a\u00020/H\u0002J\u0018\u00100\u001a\u00020'2\u0006\u00101\u001a\u00020\u00072\u0006\u00102\u001a\u000203H\u0002J\b\u00104\u001a\u00020/H\u0002J\b\u00105\u001a\u00020/H\u0002J\u0012\u00106\u001a\u0004\u0018\u0001072\u0006\u00108\u001a\u000209H\u0016J\b\u0010:\u001a\u00020/H\u0014J\u0010\u0010;\u001a\u00020/2\u0006\u0010<\u001a\u00020=H\u0016J\u0010\u0010>\u001a\u00020/2\b\u0010\r\u001a\u0004\u0018\u00010\u000eJ\u0010\u0010?\u001a\u00020/2\b\u0010\u0018\u001a\u0004\u0018\u00010\u0019J\u0010\u0010@\u001a\u00020/2\b\b\u0001\u0010\u001a\u001a\u00020\u0007J\u0010\u0010A\u001a\u00020/2\b\u0010\u001c\u001a\u0004\u0018\u00010\nJ\u0010\u0010B\u001a\u00020/2\b\u0010 \u001a\u0004\u0018\u00010!J\u0016\u0010C\u001a\u00020/2\u0006\u0010D\u001a\u00020E2\u0006\u0010F\u001a\u00020GJ\u0018\u0010C\u001a\u00020/2\b\b\u0001\u0010H\u001a\u00020\u00072\u0006\u0010F\u001a\u00020GJ\u0010\u0010I\u001a\u00020/2\u0006\u0010D\u001a\u00020EH\u0002R\u0016\u0010\t\u001a\u0004\u0018\u00010\nX\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0010\u0010\r\u001a\u0004\u0018\u00010\u000eX\u000e¢\u0006\u0002\n\u0000R\"\u0010\u0011\u001a\u0004\u0018\u00010\u00102\b\u0010\u000f\u001a\u0004\u0018\u00010\u0010@BX\u000e¢\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u0013R\u0012\u0010\u0014\u001a\u00020\u00078\u0002@\u0002X\u000e¢\u0006\u0002\n\u0000R\u0011\u0010\u0015\u001a\u00020\u00078G¢\u0006\u0006\u001a\u0004\b\u0016\u0010\u0017R\u0010\u0010\u0018\u001a\u0004\u0018\u00010\u0019X\u000e¢\u0006\u0002\n\u0000R\u0016\u0010\u001a\u001a\u0004\u0018\u00010\u00078\u0002@\u0002X\u000e¢\u0006\u0004\n\u0002\u0010\u001bR\u001c\u0010\u001c\u001a\u0004\u0018\u00010\nX\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u001d\u0010\f\"\u0004\b\u001e\u0010\u001fR\u0010\u0010 \u001a\u0004\u0018\u00010!X\u000e¢\u0006\u0002\n\u0000R\u0014\u0010\"\u001a\u00020\n8@X\u0004¢\u0006\u0006\u001a\u0004\b#\u0010\fR\u000e\u0010$\u001a\u00020%X\u0004¢\u0006\u0002\n\u0000R\u001a\u0010&\u001a\u00020'X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b&\u0010(\"\u0004\b)\u0010*R$\u0010+\u001a\u00020'2\u0006\u0010+\u001a\u00020'@FX\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b,\u0010(\"\u0004\b-\u0010*¨\u0006N"}, d2 = {"Lcom/stripe/android/view/StripeEditText;", "Lcom/google/android/material/textfield/TextInputEditText;", "context", "Landroid/content/Context;", "attrs", "Landroid/util/AttributeSet;", "defStyleAttr", "", "(Landroid/content/Context;Landroid/util/AttributeSet;I)V", "accessibilityText", "", "getAccessibilityText", "()Ljava/lang/String;", "afterTextChangedListener", "Lcom/stripe/android/view/StripeEditText$AfterTextChangedListener;", "<set-?>", "Landroid/content/res/ColorStateList;", "cachedColorStateList", "getCachedColorStateList", "()Landroid/content/res/ColorStateList;", "defaultErrorColor", "defaultErrorColorInt", "getDefaultErrorColorInt", "()I", "deleteEmptyListener", "Lcom/stripe/android/view/StripeEditText$DeleteEmptyListener;", "errorColor", "Ljava/lang/Integer;", "errorMessage", "getErrorMessage$stripe_release", "setErrorMessage$stripe_release", "(Ljava/lang/String;)V", "errorMessageListener", "Lcom/stripe/android/view/StripeEditText$ErrorMessageListener;", "fieldText", "getFieldText$stripe_release", "hintHandler", "Landroid/os/Handler;", "isLastKeyDelete", "", "()Z", "setLastKeyDelete", "(Z)V", "shouldShowError", "getShouldShowError", "setShouldShowError", "determineDefaultErrorColor", "", "isDeleteKey", "keyCode", "event", "Landroid/view/KeyEvent;", "listenForDeleteEmpty", "listenForTextChanges", "onCreateInputConnection", "Landroid/view/inputmethod/InputConnection;", "outAttrs", "Landroid/view/inputmethod/EditorInfo;", "onDetachedFromWindow", "onInitializeAccessibilityNodeInfo", "info", "Landroid/view/accessibility/AccessibilityNodeInfo;", "setAfterTextChangedListener", "setDeleteEmptyListener", "setErrorColor", "setErrorMessage", "setErrorMessageListener", "setHintDelayed", "hint", "", "delayMilliseconds", "", "hintResource", "setHintSafely", "AfterTextChangedListener", "DeleteEmptyListener", "ErrorMessageListener", "SoftDeleteInputConnection", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: StripeEditText.kt */
public class StripeEditText extends TextInputEditText {
    private final String accessibilityText;
    /* access modifiers changed from: private */
    public AfterTextChangedListener afterTextChangedListener;
    private ColorStateList cachedColorStateList;
    private int defaultErrorColor;
    /* access modifiers changed from: private */
    public DeleteEmptyListener deleteEmptyListener;
    private Integer errorColor;
    private String errorMessage;
    private ErrorMessageListener errorMessageListener;
    private final Handler hintHandler;
    private boolean isLastKeyDelete;
    private boolean shouldShowError;

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\bf\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&¨\u0006\u0006"}, d2 = {"Lcom/stripe/android/view/StripeEditText$AfterTextChangedListener;", "", "onTextChanged", "", "text", "", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: StripeEditText.kt */
    public interface AfterTextChangedListener {
        void onTextChanged(String str);
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\bf\u0018\u00002\u00020\u0001J\b\u0010\u0002\u001a\u00020\u0003H&¨\u0006\u0004"}, d2 = {"Lcom/stripe/android/view/StripeEditText$DeleteEmptyListener;", "", "onDeleteEmpty", "", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: StripeEditText.kt */
    public interface DeleteEmptyListener {
        void onDeleteEmpty();
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\bf\u0018\u00002\u00020\u0001J\u0012\u0010\u0002\u001a\u00020\u00032\b\u0010\u0004\u001a\u0004\u0018\u00010\u0005H&¨\u0006\u0006"}, d2 = {"Lcom/stripe/android/view/StripeEditText$ErrorMessageListener;", "", "displayErrorMessage", "", "message", "", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: StripeEditText.kt */
    public interface ErrorMessageListener {
        void displayErrorMessage(String str);
    }

    public StripeEditText(Context context) {
        this(context, (AttributeSet) null, 0, 6, (DefaultConstructorMarker) null);
    }

    public StripeEditText(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0, 4, (DefaultConstructorMarker) null);
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ StripeEditText(Context context, AttributeSet attributeSet, int i, int i2, DefaultConstructorMarker defaultConstructorMarker) {
        this(context, (i2 & 2) != 0 ? null : attributeSet, (i2 & 4) != 0 ? R.attr.editTextStyle : i);
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public StripeEditText(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        Intrinsics.checkParameterIsNotNull(context, "context");
        this.hintHandler = new Handler();
        setMaxLines(1);
        listenForTextChanges();
        listenForDeleteEmpty();
        determineDefaultErrorColor();
        this.cachedColorStateList = getTextColors();
    }

    /* access modifiers changed from: protected */
    public final boolean isLastKeyDelete() {
        return this.isLastKeyDelete;
    }

    /* access modifiers changed from: protected */
    public final void setLastKeyDelete(boolean z) {
        this.isLastKeyDelete = z;
    }

    public final ColorStateList getCachedColorStateList() {
        return this.cachedColorStateList;
    }

    public final boolean getShouldShowError() {
        return this.shouldShowError;
    }

    public final void setShouldShowError(boolean z) {
        ErrorMessageListener errorMessageListener2;
        String str = this.errorMessage;
        if (!(str == null || (errorMessageListener2 = this.errorMessageListener) == null)) {
            if (!z) {
                str = null;
            }
            errorMessageListener2.displayErrorMessage(str);
        }
        if (this.shouldShowError != z) {
            if (z) {
                Integer num = this.errorColor;
                setTextColor(num != null ? num.intValue() : this.defaultErrorColor);
            } else {
                setTextColor(this.cachedColorStateList);
            }
            refreshDrawableState();
        }
        this.shouldShowError = z;
    }

    public final String getErrorMessage$stripe_release() {
        return this.errorMessage;
    }

    public final void setErrorMessage$stripe_release(String str) {
        this.errorMessage = str;
    }

    public final String getFieldText$stripe_release() {
        Editable text = getText();
        String obj = text != null ? text.toString() : null;
        return obj != null ? obj : "";
    }

    public final int getDefaultErrorColorInt() {
        determineDefaultErrorColor();
        return this.defaultErrorColor;
    }

    /* access modifiers changed from: protected */
    public String getAccessibilityText() {
        return this.accessibilityText;
    }

    public InputConnection onCreateInputConnection(EditorInfo editorInfo) {
        Intrinsics.checkParameterIsNotNull(editorInfo, "outAttrs");
        InputConnection onCreateInputConnection = super.onCreateInputConnection(editorInfo);
        return onCreateInputConnection != null ? new SoftDeleteInputConnection(onCreateInputConnection, true, this.deleteEmptyListener) : null;
    }

    public final void setAfterTextChangedListener(AfterTextChangedListener afterTextChangedListener2) {
        this.afterTextChangedListener = afterTextChangedListener2;
    }

    public final void setDeleteEmptyListener(DeleteEmptyListener deleteEmptyListener2) {
        this.deleteEmptyListener = deleteEmptyListener2;
    }

    public final void setErrorMessageListener(ErrorMessageListener errorMessageListener2) {
        this.errorMessageListener = errorMessageListener2;
    }

    public final void setErrorMessage(String str) {
        this.errorMessage = str;
    }

    public final void setErrorColor(int i) {
        this.errorColor = Integer.valueOf(i);
    }

    public final void setHintDelayed(int i, long j) {
        CharSequence text = getResources().getText(i);
        Intrinsics.checkExpressionValueIsNotNull(text, "resources.getText(hintResource)");
        setHintDelayed(text, j);
    }

    public final void setHintDelayed(CharSequence charSequence, long j) {
        Intrinsics.checkParameterIsNotNull(charSequence, "hint");
        this.hintHandler.postDelayed(new StripeEditText$setHintDelayed$1(this, charSequence), j);
    }

    /* access modifiers changed from: private */
    public final void setHintSafely(CharSequence charSequence) {
        try {
            setHint(charSequence);
        } catch (NullPointerException unused) {
        }
    }

    public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo accessibilityNodeInfo) {
        Intrinsics.checkParameterIsNotNull(accessibilityNodeInfo, "info");
        super.onInitializeAccessibilityNodeInfo(accessibilityNodeInfo);
        accessibilityNodeInfo.setContentInvalid(this.shouldShowError);
        String accessibilityText2 = getAccessibilityText();
        if (accessibilityText2 != null) {
            accessibilityNodeInfo.setText(accessibilityText2);
        }
        if (Build.VERSION.SDK_INT >= 21) {
            String str = this.errorMessage;
            if (!this.shouldShowError) {
                str = null;
            }
            accessibilityNodeInfo.setError(str);
        }
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.hintHandler.removeCallbacksAndMessages((Object) null);
    }

    private final void determineDefaultErrorColor() {
        int i;
        this.cachedColorStateList = getTextColors();
        Context context = getContext();
        StripeColorUtils.Companion companion = StripeColorUtils.Companion;
        ColorStateList textColors = getTextColors();
        Intrinsics.checkExpressionValueIsNotNull(textColors, "textColors");
        if (companion.isColorDark(textColors.getDefaultColor())) {
            i = com.stripe.android.R.color.stripe_error_text_light_theme;
        } else {
            i = com.stripe.android.R.color.stripe_error_text_dark_theme;
        }
        this.defaultErrorColor = ContextCompat.getColor(context, i);
    }

    private final void listenForTextChanges() {
        addTextChangedListener(new StripeEditText$listenForTextChanges$1(this));
    }

    private final void listenForDeleteEmpty() {
        setOnKeyListener(new StripeEditText$listenForDeleteEmpty$1(this));
    }

    /* access modifiers changed from: private */
    public final boolean isDeleteKey(int i, KeyEvent keyEvent) {
        return i == 67 && keyEvent.getAction() == 0;
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0002\b\u0002\u0018\u00002\u00020\u0001B\u001f\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\b\u0010\u0006\u001a\u0004\u0018\u00010\u0007¢\u0006\u0002\u0010\bJ\u0018\u0010\t\u001a\u00020\u00052\u0006\u0010\n\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\u000bH\u0016R\u0010\u0010\u0006\u001a\u0004\u0018\u00010\u0007X\u0004¢\u0006\u0002\n\u0000¨\u0006\r"}, d2 = {"Lcom/stripe/android/view/StripeEditText$SoftDeleteInputConnection;", "Landroid/view/inputmethod/InputConnectionWrapper;", "target", "Landroid/view/inputmethod/InputConnection;", "mutable", "", "deleteEmptyListener", "Lcom/stripe/android/view/StripeEditText$DeleteEmptyListener;", "(Landroid/view/inputmethod/InputConnection;ZLcom/stripe/android/view/StripeEditText$DeleteEmptyListener;)V", "deleteSurroundingText", "beforeLength", "", "afterLength", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: StripeEditText.kt */
    private static final class SoftDeleteInputConnection extends InputConnectionWrapper {
        private final DeleteEmptyListener deleteEmptyListener;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public SoftDeleteInputConnection(InputConnection inputConnection, boolean z, DeleteEmptyListener deleteEmptyListener2) {
            super(inputConnection, z);
            Intrinsics.checkParameterIsNotNull(inputConnection, "target");
            this.deleteEmptyListener = deleteEmptyListener2;
        }

        public boolean deleteSurroundingText(int i, int i2) {
            DeleteEmptyListener deleteEmptyListener2;
            boolean z = false;
            CharSequence textBeforeCursor = getTextBeforeCursor(1, 0);
            Intrinsics.checkExpressionValueIsNotNull(textBeforeCursor, "getTextBeforeCursor(1, 0)");
            if (textBeforeCursor.length() == 0) {
                z = true;
            }
            if (z && (deleteEmptyListener2 = this.deleteEmptyListener) != null) {
                deleteEmptyListener2.onDeleteEmpty();
            }
            return super.deleteSurroundingText(i, i2);
        }
    }
}
