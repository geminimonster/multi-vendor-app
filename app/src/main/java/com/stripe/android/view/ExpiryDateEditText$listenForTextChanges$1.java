package com.stripe.android.view;

import android.text.Editable;
import com.stripe.android.view.ExpiryDateEditText;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.jvm.internal.Intrinsics;
import kotlin.ranges.RangesKt;
import kotlin.text.Regex;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000;\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\b\n\u0002\b\u0007\n\u0002\u0010\u0011\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\r\n\u0002\b\u0006*\u0001\u0000\b\n\u0018\u00002\u00020\u0001J\u0012\u0010\u0013\u001a\u00020\u00142\b\u0010\u0015\u001a\u0004\u0018\u00010\u0016H\u0016J*\u0010\u0017\u001a\u00020\u00142\b\u0010\u0015\u001a\u0004\u0018\u00010\u00182\u0006\u0010\u0019\u001a\u00020\u00072\u0006\u0010\u001a\u001a\u00020\u00072\u0006\u0010\u001b\u001a\u00020\u0007H\u0016J*\u0010\u001c\u001a\u00020\u00142\b\u0010\u0015\u001a\u0004\u0018\u00010\u00182\u0006\u0010\u0019\u001a\u00020\u00072\u0006\u0010\u001d\u001a\u00020\u00072\u0006\u0010\u001a\u001a\u00020\u0007H\u0016R\u0010\u0010\u0002\u001a\u0004\u0018\u00010\u0003X\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0007X\u000e¢\u0006\u0002\n\u0000R\u0011\u0010\t\u001a\u00020\u00038F¢\u0006\u0006\u001a\u0004\b\n\u0010\u000bR\u0012\u0010\f\u001a\u0004\u0018\u00010\u0007X\u000e¢\u0006\u0004\n\u0002\u0010\rR\u0016\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\u00030\u000fX\u000e¢\u0006\u0004\n\u0002\u0010\u0010R\u0011\u0010\u0011\u001a\u00020\u00038F¢\u0006\u0006\u001a\u0004\b\u0012\u0010\u000b¨\u0006\u001e"}, d2 = {"com/stripe/android/view/ExpiryDateEditText$listenForTextChanges$1", "Lcom/stripe/android/view/StripeTextWatcher;", "formattedDate", "", "ignoreChanges", "", "latestChangeStart", "", "latestInsertionSize", "month", "getMonth", "()Ljava/lang/String;", "newCursorPosition", "Ljava/lang/Integer;", "parts", "", "[Ljava/lang/String;", "year", "getYear", "afterTextChanged", "", "s", "Landroid/text/Editable;", "beforeTextChanged", "", "start", "count", "after", "onTextChanged", "before", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: ExpiryDateEditText.kt */
public final class ExpiryDateEditText$listenForTextChanges$1 extends StripeTextWatcher {
    private String formattedDate;
    private boolean ignoreChanges;
    private int latestChangeStart;
    private int latestInsertionSize;
    private Integer newCursorPosition;
    private String[] parts = {"", ""};
    final /* synthetic */ ExpiryDateEditText this$0;

    ExpiryDateEditText$listenForTextChanges$1(ExpiryDateEditText expiryDateEditText) {
        this.this$0 = expiryDateEditText;
    }

    public final String getMonth() {
        return this.parts[0];
    }

    public final String getYear() {
        return this.parts[1];
    }

    public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        if (!this.ignoreChanges) {
            this.latestChangeStart = i;
            this.latestInsertionSize = i3;
        }
    }

    public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        if (!this.ignoreChanges) {
            String obj = charSequence != null ? charSequence.toString() : null;
            if (obj == null) {
                obj = "";
            }
            String replace = new Regex("/").replace((CharSequence) obj, "");
            if (replace.length() == 1 && this.latestChangeStart == 0 && this.latestInsertionSize == 1) {
                char charAt = replace.charAt(0);
                if (!(charAt == '0' || charAt == '1')) {
                    replace = '0' + replace;
                    this.latestInsertionSize++;
                }
            } else if (replace.length() == 2 && this.latestChangeStart == 2 && this.latestInsertionSize == 0) {
                if (replace != null) {
                    replace = replace.substring(0, 1);
                    Intrinsics.checkExpressionValueIsNotNull(replace, "(this as java.lang.Strin…ing(startIndex, endIndex)");
                } else {
                    throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
                }
            }
            this.parts = DateUtils.separateDateStringParts(replace);
            boolean z = !DateUtils.isValidMonth(getMonth());
            StringBuilder sb = new StringBuilder();
            sb.append(getMonth());
            Intrinsics.checkExpressionValueIsNotNull(sb, "StringBuilder()\n                    .append(month)");
            if ((getMonth().length() == 2 && this.latestInsertionSize > 0 && !z) || replace.length() > 2) {
                sb.append("/");
            }
            sb.append(getYear());
            String sb2 = sb.toString();
            Intrinsics.checkExpressionValueIsNotNull(sb2, "formattedDateBuilder.toString()");
            ExpiryDateEditText expiryDateEditText = this.this$0;
            int length = sb2.length();
            int i4 = this.latestChangeStart;
            int i5 = this.latestInsertionSize;
            ExpiryDateEditText.Companion unused = ExpiryDateEditText.Companion;
            this.newCursorPosition = Integer.valueOf(expiryDateEditText.updateSelectionIndex$stripe_release(length, i4, i5, 5));
            this.formattedDate = sb2;
        }
    }

    public void afterTextChanged(Editable editable) {
        String str;
        if (!this.ignoreChanges) {
            this.ignoreChanges = true;
            if (!this.this$0.isLastKeyDelete() && (str = this.formattedDate) != null) {
                this.this$0.setText(str);
                Integer num = this.newCursorPosition;
                if (num != null) {
                    int intValue = num.intValue();
                    ExpiryDateEditText expiryDateEditText = this.this$0;
                    expiryDateEditText.setSelection(RangesKt.coerceIn(intValue, 0, expiryDateEditText.getFieldText$stripe_release().length()));
                }
            }
            this.ignoreChanges = false;
            boolean z = getMonth().length() == 2 && !DateUtils.isValidMonth(getMonth());
            if (getMonth().length() == 2 && getYear().length() == 2) {
                boolean isDateValid = this.this$0.isDateValid();
                this.this$0.updateInputValues(getMonth(), getYear());
                boolean isDateValid2 = true ^ this.this$0.isDateValid();
                if (!isDateValid && this.this$0.isDateValid()) {
                    this.this$0.getCompletionCallback$stripe_release().invoke();
                }
                z = isDateValid2;
            } else {
                this.this$0.isDateValid = false;
            }
            this.this$0.setShouldShowError(z);
            this.formattedDate = null;
            this.newCursorPosition = null;
        }
    }
}
