package com.stripe.android.view;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import java.lang.ref.WeakReference;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\b`\u0018\u0000*\u0004\b\u0000\u0010\u00012\u00020\u0002:\u0001\u0007J\u0015\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00028\u0000H&¢\u0006\u0002\u0010\u0006¨\u0006\b"}, d2 = {"Lcom/stripe/android/view/AuthActivityStarter;", "ArgsType", "", "start", "", "args", "(Ljava/lang/Object;)V", "Host", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: AuthActivityStarter.kt */
public interface AuthActivityStarter<ArgsType> {
    void start(ArgsType argstype);

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\u0018\u0000 \u00172\u00020\u0001:\u0001\u0017B\u0019\b\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\b\u0010\u0004\u001a\u0004\u0018\u00010\u0005¢\u0006\u0002\u0010\u0006J)\u0010\u000e\u001a\u00020\u000f2\n\u0010\u0010\u001a\u0006\u0012\u0002\b\u00030\u00112\u0006\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u0015H\u0000¢\u0006\u0002\b\u0016R\u0016\u0010\u0002\u001a\u0004\u0018\u00010\u00038@X\u0004¢\u0006\u0006\u001a\u0004\b\u0007\u0010\bR\u0014\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00030\nX\u0004¢\u0006\u0002\n\u0000R\u0016\u0010\u0004\u001a\u0004\u0018\u00010\u00058@X\u0004¢\u0006\u0006\u001a\u0004\b\u000b\u0010\fR\u0016\u0010\r\u001a\n\u0012\u0004\u0012\u00020\u0005\u0018\u00010\nX\u0004¢\u0006\u0002\n\u0000¨\u0006\u0018"}, d2 = {"Lcom/stripe/android/view/AuthActivityStarter$Host;", "", "activity", "Landroid/app/Activity;", "fragment", "Landroidx/fragment/app/Fragment;", "(Landroid/app/Activity;Landroidx/fragment/app/Fragment;)V", "getActivity$stripe_release", "()Landroid/app/Activity;", "activityRef", "Ljava/lang/ref/WeakReference;", "getFragment$stripe_release", "()Landroidx/fragment/app/Fragment;", "fragmentRef", "startActivityForResult", "", "target", "Ljava/lang/Class;", "extras", "Landroid/os/Bundle;", "requestCode", "", "startActivityForResult$stripe_release", "Companion", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: AuthActivityStarter.kt */
    public static final class Host {
        public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
        private final WeakReference<Activity> activityRef;
        private final WeakReference<Fragment> fragmentRef;

        private Host(Activity activity, Fragment fragment) {
            this.activityRef = new WeakReference<>(activity);
            this.fragmentRef = fragment != null ? new WeakReference<>(fragment) : null;
        }

        public /* synthetic */ Host(Activity activity, Fragment fragment, DefaultConstructorMarker defaultConstructorMarker) {
            this(activity, fragment);
        }

        public final Activity getActivity$stripe_release() {
            return (Activity) this.activityRef.get();
        }

        public final Fragment getFragment$stripe_release() {
            WeakReference<Fragment> weakReference = this.fragmentRef;
            if (weakReference != null) {
                return (Fragment) weakReference.get();
            }
            return null;
        }

        public final void startActivityForResult$stripe_release(Class<?> cls, Bundle bundle, int i) {
            Intrinsics.checkParameterIsNotNull(cls, "target");
            Intrinsics.checkParameterIsNotNull(bundle, "extras");
            Activity activity = (Activity) this.activityRef.get();
            if (activity != null) {
                Intrinsics.checkExpressionValueIsNotNull(activity, "activityRef.get() ?: return");
                Intent putExtras = new Intent(activity, cls).putExtras(bundle);
                Intrinsics.checkExpressionValueIsNotNull(putExtras, "Intent(activity, target).putExtras(extras)");
                WeakReference<Fragment> weakReference = this.fragmentRef;
                if (weakReference != null) {
                    Fragment fragment = (Fragment) weakReference.get();
                    if (fragment != null && fragment.isAdded()) {
                        fragment.startActivityForResult(putExtras, i);
                        return;
                    }
                    return;
                }
                activity.startActivityForResult(putExtras, i);
            }
        }

        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0015\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0000¢\u0006\u0002\b\u0007J\u0015\u0010\u0003\u001a\u00020\u00042\u0006\u0010\b\u001a\u00020\tH\u0000¢\u0006\u0002\b\u0007¨\u0006\n"}, d2 = {"Lcom/stripe/android/view/AuthActivityStarter$Host$Companion;", "", "()V", "create", "Lcom/stripe/android/view/AuthActivityStarter$Host;", "activity", "Landroid/app/Activity;", "create$stripe_release", "fragment", "Landroidx/fragment/app/Fragment;", "stripe_release"}, k = 1, mv = {1, 1, 16})
        /* compiled from: AuthActivityStarter.kt */
        public static final class Companion {
            private Companion() {
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }

            public final /* synthetic */ Host create$stripe_release(Fragment fragment) {
                Intrinsics.checkParameterIsNotNull(fragment, "fragment");
                FragmentActivity requireActivity = fragment.requireActivity();
                Intrinsics.checkExpressionValueIsNotNull(requireActivity, "fragment.requireActivity()");
                return new Host(requireActivity, fragment, (DefaultConstructorMarker) null);
            }

            public final /* synthetic */ Host create$stripe_release(Activity activity) {
                Intrinsics.checkParameterIsNotNull(activity, "activity");
                return new Host(activity, (Fragment) null, (DefaultConstructorMarker) null);
            }
        }
    }
}
