package com.stripe.android.view;

import android.app.Activity;
import android.content.Context;
import android.content.res.ColorStateList;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.view.ViewGroup;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.core.view.ViewCompat;
import androidx.core.widget.ImageViewCompat;
import androidx.recyclerview.widget.RecyclerView;
import com.stripe.android.R;
import com.stripe.android.databinding.GooglePayRowBinding;
import com.stripe.android.databinding.MaskedCardRowBinding;
import com.stripe.android.model.PaymentMethod;
import com.stripe.android.view.AddPaymentMethodRowView;
import com.stripe.android.view.PaymentMethodsActivityStarter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.TypeCastException;
import kotlin.collections.CollectionsKt;
import kotlin.jvm.internal.Intrinsics;
import kotlin.ranges.IntRange;
import kotlin.ranges.RangesKt;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000z\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0002\b\u000b\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0006\n\u0002\u0010\t\n\u0002\b\u0018\b\u0000\u0018\u0000 L2\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0004LMNOBG\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u000e\b\u0002\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006\u0012\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\t\u0012\b\b\u0002\u0010\n\u001a\u00020\u000b\u0012\b\b\u0002\u0010\f\u001a\u00020\u000b\u0012\b\b\u0002\u0010\r\u001a\u00020\u000b¢\u0006\u0002\u0010\u000eJ\u0010\u0010&\u001a\u00020'2\u0006\u0010(\u001a\u00020)H\u0002J\u0010\u0010*\u001a\u00020+2\u0006\u0010(\u001a\u00020)H\u0002J\u0010\u0010,\u001a\u00020-2\u0006\u0010(\u001a\u00020)H\u0002J\u0010\u0010.\u001a\u00020/2\u0006\u0010(\u001a\u00020)H\u0002J\u0015\u00100\u001a\u0002012\u0006\u00102\u001a\u00020\u001bH\u0000¢\u0006\u0002\b3J\u0010\u00104\u001a\u00020\u00102\u0006\u00105\u001a\u00020\u0010H\u0002J\b\u00106\u001a\u00020\u0010H\u0016J\u0010\u00107\u001a\u0002082\u0006\u00105\u001a\u00020\u0010H\u0016J\u0010\u00109\u001a\u00020\u00102\u0006\u00105\u001a\u00020\u0010H\u0016J\u0015\u0010:\u001a\u00020\u001b2\u0006\u00105\u001a\u00020\u0010H\u0000¢\u0006\u0002\b;J\u0010\u0010<\u001a\u00020\u00102\u0006\u00105\u001a\u00020\u0010H\u0002J\u0019\u0010=\u001a\u0004\u0018\u00010\u00102\u0006\u00102\u001a\u00020\u001bH\u0000¢\u0006\u0004\b>\u0010?J\u0010\u0010@\u001a\u00020\u000b2\u0006\u00105\u001a\u00020\u0010H\u0002J\u0010\u0010A\u001a\u00020\u000b2\u0006\u00105\u001a\u00020\u0010H\u0002J\u0018\u0010B\u001a\u0002012\u0006\u0010C\u001a\u00020\u00022\u0006\u00105\u001a\u00020\u0010H\u0016J\u0018\u0010D\u001a\u00020\u00022\u0006\u0010(\u001a\u00020)2\u0006\u0010E\u001a\u00020\u0010H\u0016J\u0010\u0010F\u001a\u0002012\u0006\u00105\u001a\u00020\u0010H\u0002J\u0015\u0010G\u001a\u0002012\u0006\u00102\u001a\u00020\u001bH\u0000¢\u0006\u0002\bHJ\u001b\u0010I\u001a\u0002012\f\u0010\u0019\u001a\b\u0012\u0004\u0012\u00020\u001b0\u0006H\u0000¢\u0006\u0002\bJJ\u0010\u0010K\u001a\u0002012\u0006\u00105\u001a\u00020\u0010H\u0002R\u0014\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000bX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0012X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0004¢\u0006\u0002\n\u0000R\u001c\u0010\u0013\u001a\u0004\u0018\u00010\u0014X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0015\u0010\u0016\"\u0004\b\u0017\u0010\u0018R\u001a\u0010\u0019\u001a\b\u0012\u0004\u0012\u00020\u001b0\u001aX\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u001c\u0010\u001dR\u0016\u0010\u001e\u001a\u0004\u0018\u00010\u001b8@X\u0004¢\u0006\u0006\u001a\u0004\b\u001f\u0010 R\u001c\u0010!\u001a\u0004\u0018\u00010\tX\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\"\u0010#\"\u0004\b$\u0010%R\u000e\u0010\n\u001a\u00020\u000bX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\u000bX\u0004¢\u0006\u0002\n\u0000¨\u0006P"}, d2 = {"Lcom/stripe/android/view/PaymentMethodsAdapter;", "Landroidx/recyclerview/widget/RecyclerView$Adapter;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "intentArgs", "Lcom/stripe/android/view/PaymentMethodsActivityStarter$Args;", "addableTypes", "", "Lcom/stripe/android/model/PaymentMethod$Type;", "initiallySelectedPaymentMethodId", "", "shouldShowGooglePay", "", "useGooglePay", "canDeletePaymentMethods", "(Lcom/stripe/android/view/PaymentMethodsActivityStarter$Args;Ljava/util/List;Ljava/lang/String;ZZZ)V", "googlePayCount", "", "handler", "Landroid/os/Handler;", "listener", "Lcom/stripe/android/view/PaymentMethodsAdapter$Listener;", "getListener$stripe_release", "()Lcom/stripe/android/view/PaymentMethodsAdapter$Listener;", "setListener$stripe_release", "(Lcom/stripe/android/view/PaymentMethodsAdapter$Listener;)V", "paymentMethods", "", "Lcom/stripe/android/model/PaymentMethod;", "getPaymentMethods$stripe_release", "()Ljava/util/List;", "selectedPaymentMethod", "getSelectedPaymentMethod$stripe_release", "()Lcom/stripe/android/model/PaymentMethod;", "selectedPaymentMethodId", "getSelectedPaymentMethodId$stripe_release", "()Ljava/lang/String;", "setSelectedPaymentMethodId$stripe_release", "(Ljava/lang/String;)V", "createAddCardPaymentMethodViewHolder", "Lcom/stripe/android/view/PaymentMethodsAdapter$ViewHolder$AddCardPaymentMethodViewHolder;", "parent", "Landroid/view/ViewGroup;", "createAddFpxPaymentMethodViewHolder", "Lcom/stripe/android/view/PaymentMethodsAdapter$ViewHolder$AddFpxPaymentMethodViewHolder;", "createGooglePayViewHolder", "Lcom/stripe/android/view/PaymentMethodsAdapter$ViewHolder$GooglePayViewHolder;", "createPaymentMethodViewHolder", "Lcom/stripe/android/view/PaymentMethodsAdapter$ViewHolder$PaymentMethodViewHolder;", "deletePaymentMethod", "", "paymentMethod", "deletePaymentMethod$stripe_release", "getAddableTypesPosition", "position", "getItemCount", "getItemId", "", "getItemViewType", "getPaymentMethodAtPosition", "getPaymentMethodAtPosition$stripe_release", "getPaymentMethodIndex", "getPosition", "getPosition$stripe_release", "(Lcom/stripe/android/model/PaymentMethod;)Ljava/lang/Integer;", "isGooglePayPosition", "isPaymentMethodsPosition", "onBindViewHolder", "holder", "onCreateViewHolder", "viewType", "onPositionClicked", "resetPaymentMethod", "resetPaymentMethod$stripe_release", "setPaymentMethods", "setPaymentMethods$stripe_release", "updateSelectedPaymentMethod", "Companion", "Listener", "ViewHolder", "ViewType", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: PaymentMethodsAdapter.kt */
public final class PaymentMethodsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
    /* access modifiers changed from: private */
    public static final long GOOGLE_PAY_ITEM_ID = ((long) -2057760476);
    private final List<PaymentMethod.Type> addableTypes;
    private final boolean canDeletePaymentMethods;
    private final int googlePayCount;
    private final Handler handler;
    private final PaymentMethodsActivityStarter.Args intentArgs;
    private Listener listener;
    private final List<PaymentMethod> paymentMethods;
    private String selectedPaymentMethodId;
    private final boolean shouldShowGooglePay;
    private final boolean useGooglePay;

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\b`\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&J\b\u0010\u0006\u001a\u00020\u0003H&J\u0010\u0010\u0007\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&¨\u0006\b"}, d2 = {"Lcom/stripe/android/view/PaymentMethodsAdapter$Listener;", "", "onDeletePaymentMethodAction", "", "paymentMethod", "Lcom/stripe/android/model/PaymentMethod;", "onGooglePayClick", "onPaymentMethodClick", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: PaymentMethodsAdapter.kt */
    public interface Listener {
        void onDeletePaymentMethodAction(PaymentMethod paymentMethod);

        void onGooglePayClick();

        void onPaymentMethodClick(PaymentMethod paymentMethod);
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0006\b\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002j\u0002\b\u0003j\u0002\b\u0004j\u0002\b\u0005j\u0002\b\u0006¨\u0006\u0007"}, d2 = {"Lcom/stripe/android/view/PaymentMethodsAdapter$ViewType;", "", "(Ljava/lang/String;I)V", "Card", "AddCard", "AddFpx", "GooglePay", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: PaymentMethodsAdapter.kt */
    public enum ViewType {
        Card,
        AddCard,
        AddFpx,
        GooglePay
    }

    @Metadata(bv = {1, 0, 3}, k = 3, mv = {1, 1, 16})
    public final /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;
        public static final /* synthetic */ int[] $EnumSwitchMapping$1;

        static {
            int[] iArr = new int[PaymentMethod.Type.values().length];
            $EnumSwitchMapping$0 = iArr;
            iArr[PaymentMethod.Type.Card.ordinal()] = 1;
            $EnumSwitchMapping$0[PaymentMethod.Type.Fpx.ordinal()] = 2;
            int[] iArr2 = new int[ViewType.values().length];
            $EnumSwitchMapping$1 = iArr2;
            iArr2[ViewType.Card.ordinal()] = 1;
            $EnumSwitchMapping$1[ViewType.AddCard.ordinal()] = 2;
            $EnumSwitchMapping$1[ViewType.AddFpx.ordinal()] = 3;
            $EnumSwitchMapping$1[ViewType.GooglePay.ordinal()] = 4;
        }
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ PaymentMethodsAdapter(PaymentMethodsActivityStarter.Args args, List list, String str, boolean z, boolean z2, boolean z3, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(args, (i & 2) != 0 ? CollectionsKt.listOf(PaymentMethod.Type.Card) : list, (i & 4) != 0 ? null : str, (i & 8) != 0 ? false : z, (i & 16) != 0 ? false : z2, (i & 32) != 0 ? true : z3);
    }

    public PaymentMethodsAdapter(PaymentMethodsActivityStarter.Args args, List<? extends PaymentMethod.Type> list, String str, boolean z, boolean z2, boolean z3) {
        Intrinsics.checkParameterIsNotNull(args, "intentArgs");
        Intrinsics.checkParameterIsNotNull(list, "addableTypes");
        this.intentArgs = args;
        this.addableTypes = list;
        this.shouldShowGooglePay = z;
        this.useGooglePay = z2;
        this.canDeletePaymentMethods = z3;
        this.paymentMethods = new ArrayList();
        this.selectedPaymentMethodId = str;
        this.handler = new Handler(Looper.getMainLooper());
        Integer num = 1;
        num.intValue();
        num = !this.shouldShowGooglePay ? null : num;
        this.googlePayCount = num != null ? num.intValue() : 0;
        setHasStableIds(true);
    }

    public final List<PaymentMethod> getPaymentMethods$stripe_release() {
        return this.paymentMethods;
    }

    public final String getSelectedPaymentMethodId$stripe_release() {
        return this.selectedPaymentMethodId;
    }

    public final void setSelectedPaymentMethodId$stripe_release(String str) {
        this.selectedPaymentMethodId = str;
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v0, resolved type: com.stripe.android.model.PaymentMethod} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v1, resolved type: com.stripe.android.model.PaymentMethod} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v1, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v0, resolved type: com.stripe.android.model.PaymentMethod} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v3, resolved type: com.stripe.android.model.PaymentMethod} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.stripe.android.model.PaymentMethod getSelectedPaymentMethod$stripe_release() {
        /*
            r5 = this;
            java.lang.String r0 = r5.selectedPaymentMethodId
            r1 = 0
            if (r0 == 0) goto L_0x0025
            java.util.List<com.stripe.android.model.PaymentMethod> r2 = r5.paymentMethods
            java.lang.Iterable r2 = (java.lang.Iterable) r2
            java.util.Iterator r2 = r2.iterator()
        L_0x000d:
            boolean r3 = r2.hasNext()
            if (r3 == 0) goto L_0x0023
            java.lang.Object r3 = r2.next()
            r4 = r3
            com.stripe.android.model.PaymentMethod r4 = (com.stripe.android.model.PaymentMethod) r4
            java.lang.String r4 = r4.id
            boolean r4 = kotlin.jvm.internal.Intrinsics.areEqual((java.lang.Object) r4, (java.lang.Object) r0)
            if (r4 == 0) goto L_0x000d
            r1 = r3
        L_0x0023:
            com.stripe.android.model.PaymentMethod r1 = (com.stripe.android.model.PaymentMethod) r1
        L_0x0025:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.stripe.android.view.PaymentMethodsAdapter.getSelectedPaymentMethod$stripe_release():com.stripe.android.model.PaymentMethod");
    }

    public final Listener getListener$stripe_release() {
        return this.listener;
    }

    public final void setListener$stripe_release(Listener listener2) {
        this.listener = listener2;
    }

    public final /* synthetic */ void setPaymentMethods$stripe_release(List<PaymentMethod> list) {
        Intrinsics.checkParameterIsNotNull(list, "paymentMethods");
        this.paymentMethods.clear();
        this.paymentMethods.addAll(list);
        notifyDataSetChanged();
    }

    public int getItemCount() {
        return this.paymentMethods.size() + this.addableTypes.size() + this.googlePayCount;
    }

    public int getItemViewType(int i) {
        if (isGooglePayPosition(i)) {
            return ViewType.GooglePay.ordinal();
        }
        if (isPaymentMethodsPosition(i)) {
            if (PaymentMethod.Type.Card == getPaymentMethodAtPosition$stripe_release(i).type) {
                return ViewType.Card.ordinal();
            }
            return super.getItemViewType(i);
        }
        PaymentMethod.Type type = this.addableTypes.get(getAddableTypesPosition(i));
        int i2 = WhenMappings.$EnumSwitchMapping$0[type.ordinal()];
        if (i2 == 1) {
            return ViewType.AddCard.ordinal();
        }
        if (i2 == 2) {
            return ViewType.AddFpx.ordinal();
        }
        throw new IllegalArgumentException("Unsupported PaymentMethod type: " + type.code);
    }

    private final boolean isGooglePayPosition(int i) {
        return this.shouldShowGooglePay && i == 0;
    }

    private final boolean isPaymentMethodsPosition(int i) {
        IntRange intRange;
        if (this.shouldShowGooglePay) {
            intRange = new IntRange(1, this.paymentMethods.size());
        } else {
            intRange = RangesKt.until(0, this.paymentMethods.size());
        }
        return intRange.contains(i);
    }

    public long getItemId(int i) {
        int i2;
        if (isGooglePayPosition(i)) {
            return GOOGLE_PAY_ITEM_ID;
        }
        if (isPaymentMethodsPosition(i)) {
            i2 = getPaymentMethodAtPosition$stripe_release(i).hashCode();
        } else {
            i2 = this.addableTypes.get(getAddableTypesPosition(i)).code.hashCode();
        }
        return (long) i2;
    }

    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
        Intrinsics.checkParameterIsNotNull(viewHolder, "holder");
        if (viewHolder instanceof ViewHolder.PaymentMethodViewHolder) {
            PaymentMethod paymentMethodAtPosition$stripe_release = getPaymentMethodAtPosition$stripe_release(i);
            ViewHolder.PaymentMethodViewHolder paymentMethodViewHolder = (ViewHolder.PaymentMethodViewHolder) viewHolder;
            paymentMethodViewHolder.setPaymentMethod(paymentMethodAtPosition$stripe_release);
            paymentMethodViewHolder.setSelected(Intrinsics.areEqual((Object) paymentMethodAtPosition$stripe_release.id, (Object) this.selectedPaymentMethodId));
            viewHolder.itemView.setOnClickListener(new PaymentMethodsAdapter$onBindViewHolder$1(this, viewHolder));
        } else if (viewHolder instanceof ViewHolder.GooglePayViewHolder) {
            viewHolder.itemView.setOnClickListener(new PaymentMethodsAdapter$onBindViewHolder$2(this));
            ((ViewHolder.GooglePayViewHolder) viewHolder).bind(this.useGooglePay);
        }
    }

    /* access modifiers changed from: private */
    public final void onPositionClicked(int i) {
        updateSelectedPaymentMethod(i);
        this.handler.post(new PaymentMethodsAdapter$onPositionClicked$1(this, i));
    }

    private final void updateSelectedPaymentMethod(int i) {
        Iterator<PaymentMethod> it = this.paymentMethods.iterator();
        int i2 = 0;
        while (true) {
            if (!it.hasNext()) {
                i2 = -1;
                break;
            } else if (Intrinsics.areEqual((Object) it.next().id, (Object) this.selectedPaymentMethodId)) {
                break;
            } else {
                i2++;
            }
        }
        if (i2 != i) {
            notifyItemChanged(i2);
            PaymentMethod paymentMethod = (PaymentMethod) CollectionsKt.getOrNull(this.paymentMethods, i);
            this.selectedPaymentMethodId = paymentMethod != null ? paymentMethod.id : null;
        }
        notifyItemChanged(i);
    }

    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        Intrinsics.checkParameterIsNotNull(viewGroup, "parent");
        int i2 = WhenMappings.$EnumSwitchMapping$1[ViewType.values()[i].ordinal()];
        if (i2 == 1) {
            return createPaymentMethodViewHolder(viewGroup);
        }
        if (i2 == 2) {
            return createAddCardPaymentMethodViewHolder(viewGroup);
        }
        if (i2 == 3) {
            return createAddFpxPaymentMethodViewHolder(viewGroup);
        }
        if (i2 == 4) {
            return createGooglePayViewHolder(viewGroup);
        }
        throw new NoWhenBranchMatchedException();
    }

    private final ViewHolder.AddCardPaymentMethodViewHolder createAddCardPaymentMethodViewHolder(ViewGroup viewGroup) {
        AddPaymentMethodRowView.Companion companion = AddPaymentMethodRowView.Companion;
        Context context = viewGroup.getContext();
        if (context != null) {
            return new ViewHolder.AddCardPaymentMethodViewHolder(companion.createCard$stripe_release((Activity) context, this.intentArgs));
        }
        throw new TypeCastException("null cannot be cast to non-null type android.app.Activity");
    }

    private final ViewHolder.AddFpxPaymentMethodViewHolder createAddFpxPaymentMethodViewHolder(ViewGroup viewGroup) {
        AddPaymentMethodRowView.Companion companion = AddPaymentMethodRowView.Companion;
        Context context = viewGroup.getContext();
        if (context != null) {
            return new ViewHolder.AddFpxPaymentMethodViewHolder(companion.createFpx$stripe_release((Activity) context, this.intentArgs));
        }
        throw new TypeCastException("null cannot be cast to non-null type android.app.Activity");
    }

    private final ViewHolder.PaymentMethodViewHolder createPaymentMethodViewHolder(ViewGroup viewGroup) {
        ViewHolder.PaymentMethodViewHolder paymentMethodViewHolder = new ViewHolder.PaymentMethodViewHolder(viewGroup);
        if (this.canDeletePaymentMethods) {
            ViewCompat.addAccessibilityAction(paymentMethodViewHolder.itemView, viewGroup.getContext().getString(R.string.delete_payment_method), new PaymentMethodsAdapter$createPaymentMethodViewHolder$1(this, paymentMethodViewHolder));
        }
        return paymentMethodViewHolder;
    }

    private final ViewHolder.GooglePayViewHolder createGooglePayViewHolder(ViewGroup viewGroup) {
        Context context = viewGroup.getContext();
        Intrinsics.checkExpressionValueIsNotNull(context, "parent.context");
        return new ViewHolder.GooglePayViewHolder(context, viewGroup);
    }

    public final /* synthetic */ void deletePaymentMethod$stripe_release(PaymentMethod paymentMethod) {
        Intrinsics.checkParameterIsNotNull(paymentMethod, "paymentMethod");
        Integer position$stripe_release = getPosition$stripe_release(paymentMethod);
        if (position$stripe_release != null) {
            int intValue = position$stripe_release.intValue();
            this.paymentMethods.remove(paymentMethod);
            notifyItemRemoved(intValue);
        }
    }

    public final /* synthetic */ void resetPaymentMethod$stripe_release(PaymentMethod paymentMethod) {
        Intrinsics.checkParameterIsNotNull(paymentMethod, "paymentMethod");
        Integer position$stripe_release = getPosition$stripe_release(paymentMethod);
        if (position$stripe_release != null) {
            notifyItemChanged(position$stripe_release.intValue());
        }
    }

    public final /* synthetic */ PaymentMethod getPaymentMethodAtPosition$stripe_release(int i) {
        return this.paymentMethods.get(getPaymentMethodIndex(i));
    }

    private final int getPaymentMethodIndex(int i) {
        return i - this.googlePayCount;
    }

    public final Integer getPosition$stripe_release(PaymentMethod paymentMethod) {
        Intrinsics.checkParameterIsNotNull(paymentMethod, "paymentMethod");
        Integer valueOf = Integer.valueOf(this.paymentMethods.indexOf(paymentMethod));
        if (!(valueOf.intValue() >= 0)) {
            valueOf = null;
        }
        if (valueOf != null) {
            return Integer.valueOf(valueOf.intValue() + this.googlePayCount);
        }
        return null;
    }

    private final int getAddableTypesPosition(int i) {
        return (i - this.paymentMethods.size()) - this.googlePayCount;
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\b0\u0018\u00002\u00020\u0001:\u0004\u0005\u0006\u0007\bB\u000f\b\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004\u0001\u0001\t¨\u0006\n"}, d2 = {"Lcom/stripe/android/view/PaymentMethodsAdapter$ViewHolder;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "itemView", "Landroid/view/View;", "(Landroid/view/View;)V", "AddCardPaymentMethodViewHolder", "AddFpxPaymentMethodViewHolder", "GooglePayViewHolder", "PaymentMethodViewHolder", "Lcom/stripe/android/view/PaymentMethodsAdapter$ViewHolder$PaymentMethodViewHolder;", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: PaymentMethodsAdapter.kt */
    public static abstract class ViewHolder extends RecyclerView.ViewHolder {
        private ViewHolder(View view) {
            super(view);
        }

        public /* synthetic */ ViewHolder(View view, DefaultConstructorMarker defaultConstructorMarker) {
            this(view);
        }

        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004¨\u0006\u0005"}, d2 = {"Lcom/stripe/android/view/PaymentMethodsAdapter$ViewHolder$AddCardPaymentMethodViewHolder;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "itemView", "Landroid/view/View;", "(Landroid/view/View;)V", "stripe_release"}, k = 1, mv = {1, 1, 16})
        /* compiled from: PaymentMethodsAdapter.kt */
        public static final class AddCardPaymentMethodViewHolder extends RecyclerView.ViewHolder {
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public AddCardPaymentMethodViewHolder(View view) {
                super(view);
                Intrinsics.checkParameterIsNotNull(view, "itemView");
            }
        }

        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004¨\u0006\u0005"}, d2 = {"Lcom/stripe/android/view/PaymentMethodsAdapter$ViewHolder$AddFpxPaymentMethodViewHolder;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "itemView", "Landroid/view/View;", "(Landroid/view/View;)V", "stripe_release"}, k = 1, mv = {1, 1, 16})
        /* compiled from: PaymentMethodsAdapter.kt */
        public static final class AddFpxPaymentMethodViewHolder extends RecyclerView.ViewHolder {
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public AddFpxPaymentMethodViewHolder(View view) {
                super(view);
                Intrinsics.checkParameterIsNotNull(view, "itemView");
            }
        }

        @Metadata(bv = {1, 0, 3}, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\b\u0000\u0018\u00002\u00020\u0001B\u0017\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006B\r\u0012\u0006\u0010\u0007\u001a\u00020\b¢\u0006\u0002\u0010\tJ\u000e\u0010\f\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000fR\u000e\u0010\n\u001a\u00020\u000bX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\bX\u0004¢\u0006\u0002\n\u0000¨\u0006\u0010"}, d2 = {"Lcom/stripe/android/view/PaymentMethodsAdapter$ViewHolder$GooglePayViewHolder;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "context", "Landroid/content/Context;", "parent", "Landroid/view/ViewGroup;", "(Landroid/content/Context;Landroid/view/ViewGroup;)V", "viewBinding", "Lcom/stripe/android/databinding/GooglePayRowBinding;", "(Lcom/stripe/android/databinding/GooglePayRowBinding;)V", "themeConfig", "Lcom/stripe/android/view/ThemeConfig;", "bind", "", "isSelected", "", "stripe_release"}, k = 1, mv = {1, 1, 16})
        /* compiled from: PaymentMethodsAdapter.kt */
        public static final class GooglePayViewHolder extends RecyclerView.ViewHolder {
            private final ThemeConfig themeConfig;
            private final GooglePayRowBinding viewBinding;

            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public GooglePayViewHolder(GooglePayRowBinding googlePayRowBinding) {
                super(googlePayRowBinding.getRoot());
                Intrinsics.checkParameterIsNotNull(googlePayRowBinding, "viewBinding");
                this.viewBinding = googlePayRowBinding;
                View view = this.itemView;
                Intrinsics.checkExpressionValueIsNotNull(view, "itemView");
                Context context = view.getContext();
                Intrinsics.checkExpressionValueIsNotNull(context, "itemView.context");
                this.themeConfig = new ThemeConfig(context);
                ImageViewCompat.setImageTintList(this.viewBinding.checkIcon, ColorStateList.valueOf(this.themeConfig.getTintColor$stripe_release(true)));
            }

            /* JADX WARNING: Illegal instructions before constructor call */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public GooglePayViewHolder(android.content.Context r2, android.view.ViewGroup r3) {
                /*
                    r1 = this;
                    java.lang.String r0 = "context"
                    kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(r2, r0)
                    java.lang.String r0 = "parent"
                    kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(r3, r0)
                    android.view.LayoutInflater r2 = android.view.LayoutInflater.from(r2)
                    r0 = 0
                    com.stripe.android.databinding.GooglePayRowBinding r2 = com.stripe.android.databinding.GooglePayRowBinding.inflate(r2, r3, r0)
                    java.lang.String r3 = "GooglePayRowBinding.infl…  false\n                )"
                    kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r2, r3)
                    r1.<init>(r2)
                    return
                */
                throw new UnsupportedOperationException("Method not decompiled: com.stripe.android.view.PaymentMethodsAdapter.ViewHolder.GooglePayViewHolder.<init>(android.content.Context, android.view.ViewGroup):void");
            }

            public final void bind(boolean z) {
                this.viewBinding.label.setTextColor(ColorStateList.valueOf(this.themeConfig.getTextColor$stripe_release(z)));
                AppCompatImageView appCompatImageView = this.viewBinding.checkIcon;
                Intrinsics.checkExpressionValueIsNotNull(appCompatImageView, "viewBinding.checkIcon");
                appCompatImageView.setVisibility(z ? 0 : 4);
                View view = this.itemView;
                Intrinsics.checkExpressionValueIsNotNull(view, "itemView");
                view.setSelected(z);
            }
        }

        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\b\u0000\u0018\u00002\u00020\u0001B\u000f\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004B\r\u0012\u0006\u0010\u0005\u001a\u00020\u0006¢\u0006\u0002\u0010\u0007J\u000e\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000bJ\u000e\u0010\f\u001a\u00020\t2\u0006\u0010\r\u001a\u00020\u000eR\u000e\u0010\u0005\u001a\u00020\u0006X\u0004¢\u0006\u0002\n\u0000¨\u0006\u000f"}, d2 = {"Lcom/stripe/android/view/PaymentMethodsAdapter$ViewHolder$PaymentMethodViewHolder;", "Lcom/stripe/android/view/PaymentMethodsAdapter$ViewHolder;", "parent", "Landroid/view/ViewGroup;", "(Landroid/view/ViewGroup;)V", "viewBinding", "Lcom/stripe/android/databinding/MaskedCardRowBinding;", "(Lcom/stripe/android/databinding/MaskedCardRowBinding;)V", "setPaymentMethod", "", "paymentMethod", "Lcom/stripe/android/model/PaymentMethod;", "setSelected", "selected", "", "stripe_release"}, k = 1, mv = {1, 1, 16})
        /* compiled from: PaymentMethodsAdapter.kt */
        public static final class PaymentMethodViewHolder extends ViewHolder {
            private final MaskedCardRowBinding viewBinding;

            /* JADX WARNING: Illegal instructions before constructor call */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public PaymentMethodViewHolder(com.stripe.android.databinding.MaskedCardRowBinding r3) {
                /*
                    r2 = this;
                    java.lang.String r0 = "viewBinding"
                    kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(r3, r0)
                    android.widget.FrameLayout r0 = r3.getRoot()
                    java.lang.String r1 = "viewBinding.root"
                    kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r0, r1)
                    android.view.View r0 = (android.view.View) r0
                    r1 = 0
                    r2.<init>(r0, r1)
                    r2.viewBinding = r3
                    return
                */
                throw new UnsupportedOperationException("Method not decompiled: com.stripe.android.view.PaymentMethodsAdapter.ViewHolder.PaymentMethodViewHolder.<init>(com.stripe.android.databinding.MaskedCardRowBinding):void");
            }

            /* JADX WARNING: Illegal instructions before constructor call */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public PaymentMethodViewHolder(android.view.ViewGroup r3) {
                /*
                    r2 = this;
                    java.lang.String r0 = "parent"
                    kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(r3, r0)
                    android.content.Context r0 = r3.getContext()
                    android.view.LayoutInflater r0 = android.view.LayoutInflater.from(r0)
                    r1 = 0
                    com.stripe.android.databinding.MaskedCardRowBinding r3 = com.stripe.android.databinding.MaskedCardRowBinding.inflate(r0, r3, r1)
                    java.lang.String r0 = "MaskedCardRowBinding.inf…  false\n                )"
                    kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r3, r0)
                    r2.<init>((com.stripe.android.databinding.MaskedCardRowBinding) r3)
                    return
                */
                throw new UnsupportedOperationException("Method not decompiled: com.stripe.android.view.PaymentMethodsAdapter.ViewHolder.PaymentMethodViewHolder.<init>(android.view.ViewGroup):void");
            }

            public final void setPaymentMethod(PaymentMethod paymentMethod) {
                Intrinsics.checkParameterIsNotNull(paymentMethod, "paymentMethod");
                this.viewBinding.maskedCardItem.setPaymentMethod(paymentMethod);
            }

            public final void setSelected(boolean z) {
                MaskedCardView maskedCardView = this.viewBinding.maskedCardItem;
                Intrinsics.checkExpressionValueIsNotNull(maskedCardView, "viewBinding.maskedCardItem");
                maskedCardView.setSelected(z);
                View view = this.itemView;
                Intrinsics.checkExpressionValueIsNotNull(view, "itemView");
                view.setSelected(z);
            }
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0003\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0014\u0010\u0003\u001a\u00020\u0004X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006¨\u0006\u0007"}, d2 = {"Lcom/stripe/android/view/PaymentMethodsAdapter$Companion;", "", "()V", "GOOGLE_PAY_ITEM_ID", "", "getGOOGLE_PAY_ITEM_ID$stripe_release", "()J", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: PaymentMethodsAdapter.kt */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        public final long getGOOGLE_PAY_ITEM_ID$stripe_release() {
            return PaymentMethodsAdapter.GOOGLE_PAY_ITEM_ID;
        }
    }
}
