package com.stripe.android;

import android.content.Context;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\u0010\u000e\n\u0002\b\u0006\b\u0000\u0018\u00002\u00020\u0001B\u000f\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004B\u000f\b\u0007\u0012\u0006\u0010\u0005\u001a\u00020\u0006¢\u0006\u0002\u0010\u0007J6\u0010\b\u001a\f\u0012\u0004\u0012\u00020\n\u0012\u0002\b\u00030\t2\u0010\u0010\u000b\u001a\f\u0012\u0004\u0012\u00020\n\u0012\u0002\b\u00030\t2\u0006\u0010\f\u001a\u00020\n2\b\u0010\r\u001a\u0004\u0018\u00010\nH\u0002J3\u0010\b\u001a\f\u0012\u0004\u0012\u00020\n\u0012\u0002\b\u00030\t2\u0010\u0010\u000e\u001a\f\u0012\u0004\u0012\u00020\n\u0012\u0002\b\u00030\t2\b\u0010\r\u001a\u0004\u0018\u00010\nH\u0000¢\u0006\u0002\b\u000fR\u000e\u0010\u0005\u001a\u00020\u0006X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0010"}, d2 = {"Lcom/stripe/android/FingerprintParamsUtils;", "", "context", "Landroid/content/Context;", "(Landroid/content/Context;)V", "apiFingerprintParamsFactory", "Lcom/stripe/android/ApiFingerprintParamsFactory;", "(Lcom/stripe/android/ApiFingerprintParamsFactory;)V", "addFingerprintData", "", "", "stripeIntentParams", "key", "fingerprintGuid", "params", "addFingerprintData$stripe_release", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: FingerprintParamsUtils.kt */
public final class FingerprintParamsUtils {
    private final ApiFingerprintParamsFactory apiFingerprintParamsFactory;

    public FingerprintParamsUtils(ApiFingerprintParamsFactory apiFingerprintParamsFactory2) {
        Intrinsics.checkParameterIsNotNull(apiFingerprintParamsFactory2, "apiFingerprintParamsFactory");
        this.apiFingerprintParamsFactory = apiFingerprintParamsFactory2;
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public FingerprintParamsUtils(Context context) {
        this(new ApiFingerprintParamsFactory(context));
        Intrinsics.checkParameterIsNotNull(context, "context");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0030, code lost:
        r5 = addFingerprintData(r4, r1, r5);
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.util.Map<java.lang.String, ?> addFingerprintData$stripe_release(java.util.Map<java.lang.String, ?> r4, java.lang.String r5) {
        /*
            r3 = this;
            java.lang.String r0 = "params"
            kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(r4, r0)
            java.lang.String r0 = "source_data"
            java.lang.String r1 = "payment_method_data"
            java.lang.String[] r0 = new java.lang.String[]{r0, r1}
            java.util.Set r0 = kotlin.collections.SetsKt.setOf(r0)
            java.lang.Iterable r0 = (java.lang.Iterable) r0
            java.util.Iterator r0 = r0.iterator()
        L_0x0017:
            boolean r1 = r0.hasNext()
            if (r1 == 0) goto L_0x002b
            java.lang.Object r1 = r0.next()
            r2 = r1
            java.lang.String r2 = (java.lang.String) r2
            boolean r2 = r4.containsKey(r2)
            if (r2 == 0) goto L_0x0017
            goto L_0x002c
        L_0x002b:
            r1 = 0
        L_0x002c:
            java.lang.String r1 = (java.lang.String) r1
            if (r1 == 0) goto L_0x0037
            java.util.Map r5 = r3.addFingerprintData(r4, r1, r5)
            if (r5 == 0) goto L_0x0037
            r4 = r5
        L_0x0037:
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.stripe.android.FingerprintParamsUtils.addFingerprintData$stripe_release(java.util.Map, java.lang.String):java.util.Map");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000d, code lost:
        r4 = kotlin.collections.MapsKt.plus(r3, (java.util.Map<java.lang.String, ?>) kotlin.collections.MapsKt.mapOf(kotlin.TuplesKt.to(r4, kotlin.collections.MapsKt.plus(r0, (java.util.Map) r2.apiFingerprintParamsFactory.createParams(r5)))));
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final java.util.Map<java.lang.String, ?> addFingerprintData(java.util.Map<java.lang.String, ?> r3, java.lang.String r4, java.lang.String r5) {
        /*
            r2 = this;
            java.lang.Object r0 = r3.get(r4)
            boolean r1 = r0 instanceof java.util.Map
            if (r1 != 0) goto L_0x0009
            r0 = 0
        L_0x0009:
            java.util.Map r0 = (java.util.Map) r0
            if (r0 == 0) goto L_0x0026
            com.stripe.android.ApiFingerprintParamsFactory r1 = r2.apiFingerprintParamsFactory
            java.util.Map r5 = r1.createParams(r5)
            java.util.Map r5 = kotlin.collections.MapsKt.plus(r0, r5)
            kotlin.Pair r4 = kotlin.TuplesKt.to(r4, r5)
            java.util.Map r4 = kotlin.collections.MapsKt.mapOf(r4)
            java.util.Map r4 = kotlin.collections.MapsKt.plus(r3, r4)
            if (r4 == 0) goto L_0x0026
            r3 = r4
        L_0x0026:
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.stripe.android.FingerprintParamsUtils.addFingerprintData(java.util.Map, java.lang.String, java.lang.String):java.util.Map");
    }
}
