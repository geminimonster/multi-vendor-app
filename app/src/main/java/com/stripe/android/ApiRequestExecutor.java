package com.stripe.android;

import com.facebook.share.internal.ShareConstants;
import com.stripe.android.ConnectionFactory;
import com.stripe.android.exception.APIConnectionException;
import com.stripe.android.exception.InvalidRequestException;
import java.net.UnknownHostException;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\b`\u0018\u00002\u00020\u0001:\u0001\u0007J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0006H&¨\u0006\b"}, d2 = {"Lcom/stripe/android/ApiRequestExecutor;", "", "execute", "Lcom/stripe/android/StripeResponse;", "request", "Lcom/stripe/android/ApiRequest;", "Lcom/stripe/android/FileUploadRequest;", "Default", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: ApiRequestExecutor.kt */
public interface ApiRequestExecutor {
    StripeResponse execute(ApiRequest apiRequest) throws APIConnectionException, InvalidRequestException, UnknownHostException;

    StripeResponse execute(FileUploadRequest fileUploadRequest) throws APIConnectionException, InvalidRequestException, UnknownHostException;

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0011\b\u0000\u0012\b\b\u0002\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u0010\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\nH\u0016J\u0010\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\u000bH\u0016J\u0010\u0010\f\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\rH\u0002R\u000e\u0010\u0005\u001a\u00020\u0006X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\u000e"}, d2 = {"Lcom/stripe/android/ApiRequestExecutor$Default;", "Lcom/stripe/android/ApiRequestExecutor;", "logger", "Lcom/stripe/android/Logger;", "(Lcom/stripe/android/Logger;)V", "connectionFactory", "Lcom/stripe/android/ConnectionFactory$Default;", "execute", "Lcom/stripe/android/StripeResponse;", "request", "Lcom/stripe/android/ApiRequest;", "Lcom/stripe/android/FileUploadRequest;", "executeInternal", "Lcom/stripe/android/StripeRequest;", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: ApiRequestExecutor.kt */
    public static final class Default implements ApiRequestExecutor {
        private final ConnectionFactory.Default connectionFactory;
        private final Logger logger;

        public Default() {
            this((Logger) null, 1, (DefaultConstructorMarker) null);
        }

        public Default(Logger logger2) {
            Intrinsics.checkParameterIsNotNull(logger2, "logger");
            this.logger = logger2;
            this.connectionFactory = new ConnectionFactory.Default();
        }

        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ Default(Logger logger2, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this((i & 1) != 0 ? Logger.Companion.noop$stripe_release() : logger2);
        }

        public StripeResponse execute(ApiRequest apiRequest) throws APIConnectionException, InvalidRequestException, UnknownHostException {
            Intrinsics.checkParameterIsNotNull(apiRequest, ShareConstants.WEB_DIALOG_RESULT_PARAM_REQUEST_ID);
            return executeInternal(apiRequest);
        }

        public StripeResponse execute(FileUploadRequest fileUploadRequest) throws APIConnectionException, InvalidRequestException, UnknownHostException {
            Intrinsics.checkParameterIsNotNull(fileUploadRequest, ShareConstants.WEB_DIALOG_RESULT_PARAM_REQUEST_ID);
            return executeInternal(fileUploadRequest);
        }

        /* JADX WARNING: Code restructure failed: missing block: B:15:0x0042, code lost:
            r1 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:16:0x0043, code lost:
            kotlin.io.CloseableKt.closeFinally(r0, r6);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:17:0x0046, code lost:
            throw r1;
         */
        /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        private final com.stripe.android.StripeResponse executeInternal(com.stripe.android.StripeRequest r6) {
            /*
                r5 = this;
                com.stripe.android.Logger r0 = r5.logger
                java.lang.String r1 = r6.toString()
                r0.info(r1)
                com.stripe.android.ConnectionFactory$Default r0 = r5.connectionFactory
                com.stripe.android.StripeConnection r0 = r0.create(r6)
                java.io.Closeable r0 = (java.io.Closeable) r0
                r1 = 0
                java.lang.Throwable r1 = (java.lang.Throwable) r1
                r2 = r0
                com.stripe.android.StripeConnection r2 = (com.stripe.android.StripeConnection) r2     // Catch:{ all -> 0x0040 }
                com.stripe.android.StripeResponse r2 = r2.getResponse()     // Catch:{ IOException -> 0x0028 }
                com.stripe.android.Logger r3 = r5.logger     // Catch:{ IOException -> 0x0028 }
                java.lang.String r4 = r2.toString()     // Catch:{ IOException -> 0x0028 }
                r3.info(r4)     // Catch:{ IOException -> 0x0028 }
                kotlin.io.CloseableKt.closeFinally(r0, r1)     // Catch:{  }
                return r2
            L_0x0028:
                r1 = move-exception
                com.stripe.android.Logger r2 = r5.logger     // Catch:{ all -> 0x0040 }
                java.lang.String r3 = "Exception while making Stripe API request"
                r4 = r1
                java.lang.Throwable r4 = (java.lang.Throwable) r4     // Catch:{ all -> 0x0040 }
                r2.error(r3, r4)     // Catch:{ all -> 0x0040 }
                com.stripe.android.exception.APIConnectionException$Companion r2 = com.stripe.android.exception.APIConnectionException.Companion     // Catch:{ all -> 0x0040 }
                java.lang.String r6 = r6.getBaseUrl()     // Catch:{ all -> 0x0040 }
                com.stripe.android.exception.APIConnectionException r6 = r2.create$stripe_release(r1, r6)     // Catch:{ all -> 0x0040 }
                java.lang.Throwable r6 = (java.lang.Throwable) r6     // Catch:{ all -> 0x0040 }
                throw r6     // Catch:{ all -> 0x0040 }
            L_0x0040:
                r6 = move-exception
                throw r6     // Catch:{ all -> 0x0042 }
            L_0x0042:
                r1 = move-exception
                kotlin.io.CloseableKt.closeFinally(r0, r6)
                throw r1
            */
            throw new UnsupportedOperationException("Method not decompiled: com.stripe.android.ApiRequestExecutor.Default.executeInternal(com.stripe.android.StripeRequest):com.stripe.android.StripeResponse");
        }
    }
}
