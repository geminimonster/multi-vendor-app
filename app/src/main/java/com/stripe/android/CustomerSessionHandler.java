package com.stripe.android;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Pair;
import androidx.core.app.NotificationCompat;
import com.google.android.gms.common.internal.ServiceSpecificExtraArgs;
import com.stripe.android.CustomerSessionRunnableFactory;
import com.stripe.android.exception.StripeException;
import com.stripe.android.model.Customer;
import com.stripe.android.model.PaymentMethod;
import com.stripe.android.model.Source;
import java.util.List;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0000\u0018\u00002\u00020\u0001:\u0001\tB\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bH\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\n"}, d2 = {"Lcom/stripe/android/CustomerSessionHandler;", "Landroid/os/Handler;", "listener", "Lcom/stripe/android/CustomerSessionHandler$Listener;", "(Lcom/stripe/android/CustomerSessionHandler$Listener;)V", "handleMessage", "", "msg", "Landroid/os/Message;", "Listener", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: CustomerSessionHandler.kt */
public final class CustomerSessionHandler extends Handler {
    private final Listener listener;

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\b`\u0018\u00002\u00020\u0001J\u001a\u0010\u0002\u001a\u00020\u00032\b\u0010\u0004\u001a\u0004\u0018\u00010\u00052\u0006\u0010\u0006\u001a\u00020\u0007H&J\u001a\u0010\b\u001a\u00020\u00032\b\u0010\u0004\u001a\u0004\u0018\u00010\u00052\u0006\u0010\u0006\u001a\u00020\u0007H&J\u0018\u0010\t\u001a\u00020\u00032\u0006\u0010\n\u001a\u00020\u000b2\u0006\u0010\u0006\u001a\u00020\u0007H&J\u001a\u0010\f\u001a\u00020\u00032\b\u0010\r\u001a\u0004\u0018\u00010\u000e2\u0006\u0010\u0006\u001a\u00020\u0007H&J\u001e\u0010\u000f\u001a\u00020\u00032\f\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u000e0\u00112\u0006\u0010\u0006\u001a\u00020\u0007H&J\u001a\u0010\u0012\u001a\u00020\u00032\b\u0010\u0013\u001a\u0004\u0018\u00010\u00142\u0006\u0010\u0006\u001a\u00020\u0007H&¨\u0006\u0015"}, d2 = {"Lcom/stripe/android/CustomerSessionHandler$Listener;", "", "onCustomerRetrieved", "", "customer", "Lcom/stripe/android/model/Customer;", "operationId", "", "onCustomerShippingInfoSaved", "onError", "exception", "Lcom/stripe/android/exception/StripeException;", "onPaymentMethodRetrieved", "paymentMethod", "Lcom/stripe/android/model/PaymentMethod;", "onPaymentMethodsRetrieved", "paymentMethods", "", "onSourceRetrieved", "source", "Lcom/stripe/android/model/Source;", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: CustomerSessionHandler.kt */
    public interface Listener {
        void onCustomerRetrieved(Customer customer, String str);

        void onCustomerShippingInfoSaved(Customer customer, String str);

        void onError(StripeException stripeException, String str);

        void onPaymentMethodRetrieved(PaymentMethod paymentMethod, String str);

        void onPaymentMethodsRetrieved(List<PaymentMethod> list, String str);

        void onSourceRetrieved(Source source, String str);
    }

    @Metadata(bv = {1, 0, 3}, k = 3, mv = {1, 1, 16})
    public final /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;

        static {
            int[] iArr = new int[CustomerSessionRunnableFactory.ResultType.values().length];
            $EnumSwitchMapping$0 = iArr;
            iArr[CustomerSessionRunnableFactory.ResultType.CustomerRetrieved.ordinal()] = 1;
            $EnumSwitchMapping$0[CustomerSessionRunnableFactory.ResultType.SourceRetrieved.ordinal()] = 2;
            $EnumSwitchMapping$0[CustomerSessionRunnableFactory.ResultType.PaymentMethod.ordinal()] = 3;
            $EnumSwitchMapping$0[CustomerSessionRunnableFactory.ResultType.ShippingInfo.ordinal()] = 4;
            $EnumSwitchMapping$0[CustomerSessionRunnableFactory.ResultType.PaymentMethods.ordinal()] = 5;
            $EnumSwitchMapping$0[CustomerSessionRunnableFactory.ResultType.Error.ordinal()] = 6;
        }
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CustomerSessionHandler(Listener listener2) {
        super(Looper.getMainLooper());
        Intrinsics.checkParameterIsNotNull(listener2, ServiceSpecificExtraArgs.CastExtraArgs.LISTENER);
        this.listener = listener2;
    }

    public void handleMessage(Message message) {
        Intrinsics.checkParameterIsNotNull(message, NotificationCompat.CATEGORY_MESSAGE);
        super.handleMessage(message);
        Object obj = message.obj;
        if (obj != null) {
            Pair pair = (Pair) obj;
            String str = (String) pair.first;
            Object obj2 = pair.second;
            switch (WhenMappings.$EnumSwitchMapping$0[CustomerSessionRunnableFactory.ResultType.values()[message.what].ordinal()]) {
                case 1:
                    Listener listener2 = this.listener;
                    if (obj2 != null) {
                        Intrinsics.checkExpressionValueIsNotNull(str, "operationId");
                        listener2.onCustomerRetrieved((Customer) obj2, str);
                        return;
                    }
                    throw new TypeCastException("null cannot be cast to non-null type com.stripe.android.model.Customer");
                case 2:
                    Listener listener3 = this.listener;
                    if (obj2 != null) {
                        Intrinsics.checkExpressionValueIsNotNull(str, "operationId");
                        listener3.onSourceRetrieved((Source) obj2, str);
                        return;
                    }
                    throw new TypeCastException("null cannot be cast to non-null type com.stripe.android.model.Source");
                case 3:
                    Listener listener4 = this.listener;
                    if (obj2 != null) {
                        Intrinsics.checkExpressionValueIsNotNull(str, "operationId");
                        listener4.onPaymentMethodRetrieved((PaymentMethod) obj2, str);
                        return;
                    }
                    throw new TypeCastException("null cannot be cast to non-null type com.stripe.android.model.PaymentMethod");
                case 4:
                    Listener listener5 = this.listener;
                    if (obj2 != null) {
                        Intrinsics.checkExpressionValueIsNotNull(str, "operationId");
                        listener5.onCustomerShippingInfoSaved((Customer) obj2, str);
                        return;
                    }
                    throw new TypeCastException("null cannot be cast to non-null type com.stripe.android.model.Customer");
                case 5:
                    Listener listener6 = this.listener;
                    if (obj2 != null) {
                        Intrinsics.checkExpressionValueIsNotNull(str, "operationId");
                        listener6.onPaymentMethodsRetrieved((List) obj2, str);
                        return;
                    }
                    throw new TypeCastException("null cannot be cast to non-null type kotlin.collections.List<com.stripe.android.model.PaymentMethod>");
                case 6:
                    if (obj2 instanceof StripeException) {
                        Intrinsics.checkExpressionValueIsNotNull(str, "operationId");
                        this.listener.onError((StripeException) obj2, str);
                        return;
                    }
                    return;
                default:
                    return;
            }
        } else {
            throw new TypeCastException("null cannot be cast to non-null type android.util.Pair<kotlin.String, kotlin.Any>");
        }
    }
}
