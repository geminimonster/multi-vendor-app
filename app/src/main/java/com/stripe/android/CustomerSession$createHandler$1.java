package com.stripe.android;

import com.stripe.android.CustomerSession;
import com.stripe.android.CustomerSessionHandler;
import com.stripe.android.exception.StripeException;
import com.stripe.android.model.Customer;
import com.stripe.android.model.PaymentMethod;
import com.stripe.android.model.Source;
import java.util.List;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000=\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000*\u0001\u0000\b\n\u0018\u00002\u00020\u0001J\u001a\u0010\u0002\u001a\u00020\u00032\b\u0010\u0004\u001a\u0004\u0018\u00010\u00052\u0006\u0010\u0006\u001a\u00020\u0007H\u0016J\u001a\u0010\b\u001a\u00020\u00032\b\u0010\u0004\u001a\u0004\u0018\u00010\u00052\u0006\u0010\u0006\u001a\u00020\u0007H\u0016J\u0018\u0010\t\u001a\u00020\u00032\u0006\u0010\n\u001a\u00020\u000b2\u0006\u0010\u0006\u001a\u00020\u0007H\u0016J\u001a\u0010\f\u001a\u00020\u00032\b\u0010\r\u001a\u0004\u0018\u00010\u000e2\u0006\u0010\u0006\u001a\u00020\u0007H\u0016J\u001e\u0010\u000f\u001a\u00020\u00032\f\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u000e0\u00112\u0006\u0010\u0006\u001a\u00020\u0007H\u0016J\u001a\u0010\u0012\u001a\u00020\u00032\b\u0010\u0013\u001a\u0004\u0018\u00010\u00142\u0006\u0010\u0006\u001a\u00020\u0007H\u0016¨\u0006\u0015"}, d2 = {"com/stripe/android/CustomerSession$createHandler$1", "Lcom/stripe/android/CustomerSessionHandler$Listener;", "onCustomerRetrieved", "", "customer", "Lcom/stripe/android/model/Customer;", "operationId", "", "onCustomerShippingInfoSaved", "onError", "exception", "Lcom/stripe/android/exception/StripeException;", "onPaymentMethodRetrieved", "paymentMethod", "Lcom/stripe/android/model/PaymentMethod;", "onPaymentMethodsRetrieved", "paymentMethods", "", "onSourceRetrieved", "source", "Lcom/stripe/android/model/Source;", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: CustomerSession.kt */
public final class CustomerSession$createHandler$1 implements CustomerSessionHandler.Listener {
    final /* synthetic */ CustomerSession this$0;

    CustomerSession$createHandler$1(CustomerSession customerSession) {
        this.this$0 = customerSession;
    }

    public void onCustomerRetrieved(Customer customer, String str) {
        Intrinsics.checkParameterIsNotNull(str, "operationId");
        this.this$0.setCustomer$stripe_release(customer);
        CustomerSession customerSession = this.this$0;
        customerSession.setCustomerCacheTime$stripe_release(((Number) customerSession.timeSupplier.invoke()).longValue());
        CustomerSession.CustomerRetrievalListener customerRetrievalListener = (CustomerSession.CustomerRetrievalListener) this.this$0.getListener(str);
        if (customer != null && customerRetrievalListener != null) {
            customerRetrievalListener.onCustomerRetrieved(customer);
        }
    }

    public void onSourceRetrieved(Source source, String str) {
        Intrinsics.checkParameterIsNotNull(str, "operationId");
        CustomerSession.SourceRetrievalListener sourceRetrievalListener = (CustomerSession.SourceRetrievalListener) this.this$0.getListener(str);
        if (source != null && sourceRetrievalListener != null) {
            sourceRetrievalListener.onSourceRetrieved(source);
        }
    }

    public void onPaymentMethodRetrieved(PaymentMethod paymentMethod, String str) {
        Intrinsics.checkParameterIsNotNull(str, "operationId");
        CustomerSession.PaymentMethodRetrievalListener paymentMethodRetrievalListener = (CustomerSession.PaymentMethodRetrievalListener) this.this$0.getListener(str);
        if (paymentMethod != null && paymentMethodRetrievalListener != null) {
            paymentMethodRetrievalListener.onPaymentMethodRetrieved(paymentMethod);
        }
    }

    public void onPaymentMethodsRetrieved(List<PaymentMethod> list, String str) {
        Intrinsics.checkParameterIsNotNull(list, "paymentMethods");
        Intrinsics.checkParameterIsNotNull(str, "operationId");
        CustomerSession.PaymentMethodsRetrievalListener paymentMethodsRetrievalListener = (CustomerSession.PaymentMethodsRetrievalListener) this.this$0.getListener(str);
        if (paymentMethodsRetrievalListener != null) {
            paymentMethodsRetrievalListener.onPaymentMethodsRetrieved(list);
        }
    }

    public void onCustomerShippingInfoSaved(Customer customer, String str) {
        Intrinsics.checkParameterIsNotNull(str, "operationId");
        this.this$0.setCustomer$stripe_release(customer);
        CustomerSession.CustomerRetrievalListener customerRetrievalListener = (CustomerSession.CustomerRetrievalListener) this.this$0.getListener(str);
        if (customer != null && customerRetrievalListener != null) {
            customerRetrievalListener.onCustomerRetrieved(customer);
        }
    }

    public void onError(StripeException stripeException, String str) {
        Intrinsics.checkParameterIsNotNull(stripeException, "exception");
        Intrinsics.checkParameterIsNotNull(str, "operationId");
        this.this$0.handleRetrievalError(str, stripeException);
    }
}
