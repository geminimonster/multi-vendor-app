package com.stripe.android;

import com.stripe.android.CustomerSession;
import com.stripe.android.EphemeralKeyManager;
import java.util.Map;
import kotlin.Metadata;
import kotlin.coroutines.Continuation;
import kotlin.coroutines.CoroutineContext;
import kotlin.jvm.internal.Intrinsics;
import kotlinx.coroutines.CoroutineDispatcher;
import kotlinx.coroutines.CoroutineScopeKt;
import kotlinx.coroutines.CoroutineStart;
import kotlinx.coroutines.Job;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010%\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0000\u0018\u00002\u00020\u0001B+\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0014\u0010\u0006\u001a\u0010\u0012\u0004\u0012\u00020\b\u0012\u0006\u0012\u0004\u0018\u00010\t0\u0007¢\u0006\u0002\u0010\nJ \u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\b2\u0006\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\bH\u0016J\u0018\u0010\u0011\u001a\u00020\f2\u0006\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u0015H\u0016R\u001c\u0010\u0006\u001a\u0010\u0012\u0004\u0012\u00020\b\u0012\u0006\u0012\u0004\u0018\u00010\t0\u0007X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0016"}, d2 = {"Lcom/stripe/android/CustomerSessionEphemeralKeyManagerListener;", "Lcom/stripe/android/EphemeralKeyManager$KeyManagerListener;", "runnableFactory", "Lcom/stripe/android/CustomerSessionRunnableFactory;", "workDispatcher", "Lkotlinx/coroutines/CoroutineDispatcher;", "listeners", "", "", "Lcom/stripe/android/CustomerSession$RetrievalListener;", "(Lcom/stripe/android/CustomerSessionRunnableFactory;Lkotlinx/coroutines/CoroutineDispatcher;Ljava/util/Map;)V", "onKeyError", "", "operationId", "errorCode", "", "errorMessage", "onKeyUpdate", "ephemeralKey", "Lcom/stripe/android/EphemeralKey;", "operation", "Lcom/stripe/android/EphemeralOperation;", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: CustomerSessionEphemeralKeyManagerListener.kt */
public final class CustomerSessionEphemeralKeyManagerListener implements EphemeralKeyManager.KeyManagerListener {
    private final Map<String, CustomerSession.RetrievalListener> listeners;
    private final CustomerSessionRunnableFactory runnableFactory;
    private final CoroutineDispatcher workDispatcher;

    public CustomerSessionEphemeralKeyManagerListener(CustomerSessionRunnableFactory customerSessionRunnableFactory, CoroutineDispatcher coroutineDispatcher, Map<String, CustomerSession.RetrievalListener> map) {
        Intrinsics.checkParameterIsNotNull(customerSessionRunnableFactory, "runnableFactory");
        Intrinsics.checkParameterIsNotNull(coroutineDispatcher, "workDispatcher");
        Intrinsics.checkParameterIsNotNull(map, "listeners");
        this.runnableFactory = customerSessionRunnableFactory;
        this.workDispatcher = coroutineDispatcher;
        this.listeners = map;
    }

    public void onKeyUpdate(EphemeralKey ephemeralKey, EphemeralOperation ephemeralOperation) {
        Intrinsics.checkParameterIsNotNull(ephemeralKey, "ephemeralKey");
        Intrinsics.checkParameterIsNotNull(ephemeralOperation, "operation");
        Runnable create$stripe_release = this.runnableFactory.create$stripe_release(ephemeralKey, ephemeralOperation);
        if (create$stripe_release != null) {
            Job unused = BuildersKt__Builders_commonKt.launch$default(CoroutineScopeKt.CoroutineScope(this.workDispatcher), (CoroutineContext) null, (CoroutineStart) null, new CustomerSessionEphemeralKeyManagerListener$onKeyUpdate$1$1(create$stripe_release, (Continuation) null), 3, (Object) null);
        }
    }

    public void onKeyError(String str, int i, String str2) {
        Intrinsics.checkParameterIsNotNull(str, "operationId");
        Intrinsics.checkParameterIsNotNull(str2, "errorMessage");
        CustomerSession.RetrievalListener remove = this.listeners.remove(str);
        if (remove != null) {
            remove.onError(i, str2, (StripeError) null);
        }
    }
}
