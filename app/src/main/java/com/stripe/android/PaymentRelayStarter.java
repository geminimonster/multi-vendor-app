package com.stripe.android;

import android.os.Parcel;
import android.os.Parcelable;
import com.stripe.android.exception.StripeException;
import com.stripe.android.model.PaymentIntent;
import com.stripe.android.model.SetupIntent;
import com.stripe.android.model.Source;
import com.stripe.android.model.StripeIntent;
import com.stripe.android.view.AuthActivityStarter;
import java.io.Serializable;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import kotlinx.android.parcel.Parceler;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\b`\u0018\u0000 \u00042\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0002\u0003\u0004¨\u0006\u0005"}, d2 = {"Lcom/stripe/android/PaymentRelayStarter;", "Lcom/stripe/android/view/AuthActivityStarter;", "Lcom/stripe/android/PaymentRelayStarter$Args;", "Args", "Companion", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: PaymentRelayStarter.kt */
public interface PaymentRelayStarter extends AuthActivityStarter<Args> {
    public static final Companion Companion = Companion.$$INSTANCE;

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u001d\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bH\u0000¢\u0006\u0002\b\t¨\u0006\n"}, d2 = {"Lcom/stripe/android/PaymentRelayStarter$Companion;", "", "()V", "create", "Lcom/stripe/android/PaymentRelayStarter;", "host", "Lcom/stripe/android/view/AuthActivityStarter$Host;", "requestCode", "", "create$stripe_release", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: PaymentRelayStarter.kt */
    public static final class Companion {
        static final /* synthetic */ Companion $$INSTANCE = new Companion();

        private Companion() {
        }

        public final /* synthetic */ PaymentRelayStarter create$stripe_release(AuthActivityStarter.Host host, int i) {
            Intrinsics.checkParameterIsNotNull(host, "host");
            return new PaymentRelayStarter$Companion$create$1(host, i);
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000F\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u000f\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\b\b\u0018\u0000 %2\u00020\u0001:\u0001%B7\b\u0000\u0012\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u0012\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\t¢\u0006\u0002\u0010\nJ\u000b\u0010\u0013\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000b\u0010\u0014\u001a\u0004\u0018\u00010\u0005HÆ\u0003J\u000b\u0010\u0015\u001a\u0004\u0018\u00010\u0007HÆ\u0003J\u000b\u0010\u0016\u001a\u0004\u0018\u00010\tHÆ\u0003J9\u0010\u0017\u001a\u00020\u00002\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u00072\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\tHÆ\u0001J\t\u0010\u0018\u001a\u00020\u0019HÖ\u0001J\u0013\u0010\u001a\u001a\u00020\u001b2\b\u0010\u001c\u001a\u0004\u0018\u00010\u001dHÖ\u0003J\t\u0010\u001e\u001a\u00020\u0019HÖ\u0001J\t\u0010\u001f\u001a\u00020\tHÖ\u0001J\u0019\u0010 \u001a\u00020!2\u0006\u0010\"\u001a\u00020#2\u0006\u0010$\u001a\u00020\u0019HÖ\u0001R\u0013\u0010\u0006\u001a\u0004\u0018\u00010\u0007¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0013\u0010\u0004\u001a\u0004\u0018\u00010\u0005¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000eR\u0013\u0010\b\u001a\u0004\u0018\u00010\t¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010R\u0013\u0010\u0002\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u0012¨\u0006&"}, d2 = {"Lcom/stripe/android/PaymentRelayStarter$Args;", "Landroid/os/Parcelable;", "stripeIntent", "Lcom/stripe/android/model/StripeIntent;", "source", "Lcom/stripe/android/model/Source;", "exception", "Lcom/stripe/android/exception/StripeException;", "stripeAccountId", "", "(Lcom/stripe/android/model/StripeIntent;Lcom/stripe/android/model/Source;Lcom/stripe/android/exception/StripeException;Ljava/lang/String;)V", "getException", "()Lcom/stripe/android/exception/StripeException;", "getSource", "()Lcom/stripe/android/model/Source;", "getStripeAccountId", "()Ljava/lang/String;", "getStripeIntent", "()Lcom/stripe/android/model/StripeIntent;", "component1", "component2", "component3", "component4", "copy", "describeContents", "", "equals", "", "other", "", "hashCode", "toString", "writeToParcel", "", "parcel", "Landroid/os/Parcel;", "flags", "Companion", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: PaymentRelayStarter.kt */
    public static final class Args implements Parcelable {
        public static final Parcelable.Creator CREATOR = new Creator();
        public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
        private final StripeException exception;
        private final Source source;
        private final String stripeAccountId;
        private final StripeIntent stripeIntent;

        @Metadata(bv = {1, 0, 3}, k = 3, mv = {1, 1, 16})
        public static class Creator implements Parcelable.Creator {
            public final Object createFromParcel(Parcel parcel) {
                Intrinsics.checkParameterIsNotNull(parcel, "in");
                return Args.Companion.create(parcel);
            }

            public final Object[] newArray(int i) {
                return new Args[i];
            }
        }

        public Args() {
            this((StripeIntent) null, (Source) null, (StripeException) null, (String) null, 15, (DefaultConstructorMarker) null);
        }

        public static /* synthetic */ Args copy$default(Args args, StripeIntent stripeIntent2, Source source2, StripeException stripeException, String str, int i, Object obj) {
            if ((i & 1) != 0) {
                stripeIntent2 = args.stripeIntent;
            }
            if ((i & 2) != 0) {
                source2 = args.source;
            }
            if ((i & 4) != 0) {
                stripeException = args.exception;
            }
            if ((i & 8) != 0) {
                str = args.stripeAccountId;
            }
            return args.copy(stripeIntent2, source2, stripeException, str);
        }

        public final StripeIntent component1() {
            return this.stripeIntent;
        }

        public final Source component2() {
            return this.source;
        }

        public final StripeException component3() {
            return this.exception;
        }

        public final String component4() {
            return this.stripeAccountId;
        }

        public final Args copy(StripeIntent stripeIntent2, Source source2, StripeException stripeException, String str) {
            return new Args(stripeIntent2, source2, stripeException, str);
        }

        public int describeContents() {
            return 0;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Args)) {
                return false;
            }
            Args args = (Args) obj;
            return Intrinsics.areEqual((Object) this.stripeIntent, (Object) args.stripeIntent) && Intrinsics.areEqual((Object) this.source, (Object) args.source) && Intrinsics.areEqual((Object) this.exception, (Object) args.exception) && Intrinsics.areEqual((Object) this.stripeAccountId, (Object) args.stripeAccountId);
        }

        public int hashCode() {
            StripeIntent stripeIntent2 = this.stripeIntent;
            int i = 0;
            int hashCode = (stripeIntent2 != null ? stripeIntent2.hashCode() : 0) * 31;
            Source source2 = this.source;
            int hashCode2 = (hashCode + (source2 != null ? source2.hashCode() : 0)) * 31;
            StripeException stripeException = this.exception;
            int hashCode3 = (hashCode2 + (stripeException != null ? stripeException.hashCode() : 0)) * 31;
            String str = this.stripeAccountId;
            if (str != null) {
                i = str.hashCode();
            }
            return hashCode3 + i;
        }

        public String toString() {
            return "Args(stripeIntent=" + this.stripeIntent + ", source=" + this.source + ", exception=" + this.exception + ", stripeAccountId=" + this.stripeAccountId + ")";
        }

        public void writeToParcel(Parcel parcel, int i) {
            Intrinsics.checkParameterIsNotNull(parcel, "parcel");
            Companion.write(this, parcel, i);
        }

        public Args(StripeIntent stripeIntent2, Source source2, StripeException stripeException, String str) {
            this.stripeIntent = stripeIntent2;
            this.source = source2;
            this.exception = stripeException;
            this.stripeAccountId = str;
        }

        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ Args(StripeIntent stripeIntent2, Source source2, StripeException stripeException, String str, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this((i & 1) != 0 ? null : stripeIntent2, (i & 2) != 0 ? null : source2, (i & 4) != 0 ? null : stripeException, (i & 8) != 0 ? null : str);
        }

        public final StripeIntent getStripeIntent() {
            return this.stripeIntent;
        }

        public final Source getSource() {
            return this.source;
        }

        public final StripeException getException() {
            return this.exception;
        }

        public final String getStripeAccountId() {
            return this.stripeAccountId;
        }

        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\u0003\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0016B\u0007\b\u0002¢\u0006\u0002\u0010\u0003J\u0010\u0010\u0004\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0006H\u0016J\u0015\u0010\u0004\u001a\u00020\u00022\u0006\u0010\u0007\u001a\u00020\bH\u0000¢\u0006\u0002\b\tJ!\u0010\u0004\u001a\u00020\u00022\u0006\u0010\n\u001a\u00020\u000b2\n\b\u0002\u0010\f\u001a\u0004\u0018\u00010\rH\u0000¢\u0006\u0002\b\tJ!\u0010\u0004\u001a\u00020\u00022\u0006\u0010\u000e\u001a\u00020\u000f2\n\b\u0002\u0010\f\u001a\u0004\u0018\u00010\rH\u0000¢\u0006\u0002\b\tJ\u0012\u0010\u0010\u001a\u0004\u0018\u00010\u000f2\u0006\u0010\u0005\u001a\u00020\u0006H\u0002J\u001a\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0005\u001a\u00020\u00062\b\u0010\u000e\u001a\u0004\u0018\u00010\u000fH\u0002J\u001c\u0010\u0013\u001a\u00020\u0012*\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0014\u001a\u00020\u0015H\u0016¨\u0006\u0017"}, d2 = {"Lcom/stripe/android/PaymentRelayStarter$Args$Companion;", "Lkotlinx/android/parcel/Parceler;", "Lcom/stripe/android/PaymentRelayStarter$Args;", "()V", "create", "parcel", "Landroid/os/Parcel;", "exception", "Lcom/stripe/android/exception/StripeException;", "create$stripe_release", "source", "Lcom/stripe/android/model/Source;", "stripeAccountId", "", "stripeIntent", "Lcom/stripe/android/model/StripeIntent;", "readStripeIntent", "writeStripeIntent", "", "write", "flags", "", "StripeIntentType", "stripe_release"}, k = 1, mv = {1, 1, 16})
        /* compiled from: PaymentRelayStarter.kt */
        public static final class Companion implements Parceler<Args> {

            @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0005\b\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002j\u0002\b\u0003j\u0002\b\u0004j\u0002\b\u0005¨\u0006\u0006"}, d2 = {"Lcom/stripe/android/PaymentRelayStarter$Args$Companion$StripeIntentType;", "", "(Ljava/lang/String;I)V", "None", "PaymentIntent", "SetupIntent", "stripe_release"}, k = 1, mv = {1, 1, 16})
            /* compiled from: PaymentRelayStarter.kt */
            private enum StripeIntentType {
                None,
                PaymentIntent,
                SetupIntent
            }

            @Metadata(bv = {1, 0, 3}, k = 3, mv = {1, 1, 16})
            public final /* synthetic */ class WhenMappings {
                public static final /* synthetic */ int[] $EnumSwitchMapping$0;

                static {
                    int[] iArr = new int[StripeIntentType.values().length];
                    $EnumSwitchMapping$0 = iArr;
                    iArr[StripeIntentType.PaymentIntent.ordinal()] = 1;
                    $EnumSwitchMapping$0[StripeIntentType.SetupIntent.ordinal()] = 2;
                }
            }

            private Companion() {
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }

            public Args[] newArray(int i) {
                return (Args[]) Parceler.DefaultImpls.newArray(this, i);
            }

            public static /* synthetic */ Args create$stripe_release$default(Companion companion, StripeIntent stripeIntent, String str, int i, Object obj) {
                if ((i & 2) != 0) {
                    str = null;
                }
                return companion.create$stripe_release(stripeIntent, str);
            }

            public final /* synthetic */ Args create$stripe_release(StripeIntent stripeIntent, String str) {
                Intrinsics.checkParameterIsNotNull(stripeIntent, "stripeIntent");
                return new Args(stripeIntent, (Source) null, (StripeException) null, str, 6, (DefaultConstructorMarker) null);
            }

            public static /* synthetic */ Args create$stripe_release$default(Companion companion, Source source, String str, int i, Object obj) {
                if ((i & 2) != 0) {
                    str = null;
                }
                return companion.create$stripe_release(source, str);
            }

            public final /* synthetic */ Args create$stripe_release(Source source, String str) {
                Intrinsics.checkParameterIsNotNull(source, "source");
                return new Args((StripeIntent) null, source, (StripeException) null, str, 5, (DefaultConstructorMarker) null);
            }

            public final /* synthetic */ Args create$stripe_release(StripeException stripeException) {
                Intrinsics.checkParameterIsNotNull(stripeException, "exception");
                return new Args((StripeIntent) null, (Source) null, stripeException, (String) null, 11, (DefaultConstructorMarker) null);
            }

            public Args create(Parcel parcel) {
                Intrinsics.checkParameterIsNotNull(parcel, "parcel");
                StripeIntent readStripeIntent = readStripeIntent(parcel);
                Source source = (Source) parcel.readParcelable(Source.class.getClassLoader());
                Serializable readSerializable = parcel.readSerializable();
                if (!(readSerializable instanceof StripeException)) {
                    readSerializable = null;
                }
                return new Args(readStripeIntent, source, (StripeException) readSerializable, parcel.readString());
            }

            public void write(Args args, Parcel parcel, int i) {
                Intrinsics.checkParameterIsNotNull(args, "$this$write");
                Intrinsics.checkParameterIsNotNull(parcel, "parcel");
                writeStripeIntent(parcel, args.getStripeIntent());
                parcel.writeParcelable(args.getSource(), 0);
                parcel.writeSerializable(args.getException());
                parcel.writeString(args.getStripeAccountId());
            }

            private final StripeIntent readStripeIntent(Parcel parcel) {
                int i = WhenMappings.$EnumSwitchMapping$0[StripeIntentType.values()[parcel.readInt()].ordinal()];
                if (i == 1) {
                    return (StripeIntent) parcel.readParcelable(PaymentIntent.class.getClassLoader());
                }
                if (i != 2) {
                    return null;
                }
                return (StripeIntent) parcel.readParcelable(SetupIntent.class.getClassLoader());
            }

            private final void writeStripeIntent(Parcel parcel, StripeIntent stripeIntent) {
                StripeIntentType stripeIntentType;
                if (stripeIntent instanceof PaymentIntent) {
                    stripeIntentType = StripeIntentType.PaymentIntent;
                } else if (stripeIntent instanceof SetupIntent) {
                    stripeIntentType = StripeIntentType.SetupIntent;
                } else {
                    stripeIntentType = StripeIntentType.None;
                }
                parcel.writeInt(stripeIntentType.ordinal());
                if (stripeIntent != null) {
                    parcel.writeParcelable(stripeIntent, 0);
                }
            }
        }
    }
}
