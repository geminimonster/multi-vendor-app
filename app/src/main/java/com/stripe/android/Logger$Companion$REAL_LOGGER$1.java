package com.stripe.android;

import android.util.Log;
import androidx.core.app.NotificationCompat;
import com.stripe.android.Logger;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000!\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u0003\n\u0002\b\u0002*\u0001\u0000\b\n\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\u0016J\u001a\u0010\u0006\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\b\u0010\u0007\u001a\u0004\u0018\u00010\bH\u0016J\u0010\u0010\t\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\u0016¨\u0006\n"}, d2 = {"com/stripe/android/Logger$Companion$REAL_LOGGER$1", "Lcom/stripe/android/Logger;", "debug", "", "msg", "", "error", "t", "", "info", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: Logger.kt */
public final class Logger$Companion$REAL_LOGGER$1 implements Logger {
    final /* synthetic */ Logger.Companion this$0;

    Logger$Companion$REAL_LOGGER$1(Logger.Companion companion) {
        this.this$0 = companion;
    }

    public void info(String str) {
        Intrinsics.checkParameterIsNotNull(str, NotificationCompat.CATEGORY_MESSAGE);
        Log.i("StripeSdk", str);
    }

    public void error(String str, Throwable th) {
        Intrinsics.checkParameterIsNotNull(str, NotificationCompat.CATEGORY_MESSAGE);
        Log.e("StripeSdk", str, th);
    }

    public void debug(String str) {
        Intrinsics.checkParameterIsNotNull(str, NotificationCompat.CATEGORY_MESSAGE);
        Log.d("StripeSdk", str);
    }
}
