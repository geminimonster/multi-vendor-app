package com.stripe.android;

import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Map;
import java.util.Scanner;
import javax.net.ssl.HttpsURLConnection;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0004\b`\u0018\u00002\u00020\u0001:\u0001\nR\u0012\u0010\u0002\u001a\u00020\u0003X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0004\u0010\u0005R\u0012\u0010\u0006\u001a\u00020\u0007X¦\u0004¢\u0006\u0006\u001a\u0004\b\b\u0010\t¨\u0006\u000b"}, d2 = {"Lcom/stripe/android/StripeConnection;", "Ljava/io/Closeable;", "response", "Lcom/stripe/android/StripeResponse;", "getResponse", "()Lcom/stripe/android/StripeResponse;", "responseCode", "", "getResponseCode", "()I", "Default", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: StripeConnection.kt */
public interface StripeConnection extends Closeable {
    StripeResponse getResponse();

    int getResponseCode();

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0002\u0018\u0000 \u00172\u00020\u0001:\u0001\u0017B\u000f\b\u0000\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\b\u0010\u0015\u001a\u00020\u0016H\u0016J\u0014\u0010\u000b\u001a\u0004\u0018\u00010\n2\b\u0010\u0011\u001a\u0004\u0018\u00010\u0012H\u0002R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\u0005\u001a\u00020\u00068VX\u0004¢\u0006\u0006\u001a\u0004\b\u0007\u0010\bR\u0016\u0010\t\u001a\u0004\u0018\u00010\n8BX\u0004¢\u0006\u0006\u001a\u0004\b\u000b\u0010\fR\u0014\u0010\r\u001a\u00020\u000e8VX\u0004¢\u0006\u0006\u001a\u0004\b\u000f\u0010\u0010R\u0016\u0010\u0011\u001a\u0004\u0018\u00010\u00128BX\u0004¢\u0006\u0006\u001a\u0004\b\u0013\u0010\u0014¨\u0006\u0018"}, d2 = {"Lcom/stripe/android/StripeConnection$Default;", "Lcom/stripe/android/StripeConnection;", "conn", "Ljavax/net/ssl/HttpsURLConnection;", "(Ljavax/net/ssl/HttpsURLConnection;)V", "response", "Lcom/stripe/android/StripeResponse;", "getResponse", "()Lcom/stripe/android/StripeResponse;", "responseBody", "", "getResponseBody", "()Ljava/lang/String;", "responseCode", "", "getResponseCode", "()I", "responseStream", "Ljava/io/InputStream;", "getResponseStream", "()Ljava/io/InputStream;", "close", "", "Companion", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: StripeConnection.kt */
    public static final class Default implements StripeConnection {
        private static final String CHARSET = StandardCharsets.UTF_8.name();
        @Deprecated
        public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
        private final HttpsURLConnection conn;

        public Default(HttpsURLConnection httpsURLConnection) {
            Intrinsics.checkParameterIsNotNull(httpsURLConnection, "conn");
            this.conn = httpsURLConnection;
        }

        public /* synthetic */ int getResponseCode() {
            return this.conn.getResponseCode();
        }

        public /* synthetic */ StripeResponse getResponse() throws IOException {
            int responseCode = getResponseCode();
            String responseBody = getResponseBody();
            Map headerFields = this.conn.getHeaderFields();
            Intrinsics.checkExpressionValueIsNotNull(headerFields, "conn.headerFields");
            return new StripeResponse(responseCode, responseBody, headerFields);
        }

        private final String getResponseBody() throws IOException {
            return getResponseBody(getResponseStream());
        }

        private final InputStream getResponseStream() throws IOException {
            int responseCode = getResponseCode();
            if (200 <= responseCode && 299 >= responseCode) {
                return this.conn.getInputStream();
            }
            return this.conn.getErrorStream();
        }

        private final String getResponseBody(InputStream inputStream) throws IOException {
            String str = null;
            if (inputStream == null) {
                return null;
            }
            Scanner useDelimiter = new Scanner(inputStream, CHARSET).useDelimiter("\\A");
            if (useDelimiter.hasNext()) {
                str = useDelimiter.next();
            }
            inputStream.close();
            return str;
        }

        public void close() {
            InputStream responseStream = getResponseStream();
            if (responseStream != null) {
                responseStream.close();
            }
            this.conn.disconnect();
        }

        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0016\u0010\u0003\u001a\n \u0005*\u0004\u0018\u00010\u00040\u0004X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0006"}, d2 = {"Lcom/stripe/android/StripeConnection$Default$Companion;", "", "()V", "CHARSET", "", "kotlin.jvm.PlatformType", "stripe_release"}, k = 1, mv = {1, 1, 16})
        /* compiled from: StripeConnection.kt */
        private static final class Companion {
            private Companion() {
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }
        }
    }
}
