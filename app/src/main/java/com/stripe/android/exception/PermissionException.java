package com.stripe.android.exception;

import com.stripe.android.StripeError;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0019\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005¢\u0006\u0002\u0010\u0006¨\u0006\u0007"}, d2 = {"Lcom/stripe/android/exception/PermissionException;", "Lcom/stripe/android/exception/StripeException;", "stripeError", "Lcom/stripe/android/StripeError;", "requestId", "", "(Lcom/stripe/android/StripeError;Ljava/lang/String;)V", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: PermissionException.kt */
public final class PermissionException extends StripeException {
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ PermissionException(StripeError stripeError, String str, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(stripeError, (i & 2) != 0 ? null : str);
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public PermissionException(StripeError stripeError, String str) {
        super(stripeError, str, 403, (Throwable) null, (String) null, 24, (DefaultConstructorMarker) null);
        Intrinsics.checkParameterIsNotNull(stripeError, "stripeError");
    }
}
