package com.stripe.android.exception;

import com.stripe.android.StripeError;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0003\n\u0002\b\u0003\u0018\u0000 \u00102\u00020\u0001:\u0001\u0010B\u0013\b\u0010\u0012\n\u0010\u0002\u001a\u00060\u0003j\u0002`\u0004¢\u0006\u0002\u0010\u0005B?\u0012\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u0012\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\t\u0012\b\b\u0002\u0010\n\u001a\u00020\u000b\u0012\n\b\u0002\u0010\f\u001a\u0004\u0018\u00010\t\u0012\n\b\u0002\u0010\r\u001a\u0004\u0018\u00010\u000e¢\u0006\u0002\u0010\u000f¨\u0006\u0011"}, d2 = {"Lcom/stripe/android/exception/APIException;", "Lcom/stripe/android/exception/StripeException;", "exception", "Ljava/lang/Exception;", "Lkotlin/Exception;", "(Ljava/lang/Exception;)V", "stripeError", "Lcom/stripe/android/StripeError;", "requestId", "", "statusCode", "", "message", "cause", "", "(Lcom/stripe/android/StripeError;Ljava/lang/String;ILjava/lang/String;Ljava/lang/Throwable;)V", "Companion", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: APIException.kt */
public final class APIException extends StripeException {
    public static final Companion Companion = new Companion((DefaultConstructorMarker) null);

    public APIException() {
        this((StripeError) null, (String) null, 0, (String) null, (Throwable) null, 31, (DefaultConstructorMarker) null);
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* synthetic */ APIException(com.stripe.android.StripeError r4, java.lang.String r5, int r6, java.lang.String r7, java.lang.Throwable r8, int r9, kotlin.jvm.internal.DefaultConstructorMarker r10) {
        /*
            r3 = this;
            r10 = r9 & 1
            r0 = 0
            if (r10 == 0) goto L_0x0008
            r4 = r0
            com.stripe.android.StripeError r4 = (com.stripe.android.StripeError) r4
        L_0x0008:
            r10 = r9 & 2
            if (r10 == 0) goto L_0x000f
            r5 = r0
            java.lang.String r5 = (java.lang.String) r5
        L_0x000f:
            r10 = r5
            r5 = r9 & 4
            if (r5 == 0) goto L_0x0017
            r6 = 0
            r1 = 0
            goto L_0x0018
        L_0x0017:
            r1 = r6
        L_0x0018:
            r5 = r9 & 8
            if (r5 == 0) goto L_0x0025
            if (r4 == 0) goto L_0x0024
            java.lang.String r5 = r4.getMessage()
            r7 = r5
            goto L_0x0025
        L_0x0024:
            r7 = r0
        L_0x0025:
            r2 = r7
            r5 = r9 & 16
            if (r5 == 0) goto L_0x002d
            r8 = r0
            java.lang.Throwable r8 = (java.lang.Throwable) r8
        L_0x002d:
            r0 = r8
            r5 = r3
            r6 = r4
            r7 = r10
            r8 = r1
            r9 = r2
            r10 = r0
            r5.<init>(r6, r7, r8, r9, r10)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.stripe.android.exception.APIException.<init>(com.stripe.android.StripeError, java.lang.String, int, java.lang.String, java.lang.Throwable, int, kotlin.jvm.internal.DefaultConstructorMarker):void");
    }

    public APIException(StripeError stripeError, String str, int i, String str2, Throwable th) {
        super(stripeError, str, i, th, str2);
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public APIException(Exception exc) {
        this((StripeError) null, (String) null, 0, exc.getMessage(), exc, 7, (DefaultConstructorMarker) null);
        Intrinsics.checkParameterIsNotNull(exc, "exception");
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0015\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0000¢\u0006\u0002\b\u0007¨\u0006\b"}, d2 = {"Lcom/stripe/android/exception/APIException$Companion;", "", "()V", "create", "Lcom/stripe/android/exception/APIException;", "e", "Lcom/stripe/android/exception/CardException;", "create$stripe_release", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: APIException.kt */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        public final /* synthetic */ APIException create$stripe_release(CardException cardException) {
            Intrinsics.checkParameterIsNotNull(cardException, "e");
            return new APIException(cardException.getStripeError(), cardException.getRequestId(), cardException.getStatusCode(), cardException.getMessage(), cardException);
        }
    }
}
