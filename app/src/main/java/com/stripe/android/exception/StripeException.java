package com.stripe.android.exception;

import com.stripe.android.StripeError;
import java.util.Objects;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0003\n\u0002\b\t\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0005\b&\u0018\u00002\u00060\u0001j\u0002`\u0002B?\u0012\n\b\u0002\u0010\u0003\u001a\u0004\u0018\u00010\u0004\u0012\n\b\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u0012\b\b\u0002\u0010\u0007\u001a\u00020\b\u0012\n\b\u0002\u0010\t\u001a\u0004\u0018\u00010\n\u0012\n\b\u0002\u0010\u000b\u001a\u0004\u0018\u00010\u0006¢\u0006\u0002\u0010\fJ\u0013\u0010\u0013\u001a\u00020\u00142\b\u0010\u0015\u001a\u0004\u0018\u00010\u0016H\u0002J\b\u0010\u0017\u001a\u00020\bH\u0016J\b\u0010\u0018\u001a\u00020\u0006H\u0016J\u0010\u0010\u0019\u001a\u00020\u00142\u0006\u0010\u001a\u001a\u00020\u0000H\u0002R\u0013\u0010\u0005\u001a\u0004\u0018\u00010\u0006¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000eR\u0011\u0010\u0007\u001a\u00020\b¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010R\u0013\u0010\u0003\u001a\u0004\u0018\u00010\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u0012¨\u0006\u001b"}, d2 = {"Lcom/stripe/android/exception/StripeException;", "Ljava/lang/Exception;", "Lkotlin/Exception;", "stripeError", "Lcom/stripe/android/StripeError;", "requestId", "", "statusCode", "", "cause", "", "message", "(Lcom/stripe/android/StripeError;Ljava/lang/String;ILjava/lang/Throwable;Ljava/lang/String;)V", "getRequestId", "()Ljava/lang/String;", "getStatusCode", "()I", "getStripeError", "()Lcom/stripe/android/StripeError;", "equals", "", "other", "", "hashCode", "toString", "typedEquals", "ex", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: StripeException.kt */
public abstract class StripeException extends Exception {
    private final String requestId;
    private final int statusCode;
    private final StripeError stripeError;

    public StripeException() {
        this((StripeError) null, (String) null, 0, (Throwable) null, (String) null, 31, (DefaultConstructorMarker) null);
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* synthetic */ StripeException(com.stripe.android.StripeError r4, java.lang.String r5, int r6, java.lang.Throwable r7, java.lang.String r8, int r9, kotlin.jvm.internal.DefaultConstructorMarker r10) {
        /*
            r3 = this;
            r10 = r9 & 1
            r0 = 0
            if (r10 == 0) goto L_0x0008
            r4 = r0
            com.stripe.android.StripeError r4 = (com.stripe.android.StripeError) r4
        L_0x0008:
            r10 = r9 & 2
            if (r10 == 0) goto L_0x000f
            r5 = r0
            java.lang.String r5 = (java.lang.String) r5
        L_0x000f:
            r10 = r5
            r5 = r9 & 4
            if (r5 == 0) goto L_0x0017
            r6 = 0
            r1 = 0
            goto L_0x0018
        L_0x0017:
            r1 = r6
        L_0x0018:
            r5 = r9 & 8
            if (r5 == 0) goto L_0x001f
            r7 = r0
            java.lang.Throwable r7 = (java.lang.Throwable) r7
        L_0x001f:
            r2 = r7
            r5 = r9 & 16
            if (r5 == 0) goto L_0x002d
            if (r4 == 0) goto L_0x002c
            java.lang.String r5 = r4.getMessage()
            r8 = r5
            goto L_0x002d
        L_0x002c:
            r8 = r0
        L_0x002d:
            r0 = r8
            r5 = r3
            r6 = r4
            r7 = r10
            r8 = r1
            r9 = r2
            r10 = r0
            r5.<init>(r6, r7, r8, r9, r10)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.stripe.android.exception.StripeException.<init>(com.stripe.android.StripeError, java.lang.String, int, java.lang.Throwable, java.lang.String, int, kotlin.jvm.internal.DefaultConstructorMarker):void");
    }

    public final StripeError getStripeError() {
        return this.stripeError;
    }

    public final String getRequestId() {
        return this.requestId;
    }

    public final int getStatusCode() {
        return this.statusCode;
    }

    public StripeException(StripeError stripeError2, String str, int i, Throwable th, String str2) {
        super(str2, th);
        this.stripeError = stripeError2;
        this.requestId = str;
        this.statusCode = i;
    }

    public String toString() {
        String str;
        String[] strArr = new String[2];
        String str2 = this.requestId;
        if (str2 != null) {
            str = "Request-id: " + str2;
        } else {
            str = null;
        }
        strArr[0] = str;
        strArr[1] = super.toString();
        return CollectionsKt.joinToString$default(CollectionsKt.listOfNotNull((T[]) strArr), "\n", (CharSequence) null, (CharSequence) null, 0, (CharSequence) null, (Function1) null, 62, (Object) null);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof StripeException) {
            return typedEquals((StripeException) obj);
        }
        return false;
    }

    public int hashCode() {
        return Objects.hash(new Object[]{this.stripeError, this.requestId, Integer.valueOf(this.statusCode), getMessage()});
    }

    private final boolean typedEquals(StripeException stripeException) {
        return Intrinsics.areEqual((Object) this.stripeError, (Object) stripeException.stripeError) && Intrinsics.areEqual((Object) this.requestId, (Object) stripeException.requestId) && this.statusCode == stripeException.statusCode && Intrinsics.areEqual((Object) getMessage(), (Object) stripeException.getMessage());
    }
}
