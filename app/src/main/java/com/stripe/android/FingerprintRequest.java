package com.stripe.android;

import com.facebook.internal.NativeProtocol;
import com.stripe.android.RequestHeadersFactory;
import com.stripe.android.StripeRequest;
import com.stripe.android.model.StripeJsonUtils;
import java.util.Map;
import kotlin.Metadata;
import kotlin.TuplesKt;
import kotlin.collections.MapsKt;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\u0010\u000e\n\u0002\u0010\u0000\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0006\b\u0000\u0018\u0000 \u001b2\u00020\u0001:\u0001\u001bB!\u0012\u0012\u0010\u0002\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u0003\u0012\u0006\u0010\u0006\u001a\u00020\u0004¢\u0006\u0002\u0010\u0007R\u0014\u0010\b\u001a\u00020\u0004XD¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0014\u0010\u000b\u001a\u00020\u00048TX\u0004¢\u0006\u0006\u001a\u0004\b\f\u0010\nR\u0014\u0010\r\u001a\u00020\u000eX\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010R\u0014\u0010\u0011\u001a\u00020\u0012X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u0014R\u0014\u0010\u0015\u001a\u00020\u0016X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0017\u0010\u0018R \u0010\u0002\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u0003X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0019\u0010\u001a¨\u0006\u001c"}, d2 = {"Lcom/stripe/android/FingerprintRequest;", "Lcom/stripe/android/StripeRequest;", "params", "", "", "", "guid", "(Ljava/util/Map;Ljava/lang/String;)V", "baseUrl", "getBaseUrl", "()Ljava/lang/String;", "body", "getBody", "headersFactory", "Lcom/stripe/android/RequestHeadersFactory;", "getHeadersFactory", "()Lcom/stripe/android/RequestHeadersFactory;", "method", "Lcom/stripe/android/StripeRequest$Method;", "getMethod", "()Lcom/stripe/android/StripeRequest$Method;", "mimeType", "Lcom/stripe/android/StripeRequest$MimeType;", "getMimeType", "()Lcom/stripe/android/StripeRequest$MimeType;", "getParams", "()Ljava/util/Map;", "Companion", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: FingerprintRequest.kt */
public final class FingerprintRequest extends StripeRequest {
    @Deprecated
    public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
    private static final String URL = "https://m.stripe.com/4";
    private final String baseUrl = URL;
    private final RequestHeadersFactory headersFactory;
    private final StripeRequest.Method method = StripeRequest.Method.POST;
    private final StripeRequest.MimeType mimeType = StripeRequest.MimeType.Json;
    private final Map<String, Object> params;

    public Map<String, Object> getParams() {
        return this.params;
    }

    public FingerprintRequest(Map<String, ? extends Object> map, String str) {
        Intrinsics.checkParameterIsNotNull(map, NativeProtocol.WEB_DIALOG_PARAMS);
        Intrinsics.checkParameterIsNotNull(str, "guid");
        this.params = map;
        this.headersFactory = new RequestHeadersFactory.Default(MapsKt.mapOf(TuplesKt.to("Cookie", "m=" + str)), (String) null, 2, (DefaultConstructorMarker) null);
    }

    public StripeRequest.Method getMethod() {
        return this.method;
    }

    public String getBaseUrl() {
        return this.baseUrl;
    }

    public StripeRequest.MimeType getMimeType() {
        return this.mimeType;
    }

    public RequestHeadersFactory getHeadersFactory() {
        return this.headersFactory;
    }

    /* access modifiers changed from: protected */
    public String getBody() {
        return String.valueOf(StripeJsonUtils.INSTANCE.mapToJsonObject$stripe_release(getParams()));
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\u0005"}, d2 = {"Lcom/stripe/android/FingerprintRequest$Companion;", "", "()V", "URL", "", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: FingerprintRequest.kt */
    private static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }
}
