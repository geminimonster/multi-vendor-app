package com.stripe.android;

import android.os.Handler;
import android.util.Pair;
import com.stripe.android.ApiRequest;
import com.stripe.android.EphemeralOperation;
import com.stripe.android.exception.StripeException;
import com.stripe.android.model.Customer;
import java.util.Set;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000p\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\"\n\u0002\b\u0003\b\u0000\u0018\u00002\u00020\u0001:\u0002&'B'\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\b\u0010\b\u001a\u0004\u0018\u00010\u0007¢\u0006\u0002\u0010\tJ\u001f\u0010\n\u001a\u0004\u0018\u00010\u000b2\u0006\u0010\f\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000fH\u0000¢\u0006\u0002\b\u0010J\u0018\u0010\u0011\u001a\u00020\u000b2\u0006\u0010\u0012\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u0013H\u0002J\u0018\u0010\u0014\u001a\u00020\u000b2\u0006\u0010\u0012\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u0015H\u0002J\u0018\u0010\u0016\u001a\u00020\u000b2\u0006\u0010\u0012\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u0017H\u0002J\u0018\u0010\u0018\u001a\u00020\u000b2\u0006\u0010\u0012\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u0019H\u0002J\u0018\u0010\u001a\u001a\u00020\u000b2\u0006\u0010\u0012\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u001bH\u0002J\u0018\u0010\u001c\u001a\u00020\u000b2\u0006\u0010\u0012\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u001dH\u0002J\u0018\u0010\u001e\u001a\u00020\u000b2\u0006\u0010\u0012\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u001fH\u0002J\u0018\u0010 \u001a\u00020\u000b2\u0006\u0010\u0012\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020!H\u0002J \u0010\"\u001a\u0004\u0018\u00010#2\u0006\u0010\u0012\u001a\u00020\r2\f\u0010$\u001a\b\u0012\u0004\u0012\u00020\u00070%H\u0002R\u000e\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\b\u001a\u0004\u0018\u00010\u0007X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006("}, d2 = {"Lcom/stripe/android/CustomerSessionRunnableFactory;", "", "stripeRepository", "Lcom/stripe/android/StripeRepository;", "handler", "Landroid/os/Handler;", "publishableKey", "", "stripeAccountId", "(Lcom/stripe/android/StripeRepository;Landroid/os/Handler;Ljava/lang/String;Ljava/lang/String;)V", "create", "Ljava/lang/Runnable;", "ephemeralKey", "Lcom/stripe/android/EphemeralKey;", "operation", "Lcom/stripe/android/EphemeralOperation;", "create$stripe_release", "createAddCustomerSourceRunnable", "key", "Lcom/stripe/android/EphemeralOperation$Customer$AddSource;", "createAttachPaymentMethodRunnable", "Lcom/stripe/android/EphemeralOperation$Customer$AttachPaymentMethod;", "createDeleteCustomerSourceRunnable", "Lcom/stripe/android/EphemeralOperation$Customer$DeleteSource;", "createDetachPaymentMethodRunnable", "Lcom/stripe/android/EphemeralOperation$Customer$DetachPaymentMethod;", "createGetPaymentMethodsRunnable", "Lcom/stripe/android/EphemeralOperation$Customer$GetPaymentMethods;", "createSetCustomerShippingInformationRunnable", "Lcom/stripe/android/EphemeralOperation$Customer$UpdateShipping;", "createSetCustomerSourceDefaultRunnable", "Lcom/stripe/android/EphemeralOperation$Customer$UpdateDefaultSource;", "createUpdateCustomerRunnable", "Lcom/stripe/android/EphemeralOperation$RetrieveKey;", "retrieveCustomerWithKey", "Lcom/stripe/android/model/Customer;", "productUsage", "", "CustomerSessionRunnable", "ResultType", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: CustomerSessionRunnableFactory.kt */
public final class CustomerSessionRunnableFactory {
    private final Handler handler;
    /* access modifiers changed from: private */
    public final String publishableKey;
    /* access modifiers changed from: private */
    public final String stripeAccountId;
    /* access modifiers changed from: private */
    public final StripeRepository stripeRepository;

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\b\b\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002j\u0002\b\u0003j\u0002\b\u0004j\u0002\b\u0005j\u0002\b\u0006j\u0002\b\u0007j\u0002\b\b¨\u0006\t"}, d2 = {"Lcom/stripe/android/CustomerSessionRunnableFactory$ResultType;", "", "(Ljava/lang/String;I)V", "Error", "CustomerRetrieved", "SourceRetrieved", "PaymentMethod", "PaymentMethods", "ShippingInfo", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: CustomerSessionRunnableFactory.kt */
    public enum ResultType {
        Error,
        CustomerRetrieved,
        SourceRetrieved,
        PaymentMethod,
        PaymentMethods,
        ShippingInfo
    }

    public CustomerSessionRunnableFactory(StripeRepository stripeRepository2, Handler handler2, String str, String str2) {
        Intrinsics.checkParameterIsNotNull(stripeRepository2, "stripeRepository");
        Intrinsics.checkParameterIsNotNull(handler2, "handler");
        Intrinsics.checkParameterIsNotNull(str, "publishableKey");
        this.stripeRepository = stripeRepository2;
        this.handler = handler2;
        this.publishableKey = str;
        this.stripeAccountId = str2;
    }

    public final /* synthetic */ Runnable create$stripe_release(EphemeralKey ephemeralKey, EphemeralOperation ephemeralOperation) {
        Intrinsics.checkParameterIsNotNull(ephemeralKey, "ephemeralKey");
        Intrinsics.checkParameterIsNotNull(ephemeralOperation, "operation");
        if (ephemeralOperation instanceof EphemeralOperation.RetrieveKey) {
            return createUpdateCustomerRunnable(ephemeralKey, (EphemeralOperation.RetrieveKey) ephemeralOperation);
        }
        if (ephemeralOperation instanceof EphemeralOperation.Customer.AddSource) {
            return createAddCustomerSourceRunnable(ephemeralKey, (EphemeralOperation.Customer.AddSource) ephemeralOperation);
        }
        if (ephemeralOperation instanceof EphemeralOperation.Customer.DeleteSource) {
            return createDeleteCustomerSourceRunnable(ephemeralKey, (EphemeralOperation.Customer.DeleteSource) ephemeralOperation);
        }
        if (ephemeralOperation instanceof EphemeralOperation.Customer.AttachPaymentMethod) {
            return createAttachPaymentMethodRunnable(ephemeralKey, (EphemeralOperation.Customer.AttachPaymentMethod) ephemeralOperation);
        }
        if (ephemeralOperation instanceof EphemeralOperation.Customer.DetachPaymentMethod) {
            return createDetachPaymentMethodRunnable(ephemeralKey, (EphemeralOperation.Customer.DetachPaymentMethod) ephemeralOperation);
        }
        if (ephemeralOperation instanceof EphemeralOperation.Customer.GetPaymentMethods) {
            return createGetPaymentMethodsRunnable(ephemeralKey, (EphemeralOperation.Customer.GetPaymentMethods) ephemeralOperation);
        }
        if (ephemeralOperation instanceof EphemeralOperation.Customer.UpdateDefaultSource) {
            return createSetCustomerSourceDefaultRunnable(ephemeralKey, (EphemeralOperation.Customer.UpdateDefaultSource) ephemeralOperation);
        }
        if (ephemeralOperation instanceof EphemeralOperation.Customer.UpdateShipping) {
            return createSetCustomerShippingInformationRunnable(ephemeralKey, (EphemeralOperation.Customer.UpdateShipping) ephemeralOperation);
        }
        return null;
    }

    private final Runnable createAddCustomerSourceRunnable(EphemeralKey ephemeralKey, EphemeralOperation.Customer.AddSource addSource) {
        return new CustomerSessionRunnableFactory$createAddCustomerSourceRunnable$1(this, ephemeralKey, addSource, this.handler, ResultType.SourceRetrieved, addSource.getId$stripe_release());
    }

    private final Runnable createDeleteCustomerSourceRunnable(EphemeralKey ephemeralKey, EphemeralOperation.Customer.DeleteSource deleteSource) {
        return new CustomerSessionRunnableFactory$createDeleteCustomerSourceRunnable$1(this, ephemeralKey, deleteSource, this.handler, ResultType.SourceRetrieved, deleteSource.getId$stripe_release());
    }

    private final Runnable createAttachPaymentMethodRunnable(EphemeralKey ephemeralKey, EphemeralOperation.Customer.AttachPaymentMethod attachPaymentMethod) {
        return new CustomerSessionRunnableFactory$createAttachPaymentMethodRunnable$1(this, ephemeralKey, attachPaymentMethod, this.handler, ResultType.PaymentMethod, attachPaymentMethod.getId$stripe_release());
    }

    private final Runnable createDetachPaymentMethodRunnable(EphemeralKey ephemeralKey, EphemeralOperation.Customer.DetachPaymentMethod detachPaymentMethod) {
        return new CustomerSessionRunnableFactory$createDetachPaymentMethodRunnable$1(this, detachPaymentMethod, ephemeralKey, this.handler, ResultType.PaymentMethod, detachPaymentMethod.getId$stripe_release());
    }

    private final Runnable createGetPaymentMethodsRunnable(EphemeralKey ephemeralKey, EphemeralOperation.Customer.GetPaymentMethods getPaymentMethods) {
        return new CustomerSessionRunnableFactory$createGetPaymentMethodsRunnable$1(this, ephemeralKey, getPaymentMethods, this.handler, ResultType.PaymentMethods, getPaymentMethods.getId$stripe_release());
    }

    private final Runnable createSetCustomerSourceDefaultRunnable(EphemeralKey ephemeralKey, EphemeralOperation.Customer.UpdateDefaultSource updateDefaultSource) {
        return new CustomerSessionRunnableFactory$createSetCustomerSourceDefaultRunnable$1(this, ephemeralKey, updateDefaultSource, this.handler, ResultType.CustomerRetrieved, updateDefaultSource.getId$stripe_release());
    }

    private final Runnable createSetCustomerShippingInformationRunnable(EphemeralKey ephemeralKey, EphemeralOperation.Customer.UpdateShipping updateShipping) {
        return new CustomerSessionRunnableFactory$createSetCustomerShippingInformationRunnable$1(this, ephemeralKey, updateShipping, this.handler, ResultType.ShippingInfo, updateShipping.getId$stripe_release());
    }

    private final Runnable createUpdateCustomerRunnable(EphemeralKey ephemeralKey, EphemeralOperation.RetrieveKey retrieveKey) {
        return new CustomerSessionRunnableFactory$createUpdateCustomerRunnable$1(this, ephemeralKey, retrieveKey, this.handler, ResultType.CustomerRetrieved, retrieveKey.getId$stripe_release());
    }

    /* access modifiers changed from: private */
    public final Customer retrieveCustomerWithKey(EphemeralKey ephemeralKey, Set<String> set) throws StripeException {
        return this.stripeRepository.retrieveCustomer(ephemeralKey.getObjectId$stripe_release(), set, new ApiRequest.Options(ephemeralKey.getSecret(), this.stripeAccountId, (String) null, 4, (DefaultConstructorMarker) null));
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\b\"\u0018\u0000*\u0004\b\u0000\u0010\u00012\u00020\u0002B\u001d\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\b¢\u0006\u0002\u0010\tJ\u0011\u0010\n\u001a\u0004\u0018\u00018\u0000H ¢\u0006\u0004\b\u000b\u0010\fJ\b\u0010\r\u001a\u00020\u000eH\u0016J\u0010\u0010\u000f\u001a\u00020\u000e2\u0006\u0010\u0010\u001a\u00020\u0011H\u0002J\u0017\u0010\u0012\u001a\u00020\u000e2\b\u0010\u0013\u001a\u0004\u0018\u00018\u0000H\u0002¢\u0006\u0002\u0010\u0014R\u000e\u0010\u0003\u001a\u00020\u0004X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\bX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0015"}, d2 = {"Lcom/stripe/android/CustomerSessionRunnableFactory$CustomerSessionRunnable;", "T", "Ljava/lang/Runnable;", "handler", "Landroid/os/Handler;", "resultType", "Lcom/stripe/android/CustomerSessionRunnableFactory$ResultType;", "operationId", "", "(Landroid/os/Handler;Lcom/stripe/android/CustomerSessionRunnableFactory$ResultType;Ljava/lang/String;)V", "createMessageObject", "createMessageObject$stripe_release", "()Ljava/lang/Object;", "run", "", "sendErrorMessage", "stripeEx", "Lcom/stripe/android/exception/StripeException;", "sendMessage", "messageObject", "(Ljava/lang/Object;)V", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: CustomerSessionRunnableFactory.kt */
    private static abstract class CustomerSessionRunnable<T> implements Runnable {
        private final Handler handler;
        private final String operationId;
        private final ResultType resultType;

        public abstract T createMessageObject$stripe_release() throws StripeException;

        public CustomerSessionRunnable(Handler handler2, ResultType resultType2, String str) {
            Intrinsics.checkParameterIsNotNull(handler2, "handler");
            Intrinsics.checkParameterIsNotNull(resultType2, "resultType");
            Intrinsics.checkParameterIsNotNull(str, "operationId");
            this.handler = handler2;
            this.resultType = resultType2;
            this.operationId = str;
        }

        public void run() {
            try {
                sendMessage(createMessageObject$stripe_release());
            } catch (StripeException e) {
                sendErrorMessage(e);
            }
        }

        private final void sendMessage(T t) {
            Handler handler2 = this.handler;
            handler2.sendMessage(handler2.obtainMessage(this.resultType.ordinal(), Pair.create(this.operationId, t)));
        }

        private final void sendErrorMessage(StripeException stripeException) {
            Handler handler2 = this.handler;
            handler2.sendMessage(handler2.obtainMessage(ResultType.Error.ordinal(), Pair.create(this.operationId, stripeException)));
        }
    }
}
