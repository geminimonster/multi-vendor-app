package com.stripe.android.utils;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Collection;
import java.util.Set;
import kotlin.Metadata;
import kotlin.Result;
import kotlin.ResultKt;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u001e\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\"\n\u0002\b\u0002\bÀ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J$\u0010\u0003\u001a\u0004\u0018\u00010\u00042\n\u0010\u0005\u001a\u0006\u0012\u0002\b\u00030\u00062\f\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\t0\bH\u0007J$\u0010\n\u001a\u0004\u0018\u00010\u000b2\n\u0010\u0005\u001a\u0006\u0012\u0002\b\u00030\u00062\f\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\t0\bH\u0007J,\u0010\f\u001a\u0004\u0018\u00010\u00012\n\u0010\u0005\u001a\u0006\u0012\u0002\b\u00030\u00062\f\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\t0\r2\u0006\u0010\u000e\u001a\u00020\u0001H\u0007¨\u0006\u000f"}, d2 = {"Lcom/stripe/android/utils/ClassUtils;", "", "()V", "findField", "Ljava/lang/reflect/Field;", "clazz", "Ljava/lang/Class;", "whitelist", "", "", "findMethod", "Ljava/lang/reflect/Method;", "getInternalObject", "", "obj", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: ClassUtils.kt */
public final class ClassUtils {
    public static final ClassUtils INSTANCE = new ClassUtils();

    private ClassUtils() {
    }

    @JvmStatic
    public static final Object getInternalObject(Class<?> cls, Set<String> set, Object obj) {
        Object obj2;
        Intrinsics.checkParameterIsNotNull(cls, "clazz");
        Intrinsics.checkParameterIsNotNull(set, "whitelist");
        Intrinsics.checkParameterIsNotNull(obj, "obj");
        Field findField = findField(cls, set);
        if (findField == null) {
            return null;
        }
        try {
            Result.Companion companion = Result.Companion;
            obj2 = Result.m4constructorimpl(findField.get(obj));
        } catch (Throwable th) {
            Result.Companion companion2 = Result.Companion;
            obj2 = Result.m4constructorimpl(ResultKt.createFailure(th));
        }
        if (Result.m10isFailureimpl(obj2)) {
            return null;
        }
        return obj2;
    }

    @JvmStatic
    public static final Field findField(Class<?> cls, Collection<String> collection) {
        Field field;
        Intrinsics.checkParameterIsNotNull(cls, "clazz");
        Intrinsics.checkParameterIsNotNull(collection, "whitelist");
        Field[] declaredFields = cls.getDeclaredFields();
        Intrinsics.checkExpressionValueIsNotNull(declaredFields, "clazz.declaredFields");
        int length = declaredFields.length;
        int i = 0;
        while (true) {
            if (i >= length) {
                field = null;
                break;
            }
            field = declaredFields[i];
            Intrinsics.checkExpressionValueIsNotNull(field, "it");
            if (collection.contains(field.getName())) {
                break;
            }
            i++;
        }
        if (field == null) {
            return null;
        }
        field.setAccessible(true);
        return field;
    }

    @JvmStatic
    public static final Method findMethod(Class<?> cls, Collection<String> collection) {
        Intrinsics.checkParameterIsNotNull(cls, "clazz");
        Intrinsics.checkParameterIsNotNull(collection, "whitelist");
        Method[] declaredMethods = cls.getDeclaredMethods();
        Intrinsics.checkExpressionValueIsNotNull(declaredMethods, "clazz.declaredMethods");
        for (Method method : declaredMethods) {
            Intrinsics.checkExpressionValueIsNotNull(method, "it");
            if (collection.contains(method.getName())) {
                return method;
            }
        }
        return null;
    }
}
