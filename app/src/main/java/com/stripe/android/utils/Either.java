package com.stripe.android.utils;

import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u0000*\u0004\b\u0000\u0010\u0001*\u0004\b\u0001\u0010\u00022\u00020\u0003:\u0002\u0005\u0006B\u0007\b\u0002¢\u0006\u0002\u0010\u0004\u0001\u0002\u0007\b¨\u0006\t"}, d2 = {"Lcom/stripe/android/utils/Either;", "A", "B", "", "()V", "Left", "Right", "Lcom/stripe/android/utils/Either$Left;", "Lcom/stripe/android/utils/Either$Right;", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: Either.kt */
public abstract class Either<A, B> {

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u0000*\u0004\b\u0002\u0010\u0001*\u0004\b\u0003\u0010\u00022\u000e\u0012\u0004\u0012\u0002H\u0001\u0012\u0004\u0012\u0002H\u00020\u0003B\r\u0012\u0006\u0010\u0004\u001a\u00028\u0002¢\u0006\u0002\u0010\u0005J\u000e\u0010\t\u001a\u00028\u0002HÆ\u0003¢\u0006\u0002\u0010\u0007J$\u0010\n\u001a\u000e\u0012\u0004\u0012\u00028\u0002\u0012\u0004\u0012\u00028\u00030\u00002\b\b\u0002\u0010\u0004\u001a\u00028\u0002HÆ\u0001¢\u0006\u0002\u0010\u000bJ\u0013\u0010\f\u001a\u00020\r2\b\u0010\u000e\u001a\u0004\u0018\u00010\u000fHÖ\u0003J\t\u0010\u0010\u001a\u00020\u0011HÖ\u0001J\t\u0010\u0012\u001a\u00020\u0013HÖ\u0001R\u0013\u0010\u0004\u001a\u00028\u0002¢\u0006\n\n\u0002\u0010\b\u001a\u0004\b\u0006\u0010\u0007¨\u0006\u0014"}, d2 = {"Lcom/stripe/android/utils/Either$Left;", "A", "B", "Lcom/stripe/android/utils/Either;", "left", "(Ljava/lang/Object;)V", "getLeft", "()Ljava/lang/Object;", "Ljava/lang/Object;", "component1", "copy", "(Ljava/lang/Object;)Lcom/stripe/android/utils/Either$Left;", "equals", "", "other", "", "hashCode", "", "toString", "", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: Either.kt */
    public static final class Left<A, B> extends Either<A, B> {
        private final A left;

        public static /* synthetic */ Left copy$default(Left left2, A a2, int i, Object obj) {
            if ((i & 1) != 0) {
                a2 = left2.left;
            }
            return left2.copy(a2);
        }

        public final A component1() {
            return this.left;
        }

        public final Left<A, B> copy(A a2) {
            return new Left<>(a2);
        }

        public boolean equals(Object obj) {
            if (this != obj) {
                return (obj instanceof Left) && Intrinsics.areEqual((Object) this.left, (Object) ((Left) obj).left);
            }
            return true;
        }

        public int hashCode() {
            A a2 = this.left;
            if (a2 != null) {
                return a2.hashCode();
            }
            return 0;
        }

        public String toString() {
            return "Left(left=" + this.left + ")";
        }

        public Left(A a2) {
            super((DefaultConstructorMarker) null);
            this.left = a2;
        }

        public final A getLeft() {
            return this.left;
        }
    }

    private Either() {
    }

    public /* synthetic */ Either(DefaultConstructorMarker defaultConstructorMarker) {
        this();
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u0000*\u0004\b\u0002\u0010\u0001*\u0004\b\u0003\u0010\u00022\u000e\u0012\u0004\u0012\u0002H\u0001\u0012\u0004\u0012\u0002H\u00020\u0003B\r\u0012\u0006\u0010\u0004\u001a\u00028\u0003¢\u0006\u0002\u0010\u0005J\u000e\u0010\t\u001a\u00028\u0003HÆ\u0003¢\u0006\u0002\u0010\u0007J$\u0010\n\u001a\u000e\u0012\u0004\u0012\u00028\u0002\u0012\u0004\u0012\u00028\u00030\u00002\b\b\u0002\u0010\u0004\u001a\u00028\u0003HÆ\u0001¢\u0006\u0002\u0010\u000bJ\u0013\u0010\f\u001a\u00020\r2\b\u0010\u000e\u001a\u0004\u0018\u00010\u000fHÖ\u0003J\t\u0010\u0010\u001a\u00020\u0011HÖ\u0001J\t\u0010\u0012\u001a\u00020\u0013HÖ\u0001R\u0013\u0010\u0004\u001a\u00028\u0003¢\u0006\n\n\u0002\u0010\b\u001a\u0004\b\u0006\u0010\u0007¨\u0006\u0014"}, d2 = {"Lcom/stripe/android/utils/Either$Right;", "A", "B", "Lcom/stripe/android/utils/Either;", "right", "(Ljava/lang/Object;)V", "getRight", "()Ljava/lang/Object;", "Ljava/lang/Object;", "component1", "copy", "(Ljava/lang/Object;)Lcom/stripe/android/utils/Either$Right;", "equals", "", "other", "", "hashCode", "", "toString", "", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: Either.kt */
    public static final class Right<A, B> extends Either<A, B> {
        private final B right;

        public static /* synthetic */ Right copy$default(Right right2, B b, int i, Object obj) {
            if ((i & 1) != 0) {
                b = right2.right;
            }
            return right2.copy(b);
        }

        public final B component1() {
            return this.right;
        }

        public final Right<A, B> copy(B b) {
            return new Right<>(b);
        }

        public boolean equals(Object obj) {
            if (this != obj) {
                return (obj instanceof Right) && Intrinsics.areEqual((Object) this.right, (Object) ((Right) obj).right);
            }
            return true;
        }

        public int hashCode() {
            B b = this.right;
            if (b != null) {
                return b.hashCode();
            }
            return 0;
        }

        public String toString() {
            return "Right(right=" + this.right + ")";
        }

        public Right(B b) {
            super((DefaultConstructorMarker) null);
            this.right = b;
        }

        public final B getRight() {
            return this.right;
        }
    }
}
