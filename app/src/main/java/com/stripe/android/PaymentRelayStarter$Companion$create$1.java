package com.stripe.android;

import com.stripe.android.PaymentController;
import com.stripe.android.PaymentRelayStarter;
import com.stripe.android.model.StripeIntent;
import com.stripe.android.view.AuthActivityStarter;
import com.stripe.android.view.PaymentRelayActivity;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0017\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000*\u0001\u0000\b\n\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\u0016¨\u0006\u0006"}, d2 = {"com/stripe/android/PaymentRelayStarter$Companion$create$1", "Lcom/stripe/android/PaymentRelayStarter;", "start", "", "args", "Lcom/stripe/android/PaymentRelayStarter$Args;", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: PaymentRelayStarter.kt */
public final class PaymentRelayStarter$Companion$create$1 implements PaymentRelayStarter {
    final /* synthetic */ AuthActivityStarter.Host $host;
    final /* synthetic */ int $requestCode;

    PaymentRelayStarter$Companion$create$1(AuthActivityStarter.Host host, int i) {
        this.$host = host;
        this.$requestCode = i;
    }

    public void start(PaymentRelayStarter.Args args) {
        Intrinsics.checkParameterIsNotNull(args, "args");
        StripeIntent stripeIntent = args.getStripeIntent();
        this.$host.startActivityForResult$stripe_release(PaymentRelayActivity.class, new PaymentController.Result(stripeIntent != null ? stripeIntent.getClientSecret() : null, 0, args.getException(), false, (String) null, args.getSource(), args.getStripeAccountId(), 26, (DefaultConstructorMarker) null).toBundle(), this.$requestCode);
    }
}
