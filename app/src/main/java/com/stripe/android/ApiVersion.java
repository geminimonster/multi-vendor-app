package com.stripe.android;

import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0007\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\b\b\u0018\u0000 \u00102\u00020\u0001:\u0001\u0010B\u000f\b\u0000\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u000e\u0010\u0007\u001a\u00020\u0003HÀ\u0003¢\u0006\u0002\b\bJ\u0013\u0010\t\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u0003HÆ\u0001J\u0013\u0010\n\u001a\u00020\u000b2\b\u0010\f\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\r\u001a\u00020\u000eHÖ\u0001J\b\u0010\u000f\u001a\u00020\u0003H\u0016R\u0014\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006¨\u0006\u0011"}, d2 = {"Lcom/stripe/android/ApiVersion;", "", "code", "", "(Ljava/lang/String;)V", "getCode$stripe_release", "()Ljava/lang/String;", "component1", "component1$stripe_release", "copy", "equals", "", "other", "hashCode", "", "toString", "Companion", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: ApiVersion.kt */
public final class ApiVersion {
    private static final String API_VERSION_CODE = "2020-03-02";
    public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
    /* access modifiers changed from: private */
    public static final ApiVersion INSTANCE = new ApiVersion(API_VERSION_CODE);
    private final String code;

    public static /* synthetic */ ApiVersion copy$default(ApiVersion apiVersion, String str, int i, Object obj) {
        if ((i & 1) != 0) {
            str = apiVersion.code;
        }
        return apiVersion.copy(str);
    }

    public final String component1$stripe_release() {
        return this.code;
    }

    public final ApiVersion copy(String str) {
        Intrinsics.checkParameterIsNotNull(str, "code");
        return new ApiVersion(str);
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            return (obj instanceof ApiVersion) && Intrinsics.areEqual((Object) this.code, (Object) ((ApiVersion) obj).code);
        }
        return true;
    }

    public int hashCode() {
        String str = this.code;
        if (str != null) {
            return str.hashCode();
        }
        return 0;
    }

    public ApiVersion(String str) {
        Intrinsics.checkParameterIsNotNull(str, "code");
        this.code = str;
    }

    public final String getCode$stripe_release() {
        return this.code;
    }

    public String toString() {
        return this.code;
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\r\u0010\u0007\u001a\u00020\u0006H\u0000¢\u0006\u0002\b\bR\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0004¢\u0006\u0002\n\u0000¨\u0006\t"}, d2 = {"Lcom/stripe/android/ApiVersion$Companion;", "", "()V", "API_VERSION_CODE", "", "INSTANCE", "Lcom/stripe/android/ApiVersion;", "get", "get$stripe_release", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: ApiVersion.kt */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        public final /* synthetic */ ApiVersion get$stripe_release() {
            return ApiVersion.INSTANCE;
        }
    }
}
