package com.stripe.android;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\b\u0002*\u0001\u0000\b\n\u0018\u00002\n\u0012\u0006\u0012\u0004\u0018\u00010\u00020\u0001J\u0012\u0010\u0003\u001a\u00020\u00042\b\u0010\u0005\u001a\u0004\u0018\u00010\u0002H\u0016¨\u0006\u0006¸\u0006\u0000"}, d2 = {"com/stripe/android/FingerprintDataRepository$Default$refresh$1$2$1", "Landroidx/lifecycle/Observer;", "Lcom/stripe/android/FingerprintData;", "onChanged", "", "fingerprintData", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: FingerprintDataRepository.kt */
public final class FingerprintDataRepository$Default$refresh$1$invokeSuspend$$inlined$let$lambda$1 implements Observer<FingerprintData> {
    final /* synthetic */ LiveData $liveData;
    final /* synthetic */ FingerprintDataRepository$Default$refresh$1 this$0;

    FingerprintDataRepository$Default$refresh$1$invokeSuspend$$inlined$let$lambda$1(LiveData liveData, FingerprintDataRepository$Default$refresh$1 fingerprintDataRepository$Default$refresh$1) {
        this.$liveData = liveData;
        this.this$0 = fingerprintDataRepository$Default$refresh$1;
    }

    public void onChanged(FingerprintData fingerprintData) {
        if ((!Intrinsics.areEqual((Object) this.this$0.this$0.cachedFingerprintData, (Object) fingerprintData)) && fingerprintData != null) {
            this.this$0.this$0.save(fingerprintData);
        }
        this.$liveData.removeObserver(this);
    }
}
