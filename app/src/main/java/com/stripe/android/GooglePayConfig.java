package com.stripe.android;

import android.content.Context;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.json.JSONException;
import org.json.JSONObject;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\u00020\u0001B\u001b\b\u0017\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005¢\u0006\u0002\u0010\u0006B\u001b\b\u0007\u0012\u0006\u0010\u0007\u001a\u00020\u0005\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005¢\u0006\u0002\u0010\bR\u000e\u0010\t\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u0004\u001a\u0004\u0018\u00010\u0005X\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\n\u001a\u00020\u00058BX\u0004¢\u0006\u0006\u001a\u0004\b\u000b\u0010\fR\u000e\u0010\u0007\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000R\u0011\u0010\r\u001a\u00020\u000e8F¢\u0006\u0006\u001a\u0004\b\u000f\u0010\u0010¨\u0006\u0011"}, d2 = {"Lcom/stripe/android/GooglePayConfig;", "", "context", "Landroid/content/Context;", "connectedAccountId", "", "(Landroid/content/Context;Ljava/lang/String;)V", "publishableKey", "(Ljava/lang/String;Ljava/lang/String;)V", "apiVersion", "key", "getKey", "()Ljava/lang/String;", "tokenizationSpecification", "Lorg/json/JSONObject;", "getTokenizationSpecification", "()Lorg/json/JSONObject;", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: GooglePayConfig.kt */
public final class GooglePayConfig {
    private final String apiVersion;
    private final String connectedAccountId;
    private final String publishableKey;

    public GooglePayConfig(Context context) {
        this(context, (String) null, 2, (DefaultConstructorMarker) null);
    }

    public GooglePayConfig(String str) {
        this(str, (String) null, 2, (DefaultConstructorMarker) null);
    }

    public GooglePayConfig(String str, String str2) {
        Intrinsics.checkParameterIsNotNull(str, "publishableKey");
        this.connectedAccountId = str2;
        this.publishableKey = ApiKeyValidator.Companion.get$stripe_release().requireValid(str);
        this.apiVersion = ApiVersion.Companion.get$stripe_release().getCode$stripe_release();
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ GooglePayConfig(String str, String str2, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(str, (i & 2) != 0 ? null : str2);
    }

    public final JSONObject getTokenizationSpecification() throws JSONException {
        JSONObject put = new JSONObject().put("type", "PAYMENT_GATEWAY").put("parameters", new JSONObject().put("gateway", "stripe").put("stripe:version", this.apiVersion).put("stripe:publishableKey", getKey()));
        Intrinsics.checkExpressionValueIsNotNull(put, "JSONObject()\n           …eKey\", key)\n            )");
        return put;
    }

    private final String getKey() {
        String str = this.connectedAccountId;
        if (str != null) {
            String str2 = this.publishableKey + '/' + str;
            if (str2 != null) {
                return str2;
            }
        }
        return this.publishableKey;
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ GooglePayConfig(Context context, String str, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(context, (i & 2) != 0 ? null : str);
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public GooglePayConfig(Context context, String str) {
        this(PaymentConfiguration.Companion.getInstance(context).getPublishableKey(), str);
        Intrinsics.checkParameterIsNotNull(context, "context");
    }
}
