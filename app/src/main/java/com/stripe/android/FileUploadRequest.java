package com.stripe.android;

import com.stripe.android.ApiRequest;
import com.stripe.android.RequestHeadersFactory;
import com.stripe.android.StripeRequest;
import com.stripe.android.model.StripeFileParams;
import java.io.FileInputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.URLConnection;
import java.util.Locale;
import java.util.Map;
import kotlin.Metadata;
import kotlin.io.ByteStreamsKt;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.LongCompanionObject;
import kotlin.random.Random;
import kotlin.text.StringsKt;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000^\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\f\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010$\n\u0002\b\u0006\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u0000\u0018\u0000 32\u00020\u0001:\u00013BA\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u0012\u0014\b\u0002\u0010\b\u001a\u000e\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\n0\t\u0012\b\b\u0002\u0010\u000b\u001a\u00020\n¢\u0006\u0002\u0010\fJ\u0015\u0010)\u001a\u00020*2\u0006\u0010+\u001a\u00020,H\u0010¢\u0006\u0002\b-J\u0010\u0010.\u001a\u00020*2\u0006\u0010+\u001a\u00020,H\u0002J\u0018\u0010/\u001a\u00020*2\u0006\u00100\u001a\u0002012\u0006\u00102\u001a\u00020\nH\u0002R\u0014\u0010\r\u001a\u00020\nXD¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u000fR\u000e\u0010\u000b\u001a\u00020\nX\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\u0010\u001a\u00020\n8PX\u0004¢\u0006\u0006\u001a\u0004\b\u0011\u0010\u000fR\u001a\u0010\u0012\u001a\u00020\n8@X\u0004¢\u0006\f\u0012\u0004\b\u0013\u0010\u0014\u001a\u0004\b\u0015\u0010\u000fR\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\u0016\u001a\u00020\u0017X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0018\u0010\u0019R\u0014\u0010\u001a\u001a\u00020\u001bX\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u001c\u0010\u001dR\u0014\u0010\u001e\u001a\u00020\u001fX\u0004¢\u0006\b\n\u0000\u001a\u0004\b \u0010!R \u0010\"\u001a\u000e\u0012\u0004\u0012\u00020\n\u0012\u0002\b\u0003\u0018\u00010#X\u0004¢\u0006\b\n\u0000\u001a\u0004\b$\u0010%R\u001a\u0010&\u001a\u00020\n8@X\u0004¢\u0006\f\u0012\u0004\b'\u0010\u0014\u001a\u0004\b(\u0010\u000f¨\u00064"}, d2 = {"Lcom/stripe/android/FileUploadRequest;", "Lcom/stripe/android/StripeRequest;", "fileParams", "Lcom/stripe/android/model/StripeFileParams;", "options", "Lcom/stripe/android/ApiRequest$Options;", "appInfo", "Lcom/stripe/android/AppInfo;", "systemPropertySupplier", "Lkotlin/Function1;", "", "boundary", "(Lcom/stripe/android/model/StripeFileParams;Lcom/stripe/android/ApiRequest$Options;Lcom/stripe/android/AppInfo;Lkotlin/jvm/functions/Function1;Ljava/lang/String;)V", "baseUrl", "getBaseUrl", "()Ljava/lang/String;", "contentType", "getContentType$stripe_release", "fileMetadata", "fileMetadata$annotations", "()V", "getFileMetadata$stripe_release", "headersFactory", "Lcom/stripe/android/RequestHeadersFactory;", "getHeadersFactory", "()Lcom/stripe/android/RequestHeadersFactory;", "method", "Lcom/stripe/android/StripeRequest$Method;", "getMethod", "()Lcom/stripe/android/StripeRequest$Method;", "mimeType", "Lcom/stripe/android/StripeRequest$MimeType;", "getMimeType", "()Lcom/stripe/android/StripeRequest$MimeType;", "params", "", "getParams", "()Ljava/util/Map;", "purposeContents", "purposeContents$annotations", "getPurposeContents$stripe_release", "writeBody", "", "outputStream", "Ljava/io/OutputStream;", "writeBody$stripe_release", "writeFile", "writeString", "writer", "Ljava/io/PrintWriter;", "contents", "Companion", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: FileUploadRequest.kt */
public final class FileUploadRequest extends StripeRequest {
    public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
    private static final String LINE_BREAK = "\r\n";
    private final String baseUrl;
    private final String boundary;
    private final StripeFileParams fileParams;
    private final RequestHeadersFactory headersFactory;
    private final StripeRequest.Method method;
    private final StripeRequest.MimeType mimeType;
    private final Map<String, ?> params;

    public static /* synthetic */ void fileMetadata$annotations() {
    }

    public static /* synthetic */ void purposeContents$annotations() {
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ FileUploadRequest(StripeFileParams stripeFileParams, ApiRequest.Options options, AppInfo appInfo, Function1<String, String> function1, String str, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(stripeFileParams, options, (i & 4) != 0 ? null : appInfo, (i & 8) != 0 ? StripeRequest.Companion.getDEFAULT_SYSTEM_PROPERTY_SUPPLIER$stripe_release() : function1, (i & 16) != 0 ? Companion.createBoundary() : str);
    }

    public FileUploadRequest(StripeFileParams stripeFileParams, ApiRequest.Options options, AppInfo appInfo, Function1<? super String, String> function1, String str) {
        Intrinsics.checkParameterIsNotNull(stripeFileParams, "fileParams");
        Intrinsics.checkParameterIsNotNull(options, "options");
        Intrinsics.checkParameterIsNotNull(function1, "systemPropertySupplier");
        Intrinsics.checkParameterIsNotNull(str, "boundary");
        this.fileParams = stripeFileParams;
        this.boundary = str;
        this.method = StripeRequest.Method.POST;
        this.baseUrl = "https://files.stripe.com/v1/files";
        this.mimeType = StripeRequest.MimeType.MultipartForm;
        this.headersFactory = new RequestHeadersFactory.Api(options, appInfo, (Locale) null, function1, (String) null, (String) null, 52, (DefaultConstructorMarker) null);
    }

    public StripeRequest.Method getMethod() {
        return this.method;
    }

    public String getBaseUrl() {
        return this.baseUrl;
    }

    public Map<String, ?> getParams() {
        return this.params;
    }

    public StripeRequest.MimeType getMimeType() {
        return this.mimeType;
    }

    public RequestHeadersFactory getHeadersFactory() {
        return this.headersFactory;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0063, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:?, code lost:
        kotlin.io.CloseableKt.closeFinally(r5, r8);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0067, code lost:
        throw r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x006a, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x006b, code lost:
        kotlin.io.CloseableKt.closeFinally(r2, r8);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x006e, code lost:
        throw r0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void writeBody$stripe_release(java.io.OutputStream r8) {
        /*
            r7 = this;
            java.lang.String r0 = "--"
            java.lang.String r1 = "outputStream"
            kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(r8, r1)
            java.nio.charset.Charset r1 = kotlin.text.Charsets.UTF_8
            java.io.OutputStreamWriter r2 = new java.io.OutputStreamWriter
            r2.<init>(r8, r1)
            java.io.Closeable r2 = (java.io.Closeable) r2
            r1 = 0
            r3 = r1
            java.lang.Throwable r3 = (java.lang.Throwable) r3
            r4 = r2
            java.io.OutputStreamWriter r4 = (java.io.OutputStreamWriter) r4     // Catch:{ all -> 0x0068 }
            java.io.PrintWriter r5 = new java.io.PrintWriter     // Catch:{ all -> 0x0068 }
            java.io.Writer r4 = (java.io.Writer) r4     // Catch:{ all -> 0x0068 }
            r6 = 1
            r5.<init>(r4, r6)     // Catch:{ all -> 0x0068 }
            java.io.Closeable r5 = (java.io.Closeable) r5     // Catch:{ all -> 0x0068 }
            java.lang.Throwable r1 = (java.lang.Throwable) r1     // Catch:{ all -> 0x0068 }
            r4 = r5
            java.io.PrintWriter r4 = (java.io.PrintWriter) r4     // Catch:{ all -> 0x0061 }
            java.lang.String r6 = r7.getPurposeContents$stripe_release()     // Catch:{ all -> 0x0061 }
            r7.writeString(r4, r6)     // Catch:{ all -> 0x0061 }
            java.lang.String r6 = r7.getFileMetadata$stripe_release()     // Catch:{ all -> 0x0061 }
            r7.writeString(r4, r6)     // Catch:{ all -> 0x0061 }
            r7.writeFile(r8)     // Catch:{ all -> 0x0061 }
            java.lang.String r8 = "\r\n"
            r4.write(r8)     // Catch:{ all -> 0x0061 }
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ all -> 0x0061 }
            r8.<init>()     // Catch:{ all -> 0x0061 }
            r8.append(r0)     // Catch:{ all -> 0x0061 }
            java.lang.String r6 = r7.boundary     // Catch:{ all -> 0x0061 }
            r8.append(r6)     // Catch:{ all -> 0x0061 }
            r8.append(r0)     // Catch:{ all -> 0x0061 }
            java.lang.String r8 = r8.toString()     // Catch:{ all -> 0x0061 }
            r4.write(r8)     // Catch:{ all -> 0x0061 }
            r4.flush()     // Catch:{ all -> 0x0061 }
            kotlin.Unit r8 = kotlin.Unit.INSTANCE     // Catch:{ all -> 0x0061 }
            kotlin.io.CloseableKt.closeFinally(r5, r1)     // Catch:{ all -> 0x0068 }
            kotlin.Unit r8 = kotlin.Unit.INSTANCE     // Catch:{ all -> 0x0068 }
            kotlin.io.CloseableKt.closeFinally(r2, r3)
            return
        L_0x0061:
            r8 = move-exception
            throw r8     // Catch:{ all -> 0x0063 }
        L_0x0063:
            r0 = move-exception
            kotlin.io.CloseableKt.closeFinally(r5, r8)     // Catch:{ all -> 0x0068 }
            throw r0     // Catch:{ all -> 0x0068 }
        L_0x0068:
            r8 = move-exception
            throw r8     // Catch:{ all -> 0x006a }
        L_0x006a:
            r0 = move-exception
            kotlin.io.CloseableKt.closeFinally(r2, r8)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.stripe.android.FileUploadRequest.writeBody$stripe_release(java.io.OutputStream):void");
    }

    private final void writeString(PrintWriter printWriter, String str) {
        printWriter.write(StringsKt.replace$default(str, "\n", LINE_BREAK, false, 4, (Object) null));
        printWriter.flush();
    }

    private final void writeFile(OutputStream outputStream) {
        ByteStreamsKt.copyTo$default(new FileInputStream(this.fileParams.getFile$stripe_release()), outputStream, 0, 2, (Object) null);
    }

    public String getContentType$stripe_release() {
        return getMimeType().getCode() + "; boundary=" + this.boundary;
    }

    public final String getFileMetadata$stripe_release() {
        String name = this.fileParams.getFile$stripe_release().getName();
        String guessContentTypeFromName = URLConnection.guessContentTypeFromName(name);
        return StringsKt.trimIndent("\n                --" + this.boundary + "\n                Content-Disposition: form-data; name=\"file\"; filename=\"" + name + "\"\n                Content-Type: " + guessContentTypeFromName + "\n                Content-Transfer-Encoding: binary\n\n\n            ");
    }

    public final String getPurposeContents$stripe_release() {
        return StringsKt.trimIndent("\n                --" + this.boundary + "\n                Content-Disposition: form-data; name=\"purpose\"\n\n                " + this.fileParams.getPurpose$stripe_release().getCode$stripe_release() + "\n\n            ");
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\b\u0010\u0005\u001a\u00020\u0004H\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\u0006"}, d2 = {"Lcom/stripe/android/FileUploadRequest$Companion;", "", "()V", "LINE_BREAK", "", "createBoundary", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: FileUploadRequest.kt */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        /* access modifiers changed from: private */
        public final String createBoundary() {
            return String.valueOf(Random.Default.nextLong(0, LongCompanionObject.MAX_VALUE));
        }
    }
}
