package com.stripe.android;

import com.stripe.android.ApiRequest;
import com.stripe.android.model.StripeIntent;
import com.stripe.android.view.AuthActivityStarter;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000!\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003*\u0001\u0000\b\n\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001J\u0014\u0010\u0003\u001a\u00020\u00042\n\u0010\u0005\u001a\u00060\u0006j\u0002`\u0007H\u0016J\u0010\u0010\b\u001a\u00020\u00042\u0006\u0010\t\u001a\u00020\u0002H\u0016¨\u0006\n"}, d2 = {"com/stripe/android/StripePaymentController$startAuth$1", "Lcom/stripe/android/ApiResultCallback;", "Lcom/stripe/android/model/StripeIntent;", "onError", "", "e", "Ljava/lang/Exception;", "Lkotlin/Exception;", "onSuccess", "result", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: StripePaymentController.kt */
public final class StripePaymentController$startAuth$1 implements ApiResultCallback<StripeIntent> {
    final /* synthetic */ AuthActivityStarter.Host $host;
    final /* synthetic */ ApiRequest.Options $requestOptions;
    final /* synthetic */ StripePaymentController this$0;

    StripePaymentController$startAuth$1(StripePaymentController stripePaymentController, AuthActivityStarter.Host host, ApiRequest.Options options) {
        this.this$0 = stripePaymentController;
        this.$host = host;
        this.$requestOptions = options;
    }

    public void onSuccess(StripeIntent stripeIntent) {
        Intrinsics.checkParameterIsNotNull(stripeIntent, "result");
        this.this$0.handleNextAction(this.$host, stripeIntent, this.$requestOptions);
    }

    public void onError(Exception exc) {
        Intrinsics.checkParameterIsNotNull(exc, "e");
        StripePaymentController.Companion.handleError(this.$host, StripePaymentController.PAYMENT_REQUEST_CODE, exc);
    }
}
