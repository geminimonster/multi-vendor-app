package com.stripe.android;

import androidx.lifecycle.LiveDataScope;
import com.stripe.android.FingerprintRequestExecutor;
import kotlin.Metadata;
import kotlin.Result;
import kotlin.ResultKt;
import kotlin.Unit;
import kotlin.coroutines.Continuation;
import kotlin.coroutines.intrinsics.IntrinsicsKt;
import kotlin.coroutines.jvm.internal.DebugMetadata;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\u0010\u0000\u001a\u00020\u0001*\n\u0012\u0006\u0012\u0004\u0018\u00010\u00030\u0002H@¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"<anonymous>", "", "Landroidx/lifecycle/LiveDataScope;", "Lcom/stripe/android/FingerprintData;", "invoke", "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;"}, k = 3, mv = {1, 1, 16})
@DebugMetadata(c = "com.stripe.android.FingerprintRequestExecutor$Default$execute$1", f = "FingerprintRequestExecutor.kt", i = {0}, l = {29}, m = "invokeSuspend", n = {"$this$liveData"}, s = {"L$0"})
/* compiled from: FingerprintRequestExecutor.kt */
final class FingerprintRequestExecutor$Default$execute$1 extends SuspendLambda implements Function2<LiveDataScope<FingerprintData>, Continuation<? super Unit>, Object> {
    final /* synthetic */ FingerprintRequest $request;
    Object L$0;
    int label;
    private LiveDataScope p$;
    final /* synthetic */ FingerprintRequestExecutor.Default this$0;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    FingerprintRequestExecutor$Default$execute$1(FingerprintRequestExecutor.Default defaultR, FingerprintRequest fingerprintRequest, Continuation continuation) {
        super(2, continuation);
        this.this$0 = defaultR;
        this.$request = fingerprintRequest;
    }

    public final Continuation<Unit> create(Object obj, Continuation<?> continuation) {
        Intrinsics.checkParameterIsNotNull(continuation, "completion");
        FingerprintRequestExecutor$Default$execute$1 fingerprintRequestExecutor$Default$execute$1 = new FingerprintRequestExecutor$Default$execute$1(this.this$0, this.$request, continuation);
        fingerprintRequestExecutor$Default$execute$1.p$ = (LiveDataScope) obj;
        return fingerprintRequestExecutor$Default$execute$1;
    }

    public final Object invoke(Object obj, Object obj2) {
        return ((FingerprintRequestExecutor$Default$execute$1) create(obj, (Continuation) obj2)).invokeSuspend(Unit.INSTANCE);
    }

    public final Object invokeSuspend(Object obj) {
        Object obj2;
        Object coroutine_suspended = IntrinsicsKt.getCOROUTINE_SUSPENDED();
        int i = this.label;
        if (i == 0) {
            ResultKt.throwOnFailure(obj);
            LiveDataScope liveDataScope = this.p$;
            try {
                Result.Companion companion = Result.Companion;
                obj2 = Result.m4constructorimpl(this.this$0.executeInternal(this.$request));
            } catch (Throwable th) {
                Result.Companion companion2 = Result.Companion;
                obj2 = Result.m4constructorimpl(ResultKt.createFailure(th));
            }
            if (Result.m10isFailureimpl(obj2)) {
                obj2 = null;
            }
            this.L$0 = liveDataScope;
            this.label = 1;
            if (liveDataScope.emit(obj2, this) == coroutine_suspended) {
                return coroutine_suspended;
            }
        } else if (i == 1) {
            LiveDataScope liveDataScope2 = (LiveDataScope) this.L$0;
            ResultKt.throwOnFailure(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return Unit.INSTANCE;
    }
}
