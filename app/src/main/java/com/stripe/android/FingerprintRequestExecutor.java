package com.stripe.android;

import androidx.lifecycle.CoroutineLiveDataKt;
import androidx.lifecycle.LiveData;
import com.facebook.share.internal.ShareConstants;
import com.stripe.android.ConnectionFactory;
import kotlin.Metadata;
import kotlin.coroutines.Continuation;
import kotlin.coroutines.CoroutineContext;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.Intrinsics;
import kotlinx.coroutines.CoroutineDispatcher;
import kotlinx.coroutines.Dispatchers;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\b`\u0018\u00002\u00020\u0001:\u0001\u0007J\u0018\u0010\u0002\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00040\u00032\u0006\u0010\u0005\u001a\u00020\u0006H&¨\u0006\b"}, d2 = {"Lcom/stripe/android/FingerprintRequestExecutor;", "", "execute", "Landroidx/lifecycle/LiveData;", "Lcom/stripe/android/FingerprintData;", "request", "Lcom/stripe/android/FingerprintRequest;", "Default", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: FingerprintRequestExecutor.kt */
public interface FingerprintRequestExecutor {
    LiveData<FingerprintData> execute(FingerprintRequest fingerprintRequest);

    @Metadata(bv = {1, 0, 3}, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0019\u0012\b\b\u0002\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0002\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\u0018\u0010\n\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\f0\u000b2\u0006\u0010\r\u001a\u00020\u000eH\u0016J\u0012\u0010\u000f\u001a\u0004\u0018\u00010\f2\u0006\u0010\r\u001a\u00020\u000eH\u0002R\u000e\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\t0\bX\u0004¢\u0006\u0002\n\u0000¨\u0006\u0010"}, d2 = {"Lcom/stripe/android/FingerprintRequestExecutor$Default;", "Lcom/stripe/android/FingerprintRequestExecutor;", "dispatcher", "Lkotlinx/coroutines/CoroutineDispatcher;", "connectionFactory", "Lcom/stripe/android/ConnectionFactory;", "(Lkotlinx/coroutines/CoroutineDispatcher;Lcom/stripe/android/ConnectionFactory;)V", "timestampSupplier", "Lkotlin/Function0;", "", "execute", "Landroidx/lifecycle/LiveData;", "Lcom/stripe/android/FingerprintData;", "request", "Lcom/stripe/android/FingerprintRequest;", "executeInternal", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: FingerprintRequestExecutor.kt */
    public static final class Default implements FingerprintRequestExecutor {
        private final ConnectionFactory connectionFactory;
        private final CoroutineDispatcher dispatcher;
        private final Function0<Long> timestampSupplier;

        public Default() {
            this((CoroutineDispatcher) null, (ConnectionFactory) null, 3, (DefaultConstructorMarker) null);
        }

        public Default(CoroutineDispatcher coroutineDispatcher, ConnectionFactory connectionFactory2) {
            Intrinsics.checkParameterIsNotNull(coroutineDispatcher, "dispatcher");
            Intrinsics.checkParameterIsNotNull(connectionFactory2, "connectionFactory");
            this.dispatcher = coroutineDispatcher;
            this.connectionFactory = connectionFactory2;
            this.timestampSupplier = FingerprintRequestExecutor$Default$timestampSupplier$1.INSTANCE;
        }

        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ Default(CoroutineDispatcher coroutineDispatcher, ConnectionFactory connectionFactory2, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this((i & 1) != 0 ? Dispatchers.getIO() : coroutineDispatcher, (i & 2) != 0 ? new ConnectionFactory.Default() : connectionFactory2);
        }

        public LiveData<FingerprintData> execute(FingerprintRequest fingerprintRequest) {
            Intrinsics.checkParameterIsNotNull(fingerprintRequest, ShareConstants.WEB_DIALOG_RESULT_PARAM_REQUEST_ID);
            return CoroutineLiveDataKt.liveData$default((CoroutineContext) this.dispatcher, 0, (Function2) new FingerprintRequestExecutor$Default$execute$1(this, fingerprintRequest, (Continuation) null), 2, (Object) null);
        }

        /* access modifiers changed from: private */
        /* JADX WARNING: Code restructure failed: missing block: B:25:0x005b, code lost:
            r1 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:26:0x005c, code lost:
            kotlin.io.CloseableKt.closeFinally(r8, r0);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:27:0x005f, code lost:
            throw r1;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final com.stripe.android.FingerprintData executeInternal(com.stripe.android.FingerprintRequest r8) {
            /*
                r7 = this;
                com.stripe.android.ConnectionFactory r0 = r7.connectionFactory
                com.stripe.android.StripeRequest r8 = (com.stripe.android.StripeRequest) r8
                com.stripe.android.StripeConnection r8 = r0.create(r8)
                java.io.Closeable r8 = (java.io.Closeable) r8
                r0 = 0
                r1 = r0
                java.lang.Throwable r1 = (java.lang.Throwable) r1
                r2 = r8
                com.stripe.android.StripeConnection r2 = (com.stripe.android.StripeConnection) r2     // Catch:{ all -> 0x0059 }
                kotlin.Result$Companion r3 = kotlin.Result.Companion     // Catch:{ all -> 0x0040 }
                r3 = r7
                com.stripe.android.FingerprintRequestExecutor$Default r3 = (com.stripe.android.FingerprintRequestExecutor.Default) r3     // Catch:{ all -> 0x0040 }
                com.stripe.android.StripeResponse r2 = r2.getResponse()     // Catch:{ all -> 0x0040 }
                boolean r4 = r2.isOk$stripe_release()     // Catch:{ all -> 0x0040 }
                if (r4 == 0) goto L_0x0021
                goto L_0x0022
            L_0x0021:
                r2 = r0
            L_0x0022:
                if (r2 == 0) goto L_0x003a
                com.stripe.android.FingerprintData r4 = new com.stripe.android.FingerprintData     // Catch:{ all -> 0x0040 }
                java.lang.String r2 = r2.getBody$stripe_release()     // Catch:{ all -> 0x0040 }
                kotlin.jvm.functions.Function0<java.lang.Long> r3 = r3.timestampSupplier     // Catch:{ all -> 0x0040 }
                java.lang.Object r3 = r3.invoke()     // Catch:{ all -> 0x0040 }
                java.lang.Number r3 = (java.lang.Number) r3     // Catch:{ all -> 0x0040 }
                long r5 = r3.longValue()     // Catch:{ all -> 0x0040 }
                r4.<init>(r2, r5)     // Catch:{ all -> 0x0040 }
                goto L_0x003b
            L_0x003a:
                r4 = r0
            L_0x003b:
                java.lang.Object r2 = kotlin.Result.m4constructorimpl(r4)     // Catch:{ all -> 0x0040 }
                goto L_0x004b
            L_0x0040:
                r2 = move-exception
                kotlin.Result$Companion r3 = kotlin.Result.Companion     // Catch:{ all -> 0x0059 }
                java.lang.Object r2 = kotlin.ResultKt.createFailure(r2)     // Catch:{ all -> 0x0059 }
                java.lang.Object r2 = kotlin.Result.m4constructorimpl(r2)     // Catch:{ all -> 0x0059 }
            L_0x004b:
                boolean r3 = kotlin.Result.m10isFailureimpl(r2)     // Catch:{ all -> 0x0059 }
                if (r3 == 0) goto L_0x0052
                goto L_0x0053
            L_0x0052:
                r0 = r2
            L_0x0053:
                com.stripe.android.FingerprintData r0 = (com.stripe.android.FingerprintData) r0     // Catch:{ all -> 0x0059 }
                kotlin.io.CloseableKt.closeFinally(r8, r1)
                return r0
            L_0x0059:
                r0 = move-exception
                throw r0     // Catch:{ all -> 0x005b }
            L_0x005b:
                r1 = move-exception
                kotlin.io.CloseableKt.closeFinally(r8, r0)
                throw r1
            */
            throw new UnsupportedOperationException("Method not decompiled: com.stripe.android.FingerprintRequestExecutor.Default.executeInternal(com.stripe.android.FingerprintRequest):com.stripe.android.FingerprintData");
        }
    }
}
