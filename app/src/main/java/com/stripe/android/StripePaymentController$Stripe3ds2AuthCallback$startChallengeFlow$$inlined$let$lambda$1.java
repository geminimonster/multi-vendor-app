package com.stripe.android;

import com.stripe.android.StripePaymentController;
import com.stripe.android.stripe3ds2.transaction.ChallengeParameters;
import com.stripe.android.stripe3ds2.transaction.ChallengeStatusReceiver;
import com.stripe.android.stripe3ds2.transaction.Stripe3ds2ActivityStarterHost;
import kotlin.Metadata;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\n\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\u0010\u0000\u001a\u00020\u0001H\n¢\u0006\u0002\b\u0002¨\u0006\u0003"}, d2 = {"<anonymous>", "", "run", "com/stripe/android/StripePaymentController$Stripe3ds2AuthCallback$startChallengeFlow$1$1"}, k = 3, mv = {1, 1, 16})
/* compiled from: StripePaymentController.kt */
final class StripePaymentController$Stripe3ds2AuthCallback$startChallengeFlow$$inlined$let$lambda$1 implements Runnable {
    final /* synthetic */ ChallengeParameters $challengeParameters$inlined;
    final /* synthetic */ Stripe3ds2ActivityStarterHost $it;
    final /* synthetic */ StripePaymentController.Stripe3ds2AuthCallback this$0;

    StripePaymentController$Stripe3ds2AuthCallback$startChallengeFlow$$inlined$let$lambda$1(Stripe3ds2ActivityStarterHost stripe3ds2ActivityStarterHost, StripePaymentController.Stripe3ds2AuthCallback stripe3ds2AuthCallback, ChallengeParameters challengeParameters) {
        this.$it = stripe3ds2ActivityStarterHost;
        this.this$0 = stripe3ds2AuthCallback;
        this.$challengeParameters$inlined = challengeParameters;
    }

    public final void run() {
        this.this$0.transaction.doChallenge(this.$it, this.$challengeParameters$inlined, (ChallengeStatusReceiver) StripePaymentController.PaymentAuth3ds2ChallengeStatusReceiver.Companion.create$stripe_release(this.this$0.stripeRepository, this.this$0.stripeIntent, this.this$0.sourceId, this.this$0.requestOptions, this.this$0.analyticsRequestExecutor, this.this$0.analyticsDataFactory, this.this$0.transaction, this.this$0.analyticsRequestFactory), this.this$0.maxTimeout);
    }
}
