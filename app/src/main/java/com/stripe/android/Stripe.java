package com.stripe.android;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import androidx.fragment.app.Fragment;
import com.facebook.internal.NativeProtocol;
import com.stripe.android.ApiRequest;
import com.stripe.android.StripeRepository;
import com.stripe.android.exception.APIConnectionException;
import com.stripe.android.exception.APIException;
import com.stripe.android.exception.AuthenticationException;
import com.stripe.android.exception.CardException;
import com.stripe.android.exception.InvalidRequestException;
import com.stripe.android.exception.StripeException;
import com.stripe.android.model.AccountParams;
import com.stripe.android.model.BankAccount;
import com.stripe.android.model.BankAccountTokenParams;
import com.stripe.android.model.Card;
import com.stripe.android.model.ConfirmPaymentIntentParams;
import com.stripe.android.model.ConfirmSetupIntentParams;
import com.stripe.android.model.CvcTokenParams;
import com.stripe.android.model.PaymentIntent;
import com.stripe.android.model.PaymentMethod;
import com.stripe.android.model.PaymentMethodCreateParams;
import com.stripe.android.model.PersonTokenParams;
import com.stripe.android.model.PiiTokenParams;
import com.stripe.android.model.SetupIntent;
import com.stripe.android.model.Source;
import com.stripe.android.model.SourceParams;
import com.stripe.android.model.StripeFile;
import com.stripe.android.model.StripeFileParams;
import com.stripe.android.model.Token;
import com.stripe.android.model.TokenParams;
import com.stripe.android.view.AuthActivityStarter;
import java.util.List;
import kotlin.Deprecated;
import kotlin.Metadata;
import kotlin.ReplaceWith;
import kotlin.coroutines.Continuation;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.internal.Intrinsics;
import kotlinx.coroutines.CoroutineScope;
import kotlinx.coroutines.CoroutineScopeKt;
import kotlinx.coroutines.Dispatchers;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000ì\u0001\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0010\u0018\u0000 l2\u00020\u0001:\blmnopqrsB-\b\u0017\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u0005\u0012\b\b\u0002\u0010\u0007\u001a\u00020\b¢\u0006\u0002\u0010\tB1\b\u0012\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\b\u0010\u0006\u001a\u0004\u0018\u00010\u0005\u0012\u0006\u0010\u0007\u001a\u00020\b¢\u0006\u0002\u0010\fB)\b\u0010\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\r\u001a\u00020\u000e\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\b\u0010\u0006\u001a\u0004\u0018\u00010\u0005¢\u0006\u0002\u0010\u000fB5\b\u0000\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\r\u001a\u00020\u000e\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u0005\u0012\b\b\u0002\u0010\u0010\u001a\u00020\u0011¢\u0006\u0002\u0010\u0012J\u0018\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u0005H\u0007J\u0018\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u0017\u001a\u00020\u0005H\u0007J\u0018\u0010\u001a\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u0005H\u0007J\u0018\u0010\u001a\u001a\u00020\u00142\u0006\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u0017\u001a\u00020\u0005H\u0007J$\u0010\u001b\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u001c\u001a\u00020\u001d2\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u0005H\u0007J$\u0010\u001b\u001a\u00020\u00142\u0006\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u001c\u001a\u00020\u001d2\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u0005H\u0007J7\u0010\u001e\u001a\u00020\u00142\u0006\u0010\u001f\u001a\u00020 2\u0006\u0010!\u001a\u00020\"2\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u00052\f\u0010#\u001a\b\u0012\u0004\u0012\u00020%0$H\u0000¢\u0006\u0002\b&J$\u0010'\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u001f\u001a\u00020 2\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u0005H\u0007J$\u0010'\u001a\u00020\u00142\u0006\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u001f\u001a\u00020 2\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u0005H\u0007J\u001e\u0010(\u001a\u0004\u0018\u00010)2\u0006\u0010\u001f\u001a\u00020 2\n\b\u0002\u0010*\u001a\u0004\u0018\u00010\u0005H\u0007J$\u0010+\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u00162\u0006\u0010,\u001a\u00020-2\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u0005H\u0007J$\u0010+\u001a\u00020\u00142\u0006\u0010\u0018\u001a\u00020\u00192\u0006\u0010,\u001a\u00020-2\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u0005H\u0007J\u001e\u0010.\u001a\u0004\u0018\u00010/2\u0006\u0010,\u001a\u00020-2\n\b\u0002\u0010*\u001a\u0004\u0018\u00010\u0005H\u0007J6\u00100\u001a\u00020\u00142\u0006\u00101\u001a\u0002022\n\b\u0002\u0010*\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u00052\f\u0010#\u001a\b\u0012\u0004\u0012\u0002030$H\u0007J*\u00104\u001a\u0004\u0018\u0001032\u0006\u00101\u001a\u0002022\n\b\u0002\u0010*\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u0005H\u0007J6\u00105\u001a\u00020\u00142\u0006\u00106\u001a\u0002072\n\b\u0002\u0010*\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u00052\f\u0010#\u001a\b\u0012\u0004\u0012\u0002030$H\u0007J6\u00105\u001a\u00020\u00142\u0006\u00108\u001a\u0002092\n\b\u0002\u0010*\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u00052\f\u0010#\u001a\b\u0012\u0004\u0012\u0002030$H\u0007J\u001e\u0010:\u001a\u0004\u0018\u0001032\u0006\u00106\u001a\u0002072\n\b\u0002\u0010*\u001a\u0004\u0018\u00010\u0005H\u0007J*\u0010:\u001a\u0004\u0018\u0001032\u0006\u00108\u001a\u0002092\n\b\u0002\u0010*\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u0005H\u0007J6\u0010;\u001a\u00020\u00142\u0006\u0010<\u001a\u00020=2\n\b\u0002\u0010*\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u00052\f\u0010#\u001a\b\u0012\u0004\u0012\u0002030$H\u0007J*\u0010>\u001a\u0004\u0018\u0001032\u0006\u0010<\u001a\u00020=2\n\b\u0002\u0010*\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u0005H\u0007J8\u0010?\u001a\u00020\u00142\b\b\u0001\u0010@\u001a\u00020\u00052\n\b\u0002\u0010*\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u00052\f\u0010#\u001a\b\u0012\u0004\u0012\u0002030$H\u0007J*\u0010A\u001a\u0004\u0018\u0001032\u0006\u0010@\u001a\u00020\u00052\n\b\u0002\u0010*\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u0005H\u0007J6\u0010B\u001a\u00020\u00142\u0006\u0010C\u001a\u00020D2\n\b\u0002\u0010*\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u00052\f\u0010#\u001a\b\u0012\u0004\u0012\u00020E0$H\u0007J(\u0010F\u001a\u00020E2\u0006\u0010C\u001a\u00020D2\n\b\u0002\u0010*\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u0005H\u0007J6\u0010G\u001a\u00020\u00142\u0006\u0010H\u001a\u00020I2\n\b\u0002\u0010*\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u00052\f\u0010#\u001a\b\u0012\u0004\u0012\u00020J0$H\u0007J*\u0010K\u001a\u0004\u0018\u00010J2\u0006\u0010H\u001a\u00020I2\n\b\u0002\u0010*\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u0005H\u0007J6\u0010L\u001a\u00020\u00142\u0006\u0010M\u001a\u00020N2\n\b\u0002\u0010*\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u00052\f\u0010#\u001a\b\u0012\u0004\u0012\u0002030$H\u0007J*\u0010O\u001a\u0004\u0018\u0001032\u0006\u0010M\u001a\u00020N2\n\b\u0002\u0010*\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u0005H\u0007J6\u0010P\u001a\u00020\u00142\u0006\u0010Q\u001a\u00020\u00052\n\b\u0002\u0010*\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u00052\f\u0010#\u001a\b\u0012\u0004\u0012\u0002030$H\u0007J*\u0010R\u001a\u0004\u0018\u0001032\u0006\u0010Q\u001a\u00020\u00052\n\b\u0002\u0010*\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u0005H\u0007J6\u0010S\u001a\u00020\u00142\u0006\u0010T\u001a\u00020U2\n\b\u0002\u0010*\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u00052\f\u0010#\u001a\b\u0012\u0004\u0012\u00020\u001d0$H\u0007J*\u0010V\u001a\u0004\u0018\u00010\u001d2\u0006\u0010M\u001a\u00020U2\n\b\u0002\u0010*\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u0005H\u0007J*\u0010W\u001a\u00020\u00142\u0006\u0010<\u001a\u00020=2\n\b\u0002\u0010*\u001a\u0004\u0018\u00010\u00052\f\u0010#\u001a\b\u0012\u0004\u0012\u0002030$H\u0007J4\u0010W\u001a\u00020\u00142\u0006\u0010X\u001a\u00020Y2\b\u0010\u0006\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010*\u001a\u0004\u0018\u00010\u00052\f\u0010#\u001a\b\u0012\u0004\u0012\u0002030$H\u0002J$\u0010Z\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u00052\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u0005H\u0007J$\u0010Z\u001a\u00020\u00142\u0006\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u0017\u001a\u00020\u00052\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u0005H\u0007J$\u0010[\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u00052\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u0005H\u0007J$\u0010[\u001a\u00020\u00142\u0006\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u0017\u001a\u00020\u00052\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u0005H\u0007J\u0018\u0010\\\u001a\u00020\b2\u0006\u0010]\u001a\u00020^2\b\u0010_\u001a\u0004\u0018\u00010`J\u001c\u0010a\u001a\u00020\u00142\u0006\u0010_\u001a\u00020`2\f\u0010#\u001a\b\u0012\u0004\u0012\u00020\u001d0$J(\u0010b\u001a\u00020\b2\u0006\u0010]\u001a\u00020^2\b\u0010_\u001a\u0004\u0018\u00010`2\f\u0010#\u001a\b\u0012\u0004\u0012\u00020%0$H\u0007J(\u0010c\u001a\u00020\b2\u0006\u0010]\u001a\u00020^2\b\u0010_\u001a\u0004\u0018\u00010`2\f\u0010#\u001a\b\u0012\u0004\u0012\u00020d0$H\u0007J*\u0010e\u001a\u00020\u00142\u0006\u0010\u0017\u001a\u00020\u00052\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u00052\f\u0010#\u001a\b\u0012\u0004\u0012\u00020)0$H\u0007J\u001e\u0010f\u001a\u0004\u0018\u00010)2\u0006\u0010\u0017\u001a\u00020\u00052\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u0005H\u0007J*\u0010g\u001a\u00020\u00142\u0006\u0010\u0017\u001a\u00020\u00052\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u00052\f\u0010#\u001a\b\u0012\u0004\u0012\u00020/0$H\u0007J\u001e\u0010h\u001a\u0004\u0018\u00010/2\u0006\u0010\u0017\u001a\u00020\u00052\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u0005H\u0007J6\u0010i\u001a\u00020\u00142\b\b\u0001\u0010j\u001a\u00020\u00052\b\b\u0001\u0010\u0017\u001a\u00020\u00052\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u00052\f\u0010#\u001a\b\u0012\u0004\u0012\u00020\u001d0$H\u0007J*\u0010k\u001a\u0004\u0018\u00010\u001d2\b\b\u0001\u0010j\u001a\u00020\u00052\b\b\u0001\u0010\u0017\u001a\u00020\u00052\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u0005H\u0007R\u000e\u0010\r\u001a\u00020\u000eX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u0006\u001a\u0004\u0018\u00010\u0005X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0004¢\u0006\u0002\n\u0000\u0002\u0004\n\u0002\b\u0019¨\u0006t"}, d2 = {"Lcom/stripe/android/Stripe;", "", "context", "Landroid/content/Context;", "publishableKey", "", "stripeAccountId", "enableLogging", "", "(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Z)V", "stripeRepository", "Lcom/stripe/android/StripeRepository;", "(Landroid/content/Context;Lcom/stripe/android/StripeRepository;Ljava/lang/String;Ljava/lang/String;Z)V", "paymentController", "Lcom/stripe/android/PaymentController;", "(Lcom/stripe/android/StripeRepository;Lcom/stripe/android/PaymentController;Ljava/lang/String;Ljava/lang/String;)V", "workScope", "Lkotlinx/coroutines/CoroutineScope;", "(Lcom/stripe/android/StripeRepository;Lcom/stripe/android/PaymentController;Ljava/lang/String;Ljava/lang/String;Lkotlinx/coroutines/CoroutineScope;)V", "authenticatePayment", "", "activity", "Landroid/app/Activity;", "clientSecret", "fragment", "Landroidx/fragment/app/Fragment;", "authenticateSetup", "authenticateSource", "source", "Lcom/stripe/android/model/Source;", "confirmAlipayPayment", "confirmPaymentIntentParams", "Lcom/stripe/android/model/ConfirmPaymentIntentParams;", "authenticator", "Lcom/stripe/android/AlipayAuthenticator;", "callback", "Lcom/stripe/android/ApiResultCallback;", "Lcom/stripe/android/PaymentIntentResult;", "confirmAlipayPayment$stripe_release", "confirmPayment", "confirmPaymentIntentSynchronous", "Lcom/stripe/android/model/PaymentIntent;", "idempotencyKey", "confirmSetupIntent", "confirmSetupIntentParams", "Lcom/stripe/android/model/ConfirmSetupIntentParams;", "confirmSetupIntentSynchronous", "Lcom/stripe/android/model/SetupIntent;", "createAccountToken", "accountParams", "Lcom/stripe/android/model/AccountParams;", "Lcom/stripe/android/model/Token;", "createAccountTokenSynchronous", "createBankAccountToken", "bankAccount", "Lcom/stripe/android/model/BankAccount;", "bankAccountTokenParams", "Lcom/stripe/android/model/BankAccountTokenParams;", "createBankAccountTokenSynchronous", "createCardToken", "card", "Lcom/stripe/android/model/Card;", "createCardTokenSynchronous", "createCvcUpdateToken", "cvc", "createCvcUpdateTokenSynchronous", "createFile", "fileParams", "Lcom/stripe/android/model/StripeFileParams;", "Lcom/stripe/android/model/StripeFile;", "createFileSynchronous", "createPaymentMethod", "paymentMethodCreateParams", "Lcom/stripe/android/model/PaymentMethodCreateParams;", "Lcom/stripe/android/model/PaymentMethod;", "createPaymentMethodSynchronous", "createPersonToken", "params", "Lcom/stripe/android/model/PersonTokenParams;", "createPersonTokenSynchronous", "createPiiToken", "personalId", "createPiiTokenSynchronous", "createSource", "sourceParams", "Lcom/stripe/android/model/SourceParams;", "createSourceSynchronous", "createToken", "tokenParams", "Lcom/stripe/android/model/TokenParams;", "handleNextActionForPayment", "handleNextActionForSetupIntent", "isAuthenticateSourceResult", "requestCode", "", "data", "Landroid/content/Intent;", "onAuthenticateSourceResult", "onPaymentResult", "onSetupResult", "Lcom/stripe/android/SetupIntentResult;", "retrievePaymentIntent", "retrievePaymentIntentSynchronous", "retrieveSetupIntent", "retrieveSetupIntentSynchronous", "retrieveSource", "sourceId", "retrieveSourceSynchronous", "Companion", "CreateFileTask", "CreatePaymentMethodTask", "CreateSourceTask", "CreateTokenTask", "RetrievePaymentIntentTask", "RetrieveSetupIntentTask", "RetrieveSourceTask", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: Stripe.kt */
public final class Stripe {
    public static final String API_VERSION = ApiVersion.Companion.get$stripe_release().getCode$stripe_release();
    public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
    public static final String VERSION = "AndroidBindings/14.5.0";
    /* access modifiers changed from: private */
    public static boolean advancedFraudSignalsEnabled = true;
    /* access modifiers changed from: private */
    public static AppInfo appInfo;
    /* access modifiers changed from: private */
    public final PaymentController paymentController;
    private final String publishableKey;
    private final String stripeAccountId;
    private final StripeRepository stripeRepository;
    private final CoroutineScope workScope;

    public Stripe(Context context, String str) {
        this(context, str, (String) null, false, 12, (DefaultConstructorMarker) null);
    }

    public Stripe(Context context, String str, String str2) {
        this(context, str, str2, false, 8, (DefaultConstructorMarker) null);
    }

    public static final boolean getAdvancedFraudSignalsEnabled() {
        return advancedFraudSignalsEnabled;
    }

    public static final AppInfo getAppInfo() {
        return appInfo;
    }

    public static final void setAdvancedFraudSignalsEnabled(boolean z) {
        advancedFraudSignalsEnabled = z;
    }

    public static final void setAppInfo(AppInfo appInfo2) {
        appInfo = appInfo2;
    }

    public final void authenticateSource(Activity activity, Source source) {
        authenticateSource$default(this, activity, source, (String) null, 4, (Object) null);
    }

    public final void authenticateSource(Fragment fragment, Source source) {
        authenticateSource$default(this, fragment, source, (String) null, 4, (Object) null);
    }

    public final void confirmPayment(Activity activity, ConfirmPaymentIntentParams confirmPaymentIntentParams) {
        confirmPayment$default(this, activity, confirmPaymentIntentParams, (String) null, 4, (Object) null);
    }

    public final void confirmPayment(Fragment fragment, ConfirmPaymentIntentParams confirmPaymentIntentParams) {
        confirmPayment$default(this, fragment, confirmPaymentIntentParams, (String) null, 4, (Object) null);
    }

    @Deprecated(message = "use {@link #confirmPayment(Activity, ConfirmPaymentIntentParams)}")
    public final PaymentIntent confirmPaymentIntentSynchronous(ConfirmPaymentIntentParams confirmPaymentIntentParams) throws AuthenticationException, InvalidRequestException, APIConnectionException, APIException {
        return confirmPaymentIntentSynchronous$default(this, confirmPaymentIntentParams, (String) null, 2, (Object) null);
    }

    public final void confirmSetupIntent(Activity activity, ConfirmSetupIntentParams confirmSetupIntentParams) {
        confirmSetupIntent$default(this, activity, confirmSetupIntentParams, (String) null, 4, (Object) null);
    }

    public final void confirmSetupIntent(Fragment fragment, ConfirmSetupIntentParams confirmSetupIntentParams) {
        confirmSetupIntent$default(this, fragment, confirmSetupIntentParams, (String) null, 4, (Object) null);
    }

    @Deprecated(message = "use {@link #confirmSetupIntent(Activity, ConfirmSetupIntentParams)}")
    public final SetupIntent confirmSetupIntentSynchronous(ConfirmSetupIntentParams confirmSetupIntentParams) throws AuthenticationException, InvalidRequestException, APIConnectionException, APIException {
        return confirmSetupIntentSynchronous$default(this, confirmSetupIntentParams, (String) null, 2, (Object) null);
    }

    public final void createAccountToken(AccountParams accountParams, ApiResultCallback<Token> apiResultCallback) {
        createAccountToken$default(this, accountParams, (String) null, (String) null, apiResultCallback, 6, (Object) null);
    }

    public final void createAccountToken(AccountParams accountParams, String str, ApiResultCallback<Token> apiResultCallback) {
        createAccountToken$default(this, accountParams, str, (String) null, apiResultCallback, 4, (Object) null);
    }

    public final Token createAccountTokenSynchronous(AccountParams accountParams) throws AuthenticationException, InvalidRequestException, APIConnectionException, APIException {
        return createAccountTokenSynchronous$default(this, accountParams, (String) null, (String) null, 6, (Object) null);
    }

    public final Token createAccountTokenSynchronous(AccountParams accountParams, String str) throws AuthenticationException, InvalidRequestException, APIConnectionException, APIException {
        return createAccountTokenSynchronous$default(this, accountParams, str, (String) null, 4, (Object) null);
    }

    @Deprecated(message = "Use BankAccountTokenParams")
    public final void createBankAccountToken(BankAccount bankAccount, ApiResultCallback<Token> apiResultCallback) {
        createBankAccountToken$default(this, bankAccount, (String) null, (String) null, (ApiResultCallback) apiResultCallback, 6, (Object) null);
    }

    @Deprecated(message = "Use BankAccountTokenParams")
    public final void createBankAccountToken(BankAccount bankAccount, String str, ApiResultCallback<Token> apiResultCallback) {
        createBankAccountToken$default(this, bankAccount, str, (String) null, (ApiResultCallback) apiResultCallback, 4, (Object) null);
    }

    public final void createBankAccountToken(BankAccountTokenParams bankAccountTokenParams, ApiResultCallback<Token> apiResultCallback) {
        createBankAccountToken$default(this, bankAccountTokenParams, (String) null, (String) null, (ApiResultCallback) apiResultCallback, 6, (Object) null);
    }

    public final void createBankAccountToken(BankAccountTokenParams bankAccountTokenParams, String str, ApiResultCallback<Token> apiResultCallback) {
        createBankAccountToken$default(this, bankAccountTokenParams, str, (String) null, (ApiResultCallback) apiResultCallback, 4, (Object) null);
    }

    @Deprecated(message = "Use BankAccountTokenParams")
    public final Token createBankAccountTokenSynchronous(BankAccount bankAccount) throws AuthenticationException, InvalidRequestException, APIConnectionException, CardException, APIException {
        return createBankAccountTokenSynchronous$default(this, bankAccount, (String) null, 2, (Object) null);
    }

    public final Token createBankAccountTokenSynchronous(BankAccountTokenParams bankAccountTokenParams) throws AuthenticationException, InvalidRequestException, APIConnectionException, CardException, APIException {
        return createBankAccountTokenSynchronous$default(this, bankAccountTokenParams, (String) null, (String) null, 6, (Object) null);
    }

    public final Token createBankAccountTokenSynchronous(BankAccountTokenParams bankAccountTokenParams, String str) throws AuthenticationException, InvalidRequestException, APIConnectionException, CardException, APIException {
        return createBankAccountTokenSynchronous$default(this, bankAccountTokenParams, str, (String) null, 4, (Object) null);
    }

    public final void createCardToken(Card card, ApiResultCallback<Token> apiResultCallback) {
        createCardToken$default(this, card, (String) null, (String) null, apiResultCallback, 6, (Object) null);
    }

    public final void createCardToken(Card card, String str, ApiResultCallback<Token> apiResultCallback) {
        createCardToken$default(this, card, str, (String) null, apiResultCallback, 4, (Object) null);
    }

    public final Token createCardTokenSynchronous(Card card) throws AuthenticationException, InvalidRequestException, APIConnectionException, CardException, APIException {
        return createCardTokenSynchronous$default(this, card, (String) null, (String) null, 6, (Object) null);
    }

    public final Token createCardTokenSynchronous(Card card, String str) throws AuthenticationException, InvalidRequestException, APIConnectionException, CardException, APIException {
        return createCardTokenSynchronous$default(this, card, str, (String) null, 4, (Object) null);
    }

    public final void createCvcUpdateToken(String str, ApiResultCallback<Token> apiResultCallback) {
        createCvcUpdateToken$default(this, str, (String) null, (String) null, apiResultCallback, 6, (Object) null);
    }

    public final void createCvcUpdateToken(String str, String str2, ApiResultCallback<Token> apiResultCallback) {
        createCvcUpdateToken$default(this, str, str2, (String) null, apiResultCallback, 4, (Object) null);
    }

    public final Token createCvcUpdateTokenSynchronous(String str) throws AuthenticationException, InvalidRequestException, APIConnectionException, CardException, APIException {
        return createCvcUpdateTokenSynchronous$default(this, str, (String) null, (String) null, 6, (Object) null);
    }

    public final Token createCvcUpdateTokenSynchronous(String str, String str2) throws AuthenticationException, InvalidRequestException, APIConnectionException, CardException, APIException {
        return createCvcUpdateTokenSynchronous$default(this, str, str2, (String) null, 4, (Object) null);
    }

    public final void createFile(StripeFileParams stripeFileParams, ApiResultCallback<StripeFile> apiResultCallback) {
        createFile$default(this, stripeFileParams, (String) null, (String) null, apiResultCallback, 6, (Object) null);
    }

    public final void createFile(StripeFileParams stripeFileParams, String str, ApiResultCallback<StripeFile> apiResultCallback) {
        createFile$default(this, stripeFileParams, str, (String) null, apiResultCallback, 4, (Object) null);
    }

    public final StripeFile createFileSynchronous(StripeFileParams stripeFileParams) {
        return createFileSynchronous$default(this, stripeFileParams, (String) null, (String) null, 6, (Object) null);
    }

    public final StripeFile createFileSynchronous(StripeFileParams stripeFileParams, String str) {
        return createFileSynchronous$default(this, stripeFileParams, str, (String) null, 4, (Object) null);
    }

    public final void createPaymentMethod(PaymentMethodCreateParams paymentMethodCreateParams, ApiResultCallback<PaymentMethod> apiResultCallback) {
        createPaymentMethod$default(this, paymentMethodCreateParams, (String) null, (String) null, apiResultCallback, 6, (Object) null);
    }

    public final void createPaymentMethod(PaymentMethodCreateParams paymentMethodCreateParams, String str, ApiResultCallback<PaymentMethod> apiResultCallback) {
        createPaymentMethod$default(this, paymentMethodCreateParams, str, (String) null, apiResultCallback, 4, (Object) null);
    }

    public final PaymentMethod createPaymentMethodSynchronous(PaymentMethodCreateParams paymentMethodCreateParams) throws APIException, AuthenticationException, InvalidRequestException, APIConnectionException {
        return createPaymentMethodSynchronous$default(this, paymentMethodCreateParams, (String) null, (String) null, 6, (Object) null);
    }

    public final PaymentMethod createPaymentMethodSynchronous(PaymentMethodCreateParams paymentMethodCreateParams, String str) throws APIException, AuthenticationException, InvalidRequestException, APIConnectionException {
        return createPaymentMethodSynchronous$default(this, paymentMethodCreateParams, str, (String) null, 4, (Object) null);
    }

    public final void createPersonToken(PersonTokenParams personTokenParams, ApiResultCallback<Token> apiResultCallback) {
        createPersonToken$default(this, personTokenParams, (String) null, (String) null, apiResultCallback, 6, (Object) null);
    }

    public final void createPersonToken(PersonTokenParams personTokenParams, String str, ApiResultCallback<Token> apiResultCallback) {
        createPersonToken$default(this, personTokenParams, str, (String) null, apiResultCallback, 4, (Object) null);
    }

    public final Token createPersonTokenSynchronous(PersonTokenParams personTokenParams) throws AuthenticationException, InvalidRequestException, APIConnectionException, CardException, APIException {
        return createPersonTokenSynchronous$default(this, personTokenParams, (String) null, (String) null, 6, (Object) null);
    }

    public final Token createPersonTokenSynchronous(PersonTokenParams personTokenParams, String str) throws AuthenticationException, InvalidRequestException, APIConnectionException, CardException, APIException {
        return createPersonTokenSynchronous$default(this, personTokenParams, str, (String) null, 4, (Object) null);
    }

    public final void createPiiToken(String str, ApiResultCallback<Token> apiResultCallback) {
        createPiiToken$default(this, str, (String) null, (String) null, apiResultCallback, 6, (Object) null);
    }

    public final void createPiiToken(String str, String str2, ApiResultCallback<Token> apiResultCallback) {
        createPiiToken$default(this, str, str2, (String) null, apiResultCallback, 4, (Object) null);
    }

    public final Token createPiiTokenSynchronous(String str) throws AuthenticationException, InvalidRequestException, APIConnectionException, CardException, APIException {
        return createPiiTokenSynchronous$default(this, str, (String) null, (String) null, 6, (Object) null);
    }

    public final Token createPiiTokenSynchronous(String str, String str2) throws AuthenticationException, InvalidRequestException, APIConnectionException, CardException, APIException {
        return createPiiTokenSynchronous$default(this, str, str2, (String) null, 4, (Object) null);
    }

    public final void createSource(SourceParams sourceParams, ApiResultCallback<Source> apiResultCallback) {
        createSource$default(this, sourceParams, (String) null, (String) null, apiResultCallback, 6, (Object) null);
    }

    public final void createSource(SourceParams sourceParams, String str, ApiResultCallback<Source> apiResultCallback) {
        createSource$default(this, sourceParams, str, (String) null, apiResultCallback, 4, (Object) null);
    }

    public final Source createSourceSynchronous(SourceParams sourceParams) throws AuthenticationException, InvalidRequestException, APIConnectionException, APIException {
        return createSourceSynchronous$default(this, sourceParams, (String) null, (String) null, 6, (Object) null);
    }

    public final Source createSourceSynchronous(SourceParams sourceParams, String str) throws AuthenticationException, InvalidRequestException, APIConnectionException, APIException {
        return createSourceSynchronous$default(this, sourceParams, str, (String) null, 4, (Object) null);
    }

    @Deprecated(message = "Deprecated, replace with Stripe#createCardToken()", replaceWith = @ReplaceWith(expression = "createCardToken(card, idempotencyKey, callback)", imports = {}))
    public final void createToken(Card card, ApiResultCallback<Token> apiResultCallback) {
        createToken$default(this, card, (String) null, apiResultCallback, 2, (Object) null);
    }

    public final void handleNextActionForPayment(Activity activity, String str) {
        handleNextActionForPayment$default(this, activity, str, (String) null, 4, (Object) null);
    }

    public final void handleNextActionForPayment(Fragment fragment, String str) {
        handleNextActionForPayment$default(this, fragment, str, (String) null, 4, (Object) null);
    }

    public final void handleNextActionForSetupIntent(Activity activity, String str) {
        handleNextActionForSetupIntent$default(this, activity, str, (String) null, 4, (Object) null);
    }

    public final void handleNextActionForSetupIntent(Fragment fragment, String str) {
        handleNextActionForSetupIntent$default(this, fragment, str, (String) null, 4, (Object) null);
    }

    public final void retrievePaymentIntent(String str, ApiResultCallback<PaymentIntent> apiResultCallback) throws APIException, AuthenticationException, InvalidRequestException, APIConnectionException {
        retrievePaymentIntent$default(this, str, (String) null, apiResultCallback, 2, (Object) null);
    }

    public final PaymentIntent retrievePaymentIntentSynchronous(String str) throws APIException, AuthenticationException, InvalidRequestException, APIConnectionException {
        return retrievePaymentIntentSynchronous$default(this, str, (String) null, 2, (Object) null);
    }

    public final void retrieveSetupIntent(String str, ApiResultCallback<SetupIntent> apiResultCallback) throws APIException, AuthenticationException, InvalidRequestException, APIConnectionException {
        retrieveSetupIntent$default(this, str, (String) null, apiResultCallback, 2, (Object) null);
    }

    public final SetupIntent retrieveSetupIntentSynchronous(String str) throws APIException, AuthenticationException, InvalidRequestException, APIConnectionException {
        return retrieveSetupIntentSynchronous$default(this, str, (String) null, 2, (Object) null);
    }

    public final void retrieveSource(String str, String str2, ApiResultCallback<Source> apiResultCallback) {
        retrieveSource$default(this, str, str2, (String) null, apiResultCallback, 4, (Object) null);
    }

    public final Source retrieveSourceSynchronous(String str, String str2) throws AuthenticationException, InvalidRequestException, APIConnectionException, APIException {
        return retrieveSourceSynchronous$default(this, str, str2, (String) null, 4, (Object) null);
    }

    public Stripe(StripeRepository stripeRepository2, PaymentController paymentController2, String str, String str2, CoroutineScope coroutineScope) {
        Intrinsics.checkParameterIsNotNull(stripeRepository2, "stripeRepository");
        Intrinsics.checkParameterIsNotNull(paymentController2, "paymentController");
        Intrinsics.checkParameterIsNotNull(str, "publishableKey");
        Intrinsics.checkParameterIsNotNull(coroutineScope, "workScope");
        this.stripeRepository = stripeRepository2;
        this.paymentController = paymentController2;
        this.stripeAccountId = str2;
        this.workScope = coroutineScope;
        this.publishableKey = new ApiKeyValidator().requireValid(str);
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ Stripe(StripeRepository stripeRepository2, PaymentController paymentController2, String str, String str2, CoroutineScope coroutineScope, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(stripeRepository2, paymentController2, str, (i & 8) != 0 ? null : str2, (i & 16) != 0 ? CoroutineScopeKt.CoroutineScope(Dispatchers.getIO()) : coroutineScope);
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ Stripe(Context context, String str, String str2, boolean z, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(context, str, (i & 4) != 0 ? null : str2, (i & 8) != 0 ? false : z);
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public Stripe(android.content.Context r21, java.lang.String r22, java.lang.String r23, boolean r24) {
        /*
            r20 = this;
            r15 = r22
            java.lang.String r0 = "context"
            r1 = r21
            kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(r1, r0)
            java.lang.String r0 = "publishableKey"
            kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(r15, r0)
            android.content.Context r14 = r21.getApplicationContext()
            java.lang.String r0 = "context.applicationContext"
            kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r14, r0)
            com.stripe.android.StripeApiRepository r16 = new com.stripe.android.StripeApiRepository
            android.content.Context r1 = r21.getApplicationContext()
            kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r1, r0)
            com.stripe.android.AppInfo r3 = appInfo
            com.stripe.android.Logger$Companion r0 = com.stripe.android.Logger.Companion
            r13 = r24
            com.stripe.android.Logger r4 = r0.getInstance$stripe_release(r13)
            r5 = 0
            r6 = 0
            r7 = 0
            r8 = 0
            r9 = 0
            r10 = 0
            r11 = 0
            r12 = 0
            r17 = 0
            r18 = 8176(0x1ff0, float:1.1457E-41)
            r19 = 0
            r0 = r16
            r2 = r22
            r13 = r17
            r17 = r14
            r14 = r18
            r15 = r19
            r0.<init>(r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15)
            r3 = r16
            com.stripe.android.StripeRepository r3 = (com.stripe.android.StripeRepository) r3
            com.stripe.android.ApiKeyValidator$Companion r0 = com.stripe.android.ApiKeyValidator.Companion
            com.stripe.android.ApiKeyValidator r0 = r0.get$stripe_release()
            r1 = r22
            java.lang.String r4 = r0.requireValid(r1)
            r1 = r20
            r2 = r17
            r5 = r23
            r6 = r24
            r1.<init>((android.content.Context) r2, (com.stripe.android.StripeRepository) r3, (java.lang.String) r4, (java.lang.String) r5, (boolean) r6)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.stripe.android.Stripe.<init>(android.content.Context, java.lang.String, java.lang.String, boolean):void");
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private Stripe(android.content.Context r17, com.stripe.android.StripeRepository r18, java.lang.String r19, java.lang.String r20, boolean r21) {
        /*
            r16 = this;
            com.stripe.android.StripePaymentController r15 = new com.stripe.android.StripePaymentController
            android.content.Context r1 = r17.getApplicationContext()
            java.lang.String r0 = "context.applicationContext"
            kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r1, r0)
            r5 = 0
            r6 = 0
            r7 = 0
            r8 = 0
            r9 = 0
            r10 = 0
            r11 = 0
            r12 = 0
            r13 = 4080(0xff0, float:5.717E-42)
            r14 = 0
            r0 = r15
            r2 = r19
            r3 = r18
            r4 = r21
            r0.<init>(r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14)
            com.stripe.android.PaymentController r15 = (com.stripe.android.PaymentController) r15
            r0 = r16
            r1 = r18
            r3 = r20
            r0.<init>((com.stripe.android.StripeRepository) r1, (com.stripe.android.PaymentController) r15, (java.lang.String) r2, (java.lang.String) r3)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.stripe.android.Stripe.<init>(android.content.Context, com.stripe.android.StripeRepository, java.lang.String, java.lang.String, boolean):void");
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public Stripe(StripeRepository stripeRepository2, PaymentController paymentController2, String str, String str2) {
        this(stripeRepository2, paymentController2, str, str2, CoroutineScopeKt.CoroutineScope(Dispatchers.getIO()));
        Intrinsics.checkParameterIsNotNull(stripeRepository2, "stripeRepository");
        Intrinsics.checkParameterIsNotNull(paymentController2, "paymentController");
        Intrinsics.checkParameterIsNotNull(str, "publishableKey");
    }

    public static /* synthetic */ void confirmPayment$default(Stripe stripe, Activity activity, ConfirmPaymentIntentParams confirmPaymentIntentParams, String str, int i, Object obj) {
        if ((i & 4) != 0) {
            str = stripe.stripeAccountId;
        }
        stripe.confirmPayment(activity, confirmPaymentIntentParams, str);
    }

    public final void confirmPayment(Activity activity, ConfirmPaymentIntentParams confirmPaymentIntentParams, String str) {
        Intrinsics.checkParameterIsNotNull(activity, "activity");
        Intrinsics.checkParameterIsNotNull(confirmPaymentIntentParams, "confirmPaymentIntentParams");
        this.paymentController.startConfirmAndAuth(AuthActivityStarter.Host.Companion.create$stripe_release(activity), confirmPaymentIntentParams, new ApiRequest.Options(this.publishableKey, str, (String) null, 4, (DefaultConstructorMarker) null));
    }

    public static /* synthetic */ void confirmAlipayPayment$stripe_release$default(Stripe stripe, ConfirmPaymentIntentParams confirmPaymentIntentParams, AlipayAuthenticator alipayAuthenticator, String str, ApiResultCallback apiResultCallback, int i, Object obj) {
        if ((i & 4) != 0) {
            str = stripe.stripeAccountId;
        }
        stripe.confirmAlipayPayment$stripe_release(confirmPaymentIntentParams, alipayAuthenticator, str, apiResultCallback);
    }

    public final void confirmAlipayPayment$stripe_release(ConfirmPaymentIntentParams confirmPaymentIntentParams, AlipayAuthenticator alipayAuthenticator, String str, ApiResultCallback<PaymentIntentResult> apiResultCallback) {
        Intrinsics.checkParameterIsNotNull(confirmPaymentIntentParams, "confirmPaymentIntentParams");
        Intrinsics.checkParameterIsNotNull(alipayAuthenticator, "authenticator");
        Intrinsics.checkParameterIsNotNull(apiResultCallback, "callback");
        this.paymentController.startConfirm(confirmPaymentIntentParams, new ApiRequest.Options(this.publishableKey, str, (String) null, 4, (DefaultConstructorMarker) null), new Stripe$confirmAlipayPayment$1(this, str, alipayAuthenticator, apiResultCallback));
    }

    public static /* synthetic */ void confirmPayment$default(Stripe stripe, Fragment fragment, ConfirmPaymentIntentParams confirmPaymentIntentParams, String str, int i, Object obj) {
        if ((i & 4) != 0) {
            str = stripe.stripeAccountId;
        }
        stripe.confirmPayment(fragment, confirmPaymentIntentParams, str);
    }

    public final void confirmPayment(Fragment fragment, ConfirmPaymentIntentParams confirmPaymentIntentParams, String str) {
        Intrinsics.checkParameterIsNotNull(fragment, "fragment");
        Intrinsics.checkParameterIsNotNull(confirmPaymentIntentParams, "confirmPaymentIntentParams");
        this.paymentController.startConfirmAndAuth(AuthActivityStarter.Host.Companion.create$stripe_release(fragment), confirmPaymentIntentParams, new ApiRequest.Options(this.publishableKey, str, (String) null, 4, (DefaultConstructorMarker) null));
    }

    @Deprecated(message = "Rename to better reflect behavior and match iOS naming.", replaceWith = @ReplaceWith(expression = "handleNextActionForPayment(activity, clientSecret)", imports = {}))
    public final void authenticatePayment(Activity activity, String str) {
        Intrinsics.checkParameterIsNotNull(activity, "activity");
        Intrinsics.checkParameterIsNotNull(str, "clientSecret");
        this.paymentController.startAuth(AuthActivityStarter.Host.Companion.create$stripe_release(activity), new PaymentIntent.ClientSecret(str).getValue$stripe_release(), new ApiRequest.Options(this.publishableKey, this.stripeAccountId, (String) null, 4, (DefaultConstructorMarker) null));
    }

    public static /* synthetic */ void handleNextActionForPayment$default(Stripe stripe, Activity activity, String str, String str2, int i, Object obj) {
        if ((i & 4) != 0) {
            str2 = stripe.stripeAccountId;
        }
        stripe.handleNextActionForPayment(activity, str, str2);
    }

    public final void handleNextActionForPayment(Activity activity, String str, String str2) {
        Intrinsics.checkParameterIsNotNull(activity, "activity");
        Intrinsics.checkParameterIsNotNull(str, "clientSecret");
        this.paymentController.startAuth(AuthActivityStarter.Host.Companion.create$stripe_release(activity), new PaymentIntent.ClientSecret(str).getValue$stripe_release(), new ApiRequest.Options(this.publishableKey, str2, (String) null, 4, (DefaultConstructorMarker) null));
    }

    @Deprecated(message = "Rename to better reflect behavior and match iOS naming.", replaceWith = @ReplaceWith(expression = "handleNextActionForPayment(fragment, clientSecret)", imports = {}))
    public final void authenticatePayment(Fragment fragment, String str) {
        Intrinsics.checkParameterIsNotNull(fragment, "fragment");
        Intrinsics.checkParameterIsNotNull(str, "clientSecret");
        this.paymentController.startAuth(AuthActivityStarter.Host.Companion.create$stripe_release(fragment), new PaymentIntent.ClientSecret(str).getValue$stripe_release(), new ApiRequest.Options(this.publishableKey, this.stripeAccountId, (String) null, 4, (DefaultConstructorMarker) null));
    }

    public static /* synthetic */ void handleNextActionForPayment$default(Stripe stripe, Fragment fragment, String str, String str2, int i, Object obj) {
        if ((i & 4) != 0) {
            str2 = stripe.stripeAccountId;
        }
        stripe.handleNextActionForPayment(fragment, str, str2);
    }

    public final void handleNextActionForPayment(Fragment fragment, String str, String str2) {
        Intrinsics.checkParameterIsNotNull(fragment, "fragment");
        Intrinsics.checkParameterIsNotNull(str, "clientSecret");
        this.paymentController.startAuth(AuthActivityStarter.Host.Companion.create$stripe_release(fragment), new PaymentIntent.ClientSecret(str).getValue$stripe_release(), new ApiRequest.Options(this.publishableKey, str2, (String) null, 4, (DefaultConstructorMarker) null));
    }

    public final boolean onPaymentResult(int i, Intent intent, ApiResultCallback<PaymentIntentResult> apiResultCallback) {
        Intrinsics.checkParameterIsNotNull(apiResultCallback, "callback");
        if (intent == null || !this.paymentController.shouldHandlePaymentResult(i, intent)) {
            return false;
        }
        this.paymentController.handlePaymentResult(intent, apiResultCallback);
        return true;
    }

    public static /* synthetic */ void retrievePaymentIntent$default(Stripe stripe, String str, String str2, ApiResultCallback apiResultCallback, int i, Object obj) throws APIException, AuthenticationException, InvalidRequestException, APIConnectionException {
        if ((i & 2) != 0) {
            str2 = stripe.stripeAccountId;
        }
        stripe.retrievePaymentIntent(str, str2, apiResultCallback);
    }

    public final void retrievePaymentIntent(String str, String str2, ApiResultCallback<PaymentIntent> apiResultCallback) throws APIException, AuthenticationException, InvalidRequestException, APIConnectionException {
        Intrinsics.checkParameterIsNotNull(str, "clientSecret");
        Intrinsics.checkParameterIsNotNull(apiResultCallback, "callback");
        new RetrievePaymentIntentTask(this.stripeRepository, str, new ApiRequest.Options(this.publishableKey, str2, (String) null, 4, (DefaultConstructorMarker) null), this.workScope, apiResultCallback).execute$stripe_release();
    }

    public static /* synthetic */ PaymentIntent retrievePaymentIntentSynchronous$default(Stripe stripe, String str, String str2, int i, Object obj) throws APIException, AuthenticationException, InvalidRequestException, APIConnectionException {
        if ((i & 2) != 0) {
            str2 = stripe.stripeAccountId;
        }
        return stripe.retrievePaymentIntentSynchronous(str, str2);
    }

    public final PaymentIntent retrievePaymentIntentSynchronous(String str, String str2) throws APIException, AuthenticationException, InvalidRequestException, APIConnectionException {
        Intrinsics.checkParameterIsNotNull(str, "clientSecret");
        return StripeRepository.DefaultImpls.retrievePaymentIntent$default(this.stripeRepository, new PaymentIntent.ClientSecret(str).getValue$stripe_release(), new ApiRequest.Options(this.publishableKey, str2, (String) null, 4, (DefaultConstructorMarker) null), (List) null, 4, (Object) null);
    }

    public static /* synthetic */ PaymentIntent confirmPaymentIntentSynchronous$default(Stripe stripe, ConfirmPaymentIntentParams confirmPaymentIntentParams, String str, int i, Object obj) throws AuthenticationException, InvalidRequestException, APIConnectionException, APIException {
        if ((i & 2) != 0) {
            str = null;
        }
        return stripe.confirmPaymentIntentSynchronous(confirmPaymentIntentParams, str);
    }

    @Deprecated(message = "use {@link #confirmPayment(Activity, ConfirmPaymentIntentParams)}")
    public final PaymentIntent confirmPaymentIntentSynchronous(ConfirmPaymentIntentParams confirmPaymentIntentParams, String str) throws AuthenticationException, InvalidRequestException, APIConnectionException, APIException {
        Intrinsics.checkParameterIsNotNull(confirmPaymentIntentParams, "confirmPaymentIntentParams");
        return StripeRepository.DefaultImpls.confirmPaymentIntent$default(this.stripeRepository, confirmPaymentIntentParams, new ApiRequest.Options(this.publishableKey, this.stripeAccountId, str), (List) null, 4, (Object) null);
    }

    public static /* synthetic */ void confirmSetupIntent$default(Stripe stripe, Activity activity, ConfirmSetupIntentParams confirmSetupIntentParams, String str, int i, Object obj) {
        if ((i & 4) != 0) {
            str = stripe.stripeAccountId;
        }
        stripe.confirmSetupIntent(activity, confirmSetupIntentParams, str);
    }

    public final void confirmSetupIntent(Activity activity, ConfirmSetupIntentParams confirmSetupIntentParams, String str) {
        Intrinsics.checkParameterIsNotNull(activity, "activity");
        Intrinsics.checkParameterIsNotNull(confirmSetupIntentParams, "confirmSetupIntentParams");
        this.paymentController.startConfirmAndAuth(AuthActivityStarter.Host.Companion.create$stripe_release(activity), confirmSetupIntentParams, new ApiRequest.Options(this.publishableKey, str, (String) null, 4, (DefaultConstructorMarker) null));
    }

    public static /* synthetic */ void confirmSetupIntent$default(Stripe stripe, Fragment fragment, ConfirmSetupIntentParams confirmSetupIntentParams, String str, int i, Object obj) {
        if ((i & 4) != 0) {
            str = stripe.stripeAccountId;
        }
        stripe.confirmSetupIntent(fragment, confirmSetupIntentParams, str);
    }

    public final void confirmSetupIntent(Fragment fragment, ConfirmSetupIntentParams confirmSetupIntentParams, String str) {
        Intrinsics.checkParameterIsNotNull(fragment, "fragment");
        Intrinsics.checkParameterIsNotNull(confirmSetupIntentParams, "confirmSetupIntentParams");
        this.paymentController.startConfirmAndAuth(AuthActivityStarter.Host.Companion.create$stripe_release(fragment), confirmSetupIntentParams, new ApiRequest.Options(this.publishableKey, str, (String) null, 4, (DefaultConstructorMarker) null));
    }

    @Deprecated(message = "Rename to better reflect behavior and match iOS naming.", replaceWith = @ReplaceWith(expression = "handleNextActionForSetupIntent(activity, clientSecret)", imports = {}))
    public final void authenticateSetup(Activity activity, String str) {
        Intrinsics.checkParameterIsNotNull(activity, "activity");
        Intrinsics.checkParameterIsNotNull(str, "clientSecret");
        this.paymentController.startAuth(AuthActivityStarter.Host.Companion.create$stripe_release(activity), new SetupIntent.ClientSecret(str).getValue$stripe_release(), new ApiRequest.Options(this.publishableKey, this.stripeAccountId, (String) null, 4, (DefaultConstructorMarker) null));
    }

    public static /* synthetic */ void handleNextActionForSetupIntent$default(Stripe stripe, Activity activity, String str, String str2, int i, Object obj) {
        if ((i & 4) != 0) {
            str2 = stripe.stripeAccountId;
        }
        stripe.handleNextActionForSetupIntent(activity, str, str2);
    }

    public final void handleNextActionForSetupIntent(Activity activity, String str, String str2) {
        Intrinsics.checkParameterIsNotNull(activity, "activity");
        Intrinsics.checkParameterIsNotNull(str, "clientSecret");
        this.paymentController.startAuth(AuthActivityStarter.Host.Companion.create$stripe_release(activity), new SetupIntent.ClientSecret(str).getValue$stripe_release(), new ApiRequest.Options(this.publishableKey, str2, (String) null, 4, (DefaultConstructorMarker) null));
    }

    @Deprecated(message = "Rename to better reflect behavior and match iOS naming.", replaceWith = @ReplaceWith(expression = "handleNextActionForSetupIntent(fragment, clientSecret)", imports = {}))
    public final void authenticateSetup(Fragment fragment, String str) {
        Intrinsics.checkParameterIsNotNull(fragment, "fragment");
        Intrinsics.checkParameterIsNotNull(str, "clientSecret");
        this.paymentController.startAuth(AuthActivityStarter.Host.Companion.create$stripe_release(fragment), new SetupIntent.ClientSecret(str).getValue$stripe_release(), new ApiRequest.Options(this.publishableKey, this.stripeAccountId, (String) null, 4, (DefaultConstructorMarker) null));
    }

    public static /* synthetic */ void handleNextActionForSetupIntent$default(Stripe stripe, Fragment fragment, String str, String str2, int i, Object obj) {
        if ((i & 4) != 0) {
            str2 = stripe.stripeAccountId;
        }
        stripe.handleNextActionForSetupIntent(fragment, str, str2);
    }

    public final void handleNextActionForSetupIntent(Fragment fragment, String str, String str2) {
        Intrinsics.checkParameterIsNotNull(fragment, "fragment");
        Intrinsics.checkParameterIsNotNull(str, "clientSecret");
        this.paymentController.startAuth(AuthActivityStarter.Host.Companion.create$stripe_release(fragment), new SetupIntent.ClientSecret(str).getValue$stripe_release(), new ApiRequest.Options(this.publishableKey, str2, (String) null, 4, (DefaultConstructorMarker) null));
    }

    public final boolean onSetupResult(int i, Intent intent, ApiResultCallback<SetupIntentResult> apiResultCallback) {
        Intrinsics.checkParameterIsNotNull(apiResultCallback, "callback");
        if (intent == null || !this.paymentController.shouldHandleSetupResult(i, intent)) {
            return false;
        }
        this.paymentController.handleSetupResult(intent, apiResultCallback);
        return true;
    }

    public static /* synthetic */ void retrieveSetupIntent$default(Stripe stripe, String str, String str2, ApiResultCallback apiResultCallback, int i, Object obj) throws APIException, AuthenticationException, InvalidRequestException, APIConnectionException {
        if ((i & 2) != 0) {
            str2 = stripe.stripeAccountId;
        }
        stripe.retrieveSetupIntent(str, str2, apiResultCallback);
    }

    public final void retrieveSetupIntent(String str, String str2, ApiResultCallback<SetupIntent> apiResultCallback) throws APIException, AuthenticationException, InvalidRequestException, APIConnectionException {
        Intrinsics.checkParameterIsNotNull(str, "clientSecret");
        Intrinsics.checkParameterIsNotNull(apiResultCallback, "callback");
        new RetrieveSetupIntentTask(this.stripeRepository, str, new ApiRequest.Options(this.publishableKey, str2, (String) null, 4, (DefaultConstructorMarker) null), this.workScope, apiResultCallback).execute$stripe_release();
    }

    public static /* synthetic */ SetupIntent retrieveSetupIntentSynchronous$default(Stripe stripe, String str, String str2, int i, Object obj) throws APIException, AuthenticationException, InvalidRequestException, APIConnectionException {
        if ((i & 2) != 0) {
            str2 = stripe.stripeAccountId;
        }
        return stripe.retrieveSetupIntentSynchronous(str, str2);
    }

    public final SetupIntent retrieveSetupIntentSynchronous(String str, String str2) throws APIException, AuthenticationException, InvalidRequestException, APIConnectionException {
        Intrinsics.checkParameterIsNotNull(str, "clientSecret");
        return StripeRepository.DefaultImpls.retrieveSetupIntent$default(this.stripeRepository, new SetupIntent.ClientSecret(str).getValue$stripe_release(), new ApiRequest.Options(this.publishableKey, str2, (String) null, 4, (DefaultConstructorMarker) null), (List) null, 4, (Object) null);
    }

    public static /* synthetic */ SetupIntent confirmSetupIntentSynchronous$default(Stripe stripe, ConfirmSetupIntentParams confirmSetupIntentParams, String str, int i, Object obj) throws AuthenticationException, InvalidRequestException, APIConnectionException, APIException {
        if ((i & 2) != 0) {
            str = null;
        }
        return stripe.confirmSetupIntentSynchronous(confirmSetupIntentParams, str);
    }

    @Deprecated(message = "use {@link #confirmSetupIntent(Activity, ConfirmSetupIntentParams)}")
    public final SetupIntent confirmSetupIntentSynchronous(ConfirmSetupIntentParams confirmSetupIntentParams, String str) throws AuthenticationException, InvalidRequestException, APIConnectionException, APIException {
        Intrinsics.checkParameterIsNotNull(confirmSetupIntentParams, "confirmSetupIntentParams");
        return StripeRepository.DefaultImpls.confirmSetupIntent$default(this.stripeRepository, confirmSetupIntentParams, new ApiRequest.Options(this.publishableKey, this.stripeAccountId, str), (List) null, 4, (Object) null);
    }

    public static /* synthetic */ void createPaymentMethod$default(Stripe stripe, PaymentMethodCreateParams paymentMethodCreateParams, String str, String str2, ApiResultCallback apiResultCallback, int i, Object obj) {
        if ((i & 2) != 0) {
            str = null;
        }
        if ((i & 4) != 0) {
            str2 = stripe.stripeAccountId;
        }
        stripe.createPaymentMethod(paymentMethodCreateParams, str, str2, apiResultCallback);
    }

    public final void createPaymentMethod(PaymentMethodCreateParams paymentMethodCreateParams, String str, String str2, ApiResultCallback<PaymentMethod> apiResultCallback) {
        Intrinsics.checkParameterIsNotNull(paymentMethodCreateParams, "paymentMethodCreateParams");
        Intrinsics.checkParameterIsNotNull(apiResultCallback, "callback");
        new CreatePaymentMethodTask(this.stripeRepository, paymentMethodCreateParams, new ApiRequest.Options(this.publishableKey, str2, str), this.workScope, apiResultCallback).execute$stripe_release();
    }

    public static /* synthetic */ PaymentMethod createPaymentMethodSynchronous$default(Stripe stripe, PaymentMethodCreateParams paymentMethodCreateParams, String str, String str2, int i, Object obj) throws APIException, AuthenticationException, InvalidRequestException, APIConnectionException {
        if ((i & 2) != 0) {
            str = null;
        }
        if ((i & 4) != 0) {
            str2 = stripe.stripeAccountId;
        }
        return stripe.createPaymentMethodSynchronous(paymentMethodCreateParams, str, str2);
    }

    public final PaymentMethod createPaymentMethodSynchronous(PaymentMethodCreateParams paymentMethodCreateParams, String str, String str2) throws APIException, AuthenticationException, InvalidRequestException, APIConnectionException {
        Intrinsics.checkParameterIsNotNull(paymentMethodCreateParams, "paymentMethodCreateParams");
        return this.stripeRepository.createPaymentMethod(paymentMethodCreateParams, new ApiRequest.Options(this.publishableKey, str2, str));
    }

    public static /* synthetic */ void authenticateSource$default(Stripe stripe, Activity activity, Source source, String str, int i, Object obj) {
        if ((i & 4) != 0) {
            str = stripe.stripeAccountId;
        }
        stripe.authenticateSource(activity, source, str);
    }

    public final void authenticateSource(Activity activity, Source source, String str) {
        Intrinsics.checkParameterIsNotNull(activity, "activity");
        Intrinsics.checkParameterIsNotNull(source, "source");
        this.paymentController.startAuthenticateSource(AuthActivityStarter.Host.Companion.create$stripe_release(activity), source, new ApiRequest.Options(this.publishableKey, str, (String) null, 4, (DefaultConstructorMarker) null));
    }

    public static /* synthetic */ void authenticateSource$default(Stripe stripe, Fragment fragment, Source source, String str, int i, Object obj) {
        if ((i & 4) != 0) {
            str = stripe.stripeAccountId;
        }
        stripe.authenticateSource(fragment, source, str);
    }

    public final void authenticateSource(Fragment fragment, Source source, String str) {
        Intrinsics.checkParameterIsNotNull(fragment, "fragment");
        Intrinsics.checkParameterIsNotNull(source, "source");
        this.paymentController.startAuthenticateSource(AuthActivityStarter.Host.Companion.create$stripe_release(fragment), source, new ApiRequest.Options(this.publishableKey, str, (String) null, 4, (DefaultConstructorMarker) null));
    }

    public final boolean isAuthenticateSourceResult(int i, Intent intent) {
        return intent != null && this.paymentController.shouldHandleSourceResult(i, intent);
    }

    public final void onAuthenticateSourceResult(Intent intent, ApiResultCallback<Source> apiResultCallback) {
        Intrinsics.checkParameterIsNotNull(intent, "data");
        Intrinsics.checkParameterIsNotNull(apiResultCallback, "callback");
        this.paymentController.handleSourceResult(intent, apiResultCallback);
    }

    public static /* synthetic */ void createSource$default(Stripe stripe, SourceParams sourceParams, String str, String str2, ApiResultCallback apiResultCallback, int i, Object obj) {
        if ((i & 2) != 0) {
            str = null;
        }
        if ((i & 4) != 0) {
            str2 = stripe.stripeAccountId;
        }
        stripe.createSource(sourceParams, str, str2, apiResultCallback);
    }

    public final void createSource(SourceParams sourceParams, String str, String str2, ApiResultCallback<Source> apiResultCallback) {
        Intrinsics.checkParameterIsNotNull(sourceParams, "sourceParams");
        Intrinsics.checkParameterIsNotNull(apiResultCallback, "callback");
        new CreateSourceTask(this.stripeRepository, sourceParams, new ApiRequest.Options(this.publishableKey, str2, str), this.workScope, apiResultCallback).execute$stripe_release();
    }

    public static /* synthetic */ Source createSourceSynchronous$default(Stripe stripe, SourceParams sourceParams, String str, String str2, int i, Object obj) throws AuthenticationException, InvalidRequestException, APIConnectionException, APIException {
        if ((i & 2) != 0) {
            str = null;
        }
        if ((i & 4) != 0) {
            str2 = stripe.stripeAccountId;
        }
        return stripe.createSourceSynchronous(sourceParams, str, str2);
    }

    public final Source createSourceSynchronous(SourceParams sourceParams, String str, String str2) throws AuthenticationException, InvalidRequestException, APIConnectionException, APIException {
        Intrinsics.checkParameterIsNotNull(sourceParams, NativeProtocol.WEB_DIALOG_PARAMS);
        return this.stripeRepository.createSource(sourceParams, new ApiRequest.Options(this.publishableKey, str2, str));
    }

    public static /* synthetic */ void retrieveSource$default(Stripe stripe, String str, String str2, String str3, ApiResultCallback apiResultCallback, int i, Object obj) {
        if ((i & 4) != 0) {
            str3 = stripe.stripeAccountId;
        }
        stripe.retrieveSource(str, str2, str3, apiResultCallback);
    }

    public final void retrieveSource(String str, String str2, String str3, ApiResultCallback<Source> apiResultCallback) {
        Intrinsics.checkParameterIsNotNull(str, "sourceId");
        Intrinsics.checkParameterIsNotNull(str2, "clientSecret");
        Intrinsics.checkParameterIsNotNull(apiResultCallback, "callback");
        new RetrieveSourceTask(this.stripeRepository, str, str2, new ApiRequest.Options(this.publishableKey, str3, (String) null, 4, (DefaultConstructorMarker) null), this.workScope, apiResultCallback).execute$stripe_release();
    }

    public static /* synthetic */ Source retrieveSourceSynchronous$default(Stripe stripe, String str, String str2, String str3, int i, Object obj) throws AuthenticationException, InvalidRequestException, APIConnectionException, APIException {
        if ((i & 4) != 0) {
            str3 = stripe.stripeAccountId;
        }
        return stripe.retrieveSourceSynchronous(str, str2, str3);
    }

    public final Source retrieveSourceSynchronous(String str, String str2, String str3) throws AuthenticationException, InvalidRequestException, APIConnectionException, APIException {
        Intrinsics.checkParameterIsNotNull(str, "sourceId");
        Intrinsics.checkParameterIsNotNull(str2, "clientSecret");
        return this.stripeRepository.retrieveSource(str, str2, new ApiRequest.Options(this.publishableKey, str3, (String) null, 4, (DefaultConstructorMarker) null));
    }

    public static /* synthetic */ void createAccountToken$default(Stripe stripe, AccountParams accountParams, String str, String str2, ApiResultCallback apiResultCallback, int i, Object obj) {
        if ((i & 2) != 0) {
            str = null;
        }
        if ((i & 4) != 0) {
            str2 = stripe.stripeAccountId;
        }
        stripe.createAccountToken(accountParams, str, str2, apiResultCallback);
    }

    public final void createAccountToken(AccountParams accountParams, String str, String str2, ApiResultCallback<Token> apiResultCallback) {
        Intrinsics.checkParameterIsNotNull(accountParams, "accountParams");
        Intrinsics.checkParameterIsNotNull(apiResultCallback, "callback");
        createToken(accountParams, str2, str, apiResultCallback);
    }

    public static /* synthetic */ Token createAccountTokenSynchronous$default(Stripe stripe, AccountParams accountParams, String str, String str2, int i, Object obj) throws AuthenticationException, InvalidRequestException, APIConnectionException, APIException {
        if ((i & 2) != 0) {
            str = null;
        }
        if ((i & 4) != 0) {
            str2 = stripe.stripeAccountId;
        }
        return stripe.createAccountTokenSynchronous(accountParams, str, str2);
    }

    public final Token createAccountTokenSynchronous(AccountParams accountParams, String str, String str2) throws AuthenticationException, InvalidRequestException, APIConnectionException, APIException {
        Intrinsics.checkParameterIsNotNull(accountParams, "accountParams");
        try {
            return this.stripeRepository.createToken(accountParams, new ApiRequest.Options(this.publishableKey, str2, str));
        } catch (CardException unused) {
            return null;
        }
    }

    public static /* synthetic */ void createBankAccountToken$default(Stripe stripe, BankAccountTokenParams bankAccountTokenParams, String str, String str2, ApiResultCallback apiResultCallback, int i, Object obj) {
        if ((i & 2) != 0) {
            str = null;
        }
        if ((i & 4) != 0) {
            str2 = stripe.stripeAccountId;
        }
        stripe.createBankAccountToken(bankAccountTokenParams, str, str2, (ApiResultCallback<Token>) apiResultCallback);
    }

    public final void createBankAccountToken(BankAccountTokenParams bankAccountTokenParams, String str, String str2, ApiResultCallback<Token> apiResultCallback) {
        Intrinsics.checkParameterIsNotNull(bankAccountTokenParams, "bankAccountTokenParams");
        Intrinsics.checkParameterIsNotNull(apiResultCallback, "callback");
        createToken(bankAccountTokenParams, str2, str, apiResultCallback);
    }

    public static /* synthetic */ Token createBankAccountTokenSynchronous$default(Stripe stripe, BankAccountTokenParams bankAccountTokenParams, String str, String str2, int i, Object obj) throws AuthenticationException, InvalidRequestException, APIConnectionException, CardException, APIException {
        if ((i & 2) != 0) {
            str = null;
        }
        if ((i & 4) != 0) {
            str2 = stripe.stripeAccountId;
        }
        return stripe.createBankAccountTokenSynchronous(bankAccountTokenParams, str, str2);
    }

    public final Token createBankAccountTokenSynchronous(BankAccountTokenParams bankAccountTokenParams, String str, String str2) throws AuthenticationException, InvalidRequestException, APIConnectionException, CardException, APIException {
        Intrinsics.checkParameterIsNotNull(bankAccountTokenParams, "bankAccountTokenParams");
        return this.stripeRepository.createToken(bankAccountTokenParams, new ApiRequest.Options(this.publishableKey, str2, str));
    }

    public static /* synthetic */ void createBankAccountToken$default(Stripe stripe, BankAccount bankAccount, String str, String str2, ApiResultCallback apiResultCallback, int i, Object obj) {
        if ((i & 2) != 0) {
            str = null;
        }
        if ((i & 4) != 0) {
            str2 = stripe.stripeAccountId;
        }
        stripe.createBankAccountToken(bankAccount, str, str2, (ApiResultCallback<Token>) apiResultCallback);
    }

    @Deprecated(message = "Use BankAccountTokenParams")
    public final void createBankAccountToken(BankAccount bankAccount, String str, String str2, ApiResultCallback<Token> apiResultCallback) {
        Intrinsics.checkParameterIsNotNull(bankAccount, "bankAccount");
        Intrinsics.checkParameterIsNotNull(apiResultCallback, "callback");
        createToken(bankAccount, str2, str, apiResultCallback);
    }

    public static /* synthetic */ Token createBankAccountTokenSynchronous$default(Stripe stripe, BankAccount bankAccount, String str, int i, Object obj) throws AuthenticationException, InvalidRequestException, APIConnectionException, CardException, APIException {
        if ((i & 2) != 0) {
            str = null;
        }
        return stripe.createBankAccountTokenSynchronous(bankAccount, str);
    }

    @Deprecated(message = "Use BankAccountTokenParams")
    public final Token createBankAccountTokenSynchronous(BankAccount bankAccount, String str) throws AuthenticationException, InvalidRequestException, APIConnectionException, CardException, APIException {
        Intrinsics.checkParameterIsNotNull(bankAccount, "bankAccount");
        return this.stripeRepository.createToken(bankAccount, new ApiRequest.Options(this.publishableKey, this.stripeAccountId, str));
    }

    public static /* synthetic */ void createPiiToken$default(Stripe stripe, String str, String str2, String str3, ApiResultCallback apiResultCallback, int i, Object obj) {
        if ((i & 2) != 0) {
            str2 = null;
        }
        if ((i & 4) != 0) {
            str3 = stripe.stripeAccountId;
        }
        stripe.createPiiToken(str, str2, str3, apiResultCallback);
    }

    public final void createPiiToken(String str, String str2, String str3, ApiResultCallback<Token> apiResultCallback) {
        Intrinsics.checkParameterIsNotNull(str, "personalId");
        Intrinsics.checkParameterIsNotNull(apiResultCallback, "callback");
        createToken(new PiiTokenParams(str), str3, str2, apiResultCallback);
    }

    public static /* synthetic */ Token createPiiTokenSynchronous$default(Stripe stripe, String str, String str2, String str3, int i, Object obj) throws AuthenticationException, InvalidRequestException, APIConnectionException, CardException, APIException {
        if ((i & 2) != 0) {
            str2 = null;
        }
        if ((i & 4) != 0) {
            str3 = stripe.stripeAccountId;
        }
        return stripe.createPiiTokenSynchronous(str, str2, str3);
    }

    public final Token createPiiTokenSynchronous(String str, String str2, String str3) throws AuthenticationException, InvalidRequestException, APIConnectionException, CardException, APIException {
        Intrinsics.checkParameterIsNotNull(str, "personalId");
        return this.stripeRepository.createToken(new PiiTokenParams(str), new ApiRequest.Options(this.publishableKey, str3, str2));
    }

    public static /* synthetic */ void createToken$default(Stripe stripe, Card card, String str, ApiResultCallback apiResultCallback, int i, Object obj) {
        if ((i & 2) != 0) {
            str = null;
        }
        stripe.createToken(card, str, apiResultCallback);
    }

    @Deprecated(message = "Deprecated, replace with Stripe#createCardToken()", replaceWith = @ReplaceWith(expression = "createCardToken(card, idempotencyKey, callback)", imports = {}))
    public final void createToken(Card card, String str, ApiResultCallback<Token> apiResultCallback) {
        Intrinsics.checkParameterIsNotNull(card, "card");
        Intrinsics.checkParameterIsNotNull(apiResultCallback, "callback");
        createCardToken$default(this, card, str, (String) null, apiResultCallback, 4, (Object) null);
    }

    public static /* synthetic */ void createCardToken$default(Stripe stripe, Card card, String str, String str2, ApiResultCallback apiResultCallback, int i, Object obj) {
        if ((i & 2) != 0) {
            str = null;
        }
        if ((i & 4) != 0) {
            str2 = stripe.stripeAccountId;
        }
        stripe.createCardToken(card, str, str2, apiResultCallback);
    }

    public final void createCardToken(Card card, String str, String str2, ApiResultCallback<Token> apiResultCallback) {
        Intrinsics.checkParameterIsNotNull(card, "card");
        Intrinsics.checkParameterIsNotNull(apiResultCallback, "callback");
        createToken(card, str2, str, apiResultCallback);
    }

    public static /* synthetic */ Token createCardTokenSynchronous$default(Stripe stripe, Card card, String str, String str2, int i, Object obj) throws AuthenticationException, InvalidRequestException, APIConnectionException, CardException, APIException {
        if ((i & 2) != 0) {
            str = null;
        }
        if ((i & 4) != 0) {
            str2 = stripe.stripeAccountId;
        }
        return stripe.createCardTokenSynchronous(card, str, str2);
    }

    public final Token createCardTokenSynchronous(Card card, String str, String str2) throws AuthenticationException, InvalidRequestException, APIConnectionException, CardException, APIException {
        Intrinsics.checkParameterIsNotNull(card, "card");
        return this.stripeRepository.createToken(card, new ApiRequest.Options(this.publishableKey, str2, str));
    }

    public static /* synthetic */ void createCvcUpdateToken$default(Stripe stripe, String str, String str2, String str3, ApiResultCallback apiResultCallback, int i, Object obj) {
        if ((i & 2) != 0) {
            str2 = null;
        }
        if ((i & 4) != 0) {
            str3 = stripe.stripeAccountId;
        }
        stripe.createCvcUpdateToken(str, str2, str3, apiResultCallback);
    }

    public final void createCvcUpdateToken(String str, String str2, String str3, ApiResultCallback<Token> apiResultCallback) {
        Intrinsics.checkParameterIsNotNull(str, "cvc");
        Intrinsics.checkParameterIsNotNull(apiResultCallback, "callback");
        createToken(new CvcTokenParams(str), str3, str2, apiResultCallback);
    }

    public static /* synthetic */ Token createCvcUpdateTokenSynchronous$default(Stripe stripe, String str, String str2, String str3, int i, Object obj) throws AuthenticationException, InvalidRequestException, APIConnectionException, CardException, APIException {
        if ((i & 2) != 0) {
            str2 = null;
        }
        if ((i & 4) != 0) {
            str3 = stripe.stripeAccountId;
        }
        return stripe.createCvcUpdateTokenSynchronous(str, str2, str3);
    }

    public final Token createCvcUpdateTokenSynchronous(String str, String str2, String str3) throws AuthenticationException, InvalidRequestException, APIConnectionException, CardException, APIException {
        Intrinsics.checkParameterIsNotNull(str, "cvc");
        return this.stripeRepository.createToken(new CvcTokenParams(str), new ApiRequest.Options(this.publishableKey, str3, str2));
    }

    public static /* synthetic */ void createPersonToken$default(Stripe stripe, PersonTokenParams personTokenParams, String str, String str2, ApiResultCallback apiResultCallback, int i, Object obj) {
        if ((i & 2) != 0) {
            str = null;
        }
        if ((i & 4) != 0) {
            str2 = stripe.stripeAccountId;
        }
        stripe.createPersonToken(personTokenParams, str, str2, apiResultCallback);
    }

    public final void createPersonToken(PersonTokenParams personTokenParams, String str, String str2, ApiResultCallback<Token> apiResultCallback) {
        Intrinsics.checkParameterIsNotNull(personTokenParams, NativeProtocol.WEB_DIALOG_PARAMS);
        Intrinsics.checkParameterIsNotNull(apiResultCallback, "callback");
        createToken(personTokenParams, str2, str, apiResultCallback);
    }

    public static /* synthetic */ Token createPersonTokenSynchronous$default(Stripe stripe, PersonTokenParams personTokenParams, String str, String str2, int i, Object obj) throws AuthenticationException, InvalidRequestException, APIConnectionException, CardException, APIException {
        if ((i & 2) != 0) {
            str = null;
        }
        if ((i & 4) != 0) {
            str2 = stripe.stripeAccountId;
        }
        return stripe.createPersonTokenSynchronous(personTokenParams, str, str2);
    }

    public final Token createPersonTokenSynchronous(PersonTokenParams personTokenParams, String str, String str2) throws AuthenticationException, InvalidRequestException, APIConnectionException, CardException, APIException {
        Intrinsics.checkParameterIsNotNull(personTokenParams, NativeProtocol.WEB_DIALOG_PARAMS);
        return this.stripeRepository.createToken(personTokenParams, new ApiRequest.Options(this.publishableKey, str2, str));
    }

    static /* synthetic */ void createToken$default(Stripe stripe, TokenParams tokenParams, String str, String str2, ApiResultCallback apiResultCallback, int i, Object obj) {
        if ((i & 4) != 0) {
            str2 = null;
        }
        stripe.createToken(tokenParams, str, str2, apiResultCallback);
    }

    private final void createToken(TokenParams tokenParams, String str, String str2, ApiResultCallback<Token> apiResultCallback) {
        new CreateTokenTask(this.stripeRepository, tokenParams, new ApiRequest.Options(this.publishableKey, str, str2), this.workScope, apiResultCallback).execute$stripe_release();
    }

    public static /* synthetic */ void createFile$default(Stripe stripe, StripeFileParams stripeFileParams, String str, String str2, ApiResultCallback apiResultCallback, int i, Object obj) {
        if ((i & 2) != 0) {
            str = null;
        }
        if ((i & 4) != 0) {
            str2 = stripe.stripeAccountId;
        }
        stripe.createFile(stripeFileParams, str, str2, apiResultCallback);
    }

    public final void createFile(StripeFileParams stripeFileParams, String str, String str2, ApiResultCallback<StripeFile> apiResultCallback) {
        Intrinsics.checkParameterIsNotNull(stripeFileParams, "fileParams");
        Intrinsics.checkParameterIsNotNull(apiResultCallback, "callback");
        new CreateFileTask(this.stripeRepository, stripeFileParams, new ApiRequest.Options(this.publishableKey, str2, str), this.workScope, apiResultCallback).execute$stripe_release();
    }

    public static /* synthetic */ StripeFile createFileSynchronous$default(Stripe stripe, StripeFileParams stripeFileParams, String str, String str2, int i, Object obj) {
        if ((i & 2) != 0) {
            str = null;
        }
        if ((i & 4) != 0) {
            str2 = stripe.stripeAccountId;
        }
        return stripe.createFileSynchronous(stripeFileParams, str, str2);
    }

    public final StripeFile createFileSynchronous(StripeFileParams stripeFileParams, String str, String str2) {
        Intrinsics.checkParameterIsNotNull(stripeFileParams, "fileParams");
        return this.stripeRepository.createFile(stripeFileParams, new ApiRequest.Options(this.publishableKey, str2, str));
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B7\b\u0000\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\b\u0012\b\b\u0002\u0010\t\u001a\u00020\n\u0012\f\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\u00020\f¢\u0006\u0002\u0010\rJ\u0015\u0010\u000e\u001a\u0004\u0018\u00010\u0002H@ø\u0001\u0000¢\u0006\u0004\b\u000f\u0010\u0010R\u000e\u0010\u0007\u001a\u00020\bX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0004¢\u0006\u0002\n\u0000\u0002\u0004\n\u0002\b\u0019¨\u0006\u0011"}, d2 = {"Lcom/stripe/android/Stripe$CreateSourceTask;", "Lcom/stripe/android/ApiOperation;", "Lcom/stripe/android/model/Source;", "stripeRepository", "Lcom/stripe/android/StripeRepository;", "sourceParams", "Lcom/stripe/android/model/SourceParams;", "options", "Lcom/stripe/android/ApiRequest$Options;", "workScope", "Lkotlinx/coroutines/CoroutineScope;", "callback", "Lcom/stripe/android/ApiResultCallback;", "(Lcom/stripe/android/StripeRepository;Lcom/stripe/android/model/SourceParams;Lcom/stripe/android/ApiRequest$Options;Lkotlinx/coroutines/CoroutineScope;Lcom/stripe/android/ApiResultCallback;)V", "getResult", "getResult$stripe_release", "(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: Stripe.kt */
    private static final class CreateSourceTask extends ApiOperation<Source> {
        private final ApiRequest.Options options;
        private final SourceParams sourceParams;
        private final StripeRepository stripeRepository;

        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ CreateSourceTask(StripeRepository stripeRepository2, SourceParams sourceParams2, ApiRequest.Options options2, CoroutineScope coroutineScope, ApiResultCallback apiResultCallback, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this(stripeRepository2, sourceParams2, options2, (i & 8) != 0 ? CoroutineScopeKt.CoroutineScope(Dispatchers.getIO()) : coroutineScope, apiResultCallback);
        }

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public CreateSourceTask(StripeRepository stripeRepository2, SourceParams sourceParams2, ApiRequest.Options options2, CoroutineScope coroutineScope, ApiResultCallback<Source> apiResultCallback) {
            super(coroutineScope, apiResultCallback);
            Intrinsics.checkParameterIsNotNull(stripeRepository2, "stripeRepository");
            Intrinsics.checkParameterIsNotNull(sourceParams2, "sourceParams");
            Intrinsics.checkParameterIsNotNull(options2, "options");
            Intrinsics.checkParameterIsNotNull(coroutineScope, "workScope");
            Intrinsics.checkParameterIsNotNull(apiResultCallback, "callback");
            this.stripeRepository = stripeRepository2;
            this.sourceParams = sourceParams2;
            this.options = options2;
        }

        public Object getResult$stripe_release(Continuation<? super Source> continuation) throws StripeException {
            return this.stripeRepository.createSource(this.sourceParams, this.options);
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B?\b\u0000\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0006\u0012\u0006\u0010\b\u001a\u00020\t\u0012\b\b\u0002\u0010\n\u001a\u00020\u000b\u0012\f\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u00020\r¢\u0006\u0002\u0010\u000eJ\u0015\u0010\u000f\u001a\u0004\u0018\u00010\u0002H@ø\u0001\u0000¢\u0006\u0004\b\u0010\u0010\u0011R\u000e\u0010\u0007\u001a\u00020\u0006X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0004¢\u0006\u0002\n\u0000\u0002\u0004\n\u0002\b\u0019¨\u0006\u0012"}, d2 = {"Lcom/stripe/android/Stripe$RetrieveSourceTask;", "Lcom/stripe/android/ApiOperation;", "Lcom/stripe/android/model/Source;", "stripeRepository", "Lcom/stripe/android/StripeRepository;", "sourceId", "", "clientSecret", "options", "Lcom/stripe/android/ApiRequest$Options;", "workScope", "Lkotlinx/coroutines/CoroutineScope;", "callback", "Lcom/stripe/android/ApiResultCallback;", "(Lcom/stripe/android/StripeRepository;Ljava/lang/String;Ljava/lang/String;Lcom/stripe/android/ApiRequest$Options;Lkotlinx/coroutines/CoroutineScope;Lcom/stripe/android/ApiResultCallback;)V", "getResult", "getResult$stripe_release", "(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: Stripe.kt */
    private static final class RetrieveSourceTask extends ApiOperation<Source> {
        private final String clientSecret;
        private final ApiRequest.Options options;
        private final String sourceId;
        private final StripeRepository stripeRepository;

        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ RetrieveSourceTask(StripeRepository stripeRepository2, String str, String str2, ApiRequest.Options options2, CoroutineScope coroutineScope, ApiResultCallback apiResultCallback, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this(stripeRepository2, str, str2, options2, (i & 16) != 0 ? CoroutineScopeKt.CoroutineScope(Dispatchers.getIO()) : coroutineScope, apiResultCallback);
        }

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public RetrieveSourceTask(StripeRepository stripeRepository2, String str, String str2, ApiRequest.Options options2, CoroutineScope coroutineScope, ApiResultCallback<Source> apiResultCallback) {
            super(coroutineScope, apiResultCallback);
            Intrinsics.checkParameterIsNotNull(stripeRepository2, "stripeRepository");
            Intrinsics.checkParameterIsNotNull(str, "sourceId");
            Intrinsics.checkParameterIsNotNull(str2, "clientSecret");
            Intrinsics.checkParameterIsNotNull(options2, "options");
            Intrinsics.checkParameterIsNotNull(coroutineScope, "workScope");
            Intrinsics.checkParameterIsNotNull(apiResultCallback, "callback");
            this.stripeRepository = stripeRepository2;
            this.sourceId = str;
            this.clientSecret = str2;
            this.options = options2;
        }

        public Object getResult$stripe_release(Continuation<? super Source> continuation) throws StripeException {
            return this.stripeRepository.retrieveSource(this.sourceId, this.clientSecret, this.options);
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B7\b\u0000\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\b\u0012\b\b\u0002\u0010\t\u001a\u00020\n\u0012\f\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\u00020\f¢\u0006\u0002\u0010\rJ\u0015\u0010\u000e\u001a\u0004\u0018\u00010\u0002H@ø\u0001\u0000¢\u0006\u0004\b\u000f\u0010\u0010R\u000e\u0010\u0007\u001a\u00020\bX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0004¢\u0006\u0002\n\u0000\u0002\u0004\n\u0002\b\u0019¨\u0006\u0011"}, d2 = {"Lcom/stripe/android/Stripe$CreatePaymentMethodTask;", "Lcom/stripe/android/ApiOperation;", "Lcom/stripe/android/model/PaymentMethod;", "stripeRepository", "Lcom/stripe/android/StripeRepository;", "paymentMethodCreateParams", "Lcom/stripe/android/model/PaymentMethodCreateParams;", "options", "Lcom/stripe/android/ApiRequest$Options;", "workScope", "Lkotlinx/coroutines/CoroutineScope;", "callback", "Lcom/stripe/android/ApiResultCallback;", "(Lcom/stripe/android/StripeRepository;Lcom/stripe/android/model/PaymentMethodCreateParams;Lcom/stripe/android/ApiRequest$Options;Lkotlinx/coroutines/CoroutineScope;Lcom/stripe/android/ApiResultCallback;)V", "getResult", "getResult$stripe_release", "(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: Stripe.kt */
    private static final class CreatePaymentMethodTask extends ApiOperation<PaymentMethod> {
        private final ApiRequest.Options options;
        private final PaymentMethodCreateParams paymentMethodCreateParams;
        private final StripeRepository stripeRepository;

        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ CreatePaymentMethodTask(StripeRepository stripeRepository2, PaymentMethodCreateParams paymentMethodCreateParams2, ApiRequest.Options options2, CoroutineScope coroutineScope, ApiResultCallback apiResultCallback, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this(stripeRepository2, paymentMethodCreateParams2, options2, (i & 8) != 0 ? CoroutineScopeKt.CoroutineScope(Dispatchers.getIO()) : coroutineScope, apiResultCallback);
        }

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public CreatePaymentMethodTask(StripeRepository stripeRepository2, PaymentMethodCreateParams paymentMethodCreateParams2, ApiRequest.Options options2, CoroutineScope coroutineScope, ApiResultCallback<PaymentMethod> apiResultCallback) {
            super(coroutineScope, apiResultCallback);
            Intrinsics.checkParameterIsNotNull(stripeRepository2, "stripeRepository");
            Intrinsics.checkParameterIsNotNull(paymentMethodCreateParams2, "paymentMethodCreateParams");
            Intrinsics.checkParameterIsNotNull(options2, "options");
            Intrinsics.checkParameterIsNotNull(coroutineScope, "workScope");
            Intrinsics.checkParameterIsNotNull(apiResultCallback, "callback");
            this.stripeRepository = stripeRepository2;
            this.paymentMethodCreateParams = paymentMethodCreateParams2;
            this.options = options2;
        }

        public Object getResult$stripe_release(Continuation<? super PaymentMethod> continuation) throws StripeException {
            return this.stripeRepository.createPaymentMethod(this.paymentMethodCreateParams, this.options);
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B7\b\u0000\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\b\u0012\b\b\u0002\u0010\t\u001a\u00020\n\u0012\f\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\u00020\f¢\u0006\u0002\u0010\rJ\u0015\u0010\u000e\u001a\u0004\u0018\u00010\u0002H@ø\u0001\u0000¢\u0006\u0004\b\u000f\u0010\u0010R\u000e\u0010\u0007\u001a\u00020\bX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0004¢\u0006\u0002\n\u0000\u0002\u0004\n\u0002\b\u0019¨\u0006\u0011"}, d2 = {"Lcom/stripe/android/Stripe$CreateTokenTask;", "Lcom/stripe/android/ApiOperation;", "Lcom/stripe/android/model/Token;", "stripeRepository", "Lcom/stripe/android/StripeRepository;", "tokenParams", "Lcom/stripe/android/model/TokenParams;", "options", "Lcom/stripe/android/ApiRequest$Options;", "workScope", "Lkotlinx/coroutines/CoroutineScope;", "callback", "Lcom/stripe/android/ApiResultCallback;", "(Lcom/stripe/android/StripeRepository;Lcom/stripe/android/model/TokenParams;Lcom/stripe/android/ApiRequest$Options;Lkotlinx/coroutines/CoroutineScope;Lcom/stripe/android/ApiResultCallback;)V", "getResult", "getResult$stripe_release", "(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: Stripe.kt */
    private static final class CreateTokenTask extends ApiOperation<Token> {
        private final ApiRequest.Options options;
        private final StripeRepository stripeRepository;
        private final TokenParams tokenParams;

        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ CreateTokenTask(StripeRepository stripeRepository2, TokenParams tokenParams2, ApiRequest.Options options2, CoroutineScope coroutineScope, ApiResultCallback apiResultCallback, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this(stripeRepository2, tokenParams2, options2, (i & 8) != 0 ? CoroutineScopeKt.CoroutineScope(Dispatchers.getIO()) : coroutineScope, apiResultCallback);
        }

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public CreateTokenTask(StripeRepository stripeRepository2, TokenParams tokenParams2, ApiRequest.Options options2, CoroutineScope coroutineScope, ApiResultCallback<Token> apiResultCallback) {
            super(coroutineScope, apiResultCallback);
            Intrinsics.checkParameterIsNotNull(stripeRepository2, "stripeRepository");
            Intrinsics.checkParameterIsNotNull(tokenParams2, "tokenParams");
            Intrinsics.checkParameterIsNotNull(options2, "options");
            Intrinsics.checkParameterIsNotNull(coroutineScope, "workScope");
            Intrinsics.checkParameterIsNotNull(apiResultCallback, "callback");
            this.stripeRepository = stripeRepository2;
            this.tokenParams = tokenParams2;
            this.options = options2;
        }

        public Object getResult$stripe_release(Continuation<? super Token> continuation) throws StripeException {
            return this.stripeRepository.createToken(this.tokenParams, this.options);
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B7\b\u0000\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\b\u0012\b\b\u0002\u0010\t\u001a\u00020\n\u0012\f\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\u00020\f¢\u0006\u0002\u0010\rJ\u0013\u0010\u000e\u001a\u00020\u0002H@ø\u0001\u0000¢\u0006\u0004\b\u000f\u0010\u0010R\u000e\u0010\u0005\u001a\u00020\u0006X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\bX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0004¢\u0006\u0002\n\u0000\u0002\u0004\n\u0002\b\u0019¨\u0006\u0011"}, d2 = {"Lcom/stripe/android/Stripe$CreateFileTask;", "Lcom/stripe/android/ApiOperation;", "Lcom/stripe/android/model/StripeFile;", "stripeRepository", "Lcom/stripe/android/StripeRepository;", "fileParams", "Lcom/stripe/android/model/StripeFileParams;", "options", "Lcom/stripe/android/ApiRequest$Options;", "workScope", "Lkotlinx/coroutines/CoroutineScope;", "callback", "Lcom/stripe/android/ApiResultCallback;", "(Lcom/stripe/android/StripeRepository;Lcom/stripe/android/model/StripeFileParams;Lcom/stripe/android/ApiRequest$Options;Lkotlinx/coroutines/CoroutineScope;Lcom/stripe/android/ApiResultCallback;)V", "getResult", "getResult$stripe_release", "(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: Stripe.kt */
    private static final class CreateFileTask extends ApiOperation<StripeFile> {
        private final StripeFileParams fileParams;
        private final ApiRequest.Options options;
        private final StripeRepository stripeRepository;

        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ CreateFileTask(StripeRepository stripeRepository2, StripeFileParams stripeFileParams, ApiRequest.Options options2, CoroutineScope coroutineScope, ApiResultCallback apiResultCallback, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this(stripeRepository2, stripeFileParams, options2, (i & 8) != 0 ? CoroutineScopeKt.CoroutineScope(Dispatchers.getIO()) : coroutineScope, apiResultCallback);
        }

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public CreateFileTask(StripeRepository stripeRepository2, StripeFileParams stripeFileParams, ApiRequest.Options options2, CoroutineScope coroutineScope, ApiResultCallback<StripeFile> apiResultCallback) {
            super(coroutineScope, apiResultCallback);
            Intrinsics.checkParameterIsNotNull(stripeRepository2, "stripeRepository");
            Intrinsics.checkParameterIsNotNull(stripeFileParams, "fileParams");
            Intrinsics.checkParameterIsNotNull(options2, "options");
            Intrinsics.checkParameterIsNotNull(coroutineScope, "workScope");
            Intrinsics.checkParameterIsNotNull(apiResultCallback, "callback");
            this.stripeRepository = stripeRepository2;
            this.fileParams = stripeFileParams;
            this.options = options2;
        }

        public Object getResult$stripe_release(Continuation<? super StripeFile> continuation) throws StripeException {
            return this.stripeRepository.createFile(this.fileParams, this.options);
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B7\b\u0000\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\b\u0012\b\b\u0002\u0010\t\u001a\u00020\n\u0012\f\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\u00020\f¢\u0006\u0002\u0010\rJ\u0015\u0010\u000e\u001a\u0004\u0018\u00010\u0002H@ø\u0001\u0000¢\u0006\u0004\b\u000f\u0010\u0010R\u000e\u0010\u0005\u001a\u00020\u0006X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\bX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0004¢\u0006\u0002\n\u0000\u0002\u0004\n\u0002\b\u0019¨\u0006\u0011"}, d2 = {"Lcom/stripe/android/Stripe$RetrievePaymentIntentTask;", "Lcom/stripe/android/ApiOperation;", "Lcom/stripe/android/model/PaymentIntent;", "stripeRepository", "Lcom/stripe/android/StripeRepository;", "clientSecret", "", "options", "Lcom/stripe/android/ApiRequest$Options;", "workScope", "Lkotlinx/coroutines/CoroutineScope;", "callback", "Lcom/stripe/android/ApiResultCallback;", "(Lcom/stripe/android/StripeRepository;Ljava/lang/String;Lcom/stripe/android/ApiRequest$Options;Lkotlinx/coroutines/CoroutineScope;Lcom/stripe/android/ApiResultCallback;)V", "getResult", "getResult$stripe_release", "(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: Stripe.kt */
    private static final class RetrievePaymentIntentTask extends ApiOperation<PaymentIntent> {
        private final String clientSecret;
        private final ApiRequest.Options options;
        private final StripeRepository stripeRepository;

        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ RetrievePaymentIntentTask(StripeRepository stripeRepository2, String str, ApiRequest.Options options2, CoroutineScope coroutineScope, ApiResultCallback apiResultCallback, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this(stripeRepository2, str, options2, (i & 8) != 0 ? CoroutineScopeKt.CoroutineScope(Dispatchers.getIO()) : coroutineScope, apiResultCallback);
        }

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public RetrievePaymentIntentTask(StripeRepository stripeRepository2, String str, ApiRequest.Options options2, CoroutineScope coroutineScope, ApiResultCallback<PaymentIntent> apiResultCallback) {
            super(coroutineScope, apiResultCallback);
            Intrinsics.checkParameterIsNotNull(stripeRepository2, "stripeRepository");
            Intrinsics.checkParameterIsNotNull(str, "clientSecret");
            Intrinsics.checkParameterIsNotNull(options2, "options");
            Intrinsics.checkParameterIsNotNull(coroutineScope, "workScope");
            Intrinsics.checkParameterIsNotNull(apiResultCallback, "callback");
            this.stripeRepository = stripeRepository2;
            this.clientSecret = str;
            this.options = options2;
        }

        public Object getResult$stripe_release(Continuation<? super PaymentIntent> continuation) throws StripeException {
            return StripeRepository.DefaultImpls.retrievePaymentIntent$default(this.stripeRepository, this.clientSecret, this.options, (List) null, 4, (Object) null);
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B7\b\u0000\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\b\u0012\b\b\u0002\u0010\t\u001a\u00020\n\u0012\f\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\u00020\f¢\u0006\u0002\u0010\rJ\u0015\u0010\u000e\u001a\u0004\u0018\u00010\u0002H@ø\u0001\u0000¢\u0006\u0004\b\u000f\u0010\u0010R\u000e\u0010\u0005\u001a\u00020\u0006X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\bX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0004¢\u0006\u0002\n\u0000\u0002\u0004\n\u0002\b\u0019¨\u0006\u0011"}, d2 = {"Lcom/stripe/android/Stripe$RetrieveSetupIntentTask;", "Lcom/stripe/android/ApiOperation;", "Lcom/stripe/android/model/SetupIntent;", "stripeRepository", "Lcom/stripe/android/StripeRepository;", "clientSecret", "", "options", "Lcom/stripe/android/ApiRequest$Options;", "workScope", "Lkotlinx/coroutines/CoroutineScope;", "callback", "Lcom/stripe/android/ApiResultCallback;", "(Lcom/stripe/android/StripeRepository;Ljava/lang/String;Lcom/stripe/android/ApiRequest$Options;Lkotlinx/coroutines/CoroutineScope;Lcom/stripe/android/ApiResultCallback;)V", "getResult", "getResult$stripe_release", "(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: Stripe.kt */
    private static final class RetrieveSetupIntentTask extends ApiOperation<SetupIntent> {
        private final String clientSecret;
        private final ApiRequest.Options options;
        private final StripeRepository stripeRepository;

        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ RetrieveSetupIntentTask(StripeRepository stripeRepository2, String str, ApiRequest.Options options2, CoroutineScope coroutineScope, ApiResultCallback apiResultCallback, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this(stripeRepository2, str, options2, (i & 8) != 0 ? CoroutineScopeKt.CoroutineScope(Dispatchers.getIO()) : coroutineScope, apiResultCallback);
        }

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public RetrieveSetupIntentTask(StripeRepository stripeRepository2, String str, ApiRequest.Options options2, CoroutineScope coroutineScope, ApiResultCallback<SetupIntent> apiResultCallback) {
            super(coroutineScope, apiResultCallback);
            Intrinsics.checkParameterIsNotNull(stripeRepository2, "stripeRepository");
            Intrinsics.checkParameterIsNotNull(str, "clientSecret");
            Intrinsics.checkParameterIsNotNull(options2, "options");
            Intrinsics.checkParameterIsNotNull(coroutineScope, "workScope");
            Intrinsics.checkParameterIsNotNull(apiResultCallback, "callback");
            this.stripeRepository = stripeRepository2;
            this.clientSecret = str;
            this.options = options2;
        }

        public Object getResult$stripe_release(Continuation<? super SetupIntent> continuation) throws StripeException {
            return StripeRepository.DefaultImpls.retrieveSetupIntent$default(this.stripeRepository, this.clientSecret, this.options, (List) null, 4, (Object) null);
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0006\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0010\u0010\u0003\u001a\u00020\u00048\u0006X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R$\u0010\u0006\u001a\u00020\u00078\u0006@\u0006X\u000e¢\u0006\u0014\n\u0000\u0012\u0004\b\b\u0010\u0002\u001a\u0004\b\t\u0010\n\"\u0004\b\u000b\u0010\fR&\u0010\r\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006X\u000e¢\u0006\u0014\n\u0000\u0012\u0004\b\u000f\u0010\u0002\u001a\u0004\b\u0010\u0010\u0011\"\u0004\b\u0012\u0010\u0013\u0002\u0004\n\u0002\b\u0019¨\u0006\u0014"}, d2 = {"Lcom/stripe/android/Stripe$Companion;", "", "()V", "API_VERSION", "", "VERSION", "advancedFraudSignalsEnabled", "", "advancedFraudSignalsEnabled$annotations", "getAdvancedFraudSignalsEnabled", "()Z", "setAdvancedFraudSignalsEnabled", "(Z)V", "appInfo", "Lcom/stripe/android/AppInfo;", "appInfo$annotations", "getAppInfo", "()Lcom/stripe/android/AppInfo;", "setAppInfo", "(Lcom/stripe/android/AppInfo;)V", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: Stripe.kt */
    public static final class Companion {
        @JvmStatic
        public static /* synthetic */ void advancedFraudSignalsEnabled$annotations() {
        }

        @JvmStatic
        public static /* synthetic */ void appInfo$annotations() {
        }

        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        public final AppInfo getAppInfo() {
            return Stripe.appInfo;
        }

        public final void setAppInfo(AppInfo appInfo) {
            Stripe.appInfo = appInfo;
        }

        public final boolean getAdvancedFraudSignalsEnabled() {
            return Stripe.advancedFraudSignalsEnabled;
        }

        public final void setAdvancedFraudSignalsEnabled(boolean z) {
            Stripe.advancedFraudSignalsEnabled = z;
        }
    }
}
