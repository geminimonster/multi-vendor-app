package com.stripe.android;

import androidx.lifecycle.LiveData;
import com.stripe.android.ApiRequest;
import com.stripe.android.exception.APIConnectionException;
import com.stripe.android.exception.APIException;
import com.stripe.android.exception.AuthenticationException;
import com.stripe.android.exception.CardException;
import com.stripe.android.exception.InvalidRequestException;
import com.stripe.android.model.ConfirmPaymentIntentParams;
import com.stripe.android.model.ConfirmSetupIntentParams;
import com.stripe.android.model.Customer;
import com.stripe.android.model.FpxBankStatuses;
import com.stripe.android.model.ListPaymentMethodsParams;
import com.stripe.android.model.PaymentIntent;
import com.stripe.android.model.PaymentMethod;
import com.stripe.android.model.PaymentMethodCreateParams;
import com.stripe.android.model.SetupIntent;
import com.stripe.android.model.ShippingInformation;
import com.stripe.android.model.Source;
import com.stripe.android.model.SourceParams;
import com.stripe.android.model.Stripe3ds2AuthResult;
import com.stripe.android.model.StripeFile;
import com.stripe.android.model.StripeFileParams;
import com.stripe.android.model.StripeIntent;
import com.stripe.android.model.Token;
import com.stripe.android.model.TokenParams;
import java.util.List;
import java.util.Set;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt;
import kotlin.coroutines.Continuation;
import org.json.JSONException;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000È\u0001\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\"\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\f\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\b`\u0018\u00002\u00020\u0001J@\u0010\u0002\u001a\u0004\u0018\u00010\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00052\f\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00050\b2\u0006\u0010\t\u001a\u00020\u00052\u0006\u0010\n\u001a\u00020\u00052\u0006\u0010\u000b\u001a\u00020\fH&J8\u0010\r\u001a\u0004\u0018\u00010\u000e2\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00052\f\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00050\b2\u0006\u0010\u000f\u001a\u00020\u00052\u0006\u0010\u000b\u001a\u00020\fH&J.\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u00132\u0006\u0010\t\u001a\u00020\u00052\u0006\u0010\u0014\u001a\u00020\f2\f\u0010\u0015\u001a\b\u0012\u0004\u0012\u00020\u00130\u0016H&J\"\u0010\u0017\u001a\u0004\u0018\u00010\u00182\u0006\u0010\u0019\u001a\u00020\u00052\u0006\u0010\t\u001a\u00020\u00052\u0006\u0010\u0014\u001a\u00020\fH&J\"\u0010\u001a\u001a\u0004\u0018\u00010\u001b2\u0006\u0010\u001c\u001a\u00020\u00052\u0006\u0010\t\u001a\u00020\u00052\u0006\u0010\u0014\u001a\u00020\fH&J&\u0010\u001d\u001a\u00020\u00112\u0006\u0010\t\u001a\u00020\u00052\u0006\u0010\u000b\u001a\u00020\f2\f\u0010\u0015\u001a\b\u0012\u0004\u0012\u00020\u001e0\u0016H&J*\u0010\u001f\u001a\u0004\u0018\u00010\u00182\u0006\u0010 \u001a\u00020!2\u0006\u0010\u0014\u001a\u00020\f2\u000e\b\u0002\u0010\"\u001a\b\u0012\u0004\u0012\u00020\u00050#H&J*\u0010$\u001a\u0004\u0018\u00010\u001b2\u0006\u0010%\u001a\u00020&2\u0006\u0010\u0014\u001a\u00020\f2\u000e\b\u0002\u0010\"\u001a\b\u0012\u0004\u0012\u00020\u00050#H&J\u0018\u0010'\u001a\u00020(2\u0006\u0010)\u001a\u00020*2\u0006\u0010\u000b\u001a\u00020\fH&J\u001a\u0010+\u001a\u0004\u0018\u00010\u000e2\u0006\u0010,\u001a\u00020-2\u0006\u0010\u0014\u001a\u00020\fH&J\u001a\u0010.\u001a\u0004\u0018\u00010\u00032\u0006\u0010/\u001a\u0002002\u0006\u0010\u0014\u001a\u00020\fH&J\u001a\u00101\u001a\u0004\u0018\u0001022\u0006\u00103\u001a\u0002042\u0006\u0010\u0014\u001a\u00020\fH&J8\u00105\u001a\u0004\u0018\u00010\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00052\f\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00050\b2\u0006\u0010\t\u001a\u00020\u00052\u0006\u0010\u000b\u001a\u00020\fH&J0\u00106\u001a\u0004\u0018\u00010\u000e2\u0006\u0010\u0006\u001a\u00020\u00052\f\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00050\b2\u0006\u0010\u000f\u001a\u00020\u00052\u0006\u0010\u000b\u001a\u00020\fH&J\u001f\u00107\u001a\b\u0012\u0004\u0012\u000209082\u0006\u0010\u0014\u001a\u00020\fH¦@ø\u0001\u0000¢\u0006\u0002\u0010:J4\u0010;\u001a\b\u0012\u0004\u0012\u00020\u000e0#2\u0006\u0010<\u001a\u00020=2\u0006\u0010\u0006\u001a\u00020\u00052\f\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00050\b2\u0006\u0010\u000b\u001a\u00020\fH&J(\u0010>\u001a\u0004\u0018\u00010?2\u0006\u0010\u0004\u001a\u00020\u00052\f\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00050\b2\u0006\u0010\u000b\u001a\u00020\fH&J6\u0010@\u001a\u00020\u00112\u0006\u0010A\u001a\u00020\u00052\u0006\u0010\u0014\u001a\u00020\f2\u000e\b\u0002\u0010\"\u001a\b\u0012\u0004\u0012\u00020\u00050#2\f\u0010\u0015\u001a\b\u0012\u0004\u0012\u00020\u00130\u0016H&J(\u0010B\u001a\u00020\u00052\u0006\u0010C\u001a\u00020\u00052\u0006\u0010D\u001a\u00020\u00052\u0006\u0010E\u001a\u00020\u00052\u0006\u0010F\u001a\u00020\u0005H&J*\u0010G\u001a\u0004\u0018\u00010\u00182\u0006\u0010A\u001a\u00020\u00052\u0006\u0010\u0014\u001a\u00020\f2\u000e\b\u0002\u0010\"\u001a\b\u0012\u0004\u0012\u00020\u00050#H&J*\u0010H\u001a\u0004\u0018\u00010\u001b2\u0006\u0010A\u001a\u00020\u00052\u0006\u0010\u0014\u001a\u00020\f2\u000e\b\u0002\u0010\"\u001a\b\u0012\u0004\u0012\u00020\u00050#H&J\"\u0010I\u001a\u0004\u0018\u00010\u00032\u0006\u0010\t\u001a\u00020\u00052\u0006\u0010A\u001a\u00020\u00052\u0006\u0010\u0014\u001a\u00020\fH&J.\u0010I\u001a\u00020\u00112\u0006\u0010\t\u001a\u00020\u00052\u0006\u0010A\u001a\u00020\u00052\u0006\u0010\u0014\u001a\u00020\f2\f\u0010\u0015\u001a\b\u0012\u0004\u0012\u00020\u00030\u0016H&J8\u0010J\u001a\u0004\u0018\u00010?2\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00052\f\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00050\b2\u0006\u0010K\u001a\u00020L2\u0006\u0010\u000b\u001a\u00020\fH&J@\u0010M\u001a\u0004\u0018\u00010?2\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00052\f\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00050\b2\u0006\u0010\t\u001a\u00020\u00052\u0006\u0010\n\u001a\u00020\u00052\u0006\u0010\u000b\u001a\u00020\fH&J.\u0010N\u001a\u00020\u00112\u0006\u0010O\u001a\u00020P2\u0006\u0010Q\u001a\u00020\u00052\u0006\u0010\u000b\u001a\u00020\f2\f\u0010\u0015\u001a\b\u0012\u0004\u0012\u00020R0\u0016H&J0\u0010S\u001a\u00020\u00112\u0006\u0010C\u001a\u00020\u00052\u0006\u0010T\u001a\u00020\u00052\u0006\u0010D\u001a\u00020\u00052\u0006\u0010E\u001a\u00020\u00052\u0006\u0010F\u001a\u00020\u0005H&\u0002\u0004\n\u0002\b\u0019¨\u0006U"}, d2 = {"Lcom/stripe/android/StripeRepository;", "", "addCustomerSource", "Lcom/stripe/android/model/Source;", "customerId", "", "publishableKey", "productUsageTokens", "", "sourceId", "sourceType", "requestOptions", "Lcom/stripe/android/ApiRequest$Options;", "attachPaymentMethod", "Lcom/stripe/android/model/PaymentMethod;", "paymentMethodId", "cancelIntent", "", "stripeIntent", "Lcom/stripe/android/model/StripeIntent;", "options", "callback", "Lcom/stripe/android/ApiResultCallback;", "cancelPaymentIntentSource", "Lcom/stripe/android/model/PaymentIntent;", "paymentIntentId", "cancelSetupIntentSource", "Lcom/stripe/android/model/SetupIntent;", "setupIntentId", "complete3ds2Auth", "", "confirmPaymentIntent", "confirmPaymentIntentParams", "Lcom/stripe/android/model/ConfirmPaymentIntentParams;", "expandFields", "", "confirmSetupIntent", "confirmSetupIntentParams", "Lcom/stripe/android/model/ConfirmSetupIntentParams;", "createFile", "Lcom/stripe/android/model/StripeFile;", "fileParams", "Lcom/stripe/android/model/StripeFileParams;", "createPaymentMethod", "paymentMethodCreateParams", "Lcom/stripe/android/model/PaymentMethodCreateParams;", "createSource", "sourceParams", "Lcom/stripe/android/model/SourceParams;", "createToken", "Lcom/stripe/android/model/Token;", "tokenParams", "Lcom/stripe/android/model/TokenParams;", "deleteCustomerSource", "detachPaymentMethod", "getFpxBankStatus", "Landroidx/lifecycle/LiveData;", "Lcom/stripe/android/model/FpxBankStatuses;", "(Lcom/stripe/android/ApiRequest$Options;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "getPaymentMethods", "listPaymentMethodsParams", "Lcom/stripe/android/model/ListPaymentMethodsParams;", "retrieveCustomer", "Lcom/stripe/android/model/Customer;", "retrieveIntent", "clientSecret", "retrieveIssuingCardPin", "cardId", "verificationId", "userOneTimeCode", "ephemeralKeySecret", "retrievePaymentIntent", "retrieveSetupIntent", "retrieveSource", "setCustomerShippingInfo", "shippingInformation", "Lcom/stripe/android/model/ShippingInformation;", "setDefaultCustomerSource", "start3ds2Auth", "authParams", "Lcom/stripe/android/Stripe3ds2AuthParams;", "stripeIntentId", "Lcom/stripe/android/model/Stripe3ds2AuthResult;", "updateIssuingCardPin", "newPin", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: StripeRepository.kt */
public interface StripeRepository {
    Source addCustomerSource(String str, String str2, Set<String> set, String str3, String str4, ApiRequest.Options options) throws AuthenticationException, InvalidRequestException, APIConnectionException, APIException, CardException;

    PaymentMethod attachPaymentMethod(String str, String str2, Set<String> set, String str3, ApiRequest.Options options) throws AuthenticationException, InvalidRequestException, APIConnectionException, APIException, CardException;

    void cancelIntent(StripeIntent stripeIntent, String str, ApiRequest.Options options, ApiResultCallback<StripeIntent> apiResultCallback);

    PaymentIntent cancelPaymentIntentSource(String str, String str2, ApiRequest.Options options) throws AuthenticationException, InvalidRequestException, APIConnectionException, APIException;

    SetupIntent cancelSetupIntentSource(String str, String str2, ApiRequest.Options options) throws AuthenticationException, InvalidRequestException, APIConnectionException, APIException;

    void complete3ds2Auth(String str, ApiRequest.Options options, ApiResultCallback<Boolean> apiResultCallback);

    PaymentIntent confirmPaymentIntent(ConfirmPaymentIntentParams confirmPaymentIntentParams, ApiRequest.Options options, List<String> list) throws AuthenticationException, InvalidRequestException, APIConnectionException, APIException;

    SetupIntent confirmSetupIntent(ConfirmSetupIntentParams confirmSetupIntentParams, ApiRequest.Options options, List<String> list) throws AuthenticationException, InvalidRequestException, APIConnectionException, APIException;

    StripeFile createFile(StripeFileParams stripeFileParams, ApiRequest.Options options);

    PaymentMethod createPaymentMethod(PaymentMethodCreateParams paymentMethodCreateParams, ApiRequest.Options options) throws AuthenticationException, InvalidRequestException, APIConnectionException, APIException;

    Source createSource(SourceParams sourceParams, ApiRequest.Options options) throws AuthenticationException, InvalidRequestException, APIConnectionException, APIException;

    Token createToken(TokenParams tokenParams, ApiRequest.Options options) throws AuthenticationException, InvalidRequestException, APIConnectionException, APIException, CardException;

    Source deleteCustomerSource(String str, String str2, Set<String> set, String str3, ApiRequest.Options options) throws AuthenticationException, InvalidRequestException, APIConnectionException, APIException, CardException;

    PaymentMethod detachPaymentMethod(String str, Set<String> set, String str2, ApiRequest.Options options) throws AuthenticationException, InvalidRequestException, APIConnectionException, APIException, CardException;

    Object getFpxBankStatus(ApiRequest.Options options, Continuation<? super LiveData<FpxBankStatuses>> continuation);

    List<PaymentMethod> getPaymentMethods(ListPaymentMethodsParams listPaymentMethodsParams, String str, Set<String> set, ApiRequest.Options options) throws AuthenticationException, InvalidRequestException, APIConnectionException, APIException, CardException;

    Customer retrieveCustomer(String str, Set<String> set, ApiRequest.Options options) throws AuthenticationException, InvalidRequestException, APIConnectionException, APIException, CardException;

    void retrieveIntent(String str, ApiRequest.Options options, List<String> list, ApiResultCallback<StripeIntent> apiResultCallback);

    String retrieveIssuingCardPin(String str, String str2, String str3, String str4) throws AuthenticationException, InvalidRequestException, APIConnectionException, APIException, CardException, JSONException;

    PaymentIntent retrievePaymentIntent(String str, ApiRequest.Options options, List<String> list) throws AuthenticationException, InvalidRequestException, APIConnectionException, APIException;

    SetupIntent retrieveSetupIntent(String str, ApiRequest.Options options, List<String> list) throws AuthenticationException, InvalidRequestException, APIConnectionException, APIException;

    Source retrieveSource(String str, String str2, ApiRequest.Options options) throws AuthenticationException, InvalidRequestException, APIConnectionException, APIException;

    void retrieveSource(String str, String str2, ApiRequest.Options options, ApiResultCallback<Source> apiResultCallback);

    Customer setCustomerShippingInfo(String str, String str2, Set<String> set, ShippingInformation shippingInformation, ApiRequest.Options options) throws AuthenticationException, InvalidRequestException, APIConnectionException, APIException, CardException;

    Customer setDefaultCustomerSource(String str, String str2, Set<String> set, String str3, String str4, ApiRequest.Options options) throws AuthenticationException, InvalidRequestException, APIConnectionException, APIException, CardException;

    void start3ds2Auth(Stripe3ds2AuthParams stripe3ds2AuthParams, String str, ApiRequest.Options options, ApiResultCallback<Stripe3ds2AuthResult> apiResultCallback);

    void updateIssuingCardPin(String str, String str2, String str3, String str4, String str5) throws AuthenticationException, InvalidRequestException, APIConnectionException, APIException, CardException;

    @Metadata(bv = {1, 0, 3}, k = 3, mv = {1, 1, 16})
    /* compiled from: StripeRepository.kt */
    public static final class DefaultImpls {
        public static /* synthetic */ PaymentIntent confirmPaymentIntent$default(StripeRepository stripeRepository, ConfirmPaymentIntentParams confirmPaymentIntentParams, ApiRequest.Options options, List list, int i, Object obj) throws AuthenticationException, InvalidRequestException, APIConnectionException, APIException {
            if (obj == null) {
                if ((i & 4) != 0) {
                    list = CollectionsKt.emptyList();
                }
                return stripeRepository.confirmPaymentIntent(confirmPaymentIntentParams, options, list);
            }
            throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: confirmPaymentIntent");
        }

        public static /* synthetic */ PaymentIntent retrievePaymentIntent$default(StripeRepository stripeRepository, String str, ApiRequest.Options options, List list, int i, Object obj) throws AuthenticationException, InvalidRequestException, APIConnectionException, APIException {
            if (obj == null) {
                if ((i & 4) != 0) {
                    list = CollectionsKt.emptyList();
                }
                return stripeRepository.retrievePaymentIntent(str, options, list);
            }
            throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: retrievePaymentIntent");
        }

        public static /* synthetic */ SetupIntent confirmSetupIntent$default(StripeRepository stripeRepository, ConfirmSetupIntentParams confirmSetupIntentParams, ApiRequest.Options options, List list, int i, Object obj) throws AuthenticationException, InvalidRequestException, APIConnectionException, APIException {
            if (obj == null) {
                if ((i & 4) != 0) {
                    list = CollectionsKt.emptyList();
                }
                return stripeRepository.confirmSetupIntent(confirmSetupIntentParams, options, list);
            }
            throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: confirmSetupIntent");
        }

        public static /* synthetic */ SetupIntent retrieveSetupIntent$default(StripeRepository stripeRepository, String str, ApiRequest.Options options, List list, int i, Object obj) throws AuthenticationException, InvalidRequestException, APIConnectionException, APIException {
            if (obj == null) {
                if ((i & 4) != 0) {
                    list = CollectionsKt.emptyList();
                }
                return stripeRepository.retrieveSetupIntent(str, options, list);
            }
            throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: retrieveSetupIntent");
        }

        public static /* synthetic */ void retrieveIntent$default(StripeRepository stripeRepository, String str, ApiRequest.Options options, List list, ApiResultCallback apiResultCallback, int i, Object obj) {
            if (obj == null) {
                if ((i & 4) != 0) {
                    list = CollectionsKt.emptyList();
                }
                stripeRepository.retrieveIntent(str, options, list, apiResultCallback);
                return;
            }
            throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: retrieveIntent");
        }
    }
}
