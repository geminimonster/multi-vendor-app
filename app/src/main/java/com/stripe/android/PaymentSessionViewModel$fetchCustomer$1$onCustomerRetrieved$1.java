package com.stripe.android;

import com.stripe.android.PaymentSessionViewModel;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.Lambda;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n¢\u0006\u0002\b\u0002"}, d2 = {"<anonymous>", "", "invoke"}, k = 3, mv = {1, 1, 16})
/* compiled from: PaymentSessionViewModel.kt */
final class PaymentSessionViewModel$fetchCustomer$1$onCustomerRetrieved$1 extends Lambda implements Function0<Unit> {
    final /* synthetic */ PaymentSessionViewModel$fetchCustomer$1 this$0;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    PaymentSessionViewModel$fetchCustomer$1$onCustomerRetrieved$1(PaymentSessionViewModel$fetchCustomer$1 paymentSessionViewModel$fetchCustomer$1) {
        super(0);
        this.this$0 = paymentSessionViewModel$fetchCustomer$1;
    }

    public final void invoke() {
        this.this$0.this$0.mutableNetworkState.setValue(PaymentSessionViewModel.NetworkState.Inactive);
        this.this$0.$resultData.setValue(PaymentSessionViewModel.FetchCustomerResult.Success.INSTANCE);
    }
}
