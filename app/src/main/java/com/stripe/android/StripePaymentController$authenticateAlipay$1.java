package com.stripe.android;

import com.stripe.android.ApiRequest;
import com.stripe.android.model.StripeIntent;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000!\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003*\u0001\u0000\b\n\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001J\u0014\u0010\u0003\u001a\u00020\u00042\n\u0010\u0005\u001a\u00060\u0006j\u0002`\u0007H\u0016J\u0010\u0010\b\u001a\u00020\u00042\u0006\u0010\t\u001a\u00020\u0002H\u0016¨\u0006\n"}, d2 = {"com/stripe/android/StripePaymentController$authenticateAlipay$1", "Lcom/stripe/android/ApiResultCallback;", "", "onError", "", "e", "Ljava/lang/Exception;", "Lkotlin/Exception;", "onSuccess", "result", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: StripePaymentController.kt */
public final class StripePaymentController$authenticateAlipay$1 implements ApiResultCallback<Integer> {
    final /* synthetic */ ApiResultCallback $callback;
    final /* synthetic */ StripeIntent $intent;
    final /* synthetic */ String $stripeAccountId;
    final /* synthetic */ StripePaymentController this$0;

    StripePaymentController$authenticateAlipay$1(StripePaymentController stripePaymentController, String str, StripeIntent stripeIntent, ApiResultCallback apiResultCallback) {
        this.this$0 = stripePaymentController;
        this.$stripeAccountId = str;
        this.$intent = stripeIntent;
        this.$callback = apiResultCallback;
    }

    public /* bridge */ /* synthetic */ void onSuccess(Object obj) {
        onSuccess(((Number) obj).intValue());
    }

    public void onSuccess(int i) {
        ApiRequest.Options options = new ApiRequest.Options(this.this$0.publishableKey, this.$stripeAccountId, (String) null, 4, (DefaultConstructorMarker) null);
        StripeRepository access$getStripeRepository$p = this.this$0.stripeRepository;
        String clientSecret = this.$intent.getClientSecret();
        if (clientSecret == null) {
            clientSecret = "";
        }
        access$getStripeRepository$p.retrieveIntent(clientSecret, options, StripePaymentController.EXPAND_PAYMENT_METHOD, this.this$0.createPaymentIntentCallback(options, i, "", false, this.$callback));
    }

    public void onError(Exception exc) {
        Intrinsics.checkParameterIsNotNull(exc, "e");
        this.$callback.onError(exc);
    }
}
