package com.stripe.android;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import com.stripe.android.ApiRequest;
import com.stripe.android.exception.StripeException;
import com.stripe.android.model.ConfirmStripeIntentParams;
import com.stripe.android.model.Source;
import com.stripe.android.model.StripeIntent;
import com.stripe.android.view.AuthActivityStarter;
import java.io.Serializable;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import kotlinx.android.parcel.Parceler;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000f\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\b\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0003\b`\u0018\u00002\u00020\u0001:\u0001(J0\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\b\u0010\u0006\u001a\u0004\u0018\u00010\u00072\u0006\u0010\b\u001a\u00020\t2\f\u0010\n\u001a\b\u0012\u0004\u0012\u00020\f0\u000bH&J \u0010\r\u001a\u00020\u00032\u0006\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u00052\u0006\u0010\u0011\u001a\u00020\u0012H&J\u001e\u0010\u0013\u001a\u00020\u00032\u0006\u0010\u0014\u001a\u00020\u00152\f\u0010\n\u001a\b\u0012\u0004\u0012\u00020\f0\u000bH&J\u001e\u0010\u0016\u001a\u00020\u00032\u0006\u0010\u0014\u001a\u00020\u00152\f\u0010\n\u001a\b\u0012\u0004\u0012\u00020\u00170\u000bH&J\u001e\u0010\u0018\u001a\u00020\u00032\u0006\u0010\u0014\u001a\u00020\u00152\f\u0010\n\u001a\b\u0012\u0004\u0012\u00020\u00190\u000bH&J\u001a\u0010\u001a\u001a\u00020\u001b2\u0006\u0010\u001c\u001a\u00020\u001d2\b\u0010\u0014\u001a\u0004\u0018\u00010\u0015H&J\u001a\u0010\u001e\u001a\u00020\u001b2\u0006\u0010\u001c\u001a\u00020\u001d2\b\u0010\u0014\u001a\u0004\u0018\u00010\u0015H&J\u001a\u0010\u001f\u001a\u00020\u001b2\u0006\u0010\u001c\u001a\u00020\u001d2\b\u0010\u0014\u001a\u0004\u0018\u00010\u0015H&J \u0010 \u001a\u00020\u00032\u0006\u0010\u000e\u001a\u00020\u000f2\u0006\u0010!\u001a\u00020\u00072\u0006\u0010\u0011\u001a\u00020\u0012H&J \u0010\"\u001a\u00020\u00032\u0006\u0010\u000e\u001a\u00020\u000f2\u0006\u0010#\u001a\u00020\u00192\u0006\u0010\u0011\u001a\u00020\u0012H&J&\u0010$\u001a\u00020\u00032\u0006\u0010%\u001a\u00020&2\u0006\u0010\u0011\u001a\u00020\u00122\f\u0010\n\u001a\b\u0012\u0004\u0012\u00020\u00050\u000bH&J \u0010'\u001a\u00020\u00032\u0006\u0010\u000e\u001a\u00020\u000f2\u0006\u0010%\u001a\u00020&2\u0006\u0010\u0011\u001a\u00020\u0012H&¨\u0006)"}, d2 = {"Lcom/stripe/android/PaymentController;", "", "authenticateAlipay", "", "intent", "Lcom/stripe/android/model/StripeIntent;", "stripeAccountId", "", "authenticator", "Lcom/stripe/android/AlipayAuthenticator;", "callback", "Lcom/stripe/android/ApiResultCallback;", "Lcom/stripe/android/PaymentIntentResult;", "handleNextAction", "host", "Lcom/stripe/android/view/AuthActivityStarter$Host;", "stripeIntent", "requestOptions", "Lcom/stripe/android/ApiRequest$Options;", "handlePaymentResult", "data", "Landroid/content/Intent;", "handleSetupResult", "Lcom/stripe/android/SetupIntentResult;", "handleSourceResult", "Lcom/stripe/android/model/Source;", "shouldHandlePaymentResult", "", "requestCode", "", "shouldHandleSetupResult", "shouldHandleSourceResult", "startAuth", "clientSecret", "startAuthenticateSource", "source", "startConfirm", "confirmStripeIntentParams", "Lcom/stripe/android/model/ConfirmStripeIntentParams;", "startConfirmAndAuth", "Result", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: PaymentController.kt */
public interface PaymentController {
    void authenticateAlipay(StripeIntent stripeIntent, String str, AlipayAuthenticator alipayAuthenticator, ApiResultCallback<PaymentIntentResult> apiResultCallback);

    void handleNextAction(AuthActivityStarter.Host host, StripeIntent stripeIntent, ApiRequest.Options options);

    void handlePaymentResult(Intent intent, ApiResultCallback<PaymentIntentResult> apiResultCallback);

    void handleSetupResult(Intent intent, ApiResultCallback<SetupIntentResult> apiResultCallback);

    void handleSourceResult(Intent intent, ApiResultCallback<Source> apiResultCallback);

    boolean shouldHandlePaymentResult(int i, Intent intent);

    boolean shouldHandleSetupResult(int i, Intent intent);

    boolean shouldHandleSourceResult(int i, Intent intent);

    void startAuth(AuthActivityStarter.Host host, String str, ApiRequest.Options options);

    void startAuthenticateSource(AuthActivityStarter.Host host, Source source, ApiRequest.Options options);

    void startConfirm(ConfirmStripeIntentParams confirmStripeIntentParams, ApiRequest.Options options, ApiResultCallback<StripeIntent> apiResultCallback);

    void startConfirmAndAuth(AuthActivityStarter.Host host, ConfirmStripeIntentParams confirmStripeIntentParams, ApiRequest.Options options);

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000J\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b \n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\b\b\u0018\u0000 72\u00020\u0001:\u00017BW\b\u0000\u0012\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\b\b\u0002\u0010\u0004\u001a\u00020\u0005\u0012\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u0012\b\b\u0002\u0010\b\u001a\u00020\t\u0012\n\b\u0002\u0010\n\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\u000b\u001a\u0004\u0018\u00010\f\u0012\n\b\u0002\u0010\r\u001a\u0004\u0018\u00010\u0003¢\u0006\u0002\u0010\u000eJ\u0010\u0010\u001b\u001a\u0004\u0018\u00010\u0003HÀ\u0003¢\u0006\u0002\b\u001cJ\u000e\u0010\u001d\u001a\u00020\u0005HÀ\u0003¢\u0006\u0002\b\u001eJ\u0010\u0010\u001f\u001a\u0004\u0018\u00010\u0007HÀ\u0003¢\u0006\u0002\b J\u000e\u0010!\u001a\u00020\tHÀ\u0003¢\u0006\u0002\b\"J\u0010\u0010#\u001a\u0004\u0018\u00010\u0003HÀ\u0003¢\u0006\u0002\b$J\u0010\u0010%\u001a\u0004\u0018\u00010\fHÀ\u0003¢\u0006\u0002\b&J\u0010\u0010'\u001a\u0004\u0018\u00010\u0003HÀ\u0003¢\u0006\u0002\b(JY\u0010)\u001a\u00020\u00002\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u00072\b\b\u0002\u0010\b\u001a\u00020\t2\n\b\u0002\u0010\n\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u000b\u001a\u0004\u0018\u00010\f2\n\b\u0002\u0010\r\u001a\u0004\u0018\u00010\u0003HÆ\u0001J\t\u0010*\u001a\u00020\u0005HÖ\u0001J\u0013\u0010+\u001a\u00020\t2\b\u0010,\u001a\u0004\u0018\u00010-HÖ\u0003J\t\u0010.\u001a\u00020\u0005HÖ\u0001J\u0006\u0010/\u001a\u000200J\t\u00101\u001a\u00020\u0003HÖ\u0001J\u0019\u00102\u001a\u0002032\u0006\u00104\u001a\u0002052\u0006\u00106\u001a\u00020\u0005HÖ\u0001R\u0016\u0010\u0002\u001a\u0004\u0018\u00010\u0003X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010R\u0016\u0010\u0006\u001a\u0004\u0018\u00010\u0007X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u0012R\u0014\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u0014R\u0014\u0010\b\u001a\u00020\tX\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0015\u0010\u0016R\u0016\u0010\u000b\u001a\u0004\u0018\u00010\fX\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0017\u0010\u0018R\u0016\u0010\n\u001a\u0004\u0018\u00010\u0003X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0019\u0010\u0010R\u0016\u0010\r\u001a\u0004\u0018\u00010\u0003X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u001a\u0010\u0010¨\u00068"}, d2 = {"Lcom/stripe/android/PaymentController$Result;", "Landroid/os/Parcelable;", "clientSecret", "", "flowOutcome", "", "exception", "Lcom/stripe/android/exception/StripeException;", "shouldCancelSource", "", "sourceId", "source", "Lcom/stripe/android/model/Source;", "stripeAccountId", "(Ljava/lang/String;ILcom/stripe/android/exception/StripeException;ZLjava/lang/String;Lcom/stripe/android/model/Source;Ljava/lang/String;)V", "getClientSecret$stripe_release", "()Ljava/lang/String;", "getException$stripe_release", "()Lcom/stripe/android/exception/StripeException;", "getFlowOutcome$stripe_release", "()I", "getShouldCancelSource$stripe_release", "()Z", "getSource$stripe_release", "()Lcom/stripe/android/model/Source;", "getSourceId$stripe_release", "getStripeAccountId$stripe_release", "component1", "component1$stripe_release", "component2", "component2$stripe_release", "component3", "component3$stripe_release", "component4", "component4$stripe_release", "component5", "component5$stripe_release", "component6", "component6$stripe_release", "component7", "component7$stripe_release", "copy", "describeContents", "equals", "other", "", "hashCode", "toBundle", "Landroid/os/Bundle;", "toString", "writeToParcel", "", "parcel", "Landroid/os/Parcel;", "flags", "Companion", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: PaymentController.kt */
    public static final class Result implements Parcelable {
        public static final Parcelable.Creator CREATOR = new Creator();
        public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
        private static final String EXTRA = "extra_args";
        private final String clientSecret;
        private final StripeException exception;
        private final int flowOutcome;
        private final boolean shouldCancelSource;
        private final Source source;
        private final String sourceId;
        private final String stripeAccountId;

        @Metadata(bv = {1, 0, 3}, k = 3, mv = {1, 1, 16})
        public static class Creator implements Parcelable.Creator {
            public final Object createFromParcel(Parcel parcel) {
                Intrinsics.checkParameterIsNotNull(parcel, "in");
                return Result.Companion.create(parcel);
            }

            public final Object[] newArray(int i) {
                return new Result[i];
            }
        }

        public Result() {
            this((String) null, 0, (StripeException) null, false, (String) null, (Source) null, (String) null, 127, (DefaultConstructorMarker) null);
        }

        public static /* synthetic */ Result copy$default(Result result, String str, int i, StripeException stripeException, boolean z, String str2, Source source2, String str3, int i2, Object obj) {
            if ((i2 & 1) != 0) {
                str = result.clientSecret;
            }
            if ((i2 & 2) != 0) {
                i = result.flowOutcome;
            }
            int i3 = i;
            if ((i2 & 4) != 0) {
                stripeException = result.exception;
            }
            StripeException stripeException2 = stripeException;
            if ((i2 & 8) != 0) {
                z = result.shouldCancelSource;
            }
            boolean z2 = z;
            if ((i2 & 16) != 0) {
                str2 = result.sourceId;
            }
            String str4 = str2;
            if ((i2 & 32) != 0) {
                source2 = result.source;
            }
            Source source3 = source2;
            if ((i2 & 64) != 0) {
                str3 = result.stripeAccountId;
            }
            return result.copy(str, i3, stripeException2, z2, str4, source3, str3);
        }

        public final String component1$stripe_release() {
            return this.clientSecret;
        }

        public final int component2$stripe_release() {
            return this.flowOutcome;
        }

        public final StripeException component3$stripe_release() {
            return this.exception;
        }

        public final boolean component4$stripe_release() {
            return this.shouldCancelSource;
        }

        public final String component5$stripe_release() {
            return this.sourceId;
        }

        public final Source component6$stripe_release() {
            return this.source;
        }

        public final String component7$stripe_release() {
            return this.stripeAccountId;
        }

        public final Result copy(String str, int i, StripeException stripeException, boolean z, String str2, Source source2, String str3) {
            return new Result(str, i, stripeException, z, str2, source2, str3);
        }

        public int describeContents() {
            return 0;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Result)) {
                return false;
            }
            Result result = (Result) obj;
            return Intrinsics.areEqual((Object) this.clientSecret, (Object) result.clientSecret) && this.flowOutcome == result.flowOutcome && Intrinsics.areEqual((Object) this.exception, (Object) result.exception) && this.shouldCancelSource == result.shouldCancelSource && Intrinsics.areEqual((Object) this.sourceId, (Object) result.sourceId) && Intrinsics.areEqual((Object) this.source, (Object) result.source) && Intrinsics.areEqual((Object) this.stripeAccountId, (Object) result.stripeAccountId);
        }

        public int hashCode() {
            String str = this.clientSecret;
            int i = 0;
            int hashCode = (((str != null ? str.hashCode() : 0) * 31) + this.flowOutcome) * 31;
            StripeException stripeException = this.exception;
            int hashCode2 = (hashCode + (stripeException != null ? stripeException.hashCode() : 0)) * 31;
            boolean z = this.shouldCancelSource;
            if (z) {
                z = true;
            }
            int i2 = (hashCode2 + (z ? 1 : 0)) * 31;
            String str2 = this.sourceId;
            int hashCode3 = (i2 + (str2 != null ? str2.hashCode() : 0)) * 31;
            Source source2 = this.source;
            int hashCode4 = (hashCode3 + (source2 != null ? source2.hashCode() : 0)) * 31;
            String str3 = this.stripeAccountId;
            if (str3 != null) {
                i = str3.hashCode();
            }
            return hashCode4 + i;
        }

        public String toString() {
            return "Result(clientSecret=" + this.clientSecret + ", flowOutcome=" + this.flowOutcome + ", exception=" + this.exception + ", shouldCancelSource=" + this.shouldCancelSource + ", sourceId=" + this.sourceId + ", source=" + this.source + ", stripeAccountId=" + this.stripeAccountId + ")";
        }

        public void writeToParcel(Parcel parcel, int i) {
            Intrinsics.checkParameterIsNotNull(parcel, "parcel");
            Companion.write(this, parcel, i);
        }

        public Result(String str, int i, StripeException stripeException, boolean z, String str2, Source source2, String str3) {
            this.clientSecret = str;
            this.flowOutcome = i;
            this.exception = stripeException;
            this.shouldCancelSource = z;
            this.sourceId = str2;
            this.source = source2;
            this.stripeAccountId = str3;
        }

        /* JADX WARNING: Illegal instructions before constructor call */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public /* synthetic */ Result(java.lang.String r6, int r7, com.stripe.android.exception.StripeException r8, boolean r9, java.lang.String r10, com.stripe.android.model.Source r11, java.lang.String r12, int r13, kotlin.jvm.internal.DefaultConstructorMarker r14) {
            /*
                r5 = this;
                r14 = r13 & 1
                r0 = 0
                if (r14 == 0) goto L_0x0008
                r6 = r0
                java.lang.String r6 = (java.lang.String) r6
            L_0x0008:
                r14 = r13 & 2
                r1 = 0
                if (r14 == 0) goto L_0x000f
                r14 = 0
                goto L_0x0010
            L_0x000f:
                r14 = r7
            L_0x0010:
                r7 = r13 & 4
                if (r7 == 0) goto L_0x0017
                r8 = r0
                com.stripe.android.exception.StripeException r8 = (com.stripe.android.exception.StripeException) r8
            L_0x0017:
                r2 = r8
                r7 = r13 & 8
                if (r7 == 0) goto L_0x001d
                goto L_0x001e
            L_0x001d:
                r1 = r9
            L_0x001e:
                r7 = r13 & 16
                if (r7 == 0) goto L_0x0025
                r10 = r0
                java.lang.String r10 = (java.lang.String) r10
            L_0x0025:
                r3 = r10
                r7 = r13 & 32
                if (r7 == 0) goto L_0x002d
                r11 = r0
                com.stripe.android.model.Source r11 = (com.stripe.android.model.Source) r11
            L_0x002d:
                r4 = r11
                r7 = r13 & 64
                if (r7 == 0) goto L_0x0035
                r12 = r0
                java.lang.String r12 = (java.lang.String) r12
            L_0x0035:
                r0 = r12
                r7 = r5
                r8 = r6
                r9 = r14
                r10 = r2
                r11 = r1
                r12 = r3
                r13 = r4
                r14 = r0
                r7.<init>(r8, r9, r10, r11, r12, r13, r14)
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.stripe.android.PaymentController.Result.<init>(java.lang.String, int, com.stripe.android.exception.StripeException, boolean, java.lang.String, com.stripe.android.model.Source, java.lang.String, int, kotlin.jvm.internal.DefaultConstructorMarker):void");
        }

        public final String getClientSecret$stripe_release() {
            return this.clientSecret;
        }

        public final int getFlowOutcome$stripe_release() {
            return this.flowOutcome;
        }

        public final StripeException getException$stripe_release() {
            return this.exception;
        }

        public final boolean getShouldCancelSource$stripe_release() {
            return this.shouldCancelSource;
        }

        public final String getSourceId$stripe_release() {
            return this.sourceId;
        }

        public final Source getSource$stripe_release() {
            return this.source;
        }

        public final String getStripeAccountId$stripe_release() {
            return this.stripeAccountId;
        }

        public final /* synthetic */ Bundle toBundle() {
            Bundle bundle = new Bundle();
            bundle.putParcelable("extra_args", this);
            return bundle;
        }

        @Metadata(bv = {1, 0, 3}, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\b\n\u0000\b\u0003\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0003J\u0010\u0010\u0006\u001a\u00020\u00022\u0006\u0010\u0007\u001a\u00020\bH\u0016J\u0017\u0010\t\u001a\u0004\u0018\u00010\u00022\u0006\u0010\n\u001a\u00020\u000bH\u0000¢\u0006\u0002\b\fJ\u001c\u0010\r\u001a\u00020\u000e*\u00020\u00022\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\u000f\u001a\u00020\u0010H\u0016R\u000e\u0010\u0004\u001a\u00020\u0005XT¢\u0006\u0002\n\u0000¨\u0006\u0011"}, d2 = {"Lcom/stripe/android/PaymentController$Result$Companion;", "Lkotlinx/android/parcel/Parceler;", "Lcom/stripe/android/PaymentController$Result;", "()V", "EXTRA", "", "create", "parcel", "Landroid/os/Parcel;", "fromIntent", "intent", "Landroid/content/Intent;", "fromIntent$stripe_release", "write", "", "flags", "", "stripe_release"}, k = 1, mv = {1, 1, 16})
        /* compiled from: PaymentController.kt */
        public static final class Companion implements Parceler<Result> {
            private Companion() {
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }

            public Result[] newArray(int i) {
                return (Result[]) Parceler.DefaultImpls.newArray(this, i);
            }

            public Result create(Parcel parcel) {
                Intrinsics.checkParameterIsNotNull(parcel, "parcel");
                String readString = parcel.readString();
                int readInt = parcel.readInt();
                Serializable readSerializable = parcel.readSerializable();
                if (!(readSerializable instanceof StripeException)) {
                    readSerializable = null;
                }
                StripeException stripeException = (StripeException) readSerializable;
                boolean z = true;
                if (parcel.readInt() != 1) {
                    z = false;
                }
                return new Result(readString, readInt, stripeException, z, parcel.readString(), (Source) parcel.readParcelable(Source.class.getClassLoader()), parcel.readString());
            }

            public void write(Result result, Parcel parcel, int i) {
                Intrinsics.checkParameterIsNotNull(result, "$this$write");
                Intrinsics.checkParameterIsNotNull(parcel, "parcel");
                parcel.writeString(result.getClientSecret$stripe_release());
                parcel.writeInt(result.getFlowOutcome$stripe_release());
                parcel.writeSerializable(result.getException$stripe_release());
                Integer num = 1;
                num.intValue();
                if (!result.getShouldCancelSource$stripe_release()) {
                    num = null;
                }
                parcel.writeInt(num != null ? num.intValue() : 0);
                parcel.writeString(result.getSourceId$stripe_release());
                parcel.writeParcelable(result.getSource$stripe_release(), i);
                parcel.writeString(result.getStripeAccountId$stripe_release());
            }

            public final /* synthetic */ Result fromIntent$stripe_release(Intent intent) {
                Intrinsics.checkParameterIsNotNull(intent, "intent");
                return (Result) intent.getParcelableExtra("extra_args");
            }
        }
    }
}
