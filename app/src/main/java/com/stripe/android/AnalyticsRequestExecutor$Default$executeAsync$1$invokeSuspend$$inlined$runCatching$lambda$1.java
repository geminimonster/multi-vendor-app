package com.stripe.android;

import kotlin.Metadata;
import kotlin.ResultKt;
import kotlin.Unit;
import kotlin.coroutines.Continuation;
import kotlin.coroutines.intrinsics.IntrinsicsKt;
import kotlin.coroutines.jvm.internal.Boxing;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.Intrinsics;
import kotlinx.coroutines.CoroutineScope;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0000\n\u0002\u0010\b\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\b\u0003\u0010\u0000\u001a\u00020\u0001*\u00020\u0002H@¢\u0006\u0004\b\u0003\u0010\u0004¨\u0006\u0005"}, d2 = {"<anonymous>", "", "Lkotlinx/coroutines/CoroutineScope;", "invoke", "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;", "com/stripe/android/AnalyticsRequestExecutor$Default$executeAsync$1$1$1"}, k = 3, mv = {1, 1, 16})
/* compiled from: AnalyticsRequestExecutor.kt */
final class AnalyticsRequestExecutor$Default$executeAsync$1$invokeSuspend$$inlined$runCatching$lambda$1 extends SuspendLambda implements Function2<CoroutineScope, Continuation<? super Integer>, Object> {
    int label;
    private CoroutineScope p$;
    final /* synthetic */ AnalyticsRequestExecutor$Default$executeAsync$1 this$0;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    AnalyticsRequestExecutor$Default$executeAsync$1$invokeSuspend$$inlined$runCatching$lambda$1(Continuation continuation, AnalyticsRequestExecutor$Default$executeAsync$1 analyticsRequestExecutor$Default$executeAsync$1) {
        super(2, continuation);
        this.this$0 = analyticsRequestExecutor$Default$executeAsync$1;
    }

    public final Continuation<Unit> create(Object obj, Continuation<?> continuation) {
        Intrinsics.checkParameterIsNotNull(continuation, "completion");
        AnalyticsRequestExecutor$Default$executeAsync$1$invokeSuspend$$inlined$runCatching$lambda$1 analyticsRequestExecutor$Default$executeAsync$1$invokeSuspend$$inlined$runCatching$lambda$1 = new AnalyticsRequestExecutor$Default$executeAsync$1$invokeSuspend$$inlined$runCatching$lambda$1(continuation, this.this$0);
        analyticsRequestExecutor$Default$executeAsync$1$invokeSuspend$$inlined$runCatching$lambda$1.p$ = (CoroutineScope) obj;
        return analyticsRequestExecutor$Default$executeAsync$1$invokeSuspend$$inlined$runCatching$lambda$1;
    }

    public final Object invoke(Object obj, Object obj2) {
        return ((AnalyticsRequestExecutor$Default$executeAsync$1$invokeSuspend$$inlined$runCatching$lambda$1) create(obj, (Continuation) obj2)).invokeSuspend(Unit.INSTANCE);
    }

    public final Object invokeSuspend(Object obj) {
        IntrinsicsKt.getCOROUTINE_SUSPENDED();
        if (this.label == 0) {
            ResultKt.throwOnFailure(obj);
            return Boxing.boxInt(this.this$0.this$0.execute$stripe_release(this.this$0.$request));
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
