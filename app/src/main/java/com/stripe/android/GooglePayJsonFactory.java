package com.stripe.android;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.share.internal.MessengerShareContentUtility;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Currency;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.collections.CollectionsKt;
import kotlin.collections.SetsKt;
import kotlin.jvm.internal.Intrinsics;
import org.json.JSONArray;
import org.json.JSONObject;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000F\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\b\u0018\u0000 \u001d2\u00020\u0001:\u0005\u001c\u001d\u001e\u001f B\u0019\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0002\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006B\u0017\u0012\u0006\u0010\u0007\u001a\u00020\b\u0012\b\b\u0002\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\tJ\b\u0010\n\u001a\u00020\u000bH\u0002J\u0012\u0010\f\u001a\u00020\u000b2\b\u0010\r\u001a\u0004\u0018\u00010\u000eH\u0002J#\u0010\u000f\u001a\u00020\u000b2\n\b\u0002\u0010\r\u001a\u0004\u0018\u00010\u000e2\n\b\u0002\u0010\u0010\u001a\u0004\u0018\u00010\u0005¢\u0006\u0002\u0010\u0011J<\u0010\u0012\u001a\u00020\u000b2\u0006\u0010\u0013\u001a\u00020\u00142\n\b\u0002\u0010\r\u001a\u0004\u0018\u00010\u000e2\n\b\u0002\u0010\u0015\u001a\u0004\u0018\u00010\u00162\b\b\u0002\u0010\u0017\u001a\u00020\u00052\n\b\u0002\u0010\u0018\u001a\u0004\u0018\u00010\u0019J\u0010\u0010\u001a\u001a\u00020\u000b2\u0006\u0010\u0015\u001a\u00020\u0016H\u0002J\u0010\u0010\u001b\u001a\u00020\u000b2\u0006\u0010\u0013\u001a\u00020\u0014H\u0002R\u000e\u0010\u0007\u001a\u00020\bX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000¨\u0006!"}, d2 = {"Lcom/stripe/android/GooglePayJsonFactory;", "", "context", "Landroid/content/Context;", "isJcbEnabled", "", "(Landroid/content/Context;Z)V", "googlePayConfig", "Lcom/stripe/android/GooglePayConfig;", "(Lcom/stripe/android/GooglePayConfig;Z)V", "createBaseCardPaymentMethodParams", "Lorg/json/JSONObject;", "createCardPaymentMethod", "billingAddressParameters", "Lcom/stripe/android/GooglePayJsonFactory$BillingAddressParameters;", "createIsReadyToPayRequest", "existingPaymentMethodRequired", "(Lcom/stripe/android/GooglePayJsonFactory$BillingAddressParameters;Ljava/lang/Boolean;)Lorg/json/JSONObject;", "createPaymentDataRequest", "transactionInfo", "Lcom/stripe/android/GooglePayJsonFactory$TransactionInfo;", "shippingAddressParameters", "Lcom/stripe/android/GooglePayJsonFactory$ShippingAddressParameters;", "isEmailRequired", "merchantInfo", "Lcom/stripe/android/GooglePayJsonFactory$MerchantInfo;", "createShippingAddressParameters", "createTransactionInfo", "BillingAddressParameters", "Companion", "MerchantInfo", "ShippingAddressParameters", "TransactionInfo", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: GooglePayJsonFactory.kt */
public final class GooglePayJsonFactory {
    private static final List<String> ALLOWED_AUTH_METHODS = CollectionsKt.listOf("PAN_ONLY", "CRYPTOGRAM_3DS");
    private static final int API_VERSION = 2;
    private static final int API_VERSION_MINOR = 0;
    private static final String CARD_PAYMENT_METHOD = "CARD";
    @Deprecated
    public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
    private static final List<String> DEFAULT_CARD_NETWORKS = CollectionsKt.listOf("AMEX", "DISCOVER", "MASTERCARD", "VISA");
    private static final String JCB_CARD_NETWORK = "JCB";
    private final GooglePayConfig googlePayConfig;
    private final boolean isJcbEnabled;

    public GooglePayJsonFactory(GooglePayConfig googlePayConfig2, boolean z) {
        Intrinsics.checkParameterIsNotNull(googlePayConfig2, "googlePayConfig");
        this.googlePayConfig = googlePayConfig2;
        this.isJcbEnabled = z;
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ GooglePayJsonFactory(GooglePayConfig googlePayConfig2, boolean z, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(googlePayConfig2, (i & 2) != 0 ? false : z);
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ GooglePayJsonFactory(Context context, boolean z, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(context, (i & 2) != 0 ? false : z);
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public GooglePayJsonFactory(Context context, boolean z) {
        this(new GooglePayConfig(context, (String) null, 2, (DefaultConstructorMarker) null), z);
        Intrinsics.checkParameterIsNotNull(context, "context");
    }

    public static /* synthetic */ JSONObject createIsReadyToPayRequest$default(GooglePayJsonFactory googlePayJsonFactory, BillingAddressParameters billingAddressParameters, Boolean bool, int i, Object obj) {
        if ((i & 1) != 0) {
            billingAddressParameters = null;
        }
        if ((i & 2) != 0) {
            bool = null;
        }
        return googlePayJsonFactory.createIsReadyToPayRequest(billingAddressParameters, bool);
    }

    public final JSONObject createIsReadyToPayRequest(BillingAddressParameters billingAddressParameters, Boolean bool) {
        JSONObject put = new JSONObject().put("apiVersion", 2).put("apiVersionMinor", 0).put("allowedPaymentMethods", new JSONArray().put(createCardPaymentMethod(billingAddressParameters)));
        if (bool != null) {
            put.put("existingPaymentMethodRequired", bool.booleanValue());
        }
        Intrinsics.checkExpressionValueIsNotNull(put, "JSONObject()\n           …          }\n            }");
        return put;
    }

    public static /* synthetic */ JSONObject createPaymentDataRequest$default(GooglePayJsonFactory googlePayJsonFactory, TransactionInfo transactionInfo, BillingAddressParameters billingAddressParameters, ShippingAddressParameters shippingAddressParameters, boolean z, MerchantInfo merchantInfo, int i, Object obj) {
        if ((i & 2) != 0) {
            billingAddressParameters = null;
        }
        BillingAddressParameters billingAddressParameters2 = billingAddressParameters;
        if ((i & 4) != 0) {
            shippingAddressParameters = null;
        }
        ShippingAddressParameters shippingAddressParameters2 = shippingAddressParameters;
        boolean z2 = (i & 8) != 0 ? false : z;
        if ((i & 16) != 0) {
            merchantInfo = null;
        }
        return googlePayJsonFactory.createPaymentDataRequest(transactionInfo, billingAddressParameters2, shippingAddressParameters2, z2, merchantInfo);
    }

    public final JSONObject createPaymentDataRequest(TransactionInfo transactionInfo, BillingAddressParameters billingAddressParameters, ShippingAddressParameters shippingAddressParameters, boolean z, MerchantInfo merchantInfo) {
        Intrinsics.checkParameterIsNotNull(transactionInfo, "transactionInfo");
        boolean z2 = false;
        JSONObject put = new JSONObject().put("apiVersion", 2).put("apiVersionMinor", 0).put("allowedPaymentMethods", new JSONArray().put(createCardPaymentMethod(billingAddressParameters))).put("transactionInfo", createTransactionInfo(transactionInfo)).put("emailRequired", z);
        if (shippingAddressParameters != null && shippingAddressParameters.isRequired$stripe_release()) {
            put.put("shippingAddressRequired", true);
            put.put("shippingAddressParameters", createShippingAddressParameters(shippingAddressParameters));
        }
        if (merchantInfo != null) {
            CharSequence merchantName$stripe_release = merchantInfo.getMerchantName$stripe_release();
            if (merchantName$stripe_release == null || merchantName$stripe_release.length() == 0) {
                z2 = true;
            }
            if (!z2) {
                put.put("merchantInfo", new JSONObject().put("merchantName", merchantInfo.getMerchantName$stripe_release()));
            }
        }
        Intrinsics.checkExpressionValueIsNotNull(put, "JSONObject()\n           …          }\n            }");
        return put;
    }

    private final JSONObject createTransactionInfo(TransactionInfo transactionInfo) {
        JSONObject jSONObject = new JSONObject();
        String currencyCode$stripe_release = transactionInfo.getCurrencyCode$stripe_release();
        Locale locale = Locale.ROOT;
        Intrinsics.checkExpressionValueIsNotNull(locale, "Locale.ROOT");
        if (currencyCode$stripe_release != null) {
            String upperCase = currencyCode$stripe_release.toUpperCase(locale);
            Intrinsics.checkExpressionValueIsNotNull(upperCase, "(this as java.lang.String).toUpperCase(locale)");
            JSONObject put = jSONObject.put("currencyCode", upperCase).put("totalPriceStatus", transactionInfo.getTotalPriceStatus$stripe_release().getCode$stripe_release());
            String countryCode$stripe_release = transactionInfo.getCountryCode$stripe_release();
            if (countryCode$stripe_release != null) {
                Locale locale2 = Locale.ROOT;
                Intrinsics.checkExpressionValueIsNotNull(locale2, "Locale.ROOT");
                if (countryCode$stripe_release != null) {
                    String upperCase2 = countryCode$stripe_release.toUpperCase(locale2);
                    Intrinsics.checkExpressionValueIsNotNull(upperCase2, "(this as java.lang.String).toUpperCase(locale)");
                    put.put("countryCode", upperCase2);
                } else {
                    throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
                }
            }
            String transactionId$stripe_release = transactionInfo.getTransactionId$stripe_release();
            if (transactionId$stripe_release != null) {
                put.put("transactionId", transactionId$stripe_release);
            }
            Integer totalPrice$stripe_release = transactionInfo.getTotalPrice$stripe_release();
            if (totalPrice$stripe_release != null) {
                int intValue = totalPrice$stripe_release.intValue();
                Currency instance = Currency.getInstance(transactionInfo.getCurrencyCode$stripe_release());
                Intrinsics.checkExpressionValueIsNotNull(instance, "Currency.getInstance(transactionInfo.currencyCode)");
                put.put("totalPrice", PayWithGoogleUtils.getPriceString(intValue, instance));
            }
            String totalPriceLabel$stripe_release = transactionInfo.getTotalPriceLabel$stripe_release();
            if (totalPriceLabel$stripe_release != null) {
                put.put("totalPriceLabel", totalPriceLabel$stripe_release);
            }
            TransactionInfo.CheckoutOption checkoutOption$stripe_release = transactionInfo.getCheckoutOption$stripe_release();
            if (checkoutOption$stripe_release != null) {
                put.put("checkoutOption", checkoutOption$stripe_release.getCode$stripe_release());
            }
            Intrinsics.checkExpressionValueIsNotNull(put, "JSONObject()\n           …          }\n            }");
            return put;
        }
        throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
    }

    private final JSONObject createShippingAddressParameters(ShippingAddressParameters shippingAddressParameters) {
        JSONObject put = new JSONObject().put("allowedCountryCodes", new JSONArray(shippingAddressParameters.getNormalizedAllowedCountryCodes$stripe_release())).put("phoneNumberRequired", shippingAddressParameters.getPhoneNumberRequired$stripe_release());
        Intrinsics.checkExpressionValueIsNotNull(put, "JSONObject()\n           …berRequired\n            )");
        return put;
    }

    private final JSONObject createCardPaymentMethod(BillingAddressParameters billingAddressParameters) {
        JSONObject createBaseCardPaymentMethodParams = createBaseCardPaymentMethodParams();
        if (billingAddressParameters != null && billingAddressParameters.isRequired$stripe_release()) {
            createBaseCardPaymentMethodParams.put("billingAddressRequired", true);
            createBaseCardPaymentMethodParams.put("billingAddressParameters", new JSONObject().put("phoneNumberRequired", billingAddressParameters.isPhoneNumberRequired$stripe_release()).put("format", billingAddressParameters.getFormat$stripe_release().getCode$stripe_release()));
        }
        JSONObject put = new JSONObject().put("type", CARD_PAYMENT_METHOD).put("parameters", createBaseCardPaymentMethodParams).put("tokenizationSpecification", this.googlePayConfig.getTokenizationSpecification());
        Intrinsics.checkExpressionValueIsNotNull(put, "JSONObject()\n           …okenizationSpecification)");
        return put;
    }

    private final JSONObject createBaseCardPaymentMethodParams() {
        JSONObject put = new JSONObject().put("allowedAuthMethods", new JSONArray(ALLOWED_AUTH_METHODS));
        Collection collection = DEFAULT_CARD_NETWORKS;
        List listOf = CollectionsKt.listOf(JCB_CARD_NETWORK);
        if (!this.isJcbEnabled) {
            listOf = null;
        }
        if (listOf == null) {
            listOf = CollectionsKt.emptyList();
        }
        JSONObject put2 = put.put("allowedCardNetworks", new JSONArray(CollectionsKt.plus(collection, listOf)));
        Intrinsics.checkExpressionValueIsNotNull(put2, "JSONObject()\n           …         )\n            ))");
        return put2;
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u000f\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\b\b\u0018\u00002\u00020\u0001:\u0001!B%\b\u0007\u0012\b\b\u0002\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0002\u0010\u0004\u001a\u00020\u0005\u0012\b\b\u0002\u0010\u0006\u001a\u00020\u0003¢\u0006\u0002\u0010\u0007J\u000e\u0010\r\u001a\u00020\u0003HÀ\u0003¢\u0006\u0002\b\u000eJ\u000e\u0010\u000f\u001a\u00020\u0005HÀ\u0003¢\u0006\u0002\b\u0010J\u000e\u0010\u0011\u001a\u00020\u0003HÀ\u0003¢\u0006\u0002\b\u0012J'\u0010\u0013\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u0003HÆ\u0001J\t\u0010\u0014\u001a\u00020\u0015HÖ\u0001J\u0013\u0010\u0016\u001a\u00020\u00032\b\u0010\u0017\u001a\u0004\u0018\u00010\u0018HÖ\u0003J\t\u0010\u0019\u001a\u00020\u0015HÖ\u0001J\t\u0010\u001a\u001a\u00020\u001bHÖ\u0001J\u0019\u0010\u001c\u001a\u00020\u001d2\u0006\u0010\u001e\u001a\u00020\u001f2\u0006\u0010 \u001a\u00020\u0015HÖ\u0001R\u0014\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u0014\u0010\u0006\u001a\u00020\u0003X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u0014\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\u000b¨\u0006\""}, d2 = {"Lcom/stripe/android/GooglePayJsonFactory$BillingAddressParameters;", "Landroid/os/Parcelable;", "isRequired", "", "format", "Lcom/stripe/android/GooglePayJsonFactory$BillingAddressParameters$Format;", "isPhoneNumberRequired", "(ZLcom/stripe/android/GooglePayJsonFactory$BillingAddressParameters$Format;Z)V", "getFormat$stripe_release", "()Lcom/stripe/android/GooglePayJsonFactory$BillingAddressParameters$Format;", "isPhoneNumberRequired$stripe_release", "()Z", "isRequired$stripe_release", "component1", "component1$stripe_release", "component2", "component2$stripe_release", "component3", "component3$stripe_release", "copy", "describeContents", "", "equals", "other", "", "hashCode", "toString", "", "writeToParcel", "", "parcel", "Landroid/os/Parcel;", "flags", "Format", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: GooglePayJsonFactory.kt */
    public static final class BillingAddressParameters implements Parcelable {
        public static final Parcelable.Creator CREATOR = new Creator();
        private final Format format;
        private final boolean isPhoneNumberRequired;
        private final boolean isRequired;

        @Metadata(bv = {1, 0, 3}, k = 3, mv = {1, 1, 16})
        public static class Creator implements Parcelable.Creator {
            public final Object createFromParcel(Parcel parcel) {
                Intrinsics.checkParameterIsNotNull(parcel, "in");
                boolean z = true;
                boolean z2 = parcel.readInt() != 0;
                Format format = (Format) Enum.valueOf(Format.class, parcel.readString());
                if (parcel.readInt() == 0) {
                    z = false;
                }
                return new BillingAddressParameters(z2, format, z);
            }

            public final Object[] newArray(int i) {
                return new BillingAddressParameters[i];
            }
        }

        public BillingAddressParameters() {
            this(false, (Format) null, false, 7, (DefaultConstructorMarker) null);
        }

        public BillingAddressParameters(boolean z) {
            this(z, (Format) null, false, 6, (DefaultConstructorMarker) null);
        }

        public BillingAddressParameters(boolean z, Format format2) {
            this(z, format2, false, 4, (DefaultConstructorMarker) null);
        }

        public static /* synthetic */ BillingAddressParameters copy$default(BillingAddressParameters billingAddressParameters, boolean z, Format format2, boolean z2, int i, Object obj) {
            if ((i & 1) != 0) {
                z = billingAddressParameters.isRequired;
            }
            if ((i & 2) != 0) {
                format2 = billingAddressParameters.format;
            }
            if ((i & 4) != 0) {
                z2 = billingAddressParameters.isPhoneNumberRequired;
            }
            return billingAddressParameters.copy(z, format2, z2);
        }

        public final boolean component1$stripe_release() {
            return this.isRequired;
        }

        public final Format component2$stripe_release() {
            return this.format;
        }

        public final boolean component3$stripe_release() {
            return this.isPhoneNumberRequired;
        }

        public final BillingAddressParameters copy(boolean z, Format format2, boolean z2) {
            Intrinsics.checkParameterIsNotNull(format2, "format");
            return new BillingAddressParameters(z, format2, z2);
        }

        public int describeContents() {
            return 0;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof BillingAddressParameters)) {
                return false;
            }
            BillingAddressParameters billingAddressParameters = (BillingAddressParameters) obj;
            return this.isRequired == billingAddressParameters.isRequired && Intrinsics.areEqual((Object) this.format, (Object) billingAddressParameters.format) && this.isPhoneNumberRequired == billingAddressParameters.isPhoneNumberRequired;
        }

        public int hashCode() {
            boolean z = this.isRequired;
            boolean z2 = true;
            if (z) {
                z = true;
            }
            int i = (z ? 1 : 0) * true;
            Format format2 = this.format;
            int hashCode = (i + (format2 != null ? format2.hashCode() : 0)) * 31;
            boolean z3 = this.isPhoneNumberRequired;
            if (!z3) {
                z2 = z3;
            }
            return hashCode + (z2 ? 1 : 0);
        }

        public String toString() {
            return "BillingAddressParameters(isRequired=" + this.isRequired + ", format=" + this.format + ", isPhoneNumberRequired=" + this.isPhoneNumberRequired + ")";
        }

        public void writeToParcel(Parcel parcel, int i) {
            Intrinsics.checkParameterIsNotNull(parcel, "parcel");
            parcel.writeInt(this.isRequired ? 1 : 0);
            parcel.writeString(this.format.name());
            parcel.writeInt(this.isPhoneNumberRequired ? 1 : 0);
        }

        public BillingAddressParameters(boolean z, Format format2, boolean z2) {
            Intrinsics.checkParameterIsNotNull(format2, "format");
            this.isRequired = z;
            this.format = format2;
            this.isPhoneNumberRequired = z2;
        }

        public final boolean isRequired$stripe_release() {
            return this.isRequired;
        }

        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ BillingAddressParameters(boolean z, Format format2, boolean z2, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this((i & 1) != 0 ? false : z, (i & 2) != 0 ? Format.Min : format2, (i & 4) != 0 ? false : z2);
        }

        public final Format getFormat$stripe_release() {
            return this.format;
        }

        public final boolean isPhoneNumberRequired$stripe_release() {
            return this.isPhoneNumberRequired;
        }

        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0006\b\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u000f\b\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004R\u0014\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006j\u0002\b\u0007j\u0002\b\b¨\u0006\t"}, d2 = {"Lcom/stripe/android/GooglePayJsonFactory$BillingAddressParameters$Format;", "", "code", "", "(Ljava/lang/String;ILjava/lang/String;)V", "getCode$stripe_release", "()Ljava/lang/String;", "Min", "Full", "stripe_release"}, k = 1, mv = {1, 1, 16})
        /* compiled from: GooglePayJsonFactory.kt */
        public enum Format {
            Min("MIN"),
            Full("FULL");
            
            private final String code;

            private Format(String str) {
                this.code = str;
            }

            public final String getCode$stripe_release() {
                return this.code;
            }
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000D\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u001f\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\b\b\u0018\u00002\u00020\u0001:\u000267BS\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\u0007\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\t\u0012\n\b\u0002\u0010\n\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\u000b\u001a\u0004\u0018\u00010\f¢\u0006\u0002\u0010\rJ\u000e\u0010\u001a\u001a\u00020\u0003HÀ\u0003¢\u0006\u0002\b\u001bJ\u000e\u0010\u001c\u001a\u00020\u0005HÀ\u0003¢\u0006\u0002\b\u001dJ\u0010\u0010\u001e\u001a\u0004\u0018\u00010\u0003HÀ\u0003¢\u0006\u0002\b\u001fJ\u0010\u0010 \u001a\u0004\u0018\u00010\u0003HÀ\u0003¢\u0006\u0002\b!J\u0012\u0010\"\u001a\u0004\u0018\u00010\tHÀ\u0003¢\u0006\u0004\b#\u0010\u0014J\u0010\u0010$\u001a\u0004\u0018\u00010\u0003HÀ\u0003¢\u0006\u0002\b%J\u0010\u0010&\u001a\u0004\u0018\u00010\fHÀ\u0003¢\u0006\u0002\b'J^\u0010(\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u0007\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\t2\n\b\u0002\u0010\n\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u000b\u001a\u0004\u0018\u00010\fHÆ\u0001¢\u0006\u0002\u0010)J\t\u0010*\u001a\u00020\tHÖ\u0001J\u0013\u0010+\u001a\u00020,2\b\u0010-\u001a\u0004\u0018\u00010.HÖ\u0003J\t\u0010/\u001a\u00020\tHÖ\u0001J\t\u00100\u001a\u00020\u0003HÖ\u0001J\u0019\u00101\u001a\u0002022\u0006\u00103\u001a\u0002042\u0006\u00105\u001a\u00020\tHÖ\u0001R\u0016\u0010\u000b\u001a\u0004\u0018\u00010\fX\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u000fR\u0016\u0010\u0006\u001a\u0004\u0018\u00010\u0003X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u0011R\u0014\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u0011R\u0018\u0010\b\u001a\u0004\u0018\u00010\tX\u0004¢\u0006\n\n\u0002\u0010\u0015\u001a\u0004\b\u0013\u0010\u0014R\u0016\u0010\n\u001a\u0004\u0018\u00010\u0003X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0016\u0010\u0011R\u0014\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0017\u0010\u0018R\u0016\u0010\u0007\u001a\u0004\u0018\u00010\u0003X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0019\u0010\u0011¨\u00068"}, d2 = {"Lcom/stripe/android/GooglePayJsonFactory$TransactionInfo;", "Landroid/os/Parcelable;", "currencyCode", "", "totalPriceStatus", "Lcom/stripe/android/GooglePayJsonFactory$TransactionInfo$TotalPriceStatus;", "countryCode", "transactionId", "totalPrice", "", "totalPriceLabel", "checkoutOption", "Lcom/stripe/android/GooglePayJsonFactory$TransactionInfo$CheckoutOption;", "(Ljava/lang/String;Lcom/stripe/android/GooglePayJsonFactory$TransactionInfo$TotalPriceStatus;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Lcom/stripe/android/GooglePayJsonFactory$TransactionInfo$CheckoutOption;)V", "getCheckoutOption$stripe_release", "()Lcom/stripe/android/GooglePayJsonFactory$TransactionInfo$CheckoutOption;", "getCountryCode$stripe_release", "()Ljava/lang/String;", "getCurrencyCode$stripe_release", "getTotalPrice$stripe_release", "()Ljava/lang/Integer;", "Ljava/lang/Integer;", "getTotalPriceLabel$stripe_release", "getTotalPriceStatus$stripe_release", "()Lcom/stripe/android/GooglePayJsonFactory$TransactionInfo$TotalPriceStatus;", "getTransactionId$stripe_release", "component1", "component1$stripe_release", "component2", "component2$stripe_release", "component3", "component3$stripe_release", "component4", "component4$stripe_release", "component5", "component5$stripe_release", "component6", "component6$stripe_release", "component7", "component7$stripe_release", "copy", "(Ljava/lang/String;Lcom/stripe/android/GooglePayJsonFactory$TransactionInfo$TotalPriceStatus;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Lcom/stripe/android/GooglePayJsonFactory$TransactionInfo$CheckoutOption;)Lcom/stripe/android/GooglePayJsonFactory$TransactionInfo;", "describeContents", "equals", "", "other", "", "hashCode", "toString", "writeToParcel", "", "parcel", "Landroid/os/Parcel;", "flags", "CheckoutOption", "TotalPriceStatus", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: GooglePayJsonFactory.kt */
    public static final class TransactionInfo implements Parcelable {
        public static final Parcelable.Creator CREATOR = new Creator();
        private final CheckoutOption checkoutOption;
        private final String countryCode;
        private final String currencyCode;
        private final Integer totalPrice;
        private final String totalPriceLabel;
        private final TotalPriceStatus totalPriceStatus;
        private final String transactionId;

        @Metadata(bv = {1, 0, 3}, k = 3, mv = {1, 1, 16})
        public static class Creator implements Parcelable.Creator {
            public final Object createFromParcel(Parcel parcel) {
                Intrinsics.checkParameterIsNotNull(parcel, "in");
                return new TransactionInfo(parcel.readString(), (TotalPriceStatus) Enum.valueOf(TotalPriceStatus.class, parcel.readString()), parcel.readString(), parcel.readString(), parcel.readInt() != 0 ? Integer.valueOf(parcel.readInt()) : null, parcel.readString(), parcel.readInt() != 0 ? (CheckoutOption) Enum.valueOf(CheckoutOption.class, parcel.readString()) : null);
            }

            public final Object[] newArray(int i) {
                return new TransactionInfo[i];
            }
        }

        public TransactionInfo(String str, TotalPriceStatus totalPriceStatus2) {
            this(str, totalPriceStatus2, (String) null, (String) null, (Integer) null, (String) null, (CheckoutOption) null, 124, (DefaultConstructorMarker) null);
        }

        public TransactionInfo(String str, TotalPriceStatus totalPriceStatus2, String str2) {
            this(str, totalPriceStatus2, str2, (String) null, (Integer) null, (String) null, (CheckoutOption) null, 120, (DefaultConstructorMarker) null);
        }

        public TransactionInfo(String str, TotalPriceStatus totalPriceStatus2, String str2, String str3) {
            this(str, totalPriceStatus2, str2, str3, (Integer) null, (String) null, (CheckoutOption) null, 112, (DefaultConstructorMarker) null);
        }

        public TransactionInfo(String str, TotalPriceStatus totalPriceStatus2, String str2, String str3, Integer num) {
            this(str, totalPriceStatus2, str2, str3, num, (String) null, (CheckoutOption) null, 96, (DefaultConstructorMarker) null);
        }

        public TransactionInfo(String str, TotalPriceStatus totalPriceStatus2, String str2, String str3, Integer num, String str4) {
            this(str, totalPriceStatus2, str2, str3, num, str4, (CheckoutOption) null, 64, (DefaultConstructorMarker) null);
        }

        public static /* synthetic */ TransactionInfo copy$default(TransactionInfo transactionInfo, String str, TotalPriceStatus totalPriceStatus2, String str2, String str3, Integer num, String str4, CheckoutOption checkoutOption2, int i, Object obj) {
            if ((i & 1) != 0) {
                str = transactionInfo.currencyCode;
            }
            if ((i & 2) != 0) {
                totalPriceStatus2 = transactionInfo.totalPriceStatus;
            }
            TotalPriceStatus totalPriceStatus3 = totalPriceStatus2;
            if ((i & 4) != 0) {
                str2 = transactionInfo.countryCode;
            }
            String str5 = str2;
            if ((i & 8) != 0) {
                str3 = transactionInfo.transactionId;
            }
            String str6 = str3;
            if ((i & 16) != 0) {
                num = transactionInfo.totalPrice;
            }
            Integer num2 = num;
            if ((i & 32) != 0) {
                str4 = transactionInfo.totalPriceLabel;
            }
            String str7 = str4;
            if ((i & 64) != 0) {
                checkoutOption2 = transactionInfo.checkoutOption;
            }
            return transactionInfo.copy(str, totalPriceStatus3, str5, str6, num2, str7, checkoutOption2);
        }

        public final String component1$stripe_release() {
            return this.currencyCode;
        }

        public final TotalPriceStatus component2$stripe_release() {
            return this.totalPriceStatus;
        }

        public final String component3$stripe_release() {
            return this.countryCode;
        }

        public final String component4$stripe_release() {
            return this.transactionId;
        }

        public final Integer component5$stripe_release() {
            return this.totalPrice;
        }

        public final String component6$stripe_release() {
            return this.totalPriceLabel;
        }

        public final CheckoutOption component7$stripe_release() {
            return this.checkoutOption;
        }

        public final TransactionInfo copy(String str, TotalPriceStatus totalPriceStatus2, String str2, String str3, Integer num, String str4, CheckoutOption checkoutOption2) {
            Intrinsics.checkParameterIsNotNull(str, "currencyCode");
            Intrinsics.checkParameterIsNotNull(totalPriceStatus2, "totalPriceStatus");
            return new TransactionInfo(str, totalPriceStatus2, str2, str3, num, str4, checkoutOption2);
        }

        public int describeContents() {
            return 0;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof TransactionInfo)) {
                return false;
            }
            TransactionInfo transactionInfo = (TransactionInfo) obj;
            return Intrinsics.areEqual((Object) this.currencyCode, (Object) transactionInfo.currencyCode) && Intrinsics.areEqual((Object) this.totalPriceStatus, (Object) transactionInfo.totalPriceStatus) && Intrinsics.areEqual((Object) this.countryCode, (Object) transactionInfo.countryCode) && Intrinsics.areEqual((Object) this.transactionId, (Object) transactionInfo.transactionId) && Intrinsics.areEqual((Object) this.totalPrice, (Object) transactionInfo.totalPrice) && Intrinsics.areEqual((Object) this.totalPriceLabel, (Object) transactionInfo.totalPriceLabel) && Intrinsics.areEqual((Object) this.checkoutOption, (Object) transactionInfo.checkoutOption);
        }

        public int hashCode() {
            String str = this.currencyCode;
            int i = 0;
            int hashCode = (str != null ? str.hashCode() : 0) * 31;
            TotalPriceStatus totalPriceStatus2 = this.totalPriceStatus;
            int hashCode2 = (hashCode + (totalPriceStatus2 != null ? totalPriceStatus2.hashCode() : 0)) * 31;
            String str2 = this.countryCode;
            int hashCode3 = (hashCode2 + (str2 != null ? str2.hashCode() : 0)) * 31;
            String str3 = this.transactionId;
            int hashCode4 = (hashCode3 + (str3 != null ? str3.hashCode() : 0)) * 31;
            Integer num = this.totalPrice;
            int hashCode5 = (hashCode4 + (num != null ? num.hashCode() : 0)) * 31;
            String str4 = this.totalPriceLabel;
            int hashCode6 = (hashCode5 + (str4 != null ? str4.hashCode() : 0)) * 31;
            CheckoutOption checkoutOption2 = this.checkoutOption;
            if (checkoutOption2 != null) {
                i = checkoutOption2.hashCode();
            }
            return hashCode6 + i;
        }

        public String toString() {
            return "TransactionInfo(currencyCode=" + this.currencyCode + ", totalPriceStatus=" + this.totalPriceStatus + ", countryCode=" + this.countryCode + ", transactionId=" + this.transactionId + ", totalPrice=" + this.totalPrice + ", totalPriceLabel=" + this.totalPriceLabel + ", checkoutOption=" + this.checkoutOption + ")";
        }

        public void writeToParcel(Parcel parcel, int i) {
            Intrinsics.checkParameterIsNotNull(parcel, "parcel");
            parcel.writeString(this.currencyCode);
            parcel.writeString(this.totalPriceStatus.name());
            parcel.writeString(this.countryCode);
            parcel.writeString(this.transactionId);
            Integer num = this.totalPrice;
            if (num != null) {
                parcel.writeInt(1);
                parcel.writeInt(num.intValue());
            } else {
                parcel.writeInt(0);
            }
            parcel.writeString(this.totalPriceLabel);
            CheckoutOption checkoutOption2 = this.checkoutOption;
            if (checkoutOption2 != null) {
                parcel.writeInt(1);
                parcel.writeString(checkoutOption2.name());
                return;
            }
            parcel.writeInt(0);
        }

        public TransactionInfo(String str, TotalPriceStatus totalPriceStatus2, String str2, String str3, Integer num, String str4, CheckoutOption checkoutOption2) {
            Intrinsics.checkParameterIsNotNull(str, "currencyCode");
            Intrinsics.checkParameterIsNotNull(totalPriceStatus2, "totalPriceStatus");
            this.currencyCode = str;
            this.totalPriceStatus = totalPriceStatus2;
            this.countryCode = str2;
            this.transactionId = str3;
            this.totalPrice = num;
            this.totalPriceLabel = str4;
            this.checkoutOption = checkoutOption2;
        }

        public final String getCurrencyCode$stripe_release() {
            return this.currencyCode;
        }

        public final TotalPriceStatus getTotalPriceStatus$stripe_release() {
            return this.totalPriceStatus;
        }

        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ TransactionInfo(String str, TotalPriceStatus totalPriceStatus2, String str2, String str3, Integer num, String str4, CheckoutOption checkoutOption2, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this(str, totalPriceStatus2, (i & 4) != 0 ? null : str2, (i & 8) != 0 ? null : str3, (i & 16) != 0 ? null : num, (i & 32) != 0 ? null : str4, (i & 64) != 0 ? null : checkoutOption2);
        }

        public final String getCountryCode$stripe_release() {
            return this.countryCode;
        }

        public final String getTransactionId$stripe_release() {
            return this.transactionId;
        }

        public final Integer getTotalPrice$stripe_release() {
            return this.totalPrice;
        }

        public final String getTotalPriceLabel$stripe_release() {
            return this.totalPriceLabel;
        }

        public final CheckoutOption getCheckoutOption$stripe_release() {
            return this.checkoutOption;
        }

        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0007\b\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u000f\b\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004R\u0014\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006j\u0002\b\u0007j\u0002\b\bj\u0002\b\t¨\u0006\n"}, d2 = {"Lcom/stripe/android/GooglePayJsonFactory$TransactionInfo$TotalPriceStatus;", "", "code", "", "(Ljava/lang/String;ILjava/lang/String;)V", "getCode$stripe_release", "()Ljava/lang/String;", "NotCurrentlyKnown", "Estimated", "Final", "stripe_release"}, k = 1, mv = {1, 1, 16})
        /* compiled from: GooglePayJsonFactory.kt */
        public enum TotalPriceStatus {
            NotCurrentlyKnown("NOT_CURRENTLY_KNOWN"),
            Estimated("ESTIMATED"),
            Final("FINAL");
            
            private final String code;

            private TotalPriceStatus(String str) {
                this.code = str;
            }

            public final String getCode$stripe_release() {
                return this.code;
            }
        }

        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0006\b\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u000f\b\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004R\u0014\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006j\u0002\b\u0007j\u0002\b\b¨\u0006\t"}, d2 = {"Lcom/stripe/android/GooglePayJsonFactory$TransactionInfo$CheckoutOption;", "", "code", "", "(Ljava/lang/String;ILjava/lang/String;)V", "getCode$stripe_release", "()Ljava/lang/String;", "Default", "CompleteImmediatePurchase", "stripe_release"}, k = 1, mv = {1, 1, 16})
        /* compiled from: GooglePayJsonFactory.kt */
        public enum CheckoutOption {
            Default(MessengerShareContentUtility.PREVIEW_DEFAULT),
            CompleteImmediatePurchase("COMPLETE_IMMEDIATE_PURCHASE");
            
            private final String code;

            private CheckoutOption(String str) {
                this.code = str;
            }

            public final String getCode$stripe_release() {
                return this.code;
            }
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\"\n\u0002\u0010\u000e\n\u0002\b\u000f\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\b\b\u0018\u00002\u00020\u0001B+\b\u0007\u0012\b\b\u0002\u0010\u0002\u001a\u00020\u0003\u0012\u000e\b\u0002\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005\u0012\b\b\u0002\u0010\u0007\u001a\u00020\u0003¢\u0006\u0002\u0010\bJ\u000e\u0010\u000f\u001a\u00020\u0003HÀ\u0003¢\u0006\u0002\b\u0010J\u000f\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005HÂ\u0003J\u000e\u0010\u0012\u001a\u00020\u0003HÀ\u0003¢\u0006\u0002\b\u0013J-\u0010\u0014\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\u000e\b\u0002\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u00052\b\b\u0002\u0010\u0007\u001a\u00020\u0003HÆ\u0001J\t\u0010\u0015\u001a\u00020\u0016HÖ\u0001J\u0013\u0010\u0017\u001a\u00020\u00032\b\u0010\u0018\u001a\u0004\u0018\u00010\u0019HÖ\u0003J\t\u0010\u001a\u001a\u00020\u0016HÖ\u0001J\t\u0010\u001b\u001a\u00020\u0006HÖ\u0001J\u0019\u0010\u001c\u001a\u00020\u001d2\u0006\u0010\u001e\u001a\u00020\u001f2\u0006\u0010 \u001a\u00020\u0016HÖ\u0001R\u0014\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005X\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u001a\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\u00060\u00058@X\u0004¢\u0006\u0006\u001a\u0004\b\f\u0010\rR\u0014\u0010\u0007\u001a\u00020\u0003X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\n¨\u0006!"}, d2 = {"Lcom/stripe/android/GooglePayJsonFactory$ShippingAddressParameters;", "Landroid/os/Parcelable;", "isRequired", "", "allowedCountryCodes", "", "", "phoneNumberRequired", "(ZLjava/util/Set;Z)V", "isRequired$stripe_release", "()Z", "normalizedAllowedCountryCodes", "getNormalizedAllowedCountryCodes$stripe_release", "()Ljava/util/Set;", "getPhoneNumberRequired$stripe_release", "component1", "component1$stripe_release", "component2", "component3", "component3$stripe_release", "copy", "describeContents", "", "equals", "other", "", "hashCode", "toString", "writeToParcel", "", "parcel", "Landroid/os/Parcel;", "flags", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: GooglePayJsonFactory.kt */
    public static final class ShippingAddressParameters implements Parcelable {
        public static final Parcelable.Creator CREATOR = new Creator();
        private final Set<String> allowedCountryCodes;
        private final boolean isRequired;
        private final boolean phoneNumberRequired;

        @Metadata(bv = {1, 0, 3}, k = 3, mv = {1, 1, 16})
        public static class Creator implements Parcelable.Creator {
            public final Object createFromParcel(Parcel parcel) {
                Intrinsics.checkParameterIsNotNull(parcel, "in");
                boolean z = true;
                boolean z2 = parcel.readInt() != 0;
                int readInt = parcel.readInt();
                LinkedHashSet linkedHashSet = new LinkedHashSet(readInt);
                while (readInt != 0) {
                    linkedHashSet.add(parcel.readString());
                    readInt--;
                }
                if (parcel.readInt() == 0) {
                    z = false;
                }
                return new ShippingAddressParameters(z2, linkedHashSet, z);
            }

            public final Object[] newArray(int i) {
                return new ShippingAddressParameters[i];
            }
        }

        public ShippingAddressParameters() {
            this(false, (Set) null, false, 7, (DefaultConstructorMarker) null);
        }

        public ShippingAddressParameters(boolean z) {
            this(z, (Set) null, false, 6, (DefaultConstructorMarker) null);
        }

        public ShippingAddressParameters(boolean z, Set<String> set) {
            this(z, set, false, 4, (DefaultConstructorMarker) null);
        }

        private final Set<String> component2() {
            return this.allowedCountryCodes;
        }

        public static /* synthetic */ ShippingAddressParameters copy$default(ShippingAddressParameters shippingAddressParameters, boolean z, Set<String> set, boolean z2, int i, Object obj) {
            if ((i & 1) != 0) {
                z = shippingAddressParameters.isRequired;
            }
            if ((i & 2) != 0) {
                set = shippingAddressParameters.allowedCountryCodes;
            }
            if ((i & 4) != 0) {
                z2 = shippingAddressParameters.phoneNumberRequired;
            }
            return shippingAddressParameters.copy(z, set, z2);
        }

        public final boolean component1$stripe_release() {
            return this.isRequired;
        }

        public final boolean component3$stripe_release() {
            return this.phoneNumberRequired;
        }

        public final ShippingAddressParameters copy(boolean z, Set<String> set, boolean z2) {
            Intrinsics.checkParameterIsNotNull(set, "allowedCountryCodes");
            return new ShippingAddressParameters(z, set, z2);
        }

        public int describeContents() {
            return 0;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof ShippingAddressParameters)) {
                return false;
            }
            ShippingAddressParameters shippingAddressParameters = (ShippingAddressParameters) obj;
            return this.isRequired == shippingAddressParameters.isRequired && Intrinsics.areEqual((Object) this.allowedCountryCodes, (Object) shippingAddressParameters.allowedCountryCodes) && this.phoneNumberRequired == shippingAddressParameters.phoneNumberRequired;
        }

        public int hashCode() {
            boolean z = this.isRequired;
            boolean z2 = true;
            if (z) {
                z = true;
            }
            int i = (z ? 1 : 0) * true;
            Set<String> set = this.allowedCountryCodes;
            int hashCode = (i + (set != null ? set.hashCode() : 0)) * 31;
            boolean z3 = this.phoneNumberRequired;
            if (!z3) {
                z2 = z3;
            }
            return hashCode + (z2 ? 1 : 0);
        }

        public String toString() {
            return "ShippingAddressParameters(isRequired=" + this.isRequired + ", allowedCountryCodes=" + this.allowedCountryCodes + ", phoneNumberRequired=" + this.phoneNumberRequired + ")";
        }

        public void writeToParcel(Parcel parcel, int i) {
            Intrinsics.checkParameterIsNotNull(parcel, "parcel");
            parcel.writeInt(this.isRequired ? 1 : 0);
            Set<String> set = this.allowedCountryCodes;
            parcel.writeInt(set.size());
            for (String writeString : set) {
                parcel.writeString(writeString);
            }
            parcel.writeInt(this.phoneNumberRequired ? 1 : 0);
        }

        public ShippingAddressParameters(boolean z, Set<String> set, boolean z2) {
            Intrinsics.checkParameterIsNotNull(set, "allowedCountryCodes");
            this.isRequired = z;
            this.allowedCountryCodes = set;
            this.phoneNumberRequired = z2;
            String[] iSOCountries = Locale.getISOCountries();
            for (String str : getNormalizedAllowedCountryCodes$stripe_release()) {
                Intrinsics.checkExpressionValueIsNotNull(iSOCountries, "countryCodes");
                int length = iSOCountries.length;
                boolean z3 = false;
                int i = 0;
                while (true) {
                    if (i >= length) {
                        break;
                    } else if (Intrinsics.areEqual((Object) str, (Object) iSOCountries[i])) {
                        z3 = true;
                        continue;
                        break;
                    } else {
                        i++;
                    }
                }
                if (!z3) {
                    throw new IllegalArgumentException(('\'' + str + "' is not a valid country code").toString());
                }
            }
        }

        public final boolean isRequired$stripe_release() {
            return this.isRequired;
        }

        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ ShippingAddressParameters(boolean z, Set set, boolean z2, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this((i & 1) != 0 ? false : z, (i & 2) != 0 ? SetsKt.emptySet() : set, (i & 4) != 0 ? false : z2);
        }

        public final boolean getPhoneNumberRequired$stripe_release() {
            return this.phoneNumberRequired;
        }

        public final Set<String> getNormalizedAllowedCountryCodes$stripe_release() {
            Iterable<String> iterable = this.allowedCountryCodes;
            Collection arrayList = new ArrayList(CollectionsKt.collectionSizeOrDefault(iterable, 10));
            for (String str : iterable) {
                Locale locale = Locale.ROOT;
                Intrinsics.checkExpressionValueIsNotNull(locale, "Locale.ROOT");
                if (str != null) {
                    String upperCase = str.toUpperCase(locale);
                    Intrinsics.checkExpressionValueIsNotNull(upperCase, "(this as java.lang.String).toUpperCase(locale)");
                    arrayList.add(upperCase);
                } else {
                    throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
                }
            }
            return CollectionsKt.toSet((List) arrayList);
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0007\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\b\b\u0018\u00002\u00020\u0001B\u0011\u0012\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u0003¢\u0006\u0002\u0010\u0004J\u0010\u0010\u0007\u001a\u0004\u0018\u00010\u0003HÀ\u0003¢\u0006\u0002\b\bJ\u0015\u0010\t\u001a\u00020\u00002\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u0003HÆ\u0001J\t\u0010\n\u001a\u00020\u000bHÖ\u0001J\u0013\u0010\f\u001a\u00020\r2\b\u0010\u000e\u001a\u0004\u0018\u00010\u000fHÖ\u0003J\t\u0010\u0010\u001a\u00020\u000bHÖ\u0001J\t\u0010\u0011\u001a\u00020\u0003HÖ\u0001J\u0019\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u000bHÖ\u0001R\u0016\u0010\u0002\u001a\u0004\u0018\u00010\u0003X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006¨\u0006\u0017"}, d2 = {"Lcom/stripe/android/GooglePayJsonFactory$MerchantInfo;", "Landroid/os/Parcelable;", "merchantName", "", "(Ljava/lang/String;)V", "getMerchantName$stripe_release", "()Ljava/lang/String;", "component1", "component1$stripe_release", "copy", "describeContents", "", "equals", "", "other", "", "hashCode", "toString", "writeToParcel", "", "parcel", "Landroid/os/Parcel;", "flags", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: GooglePayJsonFactory.kt */
    public static final class MerchantInfo implements Parcelable {
        public static final Parcelable.Creator CREATOR = new Creator();
        private final String merchantName;

        @Metadata(bv = {1, 0, 3}, k = 3, mv = {1, 1, 16})
        public static class Creator implements Parcelable.Creator {
            public final Object createFromParcel(Parcel parcel) {
                Intrinsics.checkParameterIsNotNull(parcel, "in");
                return new MerchantInfo(parcel.readString());
            }

            public final Object[] newArray(int i) {
                return new MerchantInfo[i];
            }
        }

        public MerchantInfo() {
            this((String) null, 1, (DefaultConstructorMarker) null);
        }

        public static /* synthetic */ MerchantInfo copy$default(MerchantInfo merchantInfo, String str, int i, Object obj) {
            if ((i & 1) != 0) {
                str = merchantInfo.merchantName;
            }
            return merchantInfo.copy(str);
        }

        public final String component1$stripe_release() {
            return this.merchantName;
        }

        public final MerchantInfo copy(String str) {
            return new MerchantInfo(str);
        }

        public int describeContents() {
            return 0;
        }

        public boolean equals(Object obj) {
            if (this != obj) {
                return (obj instanceof MerchantInfo) && Intrinsics.areEqual((Object) this.merchantName, (Object) ((MerchantInfo) obj).merchantName);
            }
            return true;
        }

        public int hashCode() {
            String str = this.merchantName;
            if (str != null) {
                return str.hashCode();
            }
            return 0;
        }

        public String toString() {
            return "MerchantInfo(merchantName=" + this.merchantName + ")";
        }

        public void writeToParcel(Parcel parcel, int i) {
            Intrinsics.checkParameterIsNotNull(parcel, "parcel");
            parcel.writeString(this.merchantName);
        }

        public MerchantInfo(String str) {
            this.merchantName = str;
        }

        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ MerchantInfo(String str, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this((i & 1) != 0 ? null : str);
        }

        public final String getMerchantName$stripe_release() {
            return this.merchantName;
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0002\b\u0005\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0014\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007XT¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0007XT¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0005XT¢\u0006\u0002\n\u0000R\u0014\u0010\n\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u0005XT¢\u0006\u0002\n\u0000¨\u0006\f"}, d2 = {"Lcom/stripe/android/GooglePayJsonFactory$Companion;", "", "()V", "ALLOWED_AUTH_METHODS", "", "", "API_VERSION", "", "API_VERSION_MINOR", "CARD_PAYMENT_METHOD", "DEFAULT_CARD_NETWORKS", "JCB_CARD_NETWORK", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: GooglePayJsonFactory.kt */
    private static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }
}
