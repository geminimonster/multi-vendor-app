package com.stripe.android;

import com.stripe.android.exception.StripeException;
import kotlin.Metadata;
import kotlin.Result;
import kotlin.coroutines.Continuation;
import kotlin.coroutines.CoroutineContext;
import kotlin.jvm.internal.Intrinsics;
import kotlinx.coroutines.CoroutineScope;
import kotlinx.coroutines.CoroutineScopeKt;
import kotlinx.coroutines.CoroutineStart;
import kotlinx.coroutines.Dispatchers;
import kotlinx.coroutines.Job;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\b \u0018\u0000*\u0004\b\u0000\u0010\u00012\u00020\u0002B\u001d\u0012\b\b\u0002\u0010\u0003\u001a\u00020\u0004\u0012\f\u0010\u0005\u001a\b\u0012\u0004\u0012\u00028\u00000\u0006¢\u0006\u0002\u0010\u0007J \u0010\b\u001a\u00020\t2\u000e\u0010\n\u001a\n\u0012\u0006\u0012\u0004\u0018\u00018\u00000\u000bH\u0002ø\u0001\u0000¢\u0006\u0002\u0010\fJ\r\u0010\r\u001a\u00020\tH\u0000¢\u0006\u0002\b\u000eJ\u0015\u0010\u000f\u001a\u0004\u0018\u00018\u0000H @ø\u0001\u0000¢\u0006\u0004\b\u0010\u0010\u0011R\u0014\u0010\u0005\u001a\b\u0012\u0004\u0012\u00028\u00000\u0006X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0004¢\u0006\u0002\n\u0000\u0002\u0004\n\u0002\b\u0019¨\u0006\u0012"}, d2 = {"Lcom/stripe/android/ApiOperation;", "ResultType", "", "workScope", "Lkotlinx/coroutines/CoroutineScope;", "callback", "Lcom/stripe/android/ApiResultCallback;", "(Lkotlinx/coroutines/CoroutineScope;Lcom/stripe/android/ApiResultCallback;)V", "dispatchResult", "", "result", "Lkotlin/Result;", "(Ljava/lang/Object;)V", "execute", "execute$stripe_release", "getResult", "getResult$stripe_release", "(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: ApiOperation.kt */
public abstract class ApiOperation<ResultType> {
    private final ApiResultCallback<ResultType> callback;
    private final CoroutineScope workScope;

    public abstract Object getResult$stripe_release(Continuation<? super ResultType> continuation);

    public ApiOperation(CoroutineScope coroutineScope, ApiResultCallback<ResultType> apiResultCallback) {
        Intrinsics.checkParameterIsNotNull(coroutineScope, "workScope");
        Intrinsics.checkParameterIsNotNull(apiResultCallback, "callback");
        this.workScope = coroutineScope;
        this.callback = apiResultCallback;
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ ApiOperation(CoroutineScope coroutineScope, ApiResultCallback apiResultCallback, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this((i & 1) != 0 ? CoroutineScopeKt.CoroutineScope(Dispatchers.getIO()) : coroutineScope, apiResultCallback);
    }

    public final void execute$stripe_release() {
        Job unused = BuildersKt__Builders_commonKt.launch$default(this.workScope, (CoroutineContext) null, (CoroutineStart) null, new ApiOperation$execute$1(this, (Continuation) null), 3, (Object) null);
    }

    /* access modifiers changed from: private */
    public final void dispatchResult(Object obj) {
        Exception exc;
        Throwable r0 = Result.m7exceptionOrNullimpl(obj);
        if (r0 != null) {
            ApiResultCallback<ResultType> apiResultCallback = this.callback;
            if (r0 instanceof StripeException) {
                exc = (Exception) r0;
            } else {
                exc = new RuntimeException(r0);
            }
            apiResultCallback.onError(exc);
        } else if (obj != null) {
            this.callback.onSuccess(obj);
        } else {
            this.callback.onError(new RuntimeException("The API operation returned neither a result or exception"));
        }
    }
}
