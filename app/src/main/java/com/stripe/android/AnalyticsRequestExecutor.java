package com.stripe.android;

import com.facebook.share.internal.ShareConstants;
import com.stripe.android.ConnectionFactory;
import kotlin.Metadata;
import kotlin.coroutines.Continuation;
import kotlin.coroutines.CoroutineContext;
import kotlin.jvm.internal.Intrinsics;
import kotlinx.coroutines.CompletableJob;
import kotlinx.coroutines.CoroutineScope;
import kotlinx.coroutines.CoroutineScopeKt;
import kotlinx.coroutines.CoroutineStart;
import kotlinx.coroutines.Dispatchers;
import kotlinx.coroutines.Job;
import kotlinx.coroutines.SupervisorKt;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\b`\u0018\u00002\u00020\u0001:\u0001\u0006J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&¨\u0006\u0007"}, d2 = {"Lcom/stripe/android/AnalyticsRequestExecutor;", "", "executeAsync", "", "request", "Lcom/stripe/android/AnalyticsRequest;", "Default", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: AnalyticsRequestExecutor.kt */
public interface AnalyticsRequestExecutor {
    void executeAsync(AnalyticsRequest analyticsRequest);

    @Metadata(bv = {1, 0, 3}, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\u0018\u00002\u00020\u0001B\u000f\u0012\b\b\u0002\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u0015\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000eH\u0001¢\u0006\u0002\b\u000fJ\u0010\u0010\u0010\u001a\u00020\u00112\u0006\u0010\r\u001a\u00020\u000eH\u0016R\u000e\u0010\u0005\u001a\u00020\u0006X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\bX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0004¢\u0006\u0002\n\u0000¨\u0006\u0012"}, d2 = {"Lcom/stripe/android/AnalyticsRequestExecutor$Default;", "Lcom/stripe/android/AnalyticsRequestExecutor;", "logger", "Lcom/stripe/android/Logger;", "(Lcom/stripe/android/Logger;)V", "connectionFactory", "Lcom/stripe/android/ConnectionFactory$Default;", "job", "Lkotlinx/coroutines/CompletableJob;", "scope", "Lkotlinx/coroutines/CoroutineScope;", "execute", "", "request", "Lcom/stripe/android/AnalyticsRequest;", "execute$stripe_release", "executeAsync", "", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: AnalyticsRequestExecutor.kt */
    public static final class Default implements AnalyticsRequestExecutor {
        private final ConnectionFactory.Default connectionFactory;
        private final CompletableJob job;
        /* access modifiers changed from: private */
        public final Logger logger;
        private final CoroutineScope scope;

        public Default() {
            this((Logger) null, 1, (DefaultConstructorMarker) null);
        }

        public Default(Logger logger2) {
            Intrinsics.checkParameterIsNotNull(logger2, "logger");
            this.logger = logger2;
            this.connectionFactory = new ConnectionFactory.Default();
            this.job = SupervisorKt.SupervisorJob$default((Job) null, 1, (Object) null);
            this.scope = CoroutineScopeKt.CoroutineScope(Dispatchers.getDefault().plus(this.job));
        }

        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ Default(Logger logger2, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this((i & 1) != 0 ? Logger.Companion.noop$stripe_release() : logger2);
        }

        /* JADX WARNING: Code restructure failed: missing block: B:15:0x002e, code lost:
            r1 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:16:0x002f, code lost:
            kotlin.io.CloseableKt.closeFinally(r0, r4);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:17:0x0032, code lost:
            throw r1;
         */
        /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final int execute$stripe_release(com.stripe.android.AnalyticsRequest r4) throws com.stripe.android.exception.APIConnectionException, com.stripe.android.exception.InvalidRequestException {
            /*
                r3 = this;
                java.lang.String r0 = "request"
                kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(r4, r0)
                com.stripe.android.ConnectionFactory$Default r0 = r3.connectionFactory
                r1 = r4
                com.stripe.android.StripeRequest r1 = (com.stripe.android.StripeRequest) r1
                com.stripe.android.StripeConnection r0 = r0.create(r1)
                java.io.Closeable r0 = (java.io.Closeable) r0
                r1 = 0
                java.lang.Throwable r1 = (java.lang.Throwable) r1
                r2 = r0
                com.stripe.android.StripeConnection r2 = (com.stripe.android.StripeConnection) r2     // Catch:{ all -> 0x002c }
                int r2 = r2.getResponseCode()     // Catch:{ IOException -> 0x001e }
                kotlin.io.CloseableKt.closeFinally(r0, r1)     // Catch:{  }
                return r2
            L_0x001e:
                r1 = move-exception
                com.stripe.android.exception.APIConnectionException$Companion r2 = com.stripe.android.exception.APIConnectionException.Companion     // Catch:{ all -> 0x002c }
                java.lang.String r4 = r4.getBaseUrl()     // Catch:{ all -> 0x002c }
                com.stripe.android.exception.APIConnectionException r4 = r2.create$stripe_release(r1, r4)     // Catch:{ all -> 0x002c }
                java.lang.Throwable r4 = (java.lang.Throwable) r4     // Catch:{ all -> 0x002c }
                throw r4     // Catch:{ all -> 0x002c }
            L_0x002c:
                r4 = move-exception
                throw r4     // Catch:{ all -> 0x002e }
            L_0x002e:
                r1 = move-exception
                kotlin.io.CloseableKt.closeFinally(r0, r4)
                throw r1
            */
            throw new UnsupportedOperationException("Method not decompiled: com.stripe.android.AnalyticsRequestExecutor.Default.execute$stripe_release(com.stripe.android.AnalyticsRequest):int");
        }

        public void executeAsync(AnalyticsRequest analyticsRequest) {
            Intrinsics.checkParameterIsNotNull(analyticsRequest, ShareConstants.WEB_DIALOG_RESULT_PARAM_REQUEST_ID);
            Job unused = BuildersKt__Builders_commonKt.launch$default(this.scope, (CoroutineContext) null, (CoroutineStart) null, new AnalyticsRequestExecutor$Default$executeAsync$1(this, analyticsRequest, (Continuation) null), 3, (Object) null);
        }
    }
}
