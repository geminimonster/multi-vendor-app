package com.stripe.android;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.HandlerThread;
import androidx.fragment.app.Fragment;
import com.facebook.internal.NativeProtocol;
import com.stripe.android.AnalyticsRequest;
import com.stripe.android.ApiRequest;
import com.stripe.android.PaymentAuthWebViewStarter;
import com.stripe.android.PaymentController;
import com.stripe.android.PaymentRelayStarter;
import com.stripe.android.StripeRepository;
import com.stripe.android.exception.APIException;
import com.stripe.android.exception.StripeException;
import com.stripe.android.model.ConfirmPaymentIntentParams;
import com.stripe.android.model.ConfirmSetupIntentParams;
import com.stripe.android.model.ConfirmStripeIntentParams;
import com.stripe.android.model.PaymentIntent;
import com.stripe.android.model.Source;
import com.stripe.android.model.SourceRedirect;
import com.stripe.android.model.Stripe3ds2AuthResult;
import com.stripe.android.model.Stripe3ds2Fingerprint;
import com.stripe.android.model.StripeIntent;
import com.stripe.android.stripe3ds2.init.ui.StripeToolbarCustomization;
import com.stripe.android.stripe3ds2.init.ui.StripeUiCustomization;
import com.stripe.android.stripe3ds2.service.StripeThreeDs2Service;
import com.stripe.android.stripe3ds2.transaction.AuthenticationRequestParameters;
import com.stripe.android.stripe3ds2.transaction.ChallengeParameters;
import com.stripe.android.stripe3ds2.transaction.CompletionEvent;
import com.stripe.android.stripe3ds2.transaction.MessageVersionRegistry;
import com.stripe.android.stripe3ds2.transaction.ProtocolErrorEvent;
import com.stripe.android.stripe3ds2.transaction.RuntimeErrorEvent;
import com.stripe.android.stripe3ds2.transaction.Stripe3ds2ActivityStarterHost;
import com.stripe.android.stripe3ds2.transaction.StripeChallengeStatusReceiver;
import com.stripe.android.stripe3ds2.transaction.Transaction;
import com.stripe.android.stripe3ds2.views.ChallengeProgressDialogActivity;
import com.stripe.android.view.AuthActivityStarter;
import com.stripe.android.view.Stripe3ds2CompletionActivity;
import java.security.cert.CertificateException;
import java.util.List;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.collections.CollectionsKt;
import kotlin.coroutines.Continuation;
import kotlin.coroutines.jvm.internal.Boxing;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.Intrinsics;
import kotlinx.coroutines.CoroutineScope;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000¾\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\f\n\u0002\u0018\u0002\n\u0002\b\n\b\u0000\u0018\u0000 Q2\u00020\u0001:\bNOPQRSTUBy\b\u0000\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\b\b\u0002\u0010\b\u001a\u00020\t\u0012\b\b\u0002\u0010\n\u001a\u00020\u000b\u0012\b\b\u0002\u0010\f\u001a\u00020\r\u0012\b\b\u0002\u0010\u000e\u001a\u00020\u000f\u0012\b\b\u0002\u0010\u0010\u001a\u00020\u0011\u0012\b\b\u0002\u0010\u0012\u001a\u00020\u0013\u0012\b\b\u0002\u0010\u0014\u001a\u00020\u0015\u0012\b\b\u0002\u0010\u0016\u001a\u00020\u0017\u0012\b\b\u0002\u0010\u0018\u001a\u00020\u0019¢\u0006\u0002\u0010\u001aJ0\u0010\u001f\u001a\u00020 2\u0006\u0010!\u001a\u00020\"2\b\u0010#\u001a\u0004\u0018\u00010\u00052\u0006\u0010$\u001a\u00020%2\f\u0010&\u001a\b\u0012\u0004\u0012\u00020(0'H\u0016J(\u0010)\u001a\u00020 2\u0006\u0010*\u001a\u00020+2\u0006\u0010,\u001a\u00020\"2\u0006\u0010-\u001a\u00020.2\u0006\u0010/\u001a\u000200H\u0002J\"\u00101\u001a\u00020 2\u0006\u0010*\u001a\u00020+2\u0006\u00102\u001a\u0002032\b\u0010#\u001a\u0004\u0018\u00010\u0005H\u0002J\"\u00101\u001a\u00020 2\u0006\u0010*\u001a\u00020+2\u0006\u0010,\u001a\u00020\"2\b\u0010#\u001a\u0004\u0018\u00010\u0005H\u0002J>\u00104\u001a\b\u0012\u0004\u0012\u00020\"0'2\u0006\u0010/\u001a\u0002002\u0006\u00105\u001a\u0002062\u0006\u00107\u001a\u00020\u00052\b\b\u0002\u00108\u001a\u00020\t2\f\u0010&\u001a\b\u0012\u0004\u0012\u00020(0'H\u0002J>\u00109\u001a\b\u0012\u0004\u0012\u00020\"0'2\u0006\u0010/\u001a\u0002002\u0006\u00105\u001a\u0002062\u0006\u00107\u001a\u00020\u00052\b\b\u0002\u00108\u001a\u00020\t2\f\u0010:\u001a\b\u0012\u0004\u0012\u00020;0'H\u0002J \u0010<\u001a\u00020 2\u0006\u0010*\u001a\u00020+2\u0006\u0010,\u001a\u00020\"2\u0006\u0010/\u001a\u000200H\u0017J\u001e\u0010=\u001a\u00020 2\u0006\u0010>\u001a\u00020?2\f\u0010&\u001a\b\u0012\u0004\u0012\u00020(0'H\u0016J\u001e\u0010@\u001a\u00020 2\u0006\u0010>\u001a\u00020?2\f\u0010&\u001a\b\u0012\u0004\u0012\u00020;0'H\u0016J\u001e\u0010A\u001a\u00020 2\u0006\u0010>\u001a\u00020?2\f\u0010&\u001a\b\u0012\u0004\u0012\u0002030'H\u0016J \u0010B\u001a\u00020 2\u0006\u0010*\u001a\u00020+2\u0006\u00102\u001a\u0002032\u0006\u0010/\u001a\u000200H\u0002J\u001a\u0010C\u001a\u00020\t2\u0006\u0010D\u001a\u0002062\b\u0010>\u001a\u0004\u0018\u00010?H\u0016J\u001a\u0010E\u001a\u00020\t2\u0006\u0010D\u001a\u0002062\b\u0010>\u001a\u0004\u0018\u00010?H\u0016J\u001a\u0010F\u001a\u00020\t2\u0006\u0010D\u001a\u0002062\b\u0010>\u001a\u0004\u0018\u00010?H\u0016J \u0010G\u001a\u00020 2\u0006\u0010*\u001a\u00020+2\u0006\u0010H\u001a\u00020\u00052\u0006\u0010/\u001a\u000200H\u0016J \u0010I\u001a\u00020 2\u0006\u0010*\u001a\u00020+2\u0006\u00102\u001a\u0002032\u0006\u0010/\u001a\u000200H\u0016J&\u0010J\u001a\u00020 2\u0006\u0010K\u001a\u00020L2\u0006\u0010/\u001a\u0002002\f\u0010&\u001a\b\u0012\u0004\u0012\u00020\"0'H\u0016J \u0010M\u001a\u00020 2\u0006\u0010*\u001a\u00020+2\u0006\u0010K\u001a\u00020L2\u0006\u0010/\u001a\u000200H\u0016R\u000e\u0010\u0012\u001a\u00020\u0013X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u001b\u001a\u00020\u001cX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0015X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\u0017X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\rX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u001d\u001a\u00020\u001eX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0018\u001a\u00020\u0019X\u0004¢\u0006\u0002\n\u0000\u0002\u0004\n\u0002\b\u0019¨\u0006V"}, d2 = {"Lcom/stripe/android/StripePaymentController;", "Lcom/stripe/android/PaymentController;", "context", "Landroid/content/Context;", "publishableKey", "", "stripeRepository", "Lcom/stripe/android/StripeRepository;", "enableLogging", "", "messageVersionRegistry", "Lcom/stripe/android/stripe3ds2/transaction/MessageVersionRegistry;", "config", "Lcom/stripe/android/PaymentAuthConfig;", "threeDs2Service", "Lcom/stripe/android/stripe3ds2/service/StripeThreeDs2Service;", "analyticsRequestExecutor", "Lcom/stripe/android/AnalyticsRequestExecutor;", "analyticsDataFactory", "Lcom/stripe/android/AnalyticsDataFactory;", "challengeFlowStarter", "Lcom/stripe/android/StripePaymentController$ChallengeFlowStarter;", "challengeProgressDialogActivityStarter", "Lcom/stripe/android/StripePaymentController$ChallengeProgressDialogActivityStarter;", "workScope", "Lkotlinx/coroutines/CoroutineScope;", "(Landroid/content/Context;Ljava/lang/String;Lcom/stripe/android/StripeRepository;ZLcom/stripe/android/stripe3ds2/transaction/MessageVersionRegistry;Lcom/stripe/android/PaymentAuthConfig;Lcom/stripe/android/stripe3ds2/service/StripeThreeDs2Service;Lcom/stripe/android/AnalyticsRequestExecutor;Lcom/stripe/android/AnalyticsDataFactory;Lcom/stripe/android/StripePaymentController$ChallengeFlowStarter;Lcom/stripe/android/StripePaymentController$ChallengeProgressDialogActivityStarter;Lkotlinx/coroutines/CoroutineScope;)V", "analyticsRequestFactory", "Lcom/stripe/android/AnalyticsRequest$Factory;", "logger", "Lcom/stripe/android/Logger;", "authenticateAlipay", "", "intent", "Lcom/stripe/android/model/StripeIntent;", "stripeAccountId", "authenticator", "Lcom/stripe/android/AlipayAuthenticator;", "callback", "Lcom/stripe/android/ApiResultCallback;", "Lcom/stripe/android/PaymentIntentResult;", "begin3ds2Auth", "host", "Lcom/stripe/android/view/AuthActivityStarter$Host;", "stripeIntent", "stripe3ds2Fingerprint", "Lcom/stripe/android/model/Stripe3ds2Fingerprint;", "requestOptions", "Lcom/stripe/android/ApiRequest$Options;", "bypassAuth", "source", "Lcom/stripe/android/model/Source;", "createPaymentIntentCallback", "flowOutcome", "", "sourceId", "shouldCancelSource", "createSetupIntentCallback", "resultCallback", "Lcom/stripe/android/SetupIntentResult;", "handleNextAction", "handlePaymentResult", "data", "Landroid/content/Intent;", "handleSetupResult", "handleSourceResult", "onSourceRetrieved", "shouldHandlePaymentResult", "requestCode", "shouldHandleSetupResult", "shouldHandleSourceResult", "startAuth", "clientSecret", "startAuthenticateSource", "startConfirm", "confirmStripeIntentParams", "Lcom/stripe/android/model/ConfirmStripeIntentParams;", "startConfirmAndAuth", "AlipayAuthenticationTask", "ChallengeFlowStarter", "ChallengeProgressDialogActivityStarter", "Companion", "ConfirmStripeIntentCallback", "ConfirmStripeIntentTask", "PaymentAuth3ds2ChallengeStatusReceiver", "Stripe3ds2AuthCallback", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: StripePaymentController.kt */
public final class StripePaymentController implements PaymentController {
    public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
    /* access modifiers changed from: private */
    public static final List<String> EXPAND_PAYMENT_METHOD = CollectionsKt.listOf("payment_method");
    public static final int PAYMENT_REQUEST_CODE = 50000;
    public static final int SETUP_REQUEST_CODE = 50001;
    public static final int SOURCE_REQUEST_CODE = 50002;
    private final AnalyticsDataFactory analyticsDataFactory;
    private final AnalyticsRequestExecutor analyticsRequestExecutor;
    private final AnalyticsRequest.Factory analyticsRequestFactory;
    private final ChallengeFlowStarter challengeFlowStarter;
    private final ChallengeProgressDialogActivityStarter challengeProgressDialogActivityStarter;
    private final PaymentAuthConfig config;
    private final boolean enableLogging;
    /* access modifiers changed from: private */
    public final Logger logger;
    private final MessageVersionRegistry messageVersionRegistry;
    /* access modifiers changed from: private */
    public final String publishableKey;
    /* access modifiers changed from: private */
    public final StripeRepository stripeRepository;
    private final StripeThreeDs2Service threeDs2Service;
    private final CoroutineScope workScope;

    @JvmStatic
    public static final PaymentController create(Context context, String str, StripeRepository stripeRepository2) {
        return Companion.create$default(Companion, context, str, stripeRepository2, false, 8, (Object) null);
    }

    @JvmStatic
    public static final PaymentController create(Context context, String str, StripeRepository stripeRepository2, boolean z) {
        return Companion.create(context, str, stripeRepository2, z);
    }

    public boolean shouldHandlePaymentResult(int i, Intent intent) {
        return i == 50000 && intent != null;
    }

    public boolean shouldHandleSetupResult(int i, Intent intent) {
        return i == 50001 && intent != null;
    }

    public boolean shouldHandleSourceResult(int i, Intent intent) {
        return i == 50002 && intent != null;
    }

    public StripePaymentController(Context context, String str, StripeRepository stripeRepository2, boolean z, MessageVersionRegistry messageVersionRegistry2, PaymentAuthConfig paymentAuthConfig, StripeThreeDs2Service stripeThreeDs2Service, AnalyticsRequestExecutor analyticsRequestExecutor2, AnalyticsDataFactory analyticsDataFactory2, ChallengeFlowStarter challengeFlowStarter2, ChallengeProgressDialogActivityStarter challengeProgressDialogActivityStarter2, CoroutineScope coroutineScope) {
        Intrinsics.checkParameterIsNotNull(context, "context");
        Intrinsics.checkParameterIsNotNull(str, "publishableKey");
        Intrinsics.checkParameterIsNotNull(stripeRepository2, "stripeRepository");
        Intrinsics.checkParameterIsNotNull(messageVersionRegistry2, "messageVersionRegistry");
        Intrinsics.checkParameterIsNotNull(paymentAuthConfig, "config");
        Intrinsics.checkParameterIsNotNull(stripeThreeDs2Service, "threeDs2Service");
        Intrinsics.checkParameterIsNotNull(analyticsRequestExecutor2, "analyticsRequestExecutor");
        Intrinsics.checkParameterIsNotNull(analyticsDataFactory2, "analyticsDataFactory");
        Intrinsics.checkParameterIsNotNull(challengeFlowStarter2, "challengeFlowStarter");
        Intrinsics.checkParameterIsNotNull(challengeProgressDialogActivityStarter2, "challengeProgressDialogActivityStarter");
        Intrinsics.checkParameterIsNotNull(coroutineScope, "workScope");
        this.publishableKey = str;
        this.stripeRepository = stripeRepository2;
        this.enableLogging = z;
        this.messageVersionRegistry = messageVersionRegistry2;
        this.config = paymentAuthConfig;
        this.threeDs2Service = stripeThreeDs2Service;
        this.analyticsRequestExecutor = analyticsRequestExecutor2;
        this.analyticsDataFactory = analyticsDataFactory2;
        this.challengeFlowStarter = challengeFlowStarter2;
        this.challengeProgressDialogActivityStarter = challengeProgressDialogActivityStarter2;
        this.workScope = coroutineScope;
        Logger instance$stripe_release = Logger.Companion.getInstance$stripe_release(this.enableLogging);
        this.logger = instance$stripe_release;
        this.analyticsRequestFactory = new AnalyticsRequest.Factory(instance$stripe_release);
        this.threeDs2Service.initialize(this.config.getStripe3ds2Config$stripe_release().getUiCustomization$stripe_release().getUiCustomization());
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* synthetic */ StripePaymentController(android.content.Context r16, java.lang.String r17, com.stripe.android.StripeRepository r18, boolean r19, com.stripe.android.stripe3ds2.transaction.MessageVersionRegistry r20, com.stripe.android.PaymentAuthConfig r21, com.stripe.android.stripe3ds2.service.StripeThreeDs2Service r22, com.stripe.android.AnalyticsRequestExecutor r23, com.stripe.android.AnalyticsDataFactory r24, com.stripe.android.StripePaymentController.ChallengeFlowStarter r25, com.stripe.android.StripePaymentController.ChallengeProgressDialogActivityStarter r26, kotlinx.coroutines.CoroutineScope r27, int r28, kotlin.jvm.internal.DefaultConstructorMarker r29) {
        /*
            r15 = this;
            r0 = r28
            r1 = r0 & 8
            if (r1 == 0) goto L_0x0009
            r1 = 0
            r6 = 0
            goto L_0x000b
        L_0x0009:
            r6 = r19
        L_0x000b:
            r1 = r0 & 16
            if (r1 == 0) goto L_0x0016
            com.stripe.android.stripe3ds2.transaction.MessageVersionRegistry r1 = new com.stripe.android.stripe3ds2.transaction.MessageVersionRegistry
            r1.<init>()
            r7 = r1
            goto L_0x0018
        L_0x0016:
            r7 = r20
        L_0x0018:
            r1 = r0 & 32
            if (r1 == 0) goto L_0x0024
            com.stripe.android.PaymentAuthConfig$Companion r1 = com.stripe.android.PaymentAuthConfig.Companion
            com.stripe.android.PaymentAuthConfig r1 = r1.get()
            r8 = r1
            goto L_0x0026
        L_0x0024:
            r8 = r21
        L_0x0026:
            r1 = r0 & 64
            if (r1 == 0) goto L_0x003c
            com.stripe.android.stripe3ds2.service.StripeThreeDs2ServiceImpl r1 = new com.stripe.android.stripe3ds2.service.StripeThreeDs2ServiceImpl
            com.stripe.android.StripeSSLSocketFactory r2 = new com.stripe.android.StripeSSLSocketFactory
            r2.<init>()
            javax.net.ssl.SSLSocketFactory r2 = (javax.net.ssl.SSLSocketFactory) r2
            r3 = r16
            r1.<init>(r3, r2, r6)
            com.stripe.android.stripe3ds2.service.StripeThreeDs2Service r1 = (com.stripe.android.stripe3ds2.service.StripeThreeDs2Service) r1
            r9 = r1
            goto L_0x0040
        L_0x003c:
            r3 = r16
            r9 = r22
        L_0x0040:
            r1 = r0 & 128(0x80, float:1.794E-43)
            if (r1 == 0) goto L_0x0053
            com.stripe.android.AnalyticsRequestExecutor$Default r1 = new com.stripe.android.AnalyticsRequestExecutor$Default
            com.stripe.android.Logger$Companion r2 = com.stripe.android.Logger.Companion
            com.stripe.android.Logger r2 = r2.getInstance$stripe_release(r6)
            r1.<init>(r2)
            com.stripe.android.AnalyticsRequestExecutor r1 = (com.stripe.android.AnalyticsRequestExecutor) r1
            r10 = r1
            goto L_0x0055
        L_0x0053:
            r10 = r23
        L_0x0055:
            r1 = r0 & 256(0x100, float:3.59E-43)
            if (r1 == 0) goto L_0x006b
            com.stripe.android.AnalyticsDataFactory r1 = new com.stripe.android.AnalyticsDataFactory
            android.content.Context r2 = r16.getApplicationContext()
            java.lang.String r4 = "context.applicationContext"
            kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r2, r4)
            r4 = r17
            r1.<init>(r2, r4)
            r11 = r1
            goto L_0x006f
        L_0x006b:
            r4 = r17
            r11 = r24
        L_0x006f:
            r1 = r0 & 512(0x200, float:7.175E-43)
            if (r1 == 0) goto L_0x007c
            com.stripe.android.StripePaymentController$ChallengeFlowStarter$Default r1 = new com.stripe.android.StripePaymentController$ChallengeFlowStarter$Default
            r1.<init>()
            com.stripe.android.StripePaymentController$ChallengeFlowStarter r1 = (com.stripe.android.StripePaymentController.ChallengeFlowStarter) r1
            r12 = r1
            goto L_0x007e
        L_0x007c:
            r12 = r25
        L_0x007e:
            r1 = r0 & 1024(0x400, float:1.435E-42)
            if (r1 == 0) goto L_0x008b
            com.stripe.android.StripePaymentController$ChallengeProgressDialogActivityStarter$Default r1 = new com.stripe.android.StripePaymentController$ChallengeProgressDialogActivityStarter$Default
            r1.<init>()
            com.stripe.android.StripePaymentController$ChallengeProgressDialogActivityStarter r1 = (com.stripe.android.StripePaymentController.ChallengeProgressDialogActivityStarter) r1
            r13 = r1
            goto L_0x008d
        L_0x008b:
            r13 = r26
        L_0x008d:
            r0 = r0 & 2048(0x800, float:2.87E-42)
            if (r0 == 0) goto L_0x009d
            kotlinx.coroutines.CoroutineDispatcher r0 = kotlinx.coroutines.Dispatchers.getIO()
            kotlin.coroutines.CoroutineContext r0 = (kotlin.coroutines.CoroutineContext) r0
            kotlinx.coroutines.CoroutineScope r0 = kotlinx.coroutines.CoroutineScopeKt.CoroutineScope(r0)
            r14 = r0
            goto L_0x009f
        L_0x009d:
            r14 = r27
        L_0x009f:
            r2 = r15
            r3 = r16
            r4 = r17
            r5 = r18
            r2.<init>(r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.stripe.android.StripePaymentController.<init>(android.content.Context, java.lang.String, com.stripe.android.StripeRepository, boolean, com.stripe.android.stripe3ds2.transaction.MessageVersionRegistry, com.stripe.android.PaymentAuthConfig, com.stripe.android.stripe3ds2.service.StripeThreeDs2Service, com.stripe.android.AnalyticsRequestExecutor, com.stripe.android.AnalyticsDataFactory, com.stripe.android.StripePaymentController$ChallengeFlowStarter, com.stripe.android.StripePaymentController$ChallengeProgressDialogActivityStarter, kotlinx.coroutines.CoroutineScope, int, kotlin.jvm.internal.DefaultConstructorMarker):void");
    }

    public void startConfirmAndAuth(AuthActivityStarter.Host host, ConfirmStripeIntentParams confirmStripeIntentParams, ApiRequest.Options options) {
        Intrinsics.checkParameterIsNotNull(host, "host");
        Intrinsics.checkParameterIsNotNull(confirmStripeIntentParams, "confirmStripeIntentParams");
        Intrinsics.checkParameterIsNotNull(options, "requestOptions");
        startConfirm(confirmStripeIntentParams, options, new ConfirmStripeIntentCallback(host, options, this, Companion.getRequestCode$stripe_release(confirmStripeIntentParams)));
    }

    public void startConfirm(ConfirmStripeIntentParams confirmStripeIntentParams, ApiRequest.Options options, ApiResultCallback<StripeIntent> apiResultCallback) {
        Intrinsics.checkParameterIsNotNull(confirmStripeIntentParams, "confirmStripeIntentParams");
        Intrinsics.checkParameterIsNotNull(options, "requestOptions");
        Intrinsics.checkParameterIsNotNull(apiResultCallback, "callback");
        new ConfirmStripeIntentTask(this.stripeRepository, confirmStripeIntentParams, options, this.workScope, apiResultCallback).execute$stripe_release();
    }

    public void startAuth(AuthActivityStarter.Host host, String str, ApiRequest.Options options) {
        Intrinsics.checkParameterIsNotNull(host, "host");
        Intrinsics.checkParameterIsNotNull(str, "clientSecret");
        Intrinsics.checkParameterIsNotNull(options, "requestOptions");
        StripeRepository.DefaultImpls.retrieveIntent$default(this.stripeRepository, str, options, (List) null, new StripePaymentController$startAuth$1(this, host, options), 4, (Object) null);
    }

    public void startAuthenticateSource(AuthActivityStarter.Host host, Source source, ApiRequest.Options options) {
        Intrinsics.checkParameterIsNotNull(host, "host");
        Intrinsics.checkParameterIsNotNull(source, "source");
        Intrinsics.checkParameterIsNotNull(options, "requestOptions");
        this.analyticsRequestExecutor.executeAsync(AnalyticsRequest.Factory.create$stripe_release$default(this.analyticsRequestFactory, this.analyticsDataFactory.createAuthSourceParams$stripe_release(AnalyticsEvent.AuthSourceStart, source.getId()), options, (AppInfo) null, 4, (Object) null));
        StripeRepository stripeRepository2 = this.stripeRepository;
        String id = source.getId();
        String str = "";
        if (id == null) {
            id = str;
        }
        String clientSecret = source.getClientSecret();
        if (clientSecret != null) {
            str = clientSecret;
        }
        stripeRepository2.retrieveSource(id, str, options, new StripePaymentController$startAuthenticateSource$1(this, host, options));
    }

    /* access modifiers changed from: private */
    public final void onSourceRetrieved(AuthActivityStarter.Host host, Source source, ApiRequest.Options options) {
        String str;
        if (Intrinsics.areEqual((Object) source.getFlow(), (Object) "redirect")) {
            this.analyticsRequestExecutor.executeAsync(AnalyticsRequest.Factory.create$stripe_release$default(this.analyticsRequestFactory, this.analyticsDataFactory.createAuthSourceParams$stripe_release(AnalyticsEvent.AuthSourceRedirect, source.getId()), options, (AppInfo) null, 4, (Object) null));
            PaymentAuthWebViewStarter paymentAuthWebViewStarter = new PaymentAuthWebViewStarter(host, SOURCE_REQUEST_CODE);
            String clientSecret = source.getClientSecret();
            String str2 = clientSecret != null ? clientSecret : "";
            SourceRedirect redirect = source.getRedirect();
            String url = redirect != null ? redirect.getUrl() : null;
            if (url != null) {
                str = url;
            } else {
                str = "";
            }
            SourceRedirect redirect2 = source.getRedirect();
            paymentAuthWebViewStarter.start(new PaymentAuthWebViewStarter.Args(str2, str, redirect2 != null ? redirect2.getReturnUrl() : null, this.enableLogging, (StripeToolbarCustomization) null, options.getStripeAccount$stripe_release(), 16, (DefaultConstructorMarker) null));
            return;
        }
        bypassAuth(host, source, options.getStripeAccount$stripe_release());
    }

    public void handlePaymentResult(Intent intent, ApiResultCallback<PaymentIntentResult> apiResultCallback) {
        Intrinsics.checkParameterIsNotNull(intent, "data");
        Intrinsics.checkParameterIsNotNull(apiResultCallback, "callback");
        PaymentController.Result fromIntent$stripe_release = PaymentController.Result.Companion.fromIntent$stripe_release(intent);
        if (fromIntent$stripe_release == null) {
            fromIntent$stripe_release = new PaymentController.Result((String) null, 0, (StripeException) null, false, (String) null, (Source) null, (String) null, 127, (DefaultConstructorMarker) null);
        }
        StripeException exception$stripe_release = fromIntent$stripe_release.getException$stripe_release();
        if (exception$stripe_release instanceof Exception) {
            apiResultCallback.onError(exception$stripe_release);
            return;
        }
        boolean shouldCancelSource$stripe_release = fromIntent$stripe_release.getShouldCancelSource$stripe_release();
        String sourceId$stripe_release = fromIntent$stripe_release.getSourceId$stripe_release();
        if (sourceId$stripe_release == null) {
            sourceId$stripe_release = "";
        }
        int flowOutcome$stripe_release = fromIntent$stripe_release.getFlowOutcome$stripe_release();
        ApiRequest.Options options = new ApiRequest.Options(this.publishableKey, fromIntent$stripe_release.getStripeAccountId$stripe_release(), (String) null, 4, (DefaultConstructorMarker) null);
        this.stripeRepository.retrieveIntent(Companion.getClientSecret$stripe_release(intent), options, EXPAND_PAYMENT_METHOD, createPaymentIntentCallback(options, flowOutcome$stripe_release, sourceId$stripe_release, shouldCancelSource$stripe_release, apiResultCallback));
    }

    public void handleSetupResult(Intent intent, ApiResultCallback<SetupIntentResult> apiResultCallback) {
        Intrinsics.checkParameterIsNotNull(intent, "data");
        Intrinsics.checkParameterIsNotNull(apiResultCallback, "callback");
        PaymentController.Result fromIntent$stripe_release = PaymentController.Result.Companion.fromIntent$stripe_release(intent);
        if (fromIntent$stripe_release == null) {
            fromIntent$stripe_release = new PaymentController.Result((String) null, 0, (StripeException) null, false, (String) null, (Source) null, (String) null, 127, (DefaultConstructorMarker) null);
        }
        StripeException exception$stripe_release = fromIntent$stripe_release.getException$stripe_release();
        if (exception$stripe_release instanceof Exception) {
            apiResultCallback.onError(exception$stripe_release);
            return;
        }
        boolean shouldCancelSource$stripe_release = fromIntent$stripe_release.getShouldCancelSource$stripe_release();
        String sourceId$stripe_release = fromIntent$stripe_release.getSourceId$stripe_release();
        if (sourceId$stripe_release == null) {
            sourceId$stripe_release = "";
        }
        int flowOutcome$stripe_release = fromIntent$stripe_release.getFlowOutcome$stripe_release();
        ApiRequest.Options options = new ApiRequest.Options(this.publishableKey, fromIntent$stripe_release.getStripeAccountId$stripe_release(), (String) null, 4, (DefaultConstructorMarker) null);
        this.stripeRepository.retrieveIntent(Companion.getClientSecret$stripe_release(intent), options, EXPAND_PAYMENT_METHOD, createSetupIntentCallback(options, flowOutcome$stripe_release, sourceId$stripe_release, shouldCancelSource$stripe_release, apiResultCallback));
    }

    public void handleSourceResult(Intent intent, ApiResultCallback<Source> apiResultCallback) {
        Intrinsics.checkParameterIsNotNull(intent, "data");
        Intrinsics.checkParameterIsNotNull(apiResultCallback, "callback");
        PaymentController.Result fromIntent$stripe_release = PaymentController.Result.Companion.fromIntent$stripe_release(intent);
        String str = null;
        String sourceId$stripe_release = fromIntent$stripe_release != null ? fromIntent$stripe_release.getSourceId$stripe_release() : null;
        String str2 = "";
        if (sourceId$stripe_release == null) {
            sourceId$stripe_release = str2;
        }
        String clientSecret$stripe_release = fromIntent$stripe_release != null ? fromIntent$stripe_release.getClientSecret$stripe_release() : null;
        if (clientSecret$stripe_release != null) {
            str2 = clientSecret$stripe_release;
        }
        String str3 = this.publishableKey;
        if (fromIntent$stripe_release != null) {
            str = fromIntent$stripe_release.getStripeAccountId$stripe_release();
        }
        ApiRequest.Options options = new ApiRequest.Options(str3, str, (String) null, 4, (DefaultConstructorMarker) null);
        this.analyticsRequestExecutor.executeAsync(AnalyticsRequest.Factory.create$stripe_release$default(this.analyticsRequestFactory, this.analyticsDataFactory.createAuthSourceParams$stripe_release(AnalyticsEvent.AuthSourceResult, sourceId$stripe_release), options, (AppInfo) null, 4, (Object) null));
        this.stripeRepository.retrieveSource(sourceId$stripe_release, str2, options, apiResultCallback);
    }

    public void authenticateAlipay(StripeIntent stripeIntent, String str, AlipayAuthenticator alipayAuthenticator, ApiResultCallback<PaymentIntentResult> apiResultCallback) {
        Intrinsics.checkParameterIsNotNull(stripeIntent, "intent");
        Intrinsics.checkParameterIsNotNull(alipayAuthenticator, "authenticator");
        Intrinsics.checkParameterIsNotNull(apiResultCallback, "callback");
        new AlipayAuthenticationTask(stripeIntent, alipayAuthenticator, new StripePaymentController$authenticateAlipay$1(this, str, stripeIntent, apiResultCallback)).execute$stripe_release();
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\b\u0000\u0018\u0000 \r2\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\rB#\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\f\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00020\b¢\u0006\u0002\u0010\tJ\u0015\u0010\n\u001a\u0004\u0018\u00010\u0002H@ø\u0001\u0000¢\u0006\u0004\b\u000b\u0010\fR\u000e\u0010\u0005\u001a\u00020\u0006X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0004¢\u0006\u0002\n\u0000\u0002\u0004\n\u0002\b\u0019¨\u0006\u000e"}, d2 = {"Lcom/stripe/android/StripePaymentController$AlipayAuthenticationTask;", "Lcom/stripe/android/ApiOperation;", "", "intent", "Lcom/stripe/android/model/StripeIntent;", "authenticator", "Lcom/stripe/android/AlipayAuthenticator;", "callback", "Lcom/stripe/android/ApiResultCallback;", "(Lcom/stripe/android/model/StripeIntent;Lcom/stripe/android/AlipayAuthenticator;Lcom/stripe/android/ApiResultCallback;)V", "getResult", "getResult$stripe_release", "(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "Companion", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: StripePaymentController.kt */
    public static final class AlipayAuthenticationTask extends ApiOperation<Integer> {
        public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
        private static final String RESULT_CODE_CANCELLED = "6001";
        private static final String RESULT_CODE_FAILED = "4000";
        private static final String RESULT_CODE_SUCCESS = "9000";
        private static final String RESULT_FIELD = "resultStatus";
        private final AlipayAuthenticator authenticator;
        private final StripeIntent intent;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public AlipayAuthenticationTask(StripeIntent stripeIntent, AlipayAuthenticator alipayAuthenticator, ApiResultCallback<Integer> apiResultCallback) {
            super((CoroutineScope) null, apiResultCallback, 1, (DefaultConstructorMarker) null);
            Intrinsics.checkParameterIsNotNull(stripeIntent, "intent");
            Intrinsics.checkParameterIsNotNull(alipayAuthenticator, "authenticator");
            Intrinsics.checkParameterIsNotNull(apiResultCallback, "callback");
            this.intent = stripeIntent;
            this.authenticator = alipayAuthenticator;
        }

        public Object getResult$stripe_release(Continuation<? super Integer> continuation) {
            StripeIntent.NextActionData nextActionData = this.intent.getNextActionData();
            if (nextActionData instanceof StripeIntent.NextActionData.RedirectToUrl) {
                StripeIntent.NextActionData.RedirectToUrl redirectToUrl = (StripeIntent.NextActionData.RedirectToUrl) nextActionData;
                if (redirectToUrl.getMobileData() instanceof StripeIntent.NextActionData.RedirectToUrl.MobileData.Alipay) {
                    String str = this.authenticator.onAuthenticationRequest(((StripeIntent.NextActionData.RedirectToUrl.MobileData.Alipay) redirectToUrl.getMobileData()).getData()).get(RESULT_FIELD);
                    if (str != null) {
                        int hashCode = str.hashCode();
                        if (hashCode != 1596796) {
                            if (hashCode != 1656379) {
                                if (hashCode == 1745751 && str.equals(RESULT_CODE_SUCCESS)) {
                                    return Boxing.boxInt(1);
                                }
                            } else if (str.equals(RESULT_CODE_CANCELLED)) {
                                return Boxing.boxInt(3);
                            }
                        } else if (str.equals(RESULT_CODE_FAILED)) {
                            return Boxing.boxInt(2);
                        }
                    }
                    return Boxing.boxInt(0);
                }
            }
            throw new RuntimeException("Unable to authenticate Payment Intent with Alipay SDK");
        }

        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0004\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\b"}, d2 = {"Lcom/stripe/android/StripePaymentController$AlipayAuthenticationTask$Companion;", "", "()V", "RESULT_CODE_CANCELLED", "", "RESULT_CODE_FAILED", "RESULT_CODE_SUCCESS", "RESULT_FIELD", "stripe_release"}, k = 1, mv = {1, 1, 16})
        /* compiled from: StripePaymentController.kt */
        public static final class Companion {
            private Companion() {
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }
        }
    }

    static /* synthetic */ ApiResultCallback createPaymentIntentCallback$default(StripePaymentController stripePaymentController, ApiRequest.Options options, int i, String str, boolean z, ApiResultCallback apiResultCallback, int i2, Object obj) {
        return stripePaymentController.createPaymentIntentCallback(options, i, str, (i2 & 8) != 0 ? false : z, apiResultCallback);
    }

    /* access modifiers changed from: private */
    public final ApiResultCallback<StripeIntent> createPaymentIntentCallback(ApiRequest.Options options, int i, String str, boolean z, ApiResultCallback<PaymentIntentResult> apiResultCallback) {
        return new StripePaymentController$createPaymentIntentCallback$1(this, z, str, options, i, apiResultCallback);
    }

    static /* synthetic */ ApiResultCallback createSetupIntentCallback$default(StripePaymentController stripePaymentController, ApiRequest.Options options, int i, String str, boolean z, ApiResultCallback apiResultCallback, int i2, Object obj) {
        return stripePaymentController.createSetupIntentCallback(options, i, str, (i2 & 8) != 0 ? false : z, apiResultCallback);
    }

    /* access modifiers changed from: private */
    public final ApiResultCallback<StripeIntent> createSetupIntentCallback(ApiRequest.Options options, int i, String str, boolean z, ApiResultCallback<SetupIntentResult> apiResultCallback) {
        return new StripePaymentController$createSetupIntentCallback$1(this, z, str, options, i, apiResultCallback);
    }

    public void handleNextAction(AuthActivityStarter.Host host, StripeIntent stripeIntent, ApiRequest.Options options) {
        AuthActivityStarter.Host host2 = host;
        StripeIntent stripeIntent2 = stripeIntent;
        ApiRequest.Options options2 = options;
        Intrinsics.checkParameterIsNotNull(host, "host");
        Intrinsics.checkParameterIsNotNull(stripeIntent, "stripeIntent");
        Intrinsics.checkParameterIsNotNull(options2, "requestOptions");
        if (stripeIntent.requiresAction()) {
            StripeIntent.NextActionData nextActionData = stripeIntent.getNextActionData();
            String str = "";
            if (nextActionData instanceof StripeIntent.NextActionData.SdkData.Use3DS2) {
                AnalyticsRequestExecutor analyticsRequestExecutor2 = this.analyticsRequestExecutor;
                AnalyticsRequest.Factory factory = this.analyticsRequestFactory;
                AnalyticsDataFactory analyticsDataFactory2 = this.analyticsDataFactory;
                AnalyticsEvent analyticsEvent = AnalyticsEvent.Auth3ds2Fingerprint;
                String id = stripeIntent.getId();
                if (id != null) {
                    str = id;
                }
                analyticsRequestExecutor2.executeAsync(AnalyticsRequest.Factory.create$stripe_release$default(factory, analyticsDataFactory2.createAuthParams$stripe_release(analyticsEvent, str), options, (AppInfo) null, 4, (Object) null));
                try {
                    begin3ds2Auth(host, stripeIntent, new Stripe3ds2Fingerprint((StripeIntent.NextActionData.SdkData.Use3DS2) nextActionData), options2);
                } catch (CertificateException e) {
                    Companion companion = Companion;
                    companion.handleError(host, companion.getRequestCode$stripe_release(stripeIntent), e);
                }
            } else if (nextActionData instanceof StripeIntent.NextActionData.SdkData.Use3DS1) {
                AnalyticsRequestExecutor analyticsRequestExecutor3 = this.analyticsRequestExecutor;
                AnalyticsRequest.Factory factory2 = this.analyticsRequestFactory;
                AnalyticsDataFactory analyticsDataFactory3 = this.analyticsDataFactory;
                AnalyticsEvent analyticsEvent2 = AnalyticsEvent.Auth3ds1Sdk;
                String id2 = stripeIntent.getId();
                if (id2 == null) {
                    id2 = str;
                }
                analyticsRequestExecutor3.executeAsync(AnalyticsRequest.Factory.create$stripe_release$default(factory2, analyticsDataFactory3.createAuthParams$stripe_release(analyticsEvent2, id2), options, (AppInfo) null, 4, (Object) null));
                Companion companion2 = Companion;
                int requestCode$stripe_release = companion2.getRequestCode$stripe_release(stripeIntent);
                String clientSecret = stripeIntent.getClientSecret();
                if (clientSecret != null) {
                    str = clientSecret;
                }
                Companion.beginWebAuth$default(companion2, host, requestCode$stripe_release, str, ((StripeIntent.NextActionData.SdkData.Use3DS1) nextActionData).getUrl(), options.getStripeAccount$stripe_release(), (String) null, this.enableLogging, 32, (Object) null);
            } else if (nextActionData instanceof StripeIntent.NextActionData.RedirectToUrl) {
                AnalyticsRequestExecutor analyticsRequestExecutor4 = this.analyticsRequestExecutor;
                AnalyticsRequest.Factory factory3 = this.analyticsRequestFactory;
                AnalyticsDataFactory analyticsDataFactory4 = this.analyticsDataFactory;
                AnalyticsEvent analyticsEvent3 = AnalyticsEvent.AuthRedirect;
                String id3 = stripeIntent.getId();
                if (id3 == null) {
                    id3 = str;
                }
                analyticsRequestExecutor4.executeAsync(AnalyticsRequest.Factory.create$stripe_release$default(factory3, analyticsDataFactory4.createAuthParams$stripe_release(analyticsEvent3, id3), options, (AppInfo) null, 4, (Object) null));
                Companion companion3 = Companion;
                int requestCode$stripe_release2 = companion3.getRequestCode$stripe_release(stripeIntent);
                String clientSecret2 = stripeIntent.getClientSecret();
                if (clientSecret2 != null) {
                    str = clientSecret2;
                }
                StripeIntent.NextActionData.RedirectToUrl redirectToUrl = (StripeIntent.NextActionData.RedirectToUrl) nextActionData;
                String uri = redirectToUrl.getUrl().toString();
                Intrinsics.checkExpressionValueIsNotNull(uri, "nextActionData.url.toString()");
                companion3.beginWebAuth(host, requestCode$stripe_release2, str, uri, options.getStripeAccount$stripe_release(), redirectToUrl.getReturnUrl(), this.enableLogging);
            } else {
                bypassAuth(host, stripeIntent, options.getStripeAccount$stripe_release());
            }
        } else {
            bypassAuth(host, stripeIntent, options.getStripeAccount$stripe_release());
        }
    }

    private final void bypassAuth(AuthActivityStarter.Host host, StripeIntent stripeIntent, String str) {
        PaymentRelayStarter.Companion.create$stripe_release(host, Companion.getRequestCode$stripe_release(stripeIntent)).start(PaymentRelayStarter.Args.Companion.create$stripe_release(stripeIntent, str));
    }

    private final void bypassAuth(AuthActivityStarter.Host host, Source source, String str) {
        PaymentRelayStarter.Companion.create$stripe_release(host, SOURCE_REQUEST_CODE).start(PaymentRelayStarter.Args.Companion.create$stripe_release(source, str));
    }

    private final void begin3ds2Auth(AuthActivityStarter.Host host, StripeIntent stripeIntent, Stripe3ds2Fingerprint stripe3ds2Fingerprint, ApiRequest.Options options) {
        Activity activity$stripe_release = host.getActivity$stripe_release();
        if (activity$stripe_release != null) {
            Context context = activity$stripe_release;
            Transaction createTransaction = this.threeDs2Service.createTransaction(stripe3ds2Fingerprint.getDirectoryServer().getId(), this.messageVersionRegistry.getCurrent(), stripeIntent.isLiveMode(), stripe3ds2Fingerprint.getDirectoryServer().getNetworkName(), stripe3ds2Fingerprint.getDirectoryServerEncryption().getRootCerts(), stripe3ds2Fingerprint.getDirectoryServerEncryption().getDirectoryServerPublicKey(), stripe3ds2Fingerprint.getDirectoryServerEncryption().getKeyId(), new Intent(context, Stripe3ds2CompletionActivity.class).putExtra(Stripe3ds2CompletionActivity.EXTRA_CLIENT_SECRET, stripeIntent.getClientSecret()).putExtra(Stripe3ds2CompletionActivity.EXTRA_STRIPE_ACCOUNT, options.getStripeAccount$stripe_release()).addFlags(33554432), Companion.getRequestCode$stripe_release(stripeIntent));
            this.challengeProgressDialogActivityStarter.start(context, stripe3ds2Fingerprint.getDirectoryServer().getNetworkName(), false, this.config.getStripe3ds2Config$stripe_release().getUiCustomization$stripe_release().getUiCustomization());
            AuthenticationRequestParameters authenticationRequestParameters = createTransaction.getAuthenticationRequestParameters();
            int timeout$stripe_release = this.config.getStripe3ds2Config$stripe_release().getTimeout$stripe_release();
            Stripe3ds2AuthParams stripe3ds2AuthParams = new Stripe3ds2AuthParams(stripe3ds2Fingerprint.getSource(), authenticationRequestParameters.getSdkAppId(), authenticationRequestParameters.getSdkReferenceNumber(), authenticationRequestParameters.getSdkTransactionId(), authenticationRequestParameters.getDeviceData(), authenticationRequestParameters.getSdkEphemeralPublicKey(), authenticationRequestParameters.getMessageVersion(), timeout$stripe_release, (String) null);
            StripeRepository stripeRepository2 = this.stripeRepository;
            String id = stripeIntent.getId();
            if (id == null) {
                id = "";
            }
            ApiRequest.Options options2 = options;
            stripeRepository2.start3ds2Auth(stripe3ds2AuthParams, id, options2, new Stripe3ds2AuthCallback(host, this.stripeRepository, createTransaction, timeout$stripe_release, stripeIntent, stripe3ds2Fingerprint.getSource(), options, this.analyticsRequestExecutor, this.analyticsDataFactory, this.challengeFlowStarter, this.enableLogging, (PaymentRelayStarter) null, 2048, (DefaultConstructorMarker) null));
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B3\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\b\u0012\u0006\u0010\t\u001a\u00020\n\u0012\f\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\u00020\f¢\u0006\u0002\u0010\rJ\u0015\u0010\u000e\u001a\u0004\u0018\u00010\u0002H@ø\u0001\u0000¢\u0006\u0004\b\u000f\u0010\u0010R\u000e\u0010\u0005\u001a\u00020\u0006X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\bX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0004¢\u0006\u0002\n\u0000\u0002\u0004\n\u0002\b\u0019¨\u0006\u0011"}, d2 = {"Lcom/stripe/android/StripePaymentController$ConfirmStripeIntentTask;", "Lcom/stripe/android/ApiOperation;", "Lcom/stripe/android/model/StripeIntent;", "stripeRepository", "Lcom/stripe/android/StripeRepository;", "params", "Lcom/stripe/android/model/ConfirmStripeIntentParams;", "requestOptions", "Lcom/stripe/android/ApiRequest$Options;", "workScope", "Lkotlinx/coroutines/CoroutineScope;", "callback", "Lcom/stripe/android/ApiResultCallback;", "(Lcom/stripe/android/StripeRepository;Lcom/stripe/android/model/ConfirmStripeIntentParams;Lcom/stripe/android/ApiRequest$Options;Lkotlinx/coroutines/CoroutineScope;Lcom/stripe/android/ApiResultCallback;)V", "getResult", "getResult$stripe_release", "(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: StripePaymentController.kt */
    private static final class ConfirmStripeIntentTask extends ApiOperation<StripeIntent> {
        private final ConfirmStripeIntentParams params;
        private final ApiRequest.Options requestOptions;
        private final StripeRepository stripeRepository;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public ConfirmStripeIntentTask(StripeRepository stripeRepository2, ConfirmStripeIntentParams confirmStripeIntentParams, ApiRequest.Options options, CoroutineScope coroutineScope, ApiResultCallback<StripeIntent> apiResultCallback) {
            super(coroutineScope, apiResultCallback);
            Intrinsics.checkParameterIsNotNull(stripeRepository2, "stripeRepository");
            Intrinsics.checkParameterIsNotNull(confirmStripeIntentParams, NativeProtocol.WEB_DIALOG_PARAMS);
            Intrinsics.checkParameterIsNotNull(options, "requestOptions");
            Intrinsics.checkParameterIsNotNull(coroutineScope, "workScope");
            Intrinsics.checkParameterIsNotNull(apiResultCallback, "callback");
            this.stripeRepository = stripeRepository2;
            this.requestOptions = options;
            this.params = confirmStripeIntentParams.withShouldUseStripeSdk(true);
        }

        public Object getResult$stripe_release(Continuation<? super StripeIntent> continuation) throws StripeException {
            ConfirmStripeIntentParams confirmStripeIntentParams = this.params;
            if (confirmStripeIntentParams instanceof ConfirmPaymentIntentParams) {
                return this.stripeRepository.confirmPaymentIntent((ConfirmPaymentIntentParams) confirmStripeIntentParams, this.requestOptions, StripePaymentController.EXPAND_PAYMENT_METHOD);
            }
            if (confirmStripeIntentParams instanceof ConfirmSetupIntentParams) {
                return this.stripeRepository.confirmSetupIntent((ConfirmSetupIntentParams) confirmStripeIntentParams, this.requestOptions, StripePaymentController.EXPAND_PAYMENT_METHOD);
            }
            return null;
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B%\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\b\u0012\u0006\u0010\t\u001a\u00020\n¢\u0006\u0002\u0010\u000bJ\u0014\u0010\f\u001a\u00020\r2\n\u0010\u000e\u001a\u00060\u000fj\u0002`\u0010H\u0016J\u0010\u0010\u0011\u001a\u00020\r2\u0006\u0010\u0012\u001a\u00020\u0002H\u0016R\u000e\u0010\u0003\u001a\u00020\u0004X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\bX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0004¢\u0006\u0002\n\u0000\u0002\u0004\n\u0002\b\u0019¨\u0006\u0013"}, d2 = {"Lcom/stripe/android/StripePaymentController$ConfirmStripeIntentCallback;", "Lcom/stripe/android/ApiResultCallback;", "Lcom/stripe/android/model/StripeIntent;", "host", "Lcom/stripe/android/view/AuthActivityStarter$Host;", "requestOptions", "Lcom/stripe/android/ApiRequest$Options;", "paymentController", "Lcom/stripe/android/PaymentController;", "requestCode", "", "(Lcom/stripe/android/view/AuthActivityStarter$Host;Lcom/stripe/android/ApiRequest$Options;Lcom/stripe/android/PaymentController;I)V", "onError", "", "e", "Ljava/lang/Exception;", "Lkotlin/Exception;", "onSuccess", "result", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: StripePaymentController.kt */
    private static final class ConfirmStripeIntentCallback implements ApiResultCallback<StripeIntent> {
        private final AuthActivityStarter.Host host;
        private final PaymentController paymentController;
        private final int requestCode;
        private final ApiRequest.Options requestOptions;

        public ConfirmStripeIntentCallback(AuthActivityStarter.Host host2, ApiRequest.Options options, PaymentController paymentController2, int i) {
            Intrinsics.checkParameterIsNotNull(host2, "host");
            Intrinsics.checkParameterIsNotNull(options, "requestOptions");
            Intrinsics.checkParameterIsNotNull(paymentController2, "paymentController");
            this.host = host2;
            this.requestOptions = options;
            this.paymentController = paymentController2;
            this.requestCode = i;
        }

        public void onSuccess(StripeIntent stripeIntent) {
            Intrinsics.checkParameterIsNotNull(stripeIntent, "result");
            this.paymentController.handleNextAction(this.host, stripeIntent, this.requestOptions);
        }

        public void onError(Exception exc) {
            Intrinsics.checkParameterIsNotNull(exc, "e");
            StripePaymentController.Companion.handleError(this.host, this.requestCode, exc);
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000x\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0000\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001Bk\b\u0001\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\b\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u0006\u0010\u000b\u001a\u00020\f\u0012\u0006\u0010\r\u001a\u00020\u000e\u0012\u0006\u0010\u000f\u001a\u00020\u0010\u0012\u0006\u0010\u0011\u001a\u00020\u0012\u0012\u0006\u0010\u0013\u001a\u00020\u0014\u0012\u0006\u0010\u0015\u001a\u00020\u0016\u0012\b\b\u0002\u0010\u0017\u001a\u00020\u0018\u0012\b\b\u0002\u0010\u0019\u001a\u00020\u001a¢\u0006\u0002\u0010\u001bJ\u0014\u0010\u001e\u001a\u00020\u001f2\n\u0010 \u001a\u00060!j\u0002`\"H\u0016J\u0010\u0010#\u001a\u00020\u001f2\u0006\u0010$\u001a\u00020\u0002H\u0016J\u0010\u0010%\u001a\u00020\u001f2\u0006\u0010&\u001a\u00020'H\u0002J\b\u0010(\u001a\u00020\u001fH\u0002R\u000e\u0010\u0013\u001a\u00020\u0014X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0012X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u001c\u001a\u00020\u001dX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u0016X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0017\u001a\u00020\u0018X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0019\u001a\u00020\u001aX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\fX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\bX\u0004¢\u0006\u0002\n\u0000\u0002\u0004\n\u0002\b\u0019¨\u0006)"}, d2 = {"Lcom/stripe/android/StripePaymentController$Stripe3ds2AuthCallback;", "Lcom/stripe/android/ApiResultCallback;", "Lcom/stripe/android/model/Stripe3ds2AuthResult;", "host", "Lcom/stripe/android/view/AuthActivityStarter$Host;", "stripeRepository", "Lcom/stripe/android/StripeRepository;", "transaction", "Lcom/stripe/android/stripe3ds2/transaction/Transaction;", "maxTimeout", "", "stripeIntent", "Lcom/stripe/android/model/StripeIntent;", "sourceId", "", "requestOptions", "Lcom/stripe/android/ApiRequest$Options;", "analyticsRequestExecutor", "Lcom/stripe/android/AnalyticsRequestExecutor;", "analyticsDataFactory", "Lcom/stripe/android/AnalyticsDataFactory;", "challengeFlowStarter", "Lcom/stripe/android/StripePaymentController$ChallengeFlowStarter;", "enableLogging", "", "paymentRelayStarter", "Lcom/stripe/android/PaymentRelayStarter;", "(Lcom/stripe/android/view/AuthActivityStarter$Host;Lcom/stripe/android/StripeRepository;Lcom/stripe/android/stripe3ds2/transaction/Transaction;ILcom/stripe/android/model/StripeIntent;Ljava/lang/String;Lcom/stripe/android/ApiRequest$Options;Lcom/stripe/android/AnalyticsRequestExecutor;Lcom/stripe/android/AnalyticsDataFactory;Lcom/stripe/android/StripePaymentController$ChallengeFlowStarter;ZLcom/stripe/android/PaymentRelayStarter;)V", "analyticsRequestFactory", "Lcom/stripe/android/AnalyticsRequest$Factory;", "onError", "", "e", "Ljava/lang/Exception;", "Lkotlin/Exception;", "onSuccess", "result", "startChallengeFlow", "ares", "Lcom/stripe/android/model/Stripe3ds2AuthResult$Ares;", "startFrictionlessFlow", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: StripePaymentController.kt */
    public static final class Stripe3ds2AuthCallback implements ApiResultCallback<Stripe3ds2AuthResult> {
        /* access modifiers changed from: private */
        public final AnalyticsDataFactory analyticsDataFactory;
        /* access modifiers changed from: private */
        public final AnalyticsRequestExecutor analyticsRequestExecutor;
        /* access modifiers changed from: private */
        public final AnalyticsRequest.Factory analyticsRequestFactory;
        private final ChallengeFlowStarter challengeFlowStarter;
        private final boolean enableLogging;
        private final AuthActivityStarter.Host host;
        /* access modifiers changed from: private */
        public final int maxTimeout;
        private final PaymentRelayStarter paymentRelayStarter;
        /* access modifiers changed from: private */
        public final ApiRequest.Options requestOptions;
        /* access modifiers changed from: private */
        public final String sourceId;
        /* access modifiers changed from: private */
        public final StripeIntent stripeIntent;
        /* access modifiers changed from: private */
        public final StripeRepository stripeRepository;
        /* access modifiers changed from: private */
        public final Transaction transaction;

        public Stripe3ds2AuthCallback(AuthActivityStarter.Host host2, StripeRepository stripeRepository2, Transaction transaction2, int i, StripeIntent stripeIntent2, String str, ApiRequest.Options options, AnalyticsRequestExecutor analyticsRequestExecutor2, AnalyticsDataFactory analyticsDataFactory2, ChallengeFlowStarter challengeFlowStarter2, boolean z, PaymentRelayStarter paymentRelayStarter2) {
            Intrinsics.checkParameterIsNotNull(host2, "host");
            Intrinsics.checkParameterIsNotNull(stripeRepository2, "stripeRepository");
            Intrinsics.checkParameterIsNotNull(transaction2, "transaction");
            Intrinsics.checkParameterIsNotNull(stripeIntent2, "stripeIntent");
            Intrinsics.checkParameterIsNotNull(str, "sourceId");
            Intrinsics.checkParameterIsNotNull(options, "requestOptions");
            Intrinsics.checkParameterIsNotNull(analyticsRequestExecutor2, "analyticsRequestExecutor");
            Intrinsics.checkParameterIsNotNull(analyticsDataFactory2, "analyticsDataFactory");
            Intrinsics.checkParameterIsNotNull(challengeFlowStarter2, "challengeFlowStarter");
            Intrinsics.checkParameterIsNotNull(paymentRelayStarter2, "paymentRelayStarter");
            this.host = host2;
            this.stripeRepository = stripeRepository2;
            this.transaction = transaction2;
            this.maxTimeout = i;
            this.stripeIntent = stripeIntent2;
            this.sourceId = str;
            this.requestOptions = options;
            this.analyticsRequestExecutor = analyticsRequestExecutor2;
            this.analyticsDataFactory = analyticsDataFactory2;
            this.challengeFlowStarter = challengeFlowStarter2;
            this.enableLogging = z;
            this.paymentRelayStarter = paymentRelayStarter2;
            this.analyticsRequestFactory = new AnalyticsRequest.Factory(Logger.Companion.getInstance$stripe_release(this.enableLogging));
        }

        /* JADX WARNING: Illegal instructions before constructor call */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public /* synthetic */ Stripe3ds2AuthCallback(com.stripe.android.view.AuthActivityStarter.Host r16, com.stripe.android.StripeRepository r17, com.stripe.android.stripe3ds2.transaction.Transaction r18, int r19, com.stripe.android.model.StripeIntent r20, java.lang.String r21, com.stripe.android.ApiRequest.Options r22, com.stripe.android.AnalyticsRequestExecutor r23, com.stripe.android.AnalyticsDataFactory r24, com.stripe.android.StripePaymentController.ChallengeFlowStarter r25, boolean r26, com.stripe.android.PaymentRelayStarter r27, int r28, kotlin.jvm.internal.DefaultConstructorMarker r29) {
            /*
                r15 = this;
                r0 = r28
                r1 = r0 & 1024(0x400, float:1.435E-42)
                if (r1 == 0) goto L_0x0009
                r1 = 0
                r13 = 0
                goto L_0x000b
            L_0x0009:
                r13 = r26
            L_0x000b:
                r0 = r0 & 2048(0x800, float:2.87E-42)
                if (r0 == 0) goto L_0x0021
                com.stripe.android.PaymentRelayStarter$Companion r0 = com.stripe.android.PaymentRelayStarter.Companion
                com.stripe.android.StripePaymentController$Companion r1 = com.stripe.android.StripePaymentController.Companion
                r7 = r20
                int r1 = r1.getRequestCode$stripe_release((com.stripe.android.model.StripeIntent) r7)
                r3 = r16
                com.stripe.android.PaymentRelayStarter r0 = r0.create$stripe_release(r3, r1)
                r14 = r0
                goto L_0x0027
            L_0x0021:
                r3 = r16
                r7 = r20
                r14 = r27
            L_0x0027:
                r2 = r15
                r3 = r16
                r4 = r17
                r5 = r18
                r6 = r19
                r7 = r20
                r8 = r21
                r9 = r22
                r10 = r23
                r11 = r24
                r12 = r25
                r2.<init>(r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14)
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.stripe.android.StripePaymentController.Stripe3ds2AuthCallback.<init>(com.stripe.android.view.AuthActivityStarter$Host, com.stripe.android.StripeRepository, com.stripe.android.stripe3ds2.transaction.Transaction, int, com.stripe.android.model.StripeIntent, java.lang.String, com.stripe.android.ApiRequest$Options, com.stripe.android.AnalyticsRequestExecutor, com.stripe.android.AnalyticsDataFactory, com.stripe.android.StripePaymentController$ChallengeFlowStarter, boolean, com.stripe.android.PaymentRelayStarter, int, kotlin.jvm.internal.DefaultConstructorMarker):void");
        }

        public void onSuccess(Stripe3ds2AuthResult stripe3ds2AuthResult) {
            String str;
            String str2;
            Intrinsics.checkParameterIsNotNull(stripe3ds2AuthResult, "result");
            Stripe3ds2AuthResult.Ares ares = stripe3ds2AuthResult.getAres();
            if (ares != null) {
                if (ares.isChallenge()) {
                    startChallengeFlow(ares);
                } else {
                    startFrictionlessFlow();
                }
            } else if (stripe3ds2AuthResult.getFallbackRedirectUrl() != null) {
                AnalyticsRequestExecutor analyticsRequestExecutor2 = this.analyticsRequestExecutor;
                AnalyticsRequest.Factory factory = this.analyticsRequestFactory;
                AnalyticsDataFactory analyticsDataFactory2 = this.analyticsDataFactory;
                AnalyticsEvent analyticsEvent = AnalyticsEvent.Auth3ds2Fallback;
                String id = this.stripeIntent.getId();
                if (id == null) {
                    id = "";
                }
                analyticsRequestExecutor2.executeAsync(AnalyticsRequest.Factory.create$stripe_release$default(factory, analyticsDataFactory2.createAuthParams$stripe_release(analyticsEvent, id), this.requestOptions, (AppInfo) null, 4, (Object) null));
                Companion companion = StripePaymentController.Companion;
                AuthActivityStarter.Host host2 = this.host;
                int requestCode$stripe_release = StripePaymentController.Companion.getRequestCode$stripe_release(this.stripeIntent);
                String clientSecret = this.stripeIntent.getClientSecret();
                if (clientSecret != null) {
                    str2 = clientSecret;
                } else {
                    str2 = "";
                }
                Companion.beginWebAuth$default(companion, host2, requestCode$stripe_release, str2, stripe3ds2AuthResult.getFallbackRedirectUrl(), this.requestOptions.getStripeAccount$stripe_release(), (String) null, this.enableLogging, 32, (Object) null);
            } else {
                Stripe3ds2AuthResult.ThreeDS2Error error = stripe3ds2AuthResult.getError();
                if (error != null) {
                    str = "Code: " + error.getErrorCode() + ", " + "Detail: " + error.getErrorDetail() + ", " + "Description: " + error.getErrorDescription() + ", " + "Component: " + error.getErrorComponent();
                } else {
                    str = "Invalid 3DS2 authentication response";
                }
                onError(new RuntimeException("Error encountered during 3DS2 authentication request. " + str));
            }
        }

        public void onError(Exception exc) {
            StripeException stripeException;
            Intrinsics.checkParameterIsNotNull(exc, "e");
            PaymentRelayStarter paymentRelayStarter2 = this.paymentRelayStarter;
            PaymentRelayStarter.Args.Companion companion = PaymentRelayStarter.Args.Companion;
            if (exc instanceof StripeException) {
                stripeException = (StripeException) exc;
            } else {
                stripeException = new APIException(exc);
            }
            paymentRelayStarter2.start(companion.create$stripe_release(stripeException));
        }

        private final void startFrictionlessFlow() {
            AnalyticsRequestExecutor analyticsRequestExecutor2 = this.analyticsRequestExecutor;
            AnalyticsRequest.Factory factory = this.analyticsRequestFactory;
            AnalyticsDataFactory analyticsDataFactory2 = this.analyticsDataFactory;
            AnalyticsEvent analyticsEvent = AnalyticsEvent.Auth3ds2Frictionless;
            String id = this.stripeIntent.getId();
            if (id == null) {
                id = "";
            }
            analyticsRequestExecutor2.executeAsync(AnalyticsRequest.Factory.create$stripe_release$default(factory, analyticsDataFactory2.createAuthParams$stripe_release(analyticsEvent, id), this.requestOptions, (AppInfo) null, 4, (Object) null));
            this.paymentRelayStarter.start(PaymentRelayStarter.Args.Companion.create$stripe_release$default(PaymentRelayStarter.Args.Companion, this.stripeIntent, (String) null, 2, (Object) null));
        }

        private final void startChallengeFlow(Stripe3ds2AuthResult.Ares ares) {
            Stripe3ds2ActivityStarterHost stripe3ds2ActivityStarterHost;
            ChallengeParameters challengeParameters = new ChallengeParameters(ares.getThreeDSServerTransId$stripe_release(), ares.getAcsTransId$stripe_release(), (String) null, ares.getAcsSignedContent$stripe_release(), 4, (DefaultConstructorMarker) null);
            Fragment fragment$stripe_release = this.host.getFragment$stripe_release();
            if (fragment$stripe_release != null) {
                stripe3ds2ActivityStarterHost = new Stripe3ds2ActivityStarterHost(fragment$stripe_release);
            } else {
                Activity activity$stripe_release = this.host.getActivity$stripe_release();
                stripe3ds2ActivityStarterHost = activity$stripe_release != null ? new Stripe3ds2ActivityStarterHost(activity$stripe_release) : null;
            }
            if (stripe3ds2ActivityStarterHost != null) {
                this.challengeFlowStarter.start(new StripePaymentController$Stripe3ds2AuthCallback$startChallengeFlow$$inlined$let$lambda$1(stripe3ds2ActivityStarterHost, this, challengeParameters));
            }
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000d\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u0000\u0018\u0000 $2\u00020\u0001:\u0001$BG\b\u0000\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\f\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u0012\u0006\u0010\u0010\u001a\u00020\u0011¢\u0006\u0002\u0010\u0012J\u001e\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u00072\f\u0010\u0016\u001a\b\u0012\u0004\u0012\u00020\u00140\u0017H\u0016J&\u0010\u0018\u001a\u00020\u00142\u0006\u0010\u0019\u001a\u00020\u001a2\u0006\u0010\u0015\u001a\u00020\u00072\f\u0010\u0016\u001a\b\u0012\u0004\u0012\u00020\u00140\u0017H\u0016J\u0016\u0010\u001b\u001a\u00020\u00142\f\u0010\u001c\u001a\b\u0012\u0004\u0012\u00020\u00140\u0017H\u0002J\u001e\u0010\u001d\u001a\u00020\u00142\u0006\u0010\u001e\u001a\u00020\u001f2\f\u0010\u0016\u001a\b\u0012\u0004\u0012\u00020\u00140\u0017H\u0016J\u001e\u0010 \u001a\u00020\u00142\u0006\u0010!\u001a\u00020\"2\f\u0010\u0016\u001a\b\u0012\u0004\u0012\u00020\u00140\u0017H\u0016J\u001e\u0010#\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u00072\f\u0010\u0016\u001a\b\u0012\u0004\u0012\u00020\u00140\u0017H\u0016R\u000e\u0010\f\u001a\u00020\rX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0004¢\u0006\u0002\n\u0000\u0002\u0004\n\u0002\b\u0019¨\u0006%"}, d2 = {"Lcom/stripe/android/StripePaymentController$PaymentAuth3ds2ChallengeStatusReceiver;", "Lcom/stripe/android/stripe3ds2/transaction/StripeChallengeStatusReceiver;", "stripeRepository", "Lcom/stripe/android/StripeRepository;", "stripeIntent", "Lcom/stripe/android/model/StripeIntent;", "sourceId", "", "requestOptions", "Lcom/stripe/android/ApiRequest$Options;", "analyticsRequestExecutor", "Lcom/stripe/android/AnalyticsRequestExecutor;", "analyticsDataFactory", "Lcom/stripe/android/AnalyticsDataFactory;", "transaction", "Lcom/stripe/android/stripe3ds2/transaction/Transaction;", "analyticsRequestFactory", "Lcom/stripe/android/AnalyticsRequest$Factory;", "(Lcom/stripe/android/StripeRepository;Lcom/stripe/android/model/StripeIntent;Ljava/lang/String;Lcom/stripe/android/ApiRequest$Options;Lcom/stripe/android/AnalyticsRequestExecutor;Lcom/stripe/android/AnalyticsDataFactory;Lcom/stripe/android/stripe3ds2/transaction/Transaction;Lcom/stripe/android/AnalyticsRequest$Factory;)V", "cancelled", "", "uiTypeCode", "onReceiverCompleted", "Lkotlin/Function0;", "completed", "completionEvent", "Lcom/stripe/android/stripe3ds2/transaction/CompletionEvent;", "notifyCompletion", "completed3ds2Callback", "protocolError", "protocolErrorEvent", "Lcom/stripe/android/stripe3ds2/transaction/ProtocolErrorEvent;", "runtimeError", "runtimeErrorEvent", "Lcom/stripe/android/stripe3ds2/transaction/RuntimeErrorEvent;", "timedout", "Companion", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: StripePaymentController.kt */
    public static final class PaymentAuth3ds2ChallengeStatusReceiver extends StripeChallengeStatusReceiver {
        public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
        private final AnalyticsDataFactory analyticsDataFactory;
        private final AnalyticsRequestExecutor analyticsRequestExecutor;
        private final AnalyticsRequest.Factory analyticsRequestFactory;
        private final ApiRequest.Options requestOptions;
        private final String sourceId;
        private final StripeIntent stripeIntent;
        private final StripeRepository stripeRepository;
        private final Transaction transaction;

        public PaymentAuth3ds2ChallengeStatusReceiver(StripeRepository stripeRepository2, StripeIntent stripeIntent2, String str, ApiRequest.Options options, AnalyticsRequestExecutor analyticsRequestExecutor2, AnalyticsDataFactory analyticsDataFactory2, Transaction transaction2, AnalyticsRequest.Factory factory) {
            Intrinsics.checkParameterIsNotNull(stripeRepository2, "stripeRepository");
            Intrinsics.checkParameterIsNotNull(stripeIntent2, "stripeIntent");
            Intrinsics.checkParameterIsNotNull(str, "sourceId");
            Intrinsics.checkParameterIsNotNull(options, "requestOptions");
            Intrinsics.checkParameterIsNotNull(analyticsRequestExecutor2, "analyticsRequestExecutor");
            Intrinsics.checkParameterIsNotNull(analyticsDataFactory2, "analyticsDataFactory");
            Intrinsics.checkParameterIsNotNull(transaction2, "transaction");
            Intrinsics.checkParameterIsNotNull(factory, "analyticsRequestFactory");
            this.stripeRepository = stripeRepository2;
            this.stripeIntent = stripeIntent2;
            this.sourceId = str;
            this.requestOptions = options;
            this.analyticsRequestExecutor = analyticsRequestExecutor2;
            this.analyticsDataFactory = analyticsDataFactory2;
            this.transaction = transaction2;
            this.analyticsRequestFactory = factory;
        }

        public void completed(CompletionEvent completionEvent, String str, Function0<Unit> function0) {
            Intrinsics.checkParameterIsNotNull(completionEvent, "completionEvent");
            Intrinsics.checkParameterIsNotNull(str, "uiTypeCode");
            Intrinsics.checkParameterIsNotNull(function0, "onReceiverCompleted");
            super.completed(completionEvent, str, function0);
            AnalyticsRequestExecutor analyticsRequestExecutor2 = this.analyticsRequestExecutor;
            AnalyticsRequest.Factory factory = this.analyticsRequestFactory;
            AnalyticsDataFactory analyticsDataFactory2 = this.analyticsDataFactory;
            AnalyticsEvent analyticsEvent = AnalyticsEvent.Auth3ds2ChallengeCompleted;
            String id = this.stripeIntent.getId();
            if (id == null) {
                id = "";
            }
            analyticsRequestExecutor2.executeAsync(AnalyticsRequest.Factory.create$stripe_release$default(factory, analyticsDataFactory2.create3ds2ChallengeParams$stripe_release(analyticsEvent, id, str), this.requestOptions, (AppInfo) null, 4, (Object) null));
            notifyCompletion(function0);
        }

        public void cancelled(String str, Function0<Unit> function0) {
            Intrinsics.checkParameterIsNotNull(str, "uiTypeCode");
            Intrinsics.checkParameterIsNotNull(function0, "onReceiverCompleted");
            super.cancelled(str, function0);
            AnalyticsRequestExecutor analyticsRequestExecutor2 = this.analyticsRequestExecutor;
            AnalyticsRequest.Factory factory = this.analyticsRequestFactory;
            AnalyticsDataFactory analyticsDataFactory2 = this.analyticsDataFactory;
            AnalyticsEvent analyticsEvent = AnalyticsEvent.Auth3ds2ChallengeCanceled;
            String id = this.stripeIntent.getId();
            if (id == null) {
                id = "";
            }
            analyticsRequestExecutor2.executeAsync(AnalyticsRequest.Factory.create$stripe_release$default(factory, analyticsDataFactory2.create3ds2ChallengeParams$stripe_release(analyticsEvent, id, str), this.requestOptions, (AppInfo) null, 4, (Object) null));
            notifyCompletion(function0);
        }

        public void timedout(String str, Function0<Unit> function0) {
            Intrinsics.checkParameterIsNotNull(str, "uiTypeCode");
            Intrinsics.checkParameterIsNotNull(function0, "onReceiverCompleted");
            super.timedout(str, function0);
            AnalyticsRequestExecutor analyticsRequestExecutor2 = this.analyticsRequestExecutor;
            AnalyticsRequest.Factory factory = this.analyticsRequestFactory;
            AnalyticsDataFactory analyticsDataFactory2 = this.analyticsDataFactory;
            AnalyticsEvent analyticsEvent = AnalyticsEvent.Auth3ds2ChallengeTimedOut;
            String id = this.stripeIntent.getId();
            if (id == null) {
                id = "";
            }
            analyticsRequestExecutor2.executeAsync(AnalyticsRequest.Factory.create$stripe_release$default(factory, analyticsDataFactory2.create3ds2ChallengeParams$stripe_release(analyticsEvent, id, str), this.requestOptions, (AppInfo) null, 4, (Object) null));
            notifyCompletion(function0);
        }

        public void protocolError(ProtocolErrorEvent protocolErrorEvent, Function0<Unit> function0) {
            Intrinsics.checkParameterIsNotNull(protocolErrorEvent, "protocolErrorEvent");
            Intrinsics.checkParameterIsNotNull(function0, "onReceiverCompleted");
            super.protocolError(protocolErrorEvent, function0);
            AnalyticsRequestExecutor analyticsRequestExecutor2 = this.analyticsRequestExecutor;
            AnalyticsRequest.Factory factory = this.analyticsRequestFactory;
            AnalyticsDataFactory analyticsDataFactory2 = this.analyticsDataFactory;
            String id = this.stripeIntent.getId();
            if (id == null) {
                id = "";
            }
            analyticsRequestExecutor2.executeAsync(AnalyticsRequest.Factory.create$stripe_release$default(factory, analyticsDataFactory2.create3ds2ChallengeErrorParams$stripe_release(id, protocolErrorEvent), this.requestOptions, (AppInfo) null, 4, (Object) null));
            notifyCompletion(function0);
        }

        public void runtimeError(RuntimeErrorEvent runtimeErrorEvent, Function0<Unit> function0) {
            Intrinsics.checkParameterIsNotNull(runtimeErrorEvent, "runtimeErrorEvent");
            Intrinsics.checkParameterIsNotNull(function0, "onReceiverCompleted");
            super.runtimeError(runtimeErrorEvent, function0);
            AnalyticsRequestExecutor analyticsRequestExecutor2 = this.analyticsRequestExecutor;
            AnalyticsRequest.Factory factory = this.analyticsRequestFactory;
            AnalyticsDataFactory analyticsDataFactory2 = this.analyticsDataFactory;
            String id = this.stripeIntent.getId();
            if (id == null) {
                id = "";
            }
            analyticsRequestExecutor2.executeAsync(AnalyticsRequest.Factory.create$stripe_release$default(factory, analyticsDataFactory2.create3ds2ChallengeErrorParams$stripe_release(id, runtimeErrorEvent), this.requestOptions, (AppInfo) null, 4, (Object) null));
            notifyCompletion(function0);
        }

        private final void notifyCompletion(Function0<Unit> function0) {
            AnalyticsRequestExecutor analyticsRequestExecutor2 = this.analyticsRequestExecutor;
            AnalyticsRequest.Factory factory = this.analyticsRequestFactory;
            AnalyticsDataFactory analyticsDataFactory2 = this.analyticsDataFactory;
            AnalyticsEvent analyticsEvent = AnalyticsEvent.Auth3ds2ChallengePresented;
            String id = this.stripeIntent.getId();
            String str = "";
            if (id == null) {
                id = str;
            }
            String initialChallengeUiType = this.transaction.getInitialChallengeUiType();
            if (initialChallengeUiType != null) {
                str = initialChallengeUiType;
            }
            analyticsRequestExecutor2.executeAsync(AnalyticsRequest.Factory.create$stripe_release$default(factory, analyticsDataFactory2.create3ds2ChallengeParams$stripe_release(analyticsEvent, id, str), this.requestOptions, (AppInfo) null, 4, (Object) null));
            this.stripeRepository.complete3ds2Auth(this.sourceId, this.requestOptions, new StripePaymentController$PaymentAuth3ds2ChallengeStatusReceiver$notifyCompletion$1(function0));
        }

        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000D\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002JM\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u0014H\u0000¢\u0006\u0002\b\u0015\u0002\u0004\n\u0002\b\u0019¨\u0006\u0016"}, d2 = {"Lcom/stripe/android/StripePaymentController$PaymentAuth3ds2ChallengeStatusReceiver$Companion;", "", "()V", "create", "Lcom/stripe/android/StripePaymentController$PaymentAuth3ds2ChallengeStatusReceiver;", "stripeRepository", "Lcom/stripe/android/StripeRepository;", "stripeIntent", "Lcom/stripe/android/model/StripeIntent;", "sourceId", "", "requestOptions", "Lcom/stripe/android/ApiRequest$Options;", "analyticsRequestExecutor", "Lcom/stripe/android/AnalyticsRequestExecutor;", "analyticsDataFactory", "Lcom/stripe/android/AnalyticsDataFactory;", "transaction", "Lcom/stripe/android/stripe3ds2/transaction/Transaction;", "analyticsRequestFactory", "Lcom/stripe/android/AnalyticsRequest$Factory;", "create$stripe_release", "stripe_release"}, k = 1, mv = {1, 1, 16})
        /* compiled from: StripePaymentController.kt */
        public static final class Companion {
            private Companion() {
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }

            public final PaymentAuth3ds2ChallengeStatusReceiver create$stripe_release(StripeRepository stripeRepository, StripeIntent stripeIntent, String str, ApiRequest.Options options, AnalyticsRequestExecutor analyticsRequestExecutor, AnalyticsDataFactory analyticsDataFactory, Transaction transaction, AnalyticsRequest.Factory factory) {
                Intrinsics.checkParameterIsNotNull(stripeRepository, "stripeRepository");
                Intrinsics.checkParameterIsNotNull(stripeIntent, "stripeIntent");
                Intrinsics.checkParameterIsNotNull(str, "sourceId");
                Intrinsics.checkParameterIsNotNull(options, "requestOptions");
                Intrinsics.checkParameterIsNotNull(analyticsRequestExecutor, "analyticsRequestExecutor");
                AnalyticsDataFactory analyticsDataFactory2 = analyticsDataFactory;
                Intrinsics.checkParameterIsNotNull(analyticsDataFactory2, "analyticsDataFactory");
                Transaction transaction2 = transaction;
                Intrinsics.checkParameterIsNotNull(transaction2, "transaction");
                AnalyticsRequest.Factory factory2 = factory;
                Intrinsics.checkParameterIsNotNull(factory2, "analyticsRequestFactory");
                return new PaymentAuth3ds2ChallengeStatusReceiver(stripeRepository, stripeIntent, str, options, analyticsRequestExecutor, analyticsDataFactory2, transaction2, factory2);
            }
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\b`\u0018\u00002\u00020\u0001:\u0001\u0006J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&\u0002\u0004\n\u0002\b\u0019¨\u0006\u0007"}, d2 = {"Lcom/stripe/android/StripePaymentController$ChallengeFlowStarter;", "", "start", "", "runnable", "Ljava/lang/Runnable;", "Default", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: StripePaymentController.kt */
    public interface ChallengeFlowStarter {
        void start(Runnable runnable);

        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u0000 \u00072\u00020\u0001:\u0001\u0007B\u0005¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0016\u0002\u0004\n\u0002\b\u0019¨\u0006\b"}, d2 = {"Lcom/stripe/android/StripePaymentController$ChallengeFlowStarter$Default;", "Lcom/stripe/android/StripePaymentController$ChallengeFlowStarter;", "()V", "start", "", "runnable", "Ljava/lang/Runnable;", "Companion", "stripe_release"}, k = 1, mv = {1, 1, 16})
        /* compiled from: StripePaymentController.kt */
        public static final class Default implements ChallengeFlowStarter {
            @Deprecated
            public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
            private static final long DELAY_SECONDS = 2;

            public void start(Runnable runnable) {
                Intrinsics.checkParameterIsNotNull(runnable, "runnable");
                HandlerThread handlerThread = new HandlerThread(Stripe3ds2AuthCallback.class.getSimpleName());
                Companion.createHandler(handlerThread).postDelayed(new StripePaymentController$ChallengeFlowStarter$Default$start$1(runnable, handlerThread), TimeUnit.SECONDS.toMillis(2));
            }

            @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bH\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000\u0002\u0004\n\u0002\b\u0019¨\u0006\t"}, d2 = {"Lcom/stripe/android/StripePaymentController$ChallengeFlowStarter$Default$Companion;", "", "()V", "DELAY_SECONDS", "", "createHandler", "Landroid/os/Handler;", "handlerThread", "Landroid/os/HandlerThread;", "stripe_release"}, k = 1, mv = {1, 1, 16})
            /* compiled from: StripePaymentController.kt */
            private static final class Companion {
                private Companion() {
                }

                public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                    this();
                }

                /* access modifiers changed from: private */
                public final Handler createHandler(HandlerThread handlerThread) {
                    handlerThread.start();
                    return new Handler(handlerThread.getLooper());
                }
            }
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\b`\u0018\u00002\u00020\u0001:\u0001\fJ(\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000bH&\u0002\u0004\n\u0002\b\u0019¨\u0006\r"}, d2 = {"Lcom/stripe/android/StripePaymentController$ChallengeProgressDialogActivityStarter;", "", "start", "", "context", "Landroid/content/Context;", "directoryServerName", "", "cancelable", "", "uiCustomization", "Lcom/stripe/android/stripe3ds2/init/ui/StripeUiCustomization;", "Default", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: StripePaymentController.kt */
    public interface ChallengeProgressDialogActivityStarter {
        void start(Context context, String str, boolean z, StripeUiCustomization stripeUiCustomization);

        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J(\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\fH\u0016\u0002\u0004\n\u0002\b\u0019¨\u0006\r"}, d2 = {"Lcom/stripe/android/StripePaymentController$ChallengeProgressDialogActivityStarter$Default;", "Lcom/stripe/android/StripePaymentController$ChallengeProgressDialogActivityStarter;", "()V", "start", "", "context", "Landroid/content/Context;", "directoryServerName", "", "cancelable", "", "uiCustomization", "Lcom/stripe/android/stripe3ds2/init/ui/StripeUiCustomization;", "stripe_release"}, k = 1, mv = {1, 1, 16})
        /* compiled from: StripePaymentController.kt */
        public static final class Default implements ChallengeProgressDialogActivityStarter {
            public void start(Context context, String str, boolean z, StripeUiCustomization stripeUiCustomization) {
                Intrinsics.checkParameterIsNotNull(context, "context");
                Intrinsics.checkParameterIsNotNull(str, "directoryServerName");
                Intrinsics.checkParameterIsNotNull(stripeUiCustomization, "uiCustomization");
                ChallengeProgressDialogActivity.Companion.show(context, str, z, stripeUiCustomization);
            }
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000j\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002JH\u0010\n\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u00072\u0006\u0010\u000f\u001a\u00020\u00052\u0006\u0010\u0010\u001a\u00020\u00052\b\u0010\u0011\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\u0012\u001a\u0004\u0018\u00010\u00052\b\b\u0002\u0010\u0013\u001a\u00020\u0014H\u0002J*\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00020\u00052\u0006\u0010\u001a\u001a\u00020\u001b2\b\b\u0002\u0010\u0013\u001a\u00020\u0014H\u0007J\u0015\u0010\u001c\u001a\u00020\u00052\u0006\u0010\u001d\u001a\u00020\u001eH\u0000¢\u0006\u0002\b\u001fJ\u0015\u0010 \u001a\u00020\u00072\u0006\u0010!\u001a\u00020\"H\u0000¢\u0006\u0002\b#J\u0015\u0010 \u001a\u00020\u00072\u0006\u0010$\u001a\u00020%H\u0000¢\u0006\u0002\b#J$\u0010&\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u00072\n\u0010'\u001a\u00060(j\u0002`)H\u0002R\u0014\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007XT¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0007XT¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0007XT¢\u0006\u0002\n\u0000\u0002\u0004\n\u0002\b\u0019¨\u0006*"}, d2 = {"Lcom/stripe/android/StripePaymentController$Companion;", "", "()V", "EXPAND_PAYMENT_METHOD", "", "", "PAYMENT_REQUEST_CODE", "", "SETUP_REQUEST_CODE", "SOURCE_REQUEST_CODE", "beginWebAuth", "", "host", "Lcom/stripe/android/view/AuthActivityStarter$Host;", "requestCode", "clientSecret", "authUrl", "stripeAccount", "returnUrl", "enableLogging", "", "create", "Lcom/stripe/android/PaymentController;", "context", "Landroid/content/Context;", "publishableKey", "stripeRepository", "Lcom/stripe/android/StripeRepository;", "getClientSecret", "data", "Landroid/content/Intent;", "getClientSecret$stripe_release", "getRequestCode", "params", "Lcom/stripe/android/model/ConfirmStripeIntentParams;", "getRequestCode$stripe_release", "intent", "Lcom/stripe/android/model/StripeIntent;", "handleError", "exception", "Ljava/lang/Exception;", "Lkotlin/Exception;", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: StripePaymentController.kt */
    public static final class Companion {
        @JvmStatic
        public final PaymentController create(Context context, String str, StripeRepository stripeRepository) {
            return create$default(this, context, str, stripeRepository, false, 8, (Object) null);
        }

        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        public final /* synthetic */ int getRequestCode$stripe_release(StripeIntent stripeIntent) {
            Intrinsics.checkParameterIsNotNull(stripeIntent, "intent");
            return stripeIntent instanceof PaymentIntent ? StripePaymentController.PAYMENT_REQUEST_CODE : StripePaymentController.SETUP_REQUEST_CODE;
        }

        public final /* synthetic */ int getRequestCode$stripe_release(ConfirmStripeIntentParams confirmStripeIntentParams) {
            Intrinsics.checkParameterIsNotNull(confirmStripeIntentParams, NativeProtocol.WEB_DIALOG_PARAMS);
            return confirmStripeIntentParams instanceof ConfirmPaymentIntentParams ? StripePaymentController.PAYMENT_REQUEST_CODE : StripePaymentController.SETUP_REQUEST_CODE;
        }

        static /* synthetic */ void beginWebAuth$default(Companion companion, AuthActivityStarter.Host host, int i, String str, String str2, String str3, String str4, boolean z, int i2, Object obj) {
            companion.beginWebAuth(host, i, str, str2, str3, (i2 & 32) != 0 ? null : str4, (i2 & 64) != 0 ? false : z);
        }

        /* access modifiers changed from: private */
        public final void beginWebAuth(AuthActivityStarter.Host host, int i, String str, String str2, String str3, String str4, boolean z) {
            boolean z2 = z;
            Logger.Companion.getInstance$stripe_release(z2).debug("PaymentAuthWebViewStarter#start()");
            AuthActivityStarter.Host host2 = host;
            int i2 = i;
            new PaymentAuthWebViewStarter(host, i).start(new PaymentAuthWebViewStarter.Args(str, str2, str4, z2, (StripeToolbarCustomization) null, str3, 16, (DefaultConstructorMarker) null));
        }

        /* access modifiers changed from: private */
        public final void handleError(AuthActivityStarter.Host host, int i, Exception exc) {
            StripeException stripeException;
            PaymentRelayStarter create$stripe_release = PaymentRelayStarter.Companion.create$stripe_release(host, i);
            PaymentRelayStarter.Args.Companion companion = PaymentRelayStarter.Args.Companion;
            if (exc instanceof StripeException) {
                stripeException = (StripeException) exc;
            } else {
                stripeException = new APIException(exc);
            }
            create$stripe_release.start(companion.create$stripe_release(stripeException));
        }

        public static /* synthetic */ PaymentController create$default(Companion companion, Context context, String str, StripeRepository stripeRepository, boolean z, int i, Object obj) {
            if ((i & 8) != 0) {
                z = false;
            }
            return companion.create(context, str, stripeRepository, z);
        }

        @JvmStatic
        public final PaymentController create(Context context, String str, StripeRepository stripeRepository, boolean z) {
            Intrinsics.checkParameterIsNotNull(context, "context");
            String str2 = str;
            Intrinsics.checkParameterIsNotNull(str2, "publishableKey");
            StripeRepository stripeRepository2 = stripeRepository;
            Intrinsics.checkParameterIsNotNull(stripeRepository2, "stripeRepository");
            Context applicationContext = context.getApplicationContext();
            Intrinsics.checkExpressionValueIsNotNull(applicationContext, "context.applicationContext");
            return new StripePaymentController(applicationContext, str2, stripeRepository2, z, (MessageVersionRegistry) null, (PaymentAuthConfig) null, (StripeThreeDs2Service) null, (AnalyticsRequestExecutor) null, (AnalyticsDataFactory) null, (ChallengeFlowStarter) null, (ChallengeProgressDialogActivityStarter) null, (CoroutineScope) null, 4080, (DefaultConstructorMarker) null);
        }

        public final /* synthetic */ String getClientSecret$stripe_release(Intent intent) {
            Intrinsics.checkParameterIsNotNull(intent, "data");
            PaymentController.Result fromIntent$stripe_release = PaymentController.Result.Companion.fromIntent$stripe_release(intent);
            String clientSecret$stripe_release = fromIntent$stripe_release != null ? fromIntent$stripe_release.getClientSecret$stripe_release() : null;
            if (clientSecret$stripe_release != null) {
                return clientSecret$stripe_release;
            }
            throw new IllegalArgumentException("Required value was null.".toString());
        }
    }
}
