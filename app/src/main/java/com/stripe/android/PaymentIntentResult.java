package com.stripe.android;

import android.os.Parcel;
import android.os.Parcelable;
import com.stripe.android.model.PaymentIntent;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0003\u0018\u0000 \n2\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\nB\u000f\b\u0010\u0012\u0006\u0010\u0003\u001a\u00020\u0004¢\u0006\u0002\u0010\u0005B\u0019\b\u0000\u0012\u0006\u0010\u0006\u001a\u00020\u0002\u0012\b\b\u0002\u0010\u0007\u001a\u00020\b¢\u0006\u0002\u0010\t¨\u0006\u000b"}, d2 = {"Lcom/stripe/android/PaymentIntentResult;", "Lcom/stripe/android/StripeIntentResult;", "Lcom/stripe/android/model/PaymentIntent;", "parcel", "Landroid/os/Parcel;", "(Landroid/os/Parcel;)V", "paymentIntent", "outcome", "", "(Lcom/stripe/android/model/PaymentIntent;I)V", "CREATOR", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: PaymentIntentResult.kt */
public final class PaymentIntentResult extends StripeIntentResult<PaymentIntent> {
    public static final CREATOR CREATOR = new CREATOR((DefaultConstructorMarker) null);

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ PaymentIntentResult(PaymentIntent paymentIntent, int i, int i2, DefaultConstructorMarker defaultConstructorMarker) {
        this(paymentIntent, (i2 & 2) != 0 ? 0 : i);
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public PaymentIntentResult(PaymentIntent paymentIntent, int i) {
        super(paymentIntent, i);
        Intrinsics.checkParameterIsNotNull(paymentIntent, "paymentIntent");
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public PaymentIntentResult(android.os.Parcel r2) {
        /*
            r1 = this;
            java.lang.String r0 = "parcel"
            kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(r2, r0)
            java.lang.Class<com.stripe.android.model.PaymentIntent> r0 = com.stripe.android.model.PaymentIntent.class
            java.lang.ClassLoader r0 = r0.getClassLoader()
            android.os.Parcelable r0 = r2.readParcelable(r0)
            if (r0 == 0) goto L_0x001b
            com.stripe.android.model.PaymentIntent r0 = (com.stripe.android.model.PaymentIntent) r0
            int r2 = r2.readInt()
            r1.<init>(r0, r2)
            return
        L_0x001b:
            java.lang.IllegalArgumentException r2 = new java.lang.IllegalArgumentException
            java.lang.String r0 = "Required value was null."
            java.lang.String r0 = r0.toString()
            r2.<init>(r0)
            java.lang.Throwable r2 = (java.lang.Throwable) r2
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.stripe.android.PaymentIntentResult.<init>(android.os.Parcel):void");
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0011\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\b\u0003\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0003J\u0010\u0010\u0004\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0006H\u0016J\u001d\u0010\u0007\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00020\b2\u0006\u0010\t\u001a\u00020\nH\u0016¢\u0006\u0002\u0010\u000b¨\u0006\f"}, d2 = {"Lcom/stripe/android/PaymentIntentResult$CREATOR;", "Landroid/os/Parcelable$Creator;", "Lcom/stripe/android/PaymentIntentResult;", "()V", "createFromParcel", "parcel", "Landroid/os/Parcel;", "newArray", "", "size", "", "(I)[Lcom/stripe/android/PaymentIntentResult;", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: PaymentIntentResult.kt */
    public static final class CREATOR implements Parcelable.Creator<PaymentIntentResult> {
        private CREATOR() {
        }

        public /* synthetic */ CREATOR(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        public PaymentIntentResult createFromParcel(Parcel parcel) {
            Intrinsics.checkParameterIsNotNull(parcel, "parcel");
            return new PaymentIntentResult(parcel);
        }

        public PaymentIntentResult[] newArray(int i) {
            return new PaymentIntentResult[i];
        }
    }
}
