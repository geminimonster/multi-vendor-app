package com.stripe.android;

import kotlin.Metadata;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u0003\n\u0002\b\u0003\b`\u0018\u0000 \n2\u00020\u0001:\u0001\nJ\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&J\u001c\u0010\u0006\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\n\b\u0002\u0010\u0007\u001a\u0004\u0018\u00010\bH&J\u0010\u0010\t\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&¨\u0006\u000b"}, d2 = {"Lcom/stripe/android/Logger;", "", "debug", "", "msg", "", "error", "t", "", "info", "Companion", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: Logger.kt */
public interface Logger {
    public static final Companion Companion = Companion.$$INSTANCE;

    void debug(String str);

    void error(String str, Throwable th);

    void info(String str);

    @Metadata(bv = {1, 0, 3}, k = 3, mv = {1, 1, 16})
    /* compiled from: Logger.kt */
    public static final class DefaultImpls {
        public static /* synthetic */ void error$default(Logger logger, String str, Throwable th, int i, Object obj) {
            if (obj == null) {
                if ((i & 2) != 0) {
                    th = null;
                }
                logger.error(str, th);
                return;
            }
            throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: error");
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\b\u0003\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0006*\u0002\u0004\u0007\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0015\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000eH\u0000¢\u0006\u0002\b\u000fJ\r\u0010\u0010\u001a\u00020\fH\u0000¢\u0006\u0002\b\u0011J\r\u0010\u0012\u001a\u00020\fH\u0000¢\u0006\u0002\b\u0013R\u0010\u0010\u0003\u001a\u00020\u0004X\u0004¢\u0006\u0004\n\u0002\u0010\u0005R\u0010\u0010\u0006\u001a\u00020\u0007X\u0004¢\u0006\u0004\n\u0002\u0010\bR\u000e\u0010\t\u001a\u00020\nXT¢\u0006\u0002\n\u0000¨\u0006\u0014"}, d2 = {"Lcom/stripe/android/Logger$Companion;", "", "()V", "NOOP_LOGGER", "com/stripe/android/Logger$Companion$NOOP_LOGGER$1", "Lcom/stripe/android/Logger$Companion$NOOP_LOGGER$1;", "REAL_LOGGER", "com/stripe/android/Logger$Companion$REAL_LOGGER$1", "Lcom/stripe/android/Logger$Companion$REAL_LOGGER$1;", "TAG", "", "getInstance", "Lcom/stripe/android/Logger;", "enableLogging", "", "getInstance$stripe_release", "noop", "noop$stripe_release", "real", "real$stripe_release", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: Logger.kt */
    public static final class Companion {
        static final /* synthetic */ Companion $$INSTANCE;
        private static final Logger$Companion$NOOP_LOGGER$1 NOOP_LOGGER = new Logger$Companion$NOOP_LOGGER$1();
        private static final Logger$Companion$REAL_LOGGER$1 REAL_LOGGER;
        private static final String TAG = "StripeSdk";

        static {
            Companion companion = new Companion();
            $$INSTANCE = companion;
            REAL_LOGGER = new Logger$Companion$REAL_LOGGER$1(companion);
        }

        private Companion() {
        }

        public final Logger getInstance$stripe_release(boolean z) {
            if (z) {
                return real$stripe_release();
            }
            return noop$stripe_release();
        }

        public final Logger real$stripe_release() {
            return REAL_LOGGER;
        }

        public final Logger noop$stripe_release() {
            return NOOP_LOGGER;
        }
    }
}
