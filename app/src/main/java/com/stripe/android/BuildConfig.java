package com.stripe.android;

public final class BuildConfig {
    public static final String BUILD_TYPE = "release";
    public static final boolean DEBUG = false;
    public static final String LIBRARY_PACKAGE_NAME = "com.stripe.android";
    public static final int VERSION_CODE = 104;
    public static final String VERSION_NAME = "14.5.0";
}
