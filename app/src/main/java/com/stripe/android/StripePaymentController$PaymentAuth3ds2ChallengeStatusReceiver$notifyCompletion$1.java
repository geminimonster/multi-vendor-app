package com.stripe.android;

import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000!\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003*\u0001\u0000\b\n\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001J\u0014\u0010\u0003\u001a\u00020\u00042\n\u0010\u0005\u001a\u00060\u0006j\u0002`\u0007H\u0016J\u0010\u0010\b\u001a\u00020\u00042\u0006\u0010\t\u001a\u00020\u0002H\u0016¨\u0006\n"}, d2 = {"com/stripe/android/StripePaymentController$PaymentAuth3ds2ChallengeStatusReceiver$notifyCompletion$1", "Lcom/stripe/android/ApiResultCallback;", "", "onError", "", "e", "Ljava/lang/Exception;", "Lkotlin/Exception;", "onSuccess", "result", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: StripePaymentController.kt */
public final class StripePaymentController$PaymentAuth3ds2ChallengeStatusReceiver$notifyCompletion$1 implements ApiResultCallback<Boolean> {
    final /* synthetic */ Function0 $completed3ds2Callback;

    StripePaymentController$PaymentAuth3ds2ChallengeStatusReceiver$notifyCompletion$1(Function0 function0) {
        this.$completed3ds2Callback = function0;
    }

    public /* bridge */ /* synthetic */ void onSuccess(Object obj) {
        onSuccess(((Boolean) obj).booleanValue());
    }

    public void onSuccess(boolean z) {
        this.$completed3ds2Callback.invoke();
    }

    public void onError(Exception exc) {
        Intrinsics.checkParameterIsNotNull(exc, "e");
        this.$completed3ds2Callback.invoke();
    }
}
