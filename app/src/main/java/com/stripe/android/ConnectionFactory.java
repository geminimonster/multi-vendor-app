package com.stripe.android;

import com.stripe.android.exception.InvalidRequestException;
import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.util.concurrent.TimeUnit;
import javax.net.ssl.HttpsURLConnection;
import kotlin.Metadata;
import kotlin.TypeCastException;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\b`\u0018\u00002\u00020\u0001:\u0001\u0006J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&¨\u0006\u0007"}, d2 = {"Lcom/stripe/android/ConnectionFactory;", "", "create", "Lcom/stripe/android/StripeConnection;", "request", "Lcom/stripe/android/StripeRequest;", "Default", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: ConnectionFactory.kt */
public interface ConnectionFactory {
    StripeConnection create(StripeRequest stripeRequest) throws IOException, InvalidRequestException;

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\u0018\u0000 \u000b2\u00020\u0001:\u0001\u000bB\u0005¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0016J\u0010\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\nH\u0002¨\u0006\f"}, d2 = {"Lcom/stripe/android/ConnectionFactory$Default;", "Lcom/stripe/android/ConnectionFactory;", "()V", "create", "Lcom/stripe/android/StripeConnection;", "request", "Lcom/stripe/android/StripeRequest;", "openConnection", "Ljavax/net/ssl/HttpsURLConnection;", "requestUrl", "", "Companion", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: ConnectionFactory.kt */
    public static final class Default implements ConnectionFactory {
        private static final int CONNECT_TIMEOUT = ((int) TimeUnit.SECONDS.toMillis(30));
        @Deprecated
        public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
        private static final String HEADER_CONTENT_TYPE = "Content-Type";
        private static final int READ_TIMEOUT = ((int) TimeUnit.SECONDS.toMillis(80));
        private static final StripeSSLSocketFactory SSL_SOCKET_FACTORY = new StripeSSLSocketFactory();

        /* JADX WARNING: Code restructure failed: missing block: B:13:0x0086, code lost:
            r0 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:14:0x0087, code lost:
            kotlin.io.CloseableKt.closeFinally(r1, r6);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:15:0x008a, code lost:
            throw r0;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public /* synthetic */ com.stripe.android.StripeConnection create(com.stripe.android.StripeRequest r6) throws java.io.IOException, com.stripe.android.exception.InvalidRequestException {
            /*
                r5 = this;
                java.lang.String r0 = "request"
                kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(r6, r0)
                java.lang.String r0 = r6.getUrl$stripe_release()
                javax.net.ssl.HttpsURLConnection r0 = r5.openConnection(r0)
                int r1 = CONNECT_TIMEOUT
                r0.setConnectTimeout(r1)
                int r1 = READ_TIMEOUT
                r0.setReadTimeout(r1)
                r1 = 0
                r0.setUseCaches(r1)
                com.stripe.android.StripeSSLSocketFactory r1 = SSL_SOCKET_FACTORY
                javax.net.ssl.SSLSocketFactory r1 = (javax.net.ssl.SSLSocketFactory) r1
                r0.setSSLSocketFactory(r1)
                com.stripe.android.StripeRequest$Method r1 = r6.getMethod()
                java.lang.String r1 = r1.getCode()
                r0.setRequestMethod(r1)
                java.util.Map r1 = r6.getHeaders$stripe_release()
                java.util.Set r1 = r1.entrySet()
                java.util.Iterator r1 = r1.iterator()
            L_0x0039:
                boolean r2 = r1.hasNext()
                if (r2 == 0) goto L_0x0055
                java.lang.Object r2 = r1.next()
                java.util.Map$Entry r2 = (java.util.Map.Entry) r2
                java.lang.Object r3 = r2.getKey()
                java.lang.String r3 = (java.lang.String) r3
                java.lang.Object r2 = r2.getValue()
                java.lang.String r2 = (java.lang.String) r2
                r0.setRequestProperty(r3, r2)
                goto L_0x0039
            L_0x0055:
                com.stripe.android.StripeRequest$Method r1 = com.stripe.android.StripeRequest.Method.POST
                com.stripe.android.StripeRequest$Method r2 = r6.getMethod()
                if (r1 != r2) goto L_0x008b
                r1 = 1
                r0.setDoOutput(r1)
                java.lang.String r1 = r6.getContentType$stripe_release()
                java.lang.String r2 = "Content-Type"
                r0.setRequestProperty(r2, r1)
                java.io.OutputStream r1 = r0.getOutputStream()
                java.io.Closeable r1 = (java.io.Closeable) r1
                r2 = 0
                java.lang.Throwable r2 = (java.lang.Throwable) r2
                r3 = r1
                java.io.OutputStream r3 = (java.io.OutputStream) r3     // Catch:{ all -> 0x0084 }
                java.lang.String r4 = "output"
                kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r3, r4)     // Catch:{ all -> 0x0084 }
                r6.writeBody$stripe_release(r3)     // Catch:{ all -> 0x0084 }
                kotlin.Unit r6 = kotlin.Unit.INSTANCE     // Catch:{ all -> 0x0084 }
                kotlin.io.CloseableKt.closeFinally(r1, r2)
                goto L_0x008b
            L_0x0084:
                r6 = move-exception
                throw r6     // Catch:{ all -> 0x0086 }
            L_0x0086:
                r0 = move-exception
                kotlin.io.CloseableKt.closeFinally(r1, r6)
                throw r0
            L_0x008b:
                com.stripe.android.StripeConnection$Default r6 = new com.stripe.android.StripeConnection$Default
                r6.<init>(r0)
                com.stripe.android.StripeConnection r6 = (com.stripe.android.StripeConnection) r6
                return r6
            */
            throw new UnsupportedOperationException("Method not decompiled: com.stripe.android.ConnectionFactory.Default.create(com.stripe.android.StripeRequest):com.stripe.android.StripeConnection");
        }

        private final HttpsURLConnection openConnection(String str) {
            URLConnection openConnection = new URL(str).openConnection();
            if (openConnection != null) {
                return (HttpsURLConnection) openConnection;
            }
            throw new TypeCastException("null cannot be cast to non-null type javax.net.ssl.HttpsURLConnection");
        }

        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0004¢\u0006\u0002\n\u0000¨\u0006\n"}, d2 = {"Lcom/stripe/android/ConnectionFactory$Default$Companion;", "", "()V", "CONNECT_TIMEOUT", "", "HEADER_CONTENT_TYPE", "", "READ_TIMEOUT", "SSL_SOCKET_FACTORY", "Lcom/stripe/android/StripeSSLSocketFactory;", "stripe_release"}, k = 1, mv = {1, 1, 16})
        /* compiled from: ConnectionFactory.kt */
        private static final class Companion {
            private Companion() {
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }
        }
    }
}
