package com.stripe.android;

import android.os.Parcel;
import android.os.Parcelable;
import com.stripe.android.model.PaymentMethod;
import com.stripe.android.model.ShippingInformation;
import com.stripe.android.model.ShippingMethod;
import com.stripe.android.view.BillingAddressFields;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt;
import kotlin.collections.SetsKt;
import kotlin.jvm.internal.Intrinsics;
import kotlin.text.StringsKt;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000d\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\"\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b1\n\u0002\u0010\u0000\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\b\b\u0018\u0000 T2\u00020\u0001:\u0005STUVWB»\u0001\b\u0000\u0012\u000e\b\u0002\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003\u0012\u000e\b\u0002\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003\u0012\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u0012\b\b\u0002\u0010\b\u001a\u00020\t\u0012\b\b\u0002\u0010\n\u001a\u00020\t\u0012\b\b\u0003\u0010\u000b\u001a\u00020\f\u0012\u000e\b\u0002\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u000e0\u0003\u0012\b\b\u0002\u0010\u000f\u001a\u00020\t\u0012\u000e\b\u0002\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u00040\u0011\u0012\b\b\u0002\u0010\u0012\u001a\u00020\u0013\u0012\b\b\u0002\u0010\u0014\u001a\u00020\t\u0012\b\b\u0002\u0010\u0015\u001a\u00020\t\u0012\b\b\u0002\u0010\u0016\u001a\u00020\u0017\u0012\n\b\u0002\u0010\u0018\u001a\u0004\u0018\u00010\u0019\u0012\n\b\u0002\u0010\u001a\u001a\u0004\u0018\u00010\f¢\u0006\u0002\u0010\u001bJ\u000f\u00103\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003HÆ\u0003J\t\u00104\u001a\u00020\u0013HÆ\u0003J\t\u00105\u001a\u00020\tHÆ\u0003J\u000e\u00106\u001a\u00020\tHÀ\u0003¢\u0006\u0002\b7J\u000e\u00108\u001a\u00020\u0017HÀ\u0003¢\u0006\u0002\b9J\u0010\u0010:\u001a\u0004\u0018\u00010\u0019HÀ\u0003¢\u0006\u0002\b;J\u0012\u0010<\u001a\u0004\u0018\u00010\fHÀ\u0003¢\u0006\u0004\b=\u00101J\u000f\u0010>\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003HÆ\u0003J\u000b\u0010?\u001a\u0004\u0018\u00010\u0007HÆ\u0003J\t\u0010@\u001a\u00020\tHÆ\u0003J\t\u0010A\u001a\u00020\tHÆ\u0003J\t\u0010B\u001a\u00020\fHÆ\u0003J\u000f\u0010C\u001a\b\u0012\u0004\u0012\u00020\u000e0\u0003HÆ\u0003J\t\u0010D\u001a\u00020\tHÆ\u0003J\u000f\u0010E\u001a\b\u0012\u0004\u0012\u00020\u00040\u0011HÆ\u0003JÂ\u0001\u0010F\u001a\u00020\u00002\u000e\b\u0002\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u00032\u000e\b\u0002\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00040\u00032\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u00072\b\b\u0002\u0010\b\u001a\u00020\t2\b\b\u0002\u0010\n\u001a\u00020\t2\b\b\u0003\u0010\u000b\u001a\u00020\f2\u000e\b\u0002\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u000e0\u00032\b\b\u0002\u0010\u000f\u001a\u00020\t2\u000e\b\u0002\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u00040\u00112\b\b\u0002\u0010\u0012\u001a\u00020\u00132\b\b\u0002\u0010\u0014\u001a\u00020\t2\b\b\u0002\u0010\u0015\u001a\u00020\t2\b\b\u0002\u0010\u0016\u001a\u00020\u00172\n\b\u0002\u0010\u0018\u001a\u0004\u0018\u00010\u00192\n\b\u0002\u0010\u001a\u001a\u0004\u0018\u00010\fHÆ\u0001¢\u0006\u0002\u0010GJ\t\u0010H\u001a\u00020\fHÖ\u0001J\u0013\u0010I\u001a\u00020\t2\b\u0010J\u001a\u0004\u0018\u00010KHÖ\u0003J\t\u0010L\u001a\u00020\fHÖ\u0001J\t\u0010M\u001a\u00020\u0004HÖ\u0001J\u0019\u0010N\u001a\u00020O2\u0006\u0010P\u001a\u00020Q2\u0006\u0010R\u001a\u00020\fHÖ\u0001R\u0013\u0010\u000b\u001a\u00020\f8\u0007¢\u0006\b\n\u0000\u001a\u0004\b\u001c\u0010\u001dR\u0017\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u00040\u0011¢\u0006\b\n\u0000\u001a\u0004\b\u001e\u0010\u001fR\u0011\u0010\u0012\u001a\u00020\u0013¢\u0006\b\n\u0000\u001a\u0004\b \u0010!R\u0011\u0010\u0014\u001a\u00020\t¢\u0006\b\n\u0000\u001a\u0004\b\"\u0010#R\u0017\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003¢\u0006\b\n\u0000\u001a\u0004\b$\u0010%R\u0011\u0010\b\u001a\u00020\t¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010#R\u0011\u0010\n\u001a\u00020\t¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010#R\u0017\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003¢\u0006\b\n\u0000\u001a\u0004\b&\u0010%R\u0017\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u000e0\u0003¢\u0006\b\n\u0000\u001a\u0004\b'\u0010%R\u0013\u0010\u0006\u001a\u0004\u0018\u00010\u0007¢\u0006\b\n\u0000\u001a\u0004\b(\u0010)R\u0014\u0010\u0016\u001a\u00020\u0017X\u0004¢\u0006\b\n\u0000\u001a\u0004\b*\u0010+R\u0016\u0010\u0018\u001a\u0004\u0018\u00010\u0019X\u0004¢\u0006\b\n\u0000\u001a\u0004\b,\u0010-R\u0014\u0010\u0015\u001a\u00020\tX\u0004¢\u0006\b\n\u0000\u001a\u0004\b.\u0010#R\u0011\u0010\u000f\u001a\u00020\t¢\u0006\b\n\u0000\u001a\u0004\b/\u0010#R\u0018\u0010\u001a\u001a\u0004\u0018\u00010\fX\u0004¢\u0006\n\n\u0002\u00102\u001a\u0004\b0\u00101¨\u0006X"}, d2 = {"Lcom/stripe/android/PaymentSessionConfig;", "Landroid/os/Parcelable;", "hiddenShippingInfoFields", "", "", "optionalShippingInfoFields", "prepopulatedShippingInfo", "Lcom/stripe/android/model/ShippingInformation;", "isShippingInfoRequired", "", "isShippingMethodRequired", "addPaymentMethodFooterLayoutId", "", "paymentMethodTypes", "Lcom/stripe/android/model/PaymentMethod$Type;", "shouldShowGooglePay", "allowedShippingCountryCodes", "", "billingAddressFields", "Lcom/stripe/android/view/BillingAddressFields;", "canDeletePaymentMethods", "shouldPrefetchCustomer", "shippingInformationValidator", "Lcom/stripe/android/PaymentSessionConfig$ShippingInformationValidator;", "shippingMethodsFactory", "Lcom/stripe/android/PaymentSessionConfig$ShippingMethodsFactory;", "windowFlags", "(Ljava/util/List;Ljava/util/List;Lcom/stripe/android/model/ShippingInformation;ZZILjava/util/List;ZLjava/util/Set;Lcom/stripe/android/view/BillingAddressFields;ZZLcom/stripe/android/PaymentSessionConfig$ShippingInformationValidator;Lcom/stripe/android/PaymentSessionConfig$ShippingMethodsFactory;Ljava/lang/Integer;)V", "getAddPaymentMethodFooterLayoutId", "()I", "getAllowedShippingCountryCodes", "()Ljava/util/Set;", "getBillingAddressFields", "()Lcom/stripe/android/view/BillingAddressFields;", "getCanDeletePaymentMethods", "()Z", "getHiddenShippingInfoFields", "()Ljava/util/List;", "getOptionalShippingInfoFields", "getPaymentMethodTypes", "getPrepopulatedShippingInfo", "()Lcom/stripe/android/model/ShippingInformation;", "getShippingInformationValidator$stripe_release", "()Lcom/stripe/android/PaymentSessionConfig$ShippingInformationValidator;", "getShippingMethodsFactory$stripe_release", "()Lcom/stripe/android/PaymentSessionConfig$ShippingMethodsFactory;", "getShouldPrefetchCustomer$stripe_release", "getShouldShowGooglePay", "getWindowFlags$stripe_release", "()Ljava/lang/Integer;", "Ljava/lang/Integer;", "component1", "component10", "component11", "component12", "component12$stripe_release", "component13", "component13$stripe_release", "component14", "component14$stripe_release", "component15", "component15$stripe_release", "component2", "component3", "component4", "component5", "component6", "component7", "component8", "component9", "copy", "(Ljava/util/List;Ljava/util/List;Lcom/stripe/android/model/ShippingInformation;ZZILjava/util/List;ZLjava/util/Set;Lcom/stripe/android/view/BillingAddressFields;ZZLcom/stripe/android/PaymentSessionConfig$ShippingInformationValidator;Lcom/stripe/android/PaymentSessionConfig$ShippingMethodsFactory;Ljava/lang/Integer;)Lcom/stripe/android/PaymentSessionConfig;", "describeContents", "equals", "other", "", "hashCode", "toString", "writeToParcel", "", "parcel", "Landroid/os/Parcel;", "flags", "Builder", "Companion", "DefaultShippingInfoValidator", "ShippingInformationValidator", "ShippingMethodsFactory", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: PaymentSessionConfig.kt */
public final class PaymentSessionConfig implements Parcelable {
    public static final Parcelable.Creator CREATOR = new Creator();
    @Deprecated
    public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
    /* access modifiers changed from: private */
    public static final BillingAddressFields DEFAULT_BILLING_ADDRESS_FIELDS = BillingAddressFields.PostalCode;
    private final int addPaymentMethodFooterLayoutId;
    private final Set<String> allowedShippingCountryCodes;
    private final BillingAddressFields billingAddressFields;
    private final boolean canDeletePaymentMethods;
    private final List<String> hiddenShippingInfoFields;
    private final boolean isShippingInfoRequired;
    private final boolean isShippingMethodRequired;
    private final List<String> optionalShippingInfoFields;
    private final List<PaymentMethod.Type> paymentMethodTypes;
    private final ShippingInformation prepopulatedShippingInfo;
    private final ShippingInformationValidator shippingInformationValidator;
    private final ShippingMethodsFactory shippingMethodsFactory;
    private final boolean shouldPrefetchCustomer;
    private final boolean shouldShowGooglePay;
    private final Integer windowFlags;

    @Metadata(bv = {1, 0, 3}, k = 3, mv = {1, 1, 16})
    public static class Creator implements Parcelable.Creator {
        public final Object createFromParcel(Parcel parcel) {
            Parcel parcel2 = parcel;
            Intrinsics.checkParameterIsNotNull(parcel2, "in");
            List createStringArrayList = parcel.createStringArrayList();
            List createStringArrayList2 = parcel.createStringArrayList();
            ShippingInformation shippingInformation = parcel.readInt() != 0 ? (ShippingInformation) ShippingInformation.CREATOR.createFromParcel(parcel2) : null;
            boolean z = parcel.readInt() != 0;
            boolean z2 = parcel.readInt() != 0;
            int readInt = parcel.readInt();
            int readInt2 = parcel.readInt();
            ArrayList arrayList = new ArrayList(readInt2);
            while (readInt2 != 0) {
                arrayList.add((PaymentMethod.Type) Enum.valueOf(PaymentMethod.Type.class, parcel.readString()));
                readInt2--;
            }
            boolean z3 = parcel.readInt() != 0;
            int readInt3 = parcel.readInt();
            LinkedHashSet linkedHashSet = new LinkedHashSet(readInt3);
            while (readInt3 != 0) {
                linkedHashSet.add(parcel.readString());
                readInt3--;
            }
            return new PaymentSessionConfig(createStringArrayList, createStringArrayList2, shippingInformation, z, z2, readInt, arrayList, z3, linkedHashSet, (BillingAddressFields) Enum.valueOf(BillingAddressFields.class, parcel.readString()), parcel.readInt() != 0, parcel.readInt() != 0, (ShippingInformationValidator) parcel.readSerializable(), (ShippingMethodsFactory) parcel.readSerializable(), parcel.readInt() != 0 ? Integer.valueOf(parcel.readInt()) : null);
        }

        public final Object[] newArray(int i) {
            return new PaymentSessionConfig[i];
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\b\u0002\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0016J\u0010\u0010\u0007\u001a\u00020\b2\u0006\u0010\u0005\u001a\u00020\u0006H\u0016¨\u0006\t"}, d2 = {"Lcom/stripe/android/PaymentSessionConfig$DefaultShippingInfoValidator;", "Lcom/stripe/android/PaymentSessionConfig$ShippingInformationValidator;", "()V", "getErrorMessage", "", "shippingInformation", "Lcom/stripe/android/model/ShippingInformation;", "isValid", "", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: PaymentSessionConfig.kt */
    private static final class DefaultShippingInfoValidator implements ShippingInformationValidator {
        public String getErrorMessage(ShippingInformation shippingInformation) {
            Intrinsics.checkParameterIsNotNull(shippingInformation, "shippingInformation");
            return "";
        }

        public boolean isValid(ShippingInformation shippingInformation) {
            Intrinsics.checkParameterIsNotNull(shippingInformation, "shippingInformation");
            return true;
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\bf\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H'J\u0010\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u0004\u001a\u00020\u0005H'¨\u0006\b"}, d2 = {"Lcom/stripe/android/PaymentSessionConfig$ShippingInformationValidator;", "Ljava/io/Serializable;", "getErrorMessage", "", "shippingInformation", "Lcom/stripe/android/model/ShippingInformation;", "isValid", "", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: PaymentSessionConfig.kt */
    public interface ShippingInformationValidator extends Serializable {
        String getErrorMessage(ShippingInformation shippingInformation);

        boolean isValid(ShippingInformation shippingInformation);
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\bf\u0018\u00002\u00020\u0001J\u0016\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u00032\u0006\u0010\u0005\u001a\u00020\u0006H'¨\u0006\u0007"}, d2 = {"Lcom/stripe/android/PaymentSessionConfig$ShippingMethodsFactory;", "Ljava/io/Serializable;", "create", "", "Lcom/stripe/android/model/ShippingMethod;", "shippingInformation", "Lcom/stripe/android/model/ShippingInformation;", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: PaymentSessionConfig.kt */
    public interface ShippingMethodsFactory extends Serializable {
        List<ShippingMethod> create(ShippingInformation shippingInformation);
    }

    public PaymentSessionConfig() {
        this((List) null, (List) null, (ShippingInformation) null, false, false, 0, (List) null, false, (Set) null, (BillingAddressFields) null, false, false, (ShippingInformationValidator) null, (ShippingMethodsFactory) null, (Integer) null, 32767, (DefaultConstructorMarker) null);
    }

    public static /* synthetic */ PaymentSessionConfig copy$default(PaymentSessionConfig paymentSessionConfig, List list, List list2, ShippingInformation shippingInformation, boolean z, boolean z2, int i, List list3, boolean z3, Set set, BillingAddressFields billingAddressFields2, boolean z4, boolean z5, ShippingInformationValidator shippingInformationValidator2, ShippingMethodsFactory shippingMethodsFactory2, Integer num, int i2, Object obj) {
        PaymentSessionConfig paymentSessionConfig2 = paymentSessionConfig;
        int i3 = i2;
        return paymentSessionConfig.copy((i3 & 1) != 0 ? paymentSessionConfig2.hiddenShippingInfoFields : list, (i3 & 2) != 0 ? paymentSessionConfig2.optionalShippingInfoFields : list2, (i3 & 4) != 0 ? paymentSessionConfig2.prepopulatedShippingInfo : shippingInformation, (i3 & 8) != 0 ? paymentSessionConfig2.isShippingInfoRequired : z, (i3 & 16) != 0 ? paymentSessionConfig2.isShippingMethodRequired : z2, (i3 & 32) != 0 ? paymentSessionConfig2.addPaymentMethodFooterLayoutId : i, (i3 & 64) != 0 ? paymentSessionConfig2.paymentMethodTypes : list3, (i3 & 128) != 0 ? paymentSessionConfig2.shouldShowGooglePay : z3, (i3 & 256) != 0 ? paymentSessionConfig2.allowedShippingCountryCodes : set, (i3 & 512) != 0 ? paymentSessionConfig2.billingAddressFields : billingAddressFields2, (i3 & 1024) != 0 ? paymentSessionConfig2.canDeletePaymentMethods : z4, (i3 & 2048) != 0 ? paymentSessionConfig2.shouldPrefetchCustomer : z5, (i3 & 4096) != 0 ? paymentSessionConfig2.shippingInformationValidator : shippingInformationValidator2, (i3 & 8192) != 0 ? paymentSessionConfig2.shippingMethodsFactory : shippingMethodsFactory2, (i3 & 16384) != 0 ? paymentSessionConfig2.windowFlags : num);
    }

    public final List<String> component1() {
        return this.hiddenShippingInfoFields;
    }

    public final BillingAddressFields component10() {
        return this.billingAddressFields;
    }

    public final boolean component11() {
        return this.canDeletePaymentMethods;
    }

    public final boolean component12$stripe_release() {
        return this.shouldPrefetchCustomer;
    }

    public final ShippingInformationValidator component13$stripe_release() {
        return this.shippingInformationValidator;
    }

    public final ShippingMethodsFactory component14$stripe_release() {
        return this.shippingMethodsFactory;
    }

    public final Integer component15$stripe_release() {
        return this.windowFlags;
    }

    public final List<String> component2() {
        return this.optionalShippingInfoFields;
    }

    public final ShippingInformation component3() {
        return this.prepopulatedShippingInfo;
    }

    public final boolean component4() {
        return this.isShippingInfoRequired;
    }

    public final boolean component5() {
        return this.isShippingMethodRequired;
    }

    public final int component6() {
        return this.addPaymentMethodFooterLayoutId;
    }

    public final List<PaymentMethod.Type> component7() {
        return this.paymentMethodTypes;
    }

    public final boolean component8() {
        return this.shouldShowGooglePay;
    }

    public final Set<String> component9() {
        return this.allowedShippingCountryCodes;
    }

    public final PaymentSessionConfig copy(List<String> list, List<String> list2, ShippingInformation shippingInformation, boolean z, boolean z2, int i, List<? extends PaymentMethod.Type> list3, boolean z3, Set<String> set, BillingAddressFields billingAddressFields2, boolean z4, boolean z5, ShippingInformationValidator shippingInformationValidator2, ShippingMethodsFactory shippingMethodsFactory2, Integer num) {
        List<String> list4 = list;
        Intrinsics.checkParameterIsNotNull(list4, "hiddenShippingInfoFields");
        List<String> list5 = list2;
        Intrinsics.checkParameterIsNotNull(list5, "optionalShippingInfoFields");
        List<? extends PaymentMethod.Type> list6 = list3;
        Intrinsics.checkParameterIsNotNull(list6, "paymentMethodTypes");
        Set<String> set2 = set;
        Intrinsics.checkParameterIsNotNull(set2, "allowedShippingCountryCodes");
        BillingAddressFields billingAddressFields3 = billingAddressFields2;
        Intrinsics.checkParameterIsNotNull(billingAddressFields3, "billingAddressFields");
        ShippingInformationValidator shippingInformationValidator3 = shippingInformationValidator2;
        Intrinsics.checkParameterIsNotNull(shippingInformationValidator3, "shippingInformationValidator");
        return new PaymentSessionConfig(list4, list5, shippingInformation, z, z2, i, list6, z3, set2, billingAddressFields3, z4, z5, shippingInformationValidator3, shippingMethodsFactory2, num);
    }

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof PaymentSessionConfig)) {
            return false;
        }
        PaymentSessionConfig paymentSessionConfig = (PaymentSessionConfig) obj;
        return Intrinsics.areEqual((Object) this.hiddenShippingInfoFields, (Object) paymentSessionConfig.hiddenShippingInfoFields) && Intrinsics.areEqual((Object) this.optionalShippingInfoFields, (Object) paymentSessionConfig.optionalShippingInfoFields) && Intrinsics.areEqual((Object) this.prepopulatedShippingInfo, (Object) paymentSessionConfig.prepopulatedShippingInfo) && this.isShippingInfoRequired == paymentSessionConfig.isShippingInfoRequired && this.isShippingMethodRequired == paymentSessionConfig.isShippingMethodRequired && this.addPaymentMethodFooterLayoutId == paymentSessionConfig.addPaymentMethodFooterLayoutId && Intrinsics.areEqual((Object) this.paymentMethodTypes, (Object) paymentSessionConfig.paymentMethodTypes) && this.shouldShowGooglePay == paymentSessionConfig.shouldShowGooglePay && Intrinsics.areEqual((Object) this.allowedShippingCountryCodes, (Object) paymentSessionConfig.allowedShippingCountryCodes) && Intrinsics.areEqual((Object) this.billingAddressFields, (Object) paymentSessionConfig.billingAddressFields) && this.canDeletePaymentMethods == paymentSessionConfig.canDeletePaymentMethods && this.shouldPrefetchCustomer == paymentSessionConfig.shouldPrefetchCustomer && Intrinsics.areEqual((Object) this.shippingInformationValidator, (Object) paymentSessionConfig.shippingInformationValidator) && Intrinsics.areEqual((Object) this.shippingMethodsFactory, (Object) paymentSessionConfig.shippingMethodsFactory) && Intrinsics.areEqual((Object) this.windowFlags, (Object) paymentSessionConfig.windowFlags);
    }

    public int hashCode() {
        List<String> list = this.hiddenShippingInfoFields;
        int i = 0;
        int hashCode = (list != null ? list.hashCode() : 0) * 31;
        List<String> list2 = this.optionalShippingInfoFields;
        int hashCode2 = (hashCode + (list2 != null ? list2.hashCode() : 0)) * 31;
        ShippingInformation shippingInformation = this.prepopulatedShippingInfo;
        int hashCode3 = (hashCode2 + (shippingInformation != null ? shippingInformation.hashCode() : 0)) * 31;
        boolean z = this.isShippingInfoRequired;
        boolean z2 = true;
        if (z) {
            z = true;
        }
        int i2 = (hashCode3 + (z ? 1 : 0)) * 31;
        boolean z3 = this.isShippingMethodRequired;
        if (z3) {
            z3 = true;
        }
        int i3 = (((i2 + (z3 ? 1 : 0)) * 31) + this.addPaymentMethodFooterLayoutId) * 31;
        List<PaymentMethod.Type> list3 = this.paymentMethodTypes;
        int hashCode4 = (i3 + (list3 != null ? list3.hashCode() : 0)) * 31;
        boolean z4 = this.shouldShowGooglePay;
        if (z4) {
            z4 = true;
        }
        int i4 = (hashCode4 + (z4 ? 1 : 0)) * 31;
        Set<String> set = this.allowedShippingCountryCodes;
        int hashCode5 = (i4 + (set != null ? set.hashCode() : 0)) * 31;
        BillingAddressFields billingAddressFields2 = this.billingAddressFields;
        int hashCode6 = (hashCode5 + (billingAddressFields2 != null ? billingAddressFields2.hashCode() : 0)) * 31;
        boolean z5 = this.canDeletePaymentMethods;
        if (z5) {
            z5 = true;
        }
        int i5 = (hashCode6 + (z5 ? 1 : 0)) * 31;
        boolean z6 = this.shouldPrefetchCustomer;
        if (!z6) {
            z2 = z6;
        }
        int i6 = (i5 + (z2 ? 1 : 0)) * 31;
        ShippingInformationValidator shippingInformationValidator2 = this.shippingInformationValidator;
        int hashCode7 = (i6 + (shippingInformationValidator2 != null ? shippingInformationValidator2.hashCode() : 0)) * 31;
        ShippingMethodsFactory shippingMethodsFactory2 = this.shippingMethodsFactory;
        int hashCode8 = (hashCode7 + (shippingMethodsFactory2 != null ? shippingMethodsFactory2.hashCode() : 0)) * 31;
        Integer num = this.windowFlags;
        if (num != null) {
            i = num.hashCode();
        }
        return hashCode8 + i;
    }

    public String toString() {
        return "PaymentSessionConfig(hiddenShippingInfoFields=" + this.hiddenShippingInfoFields + ", optionalShippingInfoFields=" + this.optionalShippingInfoFields + ", prepopulatedShippingInfo=" + this.prepopulatedShippingInfo + ", isShippingInfoRequired=" + this.isShippingInfoRequired + ", isShippingMethodRequired=" + this.isShippingMethodRequired + ", addPaymentMethodFooterLayoutId=" + this.addPaymentMethodFooterLayoutId + ", paymentMethodTypes=" + this.paymentMethodTypes + ", shouldShowGooglePay=" + this.shouldShowGooglePay + ", allowedShippingCountryCodes=" + this.allowedShippingCountryCodes + ", billingAddressFields=" + this.billingAddressFields + ", canDeletePaymentMethods=" + this.canDeletePaymentMethods + ", shouldPrefetchCustomer=" + this.shouldPrefetchCustomer + ", shippingInformationValidator=" + this.shippingInformationValidator + ", shippingMethodsFactory=" + this.shippingMethodsFactory + ", windowFlags=" + this.windowFlags + ")";
    }

    public void writeToParcel(Parcel parcel, int i) {
        Intrinsics.checkParameterIsNotNull(parcel, "parcel");
        parcel.writeStringList(this.hiddenShippingInfoFields);
        parcel.writeStringList(this.optionalShippingInfoFields);
        ShippingInformation shippingInformation = this.prepopulatedShippingInfo;
        if (shippingInformation != null) {
            parcel.writeInt(1);
            shippingInformation.writeToParcel(parcel, 0);
        } else {
            parcel.writeInt(0);
        }
        parcel.writeInt(this.isShippingInfoRequired ? 1 : 0);
        parcel.writeInt(this.isShippingMethodRequired ? 1 : 0);
        parcel.writeInt(this.addPaymentMethodFooterLayoutId);
        List<PaymentMethod.Type> list = this.paymentMethodTypes;
        parcel.writeInt(list.size());
        for (PaymentMethod.Type name : list) {
            parcel.writeString(name.name());
        }
        parcel.writeInt(this.shouldShowGooglePay ? 1 : 0);
        Set<String> set = this.allowedShippingCountryCodes;
        parcel.writeInt(set.size());
        for (String writeString : set) {
            parcel.writeString(writeString);
        }
        parcel.writeString(this.billingAddressFields.name());
        parcel.writeInt(this.canDeletePaymentMethods ? 1 : 0);
        parcel.writeInt(this.shouldPrefetchCustomer ? 1 : 0);
        parcel.writeSerializable(this.shippingInformationValidator);
        parcel.writeSerializable(this.shippingMethodsFactory);
        Integer num = this.windowFlags;
        if (num != null) {
            parcel.writeInt(1);
            parcel.writeInt(num.intValue());
            return;
        }
        parcel.writeInt(0);
    }

    public PaymentSessionConfig(List<String> list, List<String> list2, ShippingInformation shippingInformation, boolean z, boolean z2, int i, List<? extends PaymentMethod.Type> list3, boolean z3, Set<String> set, BillingAddressFields billingAddressFields2, boolean z4, boolean z5, ShippingInformationValidator shippingInformationValidator2, ShippingMethodsFactory shippingMethodsFactory2, Integer num) {
        List<? extends PaymentMethod.Type> list4 = list3;
        Set<String> set2 = set;
        BillingAddressFields billingAddressFields3 = billingAddressFields2;
        ShippingInformationValidator shippingInformationValidator3 = shippingInformationValidator2;
        Intrinsics.checkParameterIsNotNull(list, "hiddenShippingInfoFields");
        Intrinsics.checkParameterIsNotNull(list2, "optionalShippingInfoFields");
        Intrinsics.checkParameterIsNotNull(list4, "paymentMethodTypes");
        Intrinsics.checkParameterIsNotNull(set2, "allowedShippingCountryCodes");
        Intrinsics.checkParameterIsNotNull(billingAddressFields3, "billingAddressFields");
        Intrinsics.checkParameterIsNotNull(shippingInformationValidator3, "shippingInformationValidator");
        this.hiddenShippingInfoFields = list;
        this.optionalShippingInfoFields = list2;
        this.prepopulatedShippingInfo = shippingInformation;
        this.isShippingInfoRequired = z;
        this.isShippingMethodRequired = z2;
        this.addPaymentMethodFooterLayoutId = i;
        this.paymentMethodTypes = list4;
        this.shouldShowGooglePay = z3;
        this.allowedShippingCountryCodes = set2;
        this.billingAddressFields = billingAddressFields3;
        this.canDeletePaymentMethods = z4;
        this.shouldPrefetchCustomer = z5;
        this.shippingInformationValidator = shippingInformationValidator3;
        this.shippingMethodsFactory = shippingMethodsFactory2;
        this.windowFlags = num;
        String[] iSOCountries = Locale.getISOCountries();
        for (String str : this.allowedShippingCountryCodes) {
            Intrinsics.checkExpressionValueIsNotNull(iSOCountries, "countryCodes");
            int length = iSOCountries.length;
            boolean z6 = false;
            int i2 = 0;
            while (true) {
                if (i2 >= length) {
                    break;
                } else if (StringsKt.equals(str, iSOCountries[i2], true)) {
                    z6 = true;
                    continue;
                    break;
                } else {
                    i2++;
                }
            }
            if (!z6) {
                throw new IllegalArgumentException(('\'' + str + "' is not a valid country code").toString());
            }
        }
        if (this.isShippingMethodRequired && this.shippingMethodsFactory == null) {
            throw new IllegalArgumentException("If isShippingMethodRequired is true a ShippingMethodsFactory must also be provided.".toString());
        }
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* synthetic */ PaymentSessionConfig(java.util.List r17, java.util.List r18, com.stripe.android.model.ShippingInformation r19, boolean r20, boolean r21, int r22, java.util.List r23, boolean r24, java.util.Set r25, com.stripe.android.view.BillingAddressFields r26, boolean r27, boolean r28, com.stripe.android.PaymentSessionConfig.ShippingInformationValidator r29, com.stripe.android.PaymentSessionConfig.ShippingMethodsFactory r30, java.lang.Integer r31, int r32, kotlin.jvm.internal.DefaultConstructorMarker r33) {
        /*
            r16 = this;
            r0 = r32
            r1 = r0 & 1
            if (r1 == 0) goto L_0x000b
            java.util.List r1 = kotlin.collections.CollectionsKt.emptyList()
            goto L_0x000d
        L_0x000b:
            r1 = r17
        L_0x000d:
            r2 = r0 & 2
            if (r2 == 0) goto L_0x0016
            java.util.List r2 = kotlin.collections.CollectionsKt.emptyList()
            goto L_0x0018
        L_0x0016:
            r2 = r18
        L_0x0018:
            r3 = r0 & 4
            r4 = 0
            if (r3 == 0) goto L_0x0021
            r3 = r4
            com.stripe.android.model.ShippingInformation r3 = (com.stripe.android.model.ShippingInformation) r3
            goto L_0x0023
        L_0x0021:
            r3 = r19
        L_0x0023:
            r5 = r0 & 8
            r6 = 0
            if (r5 == 0) goto L_0x002a
            r5 = 0
            goto L_0x002c
        L_0x002a:
            r5 = r20
        L_0x002c:
            r7 = r0 & 16
            if (r7 == 0) goto L_0x0032
            r7 = 0
            goto L_0x0034
        L_0x0032:
            r7 = r21
        L_0x0034:
            r8 = r0 & 32
            if (r8 == 0) goto L_0x003a
            r8 = 0
            goto L_0x003c
        L_0x003a:
            r8 = r22
        L_0x003c:
            r9 = r0 & 64
            if (r9 == 0) goto L_0x0047
            com.stripe.android.model.PaymentMethod$Type r9 = com.stripe.android.model.PaymentMethod.Type.Card
            java.util.List r9 = kotlin.collections.CollectionsKt.listOf(r9)
            goto L_0x0049
        L_0x0047:
            r9 = r23
        L_0x0049:
            r10 = r0 & 128(0x80, float:1.794E-43)
            if (r10 == 0) goto L_0x004e
            goto L_0x0050
        L_0x004e:
            r6 = r24
        L_0x0050:
            r10 = r0 & 256(0x100, float:3.59E-43)
            if (r10 == 0) goto L_0x0059
            java.util.Set r10 = kotlin.collections.SetsKt.emptySet()
            goto L_0x005b
        L_0x0059:
            r10 = r25
        L_0x005b:
            r11 = r0 & 512(0x200, float:7.175E-43)
            if (r11 == 0) goto L_0x0062
            com.stripe.android.view.BillingAddressFields r11 = DEFAULT_BILLING_ADDRESS_FIELDS
            goto L_0x0064
        L_0x0062:
            r11 = r26
        L_0x0064:
            r12 = r0 & 1024(0x400, float:1.435E-42)
            r13 = 1
            if (r12 == 0) goto L_0x006b
            r12 = 1
            goto L_0x006d
        L_0x006b:
            r12 = r27
        L_0x006d:
            r14 = r0 & 2048(0x800, float:2.87E-42)
            if (r14 == 0) goto L_0x0072
            goto L_0x0074
        L_0x0072:
            r13 = r28
        L_0x0074:
            r14 = r0 & 4096(0x1000, float:5.74E-42)
            if (r14 == 0) goto L_0x0080
            com.stripe.android.PaymentSessionConfig$DefaultShippingInfoValidator r14 = new com.stripe.android.PaymentSessionConfig$DefaultShippingInfoValidator
            r14.<init>()
            com.stripe.android.PaymentSessionConfig$ShippingInformationValidator r14 = (com.stripe.android.PaymentSessionConfig.ShippingInformationValidator) r14
            goto L_0x0082
        L_0x0080:
            r14 = r29
        L_0x0082:
            r15 = r0 & 8192(0x2000, float:1.14794E-41)
            if (r15 == 0) goto L_0x008a
            r15 = r4
            com.stripe.android.PaymentSessionConfig$ShippingMethodsFactory r15 = (com.stripe.android.PaymentSessionConfig.ShippingMethodsFactory) r15
            goto L_0x008c
        L_0x008a:
            r15 = r30
        L_0x008c:
            r0 = r0 & 16384(0x4000, float:2.2959E-41)
            if (r0 == 0) goto L_0x0094
            r0 = r4
            java.lang.Integer r0 = (java.lang.Integer) r0
            goto L_0x0096
        L_0x0094:
            r0 = r31
        L_0x0096:
            r17 = r16
            r18 = r1
            r19 = r2
            r20 = r3
            r21 = r5
            r22 = r7
            r23 = r8
            r24 = r9
            r25 = r6
            r26 = r10
            r27 = r11
            r28 = r12
            r29 = r13
            r30 = r14
            r31 = r15
            r32 = r0
            r17.<init>(r18, r19, r20, r21, r22, r23, r24, r25, r26, r27, r28, r29, r30, r31, r32)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.stripe.android.PaymentSessionConfig.<init>(java.util.List, java.util.List, com.stripe.android.model.ShippingInformation, boolean, boolean, int, java.util.List, boolean, java.util.Set, com.stripe.android.view.BillingAddressFields, boolean, boolean, com.stripe.android.PaymentSessionConfig$ShippingInformationValidator, com.stripe.android.PaymentSessionConfig$ShippingMethodsFactory, java.lang.Integer, int, kotlin.jvm.internal.DefaultConstructorMarker):void");
    }

    public final List<String> getHiddenShippingInfoFields() {
        return this.hiddenShippingInfoFields;
    }

    public final List<String> getOptionalShippingInfoFields() {
        return this.optionalShippingInfoFields;
    }

    public final ShippingInformation getPrepopulatedShippingInfo() {
        return this.prepopulatedShippingInfo;
    }

    public final boolean isShippingInfoRequired() {
        return this.isShippingInfoRequired;
    }

    public final boolean isShippingMethodRequired() {
        return this.isShippingMethodRequired;
    }

    public final int getAddPaymentMethodFooterLayoutId() {
        return this.addPaymentMethodFooterLayoutId;
    }

    public final List<PaymentMethod.Type> getPaymentMethodTypes() {
        return this.paymentMethodTypes;
    }

    public final boolean getShouldShowGooglePay() {
        return this.shouldShowGooglePay;
    }

    public final Set<String> getAllowedShippingCountryCodes() {
        return this.allowedShippingCountryCodes;
    }

    public final BillingAddressFields getBillingAddressFields() {
        return this.billingAddressFields;
    }

    public final boolean getCanDeletePaymentMethods() {
        return this.canDeletePaymentMethods;
    }

    public final boolean getShouldPrefetchCustomer$stripe_release() {
        return this.shouldPrefetchCustomer;
    }

    public final ShippingInformationValidator getShippingInformationValidator$stripe_release() {
        return this.shippingInformationValidator;
    }

    public final ShippingMethodsFactory getShippingMethodsFactory$stripe_release() {
        return this.shippingMethodsFactory;
    }

    public final Integer getWindowFlags$stripe_release() {
        return this.windowFlags;
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000X\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\"\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010 \n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u000b\n\u0002\u0010\u0011\n\u0002\b\u000e\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0003J\b\u0010\u001e\u001a\u00020\u0002H\u0016J\u0010\u0010\u001f\u001a\u00020\u00002\b\b\u0001\u0010\u0004\u001a\u00020\u0005J\u0014\u0010 \u001a\u00020\u00002\f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\b0\u0007J\u000e\u0010!\u001a\u00020\u00002\u0006\u0010\t\u001a\u00020\nJ\u000e\u0010\"\u001a\u00020\u00002\u0006\u0010\u000b\u001a\u00020\fJ!\u0010#\u001a\u00020\u00002\u0012\u0010\r\u001a\n\u0012\u0006\b\u0001\u0012\u00020\b0$\"\u00020\bH\u0007¢\u0006\u0002\u0010%J!\u0010&\u001a\u00020\u00002\u0012\u0010\u000f\u001a\n\u0012\u0006\b\u0001\u0012\u00020\b0$\"\u00020\bH\u0007¢\u0006\u0002\u0010%J\u0014\u0010'\u001a\u00020\u00002\f\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u00110\u000eJ\u0010\u0010(\u001a\u00020\u00002\b\u0010)\u001a\u0004\u0018\u00010\u0014J\u000e\u0010*\u001a\u00020\u00002\u0006\u0010\u0012\u001a\u00020\fJ\u0010\u0010+\u001a\u00020\u00002\b\u0010\u0015\u001a\u0004\u0018\u00010\u0016J\u0010\u0010,\u001a\u00020\u00002\b\u0010\u0017\u001a\u0004\u0018\u00010\u0018J\u000e\u0010-\u001a\u00020\u00002\u0006\u0010\u0019\u001a\u00020\fJ\u000e\u0010.\u001a\u00020\u00002\u0006\u0010\u001a\u001a\u00020\fJ\u000e\u0010/\u001a\u00020\u00002\u0006\u0010\u001b\u001a\u00020\fJ\u0015\u00100\u001a\u00020\u00002\b\u0010\u001c\u001a\u0004\u0018\u00010\u0005¢\u0006\u0002\u00101R\u0012\u0010\u0004\u001a\u00020\u00058\u0002@\u0002X\u000e¢\u0006\u0002\n\u0000R\u0014\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\b0\u0007X\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\fX\u000e¢\u0006\u0002\n\u0000R\u0016\u0010\r\u001a\n\u0012\u0004\u0012\u00020\b\u0018\u00010\u000eX\u000e¢\u0006\u0002\n\u0000R\u0016\u0010\u000f\u001a\n\u0012\u0004\u0012\u00020\b\u0018\u00010\u000eX\u000e¢\u0006\u0002\n\u0000R\u0014\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u00110\u000eX\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\fX\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\u0013\u001a\u0004\u0018\u00010\u0014X\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\u0015\u001a\u0004\u0018\u00010\u0016X\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\u0017\u001a\u0004\u0018\u00010\u0018X\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0019\u001a\u00020\fX\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u001a\u001a\u00020\fX\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u001b\u001a\u00020\fX\u000e¢\u0006\u0002\n\u0000R\u0012\u0010\u001c\u001a\u0004\u0018\u00010\u0005X\u000e¢\u0006\u0004\n\u0002\u0010\u001d¨\u00062"}, d2 = {"Lcom/stripe/android/PaymentSessionConfig$Builder;", "Lcom/stripe/android/ObjectBuilder;", "Lcom/stripe/android/PaymentSessionConfig;", "()V", "addPaymentMethodFooterLayoutId", "", "allowedShippingCountryCodes", "", "", "billingAddressFields", "Lcom/stripe/android/view/BillingAddressFields;", "canDeletePaymentMethods", "", "hiddenShippingInfoFields", "", "optionalShippingInfoFields", "paymentMethodTypes", "Lcom/stripe/android/model/PaymentMethod$Type;", "shippingInfoRequired", "shippingInformation", "Lcom/stripe/android/model/ShippingInformation;", "shippingInformationValidator", "Lcom/stripe/android/PaymentSessionConfig$ShippingInformationValidator;", "shippingMethodsFactory", "Lcom/stripe/android/PaymentSessionConfig$ShippingMethodsFactory;", "shippingMethodsRequired", "shouldPrefetchCustomer", "shouldShowGooglePay", "windowFlags", "Ljava/lang/Integer;", "build", "setAddPaymentMethodFooter", "setAllowedShippingCountryCodes", "setBillingAddressFields", "setCanDeletePaymentMethods", "setHiddenShippingInfoFields", "", "([Ljava/lang/String;)Lcom/stripe/android/PaymentSessionConfig$Builder;", "setOptionalShippingInfoFields", "setPaymentMethodTypes", "setPrepopulatedShippingInfo", "shippingInfo", "setShippingInfoRequired", "setShippingInformationValidator", "setShippingMethodsFactory", "setShippingMethodsRequired", "setShouldPrefetchCustomer", "setShouldShowGooglePay", "setWindowFlags", "(Ljava/lang/Integer;)Lcom/stripe/android/PaymentSessionConfig$Builder;", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: PaymentSessionConfig.kt */
    public static final class Builder implements ObjectBuilder<PaymentSessionConfig> {
        private int addPaymentMethodFooterLayoutId;
        private Set<String> allowedShippingCountryCodes = SetsKt.emptySet();
        private BillingAddressFields billingAddressFields = PaymentSessionConfig.DEFAULT_BILLING_ADDRESS_FIELDS;
        private boolean canDeletePaymentMethods = true;
        private List<String> hiddenShippingInfoFields;
        private List<String> optionalShippingInfoFields;
        private List<? extends PaymentMethod.Type> paymentMethodTypes = CollectionsKt.listOf(PaymentMethod.Type.Card);
        private boolean shippingInfoRequired = true;
        private ShippingInformation shippingInformation;
        private ShippingInformationValidator shippingInformationValidator;
        private ShippingMethodsFactory shippingMethodsFactory;
        private boolean shippingMethodsRequired = true;
        private boolean shouldPrefetchCustomer = true;
        private boolean shouldShowGooglePay;
        private Integer windowFlags;

        public Builder() {
            Companion unused = PaymentSessionConfig.Companion;
        }

        public final Builder setBillingAddressFields(BillingAddressFields billingAddressFields2) {
            Intrinsics.checkParameterIsNotNull(billingAddressFields2, "billingAddressFields");
            Builder builder = this;
            builder.billingAddressFields = billingAddressFields2;
            return builder;
        }

        public final Builder setHiddenShippingInfoFields(String... strArr) {
            Intrinsics.checkParameterIsNotNull(strArr, "hiddenShippingInfoFields");
            Builder builder = this;
            builder.hiddenShippingInfoFields = CollectionsKt.listOf((String[]) Arrays.copyOf(strArr, strArr.length));
            return builder;
        }

        public final Builder setOptionalShippingInfoFields(String... strArr) {
            Intrinsics.checkParameterIsNotNull(strArr, "optionalShippingInfoFields");
            Builder builder = this;
            builder.optionalShippingInfoFields = CollectionsKt.listOf((String[]) Arrays.copyOf(strArr, strArr.length));
            return builder;
        }

        public final Builder setPrepopulatedShippingInfo(ShippingInformation shippingInformation2) {
            Builder builder = this;
            builder.shippingInformation = shippingInformation2;
            return builder;
        }

        public final Builder setShippingInfoRequired(boolean z) {
            Builder builder = this;
            builder.shippingInfoRequired = z;
            return builder;
        }

        public final Builder setShippingMethodsRequired(boolean z) {
            Builder builder = this;
            builder.shippingMethodsRequired = z;
            return builder;
        }

        public final Builder setAddPaymentMethodFooter(int i) {
            Builder builder = this;
            builder.addPaymentMethodFooterLayoutId = i;
            return builder;
        }

        public final Builder setPaymentMethodTypes(List<? extends PaymentMethod.Type> list) {
            Intrinsics.checkParameterIsNotNull(list, "paymentMethodTypes");
            Builder builder = this;
            builder.paymentMethodTypes = list;
            return builder;
        }

        public final Builder setShouldShowGooglePay(boolean z) {
            Builder builder = this;
            builder.shouldShowGooglePay = z;
            return builder;
        }

        public final Builder setCanDeletePaymentMethods(boolean z) {
            Builder builder = this;
            builder.canDeletePaymentMethods = z;
            return builder;
        }

        public final Builder setAllowedShippingCountryCodes(Set<String> set) {
            Intrinsics.checkParameterIsNotNull(set, "allowedShippingCountryCodes");
            Builder builder = this;
            builder.allowedShippingCountryCodes = set;
            return builder;
        }

        public final Builder setWindowFlags(Integer num) {
            Builder builder = this;
            builder.windowFlags = num;
            return builder;
        }

        public final Builder setShippingInformationValidator(ShippingInformationValidator shippingInformationValidator2) {
            Builder builder = this;
            builder.shippingInformationValidator = shippingInformationValidator2;
            return builder;
        }

        public final Builder setShippingMethodsFactory(ShippingMethodsFactory shippingMethodsFactory2) {
            Builder builder = this;
            builder.shippingMethodsFactory = shippingMethodsFactory2;
            return builder;
        }

        public final Builder setShouldPrefetchCustomer(boolean z) {
            Builder builder = this;
            builder.shouldPrefetchCustomer = z;
            return builder;
        }

        public PaymentSessionConfig build() {
            List<String> list = this.hiddenShippingInfoFields;
            if (list == null) {
                list = CollectionsKt.emptyList();
            }
            List<String> list2 = list;
            List<String> list3 = this.optionalShippingInfoFields;
            if (list3 == null) {
                list3 = CollectionsKt.emptyList();
            }
            List<String> list4 = list3;
            ShippingInformation shippingInformation2 = this.shippingInformation;
            boolean z = this.shippingInfoRequired;
            boolean z2 = this.shippingMethodsRequired;
            int i = this.addPaymentMethodFooterLayoutId;
            List<? extends PaymentMethod.Type> list5 = this.paymentMethodTypes;
            boolean z3 = this.shouldShowGooglePay;
            Set<String> set = this.allowedShippingCountryCodes;
            ShippingInformationValidator shippingInformationValidator2 = this.shippingInformationValidator;
            if (shippingInformationValidator2 == null) {
                shippingInformationValidator2 = new DefaultShippingInfoValidator();
            }
            ShippingInformationValidator shippingInformationValidator3 = shippingInformationValidator2;
            ShippingMethodsFactory shippingMethodsFactory2 = this.shippingMethodsFactory;
            Integer num = this.windowFlags;
            return new PaymentSessionConfig(list2, list4, shippingInformation2, z, z2, i, list5, z3, set, this.billingAddressFields, this.canDeletePaymentMethods, this.shouldPrefetchCustomer, shippingInformationValidator3, shippingMethodsFactory2, num);
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0005"}, d2 = {"Lcom/stripe/android/PaymentSessionConfig$Companion;", "", "()V", "DEFAULT_BILLING_ADDRESS_FIELDS", "Lcom/stripe/android/view/BillingAddressFields;", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: PaymentSessionConfig.kt */
    private static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }
}
