package com.stripe.android;

import com.stripe.android.exception.APIException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
import kotlin.collections.MapsKt;
import kotlin.jvm.internal.Intrinsics;
import kotlin.text.StringsKt;
import org.json.JSONException;
import org.json.JSONObject;

@Metadata(bv = {1, 0, 3}, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010$\n\u0002\u0010 \n\u0002\b\t\n\u0002\u0010\u000b\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0012\b\b\u0018\u0000 +2\u00020\u0001:\u0001+B5\b\u0000\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\b\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\u001a\b\u0002\u0010\u0006\u001a\u0014\u0012\u0004\u0012\u00020\u0005\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00050\b0\u0007¢\u0006\u0002\u0010\tJ\u000e\u0010\u001d\u001a\u00020\u0003HÀ\u0003¢\u0006\u0002\b\u001eJ\u0010\u0010\u001f\u001a\u0004\u0018\u00010\u0005HÀ\u0003¢\u0006\u0002\b J \u0010!\u001a\u0014\u0012\u0004\u0012\u00020\u0005\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00050\b0\u0007HÀ\u0003¢\u0006\u0002\b\"J;\u0010#\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u00052\u001a\b\u0002\u0010\u0006\u001a\u0014\u0012\u0004\u0012\u00020\u0005\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00050\b0\u0007HÆ\u0001J\u0013\u0010$\u001a\u00020\u00122\b\u0010%\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\u001d\u0010&\u001a\n\u0012\u0004\u0012\u00020\u0005\u0018\u00010\b2\u0006\u0010'\u001a\u00020\u0005H\u0000¢\u0006\u0002\b(J\t\u0010)\u001a\u00020\u0003HÖ\u0001J\b\u0010*\u001a\u00020\u0005H\u0016R\u0016\u0010\u0004\u001a\u0004\u0018\u00010\u0005X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u0014\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\rR\u0010\u0010\u000e\u001a\u0004\u0018\u00010\u0005X\u0004¢\u0006\u0002\n\u0000R&\u0010\u0006\u001a\u0014\u0012\u0004\u0012\u00020\u0005\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00050\b0\u0007X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010R\u0014\u0010\u0011\u001a\u00020\u0012X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u0014R\u0014\u0010\u0015\u001a\u00020\u0012X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0016\u0010\u0014R\u0016\u0010\u0017\u001a\u0004\u0018\u00010\u0005X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0018\u0010\u000bR\u0014\u0010\u0019\u001a\u00020\u001a8@X\u0004¢\u0006\u0006\u001a\u0004\b\u001b\u0010\u001c¨\u0006,"}, d2 = {"Lcom/stripe/android/StripeResponse;", "", "code", "", "body", "", "headers", "", "", "(ILjava/lang/String;Ljava/util/Map;)V", "getBody$stripe_release", "()Ljava/lang/String;", "getCode$stripe_release", "()I", "contentType", "getHeaders$stripe_release", "()Ljava/util/Map;", "isError", "", "isError$stripe_release", "()Z", "isOk", "isOk$stripe_release", "requestId", "getRequestId$stripe_release", "responseJson", "Lorg/json/JSONObject;", "getResponseJson$stripe_release", "()Lorg/json/JSONObject;", "component1", "component1$stripe_release", "component2", "component2$stripe_release", "component3", "component3$stripe_release", "copy", "equals", "other", "getHeaderValue", "key", "getHeaderValue$stripe_release", "hashCode", "toString", "Companion", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: StripeResponse.kt */
public final class StripeResponse {
    private static final String CONTENT_TYPE_HEADER = "Content-Type";
    @Deprecated
    public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
    private static final String REQUEST_ID_HEADER = "Request-Id";
    private final String body;
    private final int code;
    private final String contentType;
    private final Map<String, List<String>> headers;
    private final boolean isError;
    private final boolean isOk;
    private final String requestId;

    public static /* synthetic */ StripeResponse copy$default(StripeResponse stripeResponse, int i, String str, Map<String, List<String>> map, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            i = stripeResponse.code;
        }
        if ((i2 & 2) != 0) {
            str = stripeResponse.body;
        }
        if ((i2 & 4) != 0) {
            map = stripeResponse.headers;
        }
        return stripeResponse.copy(i, str, map);
    }

    public final int component1$stripe_release() {
        return this.code;
    }

    public final String component2$stripe_release() {
        return this.body;
    }

    public final Map<String, List<String>> component3$stripe_release() {
        return this.headers;
    }

    public final StripeResponse copy(int i, String str, Map<String, ? extends List<String>> map) {
        Intrinsics.checkParameterIsNotNull(map, "headers");
        return new StripeResponse(i, str, map);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof StripeResponse)) {
            return false;
        }
        StripeResponse stripeResponse = (StripeResponse) obj;
        return this.code == stripeResponse.code && Intrinsics.areEqual((Object) this.body, (Object) stripeResponse.body) && Intrinsics.areEqual((Object) this.headers, (Object) stripeResponse.headers);
    }

    public int hashCode() {
        int i = this.code * 31;
        String str = this.body;
        int i2 = 0;
        int hashCode = (i + (str != null ? str.hashCode() : 0)) * 31;
        Map<String, List<String>> map = this.headers;
        if (map != null) {
            i2 = map.hashCode();
        }
        return hashCode + i2;
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v8, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v5, resolved type: java.lang.String} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public StripeResponse(int r2, java.lang.String r3, java.util.Map<java.lang.String, ? extends java.util.List<java.lang.String>> r4) {
        /*
            r1 = this;
            java.lang.String r0 = "headers"
            kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(r4, r0)
            r1.<init>()
            r1.code = r2
            r1.body = r3
            r1.headers = r4
            r3 = 1
            r4 = 0
            r0 = 200(0xc8, float:2.8E-43)
            if (r2 != r0) goto L_0x0016
            r2 = 1
            goto L_0x0017
        L_0x0016:
            r2 = 0
        L_0x0017:
            r1.isOk = r2
            int r2 = r1.code
            if (r2 < r0) goto L_0x0023
            r0 = 300(0x12c, float:4.2E-43)
            if (r2 < r0) goto L_0x0022
            goto L_0x0023
        L_0x0022:
            r3 = 0
        L_0x0023:
            r1.isError = r3
            java.lang.String r2 = "Request-Id"
            java.util.List r2 = r1.getHeaderValue$stripe_release(r2)
            r3 = 0
            if (r2 == 0) goto L_0x0035
            java.lang.Object r2 = kotlin.collections.CollectionsKt.firstOrNull(r2)
            java.lang.String r2 = (java.lang.String) r2
            goto L_0x0036
        L_0x0035:
            r2 = r3
        L_0x0036:
            r1.requestId = r2
            java.lang.String r2 = "Content-Type"
            java.util.List r2 = r1.getHeaderValue$stripe_release(r2)
            if (r2 == 0) goto L_0x0047
            java.lang.Object r2 = kotlin.collections.CollectionsKt.firstOrNull(r2)
            r3 = r2
            java.lang.String r3 = (java.lang.String) r3
        L_0x0047:
            r1.contentType = r3
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.stripe.android.StripeResponse.<init>(int, java.lang.String, java.util.Map):void");
    }

    public final int getCode$stripe_release() {
        return this.code;
    }

    public final String getBody$stripe_release() {
        return this.body;
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ StripeResponse(int i, String str, Map map, int i2, DefaultConstructorMarker defaultConstructorMarker) {
        this(i, str, (i2 & 4) != 0 ? MapsKt.emptyMap() : map);
    }

    public final Map<String, List<String>> getHeaders$stripe_release() {
        return this.headers;
    }

    public final boolean isOk$stripe_release() {
        return this.isOk;
    }

    public final boolean isError$stripe_release() {
        return this.isError;
    }

    public final String getRequestId$stripe_release() {
        return this.requestId;
    }

    public final JSONObject getResponseJson$stripe_release() throws APIException {
        String str = this.body;
        if (str == null) {
            return new JSONObject();
        }
        try {
            return new JSONObject(str);
        } catch (JSONException e) {
            throw new APIException((StripeError) null, (String) null, 0, StringsKt.trimIndent("\n                            Exception while parsing response body.\n                              Status code: " + this.code + "\n                              Request-Id: " + this.requestId + "\n                              Content-Type: " + this.contentType + "\n                              Body: \"" + str + "\"\n                        "), e, 7, (DefaultConstructorMarker) null);
        }
    }

    public String toString() {
        return "Request-Id: " + this.requestId + ", Status Code: " + this.code;
    }

    public final List<String> getHeaderValue$stripe_release(String str) {
        Object obj;
        Intrinsics.checkParameterIsNotNull(str, "key");
        Iterator it = this.headers.entrySet().iterator();
        while (true) {
            if (!it.hasNext()) {
                obj = null;
                break;
            }
            obj = it.next();
            if (StringsKt.equals((String) ((Map.Entry) obj).getKey(), str, true)) {
                break;
            }
        }
        Map.Entry entry = (Map.Entry) obj;
        if (entry != null) {
            return (List) entry.getValue();
        }
        return null;
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\u0006"}, d2 = {"Lcom/stripe/android/StripeResponse$Companion;", "", "()V", "CONTENT_TYPE_HEADER", "", "REQUEST_ID_HEADER", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: StripeResponse.kt */
    private static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }
}
