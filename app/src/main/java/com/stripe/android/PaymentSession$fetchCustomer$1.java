package com.stripe.android;

import androidx.lifecycle.Observer;
import com.stripe.android.PaymentSession;
import com.stripe.android.PaymentSessionViewModel;
import kotlin.Metadata;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n¢\u0006\u0002\b\u0005"}, d2 = {"<anonymous>", "", "it", "Lcom/stripe/android/PaymentSessionViewModel$FetchCustomerResult;", "kotlin.jvm.PlatformType", "onChanged"}, k = 3, mv = {1, 1, 16})
/* compiled from: PaymentSession.kt */
final class PaymentSession$fetchCustomer$1<T> implements Observer<PaymentSessionViewModel.FetchCustomerResult> {
    final /* synthetic */ PaymentSession this$0;

    PaymentSession$fetchCustomer$1(PaymentSession paymentSession) {
        this.this$0 = paymentSession;
    }

    public final void onChanged(PaymentSessionViewModel.FetchCustomerResult fetchCustomerResult) {
        PaymentSession.PaymentSessionListener listener$stripe_release;
        if ((fetchCustomerResult instanceof PaymentSessionViewModel.FetchCustomerResult.Error) && (listener$stripe_release = this.this$0.getListener$stripe_release()) != null) {
            PaymentSessionViewModel.FetchCustomerResult.Error error = (PaymentSessionViewModel.FetchCustomerResult.Error) fetchCustomerResult;
            listener$stripe_release.onError(error.getErrorCode(), error.getErrorMessage());
        }
    }
}
