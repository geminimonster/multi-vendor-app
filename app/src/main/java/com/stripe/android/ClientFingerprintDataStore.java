package com.stripe.android;

import android.content.Context;
import android.content.SharedPreferences;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\b`\u0018\u00002\u00020\u0001:\u0001\u0005J\b\u0010\u0002\u001a\u00020\u0003H&J\b\u0010\u0004\u001a\u00020\u0003H&¨\u0006\u0006"}, d2 = {"Lcom/stripe/android/ClientFingerprintDataStore;", "", "getMuid", "", "getSid", "Default", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: ClientFingerprintDataStore.kt */
public interface ClientFingerprintDataStore {
    String getMuid();

    String getSid();

    @Metadata(bv = {1, 0, 3}, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\u0002\u0018\u0000 \u00122\u00020\u0001:\u0001\u0012B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u000e\b\u0002\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005¢\u0006\u0002\u0010\u0007J\b\u0010\u000b\u001a\u00020\fH\u0002J\b\u0010\r\u001a\u00020\fH\u0002J\b\u0010\u000e\u001a\u00020\fH\u0016J\b\u0010\u000f\u001a\u00020\fH\u0016J\b\u0010\u0010\u001a\u00020\u0011H\u0002R\u0016\u0010\b\u001a\n \n*\u0004\u0018\u00010\t0\tX\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0013"}, d2 = {"Lcom/stripe/android/ClientFingerprintDataStore$Default;", "Lcom/stripe/android/ClientFingerprintDataStore;", "context", "Landroid/content/Context;", "timestampSupplier", "Lkotlin/Function0;", "", "(Landroid/content/Context;Lkotlin/jvm/functions/Function0;)V", "prefs", "Landroid/content/SharedPreferences;", "kotlin.jvm.PlatformType", "createMuid", "", "createSid", "getMuid", "getSid", "isSidExpired", "", "Companion", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: ClientFingerprintDataStore.kt */
    public static final class Default implements ClientFingerprintDataStore {
        @Deprecated
        public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
        private static final String KEY_MUID = "muid";
        private static final String KEY_SID = "sid";
        private static final String KEY_SID_TIMESTAMP = "sid_timestamp";
        private static final String PREF_NAME = "client_fingerprint_data";
        private static final long SID_TTL = TimeUnit.MINUTES.toMillis(30);
        private final SharedPreferences prefs;
        private final Function0<Long> timestampSupplier;

        public Default(Context context, Function0<Long> function0) {
            Intrinsics.checkParameterIsNotNull(context, "context");
            Intrinsics.checkParameterIsNotNull(function0, "timestampSupplier");
            this.timestampSupplier = function0;
            this.prefs = context.getSharedPreferences(PREF_NAME, 0);
        }

        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ Default(Context context, Function0 function0, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this(context, (i & 2) != 0 ? AnonymousClass1.INSTANCE : function0);
        }

        public String getMuid() {
            String string = this.prefs.getString(KEY_MUID, (String) null);
            return string != null ? string : createMuid();
        }

        public String getSid() {
            String str = null;
            String string = this.prefs.getString(KEY_SID, (String) null);
            if (!isSidExpired()) {
                str = string;
            }
            return str != null ? str : createSid();
        }

        private final boolean isSidExpired() {
            return this.timestampSupplier.invoke().longValue() - this.prefs.getLong(KEY_SID_TIMESTAMP, 0) > SID_TTL;
        }

        private final String createSid() {
            String uuid = UUID.randomUUID().toString();
            this.prefs.edit().putString(KEY_SID, uuid).putLong(KEY_SID_TIMESTAMP, this.timestampSupplier.invoke().longValue()).apply();
            Intrinsics.checkExpressionValueIsNotNull(uuid, "UUID.randomUUID().toStri…   .apply()\n            }");
            return uuid;
        }

        private final String createMuid() {
            String uuid = UUID.randomUUID().toString();
            this.prefs.edit().putString(KEY_MUID, uuid).apply();
            Intrinsics.checkExpressionValueIsNotNull(uuid, "UUID.randomUUID().toStri…   .apply()\n            }");
            return uuid;
        }

        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0010\t\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0004¢\u0006\u0002\n\u0000¨\u0006\n"}, d2 = {"Lcom/stripe/android/ClientFingerprintDataStore$Default$Companion;", "", "()V", "KEY_MUID", "", "KEY_SID", "KEY_SID_TIMESTAMP", "PREF_NAME", "SID_TTL", "", "stripe_release"}, k = 1, mv = {1, 1, 16})
        /* compiled from: ClientFingerprintDataStore.kt */
        private static final class Companion {
            private Companion() {
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }
        }
    }
}
