package com.stripe.android;

import android.content.Context;
import android.util.Log;
import com.google.android.gms.common.internal.ServiceSpecificExtraArgs;
import com.stripe.android.EphemeralOperation;
import com.stripe.android.exception.InvalidRequestException;
import java.util.LinkedHashMap;
import java.util.Map;
import kotlin.Metadata;
import kotlin.Result;
import kotlin.ResultKt;
import kotlin.Unit;
import kotlin.coroutines.CoroutineContext;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\\\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010%\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0003\n\u0002\b\r\u0018\u0000 &2\u00020\u0001:\u0005%&'()B!\b\u0001\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\b\b\u0002\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ \u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u000eH\u0002J \u0010\u0018\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u00192\u0006\u0010\u0017\u001a\u00020\u0010H\u0002J\b\u0010\u001a\u001a\u00020\u0012H\u0002J\u0018\u0010\u001b\u001a\u00020\u00122\u0006\u0010\u001c\u001a\u00020\u001d2\u0006\u0010\u0017\u001a\u00020\u000eH\u0002J\u0018\u0010\u001e\u001a\u00020\u00122\u0006\u0010\u001c\u001a\u00020\u001d2\u0006\u0010\u0017\u001a\u00020\u0010H\u0002J&\u0010\u001f\u001a\u00020\u00122\u0006\u0010 \u001a\u00020\r2\u0006\u0010!\u001a\u00020\r2\u0006\u0010\"\u001a\u00020\r2\u0006\u0010\u0017\u001a\u00020\u000eJ.\u0010#\u001a\u00020\u00122\u0006\u0010 \u001a\u00020\r2\u0006\u0010$\u001a\u00020\r2\u0006\u0010!\u001a\u00020\r2\u0006\u0010\"\u001a\u00020\r2\u0006\u0010\u0017\u001a\u00020\u0010R\u000e\u0010\t\u001a\u00020\nX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0004¢\u0006\u0002\n\u0000R\u001a\u0010\u000b\u001a\u000e\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\u000e0\fX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000R\u001a\u0010\u000f\u001a\u000e\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\u00100\fX\u0004¢\u0006\u0002\n\u0000¨\u0006*"}, d2 = {"Lcom/stripe/android/IssuingCardPinService;", "", "keyProvider", "Lcom/stripe/android/EphemeralKeyProvider;", "stripeRepository", "Lcom/stripe/android/StripeRepository;", "operationIdFactory", "Lcom/stripe/android/OperationIdFactory;", "(Lcom/stripe/android/EphemeralKeyProvider;Lcom/stripe/android/StripeRepository;Lcom/stripe/android/OperationIdFactory;)V", "ephemeralKeyManager", "Lcom/stripe/android/EphemeralKeyManager;", "retrievalListeners", "", "", "Lcom/stripe/android/IssuingCardPinService$IssuingCardPinRetrievalListener;", "updateListeners", "Lcom/stripe/android/IssuingCardPinService$IssuingCardPinUpdateListener;", "fireRetrievePinRequest", "", "ephemeralKey", "Lcom/stripe/android/EphemeralKey;", "operation", "Lcom/stripe/android/EphemeralOperation$Issuing$RetrievePin;", "listener", "fireUpdatePinRequest", "Lcom/stripe/android/EphemeralOperation$Issuing$UpdatePin;", "logMissingListener", "onRetrievePinError", "throwable", "", "onUpdatePinError", "retrievePin", "cardId", "verificationId", "userOneTimeCode", "updatePin", "newPin", "CardPinActionError", "Companion", "IssuingCardPinRetrievalListener", "IssuingCardPinUpdateListener", "Listener", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: IssuingCardPinService.kt */
public final class IssuingCardPinService {
    public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
    private static final String TAG;
    private final EphemeralKeyManager ephemeralKeyManager;
    private final OperationIdFactory operationIdFactory;
    /* access modifiers changed from: private */
    public final Map<String, IssuingCardPinRetrievalListener> retrievalListeners;
    private final StripeRepository stripeRepository;
    /* access modifiers changed from: private */
    public final Map<String, IssuingCardPinUpdateListener> updateListeners;

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\b\b\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002j\u0002\b\u0003j\u0002\b\u0004j\u0002\b\u0005j\u0002\b\u0006j\u0002\b\u0007j\u0002\b\b¨\u0006\t"}, d2 = {"Lcom/stripe/android/IssuingCardPinService$CardPinActionError;", "", "(Ljava/lang/String;I)V", "UNKNOWN_ERROR", "EPHEMERAL_KEY_ERROR", "ONE_TIME_CODE_INCORRECT", "ONE_TIME_CODE_EXPIRED", "ONE_TIME_CODE_TOO_MANY_ATTEMPTS", "ONE_TIME_CODE_ALREADY_REDEEMED", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: IssuingCardPinService.kt */
    public enum CardPinActionError {
        UNKNOWN_ERROR,
        EPHEMERAL_KEY_ERROR,
        ONE_TIME_CODE_INCORRECT,
        ONE_TIME_CODE_EXPIRED,
        ONE_TIME_CODE_TOO_MANY_ATTEMPTS,
        ONE_TIME_CODE_ALREADY_REDEEMED
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\bf\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&¨\u0006\u0006"}, d2 = {"Lcom/stripe/android/IssuingCardPinService$IssuingCardPinRetrievalListener;", "Lcom/stripe/android/IssuingCardPinService$Listener;", "onIssuingCardPinRetrieved", "", "pin", "", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: IssuingCardPinService.kt */
    public interface IssuingCardPinRetrievalListener extends Listener {
        void onIssuingCardPinRetrieved(String str);
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\bf\u0018\u00002\u00020\u0001J\b\u0010\u0002\u001a\u00020\u0003H&¨\u0006\u0004"}, d2 = {"Lcom/stripe/android/IssuingCardPinService$IssuingCardPinUpdateListener;", "Lcom/stripe/android/IssuingCardPinService$Listener;", "onIssuingCardPinUpdated", "", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: IssuingCardPinService.kt */
    public interface IssuingCardPinUpdateListener extends Listener {
        void onIssuingCardPinUpdated();
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0003\n\u0000\bf\u0018\u00002\u00020\u0001J$\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\b\u0010\u0006\u001a\u0004\u0018\u00010\u00072\b\u0010\b\u001a\u0004\u0018\u00010\tH&¨\u0006\n"}, d2 = {"Lcom/stripe/android/IssuingCardPinService$Listener;", "", "onError", "", "errorCode", "Lcom/stripe/android/IssuingCardPinService$CardPinActionError;", "errorMessage", "", "exception", "", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: IssuingCardPinService.kt */
    public interface Listener {
        void onError(CardPinActionError cardPinActionError, String str, Throwable th);
    }

    @JvmStatic
    public static final IssuingCardPinService create(Context context, EphemeralKeyProvider ephemeralKeyProvider) {
        return Companion.create(context, ephemeralKeyProvider);
    }

    @JvmStatic
    public static final IssuingCardPinService create(Context context, String str, EphemeralKeyProvider ephemeralKeyProvider) {
        return Companion.create(context, str, ephemeralKeyProvider);
    }

    public IssuingCardPinService(EphemeralKeyProvider ephemeralKeyProvider, StripeRepository stripeRepository2, OperationIdFactory operationIdFactory2) {
        Intrinsics.checkParameterIsNotNull(ephemeralKeyProvider, "keyProvider");
        Intrinsics.checkParameterIsNotNull(stripeRepository2, "stripeRepository");
        Intrinsics.checkParameterIsNotNull(operationIdFactory2, "operationIdFactory");
        this.stripeRepository = stripeRepository2;
        this.operationIdFactory = operationIdFactory2;
        this.retrievalListeners = new LinkedHashMap();
        this.updateListeners = new LinkedHashMap();
        this.ephemeralKeyManager = new EphemeralKeyManager(ephemeralKeyProvider, new IssuingCardPinService$ephemeralKeyManager$1(this), this.operationIdFactory, true, (Function0) null, 0, 48, (DefaultConstructorMarker) null);
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ IssuingCardPinService(EphemeralKeyProvider ephemeralKeyProvider, StripeRepository stripeRepository2, OperationIdFactory operationIdFactory2, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(ephemeralKeyProvider, stripeRepository2, (i & 4) != 0 ? new StripeOperationIdFactory() : operationIdFactory2);
    }

    public final void retrievePin(String str, String str2, String str3, IssuingCardPinRetrievalListener issuingCardPinRetrievalListener) {
        Intrinsics.checkParameterIsNotNull(str, "cardId");
        Intrinsics.checkParameterIsNotNull(str2, "verificationId");
        Intrinsics.checkParameterIsNotNull(str3, "userOneTimeCode");
        Intrinsics.checkParameterIsNotNull(issuingCardPinRetrievalListener, ServiceSpecificExtraArgs.CastExtraArgs.LISTENER);
        String create = this.operationIdFactory.create();
        this.retrievalListeners.put(create, issuingCardPinRetrievalListener);
        this.ephemeralKeyManager.retrieveEphemeralKey$stripe_release(new EphemeralOperation.Issuing.RetrievePin(str, str2, str3, create));
    }

    public final void updatePin(String str, String str2, String str3, String str4, IssuingCardPinUpdateListener issuingCardPinUpdateListener) {
        Intrinsics.checkParameterIsNotNull(str, "cardId");
        Intrinsics.checkParameterIsNotNull(str2, "newPin");
        Intrinsics.checkParameterIsNotNull(str3, "verificationId");
        Intrinsics.checkParameterIsNotNull(str4, "userOneTimeCode");
        Intrinsics.checkParameterIsNotNull(issuingCardPinUpdateListener, ServiceSpecificExtraArgs.CastExtraArgs.LISTENER);
        String create = this.operationIdFactory.create();
        this.updateListeners.put(create, issuingCardPinUpdateListener);
        this.ephemeralKeyManager.retrieveEphemeralKey$stripe_release(new EphemeralOperation.Issuing.UpdatePin(str, str2, str3, str4, create));
    }

    /* access modifiers changed from: private */
    public final void fireRetrievePinRequest(EphemeralKey ephemeralKey, EphemeralOperation.Issuing.RetrievePin retrievePin, IssuingCardPinRetrievalListener issuingCardPinRetrievalListener) {
        Object obj;
        try {
            Result.Companion companion = Result.Companion;
            issuingCardPinRetrievalListener.onIssuingCardPinRetrieved(this.stripeRepository.retrieveIssuingCardPin(retrievePin.getCardId(), retrievePin.getVerificationId(), retrievePin.getUserOneTimeCode(), ephemeralKey.getSecret()));
            obj = Result.m4constructorimpl(Unit.INSTANCE);
        } catch (Throwable th) {
            Result.Companion companion2 = Result.Companion;
            obj = Result.m4constructorimpl(ResultKt.createFailure(th));
        }
        Throwable r4 = Result.m7exceptionOrNullimpl(obj);
        if (r4 != null) {
            Result.Companion companion3 = Result.Companion;
            onRetrievePinError(r4, issuingCardPinRetrievalListener);
            Result.m4constructorimpl(Unit.INSTANCE);
        }
    }

    private final void onRetrievePinError(Throwable th, IssuingCardPinRetrievalListener issuingCardPinRetrievalListener) {
        if (th instanceof InvalidRequestException) {
            StripeError stripeError = ((InvalidRequestException) th).getStripeError();
            String code = stripeError != null ? stripeError.getCode() : null;
            if (code != null) {
                switch (code.hashCode()) {
                    case -1309235419:
                        if (code.equals("expired")) {
                            issuingCardPinRetrievalListener.onError(CardPinActionError.ONE_TIME_CODE_EXPIRED, "The one-time code has expired", (Throwable) null);
                            return;
                        }
                        break;
                    case -1266028985:
                        if (code.equals("incorrect_code")) {
                            issuingCardPinRetrievalListener.onError(CardPinActionError.ONE_TIME_CODE_INCORRECT, "The one-time code was incorrect.", (Throwable) null);
                            return;
                        }
                        break;
                    case 830217595:
                        if (code.equals("too_many_attempts")) {
                            issuingCardPinRetrievalListener.onError(CardPinActionError.ONE_TIME_CODE_TOO_MANY_ATTEMPTS, "The verification challenge was attempted too many times.", (Throwable) null);
                            return;
                        }
                        break;
                    case 1888170818:
                        if (code.equals("already_redeemed")) {
                            issuingCardPinRetrievalListener.onError(CardPinActionError.ONE_TIME_CODE_ALREADY_REDEEMED, "The verification challenge was already redeemed.", (Throwable) null);
                            return;
                        }
                        break;
                }
            }
            issuingCardPinRetrievalListener.onError(CardPinActionError.UNKNOWN_ERROR, "The call to retrieve the PIN failed, possibly an error with the verification.", th);
            return;
        }
        issuingCardPinRetrievalListener.onError(CardPinActionError.UNKNOWN_ERROR, "An error occurred while retrieving the PIN.", th);
    }

    /* access modifiers changed from: private */
    public final void fireUpdatePinRequest(EphemeralKey ephemeralKey, EphemeralOperation.Issuing.UpdatePin updatePin, IssuingCardPinUpdateListener issuingCardPinUpdateListener) {
        Object obj;
        try {
            Result.Companion companion = Result.Companion;
            this.stripeRepository.updateIssuingCardPin(updatePin.getCardId(), updatePin.getNewPin(), updatePin.getVerificationId(), updatePin.getUserOneTimeCode(), ephemeralKey.getSecret());
            issuingCardPinUpdateListener.onIssuingCardPinUpdated();
            obj = Result.m4constructorimpl(Unit.INSTANCE);
        } catch (Throwable th) {
            Result.Companion companion2 = Result.Companion;
            obj = Result.m4constructorimpl(ResultKt.createFailure(th));
        }
        Throwable r8 = Result.m7exceptionOrNullimpl(obj);
        if (r8 != null) {
            Result.Companion companion3 = Result.Companion;
            onUpdatePinError(r8, issuingCardPinUpdateListener);
            Result.m4constructorimpl(Unit.INSTANCE);
        }
    }

    private final void onUpdatePinError(Throwable th, IssuingCardPinUpdateListener issuingCardPinUpdateListener) {
        if (th instanceof InvalidRequestException) {
            StripeError stripeError = ((InvalidRequestException) th).getStripeError();
            String code = stripeError != null ? stripeError.getCode() : null;
            if (code != null) {
                switch (code.hashCode()) {
                    case -1309235419:
                        if (code.equals("expired")) {
                            issuingCardPinUpdateListener.onError(CardPinActionError.ONE_TIME_CODE_EXPIRED, "The one-time code has expired.", (Throwable) null);
                            return;
                        }
                        break;
                    case -1266028985:
                        if (code.equals("incorrect_code")) {
                            issuingCardPinUpdateListener.onError(CardPinActionError.ONE_TIME_CODE_INCORRECT, "The one-time code was incorrect.", (Throwable) null);
                            return;
                        }
                        break;
                    case 830217595:
                        if (code.equals("too_many_attempts")) {
                            issuingCardPinUpdateListener.onError(CardPinActionError.ONE_TIME_CODE_TOO_MANY_ATTEMPTS, "The verification challenge was attempted too many times.", (Throwable) null);
                            return;
                        }
                        break;
                    case 1888170818:
                        if (code.equals("already_redeemed")) {
                            issuingCardPinUpdateListener.onError(CardPinActionError.ONE_TIME_CODE_ALREADY_REDEEMED, "The verification challenge was already redeemed.", (Throwable) null);
                            return;
                        }
                        break;
                }
            }
            issuingCardPinUpdateListener.onError(CardPinActionError.UNKNOWN_ERROR, "The call to update the PIN failed, possibly an error with the verification.", th);
            return;
        }
        issuingCardPinUpdateListener.onError(CardPinActionError.UNKNOWN_ERROR, "An error occurred while updating the PIN.", th);
    }

    /* access modifiers changed from: private */
    public final void logMissingListener() {
        String str = TAG;
        Log.e(str, getClass().getName() + " was called without a listener");
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0018\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\nH\u0007J \u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\u000b\u001a\u00020\u00042\u0006\u0010\t\u001a\u00020\nH\u0007R\u000e\u0010\u0003\u001a\u00020\u0004X\u0004¢\u0006\u0002\n\u0000¨\u0006\f"}, d2 = {"Lcom/stripe/android/IssuingCardPinService$Companion;", "", "()V", "TAG", "", "create", "Lcom/stripe/android/IssuingCardPinService;", "context", "Landroid/content/Context;", "keyProvider", "Lcom/stripe/android/EphemeralKeyProvider;", "publishableKey", "stripe_release"}, k = 1, mv = {1, 1, 16})
    /* compiled from: IssuingCardPinService.kt */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        @JvmStatic
        public final IssuingCardPinService create(Context context, EphemeralKeyProvider ephemeralKeyProvider) {
            Intrinsics.checkParameterIsNotNull(context, "context");
            Intrinsics.checkParameterIsNotNull(ephemeralKeyProvider, "keyProvider");
            return create(context, PaymentConfiguration.Companion.getInstance(context).getPublishableKey(), ephemeralKeyProvider);
        }

        @JvmStatic
        public final IssuingCardPinService create(Context context, String str, EphemeralKeyProvider ephemeralKeyProvider) {
            EphemeralKeyProvider ephemeralKeyProvider2 = ephemeralKeyProvider;
            Context context2 = context;
            Intrinsics.checkParameterIsNotNull(context2, "context");
            String str2 = str;
            Intrinsics.checkParameterIsNotNull(str2, "publishableKey");
            Intrinsics.checkParameterIsNotNull(ephemeralKeyProvider2, "keyProvider");
            return new IssuingCardPinService(ephemeralKeyProvider2, new StripeApiRepository(context2, str2, Stripe.Companion.getAppInfo(), (Logger) null, (ApiRequestExecutor) null, (AnalyticsRequestExecutor) null, (FingerprintDataRepository) null, (ApiFingerprintParamsFactory) null, (AnalyticsDataFactory) null, (FingerprintParamsUtils) null, (CoroutineContext) null, (String) null, (String) null, 8184, (DefaultConstructorMarker) null), new StripeOperationIdFactory());
        }
    }

    static {
        String name = IssuingCardPinService.class.getName();
        Intrinsics.checkExpressionValueIsNotNull(name, "IssuingCardPinService::class.java.name");
        TAG = name;
    }
}
