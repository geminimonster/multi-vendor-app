package com.stripe.android;

import android.content.SharedPreferences;
import com.stripe.android.PaymentSessionPrefs;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002*\u0001\u0000\b\n\u0018\u00002\u00020\u0001J\u0014\u0010\u0002\u001a\u0004\u0018\u00010\u00032\b\u0010\u0004\u001a\u0004\u0018\u00010\u0003H\u0016J\u001a\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0004\u001a\u00020\u00032\b\u0010\u0007\u001a\u0004\u0018\u00010\u0003H\u0016¨\u0006\b"}, d2 = {"com/stripe/android/PaymentSessionPrefs$Companion$create$1", "Lcom/stripe/android/PaymentSessionPrefs;", "getPaymentMethodId", "", "customerId", "savePaymentMethodId", "", "paymentMethodId", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: PaymentSessionPrefs.kt */
public final class PaymentSessionPrefs$Companion$create$1 implements PaymentSessionPrefs {
    final /* synthetic */ SharedPreferences $prefs;
    final /* synthetic */ PaymentSessionPrefs.Companion this$0;

    PaymentSessionPrefs$Companion$create$1(PaymentSessionPrefs.Companion companion, SharedPreferences sharedPreferences) {
        this.this$0 = companion;
        this.$prefs = sharedPreferences;
    }

    public String getPaymentMethodId(String str) {
        SharedPreferences sharedPreferences;
        if (str == null || (sharedPreferences = this.$prefs) == null) {
            return null;
        }
        return sharedPreferences.getString(this.this$0.getPaymentMethodKey(str), (String) null);
    }

    public void savePaymentMethodId(String str, String str2) {
        SharedPreferences.Editor edit;
        SharedPreferences.Editor putString;
        Intrinsics.checkParameterIsNotNull(str, "customerId");
        SharedPreferences sharedPreferences = this.$prefs;
        if (sharedPreferences != null && (edit = sharedPreferences.edit()) != null && (putString = edit.putString(this.this$0.getPaymentMethodKey(str), str2)) != null) {
            putString.apply();
        }
    }
}
