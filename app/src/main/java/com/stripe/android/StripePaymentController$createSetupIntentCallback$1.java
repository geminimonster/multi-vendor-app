package com.stripe.android;

import com.stripe.android.ApiRequest;
import com.stripe.android.model.SetupIntent;
import com.stripe.android.model.StripeIntent;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000!\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003*\u0001\u0000\b\n\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001J\u0014\u0010\u0003\u001a\u00020\u00042\n\u0010\u0005\u001a\u00060\u0006j\u0002`\u0007H\u0016J\u0010\u0010\b\u001a\u00020\u00042\u0006\u0010\t\u001a\u00020\u0002H\u0016¨\u0006\n"}, d2 = {"com/stripe/android/StripePaymentController$createSetupIntentCallback$1", "Lcom/stripe/android/ApiResultCallback;", "Lcom/stripe/android/model/StripeIntent;", "onError", "", "e", "Ljava/lang/Exception;", "Lkotlin/Exception;", "onSuccess", "result", "stripe_release"}, k = 1, mv = {1, 1, 16})
/* compiled from: StripePaymentController.kt */
public final class StripePaymentController$createSetupIntentCallback$1 implements ApiResultCallback<StripeIntent> {
    final /* synthetic */ int $flowOutcome;
    final /* synthetic */ ApiRequest.Options $requestOptions;
    final /* synthetic */ ApiResultCallback $resultCallback;
    final /* synthetic */ boolean $shouldCancelSource;
    final /* synthetic */ String $sourceId;
    final /* synthetic */ StripePaymentController this$0;

    StripePaymentController$createSetupIntentCallback$1(StripePaymentController stripePaymentController, boolean z, String str, ApiRequest.Options options, int i, ApiResultCallback apiResultCallback) {
        this.this$0 = stripePaymentController;
        this.$shouldCancelSource = z;
        this.$sourceId = str;
        this.$requestOptions = options;
        this.$flowOutcome = i;
        this.$resultCallback = apiResultCallback;
    }

    public void onSuccess(StripeIntent stripeIntent) {
        Intrinsics.checkParameterIsNotNull(stripeIntent, "result");
        if (!(stripeIntent instanceof SetupIntent)) {
            ApiResultCallback apiResultCallback = this.$resultCallback;
            apiResultCallback.onError(new IllegalArgumentException("Expected a SetupIntent, received a " + stripeIntent.getClass().getSimpleName()));
        } else if (!this.$shouldCancelSource || !stripeIntent.requiresAction()) {
            Logger access$getLogger$p = this.this$0.logger;
            access$getLogger$p.debug("Dispatching SetupIntentResult for " + stripeIntent.getId());
            this.$resultCallback.onSuccess(new SetupIntentResult((SetupIntent) stripeIntent, this.$flowOutcome));
        } else {
            Logger access$getLogger$p2 = this.this$0.logger;
            access$getLogger$p2.debug("Canceling source '" + this.$sourceId + "' for SetupIntent");
            StripeRepository access$getStripeRepository$p = this.this$0.stripeRepository;
            String str = this.$sourceId;
            ApiRequest.Options options = this.$requestOptions;
            access$getStripeRepository$p.cancelIntent(stripeIntent, str, options, this.this$0.createSetupIntentCallback(options, this.$flowOutcome, str, false, this.$resultCallback));
        }
    }

    public void onError(Exception exc) {
        Intrinsics.checkParameterIsNotNull(exc, "e");
        this.$resultCallback.onError(exc);
    }
}
