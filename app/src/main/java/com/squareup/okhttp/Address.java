package com.squareup.okhttp;

import com.squareup.okhttp.internal.Util;
import java.net.Proxy;
import java.net.UnknownHostException;
import java.util.List;
import javax.net.SocketFactory;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSocketFactory;

public final class Address {
    final Authenticator authenticator;
    final HostnameVerifier hostnameVerifier;
    final List<Protocol> protocols;
    final Proxy proxy;
    final SocketFactory socketFactory;
    final SSLSocketFactory sslSocketFactory;
    final String uriHost;
    final int uriPort;

    public Address(String str, int i, SocketFactory socketFactory2, SSLSocketFactory sSLSocketFactory, HostnameVerifier hostnameVerifier2, Authenticator authenticator2, Proxy proxy2, List<Protocol> list) throws UnknownHostException {
        if (str == null) {
            throw new NullPointerException("uriHost == null");
        } else if (i <= 0) {
            throw new IllegalArgumentException("uriPort <= 0: " + i);
        } else if (authenticator2 == null) {
            throw new IllegalArgumentException("authenticator == null");
        } else if (list != null) {
            this.proxy = proxy2;
            this.uriHost = str;
            this.uriPort = i;
            this.socketFactory = socketFactory2;
            this.sslSocketFactory = sSLSocketFactory;
            this.hostnameVerifier = hostnameVerifier2;
            this.authenticator = authenticator2;
            this.protocols = Util.immutableList(list);
        } else {
            throw new IllegalArgumentException("protocols == null");
        }
    }

    public String getUriHost() {
        return this.uriHost;
    }

    public int getUriPort() {
        return this.uriPort;
    }

    public SocketFactory getSocketFactory() {
        return this.socketFactory;
    }

    public SSLSocketFactory getSslSocketFactory() {
        return this.sslSocketFactory;
    }

    public HostnameVerifier getHostnameVerifier() {
        return this.hostnameVerifier;
    }

    public Authenticator getAuthenticator() {
        return this.authenticator;
    }

    public List<Protocol> getProtocols() {
        return this.protocols;
    }

    public Proxy getProxy() {
        return this.proxy;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof Address)) {
            return false;
        }
        Address address = (Address) obj;
        if (!Util.equal(this.proxy, address.proxy) || !this.uriHost.equals(address.uriHost) || this.uriPort != address.uriPort || !Util.equal(this.sslSocketFactory, address.sslSocketFactory) || !Util.equal(this.hostnameVerifier, address.hostnameVerifier) || !Util.equal(this.authenticator, address.authenticator) || !Util.equal(this.protocols, address.protocols)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int hashCode = (((527 + this.uriHost.hashCode()) * 31) + this.uriPort) * 31;
        SSLSocketFactory sSLSocketFactory = this.sslSocketFactory;
        int i = 0;
        int hashCode2 = (hashCode + (sSLSocketFactory != null ? sSLSocketFactory.hashCode() : 0)) * 31;
        HostnameVerifier hostnameVerifier2 = this.hostnameVerifier;
        int hashCode3 = (((hashCode2 + (hostnameVerifier2 != null ? hostnameVerifier2.hashCode() : 0)) * 31) + this.authenticator.hashCode()) * 31;
        Proxy proxy2 = this.proxy;
        if (proxy2 != null) {
            i = proxy2.hashCode();
        }
        return ((hashCode3 + i) * 31) + this.protocols.hashCode();
    }
}
