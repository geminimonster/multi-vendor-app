package com.squareup.okhttp;

import com.squareup.okhttp.internal.Platform;
import com.squareup.okhttp.internal.http.HttpConnection;
import com.squareup.okhttp.internal.http.HttpEngine;
import com.squareup.okhttp.internal.http.HttpTransport;
import com.squareup.okhttp.internal.http.OkHeaders;
import com.squareup.okhttp.internal.http.SpdyTransport;
import com.squareup.okhttp.internal.http.Transport;
import com.squareup.okhttp.internal.spdy.SpdyConnection;
import java.io.IOException;
import java.net.Proxy;
import java.net.Socket;
import java.net.URL;
import javax.net.ssl.SSLSocket;

public final class Connection {
    private boolean connected = false;
    private Handshake handshake;
    private HttpConnection httpConnection;
    private long idleStartTimeNs;
    private Object owner;
    private final ConnectionPool pool;
    private Protocol protocol = Protocol.HTTP_1_1;
    private int recycleCount;
    private final Route route;
    private Socket socket;
    private SpdyConnection spdyConnection;

    public Connection(ConnectionPool connectionPool, Route route2) {
        this.pool = connectionPool;
        this.route = route2;
    }

    /* access modifiers changed from: package-private */
    public Object getOwner() {
        Object obj;
        synchronized (this.pool) {
            obj = this.owner;
        }
        return obj;
    }

    /* access modifiers changed from: package-private */
    public void setOwner(Object obj) {
        if (!isSpdy()) {
            synchronized (this.pool) {
                if (this.owner == null) {
                    this.owner = obj;
                } else {
                    throw new IllegalStateException("Connection already has an owner!");
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public boolean clearOwner() {
        synchronized (this.pool) {
            if (this.owner == null) {
                return false;
            }
            this.owner = null;
            return true;
        }
    }

    /* access modifiers changed from: package-private */
    public void closeIfOwnedBy(Object obj) throws IOException {
        if (!isSpdy()) {
            synchronized (this.pool) {
                if (this.owner == obj) {
                    this.owner = null;
                    this.socket.close();
                    return;
                }
                return;
            }
        }
        throw new IllegalStateException();
    }

    /* access modifiers changed from: package-private */
    public void connect(int i, int i2, int i3, Request request) throws IOException {
        if (!this.connected) {
            if (this.route.proxy.type() != Proxy.Type.HTTP) {
                this.socket = new Socket(this.route.proxy);
            } else {
                this.socket = this.route.address.socketFactory.createSocket();
            }
            this.socket.setSoTimeout(i2);
            Platform.get().connectSocket(this.socket, this.route.inetSocketAddress, i);
            if (this.route.address.sslSocketFactory != null) {
                upgradeToTls(request, i2, i3);
            } else {
                this.httpConnection = new HttpConnection(this.pool, this, this.socket);
            }
            this.connected = true;
            return;
        }
        throw new IllegalStateException("already connected");
    }

    private void upgradeToTls(Request request, int i, int i2) throws IOException {
        String selectedProtocol;
        Platform platform = Platform.get();
        if (request != null) {
            makeTunnel(request, i, i2);
        }
        Socket createSocket = this.route.address.sslSocketFactory.createSocket(this.socket, this.route.address.uriHost, this.route.address.uriPort, true);
        this.socket = createSocket;
        SSLSocket sSLSocket = (SSLSocket) createSocket;
        platform.configureTls(sSLSocket, this.route.address.uriHost, this.route.tlsVersion);
        boolean supportsNpn = this.route.supportsNpn();
        if (supportsNpn) {
            platform.setProtocols(sSLSocket, this.route.address.protocols);
        }
        sSLSocket.startHandshake();
        if (this.route.address.hostnameVerifier.verify(this.route.address.uriHost, sSLSocket.getSession())) {
            this.handshake = Handshake.get(sSLSocket.getSession());
            if (supportsNpn && (selectedProtocol = platform.getSelectedProtocol(sSLSocket)) != null) {
                this.protocol = Protocol.get(selectedProtocol);
            }
            if (this.protocol == Protocol.SPDY_3 || this.protocol == Protocol.HTTP_2) {
                sSLSocket.setSoTimeout(0);
                SpdyConnection build = new SpdyConnection.Builder(this.route.address.getUriHost(), true, this.socket).protocol(this.protocol).build();
                this.spdyConnection = build;
                build.sendConnectionPreface();
                return;
            }
            this.httpConnection = new HttpConnection(this.pool, this, this.socket);
            return;
        }
        throw new IOException("Hostname '" + this.route.address.uriHost + "' was not verified");
    }

    /* access modifiers changed from: package-private */
    public boolean isConnected() {
        return this.connected;
    }

    public Route getRoute() {
        return this.route;
    }

    public Socket getSocket() {
        return this.socket;
    }

    /* access modifiers changed from: package-private */
    public boolean isAlive() {
        return !this.socket.isClosed() && !this.socket.isInputShutdown() && !this.socket.isOutputShutdown();
    }

    /* access modifiers changed from: package-private */
    public boolean isReadable() {
        HttpConnection httpConnection2 = this.httpConnection;
        if (httpConnection2 != null) {
            return httpConnection2.isReadable();
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    public void resetIdleStartTime() {
        if (this.spdyConnection == null) {
            this.idleStartTimeNs = System.nanoTime();
            return;
        }
        throw new IllegalStateException("spdyConnection != null");
    }

    /* access modifiers changed from: package-private */
    public boolean isIdle() {
        SpdyConnection spdyConnection2 = this.spdyConnection;
        return spdyConnection2 == null || spdyConnection2.isIdle();
    }

    /* access modifiers changed from: package-private */
    public boolean isExpired(long j) {
        return getIdleStartTimeNs() < System.nanoTime() - j;
    }

    /* access modifiers changed from: package-private */
    public long getIdleStartTimeNs() {
        SpdyConnection spdyConnection2 = this.spdyConnection;
        return spdyConnection2 == null ? this.idleStartTimeNs : spdyConnection2.getIdleStartTimeNs();
    }

    public Handshake getHandshake() {
        return this.handshake;
    }

    /* access modifiers changed from: package-private */
    public Transport newTransport(HttpEngine httpEngine) throws IOException {
        return this.spdyConnection != null ? new SpdyTransport(httpEngine, this.spdyConnection) : new HttpTransport(httpEngine, this.httpConnection);
    }

    /* access modifiers changed from: package-private */
    public boolean isSpdy() {
        return this.spdyConnection != null;
    }

    public Protocol getProtocol() {
        return this.protocol;
    }

    /* access modifiers changed from: package-private */
    public void setProtocol(Protocol protocol2) {
        if (protocol2 != null) {
            this.protocol = protocol2;
            return;
        }
        throw new IllegalArgumentException("protocol == null");
    }

    /* access modifiers changed from: package-private */
    public void setTimeouts(int i, int i2) throws IOException {
        if (!this.connected) {
            throw new IllegalStateException("setTimeouts - not connected");
        } else if (this.httpConnection != null) {
            this.socket.setSoTimeout(i);
            this.httpConnection.setTimeouts(i, i2);
        }
    }

    /* access modifiers changed from: package-private */
    public void incrementRecycleCount() {
        this.recycleCount++;
    }

    /* access modifiers changed from: package-private */
    public int recycleCount() {
        return this.recycleCount;
    }

    private void makeTunnel(Request request, int i, int i2) throws IOException {
        HttpConnection httpConnection2 = new HttpConnection(this.pool, this, this.socket);
        httpConnection2.setTimeouts(i, i2);
        URL url = request.url();
        String str = "CONNECT " + url.getHost() + ":" + url.getPort() + " HTTP/1.1";
        do {
            httpConnection2.writeRequest(request.headers(), str);
            httpConnection2.flush();
            Response build = httpConnection2.readResponse().request(request).build();
            httpConnection2.emptyResponseBody();
            int code = build.code();
            if (code != 200) {
                if (code == 407) {
                    request = OkHeaders.processAuthHeader(this.route.address.authenticator, build, this.route.proxy);
                } else {
                    throw new IOException("Unexpected response code for CONNECT: " + build.code());
                }
            } else if (httpConnection2.bufferSize() > 0) {
                throw new IOException("TLS tunnel buffered too many bytes!");
            } else {
                return;
            }
        } while (request != null);
        throw new IOException("Failed to authenticate with proxy");
    }
}
