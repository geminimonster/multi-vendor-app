package com.squareup.okhttp;

import com.squareup.okhttp.internal.http.HeaderParser;

public final class CacheControl {
    private final boolean isPublic;
    private final int maxAgeSeconds;
    private final int maxStaleSeconds;
    private final int minFreshSeconds;
    private final boolean mustRevalidate;
    private final boolean noCache;
    private final boolean noStore;
    private final boolean onlyIfCached;
    private final int sMaxAgeSeconds;

    private CacheControl(boolean z, boolean z2, int i, int i2, boolean z3, boolean z4, int i3, int i4, boolean z5) {
        this.noCache = z;
        this.noStore = z2;
        this.maxAgeSeconds = i;
        this.sMaxAgeSeconds = i2;
        this.isPublic = z3;
        this.mustRevalidate = z4;
        this.maxStaleSeconds = i3;
        this.minFreshSeconds = i4;
        this.onlyIfCached = z5;
    }

    public boolean noCache() {
        return this.noCache;
    }

    public boolean noStore() {
        return this.noStore;
    }

    public int maxAgeSeconds() {
        return this.maxAgeSeconds;
    }

    public int sMaxAgeSeconds() {
        return this.sMaxAgeSeconds;
    }

    public boolean isPublic() {
        return this.isPublic;
    }

    public boolean mustRevalidate() {
        return this.mustRevalidate;
    }

    public int maxStaleSeconds() {
        return this.maxStaleSeconds;
    }

    public int minFreshSeconds() {
        return this.minFreshSeconds;
    }

    public boolean onlyIfCached() {
        return this.onlyIfCached;
    }

    public static CacheControl parse(Headers headers) {
        int i;
        String str;
        Headers headers2 = headers;
        boolean z = false;
        boolean z2 = false;
        int i2 = -1;
        int i3 = -1;
        boolean z3 = false;
        boolean z4 = false;
        int i4 = -1;
        int i5 = -1;
        boolean z5 = false;
        for (int i6 = 0; i6 < headers.size(); i6++) {
            if (headers2.name(i6).equalsIgnoreCase("Cache-Control") || headers2.name(i6).equalsIgnoreCase("Pragma")) {
                String value = headers2.value(i6);
                for (int i7 = 0; i7 < value.length(); i7 = i) {
                    int skipUntil = HeaderParser.skipUntil(value, i7, "=,;");
                    String trim = value.substring(i7, skipUntil).trim();
                    if (skipUntil == value.length() || value.charAt(skipUntil) == ',' || value.charAt(skipUntil) == ';') {
                        i = skipUntil + 1;
                        str = null;
                    } else {
                        int skipWhitespace = HeaderParser.skipWhitespace(value, skipUntil + 1);
                        if (skipWhitespace >= value.length() || value.charAt(skipWhitespace) != '\"') {
                            i = HeaderParser.skipUntil(value, skipWhitespace, ",;");
                            str = value.substring(skipWhitespace, i).trim();
                        } else {
                            int i8 = skipWhitespace + 1;
                            int skipUntil2 = HeaderParser.skipUntil(value, i8, "\"");
                            str = value.substring(i8, skipUntil2);
                            i = skipUntil2 + 1;
                        }
                    }
                    if ("no-cache".equalsIgnoreCase(trim)) {
                        z = true;
                    } else if ("no-store".equalsIgnoreCase(trim)) {
                        z2 = true;
                    } else if ("max-age".equalsIgnoreCase(trim)) {
                        i2 = HeaderParser.parseSeconds(str);
                    } else if ("s-maxage".equalsIgnoreCase(trim)) {
                        i3 = HeaderParser.parseSeconds(str);
                    } else if ("public".equalsIgnoreCase(trim)) {
                        z3 = true;
                    } else if ("must-revalidate".equalsIgnoreCase(trim)) {
                        z4 = true;
                    } else if ("max-stale".equalsIgnoreCase(trim)) {
                        i4 = HeaderParser.parseSeconds(str);
                    } else if ("min-fresh".equalsIgnoreCase(trim)) {
                        i5 = HeaderParser.parseSeconds(str);
                    } else if ("only-if-cached".equalsIgnoreCase(trim)) {
                        z5 = true;
                    }
                }
            }
        }
        return new CacheControl(z, z2, i2, i3, z3, z4, i4, i5, z5);
    }
}
