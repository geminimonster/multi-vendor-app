package com.squareup.okhttp;

import androidx.browser.trusted.sharing.ShareTarget;
import com.bumptech.glide.load.Key;
import com.squareup.okhttp.internal.Util;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import kotlin.text.Typography;

public final class FormEncodingBuilder {
    private static final MediaType CONTENT_TYPE = MediaType.parse(ShareTarget.ENCODING_TYPE_URL_ENCODED);
    private final StringBuilder content = new StringBuilder();

    public FormEncodingBuilder add(String str, String str2) {
        if (this.content.length() > 0) {
            this.content.append(Typography.amp);
        }
        try {
            StringBuilder sb = this.content;
            sb.append(URLEncoder.encode(str, Key.STRING_CHARSET_NAME));
            sb.append('=');
            sb.append(URLEncoder.encode(str2, Key.STRING_CHARSET_NAME));
            return this;
        } catch (UnsupportedEncodingException e) {
            throw new AssertionError(e);
        }
    }

    public RequestBody build() {
        if (this.content.length() != 0) {
            return RequestBody.create(CONTENT_TYPE, this.content.toString().getBytes(Util.UTF_8));
        }
        throw new IllegalStateException("Form encoded body must have at least one part.");
    }
}
