package com.squareup.okhttp.internal.http;

import androidx.browser.trusted.sharing.ShareTarget;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.Set;

public final class HttpMethod {
    public static final Set<String> METHODS = new LinkedHashSet(Arrays.asList(new String[]{"OPTIONS", ShareTarget.METHOD_GET, "HEAD", ShareTarget.METHOD_POST, "PUT", "DELETE", "TRACE", "PATCH"}));

    public static boolean invalidatesCache(String str) {
        return str.equals(ShareTarget.METHOD_POST) || str.equals("PATCH") || str.equals("PUT") || str.equals("DELETE");
    }

    public static boolean hasRequestBody(String str) {
        return str.equals(ShareTarget.METHOD_POST) || str.equals("PUT") || str.equals("PATCH") || str.equals("DELETE");
    }

    private HttpMethod() {
    }
}
