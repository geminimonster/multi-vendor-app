package com.squareup.okhttp.internal.spdy;

import androidx.core.view.PointerIconCompat;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import kotlin.UByte;

class Huffman {
    private static final int[] CODES = {67108794, 67108795, 67108796, 67108797, 67108798, 67108799, 67108800, 67108801, 67108802, 67108803, 67108804, 67108805, 67108806, 67108807, 67108808, 67108809, 67108810, 67108811, 67108812, 67108813, 67108814, 67108815, 67108816, 67108817, 67108818, 67108819, 67108820, 67108821, 67108822, 67108823, 67108824, 67108825, 6, 8188, 496, 16380, 32764, 30, 100, 8189, PointerIconCompat.TYPE_ZOOM_IN, 497, PointerIconCompat.TYPE_ZOOM_OUT, PointerIconCompat.TYPE_GRAB, 101, 102, 31, 7, 0, 1, 2, 8, 32, 33, 34, 35, 36, 37, 38, 236, 131068, 39, 32765, 1021, 32766, 103, 237, 238, 104, 239, 105, 106, 498, 240, 499, 500, 501, 107, 108, 241, 242, 502, 503, 109, 40, 243, 504, 505, 244, 506, 507, 2044, 67108826, 2045, 16381, 110, 262142, 9, 111, 10, 41, 11, 112, 42, 43, 12, 245, 246, 44, 45, 46, 13, 47, 508, 48, 49, 14, 113, 114, 115, 116, 117, 247, 131069, 4092, 131070, 4093, 67108827, 67108828, 67108829, 67108830, 67108831, 67108832, 67108833, 67108834, 67108835, 67108836, 67108837, 67108838, 67108839, 67108840, 67108841, 67108842, 67108843, 67108844, 67108845, 67108846, 67108847, 67108848, 67108849, 67108850, 67108851, 67108852, 67108853, 67108854, 67108855, 67108856, 67108857, 67108858, 67108859, 67108860, 67108861, 67108862, 67108863, 33554304, 33554305, 33554306, 33554307, 33554308, 33554309, 33554310, 33554311, 33554312, 33554313, 33554314, 33554315, 33554316, 33554317, 33554318, 33554319, 33554320, 33554321, 33554322, 33554323, 33554324, 33554325, 33554326, 33554327, 33554328, 33554329, 33554330, 33554331, 33554332, 33554333, 33554334, 33554335, 33554336, 33554337, 33554338, 33554339, 33554340, 33554341, 33554342, 33554343, 33554344, 33554345, 33554346, 33554347, 33554348, 33554349, 33554350, 33554351, 33554352, 33554353, 33554354, 33554355, 33554356, 33554357, 33554358, 33554359, 33554360, 33554361, 33554362, 33554363, 33554364, 33554365, 33554366, 33554367, 33554368, 33554369, 33554370, 33554371, 33554372, 33554373, 33554374, 33554375, 33554376, 33554377, 33554378, 33554379, 33554380, 33554381, 33554382, 33554383, 33554384, 33554385, 33554386, 33554387, 33554388, 33554389, 33554390, 33554391, 33554392, 33554393, 33554394, 33554395};
    private static final byte[] CODE_LENGTHS = {JSONParserBase.EOI, JSONParserBase.EOI, JSONParserBase.EOI, JSONParserBase.EOI, JSONParserBase.EOI, JSONParserBase.EOI, JSONParserBase.EOI, JSONParserBase.EOI, JSONParserBase.EOI, JSONParserBase.EOI, JSONParserBase.EOI, JSONParserBase.EOI, JSONParserBase.EOI, JSONParserBase.EOI, JSONParserBase.EOI, JSONParserBase.EOI, JSONParserBase.EOI, JSONParserBase.EOI, JSONParserBase.EOI, JSONParserBase.EOI, JSONParserBase.EOI, JSONParserBase.EOI, JSONParserBase.EOI, JSONParserBase.EOI, JSONParserBase.EOI, JSONParserBase.EOI, JSONParserBase.EOI, JSONParserBase.EOI, JSONParserBase.EOI, JSONParserBase.EOI, JSONParserBase.EOI, JSONParserBase.EOI, 5, 13, 9, 14, 15, 6, 7, 13, 10, 9, 10, 10, 7, 7, 6, 5, 4, 4, 4, 5, 6, 6, 6, 6, 6, 6, 6, 8, 17, 6, 15, 10, 15, 7, 8, 8, 7, 8, 7, 7, 9, 8, 9, 9, 9, 7, 7, 8, 8, 9, 9, 7, 6, 8, 9, 9, 8, 9, 9, 11, JSONParserBase.EOI, 11, 14, 7, 18, 5, 7, 5, 6, 5, 7, 6, 6, 5, 8, 8, 6, 6, 6, 5, 6, 9, 6, 6, 5, 7, 7, 7, 7, 7, 8, 17, 12, 17, 12, JSONParserBase.EOI, JSONParserBase.EOI, JSONParserBase.EOI, JSONParserBase.EOI, JSONParserBase.EOI, JSONParserBase.EOI, JSONParserBase.EOI, JSONParserBase.EOI, JSONParserBase.EOI, JSONParserBase.EOI, JSONParserBase.EOI, JSONParserBase.EOI, JSONParserBase.EOI, JSONParserBase.EOI, JSONParserBase.EOI, JSONParserBase.EOI, JSONParserBase.EOI, JSONParserBase.EOI, JSONParserBase.EOI, JSONParserBase.EOI, JSONParserBase.EOI, JSONParserBase.EOI, JSONParserBase.EOI, JSONParserBase.EOI, JSONParserBase.EOI, JSONParserBase.EOI, JSONParserBase.EOI, JSONParserBase.EOI, JSONParserBase.EOI, JSONParserBase.EOI, JSONParserBase.EOI, JSONParserBase.EOI, JSONParserBase.EOI, JSONParserBase.EOI, JSONParserBase.EOI, JSONParserBase.EOI, JSONParserBase.EOI, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25};
    private static final Huffman INSTANCE = new Huffman();
    private final Node root = new Node();

    public static Huffman get() {
        return INSTANCE;
    }

    private Huffman() {
        buildTree();
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v4, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v5, resolved type: long} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v6, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v7, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v8, resolved type: byte} */
    /* access modifiers changed from: package-private */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void encode(byte[] r9, java.io.OutputStream r10) throws java.io.IOException {
        /*
            r8 = this;
            r0 = 0
            r1 = 0
            r2 = r1
            r1 = 0
        L_0x0005:
            int r4 = r9.length
            r5 = 8
            r6 = 255(0xff, float:3.57E-43)
            if (r0 >= r4) goto L_0x0029
            byte r4 = r9[r0]
            r4 = r4 & r6
            int[] r6 = CODES
            r6 = r6[r4]
            byte[] r7 = CODE_LENGTHS
            byte r4 = r7[r4]
            long r2 = r2 << r4
            long r6 = (long) r6
            long r2 = r2 | r6
            int r1 = r1 + r4
        L_0x001b:
            if (r1 < r5) goto L_0x0026
            int r1 = r1 + -8
            long r6 = r2 >> r1
            int r4 = (int) r6
            r10.write(r4)
            goto L_0x001b
        L_0x0026:
            int r0 = r0 + 1
            goto L_0x0005
        L_0x0029:
            if (r1 <= 0) goto L_0x0035
            int r5 = r5 - r1
            long r2 = r2 << r5
            int r9 = r6 >>> r1
            long r0 = (long) r9
            long r0 = r0 | r2
            int r9 = (int) r0
            r10.write(r9)
        L_0x0035:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.squareup.okhttp.internal.spdy.Huffman.encode(byte[], java.io.OutputStream):void");
    }

    /* access modifiers changed from: package-private */
    public int encodedLength(byte[] bArr) {
        long j = 0;
        for (byte b : bArr) {
            j += (long) CODE_LENGTHS[b & UByte.MAX_VALUE];
        }
        return (int) ((j + 7) >> 3);
    }

    /* access modifiers changed from: package-private */
    public byte[] decode(byte[] bArr) throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        Node node = this.root;
        byte b = 0;
        int i = 0;
        for (byte b2 : bArr) {
            b = (b << 8) | (b2 & UByte.MAX_VALUE);
            i += 8;
            while (i >= 8) {
                node = node.children[(b >>> (i - 8)) & 255];
                if (node.children == null) {
                    byteArrayOutputStream.write(node.symbol);
                    i -= node.terminalBits;
                    node = this.root;
                } else {
                    i -= 8;
                }
            }
        }
        while (i > 0) {
            Node node2 = node.children[(b << (8 - i)) & 255];
            if (node2.children != null || node2.terminalBits > i) {
                break;
            }
            byteArrayOutputStream.write(node2.symbol);
            i -= node2.terminalBits;
            node = this.root;
        }
        return byteArrayOutputStream.toByteArray();
    }

    private void buildTree() {
        int i = 0;
        while (true) {
            byte[] bArr = CODE_LENGTHS;
            if (i < bArr.length) {
                addCode(i, CODES[i], bArr[i]);
                i++;
            } else {
                return;
            }
        }
    }

    private void addCode(int i, int i2, byte b) {
        Node node = new Node(i, b);
        Node node2 = this.root;
        while (b > 8) {
            b = (byte) (b - 8);
            int i3 = (i2 >>> b) & 255;
            if (node2.children != null) {
                if (node2.children[i3] == null) {
                    node2.children[i3] = new Node();
                }
                node2 = node2.children[i3];
            } else {
                throw new IllegalStateException("invalid dictionary: prefix not unique");
            }
        }
        int i4 = 8 - b;
        int i5 = (i2 << i4) & 255;
        int i6 = 1 << i4;
        for (int i7 = i5; i7 < i5 + i6; i7++) {
            node2.children[i7] = node;
        }
    }

    private static final class Node {
        /* access modifiers changed from: private */
        public final Node[] children;
        /* access modifiers changed from: private */
        public final int symbol;
        /* access modifiers changed from: private */
        public final int terminalBits;

        Node() {
            this.children = new Node[256];
            this.symbol = 0;
            this.terminalBits = 0;
        }

        Node(int i, int i2) {
            this.children = null;
            this.symbol = i;
            int i3 = i2 & 7;
            this.terminalBits = i3 == 0 ? 8 : i3;
        }
    }
}
