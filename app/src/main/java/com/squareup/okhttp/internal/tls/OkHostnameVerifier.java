package com.squareup.okhttp.internal.tls;

import java.security.cert.CertificateParsingException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.regex.Pattern;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLException;
import javax.net.ssl.SSLSession;

public final class OkHostnameVerifier implements HostnameVerifier {
    private static final int ALT_DNS_NAME = 2;
    private static final int ALT_IPA_NAME = 7;
    public static final OkHostnameVerifier INSTANCE = new OkHostnameVerifier();
    private static final Pattern VERIFY_AS_IP_ADDRESS = Pattern.compile("([0-9a-fA-F]*:[0-9a-fA-F:.]*)|([\\d.]+)");

    private OkHostnameVerifier() {
    }

    public boolean verify(String str, SSLSession sSLSession) {
        try {
            return verify(str, (X509Certificate) sSLSession.getPeerCertificates()[0]);
        } catch (SSLException unused) {
            return false;
        }
    }

    public boolean verify(String str, X509Certificate x509Certificate) {
        return verifyAsIpAddress(str) ? verifyIpAddress(str, x509Certificate) : verifyHostName(str, x509Certificate);
    }

    static boolean verifyAsIpAddress(String str) {
        return VERIFY_AS_IP_ADDRESS.matcher(str).matches();
    }

    private boolean verifyIpAddress(String str, X509Certificate x509Certificate) {
        for (String equalsIgnoreCase : getSubjectAltNames(x509Certificate, 7)) {
            if (str.equalsIgnoreCase(equalsIgnoreCase)) {
                return true;
            }
        }
        return false;
    }

    private boolean verifyHostName(String str, X509Certificate x509Certificate) {
        String findMostSpecific;
        String lowerCase = str.toLowerCase(Locale.US);
        boolean z = false;
        for (String verifyHostName : getSubjectAltNames(x509Certificate, 2)) {
            if (verifyHostName(lowerCase, verifyHostName)) {
                return true;
            }
            z = true;
        }
        if (z || (findMostSpecific = new DistinguishedNameParser(x509Certificate.getSubjectX500Principal()).findMostSpecific("cn")) == null) {
            return false;
        }
        return verifyHostName(lowerCase, findMostSpecific);
    }

    private List<String> getSubjectAltNames(X509Certificate x509Certificate, int i) {
        String str;
        ArrayList arrayList = new ArrayList();
        try {
            Collection<List<?>> subjectAlternativeNames = x509Certificate.getSubjectAlternativeNames();
            if (subjectAlternativeNames == null) {
                return Collections.emptyList();
            }
            for (List next : subjectAlternativeNames) {
                if (next != null) {
                    if (next.size() >= 2) {
                        Integer num = (Integer) next.get(0);
                        if (num != null) {
                            if (num.intValue() == i && (str = (String) next.get(1)) != null) {
                                arrayList.add(str);
                            }
                        }
                    }
                }
            }
            return arrayList;
        } catch (CertificateParsingException unused) {
            return Collections.emptyList();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0051, code lost:
        r5 = r1 + 1;
        r4 = r9.length() - r5;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean verifyHostName(java.lang.String r8, java.lang.String r9) {
        /*
            r7 = this;
            r0 = 0
            if (r8 == 0) goto L_0x006c
            int r1 = r8.length()
            if (r1 == 0) goto L_0x006c
            if (r9 == 0) goto L_0x006c
            int r1 = r9.length()
            if (r1 != 0) goto L_0x0012
            goto L_0x006c
        L_0x0012:
            java.util.Locale r1 = java.util.Locale.US
            java.lang.String r9 = r9.toLowerCase(r1)
            java.lang.String r1 = "*"
            boolean r1 = r9.contains(r1)
            if (r1 != 0) goto L_0x0025
            boolean r8 = r8.equals(r9)
            return r8
        L_0x0025:
            java.lang.String r1 = "*."
            boolean r1 = r9.startsWith(r1)
            r2 = 1
            if (r1 == 0) goto L_0x003b
            int r1 = r9.length()
            r3 = 2
            int r1 = r1 - r3
            boolean r1 = r8.regionMatches(r0, r9, r3, r1)
            if (r1 == 0) goto L_0x003b
            return r2
        L_0x003b:
            r1 = 42
            int r1 = r9.indexOf(r1)
            r3 = 46
            int r4 = r9.indexOf(r3)
            if (r1 <= r4) goto L_0x004a
            return r0
        L_0x004a:
            boolean r4 = r8.regionMatches(r0, r9, r0, r1)
            if (r4 != 0) goto L_0x0051
            return r0
        L_0x0051:
            int r4 = r9.length()
            int r5 = r1 + 1
            int r4 = r4 - r5
            int r6 = r8.length()
            int r6 = r6 - r4
            int r1 = r8.indexOf(r3, r1)
            if (r1 >= r6) goto L_0x0064
            return r0
        L_0x0064:
            boolean r8 = r8.regionMatches(r6, r9, r5, r4)
            if (r8 != 0) goto L_0x006b
            return r0
        L_0x006b:
            return r2
        L_0x006c:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.squareup.okhttp.internal.tls.OkHostnameVerifier.verifyHostName(java.lang.String, java.lang.String):boolean");
    }
}
