package com.squareup.okhttp.internal.http;

import com.squareup.okhttp.Authenticator;
import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;

public final class AuthenticatorAdapter implements Authenticator {
    public static final Authenticator INSTANCE = new AuthenticatorAdapter();

    /* JADX WARNING: Code restructure failed: missing block: B:4:0x0027, code lost:
        r2 = r9.getHost();
        r3 = getConnectToInetAddress(r13, r9);
        r4 = r9.getPort();
        r5 = r9.getProtocol();
        r6 = r1.getRealm();
        r7 = r1.getScheme();
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.squareup.okhttp.Request authenticate(java.net.Proxy r13, com.squareup.okhttp.Response r14) throws java.io.IOException {
        /*
            r12 = this;
            java.util.List r0 = r14.challenges()
            com.squareup.okhttp.Request r14 = r14.request()
            java.net.URL r9 = r14.url()
            int r10 = r0.size()
            r1 = 0
            r11 = 0
        L_0x0012:
            if (r11 >= r10) goto L_0x0071
            java.lang.Object r1 = r0.get(r11)
            com.squareup.okhttp.Challenge r1 = (com.squareup.okhttp.Challenge) r1
            java.lang.String r2 = r1.getScheme()
            java.lang.String r3 = "Basic"
            boolean r2 = r3.equalsIgnoreCase(r2)
            if (r2 != 0) goto L_0x0027
            goto L_0x004e
        L_0x0027:
            java.lang.String r2 = r9.getHost()
            java.net.InetAddress r3 = r12.getConnectToInetAddress(r13, r9)
            int r4 = r9.getPort()
            java.lang.String r5 = r9.getProtocol()
            java.lang.String r6 = r1.getRealm()
            java.lang.String r7 = r1.getScheme()
            java.net.Authenticator$RequestorType r8 = java.net.Authenticator.RequestorType.SERVER
            r1 = r2
            r2 = r3
            r3 = r4
            r4 = r5
            r5 = r6
            r6 = r7
            r7 = r9
            java.net.PasswordAuthentication r1 = java.net.Authenticator.requestPasswordAuthentication(r1, r2, r3, r4, r5, r6, r7, r8)
            if (r1 != 0) goto L_0x0051
        L_0x004e:
            int r11 = r11 + 1
            goto L_0x0012
        L_0x0051:
            java.lang.String r13 = r1.getUserName()
            java.lang.String r0 = new java.lang.String
            char[] r1 = r1.getPassword()
            r0.<init>(r1)
            java.lang.String r13 = com.squareup.okhttp.Credentials.basic(r13, r0)
            com.squareup.okhttp.Request$Builder r14 = r14.newBuilder()
            java.lang.String r0 = "Authorization"
            com.squareup.okhttp.Request$Builder r13 = r14.header(r0, r13)
            com.squareup.okhttp.Request r13 = r13.build()
            return r13
        L_0x0071:
            r13 = 0
            return r13
        */
        throw new UnsupportedOperationException("Method not decompiled: com.squareup.okhttp.internal.http.AuthenticatorAdapter.authenticate(java.net.Proxy, com.squareup.okhttp.Response):com.squareup.okhttp.Request");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:4:0x0027, code lost:
        r2 = (java.net.InetSocketAddress) r14.address();
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.squareup.okhttp.Request authenticateProxy(java.net.Proxy r14, com.squareup.okhttp.Response r15) throws java.io.IOException {
        /*
            r13 = this;
            java.util.List r0 = r15.challenges()
            com.squareup.okhttp.Request r15 = r15.request()
            java.net.URL r9 = r15.url()
            int r10 = r0.size()
            r1 = 0
            r11 = 0
        L_0x0012:
            if (r11 >= r10) goto L_0x0078
            java.lang.Object r1 = r0.get(r11)
            com.squareup.okhttp.Challenge r1 = (com.squareup.okhttp.Challenge) r1
            java.lang.String r2 = r1.getScheme()
            java.lang.String r3 = "Basic"
            boolean r2 = r3.equalsIgnoreCase(r2)
            if (r2 != 0) goto L_0x0027
            goto L_0x0055
        L_0x0027:
            java.net.SocketAddress r2 = r14.address()
            java.net.InetSocketAddress r2 = (java.net.InetSocketAddress) r2
            java.lang.String r3 = r2.getHostName()
            java.net.InetAddress r4 = r13.getConnectToInetAddress(r14, r9)
            int r5 = r2.getPort()
            java.lang.String r6 = r9.getProtocol()
            java.lang.String r7 = r1.getRealm()
            java.lang.String r8 = r1.getScheme()
            java.net.Authenticator$RequestorType r12 = java.net.Authenticator.RequestorType.PROXY
            r1 = r3
            r2 = r4
            r3 = r5
            r4 = r6
            r5 = r7
            r6 = r8
            r7 = r9
            r8 = r12
            java.net.PasswordAuthentication r1 = java.net.Authenticator.requestPasswordAuthentication(r1, r2, r3, r4, r5, r6, r7, r8)
            if (r1 != 0) goto L_0x0058
        L_0x0055:
            int r11 = r11 + 1
            goto L_0x0012
        L_0x0058:
            java.lang.String r14 = r1.getUserName()
            java.lang.String r0 = new java.lang.String
            char[] r1 = r1.getPassword()
            r0.<init>(r1)
            java.lang.String r14 = com.squareup.okhttp.Credentials.basic(r14, r0)
            com.squareup.okhttp.Request$Builder r15 = r15.newBuilder()
            java.lang.String r0 = "Proxy-Authorization"
            com.squareup.okhttp.Request$Builder r14 = r15.header(r0, r14)
            com.squareup.okhttp.Request r14 = r14.build()
            return r14
        L_0x0078:
            r14 = 0
            return r14
        */
        throw new UnsupportedOperationException("Method not decompiled: com.squareup.okhttp.internal.http.AuthenticatorAdapter.authenticateProxy(java.net.Proxy, com.squareup.okhttp.Response):com.squareup.okhttp.Request");
    }

    private InetAddress getConnectToInetAddress(Proxy proxy, URL url) throws IOException {
        return (proxy == null || proxy.type() == Proxy.Type.DIRECT) ? InetAddress.getByName(url.getHost()) : ((InetSocketAddress) proxy.address()).getAddress();
    }
}
