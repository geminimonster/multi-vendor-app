package com.squareup.okhttp.internal;

import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;
import okio.Buffer;
import okio.BufferedSink;
import okio.Okio;

public final class DiskLruCache implements Closeable {
    static final long ANY_SEQUENCE_NUMBER = -1;
    private static final String CLEAN = "CLEAN";
    private static final String DIRTY = "DIRTY";
    static final String JOURNAL_FILE = "journal";
    static final String JOURNAL_FILE_BACKUP = "journal.bkp";
    static final String JOURNAL_FILE_TEMP = "journal.tmp";
    static final Pattern LEGAL_KEY_PATTERN = Pattern.compile("[a-z0-9_-]{1,64}");
    static final String MAGIC = "libcore.io.DiskLruCache";
    /* access modifiers changed from: private */
    public static final OutputStream NULL_OUTPUT_STREAM = new OutputStream() {
        public void write(int i) throws IOException {
        }
    };
    private static final String READ = "READ";
    private static final String REMOVE = "REMOVE";
    static final String VERSION_1 = "1";
    private final int appVersion;
    private final Runnable cleanupRunnable = new Runnable() {
        public void run() {
            synchronized (DiskLruCache.this) {
                if (DiskLruCache.this.journalWriter != null) {
                    try {
                        DiskLruCache.this.trimToSize();
                        if (DiskLruCache.this.journalRebuildRequired()) {
                            DiskLruCache.this.rebuildJournal();
                            int unused = DiskLruCache.this.redundantOpCount = 0;
                        }
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    }
                }
            }
        }
    };
    /* access modifiers changed from: private */
    public final File directory;
    final ThreadPoolExecutor executorService = new ThreadPoolExecutor(0, 1, 60, TimeUnit.SECONDS, new LinkedBlockingQueue(), Util.threadFactory("OkHttp DiskLruCache", true));
    private final File journalFile;
    private final File journalFileBackup;
    private final File journalFileTmp;
    /* access modifiers changed from: private */
    public BufferedSink journalWriter;
    private final LinkedHashMap<String, Entry> lruEntries = new LinkedHashMap<>(0, 0.75f, true);
    private long maxSize;
    private long nextSequenceNumber = 0;
    /* access modifiers changed from: private */
    public int redundantOpCount;
    private long size = 0;
    /* access modifiers changed from: private */
    public final int valueCount;

    private DiskLruCache(File file, int i, int i2, long j) {
        File file2 = file;
        this.directory = file2;
        this.appVersion = i;
        this.journalFile = new File(file2, "journal");
        this.journalFileTmp = new File(file2, "journal.tmp");
        this.journalFileBackup = new File(file2, "journal.bkp");
        this.valueCount = i2;
        this.maxSize = j;
    }

    public static DiskLruCache open(File file, int i, int i2, long j) throws IOException {
        if (j <= 0) {
            throw new IllegalArgumentException("maxSize <= 0");
        } else if (i2 > 0) {
            File file2 = new File(file, "journal.bkp");
            if (file2.exists()) {
                File file3 = new File(file, "journal");
                if (file3.exists()) {
                    file2.delete();
                } else {
                    renameTo(file2, file3, false);
                }
            }
            DiskLruCache diskLruCache = new DiskLruCache(file, i, i2, j);
            if (diskLruCache.journalFile.exists()) {
                try {
                    diskLruCache.readJournal();
                    diskLruCache.processJournal();
                    diskLruCache.journalWriter = Okio.buffer(Okio.sink((OutputStream) new FileOutputStream(diskLruCache.journalFile, true)));
                    return diskLruCache;
                } catch (IOException e) {
                    Platform platform = Platform.get();
                    platform.logW("DiskLruCache " + file + " is corrupt: " + e.getMessage() + ", removing");
                    diskLruCache.delete();
                }
            }
            file.mkdirs();
            DiskLruCache diskLruCache2 = new DiskLruCache(file, i, i2, j);
            diskLruCache2.rebuildJournal();
            return diskLruCache2;
        } else {
            throw new IllegalArgumentException("valueCount <= 0");
        }
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(4:16|17|18|19) */
    /* JADX WARNING: Code restructure failed: missing block: B:17:?, code lost:
        r9.redundantOpCount = r0 - r9.lruEntries.size();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0067, code lost:
        return;
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:16:0x005b */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:20:0x0068=Splitter:B:20:0x0068, B:16:0x005b=Splitter:B:16:0x005b} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void readJournal() throws java.io.IOException {
        /*
            r9 = this;
            java.lang.String r0 = ", "
            java.io.File r1 = r9.journalFile
            okio.Source r1 = okio.Okio.source((java.io.File) r1)
            okio.BufferedSource r1 = okio.Okio.buffer((okio.Source) r1)
            java.lang.String r2 = r1.readUtf8LineStrict()     // Catch:{ all -> 0x0096 }
            java.lang.String r3 = r1.readUtf8LineStrict()     // Catch:{ all -> 0x0096 }
            java.lang.String r4 = r1.readUtf8LineStrict()     // Catch:{ all -> 0x0096 }
            java.lang.String r5 = r1.readUtf8LineStrict()     // Catch:{ all -> 0x0096 }
            java.lang.String r6 = r1.readUtf8LineStrict()     // Catch:{ all -> 0x0096 }
            java.lang.String r7 = "libcore.io.DiskLruCache"
            boolean r7 = r7.equals(r2)     // Catch:{ all -> 0x0096 }
            if (r7 == 0) goto L_0x0068
            java.lang.String r7 = "1"
            boolean r7 = r7.equals(r3)     // Catch:{ all -> 0x0096 }
            if (r7 == 0) goto L_0x0068
            int r7 = r9.appVersion     // Catch:{ all -> 0x0096 }
            java.lang.String r7 = java.lang.Integer.toString(r7)     // Catch:{ all -> 0x0096 }
            boolean r4 = r7.equals(r4)     // Catch:{ all -> 0x0096 }
            if (r4 == 0) goto L_0x0068
            int r4 = r9.valueCount     // Catch:{ all -> 0x0096 }
            java.lang.String r4 = java.lang.Integer.toString(r4)     // Catch:{ all -> 0x0096 }
            boolean r4 = r4.equals(r5)     // Catch:{ all -> 0x0096 }
            if (r4 == 0) goto L_0x0068
            java.lang.String r4 = ""
            boolean r4 = r4.equals(r6)     // Catch:{ all -> 0x0096 }
            if (r4 == 0) goto L_0x0068
            r0 = 0
        L_0x0051:
            java.lang.String r2 = r1.readUtf8LineStrict()     // Catch:{ EOFException -> 0x005b }
            r9.readJournalLine(r2)     // Catch:{ EOFException -> 0x005b }
            int r0 = r0 + 1
            goto L_0x0051
        L_0x005b:
            java.util.LinkedHashMap<java.lang.String, com.squareup.okhttp.internal.DiskLruCache$Entry> r2 = r9.lruEntries     // Catch:{ all -> 0x0096 }
            int r2 = r2.size()     // Catch:{ all -> 0x0096 }
            int r0 = r0 - r2
            r9.redundantOpCount = r0     // Catch:{ all -> 0x0096 }
            com.squareup.okhttp.internal.Util.closeQuietly((java.io.Closeable) r1)
            return
        L_0x0068:
            java.io.IOException r4 = new java.io.IOException     // Catch:{ all -> 0x0096 }
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ all -> 0x0096 }
            r7.<init>()     // Catch:{ all -> 0x0096 }
            java.lang.String r8 = "unexpected journal header: ["
            r7.append(r8)     // Catch:{ all -> 0x0096 }
            r7.append(r2)     // Catch:{ all -> 0x0096 }
            r7.append(r0)     // Catch:{ all -> 0x0096 }
            r7.append(r3)     // Catch:{ all -> 0x0096 }
            r7.append(r0)     // Catch:{ all -> 0x0096 }
            r7.append(r5)     // Catch:{ all -> 0x0096 }
            r7.append(r0)     // Catch:{ all -> 0x0096 }
            r7.append(r6)     // Catch:{ all -> 0x0096 }
            java.lang.String r0 = "]"
            r7.append(r0)     // Catch:{ all -> 0x0096 }
            java.lang.String r0 = r7.toString()     // Catch:{ all -> 0x0096 }
            r4.<init>(r0)     // Catch:{ all -> 0x0096 }
            throw r4     // Catch:{ all -> 0x0096 }
        L_0x0096:
            r0 = move-exception
            com.squareup.okhttp.internal.Util.closeQuietly((java.io.Closeable) r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.squareup.okhttp.internal.DiskLruCache.readJournal():void");
    }

    private void readJournalLine(String str) throws IOException {
        String str2;
        int indexOf = str.indexOf(32);
        if (indexOf != -1) {
            int i = indexOf + 1;
            int indexOf2 = str.indexOf(32, i);
            if (indexOf2 == -1) {
                str2 = str.substring(i);
                if (indexOf == 6 && str.startsWith("REMOVE")) {
                    this.lruEntries.remove(str2);
                    return;
                }
            } else {
                str2 = str.substring(i, indexOf2);
            }
            Entry entry = this.lruEntries.get(str2);
            if (entry == null) {
                entry = new Entry(str2);
                this.lruEntries.put(str2, entry);
            }
            if (indexOf2 != -1 && indexOf == 5 && str.startsWith("CLEAN")) {
                String[] split = str.substring(indexOf2 + 1).split(" ");
                boolean unused = entry.readable = true;
                Editor unused2 = entry.currentEditor = null;
                entry.setLengths(split);
            } else if (indexOf2 == -1 && indexOf == 5 && str.startsWith("DIRTY")) {
                Editor unused3 = entry.currentEditor = new Editor(entry);
            } else if (indexOf2 != -1 || indexOf != 4 || !str.startsWith("READ")) {
                throw new IOException("unexpected journal line: " + str);
            }
        } else {
            throw new IOException("unexpected journal line: " + str);
        }
    }

    private void processJournal() throws IOException {
        deleteIfExists(this.journalFileTmp);
        Iterator<Entry> it = this.lruEntries.values().iterator();
        while (it.hasNext()) {
            Entry next = it.next();
            int i = 0;
            if (next.currentEditor == null) {
                while (i < this.valueCount) {
                    this.size += next.lengths[i];
                    i++;
                }
            } else {
                Editor unused = next.currentEditor = null;
                while (i < this.valueCount) {
                    deleteIfExists(next.getCleanFile(i));
                    deleteIfExists(next.getDirtyFile(i));
                    i++;
                }
                it.remove();
            }
        }
    }

    /* JADX INFO: finally extract failed */
    /* access modifiers changed from: private */
    public synchronized void rebuildJournal() throws IOException {
        if (this.journalWriter != null) {
            this.journalWriter.close();
        }
        BufferedSink buffer = Okio.buffer(Okio.sink((OutputStream) new FileOutputStream(this.journalFileTmp)));
        try {
            buffer.writeUtf8("libcore.io.DiskLruCache").writeByte(10);
            buffer.writeUtf8("1").writeByte(10);
            buffer.writeUtf8(Integer.toString(this.appVersion)).writeByte(10);
            buffer.writeUtf8(Integer.toString(this.valueCount)).writeByte(10);
            buffer.writeByte(10);
            for (Entry next : this.lruEntries.values()) {
                if (next.currentEditor != null) {
                    buffer.writeUtf8("DIRTY").writeByte(32);
                    buffer.writeUtf8(next.key);
                    buffer.writeByte(10);
                } else {
                    buffer.writeUtf8("CLEAN").writeByte(32);
                    buffer.writeUtf8(next.key);
                    buffer.writeUtf8(next.getLengths());
                    buffer.writeByte(10);
                }
            }
            buffer.close();
            if (this.journalFile.exists()) {
                renameTo(this.journalFile, this.journalFileBackup, true);
            }
            renameTo(this.journalFileTmp, this.journalFile, false);
            this.journalFileBackup.delete();
            this.journalWriter = Okio.buffer(Okio.sink((OutputStream) new FileOutputStream(this.journalFile, true)));
        } catch (Throwable th) {
            buffer.close();
            throw th;
        }
    }

    private static void deleteIfExists(File file) throws IOException {
        if (!file.delete() && file.exists()) {
            throw new IOException("failed to delete " + file);
        }
    }

    private static void renameTo(File file, File file2, boolean z) throws IOException {
        if (z) {
            deleteIfExists(file2);
        }
        if (!file.renameTo(file2)) {
            throw new IOException();
        }
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(4:32|33|28|27) */
    /* JADX WARNING: Code restructure failed: missing block: B:21:?, code lost:
        r11.redundantOpCount++;
        r11.journalWriter.writeUtf8("READ").writeByte(32).writeUtf8(r12).writeByte(10);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0055, code lost:
        if (journalRebuildRequired() == false) goto L_0x005e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0057, code lost:
        r11.executorService.execute(r11.cleanupRunnable);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0070, code lost:
        return new com.squareup.okhttp.internal.DiskLruCache.Snapshot(r11, r12, com.squareup.okhttp.internal.DiskLruCache.Entry.access$1200(r0), r8, com.squareup.okhttp.internal.DiskLruCache.Entry.access$1000(r0), (com.squareup.okhttp.internal.DiskLruCache.AnonymousClass1) null);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x0082, code lost:
        return null;
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:27:0x0071 */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0079  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized com.squareup.okhttp.internal.DiskLruCache.Snapshot get(java.lang.String r12) throws java.io.IOException {
        /*
            r11 = this;
            monitor-enter(r11)
            r11.checkNotClosed()     // Catch:{ all -> 0x0083 }
            r11.validateKey(r12)     // Catch:{ all -> 0x0083 }
            java.util.LinkedHashMap<java.lang.String, com.squareup.okhttp.internal.DiskLruCache$Entry> r0 = r11.lruEntries     // Catch:{ all -> 0x0083 }
            java.lang.Object r0 = r0.get(r12)     // Catch:{ all -> 0x0083 }
            com.squareup.okhttp.internal.DiskLruCache$Entry r0 = (com.squareup.okhttp.internal.DiskLruCache.Entry) r0     // Catch:{ all -> 0x0083 }
            r1 = 0
            if (r0 != 0) goto L_0x0014
            monitor-exit(r11)
            return r1
        L_0x0014:
            boolean r2 = r0.readable     // Catch:{ all -> 0x0083 }
            if (r2 != 0) goto L_0x001c
            monitor-exit(r11)
            return r1
        L_0x001c:
            int r2 = r11.valueCount     // Catch:{ all -> 0x0083 }
            java.io.InputStream[] r8 = new java.io.InputStream[r2]     // Catch:{ all -> 0x0083 }
            r2 = 0
            r3 = 0
        L_0x0022:
            int r4 = r11.valueCount     // Catch:{ FileNotFoundException -> 0x0071 }
            if (r3 >= r4) goto L_0x0034
            java.io.FileInputStream r4 = new java.io.FileInputStream     // Catch:{ FileNotFoundException -> 0x0071 }
            java.io.File r5 = r0.getCleanFile(r3)     // Catch:{ FileNotFoundException -> 0x0071 }
            r4.<init>(r5)     // Catch:{ FileNotFoundException -> 0x0071 }
            r8[r3] = r4     // Catch:{ FileNotFoundException -> 0x0071 }
            int r3 = r3 + 1
            goto L_0x0022
        L_0x0034:
            int r1 = r11.redundantOpCount     // Catch:{ all -> 0x0083 }
            int r1 = r1 + 1
            r11.redundantOpCount = r1     // Catch:{ all -> 0x0083 }
            okio.BufferedSink r1 = r11.journalWriter     // Catch:{ all -> 0x0083 }
            java.lang.String r2 = "READ"
            okio.BufferedSink r1 = r1.writeUtf8(r2)     // Catch:{ all -> 0x0083 }
            r2 = 32
            okio.BufferedSink r1 = r1.writeByte(r2)     // Catch:{ all -> 0x0083 }
            okio.BufferedSink r1 = r1.writeUtf8(r12)     // Catch:{ all -> 0x0083 }
            r2 = 10
            r1.writeByte(r2)     // Catch:{ all -> 0x0083 }
            boolean r1 = r11.journalRebuildRequired()     // Catch:{ all -> 0x0083 }
            if (r1 == 0) goto L_0x005e
            java.util.concurrent.ThreadPoolExecutor r1 = r11.executorService     // Catch:{ all -> 0x0083 }
            java.lang.Runnable r2 = r11.cleanupRunnable     // Catch:{ all -> 0x0083 }
            r1.execute(r2)     // Catch:{ all -> 0x0083 }
        L_0x005e:
            com.squareup.okhttp.internal.DiskLruCache$Snapshot r1 = new com.squareup.okhttp.internal.DiskLruCache$Snapshot     // Catch:{ all -> 0x0083 }
            long r6 = r0.sequenceNumber     // Catch:{ all -> 0x0083 }
            long[] r9 = r0.lengths     // Catch:{ all -> 0x0083 }
            r10 = 0
            r3 = r1
            r4 = r11
            r5 = r12
            r3.<init>(r5, r6, r8, r9)     // Catch:{ all -> 0x0083 }
            monitor-exit(r11)
            return r1
        L_0x0071:
            int r12 = r11.valueCount     // Catch:{ all -> 0x0083 }
            if (r2 >= r12) goto L_0x0081
            r12 = r8[r2]     // Catch:{ all -> 0x0083 }
            if (r12 == 0) goto L_0x0081
            r12 = r8[r2]     // Catch:{ all -> 0x0083 }
            com.squareup.okhttp.internal.Util.closeQuietly((java.io.Closeable) r12)     // Catch:{ all -> 0x0083 }
            int r2 = r2 + 1
            goto L_0x0071
        L_0x0081:
            monitor-exit(r11)
            return r1
        L_0x0083:
            r12 = move-exception
            monitor-exit(r11)
            throw r12
        */
        throw new UnsupportedOperationException("Method not decompiled: com.squareup.okhttp.internal.DiskLruCache.get(java.lang.String):com.squareup.okhttp.internal.DiskLruCache$Snapshot");
    }

    public Editor edit(String str) throws IOException {
        return edit(str, -1);
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0021, code lost:
        return null;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized com.squareup.okhttp.internal.DiskLruCache.Editor edit(java.lang.String r6, long r7) throws java.io.IOException {
        /*
            r5 = this;
            monitor-enter(r5)
            r5.checkNotClosed()     // Catch:{ all -> 0x005d }
            r5.validateKey(r6)     // Catch:{ all -> 0x005d }
            java.util.LinkedHashMap<java.lang.String, com.squareup.okhttp.internal.DiskLruCache$Entry> r0 = r5.lruEntries     // Catch:{ all -> 0x005d }
            java.lang.Object r0 = r0.get(r6)     // Catch:{ all -> 0x005d }
            com.squareup.okhttp.internal.DiskLruCache$Entry r0 = (com.squareup.okhttp.internal.DiskLruCache.Entry) r0     // Catch:{ all -> 0x005d }
            r1 = -1
            r3 = 0
            int r4 = (r7 > r1 ? 1 : (r7 == r1 ? 0 : -1))
            if (r4 == 0) goto L_0x0022
            if (r0 == 0) goto L_0x0020
            long r1 = r0.sequenceNumber     // Catch:{ all -> 0x005d }
            int r4 = (r1 > r7 ? 1 : (r1 == r7 ? 0 : -1))
            if (r4 == 0) goto L_0x0022
        L_0x0020:
            monitor-exit(r5)
            return r3
        L_0x0022:
            if (r0 != 0) goto L_0x002f
            com.squareup.okhttp.internal.DiskLruCache$Entry r0 = new com.squareup.okhttp.internal.DiskLruCache$Entry     // Catch:{ all -> 0x005d }
            r0.<init>(r6)     // Catch:{ all -> 0x005d }
            java.util.LinkedHashMap<java.lang.String, com.squareup.okhttp.internal.DiskLruCache$Entry> r7 = r5.lruEntries     // Catch:{ all -> 0x005d }
            r7.put(r6, r0)     // Catch:{ all -> 0x005d }
            goto L_0x0037
        L_0x002f:
            com.squareup.okhttp.internal.DiskLruCache$Editor r7 = r0.currentEditor     // Catch:{ all -> 0x005d }
            if (r7 == 0) goto L_0x0037
            monitor-exit(r5)
            return r3
        L_0x0037:
            com.squareup.okhttp.internal.DiskLruCache$Editor r7 = new com.squareup.okhttp.internal.DiskLruCache$Editor     // Catch:{ all -> 0x005d }
            r7.<init>(r0)     // Catch:{ all -> 0x005d }
            com.squareup.okhttp.internal.DiskLruCache.Editor unused = r0.currentEditor = r7     // Catch:{ all -> 0x005d }
            okio.BufferedSink r8 = r5.journalWriter     // Catch:{ all -> 0x005d }
            java.lang.String r0 = "DIRTY"
            okio.BufferedSink r8 = r8.writeUtf8(r0)     // Catch:{ all -> 0x005d }
            r0 = 32
            okio.BufferedSink r8 = r8.writeByte(r0)     // Catch:{ all -> 0x005d }
            okio.BufferedSink r6 = r8.writeUtf8(r6)     // Catch:{ all -> 0x005d }
            r8 = 10
            r6.writeByte(r8)     // Catch:{ all -> 0x005d }
            okio.BufferedSink r6 = r5.journalWriter     // Catch:{ all -> 0x005d }
            r6.flush()     // Catch:{ all -> 0x005d }
            monitor-exit(r5)
            return r7
        L_0x005d:
            r6 = move-exception
            monitor-exit(r5)
            throw r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.squareup.okhttp.internal.DiskLruCache.edit(java.lang.String, long):com.squareup.okhttp.internal.DiskLruCache$Editor");
    }

    public File getDirectory() {
        return this.directory;
    }

    public synchronized long getMaxSize() {
        return this.maxSize;
    }

    public synchronized void setMaxSize(long j) {
        this.maxSize = j;
        this.executorService.execute(this.cleanupRunnable);
    }

    public synchronized long size() {
        return this.size;
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x0105, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void completeEdit(com.squareup.okhttp.internal.DiskLruCache.Editor r10, boolean r11) throws java.io.IOException {
        /*
            r9 = this;
            monitor-enter(r9)
            com.squareup.okhttp.internal.DiskLruCache$Entry r0 = r10.entry     // Catch:{ all -> 0x010c }
            com.squareup.okhttp.internal.DiskLruCache$Editor r1 = r0.currentEditor     // Catch:{ all -> 0x010c }
            if (r1 != r10) goto L_0x0106
            r1 = 0
            if (r11 == 0) goto L_0x004d
            boolean r2 = r0.readable     // Catch:{ all -> 0x010c }
            if (r2 != 0) goto L_0x004d
            r2 = 0
        L_0x0015:
            int r3 = r9.valueCount     // Catch:{ all -> 0x010c }
            if (r2 >= r3) goto L_0x004d
            boolean[] r3 = r10.written     // Catch:{ all -> 0x010c }
            boolean r3 = r3[r2]     // Catch:{ all -> 0x010c }
            if (r3 == 0) goto L_0x0033
            java.io.File r3 = r0.getDirtyFile(r2)     // Catch:{ all -> 0x010c }
            boolean r3 = r3.exists()     // Catch:{ all -> 0x010c }
            if (r3 != 0) goto L_0x0030
            r10.abort()     // Catch:{ all -> 0x010c }
            monitor-exit(r9)
            return
        L_0x0030:
            int r2 = r2 + 1
            goto L_0x0015
        L_0x0033:
            r10.abort()     // Catch:{ all -> 0x010c }
            java.lang.IllegalStateException r10 = new java.lang.IllegalStateException     // Catch:{ all -> 0x010c }
            java.lang.StringBuilder r11 = new java.lang.StringBuilder     // Catch:{ all -> 0x010c }
            r11.<init>()     // Catch:{ all -> 0x010c }
            java.lang.String r0 = "Newly created entry didn't create value for index "
            r11.append(r0)     // Catch:{ all -> 0x010c }
            r11.append(r2)     // Catch:{ all -> 0x010c }
            java.lang.String r11 = r11.toString()     // Catch:{ all -> 0x010c }
            r10.<init>(r11)     // Catch:{ all -> 0x010c }
            throw r10     // Catch:{ all -> 0x010c }
        L_0x004d:
            int r10 = r9.valueCount     // Catch:{ all -> 0x010c }
            if (r1 >= r10) goto L_0x0081
            java.io.File r10 = r0.getDirtyFile(r1)     // Catch:{ all -> 0x010c }
            if (r11 == 0) goto L_0x007b
            boolean r2 = r10.exists()     // Catch:{ all -> 0x010c }
            if (r2 == 0) goto L_0x007e
            java.io.File r2 = r0.getCleanFile(r1)     // Catch:{ all -> 0x010c }
            r10.renameTo(r2)     // Catch:{ all -> 0x010c }
            long[] r10 = r0.lengths     // Catch:{ all -> 0x010c }
            r3 = r10[r1]     // Catch:{ all -> 0x010c }
            long r5 = r2.length()     // Catch:{ all -> 0x010c }
            long[] r10 = r0.lengths     // Catch:{ all -> 0x010c }
            r10[r1] = r5     // Catch:{ all -> 0x010c }
            long r7 = r9.size     // Catch:{ all -> 0x010c }
            long r7 = r7 - r3
            long r7 = r7 + r5
            r9.size = r7     // Catch:{ all -> 0x010c }
            goto L_0x007e
        L_0x007b:
            deleteIfExists(r10)     // Catch:{ all -> 0x010c }
        L_0x007e:
            int r1 = r1 + 1
            goto L_0x004d
        L_0x0081:
            int r10 = r9.redundantOpCount     // Catch:{ all -> 0x010c }
            r1 = 1
            int r10 = r10 + r1
            r9.redundantOpCount = r10     // Catch:{ all -> 0x010c }
            r10 = 0
            com.squareup.okhttp.internal.DiskLruCache.Editor unused = r0.currentEditor = r10     // Catch:{ all -> 0x010c }
            boolean r10 = r0.readable     // Catch:{ all -> 0x010c }
            r10 = r10 | r11
            r2 = 10
            r3 = 32
            if (r10 == 0) goto L_0x00c8
            boolean unused = r0.readable = r1     // Catch:{ all -> 0x010c }
            okio.BufferedSink r10 = r9.journalWriter     // Catch:{ all -> 0x010c }
            java.lang.String r1 = "CLEAN"
            okio.BufferedSink r10 = r10.writeUtf8(r1)     // Catch:{ all -> 0x010c }
            r10.writeByte(r3)     // Catch:{ all -> 0x010c }
            okio.BufferedSink r10 = r9.journalWriter     // Catch:{ all -> 0x010c }
            java.lang.String r1 = r0.key     // Catch:{ all -> 0x010c }
            r10.writeUtf8(r1)     // Catch:{ all -> 0x010c }
            okio.BufferedSink r10 = r9.journalWriter     // Catch:{ all -> 0x010c }
            java.lang.String r1 = r0.getLengths()     // Catch:{ all -> 0x010c }
            r10.writeUtf8(r1)     // Catch:{ all -> 0x010c }
            okio.BufferedSink r10 = r9.journalWriter     // Catch:{ all -> 0x010c }
            r10.writeByte(r2)     // Catch:{ all -> 0x010c }
            if (r11 == 0) goto L_0x00ea
            long r10 = r9.nextSequenceNumber     // Catch:{ all -> 0x010c }
            r1 = 1
            long r1 = r1 + r10
            r9.nextSequenceNumber = r1     // Catch:{ all -> 0x010c }
            long unused = r0.sequenceNumber = r10     // Catch:{ all -> 0x010c }
            goto L_0x00ea
        L_0x00c8:
            java.util.LinkedHashMap<java.lang.String, com.squareup.okhttp.internal.DiskLruCache$Entry> r10 = r9.lruEntries     // Catch:{ all -> 0x010c }
            java.lang.String r11 = r0.key     // Catch:{ all -> 0x010c }
            r10.remove(r11)     // Catch:{ all -> 0x010c }
            okio.BufferedSink r10 = r9.journalWriter     // Catch:{ all -> 0x010c }
            java.lang.String r11 = "REMOVE"
            okio.BufferedSink r10 = r10.writeUtf8(r11)     // Catch:{ all -> 0x010c }
            r10.writeByte(r3)     // Catch:{ all -> 0x010c }
            okio.BufferedSink r10 = r9.journalWriter     // Catch:{ all -> 0x010c }
            java.lang.String r11 = r0.key     // Catch:{ all -> 0x010c }
            r10.writeUtf8(r11)     // Catch:{ all -> 0x010c }
            okio.BufferedSink r10 = r9.journalWriter     // Catch:{ all -> 0x010c }
            r10.writeByte(r2)     // Catch:{ all -> 0x010c }
        L_0x00ea:
            okio.BufferedSink r10 = r9.journalWriter     // Catch:{ all -> 0x010c }
            r10.flush()     // Catch:{ all -> 0x010c }
            long r10 = r9.size     // Catch:{ all -> 0x010c }
            long r0 = r9.maxSize     // Catch:{ all -> 0x010c }
            int r2 = (r10 > r0 ? 1 : (r10 == r0 ? 0 : -1))
            if (r2 > 0) goto L_0x00fd
            boolean r10 = r9.journalRebuildRequired()     // Catch:{ all -> 0x010c }
            if (r10 == 0) goto L_0x0104
        L_0x00fd:
            java.util.concurrent.ThreadPoolExecutor r10 = r9.executorService     // Catch:{ all -> 0x010c }
            java.lang.Runnable r11 = r9.cleanupRunnable     // Catch:{ all -> 0x010c }
            r10.execute(r11)     // Catch:{ all -> 0x010c }
        L_0x0104:
            monitor-exit(r9)
            return
        L_0x0106:
            java.lang.IllegalStateException r10 = new java.lang.IllegalStateException     // Catch:{ all -> 0x010c }
            r10.<init>()     // Catch:{ all -> 0x010c }
            throw r10     // Catch:{ all -> 0x010c }
        L_0x010c:
            r10 = move-exception
            monitor-exit(r9)
            throw r10
        */
        throw new UnsupportedOperationException("Method not decompiled: com.squareup.okhttp.internal.DiskLruCache.completeEdit(com.squareup.okhttp.internal.DiskLruCache$Editor, boolean):void");
    }

    /* access modifiers changed from: private */
    public boolean journalRebuildRequired() {
        int i = this.redundantOpCount;
        return i >= 2000 && i >= this.lruEntries.size();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x006a, code lost:
        return true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x006c, code lost:
        return false;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized boolean remove(java.lang.String r8) throws java.io.IOException {
        /*
            r7 = this;
            monitor-enter(r7)
            r7.checkNotClosed()     // Catch:{ all -> 0x006d }
            r7.validateKey(r8)     // Catch:{ all -> 0x006d }
            java.util.LinkedHashMap<java.lang.String, com.squareup.okhttp.internal.DiskLruCache$Entry> r0 = r7.lruEntries     // Catch:{ all -> 0x006d }
            java.lang.Object r0 = r0.get(r8)     // Catch:{ all -> 0x006d }
            com.squareup.okhttp.internal.DiskLruCache$Entry r0 = (com.squareup.okhttp.internal.DiskLruCache.Entry) r0     // Catch:{ all -> 0x006d }
            r1 = 0
            if (r0 == 0) goto L_0x006b
            com.squareup.okhttp.internal.DiskLruCache$Editor r2 = r0.currentEditor     // Catch:{ all -> 0x006d }
            if (r2 == 0) goto L_0x0019
            goto L_0x006b
        L_0x0019:
            int r2 = r7.valueCount     // Catch:{ all -> 0x006d }
            if (r1 >= r2) goto L_0x003a
            java.io.File r2 = r0.getCleanFile(r1)     // Catch:{ all -> 0x006d }
            deleteIfExists(r2)     // Catch:{ all -> 0x006d }
            long r2 = r7.size     // Catch:{ all -> 0x006d }
            long[] r4 = r0.lengths     // Catch:{ all -> 0x006d }
            r5 = r4[r1]     // Catch:{ all -> 0x006d }
            long r2 = r2 - r5
            r7.size = r2     // Catch:{ all -> 0x006d }
            long[] r2 = r0.lengths     // Catch:{ all -> 0x006d }
            r3 = 0
            r2[r1] = r3     // Catch:{ all -> 0x006d }
            int r1 = r1 + 1
            goto L_0x0019
        L_0x003a:
            int r0 = r7.redundantOpCount     // Catch:{ all -> 0x006d }
            r1 = 1
            int r0 = r0 + r1
            r7.redundantOpCount = r0     // Catch:{ all -> 0x006d }
            okio.BufferedSink r0 = r7.journalWriter     // Catch:{ all -> 0x006d }
            java.lang.String r2 = "REMOVE"
            okio.BufferedSink r0 = r0.writeUtf8(r2)     // Catch:{ all -> 0x006d }
            r2 = 32
            okio.BufferedSink r0 = r0.writeByte(r2)     // Catch:{ all -> 0x006d }
            okio.BufferedSink r0 = r0.writeUtf8(r8)     // Catch:{ all -> 0x006d }
            r2 = 10
            r0.writeByte(r2)     // Catch:{ all -> 0x006d }
            java.util.LinkedHashMap<java.lang.String, com.squareup.okhttp.internal.DiskLruCache$Entry> r0 = r7.lruEntries     // Catch:{ all -> 0x006d }
            r0.remove(r8)     // Catch:{ all -> 0x006d }
            boolean r8 = r7.journalRebuildRequired()     // Catch:{ all -> 0x006d }
            if (r8 == 0) goto L_0x0069
            java.util.concurrent.ThreadPoolExecutor r8 = r7.executorService     // Catch:{ all -> 0x006d }
            java.lang.Runnable r0 = r7.cleanupRunnable     // Catch:{ all -> 0x006d }
            r8.execute(r0)     // Catch:{ all -> 0x006d }
        L_0x0069:
            monitor-exit(r7)
            return r1
        L_0x006b:
            monitor-exit(r7)
            return r1
        L_0x006d:
            r8 = move-exception
            monitor-exit(r7)
            throw r8
        */
        throw new UnsupportedOperationException("Method not decompiled: com.squareup.okhttp.internal.DiskLruCache.remove(java.lang.String):boolean");
    }

    public boolean isClosed() {
        return this.journalWriter == null;
    }

    private void checkNotClosed() {
        if (this.journalWriter == null) {
            throw new IllegalStateException("cache is closed");
        }
    }

    public synchronized void flush() throws IOException {
        checkNotClosed();
        trimToSize();
        this.journalWriter.flush();
    }

    public synchronized void close() throws IOException {
        if (this.journalWriter != null) {
            for (Object obj : this.lruEntries.values().toArray()) {
                Entry entry = (Entry) obj;
                if (entry.currentEditor != null) {
                    entry.currentEditor.abort();
                }
            }
            trimToSize();
            this.journalWriter.close();
            this.journalWriter = null;
        }
    }

    /* access modifiers changed from: private */
    public void trimToSize() throws IOException {
        while (this.size > this.maxSize) {
            remove((String) this.lruEntries.entrySet().iterator().next().getKey());
        }
    }

    public void delete() throws IOException {
        close();
        Util.deleteContents(this.directory);
    }

    private void validateKey(String str) {
        if (!LEGAL_KEY_PATTERN.matcher(str).matches()) {
            throw new IllegalArgumentException("keys must match regex [a-z0-9_-]{1,64}: \"" + str + "\"");
        }
    }

    /* access modifiers changed from: private */
    public static String inputStreamToString(InputStream inputStream) throws IOException {
        try {
            Buffer buffer = new Buffer();
            buffer.writeAll(Okio.source(inputStream));
            return buffer.readUtf8();
        } finally {
            Util.closeQuietly((Closeable) inputStream);
        }
    }

    public final class Snapshot implements Closeable {
        private final InputStream[] ins;
        private final String key;
        private final long[] lengths;
        private final long sequenceNumber;

        private Snapshot(String str, long j, InputStream[] inputStreamArr, long[] jArr) {
            this.key = str;
            this.sequenceNumber = j;
            this.ins = inputStreamArr;
            this.lengths = jArr;
        }

        public Editor edit() throws IOException {
            return DiskLruCache.this.edit(this.key, this.sequenceNumber);
        }

        public InputStream getInputStream(int i) {
            return this.ins[i];
        }

        public String getString(int i) throws IOException {
            return DiskLruCache.inputStreamToString(getInputStream(i));
        }

        public long getLength(int i) {
            return this.lengths[i];
        }

        public void close() {
            for (InputStream closeQuietly : this.ins) {
                Util.closeQuietly((Closeable) closeQuietly);
            }
        }
    }

    public final class Editor {
        private boolean committed;
        /* access modifiers changed from: private */
        public final Entry entry;
        /* access modifiers changed from: private */
        public boolean hasErrors;
        /* access modifiers changed from: private */
        public final boolean[] written;

        private Editor(Entry entry2) {
            this.entry = entry2;
            this.written = entry2.readable ? null : new boolean[DiskLruCache.this.valueCount];
        }

        public InputStream newInputStream(int i) throws IOException {
            synchronized (DiskLruCache.this) {
                if (this.entry.currentEditor != this) {
                    throw new IllegalStateException();
                } else if (!this.entry.readable) {
                    return null;
                } else {
                    try {
                        FileInputStream fileInputStream = new FileInputStream(this.entry.getCleanFile(i));
                        return fileInputStream;
                    } catch (FileNotFoundException unused) {
                        return null;
                    }
                }
            }
        }

        public String getString(int i) throws IOException {
            InputStream newInputStream = newInputStream(i);
            if (newInputStream != null) {
                return DiskLruCache.inputStreamToString(newInputStream);
            }
            return null;
        }

        /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
        /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x0024 */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public java.io.OutputStream newOutputStream(int r4) throws java.io.IOException {
            /*
                r3 = this;
                com.squareup.okhttp.internal.DiskLruCache r0 = com.squareup.okhttp.internal.DiskLruCache.this
                monitor-enter(r0)
                com.squareup.okhttp.internal.DiskLruCache$Entry r1 = r3.entry     // Catch:{ all -> 0x0046 }
                com.squareup.okhttp.internal.DiskLruCache$Editor r1 = r1.currentEditor     // Catch:{ all -> 0x0046 }
                if (r1 != r3) goto L_0x0040
                com.squareup.okhttp.internal.DiskLruCache$Entry r1 = r3.entry     // Catch:{ all -> 0x0046 }
                boolean r1 = r1.readable     // Catch:{ all -> 0x0046 }
                if (r1 != 0) goto L_0x0018
                boolean[] r1 = r3.written     // Catch:{ all -> 0x0046 }
                r2 = 1
                r1[r4] = r2     // Catch:{ all -> 0x0046 }
            L_0x0018:
                com.squareup.okhttp.internal.DiskLruCache$Entry r1 = r3.entry     // Catch:{ all -> 0x0046 }
                java.io.File r4 = r1.getDirtyFile(r4)     // Catch:{ all -> 0x0046 }
                java.io.FileOutputStream r1 = new java.io.FileOutputStream     // Catch:{ FileNotFoundException -> 0x0024 }
                r1.<init>(r4)     // Catch:{ FileNotFoundException -> 0x0024 }
                goto L_0x0032
            L_0x0024:
                com.squareup.okhttp.internal.DiskLruCache r1 = com.squareup.okhttp.internal.DiskLruCache.this     // Catch:{ all -> 0x0046 }
                java.io.File r1 = r1.directory     // Catch:{ all -> 0x0046 }
                r1.mkdirs()     // Catch:{ all -> 0x0046 }
                java.io.FileOutputStream r1 = new java.io.FileOutputStream     // Catch:{ FileNotFoundException -> 0x003a }
                r1.<init>(r4)     // Catch:{ FileNotFoundException -> 0x003a }
            L_0x0032:
                com.squareup.okhttp.internal.DiskLruCache$Editor$FaultHidingOutputStream r4 = new com.squareup.okhttp.internal.DiskLruCache$Editor$FaultHidingOutputStream     // Catch:{ all -> 0x0046 }
                r2 = 0
                r4.<init>(r1)     // Catch:{ all -> 0x0046 }
                monitor-exit(r0)     // Catch:{ all -> 0x0046 }
                return r4
            L_0x003a:
                java.io.OutputStream r4 = com.squareup.okhttp.internal.DiskLruCache.NULL_OUTPUT_STREAM     // Catch:{ all -> 0x0046 }
                monitor-exit(r0)     // Catch:{ all -> 0x0046 }
                return r4
            L_0x0040:
                java.lang.IllegalStateException r4 = new java.lang.IllegalStateException     // Catch:{ all -> 0x0046 }
                r4.<init>()     // Catch:{ all -> 0x0046 }
                throw r4     // Catch:{ all -> 0x0046 }
            L_0x0046:
                r4 = move-exception
                monitor-exit(r0)     // Catch:{ all -> 0x0046 }
                throw r4
            */
            throw new UnsupportedOperationException("Method not decompiled: com.squareup.okhttp.internal.DiskLruCache.Editor.newOutputStream(int):java.io.OutputStream");
        }

        public void set(int i, String str) throws IOException {
            BufferedSink buffer = Okio.buffer(Okio.sink(newOutputStream(i)));
            buffer.writeUtf8(str);
            buffer.close();
        }

        public void commit() throws IOException {
            if (this.hasErrors) {
                DiskLruCache.this.completeEdit(this, false);
                DiskLruCache.this.remove(this.entry.key);
            } else {
                DiskLruCache.this.completeEdit(this, true);
            }
            this.committed = true;
        }

        public void abort() throws IOException {
            DiskLruCache.this.completeEdit(this, false);
        }

        public void abortUnlessCommitted() {
            if (!this.committed) {
                try {
                    abort();
                } catch (IOException unused) {
                }
            }
        }

        private class FaultHidingOutputStream extends FilterOutputStream {
            private FaultHidingOutputStream(OutputStream outputStream) {
                super(outputStream);
            }

            public void write(int i) {
                try {
                    this.out.write(i);
                } catch (IOException unused) {
                    boolean unused2 = Editor.this.hasErrors = true;
                }
            }

            public void write(byte[] bArr, int i, int i2) {
                try {
                    this.out.write(bArr, i, i2);
                } catch (IOException unused) {
                    boolean unused2 = Editor.this.hasErrors = true;
                }
            }

            public void close() {
                try {
                    this.out.close();
                } catch (IOException unused) {
                    boolean unused2 = Editor.this.hasErrors = true;
                }
            }

            public void flush() {
                try {
                    this.out.flush();
                } catch (IOException unused) {
                    boolean unused2 = Editor.this.hasErrors = true;
                }
            }
        }
    }

    private final class Entry {
        /* access modifiers changed from: private */
        public Editor currentEditor;
        /* access modifiers changed from: private */
        public final String key;
        /* access modifiers changed from: private */
        public final long[] lengths;
        /* access modifiers changed from: private */
        public boolean readable;
        /* access modifiers changed from: private */
        public long sequenceNumber;

        private Entry(String str) {
            this.key = str;
            this.lengths = new long[DiskLruCache.this.valueCount];
        }

        public String getLengths() throws IOException {
            StringBuilder sb = new StringBuilder();
            for (long append : this.lengths) {
                sb.append(' ');
                sb.append(append);
            }
            return sb.toString();
        }

        /* access modifiers changed from: private */
        public void setLengths(String[] strArr) throws IOException {
            if (strArr.length == DiskLruCache.this.valueCount) {
                int i = 0;
                while (i < strArr.length) {
                    try {
                        this.lengths[i] = Long.parseLong(strArr[i]);
                        i++;
                    } catch (NumberFormatException unused) {
                        throw invalidLengths(strArr);
                    }
                }
                return;
            }
            throw invalidLengths(strArr);
        }

        private IOException invalidLengths(String[] strArr) throws IOException {
            throw new IOException("unexpected journal line: " + Arrays.toString(strArr));
        }

        public File getCleanFile(int i) {
            File access$1900 = DiskLruCache.this.directory;
            return new File(access$1900, this.key + "." + i);
        }

        public File getDirtyFile(int i) {
            File access$1900 = DiskLruCache.this.directory;
            return new File(access$1900, this.key + "." + i + ".tmp");
        }
    }
}
