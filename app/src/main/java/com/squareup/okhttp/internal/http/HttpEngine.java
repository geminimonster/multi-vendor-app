package com.squareup.okhttp.internal.http;

import androidx.browser.trusted.sharing.ShareTarget;
import com.squareup.okhttp.Address;
import com.squareup.okhttp.Connection;
import com.squareup.okhttp.Headers;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Protocol;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;
import com.squareup.okhttp.ResponseBody;
import com.squareup.okhttp.Route;
import com.squareup.okhttp.internal.Dns;
import com.squareup.okhttp.internal.Internal;
import com.squareup.okhttp.internal.InternalCache;
import com.squareup.okhttp.internal.Util;
import com.squareup.okhttp.internal.http.CacheStrategy;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.net.CacheRequest;
import java.net.CookieHandler;
import java.net.ProtocolException;
import java.net.Proxy;
import java.net.URL;
import java.net.UnknownHostException;
import java.security.cert.CertificateException;
import java.util.Date;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLHandshakeException;
import javax.net.ssl.SSLSocketFactory;
import okio.Buffer;
import okio.BufferedSink;
import okio.BufferedSource;
import okio.GzipSource;
import okio.Okio;
import okio.Sink;
import okio.Source;

public final class HttpEngine {
    private static final ResponseBody EMPTY_BODY = new ResponseBody() {
        public long contentLength() {
            return 0;
        }

        public MediaType contentType() {
            return null;
        }

        public BufferedSource source() {
            return new Buffer();
        }
    };
    public static final int MAX_REDIRECTS = 20;
    public final boolean bufferRequestBody;
    private BufferedSink bufferedRequestBody;
    private Response cacheResponse;
    private CacheStrategy cacheStrategy;
    final OkHttpClient client;
    private Connection connection;
    private Request networkRequest;
    private Response networkResponse;
    private final Response priorResponse;
    private Sink requestBodyOut;
    private BufferedSource responseBody;
    private InputStream responseBodyBytes;
    private Source responseTransferSource;
    private Route route;
    private RouteSelector routeSelector;
    long sentRequestMillis = -1;
    private CacheRequest storeRequest;
    private boolean transparentGzip;
    private Transport transport;
    private final Request userRequest;
    private Response userResponse;

    public HttpEngine(OkHttpClient okHttpClient, Request request, boolean z, Connection connection2, RouteSelector routeSelector2, RetryableSink retryableSink, Response response) {
        this.client = okHttpClient;
        this.userRequest = request;
        this.bufferRequestBody = z;
        this.connection = connection2;
        this.routeSelector = routeSelector2;
        this.requestBodyOut = retryableSink;
        this.priorResponse = response;
        if (connection2 != null) {
            Internal.instance.setOwner(connection2, this);
            this.route = connection2.getRoute();
            return;
        }
        this.route = null;
    }

    public void sendRequest() throws IOException {
        if (this.cacheStrategy == null) {
            if (this.transport == null) {
                Request networkRequest2 = networkRequest(this.userRequest);
                InternalCache internalCache = Internal.instance.internalCache(this.client);
                Response response = internalCache != null ? internalCache.get(networkRequest2) : null;
                CacheStrategy cacheStrategy2 = new CacheStrategy.Factory(System.currentTimeMillis(), networkRequest2, response).get();
                this.cacheStrategy = cacheStrategy2;
                this.networkRequest = cacheStrategy2.networkRequest;
                this.cacheResponse = this.cacheStrategy.cacheResponse;
                if (internalCache != null) {
                    internalCache.trackResponse(this.cacheStrategy);
                }
                if (response != null && this.cacheResponse == null) {
                    Util.closeQuietly((Closeable) response.body());
                }
                Request request = this.networkRequest;
                if (request != null) {
                    if (this.connection == null) {
                        connect(request);
                    }
                    if (Internal.instance.getOwner(this.connection) == this || Internal.instance.isSpdy(this.connection)) {
                        this.transport = Internal.instance.newTransport(this.connection, this);
                        if (hasRequestBody() && this.requestBodyOut == null) {
                            this.requestBodyOut = this.transport.createRequestBody(networkRequest2);
                            return;
                        }
                        return;
                    }
                    throw new AssertionError();
                }
                if (this.connection != null) {
                    Internal.instance.recycle(this.client.getConnectionPool(), this.connection);
                    this.connection = null;
                }
                Response response2 = this.cacheResponse;
                if (response2 != null) {
                    this.userResponse = response2.newBuilder().request(this.userRequest).priorResponse(stripBody(this.priorResponse)).cacheResponse(stripBody(this.cacheResponse)).build();
                } else {
                    this.userResponse = new Response.Builder().request(this.userRequest).priorResponse(stripBody(this.priorResponse)).protocol(Protocol.HTTP_1_1).code(504).message("Unsatisfiable Request (only-if-cached)").body(EMPTY_BODY).build();
                }
                if (this.userResponse.body() != null) {
                    initContentStream(this.userResponse.body().source());
                    return;
                }
                return;
            }
            throw new IllegalStateException();
        }
    }

    private static Response stripBody(Response response) {
        return (response == null || response.body() == null) ? response : response.newBuilder().body((ResponseBody) null).build();
    }

    private void connect(Request request) throws IOException {
        HostnameVerifier hostnameVerifier;
        SSLSocketFactory sSLSocketFactory;
        if (this.connection == null) {
            if (this.routeSelector == null) {
                String host = request.url().getHost();
                if (host == null || host.length() == 0) {
                    throw new UnknownHostException(request.url().toString());
                }
                if (request.isHttps()) {
                    sSLSocketFactory = this.client.getSslSocketFactory();
                    hostnameVerifier = this.client.getHostnameVerifier();
                } else {
                    sSLSocketFactory = null;
                    hostnameVerifier = null;
                }
                this.routeSelector = new RouteSelector(new Address(host, Util.getEffectivePort(request.url()), this.client.getSocketFactory(), sSLSocketFactory, hostnameVerifier, this.client.getAuthenticator(), this.client.getProxy(), this.client.getProtocols()), request.uri(), this.client.getProxySelector(), this.client.getConnectionPool(), Dns.DEFAULT, Internal.instance.routeDatabase(this.client));
            }
            this.connection = this.routeSelector.next(request.method());
            Internal.instance.setOwner(this.connection, this);
            if (!Internal.instance.isConnected(this.connection)) {
                Internal.instance.connect(this.connection, this.client.getConnectTimeout(), this.client.getReadTimeout(), this.client.getWriteTimeout(), tunnelRequest(this.connection, request));
                if (Internal.instance.isSpdy(this.connection)) {
                    Internal.instance.share(this.client.getConnectionPool(), this.connection);
                }
                Internal.instance.routeDatabase(this.client).connected(this.connection.getRoute());
            }
            Internal.instance.setTimeouts(this.connection, this.client.getReadTimeout(), this.client.getWriteTimeout());
            this.route = this.connection.getRoute();
            return;
        }
        throw new IllegalStateException();
    }

    public void writingRequestHeaders() {
        if (this.sentRequestMillis == -1) {
            this.sentRequestMillis = System.currentTimeMillis();
            return;
        }
        throw new IllegalStateException();
    }

    /* access modifiers changed from: package-private */
    public boolean hasRequestBody() {
        return HttpMethod.hasRequestBody(this.userRequest.method()) && !Util.emptySink().equals(this.requestBodyOut);
    }

    public Sink getRequestBody() {
        if (this.cacheStrategy != null) {
            return this.requestBodyOut;
        }
        throw new IllegalStateException();
    }

    public BufferedSink getBufferedRequestBody() {
        BufferedSink bufferedSink = this.bufferedRequestBody;
        if (bufferedSink != null) {
            return bufferedSink;
        }
        Sink requestBody = getRequestBody();
        if (requestBody == null) {
            return null;
        }
        BufferedSink buffer = Okio.buffer(requestBody);
        this.bufferedRequestBody = buffer;
        return buffer;
    }

    public boolean hasResponse() {
        return this.userResponse != null;
    }

    public Request getRequest() {
        return this.userRequest;
    }

    public Response getResponse() {
        Response response = this.userResponse;
        if (response != null) {
            return response;
        }
        throw new IllegalStateException();
    }

    public BufferedSource getResponseBody() {
        if (this.userResponse != null) {
            return this.responseBody;
        }
        throw new IllegalStateException();
    }

    public InputStream getResponseBodyBytes() {
        InputStream inputStream = this.responseBodyBytes;
        if (inputStream != null) {
            return inputStream;
        }
        InputStream inputStream2 = Okio.buffer((Source) getResponseBody()).inputStream();
        this.responseBodyBytes = inputStream2;
        return inputStream2;
    }

    public Connection getConnection() {
        return this.connection;
    }

    public HttpEngine recover(IOException iOException, Sink sink) {
        Connection connection2;
        RouteSelector routeSelector2 = this.routeSelector;
        if (!(routeSelector2 == null || (connection2 = this.connection) == null)) {
            routeSelector2.connectFailed(connection2, iOException);
        }
        boolean z = sink == null || (sink instanceof RetryableSink);
        if (this.routeSelector == null && this.connection == null) {
            return null;
        }
        RouteSelector routeSelector3 = this.routeSelector;
        if ((routeSelector3 != null && !routeSelector3.hasNext()) || !isRecoverable(iOException) || !z) {
            return null;
        }
        return new HttpEngine(this.client, this.userRequest, this.bufferRequestBody, close(), this.routeSelector, (RetryableSink) sink, this.priorResponse);
    }

    public HttpEngine recover(IOException iOException) {
        return recover(iOException, this.requestBodyOut);
    }

    private boolean isRecoverable(IOException iOException) {
        boolean z = (iOException instanceof SSLHandshakeException) && (iOException.getCause() instanceof CertificateException);
        boolean z2 = iOException instanceof ProtocolException;
        if (z || z2) {
            return false;
        }
        return true;
    }

    public Route getRoute() {
        return this.route;
    }

    private void maybeCache() throws IOException {
        InternalCache internalCache = Internal.instance.internalCache(this.client);
        if (internalCache != null) {
            if (CacheStrategy.isCacheable(this.userResponse, this.networkRequest)) {
                this.storeRequest = internalCache.put(stripBody(this.userResponse));
            } else if (HttpMethod.invalidatesCache(this.networkRequest.method())) {
                try {
                    internalCache.remove(this.networkRequest);
                } catch (IOException unused) {
                }
            }
        }
    }

    public void releaseConnection() throws IOException {
        Transport transport2 = this.transport;
        if (!(transport2 == null || this.connection == null)) {
            transport2.releaseConnectionOnIdle();
        }
        this.connection = null;
    }

    public void disconnect() {
        Transport transport2 = this.transport;
        if (transport2 != null) {
            try {
                transport2.disconnect(this);
            } catch (IOException unused) {
            }
        }
    }

    public Connection close() {
        BufferedSink bufferedSink = this.bufferedRequestBody;
        if (bufferedSink != null) {
            Util.closeQuietly((Closeable) bufferedSink);
        } else {
            Sink sink = this.requestBodyOut;
            if (sink != null) {
                Util.closeQuietly((Closeable) sink);
            }
        }
        BufferedSource bufferedSource = this.responseBody;
        if (bufferedSource == null) {
            Connection connection2 = this.connection;
            if (connection2 != null) {
                Util.closeQuietly(connection2.getSocket());
            }
            this.connection = null;
            return null;
        }
        Util.closeQuietly((Closeable) bufferedSource);
        Util.closeQuietly((Closeable) this.responseBodyBytes);
        Transport transport2 = this.transport;
        if (transport2 == null || this.connection == null || transport2.canReuseConnection()) {
            if (this.connection != null && !Internal.instance.clearOwner(this.connection)) {
                this.connection = null;
            }
            Connection connection3 = this.connection;
            this.connection = null;
            return connection3;
        }
        Util.closeQuietly(this.connection.getSocket());
        this.connection = null;
        return null;
    }

    private void initContentStream(Source source) throws IOException {
        this.responseTransferSource = source;
        if (!this.transparentGzip || !"gzip".equalsIgnoreCase(this.userResponse.header("Content-Encoding"))) {
            this.responseBody = Okio.buffer(source);
            return;
        }
        this.userResponse = this.userResponse.newBuilder().removeHeader("Content-Encoding").removeHeader("Content-Length").build();
        this.responseBody = Okio.buffer((Source) new GzipSource(source));
    }

    public boolean hasResponseBody() {
        if (this.userRequest.method().equals("HEAD")) {
            return false;
        }
        int code = this.userResponse.code();
        if (((code >= 100 && code < 200) || code == 204 || code == 304) && OkHeaders.contentLength(this.networkResponse) == -1 && !"chunked".equalsIgnoreCase(this.networkResponse.header("Transfer-Encoding"))) {
            return false;
        }
        return true;
    }

    private Request networkRequest(Request request) throws IOException {
        Request.Builder newBuilder = request.newBuilder();
        if (request.header("Host") == null) {
            newBuilder.header("Host", hostHeader(request.url()));
        }
        Connection connection2 = this.connection;
        if ((connection2 == null || connection2.getProtocol() != Protocol.HTTP_1_0) && request.header("Connection") == null) {
            newBuilder.header("Connection", "Keep-Alive");
        }
        if (request.header("Accept-Encoding") == null) {
            this.transparentGzip = true;
            newBuilder.header("Accept-Encoding", "gzip");
        }
        CookieHandler cookieHandler = this.client.getCookieHandler();
        if (cookieHandler != null) {
            OkHeaders.addCookies(newBuilder, cookieHandler.get(request.uri(), OkHeaders.toMultimap(newBuilder.build().headers(), (String) null)));
        }
        return newBuilder.build();
    }

    public static String hostHeader(URL url) {
        if (Util.getEffectivePort(url) == Util.getDefaultPort(url.getProtocol())) {
            return url.getHost();
        }
        return url.getHost() + ":" + url.getPort();
    }

    public void readResponse() throws IOException {
        if (this.userResponse == null) {
            if (this.networkRequest == null && this.cacheResponse == null) {
                throw new IllegalStateException("call sendRequest() first!");
            } else if (this.networkRequest != null) {
                BufferedSink bufferedSink = this.bufferedRequestBody;
                if (bufferedSink != null && bufferedSink.buffer().size() > 0) {
                    this.bufferedRequestBody.flush();
                }
                if (this.sentRequestMillis == -1) {
                    if (OkHeaders.contentLength(this.networkRequest) == -1) {
                        Sink sink = this.requestBodyOut;
                        if (sink instanceof RetryableSink) {
                            this.networkRequest = this.networkRequest.newBuilder().header("Content-Length", Long.toString(((RetryableSink) sink).contentLength())).build();
                        }
                    }
                    this.transport.writeRequestHeaders(this.networkRequest);
                }
                Sink sink2 = this.requestBodyOut;
                if (sink2 != null) {
                    BufferedSink bufferedSink2 = this.bufferedRequestBody;
                    if (bufferedSink2 != null) {
                        bufferedSink2.close();
                    } else {
                        sink2.close();
                    }
                    if ((this.requestBodyOut instanceof RetryableSink) && !Util.emptySink().equals(this.requestBodyOut)) {
                        this.transport.writeRequestBody((RetryableSink) this.requestBodyOut);
                    }
                }
                this.transport.flushRequest();
                this.networkResponse = this.transport.readResponseHeaders().request(this.networkRequest).handshake(this.connection.getHandshake()).header(OkHeaders.SENT_MILLIS, Long.toString(this.sentRequestMillis)).header(OkHeaders.RECEIVED_MILLIS, Long.toString(System.currentTimeMillis())).build();
                Internal.instance.setProtocol(this.connection, this.networkResponse.protocol());
                receiveHeaders(this.networkResponse.headers());
                Response response = this.cacheResponse;
                if (response != null) {
                    if (validate(response, this.networkResponse)) {
                        this.userResponse = this.cacheResponse.newBuilder().request(this.userRequest).priorResponse(stripBody(this.priorResponse)).headers(combine(this.cacheResponse.headers(), this.networkResponse.headers())).cacheResponse(stripBody(this.cacheResponse)).networkResponse(stripBody(this.networkResponse)).build();
                        this.transport.emptyTransferStream();
                        releaseConnection();
                        InternalCache internalCache = Internal.instance.internalCache(this.client);
                        internalCache.trackConditionalCacheHit();
                        internalCache.update(this.cacheResponse, stripBody(this.userResponse));
                        if (this.cacheResponse.body() != null) {
                            initContentStream(this.cacheResponse.body().source());
                            return;
                        }
                        return;
                    }
                    Util.closeQuietly((Closeable) this.cacheResponse.body());
                }
                this.userResponse = this.networkResponse.newBuilder().request(this.userRequest).priorResponse(stripBody(this.priorResponse)).cacheResponse(stripBody(this.cacheResponse)).networkResponse(stripBody(this.networkResponse)).build();
                if (!hasResponseBody()) {
                    Source transferStream = this.transport.getTransferStream(this.storeRequest);
                    this.responseTransferSource = transferStream;
                    this.responseBody = Okio.buffer(transferStream);
                    return;
                }
                maybeCache();
                initContentStream(this.transport.getTransferStream(this.storeRequest));
            }
        }
    }

    private static boolean validate(Response response, Response response2) {
        Date date;
        if (response2.code() == 304) {
            return true;
        }
        Date date2 = response.headers().getDate("Last-Modified");
        if (date2 == null || (date = response2.headers().getDate("Last-Modified")) == null || date.getTime() >= date2.getTime()) {
            return false;
        }
        return true;
    }

    private static Headers combine(Headers headers, Headers headers2) throws IOException {
        Headers.Builder builder = new Headers.Builder();
        for (int i = 0; i < headers.size(); i++) {
            String name = headers.name(i);
            String value = headers.value(i);
            if ((!"Warning".equals(name) || !value.startsWith("1")) && (!OkHeaders.isEndToEnd(name) || headers2.get(name) == null)) {
                builder.add(name, value);
            }
        }
        for (int i2 = 0; i2 < headers2.size(); i2++) {
            String name2 = headers2.name(i2);
            if (OkHeaders.isEndToEnd(name2)) {
                builder.add(name2, headers2.value(i2));
            }
        }
        return builder.build();
    }

    private Request tunnelRequest(Connection connection2, Request request) throws IOException {
        String str;
        if (!connection2.getRoute().requiresTunnel()) {
            return null;
        }
        String host = request.url().getHost();
        int effectivePort = Util.getEffectivePort(request.url());
        if (effectivePort == Util.getDefaultPort("https")) {
            str = host;
        } else {
            str = host + ":" + effectivePort;
        }
        Request.Builder header = new Request.Builder().url(new URL("https", host, effectivePort, "/")).header("Host", str).header("Proxy-Connection", "Keep-Alive");
        String header2 = request.header("User-Agent");
        if (header2 != null) {
            header.header("User-Agent", header2);
        }
        String header3 = request.header("Proxy-Authorization");
        if (header3 != null) {
            header.header("Proxy-Authorization", header3);
        }
        return header.build();
    }

    public void receiveHeaders(Headers headers) throws IOException {
        CookieHandler cookieHandler = this.client.getCookieHandler();
        if (cookieHandler != null) {
            cookieHandler.put(this.userRequest.uri(), OkHeaders.toMultimap(headers, (String) null));
        }
    }

    public Request followUpRequest() throws IOException {
        if (this.userResponse != null) {
            Proxy proxy = getRoute() != null ? getRoute().getProxy() : this.client.getProxy();
            int code = this.userResponse.code();
            if (code != 307) {
                if (code != 401) {
                    if (code != 407) {
                        switch (code) {
                            case 300:
                            case 301:
                            case 302:
                            case 303:
                                break;
                            default:
                                return null;
                        }
                    } else if (proxy.type() != Proxy.Type.HTTP) {
                        throw new ProtocolException("Received HTTP_PROXY_AUTH (407) code while not using proxy");
                    }
                }
                return OkHeaders.processAuthHeader(this.client.getAuthenticator(), this.userResponse, proxy);
            } else if (!this.userRequest.method().equals(ShareTarget.METHOD_GET) && !this.userRequest.method().equals("HEAD")) {
                return null;
            }
            String header = this.userResponse.header("Location");
            if (header == null) {
                return null;
            }
            URL url = new URL(this.userRequest.url(), header);
            if (!url.getProtocol().equals("https") && !url.getProtocol().equals("http")) {
                return null;
            }
            if (!url.getProtocol().equals(this.userRequest.url().getProtocol()) && !this.client.getFollowSslRedirects()) {
                return null;
            }
            Request.Builder newBuilder = this.userRequest.newBuilder();
            if (HttpMethod.hasRequestBody(this.userRequest.method())) {
                newBuilder.method(ShareTarget.METHOD_GET, (RequestBody) null);
                newBuilder.removeHeader("Transfer-Encoding");
                newBuilder.removeHeader("Content-Length");
                newBuilder.removeHeader("Content-Type");
            }
            if (!sameConnection(url)) {
                newBuilder.removeHeader("Authorization");
            }
            return newBuilder.url(url).build();
        }
        throw new IllegalStateException();
    }

    public boolean sameConnection(URL url) {
        URL url2 = this.userRequest.url();
        return url2.getHost().equals(url.getHost()) && Util.getEffectivePort(url2) == Util.getEffectivePort(url) && url2.getProtocol().equals(url.getProtocol());
    }
}
