package com.squareup.okhttp.internal.huc;

import com.squareup.okhttp.Handshake;
import com.squareup.okhttp.Headers;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;
import com.squareup.okhttp.ResponseBody;
import com.squareup.okhttp.internal.Util;
import com.squareup.okhttp.internal.http.OkHeaders;
import com.squareup.okhttp.internal.http.StatusLine;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.CacheResponse;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.SecureCacheResponse;
import java.net.URI;
import java.net.URLConnection;
import java.security.Principal;
import java.security.cert.Certificate;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLPeerUnverifiedException;
import javax.net.ssl.SSLSocketFactory;
import okio.BufferedSource;
import okio.Okio;

public final class JavaApiConverter {
    private JavaApiConverter() {
    }

    public static Response createOkResponse(URI uri, URLConnection uRLConnection) throws IOException {
        HttpURLConnection httpURLConnection = (HttpURLConnection) uRLConnection;
        Response.Builder builder = new Response.Builder();
        Certificate[] certificateArr = null;
        builder.request(createOkRequest(uri, httpURLConnection.getRequestMethod(), (Map<String, List<String>>) null));
        StatusLine parse = StatusLine.parse(extractStatusLine(httpURLConnection));
        builder.protocol(parse.protocol);
        builder.code(parse.code);
        builder.message(parse.message);
        Headers extractOkResponseHeaders = extractOkResponseHeaders(httpURLConnection);
        builder.headers(extractOkResponseHeaders);
        builder.body(createOkBody(extractOkResponseHeaders, uRLConnection.getInputStream()));
        if (httpURLConnection instanceof HttpsURLConnection) {
            HttpsURLConnection httpsURLConnection = (HttpsURLConnection) httpURLConnection;
            try {
                certificateArr = httpsURLConnection.getServerCertificates();
            } catch (SSLPeerUnverifiedException unused) {
            }
            builder.handshake(Handshake.get(httpsURLConnection.getCipherSuite(), nullSafeImmutableList(certificateArr), nullSafeImmutableList(httpsURLConnection.getLocalCertificates())));
        }
        return builder.build();
    }

    static Response createOkResponse(Request request, CacheResponse cacheResponse) throws IOException {
        List<Certificate> list;
        Response.Builder builder = new Response.Builder();
        builder.request(request);
        StatusLine parse = StatusLine.parse(extractStatusLine(cacheResponse));
        builder.protocol(parse.protocol);
        builder.code(parse.code);
        builder.message(parse.message);
        Headers extractOkHeaders = extractOkHeaders(cacheResponse);
        builder.headers(extractOkHeaders);
        builder.body(createOkBody(extractOkHeaders, cacheResponse.getBody()));
        if (cacheResponse instanceof SecureCacheResponse) {
            SecureCacheResponse secureCacheResponse = (SecureCacheResponse) cacheResponse;
            try {
                list = secureCacheResponse.getServerCertificateChain();
            } catch (SSLPeerUnverifiedException unused) {
                list = Collections.emptyList();
            }
            List<Certificate> localCertificateChain = secureCacheResponse.getLocalCertificateChain();
            if (localCertificateChain == null) {
                localCertificateChain = Collections.emptyList();
            }
            builder.handshake(Handshake.get(secureCacheResponse.getCipherSuite(), list, localCertificateChain));
        }
        return builder.build();
    }

    public static Request createOkRequest(URI uri, String str, Map<String, List<String>> map) {
        Request.Builder method = new Request.Builder().url(uri.toString()).method(str, (RequestBody) null);
        if (map != null) {
            method.headers(extractOkHeaders(map));
        }
        return method.build();
    }

    public static CacheResponse createJavaCacheResponse(final Response response) {
        final Headers headers = response.headers();
        final ResponseBody body = response.body();
        if (!response.request().isHttps()) {
            return new CacheResponse() {
                public Map<String, List<String>> getHeaders() throws IOException {
                    return OkHeaders.toMultimap(headers, StatusLine.get(response).toString());
                }

                public InputStream getBody() throws IOException {
                    ResponseBody responseBody = body;
                    if (responseBody == null) {
                        return null;
                    }
                    return responseBody.byteStream();
                }
            };
        }
        final Handshake handshake = response.handshake();
        return new SecureCacheResponse() {
            public String getCipherSuite() {
                Handshake handshake = handshake;
                if (handshake != null) {
                    return handshake.cipherSuite();
                }
                return null;
            }

            public List<Certificate> getLocalCertificateChain() {
                Handshake handshake = handshake;
                if (handshake == null) {
                    return null;
                }
                List<Certificate> localCertificates = handshake.localCertificates();
                if (localCertificates.size() > 0) {
                    return localCertificates;
                }
                return null;
            }

            public List<Certificate> getServerCertificateChain() throws SSLPeerUnverifiedException {
                Handshake handshake = handshake;
                if (handshake == null) {
                    return null;
                }
                List<Certificate> peerCertificates = handshake.peerCertificates();
                if (peerCertificates.size() > 0) {
                    return peerCertificates;
                }
                return null;
            }

            public Principal getPeerPrincipal() throws SSLPeerUnverifiedException {
                Handshake handshake = handshake;
                if (handshake == null) {
                    return null;
                }
                return handshake.peerPrincipal();
            }

            public Principal getLocalPrincipal() {
                Handshake handshake = handshake;
                if (handshake == null) {
                    return null;
                }
                return handshake.localPrincipal();
            }

            public Map<String, List<String>> getHeaders() throws IOException {
                return OkHeaders.toMultimap(headers, StatusLine.get(response).toString());
            }

            public InputStream getBody() throws IOException {
                ResponseBody responseBody = body;
                if (responseBody == null) {
                    return null;
                }
                return responseBody.byteStream();
            }
        };
    }

    static HttpURLConnection createJavaUrlConnection(Response response) {
        if (response.request().isHttps()) {
            return new CacheHttpsURLConnection(new CacheHttpURLConnection(response));
        }
        return new CacheHttpURLConnection(response);
    }

    static Map<String, List<String>> extractJavaHeaders(Request request) {
        return OkHeaders.toMultimap(request.headers(), (String) null);
    }

    private static Headers extractOkHeaders(CacheResponse cacheResponse) throws IOException {
        return extractOkHeaders(cacheResponse.getHeaders());
    }

    private static Headers extractOkResponseHeaders(HttpURLConnection httpURLConnection) {
        return extractOkHeaders((Map<String, List<String>>) httpURLConnection.getHeaderFields());
    }

    static Headers extractOkHeaders(Map<String, List<String>> map) {
        Headers.Builder builder = new Headers.Builder();
        for (Map.Entry next : map.entrySet()) {
            String str = (String) next.getKey();
            if (str != null) {
                for (String add : (List) next.getValue()) {
                    builder.add(str, add);
                }
            }
        }
        return builder.build();
    }

    private static String extractStatusLine(HttpURLConnection httpURLConnection) {
        return httpURLConnection.getHeaderField((String) null);
    }

    private static String extractStatusLine(CacheResponse cacheResponse) throws IOException {
        return extractStatusLine(cacheResponse.getHeaders());
    }

    static String extractStatusLine(Map<String, List<String>> map) {
        List list = map.get((Object) null);
        if (list == null || list.size() == 0) {
            return null;
        }
        return (String) list.get(0);
    }

    private static ResponseBody createOkBody(final Headers headers, InputStream inputStream) {
        final BufferedSource buffer = Okio.buffer(Okio.source(inputStream));
        return new ResponseBody() {
            public MediaType contentType() {
                String str = headers.get("Content-Type");
                if (str == null) {
                    return null;
                }
                return MediaType.parse(str);
            }

            public long contentLength() {
                return OkHeaders.contentLength(headers);
            }

            public BufferedSource source() {
                return buffer;
            }
        };
    }

    private static final class CacheHttpURLConnection extends HttpURLConnection {
        private final Request request;
        /* access modifiers changed from: private */
        public final Response response;

        public boolean getAllowUserInteraction() {
            return false;
        }

        public int getConnectTimeout() {
            return 0;
        }

        public boolean getDoInput() {
            return true;
        }

        public InputStream getErrorStream() {
            return null;
        }

        public long getIfModifiedSince() {
            return 0;
        }

        public int getReadTimeout() {
            return 0;
        }

        public boolean usingProxy() {
            return false;
        }

        public CacheHttpURLConnection(Response response2) {
            super(response2.request().url());
            this.request = response2.request();
            this.response = response2;
            boolean z = true;
            this.connected = true;
            this.doOutput = response2.body() != null ? false : z;
            this.method = this.request.method();
        }

        public void connect() throws IOException {
            throw JavaApiConverter.throwRequestModificationException();
        }

        public void disconnect() {
            throw JavaApiConverter.throwRequestModificationException();
        }

        public void setRequestProperty(String str, String str2) {
            throw JavaApiConverter.throwRequestModificationException();
        }

        public void addRequestProperty(String str, String str2) {
            throw JavaApiConverter.throwRequestModificationException();
        }

        public String getRequestProperty(String str) {
            return this.request.header(str);
        }

        public Map<String, List<String>> getRequestProperties() {
            throw JavaApiConverter.throwRequestHeaderAccessException();
        }

        public void setFixedLengthStreamingMode(int i) {
            throw JavaApiConverter.throwRequestModificationException();
        }

        public void setFixedLengthStreamingMode(long j) {
            throw JavaApiConverter.throwRequestModificationException();
        }

        public void setChunkedStreamingMode(int i) {
            throw JavaApiConverter.throwRequestModificationException();
        }

        public void setInstanceFollowRedirects(boolean z) {
            throw JavaApiConverter.throwRequestModificationException();
        }

        public boolean getInstanceFollowRedirects() {
            return super.getInstanceFollowRedirects();
        }

        public void setRequestMethod(String str) throws ProtocolException {
            throw JavaApiConverter.throwRequestModificationException();
        }

        public String getRequestMethod() {
            return this.request.method();
        }

        public String getHeaderFieldKey(int i) {
            if (i < 0) {
                throw new IllegalArgumentException("Invalid header index: " + i);
            } else if (i == 0) {
                return null;
            } else {
                return this.response.headers().name(i - 1);
            }
        }

        public String getHeaderField(int i) {
            if (i < 0) {
                throw new IllegalArgumentException("Invalid header index: " + i);
            } else if (i == 0) {
                return StatusLine.get(this.response).toString();
            } else {
                return this.response.headers().value(i - 1);
            }
        }

        public String getHeaderField(String str) {
            return str == null ? StatusLine.get(this.response).toString() : this.response.headers().get(str);
        }

        public Map<String, List<String>> getHeaderFields() {
            return OkHeaders.toMultimap(this.response.headers(), StatusLine.get(this.response).toString());
        }

        public int getResponseCode() throws IOException {
            return this.response.code();
        }

        public String getResponseMessage() throws IOException {
            return this.response.message();
        }

        public void setConnectTimeout(int i) {
            throw JavaApiConverter.throwRequestModificationException();
        }

        public void setReadTimeout(int i) {
            throw JavaApiConverter.throwRequestModificationException();
        }

        public Object getContent() throws IOException {
            throw JavaApiConverter.throwResponseBodyAccessException();
        }

        public Object getContent(Class[] clsArr) throws IOException {
            throw JavaApiConverter.throwResponseBodyAccessException();
        }

        public InputStream getInputStream() throws IOException {
            throw JavaApiConverter.throwResponseBodyAccessException();
        }

        public OutputStream getOutputStream() throws IOException {
            throw JavaApiConverter.throwRequestModificationException();
        }

        public void setDoInput(boolean z) {
            throw JavaApiConverter.throwRequestModificationException();
        }

        public void setDoOutput(boolean z) {
            throw JavaApiConverter.throwRequestModificationException();
        }

        public boolean getDoOutput() {
            return this.request.body() != null;
        }

        public void setAllowUserInteraction(boolean z) {
            throw JavaApiConverter.throwRequestModificationException();
        }

        public void setUseCaches(boolean z) {
            throw JavaApiConverter.throwRequestModificationException();
        }

        public boolean getUseCaches() {
            return super.getUseCaches();
        }

        public void setIfModifiedSince(long j) {
            throw JavaApiConverter.throwRequestModificationException();
        }

        public boolean getDefaultUseCaches() {
            return super.getDefaultUseCaches();
        }

        public void setDefaultUseCaches(boolean z) {
            super.setDefaultUseCaches(z);
        }
    }

    private static final class CacheHttpsURLConnection extends DelegatingHttpsURLConnection {
        private final CacheHttpURLConnection delegate;

        public CacheHttpsURLConnection(CacheHttpURLConnection cacheHttpURLConnection) {
            super(cacheHttpURLConnection);
            this.delegate = cacheHttpURLConnection;
        }

        /* access modifiers changed from: protected */
        public Handshake handshake() {
            return this.delegate.response.handshake();
        }

        public void setHostnameVerifier(HostnameVerifier hostnameVerifier) {
            throw JavaApiConverter.throwRequestModificationException();
        }

        public HostnameVerifier getHostnameVerifier() {
            throw JavaApiConverter.throwRequestSslAccessException();
        }

        public void setSSLSocketFactory(SSLSocketFactory sSLSocketFactory) {
            throw JavaApiConverter.throwRequestModificationException();
        }

        public SSLSocketFactory getSSLSocketFactory() {
            throw JavaApiConverter.throwRequestSslAccessException();
        }

        public long getContentLengthLong() {
            return this.delegate.getContentLengthLong();
        }

        public void setFixedLengthStreamingMode(long j) {
            this.delegate.setFixedLengthStreamingMode(j);
        }

        public long getHeaderFieldLong(String str, long j) {
            return this.delegate.getHeaderFieldLong(str, j);
        }
    }

    /* access modifiers changed from: private */
    public static RuntimeException throwRequestModificationException() {
        throw new UnsupportedOperationException("ResponseCache cannot modify the request.");
    }

    /* access modifiers changed from: private */
    public static RuntimeException throwRequestHeaderAccessException() {
        throw new UnsupportedOperationException("ResponseCache cannot access request headers");
    }

    /* access modifiers changed from: private */
    public static RuntimeException throwRequestSslAccessException() {
        throw new UnsupportedOperationException("ResponseCache cannot access SSL internals");
    }

    /* access modifiers changed from: private */
    public static RuntimeException throwResponseBodyAccessException() {
        throw new UnsupportedOperationException("ResponseCache cannot access the response body.");
    }

    private static <T> List<T> nullSafeImmutableList(T[] tArr) {
        return tArr == null ? Collections.emptyList() : Util.immutableList(tArr);
    }
}
