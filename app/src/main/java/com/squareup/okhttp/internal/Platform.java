package com.squareup.okhttp.internal;

import com.squareup.okhttp.Protocol;
import com.squareup.okhttp.internal.http.RouteSelector;
import java.io.IOException;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.net.ssl.SSLSocket;
import okio.Buffer;

public class Platform {
    private static final Platform PLATFORM = findPlatform();

    public String getPrefix() {
        return "OkHttp";
    }

    public String getSelectedProtocol(SSLSocket sSLSocket) {
        return null;
    }

    public void setProtocols(SSLSocket sSLSocket, List<Protocol> list) {
    }

    public void tagSocket(Socket socket) throws SocketException {
    }

    public void untagSocket(Socket socket) throws SocketException {
    }

    public static Platform get() {
        return PLATFORM;
    }

    public void logW(String str) {
        System.out.println(str);
    }

    public URI toUriLenient(URL url) throws URISyntaxException {
        return url.toURI();
    }

    public void configureTls(SSLSocket sSLSocket, String str, String str2) {
        if (str2.equals(RouteSelector.SSL_V3)) {
            sSLSocket.setEnabledProtocols(new String[]{RouteSelector.SSL_V3});
        }
    }

    public void connectSocket(Socket socket, InetSocketAddress inetSocketAddress, int i) throws IOException {
        socket.connect(inetSocketAddress, i);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:33:0x006d, code lost:
        r2 = "org.eclipse.jetty.alpn.ALPN";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:?, code lost:
        r3 = java.lang.Class.forName(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x0074, code lost:
        r2 = "org.eclipse.jetty.npn.NextProtoNego";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:?, code lost:
        r3 = java.lang.Class.forName(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x00df, code lost:
        return new com.squareup.okhttp.internal.Platform();
     */
    /* JADX WARNING: Removed duplicated region for block: B:34:? A[ExcHandler: NoSuchMethodException (unused java.lang.NoSuchMethodException), SYNTHETIC, Splitter:B:6:0x000c] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static com.squareup.okhttp.internal.Platform findPlatform() {
        /*
            r0 = 1
            r1 = 0
            java.lang.String r2 = "com.android.org.conscrypt.OpenSSLSocketImpl"
            java.lang.Class r2 = java.lang.Class.forName(r2)     // Catch:{ ClassNotFoundException -> 0x000a }
        L_0x0008:
            r4 = r2
            goto L_0x0011
        L_0x000a:
            java.lang.String r2 = "org.apache.harmony.xnet.provider.jsse.OpenSSLSocketImpl"
            java.lang.Class r2 = java.lang.Class.forName(r2)     // Catch:{ NoSuchMethodException -> 0x006d, NoSuchMethodException -> 0x006d }
            goto L_0x0008
        L_0x0011:
            java.lang.String r2 = "setUseSessionTickets"
            java.lang.Class[] r3 = new java.lang.Class[r0]     // Catch:{ NoSuchMethodException -> 0x006d, NoSuchMethodException -> 0x006d }
            java.lang.Class r5 = java.lang.Boolean.TYPE     // Catch:{ NoSuchMethodException -> 0x006d, NoSuchMethodException -> 0x006d }
            r3[r1] = r5     // Catch:{ NoSuchMethodException -> 0x006d, NoSuchMethodException -> 0x006d }
            java.lang.reflect.Method r5 = r4.getMethod(r2, r3)     // Catch:{ NoSuchMethodException -> 0x006d, NoSuchMethodException -> 0x006d }
            java.lang.String r2 = "setHostname"
            java.lang.Class[] r3 = new java.lang.Class[r0]     // Catch:{ NoSuchMethodException -> 0x006d, NoSuchMethodException -> 0x006d }
            java.lang.Class<java.lang.String> r6 = java.lang.String.class
            r3[r1] = r6     // Catch:{ NoSuchMethodException -> 0x006d, NoSuchMethodException -> 0x006d }
            java.lang.reflect.Method r6 = r4.getMethod(r2, r3)     // Catch:{ NoSuchMethodException -> 0x006d, NoSuchMethodException -> 0x006d }
            r2 = 0
            java.lang.String r3 = "android.net.TrafficStats"
            java.lang.Class r3 = java.lang.Class.forName(r3)     // Catch:{ ClassNotFoundException | NoSuchMethodException -> 0x004b }
            java.lang.String r7 = "tagSocket"
            java.lang.Class[] r8 = new java.lang.Class[r0]     // Catch:{ ClassNotFoundException | NoSuchMethodException -> 0x004b }
            java.lang.Class<java.net.Socket> r9 = java.net.Socket.class
            r8[r1] = r9     // Catch:{ ClassNotFoundException | NoSuchMethodException -> 0x004b }
            java.lang.reflect.Method r7 = r3.getMethod(r7, r8)     // Catch:{ ClassNotFoundException | NoSuchMethodException -> 0x004b }
            java.lang.String r8 = "untagSocket"
            java.lang.Class[] r9 = new java.lang.Class[r0]     // Catch:{ ClassNotFoundException | NoSuchMethodException -> 0x004c }
            java.lang.Class<java.net.Socket> r10 = java.net.Socket.class
            r9[r1] = r10     // Catch:{ ClassNotFoundException | NoSuchMethodException -> 0x004c }
            java.lang.reflect.Method r3 = r3.getMethod(r8, r9)     // Catch:{ ClassNotFoundException | NoSuchMethodException -> 0x004c }
            r8 = r3
            goto L_0x004d
        L_0x004b:
            r7 = r2
        L_0x004c:
            r8 = r2
        L_0x004d:
            java.lang.String r3 = "setNpnProtocols"
            java.lang.Class[] r9 = new java.lang.Class[r0]     // Catch:{ NoSuchMethodException -> 0x0062 }
            java.lang.Class<byte[]> r10 = byte[].class
            r9[r1] = r10     // Catch:{ NoSuchMethodException -> 0x0062 }
            java.lang.reflect.Method r3 = r4.getMethod(r3, r9)     // Catch:{ NoSuchMethodException -> 0x0062 }
            java.lang.String r9 = "getNpnSelectedProtocol"
            java.lang.Class[] r10 = new java.lang.Class[r1]     // Catch:{ NoSuchMethodException -> 0x0063 }
            java.lang.reflect.Method r2 = r4.getMethod(r9, r10)     // Catch:{ NoSuchMethodException -> 0x0063 }
            goto L_0x0063
        L_0x0062:
            r3 = r2
        L_0x0063:
            r10 = r2
            r9 = r3
            com.squareup.okhttp.internal.Platform$Android r2 = new com.squareup.okhttp.internal.Platform$Android     // Catch:{ NoSuchMethodException -> 0x006d, NoSuchMethodException -> 0x006d }
            r11 = 0
            r3 = r2
            r3.<init>(r4, r5, r6, r7, r8, r9, r10)     // Catch:{ NoSuchMethodException -> 0x006d, NoSuchMethodException -> 0x006d }
            return r2
        L_0x006d:
            java.lang.String r2 = "org.eclipse.jetty.alpn.ALPN"
            java.lang.Class r3 = java.lang.Class.forName(r2)     // Catch:{ ClassNotFoundException -> 0x0074 }
            goto L_0x007a
        L_0x0074:
            java.lang.String r2 = "org.eclipse.jetty.npn.NextProtoNego"
            java.lang.Class r3 = java.lang.Class.forName(r2)     // Catch:{ ClassNotFoundException | NoSuchMethodException -> 0x00da }
        L_0x007a:
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ ClassNotFoundException | NoSuchMethodException -> 0x00da }
            r4.<init>()     // Catch:{ ClassNotFoundException | NoSuchMethodException -> 0x00da }
            r4.append(r2)     // Catch:{ ClassNotFoundException | NoSuchMethodException -> 0x00da }
            java.lang.String r5 = "$Provider"
            r4.append(r5)     // Catch:{ ClassNotFoundException | NoSuchMethodException -> 0x00da }
            java.lang.String r4 = r4.toString()     // Catch:{ ClassNotFoundException | NoSuchMethodException -> 0x00da }
            java.lang.Class r4 = java.lang.Class.forName(r4)     // Catch:{ ClassNotFoundException | NoSuchMethodException -> 0x00da }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ ClassNotFoundException | NoSuchMethodException -> 0x00da }
            r5.<init>()     // Catch:{ ClassNotFoundException | NoSuchMethodException -> 0x00da }
            r5.append(r2)     // Catch:{ ClassNotFoundException | NoSuchMethodException -> 0x00da }
            java.lang.String r6 = "$ClientProvider"
            r5.append(r6)     // Catch:{ ClassNotFoundException | NoSuchMethodException -> 0x00da }
            java.lang.String r5 = r5.toString()     // Catch:{ ClassNotFoundException | NoSuchMethodException -> 0x00da }
            java.lang.Class r5 = java.lang.Class.forName(r5)     // Catch:{ ClassNotFoundException | NoSuchMethodException -> 0x00da }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ ClassNotFoundException | NoSuchMethodException -> 0x00da }
            r6.<init>()     // Catch:{ ClassNotFoundException | NoSuchMethodException -> 0x00da }
            r6.append(r2)     // Catch:{ ClassNotFoundException | NoSuchMethodException -> 0x00da }
            java.lang.String r2 = "$ServerProvider"
            r6.append(r2)     // Catch:{ ClassNotFoundException | NoSuchMethodException -> 0x00da }
            java.lang.String r2 = r6.toString()     // Catch:{ ClassNotFoundException | NoSuchMethodException -> 0x00da }
            java.lang.Class r2 = java.lang.Class.forName(r2)     // Catch:{ ClassNotFoundException | NoSuchMethodException -> 0x00da }
            java.lang.String r6 = "put"
            r7 = 2
            java.lang.Class[] r7 = new java.lang.Class[r7]     // Catch:{ ClassNotFoundException | NoSuchMethodException -> 0x00da }
            java.lang.Class<javax.net.ssl.SSLSocket> r8 = javax.net.ssl.SSLSocket.class
            r7[r1] = r8     // Catch:{ ClassNotFoundException | NoSuchMethodException -> 0x00da }
            r7[r0] = r4     // Catch:{ ClassNotFoundException | NoSuchMethodException -> 0x00da }
            java.lang.reflect.Method r4 = r3.getMethod(r6, r7)     // Catch:{ ClassNotFoundException | NoSuchMethodException -> 0x00da }
            java.lang.String r6 = "get"
            java.lang.Class[] r0 = new java.lang.Class[r0]     // Catch:{ ClassNotFoundException | NoSuchMethodException -> 0x00da }
            java.lang.Class<javax.net.ssl.SSLSocket> r7 = javax.net.ssl.SSLSocket.class
            r0[r1] = r7     // Catch:{ ClassNotFoundException | NoSuchMethodException -> 0x00da }
            java.lang.reflect.Method r0 = r3.getMethod(r6, r0)     // Catch:{ ClassNotFoundException | NoSuchMethodException -> 0x00da }
            com.squareup.okhttp.internal.Platform$JdkWithJettyBootPlatform r1 = new com.squareup.okhttp.internal.Platform$JdkWithJettyBootPlatform     // Catch:{ ClassNotFoundException | NoSuchMethodException -> 0x00da }
            r1.<init>(r4, r0, r5, r2)     // Catch:{ ClassNotFoundException | NoSuchMethodException -> 0x00da }
            return r1
        L_0x00da:
            com.squareup.okhttp.internal.Platform r0 = new com.squareup.okhttp.internal.Platform
            r0.<init>()
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.squareup.okhttp.internal.Platform.findPlatform():com.squareup.okhttp.internal.Platform");
    }

    private static class Android extends Platform {
        private final Method getNpnSelectedProtocol;
        protected final Class<?> openSslSocketClass;
        private final Method setHostname;
        private final Method setNpnProtocols;
        private final Method setUseSessionTickets;
        private final Method trafficStatsTagSocket;
        private final Method trafficStatsUntagSocket;

        private Android(Class<?> cls, Method method, Method method2, Method method3, Method method4, Method method5, Method method6) {
            this.openSslSocketClass = cls;
            this.setUseSessionTickets = method;
            this.setHostname = method2;
            this.trafficStatsTagSocket = method3;
            this.trafficStatsUntagSocket = method4;
            this.setNpnProtocols = method5;
            this.getNpnSelectedProtocol = method6;
        }

        public void connectSocket(Socket socket, InetSocketAddress inetSocketAddress, int i) throws IOException {
            try {
                socket.connect(inetSocketAddress, i);
            } catch (SecurityException e) {
                IOException iOException = new IOException("Exception in connect");
                iOException.initCause(e);
                throw iOException;
            }
        }

        public void configureTls(SSLSocket sSLSocket, String str, String str2) {
            Platform.super.configureTls(sSLSocket, str, str2);
            if (str2.equals(RouteSelector.TLS_V1) && this.openSslSocketClass.isInstance(sSLSocket)) {
                try {
                    this.setUseSessionTickets.invoke(sSLSocket, new Object[]{true});
                    this.setHostname.invoke(sSLSocket, new Object[]{str});
                } catch (InvocationTargetException e) {
                    throw new RuntimeException(e);
                } catch (IllegalAccessException e2) {
                    throw new AssertionError(e2);
                }
            }
        }

        public void setProtocols(SSLSocket sSLSocket, List<Protocol> list) {
            if (this.setNpnProtocols != null && this.openSslSocketClass.isInstance(sSLSocket)) {
                try {
                    this.setNpnProtocols.invoke(sSLSocket, new Object[]{concatLengthPrefixed(list)});
                } catch (IllegalAccessException e) {
                    throw new AssertionError(e);
                } catch (InvocationTargetException e2) {
                    throw new RuntimeException(e2);
                }
            }
        }

        public String getSelectedProtocol(SSLSocket sSLSocket) {
            if (this.getNpnSelectedProtocol == null || !this.openSslSocketClass.isInstance(sSLSocket)) {
                return null;
            }
            try {
                byte[] bArr = (byte[]) this.getNpnSelectedProtocol.invoke(sSLSocket, new Object[0]);
                if (bArr == null) {
                    return null;
                }
                return new String(bArr, Util.UTF_8);
            } catch (InvocationTargetException e) {
                throw new RuntimeException(e);
            } catch (IllegalAccessException e2) {
                throw new AssertionError(e2);
            }
        }

        public void tagSocket(Socket socket) throws SocketException {
            Method method = this.trafficStatsTagSocket;
            if (method != null) {
                try {
                    method.invoke((Object) null, new Object[]{socket});
                } catch (IllegalAccessException e) {
                    throw new RuntimeException(e);
                } catch (InvocationTargetException e2) {
                    throw new RuntimeException(e2);
                }
            }
        }

        public void untagSocket(Socket socket) throws SocketException {
            Method method = this.trafficStatsUntagSocket;
            if (method != null) {
                try {
                    method.invoke((Object) null, new Object[]{socket});
                } catch (IllegalAccessException e) {
                    throw new RuntimeException(e);
                } catch (InvocationTargetException e2) {
                    throw new RuntimeException(e2);
                }
            }
        }
    }

    private static class JdkWithJettyBootPlatform extends Platform {
        private final Class<?> clientProviderClass;
        private final Method getMethod;
        private final Method putMethod;
        private final Class<?> serverProviderClass;

        public JdkWithJettyBootPlatform(Method method, Method method2, Class<?> cls, Class<?> cls2) {
            this.putMethod = method;
            this.getMethod = method2;
            this.clientProviderClass = cls;
            this.serverProviderClass = cls2;
        }

        public void setProtocols(SSLSocket sSLSocket, List<Protocol> list) {
            try {
                ArrayList arrayList = new ArrayList(list.size());
                int size = list.size();
                for (int i = 0; i < size; i++) {
                    Protocol protocol = list.get(i);
                    if (protocol != Protocol.HTTP_1_0) {
                        arrayList.add(protocol.toString());
                    }
                }
                Object newProxyInstance = Proxy.newProxyInstance(Platform.class.getClassLoader(), new Class[]{this.clientProviderClass, this.serverProviderClass}, new JettyNegoProvider(arrayList));
                this.putMethod.invoke((Object) null, new Object[]{sSLSocket, newProxyInstance});
            } catch (InvocationTargetException e) {
                throw new AssertionError(e);
            } catch (IllegalAccessException e2) {
                throw new AssertionError(e2);
            }
        }

        public String getSelectedProtocol(SSLSocket sSLSocket) {
            try {
                JettyNegoProvider jettyNegoProvider = (JettyNegoProvider) Proxy.getInvocationHandler(this.getMethod.invoke((Object) null, new Object[]{sSLSocket}));
                if (!jettyNegoProvider.unsupported && jettyNegoProvider.selected == null) {
                    Logger.getLogger("com.squareup.okhttp.OkHttpClient").log(Level.INFO, "NPN/ALPN callback dropped: SPDY and HTTP/2 are disabled. Is npn-boot or alpn-boot on the boot class path?");
                    return null;
                } else if (jettyNegoProvider.unsupported) {
                    return null;
                } else {
                    return jettyNegoProvider.selected;
                }
            } catch (InvocationTargetException unused) {
                throw new AssertionError();
            } catch (IllegalAccessException unused2) {
                throw new AssertionError();
            }
        }
    }

    private static class JettyNegoProvider implements InvocationHandler {
        private final List<String> protocols;
        /* access modifiers changed from: private */
        public String selected;
        /* access modifiers changed from: private */
        public boolean unsupported;

        public JettyNegoProvider(List<String> list) {
            this.protocols = list;
        }

        public Object invoke(Object obj, Method method, Object[] objArr) throws Throwable {
            String name = method.getName();
            Class<?> returnType = method.getReturnType();
            if (objArr == null) {
                objArr = Util.EMPTY_STRING_ARRAY;
            }
            if (name.equals("supports") && Boolean.TYPE == returnType) {
                return true;
            }
            if (name.equals("unsupported") && Void.TYPE == returnType) {
                this.unsupported = true;
                return null;
            } else if (name.equals("protocols") && objArr.length == 0) {
                return this.protocols;
            } else {
                if ((name.equals("selectProtocol") || name.equals("select")) && String.class == returnType && objArr.length == 1 && (objArr[0] instanceof List)) {
                    List list = (List) objArr[0];
                    int size = list.size();
                    for (int i = 0; i < size; i++) {
                        if (this.protocols.contains(list.get(i))) {
                            String str = (String) list.get(i);
                            this.selected = str;
                            return str;
                        }
                    }
                    String str2 = this.protocols.get(0);
                    this.selected = str2;
                    return str2;
                } else if ((!name.equals("protocolSelected") && !name.equals("selected")) || objArr.length != 1) {
                    return method.invoke(this, objArr);
                } else {
                    this.selected = (String) objArr[0];
                    return null;
                }
            }
        }
    }

    static byte[] concatLengthPrefixed(List<Protocol> list) {
        Buffer buffer = new Buffer();
        int size = list.size();
        for (int i = 0; i < size; i++) {
            Protocol protocol = list.get(i);
            if (protocol != Protocol.HTTP_1_0) {
                buffer.writeByte(protocol.toString().length());
                buffer.writeUtf8(protocol.toString());
            }
        }
        return buffer.readByteArray();
    }
}
