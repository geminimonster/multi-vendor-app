package com.squareup.okhttp.internal.http;

import com.squareup.okhttp.Connection;
import com.squareup.okhttp.ConnectionPool;
import com.squareup.okhttp.Headers;
import com.squareup.okhttp.Response;
import com.squareup.okhttp.internal.Internal;
import com.squareup.okhttp.internal.Util;
import java.io.IOException;
import java.io.OutputStream;
import java.net.CacheRequest;
import java.net.ProtocolException;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.concurrent.TimeUnit;
import okio.Buffer;
import okio.BufferedSink;
import okio.BufferedSource;
import okio.Okio;
import okio.Sink;
import okio.Source;
import okio.Timeout;

public final class HttpConnection {
    private static final String CRLF = "\r\n";
    /* access modifiers changed from: private */
    public static final byte[] FINAL_CHUNK = {48, 13, 10, 13, 10};
    /* access modifiers changed from: private */
    public static final byte[] HEX_DIGITS = {48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 97, 98, 99, 100, 101, 102};
    private static final int ON_IDLE_CLOSE = 2;
    private static final int ON_IDLE_HOLD = 0;
    private static final int ON_IDLE_POOL = 1;
    private static final int STATE_CLOSED = 6;
    private static final int STATE_IDLE = 0;
    private static final int STATE_OPEN_REQUEST_BODY = 1;
    private static final int STATE_OPEN_RESPONSE_BODY = 4;
    private static final int STATE_READING_RESPONSE_BODY = 5;
    private static final int STATE_READ_RESPONSE_HEADERS = 3;
    private static final int STATE_WRITING_REQUEST_BODY = 2;
    /* access modifiers changed from: private */
    public final Connection connection;
    /* access modifiers changed from: private */
    public int onIdle = 0;
    /* access modifiers changed from: private */
    public final ConnectionPool pool;
    /* access modifiers changed from: private */
    public final BufferedSink sink;
    private final Socket socket;
    /* access modifiers changed from: private */
    public final BufferedSource source;
    /* access modifiers changed from: private */
    public int state = 0;

    public HttpConnection(ConnectionPool connectionPool, Connection connection2, Socket socket2) throws IOException {
        this.pool = connectionPool;
        this.connection = connection2;
        this.socket = socket2;
        this.source = Okio.buffer(Okio.source(socket2));
        this.sink = Okio.buffer(Okio.sink(socket2));
    }

    public void setTimeouts(int i, int i2) {
        if (i != 0) {
            this.source.timeout().timeout((long) i, TimeUnit.MILLISECONDS);
        }
        if (i2 != 0) {
            this.sink.timeout().timeout((long) i2, TimeUnit.MILLISECONDS);
        }
    }

    public void poolOnIdle() {
        this.onIdle = 1;
        if (this.state == 0) {
            this.onIdle = 0;
            Internal.instance.recycle(this.pool, this.connection);
        }
    }

    public void closeOnIdle() throws IOException {
        this.onIdle = 2;
        if (this.state == 0) {
            this.state = 6;
            this.connection.getSocket().close();
        }
    }

    public boolean isClosed() {
        return this.state == 6;
    }

    public void closeIfOwnedBy(Object obj) throws IOException {
        Internal.instance.closeIfOwnedBy(this.connection, obj);
    }

    public void flush() throws IOException {
        this.sink.flush();
    }

    public long bufferSize() {
        return this.source.buffer().size();
    }

    public boolean isReadable() {
        int soTimeout;
        try {
            soTimeout = this.socket.getSoTimeout();
            this.socket.setSoTimeout(1);
            if (this.source.exhausted()) {
                this.socket.setSoTimeout(soTimeout);
                return false;
            }
            this.socket.setSoTimeout(soTimeout);
            return true;
        } catch (SocketTimeoutException unused) {
            return true;
        } catch (IOException unused2) {
            return false;
        } catch (Throwable th) {
            this.socket.setSoTimeout(soTimeout);
            throw th;
        }
    }

    public void writeRequest(Headers headers, String str) throws IOException {
        if (this.state == 0) {
            this.sink.writeUtf8(str).writeUtf8(CRLF);
            for (int i = 0; i < headers.size(); i++) {
                this.sink.writeUtf8(headers.name(i)).writeUtf8(": ").writeUtf8(headers.value(i)).writeUtf8(CRLF);
            }
            this.sink.writeUtf8(CRLF);
            this.state = 1;
            return;
        }
        throw new IllegalStateException("state: " + this.state);
    }

    public Response.Builder readResponse() throws IOException {
        StatusLine parse;
        Response.Builder message;
        int i = this.state;
        if (i == 1 || i == 3) {
            do {
                parse = StatusLine.parse(this.source.readUtf8LineStrict());
                message = new Response.Builder().protocol(parse.protocol).code(parse.code).message(parse.message);
                Headers.Builder builder = new Headers.Builder();
                readHeaders(builder);
                builder.add(OkHeaders.SELECTED_PROTOCOL, parse.protocol.toString());
                message.headers(builder.build());
            } while (parse.code == 100);
            this.state = 4;
            return message;
        }
        throw new IllegalStateException("state: " + this.state);
    }

    public void readHeaders(Headers.Builder builder) throws IOException {
        while (true) {
            String readUtf8LineStrict = this.source.readUtf8LineStrict();
            if (readUtf8LineStrict.length() != 0) {
                Internal.instance.addLine(builder, readUtf8LineStrict);
            } else {
                return;
            }
        }
    }

    public boolean discard(Source source2, int i) {
        int soTimeout;
        try {
            soTimeout = this.socket.getSoTimeout();
            this.socket.setSoTimeout(i);
            boolean skipAll = Util.skipAll(source2, i);
            this.socket.setSoTimeout(soTimeout);
            return skipAll;
        } catch (IOException unused) {
            return false;
        } catch (Throwable th) {
            this.socket.setSoTimeout(soTimeout);
            throw th;
        }
    }

    public Sink newChunkedSink() {
        if (this.state == 1) {
            this.state = 2;
            return new ChunkedSink();
        }
        throw new IllegalStateException("state: " + this.state);
    }

    public Sink newFixedLengthSink(long j) {
        if (this.state == 1) {
            this.state = 2;
            return new FixedLengthSink(j);
        }
        throw new IllegalStateException("state: " + this.state);
    }

    public void writeRequestBody(RetryableSink retryableSink) throws IOException {
        if (this.state == 1) {
            this.state = 3;
            retryableSink.writeToSocket(this.sink);
            return;
        }
        throw new IllegalStateException("state: " + this.state);
    }

    public Source newFixedLengthSource(CacheRequest cacheRequest, long j) throws IOException {
        if (this.state == 4) {
            this.state = 5;
            return new FixedLengthSource(cacheRequest, j);
        }
        throw new IllegalStateException("state: " + this.state);
    }

    public void emptyResponseBody() throws IOException {
        newFixedLengthSource((CacheRequest) null, 0);
    }

    public Source newChunkedSource(CacheRequest cacheRequest, HttpEngine httpEngine) throws IOException {
        if (this.state == 4) {
            this.state = 5;
            return new ChunkedSource(cacheRequest, httpEngine);
        }
        throw new IllegalStateException("state: " + this.state);
    }

    public Source newUnknownLengthSource(CacheRequest cacheRequest) throws IOException {
        if (this.state == 4) {
            this.state = 5;
            return new UnknownLengthSource(cacheRequest);
        }
        throw new IllegalStateException("state: " + this.state);
    }

    private final class FixedLengthSink implements Sink {
        private long bytesRemaining;
        private boolean closed;

        private FixedLengthSink(long j) {
            this.bytesRemaining = j;
        }

        public Timeout timeout() {
            return HttpConnection.this.sink.timeout();
        }

        public void write(Buffer buffer, long j) throws IOException {
            if (!this.closed) {
                Util.checkOffsetAndCount(buffer.size(), 0, j);
                if (j <= this.bytesRemaining) {
                    HttpConnection.this.sink.write(buffer, j);
                    this.bytesRemaining -= j;
                    return;
                }
                throw new ProtocolException("expected " + this.bytesRemaining + " bytes but received " + j);
            }
            throw new IllegalStateException("closed");
        }

        public void flush() throws IOException {
            if (!this.closed) {
                HttpConnection.this.sink.flush();
            }
        }

        public void close() throws IOException {
            if (!this.closed) {
                this.closed = true;
                if (this.bytesRemaining <= 0) {
                    int unused = HttpConnection.this.state = 3;
                    return;
                }
                throw new ProtocolException("unexpected end of stream");
            }
        }
    }

    private final class ChunkedSink implements Sink {
        private boolean closed;
        private final byte[] hex;

        private ChunkedSink() {
            this.hex = new byte[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 13, 10};
        }

        public Timeout timeout() {
            return HttpConnection.this.sink.timeout();
        }

        public void write(Buffer buffer, long j) throws IOException {
            if (this.closed) {
                throw new IllegalStateException("closed");
            } else if (j != 0) {
                writeHex(j);
                HttpConnection.this.sink.write(buffer, j);
                HttpConnection.this.sink.writeUtf8(HttpConnection.CRLF);
            }
        }

        public synchronized void flush() throws IOException {
            if (!this.closed) {
                HttpConnection.this.sink.flush();
            }
        }

        public synchronized void close() throws IOException {
            if (!this.closed) {
                this.closed = true;
                HttpConnection.this.sink.write(HttpConnection.FINAL_CHUNK);
                int unused = HttpConnection.this.state = 3;
            }
        }

        private void writeHex(long j) throws IOException {
            int i = 16;
            do {
                i--;
                this.hex[i] = HttpConnection.HEX_DIGITS[(int) (15 & j)];
                j >>>= 4;
            } while (j != 0);
            BufferedSink access$200 = HttpConnection.this.sink;
            byte[] bArr = this.hex;
            access$200.write(bArr, i, bArr.length - i);
        }
    }

    private class AbstractSource {
        protected final OutputStream cacheBody;
        private final CacheRequest cacheRequest;
        protected boolean closed;

        AbstractSource(CacheRequest cacheRequest2) throws IOException {
            OutputStream body = cacheRequest2 != null ? cacheRequest2.getBody() : null;
            cacheRequest2 = body == null ? null : cacheRequest2;
            this.cacheBody = body;
            this.cacheRequest = cacheRequest2;
        }

        /* access modifiers changed from: protected */
        public final void cacheWrite(Buffer buffer, long j) throws IOException {
            OutputStream outputStream = this.cacheBody;
            if (outputStream != null) {
                buffer.copyTo(outputStream, buffer.size() - j, j);
            }
        }

        /* access modifiers changed from: protected */
        public final void endOfInput(boolean z) throws IOException {
            if (HttpConnection.this.state == 5) {
                if (this.cacheRequest != null) {
                    this.cacheBody.close();
                }
                int unused = HttpConnection.this.state = 0;
                if (z && HttpConnection.this.onIdle == 1) {
                    int unused2 = HttpConnection.this.onIdle = 0;
                    Internal.instance.recycle(HttpConnection.this.pool, HttpConnection.this.connection);
                } else if (HttpConnection.this.onIdle == 2) {
                    int unused3 = HttpConnection.this.state = 6;
                    HttpConnection.this.connection.getSocket().close();
                }
            } else {
                throw new IllegalStateException("state: " + HttpConnection.this.state);
            }
        }

        /* access modifiers changed from: protected */
        public final void unexpectedEndOfInput() {
            CacheRequest cacheRequest2 = this.cacheRequest;
            if (cacheRequest2 != null) {
                cacheRequest2.abort();
            }
            Util.closeQuietly(HttpConnection.this.connection.getSocket());
            int unused = HttpConnection.this.state = 6;
        }
    }

    private class FixedLengthSource extends AbstractSource implements Source {
        private long bytesRemaining;

        public FixedLengthSource(CacheRequest cacheRequest, long j) throws IOException {
            super(cacheRequest);
            this.bytesRemaining = j;
            if (j == 0) {
                endOfInput(true);
            }
        }

        public long read(Buffer buffer, long j) throws IOException {
            if (j < 0) {
                throw new IllegalArgumentException("byteCount < 0: " + j);
            } else if (this.closed) {
                throw new IllegalStateException("closed");
            } else if (this.bytesRemaining == 0) {
                return -1;
            } else {
                long read = HttpConnection.this.source.read(buffer, Math.min(this.bytesRemaining, j));
                if (read != -1) {
                    this.bytesRemaining -= read;
                    cacheWrite(buffer, read);
                    if (this.bytesRemaining == 0) {
                        endOfInput(true);
                    }
                    return read;
                }
                unexpectedEndOfInput();
                throw new ProtocolException("unexpected end of stream");
            }
        }

        public Timeout timeout() {
            return HttpConnection.this.source.timeout();
        }

        public void close() throws IOException {
            if (!this.closed) {
                if (this.bytesRemaining != 0 && !HttpConnection.this.discard(this, 100)) {
                    unexpectedEndOfInput();
                }
                this.closed = true;
            }
        }
    }

    private class ChunkedSource extends AbstractSource implements Source {
        private static final int NO_CHUNK_YET = -1;
        private int bytesRemainingInChunk = -1;
        private boolean hasMoreChunks = true;
        private final HttpEngine httpEngine;

        ChunkedSource(CacheRequest cacheRequest, HttpEngine httpEngine2) throws IOException {
            super(cacheRequest);
            this.httpEngine = httpEngine2;
        }

        public long read(Buffer buffer, long j) throws IOException {
            if (j < 0) {
                throw new IllegalArgumentException("byteCount < 0: " + j);
            } else if (this.closed) {
                throw new IllegalStateException("closed");
            } else if (!this.hasMoreChunks) {
                return -1;
            } else {
                int i = this.bytesRemainingInChunk;
                if (i == 0 || i == -1) {
                    readChunkSize();
                    if (!this.hasMoreChunks) {
                        return -1;
                    }
                }
                long read = HttpConnection.this.source.read(buffer, Math.min(j, (long) this.bytesRemainingInChunk));
                if (read != -1) {
                    this.bytesRemainingInChunk = (int) (((long) this.bytesRemainingInChunk) - read);
                    cacheWrite(buffer, read);
                    return read;
                }
                unexpectedEndOfInput();
                throw new IOException("unexpected end of stream");
            }
        }

        private void readChunkSize() throws IOException {
            if (this.bytesRemainingInChunk != -1) {
                HttpConnection.this.source.readUtf8LineStrict();
            }
            String readUtf8LineStrict = HttpConnection.this.source.readUtf8LineStrict();
            int indexOf = readUtf8LineStrict.indexOf(";");
            if (indexOf != -1) {
                readUtf8LineStrict = readUtf8LineStrict.substring(0, indexOf);
            }
            try {
                int parseInt = Integer.parseInt(readUtf8LineStrict.trim(), 16);
                this.bytesRemainingInChunk = parseInt;
                if (parseInt == 0) {
                    this.hasMoreChunks = false;
                    Headers.Builder builder = new Headers.Builder();
                    HttpConnection.this.readHeaders(builder);
                    this.httpEngine.receiveHeaders(builder.build());
                    endOfInput(true);
                }
            } catch (NumberFormatException unused) {
                throw new ProtocolException("Expected a hex chunk size but was " + readUtf8LineStrict);
            }
        }

        public Timeout timeout() {
            return HttpConnection.this.source.timeout();
        }

        public void close() throws IOException {
            if (!this.closed) {
                if (this.hasMoreChunks && !HttpConnection.this.discard(this, 100)) {
                    unexpectedEndOfInput();
                }
                this.closed = true;
            }
        }
    }

    class UnknownLengthSource extends AbstractSource implements Source {
        private boolean inputExhausted;

        UnknownLengthSource(CacheRequest cacheRequest) throws IOException {
            super(cacheRequest);
        }

        public long read(Buffer buffer, long j) throws IOException {
            if (j < 0) {
                throw new IllegalArgumentException("byteCount < 0: " + j);
            } else if (this.closed) {
                throw new IllegalStateException("closed");
            } else if (this.inputExhausted) {
                return -1;
            } else {
                long read = HttpConnection.this.source.read(buffer, j);
                if (read == -1) {
                    this.inputExhausted = true;
                    endOfInput(false);
                    return -1;
                }
                cacheWrite(buffer, read);
                return read;
            }
        }

        public Timeout timeout() {
            return HttpConnection.this.source.timeout();
        }

        public void close() throws IOException {
            if (!this.closed) {
                if (!this.inputExhausted) {
                    unexpectedEndOfInput();
                }
                this.closed = true;
            }
        }
    }
}
