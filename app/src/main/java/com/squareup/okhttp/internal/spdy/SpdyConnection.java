package com.squareup.okhttp.internal.spdy;

import com.squareup.okhttp.Protocol;
import com.squareup.okhttp.internal.NamedRunnable;
import com.squareup.okhttp.internal.Util;
import com.squareup.okhttp.internal.spdy.FrameReader;
import java.io.Closeable;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import kotlin.jvm.internal.LongCompanionObject;
import okio.Buffer;
import okio.BufferedSource;
import okio.ByteString;
import okio.Okio;

public final class SpdyConnection implements Closeable {
    static final /* synthetic */ boolean $assertionsDisabled = false;
    private static final int OKHTTP_CLIENT_WINDOW_SIZE = 16777216;
    /* access modifiers changed from: private */
    public static final ExecutorService executor = new ThreadPoolExecutor(0, Integer.MAX_VALUE, 60, TimeUnit.SECONDS, new SynchronousQueue(), Util.threadFactory("OkHttp SpdyConnection", true));
    long bytesLeftInWriteWindow;
    final boolean client;
    /* access modifiers changed from: private */
    public final Set<Integer> currentPushRequests;
    final FrameWriter frameWriter;
    /* access modifiers changed from: private */
    public final IncomingStreamHandler handler;
    /* access modifiers changed from: private */
    public final String hostName;
    private long idleStartTimeNs;
    /* access modifiers changed from: private */
    public int lastGoodStreamId;
    final long maxFrameSize;
    private int nextPingId;
    /* access modifiers changed from: private */
    public int nextStreamId;
    final Settings okHttpSettings;
    final Settings peerSettings;
    private Map<Integer, Ping> pings;
    final Protocol protocol;
    private final ExecutorService pushExecutor;
    /* access modifiers changed from: private */
    public final PushObserver pushObserver;
    final Reader readerRunnable;
    /* access modifiers changed from: private */
    public boolean receivedInitialPeerSettings;
    /* access modifiers changed from: private */
    public boolean shutdown;
    final Socket socket;
    /* access modifiers changed from: private */
    public final Map<Integer, SpdyStream> streams;
    long unacknowledgedBytesRead;
    final Variant variant;

    private SpdyConnection(Builder builder) throws IOException {
        this.streams = new HashMap();
        this.idleStartTimeNs = System.nanoTime();
        this.unacknowledgedBytesRead = 0;
        this.okHttpSettings = new Settings();
        this.peerSettings = new Settings();
        this.receivedInitialPeerSettings = false;
        this.currentPushRequests = new LinkedHashSet();
        this.protocol = builder.protocol;
        this.pushObserver = builder.pushObserver;
        this.client = builder.client;
        this.handler = builder.handler;
        int i = 2;
        this.nextStreamId = builder.client ? 1 : 2;
        if (builder.client && this.protocol == Protocol.HTTP_2) {
            this.nextStreamId += 2;
        }
        this.nextPingId = builder.client ? 1 : i;
        if (builder.client) {
            this.okHttpSettings.set(7, 0, 16777216);
        }
        this.hostName = builder.hostName;
        if (this.protocol == Protocol.HTTP_2) {
            this.variant = new Http20Draft12();
            this.pushExecutor = new ThreadPoolExecutor(0, 1, 0, TimeUnit.MILLISECONDS, new LinkedBlockingQueue(), Util.threadFactory(String.format("OkHttp %s Push Observer", new Object[]{this.hostName}), true));
        } else if (this.protocol == Protocol.SPDY_3) {
            this.variant = new Spdy3();
            this.pushExecutor = null;
        } else {
            throw new AssertionError(this.protocol);
        }
        this.bytesLeftInWriteWindow = (long) this.peerSettings.getInitialWindowSize(65536);
        this.socket = builder.socket;
        this.frameWriter = this.variant.newWriter(Okio.buffer(Okio.sink(builder.socket)), this.client);
        this.maxFrameSize = (long) this.variant.maxFrameSize();
        this.readerRunnable = new Reader();
        new Thread(this.readerRunnable).start();
    }

    public Protocol getProtocol() {
        return this.protocol;
    }

    public synchronized int openStreamCount() {
        return this.streams.size();
    }

    /* access modifiers changed from: package-private */
    public synchronized SpdyStream getStream(int i) {
        return this.streams.get(Integer.valueOf(i));
    }

    /* access modifiers changed from: package-private */
    public synchronized SpdyStream removeStream(int i) {
        SpdyStream remove;
        remove = this.streams.remove(Integer.valueOf(i));
        if (remove != null && this.streams.isEmpty()) {
            setIdle(true);
        }
        return remove;
    }

    private synchronized void setIdle(boolean z) {
        long j;
        if (z) {
            try {
                j = System.nanoTime();
            } catch (Throwable th) {
                throw th;
            }
        } else {
            j = LongCompanionObject.MAX_VALUE;
        }
        this.idleStartTimeNs = j;
    }

    public synchronized boolean isIdle() {
        return this.idleStartTimeNs != LongCompanionObject.MAX_VALUE;
    }

    public synchronized long getIdleStartTimeNs() {
        return this.idleStartTimeNs;
    }

    public SpdyStream pushStream(int i, List<Header> list, boolean z) throws IOException {
        if (this.client) {
            throw new IllegalStateException("Client cannot push requests.");
        } else if (this.protocol == Protocol.HTTP_2) {
            return newStream(i, list, z, false);
        } else {
            throw new IllegalStateException("protocol != HTTP_2");
        }
    }

    public SpdyStream newStream(List<Header> list, boolean z, boolean z2) throws IOException {
        return newStream(0, list, z, z2);
    }

    private SpdyStream newStream(int i, List<Header> list, boolean z, boolean z2) throws IOException {
        int i2;
        SpdyStream spdyStream;
        boolean z3 = !z;
        boolean z4 = !z2;
        synchronized (this.frameWriter) {
            synchronized (this) {
                if (!this.shutdown) {
                    i2 = this.nextStreamId;
                    this.nextStreamId += 2;
                    spdyStream = new SpdyStream(i2, this, z3, z4, list);
                    if (spdyStream.isOpen()) {
                        this.streams.put(Integer.valueOf(i2), spdyStream);
                        setIdle(false);
                    }
                } else {
                    throw new IOException("shutdown");
                }
            }
            if (i == 0) {
                this.frameWriter.synStream(z3, z4, i2, i, list);
            } else if (!this.client) {
                this.frameWriter.pushPromise(i, i2, list);
            } else {
                throw new IllegalArgumentException("client streams shouldn't have associated stream IDs");
            }
        }
        if (!z) {
            this.frameWriter.flush();
        }
        return spdyStream;
    }

    /* access modifiers changed from: package-private */
    public void writeSynReply(int i, boolean z, List<Header> list) throws IOException {
        this.frameWriter.synReply(z, i, list);
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(3:22|23|24) */
    /* JADX WARNING: Code restructure failed: missing block: B:12:?, code lost:
        r4 = (int) java.lang.Math.min(java.lang.Math.min(r13, r9.bytesLeftInWriteWindow), r9.maxFrameSize);
        r7 = (long) r4;
        r9.bytesLeftInWriteWindow -= r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0047, code lost:
        throw new java.io.InterruptedIOException();
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:22:0x0042 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void writeData(int r10, boolean r11, okio.Buffer r12, long r13) throws java.io.IOException {
        /*
            r9 = this;
            r0 = 0
            r1 = 0
            int r3 = (r13 > r1 ? 1 : (r13 == r1 ? 0 : -1))
            if (r3 != 0) goto L_0x000d
            com.squareup.okhttp.internal.spdy.FrameWriter r13 = r9.frameWriter
            r13.data(r11, r10, r12, r0)
            return
        L_0x000d:
            int r3 = (r13 > r1 ? 1 : (r13 == r1 ? 0 : -1))
            if (r3 <= 0) goto L_0x004a
            monitor-enter(r9)
        L_0x0012:
            long r3 = r9.bytesLeftInWriteWindow     // Catch:{ InterruptedException -> 0x0042 }
            int r5 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r5 > 0) goto L_0x001c
            r9.wait()     // Catch:{ InterruptedException -> 0x0042 }
            goto L_0x0012
        L_0x001c:
            long r3 = r9.bytesLeftInWriteWindow     // Catch:{ all -> 0x0040 }
            long r3 = java.lang.Math.min(r13, r3)     // Catch:{ all -> 0x0040 }
            long r5 = r9.maxFrameSize     // Catch:{ all -> 0x0040 }
            long r3 = java.lang.Math.min(r3, r5)     // Catch:{ all -> 0x0040 }
            int r4 = (int) r3     // Catch:{ all -> 0x0040 }
            long r5 = r9.bytesLeftInWriteWindow     // Catch:{ all -> 0x0040 }
            long r7 = (long) r4     // Catch:{ all -> 0x0040 }
            long r5 = r5 - r7
            r9.bytesLeftInWriteWindow = r5     // Catch:{ all -> 0x0040 }
            monitor-exit(r9)     // Catch:{ all -> 0x0040 }
            long r13 = r13 - r7
            com.squareup.okhttp.internal.spdy.FrameWriter r3 = r9.frameWriter
            if (r11 == 0) goto L_0x003b
            int r5 = (r13 > r1 ? 1 : (r13 == r1 ? 0 : -1))
            if (r5 != 0) goto L_0x003b
            r5 = 1
            goto L_0x003c
        L_0x003b:
            r5 = 0
        L_0x003c:
            r3.data(r5, r10, r12, r4)
            goto L_0x000d
        L_0x0040:
            r10 = move-exception
            goto L_0x0048
        L_0x0042:
            java.io.InterruptedIOException r10 = new java.io.InterruptedIOException     // Catch:{ all -> 0x0040 }
            r10.<init>()     // Catch:{ all -> 0x0040 }
            throw r10     // Catch:{ all -> 0x0040 }
        L_0x0048:
            monitor-exit(r9)     // Catch:{ all -> 0x0040 }
            throw r10
        L_0x004a:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.squareup.okhttp.internal.spdy.SpdyConnection.writeData(int, boolean, okio.Buffer, long):void");
    }

    /* access modifiers changed from: package-private */
    public void addBytesToWriteWindow(long j) {
        this.bytesLeftInWriteWindow += j;
        if (j > 0) {
            notifyAll();
        }
    }

    /* access modifiers changed from: package-private */
    public void writeSynResetLater(int i, ErrorCode errorCode) {
        final int i2 = i;
        final ErrorCode errorCode2 = errorCode;
        executor.submit(new NamedRunnable("OkHttp %s stream %d", new Object[]{this.hostName, Integer.valueOf(i)}) {
            public void execute() {
                try {
                    SpdyConnection.this.writeSynReset(i2, errorCode2);
                } catch (IOException unused) {
                }
            }
        });
    }

    /* access modifiers changed from: package-private */
    public void writeSynReset(int i, ErrorCode errorCode) throws IOException {
        this.frameWriter.rstStream(i, errorCode);
    }

    /* access modifiers changed from: package-private */
    public void writeWindowUpdateLater(int i, long j) {
        final int i2 = i;
        final long j2 = j;
        executor.submit(new NamedRunnable("OkHttp Window Update %s stream %d", new Object[]{this.hostName, Integer.valueOf(i)}) {
            public void execute() {
                try {
                    SpdyConnection.this.frameWriter.windowUpdate(i2, j2);
                } catch (IOException unused) {
                }
            }
        });
    }

    public Ping ping() throws IOException {
        int i;
        Ping ping = new Ping();
        synchronized (this) {
            if (!this.shutdown) {
                i = this.nextPingId;
                this.nextPingId += 2;
                if (this.pings == null) {
                    this.pings = new HashMap();
                }
                this.pings.put(Integer.valueOf(i), ping);
            } else {
                throw new IOException("shutdown");
            }
        }
        writePing(false, i, 1330343787, ping);
        return ping;
    }

    /* access modifiers changed from: private */
    public void writePingLater(boolean z, int i, int i2, Ping ping) {
        final boolean z2 = z;
        final int i3 = i;
        final int i4 = i2;
        final Ping ping2 = ping;
        executor.submit(new NamedRunnable("OkHttp %s ping %08x%08x", new Object[]{this.hostName, Integer.valueOf(i), Integer.valueOf(i2)}) {
            public void execute() {
                try {
                    SpdyConnection.this.writePing(z2, i3, i4, ping2);
                } catch (IOException unused) {
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public void writePing(boolean z, int i, int i2, Ping ping) throws IOException {
        synchronized (this.frameWriter) {
            if (ping != null) {
                ping.send();
            }
            this.frameWriter.ping(z, i, i2);
        }
    }

    /* access modifiers changed from: private */
    public synchronized Ping removePing(int i) {
        return this.pings != null ? this.pings.remove(Integer.valueOf(i)) : null;
    }

    public void flush() throws IOException {
        this.frameWriter.flush();
    }

    public void shutdown(ErrorCode errorCode) throws IOException {
        synchronized (this.frameWriter) {
            synchronized (this) {
                if (!this.shutdown) {
                    this.shutdown = true;
                    int i = this.lastGoodStreamId;
                    this.frameWriter.goAway(i, errorCode, Util.EMPTY_BYTE_ARRAY);
                }
            }
        }
    }

    public void close() throws IOException {
        close(ErrorCode.NO_ERROR, ErrorCode.CANCEL);
    }

    /* access modifiers changed from: private */
    public void close(ErrorCode errorCode, ErrorCode errorCode2) throws IOException {
        int i;
        SpdyStream[] spdyStreamArr;
        Ping[] pingArr = null;
        try {
            shutdown(errorCode);
            e = null;
        } catch (IOException e) {
            e = e;
        }
        synchronized (this) {
            if (!this.streams.isEmpty()) {
                spdyStreamArr = (SpdyStream[]) this.streams.values().toArray(new SpdyStream[this.streams.size()]);
                this.streams.clear();
                setIdle(false);
            } else {
                spdyStreamArr = null;
            }
            if (this.pings != null) {
                this.pings = null;
                pingArr = (Ping[]) this.pings.values().toArray(new Ping[this.pings.size()]);
            }
        }
        if (spdyStreamArr != null) {
            for (SpdyStream close : spdyStreamArr) {
                try {
                    close.close(errorCode2);
                } catch (IOException e2) {
                    if (e != null) {
                        e = e2;
                    }
                }
            }
        }
        if (pingArr != null) {
            for (Ping cancel : pingArr) {
                cancel.cancel();
            }
        }
        try {
            this.frameWriter.close();
        } catch (IOException e3) {
            if (e == null) {
                e = e3;
            }
        }
        try {
            this.socket.close();
        } catch (IOException e4) {
            e = e4;
        }
        if (e != null) {
            throw e;
        }
    }

    public void sendConnectionPreface() throws IOException {
        this.frameWriter.connectionPreface();
        this.frameWriter.settings(this.okHttpSettings);
        int initialWindowSize = this.okHttpSettings.getInitialWindowSize(65536);
        if (initialWindowSize != 65536) {
            this.frameWriter.windowUpdate(0, (long) (initialWindowSize - 65536));
        }
    }

    public static class Builder {
        /* access modifiers changed from: private */
        public boolean client;
        /* access modifiers changed from: private */
        public IncomingStreamHandler handler;
        /* access modifiers changed from: private */
        public String hostName;
        /* access modifiers changed from: private */
        public Protocol protocol;
        /* access modifiers changed from: private */
        public PushObserver pushObserver;
        /* access modifiers changed from: private */
        public Socket socket;

        public Builder(boolean z, Socket socket2) throws IOException {
            this(((InetSocketAddress) socket2.getRemoteSocketAddress()).getHostName(), z, socket2);
        }

        public Builder(String str, boolean z, Socket socket2) throws IOException {
            this.handler = IncomingStreamHandler.REFUSE_INCOMING_STREAMS;
            this.protocol = Protocol.SPDY_3;
            this.pushObserver = PushObserver.CANCEL;
            this.hostName = str;
            this.client = z;
            this.socket = socket2;
        }

        public Builder handler(IncomingStreamHandler incomingStreamHandler) {
            this.handler = incomingStreamHandler;
            return this;
        }

        public Builder protocol(Protocol protocol2) {
            this.protocol = protocol2;
            return this;
        }

        public Builder pushObserver(PushObserver pushObserver2) {
            this.pushObserver = pushObserver2;
            return this;
        }

        public SpdyConnection build() throws IOException {
            return new SpdyConnection(this);
        }
    }

    class Reader extends NamedRunnable implements FrameReader.Handler {
        FrameReader frameReader;

        public void ackSettings() {
        }

        public void alternateService(int i, String str, ByteString byteString, String str2, int i2, long j) {
        }

        public void priority(int i, int i2, int i3, boolean z) {
        }

        private Reader() {
            super("OkHttp %s", SpdyConnection.this.hostName);
        }

        /* access modifiers changed from: protected */
        /* JADX WARNING: Code restructure failed: missing block: B:11:0x0039, code lost:
            r2 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:13:?, code lost:
            r0 = com.squareup.okhttp.internal.spdy.ErrorCode.PROTOCOL_ERROR;
            r1 = com.squareup.okhttp.internal.spdy.ErrorCode.PROTOCOL_ERROR;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:15:?, code lost:
            r2 = r5.this$0;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:21:?, code lost:
            com.squareup.okhttp.internal.spdy.SpdyConnection.access$1000(r5.this$0, r0, r1);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:22:0x004f, code lost:
            com.squareup.okhttp.internal.Util.closeQuietly((java.io.Closeable) r5.frameReader);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:24:0x0054, code lost:
            throw r2;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:12:0x003b */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void execute() {
            /*
                r5 = this;
                com.squareup.okhttp.internal.spdy.ErrorCode r0 = com.squareup.okhttp.internal.spdy.ErrorCode.INTERNAL_ERROR
                com.squareup.okhttp.internal.spdy.ErrorCode r1 = com.squareup.okhttp.internal.spdy.ErrorCode.INTERNAL_ERROR
                com.squareup.okhttp.internal.spdy.SpdyConnection r2 = com.squareup.okhttp.internal.spdy.SpdyConnection.this     // Catch:{ IOException -> 0x003b }
                com.squareup.okhttp.internal.spdy.Variant r2 = r2.variant     // Catch:{ IOException -> 0x003b }
                com.squareup.okhttp.internal.spdy.SpdyConnection r3 = com.squareup.okhttp.internal.spdy.SpdyConnection.this     // Catch:{ IOException -> 0x003b }
                java.net.Socket r3 = r3.socket     // Catch:{ IOException -> 0x003b }
                okio.Source r3 = okio.Okio.source((java.net.Socket) r3)     // Catch:{ IOException -> 0x003b }
                okio.BufferedSource r3 = okio.Okio.buffer((okio.Source) r3)     // Catch:{ IOException -> 0x003b }
                com.squareup.okhttp.internal.spdy.SpdyConnection r4 = com.squareup.okhttp.internal.spdy.SpdyConnection.this     // Catch:{ IOException -> 0x003b }
                boolean r4 = r4.client     // Catch:{ IOException -> 0x003b }
                com.squareup.okhttp.internal.spdy.FrameReader r2 = r2.newReader(r3, r4)     // Catch:{ IOException -> 0x003b }
                r5.frameReader = r2     // Catch:{ IOException -> 0x003b }
                com.squareup.okhttp.internal.spdy.SpdyConnection r2 = com.squareup.okhttp.internal.spdy.SpdyConnection.this     // Catch:{ IOException -> 0x003b }
                boolean r2 = r2.client     // Catch:{ IOException -> 0x003b }
                if (r2 != 0) goto L_0x0029
                com.squareup.okhttp.internal.spdy.FrameReader r2 = r5.frameReader     // Catch:{ IOException -> 0x003b }
                r2.readConnectionPreface()     // Catch:{ IOException -> 0x003b }
            L_0x0029:
                com.squareup.okhttp.internal.spdy.FrameReader r2 = r5.frameReader     // Catch:{ IOException -> 0x003b }
                boolean r2 = r2.nextFrame(r5)     // Catch:{ IOException -> 0x003b }
                if (r2 == 0) goto L_0x0032
                goto L_0x0029
            L_0x0032:
                com.squareup.okhttp.internal.spdy.ErrorCode r0 = com.squareup.okhttp.internal.spdy.ErrorCode.NO_ERROR     // Catch:{ IOException -> 0x003b }
                com.squareup.okhttp.internal.spdy.ErrorCode r1 = com.squareup.okhttp.internal.spdy.ErrorCode.CANCEL     // Catch:{ IOException -> 0x003b }
                com.squareup.okhttp.internal.spdy.SpdyConnection r2 = com.squareup.okhttp.internal.spdy.SpdyConnection.this     // Catch:{ IOException -> 0x0044 }
                goto L_0x0041
            L_0x0039:
                r2 = move-exception
                goto L_0x004a
            L_0x003b:
                com.squareup.okhttp.internal.spdy.ErrorCode r0 = com.squareup.okhttp.internal.spdy.ErrorCode.PROTOCOL_ERROR     // Catch:{ all -> 0x0039 }
                com.squareup.okhttp.internal.spdy.ErrorCode r1 = com.squareup.okhttp.internal.spdy.ErrorCode.PROTOCOL_ERROR     // Catch:{ all -> 0x0039 }
                com.squareup.okhttp.internal.spdy.SpdyConnection r2 = com.squareup.okhttp.internal.spdy.SpdyConnection.this     // Catch:{ IOException -> 0x0044 }
            L_0x0041:
                r2.close(r0, r1)     // Catch:{ IOException -> 0x0044 }
            L_0x0044:
                com.squareup.okhttp.internal.spdy.FrameReader r0 = r5.frameReader
                com.squareup.okhttp.internal.Util.closeQuietly((java.io.Closeable) r0)
                return
            L_0x004a:
                com.squareup.okhttp.internal.spdy.SpdyConnection r3 = com.squareup.okhttp.internal.spdy.SpdyConnection.this     // Catch:{ IOException -> 0x004f }
                r3.close(r0, r1)     // Catch:{ IOException -> 0x004f }
            L_0x004f:
                com.squareup.okhttp.internal.spdy.FrameReader r0 = r5.frameReader
                com.squareup.okhttp.internal.Util.closeQuietly((java.io.Closeable) r0)
                throw r2
            */
            throw new UnsupportedOperationException("Method not decompiled: com.squareup.okhttp.internal.spdy.SpdyConnection.Reader.execute():void");
        }

        public void data(boolean z, int i, BufferedSource bufferedSource, int i2) throws IOException {
            if (SpdyConnection.this.pushedStream(i)) {
                SpdyConnection.this.pushDataLater(i, bufferedSource, i2, z);
                return;
            }
            SpdyStream stream = SpdyConnection.this.getStream(i);
            if (stream == null) {
                SpdyConnection.this.writeSynResetLater(i, ErrorCode.INVALID_STREAM);
                bufferedSource.skip((long) i2);
                return;
            }
            stream.receiveData(bufferedSource, i2);
            if (z) {
                stream.receiveFin();
            }
        }

        /* JADX WARNING: Code restructure failed: missing block: B:31:0x008f, code lost:
            if (r14.failIfStreamPresent() == false) goto L_0x009c;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:32:0x0091, code lost:
            r0.closeLater(com.squareup.okhttp.internal.spdy.ErrorCode.PROTOCOL_ERROR);
            r8.this$0.removeStream(r11);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:33:0x009b, code lost:
            return;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:34:0x009c, code lost:
            r0.receiveHeaders(r13, r14);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:35:0x009f, code lost:
            if (r10 == false) goto L_?;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:36:0x00a1, code lost:
            r0.receiveFin();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:44:?, code lost:
            return;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:45:?, code lost:
            return;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void headers(boolean r9, boolean r10, int r11, int r12, java.util.List<com.squareup.okhttp.internal.spdy.Header> r13, com.squareup.okhttp.internal.spdy.HeadersMode r14) {
            /*
                r8 = this;
                com.squareup.okhttp.internal.spdy.SpdyConnection r12 = com.squareup.okhttp.internal.spdy.SpdyConnection.this
                boolean r12 = r12.pushedStream(r11)
                if (r12 == 0) goto L_0x000e
                com.squareup.okhttp.internal.spdy.SpdyConnection r9 = com.squareup.okhttp.internal.spdy.SpdyConnection.this
                r9.pushHeadersLater(r11, r13, r10)
                return
            L_0x000e:
                com.squareup.okhttp.internal.spdy.SpdyConnection r12 = com.squareup.okhttp.internal.spdy.SpdyConnection.this
                monitor-enter(r12)
                com.squareup.okhttp.internal.spdy.SpdyConnection r0 = com.squareup.okhttp.internal.spdy.SpdyConnection.this     // Catch:{ all -> 0x00a5 }
                boolean r0 = r0.shutdown     // Catch:{ all -> 0x00a5 }
                if (r0 == 0) goto L_0x001b
                monitor-exit(r12)     // Catch:{ all -> 0x00a5 }
                return
            L_0x001b:
                com.squareup.okhttp.internal.spdy.SpdyConnection r0 = com.squareup.okhttp.internal.spdy.SpdyConnection.this     // Catch:{ all -> 0x00a5 }
                com.squareup.okhttp.internal.spdy.SpdyStream r0 = r0.getStream(r11)     // Catch:{ all -> 0x00a5 }
                if (r0 != 0) goto L_0x008a
                boolean r14 = r14.failIfStreamAbsent()     // Catch:{ all -> 0x00a5 }
                if (r14 == 0) goto L_0x0032
                com.squareup.okhttp.internal.spdy.SpdyConnection r9 = com.squareup.okhttp.internal.spdy.SpdyConnection.this     // Catch:{ all -> 0x00a5 }
                com.squareup.okhttp.internal.spdy.ErrorCode r10 = com.squareup.okhttp.internal.spdy.ErrorCode.INVALID_STREAM     // Catch:{ all -> 0x00a5 }
                r9.writeSynResetLater(r11, r10)     // Catch:{ all -> 0x00a5 }
                monitor-exit(r12)     // Catch:{ all -> 0x00a5 }
                return
            L_0x0032:
                com.squareup.okhttp.internal.spdy.SpdyConnection r14 = com.squareup.okhttp.internal.spdy.SpdyConnection.this     // Catch:{ all -> 0x00a5 }
                int r14 = r14.lastGoodStreamId     // Catch:{ all -> 0x00a5 }
                if (r11 > r14) goto L_0x003c
                monitor-exit(r12)     // Catch:{ all -> 0x00a5 }
                return
            L_0x003c:
                int r14 = r11 % 2
                com.squareup.okhttp.internal.spdy.SpdyConnection r0 = com.squareup.okhttp.internal.spdy.SpdyConnection.this     // Catch:{ all -> 0x00a5 }
                int r0 = r0.nextStreamId     // Catch:{ all -> 0x00a5 }
                r1 = 2
                int r0 = r0 % r1
                if (r14 != r0) goto L_0x004a
                monitor-exit(r12)     // Catch:{ all -> 0x00a5 }
                return
            L_0x004a:
                com.squareup.okhttp.internal.spdy.SpdyStream r14 = new com.squareup.okhttp.internal.spdy.SpdyStream     // Catch:{ all -> 0x00a5 }
                com.squareup.okhttp.internal.spdy.SpdyConnection r4 = com.squareup.okhttp.internal.spdy.SpdyConnection.this     // Catch:{ all -> 0x00a5 }
                r2 = r14
                r3 = r11
                r5 = r9
                r6 = r10
                r7 = r13
                r2.<init>(r3, r4, r5, r6, r7)     // Catch:{ all -> 0x00a5 }
                com.squareup.okhttp.internal.spdy.SpdyConnection r9 = com.squareup.okhttp.internal.spdy.SpdyConnection.this     // Catch:{ all -> 0x00a5 }
                int unused = r9.lastGoodStreamId = r11     // Catch:{ all -> 0x00a5 }
                com.squareup.okhttp.internal.spdy.SpdyConnection r9 = com.squareup.okhttp.internal.spdy.SpdyConnection.this     // Catch:{ all -> 0x00a5 }
                java.util.Map r9 = r9.streams     // Catch:{ all -> 0x00a5 }
                java.lang.Integer r10 = java.lang.Integer.valueOf(r11)     // Catch:{ all -> 0x00a5 }
                r9.put(r10, r14)     // Catch:{ all -> 0x00a5 }
                java.util.concurrent.ExecutorService r9 = com.squareup.okhttp.internal.spdy.SpdyConnection.executor     // Catch:{ all -> 0x00a5 }
                com.squareup.okhttp.internal.spdy.SpdyConnection$Reader$1 r10 = new com.squareup.okhttp.internal.spdy.SpdyConnection$Reader$1     // Catch:{ all -> 0x00a5 }
                java.lang.String r13 = "OkHttp %s stream %d"
                java.lang.Object[] r0 = new java.lang.Object[r1]     // Catch:{ all -> 0x00a5 }
                r1 = 0
                com.squareup.okhttp.internal.spdy.SpdyConnection r2 = com.squareup.okhttp.internal.spdy.SpdyConnection.this     // Catch:{ all -> 0x00a5 }
                java.lang.String r2 = r2.hostName     // Catch:{ all -> 0x00a5 }
                r0[r1] = r2     // Catch:{ all -> 0x00a5 }
                r1 = 1
                java.lang.Integer r11 = java.lang.Integer.valueOf(r11)     // Catch:{ all -> 0x00a5 }
                r0[r1] = r11     // Catch:{ all -> 0x00a5 }
                r10.<init>(r13, r0, r14)     // Catch:{ all -> 0x00a5 }
                r9.submit(r10)     // Catch:{ all -> 0x00a5 }
                monitor-exit(r12)     // Catch:{ all -> 0x00a5 }
                return
            L_0x008a:
                monitor-exit(r12)     // Catch:{ all -> 0x00a5 }
                boolean r9 = r14.failIfStreamPresent()
                if (r9 == 0) goto L_0x009c
                com.squareup.okhttp.internal.spdy.ErrorCode r9 = com.squareup.okhttp.internal.spdy.ErrorCode.PROTOCOL_ERROR
                r0.closeLater(r9)
                com.squareup.okhttp.internal.spdy.SpdyConnection r9 = com.squareup.okhttp.internal.spdy.SpdyConnection.this
                r9.removeStream(r11)
                return
            L_0x009c:
                r0.receiveHeaders(r13, r14)
                if (r10 == 0) goto L_0x00a4
                r0.receiveFin()
            L_0x00a4:
                return
            L_0x00a5:
                r9 = move-exception
                monitor-exit(r12)     // Catch:{ all -> 0x00a5 }
                throw r9
            */
            throw new UnsupportedOperationException("Method not decompiled: com.squareup.okhttp.internal.spdy.SpdyConnection.Reader.headers(boolean, boolean, int, int, java.util.List, com.squareup.okhttp.internal.spdy.HeadersMode):void");
        }

        public void rstStream(int i, ErrorCode errorCode) {
            if (SpdyConnection.this.pushedStream(i)) {
                SpdyConnection.this.pushResetLater(i, errorCode);
                return;
            }
            SpdyStream removeStream = SpdyConnection.this.removeStream(i);
            if (removeStream != null) {
                removeStream.receiveRstStream(errorCode);
            }
        }

        /* JADX WARNING: type inference failed for: r1v14, types: [java.lang.Object[]] */
        /* JADX WARNING: Multi-variable type inference failed */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void settings(boolean r7, com.squareup.okhttp.internal.spdy.Settings r8) {
            /*
                r6 = this;
                com.squareup.okhttp.internal.spdy.SpdyConnection r0 = com.squareup.okhttp.internal.spdy.SpdyConnection.this
                monitor-enter(r0)
                com.squareup.okhttp.internal.spdy.SpdyConnection r1 = com.squareup.okhttp.internal.spdy.SpdyConnection.this     // Catch:{ all -> 0x00a5 }
                com.squareup.okhttp.internal.spdy.Settings r1 = r1.peerSettings     // Catch:{ all -> 0x00a5 }
                r2 = 65536(0x10000, float:9.18355E-41)
                int r1 = r1.getInitialWindowSize(r2)     // Catch:{ all -> 0x00a5 }
                if (r7 == 0) goto L_0x0016
                com.squareup.okhttp.internal.spdy.SpdyConnection r7 = com.squareup.okhttp.internal.spdy.SpdyConnection.this     // Catch:{ all -> 0x00a5 }
                com.squareup.okhttp.internal.spdy.Settings r7 = r7.peerSettings     // Catch:{ all -> 0x00a5 }
                r7.clear()     // Catch:{ all -> 0x00a5 }
            L_0x0016:
                com.squareup.okhttp.internal.spdy.SpdyConnection r7 = com.squareup.okhttp.internal.spdy.SpdyConnection.this     // Catch:{ all -> 0x00a5 }
                com.squareup.okhttp.internal.spdy.Settings r7 = r7.peerSettings     // Catch:{ all -> 0x00a5 }
                r7.merge(r8)     // Catch:{ all -> 0x00a5 }
                com.squareup.okhttp.internal.spdy.SpdyConnection r7 = com.squareup.okhttp.internal.spdy.SpdyConnection.this     // Catch:{ all -> 0x00a5 }
                com.squareup.okhttp.Protocol r7 = r7.getProtocol()     // Catch:{ all -> 0x00a5 }
                com.squareup.okhttp.Protocol r8 = com.squareup.okhttp.Protocol.HTTP_2     // Catch:{ all -> 0x00a5 }
                if (r7 != r8) goto L_0x002a
                r6.ackSettingsLater()     // Catch:{ all -> 0x00a5 }
            L_0x002a:
                com.squareup.okhttp.internal.spdy.SpdyConnection r7 = com.squareup.okhttp.internal.spdy.SpdyConnection.this     // Catch:{ all -> 0x00a5 }
                com.squareup.okhttp.internal.spdy.Settings r7 = r7.peerSettings     // Catch:{ all -> 0x00a5 }
                int r7 = r7.getInitialWindowSize(r2)     // Catch:{ all -> 0x00a5 }
                r8 = -1
                r2 = 0
                r4 = 0
                if (r7 == r8) goto L_0x0079
                if (r7 == r1) goto L_0x0079
                int r7 = r7 - r1
                long r7 = (long) r7     // Catch:{ all -> 0x00a5 }
                com.squareup.okhttp.internal.spdy.SpdyConnection r1 = com.squareup.okhttp.internal.spdy.SpdyConnection.this     // Catch:{ all -> 0x00a5 }
                boolean r1 = r1.receivedInitialPeerSettings     // Catch:{ all -> 0x00a5 }
                if (r1 != 0) goto L_0x004f
                com.squareup.okhttp.internal.spdy.SpdyConnection r1 = com.squareup.okhttp.internal.spdy.SpdyConnection.this     // Catch:{ all -> 0x00a5 }
                r1.addBytesToWriteWindow(r7)     // Catch:{ all -> 0x00a5 }
                com.squareup.okhttp.internal.spdy.SpdyConnection r1 = com.squareup.okhttp.internal.spdy.SpdyConnection.this     // Catch:{ all -> 0x00a5 }
                r5 = 1
                boolean unused = r1.receivedInitialPeerSettings = r5     // Catch:{ all -> 0x00a5 }
            L_0x004f:
                com.squareup.okhttp.internal.spdy.SpdyConnection r1 = com.squareup.okhttp.internal.spdy.SpdyConnection.this     // Catch:{ all -> 0x00a5 }
                java.util.Map r1 = r1.streams     // Catch:{ all -> 0x00a5 }
                boolean r1 = r1.isEmpty()     // Catch:{ all -> 0x00a5 }
                if (r1 != 0) goto L_0x007a
                com.squareup.okhttp.internal.spdy.SpdyConnection r1 = com.squareup.okhttp.internal.spdy.SpdyConnection.this     // Catch:{ all -> 0x00a5 }
                java.util.Map r1 = r1.streams     // Catch:{ all -> 0x00a5 }
                java.util.Collection r1 = r1.values()     // Catch:{ all -> 0x00a5 }
                com.squareup.okhttp.internal.spdy.SpdyConnection r4 = com.squareup.okhttp.internal.spdy.SpdyConnection.this     // Catch:{ all -> 0x00a5 }
                java.util.Map r4 = r4.streams     // Catch:{ all -> 0x00a5 }
                int r4 = r4.size()     // Catch:{ all -> 0x00a5 }
                com.squareup.okhttp.internal.spdy.SpdyStream[] r4 = new com.squareup.okhttp.internal.spdy.SpdyStream[r4]     // Catch:{ all -> 0x00a5 }
                java.lang.Object[] r1 = r1.toArray(r4)     // Catch:{ all -> 0x00a5 }
                r4 = r1
                com.squareup.okhttp.internal.spdy.SpdyStream[] r4 = (com.squareup.okhttp.internal.spdy.SpdyStream[]) r4     // Catch:{ all -> 0x00a5 }
                goto L_0x007a
            L_0x0079:
                r7 = r2
            L_0x007a:
                monitor-exit(r0)     // Catch:{ all -> 0x00a5 }
                if (r4 == 0) goto L_0x00a4
                int r0 = (r7 > r2 ? 1 : (r7 == r2 ? 0 : -1))
                if (r0 == 0) goto L_0x00a4
                com.squareup.okhttp.internal.spdy.SpdyConnection r0 = com.squareup.okhttp.internal.spdy.SpdyConnection.this
                java.util.Map r0 = r0.streams
                java.util.Collection r0 = r0.values()
                java.util.Iterator r0 = r0.iterator()
            L_0x008f:
                boolean r1 = r0.hasNext()
                if (r1 == 0) goto L_0x00a4
                java.lang.Object r1 = r0.next()
                com.squareup.okhttp.internal.spdy.SpdyStream r1 = (com.squareup.okhttp.internal.spdy.SpdyStream) r1
                monitor-enter(r1)
                r1.addBytesToWriteWindow(r7)     // Catch:{ all -> 0x00a1 }
                monitor-exit(r1)     // Catch:{ all -> 0x00a1 }
                goto L_0x008f
            L_0x00a1:
                r7 = move-exception
                monitor-exit(r1)     // Catch:{ all -> 0x00a1 }
                throw r7
            L_0x00a4:
                return
            L_0x00a5:
                r7 = move-exception
                monitor-exit(r0)     // Catch:{ all -> 0x00a5 }
                throw r7
            */
            throw new UnsupportedOperationException("Method not decompiled: com.squareup.okhttp.internal.spdy.SpdyConnection.Reader.settings(boolean, com.squareup.okhttp.internal.spdy.Settings):void");
        }

        private void ackSettingsLater() {
            SpdyConnection.executor.submit(new NamedRunnable("OkHttp %s ACK Settings", SpdyConnection.this.hostName) {
                public void execute() {
                    try {
                        SpdyConnection.this.frameWriter.ackSettings();
                    } catch (IOException unused) {
                    }
                }
            });
        }

        public void ping(boolean z, int i, int i2) {
            if (z) {
                Ping access$2200 = SpdyConnection.this.removePing(i);
                if (access$2200 != null) {
                    access$2200.receive();
                    return;
                }
                return;
            }
            SpdyConnection.this.writePingLater(true, i, i2, (Ping) null);
        }

        public void goAway(int i, ErrorCode errorCode, ByteString byteString) {
            byteString.size();
            synchronized (SpdyConnection.this) {
                boolean unused = SpdyConnection.this.shutdown = true;
                Iterator it = SpdyConnection.this.streams.entrySet().iterator();
                while (it.hasNext()) {
                    Map.Entry entry = (Map.Entry) it.next();
                    if (((Integer) entry.getKey()).intValue() > i && ((SpdyStream) entry.getValue()).isLocallyInitiated()) {
                        ((SpdyStream) entry.getValue()).receiveRstStream(ErrorCode.REFUSED_STREAM);
                        it.remove();
                    }
                }
            }
        }

        public void windowUpdate(int i, long j) {
            if (i == 0) {
                synchronized (SpdyConnection.this) {
                    SpdyConnection.this.bytesLeftInWriteWindow += j;
                    SpdyConnection.this.notifyAll();
                }
                return;
            }
            SpdyStream stream = SpdyConnection.this.getStream(i);
            if (stream != null) {
                synchronized (stream) {
                    stream.addBytesToWriteWindow(j);
                }
            }
        }

        public void pushPromise(int i, int i2, List<Header> list) {
            SpdyConnection.this.pushRequestLater(i2, list);
        }
    }

    /* access modifiers changed from: private */
    public boolean pushedStream(int i) {
        return this.protocol == Protocol.HTTP_2 && i != 0 && (i & 1) == 0;
    }

    /* access modifiers changed from: private */
    public void pushRequestLater(int i, List<Header> list) {
        synchronized (this) {
            if (this.currentPushRequests.contains(Integer.valueOf(i))) {
                writeSynResetLater(i, ErrorCode.PROTOCOL_ERROR);
                return;
            }
            this.currentPushRequests.add(Integer.valueOf(i));
            final int i2 = i;
            final List<Header> list2 = list;
            this.pushExecutor.submit(new NamedRunnable("OkHttp %s Push Request[%s]", new Object[]{this.hostName, Integer.valueOf(i)}) {
                public void execute() {
                    if (SpdyConnection.this.pushObserver.onRequest(i2, list2)) {
                        try {
                            SpdyConnection.this.frameWriter.rstStream(i2, ErrorCode.CANCEL);
                            synchronized (SpdyConnection.this) {
                                SpdyConnection.this.currentPushRequests.remove(Integer.valueOf(i2));
                            }
                        } catch (IOException unused) {
                        }
                    }
                }
            });
        }
    }

    /* access modifiers changed from: private */
    public void pushHeadersLater(int i, List<Header> list, boolean z) {
        final int i2 = i;
        final List<Header> list2 = list;
        final boolean z2 = z;
        this.pushExecutor.submit(new NamedRunnable("OkHttp %s Push Headers[%s]", new Object[]{this.hostName, Integer.valueOf(i)}) {
            public void execute() {
                boolean onHeaders = SpdyConnection.this.pushObserver.onHeaders(i2, list2, z2);
                if (onHeaders) {
                    try {
                        SpdyConnection.this.frameWriter.rstStream(i2, ErrorCode.CANCEL);
                    } catch (IOException unused) {
                        return;
                    }
                }
                if (onHeaders || z2) {
                    synchronized (SpdyConnection.this) {
                        SpdyConnection.this.currentPushRequests.remove(Integer.valueOf(i2));
                    }
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public void pushDataLater(int i, BufferedSource bufferedSource, int i2, boolean z) throws IOException {
        final Buffer buffer = new Buffer();
        long j = (long) i2;
        bufferedSource.require(j);
        bufferedSource.read(buffer, j);
        if (buffer.size() == j) {
            final int i3 = i;
            final int i4 = i2;
            final boolean z2 = z;
            this.pushExecutor.submit(new NamedRunnable("OkHttp %s Push Data[%s]", new Object[]{this.hostName, Integer.valueOf(i)}) {
                public void execute() {
                    try {
                        boolean onData = SpdyConnection.this.pushObserver.onData(i3, buffer, i4, z2);
                        if (onData) {
                            SpdyConnection.this.frameWriter.rstStream(i3, ErrorCode.CANCEL);
                        }
                        if (onData || z2) {
                            synchronized (SpdyConnection.this) {
                                SpdyConnection.this.currentPushRequests.remove(Integer.valueOf(i3));
                            }
                        }
                    } catch (IOException unused) {
                    }
                }
            });
            return;
        }
        throw new IOException(buffer.size() + " != " + i2);
    }

    /* access modifiers changed from: private */
    public void pushResetLater(int i, ErrorCode errorCode) {
        final int i2 = i;
        final ErrorCode errorCode2 = errorCode;
        this.pushExecutor.submit(new NamedRunnable("OkHttp %s Push Reset[%s]", new Object[]{this.hostName, Integer.valueOf(i)}) {
            public void execute() {
                SpdyConnection.this.pushObserver.onReset(i2, errorCode2);
                synchronized (SpdyConnection.this) {
                    SpdyConnection.this.currentPushRequests.remove(Integer.valueOf(i2));
                }
            }
        });
    }
}
