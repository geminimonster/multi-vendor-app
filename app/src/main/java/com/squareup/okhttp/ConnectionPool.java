package com.squareup.okhttp;

import com.squareup.okhttp.internal.Platform;
import com.squareup.okhttp.internal.Util;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public final class ConnectionPool {
    private static final long DEFAULT_KEEP_ALIVE_DURATION_MS = 300000;
    private static final int MAX_CONNECTIONS_TO_CLEANUP = 2;
    private static final ConnectionPool systemDefault;
    /* access modifiers changed from: private */
    public final LinkedList<Connection> connections = new LinkedList<>();
    private final Runnable connectionsCleanupRunnable = new Runnable() {
        public void run() {
            ArrayList<Connection> arrayList = new ArrayList<>(2);
            synchronized (ConnectionPool.this) {
                ListIterator listIterator = ConnectionPool.this.connections.listIterator(ConnectionPool.this.connections.size());
                int i = 0;
                while (listIterator.hasPrevious()) {
                    Connection connection = (Connection) listIterator.previous();
                    if (connection.isAlive()) {
                        if (!connection.isExpired(ConnectionPool.this.keepAliveDurationNs)) {
                            if (connection.isIdle()) {
                                i++;
                            }
                        }
                    }
                    listIterator.remove();
                    arrayList.add(connection);
                    if (arrayList.size() == 2) {
                        break;
                    }
                }
                ListIterator listIterator2 = ConnectionPool.this.connections.listIterator(ConnectionPool.this.connections.size());
                while (listIterator2.hasPrevious() && i > ConnectionPool.this.maxIdleConnections) {
                    Connection connection2 = (Connection) listIterator2.previous();
                    if (connection2.isIdle()) {
                        arrayList.add(connection2);
                        listIterator2.remove();
                        i--;
                    }
                }
            }
            for (Connection socket : arrayList) {
                Util.closeQuietly(socket.getSocket());
            }
        }
    };
    private final ExecutorService executorService = new ThreadPoolExecutor(0, 1, 60, TimeUnit.SECONDS, new LinkedBlockingQueue(), Util.threadFactory("OkHttp ConnectionPool", true));
    /* access modifiers changed from: private */
    public final long keepAliveDurationNs;
    /* access modifiers changed from: private */
    public final int maxIdleConnections;

    static {
        String property = System.getProperty("http.keepAlive");
        String property2 = System.getProperty("http.keepAliveDuration");
        String property3 = System.getProperty("http.maxConnections");
        long parseLong = property2 != null ? Long.parseLong(property2) : DEFAULT_KEEP_ALIVE_DURATION_MS;
        if (property != null && !Boolean.parseBoolean(property)) {
            systemDefault = new ConnectionPool(0, parseLong);
        } else if (property3 != null) {
            systemDefault = new ConnectionPool(Integer.parseInt(property3), parseLong);
        } else {
            systemDefault = new ConnectionPool(5, parseLong);
        }
    }

    public ConnectionPool(int i, long j) {
        this.maxIdleConnections = i;
        this.keepAliveDurationNs = j * 1000 * 1000;
    }

    /* access modifiers changed from: package-private */
    public List<Connection> getConnections() {
        ArrayList arrayList;
        waitForCleanupCallableToRun();
        synchronized (this) {
            arrayList = new ArrayList(this.connections);
        }
        return arrayList;
    }

    private void waitForCleanupCallableToRun() {
        try {
            this.executorService.submit(new Runnable() {
                public void run() {
                }
            }).get();
        } catch (Exception unused) {
            throw new AssertionError();
        }
    }

    public static ConnectionPool getDefault() {
        return systemDefault;
    }

    public synchronized int getConnectionCount() {
        return this.connections.size();
    }

    public synchronized int getSpdyConnectionCount() {
        int i;
        i = 0;
        Iterator it = this.connections.iterator();
        while (it.hasNext()) {
            if (((Connection) it.next()).isSpdy()) {
                i++;
            }
        }
        return i;
    }

    public synchronized int getHttpConnectionCount() {
        int i;
        i = 0;
        Iterator it = this.connections.iterator();
        while (it.hasNext()) {
            if (!((Connection) it.next()).isSpdy()) {
                i++;
            }
        }
        return i;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0074, code lost:
        r0 = r2;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized com.squareup.okhttp.Connection get(com.squareup.okhttp.Address r9) {
        /*
            r8 = this;
            monitor-enter(r8)
            r0 = 0
            java.util.LinkedList<com.squareup.okhttp.Connection> r1 = r8.connections     // Catch:{ all -> 0x008b }
            java.util.LinkedList<com.squareup.okhttp.Connection> r2 = r8.connections     // Catch:{ all -> 0x008b }
            int r2 = r2.size()     // Catch:{ all -> 0x008b }
            java.util.ListIterator r1 = r1.listIterator(r2)     // Catch:{ all -> 0x008b }
        L_0x000e:
            boolean r2 = r1.hasPrevious()     // Catch:{ all -> 0x008b }
            if (r2 == 0) goto L_0x0075
            java.lang.Object r2 = r1.previous()     // Catch:{ all -> 0x008b }
            com.squareup.okhttp.Connection r2 = (com.squareup.okhttp.Connection) r2     // Catch:{ all -> 0x008b }
            com.squareup.okhttp.Route r3 = r2.getRoute()     // Catch:{ all -> 0x008b }
            com.squareup.okhttp.Address r3 = r3.getAddress()     // Catch:{ all -> 0x008b }
            boolean r3 = r3.equals(r9)     // Catch:{ all -> 0x008b }
            if (r3 == 0) goto L_0x000e
            boolean r3 = r2.isAlive()     // Catch:{ all -> 0x008b }
            if (r3 == 0) goto L_0x000e
            long r3 = java.lang.System.nanoTime()     // Catch:{ all -> 0x008b }
            long r5 = r2.getIdleStartTimeNs()     // Catch:{ all -> 0x008b }
            long r3 = r3 - r5
            long r5 = r8.keepAliveDurationNs     // Catch:{ all -> 0x008b }
            int r7 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
            if (r7 < 0) goto L_0x003e
            goto L_0x000e
        L_0x003e:
            r1.remove()     // Catch:{ all -> 0x008b }
            boolean r3 = r2.isSpdy()     // Catch:{ all -> 0x008b }
            if (r3 != 0) goto L_0x0074
            com.squareup.okhttp.internal.Platform r3 = com.squareup.okhttp.internal.Platform.get()     // Catch:{ SocketException -> 0x0053 }
            java.net.Socket r4 = r2.getSocket()     // Catch:{ SocketException -> 0x0053 }
            r3.tagSocket(r4)     // Catch:{ SocketException -> 0x0053 }
            goto L_0x0074
        L_0x0053:
            r3 = move-exception
            java.net.Socket r2 = r2.getSocket()     // Catch:{ all -> 0x008b }
            com.squareup.okhttp.internal.Util.closeQuietly((java.net.Socket) r2)     // Catch:{ all -> 0x008b }
            com.squareup.okhttp.internal.Platform r2 = com.squareup.okhttp.internal.Platform.get()     // Catch:{ all -> 0x008b }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x008b }
            r4.<init>()     // Catch:{ all -> 0x008b }
            java.lang.String r5 = "Unable to tagSocket(): "
            r4.append(r5)     // Catch:{ all -> 0x008b }
            r4.append(r3)     // Catch:{ all -> 0x008b }
            java.lang.String r3 = r4.toString()     // Catch:{ all -> 0x008b }
            r2.logW(r3)     // Catch:{ all -> 0x008b }
            goto L_0x000e
        L_0x0074:
            r0 = r2
        L_0x0075:
            if (r0 == 0) goto L_0x0082
            boolean r9 = r0.isSpdy()     // Catch:{ all -> 0x008b }
            if (r9 == 0) goto L_0x0082
            java.util.LinkedList<com.squareup.okhttp.Connection> r9 = r8.connections     // Catch:{ all -> 0x008b }
            r9.addFirst(r0)     // Catch:{ all -> 0x008b }
        L_0x0082:
            java.util.concurrent.ExecutorService r9 = r8.executorService     // Catch:{ all -> 0x008b }
            java.lang.Runnable r1 = r8.connectionsCleanupRunnable     // Catch:{ all -> 0x008b }
            r9.execute(r1)     // Catch:{ all -> 0x008b }
            monitor-exit(r8)
            return r0
        L_0x008b:
            r9 = move-exception
            monitor-exit(r8)
            throw r9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.squareup.okhttp.ConnectionPool.get(com.squareup.okhttp.Address):com.squareup.okhttp.Connection");
    }

    /* access modifiers changed from: package-private */
    public void recycle(Connection connection) {
        if (connection.isSpdy() || !connection.clearOwner()) {
            return;
        }
        if (!connection.isAlive()) {
            Util.closeQuietly(connection.getSocket());
            return;
        }
        try {
            Platform.get().untagSocket(connection.getSocket());
            synchronized (this) {
                this.connections.addFirst(connection);
                connection.incrementRecycleCount();
                connection.resetIdleStartTime();
            }
            this.executorService.execute(this.connectionsCleanupRunnable);
        } catch (SocketException e) {
            Platform platform = Platform.get();
            platform.logW("Unable to untagSocket(): " + e);
            Util.closeQuietly(connection.getSocket());
        }
    }

    /* access modifiers changed from: package-private */
    public void share(Connection connection) {
        if (connection.isSpdy()) {
            this.executorService.execute(this.connectionsCleanupRunnable);
            if (connection.isAlive()) {
                synchronized (this) {
                    this.connections.addFirst(connection);
                }
                return;
            }
            return;
        }
        throw new IllegalArgumentException();
    }

    public void evictAll() {
        ArrayList arrayList;
        synchronized (this) {
            arrayList = new ArrayList(this.connections);
            this.connections.clear();
        }
        int size = arrayList.size();
        for (int i = 0; i < size; i++) {
            Util.closeQuietly(((Connection) arrayList.get(i)).getSocket());
        }
    }
}
