package com.squareup.okhttp;

import com.squareup.okhttp.internal.NamedRunnable;
import com.squareup.okhttp.internal.http.HttpEngine;
import com.squareup.okhttp.internal.http.OkHeaders;
import java.io.IOException;
import okio.BufferedSource;

public final class Call {
    volatile boolean canceled;
    private final OkHttpClient client;
    /* access modifiers changed from: private */
    public final Dispatcher dispatcher;
    HttpEngine engine;
    private boolean executed;
    private int redirectionCount;
    /* access modifiers changed from: private */
    public Request request;

    Call(OkHttpClient okHttpClient, Dispatcher dispatcher2, Request request2) {
        this.client = okHttpClient;
        this.dispatcher = dispatcher2;
        this.request = request2;
    }

    public Response execute() throws IOException {
        synchronized (this) {
            if (!this.executed) {
                this.executed = true;
            } else {
                throw new IllegalStateException("Already Executed");
            }
        }
        Response response = getResponse();
        this.engine.releaseConnection();
        if (response != null) {
            return response;
        }
        throw new IOException("Canceled");
    }

    public void enqueue(Callback callback) {
        synchronized (this) {
            if (!this.executed) {
                this.executed = true;
            } else {
                throw new IllegalStateException("Already Executed");
            }
        }
        this.dispatcher.enqueue(new AsyncCall(callback));
    }

    public void cancel() {
        this.canceled = true;
        HttpEngine httpEngine = this.engine;
        if (httpEngine != null) {
            httpEngine.disconnect();
        }
    }

    final class AsyncCall extends NamedRunnable {
        private final Callback responseCallback;

        private AsyncCall(Callback callback) {
            super("OkHttp %s", Call.this.request.urlString());
            this.responseCallback = callback;
        }

        /* access modifiers changed from: package-private */
        public String host() {
            return Call.this.request.url().getHost();
        }

        /* access modifiers changed from: package-private */
        public Request request() {
            return Call.this.request;
        }

        /* access modifiers changed from: package-private */
        public Object tag() {
            return Call.this.request.tag();
        }

        /* access modifiers changed from: package-private */
        public Call get() {
            return Call.this;
        }

        /* access modifiers changed from: protected */
        /* JADX WARNING: Removed duplicated region for block: B:12:0x0037 A[SYNTHETIC, Splitter:B:12:0x0037] */
        /* JADX WARNING: Removed duplicated region for block: B:16:0x004c A[SYNTHETIC, Splitter:B:16:0x004c] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void execute() {
            /*
                r5 = this;
                r0 = 1
                r1 = 0
                com.squareup.okhttp.Call r2 = com.squareup.okhttp.Call.this     // Catch:{ IOException -> 0x0032 }
                com.squareup.okhttp.Response r2 = r2.getResponse()     // Catch:{ IOException -> 0x0032 }
                com.squareup.okhttp.Call r3 = com.squareup.okhttp.Call.this     // Catch:{ IOException -> 0x0032 }
                boolean r1 = r3.canceled     // Catch:{ IOException -> 0x0032 }
                if (r1 == 0) goto L_0x0021
                com.squareup.okhttp.Callback r1 = r5.responseCallback     // Catch:{ IOException -> 0x002e }
                com.squareup.okhttp.Call r2 = com.squareup.okhttp.Call.this     // Catch:{ IOException -> 0x002e }
                com.squareup.okhttp.Request r2 = r2.request     // Catch:{ IOException -> 0x002e }
                java.io.IOException r3 = new java.io.IOException     // Catch:{ IOException -> 0x002e }
                java.lang.String r4 = "Canceled"
                r3.<init>(r4)     // Catch:{ IOException -> 0x002e }
                r1.onFailure(r2, r3)     // Catch:{ IOException -> 0x002e }
                goto L_0x0042
            L_0x0021:
                com.squareup.okhttp.Call r1 = com.squareup.okhttp.Call.this     // Catch:{ IOException -> 0x002e }
                com.squareup.okhttp.internal.http.HttpEngine r1 = r1.engine     // Catch:{ IOException -> 0x002e }
                r1.releaseConnection()     // Catch:{ IOException -> 0x002e }
                com.squareup.okhttp.Callback r1 = r5.responseCallback     // Catch:{ IOException -> 0x002e }
                r1.onResponse(r2)     // Catch:{ IOException -> 0x002e }
                goto L_0x0042
            L_0x002e:
                r1 = move-exception
                goto L_0x0035
            L_0x0030:
                r0 = move-exception
                goto L_0x0052
            L_0x0032:
                r0 = move-exception
                r1 = r0
                r0 = 0
            L_0x0035:
                if (r0 != 0) goto L_0x004c
                com.squareup.okhttp.Callback r0 = r5.responseCallback     // Catch:{ all -> 0x0030 }
                com.squareup.okhttp.Call r2 = com.squareup.okhttp.Call.this     // Catch:{ all -> 0x0030 }
                com.squareup.okhttp.Request r2 = r2.request     // Catch:{ all -> 0x0030 }
                r0.onFailure(r2, r1)     // Catch:{ all -> 0x0030 }
            L_0x0042:
                com.squareup.okhttp.Call r0 = com.squareup.okhttp.Call.this
                com.squareup.okhttp.Dispatcher r0 = r0.dispatcher
                r0.finished(r5)
                return
            L_0x004c:
                java.lang.RuntimeException r0 = new java.lang.RuntimeException     // Catch:{ all -> 0x0030 }
                r0.<init>(r1)     // Catch:{ all -> 0x0030 }
                throw r0     // Catch:{ all -> 0x0030 }
            L_0x0052:
                com.squareup.okhttp.Call r1 = com.squareup.okhttp.Call.this
                com.squareup.okhttp.Dispatcher r1 = r1.dispatcher
                r1.finished(r5)
                throw r0
            */
            throw new UnsupportedOperationException("Method not decompiled: com.squareup.okhttp.Call.AsyncCall.execute():void");
        }
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x006e A[SYNTHETIC, Splitter:B:18:0x006e] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.squareup.okhttp.Response getResponse() throws java.io.IOException {
        /*
            r10 = this;
            com.squareup.okhttp.Request r0 = r10.request
            com.squareup.okhttp.RequestBody r0 = r0.body()
            r1 = 0
            if (r0 == 0) goto L_0x0046
            com.squareup.okhttp.Request r2 = r10.request
            com.squareup.okhttp.Request$Builder r2 = r2.newBuilder()
            com.squareup.okhttp.MediaType r3 = r0.contentType()
            if (r3 == 0) goto L_0x001e
            java.lang.String r3 = r3.toString()
            java.lang.String r4 = "Content-Type"
            r2.header(r4, r3)
        L_0x001e:
            long r3 = r0.contentLength()
            r5 = -1
            java.lang.String r0 = "Content-Length"
            java.lang.String r7 = "Transfer-Encoding"
            int r8 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
            if (r8 == 0) goto L_0x0037
            java.lang.String r3 = java.lang.Long.toString(r3)
            r2.header(r0, r3)
            r2.removeHeader(r7)
            goto L_0x003f
        L_0x0037:
            java.lang.String r3 = "chunked"
            r2.header(r7, r3)
            r2.removeHeader(r0)
        L_0x003f:
            com.squareup.okhttp.Request r0 = r2.build()
            r10.request = r0
            goto L_0x0058
        L_0x0046:
            com.squareup.okhttp.Request r0 = r10.request
            java.lang.String r0 = r0.method()
            boolean r0 = com.squareup.okhttp.internal.http.HttpMethod.hasRequestBody(r0)
            if (r0 == 0) goto L_0x0058
            com.squareup.okhttp.internal.http.RetryableSink r0 = com.squareup.okhttp.internal.Util.emptySink()
            r8 = r0
            goto L_0x0059
        L_0x0058:
            r8 = r1
        L_0x0059:
            com.squareup.okhttp.internal.http.HttpEngine r0 = new com.squareup.okhttp.internal.http.HttpEngine
            com.squareup.okhttp.OkHttpClient r3 = r10.client
            com.squareup.okhttp.Request r4 = r10.request
            r5 = 0
            r6 = 0
            r7 = 0
            r9 = 0
            r2 = r0
            r2.<init>(r3, r4, r5, r6, r7, r8, r9)
            r10.engine = r0
        L_0x0069:
            boolean r0 = r10.canceled
            if (r0 == 0) goto L_0x006e
            return r1
        L_0x006e:
            com.squareup.okhttp.internal.http.HttpEngine r0 = r10.engine     // Catch:{ IOException -> 0x0114 }
            r0.sendRequest()     // Catch:{ IOException -> 0x0114 }
            com.squareup.okhttp.Request r0 = r10.request     // Catch:{ IOException -> 0x0114 }
            com.squareup.okhttp.RequestBody r0 = r0.body()     // Catch:{ IOException -> 0x0114 }
            if (r0 == 0) goto L_0x008a
            com.squareup.okhttp.internal.http.HttpEngine r0 = r10.engine     // Catch:{ IOException -> 0x0114 }
            okio.BufferedSink r0 = r0.getBufferedRequestBody()     // Catch:{ IOException -> 0x0114 }
            com.squareup.okhttp.Request r2 = r10.request     // Catch:{ IOException -> 0x0114 }
            com.squareup.okhttp.RequestBody r2 = r2.body()     // Catch:{ IOException -> 0x0114 }
            r2.writeTo(r0)     // Catch:{ IOException -> 0x0114 }
        L_0x008a:
            com.squareup.okhttp.internal.http.HttpEngine r0 = r10.engine     // Catch:{ IOException -> 0x0114 }
            r0.readResponse()     // Catch:{ IOException -> 0x0114 }
            com.squareup.okhttp.internal.http.HttpEngine r0 = r10.engine
            com.squareup.okhttp.Response r9 = r0.getResponse()
            com.squareup.okhttp.internal.http.HttpEngine r0 = r10.engine
            com.squareup.okhttp.Request r0 = r0.followUpRequest()
            if (r0 != 0) goto L_0x00ba
            com.squareup.okhttp.internal.http.HttpEngine r0 = r10.engine
            r0.releaseConnection()
            com.squareup.okhttp.Response$Builder r0 = r9.newBuilder()
            com.squareup.okhttp.Call$RealResponseBody r1 = new com.squareup.okhttp.Call$RealResponseBody
            com.squareup.okhttp.internal.http.HttpEngine r2 = r10.engine
            okio.BufferedSource r2 = r2.getResponseBody()
            r1.<init>(r9, r2)
            com.squareup.okhttp.Response$Builder r0 = r0.body(r1)
            com.squareup.okhttp.Response r0 = r0.build()
            return r0
        L_0x00ba:
            com.squareup.okhttp.internal.http.HttpEngine r2 = r10.engine
            com.squareup.okhttp.Response r2 = r2.getResponse()
            boolean r2 = r2.isRedirect()
            if (r2 == 0) goto L_0x00ea
            int r2 = r10.redirectionCount
            int r2 = r2 + 1
            r10.redirectionCount = r2
            r3 = 20
            if (r2 > r3) goto L_0x00d1
            goto L_0x00ea
        L_0x00d1:
            java.net.ProtocolException r0 = new java.net.ProtocolException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Too many redirects: "
            r1.append(r2)
            int r2 = r10.redirectionCount
            r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x00ea:
            com.squareup.okhttp.internal.http.HttpEngine r2 = r10.engine
            java.net.URL r3 = r0.url()
            boolean r2 = r2.sameConnection(r3)
            if (r2 != 0) goto L_0x00fb
            com.squareup.okhttp.internal.http.HttpEngine r2 = r10.engine
            r2.releaseConnection()
        L_0x00fb:
            com.squareup.okhttp.internal.http.HttpEngine r2 = r10.engine
            com.squareup.okhttp.Connection r6 = r2.close()
            r10.request = r0
            com.squareup.okhttp.internal.http.HttpEngine r0 = new com.squareup.okhttp.internal.http.HttpEngine
            com.squareup.okhttp.OkHttpClient r3 = r10.client
            com.squareup.okhttp.Request r4 = r10.request
            r5 = 0
            r7 = 0
            r8 = 0
            r2 = r0
            r2.<init>(r3, r4, r5, r6, r7, r8, r9)
            r10.engine = r0
            goto L_0x0069
        L_0x0114:
            r0 = move-exception
            com.squareup.okhttp.internal.http.HttpEngine r2 = r10.engine
            com.squareup.okhttp.internal.http.HttpEngine r2 = r2.recover(r0, r1)
            if (r2 == 0) goto L_0x0121
            r10.engine = r2
            goto L_0x0069
        L_0x0121:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.squareup.okhttp.Call.getResponse():com.squareup.okhttp.Response");
    }

    private static class RealResponseBody extends ResponseBody {
        private final Response response;
        private final BufferedSource source;

        RealResponseBody(Response response2, BufferedSource bufferedSource) {
            this.response = response2;
            this.source = bufferedSource;
        }

        public MediaType contentType() {
            String header = this.response.header("Content-Type");
            if (header != null) {
                return MediaType.parse(header);
            }
            return null;
        }

        public long contentLength() {
            return OkHeaders.contentLength(this.response);
        }

        public BufferedSource source() {
            return this.source;
        }
    }
}
