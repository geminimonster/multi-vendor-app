package io.supercharge.shimmerlayout;

public final class R {

    public static final class attr {
        public static final int shimmer_angle = 2130969280;
        public static final int shimmer_animation_duration = 2130969281;
        public static final int shimmer_auto_start = 2130969282;
        public static final int shimmer_color = 2130969283;
        public static final int shimmer_gradient_center_color_width = 2130969294;
        public static final int shimmer_mask_width = 2130969295;
        public static final int shimmer_reverse_animation = 2130969296;

        private attr() {
        }
    }

    public static final class color {
        public static final int shimmer_color = 2131099911;

        private color() {
        }
    }

    public static final class string {
        public static final int app_name = 2131820630;

        private string() {
        }
    }

    public static final class styleable {
        public static final int[] ShimmerLayout = {com.store.proshop.R.attr.shimmer_angle, com.store.proshop.R.attr.shimmer_animation_duration, com.store.proshop.R.attr.shimmer_auto_start, com.store.proshop.R.attr.shimmer_color, com.store.proshop.R.attr.shimmer_gradient_center_color_width, com.store.proshop.R.attr.shimmer_mask_width, com.store.proshop.R.attr.shimmer_reverse_animation};
        public static final int ShimmerLayout_shimmer_angle = 0;
        public static final int ShimmerLayout_shimmer_animation_duration = 1;
        public static final int ShimmerLayout_shimmer_auto_start = 2;
        public static final int ShimmerLayout_shimmer_color = 3;
        public static final int ShimmerLayout_shimmer_gradient_center_color_width = 4;
        public static final int ShimmerLayout_shimmer_mask_width = 5;
        public static final int ShimmerLayout_shimmer_reverse_animation = 6;

        private styleable() {
        }
    }

    private R() {
    }
}
