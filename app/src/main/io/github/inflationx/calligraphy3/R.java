package io.github.inflationx.calligraphy3;

public final class R {

    public static final class attr {
        public static final int fontPath = 2130968960;

        private attr() {
        }
    }

    public static final class id {
        public static final int calligraphy_tag_id = 2131296383;
        public static final int viewpump_tag_id = 2131297120;

        private id() {
        }
    }

    private R() {
    }
}
