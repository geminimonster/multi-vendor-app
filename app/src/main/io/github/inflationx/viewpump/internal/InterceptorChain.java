package io.github.inflationx.viewpump.internal;

import com.facebook.share.internal.ShareConstants;
import io.github.inflationx.viewpump.InflateRequest;
import io.github.inflationx.viewpump.InflateResult;
import io.github.inflationx.viewpump.Interceptor;
import java.util.List;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\b\u0000\u0018\u00002\u00020\u0001B#\u0012\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\b¢\u0006\u0002\u0010\tJ\u0010\u0010\n\u001a\u00020\u000b2\u0006\u0010\u0007\u001a\u00020\bH\u0016J\b\u0010\u0007\u001a\u00020\bH\u0016R\u000e\u0010\u0005\u001a\u00020\u0006X\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\bX\u0004¢\u0006\u0002\n\u0000¨\u0006\f"}, d2 = {"Lio/github/inflationx/viewpump/internal/-InterceptorChain;", "Lio/github/inflationx/viewpump/Interceptor$Chain;", "interceptors", "", "Lio/github/inflationx/viewpump/Interceptor;", "index", "", "request", "Lio/github/inflationx/viewpump/InflateRequest;", "(Ljava/util/List;ILio/github/inflationx/viewpump/InflateRequest;)V", "proceed", "Lio/github/inflationx/viewpump/InflateResult;", "viewpump_release"}, k = 1, mv = {1, 1, 13})
/* renamed from: io.github.inflationx.viewpump.internal.-InterceptorChain  reason: invalid class name */
/* compiled from: -InterceptorChain.kt */
public final class InterceptorChain implements Interceptor.Chain {
    private final int index;
    private final List<Interceptor> interceptors;
    private final InflateRequest request;

    public InterceptorChain(List<? extends Interceptor> list, int i, InflateRequest inflateRequest) {
        Intrinsics.checkParameterIsNotNull(list, "interceptors");
        Intrinsics.checkParameterIsNotNull(inflateRequest, ShareConstants.WEB_DIALOG_RESULT_PARAM_REQUEST_ID);
        this.interceptors = list;
        this.index = i;
        this.request = inflateRequest;
    }

    public InflateRequest request() {
        return this.request;
    }

    public InflateResult proceed(InflateRequest inflateRequest) {
        Intrinsics.checkParameterIsNotNull(inflateRequest, ShareConstants.WEB_DIALOG_RESULT_PARAM_REQUEST_ID);
        if (this.index < this.interceptors.size()) {
            return this.interceptors.get(this.index).intercept(new InterceptorChain(this.interceptors, this.index + 1, inflateRequest));
        }
        throw new AssertionError("no interceptors added to the chain");
    }
}
