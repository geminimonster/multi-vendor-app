package io.github.inflationx.viewpump.internal;

import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.core.os.BuildCompat;
import io.github.inflationx.viewpump.FallbackViewCreator;
import io.github.inflationx.viewpump.InflateRequest;
import io.github.inflationx.viewpump.R;
import io.github.inflationx.viewpump.ViewPump;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Set;
import kotlin.Lazy;
import kotlin.LazyKt;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.collections.SetsKt;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.PropertyReference1Impl;
import kotlin.jvm.internal.Reflection;
import kotlin.reflect.KProperty;
import kotlin.text.StringsKt;
import org.xmlpull.v1.XmlPullParser;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000j\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u000e\b\u0000\u0018\u0000 /2\u00020\u00012\u00020\u0002:\n./01234567B\u001d\u0012\u0006\u0010\u0003\u001a\u00020\u0001\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ\u0010\u0010\u000f\u001a\u00020\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\u0016J.\u0010\u0010\u001a\u0004\u0018\u00010\u00112\b\u0010\u0012\u001a\u0004\u0018\u00010\u00112\u0006\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u00052\b\u0010\u0016\u001a\u0004\u0018\u00010\u0017H\u0002J$\u0010\u0018\u001a\u0004\u0018\u00010\u00112\u0006\u0010\u0019\u001a\u00020\u001a2\b\u0010\u001b\u001a\u0004\u0018\u00010\u001c2\u0006\u0010\u001d\u001a\u00020\u0007H\u0016J\"\u0010\u0018\u001a\u00020\u00112\u0006\u0010\u001e\u001a\u00020\u001f2\b\u0010\u001b\u001a\u0004\u0018\u00010\u001c2\u0006\u0010\u001d\u001a\u00020\u0007H\u0016J6\u0010 \u001a\u0004\u0018\u00010\u00112\b\u0010!\u001a\u0004\u0018\u00010\u00112\u0006\u0010\u0012\u001a\u00020\u00112\u0006\u0010\u0013\u001a\u00020\u00142\u0006\u0010\"\u001a\u00020\u00052\b\u0010\u0016\u001a\u0004\u0018\u00010\u0017H\u0016J&\u0010#\u001a\u0004\u0018\u00010\u00112\b\u0010!\u001a\u0004\u0018\u00010\u00112\u0006\u0010\u0013\u001a\u00020\u00142\b\u0010\u0016\u001a\u0004\u0018\u00010\u0017H\u0014J\u001c\u0010#\u001a\u0004\u0018\u00010\u00112\u0006\u0010\u0013\u001a\u00020\u00142\b\u0010\u0016\u001a\u0004\u0018\u00010\u0017H\u0014J\u0010\u0010$\u001a\u00020%2\u0006\u0010&\u001a\u00020'H\u0016J\u0010\u0010(\u001a\u00020%2\u0006\u0010)\u001a\u00020*H\u0016J\b\u0010+\u001a\u00020%H\u0002J\u0010\u0010,\u001a\u00020%2\u0006\u0010\u0006\u001a\u00020\u0007H\u0002J&\u0010-\u001a\u0004\u0018\u00010\u00112\b\u0010!\u001a\u0004\u0018\u00010\u00112\u0006\u0010\u0013\u001a\u00020\u00142\b\u0010\u0016\u001a\u0004\u0018\u00010\u0017H\u0002J\u001c\u0010-\u001a\u0004\u0018\u00010\u00112\u0006\u0010\u0013\u001a\u00020\u00142\b\u0010\u0016\u001a\u0004\u0018\u00010\u0017H\u0002R\u000e\u0010\t\u001a\u00020\u0007X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\u000bX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u0007X\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u0007X\u000e¢\u0006\u0002\n\u0000¨\u00068"}, d2 = {"Lio/github/inflationx/viewpump/internal/-ViewPumpLayoutInflater;", "Landroid/view/LayoutInflater;", "Lio/github/inflationx/viewpump/internal/-ViewPumpActivityFactory;", "original", "newContext", "Landroid/content/Context;", "cloned", "", "(Landroid/view/LayoutInflater;Landroid/content/Context;Z)V", "IS_AT_LEAST_Q", "nameAndAttrsViewCreator", "Lio/github/inflationx/viewpump/FallbackViewCreator;", "parentAndNameAndAttrsViewCreator", "setPrivateFactory", "storeLayoutResId", "cloneInContext", "createCustomViewInternal", "Landroid/view/View;", "view", "name", "", "viewContext", "attrs", "Landroid/util/AttributeSet;", "inflate", "resource", "", "root", "Landroid/view/ViewGroup;", "attachToRoot", "parser", "Lorg/xmlpull/v1/XmlPullParser;", "onActivityCreateView", "parent", "context", "onCreateView", "setFactory", "", "factory", "Landroid/view/LayoutInflater$Factory;", "setFactory2", "factory2", "Landroid/view/LayoutInflater$Factory2;", "setPrivateFactoryInternal", "setUpLayoutFactories", "superOnCreateView", "ActivityViewCreator", "Companion", "NameAndAttrsViewCreator", "ParentAndNameAndAttrsViewCreator", "PrivateWrapperFactory2", "PrivateWrapperFactory2ViewCreator", "WrapperFactory", "WrapperFactory2", "WrapperFactory2ViewCreator", "WrapperFactoryViewCreator", "viewpump_release"}, k = 1, mv = {1, 1, 13})
/* renamed from: io.github.inflationx.viewpump.internal.-ViewPumpLayoutInflater  reason: invalid class name */
/* compiled from: -ViewPumpLayoutInflater.kt */
public final class ViewPumpLayoutInflater extends LayoutInflater implements ViewPumpActivityFactory {
    /* access modifiers changed from: private */
    public static final Set<String> CLASS_PREFIX_LIST = SetsKt.setOf("android.widget.", "android.webkit.");
    /* access modifiers changed from: private */
    public static final Lazy CONSTRUCTOR_ARGS_FIELD$delegate = LazyKt.lazy(ViewPumpLayoutInflater$Companion$CONSTRUCTOR_ARGS_FIELD$2.INSTANCE);
    public static final Companion Companion = new Companion((DefaultConstructorMarker) null);
    private final boolean IS_AT_LEAST_Q;
    private final FallbackViewCreator nameAndAttrsViewCreator;
    private final FallbackViewCreator parentAndNameAndAttrsViewCreator;
    private boolean setPrivateFactory;
    private boolean storeLayoutResId;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ViewPumpLayoutInflater(LayoutInflater layoutInflater, Context context, boolean z) {
        super(layoutInflater, context);
        Intrinsics.checkParameterIsNotNull(layoutInflater, "original");
        Intrinsics.checkParameterIsNotNull(context, "newContext");
        this.IS_AT_LEAST_Q = Build.VERSION.SDK_INT > 28 || BuildCompat.isAtLeastQ();
        this.nameAndAttrsViewCreator = new NameAndAttrsViewCreator(this);
        this.parentAndNameAndAttrsViewCreator = new ParentAndNameAndAttrsViewCreator(this);
        this.storeLayoutResId = ViewPump.Companion.get().isStoreLayoutResId();
        setUpLayoutFactories(z);
    }

    public LayoutInflater cloneInContext(Context context) {
        Intrinsics.checkParameterIsNotNull(context, "newContext");
        return new ViewPumpLayoutInflater(this, context, true);
    }

    public View inflate(int i, ViewGroup viewGroup, boolean z) {
        View inflate = super.inflate(i, viewGroup, z);
        if (inflate != null && this.storeLayoutResId) {
            inflate.setTag(R.id.viewpump_layout_res, Integer.valueOf(i));
        }
        return inflate;
    }

    public View inflate(XmlPullParser xmlPullParser, ViewGroup viewGroup, boolean z) {
        Intrinsics.checkParameterIsNotNull(xmlPullParser, "parser");
        setPrivateFactoryInternal();
        View inflate = super.inflate(xmlPullParser, viewGroup, z);
        Intrinsics.checkExpressionValueIsNotNull(inflate, "super.inflate(parser, root, attachToRoot)");
        return inflate;
    }

    private final void setUpLayoutFactories(boolean z) {
        if (!z) {
            if (getFactory2() != null && !(getFactory2() instanceof WrapperFactory2)) {
                setFactory2(getFactory2());
            }
            if (getFactory() != null && !(getFactory() instanceof WrapperFactory)) {
                setFactory(getFactory());
            }
        }
    }

    public void setFactory(LayoutInflater.Factory factory) {
        Intrinsics.checkParameterIsNotNull(factory, "factory");
        if (!(factory instanceof WrapperFactory)) {
            super.setFactory(new WrapperFactory(factory));
        } else {
            super.setFactory(factory);
        }
    }

    public void setFactory2(LayoutInflater.Factory2 factory2) {
        Intrinsics.checkParameterIsNotNull(factory2, "factory2");
        if (!(factory2 instanceof WrapperFactory2)) {
            super.setFactory2(new WrapperFactory2(factory2));
        } else {
            super.setFactory2(factory2);
        }
    }

    private final void setPrivateFactoryInternal() {
        if (this.setPrivateFactory || !ViewPump.Companion.get().isReflection()) {
            return;
        }
        if (!(getContext() instanceof LayoutInflater.Factory2)) {
            this.setPrivateFactory = true;
            return;
        }
        Method accessibleMethod = ReflectionUtils.getAccessibleMethod(LayoutInflater.class, "setPrivateFactory");
        Object[] objArr = new Object[1];
        Context context = getContext();
        if (context != null) {
            objArr[0] = new PrivateWrapperFactory2((LayoutInflater.Factory2) context, this);
            ReflectionUtils.invokeMethod(accessibleMethod, this, objArr);
            this.setPrivateFactory = true;
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type android.view.LayoutInflater.Factory2");
    }

    public View onActivityCreateView(View view, View view2, String str, Context context, AttributeSet attributeSet) {
        Intrinsics.checkParameterIsNotNull(view2, "view");
        Intrinsics.checkParameterIsNotNull(str, "name");
        Intrinsics.checkParameterIsNotNull(context, "context");
        return ViewPump.Companion.get().inflate(new InflateRequest(str, context, attributeSet, view, new ActivityViewCreator(this, view2))).view();
    }

    /* access modifiers changed from: protected */
    public View onCreateView(View view, String str, AttributeSet attributeSet) throws ClassNotFoundException {
        Intrinsics.checkParameterIsNotNull(str, "name");
        ViewPump viewPump = ViewPump.Companion.get();
        Context context = getContext();
        Intrinsics.checkExpressionValueIsNotNull(context, "context");
        return viewPump.inflate(new InflateRequest(str, context, attributeSet, view, this.parentAndNameAndAttrsViewCreator)).view();
    }

    /* access modifiers changed from: protected */
    public View onCreateView(String str, AttributeSet attributeSet) throws ClassNotFoundException {
        Intrinsics.checkParameterIsNotNull(str, "name");
        ViewPump viewPump = ViewPump.Companion.get();
        Context context = getContext();
        Intrinsics.checkExpressionValueIsNotNull(context, "context");
        return viewPump.inflate(new InflateRequest(str, context, attributeSet, (View) null, this.nameAndAttrsViewCreator, 8, (DefaultConstructorMarker) null)).view();
    }

    /* access modifiers changed from: private */
    public final View createCustomViewInternal(View view, String str, Context context, AttributeSet attributeSet) {
        if (!ViewPump.Companion.get().isCustomViewCreation() || view != null || StringsKt.indexOf$default((CharSequence) str, '.', 0, false, 6, (Object) null) <= -1) {
            return view;
        }
        if (this.IS_AT_LEAST_Q) {
            return cloneInContext(context).createView(str, (String) null, attributeSet);
        }
        Object obj = Companion.getCONSTRUCTOR_ARGS_FIELD().get(this);
        if (obj != null) {
            Object[] objArr = (Object[]) obj;
            Object obj2 = objArr[0];
            objArr[0] = context;
            ReflectionUtils.setValueQuietly(Companion.getCONSTRUCTOR_ARGS_FIELD(), this, objArr);
            try {
                view = createView(str, (String) null, attributeSet);
                objArr[0] = obj2;
            } catch (ClassNotFoundException unused) {
                objArr[0] = obj2;
            } catch (Throwable th) {
                objArr[0] = obj2;
                ReflectionUtils.setValueQuietly(Companion.getCONSTRUCTOR_ARGS_FIELD(), this, objArr);
                throw th;
            }
            ReflectionUtils.setValueQuietly(Companion.getCONSTRUCTOR_ARGS_FIELD(), this, objArr);
            return view;
        }
        throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<kotlin.Any>");
    }

    /* access modifiers changed from: private */
    public final View superOnCreateView(View view, String str, AttributeSet attributeSet) {
        try {
            return super.onCreateView(view, str, attributeSet);
        } catch (ClassNotFoundException unused) {
            return null;
        }
    }

    /* access modifiers changed from: private */
    public final View superOnCreateView(String str, AttributeSet attributeSet) {
        try {
            return super.onCreateView(str, attributeSet);
        } catch (ClassNotFoundException unused) {
            return null;
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0002\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J.\u0010\u0007\u001a\u0004\u0018\u00010\u00052\b\u0010\b\u001a\u0004\u0018\u00010\u00052\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\f2\b\u0010\r\u001a\u0004\u0018\u00010\u000eH\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000¨\u0006\u000f"}, d2 = {"Lio/github/inflationx/viewpump/internal/-ViewPumpLayoutInflater$ActivityViewCreator;", "Lio/github/inflationx/viewpump/FallbackViewCreator;", "inflater", "Lio/github/inflationx/viewpump/internal/-ViewPumpLayoutInflater;", "view", "Landroid/view/View;", "(Lio/github/inflationx/viewpump/internal/-ViewPumpLayoutInflater;Landroid/view/View;)V", "onCreateView", "parent", "name", "", "context", "Landroid/content/Context;", "attrs", "Landroid/util/AttributeSet;", "viewpump_release"}, k = 1, mv = {1, 1, 13})
    /* renamed from: io.github.inflationx.viewpump.internal.-ViewPumpLayoutInflater$ActivityViewCreator */
    /* compiled from: -ViewPumpLayoutInflater.kt */
    private static final class ActivityViewCreator implements FallbackViewCreator {
        private final ViewPumpLayoutInflater inflater;
        private final View view;

        public ActivityViewCreator(ViewPumpLayoutInflater r2, View view2) {
            Intrinsics.checkParameterIsNotNull(r2, "inflater");
            Intrinsics.checkParameterIsNotNull(view2, "view");
            this.inflater = r2;
            this.view = view2;
        }

        public View onCreateView(View view2, String str, Context context, AttributeSet attributeSet) {
            Intrinsics.checkParameterIsNotNull(str, "name");
            Intrinsics.checkParameterIsNotNull(context, "context");
            return this.inflater.createCustomViewInternal(this.view, str, context, attributeSet);
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J.\u0010\u0005\u001a\u0004\u0018\u00010\u00062\b\u0010\u0007\u001a\u0004\u0018\u00010\u00062\u0006\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000b2\b\u0010\f\u001a\u0004\u0018\u00010\rH\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\u000e"}, d2 = {"Lio/github/inflationx/viewpump/internal/-ViewPumpLayoutInflater$ParentAndNameAndAttrsViewCreator;", "Lio/github/inflationx/viewpump/FallbackViewCreator;", "inflater", "Lio/github/inflationx/viewpump/internal/-ViewPumpLayoutInflater;", "(Lio/github/inflationx/viewpump/internal/-ViewPumpLayoutInflater;)V", "onCreateView", "Landroid/view/View;", "parent", "name", "", "context", "Landroid/content/Context;", "attrs", "Landroid/util/AttributeSet;", "viewpump_release"}, k = 1, mv = {1, 1, 13})
    /* renamed from: io.github.inflationx.viewpump.internal.-ViewPumpLayoutInflater$ParentAndNameAndAttrsViewCreator */
    /* compiled from: -ViewPumpLayoutInflater.kt */
    private static final class ParentAndNameAndAttrsViewCreator implements FallbackViewCreator {
        private final ViewPumpLayoutInflater inflater;

        public ParentAndNameAndAttrsViewCreator(ViewPumpLayoutInflater r2) {
            Intrinsics.checkParameterIsNotNull(r2, "inflater");
            this.inflater = r2;
        }

        public View onCreateView(View view, String str, Context context, AttributeSet attributeSet) {
            Intrinsics.checkParameterIsNotNull(str, "name");
            Intrinsics.checkParameterIsNotNull(context, "context");
            return this.inflater.superOnCreateView(view, str, attributeSet);
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J.\u0010\u0005\u001a\u0004\u0018\u00010\u00062\b\u0010\u0007\u001a\u0004\u0018\u00010\u00062\u0006\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000b2\b\u0010\f\u001a\u0004\u0018\u00010\rH\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\u000e"}, d2 = {"Lio/github/inflationx/viewpump/internal/-ViewPumpLayoutInflater$NameAndAttrsViewCreator;", "Lio/github/inflationx/viewpump/FallbackViewCreator;", "inflater", "Lio/github/inflationx/viewpump/internal/-ViewPumpLayoutInflater;", "(Lio/github/inflationx/viewpump/internal/-ViewPumpLayoutInflater;)V", "onCreateView", "Landroid/view/View;", "parent", "name", "", "context", "Landroid/content/Context;", "attrs", "Landroid/util/AttributeSet;", "viewpump_release"}, k = 1, mv = {1, 1, 13})
    /* renamed from: io.github.inflationx.viewpump.internal.-ViewPumpLayoutInflater$NameAndAttrsViewCreator */
    /* compiled from: -ViewPumpLayoutInflater.kt */
    private static final class NameAndAttrsViewCreator implements FallbackViewCreator {
        private final ViewPumpLayoutInflater inflater;

        public NameAndAttrsViewCreator(ViewPumpLayoutInflater r2) {
            Intrinsics.checkParameterIsNotNull(r2, "inflater");
            this.inflater = r2;
        }

        public View onCreateView(View view, String str, Context context, AttributeSet attributeSet) {
            Intrinsics.checkParameterIsNotNull(str, "name");
            Intrinsics.checkParameterIsNotNull(context, "context");
            View view2 = null;
            for (String createView : ViewPumpLayoutInflater.CLASS_PREFIX_LIST) {
                try {
                    view2 = this.inflater.createView(str, createView, attributeSet);
                    if (view2 != null) {
                        break;
                    }
                } catch (ClassNotFoundException unused) {
                }
            }
            return view2 == null ? this.inflater.superOnCreateView(str, attributeSet) : view2;
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0001¢\u0006\u0002\u0010\u0003J$\u0010\u0006\u001a\u0004\u0018\u00010\u00072\u0006\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000b2\b\u0010\f\u001a\u0004\u0018\u00010\rH\u0016R\u000e\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000¨\u0006\u000e"}, d2 = {"Lio/github/inflationx/viewpump/internal/-ViewPumpLayoutInflater$WrapperFactory;", "Landroid/view/LayoutInflater$Factory;", "factory", "(Landroid/view/LayoutInflater$Factory;)V", "viewCreator", "Lio/github/inflationx/viewpump/FallbackViewCreator;", "onCreateView", "Landroid/view/View;", "name", "", "context", "Landroid/content/Context;", "attrs", "Landroid/util/AttributeSet;", "viewpump_release"}, k = 1, mv = {1, 1, 13})
    /* renamed from: io.github.inflationx.viewpump.internal.-ViewPumpLayoutInflater$WrapperFactory */
    /* compiled from: -ViewPumpLayoutInflater.kt */
    private static final class WrapperFactory implements LayoutInflater.Factory {
        private final FallbackViewCreator viewCreator;

        public WrapperFactory(LayoutInflater.Factory factory) {
            Intrinsics.checkParameterIsNotNull(factory, "factory");
            this.viewCreator = new WrapperFactoryViewCreator(factory);
        }

        public View onCreateView(String str, Context context, AttributeSet attributeSet) {
            Intrinsics.checkParameterIsNotNull(str, "name");
            Intrinsics.checkParameterIsNotNull(context, "context");
            return ViewPump.Companion.get().inflate(new InflateRequest(str, context, attributeSet, (View) null, this.viewCreator, 8, (DefaultConstructorMarker) null)).view();
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J.\u0010\u0005\u001a\u0004\u0018\u00010\u00062\b\u0010\u0007\u001a\u0004\u0018\u00010\u00062\u0006\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000b2\b\u0010\f\u001a\u0004\u0018\u00010\rH\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\u000e"}, d2 = {"Lio/github/inflationx/viewpump/internal/-ViewPumpLayoutInflater$WrapperFactoryViewCreator;", "Lio/github/inflationx/viewpump/FallbackViewCreator;", "factory", "Landroid/view/LayoutInflater$Factory;", "(Landroid/view/LayoutInflater$Factory;)V", "onCreateView", "Landroid/view/View;", "parent", "name", "", "context", "Landroid/content/Context;", "attrs", "Landroid/util/AttributeSet;", "viewpump_release"}, k = 1, mv = {1, 1, 13})
    /* renamed from: io.github.inflationx.viewpump.internal.-ViewPumpLayoutInflater$WrapperFactoryViewCreator */
    /* compiled from: -ViewPumpLayoutInflater.kt */
    private static final class WrapperFactoryViewCreator implements FallbackViewCreator {
        private final LayoutInflater.Factory factory;

        public WrapperFactoryViewCreator(LayoutInflater.Factory factory2) {
            Intrinsics.checkParameterIsNotNull(factory2, "factory");
            this.factory = factory2;
        }

        public View onCreateView(View view, String str, Context context, AttributeSet attributeSet) {
            Intrinsics.checkParameterIsNotNull(str, "name");
            Intrinsics.checkParameterIsNotNull(context, "context");
            return this.factory.onCreateView(str, context, attributeSet);
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0012\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0001¢\u0006\u0002\u0010\u0003J.\u0010\u0006\u001a\u0004\u0018\u00010\u00072\b\u0010\b\u001a\u0004\u0018\u00010\u00072\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\f2\b\u0010\r\u001a\u0004\u0018\u00010\u000eH\u0016J$\u0010\u0006\u001a\u0004\u0018\u00010\u00072\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\f2\b\u0010\r\u001a\u0004\u0018\u00010\u000eH\u0016R\u000e\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000¨\u0006\u000f"}, d2 = {"Lio/github/inflationx/viewpump/internal/-ViewPumpLayoutInflater$WrapperFactory2;", "Landroid/view/LayoutInflater$Factory2;", "factory2", "(Landroid/view/LayoutInflater$Factory2;)V", "viewCreator", "Lio/github/inflationx/viewpump/internal/-ViewPumpLayoutInflater$WrapperFactory2ViewCreator;", "onCreateView", "Landroid/view/View;", "parent", "name", "", "context", "Landroid/content/Context;", "attrs", "Landroid/util/AttributeSet;", "viewpump_release"}, k = 1, mv = {1, 1, 13})
    /* renamed from: io.github.inflationx.viewpump.internal.-ViewPumpLayoutInflater$WrapperFactory2 */
    /* compiled from: -ViewPumpLayoutInflater.kt */
    private static class WrapperFactory2 implements LayoutInflater.Factory2 {
        private final WrapperFactory2ViewCreator viewCreator;

        public WrapperFactory2(LayoutInflater.Factory2 factory2) {
            Intrinsics.checkParameterIsNotNull(factory2, "factory2");
            this.viewCreator = new WrapperFactory2ViewCreator(factory2);
        }

        public View onCreateView(String str, Context context, AttributeSet attributeSet) {
            Intrinsics.checkParameterIsNotNull(str, "name");
            Intrinsics.checkParameterIsNotNull(context, "context");
            return onCreateView((View) null, str, context, attributeSet);
        }

        public View onCreateView(View view, String str, Context context, AttributeSet attributeSet) {
            Intrinsics.checkParameterIsNotNull(str, "name");
            Intrinsics.checkParameterIsNotNull(context, "context");
            return ViewPump.Companion.get().inflate(new InflateRequest(str, context, attributeSet, view, this.viewCreator)).view();
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0012\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J.\u0010\u0007\u001a\u0004\u0018\u00010\b2\b\u0010\t\u001a\u0004\u0018\u00010\b2\u0006\u0010\n\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\r2\b\u0010\u000e\u001a\u0004\u0018\u00010\u000fH\u0016R\u0014\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006¨\u0006\u0010"}, d2 = {"Lio/github/inflationx/viewpump/internal/-ViewPumpLayoutInflater$WrapperFactory2ViewCreator;", "Lio/github/inflationx/viewpump/FallbackViewCreator;", "factory2", "Landroid/view/LayoutInflater$Factory2;", "(Landroid/view/LayoutInflater$Factory2;)V", "getFactory2", "()Landroid/view/LayoutInflater$Factory2;", "onCreateView", "Landroid/view/View;", "parent", "name", "", "context", "Landroid/content/Context;", "attrs", "Landroid/util/AttributeSet;", "viewpump_release"}, k = 1, mv = {1, 1, 13})
    /* renamed from: io.github.inflationx.viewpump.internal.-ViewPumpLayoutInflater$WrapperFactory2ViewCreator */
    /* compiled from: -ViewPumpLayoutInflater.kt */
    private static class WrapperFactory2ViewCreator implements FallbackViewCreator {
        private final LayoutInflater.Factory2 factory2;

        public WrapperFactory2ViewCreator(LayoutInflater.Factory2 factory22) {
            Intrinsics.checkParameterIsNotNull(factory22, "factory2");
            this.factory2 = factory22;
        }

        /* access modifiers changed from: protected */
        public final LayoutInflater.Factory2 getFactory2() {
            return this.factory2;
        }

        public View onCreateView(View view, String str, Context context, AttributeSet attributeSet) {
            Intrinsics.checkParameterIsNotNull(str, "name");
            Intrinsics.checkParameterIsNotNull(context, "context");
            return this.factory2.onCreateView(view, str, context, attributeSet);
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0002\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J.\u0010\t\u001a\u0004\u0018\u00010\n2\b\u0010\u000b\u001a\u0004\u0018\u00010\n2\u0006\u0010\f\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000f2\b\u0010\u0010\u001a\u0004\u0018\u00010\u0011H\u0016R\u000e\u0010\u0007\u001a\u00020\bX\u0004¢\u0006\u0002\n\u0000¨\u0006\u0012"}, d2 = {"Lio/github/inflationx/viewpump/internal/-ViewPumpLayoutInflater$PrivateWrapperFactory2;", "Lio/github/inflationx/viewpump/internal/-ViewPumpLayoutInflater$WrapperFactory2;", "factory2", "Landroid/view/LayoutInflater$Factory2;", "inflater", "Lio/github/inflationx/viewpump/internal/-ViewPumpLayoutInflater;", "(Landroid/view/LayoutInflater$Factory2;Lio/github/inflationx/viewpump/internal/-ViewPumpLayoutInflater;)V", "viewCreator", "Lio/github/inflationx/viewpump/internal/-ViewPumpLayoutInflater$PrivateWrapperFactory2ViewCreator;", "onCreateView", "Landroid/view/View;", "parent", "name", "", "context", "Landroid/content/Context;", "attrs", "Landroid/util/AttributeSet;", "viewpump_release"}, k = 1, mv = {1, 1, 13})
    /* renamed from: io.github.inflationx.viewpump.internal.-ViewPumpLayoutInflater$PrivateWrapperFactory2 */
    /* compiled from: -ViewPumpLayoutInflater.kt */
    private static final class PrivateWrapperFactory2 extends WrapperFactory2 {
        private final PrivateWrapperFactory2ViewCreator viewCreator;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public PrivateWrapperFactory2(LayoutInflater.Factory2 factory2, ViewPumpLayoutInflater r3) {
            super(factory2);
            Intrinsics.checkParameterIsNotNull(factory2, "factory2");
            Intrinsics.checkParameterIsNotNull(r3, "inflater");
            this.viewCreator = new PrivateWrapperFactory2ViewCreator(factory2, r3);
        }

        public View onCreateView(View view, String str, Context context, AttributeSet attributeSet) {
            Intrinsics.checkParameterIsNotNull(str, "name");
            Intrinsics.checkParameterIsNotNull(context, "context");
            return ViewPump.Companion.get().inflate(new InflateRequest(str, context, attributeSet, view, this.viewCreator)).view();
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0002\u0018\u00002\u00020\u00012\u00020\u0002B\u0015\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006¢\u0006\u0002\u0010\u0007J.\u0010\b\u001a\u0004\u0018\u00010\t2\b\u0010\n\u001a\u0004\u0018\u00010\t2\u0006\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000e2\b\u0010\u000f\u001a\u0004\u0018\u00010\u0010H\u0016R\u000e\u0010\u0005\u001a\u00020\u0006X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0011"}, d2 = {"Lio/github/inflationx/viewpump/internal/-ViewPumpLayoutInflater$PrivateWrapperFactory2ViewCreator;", "Lio/github/inflationx/viewpump/internal/-ViewPumpLayoutInflater$WrapperFactory2ViewCreator;", "Lio/github/inflationx/viewpump/FallbackViewCreator;", "factory2", "Landroid/view/LayoutInflater$Factory2;", "inflater", "Lio/github/inflationx/viewpump/internal/-ViewPumpLayoutInflater;", "(Landroid/view/LayoutInflater$Factory2;Lio/github/inflationx/viewpump/internal/-ViewPumpLayoutInflater;)V", "onCreateView", "Landroid/view/View;", "parent", "name", "", "context", "Landroid/content/Context;", "attrs", "Landroid/util/AttributeSet;", "viewpump_release"}, k = 1, mv = {1, 1, 13})
    /* renamed from: io.github.inflationx.viewpump.internal.-ViewPumpLayoutInflater$PrivateWrapperFactory2ViewCreator */
    /* compiled from: -ViewPumpLayoutInflater.kt */
    private static final class PrivateWrapperFactory2ViewCreator extends WrapperFactory2ViewCreator implements FallbackViewCreator {
        private final ViewPumpLayoutInflater inflater;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public PrivateWrapperFactory2ViewCreator(LayoutInflater.Factory2 factory2, ViewPumpLayoutInflater r3) {
            super(factory2);
            Intrinsics.checkParameterIsNotNull(factory2, "factory2");
            Intrinsics.checkParameterIsNotNull(r3, "inflater");
            this.inflater = r3;
        }

        public View onCreateView(View view, String str, Context context, AttributeSet attributeSet) {
            Intrinsics.checkParameterIsNotNull(str, "name");
            Intrinsics.checkParameterIsNotNull(context, "context");
            return this.inflater.createCustomViewInternal(getFactory2().onCreateView(view, str, context, attributeSet), str, context, attributeSet);
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\"\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0014\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X\u0004¢\u0006\u0002\n\u0000R\u001b\u0010\u0006\u001a\u00020\u00078BX\u0002¢\u0006\f\n\u0004\b\n\u0010\u000b\u001a\u0004\b\b\u0010\t¨\u0006\f"}, d2 = {"Lio/github/inflationx/viewpump/internal/-ViewPumpLayoutInflater$Companion;", "", "()V", "CLASS_PREFIX_LIST", "", "", "CONSTRUCTOR_ARGS_FIELD", "Ljava/lang/reflect/Field;", "getCONSTRUCTOR_ARGS_FIELD", "()Ljava/lang/reflect/Field;", "CONSTRUCTOR_ARGS_FIELD$delegate", "Lkotlin/Lazy;", "viewpump_release"}, k = 1, mv = {1, 1, 13})
    /* renamed from: io.github.inflationx.viewpump.internal.-ViewPumpLayoutInflater$Companion */
    /* compiled from: -ViewPumpLayoutInflater.kt */
    public static final class Companion {
        static final /* synthetic */ KProperty[] $$delegatedProperties = {Reflection.property1(new PropertyReference1Impl(Reflection.getOrCreateKotlinClass(Companion.class), "CONSTRUCTOR_ARGS_FIELD", "getCONSTRUCTOR_ARGS_FIELD()Ljava/lang/reflect/Field;"))};

        /* access modifiers changed from: private */
        public final Field getCONSTRUCTOR_ARGS_FIELD() {
            Lazy access$getCONSTRUCTOR_ARGS_FIELD$cp = ViewPumpLayoutInflater.CONSTRUCTOR_ARGS_FIELD$delegate;
            Companion companion = ViewPumpLayoutInflater.Companion;
            KProperty kProperty = $$delegatedProperties[0];
            return (Field) access$getCONSTRUCTOR_ARGS_FIELD$cp.getValue();
        }

        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }
}
