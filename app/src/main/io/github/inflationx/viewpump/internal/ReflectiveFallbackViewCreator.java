package io.github.inflationx.viewpump.internal;

import android.content.Context;
import android.util.AttributeSet;
import io.github.inflationx.viewpump.FallbackViewCreator;
import kotlin.Metadata;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0000\u0018\u0000 \f2\u00020\u0001:\u0001\fB\u0005¢\u0006\u0002\u0010\u0002J.\u0010\u0003\u001a\u0004\u0018\u00010\u00042\b\u0010\u0005\u001a\u0004\u0018\u00010\u00042\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\t2\b\u0010\n\u001a\u0004\u0018\u00010\u000bH\u0016¨\u0006\r"}, d2 = {"Lio/github/inflationx/viewpump/internal/-ReflectiveFallbackViewCreator;", "Lio/github/inflationx/viewpump/FallbackViewCreator;", "()V", "onCreateView", "Landroid/view/View;", "parent", "name", "", "context", "Landroid/content/Context;", "attrs", "Landroid/util/AttributeSet;", "Companion", "viewpump_release"}, k = 1, mv = {1, 1, 13})
/* renamed from: io.github.inflationx.viewpump.internal.-ReflectiveFallbackViewCreator  reason: invalid class name */
/* compiled from: -ReflectiveFallbackViewCreator.kt */
public final class ReflectiveFallbackViewCreator implements FallbackViewCreator {
    private static final Class<? extends Object>[] CONSTRUCTOR_SIGNATURE_1 = {Context.class};
    private static final Class<? extends Object>[] CONSTRUCTOR_SIGNATURE_2 = {Context.class, AttributeSet.class};
    public static final Companion Companion = new Companion((DefaultConstructorMarker) null);

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0011\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u001e\u0010\u0003\u001a\u0010\u0012\f\u0012\n\u0012\u0006\b\u0001\u0012\u00020\u00010\u00050\u0004X\u0004¢\u0006\u0004\n\u0002\u0010\u0006R\u001e\u0010\u0007\u001a\u0010\u0012\f\u0012\n\u0012\u0006\b\u0001\u0012\u00020\u00010\u00050\u0004X\u0004¢\u0006\u0004\n\u0002\u0010\u0006¨\u0006\b"}, d2 = {"Lio/github/inflationx/viewpump/internal/-ReflectiveFallbackViewCreator$Companion;", "", "()V", "CONSTRUCTOR_SIGNATURE_1", "", "Ljava/lang/Class;", "[Ljava/lang/Class;", "CONSTRUCTOR_SIGNATURE_2", "viewpump_release"}, k = 1, mv = {1, 1, 13})
    /* renamed from: io.github.inflationx.viewpump.internal.-ReflectiveFallbackViewCreator$Companion */
    /* compiled from: -ReflectiveFallbackViewCreator.kt */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(10:0|1|2|3|4|5|6|7|8|9) */
    /* JADX WARNING: Missing exception handler attribute for start block: B:6:0x0030 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.view.View onCreateView(android.view.View r4, java.lang.String r5, android.content.Context r6, android.util.AttributeSet r7) {
        /*
            r3 = this;
            java.lang.String r4 = "name"
            kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(r5, r4)
            java.lang.String r4 = "context"
            kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(r6, r4)
            java.lang.Class r4 = java.lang.Class.forName(r5)     // Catch:{ Exception -> 0x0055 }
            java.lang.Class<android.view.View> r5 = android.view.View.class
            java.lang.Class r4 = r4.asSubclass(r5)     // Catch:{ Exception -> 0x0055 }
            r5 = 0
            r0 = 1
            java.lang.Class<? extends java.lang.Object>[] r1 = CONSTRUCTOR_SIGNATURE_2     // Catch:{ NoSuchMethodException -> 0x0030 }
            int r2 = r1.length     // Catch:{ NoSuchMethodException -> 0x0030 }
            java.lang.Object[] r1 = java.util.Arrays.copyOf(r1, r2)     // Catch:{ NoSuchMethodException -> 0x0030 }
            java.lang.Class[] r1 = (java.lang.Class[]) r1     // Catch:{ NoSuchMethodException -> 0x0030 }
            java.lang.reflect.Constructor r1 = r4.getConstructor(r1)     // Catch:{ NoSuchMethodException -> 0x0030 }
            java.lang.String r2 = "clazz.getConstructor(*CONSTRUCTOR_SIGNATURE_2)"
            kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r1, r2)     // Catch:{ NoSuchMethodException -> 0x0030 }
            r2 = 2
            java.lang.Object[] r2 = new java.lang.Object[r2]     // Catch:{ NoSuchMethodException -> 0x0030 }
            r2[r5] = r6     // Catch:{ NoSuchMethodException -> 0x0030 }
            r2[r0] = r7     // Catch:{ NoSuchMethodException -> 0x0030 }
            goto L_0x0046
        L_0x0030:
            java.lang.Class<? extends java.lang.Object>[] r7 = CONSTRUCTOR_SIGNATURE_1     // Catch:{ Exception -> 0x0055 }
            int r1 = r7.length     // Catch:{ Exception -> 0x0055 }
            java.lang.Object[] r7 = java.util.Arrays.copyOf(r7, r1)     // Catch:{ Exception -> 0x0055 }
            java.lang.Class[] r7 = (java.lang.Class[]) r7     // Catch:{ Exception -> 0x0055 }
            java.lang.reflect.Constructor r1 = r4.getConstructor(r7)     // Catch:{ Exception -> 0x0055 }
            java.lang.String r4 = "clazz.getConstructor(*CONSTRUCTOR_SIGNATURE_1)"
            kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r1, r4)     // Catch:{ Exception -> 0x0055 }
            android.content.Context[] r2 = new android.content.Context[r0]     // Catch:{ Exception -> 0x0055 }
            r2[r5] = r6     // Catch:{ Exception -> 0x0055 }
        L_0x0046:
            r1.setAccessible(r0)     // Catch:{ Exception -> 0x0055 }
            int r4 = r2.length     // Catch:{ Exception -> 0x0055 }
            java.lang.Object[] r4 = java.util.Arrays.copyOf(r2, r4)     // Catch:{ Exception -> 0x0055 }
            java.lang.Object r4 = r1.newInstance(r4)     // Catch:{ Exception -> 0x0055 }
            android.view.View r4 = (android.view.View) r4     // Catch:{ Exception -> 0x0055 }
            return r4
        L_0x0055:
            r4 = move-exception
            boolean r5 = r4 instanceof java.lang.ClassNotFoundException
            if (r5 == 0) goto L_0x005e
            r4.printStackTrace()
            goto L_0x007d
        L_0x005e:
            boolean r5 = r4 instanceof java.lang.NoSuchMethodException
            if (r5 == 0) goto L_0x0066
            r4.printStackTrace()
            goto L_0x007d
        L_0x0066:
            boolean r5 = r4 instanceof java.lang.IllegalAccessException
            if (r5 == 0) goto L_0x006e
            r4.printStackTrace()
            goto L_0x007d
        L_0x006e:
            boolean r5 = r4 instanceof java.lang.InstantiationException
            if (r5 == 0) goto L_0x0076
            r4.printStackTrace()
            goto L_0x007d
        L_0x0076:
            boolean r5 = r4 instanceof java.lang.reflect.InvocationTargetException
            if (r5 == 0) goto L_0x007f
            r4.printStackTrace()
        L_0x007d:
            r4 = 0
            return r4
        L_0x007f:
            java.lang.Throwable r4 = (java.lang.Throwable) r4
            throw r4
        */
        throw new UnsupportedOperationException("Method not decompiled: io.github.inflationx.viewpump.internal.ReflectiveFallbackViewCreator.onCreateView(android.view.View, java.lang.String, android.content.Context, android.util.AttributeSet):android.view.View");
    }
}
