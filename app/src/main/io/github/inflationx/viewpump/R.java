package io.github.inflationx.viewpump;

public final class R {

    public static final class id {
        public static final int viewpump_layout_res = 2131297119;
        public static final int viewpump_tag_id = 2131297120;

        private id() {
        }
    }

    private R() {
    }
}
