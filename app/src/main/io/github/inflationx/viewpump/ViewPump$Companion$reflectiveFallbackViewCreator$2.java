package io.github.inflationx.viewpump;

import io.github.inflationx.viewpump.internal.ReflectiveFallbackViewCreator;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.Lambda;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n¢\u0006\u0002\b\u0002"}, d2 = {"<anonymous>", "Lio/github/inflationx/viewpump/internal/-ReflectiveFallbackViewCreator;", "invoke"}, k = 3, mv = {1, 1, 13})
/* compiled from: ViewPump.kt */
final class ViewPump$Companion$reflectiveFallbackViewCreator$2 extends Lambda implements Function0<ReflectiveFallbackViewCreator> {
    public static final ViewPump$Companion$reflectiveFallbackViewCreator$2 INSTANCE = new ViewPump$Companion$reflectiveFallbackViewCreator$2();

    ViewPump$Companion$reflectiveFallbackViewCreator$2() {
        super(0);
    }

    public final ReflectiveFallbackViewCreator invoke() {
        return new ReflectiveFallbackViewCreator();
    }
}
