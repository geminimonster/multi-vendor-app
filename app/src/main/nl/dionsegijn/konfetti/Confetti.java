package nl.dionsegijn.konfetti;

import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import com.google.firebase.analytics.FirebaseAnalytics;
import java.util.Random;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import nl.dionsegijn.konfetti.models.Shape;
import nl.dionsegijn.konfetti.models.Size;
import nl.dionsegijn.konfetti.models.Vector;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000T\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0011\n\u0002\u0010\u0007\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\f\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u00002\u00020\u0001BM\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t\u0012\b\b\u0002\u0010\n\u001a\u00020\u000b\u0012\b\b\u0002\u0010\f\u001a\u00020\r\u0012\b\b\u0002\u0010\u000e\u001a\u00020\u0003\u0012\b\b\u0002\u0010\u000f\u001a\u00020\u0003¢\u0006\u0002\u0010\u0010J\u000e\u0010/\u001a\u0002002\u0006\u00101\u001a\u00020\u0003J\u0010\u00102\u001a\u0002002\u0006\u00103\u001a\u000204H\u0002J\b\u0010)\u001a\u00020\u001fH\u0002J\u0006\u00105\u001a\u00020\rJ\u0016\u00106\u001a\u0002002\u0006\u00103\u001a\u0002042\u0006\u00107\u001a\u00020\u001fJ\u0010\u00108\u001a\u0002002\u0006\u00107\u001a\u00020\u001fH\u0002J\u0010\u00109\u001a\u0002002\u0006\u00107\u001a\u00020\u001fH\u0002R\u000e\u0010\u000e\u001a\u00020\u0003X\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0005X\u000e¢\u0006\u0002\n\u0000R\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u0013R\u0011\u0010\f\u001a\u00020\r¢\u0006\b\n\u0000\u001a\u0004\b\u0014\u0010\u0015R\u001a\u0010\n\u001a\u00020\u000bX\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0016\u0010\u0017\"\u0004\b\u0018\u0010\u0019R\u001a\u0010\u0002\u001a\u00020\u0003X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u001a\u0010\u001b\"\u0004\b\u001c\u0010\u001dR\u000e\u0010\u001e\u001a\u00020\u001fX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010 \u001a\u00020!X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\"\u001a\u00020#X\u000e¢\u0006\u0002\n\u0000R\u000e\u0010$\u001a\u00020\u001fX\u000e¢\u0006\u0002\n\u0000R\u000e\u0010%\u001a\u00020\u001fX\u000e¢\u0006\u0002\n\u0000R\u000e\u0010&\u001a\u00020\u001fX\u000e¢\u0006\u0002\n\u0000R\u0011\u0010\b\u001a\u00020\t¢\u0006\b\n\u0000\u001a\u0004\b'\u0010(R\u0011\u0010\u0006\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\b)\u0010*R\u000e\u0010+\u001a\u00020\u001fX\u000e¢\u0006\u0002\n\u0000R\u001a\u0010\u000f\u001a\u00020\u0003X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b,\u0010\u001b\"\u0004\b-\u0010\u001dR\u000e\u0010.\u001a\u00020\u001fX\u000e¢\u0006\u0002\n\u0000¨\u0006:"}, d2 = {"Lnl/dionsegijn/konfetti/Confetti;", "", "location", "Lnl/dionsegijn/konfetti/models/Vector;", "color", "", "size", "Lnl/dionsegijn/konfetti/models/Size;", "shape", "Lnl/dionsegijn/konfetti/models/Shape;", "lifespan", "", "fadeOut", "", "acceleration", "velocity", "(Lnl/dionsegijn/konfetti/models/Vector;ILnl/dionsegijn/konfetti/models/Size;Lnl/dionsegijn/konfetti/models/Shape;JZLnl/dionsegijn/konfetti/models/Vector;Lnl/dionsegijn/konfetti/models/Vector;)V", "alpha", "getColor", "()I", "getFadeOut", "()Z", "getLifespan", "()J", "setLifespan", "(J)V", "getLocation", "()Lnl/dionsegijn/konfetti/models/Vector;", "setLocation", "(Lnl/dionsegijn/konfetti/models/Vector;)V", "mass", "", "paint", "Landroid/graphics/Paint;", "rectF", "Landroid/graphics/RectF;", "rotation", "rotationSpeed", "rotationWidth", "getShape", "()Lnl/dionsegijn/konfetti/models/Shape;", "getSize", "()Lnl/dionsegijn/konfetti/models/Size;", "speedF", "getVelocity", "setVelocity", "width", "applyForce", "", "force", "display", "canvas", "Landroid/graphics/Canvas;", "isDead", "render", "deltaTime", "update", "updateAlpha", "konfetti_release"}, k = 1, mv = {1, 1, 15})
/* compiled from: Confetti.kt */
public final class Confetti {
    private Vector acceleration;
    private int alpha;
    private final int color;
    private final boolean fadeOut;
    private long lifespan;
    private Vector location;
    private final float mass;
    private final Paint paint;
    private RectF rectF;
    private float rotation;
    private float rotationSpeed;
    private float rotationWidth;
    private final Shape shape;
    private final Size size;
    private float speedF;
    private Vector velocity;
    private float width;

    @Metadata(bv = {1, 0, 3}, k = 3, mv = {1, 1, 15})
    public final /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;

        static {
            int[] iArr = new int[Shape.values().length];
            $EnumSwitchMapping$0 = iArr;
            iArr[Shape.CIRCLE.ordinal()] = 1;
            $EnumSwitchMapping$0[Shape.RECT.ordinal()] = 2;
        }
    }

    public Confetti(Vector vector, int i, Size size2, Shape shape2, long j, boolean z, Vector vector2, Vector vector3) {
        Intrinsics.checkParameterIsNotNull(vector, FirebaseAnalytics.Param.LOCATION);
        Intrinsics.checkParameterIsNotNull(size2, "size");
        Intrinsics.checkParameterIsNotNull(shape2, "shape");
        Intrinsics.checkParameterIsNotNull(vector2, "acceleration");
        Intrinsics.checkParameterIsNotNull(vector3, "velocity");
        this.location = vector;
        this.color = i;
        this.size = size2;
        this.shape = shape2;
        this.lifespan = j;
        this.fadeOut = z;
        this.acceleration = vector2;
        this.velocity = vector3;
        this.mass = size2.getMass();
        this.width = this.size.getSizeInPx$konfetti_release();
        this.paint = new Paint();
        this.rotationSpeed = 1.0f;
        this.rotationWidth = this.width;
        this.rectF = new RectF();
        this.speedF = 60.0f;
        this.alpha = 255;
        Resources system = Resources.getSystem();
        Intrinsics.checkExpressionValueIsNotNull(system, "Resources.getSystem()");
        float f = system.getDisplayMetrics().density * 0.29f;
        this.rotationSpeed = (((float) 3) * f * new Random().nextFloat()) + f;
        this.paint.setColor(this.color);
    }

    public final Vector getLocation() {
        return this.location;
    }

    public final void setLocation(Vector vector) {
        Intrinsics.checkParameterIsNotNull(vector, "<set-?>");
        this.location = vector;
    }

    public final int getColor() {
        return this.color;
    }

    /* renamed from: getSize  reason: collision with other method in class */
    public final Size m1054getSize() {
        return this.size;
    }

    public final Shape getShape() {
        return this.shape;
    }

    public final long getLifespan() {
        return this.lifespan;
    }

    public final void setLifespan(long j) {
        this.lifespan = j;
    }

    public final boolean getFadeOut() {
        return this.fadeOut;
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* synthetic */ Confetti(nl.dionsegijn.konfetti.models.Vector r14, int r15, nl.dionsegijn.konfetti.models.Size r16, nl.dionsegijn.konfetti.models.Shape r17, long r18, boolean r20, nl.dionsegijn.konfetti.models.Vector r21, nl.dionsegijn.konfetti.models.Vector r22, int r23, kotlin.jvm.internal.DefaultConstructorMarker r24) {
        /*
            r13 = this;
            r0 = r23
            r1 = r0 & 16
            if (r1 == 0) goto L_0x000a
            r1 = -1
            r8 = r1
            goto L_0x000c
        L_0x000a:
            r8 = r18
        L_0x000c:
            r1 = r0 & 32
            if (r1 == 0) goto L_0x0013
            r1 = 1
            r10 = 1
            goto L_0x0015
        L_0x0013:
            r10 = r20
        L_0x0015:
            r1 = r0 & 64
            r2 = 0
            if (r1 == 0) goto L_0x0021
            nl.dionsegijn.konfetti.models.Vector r1 = new nl.dionsegijn.konfetti.models.Vector
            r1.<init>(r2, r2)
            r11 = r1
            goto L_0x0023
        L_0x0021:
            r11 = r21
        L_0x0023:
            r0 = r0 & 128(0x80, float:1.794E-43)
            if (r0 == 0) goto L_0x0030
            nl.dionsegijn.konfetti.models.Vector r0 = new nl.dionsegijn.konfetti.models.Vector
            r1 = 3
            r3 = 0
            r0.<init>(r2, r2, r1, r3)
            r12 = r0
            goto L_0x0032
        L_0x0030:
            r12 = r22
        L_0x0032:
            r3 = r13
            r4 = r14
            r5 = r15
            r6 = r16
            r7 = r17
            r3.<init>(r4, r5, r6, r7, r8, r10, r11, r12)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: nl.dionsegijn.konfetti.Confetti.<init>(nl.dionsegijn.konfetti.models.Vector, int, nl.dionsegijn.konfetti.models.Size, nl.dionsegijn.konfetti.models.Shape, long, boolean, nl.dionsegijn.konfetti.models.Vector, nl.dionsegijn.konfetti.models.Vector, int, kotlin.jvm.internal.DefaultConstructorMarker):void");
    }

    public final Vector getVelocity() {
        return this.velocity;
    }

    public final void setVelocity(Vector vector) {
        Intrinsics.checkParameterIsNotNull(vector, "<set-?>");
        this.velocity = vector;
    }

    private final float getSize() {
        return this.width;
    }

    public final boolean isDead() {
        return ((float) this.alpha) <= 0.0f;
    }

    public final void applyForce(Vector vector) {
        Intrinsics.checkParameterIsNotNull(vector, "force");
        Vector copy$default = Vector.copy$default(vector, 0.0f, 0.0f, 3, (Object) null);
        copy$default.div(this.mass);
        this.acceleration.add(copy$default);
    }

    public final void render(Canvas canvas, float f) {
        Intrinsics.checkParameterIsNotNull(canvas, "canvas");
        update(f);
        display(canvas);
    }

    private final void update(float f) {
        this.velocity.add(this.acceleration);
        Vector copy$default = Vector.copy$default(this.velocity, 0.0f, 0.0f, 3, (Object) null);
        copy$default.mult(this.speedF * f);
        this.location.add(copy$default);
        long j = this.lifespan;
        if (j <= 0) {
            updateAlpha(f);
        } else {
            this.lifespan = j - ((long) (((float) 1000) * f));
        }
        float f2 = this.rotationSpeed * f * this.speedF;
        float f3 = this.rotation + f2;
        this.rotation = f3;
        if (f3 >= ((float) 360)) {
            this.rotation = 0.0f;
        }
        float f4 = this.rotationWidth - f2;
        this.rotationWidth = f4;
        if (f4 < ((float) 0)) {
            this.rotationWidth = this.width;
        }
    }

    private final void updateAlpha(float f) {
        if (this.fadeOut) {
            float f2 = ((float) 5) * f;
            float f3 = this.speedF;
            int i = this.alpha;
            if (((float) i) - (f2 * f3) < ((float) 0)) {
                this.alpha = 0;
            } else {
                this.alpha = i - ((int) (f2 * f3));
            }
        } else {
            this.alpha = 0;
        }
    }

    private final void display(Canvas canvas) {
        if (this.location.getY() > ((float) canvas.getHeight())) {
            this.lifespan = 0;
        } else if (this.location.getX() <= ((float) canvas.getWidth())) {
            float f = (float) 0;
            if (this.location.getX() + getSize() >= f && this.location.getY() + getSize() >= f) {
                float x = this.location.getX() + (this.width - this.rotationWidth);
                float x2 = this.location.getX() + this.rotationWidth;
                if (x > x2) {
                    float f2 = x + x2;
                    x2 = f2 - x2;
                    x = f2 - x2;
                }
                this.paint.setAlpha(this.alpha);
                this.rectF.set(x, this.location.getY(), x2, this.location.getY() + getSize());
                canvas.save();
                canvas.rotate(this.rotation, this.rectF.centerX(), this.rectF.centerY());
                int i = WhenMappings.$EnumSwitchMapping$0[this.shape.ordinal()];
                if (i == 1) {
                    canvas.drawOval(this.rectF, this.paint);
                } else if (i == 2) {
                    canvas.drawRect(this.rectF, this.paint);
                }
                canvas.restore();
            }
        }
    }
}
