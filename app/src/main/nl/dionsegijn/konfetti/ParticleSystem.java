package nl.dionsegijn.konfetti;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Random;
import kotlin.Deprecated;
import kotlin.Metadata;
import kotlin.ReplaceWith;
import kotlin.TypeCastException;
import kotlin.collections.CollectionsKt;
import kotlin.jvm.internal.Intrinsics;
import nl.dionsegijn.konfetti.emitters.BurstEmitter;
import nl.dionsegijn.konfetti.emitters.Emitter;
import nl.dionsegijn.konfetti.emitters.RenderSystem;
import nl.dionsegijn.konfetti.emitters.StreamEmitter;
import nl.dionsegijn.konfetti.models.ConfettiConfig;
import nl.dionsegijn.konfetti.models.Shape;
import nl.dionsegijn.konfetti.models.Size;
import nl.dionsegijn.konfetti.modules.LocationModule;
import nl.dionsegijn.konfetti.modules.VelocityModule;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0001\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0015\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0011\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010 \n\u0002\b\u0006\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0006\n\u0002\b\u0006\n\u0002\u0010\u0007\n\u0002\b\f\n\u0002\u0010\t\n\u0002\b\n\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u0006\u0010\u001c\u001a\u00020\u001dJ\u0012\u0010\u001e\u001a\u00020\u00002\n\u0010\u0005\u001a\u00020\u0006\"\u00020\u001dJ\u0014\u0010\u001e\u001a\u00020\u00002\f\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u001d0\u001fJ\u001f\u0010 \u001a\u00020\u00002\u0012\u0010\u0013\u001a\n\u0012\u0006\b\u0001\u0012\u00020\u00150\u0014\"\u00020\u0015¢\u0006\u0002\u0010!J\u001f\u0010\"\u001a\u00020\u00002\u0012\u0010#\u001a\n\u0012\u0006\b\u0001\u0012\u00020\u00180\u0014\"\u00020\u0018¢\u0006\u0002\u0010$J\u000e\u0010%\u001a\u00020&2\u0006\u0010'\u001a\u00020\u001dJ\u0006\u0010(\u001a\u00020)J\u000e\u0010*\u001a\u00020&2\u0006\u0010*\u001a\u00020+J\u000e\u0010,\u001a\u00020\u00002\u0006\u0010-\u001a\u00020.J\u0016\u0010,\u001a\u00020\u00002\u0006\u0010/\u001a\u00020.2\u0006\u00100\u001a\u00020.J\u000e\u00101\u001a\u00020\u00002\u0006\u00102\u001a\u00020)J\u0016\u00103\u001a\u00020\u00002\u0006\u00104\u001a\u0002052\u0006\u00106\u001a\u000205J3\u00103\u001a\u00020\u00002\u0006\u00107\u001a\u0002052\n\b\u0002\u00108\u001a\u0004\u0018\u0001052\u0006\u00109\u001a\u0002052\n\b\u0002\u0010:\u001a\u0004\u0018\u000105¢\u0006\u0002\u0010;J\u000e\u0010<\u001a\u00020\u00002\u0006\u0010=\u001a\u000205J\u0016\u0010<\u001a\u00020\u00002\u0006\u0010>\u001a\u0002052\u0006\u0010?\u001a\u000205J\u000e\u0010@\u001a\u00020\u00002\u0006\u0010A\u001a\u00020BJ\b\u0010C\u001a\u00020&H\u0002J\u0010\u0010D\u001a\u00020&2\u0006\u0010*\u001a\u00020+H\u0002J\u0006\u0010E\u001a\u00020&J\u0018\u0010F\u001a\u00020&2\u0006\u0010G\u001a\u00020\u001d2\u0006\u0010H\u001a\u00020\u001dH\u0007J\u0018\u0010F\u001a\u00020&2\u0006\u0010G\u001a\u00020\u001d2\u0006\u0010I\u001a\u00020BH\u0007J\u0016\u0010J\u001a\u00020&2\u0006\u0010G\u001a\u00020\u001d2\u0006\u0010I\u001a\u00020BJ\u0016\u0010K\u001a\u00020&2\u0006\u0010G\u001a\u00020\u001d2\u0006\u0010H\u001a\u00020\u001dR\u000e\u0010\u0005\u001a\u00020\u0006X\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\bX\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\fX\u0004¢\u0006\u0002\n\u0000R\u001a\u0010\r\u001a\u00020\u000eX.¢\u0006\u000e\n\u0000\u001a\u0004\b\u000f\u0010\u0010\"\u0004\b\u0011\u0010\u0012R\u0016\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u00150\u0014X\u000e¢\u0006\u0004\n\u0002\u0010\u0016R\u0016\u0010\u0017\u001a\b\u0012\u0004\u0012\u00020\u00180\u0014X\u000e¢\u0006\u0004\n\u0002\u0010\u0019R\u000e\u0010\u001a\u001a\u00020\u001bX\u000e¢\u0006\u0002\n\u0000¨\u0006L"}, d2 = {"Lnl/dionsegijn/konfetti/ParticleSystem;", "", "konfettiView", "Lnl/dionsegijn/konfetti/KonfettiView;", "(Lnl/dionsegijn/konfetti/KonfettiView;)V", "colors", "", "confettiConfig", "Lnl/dionsegijn/konfetti/models/ConfettiConfig;", "location", "Lnl/dionsegijn/konfetti/modules/LocationModule;", "random", "Ljava/util/Random;", "renderSystem", "Lnl/dionsegijn/konfetti/emitters/RenderSystem;", "getRenderSystem$konfetti_release", "()Lnl/dionsegijn/konfetti/emitters/RenderSystem;", "setRenderSystem$konfetti_release", "(Lnl/dionsegijn/konfetti/emitters/RenderSystem;)V", "shapes", "", "Lnl/dionsegijn/konfetti/models/Shape;", "[Lnl/dionsegijn/konfetti/models/Shape;", "sizes", "Lnl/dionsegijn/konfetti/models/Size;", "[Lnl/dionsegijn/konfetti/models/Size;", "velocity", "Lnl/dionsegijn/konfetti/modules/VelocityModule;", "activeParticles", "", "addColors", "", "addShapes", "([Lnl/dionsegijn/konfetti/models/Shape;)Lnl/dionsegijn/konfetti/ParticleSystem;", "addSizes", "possibleSizes", "([Lnl/dionsegijn/konfetti/models/Size;)Lnl/dionsegijn/konfetti/ParticleSystem;", "burst", "", "amount", "doneEmitting", "", "emitter", "Lnl/dionsegijn/konfetti/emitters/Emitter;", "setDirection", "degrees", "", "minDegrees", "maxDegrees", "setFadeOutEnabled", "fade", "setPosition", "x", "", "y", "minX", "maxX", "minY", "maxY", "(FLjava/lang/Float;FLjava/lang/Float;)Lnl/dionsegijn/konfetti/ParticleSystem;", "setSpeed", "speed", "minSpeed", "maxSpeed", "setTimeToLive", "timeInMs", "", "start", "startRenderSystem", "stop", "stream", "particlesPerSecond", "maxParticles", "emittingTime", "streamFor", "streamMaxParticles", "konfetti_release"}, k = 1, mv = {1, 1, 15})
/* compiled from: ParticleSystem.kt */
public final class ParticleSystem {
    private int[] colors = {-65536};
    private ConfettiConfig confettiConfig = new ConfettiConfig(false, 0, 3, (DefaultConstructorMarker) null);
    private final KonfettiView konfettiView;
    private LocationModule location;
    private final Random random;
    public RenderSystem renderSystem;
    private Shape[] shapes = {Shape.RECT};
    private Size[] sizes = {new Size(16, 0.0f, 2, (DefaultConstructorMarker) null)};
    private VelocityModule velocity = new VelocityModule(this.random);

    public ParticleSystem(KonfettiView konfettiView2) {
        Intrinsics.checkParameterIsNotNull(konfettiView2, "konfettiView");
        this.konfettiView = konfettiView2;
        Random random2 = new Random();
        this.random = random2;
        this.location = new LocationModule(random2);
    }

    public final RenderSystem getRenderSystem$konfetti_release() {
        RenderSystem renderSystem2 = this.renderSystem;
        if (renderSystem2 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("renderSystem");
        }
        return renderSystem2;
    }

    public final void setRenderSystem$konfetti_release(RenderSystem renderSystem2) {
        Intrinsics.checkParameterIsNotNull(renderSystem2, "<set-?>");
        this.renderSystem = renderSystem2;
    }

    public final ParticleSystem setPosition(float f, float f2) {
        this.location.setX(f);
        this.location.setY(f2);
        return this;
    }

    public static /* synthetic */ ParticleSystem setPosition$default(ParticleSystem particleSystem, float f, Float f2, float f3, Float f4, int i, Object obj) {
        if ((i & 2) != 0) {
            f2 = null;
        }
        if ((i & 8) != 0) {
            f4 = null;
        }
        return particleSystem.setPosition(f, f2, f3, f4);
    }

    public final ParticleSystem setPosition(float f, Float f2, float f3, Float f4) {
        this.location.betweenX(f, f2);
        this.location.betweenY(f3, f4);
        return this;
    }

    public final ParticleSystem addColors(int... iArr) {
        Intrinsics.checkParameterIsNotNull(iArr, "colors");
        this.colors = iArr;
        return this;
    }

    public final ParticleSystem addColors(List<Integer> list) {
        Intrinsics.checkParameterIsNotNull(list, "colors");
        this.colors = CollectionsKt.toIntArray(list);
        return this;
    }

    public final ParticleSystem setDirection(double d) {
        this.velocity.setMinAngle(Math.toRadians(d));
        return this;
    }

    public final ParticleSystem setDirection(double d, double d2) {
        this.velocity.setMinAngle(Math.toRadians(d));
        this.velocity.setMaxAngle(Double.valueOf(Math.toRadians(d2)));
        return this;
    }

    public final ParticleSystem setSpeed(float f) {
        this.velocity.setMinSpeed(f);
        return this;
    }

    public final ParticleSystem setSpeed(float f, float f2) {
        this.velocity.setMinSpeed(f);
        this.velocity.setMaxSpeed(Float.valueOf(f2));
        return this;
    }

    public final ParticleSystem setFadeOutEnabled(boolean z) {
        this.confettiConfig.setFadeOut(z);
        return this;
    }

    public final ParticleSystem setTimeToLive(long j) {
        this.confettiConfig.setTimeToLive(j);
        return this;
    }

    public final void burst(int i) {
        startRenderSystem(new BurstEmitter().build(i));
    }

    @Deprecated(message = "Deprecated in favor of better function names", replaceWith = @ReplaceWith(expression = "streamFor(particlesPerSecond, emittingTime)", imports = {"nl.dionsegijn.konfetti.ParticleSystem.streamFor"}))
    public final void stream(int i, long j) {
        startRenderSystem(StreamEmitter.build$default(new StreamEmitter(), i, j, 0, 4, (Object) null));
    }

    public final void streamFor(int i, long j) {
        startRenderSystem(StreamEmitter.build$default(new StreamEmitter(), i, j, 0, 4, (Object) null));
    }

    @Deprecated(message = "Deprecated in favor of better function names", replaceWith = @ReplaceWith(expression = "streamMaxParticles(particlesPerSecond, maxParticles)", imports = {"nl.dionsegijn.konfetti.ParticleSystem.streamMaxParticles"}))
    public final void stream(int i, int i2) {
        startRenderSystem(StreamEmitter.build$default(new StreamEmitter(), i, 0, i2, 2, (Object) null));
    }

    public final void streamMaxParticles(int i, int i2) {
        startRenderSystem(StreamEmitter.build$default(new StreamEmitter(), i, 0, i2, 2, (Object) null));
    }

    public final void emitter(Emitter emitter) {
        Intrinsics.checkParameterIsNotNull(emitter, "emitter");
        startRenderSystem(emitter);
    }

    private final void startRenderSystem(Emitter emitter) {
        this.renderSystem = new RenderSystem(this.location, this.velocity, this.sizes, this.shapes, this.colors, this.confettiConfig, emitter);
        start();
    }

    public final boolean doneEmitting() {
        RenderSystem renderSystem2 = this.renderSystem;
        if (renderSystem2 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("renderSystem");
        }
        return renderSystem2.isDoneEmitting();
    }

    private final void start() {
        this.konfettiView.start(this);
    }

    public final void stop() {
        this.konfettiView.stop(this);
    }

    public final int activeParticles() {
        RenderSystem renderSystem2 = this.renderSystem;
        if (renderSystem2 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("renderSystem");
        }
        return renderSystem2.getActiveParticles();
    }

    public final ParticleSystem addSizes(Size... sizeArr) {
        Intrinsics.checkParameterIsNotNull(sizeArr, "possibleSizes");
        Collection arrayList = new ArrayList();
        for (Size size : sizeArr) {
            if (size instanceof Size) {
                arrayList.add(size);
            }
        }
        Object[] array = ((List) arrayList).toArray(new Size[0]);
        if (array != null) {
            this.sizes = (Size[]) array;
            return this;
        }
        throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
    }

    public final ParticleSystem addShapes(Shape... shapeArr) {
        Intrinsics.checkParameterIsNotNull(shapeArr, "shapes");
        Collection arrayList = new ArrayList();
        for (Shape shape : shapeArr) {
            if (shape instanceof Shape) {
                arrayList.add(shape);
            }
        }
        Object[] array = ((List) arrayList).toArray(new Shape[0]);
        if (array != null) {
            this.shapes = (Shape[]) array;
            return this;
        }
        throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
    }
}
