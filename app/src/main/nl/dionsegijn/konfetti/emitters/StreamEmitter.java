package nl.dionsegijn.konfetti.emitters;

import java.util.Iterator;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.collections.IntIterator;
import kotlin.jvm.functions.Function0;
import kotlin.ranges.IntRange;

@Metadata(bv = {1, 0, 3}, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0007\n\u0002\b\u0003\n\u0002\u0010\t\n\u0000\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\"\u0010\f\u001a\u00020\u00002\u0006\u0010\r\u001a\u00020\n2\b\b\u0002\u0010\u0007\u001a\u00020\b2\b\b\u0002\u0010\t\u001a\u00020\nJ\u0010\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u0004H\u0016J\b\u0010\u0011\u001a\u00020\u000fH\u0002J\b\u0010\u0012\u001a\u00020\u0013H\u0016J\b\u0010\u0014\u001a\u00020\u0013H\u0002J\b\u0010\u0015\u001a\u00020\u0013H\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004X\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\bX\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\nX\u000e¢\u0006\u0002\n\u0000¨\u0006\u0016"}, d2 = {"Lnl/dionsegijn/konfetti/emitters/StreamEmitter;", "Lnl/dionsegijn/konfetti/emitters/Emitter;", "()V", "amountPerMs", "", "createParticleMs", "elapsedTime", "emittingTime", "", "maxParticles", "", "particlesCreated", "build", "particlesPerSecond", "createConfetti", "", "deltaTime", "createParticle", "isFinished", "", "isTimeElapsed", "reachedMaxParticles", "konfetti_release"}, k = 1, mv = {1, 1, 15})
/* compiled from: StreamEmitter.kt */
public final class StreamEmitter extends Emitter {
    private float amountPerMs;
    private float createParticleMs;
    private float elapsedTime;
    private long emittingTime;
    private int maxParticles = -1;
    private int particlesCreated;

    public static /* synthetic */ StreamEmitter build$default(StreamEmitter streamEmitter, int i, long j, int i2, int i3, Object obj) {
        if ((i3 & 2) != 0) {
            j = 0;
        }
        if ((i3 & 4) != 0) {
            i2 = -1;
        }
        return streamEmitter.build(i, j, i2);
    }

    public final StreamEmitter build(int i, long j, int i2) {
        this.maxParticles = i2;
        this.emittingTime = j;
        this.amountPerMs = 1.0f / ((float) i);
        return this;
    }

    public void createConfetti(float f) {
        float f2 = this.createParticleMs + f;
        this.createParticleMs = f2;
        if (f2 >= this.amountPerMs && !isTimeElapsed()) {
            Iterator it = new IntRange(1, (int) (this.createParticleMs / this.amountPerMs)).iterator();
            while (it.hasNext()) {
                ((IntIterator) it).nextInt();
                createParticle();
            }
            this.createParticleMs %= this.amountPerMs;
        }
        this.elapsedTime += f * ((float) 1000);
    }

    private final void createParticle() {
        if (!reachedMaxParticles()) {
            this.particlesCreated++;
            Function0<Unit> addConfettiFunc = getAddConfettiFunc();
            if (addConfettiFunc != null) {
                Unit invoke = addConfettiFunc.invoke();
            }
        }
    }

    private final boolean isTimeElapsed() {
        long j = this.emittingTime;
        return j != 0 && this.elapsedTime >= ((float) j);
    }

    private final boolean reachedMaxParticles() {
        int i = this.particlesCreated;
        int i2 = this.maxParticles;
        return 1 <= i2 && i >= i2;
    }

    public boolean isFinished() {
        long j = this.emittingTime;
        if (j > 0) {
            if (this.elapsedTime >= ((float) j)) {
                return true;
            }
        } else if (this.maxParticles >= this.particlesCreated) {
            return true;
        }
        return false;
    }
}
