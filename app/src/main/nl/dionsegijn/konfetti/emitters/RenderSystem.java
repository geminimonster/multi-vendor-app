package nl.dionsegijn.konfetti.emitters;

import android.graphics.Canvas;
import com.google.firebase.analytics.FirebaseAnalytics;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Reflection;
import kotlin.reflect.KDeclarationContainer;
import nl.dionsegijn.konfetti.Confetti;
import nl.dionsegijn.konfetti.models.ConfettiConfig;
import nl.dionsegijn.konfetti.models.Shape;
import nl.dionsegijn.konfetti.models.Size;
import nl.dionsegijn.konfetti.models.Vector;
import nl.dionsegijn.konfetti.modules.LocationModule;
import nl.dionsegijn.konfetti.modules.VelocityModule;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000r\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0011\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0015\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0007\n\u0000\u0018\u00002\u00020\u0001BI\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\b0\u0007\u0012\f\u0010\t\u001a\b\u0012\u0004\u0012\u00020\n0\u0007\u0012\u0006\u0010\u000b\u001a\u00020\f\u0012\u0006\u0010\r\u001a\u00020\u000e\u0012\u0006\u0010\u000f\u001a\u00020\u0010¢\u0006\u0002\u0010\u0011J\b\u0010\u001b\u001a\u00020\u001cH\u0002J\u0006\u0010\u001d\u001a\u00020\u001eJ\u0006\u0010\u001f\u001a\u00020 J\u0016\u0010!\u001a\u00020\u001c2\u0006\u0010\"\u001a\u00020#2\u0006\u0010$\u001a\u00020%R\u000e\u0010\u000b\u001a\u00020\fX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0013X\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u00160\u0015X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0017\u001a\u00020\u0018X\u0004¢\u0006\u0002\n\u0000R\u0016\u0010\t\u001a\b\u0012\u0004\u0012\u00020\n0\u0007X\u0004¢\u0006\u0004\n\u0002\u0010\u0019R\u0016\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\b0\u0007X\u0004¢\u0006\u0004\n\u0002\u0010\u001aR\u000e\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000¨\u0006&"}, d2 = {"Lnl/dionsegijn/konfetti/emitters/RenderSystem;", "", "location", "Lnl/dionsegijn/konfetti/modules/LocationModule;", "velocity", "Lnl/dionsegijn/konfetti/modules/VelocityModule;", "sizes", "", "Lnl/dionsegijn/konfetti/models/Size;", "shapes", "Lnl/dionsegijn/konfetti/models/Shape;", "colors", "", "config", "Lnl/dionsegijn/konfetti/models/ConfettiConfig;", "emitter", "Lnl/dionsegijn/konfetti/emitters/Emitter;", "(Lnl/dionsegijn/konfetti/modules/LocationModule;Lnl/dionsegijn/konfetti/modules/VelocityModule;[Lnl/dionsegijn/konfetti/models/Size;[Lnl/dionsegijn/konfetti/models/Shape;[ILnl/dionsegijn/konfetti/models/ConfettiConfig;Lnl/dionsegijn/konfetti/emitters/Emitter;)V", "gravity", "Lnl/dionsegijn/konfetti/models/Vector;", "particles", "", "Lnl/dionsegijn/konfetti/Confetti;", "random", "Ljava/util/Random;", "[Lnl/dionsegijn/konfetti/models/Shape;", "[Lnl/dionsegijn/konfetti/models/Size;", "addConfetti", "", "getActiveParticles", "", "isDoneEmitting", "", "render", "canvas", "Landroid/graphics/Canvas;", "deltaTime", "", "konfetti_release"}, k = 1, mv = {1, 1, 15})
/* compiled from: RenderSystem.kt */
public final class RenderSystem {
    private final int[] colors;
    private final ConfettiConfig config;
    private final Emitter emitter;
    private Vector gravity = new Vector(0.0f, 0.01f);
    private final LocationModule location;
    private final List<Confetti> particles = new ArrayList();
    private final Random random = new Random();
    private final Shape[] shapes;
    private final Size[] sizes;
    private final VelocityModule velocity;

    public RenderSystem(LocationModule locationModule, VelocityModule velocityModule, Size[] sizeArr, Shape[] shapeArr, int[] iArr, ConfettiConfig confettiConfig, Emitter emitter2) {
        Intrinsics.checkParameterIsNotNull(locationModule, FirebaseAnalytics.Param.LOCATION);
        Intrinsics.checkParameterIsNotNull(velocityModule, "velocity");
        Intrinsics.checkParameterIsNotNull(sizeArr, "sizes");
        Intrinsics.checkParameterIsNotNull(shapeArr, "shapes");
        Intrinsics.checkParameterIsNotNull(iArr, "colors");
        Intrinsics.checkParameterIsNotNull(confettiConfig, "config");
        Intrinsics.checkParameterIsNotNull(emitter2, "emitter");
        this.location = locationModule;
        this.velocity = velocityModule;
        this.sizes = sizeArr;
        this.shapes = shapeArr;
        this.colors = iArr;
        this.config = confettiConfig;
        this.emitter = emitter2;
        this.emitter.setAddConfettiFunc(new Function0<Unit>(this) {
            public final String getName() {
                return "addConfetti";
            }

            public final KDeclarationContainer getOwner() {
                return Reflection.getOrCreateKotlinClass(RenderSystem.class);
            }

            public final String getSignature() {
                return "addConfetti()V";
            }

            public final void invoke() {
                ((RenderSystem) this.receiver).addConfetti();
            }
        });
    }

    /* access modifiers changed from: private */
    public final void addConfetti() {
        List<Confetti> list = this.particles;
        Vector vector = new Vector(this.location.getX(), this.location.getY());
        Size[] sizeArr = this.sizes;
        Size size = sizeArr[this.random.nextInt(sizeArr.length)];
        Shape[] shapeArr = this.shapes;
        Shape shape = shapeArr[this.random.nextInt(shapeArr.length)];
        int[] iArr = this.colors;
        list.add(new Confetti(vector, iArr[this.random.nextInt(iArr.length)], size, shape, this.config.getTimeToLive(), this.config.getFadeOut(), (Vector) null, this.velocity.getVelocity(), 64, (DefaultConstructorMarker) null));
    }

    public final void render(Canvas canvas, float f) {
        Intrinsics.checkParameterIsNotNull(canvas, "canvas");
        this.emitter.createConfetti(f);
        for (int size = this.particles.size() - 1; size >= 0; size--) {
            Confetti confetti = this.particles.get(size);
            confetti.applyForce(this.gravity);
            confetti.render(canvas, f);
            if (confetti.isDead()) {
                this.particles.remove(size);
            }
        }
    }

    public final int getActiveParticles() {
        return this.particles.size();
    }

    public final boolean isDoneEmitting() {
        return this.emitter.isFinished() && this.particles.size() == 0;
    }
}
