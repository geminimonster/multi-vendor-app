package nl.dionsegijn.konfetti;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.View;
import java.util.ArrayList;
import java.util.List;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import nl.dionsegijn.konfetti.listeners.OnParticleSystemUpdateListener;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000J\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u00002\u00020\u0001:\u0001 B\u0011\b\u0016\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003¢\u0006\u0002\u0010\u0004B\u001b\b\u0016\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006¢\u0006\u0002\u0010\u0007B#\b\u0016\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u0012\u0006\u0010\b\u001a\u00020\t¢\u0006\u0002\u0010\nJ\u0006\u0010\u0016\u001a\u00020\u0013J\f\u0010\u0017\u001a\b\u0012\u0004\u0012\u00020\u00130\u0012J\u0010\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00020\u001bH\u0014J\u0006\u0010\u001c\u001a\u00020\u0019J\u000e\u0010\u001d\u001a\u00020\u00192\u0006\u0010\u001e\u001a\u00020\u0013J\u000e\u0010\u001f\u001a\u00020\u00192\u0006\u0010\u001e\u001a\u00020\u0013R\u001c\u0010\u000b\u001a\u0004\u0018\u00010\fX\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\r\u0010\u000e\"\u0004\b\u000f\u0010\u0010R\u0014\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u00130\u0012X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0015X\u000e¢\u0006\u0002\n\u0000¨\u0006!"}, d2 = {"Lnl/dionsegijn/konfetti/KonfettiView;", "Landroid/view/View;", "context", "Landroid/content/Context;", "(Landroid/content/Context;)V", "attrs", "Landroid/util/AttributeSet;", "(Landroid/content/Context;Landroid/util/AttributeSet;)V", "defStyleAttr", "", "(Landroid/content/Context;Landroid/util/AttributeSet;I)V", "onParticleSystemUpdateListener", "Lnl/dionsegijn/konfetti/listeners/OnParticleSystemUpdateListener;", "getOnParticleSystemUpdateListener", "()Lnl/dionsegijn/konfetti/listeners/OnParticleSystemUpdateListener;", "setOnParticleSystemUpdateListener", "(Lnl/dionsegijn/konfetti/listeners/OnParticleSystemUpdateListener;)V", "systems", "", "Lnl/dionsegijn/konfetti/ParticleSystem;", "timer", "Lnl/dionsegijn/konfetti/KonfettiView$TimerIntegration;", "build", "getActiveSystems", "onDraw", "", "canvas", "Landroid/graphics/Canvas;", "reset", "start", "particleSystem", "stop", "TimerIntegration", "konfetti_release"}, k = 1, mv = {1, 1, 15})
/* compiled from: KonfettiView.kt */
public final class KonfettiView extends View {
    private OnParticleSystemUpdateListener onParticleSystemUpdateListener;
    private final List<ParticleSystem> systems = new ArrayList();
    private TimerIntegration timer = new TimerIntegration();

    public KonfettiView(Context context) {
        super(context);
    }

    public KonfettiView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public KonfettiView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    public final OnParticleSystemUpdateListener getOnParticleSystemUpdateListener() {
        return this.onParticleSystemUpdateListener;
    }

    public final void setOnParticleSystemUpdateListener(OnParticleSystemUpdateListener onParticleSystemUpdateListener2) {
        this.onParticleSystemUpdateListener = onParticleSystemUpdateListener2;
    }

    public final List<ParticleSystem> getActiveSystems() {
        return this.systems;
    }

    public final ParticleSystem build() {
        return new ParticleSystem(this);
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        Intrinsics.checkParameterIsNotNull(canvas, "canvas");
        super.onDraw(canvas);
        float deltaTime = this.timer.getDeltaTime();
        for (int size = this.systems.size() - 1; size >= 0; size--) {
            ParticleSystem particleSystem = this.systems.get(size);
            particleSystem.getRenderSystem$konfetti_release().render(canvas, deltaTime);
            if (particleSystem.doneEmitting()) {
                this.systems.remove(size);
                OnParticleSystemUpdateListener onParticleSystemUpdateListener2 = this.onParticleSystemUpdateListener;
                if (onParticleSystemUpdateListener2 != null) {
                    onParticleSystemUpdateListener2.onParticleSystemEnded(this, particleSystem, this.systems.size());
                }
            }
        }
        if (this.systems.size() != 0) {
            invalidate();
        } else {
            this.timer.reset();
        }
    }

    public final void start(ParticleSystem particleSystem) {
        Intrinsics.checkParameterIsNotNull(particleSystem, "particleSystem");
        this.systems.add(particleSystem);
        OnParticleSystemUpdateListener onParticleSystemUpdateListener2 = this.onParticleSystemUpdateListener;
        if (onParticleSystemUpdateListener2 != null) {
            onParticleSystemUpdateListener2.onParticleSystemStarted(this, particleSystem, this.systems.size());
        }
        invalidate();
    }

    public final void stop(ParticleSystem particleSystem) {
        Intrinsics.checkParameterIsNotNull(particleSystem, "particleSystem");
        this.systems.remove(particleSystem);
        OnParticleSystemUpdateListener onParticleSystemUpdateListener2 = this.onParticleSystemUpdateListener;
        if (onParticleSystemUpdateListener2 != null) {
            onParticleSystemUpdateListener2.onParticleSystemEnded(this, particleSystem, this.systems.size());
        }
    }

    public final void reset() {
        this.systems.clear();
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u0007\n\u0000\n\u0002\u0010\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u0006\u0010\u0005\u001a\u00020\u0006J\u0006\u0010\u0007\u001a\u00020\bR\u000e\u0010\u0003\u001a\u00020\u0004X\u000e¢\u0006\u0002\n\u0000¨\u0006\t"}, d2 = {"Lnl/dionsegijn/konfetti/KonfettiView$TimerIntegration;", "", "()V", "previousTime", "", "getDeltaTime", "", "reset", "", "konfetti_release"}, k = 1, mv = {1, 1, 15})
    /* compiled from: KonfettiView.kt */
    public static final class TimerIntegration {
        private long previousTime = -1;

        public final void reset() {
            this.previousTime = -1;
        }

        public final float getDeltaTime() {
            if (this.previousTime == -1) {
                this.previousTime = System.nanoTime();
            }
            long nanoTime = System.nanoTime();
            this.previousTime = nanoTime;
            return ((float) ((nanoTime - this.previousTime) / ((long) 1000000))) / ((float) 1000);
        }
    }
}
