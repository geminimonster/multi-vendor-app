package nl.dionsegijn.konfetti;

public final class R {

    public static final class string {
        public static final int app_name = 2131820630;

        private string() {
        }
    }

    private R() {
    }
}
