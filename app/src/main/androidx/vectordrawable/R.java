package androidx.vectordrawable;

public final class R {

    public static final class attr {
        public static final int alpha = 2130968619;
        public static final int font = 2130968958;
        public static final int fontProviderAuthority = 2130968961;
        public static final int fontProviderCerts = 2130968962;
        public static final int fontProviderFetchStrategy = 2130968963;
        public static final int fontProviderFetchTimeout = 2130968964;
        public static final int fontProviderPackage = 2130968965;
        public static final int fontProviderQuery = 2130968966;
        public static final int fontStyle = 2130968967;
        public static final int fontVariationSettings = 2130968968;
        public static final int fontWeight = 2130968969;
        public static final int ttcIndex = 2130969433;

        private attr() {
        }
    }

    public static final class color {
        public static final int notification_action_color_filter = 2131099889;
        public static final int notification_icon_bg_color = 2131099890;
        public static final int ripple_material_light = 2131099903;
        public static final int secondary_text_default_material_light = 2131099907;

        private color() {
        }
    }

    public static final class dimen {
        public static final int compat_button_inset_horizontal_material = 2131165342;
        public static final int compat_button_inset_vertical_material = 2131165343;
        public static final int compat_button_padding_horizontal_material = 2131165344;
        public static final int compat_button_padding_vertical_material = 2131165345;
        public static final int compat_control_corner_material = 2131165346;
        public static final int compat_notification_large_icon_max_height = 2131165347;
        public static final int compat_notification_large_icon_max_width = 2131165348;
        public static final int notification_action_icon_size = 2131165571;
        public static final int notification_action_text_size = 2131165572;
        public static final int notification_big_circle_margin = 2131165573;
        public static final int notification_content_margin_start = 2131165574;
        public static final int notification_large_icon_height = 2131165575;
        public static final int notification_large_icon_width = 2131165576;
        public static final int notification_main_column_padding_top = 2131165577;
        public static final int notification_media_narrow_margin = 2131165578;
        public static final int notification_right_icon_size = 2131165579;
        public static final int notification_right_side_padding_top = 2131165580;
        public static final int notification_small_icon_background_padding = 2131165581;
        public static final int notification_small_icon_size_as_large = 2131165582;
        public static final int notification_subtext_size = 2131165583;
        public static final int notification_top_pad = 2131165584;
        public static final int notification_top_pad_large_text = 2131165585;

        private dimen() {
        }
    }

    public static final class drawable {
        public static final int notification_action_background = 2131231023;
        public static final int notification_bg = 2131231024;
        public static final int notification_bg_low = 2131231025;
        public static final int notification_bg_low_normal = 2131231026;
        public static final int notification_bg_low_pressed = 2131231027;
        public static final int notification_bg_normal = 2131231028;
        public static final int notification_bg_normal_pressed = 2131231029;
        public static final int notification_icon_background = 2131231030;
        public static final int notification_template_icon_bg = 2131231031;
        public static final int notification_template_icon_low_bg = 2131231032;
        public static final int notification_tile_bg = 2131231033;
        public static final int notify_panel_notification_icon_bg = 2131231034;

        private drawable() {
        }
    }

    public static final class id {
        public static final int accessibility_action_clickable_span = 2131296270;
        public static final int accessibility_custom_action_0 = 2131296271;
        public static final int accessibility_custom_action_1 = 2131296272;
        public static final int accessibility_custom_action_10 = 2131296273;
        public static final int accessibility_custom_action_11 = 2131296274;
        public static final int accessibility_custom_action_12 = 2131296275;
        public static final int accessibility_custom_action_13 = 2131296276;
        public static final int accessibility_custom_action_14 = 2131296277;
        public static final int accessibility_custom_action_15 = 2131296278;
        public static final int accessibility_custom_action_16 = 2131296279;
        public static final int accessibility_custom_action_17 = 2131296280;
        public static final int accessibility_custom_action_18 = 2131296281;
        public static final int accessibility_custom_action_19 = 2131296282;
        public static final int accessibility_custom_action_2 = 2131296283;
        public static final int accessibility_custom_action_20 = 2131296284;
        public static final int accessibility_custom_action_21 = 2131296285;
        public static final int accessibility_custom_action_22 = 2131296286;
        public static final int accessibility_custom_action_23 = 2131296287;
        public static final int accessibility_custom_action_24 = 2131296288;
        public static final int accessibility_custom_action_25 = 2131296289;
        public static final int accessibility_custom_action_26 = 2131296290;
        public static final int accessibility_custom_action_27 = 2131296291;
        public static final int accessibility_custom_action_28 = 2131296292;
        public static final int accessibility_custom_action_29 = 2131296293;
        public static final int accessibility_custom_action_3 = 2131296294;
        public static final int accessibility_custom_action_30 = 2131296295;
        public static final int accessibility_custom_action_31 = 2131296296;
        public static final int accessibility_custom_action_4 = 2131296297;
        public static final int accessibility_custom_action_5 = 2131296298;
        public static final int accessibility_custom_action_6 = 2131296299;
        public static final int accessibility_custom_action_7 = 2131296300;
        public static final int accessibility_custom_action_8 = 2131296301;
        public static final int accessibility_custom_action_9 = 2131296302;
        public static final int action_container = 2131296315;
        public static final int action_divider = 2131296317;
        public static final int action_image = 2131296319;
        public static final int action_text = 2131296327;
        public static final int actions = 2131296329;
        public static final int async = 2131296341;
        public static final int blocking = 2131296352;
        public static final int chronometer = 2131296406;
        public static final int dialog_button = 2131296467;
        public static final int forever = 2131296543;
        public static final int icon = 2131296566;
        public static final int icon_group = 2131296567;
        public static final int info = 2131296579;
        public static final int italic = 2131296584;
        public static final int line1 = 2131296663;
        public static final int line3 = 2131296664;
        public static final int normal = 2131296746;
        public static final int notification_background = 2131296747;
        public static final int notification_main_column = 2131296748;
        public static final int notification_main_column_container = 2131296749;
        public static final int right_icon = 2131296805;
        public static final int right_side = 2131296806;
        public static final int tag_accessibility_actions = 2131296924;
        public static final int tag_accessibility_clickable_spans = 2131296925;
        public static final int tag_accessibility_heading = 2131296926;
        public static final int tag_accessibility_pane_title = 2131296927;
        public static final int tag_screen_reader_focusable = 2131296928;
        public static final int tag_transition_group = 2131296929;
        public static final int tag_unhandled_key_event_manager = 2131296930;
        public static final int tag_unhandled_key_listeners = 2131296931;
        public static final int text = 2131296935;
        public static final int text2 = 2131296936;
        public static final int time = 2131296952;
        public static final int title = 2131296953;

        private id() {
        }
    }

    public static final class integer {
        public static final int status_bar_notification_info_maxnum = 2131361813;

        private integer() {
        }
    }

    public static final class layout {
        public static final int custom_dialog = 2131492949;
        public static final int notification_action = 2131493052;
        public static final int notification_action_tombstone = 2131493053;
        public static final int notification_template_custom_big = 2131493060;
        public static final int notification_template_icon_group = 2131493061;
        public static final int notification_template_part_chronometer = 2131493065;
        public static final int notification_template_part_time = 2131493066;

        private layout() {
        }
    }

    public static final class string {
        public static final int status_bar_notification_info_overflow = 2131821111;

        private string() {
        }
    }

    public static final class style {
        public static final int TextAppearance_Compat_Notification = 2131886501;
        public static final int TextAppearance_Compat_Notification_Info = 2131886502;
        public static final int TextAppearance_Compat_Notification_Line2 = 2131886504;
        public static final int TextAppearance_Compat_Notification_Time = 2131886507;
        public static final int TextAppearance_Compat_Notification_Title = 2131886509;
        public static final int Widget_Compat_NotificationActionContainer = 2131886735;
        public static final int Widget_Compat_NotificationActionText = 2131886736;

        private style() {
        }
    }

    public static final class styleable {
        public static final int[] ColorStateListItem = {16843173, 16843551, com.store.proshop.R.attr.alpha};
        public static final int ColorStateListItem_alpha = 2;
        public static final int ColorStateListItem_android_alpha = 1;
        public static final int ColorStateListItem_android_color = 0;
        public static final int[] FontFamily = {com.store.proshop.R.attr.fontProviderAuthority, com.store.proshop.R.attr.fontProviderCerts, com.store.proshop.R.attr.fontProviderFetchStrategy, com.store.proshop.R.attr.fontProviderFetchTimeout, com.store.proshop.R.attr.fontProviderPackage, com.store.proshop.R.attr.fontProviderQuery};
        public static final int[] FontFamilyFont = {16844082, 16844083, 16844095, 16844143, 16844144, com.store.proshop.R.attr.font, com.store.proshop.R.attr.fontStyle, com.store.proshop.R.attr.fontVariationSettings, com.store.proshop.R.attr.fontWeight, com.store.proshop.R.attr.ttcIndex};
        public static final int FontFamilyFont_android_font = 0;
        public static final int FontFamilyFont_android_fontStyle = 2;
        public static final int FontFamilyFont_android_fontVariationSettings = 4;
        public static final int FontFamilyFont_android_fontWeight = 1;
        public static final int FontFamilyFont_android_ttcIndex = 3;
        public static final int FontFamilyFont_font = 5;
        public static final int FontFamilyFont_fontStyle = 6;
        public static final int FontFamilyFont_fontVariationSettings = 7;
        public static final int FontFamilyFont_fontWeight = 8;
        public static final int FontFamilyFont_ttcIndex = 9;
        public static final int FontFamily_fontProviderAuthority = 0;
        public static final int FontFamily_fontProviderCerts = 1;
        public static final int FontFamily_fontProviderFetchStrategy = 2;
        public static final int FontFamily_fontProviderFetchTimeout = 3;
        public static final int FontFamily_fontProviderPackage = 4;
        public static final int FontFamily_fontProviderQuery = 5;
        public static final int[] GradientColor = {16843165, 16843166, 16843169, 16843170, 16843171, 16843172, 16843265, 16843275, 16844048, 16844049, 16844050, 16844051};
        public static final int[] GradientColorItem = {16843173, 16844052};
        public static final int GradientColorItem_android_color = 0;
        public static final int GradientColorItem_android_offset = 1;
        public static final int GradientColor_android_centerColor = 7;
        public static final int GradientColor_android_centerX = 3;
        public static final int GradientColor_android_centerY = 4;
        public static final int GradientColor_android_endColor = 1;
        public static final int GradientColor_android_endX = 10;
        public static final int GradientColor_android_endY = 11;
        public static final int GradientColor_android_gradientRadius = 5;
        public static final int GradientColor_android_startColor = 0;
        public static final int GradientColor_android_startX = 8;
        public static final int GradientColor_android_startY = 9;
        public static final int GradientColor_android_tileMode = 6;
        public static final int GradientColor_android_type = 2;

        private styleable() {
        }
    }

    private R() {
    }
}
