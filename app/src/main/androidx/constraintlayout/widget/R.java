package androidx.constraintlayout.widget;

public final class R {

    public static final class attr {
        public static final int barrierAllowsGoneWidgets = 2130968646;
        public static final int barrierDirection = 2130968647;
        public static final int chainUseRtl = 2130968707;
        public static final int constraintSet = 2130968791;
        public static final int constraint_referenced_ids = 2130968792;
        public static final int content = 2130968793;
        public static final int emptyVisibility = 2130968916;
        public static final int layout_constrainedHeight = 2130969066;
        public static final int layout_constrainedWidth = 2130969067;
        public static final int layout_constraintBaseline_creator = 2130969068;
        public static final int layout_constraintBaseline_toBaselineOf = 2130969069;
        public static final int layout_constraintBottom_creator = 2130969070;
        public static final int layout_constraintBottom_toBottomOf = 2130969071;
        public static final int layout_constraintBottom_toTopOf = 2130969072;
        public static final int layout_constraintCircle = 2130969073;
        public static final int layout_constraintCircleAngle = 2130969074;
        public static final int layout_constraintCircleRadius = 2130969075;
        public static final int layout_constraintDimensionRatio = 2130969076;
        public static final int layout_constraintEnd_toEndOf = 2130969077;
        public static final int layout_constraintEnd_toStartOf = 2130969078;
        public static final int layout_constraintGuide_begin = 2130969079;
        public static final int layout_constraintGuide_end = 2130969080;
        public static final int layout_constraintGuide_percent = 2130969081;
        public static final int layout_constraintHeight_default = 2130969082;
        public static final int layout_constraintHeight_max = 2130969083;
        public static final int layout_constraintHeight_min = 2130969084;
        public static final int layout_constraintHeight_percent = 2130969085;
        public static final int layout_constraintHorizontal_bias = 2130969086;
        public static final int layout_constraintHorizontal_chainStyle = 2130969087;
        public static final int layout_constraintHorizontal_weight = 2130969088;
        public static final int layout_constraintLeft_creator = 2130969089;
        public static final int layout_constraintLeft_toLeftOf = 2130969090;
        public static final int layout_constraintLeft_toRightOf = 2130969091;
        public static final int layout_constraintRight_creator = 2130969092;
        public static final int layout_constraintRight_toLeftOf = 2130969093;
        public static final int layout_constraintRight_toRightOf = 2130969094;
        public static final int layout_constraintStart_toEndOf = 2130969095;
        public static final int layout_constraintStart_toStartOf = 2130969096;
        public static final int layout_constraintTop_creator = 2130969097;
        public static final int layout_constraintTop_toBottomOf = 2130969098;
        public static final int layout_constraintTop_toTopOf = 2130969099;
        public static final int layout_constraintVertical_bias = 2130969100;
        public static final int layout_constraintVertical_chainStyle = 2130969101;
        public static final int layout_constraintVertical_weight = 2130969102;
        public static final int layout_constraintWidth_default = 2130969103;
        public static final int layout_constraintWidth_max = 2130969104;
        public static final int layout_constraintWidth_min = 2130969105;
        public static final int layout_constraintWidth_percent = 2130969106;
        public static final int layout_editor_absoluteX = 2130969108;
        public static final int layout_editor_absoluteY = 2130969109;
        public static final int layout_goneMarginBottom = 2130969110;
        public static final int layout_goneMarginEnd = 2130969111;
        public static final int layout_goneMarginLeft = 2130969112;
        public static final int layout_goneMarginRight = 2130969113;
        public static final int layout_goneMarginStart = 2130969114;
        public static final int layout_goneMarginTop = 2130969115;
        public static final int layout_optimizationLevel = 2130969118;

        private attr() {
        }
    }

    public static final class id {
        public static final int bottom = 2131296355;
        public static final int end = 2131296507;
        public static final int gone = 2131296550;
        public static final int invisible = 2131296581;
        public static final int left = 2131296660;
        public static final int packed = 2131296764;
        public static final int parent = 2131296768;
        public static final int percent = 2131296775;
        public static final int right = 2131296804;
        public static final int spread = 2131296905;
        public static final int spread_inside = 2131296906;
        public static final int start = 2131296911;
        public static final int top = 2131296971;
        public static final int wrap = 2131297131;

        private id() {
        }
    }

    public static final class styleable {
        public static final int[] ConstraintLayout_Layout = {16842948, 16843039, 16843040, 16843071, 16843072, com.store.proshop.R.attr.barrierAllowsGoneWidgets, com.store.proshop.R.attr.barrierDirection, com.store.proshop.R.attr.chainUseRtl, com.store.proshop.R.attr.constraintSet, com.store.proshop.R.attr.constraint_referenced_ids, com.store.proshop.R.attr.layout_constrainedHeight, com.store.proshop.R.attr.layout_constrainedWidth, com.store.proshop.R.attr.layout_constraintBaseline_creator, com.store.proshop.R.attr.layout_constraintBaseline_toBaselineOf, com.store.proshop.R.attr.layout_constraintBottom_creator, com.store.proshop.R.attr.layout_constraintBottom_toBottomOf, com.store.proshop.R.attr.layout_constraintBottom_toTopOf, com.store.proshop.R.attr.layout_constraintCircle, com.store.proshop.R.attr.layout_constraintCircleAngle, com.store.proshop.R.attr.layout_constraintCircleRadius, com.store.proshop.R.attr.layout_constraintDimensionRatio, com.store.proshop.R.attr.layout_constraintEnd_toEndOf, com.store.proshop.R.attr.layout_constraintEnd_toStartOf, com.store.proshop.R.attr.layout_constraintGuide_begin, com.store.proshop.R.attr.layout_constraintGuide_end, com.store.proshop.R.attr.layout_constraintGuide_percent, com.store.proshop.R.attr.layout_constraintHeight_default, com.store.proshop.R.attr.layout_constraintHeight_max, com.store.proshop.R.attr.layout_constraintHeight_min, com.store.proshop.R.attr.layout_constraintHeight_percent, com.store.proshop.R.attr.layout_constraintHorizontal_bias, com.store.proshop.R.attr.layout_constraintHorizontal_chainStyle, com.store.proshop.R.attr.layout_constraintHorizontal_weight, com.store.proshop.R.attr.layout_constraintLeft_creator, com.store.proshop.R.attr.layout_constraintLeft_toLeftOf, com.store.proshop.R.attr.layout_constraintLeft_toRightOf, com.store.proshop.R.attr.layout_constraintRight_creator, com.store.proshop.R.attr.layout_constraintRight_toLeftOf, com.store.proshop.R.attr.layout_constraintRight_toRightOf, com.store.proshop.R.attr.layout_constraintStart_toEndOf, com.store.proshop.R.attr.layout_constraintStart_toStartOf, com.store.proshop.R.attr.layout_constraintTop_creator, com.store.proshop.R.attr.layout_constraintTop_toBottomOf, com.store.proshop.R.attr.layout_constraintTop_toTopOf, com.store.proshop.R.attr.layout_constraintVertical_bias, com.store.proshop.R.attr.layout_constraintVertical_chainStyle, com.store.proshop.R.attr.layout_constraintVertical_weight, com.store.proshop.R.attr.layout_constraintWidth_default, com.store.proshop.R.attr.layout_constraintWidth_max, com.store.proshop.R.attr.layout_constraintWidth_min, com.store.proshop.R.attr.layout_constraintWidth_percent, com.store.proshop.R.attr.layout_editor_absoluteX, com.store.proshop.R.attr.layout_editor_absoluteY, com.store.proshop.R.attr.layout_goneMarginBottom, com.store.proshop.R.attr.layout_goneMarginEnd, com.store.proshop.R.attr.layout_goneMarginLeft, com.store.proshop.R.attr.layout_goneMarginRight, com.store.proshop.R.attr.layout_goneMarginStart, com.store.proshop.R.attr.layout_goneMarginTop, com.store.proshop.R.attr.layout_optimizationLevel};
        public static final int ConstraintLayout_Layout_android_maxHeight = 2;
        public static final int ConstraintLayout_Layout_android_maxWidth = 1;
        public static final int ConstraintLayout_Layout_android_minHeight = 4;
        public static final int ConstraintLayout_Layout_android_minWidth = 3;
        public static final int ConstraintLayout_Layout_android_orientation = 0;
        public static final int ConstraintLayout_Layout_barrierAllowsGoneWidgets = 5;
        public static final int ConstraintLayout_Layout_barrierDirection = 6;
        public static final int ConstraintLayout_Layout_chainUseRtl = 7;
        public static final int ConstraintLayout_Layout_constraintSet = 8;
        public static final int ConstraintLayout_Layout_constraint_referenced_ids = 9;
        public static final int ConstraintLayout_Layout_layout_constrainedHeight = 10;
        public static final int ConstraintLayout_Layout_layout_constrainedWidth = 11;
        public static final int ConstraintLayout_Layout_layout_constraintBaseline_creator = 12;
        public static final int ConstraintLayout_Layout_layout_constraintBaseline_toBaselineOf = 13;
        public static final int ConstraintLayout_Layout_layout_constraintBottom_creator = 14;
        public static final int ConstraintLayout_Layout_layout_constraintBottom_toBottomOf = 15;
        public static final int ConstraintLayout_Layout_layout_constraintBottom_toTopOf = 16;
        public static final int ConstraintLayout_Layout_layout_constraintCircle = 17;
        public static final int ConstraintLayout_Layout_layout_constraintCircleAngle = 18;
        public static final int ConstraintLayout_Layout_layout_constraintCircleRadius = 19;
        public static final int ConstraintLayout_Layout_layout_constraintDimensionRatio = 20;
        public static final int ConstraintLayout_Layout_layout_constraintEnd_toEndOf = 21;
        public static final int ConstraintLayout_Layout_layout_constraintEnd_toStartOf = 22;
        public static final int ConstraintLayout_Layout_layout_constraintGuide_begin = 23;
        public static final int ConstraintLayout_Layout_layout_constraintGuide_end = 24;
        public static final int ConstraintLayout_Layout_layout_constraintGuide_percent = 25;
        public static final int ConstraintLayout_Layout_layout_constraintHeight_default = 26;
        public static final int ConstraintLayout_Layout_layout_constraintHeight_max = 27;
        public static final int ConstraintLayout_Layout_layout_constraintHeight_min = 28;
        public static final int ConstraintLayout_Layout_layout_constraintHeight_percent = 29;
        public static final int ConstraintLayout_Layout_layout_constraintHorizontal_bias = 30;
        public static final int ConstraintLayout_Layout_layout_constraintHorizontal_chainStyle = 31;
        public static final int ConstraintLayout_Layout_layout_constraintHorizontal_weight = 32;
        public static final int ConstraintLayout_Layout_layout_constraintLeft_creator = 33;
        public static final int ConstraintLayout_Layout_layout_constraintLeft_toLeftOf = 34;
        public static final int ConstraintLayout_Layout_layout_constraintLeft_toRightOf = 35;
        public static final int ConstraintLayout_Layout_layout_constraintRight_creator = 36;
        public static final int ConstraintLayout_Layout_layout_constraintRight_toLeftOf = 37;
        public static final int ConstraintLayout_Layout_layout_constraintRight_toRightOf = 38;
        public static final int ConstraintLayout_Layout_layout_constraintStart_toEndOf = 39;
        public static final int ConstraintLayout_Layout_layout_constraintStart_toStartOf = 40;
        public static final int ConstraintLayout_Layout_layout_constraintTop_creator = 41;
        public static final int ConstraintLayout_Layout_layout_constraintTop_toBottomOf = 42;
        public static final int ConstraintLayout_Layout_layout_constraintTop_toTopOf = 43;
        public static final int ConstraintLayout_Layout_layout_constraintVertical_bias = 44;
        public static final int ConstraintLayout_Layout_layout_constraintVertical_chainStyle = 45;
        public static final int ConstraintLayout_Layout_layout_constraintVertical_weight = 46;
        public static final int ConstraintLayout_Layout_layout_constraintWidth_default = 47;
        public static final int ConstraintLayout_Layout_layout_constraintWidth_max = 48;
        public static final int ConstraintLayout_Layout_layout_constraintWidth_min = 49;
        public static final int ConstraintLayout_Layout_layout_constraintWidth_percent = 50;
        public static final int ConstraintLayout_Layout_layout_editor_absoluteX = 51;
        public static final int ConstraintLayout_Layout_layout_editor_absoluteY = 52;
        public static final int ConstraintLayout_Layout_layout_goneMarginBottom = 53;
        public static final int ConstraintLayout_Layout_layout_goneMarginEnd = 54;
        public static final int ConstraintLayout_Layout_layout_goneMarginLeft = 55;
        public static final int ConstraintLayout_Layout_layout_goneMarginRight = 56;
        public static final int ConstraintLayout_Layout_layout_goneMarginStart = 57;
        public static final int ConstraintLayout_Layout_layout_goneMarginTop = 58;
        public static final int ConstraintLayout_Layout_layout_optimizationLevel = 59;
        public static final int[] ConstraintLayout_placeholder = {com.store.proshop.R.attr.content, com.store.proshop.R.attr.emptyVisibility};
        public static final int ConstraintLayout_placeholder_content = 0;
        public static final int ConstraintLayout_placeholder_emptyVisibility = 1;
        public static final int[] ConstraintSet = {16842948, 16842960, 16842972, 16842996, 16842997, 16842999, 16843000, 16843001, 16843002, 16843039, 16843040, 16843071, 16843072, 16843551, 16843552, 16843553, 16843554, 16843555, 16843556, 16843557, 16843558, 16843559, 16843560, 16843701, 16843702, 16843770, 16843840, com.store.proshop.R.attr.barrierAllowsGoneWidgets, com.store.proshop.R.attr.barrierDirection, com.store.proshop.R.attr.chainUseRtl, com.store.proshop.R.attr.constraint_referenced_ids, com.store.proshop.R.attr.layout_constrainedHeight, com.store.proshop.R.attr.layout_constrainedWidth, com.store.proshop.R.attr.layout_constraintBaseline_creator, com.store.proshop.R.attr.layout_constraintBaseline_toBaselineOf, com.store.proshop.R.attr.layout_constraintBottom_creator, com.store.proshop.R.attr.layout_constraintBottom_toBottomOf, com.store.proshop.R.attr.layout_constraintBottom_toTopOf, com.store.proshop.R.attr.layout_constraintCircle, com.store.proshop.R.attr.layout_constraintCircleAngle, com.store.proshop.R.attr.layout_constraintCircleRadius, com.store.proshop.R.attr.layout_constraintDimensionRatio, com.store.proshop.R.attr.layout_constraintEnd_toEndOf, com.store.proshop.R.attr.layout_constraintEnd_toStartOf, com.store.proshop.R.attr.layout_constraintGuide_begin, com.store.proshop.R.attr.layout_constraintGuide_end, com.store.proshop.R.attr.layout_constraintGuide_percent, com.store.proshop.R.attr.layout_constraintHeight_default, com.store.proshop.R.attr.layout_constraintHeight_max, com.store.proshop.R.attr.layout_constraintHeight_min, com.store.proshop.R.attr.layout_constraintHeight_percent, com.store.proshop.R.attr.layout_constraintHorizontal_bias, com.store.proshop.R.attr.layout_constraintHorizontal_chainStyle, com.store.proshop.R.attr.layout_constraintHorizontal_weight, com.store.proshop.R.attr.layout_constraintLeft_creator, com.store.proshop.R.attr.layout_constraintLeft_toLeftOf, com.store.proshop.R.attr.layout_constraintLeft_toRightOf, com.store.proshop.R.attr.layout_constraintRight_creator, com.store.proshop.R.attr.layout_constraintRight_toLeftOf, com.store.proshop.R.attr.layout_constraintRight_toRightOf, com.store.proshop.R.attr.layout_constraintStart_toEndOf, com.store.proshop.R.attr.layout_constraintStart_toStartOf, com.store.proshop.R.attr.layout_constraintTop_creator, com.store.proshop.R.attr.layout_constraintTop_toBottomOf, com.store.proshop.R.attr.layout_constraintTop_toTopOf, com.store.proshop.R.attr.layout_constraintVertical_bias, com.store.proshop.R.attr.layout_constraintVertical_chainStyle, com.store.proshop.R.attr.layout_constraintVertical_weight, com.store.proshop.R.attr.layout_constraintWidth_default, com.store.proshop.R.attr.layout_constraintWidth_max, com.store.proshop.R.attr.layout_constraintWidth_min, com.store.proshop.R.attr.layout_constraintWidth_percent, com.store.proshop.R.attr.layout_editor_absoluteX, com.store.proshop.R.attr.layout_editor_absoluteY, com.store.proshop.R.attr.layout_goneMarginBottom, com.store.proshop.R.attr.layout_goneMarginEnd, com.store.proshop.R.attr.layout_goneMarginLeft, com.store.proshop.R.attr.layout_goneMarginRight, com.store.proshop.R.attr.layout_goneMarginStart, com.store.proshop.R.attr.layout_goneMarginTop};
        public static final int ConstraintSet_android_alpha = 13;
        public static final int ConstraintSet_android_elevation = 26;
        public static final int ConstraintSet_android_id = 1;
        public static final int ConstraintSet_android_layout_height = 4;
        public static final int ConstraintSet_android_layout_marginBottom = 8;
        public static final int ConstraintSet_android_layout_marginEnd = 24;
        public static final int ConstraintSet_android_layout_marginLeft = 5;
        public static final int ConstraintSet_android_layout_marginRight = 7;
        public static final int ConstraintSet_android_layout_marginStart = 23;
        public static final int ConstraintSet_android_layout_marginTop = 6;
        public static final int ConstraintSet_android_layout_width = 3;
        public static final int ConstraintSet_android_maxHeight = 10;
        public static final int ConstraintSet_android_maxWidth = 9;
        public static final int ConstraintSet_android_minHeight = 12;
        public static final int ConstraintSet_android_minWidth = 11;
        public static final int ConstraintSet_android_orientation = 0;
        public static final int ConstraintSet_android_rotation = 20;
        public static final int ConstraintSet_android_rotationX = 21;
        public static final int ConstraintSet_android_rotationY = 22;
        public static final int ConstraintSet_android_scaleX = 18;
        public static final int ConstraintSet_android_scaleY = 19;
        public static final int ConstraintSet_android_transformPivotX = 14;
        public static final int ConstraintSet_android_transformPivotY = 15;
        public static final int ConstraintSet_android_translationX = 16;
        public static final int ConstraintSet_android_translationY = 17;
        public static final int ConstraintSet_android_translationZ = 25;
        public static final int ConstraintSet_android_visibility = 2;
        public static final int ConstraintSet_barrierAllowsGoneWidgets = 27;
        public static final int ConstraintSet_barrierDirection = 28;
        public static final int ConstraintSet_chainUseRtl = 29;
        public static final int ConstraintSet_constraint_referenced_ids = 30;
        public static final int ConstraintSet_layout_constrainedHeight = 31;
        public static final int ConstraintSet_layout_constrainedWidth = 32;
        public static final int ConstraintSet_layout_constraintBaseline_creator = 33;
        public static final int ConstraintSet_layout_constraintBaseline_toBaselineOf = 34;
        public static final int ConstraintSet_layout_constraintBottom_creator = 35;
        public static final int ConstraintSet_layout_constraintBottom_toBottomOf = 36;
        public static final int ConstraintSet_layout_constraintBottom_toTopOf = 37;
        public static final int ConstraintSet_layout_constraintCircle = 38;
        public static final int ConstraintSet_layout_constraintCircleAngle = 39;
        public static final int ConstraintSet_layout_constraintCircleRadius = 40;
        public static final int ConstraintSet_layout_constraintDimensionRatio = 41;
        public static final int ConstraintSet_layout_constraintEnd_toEndOf = 42;
        public static final int ConstraintSet_layout_constraintEnd_toStartOf = 43;
        public static final int ConstraintSet_layout_constraintGuide_begin = 44;
        public static final int ConstraintSet_layout_constraintGuide_end = 45;
        public static final int ConstraintSet_layout_constraintGuide_percent = 46;
        public static final int ConstraintSet_layout_constraintHeight_default = 47;
        public static final int ConstraintSet_layout_constraintHeight_max = 48;
        public static final int ConstraintSet_layout_constraintHeight_min = 49;
        public static final int ConstraintSet_layout_constraintHeight_percent = 50;
        public static final int ConstraintSet_layout_constraintHorizontal_bias = 51;
        public static final int ConstraintSet_layout_constraintHorizontal_chainStyle = 52;
        public static final int ConstraintSet_layout_constraintHorizontal_weight = 53;
        public static final int ConstraintSet_layout_constraintLeft_creator = 54;
        public static final int ConstraintSet_layout_constraintLeft_toLeftOf = 55;
        public static final int ConstraintSet_layout_constraintLeft_toRightOf = 56;
        public static final int ConstraintSet_layout_constraintRight_creator = 57;
        public static final int ConstraintSet_layout_constraintRight_toLeftOf = 58;
        public static final int ConstraintSet_layout_constraintRight_toRightOf = 59;
        public static final int ConstraintSet_layout_constraintStart_toEndOf = 60;
        public static final int ConstraintSet_layout_constraintStart_toStartOf = 61;
        public static final int ConstraintSet_layout_constraintTop_creator = 62;
        public static final int ConstraintSet_layout_constraintTop_toBottomOf = 63;
        public static final int ConstraintSet_layout_constraintTop_toTopOf = 64;
        public static final int ConstraintSet_layout_constraintVertical_bias = 65;
        public static final int ConstraintSet_layout_constraintVertical_chainStyle = 66;
        public static final int ConstraintSet_layout_constraintVertical_weight = 67;
        public static final int ConstraintSet_layout_constraintWidth_default = 68;
        public static final int ConstraintSet_layout_constraintWidth_max = 69;
        public static final int ConstraintSet_layout_constraintWidth_min = 70;
        public static final int ConstraintSet_layout_constraintWidth_percent = 71;
        public static final int ConstraintSet_layout_editor_absoluteX = 72;
        public static final int ConstraintSet_layout_editor_absoluteY = 73;
        public static final int ConstraintSet_layout_goneMarginBottom = 74;
        public static final int ConstraintSet_layout_goneMarginEnd = 75;
        public static final int ConstraintSet_layout_goneMarginLeft = 76;
        public static final int ConstraintSet_layout_goneMarginRight = 77;
        public static final int ConstraintSet_layout_goneMarginStart = 78;
        public static final int ConstraintSet_layout_goneMarginTop = 79;
        public static final int[] LinearConstraintLayout = {16842948};
        public static final int LinearConstraintLayout_android_orientation = 0;

        private styleable() {
        }
    }

    private R() {
    }
}
