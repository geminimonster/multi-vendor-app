package androidx.cardview;

public final class R {

    public static final class attr {
        public static final int cardBackgroundColor = 2130968696;
        public static final int cardCornerRadius = 2130968697;
        public static final int cardElevation = 2130968698;
        public static final int cardMaxElevation = 2130968701;
        public static final int cardPreventCornerOverlap = 2130968702;
        public static final int cardUseCompatPadding = 2130968705;
        public static final int cardViewStyle = 2130968706;
        public static final int contentPadding = 2130968801;
        public static final int contentPaddingBottom = 2130968802;
        public static final int contentPaddingLeft = 2130968803;
        public static final int contentPaddingRight = 2130968804;
        public static final int contentPaddingTop = 2130968805;

        private attr() {
        }
    }

    public static final class color {
        public static final int cardview_dark_background = 2131099694;
        public static final int cardview_light_background = 2131099695;
        public static final int cardview_shadow_end_color = 2131099696;
        public static final int cardview_shadow_start_color = 2131099697;

        private color() {
        }
    }

    public static final class dimen {
        public static final int cardview_compat_inset_shadow = 2131165322;
        public static final int cardview_default_elevation = 2131165323;
        public static final int cardview_default_radius = 2131165324;

        private dimen() {
        }
    }

    public static final class style {
        public static final int Base_CardView = 2131886103;
        public static final int CardView = 2131886313;
        public static final int CardView_Dark = 2131886314;
        public static final int CardView_Light = 2131886315;

        private style() {
        }
    }

    public static final class styleable {
        public static final int[] CardView = {16843071, 16843072, com.store.proshop.R.attr.cardBackgroundColor, com.store.proshop.R.attr.cardCornerRadius, com.store.proshop.R.attr.cardElevation, com.store.proshop.R.attr.cardMaxElevation, com.store.proshop.R.attr.cardPreventCornerOverlap, com.store.proshop.R.attr.cardUseCompatPadding, com.store.proshop.R.attr.contentPadding, com.store.proshop.R.attr.contentPaddingBottom, com.store.proshop.R.attr.contentPaddingLeft, com.store.proshop.R.attr.contentPaddingRight, com.store.proshop.R.attr.contentPaddingTop};
        public static final int CardView_android_minHeight = 1;
        public static final int CardView_android_minWidth = 0;
        public static final int CardView_cardBackgroundColor = 2;
        public static final int CardView_cardCornerRadius = 3;
        public static final int CardView_cardElevation = 4;
        public static final int CardView_cardMaxElevation = 5;
        public static final int CardView_cardPreventCornerOverlap = 6;
        public static final int CardView_cardUseCompatPadding = 7;
        public static final int CardView_contentPadding = 8;
        public static final int CardView_contentPaddingBottom = 9;
        public static final int CardView_contentPaddingLeft = 10;
        public static final int CardView_contentPaddingRight = 11;
        public static final int CardView_contentPaddingTop = 12;

        private styleable() {
        }
    }

    private R() {
    }
}
