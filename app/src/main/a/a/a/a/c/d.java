package a.a.a.a.c;

import com.stripe.android.stripe3ds2.exceptions.SDKRuntimeException;
import java.security.KeyFactory;
import kotlin.Metadata;
import kotlin.Result;
import kotlin.ResultKt;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0012\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u0000\u0018\u0000 \f2\u00020\u0001:\u0001\fB\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u000e\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bJ\u000e\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\bR\u000e\u0010\u0003\u001a\u00020\u0004X\u0004¢\u0006\u0002\n\u0000¨\u0006\r"}, d2 = {"Lcom/stripe/android/stripe3ds2/security/EcKeyFactory;", "", "()V", "keyFactory", "Ljava/security/KeyFactory;", "createPrivate", "Ljava/security/interfaces/ECPrivateKey;", "privateKeyEncoded", "", "createPublic", "Ljava/security/interfaces/ECPublicKey;", "publicKeyEncoded", "Companion", "3ds2sdk_release"}, k = 1, mv = {1, 1, 16})
public final class d {
    public static final d b = new d();
    public static final a c = new a();

    /* renamed from: a  reason: collision with root package name */
    public final KeyFactory f28a;

    public static final class a {
    }

    public d() {
        Object obj;
        try {
            Result.Companion companion = Result.Companion;
            obj = Result.m4constructorimpl(KeyFactory.getInstance("EC"));
        } catch (Throwable th) {
            Result.Companion companion2 = Result.Companion;
            obj = Result.m4constructorimpl(ResultKt.createFailure(th));
        }
        Throwable r1 = Result.m7exceptionOrNullimpl(obj);
        if (r1 == null) {
            Intrinsics.checkExpressionValueIsNotNull(obj, "runCatching {\n          …tion.create(it)\n        }");
            this.f28a = (KeyFactory) obj;
            return;
        }
        throw SDKRuntimeException.Companion.create(r1);
    }
}
