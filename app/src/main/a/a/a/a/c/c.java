package a.a.a.a.c;

import com.nimbusds.jose.jwk.KeyUse;
import com.stripe.android.stripe3ds2.exceptions.SDKRuntimeException;
import java.util.Collection;
import java.util.Set;
import kotlin.Metadata;
import kotlin.collections.SetsKt;
import kotlin.jvm.internal.Intrinsics;
import kotlin.text.StringsKt;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\u000b\n\u0002\b\u000b\b\u0001\u0018\u0000 \u001a2\b\u0012\u0004\u0012\u00020\u00000\u0001:\u0001\u001aB+\b\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0003\u0012\n\b\u0002\u0010\u0007\u001a\u0004\u0018\u00010\b¢\u0006\u0002\u0010\tR\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u0011\u0010\u0006\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\rR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\rR\u0011\u0010\u000f\u001a\u00020\u00108F¢\u0006\u0006\u001a\u0004\b\u000f\u0010\u0011R\u0013\u0010\u0007\u001a\u0004\u0018\u00010\b¢\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u0013j\u0002\b\u0014j\u0002\b\u0015j\u0002\b\u0016j\u0002\b\u0017j\u0002\b\u0018j\u0002\b\u0019¨\u0006\u001b"}, d2 = {"Lcom/stripe/android/stripe3ds2/security/DirectoryServer;", "", "id", "", "algorithm", "Lcom/stripe/android/stripe3ds2/security/Algorithm;", "fileName", "keyUse", "Lcom/nimbusds/jose/jwk/KeyUse;", "(Ljava/lang/String;ILjava/lang/String;Lcom/stripe/android/stripe3ds2/security/Algorithm;Ljava/lang/String;Lcom/nimbusds/jose/jwk/KeyUse;)V", "getAlgorithm", "()Lcom/stripe/android/stripe3ds2/security/Algorithm;", "getFileName", "()Ljava/lang/String;", "getId", "isCertificate", "", "()Z", "getKeyUse", "()Lcom/nimbusds/jose/jwk/KeyUse;", "TestRsa", "TestEc", "Visa", "Mastercard", "Amex", "Discover", "Companion", "3ds2sdk_release"}, k = 1, mv = {1, 1, 16})
public enum c {
    ;
    
    public static final Set<String> f = null;
    public static final a g = null;

    /* renamed from: a  reason: collision with root package name */
    public final String f27a;
    public final a b;
    public final String c;
    public final KeyUse d;

    public static final class a {
        public final c a(String str) {
            c cVar;
            Intrinsics.checkParameterIsNotNull(str, "directoryServerId");
            c[] values = c.values();
            int length = values.length;
            int i = 0;
            while (true) {
                if (i >= length) {
                    cVar = null;
                    break;
                }
                cVar = values[i];
                if (Intrinsics.areEqual((Object) str, (Object) cVar.f27a)) {
                    break;
                }
                i++;
            }
            if (cVar != null) {
                return cVar;
            }
            throw new SDKRuntimeException(new IllegalArgumentException("Unknown directory server id: " + str));
        }
    }

    /* access modifiers changed from: public */
    static {
        g = new a();
        f = SetsKt.setOf(".crt", ".cer", ".pem");
    }

    /* access modifiers changed from: public */
    c(String str, a aVar, String str2, KeyUse keyUse) {
        this.f27a = str;
        this.b = aVar;
        this.c = str2;
        this.d = keyUse;
    }

    public final boolean a() {
        Set<String> set = f;
        if ((set instanceof Collection) && set.isEmpty()) {
            return false;
        }
        for (String endsWith$default : set) {
            if (StringsKt.endsWith$default(this.c, endsWith$default, false, 2, (Object) null)) {
                return true;
            }
        }
        return false;
    }
}
