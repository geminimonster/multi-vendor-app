package a.a.a.a.c;

public enum a {
    EC("EC"),
    RSA("RSA");
    

    /* renamed from: a  reason: collision with root package name */
    public final String f26a;

    /* access modifiers changed from: public */
    a(String str) {
        this.f26a = str;
    }

    public String toString() {
        return this.f26a;
    }
}
