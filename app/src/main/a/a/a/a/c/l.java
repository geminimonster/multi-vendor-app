package a.a.a.a.c;

import android.content.Context;
import android.util.Base64;
import com.stripe.android.stripe3ds2.exceptions.SDKRuntimeException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.Scanner;
import kotlin.Result;
import kotlin.ResultKt;
import kotlin.TypeCastException;
import kotlin.jvm.internal.Intrinsics;
import kotlin.text.Charsets;

public final class l {

    /* renamed from: a  reason: collision with root package name */
    public final Context f32a;

    public l(Context context) {
        Intrinsics.checkParameterIsNotNull(context, "context");
        Context applicationContext = context.getApplicationContext();
        Intrinsics.checkExpressionValueIsNotNull(applicationContext, "context.applicationContext");
        this.f32a = applicationContext;
    }

    public final byte[] a(String str) {
        Object obj;
        try {
            Result.Companion companion = Result.Companion;
            InputStream open = this.f32a.getAssets().open(str);
            Intrinsics.checkExpressionValueIsNotNull(open, "context.assets.open(fileName)");
            String next = new Scanner(open).useDelimiter("\\A").next();
            Intrinsics.checkExpressionValueIsNotNull(next, "publicKey");
            Charset charset = Charsets.UTF_8;
            if (next != null) {
                byte[] bytes = next.getBytes(charset);
                Intrinsics.checkExpressionValueIsNotNull(bytes, "(this as java.lang.String).getBytes(charset)");
                obj = Result.m4constructorimpl(Base64.decode(bytes, 0));
                Throwable r0 = Result.m7exceptionOrNullimpl(obj);
                if (r0 == null) {
                    Intrinsics.checkExpressionValueIsNotNull(obj, "runCatching {\n          …eException(it))\n        }");
                    return (byte[]) obj;
                }
                throw new SDKRuntimeException(new RuntimeException(r0));
            }
            throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
        } catch (Throwable th) {
            Result.Companion companion2 = Result.Companion;
            obj = Result.m4constructorimpl(ResultKt.createFailure(th));
        }
    }
}
