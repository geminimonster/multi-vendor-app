package a.a.a.a.c;

import kotlin.jvm.internal.Intrinsics;

public final class f {

    /* renamed from: a  reason: collision with root package name */
    public final e f29a;
    public final b b;

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public f(e eVar) {
        this(eVar, new m());
        Intrinsics.checkParameterIsNotNull(eVar, "ephemeralKeyPairGenerator");
    }

    public f(e eVar, b bVar) {
        this.f29a = eVar;
        this.b = bVar;
    }
}
