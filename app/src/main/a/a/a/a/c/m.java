package a.a.a.a.c;

import com.nimbusds.jose.crypto.impl.ConcatKDF;
import com.nimbusds.jose.crypto.impl.ECDH;
import com.nimbusds.jose.util.Base64URL;
import com.stripe.android.stripe3ds2.exceptions.SDKRuntimeException;
import java.security.Provider;
import java.security.interfaces.ECPrivateKey;
import java.security.interfaces.ECPublicKey;
import javax.crypto.SecretKey;
import kotlin.Result;
import kotlin.ResultKt;
import kotlin.jvm.internal.Intrinsics;

public final class m implements b {
    public SecretKey a(ECPublicKey eCPublicKey, ECPrivateKey eCPrivateKey, String str) {
        Object obj;
        Intrinsics.checkParameterIsNotNull(eCPublicKey, "acsPublicKey");
        Intrinsics.checkParameterIsNotNull(eCPrivateKey, "sdkPrivateKey");
        Intrinsics.checkParameterIsNotNull(str, "agreementInfo");
        try {
            Result.Companion companion = Result.Companion;
            obj = Result.m4constructorimpl(new ConcatKDF("SHA-256").deriveKey(ECDH.deriveSharedSecret(eCPublicKey, eCPrivateKey, (Provider) null), 256, ConcatKDF.encodeStringData((String) null), ConcatKDF.encodeDataWithLength((Base64URL) null), ConcatKDF.encodeDataWithLength(Base64URL.encode(str)), ConcatKDF.encodeIntData(256), ConcatKDF.encodeNoData()));
        } catch (Throwable th) {
            Result.Companion companion2 = Result.Companion;
            obj = Result.m4constructorimpl(ResultKt.createFailure(th));
        }
        Throwable r11 = Result.m7exceptionOrNullimpl(obj);
        if (r11 == null) {
            Intrinsics.checkExpressionValueIsNotNull(obj, "runCatching {\n          …eException(it))\n        }");
            return (SecretKey) obj;
        }
        throw new SDKRuntimeException(new RuntimeException(r11));
    }
}
