package a.a.a.a.c;

import com.nimbusds.jose.EncryptionMethod;
import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.JWEAlgorithm;
import com.nimbusds.jose.JWECryptoParts;
import com.nimbusds.jose.JWEHeader;
import com.nimbusds.jose.KeyLengthException;
import com.nimbusds.jose.crypto.DirectEncrypter;
import com.nimbusds.jose.crypto.impl.AAD;
import com.nimbusds.jose.crypto.impl.AESCBC;
import com.nimbusds.jose.crypto.impl.AESGCM;
import com.nimbusds.jose.crypto.impl.AlgorithmSupportMessage;
import com.nimbusds.jose.crypto.impl.AuthenticatedCipherText;
import com.nimbusds.jose.crypto.impl.DeflateHelper;
import com.nimbusds.jose.crypto.impl.DirectCryptoProvider;
import com.nimbusds.jose.jca.JWEJCAContext;
import com.nimbusds.jose.util.Base64URL;
import com.nimbusds.jose.util.ByteUtils;
import com.nimbusds.jose.util.Container;
import java.security.Provider;
import java.util.Arrays;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import kotlin.jvm.internal.Intrinsics;

public final class o extends DirectEncrypter {

    /* renamed from: a  reason: collision with root package name */
    public final byte f34a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public o(byte[] bArr, byte b) throws KeyLengthException {
        super((SecretKey) new SecretKeySpec(bArr, "AES"));
        Intrinsics.checkParameterIsNotNull(bArr, "key");
        this.f34a = b;
    }

    public JWECryptoParts encrypt(JWEHeader jWEHeader, byte[] bArr) throws JOSEException {
        AuthenticatedCipherText authenticatedCipherText;
        byte[] bArr2;
        Intrinsics.checkParameterIsNotNull(jWEHeader, "header");
        Intrinsics.checkParameterIsNotNull(bArr, "clearText");
        JWEAlgorithm algorithm = jWEHeader.getAlgorithm();
        if (!(!Intrinsics.areEqual((Object) algorithm, (Object) JWEAlgorithm.DIR))) {
            EncryptionMethod encryptionMethod = jWEHeader.getEncryptionMethod();
            int cekBitLength = encryptionMethod.cekBitLength();
            SecretKey key = getKey();
            Intrinsics.checkExpressionValueIsNotNull(key, "key");
            if (cekBitLength == ByteUtils.bitLength(key.getEncoded())) {
                int cekBitLength2 = encryptionMethod.cekBitLength();
                SecretKey key2 = getKey();
                Intrinsics.checkExpressionValueIsNotNull(key2, "key");
                if (cekBitLength2 == ByteUtils.bitLength(key2.getEncoded())) {
                    byte[] applyCompression = DeflateHelper.applyCompression(jWEHeader, bArr);
                    byte[] compute = AAD.compute(jWEHeader);
                    if (Intrinsics.areEqual((Object) jWEHeader.getEncryptionMethod(), (Object) EncryptionMethod.A128CBC_HS256)) {
                        byte b = this.f34a;
                        bArr2 = new byte[16];
                        Arrays.fill(bArr2, (byte) 0);
                        bArr2[15] = b;
                        SecretKey key3 = getKey();
                        JWEJCAContext jCAContext = getJCAContext();
                        Intrinsics.checkExpressionValueIsNotNull(jCAContext, "jcaContext");
                        Provider contentEncryptionProvider = jCAContext.getContentEncryptionProvider();
                        JWEJCAContext jCAContext2 = getJCAContext();
                        Intrinsics.checkExpressionValueIsNotNull(jCAContext2, "jcaContext");
                        authenticatedCipherText = AESCBC.encryptAuthenticated(key3, bArr2, applyCompression, compute, contentEncryptionProvider, jCAContext2.getMACProvider());
                        Intrinsics.checkExpressionValueIsNotNull(authenticatedCipherText, "AESCBC.encryptAuthentica…  jcaContext.macProvider)");
                    } else if (Intrinsics.areEqual((Object) jWEHeader.getEncryptionMethod(), (Object) EncryptionMethod.A128GCM)) {
                        byte b2 = this.f34a;
                        bArr2 = new byte[12];
                        Arrays.fill(bArr2, (byte) 0);
                        bArr2[11] = b2;
                        authenticatedCipherText = AESGCM.encrypt(getKey(), new Container(bArr2), applyCompression, compute, (Provider) null);
                        Intrinsics.checkExpressionValueIsNotNull(authenticatedCipherText, "AESGCM.encrypt(key, Cont…v), plainText, aad, null)");
                    } else {
                        throw new JOSEException(AlgorithmSupportMessage.unsupportedEncryptionMethod(jWEHeader.getEncryptionMethod(), DirectCryptoProvider.SUPPORTED_ENCRYPTION_METHODS));
                    }
                    return new JWECryptoParts(jWEHeader, (Base64URL) null, Base64URL.encode(bArr2), Base64URL.encode(authenticatedCipherText.getCipherText()), Base64URL.encode(authenticatedCipherText.getAuthenticationTag()));
                }
                throw new KeyLengthException("The Content Encryption Key length for " + encryptionMethod + " must be " + encryptionMethod.cekBitLength() + " bits");
            }
            throw new KeyLengthException(encryptionMethod.cekBitLength(), encryptionMethod);
        }
        throw new JOSEException("Invalid algorithm " + algorithm);
    }
}
