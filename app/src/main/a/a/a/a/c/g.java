package a.a.a.a.c;

import com.facebook.share.internal.MessengerShareContentUtility;
import com.nimbusds.jose.EncryptionMethod;
import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.JWEAlgorithm;
import com.nimbusds.jose.JWEHeader;
import com.nimbusds.jose.JWEObject;
import com.nimbusds.jose.Payload;
import com.nimbusds.jose.crypto.DirectEncrypter;
import com.nimbusds.jose.crypto.RSAEncrypter;
import com.nimbusds.jose.jwk.Curve;
import com.nimbusds.jose.jwk.ECKey;
import com.nimbusds.jwt.JWTClaimsSet;
import com.stripe.android.stripe3ds2.exceptions.SDKRuntimeException;
import java.security.KeyPair;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.interfaces.ECPrivateKey;
import java.security.interfaces.ECPublicKey;
import java.security.interfaces.RSAPublicKey;
import java.text.ParseException;
import javax.crypto.SecretKey;
import kotlin.TypeCastException;
import kotlin.jvm.internal.Intrinsics;

public final class g {

    /* renamed from: a  reason: collision with root package name */
    public final h f30a;
    public final f b;

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public g(e eVar) {
        this(new h(), new f(eVar));
        Intrinsics.checkParameterIsNotNull(eVar, "ephemeralKeyPairGenerator");
    }

    public g(h hVar, f fVar) {
        this.f30a = hVar;
        this.b = fVar;
    }

    public final String a(String str, PublicKey publicKey, String str2, String str3) throws JOSEException, ParseException {
        Intrinsics.checkParameterIsNotNull(str, MessengerShareContentUtility.ATTACHMENT_PAYLOAD);
        Intrinsics.checkParameterIsNotNull(publicKey, "acsPublicKey");
        Intrinsics.checkParameterIsNotNull(str2, "directoryServerId");
        if (publicKey instanceof RSAPublicKey) {
            RSAPublicKey rSAPublicKey = (RSAPublicKey) publicKey;
            if (this.f30a != null) {
                Intrinsics.checkParameterIsNotNull(str, MessengerShareContentUtility.ATTACHMENT_PAYLOAD);
                Intrinsics.checkParameterIsNotNull(rSAPublicKey, "publicKey");
                Intrinsics.checkParameterIsNotNull(str, MessengerShareContentUtility.ATTACHMENT_PAYLOAD);
                JWEObject jWEObject = new JWEObject(new JWEHeader.Builder(JWEAlgorithm.RSA_OAEP_256, EncryptionMethod.A128CBC_HS256).keyID(str3).build(), new Payload(str));
                jWEObject.encrypt(new RSAEncrypter(rSAPublicKey));
                String serialize = jWEObject.serialize();
                Intrinsics.checkExpressionValueIsNotNull(serialize, "jwe.serialize()");
                return serialize;
            }
            throw null;
        } else if (publicKey instanceof ECPublicKey) {
            f fVar = this.b;
            ECPublicKey eCPublicKey = (ECPublicKey) publicKey;
            if (fVar != null) {
                Intrinsics.checkParameterIsNotNull(str, MessengerShareContentUtility.ATTACHMENT_PAYLOAD);
                Intrinsics.checkParameterIsNotNull(eCPublicKey, "acsPublicKey");
                Intrinsics.checkParameterIsNotNull(str2, "directoryServerId");
                JWTClaimsSet.parse(str);
                KeyPair a2 = fVar.f29a.a();
                b bVar = fVar.b;
                PrivateKey privateKey = a2.getPrivate();
                if (privateKey != null) {
                    SecretKey a3 = bVar.a(eCPublicKey, (ECPrivateKey) privateKey, str2);
                    Curve curve = Curve.P_256;
                    PublicKey publicKey2 = a2.getPublic();
                    if (publicKey2 != null) {
                        JWEObject jWEObject2 = new JWEObject(new JWEHeader.Builder(JWEAlgorithm.DIR, EncryptionMethod.A128CBC_HS256).ephemeralPublicKey(ECKey.parse(new ECKey.Builder(curve, (ECPublicKey) publicKey2).build().toJSONString())).build(), new Payload(str));
                        jWEObject2.encrypt(new DirectEncrypter(a3));
                        String serialize2 = jWEObject2.serialize();
                        Intrinsics.checkExpressionValueIsNotNull(serialize2, "jweObject.serialize()");
                        return serialize2;
                    }
                    throw new TypeCastException("null cannot be cast to non-null type java.security.interfaces.ECPublicKey");
                }
                throw new TypeCastException("null cannot be cast to non-null type java.security.interfaces.ECPrivateKey");
            }
            throw null;
        } else {
            throw new SDKRuntimeException(new RuntimeException("Unsupported public key algorithm: " + publicKey.getAlgorithm()));
        }
    }
}
