package a.a.a.a.c;

import a.a.a.a.e.b;
import a.a.a.a.e.d;
import com.nimbusds.jose.EncryptionMethod;
import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.JWEAlgorithm;
import com.nimbusds.jose.JWEHeader;
import com.nimbusds.jose.JWEObject;
import com.nimbusds.jose.Payload;
import com.nimbusds.jose.crypto.DirectDecrypter;
import java.text.ParseException;
import java.util.Arrays;
import java.util.Locale;
import javax.crypto.SecretKey;
import kotlin.Metadata;
import kotlin.Result;
import kotlin.ResultKt;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.StringCompanionObject;
import org.json.JSONException;
import org.json.JSONObject;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000Z\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0005\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0012\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0004\b\b\u0018\u0000 +2\u00020\u0001:\u0001+B\u001f\b\u0001\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0005¢\u0006\u0002\u0010\u0007J\t\u0010\b\u001a\u00020\u0003HÂ\u0003J\t\u0010\t\u001a\u00020\u0005HÂ\u0003J\t\u0010\n\u001a\u00020\u0005HÂ\u0003J'\u0010\u000b\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u0005HÆ\u0001J\u0015\u0010\f\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000fH\u0001¢\u0006\u0002\b\u0010J\u0018\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u000f2\u0006\u0010\u0014\u001a\u00020\u0015H\u0016J\u001d\u0010\u0016\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u000f2\u0006\u0010\u0014\u001a\u00020\u0015H\u0001¢\u0006\u0002\b\u0017J\u0018\u0010\u0018\u001a\u00020\u000f2\u0006\u0010\u0019\u001a\u00020\u00122\u0006\u0010\u0014\u001a\u00020\u0015H\u0016J\u0013\u0010\u001a\u001a\u00020\u00032\b\u0010\u001b\u001a\u0004\u0018\u00010\u001cHÖ\u0003J\u001d\u0010\u001d\u001a\u00020\u001e2\u0006\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u001f\u001a\u00020 H\u0001¢\u0006\u0002\b!J\u001d\u0010\"\u001a\u00020\u001e2\u0006\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u001f\u001a\u00020 H\u0001¢\u0006\u0002\b#J\t\u0010$\u001a\u00020%HÖ\u0001J\t\u0010&\u001a\u00020\u000fHÖ\u0001J\u0015\u0010'\u001a\u00020(2\u0006\u0010)\u001a\u00020\u0012H\u0001¢\u0006\u0002\b*R\u000e\u0010\u0006\u001a\u00020\u0005X\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006,"}, d2 = {"Lcom/stripe/android/stripe3ds2/security/MessageTransformerImpl;", "Lcom/stripe/android/stripe3ds2/security/MessageTransformer;", "isLiveMode", "", "counterSdkToAcs", "", "counterAcsToSdk", "(ZBB)V", "component1", "component2", "component3", "copy", "createEncryptionHeader", "Lcom/nimbusds/jose/JWEHeader;", "keyId", "", "createEncryptionHeader$3ds2sdk_release", "decrypt", "Lorg/json/JSONObject;", "message", "secretKey", "Ljavax/crypto/SecretKey;", "decryptMessage", "decryptMessage$3ds2sdk_release", "encrypt", "challengeRequest", "equals", "other", "", "getDecryptionKey", "", "encryptionMethod", "Lcom/nimbusds/jose/EncryptionMethod;", "getDecryptionKey$3ds2sdk_release", "getEncryptionKey", "getEncryptionKey$3ds2sdk_release", "hashCode", "", "toString", "validateAcsToSdkCounter", "", "cres", "validateAcsToSdkCounter$3ds2sdk_release", "Companion", "3ds2sdk_release"}, k = 1, mv = {1, 1, 16})
public final class k implements i {
    public static final EncryptionMethod d = EncryptionMethod.A128CBC_HS256;
    public static final a e = new a();

    /* renamed from: a  reason: collision with root package name */
    public final boolean f31a;
    public byte b;
    public byte c;

    public static final class a {
    }

    public k(boolean z, byte b2, byte b3) {
        this.f31a = z;
        this.b = b2;
        this.c = b3;
    }

    public JSONObject a(String str, SecretKey secretKey) throws ParseException, JOSEException, JSONException, b {
        Object obj;
        Intrinsics.checkParameterIsNotNull(str, "message");
        Intrinsics.checkParameterIsNotNull(secretKey, "secretKey");
        Intrinsics.checkParameterIsNotNull(str, "message");
        Intrinsics.checkParameterIsNotNull(secretKey, "secretKey");
        JWEObject parse = JWEObject.parse(str);
        Intrinsics.checkExpressionValueIsNotNull(parse, "jweObject");
        JWEHeader header = parse.getHeader();
        Intrinsics.checkExpressionValueIsNotNull(header, "jweObject.header");
        EncryptionMethod encryptionMethod = header.getEncryptionMethod();
        Intrinsics.checkExpressionValueIsNotNull(encryptionMethod, "jweObject.header.encryptionMethod");
        Intrinsics.checkParameterIsNotNull(secretKey, "secretKey");
        Intrinsics.checkParameterIsNotNull(encryptionMethod, "encryptionMethod");
        byte[] encoded = secretKey.getEncoded();
        EncryptionMethod encryptionMethod2 = EncryptionMethod.A128GCM;
        if (encryptionMethod2 == encryptionMethod) {
            encoded = Arrays.copyOfRange(encoded, encoded.length - (encryptionMethod2.cekBitLength() / 8), encoded.length);
            Intrinsics.checkExpressionValueIsNotNull(encoded, "Arrays.copyOfRange(encod…         encodedKey.size)");
        } else {
            Intrinsics.checkExpressionValueIsNotNull(encoded, "encodedKey");
        }
        parse.decrypt(new DirectDecrypter(encoded));
        JSONObject jSONObject = new JSONObject(parse.getPayload().toString());
        Intrinsics.checkParameterIsNotNull(jSONObject, "cres");
        if (this.f31a) {
            if (jSONObject.has("acsCounterAtoS")) {
                try {
                    Result.Companion companion = Result.Companion;
                    String string = jSONObject.getString("acsCounterAtoS");
                    Intrinsics.checkExpressionValueIsNotNull(string, "cres.getString(FIELD_ACS_COUNTER_ACS_TO_SDK)");
                    obj = Result.m4constructorimpl(Byte.valueOf(Byte.parseByte(string)));
                } catch (Throwable th) {
                    Result.Companion companion2 = Result.Companion;
                    obj = Result.m4constructorimpl(ResultKt.createFailure(th));
                }
                if (Result.m7exceptionOrNullimpl(obj) == null) {
                    byte byteValue = ((Number) obj).byteValue();
                    if (this.c != byteValue) {
                        d dVar = d.DataDecryptionFailure;
                        String str2 = "Counters are not equal. SDK counter: " + this.c + ", " + "ACS counter: " + byteValue;
                        Intrinsics.checkParameterIsNotNull(dVar, "protocolError");
                        Intrinsics.checkParameterIsNotNull(str2, "detail");
                        throw new b(dVar.a(), dVar.b(), str2);
                    }
                } else {
                    throw b.d.a("acsCounterAtoS");
                }
            } else {
                throw b.d.b("acsCounterAtoS");
            }
        }
        byte b2 = (byte) (this.c + 1);
        this.c = b2;
        if (b2 != 0) {
            return jSONObject;
        }
        throw new RuntimeException("ACS to SDK counter is zero");
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof k)) {
            return false;
        }
        k kVar = (k) obj;
        return this.f31a == kVar.f31a && this.b == kVar.b && this.c == kVar.c;
    }

    public int hashCode() {
        boolean z = this.f31a;
        if (z) {
            z = true;
        }
        return ((((z ? 1 : 0) * true) + this.b) * 31) + this.c;
    }

    public String toString() {
        return "MessageTransformerImpl(isLiveMode=" + this.f31a + ", counterSdkToAcs=" + this.b + ", counterAcsToSdk=" + this.c + ")";
    }

    public String a(JSONObject jSONObject, SecretKey secretKey) throws JOSEException, JSONException {
        Intrinsics.checkParameterIsNotNull(jSONObject, "challengeRequest");
        Intrinsics.checkParameterIsNotNull(secretKey, "secretKey");
        String string = jSONObject.getString("acsTransID");
        Intrinsics.checkExpressionValueIsNotNull(string, "challengeRequest.getStri…tData.FIELD_ACS_TRANS_ID)");
        Intrinsics.checkParameterIsNotNull(string, "keyId");
        JWEHeader build = new JWEHeader.Builder(JWEAlgorithm.DIR, d).keyID(string).build();
        Intrinsics.checkExpressionValueIsNotNull(build, "JWEHeader.Builder(JWEAlg…yId)\n            .build()");
        StringCompanionObject stringCompanionObject = StringCompanionObject.INSTANCE;
        Locale locale = Locale.ROOT;
        Intrinsics.checkExpressionValueIsNotNull(locale, "Locale.ROOT");
        String format = String.format(locale, "%03d", Arrays.copyOf(new Object[]{Byte.valueOf(this.b)}, 1));
        Intrinsics.checkExpressionValueIsNotNull(format, "java.lang.String.format(locale, format, *args)");
        jSONObject.put("sdkCounterStoA", format);
        JWEObject jWEObject = new JWEObject(build, new Payload(jSONObject.toString()));
        EncryptionMethod encryptionMethod = build.getEncryptionMethod();
        Intrinsics.checkExpressionValueIsNotNull(encryptionMethod, "header.encryptionMethod");
        Intrinsics.checkParameterIsNotNull(secretKey, "secretKey");
        Intrinsics.checkParameterIsNotNull(encryptionMethod, "encryptionMethod");
        byte[] encoded = secretKey.getEncoded();
        EncryptionMethod encryptionMethod2 = EncryptionMethod.A128GCM;
        if (encryptionMethod2 == encryptionMethod) {
            encoded = Arrays.copyOfRange(encoded, 0, encryptionMethod2.cekBitLength() / 8);
            Intrinsics.checkExpressionValueIsNotNull(encoded, "Arrays.copyOfRange(encod…tLength() / BITS_IN_BYTE)");
        } else {
            Intrinsics.checkExpressionValueIsNotNull(encoded, "encodedKey");
        }
        jWEObject.encrypt(new o(encoded, this.b));
        byte b2 = (byte) (this.b + 1);
        this.b = b2;
        if (b2 != 0) {
            String serialize = jWEObject.serialize();
            Intrinsics.checkExpressionValueIsNotNull(serialize, "jweObject.serialize()");
            return serialize;
        }
        throw new RuntimeException("SDK to ACS counter is zero");
    }
}
