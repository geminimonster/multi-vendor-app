package a.a.a.a.c;

import a.a.a.a.e.b;
import com.nimbusds.jose.JOSEException;
import java.io.Serializable;
import java.text.ParseException;
import javax.crypto.SecretKey;
import org.json.JSONException;
import org.json.JSONObject;

public interface i extends Serializable {
    String a(JSONObject jSONObject, SecretKey secretKey) throws JSONException, JOSEException;

    JSONObject a(String str, SecretKey secretKey) throws ParseException, JOSEException, JSONException, b;
}
