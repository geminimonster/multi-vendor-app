package a.a.a.a.c;

import com.nimbusds.jose.jwk.Curve;
import com.stripe.android.stripe3ds2.exceptions.SDKRuntimeException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.spec.ECGenParameterSpec;
import kotlin.Result;
import kotlin.ResultKt;
import kotlin.jvm.internal.Intrinsics;

public final class n implements e {

    /* renamed from: a  reason: collision with root package name */
    public static final String f33a = a.EC.f26a;

    public KeyPair a() {
        Object obj;
        try {
            Result.Companion companion = Result.Companion;
            KeyPairGenerator instance = KeyPairGenerator.getInstance(f33a);
            Curve curve = Curve.P_256;
            Intrinsics.checkExpressionValueIsNotNull(curve, "Curve.P_256");
            instance.initialize(new ECGenParameterSpec(curve.getStdName()));
            obj = Result.m4constructorimpl(instance.generateKeyPair());
        } catch (Throwable th) {
            Result.Companion companion2 = Result.Companion;
            obj = Result.m4constructorimpl(ResultKt.createFailure(th));
        }
        Throwable r1 = Result.m7exceptionOrNullimpl(obj);
        if (r1 == null) {
            Intrinsics.checkExpressionValueIsNotNull(obj, "runCatching {\n          …eException(it))\n        }");
            return (KeyPair) obj;
        }
        throw new SDKRuntimeException(new RuntimeException(r1));
    }
}
