package a.a.a.a.f;

import kotlin.text.StringsKt;

public final class c {

    /* renamed from: a  reason: collision with root package name */
    public static final c f89a = new c();

    public final boolean a(String str) {
        return str == null || StringsKt.isBlank(str);
    }
}
