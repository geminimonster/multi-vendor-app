package a.a.a.a.f;

import android.graphics.Bitmap;
import android.util.LruCache;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0004\b`\u0018\u00002\u00020\u0001:\u0001\nJ\b\u0010\u0002\u001a\u00020\u0003H&J\u0013\u0010\u0004\u001a\u0004\u0018\u00010\u00052\u0006\u0010\u0006\u001a\u00020\u0007H¦\u0002J\u0019\u0010\b\u001a\u00020\u00032\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u0010\t\u001a\u00020\u0005H¦\u0002¨\u0006\u000b"}, d2 = {"Lcom/stripe/android/stripe3ds2/utils/ImageCache;", "", "clear", "", "get", "Landroid/graphics/Bitmap;", "key", "", "set", "bitmap", "Default", "3ds2sdk_release"}, k = 1, mv = {1, 1, 16})
public interface b {

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\u0005\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\b\u0010\u000e\u001a\u00020\u000fH\u0016J\u0013\u0010\u0010\u001a\u0004\u0018\u00010\t2\u0006\u0010\u0011\u001a\u00020\bH\u0002J\u0019\u0010\u0012\u001a\u00020\u000f2\u0006\u0010\u0011\u001a\u00020\b2\u0006\u0010\u0013\u001a\u00020\tH\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R(\u0010\u0006\u001a\u000e\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\t0\u00078\u0000X\u0004¢\u0006\u000e\n\u0000\u0012\u0004\b\n\u0010\u0002\u001a\u0004\b\u000b\u0010\fR\u000e\u0010\r\u001a\u00020\u0004X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0014"}, d2 = {"Lcom/stripe/android/stripe3ds2/utils/ImageCache$Default;", "Lcom/stripe/android/stripe3ds2/utils/ImageCache;", "()V", "KB", "", "MAX_SIZE", "cache", "Landroid/util/LruCache;", "", "Landroid/graphics/Bitmap;", "cache$annotations", "getCache$3ds2sdk_release", "()Landroid/util/LruCache;", "cacheSize", "clear", "", "get", "key", "set", "bitmap", "3ds2sdk_release"}, k = 1, mv = {1, 1, 16})
    public static final class a implements b {

        /* renamed from: a  reason: collision with root package name */
        public static final int f88a = Math.min((int) ((Runtime.getRuntime().maxMemory() / ((long) 1024)) / ((long) 8)), 10240);
        public static final LruCache<String, Bitmap> b;
        public static final a c;

        /* renamed from: a.a.a.a.f.b$a$a  reason: collision with other inner class name */
        public static final class C0013a extends LruCache<String, Bitmap> {
            public C0013a(a aVar, int i) {
                super(i);
            }

            public int sizeOf(Object obj, Object obj2) {
                Bitmap bitmap = (Bitmap) obj2;
                Intrinsics.checkParameterIsNotNull((String) obj, "key");
                Intrinsics.checkParameterIsNotNull(bitmap, "bitmap");
                return bitmap.getByteCount() / 1024;
            }
        }

        static {
            a aVar = new a();
            c = aVar;
            b = new C0013a(aVar, f88a);
        }

        public Bitmap a(String str) {
            Intrinsics.checkParameterIsNotNull(str, "key");
            return b.get(str);
        }

        public void a() {
            b.evictAll();
        }

        public void a(String str, Bitmap bitmap) {
            Intrinsics.checkParameterIsNotNull(str, "key");
            Intrinsics.checkParameterIsNotNull(bitmap, "bitmap");
            b.put(str, bitmap);
        }
    }

    Bitmap a(String str);

    void a();

    void a(String str, Bitmap bitmap);
}
