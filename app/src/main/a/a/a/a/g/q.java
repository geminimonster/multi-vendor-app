package a.a.a.a.g;

import android.app.ProgressDialog;
import android.content.Context;
import com.stripe.android.stripe3ds2.exceptions.SDKRuntimeException;
import com.stripe.android.stripe3ds2.init.ui.UiCustomization;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\bf\u0018\u00002\u00020\u0001:\u0001\nJ \u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\tH&¨\u0006\u000b"}, d2 = {"Lcom/stripe/android/stripe3ds2/views/ProgressViewFactory;", "", "create", "Landroid/app/ProgressDialog;", "context", "Landroid/content/Context;", "brand", "Lcom/stripe/android/stripe3ds2/views/ProgressViewFactory$Brand;", "uiCustomization", "Lcom/stripe/android/stripe3ds2/init/ui/UiCustomization;", "Brand", "3ds2sdk_release"}, k = 1, mv = {1, 1, 16})
public interface q {

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0002\b\r\b\u0001\u0018\u0000 \u00112\b\u0012\u0004\u0012\u00020\u00000\u0001:\u0001\u0011B#\b\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0001\u0010\u0004\u001a\u00020\u0005\u0012\b\b\u0001\u0010\u0006\u001a\u00020\u0005¢\u0006\u0002\u0010\u0007R\u0014\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u0014\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u0014\u0010\u0006\u001a\u00020\u0005X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\u000bj\u0002\b\rj\u0002\b\u000ej\u0002\b\u000fj\u0002\b\u0010¨\u0006\u0012"}, d2 = {"Lcom/stripe/android/stripe3ds2/views/ProgressViewFactory$Brand;", "", "directoryServerName", "", "drawableResId", "", "nameResId", "(Ljava/lang/String;ILjava/lang/String;II)V", "getDirectoryServerName$3ds2sdk_release", "()Ljava/lang/String;", "getDrawableResId$3ds2sdk_release", "()I", "getNameResId$3ds2sdk_release", "VISA", "MASTERCARD", "AMEX", "DISCOVER", "Companion", "3ds2sdk_release"}, k = 1, mv = {1, 1, 16})
    public enum a {
        ;
        
        public static final C0017a e = null;

        /* renamed from: a  reason: collision with root package name */
        public final String f113a;
        public final int b;
        public final int c;

        /* renamed from: a.a.a.a.g.q$a$a  reason: collision with other inner class name */
        public static final class C0017a {
            public final a a(String str) throws SDKRuntimeException {
                a aVar;
                a[] values = a.values();
                int length = values.length;
                int i = 0;
                while (true) {
                    if (i >= length) {
                        aVar = null;
                        break;
                    }
                    aVar = values[i];
                    if (Intrinsics.areEqual((Object) aVar.f113a, (Object) str)) {
                        break;
                    }
                    i++;
                }
                if (aVar != null) {
                    return aVar;
                }
                SDKRuntimeException.Companion companion = SDKRuntimeException.Companion;
                throw companion.create("Directory server name " + str + " is not supported");
            }
        }

        /* access modifiers changed from: public */
        static {
            e = new C0017a();
        }

        /* access modifiers changed from: public */
        a(String str, int i, int i2) {
            this.f113a = str;
            this.b = i;
            this.c = i2;
        }
    }

    ProgressDialog a(Context context, a aVar, UiCustomization uiCustomization);
}
