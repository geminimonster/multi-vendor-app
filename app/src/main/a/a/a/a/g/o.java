package a.a.a.a.g;

import android.graphics.Bitmap;
import androidx.lifecycle.MutableLiveData;
import kotlin.Result;
import kotlin.ResultKt;
import kotlin.Unit;
import kotlin.coroutines.Continuation;
import kotlin.coroutines.intrinsics.IntrinsicsKt;
import kotlin.coroutines.jvm.internal.DebugMetadata;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.Intrinsics;
import kotlinx.coroutines.CoroutineScope;

@DebugMetadata(c = "com.stripe.android.stripe3ds2.views.ImageRepository$getImage$1", f = "ImageRepository.kt", i = {0}, l = {29}, m = "invokeSuspend", n = {"$this$launch"}, s = {"L$0"})
public final class o extends SuspendLambda implements Function2<CoroutineScope, Continuation<? super Unit>, Object> {

    /* renamed from: a  reason: collision with root package name */
    public CoroutineScope f111a;
    public Object b;
    public int c;
    public final /* synthetic */ n d;
    public final /* synthetic */ String e;
    public final /* synthetic */ MutableLiveData f;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public o(n nVar, String str, MutableLiveData mutableLiveData, Continuation continuation) {
        super(2, continuation);
        this.d = nVar;
        this.e = str;
        this.f = mutableLiveData;
    }

    public final Continuation<Unit> create(Object obj, Continuation<?> continuation) {
        Intrinsics.checkParameterIsNotNull(continuation, "completion");
        o oVar = new o(this.d, this.e, this.f, continuation);
        oVar.f111a = (CoroutineScope) obj;
        return oVar;
    }

    public final Object invoke(Object obj, Object obj2) {
        return ((o) create(obj, (Continuation) obj2)).invokeSuspend(Unit.INSTANCE);
    }

    public final Object invokeSuspend(Object obj) {
        Object coroutine_suspended = IntrinsicsKt.getCOROUTINE_SUSPENDED();
        int i = this.c;
        if (i == 0) {
            ResultKt.throwOnFailure(obj);
            CoroutineScope coroutineScope = this.f111a;
            n nVar = this.d;
            String str = this.e;
            this.b = coroutineScope;
            this.c = 1;
            obj = nVar.c.a(str, this);
            if (obj == coroutine_suspended) {
                return coroutine_suspended;
            }
        } else if (i == 1) {
            CoroutineScope coroutineScope2 = (CoroutineScope) this.b;
            ResultKt.throwOnFailure(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        Bitmap bitmap = (Bitmap) obj;
        n nVar2 = this.d;
        String str2 = this.e;
        if (nVar2 != null) {
            if (bitmap != null) {
                nVar2.b.a(str2, bitmap);
            }
            if (bitmap != null) {
                MutableLiveData mutableLiveData = this.f;
                Result.Companion companion = Result.Companion;
                mutableLiveData.postValue(Result.m3boximpl(Result.m4constructorimpl(bitmap)));
            } else {
                MutableLiveData mutableLiveData2 = this.f;
                Result.Companion companion2 = Result.Companion;
                mutableLiveData2.postValue(Result.m3boximpl(Result.m4constructorimpl(ResultKt.createFailure(new RuntimeException("Could not retrieve remote image")))));
            }
            return Unit.INSTANCE;
        }
        throw null;
    }
}
