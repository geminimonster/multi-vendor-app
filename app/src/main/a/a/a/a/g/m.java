package a.a.a.a.g;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.util.AttributeSet;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.ContextThemeWrapper;
import com.stripe.android.stripe3ds2.R;
import com.stripe.android.stripe3ds2.init.ui.ButtonCustomization;
import com.stripe.android.stripe3ds2.init.ui.ToolbarCustomization;
import com.stripe.android.stripe3ds2.utils.CustomizeUtils;
import com.stripe.android.stripe3ds2.views.ThreeDS2Button;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import kotlin.text.StringsKt;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0000\u0018\u0000 \u000b2\u00020\u0001:\u0001\u000bB\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J \u0010\u0005\u001a\u0004\u0018\u00010\u00062\n\b\u0002\u0010\u0007\u001a\u0004\u0018\u00010\b2\n\b\u0002\u0010\t\u001a\u0004\u0018\u00010\nR\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\f"}, d2 = {"Lcom/stripe/android/stripe3ds2/views/HeaderZoneCustomizer;", "", "activity", "Landroidx/appcompat/app/AppCompatActivity;", "(Landroidx/appcompat/app/AppCompatActivity;)V", "customize", "Lcom/stripe/android/stripe3ds2/views/ThreeDS2Button;", "toolbarCustomization", "Lcom/stripe/android/stripe3ds2/init/ui/ToolbarCustomization;", "cancelButtonCustomization", "Lcom/stripe/android/stripe3ds2/init/ui/ButtonCustomization;", "Companion", "3ds2sdk_release"}, k = 1, mv = {1, 1, 16})
public final class m {
    public static final a b = new a();

    /* renamed from: a  reason: collision with root package name */
    public final AppCompatActivity f109a;

    public static final class a {
        public final void a(AppCompatActivity appCompatActivity, ToolbarCustomization toolbarCustomization) {
            CustomizeUtils customizeUtils;
            int darken$3ds2sdk_release;
            Intrinsics.checkParameterIsNotNull(appCompatActivity, "activity");
            Intrinsics.checkParameterIsNotNull(toolbarCustomization, "toolbarCustomization");
            if (toolbarCustomization.getStatusBarColor() != null) {
                customizeUtils = CustomizeUtils.INSTANCE;
                darken$3ds2sdk_release = Color.parseColor(toolbarCustomization.getStatusBarColor());
            } else if (toolbarCustomization.getBackgroundColor() != null) {
                int parseColor = Color.parseColor(toolbarCustomization.getBackgroundColor());
                customizeUtils = CustomizeUtils.INSTANCE;
                darken$3ds2sdk_release = customizeUtils.darken$3ds2sdk_release(parseColor);
            } else {
                return;
            }
            customizeUtils.setStatusBarColor(appCompatActivity, darken$3ds2sdk_release);
        }
    }

    public m(AppCompatActivity appCompatActivity) {
        Intrinsics.checkParameterIsNotNull(appCompatActivity, "activity");
        this.f109a = appCompatActivity;
    }

    public final ThreeDS2Button a(ToolbarCustomization toolbarCustomization, ButtonCustomization buttonCustomization) {
        String str;
        String str2;
        ActionBar supportActionBar = this.f109a.getSupportActionBar();
        if (supportActionBar == null) {
            return null;
        }
        Intrinsics.checkExpressionValueIsNotNull(supportActionBar, "activity.supportActionBar ?: return null");
        ThreeDS2Button threeDS2Button = new ThreeDS2Button(new ContextThemeWrapper((Context) this.f109a, R.style.Stripe3DS2ActionBarButton), (AttributeSet) null, 0, 6, (DefaultConstructorMarker) null);
        boolean z = false;
        threeDS2Button.setBackgroundTintList(ColorStateList.valueOf(0));
        threeDS2Button.setButtonCustomization(buttonCustomization);
        supportActionBar.setCustomView(threeDS2Button, new ActionBar.LayoutParams(-2, -2, 8388629));
        supportActionBar.setDisplayShowCustomEnabled(true);
        if (toolbarCustomization != null) {
            String buttonText = toolbarCustomization.getButtonText();
            if (!(buttonText == null || StringsKt.isBlank(buttonText))) {
                threeDS2Button.setText(toolbarCustomization.getButtonText());
            } else {
                threeDS2Button.setText(R.string.stripe_3ds2_hzv_cancel_label);
            }
            String backgroundColor = toolbarCustomization.getBackgroundColor();
            if (backgroundColor != null) {
                supportActionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor(backgroundColor)));
                b.a(this.f109a, toolbarCustomization);
            }
            String headerText = toolbarCustomization.getHeaderText();
            if (headerText == null || StringsKt.isBlank(headerText)) {
                z = true;
            }
            if (!z) {
                str = toolbarCustomization.getHeaderText();
                str2 = "toolbarCustomization.headerText";
            } else {
                str = this.f109a.getString(R.string.stripe_3ds2_hzv_header_label);
                str2 = "activity.getString(R.str…pe_3ds2_hzv_header_label)";
            }
            Intrinsics.checkExpressionValueIsNotNull(str, str2);
            supportActionBar.setTitle((CharSequence) CustomizeUtils.INSTANCE.buildStyledText(this.f109a, str, toolbarCustomization));
        } else {
            supportActionBar.setTitle(R.string.stripe_3ds2_hzv_header_label);
            threeDS2Button.setText(R.string.stripe_3ds2_hzv_cancel_label);
        }
        return threeDS2Button;
    }
}
