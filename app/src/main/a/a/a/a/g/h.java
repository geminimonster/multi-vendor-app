package a.a.a.a.g;

import a.a.a.a.a.c;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import com.stripe.android.stripe3ds2.R;
import com.stripe.android.stripe3ds2.init.ui.UiCustomization;
import com.stripe.android.stripe3ds2.utils.CustomizeUtils;
import kotlin.Lazy;
import kotlin.LazyKt;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\tB\u0015\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006¢\u0006\u0002\u0010\u0007J\b\u0010\b\u001a\u00020\u0002H\u0016R\u000e\u0010\u0003\u001a\u00020\u0004X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0004¢\u0006\u0002\n\u0000¨\u0006\n"}, d2 = {"Lcom/stripe/android/stripe3ds2/views/ChallengeSubmitDialogFactory;", "Lcom/stripe/android/stripe3ds2/utils/Factory0;", "Landroid/app/ProgressDialog;", "context", "Landroid/content/Context;", "uiCustomization", "Lcom/stripe/android/stripe3ds2/init/ui/UiCustomization;", "(Landroid/content/Context;Lcom/stripe/android/stripe3ds2/init/ui/UiCustomization;)V", "create", "ChallengeSubmitDialog", "3ds2sdk_release"}, k = 1, mv = {1, 1, 16})
public final class h implements a.a.a.a.f.a<ProgressDialog> {

    /* renamed from: a  reason: collision with root package name */
    public final Context f101a;
    public final UiCustomization b;

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\b\u0002\u0018\u00002\u00020\u0001B\u0017\b\u0000\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\b\u0010\r\u001a\u00020\u000eH\u0016R\u000e\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000R\u001b\u0010\u0007\u001a\u00020\b8BX\u0002¢\u0006\f\n\u0004\b\u000b\u0010\f\u001a\u0004\b\t\u0010\n¨\u0006\u000f"}, d2 = {"Lcom/stripe/android/stripe3ds2/views/ChallengeSubmitDialogFactory$ChallengeSubmitDialog;", "Landroid/app/ProgressDialog;", "context", "Landroid/content/Context;", "uiCustomization", "Lcom/stripe/android/stripe3ds2/init/ui/UiCustomization;", "(Landroid/content/Context;Lcom/stripe/android/stripe3ds2/init/ui/UiCustomization;)V", "viewBinding", "Lcom/stripe/android/stripe3ds2/databinding/ChallengeSubmitDialogBinding;", "getViewBinding", "()Lcom/stripe/android/stripe3ds2/databinding/ChallengeSubmitDialogBinding;", "viewBinding$delegate", "Lkotlin/Lazy;", "onStart", "", "3ds2sdk_release"}, k = 1, mv = {1, 1, 16})
    public static final class a extends ProgressDialog {

        /* renamed from: a  reason: collision with root package name */
        public final Lazy f102a = LazyKt.lazy(new C0015a(this));
        public final UiCustomization b;

        /* renamed from: a.a.a.a.g.h$a$a  reason: collision with other inner class name */
        public static final class C0015a extends Lambda implements Function0<c> {

            /* renamed from: a  reason: collision with root package name */
            public final /* synthetic */ a f103a;

            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0015a(a aVar) {
                super(0);
                this.f103a = aVar;
            }

            public Object invoke() {
                View inflate = this.f103a.getLayoutInflater().inflate(R.layout.challenge_submit_dialog, (ViewGroup) null, false);
                ProgressBar progressBar = (ProgressBar) inflate.findViewById(R.id.progress_bar);
                if (progressBar != null) {
                    return new c((FrameLayout) inflate, progressBar);
                }
                throw new NullPointerException("Missing required view with ID: ".concat("progressBar"));
            }
        }

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(Context context, UiCustomization uiCustomization) {
            super(context);
            Intrinsics.checkParameterIsNotNull(context, "context");
            Intrinsics.checkParameterIsNotNull(uiCustomization, "uiCustomization");
            this.b = uiCustomization;
            setIndeterminate(true);
            setCancelable(false);
            Window window = getWindow();
            if (window != null) {
                window.clearFlags(2);
            }
            Window window2 = getWindow();
            if (window2 != null) {
                window2.setBackgroundDrawable(new ColorDrawable(0));
            }
        }

        public void onStart() {
            super.onStart();
            setContentView(((c) this.f102a.getValue()).f8a);
            CustomizeUtils customizeUtils = CustomizeUtils.INSTANCE;
            ProgressBar progressBar = ((c) this.f102a.getValue()).b;
            Intrinsics.checkExpressionValueIsNotNull(progressBar, "viewBinding.progressBar");
            customizeUtils.applyProgressBarColor$3ds2sdk_release(progressBar, this.b);
        }
    }

    public h(Context context, UiCustomization uiCustomization) {
        Intrinsics.checkParameterIsNotNull(context, "context");
        Intrinsics.checkParameterIsNotNull(uiCustomization, "uiCustomization");
        this.f101a = context;
        this.b = uiCustomization;
    }

    public Object a() {
        return new a(this.f101a, this.b);
    }
}
