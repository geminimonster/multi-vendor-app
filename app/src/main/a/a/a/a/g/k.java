package a.a.a.a.g;

import a.a.a.a.a.f;
import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.stripe.android.stripe3ds2.init.ui.TextBoxCustomization;
import kotlin.jvm.internal.Intrinsics;

public final class k extends LinearLayout {

    /* renamed from: a  reason: collision with root package name */
    public final TextInputLayout f106a;
    public final TextInputEditText b;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public k(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        Intrinsics.checkParameterIsNotNull(context, "context");
        f a2 = f.a(LayoutInflater.from(context), this, true);
        Intrinsics.checkExpressionValueIsNotNull(a2, "ChallengeZoneTextViewBin…           true\n        )");
        TextInputLayout textInputLayout = a2.b;
        Intrinsics.checkExpressionValueIsNotNull(textInputLayout, "viewBinding.label");
        this.f106a = textInputLayout;
        TextInputEditText textInputEditText = a2.c;
        Intrinsics.checkExpressionValueIsNotNull(textInputEditText, "viewBinding.textEntry");
        this.b = textInputEditText;
    }

    public final TextInputLayout getInfoLabel$3ds2sdk_release() {
        return this.f106a;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:2:0x0008, code lost:
        r0 = r0.toString();
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.String getTextEntry$3ds2sdk_release() {
        /*
            r1 = this;
            com.google.android.material.textfield.TextInputEditText r0 = r1.b
            android.text.Editable r0 = r0.getText()
            if (r0 == 0) goto L_0x000f
            java.lang.String r0 = r0.toString()
            if (r0 == 0) goto L_0x000f
            goto L_0x0011
        L_0x000f:
            java.lang.String r0 = ""
        L_0x0011:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: a.a.a.a.g.k.getTextEntry$3ds2sdk_release():java.lang.String");
    }

    public final TextInputEditText getTextEntryView$3ds2sdk_release() {
        return this.b;
    }

    public final void setTextBoxCustomization(TextBoxCustomization textBoxCustomization) {
        if (textBoxCustomization != null) {
            String textColor = textBoxCustomization.getTextColor();
            if (textColor != null) {
                this.b.setTextColor(Color.parseColor(textColor));
            }
            Integer valueOf = Integer.valueOf(textBoxCustomization.getTextFontSize());
            if (!(valueOf.intValue() > 0)) {
                valueOf = null;
            }
            if (valueOf != null) {
                this.b.setTextSize(2, (float) valueOf.intValue());
            }
            if (textBoxCustomization.getCornerRadius() >= 0) {
                float cornerRadius = (float) textBoxCustomization.getCornerRadius();
                this.f106a.setBoxCornerRadii(cornerRadius, cornerRadius, cornerRadius, cornerRadius);
            }
            String borderColor = textBoxCustomization.getBorderColor();
            if (borderColor != null) {
                this.f106a.setBoxBackgroundMode(2);
                this.f106a.setBoxStrokeColor(Color.parseColor(borderColor));
            }
            String hintTextColor = textBoxCustomization.getHintTextColor();
            if (hintTextColor != null) {
                this.f106a.setDefaultHintTextColor(ColorStateList.valueOf(Color.parseColor(hintTextColor)));
            }
        }
    }

    public final void setTextEntry$3ds2sdk_release(String str) {
        Intrinsics.checkParameterIsNotNull(str, "text");
        this.b.setText(str);
    }

    public final void setTextEntryLabel(String str) {
        this.f106a.setHint(str);
    }
}
