package a.a.a.a.g;

import a.a.a.a.a.d;
import a.a.a.a.a.e;
import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import androidx.core.widget.CompoundButtonCompat;
import com.google.android.material.checkbox.MaterialCheckBox;
import com.google.android.material.radiobutton.MaterialRadioButton;
import com.stripe.android.stripe3ds2.R;
import com.stripe.android.stripe3ds2.init.ui.ButtonCustomization;
import com.stripe.android.stripe3ds2.init.ui.LabelCustomization;
import com.stripe.android.stripe3ds2.transactions.ChallengeResponseData;
import com.stripe.android.stripe3ds2.views.ThreeDS2TextView;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import kotlin.TypeCastException;
import kotlin.collections.CollectionsKt;
import kotlin.collections.IntIterator;
import kotlin.jvm.internal.Intrinsics;
import kotlin.ranges.IntRange;
import kotlin.ranges.RangesKt;
import kotlin.text.StringsKt;

public final class j extends FrameLayout {

    /* renamed from: a  reason: collision with root package name */
    public final ThreeDS2TextView f105a;
    public final LinearLayout b;
    public final int c;
    public final int d;
    public final int e;
    public final int f;
    public final boolean g;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public j(Context context, AttributeSet attributeSet, int i, boolean z) {
        super(context, attributeSet, i);
        LinearLayout linearLayout;
        Intrinsics.checkParameterIsNotNull(context, "context");
        this.g = z;
        if (getId() == -1) {
            setId(R.id.stripe_3ds2_default_challenge_zone_select_view_id);
        }
        this.c = context.getResources().getDimensionPixelSize(R.dimen.stripe_3ds2_challenge_zone_select_button_vertical_margin);
        this.d = context.getResources().getDimensionPixelSize(R.dimen.stripe_3ds2_challenge_zone_select_button_label_padding);
        this.e = context.getResources().getDimensionPixelSize(R.dimen.stripe_3ds2_challenge_zone_select_button_offset_margin);
        this.f = context.getResources().getDimensionPixelSize(R.dimen.stripe_3ds2_challenge_zone_select_button_min_height);
        boolean z2 = this.g;
        LayoutInflater from = LayoutInflater.from(context);
        if (z2) {
            e a2 = e.a(from, this, true);
            Intrinsics.checkExpressionValueIsNotNull(a2, "ChallengeZoneSingleSelec…   true\n                )");
            ThreeDS2TextView threeDS2TextView = a2.b;
            Intrinsics.checkExpressionValueIsNotNull(threeDS2TextView, "viewBinding.label");
            this.f105a = threeDS2TextView;
            linearLayout = a2.c;
        } else {
            d a3 = d.a(from, this, true);
            Intrinsics.checkExpressionValueIsNotNull(a3, "ChallengeZoneMultiSelect…   true\n                )");
            ThreeDS2TextView threeDS2TextView2 = a3.b;
            Intrinsics.checkExpressionValueIsNotNull(threeDS2TextView2, "viewBinding.label");
            this.f105a = threeDS2TextView2;
            linearLayout = a3.c;
        }
        Intrinsics.checkExpressionValueIsNotNull(linearLayout, "viewBinding.selectGroup");
        this.b = linearLayout;
    }

    public final CompoundButton a(ChallengeResponseData.ChallengeSelectOption challengeSelectOption, ButtonCustomization buttonCustomization, boolean z) {
        Intrinsics.checkParameterIsNotNull(challengeSelectOption, "option");
        CompoundButton materialRadioButton = this.g ? new MaterialRadioButton(getContext()) : new MaterialCheckBox(getContext());
        if (buttonCustomization != null) {
            String backgroundColor = buttonCustomization.getBackgroundColor();
            boolean z2 = false;
            if (!(backgroundColor == null || StringsKt.isBlank(backgroundColor))) {
                CompoundButtonCompat.setButtonTintList(materialRadioButton, ColorStateList.valueOf(Color.parseColor(buttonCustomization.getBackgroundColor())));
            }
            String textColor = buttonCustomization.getTextColor();
            if (textColor == null || StringsKt.isBlank(textColor)) {
                z2 = true;
            }
            if (!z2) {
                materialRadioButton.setTextColor(Color.parseColor(buttonCustomization.getTextColor()));
            }
        }
        materialRadioButton.setId(View.generateViewId());
        materialRadioButton.setTag(challengeSelectOption);
        materialRadioButton.setText(challengeSelectOption.getText());
        materialRadioButton.setPadding(this.d, materialRadioButton.getPaddingTop(), materialRadioButton.getPaddingRight(), materialRadioButton.getPaddingBottom());
        materialRadioButton.setMinimumHeight(this.f);
        RadioGroup.LayoutParams layoutParams = new RadioGroup.LayoutParams(-1, -2);
        if (!z) {
            layoutParams.bottomMargin = this.c;
        }
        layoutParams.leftMargin = this.e;
        materialRadioButton.setLayoutParams(layoutParams);
        return materialRadioButton;
    }

    public final void a(int i) {
        View childAt = this.b.getChildAt(i);
        if (childAt != null) {
            ((CompoundButton) childAt).setChecked(true);
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type android.widget.CompoundButton");
    }

    public final void a(String str, LabelCustomization labelCustomization) {
        if (str == null || StringsKt.isBlank(str)) {
            this.f105a.setVisibility(8);
        } else {
            this.f105a.a(str, labelCustomization);
        }
    }

    public final void a(List<ChallengeResponseData.ChallengeSelectOption> list, ButtonCustomization buttonCustomization) {
        if (list != null) {
            int size = list.size();
            Iterator it = RangesKt.until(0, size).iterator();
            while (it.hasNext()) {
                int nextInt = ((IntIterator) it).nextInt();
                ChallengeResponseData.ChallengeSelectOption challengeSelectOption = list.get(nextInt);
                boolean z = true;
                if (nextInt != size - 1) {
                    z = false;
                }
                this.b.addView(a(challengeSelectOption, buttonCustomization, z));
            }
        }
    }

    public final List<CheckBox> getCheckBoxes() {
        if (this.g) {
            return null;
        }
        IntRange until = RangesKt.until(0, this.b.getChildCount());
        ArrayList arrayList = new ArrayList(CollectionsKt.collectionSizeOrDefault(until, 10));
        Iterator it = until.iterator();
        while (it.hasNext()) {
            View childAt = this.b.getChildAt(((IntIterator) it).nextInt());
            if (childAt != null) {
                arrayList.add((CheckBox) childAt);
            } else {
                throw new TypeCastException("null cannot be cast to non-null type android.widget.CheckBox");
            }
        }
        return arrayList;
    }

    public final ThreeDS2TextView getInfoLabel$3ds2sdk_release() {
        return this.f105a;
    }

    public final LinearLayout getSelectGroup$3ds2sdk_release() {
        return this.b;
    }

    public final List<Integer> getSelectedIndexes$3ds2sdk_release() {
        IntRange until = RangesKt.until(0, this.b.getChildCount());
        ArrayList arrayList = new ArrayList();
        Iterator it = until.iterator();
        while (it.hasNext()) {
            int nextInt = ((IntIterator) it).nextInt();
            View childAt = this.b.getChildAt(nextInt);
            if (childAt != null) {
                Integer valueOf = ((CompoundButton) childAt).isChecked() ? Integer.valueOf(nextInt) : null;
                if (valueOf != null) {
                    arrayList.add(valueOf);
                }
            } else {
                throw new TypeCastException("null cannot be cast to non-null type android.widget.CompoundButton");
            }
        }
        return CollectionsKt.take(arrayList, this.g ? 1 : arrayList.size());
    }

    public final List<ChallengeResponseData.ChallengeSelectOption> getSelectedOptions() {
        List<Integer> selectedIndexes$3ds2sdk_release = getSelectedIndexes$3ds2sdk_release();
        ArrayList arrayList = new ArrayList(CollectionsKt.collectionSizeOrDefault(selectedIndexes$3ds2sdk_release, 10));
        for (Number intValue : selectedIndexes$3ds2sdk_release) {
            View childAt = this.b.getChildAt(intValue.intValue());
            Intrinsics.checkExpressionValueIsNotNull(childAt, "selectGroup.getChildAt(it)");
            Object tag = childAt.getTag();
            if (tag != null) {
                arrayList.add((ChallengeResponseData.ChallengeSelectOption) tag);
            } else {
                throw new TypeCastException("null cannot be cast to non-null type com.stripe.android.stripe3ds2.transactions.ChallengeResponseData.ChallengeSelectOption");
            }
        }
        return arrayList;
    }

    public void onRestoreInstanceState(Parcelable parcelable) {
        Intrinsics.checkParameterIsNotNull(parcelable, "state");
        if (parcelable instanceof Bundle) {
            Bundle bundle = (Bundle) parcelable;
            super.onRestoreInstanceState(bundle.getParcelable("state_super"));
            ArrayList<Integer> integerArrayList = bundle.getIntegerArrayList("state_selected_indexes");
            if (integerArrayList != null) {
                for (Integer num : integerArrayList) {
                    Intrinsics.checkExpressionValueIsNotNull(num, "it");
                    a(num.intValue());
                }
                return;
            }
            return;
        }
        super.onRestoreInstanceState(parcelable);
    }

    public Parcelable onSaveInstanceState() {
        Bundle bundle = new Bundle();
        bundle.putParcelable("state_super", super.onSaveInstanceState());
        bundle.putIntegerArrayList("state_selected_indexes", new ArrayList(getSelectedIndexes$3ds2sdk_release()));
        return bundle;
    }
}
