package a.a.a.a.g;

import a.a.a.a.d.c;
import android.view.View;
import com.stripe.android.stripe3ds2.views.ThreeDS2Button;

public final class f implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ c f99a;
    public final /* synthetic */ ThreeDS2Button b;

    public f(c cVar, ThreeDS2Button threeDS2Button) {
        this.f99a = cVar;
        this.b = threeDS2Button;
    }

    public final void onClick(View view) {
        c cVar = this.f99a;
        ThreeDS2Button threeDS2Button = this.b;
        if (cVar != null) {
            if (threeDS2Button != null) {
                threeDS2Button.setClickable(false);
            }
            cVar.m.a(c.a.f37a);
            return;
        }
        throw null;
    }
}
