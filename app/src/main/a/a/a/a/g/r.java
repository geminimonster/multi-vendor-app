package a.a.a.a.g;

import a.a.a.a.a.j;
import a.a.a.a.g.q;
import android.app.ProgressDialog;
import android.content.Context;
import android.view.Window;
import android.widget.ImageView;
import android.widget.ProgressBar;
import androidx.core.content.ContextCompat;
import com.stripe.android.stripe3ds2.R;
import com.stripe.android.stripe3ds2.init.ui.UiCustomization;
import com.stripe.android.stripe3ds2.utils.CustomizeUtils;
import kotlin.Lazy;
import kotlin.LazyKt;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001:\u0001\u000bB\u0005¢\u0006\u0002\u0010\u0002J \u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\nH\u0016¨\u0006\f"}, d2 = {"Lcom/stripe/android/stripe3ds2/views/ProgressViewFactoryImpl;", "Lcom/stripe/android/stripe3ds2/views/ProgressViewFactory;", "()V", "create", "Landroid/app/ProgressDialog;", "context", "Landroid/content/Context;", "brand", "Lcom/stripe/android/stripe3ds2/views/ProgressViewFactory$Brand;", "uiCustomization", "Lcom/stripe/android/stripe3ds2/init/ui/UiCustomization;", "ThreeDS2ProgressDialog", "3ds2sdk_release"}, k = 1, mv = {1, 1, 16})
public final class r implements q {

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\b\u0002\u0018\u00002\u00020\u0001B\u001f\b\u0000\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ\b\u0010\u000f\u001a\u00020\u0010H\u0016R\u000e\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0004¢\u0006\u0002\n\u0000R\u001b\u0010\t\u001a\u00020\n8BX\u0002¢\u0006\f\n\u0004\b\r\u0010\u000e\u001a\u0004\b\u000b\u0010\f¨\u0006\u0011"}, d2 = {"Lcom/stripe/android/stripe3ds2/views/ProgressViewFactoryImpl$ThreeDS2ProgressDialog;", "Landroid/app/ProgressDialog;", "context", "Landroid/content/Context;", "brand", "Lcom/stripe/android/stripe3ds2/views/ProgressViewFactory$Brand;", "uiCustomization", "Lcom/stripe/android/stripe3ds2/init/ui/UiCustomization;", "(Landroid/content/Context;Lcom/stripe/android/stripe3ds2/views/ProgressViewFactory$Brand;Lcom/stripe/android/stripe3ds2/init/ui/UiCustomization;)V", "viewBinding", "Lcom/stripe/android/stripe3ds2/databinding/ProgressViewLayoutBinding;", "getViewBinding", "()Lcom/stripe/android/stripe3ds2/databinding/ProgressViewLayoutBinding;", "viewBinding$delegate", "Lkotlin/Lazy;", "onStart", "", "3ds2sdk_release"}, k = 1, mv = {1, 1, 16})
    public static final class a extends ProgressDialog {

        /* renamed from: a  reason: collision with root package name */
        public final Lazy f114a = LazyKt.lazy(new C0018a(this));
        public final q.a b;
        public final UiCustomization c;

        /* renamed from: a.a.a.a.g.r$a$a  reason: collision with other inner class name */
        public static final class C0018a extends Lambda implements Function0<j> {

            /* renamed from: a  reason: collision with root package name */
            public final /* synthetic */ a f115a;

            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0018a(a aVar) {
                super(0);
                this.f115a = aVar;
            }

            public Object invoke() {
                return j.a(this.f115a.getLayoutInflater());
            }
        }

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(Context context, q.a aVar, UiCustomization uiCustomization) {
            super(context, R.style.Stripe3DS2FullScreenDialog);
            Intrinsics.checkParameterIsNotNull(context, "context");
            Intrinsics.checkParameterIsNotNull(aVar, "brand");
            Intrinsics.checkParameterIsNotNull(uiCustomization, "uiCustomization");
            this.b = aVar;
            this.c = uiCustomization;
            setIndeterminate(true);
            setCancelable(false);
        }

        public final j a() {
            return (j) this.f114a.getValue();
        }

        public void onStart() {
            super.onStart();
            Window window = getWindow();
            if (window != null) {
                window.setLayout(-1, -1);
            }
            setContentView(a().f15a);
            ImageView imageView = a().b;
            Intrinsics.checkExpressionValueIsNotNull(imageView, "viewBinding.brandLogo");
            imageView.setImageDrawable(ContextCompat.getDrawable(getContext(), this.b.b));
            imageView.setContentDescription(getContext().getString(this.b.c));
            imageView.setVisibility(0);
            CustomizeUtils customizeUtils = CustomizeUtils.INSTANCE;
            ProgressBar progressBar = a().c;
            Intrinsics.checkExpressionValueIsNotNull(progressBar, "viewBinding.progressBar");
            customizeUtils.applyProgressBarColor$3ds2sdk_release(progressBar, this.c);
        }
    }

    public ProgressDialog a(Context context, q.a aVar, UiCustomization uiCustomization) {
        Intrinsics.checkParameterIsNotNull(context, "context");
        Intrinsics.checkParameterIsNotNull(aVar, "brand");
        Intrinsics.checkParameterIsNotNull(uiCustomization, "uiCustomization");
        return new a(context, aVar, uiCustomization);
    }
}
