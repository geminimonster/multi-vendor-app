package a.a.a.a.g;

import android.content.Context;
import android.util.AttributeSet;
import com.stripe.android.stripe3ds2.init.ui.UiCustomization;
import com.stripe.android.stripe3ds2.transactions.ChallengeResponseData;
import kotlin.jvm.internal.Intrinsics;

public final class b {

    /* renamed from: a  reason: collision with root package name */
    public final Context f91a;

    public b(Context context) {
        Intrinsics.checkParameterIsNotNull(context, "context");
        this.f91a = context;
    }

    public final j a(ChallengeResponseData challengeResponseData, UiCustomization uiCustomization) {
        Intrinsics.checkParameterIsNotNull(challengeResponseData, "challengeResponseData");
        Intrinsics.checkParameterIsNotNull(uiCustomization, "uiCustomization");
        j jVar = new j(this.f91a, (AttributeSet) null, 0, challengeResponseData.getUiType() == ChallengeResponseData.c.SINGLE_SELECT);
        jVar.a(challengeResponseData.getChallengeInfoLabel(), uiCustomization.getLabelCustomization());
        jVar.a(challengeResponseData.getChallengeSelectOptions(), uiCustomization.getButtonCustomization(UiCustomization.ButtonType.SELECT));
        return jVar;
    }

    public final k b(ChallengeResponseData challengeResponseData, UiCustomization uiCustomization) {
        Intrinsics.checkParameterIsNotNull(challengeResponseData, "challengeResponseData");
        Intrinsics.checkParameterIsNotNull(uiCustomization, "uiCustomization");
        k kVar = new k(this.f91a, (AttributeSet) null, 0);
        kVar.setTextEntryLabel(challengeResponseData.getChallengeInfoLabel());
        kVar.setTextBoxCustomization(uiCustomization.getTextBoxCustomization());
        return kVar;
    }

    public final l a(ChallengeResponseData challengeResponseData) {
        Intrinsics.checkParameterIsNotNull(challengeResponseData, "challengeResponseData");
        l lVar = new l(this.f91a, (AttributeSet) null, 0);
        lVar.a(challengeResponseData.getAcsHtml());
        return lVar;
    }
}
