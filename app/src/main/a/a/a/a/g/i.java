package a.a.a.a.g;

import a.a.a.a.d.f;
import a.a.a.a.d.k;
import android.content.Intent;
import com.stripe.android.stripe3ds2.init.ui.StripeUiCustomization;
import com.stripe.android.stripe3ds2.transactions.ChallengeResponseData;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000D\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0012\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0000\u0018\u0000 %2\u00020\u0001:\u0001%BK\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\f\u001a\u00020\r\u0012\n\b\u0002\u0010\u000e\u001a\u0004\u0018\u00010\u000f\u0012\b\b\u0002\u0010\u0010\u001a\u00020\u0011¢\u0006\u0002\u0010\u0012J\u0006\u0010#\u001a\u00020$R\u0016\u0010\u000e\u001a\u0004\u0018\u00010\u000fX\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u0014R\u0014\u0010\u0010\u001a\u00020\u0011X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0015\u0010\u0016R\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0017\u0010\u0018R\u0011\u0010\b\u001a\u00020\t¢\u0006\b\n\u0000\u001a\u0004\b\u0019\u0010\u001aR\u0011\u0010\n\u001a\u00020\u000b¢\u0006\b\n\u0000\u001a\u0004\b\u001b\u0010\u001cR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u001d\u0010\u001eR\u0011\u0010\f\u001a\u00020\r¢\u0006\b\n\u0000\u001a\u0004\b\u001f\u0010 R\u0011\u0010\u0006\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\b!\u0010\"¨\u0006&"}, d2 = {"Lcom/stripe/android/stripe3ds2/views/ChallengeViewArgs;", "", "cresData", "Lcom/stripe/android/stripe3ds2/transactions/ChallengeResponseData;", "creqData", "Lcom/stripe/android/stripe3ds2/transactions/ChallengeRequestData;", "uiCustomization", "Lcom/stripe/android/stripe3ds2/init/ui/StripeUiCustomization;", "creqExecutorConfig", "Lcom/stripe/android/stripe3ds2/transaction/ChallengeRequestExecutor$Config;", "creqExecutorFactory", "Lcom/stripe/android/stripe3ds2/transaction/ChallengeRequestExecutor$Factory;", "errorExecutorFactory", "Lcom/stripe/android/stripe3ds2/transaction/ErrorRequestExecutor$Factory;", "challengeCompletionIntent", "Landroid/content/Intent;", "challengeCompletionRequestCode", "", "(Lcom/stripe/android/stripe3ds2/transactions/ChallengeResponseData;Lcom/stripe/android/stripe3ds2/transactions/ChallengeRequestData;Lcom/stripe/android/stripe3ds2/init/ui/StripeUiCustomization;Lcom/stripe/android/stripe3ds2/transaction/ChallengeRequestExecutor$Config;Lcom/stripe/android/stripe3ds2/transaction/ChallengeRequestExecutor$Factory;Lcom/stripe/android/stripe3ds2/transaction/ErrorRequestExecutor$Factory;Landroid/content/Intent;I)V", "getChallengeCompletionIntent$3ds2sdk_release", "()Landroid/content/Intent;", "getChallengeCompletionRequestCode$3ds2sdk_release", "()I", "getCreqData", "()Lcom/stripe/android/stripe3ds2/transactions/ChallengeRequestData;", "getCreqExecutorConfig", "()Lcom/stripe/android/stripe3ds2/transaction/ChallengeRequestExecutor$Config;", "getCreqExecutorFactory", "()Lcom/stripe/android/stripe3ds2/transaction/ChallengeRequestExecutor$Factory;", "getCresData", "()Lcom/stripe/android/stripe3ds2/transactions/ChallengeResponseData;", "getErrorExecutorFactory", "()Lcom/stripe/android/stripe3ds2/transaction/ErrorRequestExecutor$Factory;", "getUiCustomization", "()Lcom/stripe/android/stripe3ds2/init/ui/StripeUiCustomization;", "toBundle", "Landroid/os/Bundle;", "Companion", "3ds2sdk_release"}, k = 1, mv = {1, 1, 16})
public final class i {
    public static final a i = new a();

    /* renamed from: a  reason: collision with root package name */
    public final ChallengeResponseData f104a;
    public final a.a.a.a.e.a b;
    public final StripeUiCustomization c;
    public final f.a d;
    public final f.b e;
    public final k.a f;
    public final Intent g;
    public final int h;

    public static final class a {
    }

    public i(ChallengeResponseData challengeResponseData, a.a.a.a.e.a aVar, StripeUiCustomization stripeUiCustomization, f.a aVar2, f.b bVar, k.a aVar3, Intent intent, int i2) {
        Intrinsics.checkParameterIsNotNull(challengeResponseData, "cresData");
        Intrinsics.checkParameterIsNotNull(aVar, "creqData");
        Intrinsics.checkParameterIsNotNull(stripeUiCustomization, "uiCustomization");
        Intrinsics.checkParameterIsNotNull(aVar2, "creqExecutorConfig");
        Intrinsics.checkParameterIsNotNull(bVar, "creqExecutorFactory");
        Intrinsics.checkParameterIsNotNull(aVar3, "errorExecutorFactory");
        this.f104a = challengeResponseData;
        this.b = aVar;
        this.c = stripeUiCustomization;
        this.d = aVar2;
        this.e = bVar;
        this.f = aVar3;
        this.g = intent;
        this.h = i2;
    }

    public final Intent a() {
        return this.g;
    }

    public final a.a.a.a.e.a b() {
        return this.b;
    }

    public final f.a c() {
        return this.d;
    }

    public final ChallengeResponseData d() {
        return this.f104a;
    }

    public final StripeUiCustomization e() {
        return this.c;
    }
}
