package a.a.a.a.g;

import a.a.a.a.f.b;
import a.a.a.a.g.n;
import android.app.Application;
import android.content.res.Resources;
import androidx.lifecycle.AndroidViewModel;
import java.util.concurrent.CancellationException;
import kotlin.jvm.internal.Intrinsics;
import kotlinx.coroutines.CoroutineScope;
import kotlinx.coroutines.CoroutineScopeKt;
import kotlinx.coroutines.Dispatchers;
import kotlinx.coroutines.Job;
import kotlinx.coroutines.SupervisorKt;

public final class a extends AndroidViewModel {

    /* renamed from: a  reason: collision with root package name */
    public final CoroutineScope f90a = CoroutineScopeKt.CoroutineScope(Dispatchers.getIO().plus(SupervisorKt.SupervisorJob$default((Job) null, 1, (Object) null)));
    public final int b;
    public final n c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public a(Application application) {
        super(application);
        Intrinsics.checkParameterIsNotNull(application, "application");
        Resources resources = application.getResources();
        Intrinsics.checkExpressionValueIsNotNull(resources, "application.resources");
        this.b = resources.getDisplayMetrics().densityDpi;
        this.c = new n(this.f90a, (b) null, (n.a) null, 6);
    }

    public void onCleared() {
        super.onCleared();
        JobKt__JobKt.cancelChildren$default(this.f90a.getCoroutineContext(), (CancellationException) null, 1, (Object) null);
    }
}
