package a.a.a.a.g;

import android.graphics.Rect;
import android.view.View;

public final class p implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ View f112a;

    public p(View view) {
        this.f112a = view;
    }

    public final void run() {
        Rect rect = new Rect(0, 0, this.f112a.getWidth(), this.f112a.getHeight());
        this.f112a.getHitRect(rect);
        this.f112a.requestRectangleOnScreen(rect, false);
    }
}
