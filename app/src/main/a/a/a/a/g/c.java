package a.a.a.a.g;

import a.a.a.a.d.c;
import a.a.a.a.d.y;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.widget.ImageView;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import com.stripe.android.stripe3ds2.init.ui.StripeUiCustomization;
import com.stripe.android.stripe3ds2.transaction.ChallengeCompletionIntentStarter;
import com.stripe.android.stripe3ds2.transaction.ChallengeFlowOutcome;
import com.stripe.android.stripe3ds2.transaction.Stripe3ds2ActivityStarterHost;
import com.stripe.android.stripe3ds2.transactions.ChallengeResponseData;
import com.stripe.android.stripe3ds2.views.BrandZoneView;
import com.stripe.android.stripe3ds2.views.ChallengeActivity;
import com.stripe.android.stripe3ds2.views.ChallengeZoneView;
import com.stripe.android.stripe3ds2.views.InformationZoneView;
import java.lang.ref.WeakReference;
import java.util.Map;
import kotlin.Metadata;
import kotlin.Result;
import kotlin.ResultKt;
import kotlin.TuplesKt;
import kotlin.collections.MapsKt;
import kotlin.coroutines.Continuation;
import kotlin.coroutines.CoroutineContext;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;
import kotlin.text.StringsKt;
import kotlinx.coroutines.CoroutineStart;
import kotlinx.coroutines.Job;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000Î\u0001\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\b\n\u0002\b\u0006\b\u0000\u0018\u00002\u00020\u0001:\u0001YB/\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b¢\u0006\u0002\u0010\fBm\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\r\u001a\u00020\u000e\u0012\u0006\u0010\u000f\u001a\u00020\u0010\u0012\f\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u00130\u0012\u0012\u0006\u0010\u0014\u001a\u00020\u0015\u0012\u0006\u0010\u0016\u001a\u00020\u0017\u0012\n\b\u0002\u0010\u0018\u001a\u0004\u0018\u00010\u0019\u0012\b\b\u0002\u0010\u001a\u001a\u00020\u001b\u0012\b\b\u0002\u0010\u001c\u001a\u00020\u001d\u0012\b\b\u0002\u0010\u001e\u001a\u00020\u001f¢\u0006\u0002\u0010 J\u0006\u0010E\u001a\u00020FJ\u0006\u0010G\u001a\u00020FJ\b\u0010H\u001a\u00020FH\u0002J\u0010\u0010I\u001a\u00020F2\u0006\u0010\u001a\u001a\u00020\u001bH\u0002J\b\u0010J\u001a\u00020FH\u0002J\u0006\u0010K\u001a\u00020FJ\u0012\u0010L\u001a\u00020F2\n\b\u0002\u0010M\u001a\u0004\u0018\u00010NJ\u0006\u0010O\u001a\u00020FJ\u0006\u0010P\u001a\u00020FJ\u0006\u0010Q\u001a\u00020FJ\u000e\u0010R\u001a\u00020F2\u0006\u0010S\u001a\u00020TJ\u0006\u0010U\u001a\u00020FJ\b\u0010V\u001a\u00020FH\u0002J\u000e\u0010W\u001a\u00020F2\u0006\u0010X\u001a\u00020BR\u000e\u0010\u0014\u001a\u00020\u0015X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010!\u001a\u00020\"X\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u0018\u001a\u0004\u0018\u00010\u0019X\u0004¢\u0006\u0002\n\u0000R\u001b\u0010#\u001a\f\u0012\u0006\u0012\u0004\u0018\u00010%\u0018\u00010$8F¢\u0006\u0006\u001a\u0004\b&\u0010'R\u0013\u0010(\u001a\u0004\u0018\u00010)8F¢\u0006\u0006\u001a\u0004\b*\u0010+R\u0013\u0010,\u001a\u0004\u0018\u00010-8F¢\u0006\u0006\u001a\u0004\b.\u0010/R\u0013\u00100\u001a\u0004\u0018\u000101¢\u0006\b\n\u0000\u001a\u0004\b2\u00103R\u0013\u00104\u001a\u0004\u0018\u000105¢\u0006\b\n\u0000\u001a\u0004\b6\u00107R\u000e\u00108\u001a\u000209X\u0004¢\u0006\u0002\n\u0000R\u0013\u0010:\u001a\u0004\u0018\u00010;¢\u0006\b\n\u0000\u001a\u0004\b<\u0010=R\u000e\u0010\r\u001a\u00020\u000eX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u001a\u001a\u00020\u001bX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u001e\u001a\u00020\u001fX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010>\u001a\u00020?X\u0004¢\u0006\u0002\n\u0000R\u0010\u0010@\u001a\u0004\u0018\u00010\u0013X\u000e¢\u0006\u0002\n\u0000R\u0014\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u00130\u0012X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\u0017X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0004¢\u0006\u0002\n\u0000R\u0014\u0010A\u001a\u00020B8AX\u0004¢\u0006\u0006\u001a\u0004\bC\u0010DR\u000e\u0010\n\u001a\u00020\u000bX\u0004¢\u0006\u0002\n\u0000¨\u0006Z"}, d2 = {"Lcom/stripe/android/stripe3ds2/views/ChallengePresenter;", "", "activity", "Lcom/stripe/android/stripe3ds2/views/ChallengeActivity;", "args", "Lcom/stripe/android/stripe3ds2/views/ChallengeViewArgs;", "creqExecutorFactory", "Lcom/stripe/android/stripe3ds2/transaction/ChallengeRequestExecutor$Factory;", "errorExecutorFactory", "Lcom/stripe/android/stripe3ds2/transaction/ErrorRequestExecutor$Factory;", "viewModel", "Lcom/stripe/android/stripe3ds2/views/ChallengeActivityViewModel;", "(Lcom/stripe/android/stripe3ds2/views/ChallengeActivity;Lcom/stripe/android/stripe3ds2/views/ChallengeViewArgs;Lcom/stripe/android/stripe3ds2/transaction/ChallengeRequestExecutor$Factory;Lcom/stripe/android/stripe3ds2/transaction/ErrorRequestExecutor$Factory;Lcom/stripe/android/stripe3ds2/views/ChallengeActivityViewModel;)V", "cresData", "Lcom/stripe/android/stripe3ds2/transactions/ChallengeResponseData;", "uiCustomization", "Lcom/stripe/android/stripe3ds2/init/ui/StripeUiCustomization;", "progressDialogFactory", "Lcom/stripe/android/stripe3ds2/utils/Factory0;", "Landroid/app/ProgressDialog;", "actionHandler", "Lcom/stripe/android/stripe3ds2/transaction/ChallengeActionHandler;", "transactionTimer", "Lcom/stripe/android/stripe3ds2/transaction/TransactionTimer;", "challengeCompletionIntent", "Landroid/content/Intent;", "headerZoneCustomizer", "Lcom/stripe/android/stripe3ds2/views/HeaderZoneCustomizer;", "challengeEntryViewFactory", "Lcom/stripe/android/stripe3ds2/views/ChallengeEntryViewFactory;", "imageCache", "Lcom/stripe/android/stripe3ds2/utils/ImageCache;", "(Lcom/stripe/android/stripe3ds2/views/ChallengeActivity;Lcom/stripe/android/stripe3ds2/views/ChallengeActivityViewModel;Lcom/stripe/android/stripe3ds2/transactions/ChallengeResponseData;Lcom/stripe/android/stripe3ds2/init/ui/StripeUiCustomization;Lcom/stripe/android/stripe3ds2/utils/Factory0;Lcom/stripe/android/stripe3ds2/transaction/ChallengeActionHandler;Lcom/stripe/android/stripe3ds2/transaction/TransactionTimer;Landroid/content/Intent;Lcom/stripe/android/stripe3ds2/views/HeaderZoneCustomizer;Lcom/stripe/android/stripe3ds2/views/ChallengeEntryViewFactory;Lcom/stripe/android/stripe3ds2/utils/ImageCache;)V", "brandZoneView", "Lcom/stripe/android/stripe3ds2/views/BrandZoneView;", "challengeEntryCheckBoxes", "", "Landroid/widget/CheckBox;", "getChallengeEntryCheckBoxes", "()Ljava/util/List;", "challengeType", "Lcom/ults/listeners/ChallengeType;", "getChallengeType", "()Lcom/ults/listeners/ChallengeType;", "challengeWebView", "Landroid/webkit/WebView;", "getChallengeWebView", "()Landroid/webkit/WebView;", "challengeZoneSelectView", "Lcom/stripe/android/stripe3ds2/views/ChallengeZoneSelectView;", "getChallengeZoneSelectView", "()Lcom/stripe/android/stripe3ds2/views/ChallengeZoneSelectView;", "challengeZoneTextView", "Lcom/stripe/android/stripe3ds2/views/ChallengeZoneTextView;", "getChallengeZoneTextView", "()Lcom/stripe/android/stripe3ds2/views/ChallengeZoneTextView;", "challengeZoneView", "Lcom/stripe/android/stripe3ds2/views/ChallengeZoneView;", "challengeZoneWebView", "Lcom/stripe/android/stripe3ds2/views/ChallengeZoneWebView;", "getChallengeZoneWebView", "()Lcom/stripe/android/stripe3ds2/views/ChallengeZoneWebView;", "informationZoneView", "Lcom/stripe/android/stripe3ds2/views/InformationZoneView;", "progressDialog", "userEntry", "", "getUserEntry$3ds2sdk_release", "()Ljava/lang/String;", "cancelChallengeRequest", "", "clearImageCache", "configureChallengeZoneView", "configureHeaderZone", "configureInformationZoneView", "expandInformationZoneViews", "onCancelClicked", "cancelButton", "Landroid/widget/Button;", "onDestroy", "onSubmitClicked", "refreshUi", "selectChallengeOption", "index", "", "start", "updateBrandZoneImages", "updateChallengeText", "text", "TransactionTimerListenerImpl", "3ds2sdk_release"}, k = 1, mv = {1, 1, 16})
public final class c {

    /* renamed from: a  reason: collision with root package name */
    public final InformationZoneView f92a;
    public final ChallengeZoneView b;
    public final BrandZoneView c;
    public final k d;
    public final j e;
    public final l f;
    public ProgressDialog g;
    public final ChallengeActivity h;
    public final a i;
    public final ChallengeResponseData j;
    public final StripeUiCustomization k;
    public final a.a.a.a.f.a<ProgressDialog> l;
    public final a.a.a.a.d.d m;
    public final y n;
    public final Intent o;
    public final m p;
    public final a.a.a.a.f.b q;

    public static final class a implements y.b {

        /* renamed from: a  reason: collision with root package name */
        public final WeakReference<Activity> f93a;
        public final Intent b;

        public a(Activity activity, Intent intent) {
            Intrinsics.checkParameterIsNotNull(activity, "activity");
            this.b = intent;
            this.f93a = new WeakReference<>(activity);
        }

        public void a() {
            Activity activity = (Activity) this.f93a.get();
            if (activity != null) {
                Intent intent = this.b;
                if (intent != null) {
                    Intrinsics.checkExpressionValueIsNotNull(activity, "activity");
                    new ChallengeCompletionIntentStarter(new Stripe3ds2ActivityStarterHost(activity), 0, 2, (DefaultConstructorMarker) null).start(intent, ChallengeFlowOutcome.Timeout);
                }
                Intrinsics.checkExpressionValueIsNotNull(activity, "activity");
                if (!activity.isFinishing()) {
                    activity.finish();
                }
            }
        }
    }

    public static final class b implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        public final /* synthetic */ c f94a;

        public b(c cVar) {
            this.f94a = cVar;
        }

        public final void run() {
            ProgressDialog a2 = this.f94a.l.a();
            a2.show();
            c cVar = this.f94a;
            cVar.g = a2;
            ChallengeResponseData.c uiType = cVar.j.getUiType();
            if (uiType != null) {
                int ordinal = uiType.ordinal();
                if (ordinal == 3) {
                    this.f94a.m.a(c.d.f40a);
                    return;
                } else if (ordinal == 4) {
                    c cVar2 = this.f94a;
                    cVar2.m.a(new c.b(cVar2.a()));
                    return;
                }
            }
            c cVar3 = this.f94a;
            cVar3.m.a(new c.C0001c(cVar3.a()));
        }
    }

    /* renamed from: a.a.a.a.g.c$c  reason: collision with other inner class name */
    public static final class C0014c<T> implements Observer<Result<? extends Bitmap>> {

        /* renamed from: a  reason: collision with root package name */
        public final /* synthetic */ ImageView f95a;

        public C0014c(ImageView imageView) {
            this.f95a = imageView;
        }

        public void onChanged(Object obj) {
            Object r3 = ((Result) obj).m13unboximpl();
            if (Result.m7exceptionOrNullimpl(r3) == null) {
                this.f95a.setVisibility(0);
                this.f95a.setImageBitmap((Bitmap) r3);
                return;
            }
            this.f95a.setVisibility(8);
        }
    }

    public static final class d extends Lambda implements Function1<ChallengeResponseData.ChallengeSelectOption, String> {

        /* renamed from: a  reason: collision with root package name */
        public static final d f96a = new d();

        public d() {
            super(1);
        }

        public Object invoke(Object obj) {
            ChallengeResponseData.ChallengeSelectOption challengeSelectOption = (ChallengeResponseData.ChallengeSelectOption) obj;
            Intrinsics.checkParameterIsNotNull(challengeSelectOption, "it");
            return challengeSelectOption.getName();
        }
    }

    public c(ChallengeActivity challengeActivity, a aVar, ChallengeResponseData challengeResponseData, StripeUiCustomization stripeUiCustomization, a.a.a.a.f.a<ProgressDialog> aVar2, a.a.a.a.d.d dVar, y yVar, Intent intent, m mVar, b bVar, a.a.a.a.f.b bVar2) {
        Intrinsics.checkParameterIsNotNull(challengeActivity, "activity");
        Intrinsics.checkParameterIsNotNull(aVar, "viewModel");
        Intrinsics.checkParameterIsNotNull(challengeResponseData, "cresData");
        Intrinsics.checkParameterIsNotNull(stripeUiCustomization, "uiCustomization");
        Intrinsics.checkParameterIsNotNull(aVar2, "progressDialogFactory");
        Intrinsics.checkParameterIsNotNull(dVar, "actionHandler");
        Intrinsics.checkParameterIsNotNull(yVar, "transactionTimer");
        Intrinsics.checkParameterIsNotNull(mVar, "headerZoneCustomizer");
        Intrinsics.checkParameterIsNotNull(bVar, "challengeEntryViewFactory");
        Intrinsics.checkParameterIsNotNull(bVar2, "imageCache");
        this.h = challengeActivity;
        this.i = aVar;
        this.j = challengeResponseData;
        this.k = stripeUiCustomization;
        this.l = aVar2;
        this.m = dVar;
        this.n = yVar;
        this.o = intent;
        this.p = mVar;
        this.q = bVar2;
        InformationZoneView informationZoneView = challengeActivity.c().d;
        Intrinsics.checkExpressionValueIsNotNull(informationZoneView, "activity.viewBinding.caInformationZone");
        this.f92a = informationZoneView;
        ChallengeZoneView challengeZoneView = this.h.c().c;
        Intrinsics.checkExpressionValueIsNotNull(challengeZoneView, "activity.viewBinding.caChallengeZone");
        this.b = challengeZoneView;
        BrandZoneView brandZoneView = this.h.c().b;
        Intrinsics.checkExpressionValueIsNotNull(brandZoneView, "activity.viewBinding.caBrandZone");
        this.c = brandZoneView;
        l lVar = null;
        this.d = this.j.getUiType() == ChallengeResponseData.c.TEXT ? bVar.b(this.j, this.k) : null;
        this.e = (this.j.getUiType() == ChallengeResponseData.c.SINGLE_SELECT || this.j.getUiType() == ChallengeResponseData.c.MULTI_SELECT) ? bVar.a(this.j, this.k) : null;
        this.f = this.j.getUiType() == ChallengeResponseData.c.HTML ? bVar.a(this.j) : lVar;
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* synthetic */ c(com.stripe.android.stripe3ds2.views.ChallengeActivity r13, a.a.a.a.g.a r14, com.stripe.android.stripe3ds2.transactions.ChallengeResponseData r15, com.stripe.android.stripe3ds2.init.ui.StripeUiCustomization r16, a.a.a.a.f.a r17, a.a.a.a.d.d r18, a.a.a.a.d.y r19, android.content.Intent r20, a.a.a.a.g.m r21, a.a.a.a.g.b r22, a.a.a.a.f.b r23, int r24) {
        /*
            r12 = this;
            r1 = r13
            r0 = r24
            r2 = r0 & 128(0x80, float:1.794E-43)
            if (r2 == 0) goto L_0x000a
            r2 = 0
            r8 = r2
            goto L_0x000c
        L_0x000a:
            r8 = r20
        L_0x000c:
            r2 = r0 & 256(0x100, float:3.59E-43)
            if (r2 == 0) goto L_0x0017
            a.a.a.a.g.m r2 = new a.a.a.a.g.m
            r2.<init>(r13)
            r9 = r2
            goto L_0x0019
        L_0x0017:
            r9 = r21
        L_0x0019:
            r2 = r0 & 512(0x200, float:7.175E-43)
            if (r2 == 0) goto L_0x0024
            a.a.a.a.g.b r2 = new a.a.a.a.g.b
            r2.<init>(r13)
            r10 = r2
            goto L_0x0026
        L_0x0024:
            r10 = r22
        L_0x0026:
            r0 = r0 & 1024(0x400, float:1.435E-42)
            if (r0 == 0) goto L_0x002e
            a.a.a.a.f.b$a r0 = a.a.a.a.f.b.a.c
            r11 = r0
            goto L_0x0030
        L_0x002e:
            r11 = r23
        L_0x0030:
            r0 = r12
            r1 = r13
            r2 = r14
            r3 = r15
            r4 = r16
            r5 = r17
            r6 = r18
            r7 = r19
            r0.<init>(r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: a.a.a.a.g.c.<init>(com.stripe.android.stripe3ds2.views.ChallengeActivity, a.a.a.a.g.a, com.stripe.android.stripe3ds2.transactions.ChallengeResponseData, com.stripe.android.stripe3ds2.init.ui.StripeUiCustomization, a.a.a.a.f.a, a.a.a.a.d.d, a.a.a.a.d.y, android.content.Intent, a.a.a.a.g.m, a.a.a.a.g.b, a.a.a.a.f.b, int):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0027, code lost:
        r0 = r0.getUserEntry();
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.String a() {
        /*
            r10 = this;
            a.a.a.a.g.k r0 = r10.d
            if (r0 == 0) goto L_0x000b
            java.lang.String r0 = r0.getTextEntry$3ds2sdk_release()
            if (r0 == 0) goto L_0x000b
            goto L_0x0030
        L_0x000b:
            a.a.a.a.g.j r0 = r10.e
            if (r0 == 0) goto L_0x0023
            java.util.List r1 = r0.getSelectedOptions()
            a.a.a.a.g.c$d r7 = a.a.a.a.g.c.d.f96a
            r3 = 0
            r4 = 0
            r5 = 0
            r6 = 0
            r8 = 30
            r9 = 0
            java.lang.String r2 = ","
            java.lang.String r0 = kotlin.collections.CollectionsKt.joinToString$default(r1, r2, r3, r4, r5, r6, r7, r8, r9)
            goto L_0x0030
        L_0x0023:
            a.a.a.a.g.l r0 = r10.f
            if (r0 == 0) goto L_0x002e
            java.lang.String r0 = r0.getUserEntry()
            if (r0 == 0) goto L_0x002e
            goto L_0x0030
        L_0x002e:
            java.lang.String r0 = ""
        L_0x0030:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: a.a.a.a.g.c.a():java.lang.String");
    }

    public final void b() {
        if (!this.h.isFinishing()) {
            this.h.a();
            this.h.runOnUiThread(new b(this));
        }
    }

    public final void c() {
        boolean z = true;
        if (this.j.getUiType() == ChallengeResponseData.c.HTML) {
            String acsHtmlRefresh = this.j.getAcsHtmlRefresh();
            if (!(acsHtmlRefresh == null || StringsKt.isBlank(acsHtmlRefresh))) {
                l lVar = this.f;
                if (lVar != null) {
                    lVar.a(this.j.getAcsHtmlRefresh());
                    return;
                }
                return;
            }
        }
        if (this.j.getUiType() == ChallengeResponseData.c.OOB) {
            String challengeAdditionalInfoText = this.j.getChallengeAdditionalInfoText();
            if (challengeAdditionalInfoText != null && !StringsKt.isBlank(challengeAdditionalInfoText)) {
                z = false;
            }
            if (!z) {
                this.b.b(this.j.getChallengeAdditionalInfoText(), this.k.getLabelCustomization());
                this.b.setInfoTextIndicator(0);
            }
        }
    }

    public final void d() {
        for (Map.Entry entry : MapsKt.mapOf(TuplesKt.to(this.c.getIssuerImageView$3ds2sdk_release(), this.j.getIssuerImage()), TuplesKt.to(this.c.getPaymentSystemImageView$3ds2sdk_release(), this.j.getPaymentSystemImage())).entrySet()) {
            ImageView imageView = (ImageView) entry.getKey();
            ChallengeResponseData.Image image = (ChallengeResponseData.Image) entry.getValue();
            a aVar = this.i;
            n nVar = aVar.c;
            String urlForDensity = image != null ? image.getUrlForDensity(aVar.b) : null;
            if (nVar != null) {
                MutableLiveData mutableLiveData = new MutableLiveData();
                if (urlForDensity == null) {
                    Result.Companion companion = Result.Companion;
                    mutableLiveData.postValue(Result.m3boximpl(Result.m4constructorimpl(ResultKt.createFailure(new IllegalArgumentException("Image URL not available")))));
                } else {
                    Bitmap a2 = nVar.b.a(urlForDensity);
                    if (a2 != null) {
                        Result.Companion companion2 = Result.Companion;
                        mutableLiveData.postValue(Result.m3boximpl(Result.m4constructorimpl(a2)));
                    } else {
                        Job unused = BuildersKt__Builders_commonKt.launch$default(nVar.f110a, (CoroutineContext) null, (CoroutineStart) null, new o(nVar, urlForDensity, mutableLiveData, (Continuation) null), 3, (Object) null);
                    }
                }
                mutableLiveData.observe(this.h, new C0014c(imageView));
            } else {
                throw null;
            }
        }
    }
}
