package a.a.a.a.g;

import a.a.a.a.f.b;
import android.graphics.Bitmap;
import kotlin.Metadata;
import kotlin.coroutines.Continuation;
import kotlin.jvm.internal.Intrinsics;
import kotlinx.coroutines.CoroutineScope;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\b\u0000\u0018\u00002\u00020\u0001:\u0001\u0016B!\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0002\u0010\u0004\u001a\u00020\u0005\u0012\b\b\u0002\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ\u001a\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\f2\b\u0010\r\u001a\u0004\u0018\u00010\u000eH\u0002J&\u0010\u000f\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u000e0\u00110\u00102\b\u0010\u000b\u001a\u0004\u0018\u00010\fH\u0000ø\u0001\u0000¢\u0006\u0002\b\u0012J\u0012\u0010\u0013\u001a\u0004\u0018\u00010\u000e2\u0006\u0010\u000b\u001a\u00020\fH\u0002J\u001b\u0010\u0014\u001a\u0004\u0018\u00010\u000e2\u0006\u0010\u000b\u001a\u00020\fH@ø\u0001\u0000¢\u0006\u0002\u0010\u0015R\u000e\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000\u0002\u0004\n\u0002\b\u0019¨\u0006\u0017"}, d2 = {"Lcom/stripe/android/stripe3ds2/views/ImageRepository;", "", "workScope", "Lkotlinx/coroutines/CoroutineScope;", "imageCache", "Lcom/stripe/android/stripe3ds2/utils/ImageCache;", "imageSupplier", "Lcom/stripe/android/stripe3ds2/views/ImageRepository$ImageSupplier;", "(Lkotlinx/coroutines/CoroutineScope;Lcom/stripe/android/stripe3ds2/utils/ImageCache;Lcom/stripe/android/stripe3ds2/views/ImageRepository$ImageSupplier;)V", "cacheImage", "", "imageUrl", "", "image", "Landroid/graphics/Bitmap;", "getImage", "Landroidx/lifecycle/LiveData;", "Lkotlin/Result;", "getImage$3ds2sdk_release", "getLocalImage", "getRemoteImage", "(Ljava/lang/String;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "ImageSupplier", "3ds2sdk_release"}, k = 1, mv = {1, 1, 16})
public final class n {

    /* renamed from: a  reason: collision with root package name */
    public final CoroutineScope f110a;
    public final b b;
    public final a c;

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\bf\u0018\u00002\u00020\u0001:\u0001\u0007J\u001b\u0010\u0002\u001a\u0004\u0018\u00010\u00032\u0006\u0010\u0004\u001a\u00020\u0005H¦@ø\u0001\u0000¢\u0006\u0002\u0010\u0006\u0002\u0004\n\u0002\b\u0019¨\u0006\b"}, d2 = {"Lcom/stripe/android/stripe3ds2/views/ImageRepository$ImageSupplier;", "", "getBitmap", "Landroid/graphics/Bitmap;", "url", "", "(Ljava/lang/String;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "Default", "3ds2sdk_release"}, k = 1, mv = {1, 1, 16})
    public interface a {

        /* renamed from: a.a.a.a.g.n$a$a  reason: collision with other inner class name */
        public static final class C0016a implements a {
            /* JADX WARNING: Code restructure failed: missing block: B:14:0x002d, code lost:
                r1 = move-exception;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:16:?, code lost:
                kotlin.io.CloseableKt.closeFinally(r3, r0);
             */
            /* JADX WARNING: Code restructure failed: missing block: B:17:0x0031, code lost:
                throw r1;
             */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public java.lang.Object a(java.lang.String r3, kotlin.coroutines.Continuation<? super android.graphics.Bitmap> r4) {
                /*
                    r2 = this;
                    r4 = 0
                    kotlin.Result$Companion r0 = kotlin.Result.Companion     // Catch:{ all -> 0x0038 }
                    a.a.a.a.d.s r0 = new a.a.a.a.d.s     // Catch:{ all -> 0x0038 }
                    r0.<init>(r3)     // Catch:{ all -> 0x0038 }
                    java.net.HttpURLConnection r3 = r0.a()     // Catch:{ all -> 0x0038 }
                    r0 = 1
                    r3.setDoInput(r0)     // Catch:{ all -> 0x0038 }
                    r3.connect()     // Catch:{ all -> 0x0038 }
                    int r0 = r3.getResponseCode()     // Catch:{ all -> 0x0038 }
                    r1 = 200(0xc8, float:2.8E-43)
                    if (r0 == r1) goto L_0x001d
                    r3 = r4
                    goto L_0x0021
                L_0x001d:
                    java.io.InputStream r3 = r3.getInputStream()     // Catch:{ all -> 0x0038 }
                L_0x0021:
                    if (r3 == 0) goto L_0x0032
                    android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeStream(r3)     // Catch:{ all -> 0x002b }
                    kotlin.io.CloseableKt.closeFinally(r3, r4)     // Catch:{ all -> 0x0038 }
                    goto L_0x0033
                L_0x002b:
                    r0 = move-exception
                    throw r0     // Catch:{ all -> 0x002d }
                L_0x002d:
                    r1 = move-exception
                    kotlin.io.CloseableKt.closeFinally(r3, r0)     // Catch:{ all -> 0x0038 }
                    throw r1     // Catch:{ all -> 0x0038 }
                L_0x0032:
                    r0 = r4
                L_0x0033:
                    java.lang.Object r3 = kotlin.Result.m4constructorimpl(r0)     // Catch:{ all -> 0x0038 }
                    goto L_0x0043
                L_0x0038:
                    r3 = move-exception
                    kotlin.Result$Companion r0 = kotlin.Result.Companion
                    java.lang.Object r3 = kotlin.ResultKt.createFailure(r3)
                    java.lang.Object r3 = kotlin.Result.m4constructorimpl(r3)
                L_0x0043:
                    boolean r0 = kotlin.Result.m10isFailureimpl(r3)
                    if (r0 == 0) goto L_0x004a
                    goto L_0x004b
                L_0x004a:
                    r4 = r3
                L_0x004b:
                    return r4
                */
                throw new UnsupportedOperationException("Method not decompiled: a.a.a.a.g.n.a.C0016a.a(java.lang.String, kotlin.coroutines.Continuation):java.lang.Object");
            }
        }

        Object a(String str, Continuation<? super Bitmap> continuation);
    }

    public n(CoroutineScope coroutineScope, b bVar, a aVar) {
        Intrinsics.checkParameterIsNotNull(coroutineScope, "workScope");
        Intrinsics.checkParameterIsNotNull(bVar, "imageCache");
        Intrinsics.checkParameterIsNotNull(aVar, "imageSupplier");
        this.f110a = coroutineScope;
        this.b = bVar;
        this.c = aVar;
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ n(CoroutineScope coroutineScope, b bVar, a aVar, int i) {
        this(coroutineScope, (i & 2) != 0 ? b.a.c : bVar, (i & 4) != 0 ? new a.C0016a() : aVar);
    }
}
