package a.a.a.a.g;

import a.a.a.a.a.h;
import a.a.a.a.g.s;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import com.bumptech.glide.load.Key;
import com.stripe.android.stripe3ds2.views.ThreeDS2WebView;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import kotlin.text.Regex;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\b\b\u0000\u0018\u0000  2\u00020\u0001:\u0001 B%\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\b\b\u0002\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ\u0010\u0010\u0018\u001a\u00020\u00192\b\u0010\u001a\u001a\u0004\u0018\u00010\u0010J\u0012\u0010\u001b\u001a\u00020\u00192\b\u0010\t\u001a\u0004\u0018\u00010\nH\u0016J\u0010\u0010\u001c\u001a\u00020\u00102\u0006\u0010\u001a\u001a\u00020\u0010H\u0002J\u0010\u0010\u001d\u001a\u00020\u00102\u0006\u0010\u001a\u001a\u00020\u0010H\u0002J\u0015\u0010\u001e\u001a\u00020\u00102\u0006\u0010\u001a\u001a\u00020\u0010H\u0001¢\u0006\u0002\b\u001fR\u001c\u0010\t\u001a\u0004\u0018\u00010\nX\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u000b\u0010\f\"\u0004\b\r\u0010\u000eR\"\u0010\u0011\u001a\u0004\u0018\u00010\u00102\b\u0010\u000f\u001a\u0004\u0018\u00010\u0010@BX\u000e¢\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u0013R\u0011\u0010\u0014\u001a\u00020\u0015¢\u0006\b\n\u0000\u001a\u0004\b\u0016\u0010\u0017¨\u0006!"}, d2 = {"Lcom/stripe/android/stripe3ds2/views/ChallengeZoneWebView;", "Landroid/widget/FrameLayout;", "context", "Landroid/content/Context;", "attrs", "Landroid/util/AttributeSet;", "defStyleAttr", "", "(Landroid/content/Context;Landroid/util/AttributeSet;I)V", "onClickListener", "Landroid/view/View$OnClickListener;", "getOnClickListener$3ds2sdk_release", "()Landroid/view/View$OnClickListener;", "setOnClickListener$3ds2sdk_release", "(Landroid/view/View$OnClickListener;)V", "<set-?>", "", "userEntry", "getUserEntry", "()Ljava/lang/String;", "webView", "Lcom/stripe/android/stripe3ds2/views/ThreeDS2WebView;", "getWebView", "()Lcom/stripe/android/stripe3ds2/views/ThreeDS2WebView;", "loadHtml", "", "html", "setOnClickListener", "transformFormActionUrl", "transformFormMethod", "transformHtml", "transformHtml$3ds2sdk_release", "Companion", "3ds2sdk_release"}, k = 1, mv = {1, 1, 16})
public final class l extends FrameLayout {
    public static final Pattern d = Pattern.compile("method=\"post\"", 10);
    public static final Pattern e = Pattern.compile("action=\"(.+?)\"", 10);

    /* renamed from: a  reason: collision with root package name */
    public final ThreeDS2WebView f107a;
    public String b;
    public View.OnClickListener c;

    public static final class a implements s.a {

        /* renamed from: a  reason: collision with root package name */
        public final /* synthetic */ l f108a;

        public a(l lVar) {
            this.f108a = lVar;
        }

        public void a(String str) {
            l lVar = this.f108a;
            lVar.b = str;
            View.OnClickListener onClickListener$3ds2sdk_release = lVar.getOnClickListener$3ds2sdk_release();
            if (onClickListener$3ds2sdk_release != null) {
                onClickListener$3ds2sdk_release.onClick(this.f108a);
            }
        }
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public l(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        Intrinsics.checkParameterIsNotNull(context, "context");
        h a2 = h.a(LayoutInflater.from(context), this);
        Intrinsics.checkExpressionValueIsNotNull(a2, "ChallengeZoneWebViewBind…           this\n        )");
        ThreeDS2WebView threeDS2WebView = a2.b;
        Intrinsics.checkExpressionValueIsNotNull(threeDS2WebView, "viewBinding.webView");
        this.f107a = threeDS2WebView;
        threeDS2WebView.setOnHtmlSubmitListener$3ds2sdk_release(new a(this));
    }

    public final void a(String str) {
        String group;
        if (str != null) {
            ThreeDS2WebView threeDS2WebView = this.f107a;
            Intrinsics.checkParameterIsNotNull(str, "html");
            String replaceAll = d.matcher(str).replaceAll("method=\"get\"");
            Intrinsics.checkExpressionValueIsNotNull(replaceAll, "methodMatcher.replaceAll(METHOD_GET)");
            Matcher matcher = e.matcher(replaceAll);
            if (matcher.find() && (group = matcher.group(1)) != null && (true ^ Intrinsics.areEqual((Object) "https://emv3ds/challenge", (Object) group))) {
                replaceAll = new Regex(group).replace((CharSequence) replaceAll, "https://emv3ds/challenge");
            }
            threeDS2WebView.loadDataWithBaseURL((String) null, replaceAll, "text/html", Key.STRING_CHARSET_NAME, (String) null);
        }
    }

    public final View.OnClickListener getOnClickListener$3ds2sdk_release() {
        return this.c;
    }

    public final String getUserEntry() {
        return this.b;
    }

    public final ThreeDS2WebView getWebView() {
        return this.f107a;
    }

    public void setOnClickListener(View.OnClickListener onClickListener) {
        this.c = onClickListener;
    }

    public final void setOnClickListener$3ds2sdk_release(View.OnClickListener onClickListener) {
        this.c = onClickListener;
    }
}
