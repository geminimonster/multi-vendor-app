package a.a.a.a.d;

import a.a.a.a.c.c;
import a.a.a.a.c.i;
import a.a.a.a.d.a;
import a.a.a.a.d.f;
import a.a.a.a.d.i;
import a.a.a.a.d.k;
import a.a.a.a.d.q;
import a.a.a.a.d.y;
import a.a.a.a.d.z;
import a.a.a.a.e.a;
import a.a.a.a.g.q;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import androidx.core.view.PointerIconCompat;
import com.facebook.share.internal.MessengerShareContentUtility;
import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.jwk.ECKey;
import com.stripe.android.stripe3ds2.exceptions.InvalidInputException;
import com.stripe.android.stripe3ds2.exceptions.SDKRuntimeException;
import com.stripe.android.stripe3ds2.init.ui.StripeUiCustomization;
import com.stripe.android.stripe3ds2.transaction.AuthenticationRequestParameters;
import com.stripe.android.stripe3ds2.transaction.ChallengeCompletionIntentStarter;
import com.stripe.android.stripe3ds2.transaction.ChallengeFlowOutcome;
import com.stripe.android.stripe3ds2.transaction.ChallengeParameters;
import com.stripe.android.stripe3ds2.transaction.ChallengeStatusReceiver;
import com.stripe.android.stripe3ds2.transaction.MessageVersionRegistry;
import com.stripe.android.stripe3ds2.transaction.RuntimeErrorEvent;
import com.stripe.android.stripe3ds2.transaction.Stripe3ds2ActivityStarterHost;
import com.stripe.android.stripe3ds2.transaction.Transaction;
import com.stripe.android.stripe3ds2.transactions.ChallengeResponseData;
import java.security.KeyPair;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.security.interfaces.ECPublicKey;
import java.text.ParseException;
import java.util.List;
import kotlin.Metadata;
import kotlin.Result;
import kotlin.ResultKt;
import kotlin.Unit;
import kotlin.coroutines.Continuation;
import kotlin.coroutines.CoroutineContext;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;
import kotlinx.coroutines.CoroutineScopeKt;
import kotlinx.coroutines.CoroutineStart;
import kotlinx.coroutines.Dispatchers;
import kotlinx.coroutines.Job;
import kotlinx.coroutines.SupervisorKt;
import org.json.JSONException;
import org.json.JSONObject;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000Ö\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0000\u0018\u0000 P2\u00020\u0001:\u0001PBµ\u0001\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\f\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u0012\u0006\u0010\u0010\u001a\u00020\u000b\u0012\u0006\u0010\u0011\u001a\u00020\u0012\u0012\b\u0010\u0013\u001a\u0004\u0018\u00010\u000b\u0012\u0006\u0010\u0014\u001a\u00020\u000b\u0012\u0006\u0010\u0015\u001a\u00020\u0016\u0012\u0006\u0010\u0017\u001a\u00020\u0018\u0012\f\u0010\u0019\u001a\b\u0012\u0004\u0012\u00020\u001b0\u001a\u0012\u0006\u0010\u001c\u001a\u00020\u001d\u0012\b\u0010\u001e\u001a\u0004\u0018\u00010\u001f\u0012\u0006\u0010 \u001a\u00020!\u0012\u0006\u0010\"\u001a\u00020#\u0012\n\b\u0002\u0010$\u001a\u0004\u0018\u00010%\u0012\b\b\u0002\u0010&\u001a\u00020'¢\u0006\u0002\u0010(J\b\u00102\u001a\u000203H\u0016J(\u00104\u001a\u0002032\u0006\u00105\u001a\u0002062\u0006\u00107\u001a\u0002082\u0006\u00109\u001a\u00020:2\u0006\u0010;\u001a\u00020'H\u0016J(\u00104\u001a\u0002032\u0006\u0010<\u001a\u00020=2\u0006\u00107\u001a\u0002082\u0006\u00109\u001a\u00020:2\u0006\u0010;\u001a\u00020'H\u0016J&\u0010>\u001a\u00020?2\u0006\u0010@\u001a\u00020\u000b2\u0006\u0010\u0017\u001a\u00020\u00182\f\u0010\u0019\u001a\b\u0012\u0004\u0012\u00020\u001b0\u001aH\u0002J\u0010\u0010A\u001a\u00020B2\u0006\u0010C\u001a\u000206H\u0016J0\u0010D\u001a\u0002032\u0006\u0010<\u001a\u00020=2\u0006\u0010E\u001a\u00020F2\u0006\u0010G\u001a\u00020H2\u0006\u0010\u001e\u001a\u00020\u001f2\u0006\u0010I\u001a\u00020JH\u0002J\u0018\u0010K\u001a\u0002032\u0006\u0010L\u001a\u00020M2\u0006\u0010N\u001a\u00020OH\u0002R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u0014\u0010)\u001a\u00020*8VX\u0004¢\u0006\u0006\u001a\u0004\b+\u0010,R\u000e\u0010 \u001a\u00020!X\u0004¢\u0006\u0002\n\u0000R\u0010\u0010$\u001a\u0004\u0018\u00010%X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010&\u001a\u00020'X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u000bX\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u0013\u001a\u0004\u0018\u00010\u000bX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0012X\u0004¢\u0006\u0002\n\u0000R\u001c\u0010-\u001a\u0004\u0018\u00010\u000bX\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b.\u0010/\"\u0004\b0\u00101R\u000e\u0010\u0017\u001a\u00020\u0018X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\rX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\"\u001a\u00020#X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u001c\u001a\u00020\u001dX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\u0019\u001a\b\u0012\u0004\u0012\u00020\u001b0\u001aX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u0016X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u000bX\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u001e\u001a\u0004\u0018\u00010\u001fX\u0004¢\u0006\u0002\n\u0000¨\u0006Q"}, d2 = {"Lcom/stripe/android/stripe3ds2/transaction/StripeTransaction;", "Lcom/stripe/android/stripe3ds2/transaction/Transaction;", "areqParamsFactory", "Lcom/stripe/android/stripe3ds2/transaction/AuthenticationRequestParametersFactory;", "progressViewFactory", "Lcom/stripe/android/stripe3ds2/views/ProgressViewFactory;", "challengeStatusReceiverProvider", "Lcom/stripe/android/stripe3ds2/transaction/ChallengeStatusReceiverProvider;", "messageVersionRegistry", "Lcom/stripe/android/stripe3ds2/transaction/MessageVersionRegistry;", "sdkReferenceNumber", "", "jwsValidator", "Lcom/stripe/android/stripe3ds2/transaction/JwsValidator;", "protocolErrorEventFactory", "Lcom/stripe/android/stripe3ds2/transaction/ProtocolErrorEventFactory;", "directoryServerId", "directoryServerPublicKey", "Ljava/security/PublicKey;", "directoryServerKeyId", "sdkTransactionId", "sdkKeyPair", "Ljava/security/KeyPair;", "isLiveMode", "", "rootCerts", "", "Ljava/security/cert/X509Certificate;", "messageTransformer", "Lcom/stripe/android/stripe3ds2/security/MessageTransformer;", "uiCustomization", "Lcom/stripe/android/stripe3ds2/init/ui/StripeUiCustomization;", "brand", "Lcom/stripe/android/stripe3ds2/views/ProgressViewFactory$Brand;", "logger", "Lcom/stripe/android/stripe3ds2/transaction/Logger;", "challengeCompletionIntent", "Landroid/content/Intent;", "challengeCompletionRequestCode", "", "(Lcom/stripe/android/stripe3ds2/transaction/AuthenticationRequestParametersFactory;Lcom/stripe/android/stripe3ds2/views/ProgressViewFactory;Lcom/stripe/android/stripe3ds2/transaction/ChallengeStatusReceiverProvider;Lcom/stripe/android/stripe3ds2/transaction/MessageVersionRegistry;Ljava/lang/String;Lcom/stripe/android/stripe3ds2/transaction/JwsValidator;Lcom/stripe/android/stripe3ds2/transaction/ProtocolErrorEventFactory;Ljava/lang/String;Ljava/security/PublicKey;Ljava/lang/String;Ljava/lang/String;Ljava/security/KeyPair;ZLjava/util/List;Lcom/stripe/android/stripe3ds2/security/MessageTransformer;Lcom/stripe/android/stripe3ds2/init/ui/StripeUiCustomization;Lcom/stripe/android/stripe3ds2/views/ProgressViewFactory$Brand;Lcom/stripe/android/stripe3ds2/transaction/Logger;Landroid/content/Intent;I)V", "authenticationRequestParameters", "Lcom/stripe/android/stripe3ds2/transaction/AuthenticationRequestParameters;", "getAuthenticationRequestParameters", "()Lcom/stripe/android/stripe3ds2/transaction/AuthenticationRequestParameters;", "initialChallengeUiType", "getInitialChallengeUiType", "()Ljava/lang/String;", "setInitialChallengeUiType", "(Ljava/lang/String;)V", "close", "", "doChallenge", "activity", "Landroid/app/Activity;", "challengeParameters", "Lcom/stripe/android/stripe3ds2/transaction/ChallengeParameters;", "challengeStatusReceiver", "Lcom/stripe/android/stripe3ds2/transaction/ChallengeStatusReceiver;", "timeOut", "host", "Lcom/stripe/android/stripe3ds2/transaction/Stripe3ds2ActivityStarterHost;", "getAcsData", "Lcom/stripe/android/stripe3ds2/transaction/AcsData;", "acsSignedContent", "getProgressView", "Landroid/app/ProgressDialog;", "currentActivity", "handleChallengeResponse", "creqData", "Lcom/stripe/android/stripe3ds2/transactions/ChallengeRequestData;", "cresData", "Lcom/stripe/android/stripe3ds2/transactions/ChallengeResponseData;", "creqExecutorConfig", "Lcom/stripe/android/stripe3ds2/transaction/ChallengeRequestExecutor$Config;", "sendErrorData", "errorRequestExecutor", "Lcom/stripe/android/stripe3ds2/transaction/ErrorRequestExecutor;", "errorData", "Lcom/stripe/android/stripe3ds2/transactions/ErrorData;", "Companion", "3ds2sdk_release"}, k = 1, mv = {1, 1, 16})
public final class t implements Transaction {

    /* renamed from: a  reason: collision with root package name */
    public String f70a;
    public final b b;
    public final q c;
    public final j d;
    public final MessageVersionRegistry e;
    public final String f;
    public final n g;
    public final p h;
    public final String i;
    public final PublicKey j;
    public final String k;
    public final String l;
    public final KeyPair m;
    public final boolean n;
    public final List<X509Certificate> o;
    public final i p;
    public final StripeUiCustomization q;
    public final q.a r;
    public final o s;
    public final Intent t;
    public final int u;

    public static final class a extends Lambda implements Function0<Unit> {

        /* renamed from: a  reason: collision with root package name */
        public final /* synthetic */ t f71a;
        public final /* synthetic */ ChallengeCompletionIntentStarter b;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(t tVar, ChallengeStatusReceiver challengeStatusReceiver, ChallengeCompletionIntentStarter challengeCompletionIntentStarter) {
            super(0);
            this.f71a = tVar;
            this.b = challengeCompletionIntentStarter;
        }

        public Object invoke() {
            Intent intent = this.f71a.t;
            if (intent != null) {
                this.b.start(intent, ChallengeFlowOutcome.RuntimeError);
            }
            return Unit.INSTANCE;
        }
    }

    public t(b bVar, q qVar, j jVar, MessageVersionRegistry messageVersionRegistry, String str, n nVar, p pVar, String str2, PublicKey publicKey, String str3, String str4, KeyPair keyPair, boolean z, List<? extends X509Certificate> list, i iVar, StripeUiCustomization stripeUiCustomization, q.a aVar, o oVar, Intent intent, int i2) {
        b bVar2 = bVar;
        q qVar2 = qVar;
        j jVar2 = jVar;
        MessageVersionRegistry messageVersionRegistry2 = messageVersionRegistry;
        String str5 = str;
        n nVar2 = nVar;
        p pVar2 = pVar;
        String str6 = str2;
        PublicKey publicKey2 = publicKey;
        String str7 = str4;
        KeyPair keyPair2 = keyPair;
        List<? extends X509Certificate> list2 = list;
        i iVar2 = iVar;
        q.a aVar2 = aVar;
        o oVar2 = oVar;
        Intrinsics.checkParameterIsNotNull(bVar2, "areqParamsFactory");
        Intrinsics.checkParameterIsNotNull(qVar2, "progressViewFactory");
        Intrinsics.checkParameterIsNotNull(jVar2, "challengeStatusReceiverProvider");
        Intrinsics.checkParameterIsNotNull(messageVersionRegistry2, "messageVersionRegistry");
        Intrinsics.checkParameterIsNotNull(str5, "sdkReferenceNumber");
        Intrinsics.checkParameterIsNotNull(nVar2, "jwsValidator");
        Intrinsics.checkParameterIsNotNull(pVar2, "protocolErrorEventFactory");
        Intrinsics.checkParameterIsNotNull(str6, "directoryServerId");
        Intrinsics.checkParameterIsNotNull(publicKey2, "directoryServerPublicKey");
        Intrinsics.checkParameterIsNotNull(str7, "sdkTransactionId");
        Intrinsics.checkParameterIsNotNull(keyPair2, "sdkKeyPair");
        Intrinsics.checkParameterIsNotNull(list2, "rootCerts");
        Intrinsics.checkParameterIsNotNull(iVar2, "messageTransformer");
        Intrinsics.checkParameterIsNotNull(aVar2, "brand");
        Intrinsics.checkParameterIsNotNull(oVar2, "logger");
        this.b = bVar2;
        this.c = qVar2;
        this.d = jVar2;
        this.e = messageVersionRegistry2;
        this.f = str5;
        this.g = nVar2;
        this.h = pVar2;
        this.i = str6;
        this.j = publicKey2;
        this.k = str3;
        this.l = str7;
        this.m = keyPair2;
        this.n = z;
        this.o = list2;
        this.p = iVar2;
        this.q = stripeUiCustomization;
        this.r = aVar2;
        this.s = oVar2;
        this.t = intent;
        this.u = i2;
    }

    public final a a(String str, boolean z, List<? extends X509Certificate> list) throws ParseException, JOSEException, JSONException, CertificateException {
        a.C0000a aVar = a.d;
        JSONObject a2 = this.g.a(str, z, list);
        Intrinsics.checkParameterIsNotNull(a2, MessengerShareContentUtility.ATTACHMENT_PAYLOAD);
        String string = a2.getString("acsURL");
        Intrinsics.checkExpressionValueIsNotNull(string, "payload.getString(FIELD_ACS_URL)");
        ECPublicKey eCPublicKey = ECKey.parse(a2.getString("acsEphemPubKey")).toECPublicKey();
        Intrinsics.checkExpressionValueIsNotNull(eCPublicKey, "ECKey.parse(payload.getS…PUB_KEY)).toECPublicKey()");
        ECPublicKey eCPublicKey2 = ECKey.parse(a2.getString("sdkEphemPubKey")).toECPublicKey();
        Intrinsics.checkExpressionValueIsNotNull(eCPublicKey2, "ECKey.parse(payload.getS…PUB_KEY)).toECPublicKey()");
        return new a(string, eCPublicKey, eCPublicKey2);
    }

    public void close() {
    }

    public void doChallenge(Activity activity, ChallengeParameters challengeParameters, ChallengeStatusReceiver challengeStatusReceiver, int i2) throws InvalidInputException {
        Object obj;
        Activity activity2 = activity;
        ChallengeParameters challengeParameters2 = challengeParameters;
        ChallengeStatusReceiver challengeStatusReceiver2 = challengeStatusReceiver;
        Intrinsics.checkParameterIsNotNull(activity2, "activity");
        Intrinsics.checkParameterIsNotNull(challengeParameters2, "challengeParameters");
        Intrinsics.checkParameterIsNotNull(challengeStatusReceiver2, "challengeStatusReceiver");
        Stripe3ds2ActivityStarterHost stripe3ds2ActivityStarterHost = new Stripe3ds2ActivityStarterHost(activity2);
        Intrinsics.checkParameterIsNotNull(stripe3ds2ActivityStarterHost, "host");
        Intrinsics.checkParameterIsNotNull(challengeParameters2, "challengeParameters");
        Intrinsics.checkParameterIsNotNull(challengeStatusReceiver2, "challengeStatusReceiver");
        this.s.a("Starting challenge flow.");
        ChallengeCompletionIntentStarter challengeCompletionIntentStarter = new ChallengeCompletionIntentStarter(stripe3ds2ActivityStarterHost, this.u);
        try {
            Result.Companion companion = Result.Companion;
            if (i2 >= 5) {
                this.d.a(this.l, challengeStatusReceiver2);
                String acsSignedContent = challengeParameters.getAcsSignedContent();
                if (acsSignedContent != null) {
                    a a2 = a(acsSignedContent, this.n, this.o);
                    String str = a2.f35a;
                    ECPublicKey eCPublicKey = a2.b;
                    String acsTransactionId = challengeParameters.getAcsTransactionId();
                    if (acsTransactionId != null) {
                        String threeDsServerTransactionId = challengeParameters.getThreeDsServerTransactionId();
                        if (threeDsServerTransactionId != null) {
                            a.a.a.a.e.a aVar = new a.a.a.a.e.a(this.e.getCurrent(), threeDsServerTransactionId, acsTransactionId, this.l, (String) null, (a.C0011a) null, (String) null, (List) null, (Boolean) null, (Boolean) null, PointerIconCompat.TYPE_TEXT);
                            Intrinsics.checkParameterIsNotNull(str, "acsUrl");
                            r rVar = new r(new s(str), CoroutineScopeKt.CoroutineScope(Dispatchers.getIO()));
                            y.a aVar2 = r0;
                            a.a.a.a.e.a aVar3 = aVar;
                            y.a aVar4 = new y.a(challengeStatusReceiver, i2, rVar, aVar, z.a.b, CoroutineScopeKt.CoroutineScope(Dispatchers.getMain().plus(SupervisorKt.SupervisorJob$default((Job) null, 1, (Object) null))));
                            aVar2.g.a(aVar2.f.d, aVar2);
                            aVar2.c = BuildersKt__Builders_commonKt.launch$default(aVar2.h, (CoroutineContext) null, (CoroutineStart) null, new x(aVar2, (Continuation) null), 3, (Object) null);
                            i iVar = this.p;
                            String str2 = this.f;
                            PrivateKey privateKey = this.m.getPrivate();
                            Intrinsics.checkExpressionValueIsNotNull(privateKey, "sdkKeyPair.private");
                            byte[] encoded = privateKey.getEncoded();
                            Intrinsics.checkExpressionValueIsNotNull(encoded, "sdkKeyPair.private.encoded");
                            byte[] encoded2 = eCPublicKey.getEncoded();
                            Intrinsics.checkExpressionValueIsNotNull(encoded2, "acsEphemPubKey.encoded");
                            f.a aVar5 = new f.a(iVar, str2, encoded, encoded2, str, aVar3);
                            a.a.a.a.e.a aVar6 = aVar3;
                            new q.c().a(aVar5).b(aVar6, new b(this, aVar5, aVar2, rVar, i2, challengeStatusReceiver, challengeParameters, stripe3ds2ActivityStarterHost, challengeCompletionIntentStarter));
                            obj = Result.m4constructorimpl(Unit.INSTANCE);
                            Throwable r0 = Result.m7exceptionOrNullimpl(obj);
                            if (r0 != null) {
                                this.s.a("Exception during challenge flow.", r0);
                                challengeStatusReceiver2.runtimeError(new RuntimeErrorEvent(r0), new a(this, challengeStatusReceiver2, challengeCompletionIntentStarter));
                                return;
                            }
                            return;
                        }
                        throw new IllegalArgumentException("Required value was null.".toString());
                    }
                    throw new IllegalArgumentException("Required value was null.".toString());
                }
                throw new IllegalArgumentException("Required value was null.".toString());
            }
            throw new InvalidInputException(new RuntimeException("Timeout must be at least 5 minutes"));
        } catch (Throwable th) {
            Result.Companion companion2 = Result.Companion;
            obj = Result.m4constructorimpl(ResultKt.createFailure(th));
        }
    }

    public AuthenticationRequestParameters getAuthenticationRequestParameters() {
        Object obj;
        b bVar = this.b;
        String str = this.i;
        PublicKey publicKey = this.j;
        String str2 = this.k;
        String str3 = this.l;
        PublicKey publicKey2 = this.m.getPublic();
        Intrinsics.checkExpressionValueIsNotNull(publicKey2, "sdkKeyPair.public");
        if (bVar != null) {
            Intrinsics.checkParameterIsNotNull(str, "directoryServerId");
            Intrinsics.checkParameterIsNotNull(publicKey, "directoryServerPublicKey");
            Intrinsics.checkParameterIsNotNull(str3, "transactionId");
            Intrinsics.checkParameterIsNotNull(publicKey2, "sdkPublicKey");
            c a2 = c.g.a(str);
            try {
                Result.Companion companion = Result.Companion;
                obj = Result.m4constructorimpl(bVar.e.a(bVar.a(), publicKey, str, str2));
            } catch (Throwable th) {
                Result.Companion companion2 = Result.Companion;
                obj = Result.m4constructorimpl(ResultKt.createFailure(th));
            }
            Throwable r2 = Result.m7exceptionOrNullimpl(obj);
            if (r2 == null) {
                String str4 = bVar.d.a().f22a;
                String str5 = bVar.g;
                String jSONString = b.h.a(publicKey2, str2, a2.d).toJSONString();
                Intrinsics.checkExpressionValueIsNotNull(jSONString, "createPublicJwk(\n       …         ).toJSONString()");
                return new AuthenticationRequestParameters((String) obj, str3, str4, str5, jSONString, bVar.f.getCurrent());
            }
            throw new SDKRuntimeException(new RuntimeException(r2));
        }
        throw null;
    }

    public String getInitialChallengeUiType() {
        return this.f70a;
    }

    public ProgressDialog getProgressView(Activity activity) throws InvalidInputException {
        Intrinsics.checkParameterIsNotNull(activity, "currentActivity");
        a.a.a.a.g.q qVar = this.c;
        q.a aVar = this.r;
        StripeUiCustomization stripeUiCustomization = this.q;
        if (stripeUiCustomization == null) {
            Intrinsics.throwNpe();
        }
        return qVar.a(activity, aVar, stripeUiCustomization);
    }

    public void setInitialChallengeUiType(String str) {
        this.f70a = str;
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000?\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0003\n\u0002\b\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\b\u0003\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000*\u0001\u0000\b\n\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\u0016J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0006\u001a\u00020\u0007H\u0016J\u0018\u0010\b\u001a\u00020\u00032\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\fH\u0016J\u0010\u0010\r\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\u0016J\u0010\u0010\u000e\u001a\u00020\u00032\u0006\u0010\u000f\u001a\u00020\u0010H\u0002¨\u0006\u0011¸\u0006\u0000"}, d2 = {"com/stripe/android/stripe3ds2/transaction/StripeTransaction$doChallenge$1$1", "Lcom/stripe/android/stripe3ds2/transaction/ChallengeRequestExecutor$Listener;", "onError", "", "data", "Lcom/stripe/android/stripe3ds2/transactions/ErrorData;", "throwable", "", "onSuccess", "creqData", "Lcom/stripe/android/stripe3ds2/transactions/ChallengeRequestData;", "cresData", "Lcom/stripe/android/stripe3ds2/transactions/ChallengeResponseData;", "onTimeout", "startChallengeCompletionIntent", "outcome", "Lcom/stripe/android/stripe3ds2/transaction/ChallengeFlowOutcome;", "3ds2sdk_release"}, k = 1, mv = {1, 1, 16})
    public static final class b implements f.c {

        /* renamed from: a  reason: collision with root package name */
        public final /* synthetic */ t f72a;
        public final /* synthetic */ f.a b;
        public final /* synthetic */ y.a c;
        public final /* synthetic */ k d;
        public final /* synthetic */ ChallengeStatusReceiver e;
        public final /* synthetic */ Stripe3ds2ActivityStarterHost f;
        public final /* synthetic */ ChallengeCompletionIntentStarter g;

        public static final class a extends Lambda implements Function0<Unit> {

            /* renamed from: a  reason: collision with root package name */
            public final /* synthetic */ b f73a;

            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(b bVar) {
                super(0);
                this.f73a = bVar;
            }

            public Object invoke() {
                b.a(this.f73a, ChallengeFlowOutcome.ProtocolError);
                return Unit.INSTANCE;
            }
        }

        /* renamed from: a.a.a.a.d.t$b$b  reason: collision with other inner class name */
        public static final class C0010b extends Lambda implements Function0<Unit> {

            /* renamed from: a  reason: collision with root package name */
            public final /* synthetic */ b f74a;

            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0010b(b bVar) {
                super(0);
                this.f74a = bVar;
            }

            public Object invoke() {
                b.a(this.f74a, ChallengeFlowOutcome.RuntimeError);
                return Unit.INSTANCE;
            }
        }

        public static final class c extends Lambda implements Function0<Unit> {

            /* renamed from: a  reason: collision with root package name */
            public final /* synthetic */ b f75a;

            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public c(b bVar) {
                super(0);
                this.f75a = bVar;
            }

            public Object invoke() {
                b.a(this.f75a, ChallengeFlowOutcome.RuntimeError);
                return Unit.INSTANCE;
            }
        }

        public b(t tVar, f.a aVar, y.a aVar2, k kVar, int i, ChallengeStatusReceiver challengeStatusReceiver, ChallengeParameters challengeParameters, Stripe3ds2ActivityStarterHost stripe3ds2ActivityStarterHost, ChallengeCompletionIntentStarter challengeCompletionIntentStarter) {
            this.f72a = tVar;
            this.b = aVar;
            this.c = aVar2;
            this.d = kVar;
            this.e = challengeStatusReceiver;
            this.f = stripe3ds2ActivityStarterHost;
            this.g = challengeCompletionIntentStarter;
        }

        public static final /* synthetic */ void a(b bVar, ChallengeFlowOutcome challengeFlowOutcome) {
            Intent intent = bVar.f72a.t;
            if (intent != null) {
                bVar.g.start(intent, challengeFlowOutcome);
            }
        }

        public void a(Throwable th) {
            Intrinsics.checkParameterIsNotNull(th, "throwable");
            this.e.runtimeError(new RuntimeErrorEvent(th), new c(this));
        }

        public void b(a.a.a.a.e.c cVar) {
            Intrinsics.checkParameterIsNotNull(cVar, "data");
            this.c.a();
            t tVar = this.f72a;
            k kVar = this.d;
            if (tVar != null) {
                kVar.a(cVar);
                this.e.protocolError(this.f72a.h.a(cVar), new a(this));
                return;
            }
            throw null;
        }

        public void a(a.a.a.a.e.a aVar, ChallengeResponseData challengeResponseData) {
            Intrinsics.checkParameterIsNotNull(aVar, "creqData");
            Intrinsics.checkParameterIsNotNull(challengeResponseData, "cresData");
            t tVar = this.f72a;
            Stripe3ds2ActivityStarterHost stripe3ds2ActivityStarterHost = this.f;
            StripeUiCustomization stripeUiCustomization = tVar.q;
            if (stripeUiCustomization == null) {
                Intrinsics.throwNpe();
            }
            f.a aVar2 = this.b;
            String str = null;
            if (tVar != null) {
                ChallengeResponseData.c uiType = challengeResponseData.getUiType();
                if (uiType != null) {
                    str = uiType.f128a;
                }
                tVar.f70a = str;
                i.a.a(i.d, stripe3ds2ActivityStarterHost, aVar, challengeResponseData, stripeUiCustomization, aVar2, (f.b) null, (k.a) null, tVar.t, tVar.u, 96).b();
                return;
            }
            throw null;
        }

        public void a(a.a.a.a.e.c cVar) {
            Intrinsics.checkParameterIsNotNull(cVar, "data");
            this.c.a();
            t tVar = this.f72a;
            k kVar = this.d;
            if (tVar != null) {
                kVar.a(cVar);
                this.e.runtimeError(new RuntimeErrorEvent(cVar), new C0010b(this));
                return;
            }
            throw null;
        }
    }

    public void doChallenge(Stripe3ds2ActivityStarterHost stripe3ds2ActivityStarterHost, ChallengeParameters challengeParameters, ChallengeStatusReceiver challengeStatusReceiver, int i2) {
        Object obj;
        Stripe3ds2ActivityStarterHost stripe3ds2ActivityStarterHost2 = stripe3ds2ActivityStarterHost;
        ChallengeStatusReceiver challengeStatusReceiver2 = challengeStatusReceiver;
        Intrinsics.checkParameterIsNotNull(stripe3ds2ActivityStarterHost2, "host");
        Intrinsics.checkParameterIsNotNull(challengeParameters, "challengeParameters");
        Intrinsics.checkParameterIsNotNull(challengeStatusReceiver2, "challengeStatusReceiver");
        this.s.a("Starting challenge flow.");
        ChallengeCompletionIntentStarter challengeCompletionIntentStarter = new ChallengeCompletionIntentStarter(stripe3ds2ActivityStarterHost2, this.u);
        try {
            Result.Companion companion = Result.Companion;
            if (i2 >= 5) {
                this.d.a(this.l, challengeStatusReceiver2);
                String acsSignedContent = challengeParameters.getAcsSignedContent();
                if (acsSignedContent != null) {
                    a a2 = a(acsSignedContent, this.n, this.o);
                    String str = a2.f35a;
                    ECPublicKey eCPublicKey = a2.b;
                    String acsTransactionId = challengeParameters.getAcsTransactionId();
                    if (acsTransactionId != null) {
                        String threeDsServerTransactionId = challengeParameters.getThreeDsServerTransactionId();
                        if (threeDsServerTransactionId != null) {
                            a.a.a.a.e.a aVar = new a.a.a.a.e.a(this.e.getCurrent(), threeDsServerTransactionId, acsTransactionId, this.l, (String) null, (a.C0011a) null, (String) null, (List) null, (Boolean) null, (Boolean) null, PointerIconCompat.TYPE_TEXT);
                            Intrinsics.checkParameterIsNotNull(str, "acsUrl");
                            r rVar = new r(new s(str), CoroutineScopeKt.CoroutineScope(Dispatchers.getIO()));
                            y.a aVar2 = r1;
                            a.a.a.a.e.a aVar3 = aVar;
                            y.a aVar4 = new y.a(challengeStatusReceiver, i2, rVar, aVar, z.a.b, CoroutineScopeKt.CoroutineScope(Dispatchers.getMain().plus(SupervisorKt.SupervisorJob$default((Job) null, 1, (Object) null))));
                            aVar2.g.a(aVar2.f.d, aVar2);
                            aVar2.c = BuildersKt__Builders_commonKt.launch$default(aVar2.h, (CoroutineContext) null, (CoroutineStart) null, new x(aVar2, (Continuation) null), 3, (Object) null);
                            a.a.a.a.c.i iVar = this.p;
                            String str2 = this.f;
                            PrivateKey privateKey = this.m.getPrivate();
                            Intrinsics.checkExpressionValueIsNotNull(privateKey, "sdkKeyPair.private");
                            byte[] encoded = privateKey.getEncoded();
                            Intrinsics.checkExpressionValueIsNotNull(encoded, "sdkKeyPair.private.encoded");
                            byte[] encoded2 = eCPublicKey.getEncoded();
                            Intrinsics.checkExpressionValueIsNotNull(encoded2, "acsEphemPubKey.encoded");
                            f.a aVar5 = new f.a(iVar, str2, encoded, encoded2, str, aVar3);
                            a.a.a.a.e.a aVar6 = aVar3;
                            new q.c().a(aVar5).b(aVar6, new b(this, aVar5, aVar2, rVar, i2, challengeStatusReceiver, challengeParameters, stripe3ds2ActivityStarterHost, challengeCompletionIntentStarter));
                            obj = Result.m4constructorimpl(Unit.INSTANCE);
                            Throwable r0 = Result.m7exceptionOrNullimpl(obj);
                            if (r0 != null) {
                                this.s.a("Exception during challenge flow.", r0);
                                challengeStatusReceiver2.runtimeError(new RuntimeErrorEvent(r0), new a(this, challengeStatusReceiver2, challengeCompletionIntentStarter));
                                return;
                            }
                            return;
                        }
                        throw new IllegalArgumentException("Required value was null.".toString());
                    }
                    throw new IllegalArgumentException("Required value was null.".toString());
                }
                throw new IllegalArgumentException("Required value was null.".toString());
            }
            throw new InvalidInputException(new RuntimeException("Timeout must be at least 5 minutes"));
        } catch (Throwable th) {
            Result.Companion companion2 = Result.Companion;
            obj = Result.m4constructorimpl(ResultKt.createFailure(th));
        }
    }
}
