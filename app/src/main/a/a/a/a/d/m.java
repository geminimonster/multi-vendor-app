package a.a.a.a.d;

import com.google.firebase.analytics.FirebaseAnalytics;
import kotlin.jvm.internal.Intrinsics;
import kotlin.text.StringsKt;

public final class m {

    /* renamed from: a  reason: collision with root package name */
    public final boolean f56a;
    public final String b;

    public m(String str, String str2) {
        Intrinsics.checkParameterIsNotNull(str, FirebaseAnalytics.Param.CONTENT);
        this.b = str;
        boolean z = true;
        this.f56a = (str2 == null || !StringsKt.startsWith$default(str2, "application/json", false, 2, (Object) null)) ? false : z;
    }
}
