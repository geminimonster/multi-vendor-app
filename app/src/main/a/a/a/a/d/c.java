package a.a.a.a.d;

import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b0\u0018\u00002\u00020\u0001:\u0005\u0003\u0004\u0005\u0006\u0007B\u0007\b\u0002¢\u0006\u0002\u0010\u0002\u0001\u0005\b\t\n\u000b\f¨\u0006\r"}, d2 = {"Lcom/stripe/android/stripe3ds2/transaction/ChallengeAction;", "", "()V", "Cancel", "HtmlForm", "NativeForm", "Oob", "Resend", "Lcom/stripe/android/stripe3ds2/transaction/ChallengeAction$NativeForm;", "Lcom/stripe/android/stripe3ds2/transaction/ChallengeAction$HtmlForm;", "Lcom/stripe/android/stripe3ds2/transaction/ChallengeAction$Oob;", "Lcom/stripe/android/stripe3ds2/transaction/ChallengeAction$Resend;", "Lcom/stripe/android/stripe3ds2/transaction/ChallengeAction$Cancel;", "3ds2sdk_release"}, k = 1, mv = {1, 1, 16})
public abstract class c {

    public static final class a extends c {

        /* renamed from: a  reason: collision with root package name */
        public static final a f37a = new a();

        public a() {
            super((DefaultConstructorMarker) null);
        }
    }

    public static final class b extends c {

        /* renamed from: a  reason: collision with root package name */
        public final String f38a;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(String str) {
            super((DefaultConstructorMarker) null);
            Intrinsics.checkParameterIsNotNull(str, "userEntry");
            this.f38a = str;
        }

        public boolean equals(Object obj) {
            if (this != obj) {
                return (obj instanceof b) && Intrinsics.areEqual((Object) this.f38a, (Object) ((b) obj).f38a);
            }
            return true;
        }

        public int hashCode() {
            String str = this.f38a;
            if (str != null) {
                return str.hashCode();
            }
            return 0;
        }

        public String toString() {
            return "HtmlForm(userEntry=" + this.f38a + ")";
        }
    }

    /* renamed from: a.a.a.a.d.c$c  reason: collision with other inner class name */
    public static final class C0001c extends c {

        /* renamed from: a  reason: collision with root package name */
        public final String f39a;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public C0001c(String str) {
            super((DefaultConstructorMarker) null);
            Intrinsics.checkParameterIsNotNull(str, "userEntry");
            this.f39a = str;
        }

        public boolean equals(Object obj) {
            if (this != obj) {
                return (obj instanceof C0001c) && Intrinsics.areEqual((Object) this.f39a, (Object) ((C0001c) obj).f39a);
            }
            return true;
        }

        public int hashCode() {
            String str = this.f39a;
            if (str != null) {
                return str.hashCode();
            }
            return 0;
        }

        public String toString() {
            return "NativeForm(userEntry=" + this.f39a + ")";
        }
    }

    public static final class d extends c {

        /* renamed from: a  reason: collision with root package name */
        public static final d f40a = new d();

        public d() {
            super((DefaultConstructorMarker) null);
        }
    }

    public static final class e extends c {

        /* renamed from: a  reason: collision with root package name */
        public static final e f41a = new e();

        public e() {
            super((DefaultConstructorMarker) null);
        }
    }

    public c() {
    }

    public /* synthetic */ c(DefaultConstructorMarker defaultConstructorMarker) {
        this();
    }
}
