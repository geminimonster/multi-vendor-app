package a.a.a.a.d;

import a.a.a.a.d.f;
import a.a.a.a.d.k;
import a.a.a.a.d.q;
import a.a.a.a.d.r;
import android.content.Intent;
import android.os.Bundle;
import com.stripe.android.stripe3ds2.init.ui.StripeUiCustomization;
import com.stripe.android.stripe3ds2.transaction.Stripe3ds2ActivityStarterHost;
import com.stripe.android.stripe3ds2.transactions.ChallengeResponseData;
import com.stripe.android.stripe3ds2.views.ChallengeActivity;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000R\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0002\b\u0002\u0018\u0000 \u001c2\u00020\u0001:\u0001\u001cBU\b\u0000\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\f\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u0012\n\b\u0002\u0010\u0010\u001a\u0004\u0018\u00010\u0011\u0012\b\b\u0002\u0010\u0012\u001a\u00020\u0013¢\u0006\u0002\u0010\u0014J\u0006\u0010\u001a\u001a\u00020\u001bR\u000e\u0010\u0015\u001a\u00020\u0016X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0013X\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\u0017\u001a\u00020\u00118BX\u0004¢\u0006\u0006\u001a\u0004\b\u0018\u0010\u0019R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\u001d"}, d2 = {"Lcom/stripe/android/stripe3ds2/transaction/ChallengeStarter;", "", "host", "Lcom/stripe/android/stripe3ds2/transaction/Stripe3ds2ActivityStarterHost;", "creqData", "Lcom/stripe/android/stripe3ds2/transactions/ChallengeRequestData;", "cresData", "Lcom/stripe/android/stripe3ds2/transactions/ChallengeResponseData;", "uiCustomization", "Lcom/stripe/android/stripe3ds2/init/ui/StripeUiCustomization;", "creqExecutorConfig", "Lcom/stripe/android/stripe3ds2/transaction/ChallengeRequestExecutor$Config;", "creqExecutorFactory", "Lcom/stripe/android/stripe3ds2/transaction/ChallengeRequestExecutor$Factory;", "errorExecutorFactory", "Lcom/stripe/android/stripe3ds2/transaction/ErrorRequestExecutor$Factory;", "challengeCompletionIntent", "Landroid/content/Intent;", "challengeCompletionRequestCode", "", "(Lcom/stripe/android/stripe3ds2/transaction/Stripe3ds2ActivityStarterHost;Lcom/stripe/android/stripe3ds2/transactions/ChallengeRequestData;Lcom/stripe/android/stripe3ds2/transactions/ChallengeResponseData;Lcom/stripe/android/stripe3ds2/init/ui/StripeUiCustomization;Lcom/stripe/android/stripe3ds2/transaction/ChallengeRequestExecutor$Config;Lcom/stripe/android/stripe3ds2/transaction/ChallengeRequestExecutor$Factory;Lcom/stripe/android/stripe3ds2/transaction/ErrorRequestExecutor$Factory;Landroid/content/Intent;I)V", "args", "Lcom/stripe/android/stripe3ds2/views/ChallengeViewArgs;", "challengeIntent", "getChallengeIntent", "()Landroid/content/Intent;", "start", "", "Companion", "3ds2sdk_release"}, k = 1, mv = {1, 1, 16})
public final class i {
    public static final a d = new a();

    /* renamed from: a  reason: collision with root package name */
    public final a.a.a.a.g.i f54a;
    public final Stripe3ds2ActivityStarterHost b;
    public final int c;

    public static final class a {
        public static /* synthetic */ i a(a aVar, Stripe3ds2ActivityStarterHost stripe3ds2ActivityStarterHost, a.a.a.a.e.a aVar2, ChallengeResponseData challengeResponseData, StripeUiCustomization stripeUiCustomization, f.a aVar3, f.b bVar, k.a aVar4, Intent intent, int i, int i2) {
            int i3 = i2;
            q.c cVar = (i3 & 32) != 0 ? new q.c() : bVar;
            r.b bVar2 = (i3 & 64) != 0 ? new r.b() : aVar4;
            Intent intent2 = (i3 & 128) != 0 ? null : intent;
            int i4 = (i3 & 256) != 0 ? 0 : i;
            if (aVar != null) {
                Stripe3ds2ActivityStarterHost stripe3ds2ActivityStarterHost2 = stripe3ds2ActivityStarterHost;
                Intrinsics.checkParameterIsNotNull(stripe3ds2ActivityStarterHost, "host");
                Intrinsics.checkParameterIsNotNull(aVar2, "creqData");
                Intrinsics.checkParameterIsNotNull(challengeResponseData, "cresData");
                StripeUiCustomization stripeUiCustomization2 = stripeUiCustomization;
                Intrinsics.checkParameterIsNotNull(stripeUiCustomization2, "uiCustomization");
                f.a aVar5 = aVar3;
                Intrinsics.checkParameterIsNotNull(aVar5, "creqExecutorConfig");
                Intrinsics.checkParameterIsNotNull(cVar, "creqExecutorFactory");
                Intrinsics.checkParameterIsNotNull(bVar2, "errorRequestExecutor");
                return new i(stripe3ds2ActivityStarterHost, aVar2, challengeResponseData, stripeUiCustomization2, aVar5, cVar, bVar2, intent2, i4);
            }
            throw null;
        }
    }

    public i(Stripe3ds2ActivityStarterHost stripe3ds2ActivityStarterHost, a.a.a.a.e.a aVar, ChallengeResponseData challengeResponseData, StripeUiCustomization stripeUiCustomization, f.a aVar2, f.b bVar, k.a aVar3, Intent intent, int i) {
        Intrinsics.checkParameterIsNotNull(stripe3ds2ActivityStarterHost, "host");
        Intrinsics.checkParameterIsNotNull(aVar, "creqData");
        Intrinsics.checkParameterIsNotNull(challengeResponseData, "cresData");
        StripeUiCustomization stripeUiCustomization2 = stripeUiCustomization;
        Intrinsics.checkParameterIsNotNull(stripeUiCustomization2, "uiCustomization");
        f.a aVar4 = aVar2;
        Intrinsics.checkParameterIsNotNull(aVar4, "creqExecutorConfig");
        f.b bVar2 = bVar;
        Intrinsics.checkParameterIsNotNull(bVar2, "creqExecutorFactory");
        k.a aVar5 = aVar3;
        Intrinsics.checkParameterIsNotNull(aVar5, "errorExecutorFactory");
        this.b = stripe3ds2ActivityStarterHost;
        this.c = i;
        this.f54a = new a.a.a.a.g.i(challengeResponseData, aVar, stripeUiCustomization2, aVar4, bVar2, aVar5, intent, this.c);
    }

    public final Intent a() {
        Intent intent = new Intent(this.b.getActivity$3ds2sdk_release(), ChallengeActivity.class);
        a.a.a.a.g.i iVar = this.f54a;
        if (iVar != null) {
            Bundle bundle = new Bundle();
            bundle.putSerializable("extra_creq_data", iVar.b);
            bundle.putParcelable("extra_cres_data", iVar.f104a);
            bundle.putParcelable("extra_ui_customization", iVar.c);
            bundle.putSerializable("extra_creq_executor_config", iVar.d);
            bundle.putSerializable("extra_creq_executor_factory", iVar.e);
            bundle.putSerializable("extra_error_executor_factory", iVar.f);
            bundle.putParcelable("extra_challenge_completion_intent", iVar.g);
            bundle.putInt("extra_challenge_completion_request_code", iVar.h);
            Intent putExtras = intent.putExtras(bundle);
            Intrinsics.checkExpressionValueIsNotNull(putExtras, "Intent(host.activity, Ch…utExtras(args.toBundle())");
            return putExtras;
        }
        throw null;
    }

    public final void b() {
        if (this.c > 0) {
            this.b.startActivityForResult$3ds2sdk_release(a(), this.c);
            return;
        }
        Stripe3ds2ActivityStarterHost stripe3ds2ActivityStarterHost = this.b;
        Intent addFlags = a().addFlags(33554432);
        Intrinsics.checkExpressionValueIsNotNull(addFlags, "challengeIntent\n        …_ACTIVITY_FORWARD_RESULT)");
        stripe3ds2ActivityStarterHost.startActivity$3ds2sdk_release(addFlags);
    }
}
