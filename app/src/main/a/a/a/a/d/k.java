package a.a.a.a.d;

import a.a.a.a.e.c;
import java.io.Serializable;
import kotlin.Metadata;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\bf\u0018\u00002\u00020\u0001:\u0001\u0006J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&¨\u0006\u0007"}, d2 = {"Lcom/stripe/android/stripe3ds2/transaction/ErrorRequestExecutor;", "", "executeAsync", "", "errorData", "Lcom/stripe/android/stripe3ds2/transactions/ErrorData;", "Factory", "3ds2sdk_release"}, k = 1, mv = {1, 1, 16})
public interface k {

    public interface a extends Serializable {
        k create(String str);
    }

    void a(c cVar);
}
