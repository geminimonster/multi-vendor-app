package a.a.a.a.d;

import a.a.a.a.e.c;
import com.stripe.android.stripe3ds2.transaction.ErrorMessage;
import com.stripe.android.stripe3ds2.transaction.ProtocolErrorEvent;
import kotlin.jvm.internal.Intrinsics;

public final class p {
    public final ProtocolErrorEvent a(c cVar) {
        Intrinsics.checkParameterIsNotNull(cVar, "errorData");
        String str = cVar.b;
        if (str == null) {
            str = "";
        }
        return new ProtocolErrorEvent(cVar.j, new ErrorMessage(str, cVar.d, cVar.f, cVar.g));
    }
}
