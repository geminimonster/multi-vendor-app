package a.a.a.a.d;

import a.a.a.a.d.d;
import a.a.a.a.e.a;
import com.stripe.android.stripe3ds2.exceptions.SDKRuntimeException;
import kotlin.Result;
import kotlin.ResultKt;
import kotlin.Unit;
import kotlin.coroutines.Continuation;
import kotlin.coroutines.intrinsics.IntrinsicsKt;
import kotlin.coroutines.jvm.internal.DebugMetadata;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.Intrinsics;
import kotlinx.coroutines.CoroutineScope;
import kotlinx.coroutines.DelayKt;

@DebugMetadata(c = "com.stripe.android.stripe3ds2.transaction.ChallengeActionHandler$Default$executeChallengeRequest$1", f = "ChallengeActionHandler.kt", i = {0}, l = {111}, m = "invokeSuspend", n = {"$this$launch"}, s = {"L$0"})
public final class e extends SuspendLambda implements Function2<CoroutineScope, Continuation<? super Unit>, Object> {

    /* renamed from: a  reason: collision with root package name */
    public CoroutineScope f49a;
    public Object b;
    public int c;
    public final /* synthetic */ d.a d;
    public final /* synthetic */ a e;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public e(d.a aVar, a aVar2, Continuation continuation) {
        super(2, continuation);
        this.d = aVar;
        this.e = aVar2;
    }

    public final Continuation<Unit> create(Object obj, Continuation<?> continuation) {
        Intrinsics.checkParameterIsNotNull(continuation, "completion");
        e eVar = new e(this.d, this.e, continuation);
        eVar.f49a = (CoroutineScope) obj;
        return eVar;
    }

    public final Object invoke(Object obj, Object obj2) {
        return ((e) create(obj, (Continuation) obj2)).invokeSuspend(Unit.INSTANCE);
    }

    public final Object invokeSuspend(Object obj) {
        Object obj2;
        Object coroutine_suspended = IntrinsicsKt.getCOROUTINE_SUSPENDED();
        int i = this.c;
        if (i == 0) {
            ResultKt.throwOnFailure(obj);
            CoroutineScope coroutineScope = this.f49a;
            d.a.C0002a aVar = d.a.g;
            long j = d.a.f;
            this.b = coroutineScope;
            this.c = 1;
            if (DelayKt.delay(j, this) == coroutine_suspended) {
                return coroutine_suspended;
            }
        } else if (i == 1) {
            CoroutineScope coroutineScope2 = (CoroutineScope) this.b;
            ResultKt.throwOnFailure(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        try {
            Result.Companion companion = Result.Companion;
            this.d.c.a(this.e, this.d.f42a);
            obj2 = Result.m4constructorimpl(Unit.INSTANCE);
        } catch (Throwable th) {
            Result.Companion companion2 = Result.Companion;
            obj2 = Result.m4constructorimpl(ResultKt.createFailure(th));
        }
        Throwable r6 = Result.m7exceptionOrNullimpl(obj2);
        if (r6 == null) {
            return Unit.INSTANCE;
        }
        throw new SDKRuntimeException(new RuntimeException(r6));
    }
}
