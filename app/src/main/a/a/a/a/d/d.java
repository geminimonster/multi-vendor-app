package a.a.a.a.d;

import a.a.a.a.d.c;
import a.a.a.a.d.f;
import a.a.a.a.d.i;
import a.a.a.a.d.k;
import a.a.a.a.e.a;
import android.app.Activity;
import android.content.Intent;
import androidx.core.view.PointerIconCompat;
import com.facebook.internal.NativeProtocol;
import com.stripe.android.stripe3ds2.init.ui.StripeUiCustomization;
import com.stripe.android.stripe3ds2.transaction.ChallengeCompletionIntentStarter;
import com.stripe.android.stripe3ds2.transaction.ChallengeFlowOutcome;
import com.stripe.android.stripe3ds2.transaction.ChallengeStatusReceiver;
import com.stripe.android.stripe3ds2.transaction.CompletionEvent;
import com.stripe.android.stripe3ds2.transaction.ErrorMessage;
import com.stripe.android.stripe3ds2.transaction.ProtocolErrorEvent;
import com.stripe.android.stripe3ds2.transaction.RuntimeErrorEvent;
import com.stripe.android.stripe3ds2.transaction.Stripe3ds2ActivityStarterHost;
import com.stripe.android.stripe3ds2.transactions.ChallengeResponseData;
import java.lang.ref.WeakReference;
import java.util.List;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.Unit;
import kotlin.coroutines.Continuation;
import kotlin.coroutines.CoroutineContext;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;
import kotlinx.coroutines.CoroutineScope;
import kotlinx.coroutines.CoroutineStart;
import kotlinx.coroutines.Job;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\b`\u0018\u00002\u00020\u0001:\u0001\u0006J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&¨\u0006\u0007"}, d2 = {"Lcom/stripe/android/stripe3ds2/transaction/ChallengeActionHandler;", "", "submit", "", "action", "Lcom/stripe/android/stripe3ds2/transaction/ChallengeAction;", "Default", "3ds2sdk_release"}, k = 1, mv = {1, 1, 16})
public interface d {

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000r\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u0000 %2\u00020\u0001:\u0002%&BK\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\f\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u0012\n\b\u0002\u0010\u0010\u001a\u0004\u0018\u00010\u0011¢\u0006\u0002\u0010\u0012Be\b\u0000\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t\u0012\u0006\u0010\u0013\u001a\u00020\u0014\u0012\u0006\u0010\u0015\u001a\u00020\u0016\u0012\u0006\u0010\f\u001a\u00020\r\u0012\u0006\u0010\u0017\u001a\u00020\u0018\u0012\u0006\u0010\u0019\u001a\u00020\u001a\u0012\b\b\u0002\u0010\u001b\u001a\u00020\u001c\u0012\n\b\u0002\u0010\u0010\u001a\u0004\u0018\u00010\u0011¢\u0006\u0002\u0010\u001dJ\u0010\u0010 \u001a\u00020!2\u0006\u0010\u0004\u001a\u00020\u0005H\u0002J\u0010\u0010\"\u001a\u00020!2\u0006\u0010#\u001a\u00020$H\u0016R\u0010\u0010\u0010\u001a\u0004\u0018\u00010\u0011X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0017\u001a\u00020\u0018X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u001b\u001a\u00020\u001cX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u001e\u001a\u00020\u001fX\u0004¢\u0006\u0002\n\u0000¨\u0006'"}, d2 = {"Lcom/stripe/android/stripe3ds2/transaction/ChallengeActionHandler$Default;", "Lcom/stripe/android/stripe3ds2/transaction/ChallengeActionHandler;", "activity", "Landroid/app/Activity;", "creqData", "Lcom/stripe/android/stripe3ds2/transactions/ChallengeRequestData;", "uiTypeCode", "", "uiCustomization", "Lcom/stripe/android/stripe3ds2/init/ui/StripeUiCustomization;", "creqExecutorFactory", "Lcom/stripe/android/stripe3ds2/transaction/ChallengeRequestExecutor$Factory;", "creqExecutorConfig", "Lcom/stripe/android/stripe3ds2/transaction/ChallengeRequestExecutor$Config;", "errorExecutorFactory", "Lcom/stripe/android/stripe3ds2/transaction/ErrorRequestExecutor$Factory;", "challengeCompletionIntent", "Landroid/content/Intent;", "(Landroid/app/Activity;Lcom/stripe/android/stripe3ds2/transactions/ChallengeRequestData;Ljava/lang/String;Lcom/stripe/android/stripe3ds2/init/ui/StripeUiCustomization;Lcom/stripe/android/stripe3ds2/transaction/ChallengeRequestExecutor$Factory;Lcom/stripe/android/stripe3ds2/transaction/ChallengeRequestExecutor$Config;Lcom/stripe/android/stripe3ds2/transaction/ErrorRequestExecutor$Factory;Landroid/content/Intent;)V", "challengeStatusReceiver", "Lcom/stripe/android/stripe3ds2/transaction/ChallengeStatusReceiver;", "transactionTimer", "Lcom/stripe/android/stripe3ds2/transaction/TransactionTimer;", "challengeRequestExecutor", "Lcom/stripe/android/stripe3ds2/transaction/ChallengeRequestExecutor;", "errorRequestExecutor", "Lcom/stripe/android/stripe3ds2/transaction/ErrorRequestExecutor;", "coroutineScope", "Lkotlinx/coroutines/CoroutineScope;", "(Landroid/app/Activity;Lcom/stripe/android/stripe3ds2/transactions/ChallengeRequestData;Ljava/lang/String;Lcom/stripe/android/stripe3ds2/init/ui/StripeUiCustomization;Lcom/stripe/android/stripe3ds2/transaction/ChallengeStatusReceiver;Lcom/stripe/android/stripe3ds2/transaction/TransactionTimer;Lcom/stripe/android/stripe3ds2/transaction/ChallengeRequestExecutor$Config;Lcom/stripe/android/stripe3ds2/transaction/ChallengeRequestExecutor;Lcom/stripe/android/stripe3ds2/transaction/ErrorRequestExecutor;Lkotlinx/coroutines/CoroutineScope;Landroid/content/Intent;)V", "listener", "Lcom/stripe/android/stripe3ds2/transaction/ChallengeActionHandler$Default$RequestListener;", "executeChallengeRequest", "", "submit", "action", "Lcom/stripe/android/stripe3ds2/transaction/ChallengeAction;", "Companion", "RequestListener", "3ds2sdk_release"}, k = 1, mv = {1, 1, 16})
    public static final class a implements d {
        public static final long f = TimeUnit.SECONDS.toMillis(1);
        public static final C0002a g = new C0002a();

        /* renamed from: a  reason: collision with root package name */
        public final b f42a;
        public final a.a.a.a.e.a b;
        public final f c;
        public final CoroutineScope d;
        public final Intent e;

        /* renamed from: a.a.a.a.d.d$a$a  reason: collision with other inner class name */
        public static final class C0002a {
        }

        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000j\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0003\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\b\u0002\u0018\u00002\u00020\u0001BQ\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\f\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u0012\u0006\u0010\u0010\u001a\u00020\u0011\u0012\n\b\u0002\u0010\u0012\u001a\u0004\u0018\u00010\u0013¢\u0006\u0002\u0010\u0014J\u0010\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00020\u001aH\u0016J\u0010\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u001b\u001a\u00020\u001cH\u0016J\u0018\u0010\u001d\u001a\u00020\u00182\u0006\u0010\b\u001a\u00020\t2\u0006\u0010\u001e\u001a\u00020\u001fH\u0016J\u0010\u0010 \u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00020\u001aH\u0016J\u0010\u0010!\u001a\u00020\u00182\u0006\u0010\"\u001a\u00020#H\u0002R\u0014\u0010\u0015\u001a\b\u0012\u0004\u0012\u00020\u00110\u0016X\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u0012\u001a\u0004\u0018\u00010\u0013X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\rX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0004¢\u0006\u0002\n\u0000¨\u0006$"}, d2 = {"Lcom/stripe/android/stripe3ds2/transaction/ChallengeActionHandler$Default$RequestListener;", "Lcom/stripe/android/stripe3ds2/transaction/ChallengeRequestExecutor$Listener;", "transactionTimer", "Lcom/stripe/android/stripe3ds2/transaction/TransactionTimer;", "errorRequestExecutor", "Lcom/stripe/android/stripe3ds2/transaction/ErrorRequestExecutor;", "requestExecutorConfig", "Lcom/stripe/android/stripe3ds2/transaction/ChallengeRequestExecutor$Config;", "creqData", "Lcom/stripe/android/stripe3ds2/transactions/ChallengeRequestData;", "uiTypeCode", "", "challengeStatusReceiver", "Lcom/stripe/android/stripe3ds2/transaction/ChallengeStatusReceiver;", "uiCustomization", "Lcom/stripe/android/stripe3ds2/init/ui/StripeUiCustomization;", "activity", "Landroid/app/Activity;", "challengeCompletionIntent", "Landroid/content/Intent;", "(Lcom/stripe/android/stripe3ds2/transaction/TransactionTimer;Lcom/stripe/android/stripe3ds2/transaction/ErrorRequestExecutor;Lcom/stripe/android/stripe3ds2/transaction/ChallengeRequestExecutor$Config;Lcom/stripe/android/stripe3ds2/transactions/ChallengeRequestData;Ljava/lang/String;Lcom/stripe/android/stripe3ds2/transaction/ChallengeStatusReceiver;Lcom/stripe/android/stripe3ds2/init/ui/StripeUiCustomization;Landroid/app/Activity;Landroid/content/Intent;)V", "activityRef", "Ljava/lang/ref/WeakReference;", "onError", "", "data", "Lcom/stripe/android/stripe3ds2/transactions/ErrorData;", "throwable", "", "onSuccess", "cresData", "Lcom/stripe/android/stripe3ds2/transactions/ChallengeResponseData;", "onTimeout", "startChallengeCompletionIntent", "outcome", "Lcom/stripe/android/stripe3ds2/transaction/ChallengeFlowOutcome;", "3ds2sdk_release"}, k = 1, mv = {1, 1, 16})
        public static final class b implements f.c {

            /* renamed from: a  reason: collision with root package name */
            public final WeakReference<Activity> f43a;
            public final y b;
            public final k c;
            public final f.a d;
            public final a.a.a.a.e.a e;
            public final String f;
            public final ChallengeStatusReceiver g;
            public final StripeUiCustomization h;
            public final Intent i;

            /* renamed from: a.a.a.a.d.d$a$b$a  reason: collision with other inner class name */
            public static final class C0003a extends Lambda implements Function0<Unit> {

                /* renamed from: a  reason: collision with root package name */
                public final /* synthetic */ b f44a;

                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0003a(b bVar) {
                    super(0);
                    this.f44a = bVar;
                }

                public Object invoke() {
                    b.a(this.f44a, ChallengeFlowOutcome.ProtocolError);
                    Activity activity = (Activity) this.f44a.f43a.get();
                    if (activity != null) {
                        activity.finish();
                    }
                    return Unit.INSTANCE;
                }
            }

            /* renamed from: a.a.a.a.d.d$a$b$b  reason: collision with other inner class name */
            public static final class C0004b extends Lambda implements Function0<Unit> {

                /* renamed from: a  reason: collision with root package name */
                public final /* synthetic */ b f45a;

                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0004b(b bVar) {
                    super(0);
                    this.f45a = bVar;
                }

                public Object invoke() {
                    b.a(this.f45a, ChallengeFlowOutcome.RuntimeError);
                    return Unit.INSTANCE;
                }
            }

            public static final class c extends Lambda implements Function0<Unit> {

                /* renamed from: a  reason: collision with root package name */
                public final /* synthetic */ b f46a;
                public final /* synthetic */ Activity b;

                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public c(b bVar, Activity activity) {
                    super(0);
                    this.f46a = bVar;
                    this.b = activity;
                }

                public Object invoke() {
                    b.a(this.f46a, ChallengeFlowOutcome.Cancel);
                    Activity activity = this.b;
                    if (activity != null) {
                        activity.finish();
                    }
                    return Unit.INSTANCE;
                }
            }

            /* renamed from: a.a.a.a.d.d$a$b$d  reason: collision with other inner class name */
            public static final class C0005d extends Lambda implements Function0<Unit> {

                /* renamed from: a  reason: collision with root package name */
                public final /* synthetic */ b f47a;
                public final /* synthetic */ String b;
                public final /* synthetic */ Activity c;

                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0005d(b bVar, String str, Activity activity) {
                    super(0);
                    this.f47a = bVar;
                    this.b = str;
                    this.c = activity;
                }

                public Object invoke() {
                    b.a(this.f47a, Intrinsics.areEqual((Object) "Y", (Object) this.b) ? ChallengeFlowOutcome.CompleteSuccessful : ChallengeFlowOutcome.CompleteUnsuccessful);
                    Activity activity = this.c;
                    if (activity != null) {
                        activity.finish();
                    }
                    return Unit.INSTANCE;
                }
            }

            public static final class e extends Lambda implements Function0<Unit> {

                /* renamed from: a  reason: collision with root package name */
                public final /* synthetic */ b f48a;

                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public e(b bVar) {
                    super(0);
                    this.f48a = bVar;
                }

                public Object invoke() {
                    b.a(this.f48a, ChallengeFlowOutcome.RuntimeError);
                    return Unit.INSTANCE;
                }
            }

            public b(y yVar, k kVar, f.a aVar, a.a.a.a.e.a aVar2, String str, ChallengeStatusReceiver challengeStatusReceiver, StripeUiCustomization stripeUiCustomization, Activity activity, Intent intent) {
                Intrinsics.checkParameterIsNotNull(yVar, "transactionTimer");
                Intrinsics.checkParameterIsNotNull(kVar, "errorRequestExecutor");
                Intrinsics.checkParameterIsNotNull(aVar, "requestExecutorConfig");
                Intrinsics.checkParameterIsNotNull(aVar2, "creqData");
                Intrinsics.checkParameterIsNotNull(str, "uiTypeCode");
                Intrinsics.checkParameterIsNotNull(challengeStatusReceiver, "challengeStatusReceiver");
                Intrinsics.checkParameterIsNotNull(stripeUiCustomization, "uiCustomization");
                Intrinsics.checkParameterIsNotNull(activity, "activity");
                this.b = yVar;
                this.c = kVar;
                this.d = aVar;
                this.e = aVar2;
                this.f = str;
                this.g = challengeStatusReceiver;
                this.h = stripeUiCustomization;
                this.i = intent;
                this.f43a = new WeakReference<>(activity);
            }

            public static final /* synthetic */ void a(b bVar, ChallengeFlowOutcome challengeFlowOutcome) {
                Activity activity = (Activity) bVar.f43a.get();
                if (activity != null && bVar.i != null) {
                    Intrinsics.checkExpressionValueIsNotNull(activity, "it");
                    new ChallengeCompletionIntentStarter(new Stripe3ds2ActivityStarterHost(activity), 0, 2, (DefaultConstructorMarker) null).start(bVar.i, challengeFlowOutcome);
                }
            }

            public void a(a.a.a.a.e.c cVar) {
                Intrinsics.checkParameterIsNotNull(cVar, "data");
                this.b.a();
                this.c.a(cVar);
                this.g.runtimeError(new RuntimeErrorEvent(cVar), new e(this));
                Activity activity = (Activity) this.f43a.get();
                if (activity != null) {
                    activity.finish();
                }
            }

            public void a(Throwable th) {
                Intrinsics.checkParameterIsNotNull(th, "throwable");
                this.g.runtimeError(new RuntimeErrorEvent(th), new C0004b(this));
            }

            public void b(a.a.a.a.e.c cVar) {
                Intrinsics.checkParameterIsNotNull(cVar, "data");
                ChallengeStatusReceiver challengeStatusReceiver = this.g;
                Intrinsics.checkParameterIsNotNull(cVar, "errorData");
                String str = cVar.b;
                if (str == null) {
                    str = "";
                }
                challengeStatusReceiver.protocolError(new ProtocolErrorEvent(cVar.j, new ErrorMessage(str, cVar.d, cVar.f, cVar.g)), new C0003a(this));
                this.b.a();
                this.c.a(cVar);
            }

            public void a(a.a.a.a.e.a aVar, ChallengeResponseData challengeResponseData) {
                a.a.a.a.e.a aVar2 = aVar;
                Intrinsics.checkParameterIsNotNull(aVar, "creqData");
                Intrinsics.checkParameterIsNotNull(challengeResponseData, "cresData");
                Activity activity = (Activity) this.f43a.get();
                if (challengeResponseData.isChallengeCompleted()) {
                    this.b.a();
                    if (aVar2.f != null) {
                        this.g.cancelled(this.f, new c(this, activity));
                        return;
                    }
                    String transStatus = challengeResponseData.getTransStatus();
                    if (transStatus == null) {
                        transStatus = "";
                    }
                    this.g.completed(new CompletionEvent(challengeResponseData.getSdkTransId(), transStatus), this.f, new C0005d(this, transStatus, activity));
                } else if (activity != null) {
                    i.a.a(i.d, new Stripe3ds2ActivityStarterHost(activity), this.e, challengeResponseData, this.h, this.d, (f.b) null, (k.a) null, this.i, 0, 352).b();
                    activity.finish();
                }
            }
        }

        /* JADX WARNING: Illegal instructions before constructor call */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public a(android.app.Activity r16, a.a.a.a.e.a r17, java.lang.String r18, com.stripe.android.stripe3ds2.init.ui.StripeUiCustomization r19, a.a.a.a.d.f.b r20, a.a.a.a.d.f.a r21, a.a.a.a.d.k.a r22, android.content.Intent r23) {
            /*
                r15 = this;
                r0 = r22
                java.lang.String r1 = "activity"
                r3 = r16
                kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(r3, r1)
                java.lang.String r1 = "creqData"
                r4 = r17
                kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(r4, r1)
                java.lang.String r1 = "uiTypeCode"
                r5 = r18
                kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(r5, r1)
                java.lang.String r1 = "uiCustomization"
                r6 = r19
                kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(r6, r1)
                java.lang.String r1 = "creqExecutorFactory"
                r2 = r20
                kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(r2, r1)
                java.lang.String r1 = "creqExecutorConfig"
                r9 = r21
                kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(r9, r1)
                java.lang.String r1 = "errorExecutorFactory"
                kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(r0, r1)
                a.a.a.a.d.j$a r1 = a.a.a.a.d.j.a.b
                java.lang.String r7 = r17.a()
                com.stripe.android.stripe3ds2.transaction.ChallengeStatusReceiver r7 = r1.a(r7)
                a.a.a.a.d.z$a r1 = a.a.a.a.d.z.a.b
                java.lang.String r8 = r17.a()
                a.a.a.a.d.y r8 = r1.b(r8)
                a.a.a.a.d.f r10 = r20.a(r21)
                java.lang.String r1 = r21.a()
                a.a.a.a.d.k r11 = r0.create(r1)
                r12 = 0
                r14 = 512(0x200, float:7.175E-43)
                r2 = r15
                r13 = r23
                r2.<init>(r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14)
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: a.a.a.a.d.d.a.<init>(android.app.Activity, a.a.a.a.e.a, java.lang.String, com.stripe.android.stripe3ds2.init.ui.StripeUiCustomization, a.a.a.a.d.f$b, a.a.a.a.d.f$a, a.a.a.a.d.k$a, android.content.Intent):void");
        }

        public a(Activity activity, a.a.a.a.e.a aVar, String str, StripeUiCustomization stripeUiCustomization, ChallengeStatusReceiver challengeStatusReceiver, y yVar, f.a aVar2, f fVar, k kVar, CoroutineScope coroutineScope, Intent intent) {
            a.a.a.a.e.a aVar3 = aVar;
            f fVar2 = fVar;
            CoroutineScope coroutineScope2 = coroutineScope;
            Intrinsics.checkParameterIsNotNull(activity, "activity");
            Intrinsics.checkParameterIsNotNull(aVar3, "creqData");
            String str2 = str;
            Intrinsics.checkParameterIsNotNull(str2, "uiTypeCode");
            StripeUiCustomization stripeUiCustomization2 = stripeUiCustomization;
            Intrinsics.checkParameterIsNotNull(stripeUiCustomization2, "uiCustomization");
            ChallengeStatusReceiver challengeStatusReceiver2 = challengeStatusReceiver;
            Intrinsics.checkParameterIsNotNull(challengeStatusReceiver2, "challengeStatusReceiver");
            Intrinsics.checkParameterIsNotNull(yVar, "transactionTimer");
            Intrinsics.checkParameterIsNotNull(aVar2, "creqExecutorConfig");
            Intrinsics.checkParameterIsNotNull(fVar2, "challengeRequestExecutor");
            Intrinsics.checkParameterIsNotNull(kVar, "errorRequestExecutor");
            Intrinsics.checkParameterIsNotNull(coroutineScope2, "coroutineScope");
            this.b = aVar3;
            this.c = fVar2;
            this.d = coroutineScope2;
            this.e = intent;
            this.f42a = new b(yVar, kVar, aVar2, aVar3, str2, challengeStatusReceiver2, stripeUiCustomization2, activity, intent);
        }

        /* JADX WARNING: Illegal instructions before constructor call */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public /* synthetic */ a(android.app.Activity r15, a.a.a.a.e.a r16, java.lang.String r17, com.stripe.android.stripe3ds2.init.ui.StripeUiCustomization r18, com.stripe.android.stripe3ds2.transaction.ChallengeStatusReceiver r19, a.a.a.a.d.y r20, a.a.a.a.d.f.a r21, a.a.a.a.d.f r22, a.a.a.a.d.k r23, kotlinx.coroutines.CoroutineScope r24, android.content.Intent r25, int r26) {
            /*
                r14 = this;
                r0 = r26
                r1 = r0 & 512(0x200, float:7.175E-43)
                if (r1 == 0) goto L_0x000c
                kotlinx.coroutines.CoroutineScope r1 = kotlinx.coroutines.CoroutineScopeKt.MainScope()
                r12 = r1
                goto L_0x000e
            L_0x000c:
                r12 = r24
            L_0x000e:
                r0 = r0 & 1024(0x400, float:1.435E-42)
                if (r0 == 0) goto L_0x0015
                r0 = 0
                r13 = r0
                goto L_0x0017
            L_0x0015:
                r13 = r25
            L_0x0017:
                r2 = r14
                r3 = r15
                r4 = r16
                r5 = r17
                r6 = r18
                r7 = r19
                r8 = r20
                r9 = r21
                r10 = r22
                r11 = r23
                r2.<init>(r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13)
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: a.a.a.a.d.d.a.<init>(android.app.Activity, a.a.a.a.e.a, java.lang.String, com.stripe.android.stripe3ds2.init.ui.StripeUiCustomization, com.stripe.android.stripe3ds2.transaction.ChallengeStatusReceiver, a.a.a.a.d.y, a.a.a.a.d.f$a, a.a.a.a.d.f, a.a.a.a.d.k, kotlinx.coroutines.CoroutineScope, android.content.Intent, int):void");
        }

        public void a(c cVar) {
            a.a.a.a.e.a aVar;
            a.C0011a aVar2;
            String str;
            String str2;
            String str3;
            String str4;
            String str5;
            String str6;
            List list;
            boolean z;
            boolean z2;
            int i;
            Intrinsics.checkParameterIsNotNull(cVar, NativeProtocol.WEB_DIALOG_ACTION);
            a.a.a.a.e.a aVar3 = this.b;
            a.a.a.a.e.a aVar4 = new a.a.a.a.e.a(aVar3.f82a, aVar3.b, aVar3.c, aVar3.d, (String) null, (a.C0011a) null, (String) null, aVar3.h, (Boolean) null, (Boolean) null, 880);
            if (cVar instanceof c.C0001c) {
                aVar = a.a.a.a.e.a.a(aVar4, (String) null, (String) null, (String) null, (String) null, ((c.C0001c) cVar).f39a, (a.C0011a) null, (String) null, (List) null, (Boolean) null, (Boolean) null, PointerIconCompat.TYPE_CROSSHAIR);
            } else {
                if (cVar instanceof c.b) {
                    str = null;
                    str2 = null;
                    str3 = null;
                    str4 = null;
                    str5 = null;
                    aVar2 = null;
                    str6 = ((c.b) cVar).f38a;
                    list = null;
                    z = null;
                    z2 = null;
                    i = 959;
                } else if (cVar instanceof c.d) {
                    z = true;
                    str = null;
                    str2 = null;
                    str3 = null;
                    str4 = null;
                    str5 = null;
                    aVar2 = null;
                    str6 = null;
                    list = null;
                    z2 = null;
                    i = 767;
                } else if (cVar instanceof c.e) {
                    z2 = true;
                    str = null;
                    str2 = null;
                    str3 = null;
                    str4 = null;
                    str5 = null;
                    aVar2 = null;
                    str6 = null;
                    list = null;
                    z = null;
                    i = 511;
                } else if (cVar instanceof c.a) {
                    aVar2 = a.C0011a.UserSelected;
                    str = null;
                    str2 = null;
                    str3 = null;
                    str4 = null;
                    str5 = null;
                    str6 = null;
                    list = null;
                    z = null;
                    z2 = null;
                    i = 991;
                } else {
                    throw new NoWhenBranchMatchedException();
                }
                aVar = a.a.a.a.e.a.a(aVar4, str, str2, str3, str4, str5, aVar2, str6, list, z, z2, i);
            }
            Job unused = BuildersKt__Builders_commonKt.launch$default(this.d, (CoroutineContext) null, (CoroutineStart) null, new e(this, aVar, (Continuation) null), 3, (Object) null);
        }
    }

    void a(c cVar);
}
