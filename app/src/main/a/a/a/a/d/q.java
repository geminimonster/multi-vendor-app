package a.a.a.a.d;

import a.a.a.a.c.d;
import a.a.a.a.c.i;
import a.a.a.a.c.m;
import a.a.a.a.d.f;
import a.a.a.a.d.g;
import a.a.a.a.e.c;
import android.os.AsyncTask;
import com.google.android.gms.common.internal.ServiceSpecificExtraArgs;
import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.JOSEObject;
import com.stripe.android.stripe3ds2.exceptions.SDKRuntimeException;
import java.io.IOException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.interfaces.ECPrivateKey;
import java.security.interfaces.ECPublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.text.ParseException;
import java.util.UUID;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ConcurrentHashMap;
import javax.crypto.SecretKey;
import kotlin.Metadata;
import kotlin.Result;
import kotlin.ResultKt;
import kotlin.TypeCastException;
import kotlin.Unit;
import kotlin.jvm.internal.Intrinsics;
import kotlinx.coroutines.Job;
import org.json.JSONException;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000h\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\b\u0000\u0018\u0000 &2\u00020\u0001:\u0003%&'B?\b\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u0005\u0012\u0006\u0010\u000b\u001a\u00020\f\u0012\u0006\u0010\r\u001a\u00020\u000e¢\u0006\u0002\u0010\u000fJ\u0018\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00020\u001bH\u0016J\u0018\u0010\u001c\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00020\u001bH\u0016J\b\u0010\u001d\u001a\u00020\u0015H\u0002J\u0010\u0010\u001e\u001a\u00020\u00052\u0006\u0010\u001f\u001a\u00020 H\u0002J6\u0010!\u001a\u00020\u00172\u0006\u0010\"\u001a\u00020\u00052\u0006\u0010\u0018\u001a\u00020\u00192\u0014\u0010#\u001a\u0010\u0012\u0002\b\u0003\u0012\u0002\b\u0003\u0012\u0002\b\u0003\u0018\u00010$2\u0006\u0010\u001a\u001a\u00020\u001bH\u0002R\u000e\u0010\b\u001a\u00020\tX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\fX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0013X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0015X\u0004¢\u0006\u0002\n\u0000¨\u0006("}, d2 = {"Lcom/stripe/android/stripe3ds2/transaction/StripeChallengeRequestExecutor;", "Lcom/stripe/android/stripe3ds2/transaction/ChallengeRequestExecutor;", "messageTransformer", "Lcom/stripe/android/stripe3ds2/security/MessageTransformer;", "sdkReferenceId", "", "sdkPrivateKey", "Ljava/security/PrivateKey;", "acsPublicKey", "Ljava/security/interfaces/ECPublicKey;", "acsUrl", "requestTimerFactory", "Lcom/stripe/android/stripe3ds2/transaction/ChallengeRequestTimer$Factory;", "dhKeyGenerator", "Lcom/stripe/android/stripe3ds2/security/DiffieHellmanKeyGenerator;", "(Lcom/stripe/android/stripe3ds2/security/MessageTransformer;Ljava/lang/String;Ljava/security/PrivateKey;Ljava/security/interfaces/ECPublicKey;Ljava/lang/String;Lcom/stripe/android/stripe3ds2/transaction/ChallengeRequestTimer$Factory;Lcom/stripe/android/stripe3ds2/security/DiffieHellmanKeyGenerator;)V", "httpClient", "Lcom/stripe/android/stripe3ds2/transaction/HttpClient;", "responseProcessor", "Lcom/stripe/android/stripe3ds2/transaction/ChallengeResponseProcessor;", "secretKey", "Ljavax/crypto/SecretKey;", "execute", "", "creqData", "Lcom/stripe/android/stripe3ds2/transactions/ChallengeRequestData;", "listener", "Lcom/stripe/android/stripe3ds2/transaction/ChallengeRequestExecutor$Listener;", "executeAsync", "generateSecretKey", "getRequestBody", "payload", "Lorg/json/JSONObject;", "onRequestTimeout", "requestId", "asyncTask", "Landroid/os/AsyncTask;", "AsyncRequestTask", "Companion", "Factory", "3ds2sdk_release"}, k = 1, mv = {1, 1, 16})
public final class q implements f {
    public static final ConcurrentHashMap<String, Boolean> j = new ConcurrentHashMap<>();
    @Deprecated
    public static final b k = new b();

    /* renamed from: a  reason: collision with root package name */
    public final l f60a;
    public final SecretKey b;
    public final h c;
    public final i d;
    public final String e;
    public final PrivateKey f;
    public final ECPublicKey g;
    public final g.b h;
    public final a.a.a.a.c.b i;

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000J\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0011\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\b\u0002\u0018\u00002\u0014\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00030\u0001:\u0001\u0019B=\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u0007\u0012\u0006\u0010\u000b\u001a\u00020\f\u0012\u0006\u0010\r\u001a\u00020\u000e\u0012\u0006\u0010\u000f\u001a\u00020\u0010¢\u0006\u0002\u0010\u0011J#\u0010\u0012\u001a\u0004\u0018\u00010\u00032\u0012\u0010\u0013\u001a\n\u0012\u0006\b\u0001\u0012\u00020\u00020\u0014\"\u00020\u0002H\u0014¢\u0006\u0002\u0010\u0015J\u0012\u0010\u0016\u001a\u00020\u00172\b\u0010\u0018\u001a\u0004\u0018\u00010\u0003H\u0014R\u000e\u0010\b\u001a\u00020\tX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u0007X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\fX\u0004¢\u0006\u0002\n\u0000¨\u0006\u001a"}, d2 = {"Lcom/stripe/android/stripe3ds2/transaction/StripeChallengeRequestExecutor$AsyncRequestTask;", "Landroid/os/AsyncTask;", "Ljava/lang/Void;", "Lcom/stripe/android/stripe3ds2/transaction/StripeChallengeRequestExecutor$AsyncRequestTask$Result;", "httpClient", "Lcom/stripe/android/stripe3ds2/transaction/HttpClient;", "requestId", "", "creqData", "Lcom/stripe/android/stripe3ds2/transactions/ChallengeRequestData;", "requestBody", "responseProcessor", "Lcom/stripe/android/stripe3ds2/transaction/ChallengeResponseProcessor;", "requestTimer", "Lcom/stripe/android/stripe3ds2/transaction/ChallengeRequestTimer;", "listener", "Lcom/stripe/android/stripe3ds2/transaction/ChallengeRequestExecutor$Listener;", "(Lcom/stripe/android/stripe3ds2/transaction/HttpClient;Ljava/lang/String;Lcom/stripe/android/stripe3ds2/transactions/ChallengeRequestData;Ljava/lang/String;Lcom/stripe/android/stripe3ds2/transaction/ChallengeResponseProcessor;Lcom/stripe/android/stripe3ds2/transaction/ChallengeRequestTimer;Lcom/stripe/android/stripe3ds2/transaction/ChallengeRequestExecutor$Listener;)V", "doInBackground", "voids", "", "([Ljava/lang/Void;)Lcom/stripe/android/stripe3ds2/transaction/StripeChallengeRequestExecutor$AsyncRequestTask$Result;", "onPostExecute", "", "result", "Result", "3ds2sdk_release"}, k = 1, mv = {1, 1, 16})
    public static final class a extends AsyncTask<Void, C0008a, C0008a> {

        /* renamed from: a  reason: collision with root package name */
        public final l f61a;
        public final String b;
        public final a.a.a.a.e.a c;
        public final String d;
        public final h e;
        public final g f;
        public final f.c g;

        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b2\u0018\u00002\u00020\u0001:\u0002\u0003\u0004B\u0007\b\u0002¢\u0006\u0002\u0010\u0002\u0001\u0002\u0005\u0006¨\u0006\u0007"}, d2 = {"Lcom/stripe/android/stripe3ds2/transaction/StripeChallengeRequestExecutor$AsyncRequestTask$Result;", "", "()V", "Failure", "Success", "Lcom/stripe/android/stripe3ds2/transaction/StripeChallengeRequestExecutor$AsyncRequestTask$Result$Success;", "Lcom/stripe/android/stripe3ds2/transaction/StripeChallengeRequestExecutor$AsyncRequestTask$Result$Failure;", "3ds2sdk_release"}, k = 1, mv = {1, 1, 16})
        /* renamed from: a.a.a.a.d.q$a$a  reason: collision with other inner class name */
        public static abstract class C0008a {

            /* renamed from: a.a.a.a.d.q$a$a$a  reason: collision with other inner class name */
            public static final class C0009a extends C0008a {

                /* renamed from: a  reason: collision with root package name */
                public final Throwable f62a;

                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0009a(Throwable th) {
                    super((DefaultConstructorMarker) null);
                    Intrinsics.checkParameterIsNotNull(th, "throwable");
                    this.f62a = th;
                }

                public boolean equals(Object obj) {
                    if (this != obj) {
                        return (obj instanceof C0009a) && Intrinsics.areEqual((Object) this.f62a, (Object) ((C0009a) obj).f62a);
                    }
                    return true;
                }

                public int hashCode() {
                    Throwable th = this.f62a;
                    if (th != null) {
                        return th.hashCode();
                    }
                    return 0;
                }

                public String toString() {
                    return "Failure(throwable=" + this.f62a + ")";
                }
            }

            /* renamed from: a.a.a.a.d.q$a$a$b */
            public static final class b extends C0008a {

                /* renamed from: a  reason: collision with root package name */
                public final m f63a;

                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public b(m mVar) {
                    super((DefaultConstructorMarker) null);
                    Intrinsics.checkParameterIsNotNull(mVar, "response");
                    this.f63a = mVar;
                }

                public boolean equals(Object obj) {
                    if (this != obj) {
                        return (obj instanceof b) && Intrinsics.areEqual((Object) this.f63a, (Object) ((b) obj).f63a);
                    }
                    return true;
                }

                public int hashCode() {
                    m mVar = this.f63a;
                    if (mVar != null) {
                        return mVar.hashCode();
                    }
                    return 0;
                }

                public String toString() {
                    return "Success(response=" + this.f63a + ")";
                }
            }

            public C0008a() {
            }

            public /* synthetic */ C0008a(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }
        }

        public a(l lVar, String str, a.a.a.a.e.a aVar, String str2, h hVar, g gVar, f.c cVar) {
            Intrinsics.checkParameterIsNotNull(lVar, "httpClient");
            Intrinsics.checkParameterIsNotNull(str, "requestId");
            Intrinsics.checkParameterIsNotNull(aVar, "creqData");
            Intrinsics.checkParameterIsNotNull(str2, "requestBody");
            Intrinsics.checkParameterIsNotNull(hVar, "responseProcessor");
            Intrinsics.checkParameterIsNotNull(gVar, "requestTimer");
            Intrinsics.checkParameterIsNotNull(cVar, ServiceSpecificExtraArgs.CastExtraArgs.LISTENER);
            this.f61a = lVar;
            this.b = str;
            this.c = aVar;
            this.d = str2;
            this.e = hVar;
            this.f = gVar;
            this.g = cVar;
        }

        public Object doInBackground(Object[] objArr) {
            Object obj;
            Intrinsics.checkParameterIsNotNull((Void[]) objArr, "voids");
            if (isCancelled()) {
                return null;
            }
            try {
                Result.Companion companion = Result.Companion;
                obj = Result.m4constructorimpl(new C0008a.b(this.f61a.a(this.d, JOSEObject.MIME_TYPE_COMPACT)));
            } catch (Throwable th) {
                Result.Companion companion2 = Result.Companion;
                obj = Result.m4constructorimpl(ResultKt.createFailure(th));
            }
            Throwable r0 = Result.m7exceptionOrNullimpl(obj);
            if (r0 != null) {
                obj = new C0008a.C0009a(r0);
            }
            return (C0008a) obj;
        }

        public void onPostExecute(Object obj) {
            Object obj2;
            C0008a aVar = (C0008a) obj;
            super.onPostExecute(aVar);
            if (!isCancelled()) {
                if (aVar instanceof C0008a.C0009a) {
                    this.g.a(((C0008a.C0009a) aVar).f62a);
                } else if ((aVar instanceof C0008a.b) && !b.a(q.k, this.b)) {
                    g gVar = this.f;
                    Job job = gVar.b;
                    if (job != null) {
                        Job.DefaultImpls.cancel$default(job, (CancellationException) null, 1, (Object) null);
                    }
                    gVar.b = null;
                    try {
                        Result.Companion companion = Result.Companion;
                        this.e.a(this.c, ((C0008a.b) aVar).f63a, this.g);
                        obj2 = Result.m4constructorimpl(Unit.INSTANCE);
                    } catch (Throwable th) {
                        Result.Companion companion2 = Result.Companion;
                        obj2 = Result.m4constructorimpl(ResultKt.createFailure(th));
                    }
                    Throwable r5 = Result.m7exceptionOrNullimpl(obj2);
                    if (r5 != null) {
                        Result.Companion companion3 = Result.Companion;
                        this.g.a(r5);
                        Result.m4constructorimpl(Unit.INSTANCE);
                    }
                }
            }
        }
    }

    public static final class b {
        public static final /* synthetic */ boolean a(b bVar, String str) {
            if (bVar != null) {
                Boolean bool = q.j.get(str);
                if (bool != null) {
                    return bool.booleanValue();
                }
                return false;
            }
            throw null;
        }
    }

    public static final class c implements f.b {

        /* renamed from: a  reason: collision with root package name */
        public final a.a.a.a.c.b f64a = new m();

        public f a(f.a aVar) {
            Object obj;
            Object obj2;
            Intrinsics.checkParameterIsNotNull(aVar, "config");
            d.a aVar2 = a.a.a.a.c.d.c;
            a.a.a.a.c.d dVar = a.a.a.a.c.d.b;
            i iVar = aVar.f50a;
            String str = aVar.b;
            byte[] bArr = aVar.c;
            if (dVar != null) {
                Intrinsics.checkParameterIsNotNull(bArr, "privateKeyEncoded");
                try {
                    Result.Companion companion = Result.Companion;
                    PrivateKey generatePrivate = dVar.f28a.generatePrivate(new PKCS8EncodedKeySpec(bArr));
                    if (generatePrivate != null) {
                        obj = Result.m4constructorimpl((ECPrivateKey) generatePrivate);
                        Throwable r4 = Result.m7exceptionOrNullimpl(obj);
                        if (r4 == null) {
                            ECPrivateKey eCPrivateKey = (ECPrivateKey) obj;
                            byte[] bArr2 = aVar.d;
                            Intrinsics.checkParameterIsNotNull(bArr2, "publicKeyEncoded");
                            try {
                                Result.Companion companion2 = Result.Companion;
                                PublicKey generatePublic = dVar.f28a.generatePublic(new X509EncodedKeySpec(bArr2));
                                if (generatePublic != null) {
                                    obj2 = Result.m4constructorimpl((ECPublicKey) generatePublic);
                                    Throwable r1 = Result.m7exceptionOrNullimpl(obj2);
                                    if (r1 == null) {
                                        return new q(iVar, str, eCPrivateKey, (ECPublicKey) obj2, aVar.e, new g.b(), this.f64a);
                                    }
                                    throw SDKRuntimeException.Companion.create(r1);
                                }
                                throw new TypeCastException("null cannot be cast to non-null type java.security.interfaces.ECPublicKey");
                            } catch (Throwable th) {
                                Result.Companion companion3 = Result.Companion;
                                obj2 = Result.m4constructorimpl(ResultKt.createFailure(th));
                            }
                        } else {
                            throw SDKRuntimeException.Companion.create(r4);
                        }
                    } else {
                        throw new TypeCastException("null cannot be cast to non-null type java.security.interfaces.ECPrivateKey");
                    }
                } catch (Throwable th2) {
                    Result.Companion companion4 = Result.Companion;
                    obj = Result.m4constructorimpl(ResultKt.createFailure(th2));
                }
            } else {
                throw null;
            }
        }
    }

    public static final class d implements g.c {

        /* renamed from: a  reason: collision with root package name */
        public final /* synthetic */ q f65a;
        public final /* synthetic */ String b;
        public final /* synthetic */ a.a.a.a.e.a c;
        public final /* synthetic */ f.c d;

        public d(q qVar, String str, a.a.a.a.e.a aVar, f.c cVar) {
            this.f65a = qVar;
            this.b = str;
            this.c = aVar;
            this.d = cVar;
        }

        public void a() {
            q.a(this.f65a, this.b, this.c, (AsyncTask) null, this.d);
        }
    }

    public static final class e implements g.c {

        /* renamed from: a  reason: collision with root package name */
        public final /* synthetic */ q f66a;
        public final /* synthetic */ String b;
        public final /* synthetic */ a.a.a.a.e.a c;
        public final /* synthetic */ a d;
        public final /* synthetic */ f.c e;

        public e(q qVar, String str, a.a.a.a.e.a aVar, a aVar2, f.c cVar) {
            this.f66a = qVar;
            this.b = str;
            this.c = aVar;
            this.d = aVar2;
            this.e = cVar;
        }

        public void a() {
            q.a(this.f66a, this.b, this.c, this.d, this.e);
        }
    }

    public q(i iVar, String str, PrivateKey privateKey, ECPublicKey eCPublicKey, String str2, g.b bVar, a.a.a.a.c.b bVar2) {
        this.d = iVar;
        this.e = str;
        this.f = privateKey;
        this.g = eCPublicKey;
        this.h = bVar;
        this.i = bVar2;
        this.f60a = new s(str2);
        SecretKey a2 = a();
        this.b = a2;
        this.c = new h(this.d, a2);
    }

    public static final /* synthetic */ void a(q qVar, String str, a.a.a.a.e.a aVar, AsyncTask asyncTask, f.c cVar) {
        a.a.a.a.e.a aVar2 = aVar;
        AsyncTask asyncTask2 = asyncTask;
        if (qVar != null) {
            j.put(str, true);
            if (asyncTask2 != null) {
                asyncTask2.cancel(true);
            }
            String str2 = aVar2.d;
            String str3 = aVar2.f82a;
            String str4 = aVar2.c;
            f.c cVar2 = cVar;
            cVar2.a(new a.a.a.a.e.c(aVar2.b, str4, (String) null, String.valueOf(a.a.a.a.e.d.TransactionTimedout.f87a), c.C0012c.ThreeDsSdk, a.a.a.a.e.d.TransactionTimedout.b, "Challenge request timed-out", "CReq", str3, str2, 4));
            return;
        }
        throw null;
    }

    public final SecretKey a() {
        a.a.a.a.c.b bVar = this.i;
        ECPublicKey eCPublicKey = this.g;
        PrivateKey privateKey = this.f;
        if (privateKey != null) {
            return bVar.a(eCPublicKey, (ECPrivateKey) privateKey, this.e);
        }
        throw new TypeCastException("null cannot be cast to non-null type java.security.interfaces.ECPrivateKey");
    }

    public void b(a.a.a.a.e.a aVar, f.c cVar) throws IOException, JSONException, ParseException, JOSEException, SDKRuntimeException {
        Intrinsics.checkParameterIsNotNull(aVar, "creqData");
        Intrinsics.checkParameterIsNotNull(cVar, ServiceSpecificExtraArgs.CastExtraArgs.LISTENER);
        String uuid = UUID.randomUUID().toString();
        Intrinsics.checkExpressionValueIsNotNull(uuid, "UUID.randomUUID().toString()");
        g a2 = this.h.a();
        a2.f51a = new d(this, uuid, aVar, cVar);
        a2.a();
        m a3 = this.f60a.a(this.d.a(aVar.b(), this.b), JOSEObject.MIME_TYPE_COMPACT);
        if (!b.a(k, uuid)) {
            Job job = a2.b;
            if (job != null) {
                Job.DefaultImpls.cancel$default(job, (CancellationException) null, 1, (Object) null);
            }
            a2.b = null;
            this.c.a(aVar, a3, cVar);
        }
    }

    public void a(a.a.a.a.e.a aVar, f.c cVar) throws JSONException, JOSEException {
        Intrinsics.checkParameterIsNotNull(aVar, "creqData");
        Intrinsics.checkParameterIsNotNull(cVar, ServiceSpecificExtraArgs.CastExtraArgs.LISTENER);
        String uuid = UUID.randomUUID().toString();
        Intrinsics.checkExpressionValueIsNotNull(uuid, "UUID.randomUUID().toString()");
        g a2 = this.h.a();
        String str = uuid;
        a.a.a.a.e.a aVar2 = aVar;
        a aVar3 = new a(this.f60a, str, aVar2, this.d.a(aVar.b(), this.b), this.c, a2, cVar);
        a2.f51a = new e(this, str, aVar2, aVar3, cVar);
        a2.a();
        aVar3.execute(new Void[0]);
    }
}
