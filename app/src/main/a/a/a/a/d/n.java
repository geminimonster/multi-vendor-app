package a.a.a.a.d;

import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.JWSHeader;
import com.nimbusds.jose.JWSObject;
import com.nimbusds.jose.JWSVerifier;
import com.nimbusds.jose.crypto.bc.BouncyCastleProviderSingleton;
import com.nimbusds.jose.crypto.factories.DefaultJWSVerifierFactory;
import com.nimbusds.jose.jca.JCAContext;
import com.nimbusds.jose.util.Base64;
import com.nimbusds.jose.util.X509CertChainUtils;
import com.nimbusds.jose.util.X509CertUtils;
import java.io.IOException;
import java.io.InputStream;
import java.security.GeneralSecurityException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.cert.CertPathBuilder;
import java.security.cert.CertStore;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CollectionCertStoreParameters;
import java.security.cert.PKIXBuilderParameters;
import java.security.cert.X509CertSelector;
import java.security.cert.X509Certificate;
import java.text.ParseException;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import kotlin.Metadata;
import kotlin.Result;
import kotlin.ResultKt;
import kotlin.Unit;
import kotlin.collections.CollectionsKt;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.StringCompanionObject;
import org.json.JSONException;
import org.json.JSONObject;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\b`\u0018\u00002\u00020\u0001:\u0001\u000bJ&\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00072\f\u0010\b\u001a\b\u0012\u0004\u0012\u00020\n0\tH&¨\u0006\f"}, d2 = {"Lcom/stripe/android/stripe3ds2/transaction/JwsValidator;", "", "getPayload", "Lorg/json/JSONObject;", "jws", "", "isLiveMode", "", "rootCerts", "", "Ljava/security/cert/X509Certificate;", "Default", "3ds2sdk_release"}, k = 1, mv = {1, 1, 16})
public interface n {

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000D\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u0000 \u00152\u00020\u0001:\u0001\u0015B\u0005¢\u0006\u0002\u0010\u0002J&\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\f\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u000b0\nH\u0016J\u0010\u0010\f\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000fH\u0002J\u0010\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u000e\u001a\u00020\u000fH\u0002J\u001e\u0010\u0012\u001a\u00020\b2\u0006\u0010\u0013\u001a\u00020\u00142\f\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u000b0\nH\u0002¨\u0006\u0016"}, d2 = {"Lcom/stripe/android/stripe3ds2/transaction/JwsValidator$Default;", "Lcom/stripe/android/stripe3ds2/transaction/JwsValidator;", "()V", "getPayload", "Lorg/json/JSONObject;", "jws", "", "isLiveMode", "", "rootCerts", "", "Ljava/security/cert/X509Certificate;", "getPublicKeyFromHeader", "Ljava/security/PublicKey;", "jwsHeader", "Lcom/nimbusds/jose/JWSHeader;", "getVerifier", "Lcom/nimbusds/jose/JWSVerifier;", "isValid", "jwsObject", "Lcom/nimbusds/jose/JWSObject;", "Companion", "3ds2sdk_release"}, k = 1, mv = {1, 1, 16})
    public static final class a implements n {

        /* renamed from: a  reason: collision with root package name */
        public static final C0006a f57a = new C0006a();

        /* renamed from: a.a.a.a.d.n$a$a  reason: collision with other inner class name */
        public static final class C0006a {
            public final KeyStore a(List<? extends X509Certificate> list) throws KeyStoreException, CertificateException, NoSuchAlgorithmException, IOException {
                Intrinsics.checkParameterIsNotNull(list, "rootCerts");
                KeyStore instance = KeyStore.getInstance(KeyStore.getDefaultType());
                instance.load((InputStream) null, (char[]) null);
                int i = 0;
                for (T next : list) {
                    int i2 = i + 1;
                    if (i < 0) {
                        CollectionsKt.throwIndexOverflow();
                    }
                    X509Certificate x509Certificate = (X509Certificate) next;
                    StringCompanionObject stringCompanionObject = StringCompanionObject.INSTANCE;
                    Locale locale = Locale.ROOT;
                    Intrinsics.checkExpressionValueIsNotNull(locale, "Locale.ROOT");
                    String format = String.format(locale, "ca_%d", Arrays.copyOf(new Object[]{Integer.valueOf(i)}, 1));
                    Intrinsics.checkExpressionValueIsNotNull(format, "java.lang.String.format(locale, format, *args)");
                    instance.setCertificateEntry(format, (Certificate) list.get(i));
                    i = i2;
                }
                Intrinsics.checkExpressionValueIsNotNull(instance, "keyStore");
                return instance;
            }

            public final void a(List<? extends Base64> list, List<? extends X509Certificate> list2) throws GeneralSecurityException, IOException, ParseException {
                List<X509Certificate> parse = X509CertChainUtils.parse((List<Base64>) list);
                KeyStore a2 = a(list2);
                X509CertSelector x509CertSelector = new X509CertSelector();
                x509CertSelector.setCertificate(parse.get(0));
                PKIXBuilderParameters pKIXBuilderParameters = new PKIXBuilderParameters(a2, x509CertSelector);
                pKIXBuilderParameters.setRevocationEnabled(false);
                pKIXBuilderParameters.addCertStore(CertStore.getInstance("Collection", new CollectionCertStoreParameters(parse)));
                CertPathBuilder.getInstance("PKIX").build(pKIXBuilderParameters);
            }
        }

        public final PublicKey a(JWSHeader jWSHeader) throws CertificateException {
            List x509CertChain = jWSHeader.getX509CertChain();
            Intrinsics.checkExpressionValueIsNotNull(x509CertChain, "jwsHeader.x509CertChain");
            X509Certificate parseWithException = X509CertUtils.parseWithException(((Base64) CollectionsKt.first(x509CertChain)).decode());
            Intrinsics.checkExpressionValueIsNotNull(parseWithException, "X509CertUtils.parseWithE…().decode()\n            )");
            PublicKey publicKey = parseWithException.getPublicKey();
            Intrinsics.checkExpressionValueIsNotNull(publicKey, "X509CertUtils.parseWithE…)\n            ).publicKey");
            return publicKey;
        }

        public JSONObject a(String str, boolean z, List<? extends X509Certificate> list) throws JSONException, ParseException, JOSEException, CertificateException {
            boolean z2;
            Object obj;
            Intrinsics.checkParameterIsNotNull(str, "jws");
            Intrinsics.checkParameterIsNotNull(list, "rootCerts");
            JWSObject parse = JWSObject.parse(str);
            if (z) {
                Intrinsics.checkExpressionValueIsNotNull(parse, "jwsObject");
                JWSHeader header = parse.getHeader();
                C0006a aVar = f57a;
                Intrinsics.checkExpressionValueIsNotNull(header, "jwsHeader");
                List x509CertChain = header.getX509CertChain();
                Intrinsics.checkParameterIsNotNull(list, "rootCerts");
                boolean z3 = false;
                if ((x509CertChain == null || x509CertChain.isEmpty()) || list.isEmpty()) {
                    z2 = false;
                } else {
                    try {
                        Result.Companion companion = Result.Companion;
                        aVar.a(x509CertChain, list);
                        obj = Result.m4constructorimpl(Unit.INSTANCE);
                    } catch (Throwable th) {
                        Result.Companion companion2 = Result.Companion;
                        obj = Result.m4constructorimpl(ResultKt.createFailure(th));
                    }
                    z2 = Result.m11isSuccessimpl(obj);
                }
                if (z2) {
                    DefaultJWSVerifierFactory defaultJWSVerifierFactory = new DefaultJWSVerifierFactory();
                    JCAContext jCAContext = defaultJWSVerifierFactory.getJCAContext();
                    Intrinsics.checkExpressionValueIsNotNull(jCAContext, "verifierFactory.jcaContext");
                    jCAContext.setProvider(BouncyCastleProviderSingleton.getInstance());
                    JWSVerifier createJWSVerifier = defaultJWSVerifierFactory.createJWSVerifier(header, a(header));
                    Intrinsics.checkExpressionValueIsNotNull(createJWSVerifier, "verifierFactory.createJW…KeyFromHeader(jwsHeader))");
                    z3 = parse.verify(createJWSVerifier);
                }
                if (!z3) {
                    throw new IllegalStateException("Could not validate JWS");
                }
            }
            Intrinsics.checkExpressionValueIsNotNull(parse, "jwsObject");
            return new JSONObject(parse.getPayload().toString());
        }
    }

    JSONObject a(String str, boolean z, List<? extends X509Certificate> list);
}
