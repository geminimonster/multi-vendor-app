package a.a.a.a.d;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSocketFactory;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\b\u0000\u0018\u0000 \u00182\u00020\u0001:\u0001\u0018B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\b\u0010\u0005\u001a\u00020\u0006H\u0002J\b\u0010\u0007\u001a\u00020\u0006H\u0002J\u0018\u0010\b\u001a\u00020\u00062\u0006\u0010\t\u001a\u00020\u00032\u0006\u0010\n\u001a\u00020\u0003H\u0002J\n\u0010\u000b\u001a\u0004\u0018\u00010\fH\u0016J\u0018\u0010\r\u001a\u00020\u000e2\u0006\u0010\t\u001a\u00020\u00032\u0006\u0010\n\u001a\u00020\u0003H\u0016J\u0010\u0010\u000f\u001a\u00020\u00032\u0006\u0010\u0010\u001a\u00020\fH\u0002J\u0015\u0010\u0011\u001a\u00020\u000e2\u0006\u0010\u0012\u001a\u00020\u0006H\u0001¢\u0006\u0002\b\u0013J\u0010\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u0017H\u0002R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0019"}, d2 = {"Lcom/stripe/android/stripe3ds2/transaction/StripeHttpClient;", "Lcom/stripe/android/stripe3ds2/transaction/HttpClient;", "url", "", "(Ljava/lang/String;)V", "createConnection", "Ljava/net/HttpURLConnection;", "createGetConnection", "createPostConnection", "requestBody", "contentType", "doGetRequest", "Ljava/io/InputStream;", "doPostRequest", "Lcom/stripe/android/stripe3ds2/transaction/HttpResponse;", "getResponseBody", "inputStream", "handlePostResponse", "conn", "handlePostResponse$3ds2sdk_release", "isSuccessfulResponse", "", "responseCode", "", "Companion", "3ds2sdk_release"}, k = 1, mv = {1, 1, 16})
public final class s implements l {
    public static SSLSocketFactory b;
    public static final a c = new a();

    /* renamed from: a  reason: collision with root package name */
    public final String f69a;

    public static final class a {
        public final void a(SSLSocketFactory sSLSocketFactory) {
            Intrinsics.checkParameterIsNotNull(sSLSocketFactory, "sslSocketFactory");
            s.b = sSLSocketFactory;
        }
    }

    public s(String str) {
        Intrinsics.checkParameterIsNotNull(str, "url");
        this.f69a = str;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:24:0x009a, code lost:
        r6 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x009b, code lost:
        kotlin.io.CloseableKt.closeFinally(r3, r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x009e, code lost:
        throw r6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x00c3, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:?, code lost:
        kotlin.io.CloseableKt.closeFinally(r3, r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x00c7, code lost:
        throw r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x00ca, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x00cb, code lost:
        kotlin.io.CloseableKt.closeFinally(r6, r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x00ce, code lost:
        throw r0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public a.a.a.a.d.m a(java.lang.String r5, java.lang.String r6) throws java.io.IOException, com.stripe.android.stripe3ds2.exceptions.SDKRuntimeException {
        /*
            r4 = this;
            java.lang.String r0 = "requestBody"
            kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(r5, r0)
            java.lang.String r0 = "contentType"
            kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(r6, r0)
            java.net.HttpURLConnection r0 = r4.a()
            java.lang.String r1 = "POST"
            r0.setRequestMethod(r1)
            r1 = 1
            r0.setDoOutput(r1)
            java.lang.String r2 = "Content-Type"
            r0.setRequestProperty(r2, r6)
            int r6 = r5.length()
            java.lang.String r6 = java.lang.String.valueOf(r6)
            java.lang.String r2 = "Content-Length"
            r0.setRequestProperty(r2, r6)
            java.io.OutputStream r6 = r0.getOutputStream()
            java.lang.String r2 = "os"
            kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r6, r2)     // Catch:{ all -> 0x00c8 }
            java.nio.charset.Charset r2 = java.nio.charset.StandardCharsets.UTF_8     // Catch:{ all -> 0x00c8 }
            java.lang.String r3 = "StandardCharsets.UTF_8"
            kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r2, r3)     // Catch:{ all -> 0x00c8 }
            java.io.OutputStreamWriter r3 = new java.io.OutputStreamWriter     // Catch:{ all -> 0x00c8 }
            r3.<init>(r6, r2)     // Catch:{ all -> 0x00c8 }
            r3.write(r5)     // Catch:{ all -> 0x00c1 }
            r3.flush()     // Catch:{ all -> 0x00c1 }
            kotlin.Unit r5 = kotlin.Unit.INSTANCE     // Catch:{ all -> 0x00c1 }
            r5 = 0
            kotlin.io.CloseableKt.closeFinally(r3, r5)     // Catch:{ all -> 0x00c8 }
            kotlin.Unit r2 = kotlin.Unit.INSTANCE     // Catch:{ all -> 0x00c8 }
            kotlin.io.CloseableKt.closeFinally(r6, r5)
            r0.connect()
            java.lang.String r6 = "conn"
            kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(r0, r6)
            int r6 = r0.getResponseCode()
            r2 = 299(0x12b, float:4.19E-43)
            r3 = 200(0xc8, float:2.8E-43)
            if (r3 <= r6) goto L_0x0062
            goto L_0x0065
        L_0x0062:
            if (r2 < r6) goto L_0x0065
            goto L_0x0066
        L_0x0065:
            r1 = 0
        L_0x0066:
            if (r1 == 0) goto L_0x009f
            a.a.a.a.d.m r6 = new a.a.a.a.d.m
            java.io.InputStream r1 = r0.getInputStream()
            java.lang.String r2 = "conn.inputStream"
            kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r1, r2)
            java.nio.charset.Charset r2 = kotlin.text.Charsets.UTF_8
            java.io.InputStreamReader r3 = new java.io.InputStreamReader
            r3.<init>(r1, r2)
            r1 = 8192(0x2000, float:1.14794E-41)
            boolean r2 = r3 instanceof java.io.BufferedReader
            if (r2 == 0) goto L_0x0083
            java.io.BufferedReader r3 = (java.io.BufferedReader) r3
            goto L_0x0089
        L_0x0083:
            java.io.BufferedReader r2 = new java.io.BufferedReader
            r2.<init>(r3, r1)
            r3 = r2
        L_0x0089:
            java.lang.String r1 = kotlin.io.TextStreamsKt.readText(r3)     // Catch:{ all -> 0x0098 }
            kotlin.io.CloseableKt.closeFinally(r3, r5)
            java.lang.String r5 = r0.getContentType()
            r6.<init>(r1, r5)
            return r6
        L_0x0098:
            r5 = move-exception
            throw r5     // Catch:{ all -> 0x009a }
        L_0x009a:
            r6 = move-exception
            kotlin.io.CloseableKt.closeFinally(r3, r5)
            throw r6
        L_0x009f:
            com.stripe.android.stripe3ds2.exceptions.SDKRuntimeException$Companion r5 = com.stripe.android.stripe3ds2.exceptions.SDKRuntimeException.Companion
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "Unsuccessful response code from "
            r0.append(r1)
            java.lang.String r1 = r4.f69a
            r0.append(r1)
            java.lang.String r1 = ": "
            r0.append(r1)
            r0.append(r6)
            java.lang.String r6 = r0.toString()
            com.stripe.android.stripe3ds2.exceptions.SDKRuntimeException r5 = r5.create((java.lang.String) r6)
            throw r5
        L_0x00c1:
            r5 = move-exception
            throw r5     // Catch:{ all -> 0x00c3 }
        L_0x00c3:
            r0 = move-exception
            kotlin.io.CloseableKt.closeFinally(r3, r5)     // Catch:{ all -> 0x00c8 }
            throw r0     // Catch:{ all -> 0x00c8 }
        L_0x00c8:
            r5 = move-exception
            throw r5     // Catch:{ all -> 0x00ca }
        L_0x00ca:
            r0 = move-exception
            kotlin.io.CloseableKt.closeFinally(r6, r5)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: a.a.a.a.d.s.a(java.lang.String, java.lang.String):a.a.a.a.d.m");
    }

    public final HttpURLConnection a() throws IOException {
        SSLSocketFactory sSLSocketFactory;
        URLConnection openConnection = new URL(this.f69a).openConnection();
        if (openConnection != null) {
            HttpURLConnection httpURLConnection = (HttpURLConnection) openConnection;
            if ((httpURLConnection instanceof HttpsURLConnection) && (sSLSocketFactory = b) != null) {
                ((HttpsURLConnection) httpURLConnection).setSSLSocketFactory(sSLSocketFactory);
            }
            return httpURLConnection;
        }
        throw new TypeCastException("null cannot be cast to non-null type java.net.HttpURLConnection");
    }
}
