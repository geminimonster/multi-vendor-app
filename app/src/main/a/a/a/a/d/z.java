package a.a.a.a.d;

import com.stripe.android.stripe3ds2.exceptions.SDKRuntimeException;
import java.util.HashMap;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0005\bf\u0018\u00002\u00020\u0001:\u0001\u000bJ\b\u0010\u0002\u001a\u00020\u0003H&J\u0011\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0007H¦\u0002J\u0018\u0010\b\u001a\u00020\u00032\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u0010\t\u001a\u00020\u0005H&J\u0010\u0010\n\u001a\u00020\u00032\u0006\u0010\u0006\u001a\u00020\u0007H&¨\u0006\f"}, d2 = {"Lcom/stripe/android/stripe3ds2/transaction/TransactionTimerProvider;", "", "clear", "", "get", "Lcom/stripe/android/stripe3ds2/transaction/TransactionTimer;", "sdkTransactionId", "", "put", "transactionTimer", "remove", "Default", "3ds2sdk_release"}, k = 1, mv = {1, 1, 16})
public interface z {

    public static final class a implements z {

        /* renamed from: a  reason: collision with root package name */
        public static final HashMap<String, y> f81a = new HashMap<>();
        public static final a b = new a();

        public void a() {
            f81a.clear();
        }

        public void a(String str) {
            Intrinsics.checkParameterIsNotNull(str, "sdkTransactionId");
            f81a.remove(str);
        }

        public void a(String str, y yVar) {
            Intrinsics.checkParameterIsNotNull(str, "sdkTransactionId");
            Intrinsics.checkParameterIsNotNull(yVar, "transactionTimer");
            f81a.put(str, yVar);
        }

        public y b(String str) {
            Intrinsics.checkParameterIsNotNull(str, "sdkTransactionId");
            y yVar = f81a.get(str);
            if (yVar != null) {
                return yVar;
            }
            throw new SDKRuntimeException(new RuntimeException("No TransactionTimer for transaction id " + str));
        }
    }

    void a();

    void a(String str);

    void a(String str, y yVar);
}
