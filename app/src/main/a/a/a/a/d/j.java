package a.a.a.a.d;

import com.stripe.android.stripe3ds2.exceptions.SDKRuntimeException;
import com.stripe.android.stripe3ds2.transaction.ChallengeStatusReceiver;
import java.util.LinkedHashMap;
import java.util.Map;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0004\bf\u0018\u00002\u00020\u0001:\u0001\nJ\b\u0010\u0002\u001a\u00020\u0003H&J\u0011\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0007H¦\u0002J\u0018\u0010\b\u001a\u00020\u00032\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u0010\t\u001a\u00020\u0005H&¨\u0006\u000b"}, d2 = {"Lcom/stripe/android/stripe3ds2/transaction/ChallengeStatusReceiverProvider;", "", "clear", "", "get", "Lcom/stripe/android/stripe3ds2/transaction/ChallengeStatusReceiver;", "sdkTransactionId", "", "put", "challengeStatusReceiver", "Default", "3ds2sdk_release"}, k = 1, mv = {1, 1, 16})
public interface j {

    public static final class a implements j {

        /* renamed from: a  reason: collision with root package name */
        public static final Map<String, ChallengeStatusReceiver> f55a = new LinkedHashMap();
        public static final a b = new a();

        public ChallengeStatusReceiver a(String str) {
            Intrinsics.checkParameterIsNotNull(str, "sdkTransactionId");
            ChallengeStatusReceiver challengeStatusReceiver = f55a.get(str);
            if (challengeStatusReceiver != null) {
                return challengeStatusReceiver;
            }
            throw new SDKRuntimeException(new RuntimeException("No ChallengeStatusReceiver for transaction id " + str));
        }

        public void a() {
            f55a.clear();
        }

        public void a(String str, ChallengeStatusReceiver challengeStatusReceiver) {
            Intrinsics.checkParameterIsNotNull(str, "sdkTransactionId");
            Intrinsics.checkParameterIsNotNull(challengeStatusReceiver, "challengeStatusReceiver");
            f55a.put(str, challengeStatusReceiver);
        }
    }

    void a();

    void a(String str, ChallengeStatusReceiver challengeStatusReceiver);
}
