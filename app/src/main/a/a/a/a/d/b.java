package a.a.a.a.d;

import a.a.a.a.b.d;
import a.a.a.a.b.h;
import a.a.a.a.b.k;
import a.a.a.a.c.g;
import com.nimbusds.jose.jwk.Curve;
import com.nimbusds.jose.jwk.ECKey;
import com.nimbusds.jose.jwk.JWK;
import com.nimbusds.jose.jwk.KeyUse;
import com.stripe.android.stripe3ds2.init.Warning;
import com.stripe.android.stripe3ds2.transaction.MessageVersionRegistry;
import java.security.PublicKey;
import java.security.interfaces.ECPublicKey;
import java.util.ArrayList;
import java.util.List;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt;
import kotlin.jvm.internal.Intrinsics;
import kotlin.text.StringsKt;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000R\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0010\u0018\u0000  2\u00020\u0001:\u0001 BE\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t\u0012\f\u0010\n\u001a\b\u0012\u0004\u0012\u00020\f0\u000b\u0012\u0006\u0010\r\u001a\u00020\u000e\u0012\u0006\u0010\u000f\u001a\u00020\u0010¢\u0006\u0002\u0010\u0011BE\b\u0000\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\f\u0010\n\u001a\b\u0012\u0004\u0012\u00020\f0\u000b\u0012\u0006\u0010\u0012\u001a\u00020\u0013\u0012\u0006\u0010\r\u001a\u00020\u000e\u0012\u0006\u0010\u000f\u001a\u00020\u0010¢\u0006\u0002\u0010\u0014J2\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00020\u00102\u0006\u0010\u001b\u001a\u00020\u001c2\b\u0010\u001d\u001a\u0004\u0018\u00010\u00102\u0006\u0010\u001e\u001a\u00020\u00102\u0006\u0010\u001f\u001a\u00020\u001cH\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\u0015\u001a\u00020\u00108AX\u0004¢\u0006\u0006\u001a\u0004\b\u0016\u0010\u0017R\u000e\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0013X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\n\u001a\b\u0012\u0004\u0012\u00020\f0\u000bX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0004¢\u0006\u0002\n\u0000¨\u0006!"}, d2 = {"Lcom/stripe/android/stripe3ds2/transaction/AuthenticationRequestParametersFactory;", "", "deviceDataFactory", "Lcom/stripe/android/stripe3ds2/init/DeviceDataFactory;", "deviceParamNotAvailableFactory", "Lcom/stripe/android/stripe3ds2/init/DeviceParamNotAvailableFactory;", "securityChecker", "Lcom/stripe/android/stripe3ds2/init/SecurityChecker;", "ephemeralKeyPairGenerator", "Lcom/stripe/android/stripe3ds2/security/EphemeralKeyPairGenerator;", "sdkAppIdSupplier", "Lcom/stripe/android/stripe3ds2/utils/Supplier;", "Lcom/stripe/android/stripe3ds2/init/SdkAppId;", "messageVersionRegistry", "Lcom/stripe/android/stripe3ds2/transaction/MessageVersionRegistry;", "sdkReferenceNumber", "", "(Lcom/stripe/android/stripe3ds2/init/DeviceDataFactory;Lcom/stripe/android/stripe3ds2/init/DeviceParamNotAvailableFactory;Lcom/stripe/android/stripe3ds2/init/SecurityChecker;Lcom/stripe/android/stripe3ds2/security/EphemeralKeyPairGenerator;Lcom/stripe/android/stripe3ds2/utils/Supplier;Lcom/stripe/android/stripe3ds2/transaction/MessageVersionRegistry;Ljava/lang/String;)V", "jweEncrypter", "Lcom/stripe/android/stripe3ds2/security/JweEncrypter;", "(Lcom/stripe/android/stripe3ds2/init/DeviceDataFactory;Lcom/stripe/android/stripe3ds2/init/DeviceParamNotAvailableFactory;Lcom/stripe/android/stripe3ds2/init/SecurityChecker;Lcom/stripe/android/stripe3ds2/utils/Supplier;Lcom/stripe/android/stripe3ds2/security/JweEncrypter;Lcom/stripe/android/stripe3ds2/transaction/MessageVersionRegistry;Ljava/lang/String;)V", "deviceDataJson", "getDeviceDataJson$3ds2sdk_release", "()Ljava/lang/String;", "create", "Lcom/stripe/android/stripe3ds2/transaction/AuthenticationRequestParameters;", "directoryServerId", "directoryServerPublicKey", "Ljava/security/PublicKey;", "keyId", "transactionId", "sdkPublicKey", "Companion", "3ds2sdk_release"}, k = 1, mv = {1, 1, 16})
public class b {
    public static final a h = new a();

    /* renamed from: a  reason: collision with root package name */
    public final a.a.a.a.b.a f36a;
    public final d b;
    public final k c;
    public final a.a.a.a.f.d<h> d;
    public final g e;
    public final MessageVersionRegistry f;
    public final String g;

    public static final class a {
        public final JWK a(PublicKey publicKey, String str, KeyUse keyUse) {
            Intrinsics.checkParameterIsNotNull(publicKey, "publicKey");
            ECKey.Builder keyUse2 = new ECKey.Builder(Curve.P_256, (ECPublicKey) publicKey).keyUse(keyUse);
            if (str == null || StringsKt.isBlank(str)) {
                str = null;
            }
            ECKey publicJWK = keyUse2.keyID(str).build().toPublicJWK();
            Intrinsics.checkExpressionValueIsNotNull(publicJWK, "ECKey.Builder(Curve.P_25…           .toPublicJWK()");
            return publicJWK;
        }
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public b(a.a.a.a.b.a r11, a.a.a.a.b.d r12, a.a.a.a.b.k r13, a.a.a.a.c.e r14, a.a.a.a.f.d<a.a.a.a.b.h> r15, com.stripe.android.stripe3ds2.transaction.MessageVersionRegistry r16, java.lang.String r17) {
        /*
            r10 = this;
            r0 = r14
            java.lang.String r1 = "deviceDataFactory"
            r3 = r11
            kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(r11, r1)
            java.lang.String r1 = "deviceParamNotAvailableFactory"
            r4 = r12
            kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(r12, r1)
            java.lang.String r1 = "securityChecker"
            r5 = r13
            kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(r13, r1)
            java.lang.String r1 = "ephemeralKeyPairGenerator"
            kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(r14, r1)
            java.lang.String r1 = "sdkAppIdSupplier"
            r6 = r15
            kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(r15, r1)
            java.lang.String r1 = "messageVersionRegistry"
            r8 = r16
            kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(r8, r1)
            java.lang.String r1 = "sdkReferenceNumber"
            r9 = r17
            kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(r9, r1)
            a.a.a.a.c.g r7 = new a.a.a.a.c.g
            r7.<init>(r14)
            r2 = r10
            r2.<init>((a.a.a.a.b.a) r3, (a.a.a.a.b.d) r4, (a.a.a.a.b.k) r5, (a.a.a.a.f.d<a.a.a.a.b.h>) r6, (a.a.a.a.c.g) r7, (com.stripe.android.stripe3ds2.transaction.MessageVersionRegistry) r8, (java.lang.String) r9)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: a.a.a.a.d.b.<init>(a.a.a.a.b.a, a.a.a.a.b.d, a.a.a.a.b.k, a.a.a.a.c.e, a.a.a.a.f.d, com.stripe.android.stripe3ds2.transaction.MessageVersionRegistry, java.lang.String):void");
    }

    public b(a.a.a.a.b.a aVar, d dVar, k kVar, a.a.a.a.f.d<h> dVar2, g gVar, MessageVersionRegistry messageVersionRegistry, String str) {
        Intrinsics.checkParameterIsNotNull(aVar, "deviceDataFactory");
        Intrinsics.checkParameterIsNotNull(dVar, "deviceParamNotAvailableFactory");
        Intrinsics.checkParameterIsNotNull(kVar, "securityChecker");
        Intrinsics.checkParameterIsNotNull(dVar2, "sdkAppIdSupplier");
        Intrinsics.checkParameterIsNotNull(gVar, "jweEncrypter");
        Intrinsics.checkParameterIsNotNull(messageVersionRegistry, "messageVersionRegistry");
        Intrinsics.checkParameterIsNotNull(str, "sdkReferenceNumber");
        this.f36a = aVar;
        this.b = dVar;
        this.c = kVar;
        this.d = dVar2;
        this.e = gVar;
        this.f = messageVersionRegistry;
        this.g = str;
    }

    public final String a() throws JSONException {
        JSONObject put = new JSONObject().put("DV", "1.1").put("DD", new JSONObject(this.f36a.a())).put("DPNA", new JSONObject(this.b.a()));
        List<Warning> warnings = this.c.getWarnings();
        ArrayList arrayList = new ArrayList(CollectionsKt.collectionSizeOrDefault(warnings, 10));
        for (Warning id : warnings) {
            arrayList.add(id.getId());
        }
        String jSONObject = put.put("SW", new JSONArray(arrayList)).toString();
        Intrinsics.checkExpressionValueIsNotNull(jSONObject, "JSONObject()\n           …              .toString()");
        return jSONObject;
    }
}
