package a.a.a.a.d;

import a.a.a.a.c.e;
import a.a.a.a.c.j;
import a.a.a.a.c.k;
import a.a.a.a.g.q;
import android.content.Intent;
import com.stripe.android.stripe3ds2.init.ui.StripeUiCustomization;
import com.stripe.android.stripe3ds2.transaction.MessageVersionRegistry;
import com.stripe.android.stripe3ds2.transaction.Transaction;
import java.security.KeyPair;
import java.security.PublicKey;
import java.security.cert.X509Certificate;
import java.util.List;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000H\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\b`\u0018\u00002\u00020\u0001:\u0001\u0017Jd\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\b0\u00072\u0006\u0010\t\u001a\u00020\n2\b\u0010\u000b\u001a\u0004\u0018\u00010\u00052\u0006\u0010\f\u001a\u00020\u00052\b\u0010\r\u001a\u0004\u0018\u00010\u000e2\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u00122\b\u0010\u0013\u001a\u0004\u0018\u00010\u00142\u0006\u0010\u0015\u001a\u00020\u0016H&¨\u0006\u0018"}, d2 = {"Lcom/stripe/android/stripe3ds2/transaction/TransactionFactory;", "", "create", "Lcom/stripe/android/stripe3ds2/transaction/Transaction;", "directoryServerId", "", "rootCerts", "", "Ljava/security/cert/X509Certificate;", "directoryServerPublicKey", "Ljava/security/PublicKey;", "keyId", "sdkTransactionId", "uiCustomization", "Lcom/stripe/android/stripe3ds2/init/ui/StripeUiCustomization;", "isLiveMode", "", "brand", "Lcom/stripe/android/stripe3ds2/views/ProgressViewFactory$Brand;", "challengeCompletionIntent", "Landroid/content/Intent;", "challengeCompletionRequestCode", "", "Default", "3ds2sdk_release"}, k = 1, mv = {1, 1, 16})
public interface u {

    public static final class a implements u {

        /* renamed from: a  reason: collision with root package name */
        public final p f76a;
        public final j b;
        public final b c;
        public final e d;
        public final MessageVersionRegistry e;
        public final String f;
        public final o g;
        public final q h;
        public final n i;
        public final j j;

        public a(b bVar, e eVar, MessageVersionRegistry messageVersionRegistry, String str, o oVar, q qVar, n nVar, j jVar, Intent intent, int i2) {
            Intrinsics.checkParameterIsNotNull(bVar, "areqParamsFactory");
            Intrinsics.checkParameterIsNotNull(eVar, "ephemeralKeyPairGenerator");
            Intrinsics.checkParameterIsNotNull(messageVersionRegistry, "messageVersionRegistry");
            Intrinsics.checkParameterIsNotNull(str, "sdkReferenceNumber");
            Intrinsics.checkParameterIsNotNull(oVar, "logger");
            Intrinsics.checkParameterIsNotNull(qVar, "progressViewFactory");
            Intrinsics.checkParameterIsNotNull(nVar, "jwsValidator");
            Intrinsics.checkParameterIsNotNull(jVar, "challengeStatusReceiverProvider");
            this.c = bVar;
            this.d = eVar;
            this.e = messageVersionRegistry;
            this.f = str;
            this.g = oVar;
            this.h = qVar;
            this.i = nVar;
            this.j = jVar;
            this.f76a = new p();
            this.b = new j();
        }

        /* JADX WARNING: Illegal instructions before constructor call */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public /* synthetic */ a(a.a.a.a.d.b r14, a.a.a.a.c.e r15, com.stripe.android.stripe3ds2.transaction.MessageVersionRegistry r16, java.lang.String r17, a.a.a.a.d.o r18, a.a.a.a.g.q r19, a.a.a.a.d.n r20, a.a.a.a.d.j r21, android.content.Intent r22, int r23, int r24) {
            /*
                r13 = this;
                r0 = r24
                r1 = r0 & 16
                if (r1 == 0) goto L_0x000e
                a.a.a.a.d.o$a r1 = a.a.a.a.d.o.f58a
                a.a.a.a.d.o r1 = r1.a()
                r7 = r1
                goto L_0x0010
            L_0x000e:
                r7 = r18
            L_0x0010:
                r1 = r0 & 32
                if (r1 == 0) goto L_0x001b
                a.a.a.a.g.r r1 = new a.a.a.a.g.r
                r1.<init>()
                r8 = r1
                goto L_0x001d
            L_0x001b:
                r8 = r19
            L_0x001d:
                r1 = r0 & 64
                if (r1 == 0) goto L_0x0028
                a.a.a.a.d.n$a r1 = new a.a.a.a.d.n$a
                r1.<init>()
                r9 = r1
                goto L_0x002a
            L_0x0028:
                r9 = r20
            L_0x002a:
                r1 = r0 & 128(0x80, float:1.794E-43)
                if (r1 == 0) goto L_0x0032
                a.a.a.a.d.j$a r1 = a.a.a.a.d.j.a.b
                r10 = r1
                goto L_0x0034
            L_0x0032:
                r10 = r21
            L_0x0034:
                r1 = r0 & 256(0x100, float:3.59E-43)
                if (r1 == 0) goto L_0x003b
                r1 = 0
                r11 = r1
                goto L_0x003d
            L_0x003b:
                r11 = r22
            L_0x003d:
                r0 = r0 & 512(0x200, float:7.175E-43)
                if (r0 == 0) goto L_0x0044
                r0 = 0
                r12 = 0
                goto L_0x0046
            L_0x0044:
                r12 = r23
            L_0x0046:
                r2 = r13
                r3 = r14
                r4 = r15
                r5 = r16
                r6 = r17
                r2.<init>(r3, r4, r5, r6, r7, r8, r9, r10, r11, r12)
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: a.a.a.a.d.u.a.<init>(a.a.a.a.d.b, a.a.a.a.c.e, com.stripe.android.stripe3ds2.transaction.MessageVersionRegistry, java.lang.String, a.a.a.a.d.o, a.a.a.a.g.q, a.a.a.a.d.n, a.a.a.a.d.j, android.content.Intent, int, int):void");
        }

        public Transaction a(String str, List<? extends X509Certificate> list, PublicKey publicKey, String str2, String str3, StripeUiCustomization stripeUiCustomization, boolean z, q.a aVar, Intent intent, int i2) {
            Intrinsics.checkParameterIsNotNull(str, "directoryServerId");
            Intrinsics.checkParameterIsNotNull(list, "rootCerts");
            Intrinsics.checkParameterIsNotNull(publicKey, "directoryServerPublicKey");
            Intrinsics.checkParameterIsNotNull(str3, "sdkTransactionId");
            Intrinsics.checkParameterIsNotNull(aVar, "brand");
            KeyPair a2 = this.d.a();
            b bVar = this.c;
            q qVar = this.h;
            j jVar = this.j;
            MessageVersionRegistry messageVersionRegistry = this.e;
            String str4 = this.f;
            n nVar = this.i;
            p pVar = this.f76a;
            if (this.b != null) {
                k.a aVar2 = k.e;
                k kVar = r2;
                byte b2 = (byte) 0;
                boolean z2 = z;
                k kVar2 = new k(z2, b2, b2);
                return new t(bVar, qVar, jVar, messageVersionRegistry, str4, nVar, pVar, str, publicKey, str2, str3, a2, z2, list, kVar, stripeUiCustomization, aVar, this.g, intent, i2);
            }
            throw null;
        }
    }

    Transaction a(String str, List<? extends X509Certificate> list, PublicKey publicKey, String str2, String str3, StripeUiCustomization stripeUiCustomization, boolean z, q.a aVar, Intent intent, int i);
}
