package a.a.a.a.d;

import a.a.a.a.c.i;
import com.nimbusds.jose.JOSEException;
import com.stripe.android.stripe3ds2.exceptions.SDKRuntimeException;
import com.stripe.android.stripe3ds2.transactions.ChallengeResponseData;
import java.io.IOException;
import java.io.Serializable;
import java.text.ParseException;
import java.util.Arrays;
import java.util.Objects;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.json.JSONException;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\bf\u0018\u00002\u00020\u0001:\u0003\t\n\u000bJ\u0018\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0007H&J\u0018\u0010\b\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0007H&¨\u0006\f"}, d2 = {"Lcom/stripe/android/stripe3ds2/transaction/ChallengeRequestExecutor;", "", "execute", "", "creqData", "Lcom/stripe/android/stripe3ds2/transactions/ChallengeRequestData;", "listener", "Lcom/stripe/android/stripe3ds2/transaction/ChallengeRequestExecutor$Listener;", "executeAsync", "Config", "Factory", "Listener", "3ds2sdk_release"}, k = 1, mv = {1, 1, 16})
public interface f {

    public static final class a implements Serializable {

        /* renamed from: a  reason: collision with root package name */
        public final i f50a;
        public final String b;
        public final byte[] c;
        public final byte[] d;
        public final String e;
        public final a.a.a.a.e.a f;

        public a(i iVar, String str, byte[] bArr, byte[] bArr2, String str2, a.a.a.a.e.a aVar) {
            Intrinsics.checkParameterIsNotNull(iVar, "messageTransformer");
            Intrinsics.checkParameterIsNotNull(str, "sdkReferenceId");
            Intrinsics.checkParameterIsNotNull(bArr, "sdkPrivateKeyEncoded");
            Intrinsics.checkParameterIsNotNull(bArr2, "acsPublicKeyEncoded");
            Intrinsics.checkParameterIsNotNull(str2, "acsUrl");
            Intrinsics.checkParameterIsNotNull(aVar, "creqData");
            this.f50a = iVar;
            this.b = str;
            this.c = bArr;
            this.d = bArr2;
            this.e = str2;
            this.f = aVar;
        }

        public final String a() {
            return this.e;
        }

        public boolean equals(Object obj) {
            if (this != obj) {
                if (obj instanceof a) {
                    a aVar = (a) obj;
                    if (!Intrinsics.areEqual((Object) this.f50a, (Object) aVar.f50a) || !Intrinsics.areEqual((Object) this.b, (Object) aVar.b) || !Intrinsics.areEqual((Object) this.c, (Object) aVar.c) || !Intrinsics.areEqual((Object) this.d, (Object) aVar.d) || !Intrinsics.areEqual((Object) this.e, (Object) aVar.e) || !Intrinsics.areEqual((Object) this.f, (Object) aVar.f)) {
                        return false;
                    }
                }
                return false;
            }
            return true;
        }

        public int hashCode() {
            Object[] objArr = {this.f50a, this.b, this.c, this.d, this.e, this.f};
            Intrinsics.checkParameterIsNotNull(objArr, "values");
            return Objects.hash(Arrays.copyOf(objArr, 6));
        }

        public String toString() {
            return "Config(messageTransformer=" + this.f50a + ", sdkReferenceId=" + this.b + ", sdkPrivateKeyEncoded=" + Arrays.toString(this.c) + ", acsPublicKeyEncoded=" + Arrays.toString(this.d) + ", acsUrl=" + this.e + ", creqData=" + this.f + ")";
        }
    }

    public interface b extends Serializable {
        f a(a aVar);
    }

    public interface c {
        void a(a.a.a.a.e.a aVar, ChallengeResponseData challengeResponseData);

        void a(a.a.a.a.e.c cVar);

        void a(Throwable th);

        void b(a.a.a.a.e.c cVar) throws IOException, ParseException, JOSEException;
    }

    void a(a.a.a.a.e.a aVar, c cVar) throws JSONException, JOSEException;

    void b(a.a.a.a.e.a aVar, c cVar) throws IOException, JSONException, ParseException, JOSEException, SDKRuntimeException;
}
