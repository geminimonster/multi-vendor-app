package a.a.a.a.d;

import a.a.a.a.c.i;
import a.a.a.a.d.f;
import a.a.a.a.e.a;
import a.a.a.a.e.b;
import a.a.a.a.e.c;
import a.a.a.a.e.d;
import com.facebook.share.internal.MessengerShareContentUtility;
import com.google.android.gms.common.internal.ServiceSpecificExtraArgs;
import com.nimbusds.jose.JOSEException;
import com.stripe.android.stripe3ds2.transactions.ChallengeResponseData;
import java.io.IOException;
import java.text.ParseException;
import javax.crypto.SecretKey;
import kotlin.Result;
import kotlin.ResultKt;
import kotlin.jvm.internal.Intrinsics;
import org.json.JSONException;
import org.json.JSONObject;

public final class h {

    /* renamed from: a  reason: collision with root package name */
    public final i f53a;
    public final SecretKey b;

    public h(i iVar, SecretKey secretKey) {
        Intrinsics.checkParameterIsNotNull(iVar, "messageTransformer");
        Intrinsics.checkParameterIsNotNull(secretKey, "secretKey");
        this.f53a = iVar;
        this.b = secretKey;
    }

    public final c a(a aVar, int i, String str, String str2) {
        a aVar2 = aVar;
        String valueOf = String.valueOf(i);
        c.C0012c cVar = c.C0012c.ThreeDsSdk;
        return new c(aVar2.b, aVar2.c, (String) null, valueOf, cVar, str, str2, ChallengeResponseData.MESSAGE_TYPE, aVar2.f82a, aVar2.d, 4);
    }

    public final void a(a aVar, m mVar, f.c cVar) throws JOSEException, ParseException, JSONException, IOException {
        c cVar2;
        Object obj;
        c cVar3;
        Object obj2;
        String str;
        String str2;
        int i;
        Intrinsics.checkParameterIsNotNull(aVar, "creqData");
        Intrinsics.checkParameterIsNotNull(mVar, "response");
        Intrinsics.checkParameterIsNotNull(cVar, ServiceSpecificExtraArgs.CastExtraArgs.LISTENER);
        if (mVar.f56a) {
            JSONObject jSONObject = new JSONObject(mVar.b);
            c.a aVar2 = c.k;
            Intrinsics.checkParameterIsNotNull(jSONObject, MessengerShareContentUtility.ATTACHMENT_PAYLOAD);
            if (Intrinsics.areEqual((Object) "Erro", (Object) jSONObject.optString("messageType"))) {
                cVar2 = c.k.a(jSONObject);
            } else {
                return;
            }
        } else {
            try {
                Result.Companion companion = Result.Companion;
                obj = Result.m4constructorimpl(this.f53a.a(mVar.b, this.b));
            } catch (Throwable th) {
                Result.Companion companion2 = Result.Companion;
                obj = Result.m4constructorimpl(ResultKt.createFailure(th));
            }
            Throwable r2 = Result.m7exceptionOrNullimpl(obj);
            if (r2 == null) {
                JSONObject jSONObject2 = (JSONObject) obj;
                Intrinsics.checkParameterIsNotNull(aVar, "creqData");
                Intrinsics.checkParameterIsNotNull(jSONObject2, MessengerShareContentUtility.ATTACHMENT_PAYLOAD);
                Intrinsics.checkParameterIsNotNull(cVar, ServiceSpecificExtraArgs.CastExtraArgs.LISTENER);
                c.a aVar3 = c.k;
                Intrinsics.checkParameterIsNotNull(jSONObject2, MessengerShareContentUtility.ATTACHMENT_PAYLOAD);
                if (Intrinsics.areEqual((Object) "Erro", (Object) jSONObject2.optString("messageType"))) {
                    cVar3 = c.k.a(jSONObject2);
                } else {
                    try {
                        Result.Companion companion3 = Result.Companion;
                        obj2 = Result.m4constructorimpl(ChallengeResponseData.Companion.a(jSONObject2));
                    } catch (Throwable th2) {
                        Result.Companion companion4 = Result.Companion;
                        obj2 = Result.m4constructorimpl(ResultKt.createFailure(th2));
                    }
                    Throwable r0 = Result.m7exceptionOrNullimpl(obj2);
                    if (r0 == null) {
                        ChallengeResponseData challengeResponseData = (ChallengeResponseData) obj2;
                        if (!(Intrinsics.areEqual((Object) aVar.d, (Object) challengeResponseData.getSdkTransId()) && Intrinsics.areEqual((Object) aVar.b, (Object) challengeResponseData.getServerTransId()) && Intrinsics.areEqual((Object) aVar.c, (Object) challengeResponseData.getAcsTransId()))) {
                            d dVar = d.InvalidTransactionId;
                            cVar3 = a(aVar, dVar.f87a, dVar.b, "The Transaction ID received was invalid.");
                        } else if (!Intrinsics.areEqual((Object) aVar.f82a, (Object) challengeResponseData.getMessageVersion())) {
                            d dVar2 = d.UnsupportedMessageVersion;
                            i = dVar2.f87a;
                            str = dVar2.b;
                            str2 = aVar.f82a;
                        } else {
                            cVar.a(aVar, challengeResponseData);
                            return;
                        }
                    } else if (r0 instanceof b) {
                        b bVar = (b) r0;
                        int i2 = bVar.f84a;
                        String str3 = bVar.b;
                        String str4 = bVar.c;
                        i = i2;
                        str = str3;
                        str2 = str4;
                    } else {
                        cVar.a(r0);
                        return;
                    }
                    cVar3 = a(aVar, i, str, str2);
                }
                cVar.b(cVar3);
                return;
            }
            d dVar3 = d.DataDecryptionFailure;
            int i3 = dVar3.f87a;
            String str5 = dVar3.b;
            String message = r2.getMessage();
            if (message == null) {
                message = "";
            }
            cVar2 = a(aVar, i3, str5, message);
        }
        cVar.b(cVar2);
    }
}
