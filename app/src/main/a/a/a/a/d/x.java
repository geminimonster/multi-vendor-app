package a.a.a.a.d;

import a.a.a.a.d.y;
import a.a.a.a.e.a;
import a.a.a.a.e.c;
import a.a.a.a.e.d;
import kotlin.ResultKt;
import kotlin.Unit;
import kotlin.coroutines.Continuation;
import kotlin.coroutines.intrinsics.IntrinsicsKt;
import kotlin.coroutines.jvm.internal.DebugMetadata;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.Intrinsics;
import kotlinx.coroutines.CoroutineScope;
import kotlinx.coroutines.DelayKt;
import org.bouncycastle.crypto.tls.CipherSuite;

@DebugMetadata(c = "com.stripe.android.stripe3ds2.transaction.TransactionTimer$Default$start$1", f = "TransactionTimer.kt", i = {0}, l = {43}, m = "invokeSuspend", n = {"$this$launch"}, s = {"L$0"})
public final class x extends SuspendLambda implements Function2<CoroutineScope, Continuation<? super Unit>, Object> {

    /* renamed from: a  reason: collision with root package name */
    public CoroutineScope f79a;
    public Object b;
    public int c;
    public final /* synthetic */ y.a d;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public x(y.a aVar, Continuation continuation) {
        super(2, continuation);
        this.d = aVar;
    }

    public final Continuation<Unit> create(Object obj, Continuation<?> continuation) {
        Intrinsics.checkParameterIsNotNull(continuation, "completion");
        x xVar = new x(this.d, continuation);
        xVar.f79a = (CoroutineScope) obj;
        return xVar;
    }

    public final Object invoke(Object obj, Object obj2) {
        return ((x) create(obj, (Continuation) obj2)).invokeSuspend(Unit.INSTANCE);
    }

    public final Object invokeSuspend(Object obj) {
        Object coroutine_suspended = IntrinsicsKt.getCOROUTINE_SUSPENDED();
        int i = this.c;
        if (i == 0) {
            ResultKt.throwOnFailure(obj);
            CoroutineScope coroutineScope = this.f79a;
            long j = this.d.f80a;
            this.b = coroutineScope;
            this.c = 1;
            if (DelayKt.delay(j, this) == coroutine_suspended) {
                return coroutine_suspended;
            }
        } else if (i == 1) {
            CoroutineScope coroutineScope2 = (CoroutineScope) this.b;
            ResultKt.throwOnFailure(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        y.a aVar = this.d;
        aVar.c = null;
        aVar.g.a(aVar.f.d);
        k kVar = aVar.e;
        a aVar2 = aVar.f;
        String str = aVar2.b;
        String str2 = aVar2.c;
        String valueOf = String.valueOf(d.TransactionTimedout.f87a);
        c.C0012c cVar = c.C0012c.ThreeDsSdk;
        String str3 = d.TransactionTimedout.b;
        a aVar3 = aVar.f;
        kVar.a(new c(str, str2, (String) null, valueOf, cVar, str3, "Timeout expiry reached for the transaction", (String) null, aVar3.f82a, aVar3.d, CipherSuite.TLS_RSA_WITH_CAMELLIA_256_CBC_SHA));
        aVar.d.timedout("", new w(aVar));
        return Unit.INSTANCE;
    }
}
