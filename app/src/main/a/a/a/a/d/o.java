package a.a.a.a.d;

import android.util.Log;
import androidx.core.app.NotificationCompat;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0003\n\u0002\b\u0003\b`\u0018\u0000 \t2\u00020\u0001:\u0001\tJ\u001c\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u0007H&J\u0010\u0010\b\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&¨\u0006\n"}, d2 = {"Lcom/stripe/android/stripe3ds2/transaction/Logger;", "", "error", "", "msg", "", "t", "", "info", "Companion", "3ds2sdk_release"}, k = 1, mv = {1, 1, 16})
public interface o {

    /* renamed from: a  reason: collision with root package name */
    public static final a f58a = a.c;

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\b\u0003\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004*\u0002\u0004\u0007\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\r\u0010\u000b\u001a\u00020\fH\u0000¢\u0006\u0002\b\rJ\r\u0010\u000e\u001a\u00020\fH\u0000¢\u0006\u0002\b\u000fR\u0010\u0010\u0003\u001a\u00020\u0004X\u0004¢\u0006\u0004\n\u0002\u0010\u0005R\u0010\u0010\u0006\u001a\u00020\u0007X\u0004¢\u0006\u0004\n\u0002\u0010\bR\u000e\u0010\t\u001a\u00020\nXT¢\u0006\u0002\n\u0000¨\u0006\u0010"}, d2 = {"Lcom/stripe/android/stripe3ds2/transaction/Logger$Companion;", "", "()V", "NOOP_LOGGER", "com/stripe/android/stripe3ds2/transaction/Logger$Companion$NOOP_LOGGER$1", "Lcom/stripe/android/stripe3ds2/transaction/Logger$Companion$NOOP_LOGGER$1;", "REAL_LOGGER", "com/stripe/android/stripe3ds2/transaction/Logger$Companion$REAL_LOGGER$1", "Lcom/stripe/android/stripe3ds2/transaction/Logger$Companion$REAL_LOGGER$1;", "TAG", "", "noop", "Lcom/stripe/android/stripe3ds2/transaction/Logger;", "noop$3ds2sdk_release", "real", "real$3ds2sdk_release", "3ds2sdk_release"}, k = 1, mv = {1, 1, 16})
    public static final class a {

        /* renamed from: a  reason: collision with root package name */
        public static final b f59a;
        public static final C0007a b = new C0007a();
        public static final /* synthetic */ a c;

        /* renamed from: a.a.a.a.d.o$a$a  reason: collision with other inner class name */
        public static final class C0007a implements o {
            public void a(String str) {
                Intrinsics.checkParameterIsNotNull(str, NotificationCompat.CATEGORY_MESSAGE);
            }

            public void a(String str, Throwable th) {
                Intrinsics.checkParameterIsNotNull(str, NotificationCompat.CATEGORY_MESSAGE);
            }
        }

        public static final class b implements o {
            public b(a aVar) {
            }

            public void a(String str) {
                Intrinsics.checkParameterIsNotNull(str, NotificationCompat.CATEGORY_MESSAGE);
                Log.i("StripeSdk", str);
            }

            public void a(String str, Throwable th) {
                Intrinsics.checkParameterIsNotNull(str, NotificationCompat.CATEGORY_MESSAGE);
                Log.e("StripeSdk", str, th);
            }
        }

        static {
            a aVar = new a();
            c = aVar;
            f59a = new b(aVar);
        }

        public final o a() {
            return b;
        }

        public final o b() {
            return f59a;
        }
    }

    void a(String str);

    void a(String str, Throwable th);
}
