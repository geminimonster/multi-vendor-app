package a.a.a.a.d;

import java.security.interfaces.ECPublicKey;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\f\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\b\b\u0018\u0000 \u00172\u00020\u0001:\u0001\u0017B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0005¢\u0006\u0002\u0010\u0007J\t\u0010\r\u001a\u00020\u0003HÆ\u0003J\t\u0010\u000e\u001a\u00020\u0005HÆ\u0003J\t\u0010\u000f\u001a\u00020\u0005HÆ\u0003J'\u0010\u0010\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u0005HÆ\u0001J\u0013\u0010\u0011\u001a\u00020\u00122\b\u0010\u0013\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0014\u001a\u00020\u0015HÖ\u0001J\t\u0010\u0016\u001a\u00020\u0003HÖ\u0001R\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u0011\u0010\u0006\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\t¨\u0006\u0018"}, d2 = {"Lcom/stripe/android/stripe3ds2/transaction/AcsData;", "", "acsUrl", "", "acsEphemPubKey", "Ljava/security/interfaces/ECPublicKey;", "sdkEphemPubKey", "(Ljava/lang/String;Ljava/security/interfaces/ECPublicKey;Ljava/security/interfaces/ECPublicKey;)V", "getAcsEphemPubKey", "()Ljava/security/interfaces/ECPublicKey;", "getAcsUrl", "()Ljava/lang/String;", "getSdkEphemPubKey", "component1", "component2", "component3", "copy", "equals", "", "other", "hashCode", "", "toString", "Companion", "3ds2sdk_release"}, k = 1, mv = {1, 1, 16})
public final class a {
    public static final C0000a d = new C0000a();

    /* renamed from: a  reason: collision with root package name */
    public final String f35a;
    public final ECPublicKey b;
    public final ECPublicKey c;

    /* renamed from: a.a.a.a.d.a$a  reason: collision with other inner class name */
    public static final class C0000a {
    }

    public a(String str, ECPublicKey eCPublicKey, ECPublicKey eCPublicKey2) {
        Intrinsics.checkParameterIsNotNull(str, "acsUrl");
        Intrinsics.checkParameterIsNotNull(eCPublicKey, "acsEphemPubKey");
        Intrinsics.checkParameterIsNotNull(eCPublicKey2, "sdkEphemPubKey");
        this.f35a = str;
        this.b = eCPublicKey;
        this.c = eCPublicKey2;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof a)) {
            return false;
        }
        a aVar = (a) obj;
        return Intrinsics.areEqual((Object) this.f35a, (Object) aVar.f35a) && Intrinsics.areEqual((Object) this.b, (Object) aVar.b) && Intrinsics.areEqual((Object) this.c, (Object) aVar.c);
    }

    public int hashCode() {
        String str = this.f35a;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        ECPublicKey eCPublicKey = this.b;
        int hashCode2 = (hashCode + (eCPublicKey != null ? eCPublicKey.hashCode() : 0)) * 31;
        ECPublicKey eCPublicKey2 = this.c;
        if (eCPublicKey2 != null) {
            i = eCPublicKey2.hashCode();
        }
        return hashCode2 + i;
    }

    public String toString() {
        return "AcsData(acsUrl=" + this.f35a + ", acsEphemPubKey=" + this.b + ", sdkEphemPubKey=" + this.c + ")";
    }
}
