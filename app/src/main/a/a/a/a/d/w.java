package a.a.a.a.d;

import a.a.a.a.d.y;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.Lambda;

public final class w extends Lambda implements Function0<Unit> {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ y.a f78a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public w(y.a aVar) {
        super(0);
        this.f78a = aVar;
    }

    public Object invoke() {
        y.b bVar = this.f78a.b;
        if (bVar != null) {
            bVar.a();
        }
        this.f78a.b = null;
        return Unit.INSTANCE;
    }
}
