package a.a.a.a.d;

import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import kotlin.ResultKt;
import kotlin.Unit;
import kotlin.coroutines.Continuation;
import kotlin.coroutines.CoroutineContext;
import kotlin.coroutines.intrinsics.IntrinsicsKt;
import kotlin.coroutines.jvm.internal.DebugMetadata;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.Intrinsics;
import kotlinx.coroutines.CoroutineScope;
import kotlinx.coroutines.CoroutineScopeKt;
import kotlinx.coroutines.CoroutineStart;
import kotlinx.coroutines.DelayKt;
import kotlinx.coroutines.Dispatchers;
import kotlinx.coroutines.Job;
import kotlinx.coroutines.SupervisorKt;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\u0005\b\u0000\u0018\u0000 \u00102\u00020\u0001:\u0003\u0010\u0011\u0012B\u000f\u0012\b\b\u0002\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u0006\u0010\r\u001a\u00020\u000eJ\u0006\u0010\u000f\u001a\u00020\u000eR\u0010\u0010\u0005\u001a\u0004\u0018\u00010\u0006X\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u001c\u0010\u0007\u001a\u0004\u0018\u00010\bX\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\t\u0010\n\"\u0004\b\u000b\u0010\f¨\u0006\u0013"}, d2 = {"Lcom/stripe/android/stripe3ds2/transaction/ChallengeRequestTimer;", "", "coroutineScope", "Lkotlinx/coroutines/CoroutineScope;", "(Lkotlinx/coroutines/CoroutineScope;)V", "activeJob", "Lkotlinx/coroutines/Job;", "listener", "Lcom/stripe/android/stripe3ds2/transaction/ChallengeRequestTimer$Listener;", "getListener$3ds2sdk_release", "()Lcom/stripe/android/stripe3ds2/transaction/ChallengeRequestTimer$Listener;", "setListener$3ds2sdk_release", "(Lcom/stripe/android/stripe3ds2/transaction/ChallengeRequestTimer$Listener;)V", "cancel", "", "start", "Companion", "Factory", "Listener", "3ds2sdk_release"}, k = 1, mv = {1, 1, 16})
public final class g {
    public static final long d = TimeUnit.SECONDS.toMillis(10);
    public static final a e = new a();

    /* renamed from: a  reason: collision with root package name */
    public c f51a;
    public Job b;
    public final CoroutineScope c;

    public static final class a {
    }

    public static final class b implements a.a.a.a.f.a<g> {
        public g a() {
            return new g(CoroutineScopeKt.CoroutineScope(Dispatchers.getMain().plus(SupervisorKt.SupervisorJob$default((Job) null, 1, (Object) null))));
        }
    }

    public interface c {
        void a();
    }

    @DebugMetadata(c = "com.stripe.android.stripe3ds2.transaction.ChallengeRequestTimer$start$1", f = "ChallengeRequestTimer.kt", i = {0}, l = {20}, m = "invokeSuspend", n = {"$this$launch"}, s = {"L$0"})
    public static final class d extends SuspendLambda implements Function2<CoroutineScope, Continuation<? super Unit>, Object> {

        /* renamed from: a  reason: collision with root package name */
        public CoroutineScope f52a;
        public Object b;
        public int c;
        public final /* synthetic */ g d;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(g gVar, Continuation continuation) {
            super(2, continuation);
            this.d = gVar;
        }

        public final Continuation<Unit> create(Object obj, Continuation<?> continuation) {
            Intrinsics.checkParameterIsNotNull(continuation, "completion");
            d dVar = new d(this.d, continuation);
            dVar.f52a = (CoroutineScope) obj;
            return dVar;
        }

        public final Object invoke(Object obj, Object obj2) {
            return ((d) create(obj, (Continuation) obj2)).invokeSuspend(Unit.INSTANCE);
        }

        public final Object invokeSuspend(Object obj) {
            Object coroutine_suspended = IntrinsicsKt.getCOROUTINE_SUSPENDED();
            int i = this.c;
            if (i == 0) {
                ResultKt.throwOnFailure(obj);
                CoroutineScope coroutineScope = this.f52a;
                a aVar = g.e;
                long j = g.d;
                this.b = coroutineScope;
                this.c = 1;
                if (DelayKt.delay(j, this) == coroutine_suspended) {
                    return coroutine_suspended;
                }
            } else if (i == 1) {
                CoroutineScope coroutineScope2 = (CoroutineScope) this.b;
                ResultKt.throwOnFailure(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            c cVar = this.d.f51a;
            if (cVar != null) {
                cVar.a();
            }
            return Unit.INSTANCE;
        }
    }

    public g(CoroutineScope coroutineScope) {
        Intrinsics.checkParameterIsNotNull(coroutineScope, "coroutineScope");
        this.c = coroutineScope;
    }

    public final void a() {
        this.b = BuildersKt__Builders_commonKt.launch$default(this.c, (CoroutineContext) null, (CoroutineStart) null, new d(this, (Continuation) null), 3, (Object) null);
    }
}
