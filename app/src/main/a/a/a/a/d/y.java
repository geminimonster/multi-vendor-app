package a.a.a.a.d;

import com.stripe.android.stripe3ds2.transaction.ChallengeStatusReceiver;
import java.util.concurrent.CancellationException;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import kotlinx.coroutines.CoroutineScope;
import kotlinx.coroutines.Job;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\u0004\bf\u0018\u00002\u00020\u0001:\u0002\u000b\fJ\b\u0010\b\u001a\u00020\tH&J\b\u0010\n\u001a\u00020\tH&R\u001a\u0010\u0002\u001a\u0004\u0018\u00010\u0003X¦\u000e¢\u0006\f\u001a\u0004\b\u0004\u0010\u0005\"\u0004\b\u0006\u0010\u0007¨\u0006\r"}, d2 = {"Lcom/stripe/android/stripe3ds2/transaction/TransactionTimer;", "", "listener", "Lcom/stripe/android/stripe3ds2/transaction/TransactionTimer$Listener;", "getListener", "()Lcom/stripe/android/stripe3ds2/transaction/TransactionTimer$Listener;", "setListener", "(Lcom/stripe/android/stripe3ds2/transaction/TransactionTimer$Listener;)V", "start", "", "stop", "Default", "Listener", "3ds2sdk_release"}, k = 1, mv = {1, 1, 16})
public interface y {

    public static final class a implements y {

        /* renamed from: a  reason: collision with root package name */
        public final long f80a;
        public b b;
        public Job c;
        public final ChallengeStatusReceiver d;
        public final k e;
        public final a.a.a.a.e.a f;
        public final z g;
        public final CoroutineScope h;

        public a(ChallengeStatusReceiver challengeStatusReceiver, int i, k kVar, a.a.a.a.e.a aVar, z zVar, CoroutineScope coroutineScope) {
            Intrinsics.checkParameterIsNotNull(challengeStatusReceiver, "challengeStatusReceiver");
            Intrinsics.checkParameterIsNotNull(kVar, "errorRequestExecutor");
            Intrinsics.checkParameterIsNotNull(aVar, "creqData");
            Intrinsics.checkParameterIsNotNull(zVar, "transactionTimerProvider");
            Intrinsics.checkParameterIsNotNull(coroutineScope, "coroutineScope");
            this.d = challengeStatusReceiver;
            this.e = kVar;
            this.f = aVar;
            this.g = zVar;
            this.h = coroutineScope;
            this.f80a = TimeUnit.MINUTES.toMillis((long) i);
        }

        public void a() {
            Job job = this.c;
            if (job != null) {
                Job.DefaultImpls.cancel$default(job, (CancellationException) null, 1, (Object) null);
            }
            this.c = null;
            this.g.a(this.f.d);
            this.b = null;
        }

        public void a(b bVar) {
            this.b = bVar;
        }
    }

    public interface b {
        void a();
    }

    void a();

    void a(b bVar);
}
