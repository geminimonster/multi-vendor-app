package a.a.a.a.d;

import a.a.a.a.d.k;
import kotlin.Metadata;
import kotlin.Result;
import kotlin.ResultKt;
import kotlin.Unit;
import kotlin.coroutines.Continuation;
import kotlin.coroutines.CoroutineContext;
import kotlin.coroutines.intrinsics.IntrinsicsKt;
import kotlin.coroutines.jvm.internal.DebugMetadata;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.Intrinsics;
import kotlinx.coroutines.CoroutineScope;
import kotlinx.coroutines.CoroutineScopeKt;
import kotlinx.coroutines.CoroutineStart;
import kotlinx.coroutines.Dispatchers;
import kotlinx.coroutines.Job;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u0000\u0018\u0000 \u000b2\u00020\u0001:\u0002\u000b\fB\u0017\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0002\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\u0010\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\nH\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000¨\u0006\r"}, d2 = {"Lcom/stripe/android/stripe3ds2/transaction/StripeErrorRequestExecutor;", "Lcom/stripe/android/stripe3ds2/transaction/ErrorRequestExecutor;", "httpClient", "Lcom/stripe/android/stripe3ds2/transaction/HttpClient;", "workerScope", "Lkotlinx/coroutines/CoroutineScope;", "(Lcom/stripe/android/stripe3ds2/transaction/HttpClient;Lkotlinx/coroutines/CoroutineScope;)V", "executeAsync", "", "errorData", "Lcom/stripe/android/stripe3ds2/transactions/ErrorData;", "Companion", "Factory", "3ds2sdk_release"}, k = 1, mv = {1, 1, 16})
public final class r implements k {
    @Deprecated
    public static final a c = new a();

    /* renamed from: a  reason: collision with root package name */
    public final l f67a;
    public final CoroutineScope b;

    public static final class a {
    }

    public static final class b implements k.a {
        public k create(String str) {
            Intrinsics.checkParameterIsNotNull(str, "acsUrl");
            return new r(new s(str), CoroutineScopeKt.CoroutineScope(Dispatchers.getIO()));
        }
    }

    @DebugMetadata(c = "com.stripe.android.stripe3ds2.transaction.StripeErrorRequestExecutor$executeAsync$1", f = "StripeErrorRequestExecutor.kt", i = {}, l = {}, m = "invokeSuspend", n = {}, s = {})
    public static final class c extends SuspendLambda implements Function2<CoroutineScope, Continuation<? super Unit>, Object> {

        /* renamed from: a  reason: collision with root package name */
        public CoroutineScope f68a;
        public final /* synthetic */ r b;
        public final /* synthetic */ String c;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(r rVar, String str, Continuation continuation) {
            super(2, continuation);
            this.b = rVar;
            this.c = str;
        }

        public final Continuation<Unit> create(Object obj, Continuation<?> continuation) {
            Intrinsics.checkParameterIsNotNull(continuation, "completion");
            c cVar = new c(this.b, this.c, continuation);
            cVar.f68a = (CoroutineScope) obj;
            return cVar;
        }

        public final Object invoke(Object obj, Object obj2) {
            return ((c) create(obj, (Continuation) obj2)).invokeSuspend(Unit.INSTANCE);
        }

        public final Object invokeSuspend(Object obj) {
            IntrinsicsKt.getCOROUTINE_SUSPENDED();
            ResultKt.throwOnFailure(obj);
            try {
                Result.Companion companion = Result.Companion;
                l lVar = this.b.f67a;
                String str = this.c;
                a aVar = r.c;
                Result.m4constructorimpl(lVar.a(str, "application/json; charset=utf-8"));
            } catch (Throwable th) {
                Result.Companion companion2 = Result.Companion;
                Result.m4constructorimpl(ResultKt.createFailure(th));
            }
            return Unit.INSTANCE;
        }
    }

    public r(l lVar, CoroutineScope coroutineScope) {
        Intrinsics.checkParameterIsNotNull(lVar, "httpClient");
        Intrinsics.checkParameterIsNotNull(coroutineScope, "workerScope");
        this.f67a = lVar;
        this.b = coroutineScope;
    }

    public void a(a.a.a.a.e.c cVar) {
        Object obj;
        Intrinsics.checkParameterIsNotNull(cVar, "errorData");
        try {
            Result.Companion companion = Result.Companion;
            obj = Result.m4constructorimpl(cVar.c().toString());
        } catch (Throwable th) {
            Result.Companion companion2 = Result.Companion;
            obj = Result.m4constructorimpl(ResultKt.createFailure(th));
        }
        if (Result.m7exceptionOrNullimpl(obj) == null) {
            Intrinsics.checkExpressionValueIsNotNull(obj, "runCatching {\n          …         return\n        }");
            Job unused = BuildersKt__Builders_commonKt.launch$default(this.b, (CoroutineContext) null, (CoroutineStart) null, new c(this, (String) obj, (Continuation) null), 3, (Object) null);
        }
    }
}
