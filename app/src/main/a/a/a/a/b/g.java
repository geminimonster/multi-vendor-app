package a.a.a.a.b;

import a.a.a.a.f.d;
import android.content.Context;
import android.provider.Settings;
import kotlin.jvm.internal.Intrinsics;

public final class g implements d<f> {

    /* renamed from: a  reason: collision with root package name */
    public final Context f21a;

    public g(Context context) {
        Intrinsics.checkParameterIsNotNull(context, "context");
        Context applicationContext = context.getApplicationContext();
        Intrinsics.checkExpressionValueIsNotNull(applicationContext, "context.applicationContext");
        this.f21a = applicationContext;
    }

    public Object a() {
        String string = Settings.Secure.getString(this.f21a.getContentResolver(), "android_id");
        if (string == null) {
            string = "";
        }
        return new f(string);
    }
}
