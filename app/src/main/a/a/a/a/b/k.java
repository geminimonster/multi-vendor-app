package a.a.a.a.b;

import a.a.a.a.b.j;
import android.os.Debug;
import com.stripe.android.stripe3ds2.init.Warning;
import java.util.ArrayList;
import java.util.List;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0004\b`\u0018\u00002\u00020\u0001:\u0001\u0007R\u0018\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0005\u0010\u0006¨\u0006\b"}, d2 = {"Lcom/stripe/android/stripe3ds2/init/SecurityChecker;", "", "warnings", "", "Lcom/stripe/android/stripe3ds2/init/Warning;", "getWarnings", "()Ljava/util/List;", "Default", "3ds2sdk_release"}, k = 1, mv = {1, 1, 16})
public interface k {

    public static final class a implements k {
        public static final List<j> b = CollectionsKt.listOf(new j.c(), new j.d(), new j.b(), new j.a(Debug.isDebuggerConnected()), new j.e());

        /* renamed from: a  reason: collision with root package name */
        public final List<Warning> f25a;

        public a(List<? extends j> list) {
            Intrinsics.checkParameterIsNotNull(list, "securityChecks");
            ArrayList<j> arrayList = new ArrayList<>();
            for (T next : list) {
                if (((j) next).a()) {
                    arrayList.add(next);
                }
            }
            ArrayList arrayList2 = new ArrayList(CollectionsKt.collectionSizeOrDefault(arrayList, 10));
            for (j b2 : arrayList) {
                arrayList2.add(b2.b());
            }
            this.f25a = arrayList2;
        }

        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ a(List<j> list, int i) {
            this((i & 1) != 0 ? b : list);
        }

        public List<Warning> getWarnings() {
            return this.f25a;
        }
    }

    List<Warning> getWarnings();
}
