package a.a.a.a.b;

import a.a.a.a.f.d;
import android.os.Build;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt;
import kotlin.collections.MapsKt;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\u0010\u000e\n\u0002\b\r\b\u0000\u0018\u00002\u00020\u0001:\u0001\u0017B\u0015\b\u0010\u0012\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003¢\u0006\u0002\u0010\u0005B\u001d\b\u0000\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003¢\u0006\u0002\u0010\bJ\u0014\u0010\u0016\u001a\u000e\u0012\u0004\u0012\u00020\u000b\u0012\u0004\u0012\u00020\u000b0\nH\u0016R\u000e\u0010\u0006\u001a\u00020\u0007X\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003X\u0004¢\u0006\u0002\n\u0000R&\u0010\t\u001a\u000e\u0012\u0004\u0012\u00020\u000b\u0012\u0004\u0012\u00020\u000b0\n8@X\u0004¢\u0006\f\u0012\u0004\b\f\u0010\r\u001a\u0004\b\u000e\u0010\u000fR&\u0010\u0010\u001a\u000e\u0012\u0004\u0012\u00020\u000b\u0012\u0004\u0012\u00020\u000b0\n8@X\u0004¢\u0006\f\u0012\u0004\b\u0011\u0010\r\u001a\u0004\b\u0012\u0010\u000fR&\u0010\u0013\u001a\u000e\u0012\u0004\u0012\u00020\u000b\u0012\u0004\u0012\u00020\u000b0\n8@X\u0004¢\u0006\f\u0012\u0004\b\u0014\u0010\r\u001a\u0004\b\u0015\u0010\u000f¨\u0006\u0018"}, d2 = {"Lcom/stripe/android/stripe3ds2/init/DeviceParamNotAvailableFactoryImpl;", "Lcom/stripe/android/stripe3ds2/init/DeviceParamNotAvailableFactory;", "hardwareIdSupplier", "Lcom/stripe/android/stripe3ds2/utils/Supplier;", "Lcom/stripe/android/stripe3ds2/init/HardwareId;", "(Lcom/stripe/android/stripe3ds2/utils/Supplier;)V", "apiVersion", "", "(ILcom/stripe/android/stripe3ds2/utils/Supplier;)V", "marketOrRegionRestrictionParams", "", "", "marketOrRegionRestrictionParams$annotations", "()V", "getMarketOrRegionRestrictionParams$3ds2sdk_release", "()Ljava/util/Map;", "permissionParams", "permissionParams$annotations", "getPermissionParams$3ds2sdk_release", "platformVersionParams", "platformVersionParams$annotations", "getPlatformVersionParams$3ds2sdk_release", "create", "Reason", "3ds2sdk_release"}, k = 1, mv = {1, 1, 16})
public final class e implements d {

    /* renamed from: a  reason: collision with root package name */
    public final int f18a;
    public final d<f> b;

    public enum a {
        MARKET_OR_REGION_RESTRICTION("RE01"),
        PLATFORM_VERSION("RE02"),
        PERMISSION("RE03");
        

        /* renamed from: a  reason: collision with root package name */
        public final String f19a;

        /* access modifiers changed from: public */
        a(String str) {
            this.f19a = str;
        }

        public String toString() {
            return this.f19a;
        }
    }

    public e(int i, d<f> dVar) {
        Intrinsics.checkParameterIsNotNull(dVar, "hardwareIdSupplier");
        this.f18a = i;
        this.b = dVar;
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public e(d<f> dVar) {
        this(Build.VERSION.SDK_INT, dVar);
        Intrinsics.checkParameterIsNotNull(dVar, "hardwareIdSupplier");
    }

    public Map<String, String> a() {
        Map<String, String> b2 = b();
        HashMap hashMap = new HashMap();
        if (this.f18a < 26) {
            hashMap.put(c.PARAM_TELE_IMEI_SV.f17a, a.PLATFORM_VERSION.f19a);
            hashMap.put(c.PARAM_BUILD_SERIAL.f17a, a.PLATFORM_VERSION.f19a);
            hashMap.put(c.PARAM_SECURE_INSTALL_NON_MARKET_APPS.f17a, a.PLATFORM_VERSION.f19a);
        }
        if (this.f18a < 23) {
            hashMap.put(c.PARAM_TELE_PHONE_COUNT.f17a, a.PLATFORM_VERSION.f19a);
            hashMap.put(c.PARAM_TELE_IS_HEARING_AID_COMPATIBILITY_SUPPORTED.f17a, a.PLATFORM_VERSION.f19a);
            hashMap.put(c.PARAM_TELE_IS_TTY_MODE_SUPPORTED.f17a, a.PLATFORM_VERSION.f19a);
            hashMap.put(c.PARAM_TELE_IS_WORLD_PHONE.f17a, a.PLATFORM_VERSION.f19a);
            hashMap.put(c.PARAM_BUILD_VERSION_PREVIEW_SDK_INT.f17a, a.PLATFORM_VERSION.f19a);
            hashMap.put(c.PARAM_BUILD_VERSION_SDK_INT.f17a, a.PLATFORM_VERSION.f19a);
            hashMap.put(c.PARAM_BUILD_VERSION_SECURITY_PATCH.f17a, a.PLATFORM_VERSION.f19a);
            hashMap.put(c.PARAM_SYSTEM_DTMF_TONE_TYPE_WHEN_DIALING.f17a, a.PLATFORM_VERSION.f19a);
            hashMap.put(c.PARAM_SYSTEM_VIBRATE_WHEN_RINGING.f17a, a.PLATFORM_VERSION.f19a);
        }
        if (this.f18a > 23) {
            hashMap.put(c.PARAM_SECURE_SYS_PROP_SETTING_VERSION.f17a, a.PLATFORM_VERSION.f19a);
        }
        if (this.f18a < 22) {
            hashMap.put(c.PARAM_TELE_IS_VOICE_CAPABLE.f17a, a.PLATFORM_VERSION.f19a);
        }
        if (this.f18a < 21) {
            hashMap.put(c.PARAM_TELE_IS_SMS_CAPABLE.f17a, a.PLATFORM_VERSION.f19a);
            hashMap.put(c.PARAM_WIFI_IS_5GHZ_BAND_SUPPORTED.f17a, a.PLATFORM_VERSION.f19a);
            hashMap.put(c.PARAM_WIFI_IS_DEVICE_TO_AP_RTT_SUPPORTED.f17a, a.PLATFORM_VERSION.f19a);
            hashMap.put(c.PARAM_WIFI_IS_ENHANCED_POWER_REPORTING_SUPPORTED.f17a, a.PLATFORM_VERSION.f19a);
            hashMap.put(c.PARAM_WIFI_IS_P2P_SUPPORTED.f17a, a.PLATFORM_VERSION.f19a);
            hashMap.put(c.PARAM_WIFI_IS_PREFERRED_NETWORK_OFFLOAD_SUPPORTED.f17a, a.PLATFORM_VERSION.f19a);
            hashMap.put(c.PARAM_WIFI_IS_TDLS_SUPPORTED.f17a, a.PLATFORM_VERSION.f19a);
            hashMap.put(c.PARAM_BUILD_SUPPORTED_32_BIT_ABIS.f17a, a.PLATFORM_VERSION.f19a);
            hashMap.put(c.PARAM_BUILD_SUPPORTED_64_BIT_ABIS.f17a, a.PLATFORM_VERSION.f19a);
            hashMap.put(c.PARAM_SECURE_ACCESSIBILITY_DISPLAY_INVERSION_ENABLED.f17a, a.PLATFORM_VERSION.f19a);
            hashMap.put(c.PARAM_SECURE_SKIP_FIRST_USE_HINTS.f17a, a.PLATFORM_VERSION.f19a);
        }
        Map<String, String> plus = MapsKt.plus(b2, (Map<String, String>) hashMap);
        HashMap hashMap2 = new HashMap();
        hashMap2.put(c.PARAM_WIFI_MAC.f17a, a.PERMISSION.f19a);
        hashMap2.put(c.PARAM_WIFI_BSSID.f17a, a.PERMISSION.f19a);
        hashMap2.put(c.PARAM_WIFI_SSID.f17a, a.PERMISSION.f19a);
        hashMap2.put(c.PARAM_WIFI_NETWORK_ID.f17a, a.PERMISSION.f19a);
        hashMap2.put(c.PARAM_WIFI_IS_5GHZ_BAND_SUPPORTED.f17a, a.PERMISSION.f19a);
        hashMap2.put(c.PARAM_WIFI_IS_DEVICE_TO_AP_RTT_SUPPORTED.f17a, a.PERMISSION.f19a);
        hashMap2.put(c.PARAM_WIFI_IS_ENHANCED_POWER_REPORTING_SUPPORTED.f17a, a.PERMISSION.f19a);
        hashMap2.put(c.PARAM_WIFI_IS_P2P_SUPPORTED.f17a, a.PERMISSION.f19a);
        hashMap2.put(c.PARAM_WIFI_IS_PREFERRED_NETWORK_OFFLOAD_SUPPORTED.f17a, a.PERMISSION.f19a);
        hashMap2.put(c.PARAM_WIFI_IS_SCAN_ALWAYS_AVAILABLE.f17a, a.PERMISSION.f19a);
        hashMap2.put(c.PARAM_WIFI_IS_TDLS_SUPPORTED.f17a, a.PERMISSION.f19a);
        hashMap2.put(c.PARAM_LATITUDE.f17a, a.PERMISSION.f19a);
        hashMap2.put(c.PARAM_LONGITUDE.f17a, a.PERMISSION.f19a);
        if (!(this.b.a().f20a.length() > 0)) {
            hashMap2.put(c.PARAM_HARDWARE_ID.f17a, a.PLATFORM_VERSION.f19a);
        }
        hashMap2.put(c.PARAM_DEVICE_NAME.f17a, a.PERMISSION.f19a);
        hashMap2.put(c.PARAM_BLUETOOTH_ADDRESS.f17a, a.PERMISSION.f19a);
        hashMap2.put(c.PARAM_BLUETOOTH_BONDED_DEVICE.f17a, a.PERMISSION.f19a);
        hashMap2.put(c.PARAM_BLUETOOTH_IS_ENABLED.f17a, a.PERMISSION.f19a);
        hashMap2.put(c.PARAM_TELE_DEVICE_ID.f17a, a.PERMISSION.f19a);
        hashMap2.put(c.PARAM_TELE_SUBSCRIBER_ID.f17a, a.PERMISSION.f19a);
        hashMap2.put(c.PARAM_TELE_IMEI_SV.f17a, a.PERMISSION.f19a);
        hashMap2.put(c.PARAM_TELE_GROUP_IDENTIFIER_L1.f17a, a.PERMISSION.f19a);
        hashMap2.put(c.PARAM_TELE_SIM_SERIAL_NUMBER.f17a, a.PERMISSION.f19a);
        hashMap2.put(c.PARAM_TELE_VOICE_MAIL_ALPHA_TAG.f17a, a.PERMISSION.f19a);
        hashMap2.put(c.PARAM_TELE_VOICE_MAIL_NUMBER.f17a, a.PERMISSION.f19a);
        hashMap2.put(c.PARAM_TELE_IS_TTY_MODE_SUPPORTED.f17a, a.PERMISSION.f19a);
        hashMap2.put(c.PARAM_TELE_IS_WORLD_PHONE.f17a, a.PERMISSION.f19a);
        hashMap2.put(c.PARAM_BUILD_SERIAL.f17a, a.PERMISSION.f19a);
        hashMap2.put(c.PARAM_SECURE_INSTALL_NON_MARKET_APPS.f17a, a.PERMISSION.f19a);
        return MapsKt.plus(plus, (Map<String, String>) hashMap2);
    }

    public final Map<String, String> b() {
        HashMap hashMap = new HashMap();
        List listOf = CollectionsKt.listOf(c.PARAM_PLATFORM, c.PARAM_DEVICE_MODEL, c.PARAM_OS_NAME, c.PARAM_OS_VERSION, c.PARAM_LOCALE, c.PARAM_TIME_ZONE, c.PARAM_HARDWARE_ID, c.PARAM_SCREEN_RESOLUTION);
        for (c cVar : c.values()) {
            if (!listOf.contains(cVar)) {
                hashMap.put(cVar.f17a, a.MARKET_OR_REGION_RESTRICTION.f19a);
            }
        }
        return hashMap;
    }
}
