package a.a.a.a.b;

import kotlin.jvm.internal.Intrinsics;

public final class f {

    /* renamed from: a  reason: collision with root package name */
    public final String f20a;

    public f(String str) {
        Intrinsics.checkParameterIsNotNull(str, "value");
        this.f20a = str;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            return (obj instanceof f) && Intrinsics.areEqual((Object) this.f20a, (Object) ((f) obj).f20a);
        }
        return true;
    }

    public int hashCode() {
        String str = this.f20a;
        if (str != null) {
            return str.hashCode();
        }
        return 0;
    }

    public String toString() {
        return "HardwareId(value=" + this.f20a + ")";
    }
}
