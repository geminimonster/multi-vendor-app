package a.a.a.a.b;

import com.stripe.android.stripe3ds2.init.Warning;
import com.stripe.android.stripe3ds2.service.StripeThreeDs2ServiceImpl;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.List;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b0\u0018\u00002\u00020\u0001:\u0005\t\n\u000b\f\rB\u000f\b\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\b\u0010\u0007\u001a\u00020\bH&R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006\u0001\u0005\u000e\u000f\u0010\u0011\u0012¨\u0006\u0013"}, d2 = {"Lcom/stripe/android/stripe3ds2/init/SecurityCheck;", "", "warning", "Lcom/stripe/android/stripe3ds2/init/Warning;", "(Lcom/stripe/android/stripe3ds2/init/Warning;)V", "getWarning", "()Lcom/stripe/android/stripe3ds2/init/Warning;", "check", "", "DebuggerAttached", "Emulator", "RootedCheck", "Tampered", "UnsupportedOS", "Lcom/stripe/android/stripe3ds2/init/SecurityCheck$RootedCheck;", "Lcom/stripe/android/stripe3ds2/init/SecurityCheck$Tampered;", "Lcom/stripe/android/stripe3ds2/init/SecurityCheck$Emulator;", "Lcom/stripe/android/stripe3ds2/init/SecurityCheck$DebuggerAttached;", "Lcom/stripe/android/stripe3ds2/init/SecurityCheck$UnsupportedOS;", "3ds2sdk_release"}, k = 1, mv = {1, 1, 16})
public abstract class j {

    /* renamed from: a  reason: collision with root package name */
    public final Warning f24a;

    public static final class a extends j {
        public static final Warning c = new Warning("SW04", "A debugger is attached to the App.", Warning.Severity.MEDIUM);
        public final boolean b;

        public a(boolean z) {
            super(c, (DefaultConstructorMarker) null);
            this.b = z;
        }

        public boolean a() {
            return this.b;
        }
    }

    public static final class b extends j {
        public static final Warning b = new Warning("SW02", "An emulator is being used to run the App.", Warning.Severity.HIGH);

        public b() {
            super(b, (DefaultConstructorMarker) null);
        }

        public boolean a() {
            return c();
        }

        /* JADX WARNING: Code restructure failed: missing block: B:15:0x006f, code lost:
            if (kotlin.text.StringsKt.startsWith$default(r0, com.facebook.share.internal.MessengerShareContentUtility.TEMPLATE_GENERIC_TYPE, false, 2, (java.lang.Object) null) == false) goto L_0x0071;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final boolean c() {
            /*
                r7 = this;
                java.lang.String r0 = android.os.Build.FINGERPRINT
                java.lang.String r1 = "Build.FINGERPRINT"
                kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r0, r1)
                java.lang.String r2 = "generic"
                r3 = 0
                r4 = 2
                r5 = 0
                boolean r0 = kotlin.text.StringsKt.startsWith$default(r0, r2, r5, r4, r3)
                if (r0 != 0) goto L_0x0079
                java.lang.String r0 = android.os.Build.FINGERPRINT
                kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r0, r1)
                java.lang.String r1 = "unknown"
                boolean r0 = kotlin.text.StringsKt.startsWith$default(r0, r1, r5, r4, r3)
                if (r0 != 0) goto L_0x0079
                java.lang.String r0 = android.os.Build.MODEL
                java.lang.String r1 = "Build.MODEL"
                kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r0, r1)
                java.lang.String r6 = "Emulator"
                boolean r0 = kotlin.text.StringsKt.contains$default((java.lang.CharSequence) r0, (java.lang.CharSequence) r6, (boolean) r5, (int) r4, (java.lang.Object) r3)
                if (r0 != 0) goto L_0x0079
                java.lang.String r0 = android.os.Build.MODEL
                kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r0, r1)
                java.lang.String r6 = "Android SDK built for x86"
                boolean r0 = kotlin.text.StringsKt.contains$default((java.lang.CharSequence) r0, (java.lang.CharSequence) r6, (boolean) r5, (int) r4, (java.lang.Object) r3)
                if (r0 != 0) goto L_0x0079
                java.lang.String r0 = android.os.Build.MODEL
                kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r0, r1)
                java.lang.String r1 = "google_sdk"
                boolean r0 = kotlin.text.StringsKt.contains$default((java.lang.CharSequence) r0, (java.lang.CharSequence) r1, (boolean) r5, (int) r4, (java.lang.Object) r3)
                if (r0 != 0) goto L_0x0079
                java.lang.String r0 = android.os.Build.MANUFACTURER
                java.lang.String r6 = "Build.MANUFACTURER"
                kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r0, r6)
                java.lang.String r6 = "Genymotion"
                boolean r0 = kotlin.text.StringsKt.contains$default((java.lang.CharSequence) r0, (java.lang.CharSequence) r6, (boolean) r5, (int) r4, (java.lang.Object) r3)
                if (r0 != 0) goto L_0x0079
                java.lang.String r0 = android.os.Build.BRAND
                java.lang.String r6 = "Build.BRAND"
                kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r0, r6)
                boolean r0 = kotlin.text.StringsKt.startsWith$default(r0, r2, r5, r4, r3)
                if (r0 == 0) goto L_0x0071
                java.lang.String r0 = android.os.Build.DEVICE
                java.lang.String r6 = "Build.DEVICE"
                kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r0, r6)
                boolean r0 = kotlin.text.StringsKt.startsWith$default(r0, r2, r5, r4, r3)
                if (r0 != 0) goto L_0x0079
            L_0x0071:
                java.lang.String r0 = android.os.Build.PRODUCT
                boolean r0 = kotlin.jvm.internal.Intrinsics.areEqual((java.lang.Object) r1, (java.lang.Object) r0)
                if (r0 == 0) goto L_0x007a
            L_0x0079:
                r5 = 1
            L_0x007a:
                return r5
            */
            throw new UnsupportedOperationException("Method not decompiled: a.a.a.a.b.j.b.c():boolean");
        }
    }

    public static final class c extends j {
        public static final List<String> b = CollectionsKt.listOf("/sbin/", "/system/bin/", "/system/xbin/", "/data/local/xbin/", "/data/local/bin/", "/system/sd/xbin/", "/system/bin/failsafe/", "/data/local/");
        public static final Warning c = new Warning("SW01", "The device is jailbroken.", Warning.Severity.HIGH);

        public c() {
            super(c, (DefaultConstructorMarker) null);
        }

        /* JADX WARNING: Removed duplicated region for block: B:12:0x0040  */
        /* JADX WARNING: Removed duplicated region for block: B:17:? A[RETURN, SYNTHETIC] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public boolean a() {
            /*
                r6 = this;
                java.util.List<java.lang.String> r0 = b
                boolean r1 = r0 instanceof java.util.Collection
                r2 = 1
                r3 = 0
                if (r1 == 0) goto L_0x000f
                boolean r1 = r0.isEmpty()
                if (r1 == 0) goto L_0x000f
                goto L_0x003d
            L_0x000f:
                java.util.Iterator r0 = r0.iterator()
            L_0x0013:
                boolean r1 = r0.hasNext()
                if (r1 == 0) goto L_0x003d
                java.lang.Object r1 = r0.next()
                java.lang.String r1 = (java.lang.String) r1
                java.io.File r4 = new java.io.File
                java.lang.StringBuilder r5 = new java.lang.StringBuilder
                r5.<init>()
                r5.append(r1)
                java.lang.String r1 = "su"
                r5.append(r1)
                java.lang.String r1 = r5.toString()
                r4.<init>(r1)
                boolean r1 = r4.exists()
                if (r1 == 0) goto L_0x0013
                r0 = 1
                goto L_0x003e
            L_0x003d:
                r0 = 0
            L_0x003e:
                if (r0 != 0) goto L_0x0066
                java.io.File r0 = new java.io.File
                java.lang.StringBuilder r1 = new java.lang.StringBuilder
                r1.<init>()
                java.io.File r4 = android.os.Environment.getRootDirectory()
                java.lang.String r4 = r4.toString()
                r1.append(r4)
                java.lang.String r4 = "/Superuser"
                r1.append(r4)
                java.lang.String r1 = r1.toString()
                r0.<init>(r1)
                boolean r0 = r0.isDirectory()
                if (r0 == 0) goto L_0x0065
                goto L_0x0066
            L_0x0065:
                r2 = 0
            L_0x0066:
                return r2
            */
            throw new UnsupportedOperationException("Method not decompiled: a.a.a.a.b.j.c.a():boolean");
        }
    }

    public static final class d extends j {
        public static final Warning b = new Warning("SW02", "The integrity of the SDK has been tampered.", Warning.Severity.HIGH);

        public d() {
            super(b, (DefaultConstructorMarker) null);
        }

        public boolean a() {
            Field[] declaredFields = StripeThreeDs2ServiceImpl.class.getDeclaredFields();
            Intrinsics.checkExpressionValueIsNotNull(declaredFields, "StripeThreeDs2ServiceImp…class.java.declaredFields");
            if (!(declaredFields.length == 12)) {
                return true;
            }
            Method[] declaredMethods = StripeThreeDs2ServiceImpl.class.getDeclaredMethods();
            Intrinsics.checkExpressionValueIsNotNull(declaredMethods, "StripeThreeDs2ServiceImp…lass.java.declaredMethods");
            if (!(declaredMethods.length == 12)) {
                return true;
            }
            return false;
        }
    }

    public static final class e extends j {
        public static final Warning b = new Warning("SW05", "The OS or the OS version is not supported.", Warning.Severity.HIGH);

        public e() {
            super(b, (DefaultConstructorMarker) null);
        }

        public boolean a() {
            return false;
        }
    }

    public j(Warning warning) {
        this.f24a = warning;
    }

    public /* synthetic */ j(Warning warning, DefaultConstructorMarker defaultConstructorMarker) {
        this(warning);
    }

    public abstract boolean a();

    public final Warning b() {
        return this.f24a;
    }
}
