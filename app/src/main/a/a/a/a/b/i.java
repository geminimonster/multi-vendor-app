package a.a.a.a.b;

import a.a.a.a.f.d;
import android.content.Context;
import android.content.SharedPreferences;
import com.stripe.android.AnalyticsDataFactory;
import com.stripe.android.stripe3ds2.exceptions.SDKRuntimeException;
import java.util.UUID;
import kotlin.Metadata;
import kotlin.Result;
import kotlin.ResultKt;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u0000\u0018\u0000 \f2\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\fB\u000f\b\u0016\u0012\u0006\u0010\u0003\u001a\u00020\u0004¢\u0006\u0002\u0010\u0005B\u0017\b\u0001\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ\b\u0010\u000b\u001a\u00020\u0002H\u0016R\u000e\u0010\t\u001a\u00020\nX\u0004¢\u0006\u0002\n\u0000¨\u0006\r"}, d2 = {"Lcom/stripe/android/stripe3ds2/init/SdkAppIdSupplierImpl;", "Lcom/stripe/android/stripe3ds2/utils/Supplier;", "Lcom/stripe/android/stripe3ds2/init/SdkAppId;", "context", "Landroid/content/Context;", "(Landroid/content/Context;)V", "appVersion", "", "(Landroid/content/Context;I)V", "sharedPrefs", "Landroid/content/SharedPreferences;", "get", "Companion", "3ds2sdk_release"}, k = 1, mv = {1, 1, 16})
public final class i implements d<h> {
    @Deprecated
    public static final a b = new a();

    /* renamed from: a  reason: collision with root package name */
    public final SharedPreferences f23a;

    public static final class a {
        public static final /* synthetic */ int a(a aVar, Context context) {
            Integer num;
            if (aVar != null) {
                try {
                    Result.Companion companion = Result.Companion;
                    num = Result.m4constructorimpl(Integer.valueOf(context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionCode));
                } catch (Throwable th) {
                    Result.Companion companion2 = Result.Companion;
                    num = Result.m4constructorimpl(ResultKt.createFailure(th));
                }
                if (Result.m10isFailureimpl(num)) {
                    num = -1;
                }
                return ((Number) num).intValue();
            }
            throw null;
        }
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public i(Context context) {
        this(context, a.a(b, context));
        Intrinsics.checkParameterIsNotNull(context, "context");
    }

    public i(Context context, int i) {
        Intrinsics.checkParameterIsNotNull(context, "context");
        SharedPreferences sharedPreferences = context.getSharedPreferences("app_info", 0);
        Intrinsics.checkExpressionValueIsNotNull(sharedPreferences, "context.getSharedPrefere…o\", Context.MODE_PRIVATE)");
        this.f23a = sharedPreferences;
        if (i != sharedPreferences.getInt(AnalyticsDataFactory.FIELD_APP_VERSION, 0)) {
            String uuid = UUID.randomUUID().toString();
            Intrinsics.checkExpressionValueIsNotNull(uuid, "UUID.randomUUID().toString()");
            this.f23a.edit().putInt(AnalyticsDataFactory.FIELD_APP_VERSION, i).putString("sdk_app_id", uuid).apply();
        }
    }

    public Object a() {
        String string = this.f23a.getString("sdk_app_id", (String) null);
        if (string != null) {
            Intrinsics.checkExpressionValueIsNotNull(string, "sharedPrefs.getString(KE…pp id is not available\"))");
            return new h(string);
        }
        throw new SDKRuntimeException(new RuntimeException("SDK app id is not available"));
    }
}
