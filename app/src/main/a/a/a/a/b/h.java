package a.a.a.a.b;

import kotlin.jvm.internal.Intrinsics;

public final class h {

    /* renamed from: a  reason: collision with root package name */
    public final String f22a;

    public h(String str) {
        Intrinsics.checkParameterIsNotNull(str, "value");
        this.f22a = str;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            return (obj instanceof h) && Intrinsics.areEqual((Object) this.f22a, (Object) ((h) obj).f22a);
        }
        return true;
    }

    public int hashCode() {
        String str = this.f22a;
        if (str != null) {
            return str.hashCode();
        }
        return 0;
    }

    public String toString() {
        return "SdkAppId(value=" + this.f22a + ")";
    }
}
