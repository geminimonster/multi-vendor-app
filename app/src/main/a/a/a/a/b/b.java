package a.a.a.a.b;

import a.a.a.a.f.d;
import android.content.Context;
import android.content.res.Resources;
import android.os.Build;
import android.util.DisplayMetrics;
import androidx.core.os.LocaleListCompat;
import java.util.Arrays;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;
import kotlin.TuplesKt;
import kotlin.collections.MapsKt;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.StringCompanionObject;

public final class b implements a {

    /* renamed from: a  reason: collision with root package name */
    public final DisplayMetrics f16a;
    public final d<f> b;

    public b(Context context, d<f> dVar) {
        Intrinsics.checkParameterIsNotNull(context, "context");
        Intrinsics.checkParameterIsNotNull(dVar, "hardwareIdSupplier");
        this.b = dVar;
        Resources resources = context.getResources();
        Intrinsics.checkExpressionValueIsNotNull(resources, "context.resources");
        DisplayMetrics displayMetrics = resources.getDisplayMetrics();
        Intrinsics.checkExpressionValueIsNotNull(displayMetrics, "context.resources.displayMetrics");
        this.f16a = displayMetrics;
    }

    public Map<String, Object> a() {
        String str = this.b.a().f20a;
        boolean z = false;
        String str2 = c.PARAM_LOCALE.f17a;
        Locale[] localeArr = {Locale.getDefault()};
        String str3 = c.PARAM_TIME_ZONE.f17a;
        TimeZone timeZone = TimeZone.getDefault();
        Intrinsics.checkExpressionValueIsNotNull(timeZone, "TimeZone.getDefault()");
        String str4 = c.PARAM_SCREEN_RESOLUTION.f17a;
        StringCompanionObject stringCompanionObject = StringCompanionObject.INSTANCE;
        Locale locale = Locale.ROOT;
        Intrinsics.checkExpressionValueIsNotNull(locale, "Locale.ROOT");
        String format = String.format(locale, "%sx%s", Arrays.copyOf(new Object[]{Integer.valueOf(this.f16a.heightPixels), Integer.valueOf(this.f16a.widthPixels)}, 2));
        Intrinsics.checkExpressionValueIsNotNull(format, "java.lang.String.format(locale, format, *args)");
        Map mapOf = MapsKt.mapOf(TuplesKt.to(c.PARAM_PLATFORM.f17a, "Android"), TuplesKt.to(c.PARAM_DEVICE_MODEL.f17a, Build.MODEL), TuplesKt.to(c.PARAM_OS_NAME.f17a, Build.VERSION.CODENAME), TuplesKt.to(c.PARAM_OS_VERSION.f17a, Build.VERSION.RELEASE), TuplesKt.to(str2, LocaleListCompat.create(localeArr).toLanguageTags()), TuplesKt.to(str3, timeZone.getDisplayName()), TuplesKt.to(str4, format));
        if (str.length() > 0) {
            z = true;
        }
        return MapsKt.plus(mapOf, z ? MapsKt.mapOf(TuplesKt.to(c.PARAM_HARDWARE_ID.f17a, str)) : MapsKt.emptyMap());
    }
}
