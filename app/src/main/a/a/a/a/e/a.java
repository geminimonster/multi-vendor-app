package a.a.a.a.e;

import android.os.Parcel;
import android.os.Parcelable;
import com.stripe.android.stripe3ds2.exceptions.SDKRuntimeException;
import com.stripe.android.stripe3ds2.transactions.MessageExtension;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import kotlin.Metadata;
import kotlin.Result;
import kotlin.ResultKt;
import kotlin.jvm.internal.Intrinsics;
import org.json.JSONArray;
import org.json.JSONObject;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000V\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u001e\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\b\b\u0018\u0000 >2\u00020\u00012\u00020\u0002:\u0002=>Bs\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0004\u0012\u0006\u0010\u0007\u001a\u00020\u0004\u0012\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\u0004\u0012\n\b\u0002\u0010\t\u001a\u0004\u0018\u00010\n\u0012\n\b\u0002\u0010\u000b\u001a\u0004\u0018\u00010\u0004\u0012\u0010\b\u0002\u0010\f\u001a\n\u0012\u0004\u0012\u00020\u000e\u0018\u00010\r\u0012\n\b\u0002\u0010\u000f\u001a\u0004\u0018\u00010\u0010\u0012\n\b\u0002\u0010\u0011\u001a\u0004\u0018\u00010\u0010¢\u0006\u0002\u0010\u0012J\t\u0010\"\u001a\u00020\u0004HÆ\u0003J\u0010\u0010#\u001a\u0004\u0018\u00010\u0010HÆ\u0003¢\u0006\u0002\u0010\u001dJ\t\u0010$\u001a\u00020\u0004HÆ\u0003J\t\u0010%\u001a\u00020\u0004HÆ\u0003J\t\u0010&\u001a\u00020\u0004HÆ\u0003J\u000b\u0010'\u001a\u0004\u0018\u00010\u0004HÆ\u0003J\u000b\u0010(\u001a\u0004\u0018\u00010\nHÆ\u0003J\u000b\u0010)\u001a\u0004\u0018\u00010\u0004HÆ\u0003J\u0011\u0010*\u001a\n\u0012\u0004\u0012\u00020\u000e\u0018\u00010\rHÆ\u0003J\u0010\u0010+\u001a\u0004\u0018\u00010\u0010HÆ\u0003¢\u0006\u0002\u0010\u001dJ\u0001\u0010,\u001a\u00020\u00002\b\b\u0002\u0010\u0003\u001a\u00020\u00042\b\b\u0002\u0010\u0005\u001a\u00020\u00042\b\b\u0002\u0010\u0006\u001a\u00020\u00042\b\b\u0002\u0010\u0007\u001a\u00020\u00042\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\u00042\n\b\u0002\u0010\t\u001a\u0004\u0018\u00010\n2\n\b\u0002\u0010\u000b\u001a\u0004\u0018\u00010\u00042\u0010\b\u0002\u0010\f\u001a\n\u0012\u0004\u0012\u00020\u000e\u0018\u00010\r2\n\b\u0002\u0010\u000f\u001a\u0004\u0018\u00010\u00102\n\b\u0002\u0010\u0011\u001a\u0004\u0018\u00010\u0010HÆ\u0001¢\u0006\u0002\u0010-J\t\u0010.\u001a\u00020/HÖ\u0001J\u0013\u00100\u001a\u00020\u00102\b\u00101\u001a\u0004\u0018\u000102HÖ\u0003J\t\u00103\u001a\u00020/HÖ\u0001J\r\u00104\u001a\u000205H\u0000¢\u0006\u0002\b6J\t\u00107\u001a\u00020\u0004HÖ\u0001J\u0019\u00108\u001a\u0002092\u0006\u0010:\u001a\u00020;2\u0006\u0010<\u001a\u00020/HÖ\u0001R\u0011\u0010\u0006\u001a\u00020\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u0014R\u0013\u0010\t\u001a\u0004\u0018\u00010\n¢\u0006\b\n\u0000\u001a\u0004\b\u0015\u0010\u0016R\u0013\u0010\b\u001a\u0004\u0018\u00010\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0017\u0010\u0014R\u0013\u0010\u000b\u001a\u0004\u0018\u00010\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0018\u0010\u0014R\u0019\u0010\f\u001a\n\u0012\u0004\u0012\u00020\u000e\u0018\u00010\r¢\u0006\b\n\u0000\u001a\u0004\b\u0019\u0010\u001aR\u0011\u0010\u0003\u001a\u00020\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u001b\u0010\u0014R\u0015\u0010\u000f\u001a\u0004\u0018\u00010\u0010¢\u0006\n\n\u0002\u0010\u001e\u001a\u0004\b\u001c\u0010\u001dR\u0011\u0010\u0007\u001a\u00020\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u001f\u0010\u0014R\u0015\u0010\u0011\u001a\u0004\u0018\u00010\u0010¢\u0006\n\n\u0002\u0010\u001e\u001a\u0004\b \u0010\u001dR\u0011\u0010\u0005\u001a\u00020\u0004¢\u0006\b\n\u0000\u001a\u0004\b!\u0010\u0014¨\u0006?"}, d2 = {"Lcom/stripe/android/stripe3ds2/transactions/ChallengeRequestData;", "Ljava/io/Serializable;", "Landroid/os/Parcelable;", "messageVersion", "", "threeDsServerTransId", "acsTransId", "sdkTransId", "challengeDataEntry", "cancelReason", "Lcom/stripe/android/stripe3ds2/transactions/ChallengeRequestData$CancelReason;", "challengeHtmlDataEntry", "messageExtensions", "", "Lcom/stripe/android/stripe3ds2/transactions/MessageExtension;", "oobContinue", "", "shouldResendChallenge", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/stripe/android/stripe3ds2/transactions/ChallengeRequestData$CancelReason;Ljava/lang/String;Ljava/util/List;Ljava/lang/Boolean;Ljava/lang/Boolean;)V", "getAcsTransId", "()Ljava/lang/String;", "getCancelReason", "()Lcom/stripe/android/stripe3ds2/transactions/ChallengeRequestData$CancelReason;", "getChallengeDataEntry", "getChallengeHtmlDataEntry", "getMessageExtensions", "()Ljava/util/List;", "getMessageVersion", "getOobContinue", "()Ljava/lang/Boolean;", "Ljava/lang/Boolean;", "getSdkTransId", "getShouldResendChallenge", "getThreeDsServerTransId", "component1", "component10", "component2", "component3", "component4", "component5", "component6", "component7", "component8", "component9", "copy", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/stripe/android/stripe3ds2/transactions/ChallengeRequestData$CancelReason;Ljava/lang/String;Ljava/util/List;Ljava/lang/Boolean;Ljava/lang/Boolean;)Lcom/stripe/android/stripe3ds2/transactions/ChallengeRequestData;", "describeContents", "", "equals", "other", "", "hashCode", "toJson", "Lorg/json/JSONObject;", "toJson$3ds2sdk_release", "toString", "writeToParcel", "", "parcel", "Landroid/os/Parcel;", "flags", "CancelReason", "Companion", "3ds2sdk_release"}, k = 1, mv = {1, 1, 16})
public final class a implements Serializable, Parcelable {
    public static final Parcelable.Creator CREATOR = new b();

    /* renamed from: a  reason: collision with root package name */
    public final String f82a;
    public final String b;
    public final String c;
    public final String d;
    public final String e;
    public final C0011a f;
    public final String g;
    public final List<MessageExtension> h;
    public final Boolean i;
    public final Boolean j;

    /* renamed from: a.a.a.a.e.a$a  reason: collision with other inner class name */
    public enum C0011a {
        UserSelected("01");
        

        /* renamed from: a  reason: collision with root package name */
        public final String f83a;

        /* access modifiers changed from: public */
        C0011a(String str) {
            this.f83a = str;
        }
    }

    public static class b implements Parcelable.Creator {
        public final Object createFromParcel(Parcel parcel) {
            ArrayList arrayList;
            Boolean bool;
            Boolean bool2;
            Intrinsics.checkParameterIsNotNull(parcel, "in");
            String readString = parcel.readString();
            String readString2 = parcel.readString();
            String readString3 = parcel.readString();
            String readString4 = parcel.readString();
            String readString5 = parcel.readString();
            C0011a aVar = parcel.readInt() != 0 ? (C0011a) Enum.valueOf(C0011a.class, parcel.readString()) : null;
            String readString6 = parcel.readString();
            if (parcel.readInt() != 0) {
                int readInt = parcel.readInt();
                arrayList = new ArrayList(readInt);
                while (readInt != 0) {
                    arrayList.add((MessageExtension) MessageExtension.CREATOR.createFromParcel(parcel));
                    readInt--;
                }
            } else {
                arrayList = null;
            }
            boolean z = true;
            if (parcel.readInt() != 0) {
                bool = Boolean.valueOf(parcel.readInt() != 0);
            } else {
                bool = null;
            }
            if (parcel.readInt() != 0) {
                if (parcel.readInt() == 0) {
                    z = false;
                }
                bool2 = Boolean.valueOf(z);
            } else {
                bool2 = null;
            }
            return new a(readString, readString2, readString3, readString4, readString5, aVar, readString6, arrayList, bool, bool2);
        }

        public final Object[] newArray(int i) {
            return new a[i];
        }
    }

    public a(String str, String str2, String str3, String str4, String str5, C0011a aVar, String str6, List<MessageExtension> list, Boolean bool, Boolean bool2) {
        Intrinsics.checkParameterIsNotNull(str, "messageVersion");
        Intrinsics.checkParameterIsNotNull(str2, "threeDsServerTransId");
        Intrinsics.checkParameterIsNotNull(str3, "acsTransId");
        Intrinsics.checkParameterIsNotNull(str4, "sdkTransId");
        this.f82a = str;
        this.b = str2;
        this.c = str3;
        this.d = str4;
        this.e = str5;
        this.f = aVar;
        this.g = str6;
        this.h = list;
        this.i = bool;
        this.j = bool2;
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* synthetic */ a(java.lang.String r15, java.lang.String r16, java.lang.String r17, java.lang.String r18, java.lang.String r19, a.a.a.a.e.a.C0011a r20, java.lang.String r21, java.util.List r22, java.lang.Boolean r23, java.lang.Boolean r24, int r25) {
        /*
            r14 = this;
            r0 = r25
            r1 = r0 & 16
            r2 = 0
            if (r1 == 0) goto L_0x0009
            r8 = r2
            goto L_0x000b
        L_0x0009:
            r8 = r19
        L_0x000b:
            r1 = r0 & 32
            if (r1 == 0) goto L_0x0011
            r9 = r2
            goto L_0x0013
        L_0x0011:
            r9 = r20
        L_0x0013:
            r1 = r0 & 64
            if (r1 == 0) goto L_0x0019
            r10 = r2
            goto L_0x001b
        L_0x0019:
            r10 = r21
        L_0x001b:
            r1 = r0 & 128(0x80, float:1.794E-43)
            if (r1 == 0) goto L_0x0021
            r11 = r2
            goto L_0x0023
        L_0x0021:
            r11 = r22
        L_0x0023:
            r1 = r0 & 256(0x100, float:3.59E-43)
            if (r1 == 0) goto L_0x0029
            r12 = r2
            goto L_0x002b
        L_0x0029:
            r12 = r23
        L_0x002b:
            r0 = r0 & 512(0x200, float:7.175E-43)
            if (r0 == 0) goto L_0x0031
            r13 = r2
            goto L_0x0033
        L_0x0031:
            r13 = r24
        L_0x0033:
            r3 = r14
            r4 = r15
            r5 = r16
            r6 = r17
            r7 = r18
            r3.<init>(r4, r5, r6, r7, r8, r9, r10, r11, r12, r13)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: a.a.a.a.e.a.<init>(java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, a.a.a.a.e.a$a, java.lang.String, java.util.List, java.lang.Boolean, java.lang.Boolean, int):void");
    }

    public static /* synthetic */ a a(a aVar, String str, String str2, String str3, String str4, String str5, C0011a aVar2, String str6, List list, Boolean bool, Boolean bool2, int i2) {
        a aVar3 = aVar;
        int i3 = i2;
        String str7 = (i3 & 1) != 0 ? aVar3.f82a : str;
        String str8 = (i3 & 2) != 0 ? aVar3.b : str2;
        String str9 = (i3 & 4) != 0 ? aVar3.c : str3;
        String str10 = (i3 & 8) != 0 ? aVar3.d : str4;
        String str11 = (i3 & 16) != 0 ? aVar3.e : str5;
        C0011a aVar4 = (i3 & 32) != 0 ? aVar3.f : aVar2;
        String str12 = (i3 & 64) != 0 ? aVar3.g : str6;
        List list2 = (i3 & 128) != 0 ? aVar3.h : list;
        Boolean bool3 = (i3 & 256) != 0 ? aVar3.i : bool;
        Boolean bool4 = (i3 & 512) != 0 ? aVar3.j : bool2;
        if (aVar3 != null) {
            Intrinsics.checkParameterIsNotNull(str7, "messageVersion");
            Intrinsics.checkParameterIsNotNull(str8, "threeDsServerTransId");
            Intrinsics.checkParameterIsNotNull(str9, "acsTransId");
            Intrinsics.checkParameterIsNotNull(str10, "sdkTransId");
            return new a(str7, str8, str9, str10, str11, aVar4, str12, list2, bool3, bool4);
        }
        throw null;
    }

    public final String a() {
        return this.d;
    }

    public final JSONObject b() {
        try {
            Result.Companion companion = Result.Companion;
            JSONObject put = new JSONObject().put("messageType", "CReq").put("messageVersion", this.f82a).put("sdkTransID", this.d).put("threeDSServerTransID", this.b).put("acsTransID", this.c);
            if (this.f != null) {
                put.put("challengeCancel", this.f.f83a);
            }
            if (this.e != null) {
                put.put("challengeDataEntry", this.e);
            }
            if (this.g != null) {
                put.put("challengeHTMLDataEntry", this.g);
            }
            JSONArray a2 = MessageExtension.Companion.a(this.h);
            if (a2 != null) {
                put.put("messageExtensions", a2);
            }
            if (this.i != null) {
                put.put("oobContinue", this.i.booleanValue());
            }
            if (this.j != null) {
                put.put("resendChallenge", this.j.booleanValue() ? "Y" : "N");
            }
            Intrinsics.checkExpressionValueIsNotNull(put, "json");
            return put;
        } catch (Throwable th) {
            Result.Companion companion2 = Result.Companion;
            Throwable r0 = Result.m7exceptionOrNullimpl(Result.m4constructorimpl(ResultKt.createFailure(th)));
            if (r0 == null) {
                throw null;
            }
            throw new SDKRuntimeException(new RuntimeException(r0));
        }
    }

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof a)) {
            return false;
        }
        a aVar = (a) obj;
        return Intrinsics.areEqual((Object) this.f82a, (Object) aVar.f82a) && Intrinsics.areEqual((Object) this.b, (Object) aVar.b) && Intrinsics.areEqual((Object) this.c, (Object) aVar.c) && Intrinsics.areEqual((Object) this.d, (Object) aVar.d) && Intrinsics.areEqual((Object) this.e, (Object) aVar.e) && Intrinsics.areEqual((Object) this.f, (Object) aVar.f) && Intrinsics.areEqual((Object) this.g, (Object) aVar.g) && Intrinsics.areEqual((Object) this.h, (Object) aVar.h) && Intrinsics.areEqual((Object) this.i, (Object) aVar.i) && Intrinsics.areEqual((Object) this.j, (Object) aVar.j);
    }

    public int hashCode() {
        String str = this.f82a;
        int i2 = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.b;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.c;
        int hashCode3 = (hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31;
        String str4 = this.d;
        int hashCode4 = (hashCode3 + (str4 != null ? str4.hashCode() : 0)) * 31;
        String str5 = this.e;
        int hashCode5 = (hashCode4 + (str5 != null ? str5.hashCode() : 0)) * 31;
        C0011a aVar = this.f;
        int hashCode6 = (hashCode5 + (aVar != null ? aVar.hashCode() : 0)) * 31;
        String str6 = this.g;
        int hashCode7 = (hashCode6 + (str6 != null ? str6.hashCode() : 0)) * 31;
        List<MessageExtension> list = this.h;
        int hashCode8 = (hashCode7 + (list != null ? list.hashCode() : 0)) * 31;
        Boolean bool = this.i;
        int hashCode9 = (hashCode8 + (bool != null ? bool.hashCode() : 0)) * 31;
        Boolean bool2 = this.j;
        if (bool2 != null) {
            i2 = bool2.hashCode();
        }
        return hashCode9 + i2;
    }

    public String toString() {
        return "ChallengeRequestData(messageVersion=" + this.f82a + ", threeDsServerTransId=" + this.b + ", acsTransId=" + this.c + ", sdkTransId=" + this.d + ", challengeDataEntry=" + this.e + ", cancelReason=" + this.f + ", challengeHtmlDataEntry=" + this.g + ", messageExtensions=" + this.h + ", oobContinue=" + this.i + ", shouldResendChallenge=" + this.j + ")";
    }

    public void writeToParcel(Parcel parcel, int i2) {
        Intrinsics.checkParameterIsNotNull(parcel, "parcel");
        parcel.writeString(this.f82a);
        parcel.writeString(this.b);
        parcel.writeString(this.c);
        parcel.writeString(this.d);
        parcel.writeString(this.e);
        C0011a aVar = this.f;
        if (aVar != null) {
            parcel.writeInt(1);
            parcel.writeString(aVar.name());
        } else {
            parcel.writeInt(0);
        }
        parcel.writeString(this.g);
        List<MessageExtension> list = this.h;
        if (list != null) {
            parcel.writeInt(1);
            parcel.writeInt(list.size());
            for (MessageExtension writeToParcel : list) {
                writeToParcel.writeToParcel(parcel, 0);
            }
        } else {
            parcel.writeInt(0);
        }
        Boolean bool = this.i;
        if (bool != null) {
            parcel.writeInt(1);
            parcel.writeInt(bool.booleanValue() ? 1 : 0);
        } else {
            parcel.writeInt(0);
        }
        Boolean bool2 = this.j;
        if (bool2 != null) {
            parcel.writeInt(1);
            parcel.writeInt(bool2.booleanValue() ? 1 : 0);
            return;
        }
        parcel.writeInt(0);
    }
}
