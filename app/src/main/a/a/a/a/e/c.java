package a.a.a.a.e;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.share.internal.MessengerShareContentUtility;
import com.stripe.android.model.Stripe3ds2AuthResult;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.json.JSONException;
import org.json.JSONObject;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000D\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u001e\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\b\b\u0018\u0000 62\u00020\u0001:\u000267Bk\b\u0000\u0012\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u0003\u0012\u0006\u0010\u0006\u001a\u00020\u0003\u0012\n\b\u0002\u0010\u0007\u001a\u0004\u0018\u00010\b\u0012\u0006\u0010\t\u001a\u00020\u0003\u0012\u0006\u0010\n\u001a\u00020\u0003\u0012\n\b\u0002\u0010\u000b\u001a\u0004\u0018\u00010\u0003\u0012\u0006\u0010\f\u001a\u00020\u0003\u0012\u0006\u0010\r\u001a\u00020\u0003¢\u0006\u0002\u0010\u000eJ\u000b\u0010\u001b\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\t\u0010\u001c\u001a\u00020\u0003HÆ\u0003J\u000b\u0010\u001d\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000b\u0010\u001e\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\t\u0010\u001f\u001a\u00020\u0003HÆ\u0003J\u000b\u0010 \u001a\u0004\u0018\u00010\bHÆ\u0003J\t\u0010!\u001a\u00020\u0003HÆ\u0003J\t\u0010\"\u001a\u00020\u0003HÆ\u0003J\u000b\u0010#\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\t\u0010$\u001a\u00020\u0003HÆ\u0003Jw\u0010%\u001a\u00020\u00002\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u00032\b\b\u0002\u0010\u0006\u001a\u00020\u00032\n\b\u0002\u0010\u0007\u001a\u0004\u0018\u00010\b2\b\b\u0002\u0010\t\u001a\u00020\u00032\b\b\u0002\u0010\n\u001a\u00020\u00032\n\b\u0002\u0010\u000b\u001a\u0004\u0018\u00010\u00032\b\b\u0002\u0010\f\u001a\u00020\u00032\b\b\u0002\u0010\r\u001a\u00020\u0003HÆ\u0001J\t\u0010&\u001a\u00020'HÖ\u0001J\u0013\u0010(\u001a\u00020)2\b\u0010*\u001a\u0004\u0018\u00010+HÖ\u0003J\t\u0010,\u001a\u00020'HÖ\u0001J\r\u0010-\u001a\u00020.H\u0000¢\u0006\u0002\b/J\t\u00100\u001a\u00020\u0003HÖ\u0001J\u0019\u00101\u001a\u0002022\u0006\u00103\u001a\u0002042\u0006\u00105\u001a\u00020'HÖ\u0001R\u0013\u0010\u0004\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010R\u0013\u0010\u0005\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u0010R\u0011\u0010\u0006\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u0010R\u0013\u0010\u0007\u001a\u0004\u0018\u00010\b¢\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u0014R\u0011\u0010\t\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0015\u0010\u0010R\u0011\u0010\n\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0016\u0010\u0010R\u0013\u0010\u000b\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0017\u0010\u0010R\u0011\u0010\f\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0018\u0010\u0010R\u0011\u0010\r\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0019\u0010\u0010R\u0013\u0010\u0002\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u001a\u0010\u0010¨\u00068"}, d2 = {"Lcom/stripe/android/stripe3ds2/transactions/ErrorData;", "Landroid/os/Parcelable;", "serverTransId", "", "acsTransId", "dsTransId", "errorCode", "errorComponent", "Lcom/stripe/android/stripe3ds2/transactions/ErrorData$ErrorComponent;", "errorDescription", "errorDetail", "errorMessageType", "messageVersion", "sdkTransId", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/stripe/android/stripe3ds2/transactions/ErrorData$ErrorComponent;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", "getAcsTransId", "()Ljava/lang/String;", "getDsTransId", "getErrorCode", "getErrorComponent", "()Lcom/stripe/android/stripe3ds2/transactions/ErrorData$ErrorComponent;", "getErrorDescription", "getErrorDetail", "getErrorMessageType", "getMessageVersion", "getSdkTransId", "getServerTransId", "component1", "component10", "component2", "component3", "component4", "component5", "component6", "component7", "component8", "component9", "copy", "describeContents", "", "equals", "", "other", "", "hashCode", "toJson", "Lorg/json/JSONObject;", "toJson$3ds2sdk_release", "toString", "writeToParcel", "", "parcel", "Landroid/os/Parcel;", "flags", "Companion", "ErrorComponent", "3ds2sdk_release"}, k = 1, mv = {1, 1, 16})
public final class c implements Parcelable {
    public static final Parcelable.Creator CREATOR = new b();
    public static final a k = new a();

    /* renamed from: a  reason: collision with root package name */
    public final String f85a;
    public final String b;
    public final String c;
    public final String d;
    public final C0012c e;
    public final String f;
    public final String g;
    public final String h;
    public final String i;
    public final String j;

    public static final class a {
        public final c a(JSONObject jSONObject) {
            C0012c cVar;
            Intrinsics.checkParameterIsNotNull(jSONObject, MessengerShareContentUtility.ATTACHMENT_PAYLOAD);
            String optString = jSONObject.optString("threeDSServerTransID");
            String optString2 = jSONObject.optString("acsTransID");
            String optString3 = jSONObject.optString("dsTransID");
            String optString4 = jSONObject.optString("errorCode");
            Intrinsics.checkExpressionValueIsNotNull(optString4, "payload.optString(FIELD_ERROR_CODE)");
            C0012c.a aVar = C0012c.d;
            String optString5 = jSONObject.optString("errorComponent");
            if (aVar != null) {
                C0012c[] values = C0012c.values();
                int length = values.length;
                int i = 0;
                while (true) {
                    if (i >= length) {
                        cVar = null;
                        break;
                    }
                    C0012c cVar2 = values[i];
                    if (Intrinsics.areEqual((Object) cVar2.f86a, (Object) optString5)) {
                        cVar = cVar2;
                        break;
                    }
                    i++;
                }
                String optString6 = jSONObject.optString("errorDescription");
                Intrinsics.checkExpressionValueIsNotNull(optString6, "payload.optString(FIELD_ERROR_DESCRIPTION)");
                String optString7 = jSONObject.optString("errorDetail");
                Intrinsics.checkExpressionValueIsNotNull(optString7, "payload.optString(FIELD_ERROR_DETAIL)");
                String optString8 = jSONObject.optString("errorMessageType");
                String optString9 = jSONObject.optString("messageVersion");
                Intrinsics.checkExpressionValueIsNotNull(optString9, "payload.optString(FIELD_MESSAGE_VERSION)");
                String optString10 = jSONObject.optString("sdkTransID");
                Intrinsics.checkExpressionValueIsNotNull(optString10, "payload.optString(FIELD_SDK_TRANS_ID)");
                return new c(optString, optString2, optString3, optString4, cVar, optString6, optString7, optString8, optString9, optString10);
            }
            throw null;
        }
    }

    public static class b implements Parcelable.Creator {
        public final Object createFromParcel(Parcel parcel) {
            C0012c cVar;
            Intrinsics.checkParameterIsNotNull(parcel, "in");
            String readString = parcel.readString();
            String readString2 = parcel.readString();
            String readString3 = parcel.readString();
            String readString4 = parcel.readString();
            if (parcel.readInt() != 0) {
                cVar = (C0012c) Enum.valueOf(C0012c.class, parcel.readString());
            } else {
                cVar = null;
            }
            return new c(readString, readString2, readString3, readString4, cVar, parcel.readString(), parcel.readString(), parcel.readString(), parcel.readString(), parcel.readString());
        }

        public final Object[] newArray(int i) {
            return new c[i];
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\u000e\n\u0002\b\t\b\u0001\u0018\u0000 \u000b2\b\u0012\u0004\u0012\u00020\u00000\u0001:\u0001\u000bB\u000f\b\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006j\u0002\b\u0007j\u0002\b\bj\u0002\b\tj\u0002\b\n¨\u0006\f"}, d2 = {"Lcom/stripe/android/stripe3ds2/transactions/ErrorData$ErrorComponent;", "", "code", "", "(Ljava/lang/String;ILjava/lang/String;)V", "getCode", "()Ljava/lang/String;", "ThreeDsSdk", "ThreeDsServer", "DirectoryServer", "Acs", "Companion", "3ds2sdk_release"}, k = 1, mv = {1, 1, 16})
    /* renamed from: a.a.a.a.e.c$c  reason: collision with other inner class name */
    public enum C0012c {
        ThreeDsSdk(Stripe3ds2AuthResult.Ares.VALUE_CHALLENGE);
        
        public static final a d = null;

        /* renamed from: a  reason: collision with root package name */
        public final String f86a;

        /* renamed from: a.a.a.a.e.c$c$a */
        public static final class a {
        }

        /* access modifiers changed from: public */
        static {
            d = new a();
        }

        /* access modifiers changed from: public */
        C0012c(String str) {
            this.f86a = str;
        }
    }

    public c(String str, String str2, String str3, String str4, C0012c cVar, String str5, String str6, String str7, String str8, String str9) {
        Intrinsics.checkParameterIsNotNull(str4, "errorCode");
        Intrinsics.checkParameterIsNotNull(str5, "errorDescription");
        Intrinsics.checkParameterIsNotNull(str6, "errorDetail");
        Intrinsics.checkParameterIsNotNull(str8, "messageVersion");
        Intrinsics.checkParameterIsNotNull(str9, "sdkTransId");
        this.f85a = str;
        this.b = str2;
        this.c = str3;
        this.d = str4;
        this.e = cVar;
        this.f = str5;
        this.g = str6;
        this.h = str7;
        this.i = str8;
        this.j = str9;
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* synthetic */ c(java.lang.String r15, java.lang.String r16, java.lang.String r17, java.lang.String r18, a.a.a.a.e.c.C0012c r19, java.lang.String r20, java.lang.String r21, java.lang.String r22, java.lang.String r23, java.lang.String r24, int r25) {
        /*
            r14 = this;
            r0 = r25
            r1 = r0 & 1
            r2 = 0
            if (r1 == 0) goto L_0x0009
            r4 = r2
            goto L_0x000a
        L_0x0009:
            r4 = r15
        L_0x000a:
            r1 = r0 & 2
            if (r1 == 0) goto L_0x0010
            r5 = r2
            goto L_0x0012
        L_0x0010:
            r5 = r16
        L_0x0012:
            r1 = r0 & 4
            if (r1 == 0) goto L_0x0018
            r6 = r2
            goto L_0x001a
        L_0x0018:
            r6 = r17
        L_0x001a:
            r1 = r0 & 16
            if (r1 == 0) goto L_0x0020
            r8 = r2
            goto L_0x0022
        L_0x0020:
            r8 = r19
        L_0x0022:
            r0 = r0 & 128(0x80, float:1.794E-43)
            if (r0 == 0) goto L_0x0028
            r11 = r2
            goto L_0x002a
        L_0x0028:
            r11 = r22
        L_0x002a:
            r3 = r14
            r7 = r18
            r9 = r20
            r10 = r21
            r12 = r23
            r13 = r24
            r3.<init>(r4, r5, r6, r7, r8, r9, r10, r11, r12, r13)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: a.a.a.a.e.c.<init>(java.lang.String, java.lang.String, java.lang.String, java.lang.String, a.a.a.a.e.c$c, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, int):void");
    }

    public final String a() {
        return this.d;
    }

    public final String b() {
        return this.g;
    }

    public final JSONObject c() throws JSONException {
        JSONObject put = new JSONObject().put("messageType", "Erro").put("messageVersion", this.i).put("sdkTransID", this.j).put("errorCode", this.d).put("errorDescription", this.f).put("errorDetail", this.g);
        String str = this.f85a;
        if (str != null) {
            put.put("threeDSServerTransID", str);
        }
        String str2 = this.b;
        if (str2 != null) {
            put.put("acsTransID", str2);
        }
        String str3 = this.c;
        if (str3 != null) {
            put.put("dsTransID", str3);
        }
        C0012c cVar = this.e;
        if (cVar != null) {
            put.put("errorComponent", cVar.f86a);
        }
        String str4 = this.h;
        if (str4 != null) {
            put.put("errorMessageType", str4);
        }
        Intrinsics.checkExpressionValueIsNotNull(put, "json");
        return put;
    }

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof c)) {
            return false;
        }
        c cVar = (c) obj;
        return Intrinsics.areEqual((Object) this.f85a, (Object) cVar.f85a) && Intrinsics.areEqual((Object) this.b, (Object) cVar.b) && Intrinsics.areEqual((Object) this.c, (Object) cVar.c) && Intrinsics.areEqual((Object) this.d, (Object) cVar.d) && Intrinsics.areEqual((Object) this.e, (Object) cVar.e) && Intrinsics.areEqual((Object) this.f, (Object) cVar.f) && Intrinsics.areEqual((Object) this.g, (Object) cVar.g) && Intrinsics.areEqual((Object) this.h, (Object) cVar.h) && Intrinsics.areEqual((Object) this.i, (Object) cVar.i) && Intrinsics.areEqual((Object) this.j, (Object) cVar.j);
    }

    public int hashCode() {
        String str = this.f85a;
        int i2 = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.b;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.c;
        int hashCode3 = (hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31;
        String str4 = this.d;
        int hashCode4 = (hashCode3 + (str4 != null ? str4.hashCode() : 0)) * 31;
        C0012c cVar = this.e;
        int hashCode5 = (hashCode4 + (cVar != null ? cVar.hashCode() : 0)) * 31;
        String str5 = this.f;
        int hashCode6 = (hashCode5 + (str5 != null ? str5.hashCode() : 0)) * 31;
        String str6 = this.g;
        int hashCode7 = (hashCode6 + (str6 != null ? str6.hashCode() : 0)) * 31;
        String str7 = this.h;
        int hashCode8 = (hashCode7 + (str7 != null ? str7.hashCode() : 0)) * 31;
        String str8 = this.i;
        int hashCode9 = (hashCode8 + (str8 != null ? str8.hashCode() : 0)) * 31;
        String str9 = this.j;
        if (str9 != null) {
            i2 = str9.hashCode();
        }
        return hashCode9 + i2;
    }

    public String toString() {
        return "ErrorData(serverTransId=" + this.f85a + ", acsTransId=" + this.b + ", dsTransId=" + this.c + ", errorCode=" + this.d + ", errorComponent=" + this.e + ", errorDescription=" + this.f + ", errorDetail=" + this.g + ", errorMessageType=" + this.h + ", messageVersion=" + this.i + ", sdkTransId=" + this.j + ")";
    }

    public void writeToParcel(Parcel parcel, int i2) {
        Intrinsics.checkParameterIsNotNull(parcel, "parcel");
        parcel.writeString(this.f85a);
        parcel.writeString(this.b);
        parcel.writeString(this.c);
        parcel.writeString(this.d);
        C0012c cVar = this.e;
        if (cVar != null) {
            parcel.writeInt(1);
            parcel.writeString(cVar.name());
        } else {
            parcel.writeInt(0);
        }
        parcel.writeString(this.f);
        parcel.writeString(this.g);
        parcel.writeString(this.h);
        parcel.writeString(this.i);
        parcel.writeString(this.j);
    }
}
