package a.a.a.a.e;

import com.iqonic.store.utils.Constants;

public enum d {
    InvalidMessageReceived(101, "Message is not AReq, ARes, CReq, CRes, PReq, PRes, RReq, or RRes"),
    UnsupportedMessageVersion(102, "Message Version Number received is not valid for the receiving component."),
    RequiredDataElementMissing(201, "A message element required as defined in Table A.1 is missing from the message."),
    UnrecognizedCriticalMessageExtensions(Constants.RequestCode.COUPON_CODE, "Critical message extension not recognised."),
    InvalidDataElementFormat(203, "Data element not in the required format or value is invalid as defined in Table A.1"),
    InvalidTransactionId(301, "Transaction ID received is not valid for the receiving component."),
    DataDecryptionFailure(302, "Data could not be decrypted by the receiving system due to technical or other reason."),
    TransactionTimedout(402, "Transaction timed-out.");
    

    /* renamed from: a  reason: collision with root package name */
    public final int f87a;
    public final String b;

    /* access modifiers changed from: public */
    d(int i, String str) {
        this.f87a = i;
        this.b = str;
    }

    public final int a() {
        return this.f87a;
    }

    public final String b() {
        return this.b;
    }
}
