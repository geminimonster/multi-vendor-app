package a.a.a.a.e;

import kotlin.Metadata;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\t\b\u0000\u0018\u0000 \u00112\u00060\u0001j\u0002`\u0002:\u0001\u0011B\u0017\b\u0016\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006¢\u0006\u0002\u0010\u0007B\u001f\b\u0000\u0012\u0006\u0010\b\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u0006\u0012\u0006\u0010\u0005\u001a\u00020\u0006¢\u0006\u0002\u0010\u000bR\u0011\u0010\b\u001a\u00020\t¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\rR\u0011\u0010\n\u001a\u00020\u0006¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u000fR\u0011\u0010\u0005\u001a\u00020\u0006¢\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u000f¨\u0006\u0012"}, d2 = {"Lcom/stripe/android/stripe3ds2/transactions/ChallengeResponseParseException;", "Ljava/lang/Exception;", "Lkotlin/Exception;", "protocolError", "Lcom/stripe/android/stripe3ds2/transactions/ProtocolError;", "detail", "", "(Lcom/stripe/android/stripe3ds2/transactions/ProtocolError;Ljava/lang/String;)V", "code", "", "description", "(ILjava/lang/String;Ljava/lang/String;)V", "getCode", "()I", "getDescription", "()Ljava/lang/String;", "getDetail", "Companion", "3ds2sdk_release"}, k = 1, mv = {1, 1, 16})
public final class b extends Exception {
    public static final a d = new a();

    /* renamed from: a  reason: collision with root package name */
    public final int f84a;
    public final String b;
    public final String c;

    public static final class a {
        @JvmStatic
        public final b a(String str) {
            Intrinsics.checkParameterIsNotNull(str, "fieldName");
            return new b(d.InvalidDataElementFormat.f87a, "Data element not in the required format or value is invalid as defined in Table A.1", str);
        }

        @JvmStatic
        public final b b(String str) {
            Intrinsics.checkParameterIsNotNull(str, "fieldName");
            return new b(d.RequiredDataElementMissing.f87a, "A message element required as defined in Table A.1 is missing from the message.", str);
        }
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public b(int i, String str, String str2) {
        super(i + " - " + str + " (" + str2 + ')');
        Intrinsics.checkParameterIsNotNull(str, "description");
        Intrinsics.checkParameterIsNotNull(str2, "detail");
        this.f84a = i;
        this.b = str;
        this.c = str2;
    }
}
