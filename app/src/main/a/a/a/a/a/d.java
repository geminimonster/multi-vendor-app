package a.a.a.a.a;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import androidx.viewbinding.ViewBinding;
import com.stripe.android.stripe3ds2.R;
import com.stripe.android.stripe3ds2.views.ThreeDS2TextView;
import org.bouncycastle.jcajce.util.AnnotatedPrivateKey;

public final class d implements ViewBinding {

    /* renamed from: a  reason: collision with root package name */
    public final LinearLayout f9a;
    public final ThreeDS2TextView b;
    public final LinearLayout c;

    public d(LinearLayout linearLayout, ThreeDS2TextView threeDS2TextView, LinearLayout linearLayout2) {
        this.f9a = linearLayout;
        this.b = threeDS2TextView;
        this.c = linearLayout2;
    }

    public static d a(LayoutInflater layoutInflater, ViewGroup viewGroup, boolean z) {
        String str;
        View inflate = layoutInflater.inflate(R.layout.challenge_zone_multi_select_view, viewGroup, false);
        if (z) {
            viewGroup.addView(inflate);
        }
        ThreeDS2TextView threeDS2TextView = (ThreeDS2TextView) inflate.findViewById(R.id.label);
        if (threeDS2TextView != null) {
            LinearLayout linearLayout = (LinearLayout) inflate.findViewById(R.id.select_group);
            if (linearLayout != null) {
                return new d((LinearLayout) inflate, threeDS2TextView, linearLayout);
            }
            str = "selectGroup";
        } else {
            str = AnnotatedPrivateKey.LABEL;
        }
        throw new NullPointerException("Missing required view with ID: ".concat(str));
    }

    public View getRoot() {
        return this.f9a;
    }
}
