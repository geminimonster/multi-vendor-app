package a.a.a.a.a;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.viewbinding.ViewBinding;
import com.stripe.android.stripe3ds2.R;

public final class a implements ViewBinding {

    /* renamed from: a  reason: collision with root package name */
    public final View f6a;
    public final ImageView b;
    public final ImageView c;

    public a(View view, ImageView imageView, ImageView imageView2) {
        this.f6a = view;
        this.b = imageView;
        this.c = imageView2;
    }

    public static a a(LayoutInflater layoutInflater, ViewGroup viewGroup) {
        String str;
        if (viewGroup != null) {
            layoutInflater.inflate(R.layout.brand_zone_view, viewGroup);
            ImageView imageView = (ImageView) viewGroup.findViewById(R.id.issuer_image);
            if (imageView != null) {
                ImageView imageView2 = (ImageView) viewGroup.findViewById(R.id.payment_system_image);
                if (imageView2 != null) {
                    return new a(viewGroup, imageView, imageView2);
                }
                str = "paymentSystemImage";
            } else {
                str = "issuerImage";
            }
            throw new NullPointerException("Missing required view with ID: ".concat(str));
        }
        throw new NullPointerException("parent");
    }

    public View getRoot() {
        return this.f6a;
    }
}
