package a.a.a.a.a;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import androidx.viewbinding.ViewBinding;
import com.stripe.android.stripe3ds2.R;
import com.stripe.android.stripe3ds2.views.ThreeDS2Button;
import com.stripe.android.stripe3ds2.views.ThreeDS2HeaderTextView;
import com.stripe.android.stripe3ds2.views.ThreeDS2TextView;

public final class g implements ViewBinding {

    /* renamed from: a  reason: collision with root package name */
    public final View f12a;
    public final FrameLayout b;
    public final ThreeDS2HeaderTextView c;
    public final ThreeDS2TextView d;
    public final ThreeDS2Button e;
    public final ThreeDS2Button f;
    public final RadioButton g;
    public final RadioGroup h;
    public final RadioButton i;
    public final ThreeDS2TextView j;

    public g(View view, FrameLayout frameLayout, ThreeDS2HeaderTextView threeDS2HeaderTextView, ThreeDS2TextView threeDS2TextView, ThreeDS2Button threeDS2Button, ThreeDS2Button threeDS2Button2, RadioButton radioButton, RadioGroup radioGroup, RadioButton radioButton2, ThreeDS2TextView threeDS2TextView2) {
        this.f12a = view;
        this.b = frameLayout;
        this.c = threeDS2HeaderTextView;
        this.d = threeDS2TextView;
        this.e = threeDS2Button;
        this.f = threeDS2Button2;
        this.g = radioButton;
        this.h = radioGroup;
        this.i = radioButton2;
        this.j = threeDS2TextView2;
    }

    public static g a(LayoutInflater layoutInflater, ViewGroup viewGroup) {
        String str;
        if (viewGroup != null) {
            layoutInflater.inflate(R.layout.challenge_zone_view, viewGroup);
            FrameLayout frameLayout = (FrameLayout) viewGroup.findViewById(R.id.czv_entry_view);
            if (frameLayout != null) {
                ThreeDS2HeaderTextView threeDS2HeaderTextView = (ThreeDS2HeaderTextView) viewGroup.findViewById(R.id.czv_header);
                if (threeDS2HeaderTextView != null) {
                    ThreeDS2TextView threeDS2TextView = (ThreeDS2TextView) viewGroup.findViewById(R.id.czv_info);
                    if (threeDS2TextView != null) {
                        ThreeDS2Button threeDS2Button = (ThreeDS2Button) viewGroup.findViewById(R.id.czv_resend_button);
                        if (threeDS2Button != null) {
                            ThreeDS2Button threeDS2Button2 = (ThreeDS2Button) viewGroup.findViewById(R.id.czv_submit_button);
                            if (threeDS2Button2 != null) {
                                RadioButton radioButton = (RadioButton) viewGroup.findViewById(R.id.czv_whitelist_no_button);
                                if (radioButton != null) {
                                    RadioGroup radioGroup = (RadioGroup) viewGroup.findViewById(R.id.czv_whitelist_radio_group);
                                    if (radioGroup != null) {
                                        RadioButton radioButton2 = (RadioButton) viewGroup.findViewById(R.id.czv_whitelist_yes_button);
                                        if (radioButton2 != null) {
                                            ThreeDS2TextView threeDS2TextView2 = (ThreeDS2TextView) viewGroup.findViewById(R.id.czv_whitelisting_label);
                                            if (threeDS2TextView2 != null) {
                                                return new g(viewGroup, frameLayout, threeDS2HeaderTextView, threeDS2TextView, threeDS2Button, threeDS2Button2, radioButton, radioGroup, radioButton2, threeDS2TextView2);
                                            }
                                            str = "czvWhitelistingLabel";
                                        } else {
                                            str = "czvWhitelistYesButton";
                                        }
                                    } else {
                                        str = "czvWhitelistRadioGroup";
                                    }
                                } else {
                                    str = "czvWhitelistNoButton";
                                }
                            } else {
                                str = "czvSubmitButton";
                            }
                        } else {
                            str = "czvResendButton";
                        }
                    } else {
                        str = "czvInfo";
                    }
                } else {
                    str = "czvHeader";
                }
            } else {
                str = "czvEntryView";
            }
            throw new NullPointerException("Missing required view with ID: ".concat(str));
        }
        throw new NullPointerException("parent");
    }

    public View getRoot() {
        return this.f12a;
    }
}
