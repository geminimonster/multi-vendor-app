package a.a.a.a.a;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.viewbinding.ViewBinding;
import com.stripe.android.stripe3ds2.R;
import com.stripe.android.stripe3ds2.views.ThreeDS2TextView;

public final class i implements ViewBinding {

    /* renamed from: a  reason: collision with root package name */
    public final LinearLayout f14a;
    public final AppCompatImageView b;
    public final LinearLayout c;
    public final ThreeDS2TextView d;
    public final ThreeDS2TextView e;
    public final AppCompatImageView f;
    public final LinearLayout g;
    public final ThreeDS2TextView h;
    public final ThreeDS2TextView i;

    public i(LinearLayout linearLayout, AppCompatImageView appCompatImageView, LinearLayout linearLayout2, ThreeDS2TextView threeDS2TextView, ThreeDS2TextView threeDS2TextView2, AppCompatImageView appCompatImageView2, LinearLayout linearLayout3, ThreeDS2TextView threeDS2TextView3, ThreeDS2TextView threeDS2TextView4) {
        this.f14a = linearLayout;
        this.b = appCompatImageView;
        this.c = linearLayout2;
        this.d = threeDS2TextView;
        this.e = threeDS2TextView2;
        this.f = appCompatImageView2;
        this.g = linearLayout3;
        this.h = threeDS2TextView3;
        this.i = threeDS2TextView4;
    }

    public static i a(LayoutInflater layoutInflater, ViewGroup viewGroup, boolean z) {
        String str;
        View inflate = layoutInflater.inflate(R.layout.information_zone_view, viewGroup, false);
        if (z) {
            viewGroup.addView(inflate);
        }
        AppCompatImageView appCompatImageView = (AppCompatImageView) inflate.findViewById(R.id.expand_arrow);
        if (appCompatImageView != null) {
            LinearLayout linearLayout = (LinearLayout) inflate.findViewById(R.id.expand_container);
            if (linearLayout != null) {
                ThreeDS2TextView threeDS2TextView = (ThreeDS2TextView) inflate.findViewById(R.id.expand_label);
                if (threeDS2TextView != null) {
                    ThreeDS2TextView threeDS2TextView2 = (ThreeDS2TextView) inflate.findViewById(R.id.expand_text);
                    if (threeDS2TextView2 != null) {
                        AppCompatImageView appCompatImageView2 = (AppCompatImageView) inflate.findViewById(R.id.why_arrow);
                        if (appCompatImageView2 != null) {
                            LinearLayout linearLayout2 = (LinearLayout) inflate.findViewById(R.id.why_container);
                            if (linearLayout2 != null) {
                                ThreeDS2TextView threeDS2TextView3 = (ThreeDS2TextView) inflate.findViewById(R.id.why_label);
                                if (threeDS2TextView3 != null) {
                                    ThreeDS2TextView threeDS2TextView4 = (ThreeDS2TextView) inflate.findViewById(R.id.why_text);
                                    if (threeDS2TextView4 != null) {
                                        return new i((LinearLayout) inflate, appCompatImageView, linearLayout, threeDS2TextView, threeDS2TextView2, appCompatImageView2, linearLayout2, threeDS2TextView3, threeDS2TextView4);
                                    }
                                    str = "whyText";
                                } else {
                                    str = "whyLabel";
                                }
                            } else {
                                str = "whyContainer";
                            }
                        } else {
                            str = "whyArrow";
                        }
                    } else {
                        str = "expandText";
                    }
                } else {
                    str = "expandLabel";
                }
            } else {
                str = "expandContainer";
            }
        } else {
            str = "expandArrow";
        }
        throw new NullPointerException("Missing required view with ID: ".concat(str));
    }

    public View getRoot() {
        return this.f14a;
    }
}
