package a.a.a.a.a;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.viewbinding.ViewBinding;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.stripe.android.stripe3ds2.R;
import org.bouncycastle.jcajce.util.AnnotatedPrivateKey;

public final class f implements ViewBinding {

    /* renamed from: a  reason: collision with root package name */
    public final TextInputLayout f11a;
    public final TextInputLayout b;
    public final TextInputEditText c;

    public f(TextInputLayout textInputLayout, TextInputLayout textInputLayout2, TextInputEditText textInputEditText) {
        this.f11a = textInputLayout;
        this.b = textInputLayout2;
        this.c = textInputEditText;
    }

    public static f a(LayoutInflater layoutInflater, ViewGroup viewGroup, boolean z) {
        String str;
        View inflate = layoutInflater.inflate(R.layout.challenge_zone_text_view, viewGroup, false);
        if (z) {
            viewGroup.addView(inflate);
        }
        TextInputLayout textInputLayout = (TextInputLayout) inflate.findViewById(R.id.label);
        if (textInputLayout != null) {
            TextInputEditText textInputEditText = (TextInputEditText) inflate.findViewById(R.id.text_entry);
            if (textInputEditText != null) {
                return new f((TextInputLayout) inflate, textInputLayout, textInputEditText);
            }
            str = "textEntry";
        } else {
            str = AnnotatedPrivateKey.LABEL;
        }
        throw new NullPointerException("Missing required view with ID: ".concat(str));
    }

    public View getRoot() {
        return this.f11a;
    }
}
