package a.a.a.a.a;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import androidx.viewbinding.ViewBinding;
import com.stripe.android.stripe3ds2.R;
import com.stripe.android.stripe3ds2.views.ThreeDS2TextView;
import org.bouncycastle.jcajce.util.AnnotatedPrivateKey;

public final class e implements ViewBinding {

    /* renamed from: a  reason: collision with root package name */
    public final LinearLayout f10a;
    public final ThreeDS2TextView b;
    public final RadioGroup c;

    public e(LinearLayout linearLayout, ThreeDS2TextView threeDS2TextView, RadioGroup radioGroup) {
        this.f10a = linearLayout;
        this.b = threeDS2TextView;
        this.c = radioGroup;
    }

    public static e a(LayoutInflater layoutInflater, ViewGroup viewGroup, boolean z) {
        String str;
        View inflate = layoutInflater.inflate(R.layout.challenge_zone_single_select_view, viewGroup, false);
        if (z) {
            viewGroup.addView(inflate);
        }
        ThreeDS2TextView threeDS2TextView = (ThreeDS2TextView) inflate.findViewById(R.id.label);
        if (threeDS2TextView != null) {
            RadioGroup radioGroup = (RadioGroup) inflate.findViewById(R.id.select_group);
            if (radioGroup != null) {
                return new e((LinearLayout) inflate, threeDS2TextView, radioGroup);
            }
            str = "selectGroup";
        } else {
            str = AnnotatedPrivateKey.LABEL;
        }
        throw new NullPointerException("Missing required view with ID: ".concat(str));
    }

    public View getRoot() {
        return this.f10a;
    }
}
