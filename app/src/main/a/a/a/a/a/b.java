package a.a.a.a.a;

import android.view.View;
import android.widget.ScrollView;
import androidx.viewbinding.ViewBinding;
import com.stripe.android.stripe3ds2.views.BrandZoneView;
import com.stripe.android.stripe3ds2.views.ChallengeZoneView;
import com.stripe.android.stripe3ds2.views.InformationZoneView;

public final class b implements ViewBinding {

    /* renamed from: a  reason: collision with root package name */
    public final ScrollView f7a;
    public final BrandZoneView b;
    public final ChallengeZoneView c;
    public final InformationZoneView d;

    public b(ScrollView scrollView, BrandZoneView brandZoneView, ChallengeZoneView challengeZoneView, InformationZoneView informationZoneView) {
        this.f7a = scrollView;
        this.b = brandZoneView;
        this.c = challengeZoneView;
        this.d = informationZoneView;
    }

    public View getRoot() {
        return this.f7a;
    }
}
