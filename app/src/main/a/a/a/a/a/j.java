package a.a.a.a.a;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import androidx.viewbinding.ViewBinding;
import com.stripe.android.stripe3ds2.R;

public final class j implements ViewBinding {

    /* renamed from: a  reason: collision with root package name */
    public final LinearLayout f15a;
    public final ImageView b;
    public final ProgressBar c;

    public j(LinearLayout linearLayout, ImageView imageView, ProgressBar progressBar) {
        this.f15a = linearLayout;
        this.b = imageView;
        this.c = progressBar;
    }

    public static j a(LayoutInflater layoutInflater) {
        String str;
        View inflate = layoutInflater.inflate(R.layout.progress_view_layout, (ViewGroup) null, false);
        ImageView imageView = (ImageView) inflate.findViewById(R.id.brand_logo);
        if (imageView != null) {
            ProgressBar progressBar = (ProgressBar) inflate.findViewById(R.id.progress_bar);
            if (progressBar != null) {
                return new j((LinearLayout) inflate, imageView, progressBar);
            }
            str = "progressBar";
        } else {
            str = "brandLogo";
        }
        throw new NullPointerException("Missing required view with ID: ".concat(str));
    }

    public View getRoot() {
        return this.f15a;
    }
}
