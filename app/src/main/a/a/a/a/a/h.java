package a.a.a.a.a;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.viewbinding.ViewBinding;
import com.stripe.android.stripe3ds2.R;
import com.stripe.android.stripe3ds2.views.ThreeDS2WebView;

public final class h implements ViewBinding {

    /* renamed from: a  reason: collision with root package name */
    public final View f13a;
    public final ThreeDS2WebView b;

    public h(View view, ThreeDS2WebView threeDS2WebView) {
        this.f13a = view;
        this.b = threeDS2WebView;
    }

    public static h a(LayoutInflater layoutInflater, ViewGroup viewGroup) {
        if (viewGroup != null) {
            layoutInflater.inflate(R.layout.challenge_zone_web_view, viewGroup);
            ThreeDS2WebView threeDS2WebView = (ThreeDS2WebView) viewGroup.findViewById(R.id.web_view);
            if (threeDS2WebView != null) {
                return new h(viewGroup, threeDS2WebView);
            }
            throw new NullPointerException("Missing required view with ID: ".concat("webView"));
        }
        throw new NullPointerException("parent");
    }

    public View getRoot() {
        return this.f13a;
    }
}
