package a.a.a.a.a;

import android.view.View;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import androidx.viewbinding.ViewBinding;

public final class c implements ViewBinding {

    /* renamed from: a  reason: collision with root package name */
    public final FrameLayout f8a;
    public final ProgressBar b;

    public c(FrameLayout frameLayout, ProgressBar progressBar) {
        this.f8a = frameLayout;
        this.b = progressBar;
    }

    public View getRoot() {
        return this.f8a;
    }
}
